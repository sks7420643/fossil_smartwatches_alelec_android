package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xb3 {
    @DexIgnore
    public /* final */ Fc3 a;

    @DexIgnore
    public Xb3(Fc3 fc3) {
        this.a = fc3;
    }

    @DexIgnore
    public final boolean a() {
        try {
            return this.a.A0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean b() {
        try {
            return this.a.l1();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean c() {
        try {
            return this.a.D0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean d() {
        try {
            return this.a.U();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean e() {
        try {
            return this.a.r2();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean f() {
        try {
            return this.a.e0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean g() {
        try {
            return this.a.J2();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean h() {
        try {
            return this.a.f0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.a.setCompassEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void j(boolean z) {
        try {
            this.a.setMapToolbarEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void k(boolean z) {
        try {
            this.a.setMyLocationButtonEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void l(boolean z) {
        try {
            this.a.setRotateGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void m(boolean z) {
        try {
            this.a.setScrollGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void n(boolean z) {
        try {
            this.a.setTiltGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void o(boolean z) {
        try {
            this.a.setZoomControlsEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void p(boolean z) {
        try {
            this.a.setZoomGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }
}
