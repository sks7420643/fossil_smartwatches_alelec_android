package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sx2<E> extends Tx2<E> implements List<E>, RandomAccess {
    @DexIgnore
    public static /* final */ Bz2<Object> c; // = new Vx2(Qy2.zza, 0);

    @DexIgnore
    public static <E> Sx2<E> zza() {
        return (Sx2<E>) Qy2.zza;
    }

    @DexIgnore
    public static <E> Sx2<E> zza(E e) {
        Object[] objArr = {e};
        for (int i = 0; i < 1; i++) {
            Oy2.a(objArr[i], i);
        }
        return zza(objArr, 1);
    }

    @DexIgnore
    public static <E> Sx2<E> zza(Object[] objArr) {
        return zza(objArr, objArr.length);
    }

    @DexIgnore
    public static <E> Sx2<E> zza(Object[] objArr, int i) {
        return i == 0 ? (Sx2<E>) Qy2.zza : new Qy2(objArr, i);
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public boolean contains(@NullableDecl Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        Sw2.b(this);
        if (obj == this) {
            return true;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            int size = size();
            if (size == list.size()) {
                if (list instanceof RandomAccess) {
                    for (int i = 0; i < size; i++) {
                        if (Qw2.a(get(i), list.get(i))) {
                        }
                    }
                    return true;
                }
                int size2 = size();
                Iterator<E> it = list.iterator();
                int i2 = 0;
                while (true) {
                    if (i2 < size2) {
                        if (!it.hasNext()) {
                            break;
                        }
                        E e = get(i2);
                        i2++;
                        if (!Qw2.a(e, it.next())) {
                            break;
                        }
                    } else if (!it.hasNext()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    @DexIgnore
    public int indexOf(@NullableDecl Object obj) {
        int i = 0;
        if (obj == null) {
            return -1;
        }
        int size = size();
        if (obj == null) {
            while (i < size) {
                if (get(i) == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        while (i < size) {
            if (obj.equals(get(i))) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.Tx2, java.util.AbstractCollection, java.util.List, java.util.Collection, java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    public int lastIndexOf(@NullableDecl Object obj) {
        if (obj == null) {
            return -1;
        }
        if (obj == null) {
            for (int size = size() - 1; size >= 0; size--) {
                if (get(size) == null) {
                    return size;
                }
            }
            return -1;
        }
        for (int size2 = size() - 1; size2 >= 0; size2--) {
            if (obj.equals(get(size2))) {
                return size2;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ ListIterator listIterator() {
        return (Bz2) listIterator(0);
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ ListIterator listIterator(int i) {
        Sw2.g(i, size());
        return isEmpty() ? c : new Vx2(this, i);
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ List subList(int i, int i2) {
        return zza(i, i2);
    }

    @DexIgnore
    public Sx2<E> zza(int i, int i2) {
        Sw2.e(i, i2, size());
        int i3 = i2 - i;
        return i3 == size() ? this : i3 == 0 ? (Sx2<E>) Qy2.zza : new Xx2(this, i, i3);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public int zzb(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Cz2<E> zzb() {
        return (Bz2) listIterator();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Sx2<E> zzc() {
        return this;
    }

    @DexIgnore
    public Sx2<E> zzd() {
        return size() <= 1 ? this : new Ux2(this);
    }
}
