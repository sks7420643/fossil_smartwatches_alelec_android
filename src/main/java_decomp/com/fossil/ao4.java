package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ao4 {
    @DexIgnore
    public static /* final */ Ao4[] d; // = a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Bi[] b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ai(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.a;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Ai[] b;

        @DexIgnore
        public Bi(int i, Ai... aiArr) {
            this.a = i;
            this.b = aiArr;
        }

        @DexIgnore
        public Ai[] a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }

        @DexIgnore
        public int c() {
            int i = 0;
            for (Ai ai : this.b) {
                i += ai.a();
            }
            return i;
        }

        @DexIgnore
        public int d() {
            return this.a * c();
        }
    }

    @DexIgnore
    public Ao4(int i, int[] iArr, Bi... biArr) {
        this.a = i;
        this.b = biArr;
        int b2 = biArr[0].b();
        Ai[] a2 = biArr[0].a();
        int i2 = 0;
        for (Ai ai : a2) {
            i2 += (ai.b() + b2) * ai.a();
        }
        this.c = i2;
    }

    @DexIgnore
    public static Ao4[] a() {
        return new Ao4[]{new Ao4(1, new int[0], new Bi(7, new Ai(1, 19)), new Bi(10, new Ai(1, 16)), new Bi(13, new Ai(1, 13)), new Bi(17, new Ai(1, 9))), new Ao4(2, new int[]{6, 18}, new Bi(10, new Ai(1, 34)), new Bi(16, new Ai(1, 28)), new Bi(22, new Ai(1, 22)), new Bi(28, new Ai(1, 16))), new Ao4(3, new int[]{6, 22}, new Bi(15, new Ai(1, 55)), new Bi(26, new Ai(1, 44)), new Bi(18, new Ai(2, 17)), new Bi(22, new Ai(2, 13))), new Ao4(4, new int[]{6, 26}, new Bi(20, new Ai(1, 80)), new Bi(18, new Ai(2, 32)), new Bi(26, new Ai(2, 24)), new Bi(16, new Ai(4, 9))), new Ao4(5, new int[]{6, 30}, new Bi(26, new Ai(1, 108)), new Bi(24, new Ai(2, 43)), new Bi(18, new Ai(2, 15), new Ai(2, 16)), new Bi(22, new Ai(2, 11), new Ai(2, 12))), new Ao4(6, new int[]{6, 34}, new Bi(18, new Ai(2, 68)), new Bi(16, new Ai(4, 27)), new Bi(24, new Ai(4, 19)), new Bi(28, new Ai(4, 15))), new Ao4(7, new int[]{6, 22, 38}, new Bi(20, new Ai(2, 78)), new Bi(18, new Ai(4, 31)), new Bi(18, new Ai(2, 14), new Ai(4, 15)), new Bi(26, new Ai(4, 13), new Ai(1, 14))), new Ao4(8, new int[]{6, 24, 42}, new Bi(24, new Ai(2, 97)), new Bi(22, new Ai(2, 38), new Ai(2, 39)), new Bi(22, new Ai(4, 18), new Ai(2, 19)), new Bi(26, new Ai(4, 14), new Ai(2, 15))), new Ao4(9, new int[]{6, 26, 46}, new Bi(30, new Ai(2, 116)), new Bi(22, new Ai(3, 36), new Ai(2, 37)), new Bi(20, new Ai(4, 16), new Ai(4, 17)), new Bi(24, new Ai(4, 12), new Ai(4, 13))), new Ao4(10, new int[]{6, 28, 50}, new Bi(18, new Ai(2, 68), new Ai(2, 69)), new Bi(26, new Ai(4, 43), new Ai(1, 44)), new Bi(24, new Ai(6, 19), new Ai(2, 20)), new Bi(28, new Ai(6, 15), new Ai(2, 16))), new Ao4(11, new int[]{6, 30, 54}, new Bi(20, new Ai(4, 81)), new Bi(30, new Ai(1, 50), new Ai(4, 51)), new Bi(28, new Ai(4, 22), new Ai(4, 23)), new Bi(24, new Ai(3, 12), new Ai(8, 13))), new Ao4(12, new int[]{6, 32, 58}, new Bi(24, new Ai(2, 92), new Ai(2, 93)), new Bi(22, new Ai(6, 36), new Ai(2, 37)), new Bi(26, new Ai(4, 20), new Ai(6, 21)), new Bi(28, new Ai(7, 14), new Ai(4, 15))), new Ao4(13, new int[]{6, 34, 62}, new Bi(26, new Ai(4, 107)), new Bi(22, new Ai(8, 37), new Ai(1, 38)), new Bi(24, new Ai(8, 20), new Ai(4, 21)), new Bi(22, new Ai(12, 11), new Ai(4, 12))), new Ao4(14, new int[]{6, 26, 46, 66}, new Bi(30, new Ai(3, 115), new Ai(1, 116)), new Bi(24, new Ai(4, 40), new Ai(5, 41)), new Bi(20, new Ai(11, 16), new Ai(5, 17)), new Bi(24, new Ai(11, 12), new Ai(5, 13))), new Ao4(15, new int[]{6, 26, 48, 70}, new Bi(22, new Ai(5, 87), new Ai(1, 88)), new Bi(24, new Ai(5, 41), new Ai(5, 42)), new Bi(30, new Ai(5, 24), new Ai(7, 25)), new Bi(24, new Ai(11, 12), new Ai(7, 13))), new Ao4(16, new int[]{6, 26, 50, 74}, new Bi(24, new Ai(5, 98), new Ai(1, 99)), new Bi(28, new Ai(7, 45), new Ai(3, 46)), new Bi(24, new Ai(15, 19), new Ai(2, 20)), new Bi(30, new Ai(3, 15), new Ai(13, 16))), new Ao4(17, new int[]{6, 30, 54, 78}, new Bi(28, new Ai(1, 107), new Ai(5, 108)), new Bi(28, new Ai(10, 46), new Ai(1, 47)), new Bi(28, new Ai(1, 22), new Ai(15, 23)), new Bi(28, new Ai(2, 14), new Ai(17, 15))), new Ao4(18, new int[]{6, 30, 56, 82}, new Bi(30, new Ai(5, 120), new Ai(1, 121)), new Bi(26, new Ai(9, 43), new Ai(4, 44)), new Bi(28, new Ai(17, 22), new Ai(1, 23)), new Bi(28, new Ai(2, 14), new Ai(19, 15))), new Ao4(19, new int[]{6, 30, 58, 86}, new Bi(28, new Ai(3, 113), new Ai(4, 114)), new Bi(26, new Ai(3, 44), new Ai(11, 45)), new Bi(26, new Ai(17, 21), new Ai(4, 22)), new Bi(26, new Ai(9, 13), new Ai(16, 14))), new Ao4(20, new int[]{6, 34, 62, 90}, new Bi(28, new Ai(3, 107), new Ai(5, 108)), new Bi(26, new Ai(3, 41), new Ai(13, 42)), new Bi(30, new Ai(15, 24), new Ai(5, 25)), new Bi(28, new Ai(15, 15), new Ai(10, 16))), new Ao4(21, new int[]{6, 28, 50, 72, 94}, new Bi(28, new Ai(4, 116), new Ai(4, 117)), new Bi(26, new Ai(17, 42)), new Bi(28, new Ai(17, 22), new Ai(6, 23)), new Bi(30, new Ai(19, 16), new Ai(6, 17))), new Ao4(22, new int[]{6, 26, 50, 74, 98}, new Bi(28, new Ai(2, 111), new Ai(7, 112)), new Bi(28, new Ai(17, 46)), new Bi(30, new Ai(7, 24), new Ai(16, 25)), new Bi(24, new Ai(34, 13))), new Ao4(23, new int[]{6, 30, 54, 78, 102}, new Bi(30, new Ai(4, 121), new Ai(5, 122)), new Bi(28, new Ai(4, 47), new Ai(14, 48)), new Bi(30, new Ai(11, 24), new Ai(14, 25)), new Bi(30, new Ai(16, 15), new Ai(14, 16))), new Ao4(24, new int[]{6, 28, 54, 80, 106}, new Bi(30, new Ai(6, 117), new Ai(4, 118)), new Bi(28, new Ai(6, 45), new Ai(14, 46)), new Bi(30, new Ai(11, 24), new Ai(16, 25)), new Bi(30, new Ai(30, 16), new Ai(2, 17))), new Ao4(25, new int[]{6, 32, 58, 84, 110}, new Bi(26, new Ai(8, 106), new Ai(4, 107)), new Bi(28, new Ai(8, 47), new Ai(13, 48)), new Bi(30, new Ai(7, 24), new Ai(22, 25)), new Bi(30, new Ai(22, 15), new Ai(13, 16))), new Ao4(26, new int[]{6, 30, 58, 86, 114}, new Bi(28, new Ai(10, 114), new Ai(2, 115)), new Bi(28, new Ai(19, 46), new Ai(4, 47)), new Bi(28, new Ai(28, 22), new Ai(6, 23)), new Bi(30, new Ai(33, 16), new Ai(4, 17))), new Ao4(27, new int[]{6, 34, 62, 90, 118}, new Bi(30, new Ai(8, 122), new Ai(4, 123)), new Bi(28, new Ai(22, 45), new Ai(3, 46)), new Bi(30, new Ai(8, 23), new Ai(26, 24)), new Bi(30, new Ai(12, 15), new Ai(28, 16))), new Ao4(28, new int[]{6, 26, 50, 74, 98, 122}, new Bi(30, new Ai(3, 117), new Ai(10, 118)), new Bi(28, new Ai(3, 45), new Ai(23, 46)), new Bi(30, new Ai(4, 24), new Ai(31, 25)), new Bi(30, new Ai(11, 15), new Ai(31, 16))), new Ao4(29, new int[]{6, 30, 54, 78, 102, 126}, new Bi(30, new Ai(7, 116), new Ai(7, 117)), new Bi(28, new Ai(21, 45), new Ai(7, 46)), new Bi(30, new Ai(1, 23), new Ai(37, 24)), new Bi(30, new Ai(19, 15), new Ai(26, 16))), new Ao4(30, new int[]{6, 26, 52, 78, 104, 130}, new Bi(30, new Ai(5, 115), new Ai(10, 116)), new Bi(28, new Ai(19, 47), new Ai(10, 48)), new Bi(30, new Ai(15, 24), new Ai(25, 25)), new Bi(30, new Ai(23, 15), new Ai(25, 16))), new Ao4(31, new int[]{6, 30, 56, 82, 108, 134}, new Bi(30, new Ai(13, 115), new Ai(3, 116)), new Bi(28, new Ai(2, 46), new Ai(29, 47)), new Bi(30, new Ai(42, 24), new Ai(1, 25)), new Bi(30, new Ai(23, 15), new Ai(28, 16))), new Ao4(32, new int[]{6, 34, 60, 86, 112, 138}, new Bi(30, new Ai(17, 115)), new Bi(28, new Ai(10, 46), new Ai(23, 47)), new Bi(30, new Ai(10, 24), new Ai(35, 25)), new Bi(30, new Ai(19, 15), new Ai(35, 16))), new Ao4(33, new int[]{6, 30, 58, 86, 114, 142}, new Bi(30, new Ai(17, 115), new Ai(1, 116)), new Bi(28, new Ai(14, 46), new Ai(21, 47)), new Bi(30, new Ai(29, 24), new Ai(19, 25)), new Bi(30, new Ai(11, 15), new Ai(46, 16))), new Ao4(34, new int[]{6, 34, 62, 90, 118, 146}, new Bi(30, new Ai(13, 115), new Ai(6, 116)), new Bi(28, new Ai(14, 46), new Ai(23, 47)), new Bi(30, new Ai(44, 24), new Ai(7, 25)), new Bi(30, new Ai(59, 16), new Ai(1, 17))), new Ao4(35, new int[]{6, 30, 54, 78, 102, 126, 150}, new Bi(30, new Ai(12, 121), new Ai(7, 122)), new Bi(28, new Ai(12, 47), new Ai(26, 48)), new Bi(30, new Ai(39, 24), new Ai(14, 25)), new Bi(30, new Ai(22, 15), new Ai(41, 16))), new Ao4(36, new int[]{6, 24, 50, 76, 102, 128, 154}, new Bi(30, new Ai(6, 121), new Ai(14, 122)), new Bi(28, new Ai(6, 47), new Ai(34, 48)), new Bi(30, new Ai(46, 24), new Ai(10, 25)), new Bi(30, new Ai(2, 15), new Ai(64, 16))), new Ao4(37, new int[]{6, 28, 54, 80, 106, 132, 158}, new Bi(30, new Ai(17, 122), new Ai(4, 123)), new Bi(28, new Ai(29, 46), new Ai(14, 47)), new Bi(30, new Ai(49, 24), new Ai(10, 25)), new Bi(30, new Ai(24, 15), new Ai(46, 16))), new Ao4(38, new int[]{6, 32, 58, 84, 110, 136, 162}, new Bi(30, new Ai(4, 122), new Ai(18, 123)), new Bi(28, new Ai(13, 46), new Ai(32, 47)), new Bi(30, new Ai(48, 24), new Ai(14, 25)), new Bi(30, new Ai(42, 15), new Ai(32, 16))), new Ao4(39, new int[]{6, 26, 54, 82, 110, 138, 166}, new Bi(30, new Ai(20, 117), new Ai(4, 118)), new Bi(28, new Ai(40, 47), new Ai(7, 48)), new Bi(30, new Ai(43, 24), new Ai(22, 25)), new Bi(30, new Ai(10, 15), new Ai(67, 16))), new Ao4(40, new int[]{6, 30, 58, 86, 114, 142, 170}, new Bi(30, new Ai(19, 118), new Ai(6, 119)), new Bi(28, new Ai(18, 47), new Ai(31, 48)), new Bi(30, new Ai(34, 24), new Ai(34, 25)), new Bi(30, new Ai(20, 15), new Ai(61, 16)))};
    }

    @DexIgnore
    public static Ao4 e(int i) {
        if (i > 0 && i <= 40) {
            return d[i - 1];
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int b() {
        return (this.a * 4) + 17;
    }

    @DexIgnore
    public Bi c(Yn4 yn4) {
        return this.b[yn4.ordinal()];
    }

    @DexIgnore
    public int d() {
        return this.c;
    }

    @DexIgnore
    public int f() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return String.valueOf(this.a);
    }
}
