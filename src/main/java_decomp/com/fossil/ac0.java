package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ac0 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Zb0 CREATOR; // = new Zb0(null);
    @DexIgnore
    public /* final */ Jw1 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public byte[] f;

    @DexIgnore
    public Ac0(byte[] bArr) throws IllegalArgumentException {
        this.f = bArr;
        try {
            Jw1 a2 = Jw1.d.a(bArr[0]);
            if (a2 != null) {
                this.b = a2;
                int p = Hy1.p(this.f[1]) + 2;
                this.c = Iy1.c(new String(Dm7.k(this.f, 2, p), Et7.a));
                this.d = (byte) this.f[p];
                int i = p + 1;
                int i2 = i + 4;
                this.e = Hy1.o(ByteBuffer.wrap(Dm7.k(this.f, i, i2)).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                byte b2 = this.f[i2];
                int i3 = i2 + 1;
                new Ry1(b2, this.f[i3]);
                int i4 = i3 + 1;
                new Ry1(this.f[i4], this.f[i4 + 1]);
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final long b() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.e, Ey1.a(this.b)), Jd0.Y4, this.c), Jd0.Z4, Byte.valueOf(this.d)), Jd0.a5, Hy1.k((int) this.e, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
    }
}
