package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mg6 {
    @DexIgnore
    public /* final */ Gg6 a;
    @DexIgnore
    public /* final */ Xg6 b;
    @DexIgnore
    public /* final */ Rg6 c;

    @DexIgnore
    public Mg6(Gg6 gg6, Xg6 xg6, Rg6 rg6) {
        Wg6.c(gg6, "mCaloriesOverviewDayView");
        Wg6.c(xg6, "mCaloriesOverviewWeekView");
        Wg6.c(rg6, "mCaloriesOverviewMonthView");
        this.a = gg6;
        this.b = xg6;
        this.c = rg6;
    }

    @DexIgnore
    public final Gg6 a() {
        return this.a;
    }

    @DexIgnore
    public final Rg6 b() {
        return this.c;
    }

    @DexIgnore
    public final Xg6 c() {
        return this.b;
    }
}
