package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T44 extends I44<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ T44 INSTANCE; // = new T44();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        I14.l(comparable);
        if (comparable == comparable2) {
            return 0;
        }
        return comparable2.compareTo(comparable);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.I44, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Comparable comparable, Comparable comparable2) {
        return compare(comparable, comparable2);
    }

    @DexIgnore
    public <E extends Comparable> E max(E e, E e2) {
        return (E) ((Comparable) E44.INSTANCE.min(e, e2));
    }

    @DexIgnore
    public <E extends Comparable> E max(E e, E e2, E e3, E... eArr) {
        return (E) ((Comparable) E44.INSTANCE.min(e, e2, e3, eArr));
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <E extends Comparable> E max(Iterable<E> iterable) {
        return (E) ((Comparable) E44.INSTANCE.min(iterable));
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <E extends Comparable> E max(Iterator<E> it) {
        return (E) ((Comparable) E44.INSTANCE.min(it));
    }

    @DexIgnore
    @Override // com.fossil.I44
    public /* bridge */ /* synthetic */ Object max(Object obj, Object obj2) {
        return max((Comparable) obj, (Comparable) obj2);
    }

    @DexIgnore
    @Override // com.fossil.I44
    public /* bridge */ /* synthetic */ Object max(Object obj, Object obj2, Object obj3, Object[] objArr) {
        return max((Comparable) obj, (Comparable) obj2, (Comparable) obj3, (Comparable[]) objArr);
    }

    @DexIgnore
    public <E extends Comparable> E min(E e, E e2) {
        return (E) ((Comparable) E44.INSTANCE.max(e, e2));
    }

    @DexIgnore
    public <E extends Comparable> E min(E e, E e2, E e3, E... eArr) {
        return (E) ((Comparable) E44.INSTANCE.max(e, e2, e3, eArr));
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <E extends Comparable> E min(Iterable<E> iterable) {
        return (E) ((Comparable) E44.INSTANCE.max(iterable));
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <E extends Comparable> E min(Iterator<E> it) {
        return (E) ((Comparable) E44.INSTANCE.max(it));
    }

    @DexIgnore
    @Override // com.fossil.I44
    public /* bridge */ /* synthetic */ Object min(Object obj, Object obj2) {
        return min((Comparable) obj, (Comparable) obj2);
    }

    @DexIgnore
    @Override // com.fossil.I44
    public /* bridge */ /* synthetic */ Object min(Object obj, Object obj2, Object obj3, Object[] objArr) {
        return min((Comparable) obj, (Comparable) obj2, (Comparable) obj3, (Comparable[]) objArr);
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends Comparable> I44<S> reverse() {
        return I44.natural();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural().reverse()";
    }
}
