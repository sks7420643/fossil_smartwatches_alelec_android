package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I7 extends H7 {
    @DexIgnore
    public /* final */ N6 b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore
    public I7(G7 g7, N6 n6, byte[] bArr) {
        super(g7);
        this.b = n6;
        this.c = bArr;
    }
}
