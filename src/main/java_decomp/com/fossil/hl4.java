package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Hl4 {
    @DexIgnore
    public Hl4 a() throws CloneNotSupportedException {
        return (Hl4) super.clone();
    }

    @DexIgnore
    public abstract Hl4 b(El4 el4) throws IOException;

    @DexIgnore
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return a();
    }

    @DexIgnore
    public String toString() {
        return Il4.d(this);
    }
}
