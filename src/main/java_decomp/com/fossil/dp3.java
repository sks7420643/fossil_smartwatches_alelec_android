package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Xo3 b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;
    @DexIgnore
    public /* final */ /* synthetic */ Ap3 d;

    @DexIgnore
    public Dp3(Ap3 ap3, Xo3 xo3, long j) {
        this.d = ap3;
        this.b = xo3;
        this.c = j;
    }

    @DexIgnore
    public final void run() {
        Ap3.Q(this.d, this.b, false, this.c);
        Ap3 ap3 = this.d;
        ap3.e = null;
        ap3.r().N(null);
    }
}
