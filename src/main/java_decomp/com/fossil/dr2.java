package com.fossil;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dr2 extends Mb3 {
    @DexIgnore
    public /* final */ P72<Ga3> b;

    @DexIgnore
    public Dr2(P72<Ga3> p72) {
        this.b = p72;
    }

    @DexIgnore
    public final void i() {
        synchronized (this) {
            this.b.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb3
    public final void onLocationChanged(Location location) {
        synchronized (this) {
            this.b.c(new Er2(this, location));
        }
    }
}
