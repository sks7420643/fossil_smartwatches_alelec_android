package com.fossil;

import android.graphics.drawable.Drawable;
import com.mapped.Wg6;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C61 extends D61 {
    @DexIgnore
    public /* final */ Drawable a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ Q51 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C61(Drawable drawable, boolean z, Q51 q51) {
        super(null);
        Wg6.c(drawable, ResourceManager.DRAWABLE);
        Wg6.c(q51, "dataSource");
        this.a = drawable;
        this.b = z;
        this.c = q51;
    }

    @DexIgnore
    public static /* synthetic */ C61 e(C61 c61, Drawable drawable, boolean z, Q51 q51, int i, Object obj) {
        if ((i & 1) != 0) {
            drawable = c61.a;
        }
        if ((i & 2) != 0) {
            z = c61.b;
        }
        if ((i & 4) != 0) {
            q51 = c61.c;
        }
        return c61.d(drawable, z, q51);
    }

    @DexIgnore
    public final Drawable a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final Q51 c() {
        return this.c;
    }

    @DexIgnore
    public final C61 d(Drawable drawable, boolean z, Q51 q51) {
        Wg6.c(drawable, ResourceManager.DRAWABLE);
        Wg6.c(q51, "dataSource");
        return new C61(drawable, z, q51);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C61) {
                C61 c61 = (C61) obj;
                if (!Wg6.a(this.a, c61.a) || this.b != c61.b || !Wg6.a(this.c, c61.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Drawable f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Drawable drawable = this.a;
        int hashCode = drawable != null ? drawable.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        Q51 q51 = this.c;
        if (q51 != null) {
            i = q51.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "DrawableResult(drawable=" + this.a + ", isSampled=" + this.b + ", dataSource=" + this.c + ")";
    }
}
