package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lh2 extends Rl2 implements Mh2 {
    @DexIgnore
    public Lh2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @DexIgnore
    @Override // com.fossil.Mh2
    public final Rg2 G(Rg2 rg2, String str, int i, Rg2 rg22) throws RemoteException {
        Parcel d = d();
        Sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        Sl2.c(d, rg22);
        Parcel e = e(2, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Mh2
    public final Rg2 L(Rg2 rg2, String str, int i, Rg2 rg22) throws RemoteException {
        Parcel d = d();
        Sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        Sl2.c(d, rg22);
        Parcel e = e(3, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
