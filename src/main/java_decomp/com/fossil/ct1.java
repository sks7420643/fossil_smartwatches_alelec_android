package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ct1 extends At1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ct1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Ct1 a(Parcel parcel) {
            return new Ct1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ct1 createFromParcel(Parcel parcel) {
            return new Ct1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ct1[] newArray(int i) {
            return new Ct1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Ct1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
            this.e = parcel.readInt();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public Ct1(String str, int i) {
        super(G90.c, false, 2);
        this.d = str;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.At1
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.At1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ct1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Ct1 ct1 = (Ct1) obj;
            if (!Wg6.a(this.d, ct1.d)) {
                return false;
            }
            return this.e == ct1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    public final String getLocation() {
        return this.d;
    }

    @DexIgnore
    public final int getUtcOffsetInMinutes() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.At1
    public int hashCode() {
        return (((super.hashCode() * 31) + this.d.hashCode()) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.At1
    public JSONObject toJSONObject() {
        JSONObject put = super.toJSONObject().put("loc", this.d).put("utc", this.e);
        Wg6.b(put, "super.toJSONObject()\n   \u2026.UTC, utcOffsetInMinutes)");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.At1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
