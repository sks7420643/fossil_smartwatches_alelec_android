package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S08 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(S08.class, Object.class, "lastScheduledTask");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c; // = AtomicIntegerFieldUpdater.newUpdater(S08.class, "producerIndex");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater d; // = AtomicIntegerFieldUpdater.newUpdater(S08.class, "consumerIndex");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater e; // = AtomicIntegerFieldUpdater.newUpdater(S08.class, "blockingTasksInBuffer");
    @DexIgnore
    public /* final */ AtomicReferenceArray<N08> a; // = new AtomicReferenceArray<>(128);
    @DexIgnore
    public volatile int blockingTasksInBuffer; // = 0;
    @DexIgnore
    public volatile int consumerIndex; // = 0;
    @DexIgnore
    public volatile Object lastScheduledTask; // = null;
    @DexIgnore
    public volatile int producerIndex; // = 0;

    @DexIgnore
    public static /* synthetic */ N08 b(S08 s08, N08 n08, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return s08.a(n08, z);
    }

    @DexIgnore
    public final N08 a(N08 n08, boolean z) {
        if (z) {
            return c(n08);
        }
        N08 n082 = (N08) b.getAndSet(this, n08);
        if (n082 != null) {
            return c(n082);
        }
        return null;
    }

    @DexIgnore
    public final N08 c(N08 n08) {
        boolean z = true;
        if (n08.c.o() != 1) {
            z = false;
        }
        if (z) {
            e.incrementAndGet(this);
        }
        if (e() == 127) {
            return n08;
        }
        int i = this.producerIndex & 127;
        while (this.a.get(i) != null) {
            Thread.yield();
        }
        this.a.lazySet(i, n08);
        c.incrementAndGet(this);
        return null;
    }

    @DexIgnore
    public final void d(N08 n08) {
        boolean z = true;
        if (n08 != null) {
            if (n08.c.o() == 1) {
                int decrementAndGet = e.decrementAndGet(this);
                if (Nv7.a()) {
                    if (decrementAndGet < 0) {
                        z = false;
                    }
                    if (!z) {
                        throw new AssertionError();
                    }
                }
            }
        }
    }

    @DexIgnore
    public final int e() {
        return this.producerIndex - this.consumerIndex;
    }

    @DexIgnore
    public final int f() {
        return this.lastScheduledTask != null ? e() + 1 : e();
    }

    @DexIgnore
    public final void g(J08 j08) {
        N08 n08 = (N08) b.getAndSet(this, null);
        if (n08 != null) {
            j08.a(n08);
        }
        do {
        } while (j(j08));
    }

    @DexIgnore
    public final N08 h() {
        N08 n08 = (N08) b.getAndSet(this, null);
        return n08 != null ? n08 : i();
    }

    @DexIgnore
    public final N08 i() {
        N08 andSet;
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            if (d.compareAndSet(this, i, i + 1) && (andSet = this.a.getAndSet(i & 127, null)) != null) {
                d(andSet);
                return andSet;
            }
        }
    }

    @DexIgnore
    public final boolean j(J08 j08) {
        N08 i = i();
        if (i == null) {
            return false;
        }
        j08.a(i);
        return true;
    }

    @DexIgnore
    public final long k(S08 s08) {
        if (Nv7.a()) {
            if (!(e() == 0)) {
                throw new AssertionError();
            }
        }
        int i = s08.consumerIndex;
        int i2 = s08.producerIndex;
        AtomicReferenceArray<N08> atomicReferenceArray = s08.a;
        for (int i3 = i; i3 != i2; i3++) {
            int i4 = i3 & 127;
            if (s08.blockingTasksInBuffer == 0) {
                break;
            }
            N08 n08 = atomicReferenceArray.get(i4);
            if (n08 != null) {
                if ((n08.c.o() == 1) && atomicReferenceArray.compareAndSet(i4, n08, null)) {
                    e.decrementAndGet(s08);
                    b(this, n08, false, 2, null);
                    return -1;
                }
            }
        }
        return m(s08, true);
    }

    @DexIgnore
    public final long l(S08 s08) {
        boolean z = true;
        if (Nv7.a()) {
            if (!(e() == 0)) {
                throw new AssertionError();
            }
        }
        N08 i = s08.i();
        if (i == null) {
            return m(s08, false);
        }
        N08 b2 = b(this, i, false, 2, null);
        if (Nv7.a()) {
            if (b2 != null) {
                z = false;
            }
            if (!z) {
                throw new AssertionError();
            }
        }
        return -1;
    }

    @DexIgnore
    public final long m(S08 s08, boolean z) {
        N08 n08;
        do {
            n08 = (N08) s08.lastScheduledTask;
            if (n08 == null) {
                return -2;
            }
            if (z) {
                if (!(n08.c.o() == 1)) {
                    return -2;
                }
            }
            long a2 = Q08.e.a() - n08.b;
            long j = Q08.a;
            if (a2 < j) {
                return j - a2;
            }
        } while (!b.compareAndSet(s08, n08, null));
        b(this, n08, false, 2, null);
        return -1;
    }
}
