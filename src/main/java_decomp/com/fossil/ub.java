package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class Ub {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Ap1.values().length];
        a = iArr;
        iArr[Ap1.DIAGNOSTICS.ordinal()] = 1;
        a[Ap1.WELLNESS.ordinal()] = 2;
        a[Ap1.WORKOUT.ordinal()] = 3;
        a[Ap1.MUSIC.ordinal()] = 4;
        a[Ap1.NOTIFICATIONS_PANEL.ordinal()] = 5;
        a[Ap1.EMPTY.ordinal()] = 6;
        a[Ap1.STOP_WATCH.ordinal()] = 7;
        a[Ap1.ASSISTANT.ordinal()] = 8;
        a[Ap1.TIMER.ordinal()] = 9;
        a[Ap1.WEATHER.ordinal()] = 10;
        a[Ap1.COMMUTE.ordinal()] = 11;
        a[Ap1.BUDDY_CHALLENGE.ordinal()] = 12;
    }
    */
}
