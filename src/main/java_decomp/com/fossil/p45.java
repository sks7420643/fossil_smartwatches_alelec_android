package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class P45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ View u;

    @DexIgnore
    public P45(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, ImageView imageView, View view2, View view3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = imageView;
        this.t = view2;
        this.u = view3;
    }
}
