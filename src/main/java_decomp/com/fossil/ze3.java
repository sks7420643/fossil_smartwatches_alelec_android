package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ze3 implements Parcelable.Creator<Ee3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ee3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        double d = 0.0d;
        boolean z = false;
        boolean z2 = false;
        float f = 0.0f;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        ArrayList arrayList = null;
        LatLng latLng = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    latLng = (LatLng) Ad2.e(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    d = Ad2.p(parcel, t);
                    break;
                case 4:
                    f2 = Ad2.r(parcel, t);
                    break;
                case 5:
                    i2 = Ad2.v(parcel, t);
                    break;
                case 6:
                    i = Ad2.v(parcel, t);
                    break;
                case 7:
                    f = Ad2.r(parcel, t);
                    break;
                case 8:
                    z2 = Ad2.m(parcel, t);
                    break;
                case 9:
                    z = Ad2.m(parcel, t);
                    break;
                case 10:
                    arrayList = Ad2.j(parcel, t, Me3.CREATOR);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Ee3(latLng, d, f2, i2, i, f, z2, z, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ee3[] newArray(int i) {
        return new Ee3[i];
    }
}
