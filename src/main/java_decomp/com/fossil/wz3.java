package com.fossil;

import android.graphics.RectF;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wz3 implements Yz3 {
    @DexIgnore
    public /* final */ float a;

    @DexIgnore
    public Wz3(float f) {
        this.a = f;
    }

    @DexIgnore
    @Override // com.fossil.Yz3
    public float a(RectF rectF) {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Wz3)) {
            return false;
        }
        return this.a == ((Wz3) obj).a;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.a)});
    }
}
