package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Cw0 extends Bw0 {
    @DexIgnore
    public Cw0(long j, RenderScript renderScript) {
        super(j, renderScript);
        if (j == 0) {
            throw new Aw0("Loading of ScriptIntrinsic failed.");
        }
    }
}
