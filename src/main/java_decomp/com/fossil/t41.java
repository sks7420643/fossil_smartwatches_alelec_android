package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class T41<TResult> {
    @DexIgnore
    public static /* final */ Executor i; // = P41.b();
    @DexIgnore
    public static volatile Gi j;
    @DexIgnore
    public static T41<?> k; // = new T41<>((Object) null);
    @DexIgnore
    public static T41<Boolean> l; // = new T41<>(Boolean.TRUE);
    @DexIgnore
    public static T41<Boolean> m; // = new T41<>(Boolean.FALSE);
    @DexIgnore
    public static T41<?> n; // = new T41<>(true);
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public TResult d;
    @DexIgnore
    public Exception e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public V41 g;
    @DexIgnore
    public List<R41<TResult, Void>> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements R41<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ U41 a;
        @DexIgnore
        public /* final */ /* synthetic */ R41 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ Q41 d;

        @DexIgnore
        public Ai(T41 t41, U41 u41, R41 r41, Executor executor, Q41 q41) {
            this.a = u41;
            this.b = r41;
            this.c = executor;
            this.d = q41;
        }

        @DexIgnore
        public Void a(T41<TResult> t41) {
            T41.e(this.a, this.b, t41, this.c, this.d);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.R41
        public /* bridge */ /* synthetic */ Void then(T41 t41) throws Exception {
            return a(t41);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements R41<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ U41 a;
        @DexIgnore
        public /* final */ /* synthetic */ R41 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ Q41 d;

        @DexIgnore
        public Bi(T41 t41, U41 u41, R41 r41, Executor executor, Q41 q41) {
            this.a = u41;
            this.b = r41;
            this.c = executor;
            this.d = q41;
        }

        @DexIgnore
        public Void a(T41<TResult> t41) {
            T41.d(this.a, this.b, t41, this.c, this.d);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.R41
        public /* bridge */ /* synthetic */ Void then(T41 t41) throws Exception {
            return a(t41);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements R41<TResult, T41<TContinuationResult>> {
        @DexIgnore
        public /* final */ /* synthetic */ Q41 a;
        @DexIgnore
        public /* final */ /* synthetic */ R41 b;

        @DexIgnore
        public Ci(T41 t41, Q41 q41, R41 r41) {
            this.a = q41;
            this.b = r41;
        }

        @DexIgnore
        public T41<TContinuationResult> a(T41<TResult> t41) {
            Q41 q41 = this.a;
            return (q41 == null || !q41.a()) ? t41.r() ? T41.k(t41.m()) : t41.p() ? T41.c() : t41.f(this.b) : T41.c();
        }

        @DexIgnore
        @Override // com.fossil.R41
        public /* bridge */ /* synthetic */ Object then(T41 t41) throws Exception {
            return a(t41);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Q41 b;
        @DexIgnore
        public /* final */ /* synthetic */ U41 c;
        @DexIgnore
        public /* final */ /* synthetic */ R41 d;
        @DexIgnore
        public /* final */ /* synthetic */ T41 e;

        @DexIgnore
        public Di(Q41 q41, U41 u41, R41 r41, T41 t41) {
            this.b = q41;
            this.c = u41;
            this.d = r41;
            this.e = t41;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: com.fossil.U41 */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            Q41 q41 = this.b;
            if (q41 == null || !q41.a()) {
                try {
                    this.c.d(this.d.then(this.e));
                } catch (CancellationException e2) {
                    this.c.b();
                } catch (Exception e3) {
                    this.c.c(e3);
                }
            } else {
                this.c.b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Q41 b;
        @DexIgnore
        public /* final */ /* synthetic */ U41 c;
        @DexIgnore
        public /* final */ /* synthetic */ R41 d;
        @DexIgnore
        public /* final */ /* synthetic */ T41 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements R41<TContinuationResult, Void> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public Void a(T41<TContinuationResult> t41) {
                Q41 q41 = Ei.this.b;
                if (q41 != null && q41.a()) {
                    Ei.this.c.b();
                } else if (t41.p()) {
                    Ei.this.c.b();
                } else if (t41.r()) {
                    Ei.this.c.c(t41.m());
                } else {
                    Ei.this.c.d(t41.n());
                }
                return null;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.R41
            public /* bridge */ /* synthetic */ Void then(T41 t41) throws Exception {
                return a(t41);
            }
        }

        @DexIgnore
        public Ei(Q41 q41, U41 u41, R41 r41, T41 t41) {
            this.b = q41;
            this.c = u41;
            this.d = r41;
            this.e = t41;
        }

        @DexIgnore
        public void run() {
            Q41 q41 = this.b;
            if (q41 == null || !q41.a()) {
                try {
                    T41 t41 = (T41) this.d.then(this.e);
                    if (t41 == null) {
                        this.c.d(null);
                    } else {
                        t41.f(new Aii());
                    }
                } catch (CancellationException e2) {
                    this.c.b();
                } catch (Exception e3) {
                    this.c.c(e3);
                }
            } else {
                this.c.b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends U41<TResult> {
        @DexIgnore
        public Fi(T41 t41) {
        }
    }

    @DexIgnore
    public interface Gi {
        @DexIgnore
        void a(T41<?> t41, W41 w41);
    }

    /*
    static {
        P41.a();
        M41.c();
    }
    */

    @DexIgnore
    public T41() {
    }

    @DexIgnore
    public T41(TResult tresult) {
        x(tresult);
    }

    @DexIgnore
    public T41(boolean z) {
        if (z) {
            v();
        } else {
            x(null);
        }
    }

    @DexIgnore
    public static <TResult> T41<TResult> c() {
        return (T41<TResult>) n;
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void d(U41<TContinuationResult> u41, R41<TResult, T41<TContinuationResult>> r41, T41<TResult> t41, Executor executor, Q41 q41) {
        try {
            executor.execute(new Ei(q41, u41, r41, t41));
        } catch (Exception e2) {
            u41.c(new S41(e2));
        }
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void e(U41<TContinuationResult> u41, R41<TResult, TContinuationResult> r41, T41<TResult> t41, Executor executor, Q41 q41) {
        try {
            executor.execute(new Di(q41, u41, r41, t41));
        } catch (Exception e2) {
            u41.c(new S41(e2));
        }
    }

    @DexIgnore
    public static <TResult> T41<TResult>.f j() {
        T41 t41 = new T41();
        t41.getClass();
        return new Fi(t41);
    }

    @DexIgnore
    public static <TResult> T41<TResult> k(Exception exc) {
        U41 u41 = new U41();
        u41.c(exc);
        return u41.a();
    }

    @DexIgnore
    public static <TResult> T41<TResult> l(TResult tresult) {
        if (tresult == null) {
            return (T41<TResult>) k;
        }
        if (tresult instanceof Boolean) {
            return tresult.booleanValue() ? (T41<TResult>) l : (T41<TResult>) m;
        }
        U41 u41 = new U41();
        u41.d(tresult);
        return u41.a();
    }

    @DexIgnore
    public static Gi o() {
        return j;
    }

    @DexIgnore
    public <TContinuationResult> T41<TContinuationResult> f(R41<TResult, TContinuationResult> r41) {
        return g(r41, i, null);
    }

    @DexIgnore
    public <TContinuationResult> T41<TContinuationResult> g(R41<TResult, TContinuationResult> r41, Executor executor, Q41 q41) {
        boolean q;
        U41 u41 = new U41();
        synchronized (this.a) {
            q = q();
            if (!q) {
                this.h.add(new Ai(this, u41, r41, executor, q41));
            }
        }
        if (q) {
            e(u41, r41, this, executor, q41);
        }
        return u41.a();
    }

    @DexIgnore
    public <TContinuationResult> T41<TContinuationResult> h(R41<TResult, T41<TContinuationResult>> r41, Executor executor) {
        return i(r41, executor, null);
    }

    @DexIgnore
    public <TContinuationResult> T41<TContinuationResult> i(R41<TResult, T41<TContinuationResult>> r41, Executor executor, Q41 q41) {
        boolean q;
        U41 u41 = new U41();
        synchronized (this.a) {
            q = q();
            if (!q) {
                this.h.add(new Bi(this, u41, r41, executor, q41));
            }
        }
        if (q) {
            d(u41, r41, this, executor, q41);
        }
        return u41.a();
    }

    @DexIgnore
    public Exception m() {
        Exception exc;
        synchronized (this.a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    @DexIgnore
    public TResult n() {
        TResult tresult;
        synchronized (this.a) {
            tresult = this.d;
        }
        return tresult;
    }

    @DexIgnore
    public boolean p() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public boolean q() {
        boolean z;
        synchronized (this.a) {
            z = this.b;
        }
        return z;
    }

    @DexIgnore
    public boolean r() {
        boolean z;
        synchronized (this.a) {
            z = m() != null;
        }
        return z;
    }

    @DexIgnore
    public <TContinuationResult> T41<TContinuationResult> s(R41<TResult, TContinuationResult> r41) {
        return t(r41, i, null);
    }

    @DexIgnore
    public <TContinuationResult> T41<TContinuationResult> t(R41<TResult, TContinuationResult> r41, Executor executor, Q41 q41) {
        return h(new Ci(this, q41, r41), executor);
    }

    @DexIgnore
    public final void u() {
        synchronized (this.a) {
            for (R41<TResult, Void> r41 : this.h) {
                try {
                    r41.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    @DexIgnore
    public boolean v() {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.a.notifyAll();
            u();
            return true;
        }
    }

    @DexIgnore
    public boolean w(Exception exc) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.a.notifyAll();
            u();
            if (!this.f && o() != null) {
                this.g = new V41(this);
            }
            return true;
        }
    }

    @DexIgnore
    public boolean x(TResult tresult) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.a.notifyAll();
            u();
            return true;
        }
    }
}
