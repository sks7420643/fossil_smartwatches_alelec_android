package com.fossil;

import android.os.Bundle;
import com.fossil.R62;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V82 implements R62.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ V72 c;
    @DexIgnore
    public /* final */ /* synthetic */ T82 d;

    @DexIgnore
    public V82(T82 t82, AtomicReference atomicReference, V72 v72) {
        this.d = t82;
        this.b = atomicReference;
        this.c = v72;
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void e(Bundle bundle) {
        this.d.y((R62) this.b.get(), this.c, true);
    }
}
