package com.fossil;

import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N51<K, V> {
    @DexIgnore
    public /* final */ Ai<K, V> a; // = new Ai<>(null, 1, null);
    @DexIgnore
    public /* final */ HashMap<K, Ai<K, V>> b; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<K, V> {
        @DexIgnore
        public List<V> a;
        @DexIgnore
        public Ai<K, V> b;
        @DexIgnore
        public Ai<K, V> c;
        @DexIgnore
        public /* final */ K d;

        @DexIgnore
        public Ai() {
            this(null, 1, null);
        }

        @DexIgnore
        public Ai(K k) {
            this.d = k;
            this.b = this;
            this.c = this;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Object obj, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : obj);
        }

        @DexIgnore
        public final void a(V v) {
            List<V> list = this.a;
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(v);
            this.a = list;
        }

        @DexIgnore
        public final K b() {
            return this.d;
        }

        @DexIgnore
        public final Ai<K, V> c() {
            return this.c;
        }

        @DexIgnore
        public final Ai<K, V> d() {
            return this.b;
        }

        @DexIgnore
        public final V e() {
            List<V> list = this.a;
            if (list == null || !(!list.isEmpty())) {
                return null;
            }
            return list.remove(Hm7.g(list));
        }

        @DexIgnore
        public final void f(Ai<K, V> ai) {
            Wg6.c(ai, "<set-?>");
            this.c = ai;
        }

        @DexIgnore
        public final void g(Ai<K, V> ai) {
            Wg6.c(ai, "<set-?>");
            this.b = ai;
        }

        @DexIgnore
        public final int h() {
            List<V> list = this.a;
            if (list != null) {
                return list.size();
            }
            return 0;
        }
    }

    @DexIgnore
    public final V a(K k) {
        HashMap<K, Ai<K, V>> hashMap = this.b;
        Ai<K, V> ai = hashMap.get(k);
        if (ai == null) {
            ai = new Ai<>(k);
            hashMap.put(k, ai);
        }
        Ai<K, V> ai2 = ai;
        b(ai2);
        return ai2.e();
    }

    @DexIgnore
    public final void b(Ai<K, V> ai) {
        d(ai);
        ai.g(this.a);
        ai.f(this.a.c());
        g(ai);
    }

    @DexIgnore
    public final void c(Ai<K, V> ai) {
        d(ai);
        ai.g(this.a.d());
        ai.f(this.a);
        g(ai);
    }

    @DexIgnore
    public final <K, V> void d(Ai<K, V> ai) {
        ai.d().f(ai.c());
        ai.c().g(ai.d());
    }

    @DexIgnore
    public final V e() {
        for (Ai<K, V> d = this.a.d(); !Wg6.a(d, this.a); d = d.d()) {
            V e = d.e();
            if (e != null) {
                return e;
            }
            d(d);
            HashMap<K, Ai<K, V>> hashMap = this.b;
            K b2 = d.b();
            if (hashMap != null) {
                Ir7.c(hashMap).remove(b2);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>");
            }
        }
        return null;
    }

    @DexIgnore
    public final void f(K k, V v) {
        HashMap<K, Ai<K, V>> hashMap = this.b;
        Ai<K, V> ai = hashMap.get(k);
        if (ai == null) {
            ai = new Ai<>(k);
            c(ai);
            hashMap.put(k, ai);
        }
        ai.a(v);
    }

    @DexIgnore
    public final <K, V> void g(Ai<K, V> ai) {
        ai.c().g(ai);
        ai.d().f(ai);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GroupedLinkedMap( ");
        Ai<K, V> c = this.a.c();
        boolean z = false;
        while (!Wg6.a(c, this.a)) {
            sb.append('{');
            sb.append((Object) c.b());
            sb.append(':');
            sb.append(c.h());
            sb.append("}, ");
            c = c.c();
            z = true;
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        String sb2 = sb.toString();
        Wg6.b(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
