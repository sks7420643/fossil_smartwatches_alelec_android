package com.fossil;

import android.text.Html;
import android.text.Spanned;
import com.fossil.m47;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.SoLibraryLoader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i27 extends f27 {
    @DexIgnore
    public /* final */ g27 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.welcome.WelcomePresenter$navigateToAccountCreation$1", f = "WelcomePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ i27 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.i27$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.welcome.WelcomePresenter$navigateToAccountCreation$1$isAA$1", f = "WelcomePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.i27$a$a  reason: collision with other inner class name */
        public static final class C0119a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public C0119a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0119a aVar = new C0119a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                throw null;
                //return ((C0119a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return ao7.a(SoLibraryLoader.f().c(PortfolioApp.h0.c()) != null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(i27 i27, qn7 qn7) {
            super(2, qn7);
            this.this$0 = i27;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                C0119a aVar = new C0119a(null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Boolean) g).booleanValue()) {
                this.this$0.p().V5();
            } else {
                this.this$0.p().d2();
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        pq7.b(i27.class.getSimpleName(), "WelcomePresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public i27(g27 g27) {
        pq7.c(g27, "mView");
        this.e = g27;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        g27 g27 = this.e;
        Spanned fromHtml = Html.fromHtml(q());
        pq7.b(fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        g27.w0(fromHtml);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.f27
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.f27
    public void o() {
        this.e.y5();
    }

    @DexIgnore
    public final g27 p() {
        return this.e;
    }

    @DexIgnore
    public final String q() {
        String a2 = m47.a(m47.c.TERMS, null);
        String a3 = m47.a(m47.c.PRIVACY, null);
        String str = um5.c(PortfolioApp.h0.c(), 2131886977).toString();
        pq7.b(a2, "termOfUseUrl");
        String q = vt7.q(str, "term_of_use_url", a2, false, 4, null);
        pq7.b(a3, "privacyPolicyUrl");
        return vt7.q(q, "privacy_policy", a3, false, 4, null);
    }

    @DexIgnore
    public void r() {
        this.e.M5(this);
    }
}
