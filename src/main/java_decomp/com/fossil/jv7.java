package com.fossil;

import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Rm6;
import com.mapped.Xe6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jv7 {
    @DexIgnore
    public static final Il6 a(Af6 af6) {
        if (af6.get(Rm6.r) == null) {
            af6 = af6.plus(Dx7.b(null, 1, null));
        }
        return new Ez7(af6);
    }

    @DexIgnore
    public static final Il6 b() {
        return new Ez7(Ux7.b(null, 1, null).plus(Bw7.c()));
    }

    @DexIgnore
    public static final void c(Il6 il6, CancellationException cancellationException) {
        Rm6 rm6 = (Rm6) il6.h().get(Rm6.r);
        if (rm6 != null) {
            rm6.D(cancellationException);
            return;
        }
        throw new IllegalStateException(("Scope cannot be cancelled because it does not have a job: " + il6).toString());
    }

    @DexIgnore
    public static /* synthetic */ void d(Il6 il6, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        c(il6, cancellationException);
    }

    @DexIgnore
    public static final <R> Object e(Coroutine<? super Il6, ? super Xe6<? super R>, ? extends Object> coroutine, Xe6<? super R> xe6) {
        Tz7 tz7 = new Tz7(xe6.getContext(), xe6);
        Object c = E08.c(tz7, tz7, coroutine);
        if (c == Yn7.d()) {
            Go7.c(xe6);
        }
        return c;
    }

    @DexIgnore
    public static final void f(Il6 il6) {
        Bx7.g(il6.h());
    }

    @DexIgnore
    public static final boolean g(Il6 il6) {
        Rm6 rm6 = (Rm6) il6.h().get(Rm6.r);
        if (rm6 != null) {
            return rm6.isActive();
        }
        return true;
    }

    @DexIgnore
    public static final Il6 h(Il6 il6, Af6 af6) {
        return new Ez7(il6.h().plus(af6));
    }
}
