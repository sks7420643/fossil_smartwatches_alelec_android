package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H56 implements Factory<D56> {
    @DexIgnore
    public static D56 a(F56 f56) {
        D56 c = f56.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
