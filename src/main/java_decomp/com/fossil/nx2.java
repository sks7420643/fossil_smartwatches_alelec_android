package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nx2 extends AbstractSet<K> {
    @DexIgnore
    public /* final */ /* synthetic */ Hx2 b;

    @DexIgnore
    public Nx2(Hx2 hx2) {
        this.b = hx2;
    }

    @DexIgnore
    public final void clear() {
        this.b.clear();
    }

    @DexIgnore
    public final boolean contains(Object obj) {
        return this.b.containsKey(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<K> iterator() {
        return this.b.zze();
    }

    @DexIgnore
    public final boolean remove(@NullableDecl Object obj) {
        Map zzb = this.b.zzb();
        return zzb != null ? zzb.keySet().remove(obj) : this.b.c(obj) != Hx2.h;
    }

    @DexIgnore
    public final int size() {
        return this.b.size();
    }
}
