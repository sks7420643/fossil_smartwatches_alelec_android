package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.SecureApiService;
import com.portfolio.platform.preset.data.source.DianaPresetRemote;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class To5 implements Factory<DianaPresetRemote> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> a;
    @DexIgnore
    public /* final */ Provider<SecureApiService> b;

    @DexIgnore
    public To5(Provider<ApiServiceV2> provider, Provider<SecureApiService> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static To5 a(Provider<ApiServiceV2> provider, Provider<SecureApiService> provider2) {
        return new To5(provider, provider2);
    }

    @DexIgnore
    public static DianaPresetRemote c(ApiServiceV2 apiServiceV2, SecureApiService secureApiService) {
        return new DianaPresetRemote(apiServiceV2, secureApiService);
    }

    @DexIgnore
    public DianaPresetRemote b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
