package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum xu1 {
    TOP(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    MIDDLE((byte) 32),
    BOTTOM((byte) 48);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public xu1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
