package com.fossil;

import com.fossil.Ta4;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G94 implements L94 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public G94(String str, String str2, File file) {
        this.b = str;
        this.c = str2;
        this.a = file;
    }

    @DexIgnore
    @Override // com.fossil.L94
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.L94
    public InputStream b() {
        if (this.a.exists() && this.a.isFile()) {
            try {
                return new FileInputStream(this.a);
            } catch (FileNotFoundException e) {
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.L94
    public Ta4.Ci.Bii c() {
        byte[] d = d();
        if (d == null) {
            return null;
        }
        Ta4.Ci.Bii.Aiii a2 = Ta4.Ci.Bii.a();
        a2.b(d);
        a2.c(this.b);
        return a2.a();
    }

    @DexIgnore
    public final byte[] d() {
        byte[] bArr = new byte[8192];
        try {
            InputStream b2 = b();
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                    if (b2 == null) {
                        gZIPOutputStream.close();
                        byteArrayOutputStream.close();
                        if (b2 == null) {
                            return null;
                        }
                        b2.close();
                        return null;
                    }
                    while (true) {
                        try {
                            int read = b2.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            gZIPOutputStream.write(bArr, 0, read);
                        } catch (Throwable th) {
                        }
                    }
                    gZIPOutputStream.finish();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    gZIPOutputStream.close();
                    byteArrayOutputStream.close();
                    if (b2 != null) {
                        b2.close();
                    }
                    return byteArray;
                } catch (Throwable th2) {
                }
                throw th;
                throw th;
                throw th;
            } catch (Throwable th3) {
            }
        } catch (IOException e) {
            return null;
        }
    }
}
