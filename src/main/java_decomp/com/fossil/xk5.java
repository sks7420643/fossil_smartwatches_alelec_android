package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xk5 {
    @DexIgnore
    public static /* final */ Xk5 a; // = new Xk5();

    @DexIgnore
    public final void a(View view, Context context) {
        Wg6.c(view, "view");
        Wg6.c(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @DexIgnore
    public final void b(View view, Context context) {
        Wg6.c(view, "view");
        Wg6.c(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, 2);
        }
    }
}
