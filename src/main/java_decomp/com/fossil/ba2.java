package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ba2 implements Ft3<Boolean, Void> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Nt3] */
    @Override // com.fossil.Ft3
    public final /* synthetic */ Void then(Nt3<Boolean> nt3) throws Exception {
        if (nt3.m().booleanValue()) {
            return null;
        }
        throw new N62(new Status(13, "listener already unregistered"));
    }
}
