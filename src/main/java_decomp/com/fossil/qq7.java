package com.fossil;

import com.mapped.Wg6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qq7<R> implements Mq7<R>, Serializable {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public Qq7(int i) {
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.Mq7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public String toString() {
        String i = Er7.i(this);
        Wg6.b(i, "Reflection.renderLambdaToString(this)");
        return i;
    }
}
