package com.fossil;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Bb1 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Bitmap a(int i, int i2, Bitmap.Config config);

        @DexIgnore
        int[] b(int i);

        @DexIgnore
        void c(Bitmap bitmap);

        @DexIgnore
        void d(byte[] bArr);

        @DexIgnore
        byte[] e(int i);

        @DexIgnore
        void f(int[] iArr);
    }

    @DexIgnore
    Bitmap a();

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    int c();

    @DexIgnore
    Object clear();  // void declaration

    @DexIgnore
    int d();

    @DexIgnore
    void e(Bitmap.Config config);

    @DexIgnore
    ByteBuffer f();

    @DexIgnore
    Object g();  // void declaration

    @DexIgnore
    int h();

    @DexIgnore
    int i();
}
