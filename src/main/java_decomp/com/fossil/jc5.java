package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jc5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ShimmerFrameLayout w;

    @DexIgnore
    public Jc5(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView, ShimmerFrameLayout shimmerFrameLayout) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = flexibleButton;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = flexibleTextView3;
        this.v = imageView;
        this.w = shimmerFrameLayout;
    }
}
