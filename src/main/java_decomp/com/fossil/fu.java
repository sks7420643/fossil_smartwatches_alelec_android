package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Fu extends Enum<Fu> {
    @DexIgnore
    public static /* final */ Fu h;
    @DexIgnore
    public static /* final */ Fu i;
    @DexIgnore
    public static /* final */ Fu j;
    @DexIgnore
    public static /* final */ Fu k;
    @DexIgnore
    public static /* final */ Fu l;
    @DexIgnore
    public static /* final */ Fu m;
    @DexIgnore
    public static /* final */ Fu n;
    @DexIgnore
    public static /* final */ Fu o;
    @DexIgnore
    public static /* final */ Fu p;
    @DexIgnore
    public static /* final */ Fu q;
    @DexIgnore
    public static /* final */ Fu r;
    @DexIgnore
    public static /* final */ Fu s;
    @DexIgnore
    public static /* final */ Fu t;
    @DexIgnore
    public static /* final */ Fu u;
    @DexIgnore
    public static /* final */ /* synthetic */ Fu[] v;
    @DexIgnore
    public /* final */ Yt b;
    @DexIgnore
    public /* final */ Zt c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ Yt e;
    @DexIgnore
    public /* final */ Zt f;
    @DexIgnore
    public /* final */ byte[] g;

    /*
    static {
        Fu fu = new Fu("GET_CONNECTION_PARAMETERS", 0, Yt.c, Zt.e, null, null, null, null, 60);
        h = fu;
        Fu fu2 = new Fu("REQUEST_CONNECTION_PRIORITY", 1, Yt.d, Zt.d, null, null, Zt.e, null, 44);
        i = fu2;
        Fu fu3 = new Fu("PLAY_ANIMATION", 2, Yt.d, Zt.i, Au.c.b, null, null, null, 56);
        j = fu3;
        Yt yt = Yt.c;
        Zt zt = Zt.i;
        byte[] bArr = Au.d.b;
        Fu fu4 = new Fu("GET_OPTIMAL_PAYLOAD", 3, yt, zt, bArr, null, null, bArr, 24);
        k = fu4;
        Yt yt2 = Yt.c;
        Zt zt2 = Zt.i;
        byte[] bArr2 = Au.f.b;
        Fu fu5 = new Fu("BLE_TROUBLESHOOT", 4, yt2, zt2, bArr2, null, null, bArr2, 24);
        Fu fu6 = new Fu("REQUEST_HANDS", 5, Yt.d, Zt.g, Bu.c.b, null, null, null, 56);
        l = fu6;
        Fu fu7 = new Fu("RELEASE_HANDS", 6, Yt.d, Zt.g, Bu.d.b, null, null, null, 56);
        m = fu7;
        Fu fu8 = new Fu("MOVE_HANDS", 7, Yt.d, Zt.g, Bu.e.b, null, null, null, 56);
        n = fu8;
        Fu fu9 = new Fu("SET_CALIBRATION_POSITION", 8, Yt.d, Zt.j, Bu.f.b, null, null, null, 56);
        o = fu9;
        Yt yt3 = Yt.c;
        Zt zt3 = Zt.c;
        byte[] bArr3 = Eu.c.b;
        Fu fu10 = new Fu("GET_CURRENT_WORKOUT_SESSION", 9, yt3, zt3, bArr3, null, null, bArr3, 24);
        p = fu10;
        Fu fu11 = new Fu("STOP_CURRENT_WORKOUT_SESSION", 10, Yt.d, Zt.c, Eu.c.b, null, null, null, 56);
        q = fu11;
        Fu fu12 = new Fu("GET_HEARTBEAT_STATISTIC", 11, Yt.c, Zt.i, Au.e.b, Yt.e, Zt.i, Au.e.b);
        Fu fu13 = new Fu("GET_HEARTBEAT_INTERVAL", 12, Yt.c, Zt.f, Du.c.b, null, null, null, 56);
        Fu fu14 = new Fu("SET_HEARTBEAT_INTERVAL", 13, Yt.d, Zt.f, Du.c.b, null, null, null, 56);
        r = fu14;
        Fu fu15 = new Fu("REQUEST_DISCOVER_SERVICE", 14, Yt.d, Zt.h, Xt.c.b, null, null, null, 56);
        Fu fu16 = new Fu("CLEAN_UP_DEVICE", 15, Yt.d, Zt.i, Xt.d.b, null, null, null, 56);
        s = fu16;
        Fu fu17 = new Fu("LEGACY_OTA_ENTER", 16, Yt.d, Zt.i, Cu.c.b, null, null, Cu.d.b, 24);
        t = fu17;
        Fu fu18 = new Fu("LEGACY_OTA_RESET", 17, Yt.d, Zt.i, Cu.e.b, null, null, null, 56);
        u = fu18;
        v = new Fu[]{fu, fu2, fu3, fu4, fu5, fu6, fu7, fu8, fu9, fu10, fu11, fu12, fu13, fu14, fu15, fu16, fu17, fu18};
    }
    */

    @DexIgnore
    public Fu(String str, int i2, Yt yt, Zt zt, byte[] bArr, Yt yt2, Zt zt2, byte[] bArr2) {
        this.b = yt;
        this.c = zt;
        this.d = bArr;
        this.e = yt2;
        this.f = zt2;
        this.g = bArr2;
    }

    @DexIgnore
    public /* synthetic */ Fu(String str, int i2, Yt yt, Zt zt, byte[] bArr, Yt yt2, Zt zt2, byte[] bArr2, int i3) {
        bArr = (i3 & 4) != 0 ? new byte[0] : bArr;
        yt2 = (i3 & 8) != 0 ? Yt.e : yt2;
        zt2 = (i3 & 16) != 0 ? zt : zt2;
        bArr2 = (i3 & 32) != 0 ? new byte[0] : bArr2;
        this.b = yt;
        this.c = zt;
        this.d = bArr;
        this.e = yt2;
        this.f = zt2;
        this.g = bArr2;
    }

    @DexIgnore
    public static Fu valueOf(String str) {
        return (Fu) Enum.valueOf(Fu.class, str);
    }

    @DexIgnore
    public static Fu[] values() {
        return (Fu[]) v.clone();
    }
}
