package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yo4 implements Factory<ApiServiceV2> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<AuthenticationInterceptor> b;
    @DexIgnore
    public /* final */ Provider<Uq5> c;

    @DexIgnore
    public Yo4(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static Yo4 a(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        return new Yo4(uo4, provider, provider2);
    }

    @DexIgnore
    public static ApiServiceV2 c(Uo4 uo4, AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        ApiServiceV2 e = uo4.e(authenticationInterceptor, uq5);
        Lk7.c(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }

    @DexIgnore
    public ApiServiceV2 b() {
        return c(this.a, this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
