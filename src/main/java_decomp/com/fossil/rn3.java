package com.fossil;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rn3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;
    @DexIgnore
    public long f;
    @DexIgnore
    public Xs2 g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public Long i;

    @DexIgnore
    public Rn3(Context context, Xs2 xs2, Long l) {
        Rc2.k(context);
        Context applicationContext = context.getApplicationContext();
        Rc2.k(applicationContext);
        this.a = applicationContext;
        this.i = l;
        if (xs2 != null) {
            this.g = xs2;
            this.b = xs2.g;
            this.c = xs2.f;
            this.d = xs2.e;
            this.h = xs2.d;
            this.f = xs2.c;
            Bundle bundle = xs2.h;
            if (bundle != null) {
                this.e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
