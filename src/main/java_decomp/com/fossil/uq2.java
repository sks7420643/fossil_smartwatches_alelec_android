package com.fossil;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Uq2 extends IInterface {
    @DexIgnore
    void O2(Ia3 ia3, Wq2 wq2, String str) throws RemoteException;

    @DexIgnore
    void X1(boolean z) throws RemoteException;

    @DexIgnore
    void c1(Vr2 vr2) throws RemoteException;

    @DexIgnore
    void g2(Kr2 kr2) throws RemoteException;

    @DexIgnore
    Location zza(String str) throws RemoteException;
}
