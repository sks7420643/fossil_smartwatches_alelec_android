package com.fossil;

import com.fossil.V18;
import com.fossil.blesdk.network.interceptor.AuthenticationInterceptor$intercept$accessToken$Anon1;
import com.mapped.Wg6;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wc0 extends Rb0 implements Interceptor {
    @DexIgnore
    public Wc0(Ft1 ft1) {
        super(ft1);
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        boolean z = true;
        V18.Ai h = chain.c().h();
        String str = (String) Fu7.b(null, new AuthenticationInterceptor$intercept$accessToken$Anon1(this, null), 1, null);
        if (str != null && !Vt7.l(str)) {
            z = false;
        }
        if (!z) {
            h.a("Authorization", "Bearer " + str);
        }
        Response d = chain.d(h.b());
        Wg6.b(d, "chain.proceed(requestBuilder.build())");
        return d;
    }
}
