package com.fossil;

import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cq4 implements Factory<DownloadServiceApi> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Cq4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Cq4 a(Uo4 uo4) {
        return new Cq4(uo4);
    }

    @DexIgnore
    public static DownloadServiceApi c(Uo4 uo4) {
        DownloadServiceApi J = uo4.J();
        Lk7.c(J, "Cannot return null from a non-@Nullable @Provides method");
        return J;
    }

    @DexIgnore
    public DownloadServiceApi b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
