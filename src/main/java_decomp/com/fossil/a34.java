package com.fossil;

import com.fossil.U24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class A34<K, V> implements Map<K, V>, Serializable {
    @DexIgnore
    public static /* final */ Map.Entry<?, ?>[] EMPTY_ENTRY_ARRAY; // = new Map.Entry[0];
    @DexIgnore
    @LazyInit
    public transient H34<Map.Entry<K, V>> b;
    @DexIgnore
    @LazyInit
    public transient H34<K> c;
    @DexIgnore
    @LazyInit
    public transient U24<V> d;
    @DexIgnore
    @LazyInit
    public transient I34<K, V> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends H54<K> {
        @DexIgnore
        public /* final */ /* synthetic */ H54 b;

        @DexIgnore
        public Ai(A34 a34, H54 h54) {
            this.b = h54;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public K next() {
            return (K) ((Map.Entry) this.b.next()).getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<K, V> {
        @DexIgnore
        public Comparator<? super V> a;
        @DexIgnore
        public B34<K, V>[] b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public Bi() {
            this(4);
        }

        @DexIgnore
        public Bi(int i) {
            this.b = new B34[i];
            this.c = 0;
            this.d = false;
        }

        @DexIgnore
        public A34<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return A34.of();
            }
            if (i == 1) {
                return A34.of((Object) this.b[0].getKey(), (Object) this.b[0].getValue());
            }
            if (this.a != null) {
                if (this.d) {
                    this.b = (B34[]) H44.a(this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, I44.from(this.a).onResultOf(X34.s()));
            }
            this.d = this.c == this.b.length;
            return P44.fromEntryArray(this.c, this.b);
        }

        @DexIgnore
        public final void b(int i) {
            B34<K, V>[] b34Arr = this.b;
            if (i > b34Arr.length) {
                this.b = (B34[]) H44.a(b34Arr, U24.Bi.d(b34Arr.length, i));
                this.d = false;
            }
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> c(K k, V v) {
            b(this.c + 1);
            B34<K, V> entryOf = A34.entryOf(k, v);
            B34<K, V>[] b34Arr = this.b;
            int i = this.c;
            this.c = i + 1;
            b34Arr[i] = entryOf;
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.A34$Bi<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public Bi<K, V> d(Map.Entry<? extends K, ? extends V> entry) {
            return c(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> e(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            if (iterable instanceof Collection) {
                b(((Collection) iterable).size() + this.c);
            }
            for (Map.Entry<? extends K, ? extends V> entry : iterable) {
                d(entry);
            }
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> f(Map<? extends K, ? extends V> map) {
            return e(map.entrySet());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<K, V> extends A34<K, V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends C34<K, V> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
            public H54<Map.Entry<K, V>> iterator() {
                return Ci.this.entryIterator();
            }

            @DexIgnore
            @Override // com.fossil.C34
            public A34<K, V> map() {
                return Ci.this;
            }
        }

        @DexIgnore
        @Override // com.fossil.A34
        public H34<Map.Entry<K, V>> createEntrySet() {
            return new Aii();
        }

        @DexIgnore
        public abstract H54<Map.Entry<K, V>> entryIterator();

        @DexIgnore
        @Override // java.util.Map, com.fossil.A34, com.fossil.A34
        public /* bridge */ /* synthetic */ Set entrySet() {
            return A34.super.entrySet();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.A34, com.fossil.A34
        public /* bridge */ /* synthetic */ Set keySet() {
            return A34.super.keySet();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.A34, com.fossil.A34
        public /* bridge */ /* synthetic */ Collection values() {
            return A34.super.values();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends Ci<K, H34<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends H54<Map.Entry<K, H34<V>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Iterator b;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Aiii extends T14<K, H34<V>> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry b;

                @DexIgnore
                public Aiii(Aii aii, Map.Entry entry) {
                    this.b = entry;
                }

                @DexIgnore
                public H34<V> a() {
                    return H34.of(this.b.getValue());
                }

                @DexIgnore
                @Override // java.util.Map.Entry, com.fossil.T14
                public K getKey() {
                    return (K) this.b.getKey();
                }

                @DexIgnore
                @Override // java.util.Map.Entry, com.fossil.T14
                public /* bridge */ /* synthetic */ Object getValue() {
                    return a();
                }
            }

            @DexIgnore
            public Aii(Di di, Iterator it) {
                this.b = it;
            }

            @DexIgnore
            public Map.Entry<K, H34<V>> a() {
                return new Aiii(this, (Map.Entry) this.b.next());
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public /* bridge */ /* synthetic */ Object next() {
                return a();
            }
        }

        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public /* synthetic */ Di(A34 a34, Ai ai) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.A34
        public boolean containsKey(Object obj) {
            return A34.this.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.A34.Ci
        public H54<Map.Entry<K, H34<V>>> entryIterator() {
            return new Aii(this, A34.this.entrySet().iterator());
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.A34
        public H34<V> get(Object obj) {
            Object obj2 = A34.this.get(obj);
            if (obj2 == null) {
                return null;
            }
            return H34.of(obj2);
        }

        @DexIgnore
        @Override // com.fossil.A34
        public int hashCode() {
            return A34.this.hashCode();
        }

        @DexIgnore
        @Override // com.fossil.A34
        public boolean isHashCodeFast() {
            return A34.this.isHashCodeFast();
        }

        @DexIgnore
        @Override // com.fossil.A34
        public boolean isPartialView() {
            return A34.this.isPartialView();
        }

        @DexIgnore
        @Override // com.fossil.A34.Ci, java.util.Map, com.fossil.A34, com.fossil.A34
        public H34<K> keySet() {
            return A34.this.keySet();
        }

        @DexIgnore
        public int size() {
            return A34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] keys;
        @DexIgnore
        public /* final */ Object[] values;

        @DexIgnore
        public Ei(A34<?, ?> a34) {
            this.keys = new Object[a34.size()];
            this.values = new Object[a34.size()];
            Iterator it = a34.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.keys[i] = entry.getKey();
                this.values[i] = entry.getValue();
                i++;
            }
        }

        @DexIgnore
        public Object createMap(Bi<Object, Object> bi) {
            int i = 0;
            while (true) {
                Object[] objArr = this.keys;
                if (i >= objArr.length) {
                    return bi.a();
                }
                bi.c(objArr[i], this.values[i]);
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            return createMap(new Bi<>(this.keys.length));
        }
    }

    @DexIgnore
    public static <K extends Enum<K>, V> A34<K, V> a(EnumMap<K, ? extends V> enumMap) {
        EnumMap enumMap2 = new EnumMap((EnumMap) enumMap);
        for (Map.Entry<K, V> entry : enumMap2.entrySet()) {
            A24.a(entry.getKey(), entry.getValue());
        }
        return W24.asImmutable(enumMap2);
    }

    @DexIgnore
    public static <K, V> Bi<K, V> builder() {
        return new Bi<>();
    }

    @DexIgnore
    public static void checkNoConflict(boolean z, String str, Map.Entry<?, ?> entry, Map.Entry<?, ?> entry2) {
        if (!z) {
            throw new IllegalArgumentException("Multiple entries with same " + str + ": " + entry + " and " + entry2);
        }
    }

    @DexIgnore
    public static <K, V> A34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) O34.i(iterable, EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return P44.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static <K, V> A34<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if ((map instanceof A34) && !(map instanceof K34)) {
            A34<K, V> a34 = (A34) map;
            if (!a34.isPartialView()) {
                return a34;
            }
        } else if (map instanceof EnumMap) {
            return a((EnumMap) map);
        }
        return copyOf(map.entrySet());
    }

    @DexIgnore
    public static <K, V> B34<K, V> entryOf(K k, V v) {
        return new B34<>(k, v);
    }

    @DexIgnore
    public static <K, V> A34<K, V> of() {
        return T24.of();
    }

    @DexIgnore
    public static <K, V> A34<K, V> of(K k, V v) {
        return T24.of((Object) k, (Object) v);
    }

    @DexIgnore
    public static <K, V> A34<K, V> of(K k, V v, K k2, V v2) {
        return P44.fromEntries(entryOf(k, v), entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> A34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return P44.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3));
    }

    @DexIgnore
    public static <K, V> A34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return P44.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> A34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return P44.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4), entryOf(k5, v5));
    }

    @DexIgnore
    public I34<K, V> asMultimap() {
        if (isEmpty()) {
            return I34.of();
        }
        I34<K, V> i34 = this.e;
        if (i34 != null) {
            return i34;
        }
        I34<K, V> i342 = new I34<>(new Di(this, null), size(), null);
        this.e = i342;
        return i342;
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    @DexIgnore
    public abstract H34<Map.Entry<K, V>> createEntrySet();

    @DexIgnore
    public H34<K> createKeySet() {
        return isEmpty() ? H34.of() : new D34(this);
    }

    @DexIgnore
    public U24<V> createValues() {
        return new E34(this);
    }

    @DexIgnore
    @Override // java.util.Map
    public H34<Map.Entry<K, V>> entrySet() {
        H34<Map.Entry<K, V>> h34 = this.b;
        if (h34 != null) {
            return h34;
        }
        H34<Map.Entry<K, V>> createEntrySet = createEntrySet();
        this.b = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return X34.d(this, obj);
    }

    @DexIgnore
    @Override // java.util.Map
    public abstract V get(Object obj);

    @DexIgnore
    public int hashCode() {
        return X44.b(entrySet());
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    public H54<K> keyIterator() {
        return new Ai(this, entrySet().iterator());
    }

    @DexIgnore
    @Override // java.util.Map
    public H34<K> keySet() {
        H34<K> h34 = this.c;
        if (h34 != null) {
            return h34;
        }
        H34<K> createKeySet = createKeySet();
        this.c = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return X34.p(this);
    }

    @DexIgnore
    @Override // java.util.Map
    public U24<V> values() {
        U24<V> u24 = this.d;
        if (u24 != null) {
            return u24;
        }
        U24<V> createValues = createValues();
        this.d = createValues;
        return createValues;
    }

    @DexIgnore
    public Object writeReplace() {
        return new Ei(this);
    }
}
