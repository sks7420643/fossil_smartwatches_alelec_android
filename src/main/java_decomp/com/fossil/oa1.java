package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.Dc1;
import com.fossil.Fc1;
import com.fossil.Ff1;
import com.fossil.Hf1;
import com.fossil.If1;
import com.fossil.Jf1;
import com.fossil.Kf1;
import com.fossil.Lf1;
import com.fossil.Mf1;
import com.fossil.Ne1;
import com.fossil.Nf1;
import com.fossil.Oe1;
import com.fossil.Of1;
import com.fossil.Pf1;
import com.fossil.Qe1;
import com.fossil.Qf1;
import com.fossil.Re1;
import com.fossil.Se1;
import com.fossil.Wg1;
import com.fossil.Xe1;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oa1 implements ComponentCallbacks2 {
    @DexIgnore
    public static volatile Oa1 j;
    @DexIgnore
    public static volatile boolean k;
    @DexIgnore
    public /* final */ Rd1 b;
    @DexIgnore
    public /* final */ Ie1 c;
    @DexIgnore
    public /* final */ Qa1 d;
    @DexIgnore
    public /* final */ Ua1 e;
    @DexIgnore
    public /* final */ Od1 f;
    @DexIgnore
    public /* final */ Hi1 g;
    @DexIgnore
    public /* final */ Zh1 h;
    @DexIgnore
    public /* final */ List<Wa1> i; // = new ArrayList();

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Fj1 build();
    }

    @DexIgnore
    public Oa1(Context context, Xc1 xc1, Ie1 ie1, Rd1 rd1, Od1 od1, Hi1 hi1, Zh1 zh1, int i2, Ai ai, Map<Class<?>, Xa1<?, ?>> map, List<Ej1<Object>> list, boolean z, boolean z2) {
        Qb1 ag1;
        Qb1 sg1;
        Ra1 ra1 = Ra1.NORMAL;
        this.b = rd1;
        this.f = od1;
        this.c = ie1;
        this.g = hi1;
        this.h = zh1;
        Resources resources = context.getResources();
        Ua1 ua1 = new Ua1();
        this.e = ua1;
        ua1.o(new Eg1());
        if (Build.VERSION.SDK_INT >= 27) {
            this.e.o(new Jg1());
        }
        List<ImageHeaderParser> g2 = this.e.g();
        Fh1 fh1 = new Fh1(context, g2, rd1, od1);
        Qb1<ParcelFileDescriptor, Bitmap> h2 = Vg1.h(rd1);
        Gg1 gg1 = new Gg1(this.e.g(), resources.getDisplayMetrics(), rd1, od1);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            ag1 = new Ag1(gg1);
            sg1 = new Sg1(gg1, od1);
        } else {
            sg1 = new Ng1();
            ag1 = new Bg1();
        }
        Bh1 bh1 = new Bh1(context);
        Ff1.Ci ci = new Ff1.Ci(resources);
        Ff1.Di di = new Ff1.Di(resources);
        Ff1.Bi bi = new Ff1.Bi(resources);
        Ff1.Ai ai2 = new Ff1.Ai(resources);
        Wf1 wf1 = new Wf1(od1);
        Ph1 ph1 = new Ph1();
        Sh1 sh1 = new Sh1();
        ContentResolver contentResolver = context.getContentResolver();
        Ua1 ua12 = this.e;
        ua12.a(ByteBuffer.class, new Pe1());
        ua12.a(InputStream.class, new Gf1(od1));
        ua12.e("Bitmap", ByteBuffer.class, Bitmap.class, ag1);
        ua12.e("Bitmap", InputStream.class, Bitmap.class, sg1);
        if (Fc1.c()) {
            this.e.e("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new Pg1(gg1));
        }
        Ua1 ua13 = this.e;
        ua13.e("Bitmap", ParcelFileDescriptor.class, Bitmap.class, h2);
        ua13.e("Bitmap", AssetFileDescriptor.class, Bitmap.class, Vg1.c(rd1));
        ua13.d(Bitmap.class, Bitmap.class, If1.Ai.a());
        ua13.e("Bitmap", Bitmap.class, Bitmap.class, new Ug1());
        ua13.b(Bitmap.class, wf1);
        ua13.e("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new Uf1(resources, ag1));
        ua13.e("BitmapDrawable", InputStream.class, BitmapDrawable.class, new Uf1(resources, sg1));
        ua13.e("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new Uf1(resources, h2));
        ua13.b(BitmapDrawable.class, new Vf1(rd1, wf1));
        ua13.e("Gif", InputStream.class, Hh1.class, new Oh1(g2, fh1, od1));
        ua13.e("Gif", ByteBuffer.class, Hh1.class, fh1);
        ua13.b(Hh1.class, new Ih1());
        ua13.d(Bb1.class, Bb1.class, If1.Ai.a());
        ua13.e("Bitmap", Bb1.class, Bitmap.class, new Mh1(rd1));
        ua13.c(Uri.class, Drawable.class, bh1);
        ua13.c(Uri.class, Bitmap.class, new Rg1(bh1, rd1));
        ua13.p(new Wg1.Ai());
        ua13.d(File.class, ByteBuffer.class, new Qe1.Bi());
        ua13.d(File.class, InputStream.class, new Se1.Ei());
        ua13.c(File.class, File.class, new Dh1());
        ua13.d(File.class, ParcelFileDescriptor.class, new Se1.Bi());
        ua13.d(File.class, File.class, If1.Ai.a());
        ua13.p(new Dc1.Ai(od1));
        if (Fc1.c()) {
            this.e.p(new Fc1.Ai());
        }
        Ua1 ua14 = this.e;
        ua14.d(Integer.TYPE, InputStream.class, ci);
        ua14.d(Integer.TYPE, ParcelFileDescriptor.class, bi);
        ua14.d(Integer.class, InputStream.class, ci);
        ua14.d(Integer.class, ParcelFileDescriptor.class, bi);
        ua14.d(Integer.class, Uri.class, di);
        ua14.d(Integer.TYPE, AssetFileDescriptor.class, ai2);
        ua14.d(Integer.class, AssetFileDescriptor.class, ai2);
        ua14.d(Integer.TYPE, Uri.class, di);
        ua14.d(String.class, InputStream.class, new Re1.Ci());
        ua14.d(Uri.class, InputStream.class, new Re1.Ci());
        ua14.d(String.class, InputStream.class, new Hf1.Ci());
        ua14.d(String.class, ParcelFileDescriptor.class, new Hf1.Bi());
        ua14.d(String.class, AssetFileDescriptor.class, new Hf1.Ai());
        ua14.d(Uri.class, InputStream.class, new Mf1.Ai());
        ua14.d(Uri.class, InputStream.class, new Ne1.Ci(context.getAssets()));
        ua14.d(Uri.class, ParcelFileDescriptor.class, new Ne1.Bi(context.getAssets()));
        ua14.d(Uri.class, InputStream.class, new Nf1.Ai(context));
        ua14.d(Uri.class, InputStream.class, new Of1.Ai(context));
        if (Build.VERSION.SDK_INT >= 29) {
            this.e.d(Uri.class, InputStream.class, new Pf1.Ci(context));
            this.e.d(Uri.class, ParcelFileDescriptor.class, new Pf1.Bi(context));
        }
        Ua1 ua15 = this.e;
        ua15.d(Uri.class, InputStream.class, new Jf1.Di(contentResolver));
        ua15.d(Uri.class, ParcelFileDescriptor.class, new Jf1.Bi(contentResolver));
        ua15.d(Uri.class, AssetFileDescriptor.class, new Jf1.Ai(contentResolver));
        ua15.d(Uri.class, InputStream.class, new Kf1.Ai());
        ua15.d(URL.class, InputStream.class, new Qf1.Ai());
        ua15.d(Uri.class, File.class, new Xe1.Ai(context));
        ua15.d(Te1.class, InputStream.class, new Lf1.Ai());
        ua15.d(byte[].class, ByteBuffer.class, new Oe1.Ai());
        ua15.d(byte[].class, InputStream.class, new Oe1.Di());
        ua15.d(Uri.class, Uri.class, If1.Ai.a());
        ua15.d(Drawable.class, Drawable.class, If1.Ai.a());
        ua15.c(Drawable.class, Drawable.class, new Ch1());
        ua15.q(Bitmap.class, BitmapDrawable.class, new Qh1(resources));
        ua15.q(Bitmap.class, byte[].class, ph1);
        ua15.q(Drawable.class, byte[].class, new Rh1(rd1, ph1, sh1));
        ua15.q(Hh1.class, byte[].class, sh1);
        if (Build.VERSION.SDK_INT >= 23) {
            Qb1<ByteBuffer, Bitmap> d2 = Vg1.d(rd1);
            this.e.c(ByteBuffer.class, Bitmap.class, d2);
            this.e.c(ByteBuffer.class, BitmapDrawable.class, new Uf1(resources, d2));
        }
        this.d = new Qa1(context, od1, this.e, new Oj1(), ai, map, list, xc1, z, i2);
    }

    @DexIgnore
    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!k) {
            k = true;
            m(context, generatedAppGlideModule);
            k = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    public static Oa1 c(Context context) {
        if (j == null) {
            GeneratedAppGlideModule d2 = d(context.getApplicationContext());
            synchronized (Oa1.class) {
                try {
                    if (j == null) {
                        a(context, d2);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return j;
    }

    @DexIgnore
    public static GeneratedAppGlideModule d(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(Context.class).newInstance(context.getApplicationContext());
        } catch (ClassNotFoundException e2) {
            if (!Log.isLoggable("Glide", 5)) {
                return null;
            }
            Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            return null;
        } catch (InstantiationException e3) {
            q(e3);
            throw null;
        } catch (IllegalAccessException e4) {
            q(e4);
            throw null;
        } catch (NoSuchMethodException e5) {
            q(e5);
            throw null;
        } catch (InvocationTargetException e6) {
            q(e6);
            throw null;
        }
    }

    @DexIgnore
    public static Hi1 l(Context context) {
        Ik1.e(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return c(context).k();
    }

    @DexIgnore
    public static void m(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        n(context, new Pa1(), generatedAppGlideModule);
    }

    @DexIgnore
    public static void n(Context context, Pa1 pa1, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<Oi1> a2 = (generatedAppGlideModule == null || generatedAppGlideModule.c()) ? new Qi1(applicationContext).a() : Collections.emptyList();
        if (generatedAppGlideModule != null && !generatedAppGlideModule.d().isEmpty()) {
            Set<Class<?>> d2 = generatedAppGlideModule.d();
            Iterator<Oi1> it = a2.iterator();
            while (it.hasNext()) {
                Oi1 next = it.next();
                if (d2.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            Iterator<Oi1> it2 = a2.iterator();
            while (it2.hasNext()) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + it2.next().getClass());
            }
        }
        pa1.b(generatedAppGlideModule != null ? generatedAppGlideModule.e() : null);
        for (Oi1 oi1 : a2) {
            oi1.a(applicationContext, pa1);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, pa1);
        }
        Oa1 a3 = pa1.a(applicationContext);
        for (Oi1 oi12 : a2) {
            try {
                oi12.b(applicationContext, a3, a3.e);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + oi12.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.b(applicationContext, a3, a3.e);
        }
        applicationContext.registerComponentCallbacks(a3);
        j = a3;
    }

    @DexIgnore
    public static void q(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    public static Wa1 t(Context context) {
        return l(context).k(context);
    }

    @DexIgnore
    public static Wa1 u(View view) {
        return l(view.getContext()).l(view);
    }

    @DexIgnore
    public static Wa1 v(Fragment fragment) {
        return l(fragment.getContext()).m(fragment);
    }

    @DexIgnore
    public void b() {
        Jk1.b();
        this.c.d();
        this.b.d();
        this.f.d();
    }

    @DexIgnore
    public Od1 e() {
        return this.f;
    }

    @DexIgnore
    public Rd1 f() {
        return this.b;
    }

    @DexIgnore
    public Zh1 g() {
        return this.h;
    }

    @DexIgnore
    public Context h() {
        return this.d.getBaseContext();
    }

    @DexIgnore
    public Qa1 i() {
        return this.d;
    }

    @DexIgnore
    public Ua1 j() {
        return this.e;
    }

    @DexIgnore
    public Hi1 k() {
        return this.g;
    }

    @DexIgnore
    public void o(Wa1 wa1) {
        synchronized (this.i) {
            if (!this.i.contains(wa1)) {
                this.i.add(wa1);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        b();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        r(i2);
    }

    @DexIgnore
    public boolean p(Qj1<?> qj1) {
        synchronized (this.i) {
            for (Wa1 wa1 : this.i) {
                if (wa1.A(qj1)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void r(int i2) {
        Jk1.b();
        for (Wa1 wa1 : this.i) {
            wa1.onTrimMemory(i2);
        }
        this.c.a(i2);
        this.b.a(i2);
        this.f.a(i2);
    }

    @DexIgnore
    public void s(Wa1 wa1) {
        synchronized (this.i) {
            if (this.i.contains(wa1)) {
                this.i.remove(wa1);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }
}
