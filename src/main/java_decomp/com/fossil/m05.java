package com.fossil;

import android.text.TextUtils;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Collection;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M05 {
    @DexIgnore
    public final ArrayList<String> a(String str) {
        Wg6.c(str, "arrayValue");
        ArrayList<String> arrayList = new ArrayList<>();
        if (TextUtils.isEmpty(str)) {
            return arrayList;
        }
        JSONArray jSONArray = new JSONArray(str);
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            Object obj = jSONArray.get(i);
            if (obj != null) {
                arrayList.add((String) obj);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.String");
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final String b(ArrayList<String> arrayList) {
        if (arrayList == null) {
            return "";
        }
        String jSONArray = new JSONArray((Collection) arrayList).toString();
        Wg6.b(jSONArray, "JSONArray(value).toString()");
        return jSONArray;
    }
}
