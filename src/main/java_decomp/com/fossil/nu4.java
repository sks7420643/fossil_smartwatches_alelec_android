package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nu4 implements Factory<BCCreateChallengeInputViewModel> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<Tt4> b;

    @DexIgnore
    public Nu4(Provider<An4> provider, Provider<Tt4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Nu4 a(Provider<An4> provider, Provider<Tt4> provider2) {
        return new Nu4(provider, provider2);
    }

    @DexIgnore
    public static BCCreateChallengeInputViewModel c(An4 an4, Tt4 tt4) {
        return new BCCreateChallengeInputViewModel(an4, tt4);
    }

    @DexIgnore
    public BCCreateChallengeInputViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
