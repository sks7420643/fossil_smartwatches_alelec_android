package com.fossil;

import com.fossil.M62;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M82 extends R82 {
    @DexIgnore
    public /* final */ Map<M62.Fi, J82> c;
    @DexIgnore
    public /* final */ /* synthetic */ H82 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public M82(H82 h82, Map<M62.Fi, J82> map) {
        super(h82, null);
        this.d = h82;
        this.c = map;
    }

    @DexIgnore
    @Override // com.fossil.R82
    public final void a() {
        int i = 0;
        Ic2 ic2 = new Ic2(this.d.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (M62.Fi fi : this.c.keySet()) {
            if (!fi.r() || (this.c.get(fi).c)) {
                arrayList2.add(fi);
            } else {
                arrayList.add(fi);
            }
        }
        int i2 = -1;
        if (arrayList.isEmpty()) {
            int size = arrayList2.size();
            while (i < size) {
                Object obj = arrayList2.get(i);
                i++;
                i2 = ic2.b(this.d.c, (M62.Fi) obj);
                if (i2 == 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList.size();
            int i3 = 0;
            while (i3 < size2) {
                Object obj2 = arrayList.get(i3);
                i3++;
                i2 = ic2.b(this.d.c, (M62.Fi) obj2);
                if (i2 != 0) {
                    break;
                }
            }
        }
        if (i2 != 0) {
            this.d.a.p(new L82(this, this.d, new Z52(i2, null)));
            return;
        }
        if ((this.d.m) && this.d.k != null) {
            this.d.k.b();
        }
        for (M62.Fi fi2 : this.c.keySet()) {
            J82 j82 = this.c.get(fi2);
            if (!fi2.r() || ic2.b(this.d.c, fi2) == 0) {
                fi2.l(j82);
            } else {
                this.d.a.p(new O82(this, this.d, j82));
            }
        }
    }
}
