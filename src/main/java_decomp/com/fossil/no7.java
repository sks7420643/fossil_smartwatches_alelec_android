package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class No7 {
    @DexIgnore
    public static final int a(int i, int i2, int i3) {
        return e(e(i, i3) - e(i2, i3), i3);
    }

    @DexIgnore
    public static final long b(long j, long j2, long j3) {
        return f(f(j, j3) - f(j2, j3), j3);
    }

    @DexIgnore
    public static final int c(int i, int i2, int i3) {
        if (i3 > 0) {
            return i >= i2 ? i2 : i2 - a(i2, i, i3);
        }
        if (i3 < 0) {
            return i > i2 ? i2 + a(i, i2, -i3) : i2;
        }
        throw new IllegalArgumentException("Step is zero.");
    }

    @DexIgnore
    public static final long d(long j, long j2, long j3) {
        int i = (j3 > 0 ? 1 : (j3 == 0 ? 0 : -1));
        if (i > 0) {
            return j >= j2 ? j2 : j2 - b(j2, j, j3);
        }
        if (i < 0) {
            return j > j2 ? j2 + b(j, j2, -j3) : j2;
        }
        throw new IllegalArgumentException("Step is zero.");
    }

    @DexIgnore
    public static final int e(int i, int i2) {
        int i3 = i % i2;
        return i3 >= 0 ? i3 : i3 + i2;
    }

    @DexIgnore
    public static final long f(long j, long j2) {
        long j3 = j % j2;
        return j3 >= 0 ? j3 : j3 + j2;
    }
}
