package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xm5 {
    @DexIgnore
    public static /* final */ String b; // = ("Localization_" + Xm5.class.getSimpleName());
    @DexIgnore
    public Map<String, String> a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public Ai(Xm5 xm5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public Bi(Xm5 xm5) {
        }
    }

    @DexIgnore
    public void a() {
        Map<String, String> map = this.a;
        if (map != null) {
            map.clear();
        }
    }

    @DexIgnore
    public Map<String, String> b() {
        return this.a;
    }

    @DexIgnore
    public String c(String str) {
        return (this.a == null || TextUtils.isEmpty(str)) ? "" : this.a.get(str);
    }

    @DexIgnore
    public void d(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "parseJSONFile() called with: filePath = [" + str + "], isDownloaded = [" + z + "]");
        try {
            Gson gson = new Gson();
            if (!z) {
                InputStream open = PortfolioApp.d0.getAssets().open(str);
                InputStreamReader inputStreamReader = new InputStreamReader(open, "UTF-8");
                try {
                    this.a.putAll((Map) gson.j(inputStreamReader, new Ai(this).getType()));
                    inputStreamReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    inputStreamReader.close();
                } catch (Throwable th) {
                    inputStreamReader.close();
                    open.close();
                    throw th;
                }
                open.close();
                return;
            }
            FileReader fileReader = new FileReader(new File(str));
            try {
                this.a.putAll((Map) gson.j(fileReader, new Bi(this).getType()));
            } catch (Exception e2) {
                e2.printStackTrace();
            } catch (Throwable th2) {
                fileReader.close();
                throw th2;
            }
            fileReader.close();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.e(str3, "parseJSONFile failed exception=" + e3);
        }
    }
}
