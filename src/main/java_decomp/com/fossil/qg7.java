package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qg7 extends Ng7 {
    @DexIgnore
    public static String o;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;

    @DexIgnore
    public Qg7(Context context, int i, Jg7 jg7) {
        super(context, i, jg7);
        this.m = Tg7.a(context).e();
        if (o == null) {
            o = Ei7.E(context);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public Og7 a() {
        return Og7.h;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public boolean b(JSONObject jSONObject) {
        Ji7.d(jSONObject, "op", o);
        Ji7.d(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }

    @DexIgnore
    public void i(String str) {
        this.n = str;
    }
}
