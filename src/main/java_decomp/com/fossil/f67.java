package com.fossil;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class F67 extends RecyclerView.q {
    @DexIgnore
    public int a;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public int c; // = 1;
    @DexIgnore
    public /* final */ LinearLayoutManager d;

    @DexIgnore
    public F67(LinearLayoutManager linearLayoutManager) {
        Wg6.c(linearLayoutManager, "mLinearLayoutManager");
        this.d = linearLayoutManager;
    }

    @DexIgnore
    public final void a(RecyclerView recyclerView) {
        int childCount = recyclerView.getChildCount();
        int Z = this.d.Z();
        int a2 = this.d.a2();
        if (this.b && Z > this.a) {
            this.b = false;
            this.a = Z;
        }
        if (!this.b && (Z - a2) - childCount == 0) {
            int i = this.c + 1;
            this.c = i;
            b(i);
            this.b = true;
        }
    }

    @DexIgnore
    public abstract void b(int i);

    @DexIgnore
    public abstract void c(int i, int i2);

    @DexIgnore
    public final void d() {
        this.c = 1;
        this.a = 0;
        this.b = true;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        Wg6.c(recyclerView, "recyclerView");
        super.onScrollStateChanged(recyclerView, i);
        if (i == 0) {
            a(recyclerView);
            c(this.d.a2(), this.d.d2());
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        Wg6.c(recyclerView, "recyclerView");
        super.onScrolled(recyclerView, i, i2);
        a(recyclerView);
    }
}
