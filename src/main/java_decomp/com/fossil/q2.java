package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q2 extends C2 {
    @DexIgnore
    public static /* final */ P2 CREATOR; // = new P2(null);
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ JSONObject f;

    @DexIgnore
    public Q2(byte b, int i, JSONObject jSONObject) {
        super(Lt.c, b, false, 4);
        this.e = i;
        this.f = jSONObject;
    }

    @DexIgnore
    public /* synthetic */ Q2(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            this.f = new JSONObject(readString);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Q2.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Q2 q2 = (Q2) obj;
            if (this.b != q2.b) {
                return false;
            }
            if (this.c != q2.c) {
                return false;
            }
            return this.e == q2.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.JSONRequestEvent");
    }

    @DexIgnore
    public int hashCode() {
        return (((this.b.hashCode() * 31) + this.c) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.q, Integer.valueOf(this.e)), Jd0.d3, this.f);
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f.toString());
        }
    }
}
