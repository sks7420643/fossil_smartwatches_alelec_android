package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Vw1 implements Serializable {
    TOP_PRESS,
    TOP_HOLD,
    TOP_SHORT_PRESS_RELEASE,
    TOP_LONG_PRESS_RELEASE,
    TOP_SINGLE_CLICK,
    TOP_DOUBLE_CLICK,
    MIDDLE_PRESS,
    MIDDLE_HOLD,
    MIDDLE_SHORT_PRESS_RELEASE,
    MIDDLE_LONG_PRESS_RELEASE,
    MIDDLE_SINGLE_CLICK,
    MIDDLE_DOUBLE_CLICK,
    BOTTOM_PRESS,
    BOTTOM_HOLD,
    BOTTOM_SHORT_PRESS_RELEASE,
    BOTTOM_LONG_PRESS_RELEASE,
    BOTTOM_SINGLE_CLICK,
    BOTTOM_DOUBLE_CLICK;
    
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Vw1 a(String str) {
            Vw1[] values = Vw1.values();
            for (Vw1 vw1 : values) {
                if (Wg6.a(Ey1.a(vw1), str)) {
                    return vw1;
                }
            }
            return null;
        }
    }
}
