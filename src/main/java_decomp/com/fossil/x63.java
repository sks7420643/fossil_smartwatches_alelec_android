package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X63 implements Xw2<A73> {
    @DexIgnore
    public static X63 c; // = new X63();
    @DexIgnore
    public /* final */ Xw2<A73> b;

    @DexIgnore
    public X63() {
        this(Ww2.b(new Z63()));
    }

    @DexIgnore
    public X63(Xw2<A73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((A73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((A73) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ A73 zza() {
        return this.b.zza();
    }
}
