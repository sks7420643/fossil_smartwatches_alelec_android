package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.logic.phase.SyncPhase$onStart$1$1", f = "SyncPhase.kt", l = {}, m = "invokeSuspend")
public final class wf extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ kg d;
    @DexIgnore
    public /* final */ /* synthetic */ lp e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wf(kg kgVar, lp lpVar, qn7 qn7) {
        super(2, qn7);
        this.d = kgVar;
        this.e = lpVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        wf wfVar = new wf(this.d, this.e, qn7);
        wfVar.b = (iv7) obj;
        throw null;
        //return wfVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        wf wfVar = new wf(this.d, this.e, qn7);
        wfVar.b = iv7;
        return wfVar.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.c == 0) {
            el7.b(obj);
            this.d.b.I.clear();
            ArrayList<j0> arrayList = this.d.b.I;
            lp lpVar = this.e;
            if (lpVar != null) {
                arrayList.addAll(((nm) lpVar).G);
                mi miVar = this.d.b;
                miVar.b0(miVar.I);
                mi miVar2 = this.d.b;
                miVar2.l(nr.a(miVar2.v, null, zq.SUCCESS, null, null, 13));
                return tl7.f3441a;
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.logic.phase.LegacySyncPhase");
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
