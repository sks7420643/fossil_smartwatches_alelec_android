package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<ArrayList<String>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<ArrayList<String>> {
    }

    @DexIgnore
    public final String a(ArrayList<String> arrayList) {
        if (Ff2.a(arrayList)) {
            return "";
        }
        String u = new Gson().u(arrayList, new Ai().getType());
        Wg6.b(u, "Gson().toJson(list, type)");
        return u;
    }

    @DexIgnore
    public final ArrayList<String> b(String str) {
        Wg6.c(str, "value");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object l = new Gson().l(str, new Bi().getType());
        Wg6.b(l, "Gson().fromJson(value, type)");
        return (ArrayList) l;
    }
}
