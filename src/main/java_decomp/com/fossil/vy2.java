package com.fossil;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vy2<E> extends Gy2<E> {
    @DexIgnore
    public static /* final */ Vy2<Comparable> zzb; // = new Vy2<>(Sx2.zza(), Ly2.zza);
    @DexIgnore
    public /* final */ transient Sx2<E> e;

    @DexIgnore
    public Vy2(Sx2<E> sx2, Comparator<? super E> comparator) {
        super(comparator);
        this.e = sx2;
    }

    @DexIgnore
    public final Vy2<E> a(int i, int i2) {
        return (i == 0 && i2 == size()) ? this : i < i2 ? new Vy2<>((Sx2) this.e.subList(i, i2), this.zza) : Gy2.zza(this.zza);
    }

    @DexIgnore
    public final int b(E e2, boolean z) {
        Sx2<E> sx2 = this.e;
        Sw2.b(e2);
        int binarySearch = Collections.binarySearch(sx2, e2, comparator());
        return (binarySearch < 0 || !z) ? binarySearch : binarySearch + 1;
    }

    @DexIgnore
    public final int c(E e2, boolean z) {
        Sx2<E> sx2 = this.e;
        Sw2.b(e2);
        int binarySearch = Collections.binarySearch(sx2, e2, comparator());
        return (binarySearch < 0 || z) ? binarySearch : binarySearch + 1;
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.NavigableSet
    public final E ceiling(E e2) {
        int c = c(e2, true);
        if (c == size()) {
            return null;
        }
        return this.e.get(c);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean contains(@NullableDecl Object obj) {
        if (obj != null) {
            try {
                if (Collections.binarySearch(this.e, obj, this.zza) >= 0) {
                    return true;
                }
            } catch (ClassCastException e2) {
            }
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, java.util.AbstractCollection
    public final boolean containsAll(Collection<?> collection) {
        if (collection instanceof My2) {
            collection = ((My2) collection).zza();
        }
        if (!Zy2.a(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        Cz2 cz2 = (Cz2) iterator();
        Iterator<?> it = collection.iterator();
        if (!cz2.hasNext()) {
            return false;
        }
        Object next = it.next();
        Object next2 = cz2.next();
        while (true) {
            try {
                int zza = zza(next2, next);
                if (zza < 0) {
                    if (!cz2.hasNext()) {
                        return false;
                    }
                    next2 = cz2.next();
                } else if (zza == 0) {
                    if (!it.hasNext()) {
                        return true;
                    }
                    next = it.next();
                } else if (zza > 0) {
                    break;
                }
            } catch (ClassCastException | NullPointerException e2) {
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.NavigableSet
    public final /* synthetic */ Iterator descendingIterator() {
        return zzj();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003a A[Catch:{ ClassCastException | NoSuchElementException -> 0x004e }] */
    @Override // com.fossil.Ay2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r7) {
        /*
            r6 = this;
            r1 = 1
            r2 = 0
            if (r7 != r6) goto L_0x0006
            r0 = r1
        L_0x0005:
            return r0
        L_0x0006:
            boolean r0 = r7 instanceof java.util.Set
            if (r0 != 0) goto L_0x000c
            r0 = r2
            goto L_0x0005
        L_0x000c:
            java.util.Set r7 = (java.util.Set) r7
            int r0 = r6.size()
            int r3 = r7.size()
            if (r0 == r3) goto L_0x001a
            r0 = r2
            goto L_0x0005
        L_0x001a:
            boolean r0 = r6.isEmpty()
            if (r0 == 0) goto L_0x0022
            r0 = r1
            goto L_0x0005
        L_0x0022:
            java.util.Comparator<? super E> r0 = r6.zza
            boolean r0 = com.fossil.Zy2.a(r0, r7)
            if (r0 == 0) goto L_0x0051
            java.util.Iterator r3 = r7.iterator()
            java.util.Iterator r0 = r6.iterator()     // Catch:{ ClassCastException -> 0x004e, NoSuchElementException -> 0x0056 }
            com.fossil.Cz2 r0 = (com.fossil.Cz2) r0     // Catch:{ ClassCastException -> 0x004e, NoSuchElementException -> 0x0056 }
        L_0x0034:
            boolean r4 = r0.hasNext()     // Catch:{ ClassCastException -> 0x004e, NoSuchElementException -> 0x0056 }
            if (r4 == 0) goto L_0x004c
            java.lang.Object r4 = r0.next()     // Catch:{ ClassCastException -> 0x004e, NoSuchElementException -> 0x0056 }
            java.lang.Object r5 = r3.next()     // Catch:{ ClassCastException -> 0x004e, NoSuchElementException -> 0x0056 }
            if (r5 == 0) goto L_0x004a
            int r4 = r6.zza(r4, r5)     // Catch:{ ClassCastException -> 0x004e, NoSuchElementException -> 0x0056 }
            if (r4 == 0) goto L_0x0034
        L_0x004a:
            r0 = r2
            goto L_0x0005
        L_0x004c:
            r0 = r1
            goto L_0x0005
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            r0 = r2
            goto L_0x0005
        L_0x0051:
            boolean r0 = r6.containsAll(r7)
            goto L_0x0005
        L_0x0056:
            r0 = move-exception
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vy2.equals(java.lang.Object):boolean");
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.SortedSet
    public final E first() {
        if (!isEmpty()) {
            return this.e.get(0);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.NavigableSet
    public final E floor(E e2) {
        int b = b(e2, true) - 1;
        if (b == -1) {
            return null;
        }
        return this.e.get(b);
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.NavigableSet
    public final E higher(E e2) {
        int c = c(e2, false);
        if (c == size()) {
            return null;
        }
        return this.e.get(c);
    }

    @DexIgnore
    @Override // com.fossil.Ay2, com.fossil.Gy2, java.util.Collection, java.util.Set, java.util.NavigableSet, java.lang.Iterable, com.fossil.Tx2, java.util.AbstractCollection
    public final /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.SortedSet
    public final E last() {
        if (!isEmpty()) {
            return this.e.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // com.fossil.Gy2, java.util.NavigableSet
    public final E lower(E e2) {
        int b = b(e2, false) - 1;
        if (b == -1) {
            return null;
        }
        return this.e.get(b);
    }

    @DexIgnore
    public final int size() {
        return this.e.size();
    }

    @DexIgnore
    @Override // com.fossil.Gy2
    public final Gy2<E> zza(E e2, boolean z) {
        return a(0, b(e2, z));
    }

    @DexIgnore
    @Override // com.fossil.Gy2
    public final Gy2<E> zza(E e2, boolean z, E e3, boolean z2) {
        return zzb(e2, z).zza(e3, z2);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzb(Object[] objArr, int i) {
        return this.e.zzb(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Cz2<E> zzb() {
        return (Cz2) this.e.iterator();
    }

    @DexIgnore
    @Override // com.fossil.Gy2
    public final Gy2<E> zzb(E e2, boolean z) {
        return a(c(e2, z), size());
    }

    @DexIgnore
    @Override // com.fossil.Ay2, com.fossil.Tx2
    public final Sx2<E> zzc() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Object[] zze() {
        return this.e.zze();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzf() {
        return this.e.zzf();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzg() {
        return this.e.zzg();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return this.e.zzh();
    }

    @DexIgnore
    @Override // com.fossil.Gy2
    public final Gy2<E> zzi() {
        Comparator reverseOrder = Collections.reverseOrder(this.zza);
        return isEmpty() ? Gy2.zza(reverseOrder) : new Vy2(this.e.zzd(), reverseOrder);
    }

    @DexIgnore
    @Override // com.fossil.Gy2
    public final Cz2<E> zzj() {
        return (Cz2) this.e.zzd().iterator();
    }
}
