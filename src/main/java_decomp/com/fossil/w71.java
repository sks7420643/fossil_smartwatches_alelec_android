package com.fossil;

import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W71 implements Iterable<Lc6<? extends String, ? extends Ci>>, Jr7 {
    @DexIgnore
    public static /* final */ W71 c; // = new Ai().a();
    @DexIgnore
    public static /* final */ Bi d; // = new Bi(null);
    @DexIgnore
    public /* final */ SortedMap<String, Ci> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ SortedMap<String, Ci> a; // = Ym7.d(new Lc6[0]);

        @DexIgnore
        public final W71 a() {
            return new W71(Ym7.f(this.a), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ci) {
                    Ci ci = (Ci) obj;
                    if (!Wg6.a(this.a, ci.a) || !Wg6.a(this.b, ci.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Object obj = this.a;
            int hashCode = obj != null ? obj.hashCode() : 0;
            String str = this.b;
            if (str != null) {
                i = str.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "Entry(value=" + this.a + ", cacheKey=" + this.b + ")";
        }
    }

    @DexIgnore
    public W71(SortedMap<String, Ci> sortedMap) {
        this.b = sortedMap;
    }

    @DexIgnore
    public /* synthetic */ W71(SortedMap sortedMap, Qg6 qg6) {
        this(sortedMap);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return Wg6.a(this.b, obj);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final boolean isEmpty() {
        return this.b.isEmpty();
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator<com.mapped.Lc6<java.lang.String, com.fossil.W71$Ci>>' to match base method */
    @Override // java.lang.Iterable
    public Iterator<Lc6<? extends String, ? extends Ci>> iterator() {
        SortedMap<String, Ci> sortedMap = this.b;
        ArrayList arrayList = new ArrayList(sortedMap.size());
        for (Map.Entry<String, Ci> entry : sortedMap.entrySet()) {
            arrayList.add(Hl7.a(entry.getKey(), entry.getValue()));
        }
        return arrayList.iterator();
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
