package com.fossil;

import android.util.SparseArray;
import java.util.EnumMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z32 {
    @DexIgnore
    public static SparseArray<Vy1> a; // = new SparseArray<>();
    @DexIgnore
    public static EnumMap<Vy1, Integer> b;

    /*
    static {
        EnumMap<Vy1, Integer> enumMap = new EnumMap<>(Vy1.class);
        b = enumMap;
        enumMap.put((EnumMap<Vy1, Integer>) Vy1.DEFAULT, (Vy1) 0);
        b.put((EnumMap<Vy1, Integer>) Vy1.VERY_LOW, (Vy1) 1);
        b.put((EnumMap<Vy1, Integer>) Vy1.HIGHEST, (Vy1) 2);
        for (Vy1 vy1 : b.keySet()) {
            a.append(b.get(vy1).intValue(), vy1);
        }
    }
    */

    @DexIgnore
    public static int a(Vy1 vy1) {
        Integer num = b.get(vy1);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + vy1);
    }

    @DexIgnore
    public static Vy1 b(int i) {
        Vy1 vy1 = a.get(i);
        if (vy1 != null) {
            return vy1;
        }
        throw new IllegalArgumentException("Unknown Priority for value " + i);
    }
}
