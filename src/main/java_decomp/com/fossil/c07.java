package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C07 implements Factory<B07> {
    @DexIgnore
    public static B07 a(Zz6 zz6) {
        return new B07(zz6);
    }
}
