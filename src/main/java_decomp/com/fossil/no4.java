package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class no4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f2553a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            pq7.c(runnable, Constants.COMMAND);
            this.b.post(runnable);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public no4() {
        /*
            r3 = this;
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newSingleThreadExecutor()
            java.lang.String r1 = "Executors.newSingleThreadExecutor()"
            com.fossil.pq7.b(r0, r1)
            r1 = 3
            java.util.concurrent.ExecutorService r1 = java.util.concurrent.Executors.newFixedThreadPool(r1)
            java.lang.String r2 = "Executors.newFixedThreadPool(3)"
            com.fossil.pq7.b(r1, r2)
            com.fossil.no4$a r2 = new com.fossil.no4$a
            r2.<init>()
            r3.<init>(r0, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.no4.<init>():void");
    }

    @DexIgnore
    public no4(Executor executor, Executor executor2, Executor executor3) {
        this.f2553a = executor;
        this.b = executor2;
    }

    @DexIgnore
    public Executor a() {
        return this.f2553a;
    }

    @DexIgnore
    public Executor b() {
        return this.b;
    }
}
