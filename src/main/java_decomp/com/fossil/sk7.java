package com.fossil;

import io.flutter.util.Predicate;
import io.flutter.view.AccessibilityBridge;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Sk7 implements Predicate {
    @DexIgnore
    public static /* final */ /* synthetic */ Sk7 a; // = new Sk7();

    @DexIgnore
    private /* synthetic */ Sk7() {
    }

    @DexIgnore
    @Override // io.flutter.util.Predicate
    public final boolean test(Object obj) {
        return ((AccessibilityBridge.SemanticsNode) obj).hasFlag(AccessibilityBridge.Flag.HAS_IMPLICIT_SCROLLING);
    }
}
