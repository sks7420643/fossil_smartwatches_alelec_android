package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ja4 extends Ta4.Di.Dii.Aiii.Biiii {
    @DexIgnore
    public /* final */ Ua4<Ta4$d$d$a$b$e> a;
    @DexIgnore
    public /* final */ Ta4$d$d$a$b$c b;
    @DexIgnore
    public /* final */ Ta4$d$d$a$b$d c;
    @DexIgnore
    public /* final */ Ua4<Ta4$d$d$a$b$a> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4$d$d$a$b$b {
        @DexIgnore
        public Ua4<Ta4$d$d$a$b$e> a;
        @DexIgnore
        public Ta4$d$d$a$b$c b;
        @DexIgnore
        public Ta4$d$d$a$b$d c;
        @DexIgnore
        public Ua4<Ta4$d$d$a$b$a> d;

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$b
        public Ta4.Di.Dii.Aiii.Biiii a() {
            String str = "";
            if (this.a == null) {
                str = " threads";
            }
            if (this.b == null) {
                str = str + " exception";
            }
            if (this.c == null) {
                str = str + " signal";
            }
            if (this.d == null) {
                str = str + " binaries";
            }
            if (str.isEmpty()) {
                return new Ja4(this.a, this.b, this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$b
        public Ta4$d$d$a$b$b b(Ua4<Ta4$d$d$a$b$a> ua4) {
            if (ua4 != null) {
                this.d = ua4;
                return this;
            }
            throw new NullPointerException("Null binaries");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$b
        public Ta4$d$d$a$b$b c(Ta4$d$d$a$b$c ta4$d$d$a$b$c) {
            if (ta4$d$d$a$b$c != null) {
                this.b = ta4$d$d$a$b$c;
                return this;
            }
            throw new NullPointerException("Null exception");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$b
        public Ta4$d$d$a$b$b d(Ta4$d$d$a$b$d ta4$d$d$a$b$d) {
            if (ta4$d$d$a$b$d != null) {
                this.c = ta4$d$d$a$b$d;
                return this;
            }
            throw new NullPointerException("Null signal");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$b
        public Ta4$d$d$a$b$b e(Ua4<Ta4$d$d$a$b$e> ua4) {
            if (ua4 != null) {
                this.a = ua4;
                return this;
            }
            throw new NullPointerException("Null threads");
        }
    }

    @DexIgnore
    public Ja4(Ua4<Ta4$d$d$a$b$e> ua4, Ta4$d$d$a$b$c ta4$d$d$a$b$c, Ta4$d$d$a$b$d ta4$d$d$a$b$d, Ua4<Ta4$d$d$a$b$a> ua42) {
        this.a = ua4;
        this.b = ta4$d$d$a$b$c;
        this.c = ta4$d$d$a$b$d;
        this.d = ua42;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii.Biiii
    public Ua4<Ta4$d$d$a$b$a> b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii.Biiii
    public Ta4$d$d$a$b$c c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii.Biiii
    public Ta4$d$d$a$b$d d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii.Biiii
    public Ua4<Ta4$d$d$a$b$e> e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di.Dii.Aiii.Biiii)) {
            return false;
        }
        Ta4.Di.Dii.Aiii.Biiii biiii = (Ta4.Di.Dii.Aiii.Biiii) obj;
        return this.a.equals(biiii.e()) && this.b.equals(biiii.c()) && this.c.equals(biiii.d()) && this.d.equals(biiii.b());
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Execution{threads=" + this.a + ", exception=" + this.b + ", signal=" + this.c + ", binaries=" + this.d + "}";
    }
}
