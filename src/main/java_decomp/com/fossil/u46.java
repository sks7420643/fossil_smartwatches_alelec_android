package com.fossil;

import androidx.loader.app.LoaderManager;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U46 {
    @DexIgnore
    public /* final */ S46 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public U46(S46 s46, LoaderManager loaderManager) {
        Wg6.c(s46, "mView");
        Wg6.c(loaderManager, "mLoaderManager");
        this.a = s46;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final S46 b() {
        return this.a;
    }
}
