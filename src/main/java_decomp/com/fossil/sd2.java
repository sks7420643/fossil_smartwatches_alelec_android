package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sd2 extends Cc2 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ O72 c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;

    @DexIgnore
    public Sd2(Intent intent, O72 o72, int i) {
        this.b = intent;
        this.c = o72;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.Cc2
    public final void c() {
        Intent intent = this.b;
        if (intent != null) {
            this.c.startActivityForResult(intent, this.d);
        }
    }
}
