package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gm1 extends Dm1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Gm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Gm1 a(Parcel parcel) {
            return new Gm1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gm1 createFromParcel(Parcel parcel) {
            return new Gm1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gm1[] newArray(int i) {
            return new Gm1[i];
        }
    }

    @DexIgnore
    public Gm1() {
        super(Fm1.DATE, null, null, null, 14);
    }

    @DexIgnore
    public /* synthetic */ Gm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Gm1(Dt1 dt1, Et1 et1) {
        super(Fm1.DATE, null, dt1, et1, 2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Gm1(Dt1 dt1, Et1 et1, int i, Qg6 qg6) {
        this(dt1, (i & 2) != 0 ? new Et1(Et1.CREATOR.a()) : et1);
    }
}
