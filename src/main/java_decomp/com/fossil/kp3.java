package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Or3 b;
    @DexIgnore
    public /* final */ /* synthetic */ U93 c;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 d;

    @DexIgnore
    public Kp3(Fp3 fp3, Or3 or3, U93 u93) {
        this.d = fp3;
        this.b = or3;
        this.c = u93;
    }

    @DexIgnore
    public final void run() {
        try {
            Cl3 cl3 = this.d.d;
            if (cl3 == null) {
                this.d.d().F().a("Failed to get app instance id");
                return;
            }
            String n0 = cl3.n0(this.b);
            if (n0 != null) {
                this.d.p().M(n0);
                this.d.l().l.b(n0);
            }
            this.d.e0();
            this.d.k().Q(this.c, n0);
        } catch (RemoteException e) {
            this.d.d().F().b("Failed to get app instance id", e);
        } finally {
            this.d.k().Q(this.c, null);
        }
    }
}
