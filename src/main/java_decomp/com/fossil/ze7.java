package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.Ye7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ze7 implements Ye7.Ai {
    @DexIgnore
    public Ze7() {
        new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    @Override // com.fossil.Ye7.Ai
    public final void a(String str, String str2) {
        if (Ye7.a <= 1) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ye7.Ai
    public final void b(String str, String str2) {
        if (Ye7.a <= 2) {
            Log.i(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ye7.Ai
    public final int c() {
        return Ye7.a;
    }

    @DexIgnore
    @Override // com.fossil.Ye7.Ai
    public final void d(String str, String str2) {
        if (Ye7.a <= 3) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ye7.Ai
    public final void i(String str, String str2) {
        if (Ye7.a <= 4) {
            Log.e(str, str2);
        }
    }
}
