package com.fossil;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.M62;
import com.fossil.P72;
import com.fossil.Pc2;
import com.fossil.R62;
import com.fossil.Yb2;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L72 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Status n; // = new Status(4, "Sign-out occurred while this API call was in progress.");
    @DexIgnore
    public static /* final */ Status o; // = new Status(4, "The user must be signed in to make this API call.");
    @DexIgnore
    public static /* final */ Object p; // = new Object();
    @DexIgnore
    public static L72 q;
    @DexIgnore
    public long a; // = 5000;
    @DexIgnore
    public long b; // = 120000;
    @DexIgnore
    public long c; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ C62 e;
    @DexIgnore
    public /* final */ Ic2 f;
    @DexIgnore
    public /* final */ AtomicInteger g; // = new AtomicInteger(1);
    @DexIgnore
    public /* final */ AtomicInteger h; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ Map<G72<?>, Ai<?>> i; // = new ConcurrentHashMap(5, 0.75f, 1);
    @DexIgnore
    public B82 j; // = null;
    @DexIgnore
    public /* final */ Set<G72<?>> k; // = new Aj0();
    @DexIgnore
    public /* final */ Set<G72<?>> l; // = new Aj0();
    @DexIgnore
    public /* final */ Handler m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai<O extends M62.Di> implements R62.Bi, R62.Ci, Va2 {
        @DexIgnore
        public /* final */ Queue<Z82> b; // = new LinkedList();
        @DexIgnore
        public /* final */ M62.Fi c;
        @DexIgnore
        public /* final */ M62.Bi d;
        @DexIgnore
        public /* final */ G72<O> e;
        @DexIgnore
        public /* final */ A82 f;
        @DexIgnore
        public /* final */ Set<Pa2> g; // = new HashSet();
        @DexIgnore
        public /* final */ Map<P72.Ai<?>, U92> h; // = new HashMap();
        @DexIgnore
        public /* final */ int i;
        @DexIgnore
        public /* final */ X92 j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public /* final */ List<Bi> l; // = new ArrayList();
        @DexIgnore
        public Z52 m; // = null;

        @DexIgnore
        public Ai(Q62<O> q62) {
            M62.Fi n = q62.n(L72.this.m.getLooper(), this);
            this.c = n;
            if (n instanceof Wc2) {
                this.d = ((Wc2) n).t0();
            } else {
                this.d = n;
            }
            this.e = q62.a();
            this.f = new A82();
            this.i = q62.l();
            if (this.c.v()) {
                this.j = q62.p(L72.this.d, L72.this.m);
            } else {
                this.j = null;
            }
        }

        @DexIgnore
        public final Map<P72.Ai<?>, U92> A() {
            return this.h;
        }

        @DexIgnore
        public final void B() {
            Rc2.d(L72.this.m);
            this.m = null;
        }

        @DexIgnore
        public final Z52 C() {
            Rc2.d(L72.this.m);
            return this.m;
        }

        @DexIgnore
        public final void D() {
            if (this.k) {
                L72.this.m.removeMessages(11, this.e);
                L72.this.m.removeMessages(9, this.e);
                this.k = false;
            }
        }

        @DexIgnore
        public final void E() {
            L72.this.m.removeMessages(12, this.e);
            L72.this.m.sendMessageDelayed(L72.this.m.obtainMessage(12, this.e), L72.this.c);
        }

        @DexIgnore
        public final boolean F() {
            return J(true);
        }

        @DexIgnore
        public final Ys3 G() {
            X92 x92 = this.j;
            if (x92 == null) {
                return null;
            }
            return x92.b3();
        }

        @DexIgnore
        public final void H(Status status) {
            Rc2.d(L72.this.m);
            k(status, null, false);
        }

        @DexIgnore
        public final void I(Z82 z82) {
            z82.d(this.f, f());
            try {
                z82.c(this);
            } catch (DeadObjectException e2) {
                d(1);
                this.c.a();
            } catch (Throwable th) {
                throw new IllegalStateException(String.format("Error in GoogleApi implementation for client %s.", this.d.getClass().getName()), th);
            }
        }

        @DexIgnore
        public final boolean J(boolean z) {
            Rc2.d(L72.this.m);
            if (!this.c.c() || this.h.size() != 0) {
                return false;
            }
            if (!this.f.e()) {
                this.c.a();
                return true;
            } else if (!z) {
                return false;
            } else {
                E();
                return false;
            }
        }

        @DexIgnore
        public final void N(Z52 z52) {
            Rc2.d(L72.this.m);
            this.c.a();
            n(z52);
        }

        @DexIgnore
        public final boolean O(Z52 z52) {
            synchronized (L72.p) {
                if (L72.this.j == null || !L72.this.k.contains(this.e)) {
                    return false;
                }
                L72.this.j.n(z52, this.i);
                return true;
            }
        }

        @DexIgnore
        public final void P(Z52 z52) {
            for (Pa2 pa2 : this.g) {
                String str = null;
                if (Pc2.a(z52, Z52.f)) {
                    str = this.c.k();
                }
                pa2.b(this.e, z52, str);
            }
            this.g.clear();
        }

        @DexIgnore
        public final Status Q(Z52 z52) {
            String a2 = this.e.a();
            String valueOf = String.valueOf(z52);
            StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 63 + String.valueOf(valueOf).length());
            sb.append("API: ");
            sb.append(a2);
            sb.append(" is not available on this device. Connection failed with: ");
            sb.append(valueOf);
            return new Status(17, sb.toString());
        }

        @DexIgnore
        public final M62.Fi R() {
            return this.c;
        }

        @DexIgnore
        public final void a() {
            Rc2.d(L72.this.m);
            if (!this.c.c() && !this.c.j()) {
                try {
                    int b2 = L72.this.f.b(L72.this.d, this.c);
                    if (b2 != 0) {
                        Z52 z52 = new Z52(b2, null);
                        String name = this.d.getClass().getName();
                        String valueOf = String.valueOf(z52);
                        StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 35 + String.valueOf(valueOf).length());
                        sb.append("The service for ");
                        sb.append(name);
                        sb.append(" is not available: ");
                        sb.append(valueOf);
                        Log.w("GoogleApiManager", sb.toString());
                        n(z52);
                        return;
                    }
                    Ci ci = new Ci(this.c, this.e);
                    if (this.c.v()) {
                        this.j.a3(ci);
                    }
                    try {
                        this.c.l(ci);
                    } catch (SecurityException e2) {
                        j(new Z52(10), e2);
                    }
                } catch (IllegalStateException e3) {
                    j(new Z52(10), e3);
                }
            }
        }

        @DexIgnore
        public final int b() {
            return this.i;
        }

        @DexIgnore
        public final boolean c() {
            return this.c.c();
        }

        @DexIgnore
        @Override // com.fossil.K72
        public final void d(int i2) {
            if (Looper.myLooper() == L72.this.m.getLooper()) {
                x();
            } else {
                L72.this.m.post(new I92(this));
            }
        }

        @DexIgnore
        @Override // com.fossil.K72
        public final void e(Bundle bundle) {
            if (Looper.myLooper() == L72.this.m.getLooper()) {
                w();
            } else {
                L72.this.m.post(new J92(this));
            }
        }

        @DexIgnore
        public final boolean f() {
            return this.c.v();
        }

        @DexIgnore
        public final void g() {
            Rc2.d(L72.this.m);
            if (this.k) {
                a();
            }
        }

        @DexIgnore
        public final B62 h(B62[] b62Arr) {
            if (!(b62Arr == null || b62Arr.length == 0)) {
                B62[] t = this.c.t();
                if (t == null) {
                    t = new B62[0];
                }
                Zi0 zi0 = new Zi0(t.length);
                for (B62 b62 : t) {
                    zi0.put(b62.c(), Long.valueOf(b62.f()));
                }
                for (B62 b622 : b62Arr) {
                    if (!zi0.containsKey(b622.c()) || ((Long) zi0.get(b622.c())).longValue() < b622.f()) {
                        return b622;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.Va2
        public final void i(Z52 z52, M62<?> m62, boolean z) {
            if (Looper.myLooper() == L72.this.m.getLooper()) {
                n(z52);
            } else {
                L72.this.m.post(new L92(this, z52));
            }
        }

        @DexIgnore
        public final void j(Z52 z52, Exception exc) {
            Rc2.d(L72.this.m);
            X92 x92 = this.j;
            if (x92 != null) {
                x92.c3();
            }
            B();
            L72.this.f.a();
            P(z52);
            if (z52.c() == 4) {
                H(L72.o);
            } else if (this.b.isEmpty()) {
                this.m = z52;
            } else if (exc != null) {
                Rc2.d(L72.this.m);
                k(null, exc, false);
            } else {
                k(Q(z52), null, true);
                if (!this.b.isEmpty() && !O(z52) && !L72.this.v(z52, this.i)) {
                    if (z52.c() == 18) {
                        this.k = true;
                    }
                    if (this.k) {
                        L72.this.m.sendMessageDelayed(Message.obtain(L72.this.m, 9, this.e), L72.this.a);
                    } else {
                        H(Q(z52));
                    }
                }
            }
        }

        @DexIgnore
        public final void k(Status status, Exception exc, boolean z) {
            boolean z2 = true;
            Rc2.d(L72.this.m);
            boolean z3 = status == null;
            if (exc != null) {
                z2 = false;
            }
            if (z3 != z2) {
                Iterator<Z82> it = this.b.iterator();
                while (it.hasNext()) {
                    Z82 next = it.next();
                    if (!z || next.a == 2) {
                        if (status != null) {
                            next.b(status);
                        } else {
                            next.e(exc);
                        }
                        it.remove();
                    }
                }
                return;
            }
            throw new IllegalArgumentException("Status XOR exception should be null");
        }

        @DexIgnore
        @Override // com.fossil.R72
        public final void n(Z52 z52) {
            j(z52, null);
        }

        @DexIgnore
        public final void o(Bi bi) {
            if (!this.l.contains(bi) || this.k) {
                return;
            }
            if (!this.c.c()) {
                a();
            } else {
                y();
            }
        }

        @DexIgnore
        public final void p(Z82 z82) {
            Rc2.d(L72.this.m);
            if (!this.c.c()) {
                this.b.add(z82);
                Z52 z52 = this.m;
                if (z52 == null || !z52.k()) {
                    a();
                } else {
                    n(this.m);
                }
            } else if (v(z82)) {
                E();
            } else {
                this.b.add(z82);
            }
        }

        @DexIgnore
        public final void q(Pa2 pa2) {
            Rc2.d(L72.this.m);
            this.g.add(pa2);
        }

        @DexIgnore
        public final void s() {
            Rc2.d(L72.this.m);
            if (this.k) {
                D();
                H(L72.this.e.i(L72.this.d) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
                this.c.a();
            }
        }

        @DexIgnore
        public final void u(Bi bi) {
            B62[] g2;
            if (this.l.remove(bi)) {
                L72.this.m.removeMessages(15, bi);
                L72.this.m.removeMessages(16, bi);
                B62 b62 = bi.b;
                ArrayList arrayList = new ArrayList(this.b.size());
                for (Z82 z82 : this.b) {
                    if ((z82 instanceof Ja2) && (g2 = ((Ja2) z82).g(this)) != null && Bf2.b(g2, b62)) {
                        arrayList.add(z82);
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    Z82 z822 = (Z82) obj;
                    this.b.remove(z822);
                    z822.e(new E72(b62));
                }
            }
        }

        @DexIgnore
        public final boolean v(Z82 z82) {
            if (!(z82 instanceof Ja2)) {
                I(z82);
                return true;
            }
            Ja2 ja2 = (Ja2) z82;
            B62 h2 = h(ja2.g(this));
            if (h2 == null) {
                I(z82);
                return true;
            }
            String name = this.d.getClass().getName();
            String c2 = h2.c();
            long f2 = h2.f();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 77 + String.valueOf(c2).length());
            sb.append(name);
            sb.append(" could not execute call because it requires feature (");
            sb.append(c2);
            sb.append(", ");
            sb.append(f2);
            sb.append(").");
            Log.w("GoogleApiManager", sb.toString());
            if (ja2.h(this)) {
                Bi bi = new Bi(this.e, h2, null);
                int indexOf = this.l.indexOf(bi);
                if (indexOf >= 0) {
                    Bi bi2 = this.l.get(indexOf);
                    L72.this.m.removeMessages(15, bi2);
                    L72.this.m.sendMessageDelayed(Message.obtain(L72.this.m, 15, bi2), L72.this.a);
                } else {
                    this.l.add(bi);
                    L72.this.m.sendMessageDelayed(Message.obtain(L72.this.m, 15, bi), L72.this.a);
                    L72.this.m.sendMessageDelayed(Message.obtain(L72.this.m, 16, bi), L72.this.b);
                    Z52 z52 = new Z52(2, null);
                    if (!O(z52)) {
                        L72.this.v(z52, this.i);
                    }
                }
                return false;
            }
            ja2.e(new E72(h2));
            return true;
        }

        @DexIgnore
        public final void w() {
            B();
            P(Z52.f);
            D();
            Iterator<U92> it = this.h.values().iterator();
            while (it.hasNext()) {
                U92 next = it.next();
                if (h(next.a.c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.a.d(this.d, new Ot3<>());
                    } catch (DeadObjectException e2) {
                        d(1);
                        this.c.a();
                    } catch (RemoteException e3) {
                        it.remove();
                    }
                }
            }
            y();
            E();
        }

        @DexIgnore
        public final void x() {
            B();
            this.k = true;
            this.f.g();
            L72.this.m.sendMessageDelayed(Message.obtain(L72.this.m, 9, this.e), L72.this.a);
            L72.this.m.sendMessageDelayed(Message.obtain(L72.this.m, 11, this.e), L72.this.b);
            L72.this.f.a();
            for (U92 u92 : this.h.values()) {
                u92.c.run();
            }
        }

        @DexIgnore
        public final void y() {
            ArrayList arrayList = new ArrayList(this.b);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                Z82 z82 = (Z82) obj;
                if (!this.c.c()) {
                    return;
                }
                if (v(z82)) {
                    this.b.remove(z82);
                }
            }
        }

        @DexIgnore
        public final void z() {
            Rc2.d(L72.this.m);
            H(L72.n);
            this.f.f();
            for (P72.Ai ai : (P72.Ai[]) this.h.keySet().toArray(new P72.Ai[this.h.size()])) {
                p(new Ma2(ai, new Ot3()));
            }
            P(new Z52(4));
            if (this.c.c()) {
                this.c.n(new K92(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ G72<?> a;
        @DexIgnore
        public /* final */ B62 b;

        @DexIgnore
        public Bi(G72<?> g72, B62 b62) {
            this.a = g72;
            this.b = b62;
        }

        @DexIgnore
        public /* synthetic */ Bi(G72 g72, B62 b62, H92 h92) {
            this(g72, b62);
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof Bi)) {
                Bi bi = (Bi) obj;
                return Pc2.a(this.a, bi.a) && Pc2.a(this.b, bi.b);
            }
        }

        @DexIgnore
        public final int hashCode() {
            return Pc2.b(this.a, this.b);
        }

        @DexIgnore
        public final String toString() {
            Pc2.Ai c = Pc2.c(this);
            c.a("key", this.a);
            c.a("feature", this.b);
            return c.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements Aa2, Yb2.Ci {
        @DexIgnore
        public /* final */ M62.Fi a;
        @DexIgnore
        public /* final */ G72<?> b;
        @DexIgnore
        public Jc2 c; // = null;
        @DexIgnore
        public Set<Scope> d; // = null;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public Ci(M62.Fi fi, G72<?> g72) {
            this.a = fi;
            this.b = g72;
        }

        @DexIgnore
        @Override // com.fossil.Yb2.Ci
        public final void a(Z52 z52) {
            L72.this.m.post(new N92(this, z52));
        }

        @DexIgnore
        @Override // com.fossil.Aa2
        public final void b(Jc2 jc2, Set<Scope> set) {
            if (jc2 == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                c(new Z52(4));
                return;
            }
            this.c = jc2;
            this.d = set;
            g();
        }

        @DexIgnore
        @Override // com.fossil.Aa2
        public final void c(Z52 z52) {
            ((Ai) L72.this.i.get(this.b)).N(z52);
        }

        @DexIgnore
        public final void g() {
            Jc2 jc2;
            if (this.e && (jc2 = this.c) != null) {
                this.a.i(jc2, this.d);
            }
        }
    }

    @DexIgnore
    public L72(Context context, Looper looper, C62 c62) {
        this.d = context;
        this.m = new Ol2(looper, this);
        this.e = c62;
        this.f = new Ic2(c62);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    @DexIgnore
    public static void b() {
        synchronized (p) {
            if (q != null) {
                L72 l72 = q;
                l72.h.incrementAndGet();
                l72.m.sendMessageAtFrontOfQueue(l72.m.obtainMessage(10));
            }
        }
    }

    @DexIgnore
    public static L72 m() {
        L72 l72;
        synchronized (p) {
            Rc2.l(q, "Must guarantee manager is non-null before using getInstance");
            l72 = q;
        }
        return l72;
    }

    @DexIgnore
    public static L72 o(Context context) {
        L72 l72;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new L72(context.getApplicationContext(), handlerThread.getLooper(), C62.q());
            }
            l72 = q;
        }
        return l72;
    }

    @DexIgnore
    public final void D() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    @DexIgnore
    public final void a() {
        this.h.incrementAndGet();
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(10));
    }

    @DexIgnore
    public final PendingIntent c(G72<?> g72, int i2) {
        Ai<?> ai = this.i.get(g72);
        if (ai == null) {
            return null;
        }
        Ys3 G = ai.G();
        if (G == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, G.u(), 134217728);
    }

    @DexIgnore
    public final <O extends M62.Di> Nt3<Boolean> e(Q62<O> q62, P72.Ai<?> ai) {
        Ot3 ot3 = new Ot3();
        Ma2 ma2 = new Ma2(ai, ot3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new T92(ma2, this.h.get(), q62)));
        return ot3.a();
    }

    @DexIgnore
    public final <O extends M62.Di> Nt3<Void> f(Q62<O> q62, S72<M62.Bi, ?> s72, Y72<M62.Bi, ?> y72, Runnable runnable) {
        Ot3 ot3 = new Ot3();
        Ka2 ka2 = new Ka2(new U92(s72, y72, runnable), ot3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new T92(ka2, this.h.get(), q62)));
        return ot3.a();
    }

    @DexIgnore
    public final Nt3<Map<G72<?>, String>> g(Iterable<? extends S62<?>> iterable) {
        Pa2 pa2 = new Pa2(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, pa2));
        return pa2.a();
    }

    @DexIgnore
    public final void h(Z52 z52, int i2) {
        if (!v(z52, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, z52));
        }
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        Ai<?> ai;
        int i2 = message.what;
        switch (i2) {
            case 1:
                this.c = ((Boolean) message.obj).booleanValue() ? ButtonService.CONNECT_TIMEOUT : 300000;
                this.m.removeMessages(12);
                for (G72<?> g72 : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, g72), this.c);
                }
                break;
            case 2:
                Pa2 pa2 = (Pa2) message.obj;
                Iterator<G72<?>> it = pa2.c().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        G72<?> next = it.next();
                        Ai<?> ai2 = this.i.get(next);
                        if (ai2 == null) {
                            pa2.b(next, new Z52(13), null);
                            break;
                        } else if (ai2.c()) {
                            pa2.b(next, Z52.f, ai2.R().k());
                        } else if (ai2.C() != null) {
                            pa2.b(next, ai2.C(), null);
                        } else {
                            ai2.q(pa2);
                            ai2.a();
                        }
                    }
                }
            case 3:
                for (Ai<?> ai3 : this.i.values()) {
                    ai3.B();
                    ai3.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                T92 t92 = (T92) message.obj;
                Ai<?> ai4 = this.i.get(t92.c.a());
                if (ai4 == null) {
                    p(t92.c);
                    ai4 = this.i.get(t92.c.a());
                }
                if (!ai4.f() || this.h.get() == t92.b) {
                    ai4.p(t92.a);
                    break;
                } else {
                    t92.a.b(n);
                    ai4.z();
                    break;
                }
            case 5:
                int i3 = message.arg1;
                Z52 z52 = (Z52) message.obj;
                Iterator<Ai<?>> it2 = this.i.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        ai = it2.next();
                        if (ai.b() == i3) {
                        }
                    } else {
                        ai = null;
                    }
                }
                if (ai != null) {
                    String g2 = this.e.g(z52.c());
                    String f2 = z52.f();
                    StringBuilder sb = new StringBuilder(String.valueOf(g2).length() + 69 + String.valueOf(f2).length());
                    sb.append("Error resolution was canceled by the user, original error message: ");
                    sb.append(g2);
                    sb.append(": ");
                    sb.append(f2);
                    ai.H(new Status(17, sb.toString()));
                    break;
                } else {
                    StringBuilder sb2 = new StringBuilder(76);
                    sb2.append("Could not find API instance ");
                    sb2.append(i3);
                    sb2.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb2.toString(), new Exception());
                    break;
                }
            case 6:
                if (this.d.getApplicationContext() instanceof Application) {
                    H72.c((Application) this.d.getApplicationContext());
                    H72.b().a(new H92(this));
                    if (!H72.b().f(true)) {
                        this.c = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
                        break;
                    }
                }
                break;
            case 7:
                p((Q62) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).g();
                    break;
                }
                break;
            case 10:
                for (G72<?> g722 : this.l) {
                    this.i.remove(g722).z();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).s();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).F();
                    break;
                }
                break;
            case 14:
                E82 e82 = (E82) message.obj;
                G72<?> a2 = e82.a();
                if (!this.i.containsKey(a2)) {
                    e82.b().c(Boolean.FALSE);
                    break;
                } else {
                    e82.b().c(Boolean.valueOf(this.i.get(a2).J(false)));
                    break;
                }
            case 15:
                Bi bi = (Bi) message.obj;
                if (this.i.containsKey(bi.a)) {
                    this.i.get(bi.a).o(bi);
                    break;
                }
                break;
            case 16:
                Bi bi2 = (Bi) message.obj;
                if (this.i.containsKey(bi2.a)) {
                    this.i.get(bi2.a).u(bi2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @DexIgnore
    public final void i(Q62<?> q62) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, q62));
    }

    @DexIgnore
    public final <O extends M62.Di> void j(Q62<O> q62, int i2, I72<? extends Z62, M62.Bi> i72) {
        La2 la2 = new La2(i2, i72);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new T92(la2, this.h.get(), q62)));
    }

    @DexIgnore
    public final <O extends M62.Di, ResultT> void k(Q62<O> q62, int i2, W72<M62.Bi, ResultT> w72, Ot3<ResultT> ot3, U72 u72) {
        Na2 na2 = new Na2(i2, w72, ot3, u72);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new T92(na2, this.h.get(), q62)));
    }

    @DexIgnore
    public final void l(B82 b82) {
        synchronized (p) {
            if (this.j != b82) {
                this.j = b82;
                this.k.clear();
            }
            this.k.addAll(b82.r());
        }
    }

    @DexIgnore
    public final void p(Q62<?> q62) {
        G72<?> a2 = q62.a();
        Ai<?> ai = this.i.get(a2);
        if (ai == null) {
            ai = new Ai<>(q62);
            this.i.put(a2, ai);
        }
        if (ai.f()) {
            this.l.add(a2);
        }
        ai.a();
    }

    @DexIgnore
    public final void q(B82 b82) {
        synchronized (p) {
            if (this.j == b82) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    @DexIgnore
    public final int r() {
        return this.g.getAndIncrement();
    }

    @DexIgnore
    public final boolean v(Z52 z52, int i2) {
        return this.e.B(this.d, z52, i2);
    }
}
