package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Hs extends Enum<Hs> {
    @DexIgnore
    public static /* final */ Hs A;
    @DexIgnore
    public static /* final */ Hs B;
    @DexIgnore
    public static /* final */ Hs C;
    @DexIgnore
    public static /* final */ Hs D;
    @DexIgnore
    public static /* final */ Hs E;
    @DexIgnore
    public static /* final */ Hs F;
    @DexIgnore
    public static /* final */ Hs G;
    @DexIgnore
    public static /* final */ Hs H;
    @DexIgnore
    public static /* final */ Hs I;
    @DexIgnore
    public static /* final */ Hs J;
    @DexIgnore
    public static /* final */ Hs K;
    @DexIgnore
    public static /* final */ Hs L;
    @DexIgnore
    public static /* final */ Hs M;
    @DexIgnore
    public static /* final */ Hs N;
    @DexIgnore
    public static /* final */ Hs O;
    @DexIgnore
    public static /* final */ Hs P;
    @DexIgnore
    public static /* final */ Hs Q;
    @DexIgnore
    public static /* final */ Hs R;
    @DexIgnore
    public static /* final */ Hs S;
    @DexIgnore
    public static /* final */ Hs T;
    @DexIgnore
    public static /* final */ Hs U;
    @DexIgnore
    public static /* final */ Hs V;
    @DexIgnore
    public static /* final */ Hs W;
    @DexIgnore
    public static /* final */ Hs X;
    @DexIgnore
    public static /* final */ Hs Y;
    @DexIgnore
    public static /* final */ Hs Z;
    @DexIgnore
    public static /* final */ Hs a0;
    @DexIgnore
    public static /* final */ Hs b;
    @DexIgnore
    public static /* final */ Hs b0;
    @DexIgnore
    public static /* final */ Hs c;
    @DexIgnore
    public static /* final */ Hs c0;
    @DexIgnore
    public static /* final */ Hs d;
    @DexIgnore
    public static /* final */ Hs d0;
    @DexIgnore
    public static /* final */ Hs e;
    @DexIgnore
    public static /* final */ Hs e0;
    @DexIgnore
    public static /* final */ Hs f;
    @DexIgnore
    public static /* final */ Hs f0;
    @DexIgnore
    public static /* final */ Hs g;
    @DexIgnore
    public static /* final */ Hs g0;
    @DexIgnore
    public static /* final */ Hs h;
    @DexIgnore
    public static /* final */ /* synthetic */ Hs[] h0;
    @DexIgnore
    public static /* final */ Hs i;
    @DexIgnore
    public static /* final */ Hs j;
    @DexIgnore
    public static /* final */ Hs k;
    @DexIgnore
    public static /* final */ Hs l;
    @DexIgnore
    public static /* final */ Hs m;
    @DexIgnore
    public static /* final */ Hs n;
    @DexIgnore
    public static /* final */ Hs o;
    @DexIgnore
    public static /* final */ Hs p;
    @DexIgnore
    public static /* final */ Hs q;
    @DexIgnore
    public static /* final */ Hs r;
    @DexIgnore
    public static /* final */ Hs s;
    @DexIgnore
    public static /* final */ Hs t;
    @DexIgnore
    public static /* final */ Hs u;
    @DexIgnore
    public static /* final */ Hs v;
    @DexIgnore
    public static /* final */ Hs w;
    @DexIgnore
    public static /* final */ Hs x;
    @DexIgnore
    public static /* final */ Hs y;
    @DexIgnore
    public static /* final */ Hs z;

    /*
    static {
        Hs hs = new Hs("UNKNOWN", 0);
        b = hs;
        Hs hs2 = new Hs("CONNECT", 1);
        c = hs2;
        Hs hs3 = new Hs("DISCONNECT", 2);
        d = hs3;
        Hs hs4 = new Hs("CLOSE", 3);
        Hs hs5 = new Hs("DISCOVER_SERVICES", 4);
        e = hs5;
        Hs hs6 = new Hs("SUBSCRIBE_CHARACTERISTIC", 5);
        f = hs6;
        Hs hs7 = new Hs("REQUEST_MTU", 6);
        g = hs7;
        Hs hs8 = new Hs("GET_OPTIMAL_PAYLOAD", 7);
        h = hs8;
        Hs hs9 = new Hs("READ_RSSI", 8);
        i = hs9;
        Hs hs10 = new Hs("READ_SERIAL_NUMBER", 9);
        Hs hs11 = new Hs("READ_FIRMWARE_VERSION", 10);
        j = hs11;
        Hs hs12 = new Hs("READ_MODEL_NUMBER", 11);
        k = hs12;
        Hs hs13 = new Hs("STREAMING", 12);
        l = hs13;
        Hs hs14 = new Hs("PUT_FILE", 13);
        m = hs14;
        Hs hs15 = new Hs("VERIFY_FILE", 14);
        n = hs15;
        Hs hs16 = new Hs("GET_FILE_SIZE_WRITTEN", 15);
        o = hs16;
        Hs hs17 = new Hs("VERIFY_DATA", 16);
        p = hs17;
        Hs hs18 = new Hs("ABORT_FILE", 17);
        q = hs18;
        Hs hs19 = new Hs("TRANSFER_DATA", 18);
        r = hs19;
        Hs hs20 = new Hs("GET_CONNECTION_PARAMS", 19);
        s = hs20;
        Hs hs21 = new Hs("SET_CONNECTION_PARAMS", 20);
        t = hs21;
        Hs hs22 = new Hs("PLAY_ANIMATION", 21);
        u = hs22;
        Hs hs23 = new Hs("LIST_FILE", 22);
        v = hs23;
        Hs hs24 = new Hs("GET_FILE", 23);
        w = hs24;
        Hs hs25 = new Hs("ERASE_FILE", 24);
        x = hs25;
        Hs hs26 = new Hs("REQUEST_HANDS", 25);
        y = hs26;
        Hs hs27 = new Hs("RELEASE_HANDS", 26);
        z = hs27;
        Hs hs28 = new Hs("MOVE_HANDS", 27);
        A = hs28;
        Hs hs29 = new Hs("SET_CALIBRATION_POSITION", 28);
        B = hs29;
        Hs hs30 = new Hs("NOTIFY_MUSIC_EVENT", 29);
        C = hs30;
        Hs hs31 = new Hs("GET_CURRENT_WORKOUT_SESSION", 30);
        D = hs31;
        Hs hs32 = new Hs("STOP_CURRENT_WORKOUT_SESSION", 31);
        E = hs32;
        Hs hs33 = new Hs("GET_HEARTBEAT_STATISTIC", 32);
        Hs hs34 = new Hs("GET_HEARTBEAT_INTERVAL", 33);
        Hs hs35 = new Hs("SET_HEARTBEAT_INTERVAL", 34);
        F = hs35;
        Hs hs36 = new Hs("SEND_ASYNC_EVENT_ACK", 35);
        G = hs36;
        Hs hs37 = new Hs("SEND_PHONE_RANDOM_NUMBER", 36);
        H = hs37;
        Hs hs38 = new Hs("SEND_BOTH_SIDES_RANDOM_NUMBERS", 37);
        I = hs38;
        Hs hs39 = new Hs("EXCHANGE_PUBLIC_KEYS", 38);
        J = hs39;
        Hs hs40 = new Hs("CONNECT_HID", 39);
        K = hs40;
        Hs hs41 = new Hs("DISCONNECT_HID", 40);
        L = hs41;
        Hs hs42 = new Hs("TROUBLESHOOT_DEVICE_BLE", 41);
        Hs hs43 = new Hs("LEGACY_OTA_ENTER", 42);
        M = hs43;
        Hs hs44 = new Hs("LEGACY_GET_FILE_SIZE_WRITTEN", 43);
        N = hs44;
        Hs hs45 = new Hs("LEGACY_PUT_FILE", 44);
        O = hs45;
        Hs hs46 = new Hs("LEGACY_TRANSFER_DATA", 45);
        P = hs46;
        Hs hs47 = new Hs("LEGACY_VERIFY_FILE", 46);
        Q = hs47;
        Hs hs48 = new Hs("LEGACY_VERIFY_SEGMENT", 47);
        R = hs48;
        Hs hs49 = new Hs("LEGACY_OTA_RESET", 48);
        S = hs49;
        Hs hs50 = new Hs("LEGACY_ERASE_SEGMENT", 49);
        T = hs50;
        Hs hs51 = new Hs("LEGACY_ABORT_FILE", 50);
        U = hs51;
        Hs hs52 = new Hs("LEGACY_LIST_FILE", 51);
        V = hs52;
        Hs hs53 = new Hs("LEGACY_GET_ACTIVITY_FILE", 52);
        W = hs53;
        Hs hs54 = new Hs("LEGACY_CLOSE_CURRENT_ACTIVITY_FILE", 53);
        X = hs54;
        Hs hs55 = new Hs("LEGACY_ERASE_ACTIVITY_FILE", 54);
        Y = hs55;
        Hs hs56 = new Hs("CREATE_BOND", 55);
        Z = hs56;
        Hs hs57 = new Hs("REMOVE_BOND", 56);
        a0 = hs57;
        Hs hs58 = new Hs("CUSTOM_COMMAND", 57);
        b0 = hs58;
        Hs hs59 = new Hs("NOTIFY_APP_NOTIFICATION_EVENT", 58);
        c0 = hs59;
        Hs hs60 = new Hs("READ_SOFTWARE_REVISION", 59);
        d0 = hs60;
        Hs hs61 = new Hs("CUSTOM_COMMAND_WITH_RESPONSE", 60);
        Hs hs62 = new Hs("REQUEST_DISCOVER_SERVICE", 61);
        Hs hs63 = new Hs("CLEAN_UP_DEVICE", 62);
        e0 = hs63;
        Hs hs64 = new Hs("CONFIRM_AUTHORIZATION", 63);
        f0 = hs64;
        Hs hs65 = new Hs("STOP_AUTHORIZATION_PROCESS", 64);
        g0 = hs65;
        h0 = new Hs[]{hs, hs2, hs3, hs4, hs5, hs6, hs7, hs8, hs9, hs10, hs11, hs12, hs13, hs14, hs15, hs16, hs17, hs18, hs19, hs20, hs21, hs22, hs23, hs24, hs25, hs26, hs27, hs28, hs29, hs30, hs31, hs32, hs33, hs34, hs35, hs36, hs37, hs38, hs39, hs40, hs41, hs42, hs43, hs44, hs45, hs46, hs47, hs48, hs49, hs50, hs51, hs52, hs53, hs54, hs55, hs56, hs57, hs58, hs59, hs60, hs61, hs62, hs63, hs64, hs65};
    }
    */

    @DexIgnore
    public Hs(String str, int i2) {
    }

    @DexIgnore
    public static Hs valueOf(String str) {
        return (Hs) Enum.valueOf(Hs.class, str);
    }

    @DexIgnore
    public static Hs[] values() {
        return (Hs[]) h0.clone();
    }
}
