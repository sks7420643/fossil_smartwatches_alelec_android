package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.D46;
import com.fossil.Kw5;
import com.fossil.wearables.fsl.contact.Contact;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I46 extends Qv5 implements H46, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G46 h;
    @DexIgnore
    public G37<Z85> i;
    @DexIgnore
    public Kw5 j;
    @DexIgnore
    public D46 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return I46.m;
        }

        @DexIgnore
        public final I46 b() {
            return new I46();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Kw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ I46 a;

        @DexIgnore
        public Bi(I46 i46) {
            this.a = i46;
        }

        @DexIgnore
        @Override // com.fossil.Kw5.Bi
        public void a(J06 j06) {
            Wg6.c(j06, "contactWrapper");
            D46 d46 = this.a.k;
            if (d46 != null) {
                Contact contact = j06.getContact();
                Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Contact contact2 = j06.getContact();
                    Boolean valueOf2 = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                    if (valueOf2 != null) {
                        boolean booleanValue = valueOf2.booleanValue();
                        Contact contact3 = j06.getContact();
                        Boolean valueOf3 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                        if (valueOf3 != null) {
                            d46.E6(intValue, booleanValue, valueOf3.booleanValue());
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            D46 d462 = this.a.k;
            if (d462 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                d462.show(childFragmentManager, D46.z.a());
            }
        }

        @DexIgnore
        @Override // com.fossil.Kw5.Bi
        public void b() {
            I46.L6(this.a).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements D46.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ I46 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(I46 i46) {
            this.a = i46;
        }

        @DexIgnore
        @Override // com.fossil.D46.Bi
        public void a(int i, boolean z, boolean z2) {
            I46.L6(this.a).x(i, z, z2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ I46 b;

        @DexIgnore
        public Di(I46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ I46 b;

        @DexIgnore
        public Ei(I46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ I46 b;

        @DexIgnore
        public Fi(I46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            I46.L6(this.b).s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ I46 b;

        @DexIgnore
        public Gi(I46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            I46.L6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ I46 b;

        @DexIgnore
        public Hi(I46 i46) {
            this.b = i46;
        }

        @DexIgnore
        public final void onClick(View view) {
            I46.L6(this.b).t();
        }
    }

    /*
    static {
        String simpleName = I46.class.getSimpleName();
        Wg6.b(simpleName, "NotificationContactsAndA\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G46 L6(I46 i46) {
        G46 g46 = i46.h;
        if (g46 != null) {
            return g46;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void B3(boolean z) {
        ImageView imageView;
        if (isActive()) {
            G37<Z85> g37 = this.i;
            if (g37 != null) {
                Z85 a2 = g37.a();
                if (a2 != null && (imageView = a2.y) != null) {
                    Wg6.b(imageView, "doneButton");
                    imageView.setEnabled(z);
                    imageView.setClickable(z);
                    if (z) {
                        imageView.setBackground(W6.f(requireContext(), 2131099967));
                    } else {
                        imageView.setBackground(W6.f(requireContext(), 2131099820));
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void F0(List<Object> list) {
        Wg6.c(list, "contactAndAppData");
        Kw5 kw5 = this.j;
        if (kw5 != null) {
            kw5.p(list);
            if (!isActive()) {
                return;
            }
            if (list.isEmpty()) {
                G37<Z85> g37 = this.i;
                if (g37 != null) {
                    Z85 a2 = g37.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.r;
                        Wg6.b(flexibleTextView, "it.ftvAssignSection");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.D;
                        Wg6.b(recyclerView, "it.rvAssign");
                        recyclerView.setVisibility(8);
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            G37<Z85> g372 = this.i;
            if (g372 != null) {
                Z85 a3 = g372.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.r;
                    Wg6.b(flexibleTextView2, "it.ftvAssignSection");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a3.D;
                    Wg6.b(recyclerView2, "it.rvAssign");
                    recyclerView2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        N6();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        P6((G46) obj);
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void N4(int i2, ArrayList<String> arrayList) {
        Wg6.c(arrayList, "stringAppsSelected");
        NotificationHybridAppActivity.B.a(this, i2, arrayList);
    }

    @DexIgnore
    public void N6() {
        G37<Z85> g37 = this.i;
        if (g37 == null) {
            Wg6.n("mBinding");
            throw null;
        } else if (g37.a() != null) {
            G46 g46 = this.h;
            if (g46 == null) {
                Wg6.n("mPresenter");
                throw null;
            } else if (!g46.q()) {
                close();
            } else {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.b(childFragmentManager);
            }
        }
    }

    @DexIgnore
    public final void O6() {
        Kw5 kw5 = this.j;
        if (kw5 == null) {
            Wg6.n("mAdapter");
            throw null;
        } else if (kw5.m()) {
            G46 g46 = this.h;
            if (g46 != null) {
                g46.v();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.a(childFragmentManager);
        }
    }

    @DexIgnore
    public void P6(G46 g46) {
        Wg6.c(g46, "presenter");
        this.h = g46;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            return;
        }
        if (i2 == 2131363291) {
            close();
        } else if (i2 == 2131363373) {
            O6();
        }
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void X1(int i2, ArrayList<J06> arrayList) {
        Wg6.c(arrayList, "contactWrappersSelected");
        NotificationHybridEveryoneActivity.B.a(this, i2, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void m() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void m6(int i2, ArrayList<J06> arrayList) {
        Wg6.c(arrayList, "contactWrappersSelected");
        NotificationHybridContactActivity.B.a(this, i2, arrayList);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 != 4567) {
            if (i2 != 5678) {
                if (i2 == 6789 && i3 == -1 && intent != null) {
                    Serializable serializableExtra = intent.getSerializableExtra("CONTACT_DATA");
                    if (serializableExtra != null) {
                        ArrayList<J06> arrayList = (ArrayList) serializableExtra;
                        G46 g46 = this.h;
                        if (g46 != null) {
                            g46.o(arrayList);
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                    }
                }
            } else if (i3 == -1 && intent != null) {
                Serializable serializableExtra2 = intent.getSerializableExtra("CONTACT_DATA");
                if (serializableExtra2 != null) {
                    ArrayList<J06> arrayList2 = (ArrayList) serializableExtra2;
                    G46 g462 = this.h;
                    if (g462 != null) {
                        g462.n(arrayList2);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                }
            }
        } else if (i3 == -1 && intent != null) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("APP_DATA");
            G46 g463 = this.h;
            if (g463 != null) {
                Wg6.b(stringArrayListExtra, "stringAppsSelected");
                g463.u(stringArrayListExtra);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Z85 z85 = (Z85) Aq0.f(layoutInflater, 2131558591, viewGroup, false, A6());
        D46 d46 = (D46) getChildFragmentManager().Z(D46.z.a());
        this.k = d46;
        if (d46 == null) {
            this.k = D46.z.b();
        }
        D46 d462 = this.k;
        if (d462 != null) {
            d462.D6(new Ci(this));
        }
        FlexibleTextView flexibleTextView = z85.r;
        Wg6.b(flexibleTextView, "binding.ftvAssignSection");
        flexibleTextView.setVisibility(8);
        RecyclerView recyclerView = z85.D;
        Wg6.b(recyclerView, "binding.rvAssign");
        recyclerView.setVisibility(8);
        ImageView imageView = z85.y;
        Wg6.b(imageView, "binding.ivDone");
        imageView.setBackground(W6.f(requireContext(), 2131230888));
        z85.x.setOnClickListener(new Di(this));
        z85.y.setOnClickListener(new Ei(this));
        z85.B.setOnClickListener(new Fi(this));
        z85.A.setOnClickListener(new Gi(this));
        z85.z.setOnClickListener(new Hi(this));
        Kw5 kw5 = new Kw5();
        kw5.q(new Bi(this));
        this.j = kw5;
        RecyclerView recyclerView2 = z85.D;
        recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
        recyclerView2.setHasFixedSize(true);
        Kw5 kw52 = this.j;
        if (kw52 != null) {
            recyclerView2.setAdapter(kw52);
            String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d)) {
                int parseColor = Color.parseColor(d);
                z85.E.setBackgroundColor(parseColor);
                z85.F.setBackgroundColor(parseColor);
                z85.G.setBackgroundColor(parseColor);
            }
            this.i = new G37<>(this, z85);
            Wg6.b(z85, "binding");
            return z85.n();
        }
        Wg6.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        G46 g46 = this.h;
        if (g46 != null) {
            g46.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        G46 g46 = this.h;
        if (g46 != null) {
            g46.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void u() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Wg6.b(activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.H46
    public void w5(int i2) {
        FlexibleTextView flexibleTextView;
        G37<Z85> g37 = this.i;
        if (g37 != null) {
            Z85 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.w) != null) {
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(getContext(), 2131886171);
                Wg6.b(c, "LanguageHelper.getString\u2026ct_Title__AssignToNumber)");
                String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }
}
