package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K96 extends U47 {
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public /* final */ Zp0 k; // = new Sr4(this);
    @DexIgnore
    public G37<K25> l;
    @DexIgnore
    public Bi m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final K96 a() {
            return new K96();
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Object p5();  // void declaration

        @DexIgnore
        Object u1();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog b;
        @DexIgnore
        public /* final */ /* synthetic */ K96 c;

        @DexIgnore
        public Ci(Dialog dialog, K25 k25, K96 k96) {
            this.b = dialog;
            this.c = k96;
        }

        @DexIgnore
        public final void onClick(View view) {
            K96 k96 = this.c;
            Dialog dialog = this.b;
            Wg6.b(dialog, "dialog");
            k96.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog b;
        @DexIgnore
        public /* final */ /* synthetic */ K96 c;

        @DexIgnore
        public Di(Dialog dialog, K25 k25, K96 k96) {
            this.b = dialog;
            this.c = k96;
        }

        @DexIgnore
        public final void onClick(View view) {
            Bi bi = this.c.m;
            if (bi != null) {
                bi.u1();
            }
            K96 k96 = this.c;
            Dialog dialog = this.b;
            Wg6.b(dialog, "dialog");
            k96.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog b;
        @DexIgnore
        public /* final */ /* synthetic */ K96 c;

        @DexIgnore
        public Ei(Dialog dialog, K25 k25, K96 k96) {
            this.b = dialog;
            this.c = k96;
        }

        @DexIgnore
        public final void onClick(View view) {
            Bi bi = this.c.m;
            if (bi != null) {
                bi.p5();
            }
            K96 k96 = this.c;
            Dialog dialog = this.b;
            Wg6.b(dialog, "dialog");
            k96.onDismiss(dialog);
        }
    }

    @DexIgnore
    public final void B6(Bi bi) {
        this.m = bi;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Dialog dialog;
        Wg6.c(layoutInflater, "inflater");
        K25 k25 = (K25) Aq0.f(layoutInflater, 2131558501, viewGroup, false, this.k);
        G37<K25> g37 = new G37<>(this, k25);
        this.l = g37;
        K25 a2 = g37.a();
        if (!(a2 == null || (dialog = getDialog()) == null)) {
            a2.q.setOnClickListener(new Ci(dialog, a2, this));
            a2.s.setOnClickListener(new Di(dialog, a2, this));
            a2.r.setOnClickListener(new Ei(dialog, a2, this));
        }
        Wg6.b(k25, "binding");
        return k25.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
