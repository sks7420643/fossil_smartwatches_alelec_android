package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G67 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static /* final */ String d; // = "g67";
    @DexIgnore
    public /* final */ FragmentManager a;
    @DexIgnore
    public /* final */ List<Fragment> b;
    @DexIgnore
    public int[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public Ai(G67 g67, View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;

        @DexIgnore
        public Bi(RecyclerView.ViewHolder viewHolder) {
            this.b = viewHolder;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            this.b.itemView.removeOnAttachStateChangeListener(this);
            BaseFragment baseFragment = (BaseFragment) G67.this.b.get(this.b.getAdapterPosition());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = G67.d;
            local.d(str, "onBindViewHolder tag=" + baseFragment.D6() + " childFragmentManager=" + G67.this.a + "this=" + this);
            Fragment Z = G67.this.a.Z(baseFragment.D6());
            if (Z != null) {
                Xq0 j = G67.this.a.j();
                j.q(Z);
                j.k();
            }
            Xq0 j2 = G67.this.a.j();
            j2.b(view.getId(), baseFragment, baseFragment.D6());
            j2.k();
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public G67(FragmentManager fragmentManager, List<Fragment> list) {
        this.a = fragmentManager;
        this.b = list;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        int size = this.b.size();
        int[] iArr = this.c;
        if (iArr == null || size != iArr.length) {
            this.c = new int[size];
        }
        return size;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        this.c[i] = this.b.get(i).getId();
        int[] iArr = this.c;
        if (iArr[i] == 0) {
            iArr[i] = View.generateViewId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = d;
            local.d(str, "getItemId: position = " + i + ", id = " + this.c[i]);
        }
        return (long) this.c[i];
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "onBindViewHolder: position = " + i);
        Fragment Y = this.a.Y(viewHolder.itemView.getId());
        if (Y != null) {
            Xq0 j = this.a.j();
            j.q(Y);
            j.k();
        }
        viewHolder.itemView.setId((int) getItemId(i));
        viewHolder.itemView.addOnAttachStateChangeListener(new Bi(viewHolder));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        return new Ai(this, frameLayout);
    }
}
