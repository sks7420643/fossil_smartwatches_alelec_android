package com.fossil;

import com.fossil.A34;
import com.fossil.F34;
import com.fossil.Y24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z24<K, V> extends F34<K, V> implements S34<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient Z24<V, K> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<K, V> extends F34.Ci<K, V> {
        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.F34.Ci
        public /* bridge */ /* synthetic */ F34.Ci b(Object obj, Object obj2) {
            f(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F34.Ci
        public /* bridge */ /* synthetic */ F34.Ci c(Map.Entry entry) {
            g(entry);
            return this;
        }

        @DexIgnore
        public Z24<K, V> e() {
            return (Z24) super.a();
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> f(K k, V v) {
            super.b(k, v);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> g(Map.Entry<? extends K, ? extends V> entry) {
            super.c(entry);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> h(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.d(iterable);
            return this;
        }
    }

    @DexIgnore
    public Z24(A34<K, Y24<V>> a34, int i) {
        super(a34, i);
    }

    @DexIgnore
    public static <K, V> Ai<K, V> builder() {
        return new Ai<>();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.A34$Bi */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> Z24<K, V> copyOf(Y34<? extends K, ? extends V> y34) {
        if (y34.isEmpty()) {
            return of();
        }
        if (y34 instanceof Z24) {
            Z24<K, V> z24 = (Z24) y34;
            if (!z24.isPartialView()) {
                return z24;
            }
        }
        A34.Bi bi = new A34.Bi(y34.asMap().size());
        int i = 0;
        for (Map.Entry<? extends K, Collection<? extends V>> entry : y34.asMap().entrySet()) {
            Y24 copyOf = Y24.copyOf((Collection) entry.getValue());
            if (!copyOf.isEmpty()) {
                bi.c(entry.getKey(), copyOf);
                i = copyOf.size() + i;
            }
        }
        return new Z24<>(bi.a(), i);
    }

    @DexIgnore
    public static <K, V> Z24<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Ai ai = new Ai();
        ai.h(iterable);
        return ai.e();
    }

    @DexIgnore
    public static <K, V> Z24<K, V> of() {
        return G24.INSTANCE;
    }

    @DexIgnore
    public static <K, V> Z24<K, V> of(K k, V v) {
        Ai builder = builder();
        builder.f(k, v);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> Z24<K, V> of(K k, V v, K k2, V v2) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> Z24<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> Z24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> Z24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        builder.f(k5, v5);
        return builder.e();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.A34$Bi */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            A34.Bi builder = A34.builder();
            int i = 0;
            int i2 = 0;
            while (i2 < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    Y24.Bi builder2 = Y24.builder();
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        builder2.g(objectInputStream.readObject());
                    }
                    builder.c(readObject, builder2.i());
                    i2++;
                    i += readInt2;
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                F34.Ei.a.b(this, builder.a());
                F34.Ei.b.a(this, i);
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        V44.d(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.Z24<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.Z24$Ai */
    /* JADX WARN: Multi-variable type inference failed */
    public final Z24<V, K> a() {
        Ai builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.f(entry.getValue(), entry.getKey());
        }
        Z24<V, K> e = builder.e();
        e.g = this;
        return e;
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34, com.fossil.Y34
    public Y24<V> get(K k) {
        Y24<V> y24 = (Y24) this.map.get(k);
        return y24 == null ? Y24.of() : y24;
    }

    @DexIgnore
    @Override // com.fossil.F34
    public Z24<V, K> inverse() {
        Z24<V, K> z24 = this.g;
        if (z24 != null) {
            return z24;
        }
        Z24<V, K> a2 = a();
        this.g = a2;
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34
    @CanIgnoreReturnValue
    @Deprecated
    public Y24<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34, com.fossil.U14
    @CanIgnoreReturnValue
    @Deprecated
    public Y24<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }
}
