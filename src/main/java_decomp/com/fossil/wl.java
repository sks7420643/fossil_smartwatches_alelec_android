package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wl extends Qq7 implements Coroutine<Fs, Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Ro b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Wl(Ro ro, long j) {
        super(2);
        this.b = ro;
        this.c = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(Fs fs, Float f) {
        Ro ro = this.b;
        long floatValue = ((long) (f.floatValue() * ((float) this.c))) + ro.I;
        ro.F = floatValue;
        float N = (((float) floatValue) * 1.0f) / ((float) ro.N());
        if (Math.abs(N - this.b.G) > this.b.R || N == 1.0f) {
            Ro ro2 = this.b;
            ro2.G = N;
            ro2.d(N);
        }
        return Cd6.a;
    }
}
