package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Od2 extends Gl2 implements Ld2 {
    @DexIgnore
    public Od2() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.Gl2
    public boolean X2(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        u0(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
