package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ix1;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bi extends Mj {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ Kb F;
    @DexIgnore
    public /* final */ ArrayList<byte[]> G;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<J0> H;
    @DexIgnore
    public /* final */ ArrayList<J0> I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public float L;
    @DexIgnore
    public int M;
    @DexIgnore
    public long N;
    @DexIgnore
    public int O;
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ boolean R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public /* final */ boolean T;
    @DexIgnore
    public /* final */ float U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Bi(com.fossil.K5 r10, com.fossil.I60 r11, com.fossil.Yp r12, short r13, boolean r14, java.util.HashMap r15, float r16, java.lang.String r17, int r18) {
        /*
            r9 = this;
            r1 = r18 & 4
            if (r1 == 0) goto L_0x00a6
            com.fossil.Yp r4 = com.fossil.Yp.m
        L_0x0006:
            r8 = 0
            r1 = r18 & 16
            if (r1 == 0) goto L_0x000c
            r14 = 0
        L_0x000c:
            r1 = r18 & 32
            if (r1 == 0) goto L_0x0015
            java.util.HashMap r15 = new java.util.HashMap
            r15.<init>()
        L_0x0015:
            r1 = r18 & 64
            if (r1 == 0) goto L_0x001c
            r16 = 981668463(0x3a83126f, float:0.001)
        L_0x001c:
            r0 = r18
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x00a9
            java.lang.String r1 = "UUID.randomUUID().toString()"
            java.lang.String r6 = com.fossil.E.a(r1)
        L_0x0028:
            r7 = 1
            r1 = r9
            r2 = r10
            r3 = r11
            r5 = r13
            r1.<init>(r2, r3, r4, r5, r6, r7)
            r0 = r16
            r9.U = r0
            r9.E = r14
            com.fossil.Kb r1 = new com.fossil.Kb
            r1.<init>(r13)
            r9.F = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r9.G = r1
            java.util.concurrent.CopyOnWriteArrayList r1 = new java.util.concurrent.CopyOnWriteArrayList
            r1.<init>()
            r9.H = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r9.I = r1
            r1 = -1
            r9.M = r1
            com.fossil.Hu1 r1 = com.fossil.Hu1.SKIP_LIST
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00ad
            boolean r1 = r1.booleanValue()
        L_0x0063:
            r9.P = r1
            com.fossil.Hu1 r1 = com.fossil.Hu1.SKIP_ERASE
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00af
            boolean r1 = r1.booleanValue()
        L_0x0073:
            r9.Q = r1
            com.fossil.Hu1 r1 = com.fossil.Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00b1
            boolean r1 = r1.booleanValue()
        L_0x0083:
            r9.R = r1
            com.fossil.Hu1 r1 = com.fossil.Hu1.NUMBER_OF_FILE_REQUIRED
            java.lang.Object r1 = r15.get(r1)
            java.lang.Integer r1 = (java.lang.Integer) r1
            if (r1 == 0) goto L_0x00b3
            int r1 = r1.intValue()
        L_0x0093:
            r9.S = r1
            com.fossil.Hu1 r1 = com.fossil.Hu1.ERASE_CACHE_FILE_BEFORE_GET
            java.lang.Object r1 = r15.get(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00b5
            boolean r1 = r1.booleanValue()
        L_0x00a3:
            r9.T = r1
            return
        L_0x00a6:
            r4 = r12
            goto L_0x0006
        L_0x00a9:
            r6 = r17
            goto L_0x0028
        L_0x00ad:
            r1 = 0
            goto L_0x0063
        L_0x00af:
            r1 = 0
            goto L_0x0073
        L_0x00b1:
            r1 = 0
            goto L_0x0083
        L_0x00b3:
            r1 = 0
            goto L_0x0093
        L_0x00b5:
            r1 = r8
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Bi.<init>(com.fossil.K5, com.fossil.I60, com.fossil.Yp, short, boolean, java.util.HashMap, float, java.lang.String, int):void");
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        if (this.T) {
            V();
        }
        if (!this.R) {
            v(new Oh(this));
        }
        if (this.P) {
            this.H.add(new J0(this.w.x, this.D, 4294967295L, 0));
            Y();
            return;
        }
        Lp.j(this, Hs.v, null, 2, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Mj
    public JSONObject C() {
        JSONObject put = super.C().put(Ey1.a(Hu1.SKIP_LIST), this.P).put(Ey1.a(Hu1.SKIP_ERASE), this.Q).put(Ey1.a(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.R).put(Ey1.a(Hu1.NUMBER_OF_FILE_REQUIRED), this.S).put(Ey1.a(Hu1.ERASE_CACHE_FILE_BEFORE_GET), this.T);
        Wg6.b(put, "super.optionDescription(\u2026 eraseCacheFileBeforeGet)");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject put = super.E().put(Ey1.a(Hu1.SKIP_ERASE), this.Q);
        Wg6.b(put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        Jd0 jd0 = Jd0.P2;
        Object[] array = this.I.toArray(new J0[0]);
        if (array != null) {
            return G80.k(put, jd0, Px1.a((Ox1[]) array));
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final long G(J0 j0) {
        U.a.c(j0.c, j0.d, j0.e);
        if (!(j0.f.length == 0)) {
            return U.a.d(j0);
        }
        K5 k5 = this.w;
        Ky1 ky1 = Ky1.DEBUG;
        String str = k5.x;
        byte b = j0.d;
        byte b2 = j0.e;
        return -1;
    }

    @DexIgnore
    public final J0 H(byte b, byte b2) {
        return (J0) Pm7.I(U.a.i(this.w.x, b, b2), 0);
    }

    @DexIgnore
    public final void L(Oi oi) {
        long j = oi.D;
        this.N = j;
        J0 j0 = oi.H;
        G(J0.a(j0, null, (byte) 0, (byte) 0, Dm7.k(j0.f, 0, (int) j), 0, 0, 0, false, 119));
        long j2 = this.J;
        float f = j2 > 0 ? (((float) (this.K + this.N)) * 1.0f) / ((float) j2) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (Math.abs(f - this.L) > this.U || f == 1.0f) {
            this.L = f;
            d(f);
        }
        X();
    }

    @DexIgnore
    public void M(ArrayList<J0> arrayList) {
        l(Nr.a(this.v, null, Zq.b, null, null, 13));
    }

    @DexIgnore
    public final void O(J0 j0) {
        Lp.h(this, new Oi(this.w, this.x, j0, this.z), new Ag(this), new Og(this), null, null, null, 56, null);
    }

    @DexIgnore
    public void Q(J0 j0) {
        Ky1 ky1 = Ky1.DEBUG;
        j0.toJSONString(2);
    }

    @DexIgnore
    public final void S(J0 j0) {
        if (j0.h != Ix1.a.b(j0.f, Ix1.Ai.CRC32) || ((int) j0.g) != j0.f.length) {
            int i = this.O;
            if (i < 3) {
                this.O = i + 1;
                O(j0);
                return;
            }
            l(Nr.a(this.v, null, Zq.n, null, null, 13));
        } else if (G(J0.a(j0, null, (byte) 0, (byte) 0, null, 0, 0, 0, true, 127)) < 0) {
            l(Nr.a(this.v, null, Zq.C, null, null, 13));
        } else {
            this.K += j0.g;
            this.G.add(j0.f);
            Q(j0);
            if (this.Q) {
                Y();
            } else {
                Lp.j(this, Hs.x, null, 2, null);
            }
        }
    }

    @DexIgnore
    public final void V() {
        U.a.b(this.w.x, this.F.b);
    }

    @DexIgnore
    public final J0 W() {
        return (J0) Pm7.I(this.H, this.M);
    }

    @DexIgnore
    public final void X() {
        J0 W = W();
        if (W == null) {
            Wg6.i();
            throw null;
        } else if (W.g > 0) {
            n(Hs.w, new Bh(this));
        } else if (this.Q) {
            Y();
        } else {
            Lp.j(this, Hs.x, null, 2, null);
        }
    }

    @DexIgnore
    public final void Y() {
        int i = this.M + 1;
        this.M = i;
        if (i < this.H.size()) {
            J0 W = W();
            if (W != null) {
                Kb kb = new Kb(W.b());
                J0 H2 = H(kb.b, kb.c);
                K5 k5 = this.w;
                Ky1 ky1 = Ky1.DEBUG;
                String str = k5.x;
                if (H2 != null) {
                    H2.toString();
                }
                J0 W2 = W();
                if (W2 == null) {
                    Wg6.i();
                    throw null;
                } else if (W2.g <= 0) {
                    Y();
                } else if (H2 != null) {
                    J0 W3 = W();
                    if (W3 != null) {
                        long j = W3.g;
                        J0 W4 = W();
                        if (W4 != null) {
                            S(J0.a(H2, null, (byte) 0, (byte) 0, null, j, W4.h, 0, false, 207));
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    this.N = 0;
                    X();
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else if (this.G.size() < this.S) {
            l(Nr.a(this.v, null, Zq.s, null, null, 13));
        } else {
            this.I.clear();
            this.I.addAll(U.a.h(this.w.x, this.F.b));
            M(this.I);
        }
    }

    @DexIgnore
    public final void Z() {
        T t;
        boolean z;
        for (T t2 : U.a.j(this.w.x, this.F.b)) {
            Iterator<T> it = this.H.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (t.b() == ByteBuffer.allocate(2).put(t2.d).put(t2.e).getShort(0)) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t == null && Ix1.a.b(t2.f, Ix1.Ai.CRC32) != t2.h) {
                U.a.c(this.w.x, t2.d, t2.e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Fs b(Hs hs) {
        int i = Gq.a[hs.ordinal()];
        if (i == 1) {
            Iv iv = new Iv(this.D, this.w, 0, 4);
            iv.o(new Mf(this));
            return iv;
        } else if (i == 2) {
            J0 W = W();
            if (W != null) {
                Gv gv = new Gv(W.b(), this.N, 4294967295L, this.w, 0, 16);
                Ir ir = new Ir(this, W);
                if (!gv.t) {
                    gv.n.add(ir);
                } else {
                    ir.invoke(gv);
                }
                Ye ye = new Ye(this, W);
                if (!gv.t) {
                    gv.o.add(ye);
                }
                gv.s = w();
                return gv;
            }
            Wg6.i();
            throw null;
        } else if (i != 3) {
            return null;
        } else {
            J0 W2 = W();
            if (W2 != null) {
                Cv cv = new Cv(W2.b(), this.w, 0, 4);
                cv.o(new Uq(this));
                return cv;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public final boolean t() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        Object[] array = this.I.toArray(new J0[0]);
        if (array != null) {
            return array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
