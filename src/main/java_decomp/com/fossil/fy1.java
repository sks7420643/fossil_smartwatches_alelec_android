package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fy1 {
    @DexIgnore
    public static final String a(Throwable th) {
        Wg6.c(th, "$this$wrappedLocalizedMessage");
        String localizedMessage = th.getLocalizedMessage();
        return localizedMessage != null ? localizedMessage : th.toString();
    }
}
