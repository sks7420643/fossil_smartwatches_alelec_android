package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G2 extends C2 {
    @DexIgnore
    public static /* final */ F2 CREATOR; // = new F2(null);
    @DexIgnore
    public /* final */ V8[] e;
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore
    public G2(byte b, V8[] v8Arr, byte[] bArr) {
        super(Lt.h, b, false, 4);
        this.e = v8Arr;
        this.f = bArr;
    }

    @DexIgnore
    public /* synthetic */ G2(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Object[] createTypedArray = parcel.createTypedArray(V8.CREATOR);
        if (createTypedArray != null) {
            this.e = (V8[]) createTypedArray;
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                this.f = createByteArray;
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.C2
    public byte[] a() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(G2.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            G2 g2 = (G2) obj;
            return this.c == g2.c && Arrays.equals(this.e, g2.e) && Arrays.equals(this.f, g2.f);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        byte b = this.c;
        return (((b * 31) + this.e.hashCode()) * 31) + Arrays.hashCode(this.f);
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (V8 v8 : this.e) {
            jSONArray.put(v8.toJSONObject());
        }
        return G80.k(G80.k(super.toJSONObject(), Jd0.N1, jSONArray), Jd0.O1, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.e, i);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
    }
}
