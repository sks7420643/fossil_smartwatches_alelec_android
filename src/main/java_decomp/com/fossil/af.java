package com.fossil;

import com.fossil.Ix1;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Af extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.d, Ow.e));
    @DexIgnore
    public /* final */ long D; // = ((long) this.N.length);
    @DexIgnore
    public long E;
    @DexIgnore
    public float F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public /* final */ Ve M; // = Hd0.y.m();
    @DexIgnore
    public /* final */ byte[] N;
    @DexIgnore
    public /* final */ boolean O;
    @DexIgnore
    public /* final */ short P;
    @DexIgnore
    public /* final */ float Q;

    @DexIgnore
    public Af(K5 k5, I60 i60, byte[] bArr, boolean z, short s, float f, String str) {
        super(k5, i60, Yp.l0, str, false, 16);
        this.N = bArr;
        this.O = z;
        this.P = (short) s;
        this.Q = f;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.h(this, new Tl(this.w, this.x, this.M, this.z), new Jn(this), new Vn(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(G80.k(G80.k(super.C(), Jd0.J, Long.valueOf(Ix1.a.b(this.N, Ix1.Ai.CRC32))), Jd0.x0, Boolean.valueOf(this.O)), Jd0.A0, Hy1.l(this.P, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void D() {
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
    }

    @DexIgnore
    public final void T() {
        long j = this.J + this.I;
        Lp.i(this, new Aw(j, Math.min(6144L, this.D - j), this.D, this.P, this.w, 0, 32), new Mm(this), new Xm(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void U() {
        Lp.i(this, new Ew(this.P, this.w, 0, 4), new Up(this), new Iq(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void V() {
        long j = this.H;
        Lp.i(this, new Fw(0, j, this.D, this.P, this.w, 0, 32), new Wq(this, Ix1.a.a(this.N, (int) 0, (int) j, Ix1.Ai.CRC32)), new Kr(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
