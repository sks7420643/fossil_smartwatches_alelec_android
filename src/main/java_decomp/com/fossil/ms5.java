package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.BaseActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ms5 implements MembersInjector<BaseActivity> {
    @DexIgnore
    public static void a(BaseActivity baseActivity, DeviceRepository deviceRepository) {
        baseActivity.l = deviceRepository;
    }

    @DexIgnore
    public static void b(BaseActivity baseActivity, Gu5 gu5) {
        baseActivity.s = gu5;
    }

    @DexIgnore
    public static void c(BaseActivity baseActivity, MigrationManager migrationManager) {
        baseActivity.m = migrationManager;
    }

    @DexIgnore
    public static void d(BaseActivity baseActivity, An4 an4) {
        baseActivity.k = an4;
    }

    @DexIgnore
    public static void e(BaseActivity baseActivity, UserRepository userRepository) {
        baseActivity.j = userRepository;
    }
}
