package com.fossil;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yf0 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public SimpleArrayMap<Hm0, MenuItem> b;
    @DexIgnore
    public SimpleArrayMap<Im0, SubMenu> c;

    @DexIgnore
    public Yf0(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final MenuItem c(MenuItem menuItem) {
        if (!(menuItem instanceof Hm0)) {
            return menuItem;
        }
        Hm0 hm0 = (Hm0) menuItem;
        if (this.b == null) {
            this.b = new SimpleArrayMap<>();
        }
        MenuItem menuItem2 = this.b.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        Fg0 fg0 = new Fg0(this.a, hm0);
        this.b.put(hm0, fg0);
        return fg0;
    }

    @DexIgnore
    public final SubMenu d(SubMenu subMenu) {
        if (!(subMenu instanceof Im0)) {
            return subMenu;
        }
        Im0 im0 = (Im0) subMenu;
        if (this.c == null) {
            this.c = new SimpleArrayMap<>();
        }
        SubMenu subMenu2 = this.c.get(im0);
        if (subMenu2 != null) {
            return subMenu2;
        }
        Og0 og0 = new Og0(this.a, im0);
        this.c.put(im0, og0);
        return og0;
    }

    @DexIgnore
    public final void e() {
        SimpleArrayMap<Hm0, MenuItem> simpleArrayMap = this.b;
        if (simpleArrayMap != null) {
            simpleArrayMap.clear();
        }
        SimpleArrayMap<Im0, SubMenu> simpleArrayMap2 = this.c;
        if (simpleArrayMap2 != null) {
            simpleArrayMap2.clear();
        }
    }

    @DexIgnore
    public final void f(int i) {
        if (this.b != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.b.size()) {
                    if (this.b.j(i3).getGroupId() == i) {
                        this.b.l(i3);
                        i3--;
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void g(int i) {
        if (this.b != null) {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.j(i2).getItemId() == i) {
                    this.b.l(i2);
                    return;
                }
            }
        }
    }
}
