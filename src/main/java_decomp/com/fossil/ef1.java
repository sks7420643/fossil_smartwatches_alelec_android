package com.fossil;

import com.fossil.Af1;
import com.fossil.Ua1;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ef1 {
    @DexIgnore
    public static /* final */ Ci e; // = new Ci();
    @DexIgnore
    public static /* final */ Af1<Object, Object> f; // = new Ai();
    @DexIgnore
    public /* final */ List<Bi<?, ?>> a;
    @DexIgnore
    public /* final */ Ci b;
    @DexIgnore
    public /* final */ Set<Bi<?, ?>> c;
    @DexIgnore
    public /* final */ Mn0<List<Throwable>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Af1<Object, Object> {
        @DexIgnore
        @Override // com.fossil.Af1
        public boolean a(Object obj) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Af1
        public Af1.Ai<Object> b(Object obj, int i, int i2, Ob1 ob1) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<Model, Data> {
        @DexIgnore
        public /* final */ Class<Model> a;
        @DexIgnore
        public /* final */ Class<Data> b;
        @DexIgnore
        public /* final */ Bf1<? extends Model, ? extends Data> c;

        @DexIgnore
        public Bi(Class<Model> cls, Class<Data> cls2, Bf1<? extends Model, ? extends Data> bf1) {
            this.a = cls;
            this.b = cls2;
            this.c = bf1;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }

        @DexIgnore
        public boolean b(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public <Model, Data> Df1<Model, Data> a(List<Af1<Model, Data>> list, Mn0<List<Throwable>> mn0) {
            return new Df1<>(list, mn0);
        }
    }

    @DexIgnore
    public Ef1(Mn0<List<Throwable>> mn0) {
        this(mn0, e);
    }

    @DexIgnore
    public Ef1(Mn0<List<Throwable>> mn0, Ci ci) {
        this.a = new ArrayList();
        this.c = new HashSet();
        this.d = mn0;
        this.b = ci;
    }

    @DexIgnore
    public static <Model, Data> Af1<Model, Data> f() {
        return (Af1<Model, Data>) f;
    }

    @DexIgnore
    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, Bf1<? extends Model, ? extends Data> bf1, boolean z) {
        Bi<?, ?> bi = new Bi<>(cls, cls2, bf1);
        List<Bi<?, ?>> list = this.a;
        list.add(z ? list.size() : 0, bi);
    }

    @DexIgnore
    public <Model, Data> void b(Class<Model> cls, Class<Data> cls2, Bf1<? extends Model, ? extends Data> bf1) {
        synchronized (this) {
            a(cls, cls2, bf1, true);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: com.fossil.Af1<? extends Model, ? extends Data>, com.fossil.Af1<Model, Data> */
    public final <Model, Data> Af1<Model, Data> c(Bi<?, ?> bi) {
        Af1 b2 = bi.c.b(this);
        Ik1.d(b2);
        return (Af1<? extends Model, ? extends Data>) b2;
    }

    @DexIgnore
    public <Model, Data> Af1<Model, Data> d(Class<Model> cls, Class<Data> cls2) {
        Af1<Model, Data> f2;
        synchronized (this) {
            try {
                ArrayList arrayList = new ArrayList();
                boolean z = false;
                for (Bi<?, ?> bi : this.a) {
                    if (this.c.contains(bi)) {
                        z = true;
                    } else if (bi.b(cls, cls2)) {
                        this.c.add(bi);
                        arrayList.add(c(bi));
                        this.c.remove(bi);
                    }
                }
                if (arrayList.size() > 1) {
                    f2 = this.b.a(arrayList, this.d);
                } else if (arrayList.size() == 1) {
                    f2 = (Af1) arrayList.get(0);
                } else if (z) {
                    f2 = f();
                } else {
                    throw new Ua1.Ci((Class<?>) cls, (Class<?>) cls2);
                }
            } catch (Throwable th) {
                this.c.clear();
                throw th;
            }
        }
        return f2;
    }

    @DexIgnore
    public <Model> List<Af1<Model, ?>> e(Class<Model> cls) {
        ArrayList arrayList;
        synchronized (this) {
            try {
                arrayList = new ArrayList();
                for (Bi<?, ?> bi : this.a) {
                    if (!this.c.contains(bi) && bi.a(cls)) {
                        this.c.add(bi);
                        arrayList.add(c(bi));
                        this.c.remove(bi);
                    }
                }
            } catch (Throwable th) {
                this.c.clear();
                throw th;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<Class<?>> g(Class<?> cls) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (Bi<?, ?> bi : this.a) {
                if (!arrayList.contains(bi.b) && bi.a(cls)) {
                    arrayList.add(bi.b);
                }
            }
        }
        return arrayList;
    }
}
