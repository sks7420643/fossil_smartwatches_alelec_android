package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tx implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Sz b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;

    @DexIgnore
    public Tx(Sz sz, Object obj) {
        this.b = sz;
        this.c = obj;
    }

    @DexIgnore
    public final void run() {
        this.b.b.o(this.c);
    }
}
