package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rt3 extends Dt3 {
    @DexIgnore
    public /* final */ Lu3<Void> a; // = new Lu3<>();

    @DexIgnore
    @Override // com.fossil.Dt3
    public final boolean a() {
        return this.a.p();
    }

    @DexIgnore
    @Override // com.fossil.Dt3
    public final Dt3 b(Kt3 kt3) {
        this.a.f(new St3(this, kt3));
        return this;
    }

    @DexIgnore
    public final void c() {
        this.a.y(null);
    }
}
