package com.fossil;

import android.util.Log;
import com.crashlytics.android.answers.Answers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nk1 implements Qk1 {
    @DexIgnore
    public /* final */ Answers a;

    @DexIgnore
    public Nk1(Answers answers) {
        this.a = answers;
    }

    @DexIgnore
    public static Nk1 b() throws NoClassDefFoundError, IllegalStateException {
        return c(Answers.getInstance());
    }

    @DexIgnore
    public static Nk1 c(Answers answers) throws IllegalStateException {
        if (answers != null) {
            return new Nk1(answers);
        }
        throw new IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    @Override // com.fossil.Qk1
    public void a(Pk1 pk1) {
        try {
            this.a.logCustom(pk1.b());
        } catch (Throwable th) {
            Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
