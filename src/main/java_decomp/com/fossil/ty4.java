package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.F57;
import com.fossil.X71;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ty4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements X71.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ ImageView a;
        @DexIgnore
        public /* final */ /* synthetic */ F57 b;
        @DexIgnore
        public /* final */ /* synthetic */ Sy4 c;

        @DexIgnore
        public Ai(ImageView imageView, F57 f57, Sy4 sy4) {
            this.a = imageView;
            this.b = f57;
            this.c = sy4;
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void a(Object obj) {
            Wg6.c(obj, "data");
            X71.Ai.Aii.c(this, obj);
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void b(Object obj, Throwable th) {
            Wg6.c(th, "throwable");
            X71.Ai.Aii.b(this, obj, th);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("Coil", "error: " + th.getMessage() + " - phoneDate: " + new Date() + " - realDate: " + Xy4.a.a());
            this.a.setImageDrawable(this.b);
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void c(Object obj, Q51 q51) {
            Wg6.c(obj, "data");
            Wg6.c(q51, "source");
            X71.Ai.Aii.d(this, obj, q51);
            this.a.setTag(this.c);
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void onCancel(Object obj) {
            X71.Ai.Aii.a(this, obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements X71.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ ImageView a;
        @DexIgnore
        public /* final */ /* synthetic */ Drawable b;

        @DexIgnore
        public Bi(ImageView imageView, Drawable drawable) {
            this.a = imageView;
            this.b = drawable;
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void a(Object obj) {
            Wg6.c(obj, "data");
            X71.Ai.Aii.c(this, obj);
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void b(Object obj, Throwable th) {
            Wg6.c(th, "throwable");
            X71.Ai.Aii.b(this, obj, th);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("Coil", "error: " + th.getMessage() + " - phoneDate: " + new Date() + " - realDate: " + Xy4.a.a());
            this.a.setImageDrawable(this.b);
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void c(Object obj, Q51 q51) {
            Wg6.c(obj, "data");
            Wg6.c(q51, "source");
            X71.Ai.Aii.d(this, obj, q51);
        }

        @DexIgnore
        @Override // com.fossil.X71.Ai
        public void onCancel(Object obj) {
            X71.Ai.Aii.a(this, obj);
        }
    }

    @DexIgnore
    public static final void a(ImageView imageView, String str, Drawable drawable) {
        Wg6.c(imageView, "$this$setUrl");
        E51.a(imageView);
        if (str == null || str.length() == 0) {
            imageView.setImageDrawable(drawable);
            return;
        }
        A51 b = X41.b();
        Context context = imageView.getContext();
        Wg6.b(context, "context");
        U71 u71 = new U71(context, b.a());
        u71.x(str);
        u71.z(imageView);
        u71.y(drawable);
        u71.v(new L81());
        u71.t(new Bi(imageView, drawable));
        b.b(u71.w());
    }

    @DexIgnore
    public static final void b(ImageView imageView, String str, String str2, F57.Bi bi, Uy4 uy4) {
        Wg6.c(imageView, "$this$setUrl");
        Wg6.c(bi, "drawableBuilder");
        Wg6.c(uy4, "colorGenerator");
        Sy4 sy4 = new Sy4(str, str2);
        E51.a(imageView);
        String valueOf = str2 == null || str2.length() == 0 ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : String.valueOf(Yt7.w0(str2));
        if (str2 == null) {
            str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        F57 e = bi.e(valueOf, uy4.b(str2));
        Wg6.b(e, "drawableBuilder.build(av\u2026tor.getColor(str ?: \"-\"))");
        if (str == null || str.length() == 0) {
            imageView.setImageDrawable(e);
            imageView.setTag(sy4);
            return;
        }
        A51 b = X41.b();
        Context context = imageView.getContext();
        Wg6.b(context, "context");
        U71 u71 = new U71(context, b.a());
        u71.x(str);
        u71.z(imageView);
        u71.y(e);
        u71.v(new L81());
        u71.t(new Ai(imageView, e, sy4));
        b.b(u71.w());
    }
}
