package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mu5 implements Factory<Lu5> {
    @DexIgnore
    public static Lu5 a(NotificationsRepository notificationsRepository) {
        return new Lu5(notificationsRepository);
    }
}
