package com.fossil;

import com.mapped.Hg6;
import com.mapped.Hi6;
import com.mapped.V60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Jb extends Nq7 implements Hg6<byte[], V60> {
    @DexIgnore
    public Jb(V60.Bi bi) {
        super(1, bi);
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public final Hi6 getOwner() {
        return Er7.b(V60.Bi.class);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/HeartRateModeConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public V60 invoke(byte[] bArr) {
        return ((V60.Bi) this.receiver).a(bArr);
    }
}
