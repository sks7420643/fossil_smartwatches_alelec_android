package com.fossil;

import com.fossil.Ix1;
import com.mapped.Wg6;
import com.mapped.Zd0;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ha extends Qx1<Zd0[]> {
    @DexIgnore
    public static /* final */ Ha b; // = new Ha();

    @DexIgnore
    public Ha() {
        super(new Ry1(2, 0));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [short, java.lang.Object] */
    @Override // com.fossil.Qx1
    public byte[] a(short s, Zd0[] zd0Arr) {
        Zd0[] zd0Arr2 = zd0Arr;
        byte[] d = d(zd0Arr2);
        byte[] array = ByteBuffer.allocate(d.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put((byte) c().getMajor()).put((byte) c().getMinor()).putShort(0).putShort((short) e(zd0Arr2)).putInt(d.length).put(d).putInt((int) Ix1.a.b(d, Ix1.Ai.CRC32C)).array();
        Wg6.b(array, "result.array()");
        return array;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Qx1
    public /* bridge */ /* synthetic */ byte[] b(Zd0[] zd0Arr) {
        return d(zd0Arr);
    }

    @DexIgnore
    public byte[] d(Zd0[] zd0Arr) {
        int e = e(zd0Arr);
        ArrayList arrayList = new ArrayList(zd0Arr.length);
        for (Zd0 zd0 : zd0Arr) {
            arrayList.add(zd0.getReplyMessageGroup());
        }
        List<Ev1> C = Pm7.C(arrayList);
        ByteBuffer order = ByteBuffer.allocate(e(zd0Arr)).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) 2).put((byte) 11);
        int i = 0;
        for (Ev1 ev1 : C) {
            ArrayList<Zd0> arrayList2 = new ArrayList();
            for (Zd0 zd02 : zd0Arr) {
                if (Wg6.a(zd02.getReplyMessageGroup(), ev1)) {
                    arrayList2.add(zd02);
                }
            }
            int i2 = 0;
            for (Zd0 zd03 : arrayList2) {
                i2 = zd03.getNotificationType().a() | i2;
            }
            order.putShort((short) i2).put((byte) ev1.getReplyMessages().length).putInt(e + 12 + i).putInt(ev1.a().length);
            i = ev1.a().length + i;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(order.array());
        for (Ev1 ev12 : C) {
            byteArrayOutputStream.write(ev12.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "entriesDataInByte.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final int e(Zd0[] zd0Arr) {
        ArrayList arrayList = new ArrayList(zd0Arr.length);
        for (Zd0 zd0 : zd0Arr) {
            arrayList.add(zd0.getReplyMessageGroup());
        }
        return (Pm7.C(arrayList).size() * 11) + 2;
    }
}
