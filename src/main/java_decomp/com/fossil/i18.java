package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface I18 {
    @DexIgnore
    public static final I18 a = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements I18 {
        @DexIgnore
        @Override // com.fossil.I18
        public void a(Q18 q18, List<H18> list) {
        }

        @DexIgnore
        @Override // com.fossil.I18
        public List<H18> b(Q18 q18) {
            return Collections.emptyList();
        }
    }

    @DexIgnore
    void a(Q18 q18, List<H18> list);

    @DexIgnore
    List<H18> b(Q18 q18);
}
