package com.fossil;

import com.fossil.Af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lf1 implements Af1<Te1, InputStream> {
    @DexIgnore
    public static /* final */ Nb1<Integer> b; // = Nb1.f("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);
    @DexIgnore
    public /* final */ Ze1<Te1, Te1> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Bf1<Te1, InputStream> {
        @DexIgnore
        public /* final */ Ze1<Te1, Te1> a; // = new Ze1<>(500);

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Te1, InputStream> b(Ef1 ef1) {
            return new Lf1(this.a);
        }
    }

    @DexIgnore
    public Lf1(Ze1<Te1, Te1> ze1) {
        this.a = ze1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Te1 te1) {
        return d(te1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(Te1 te1, int i, int i2, Ob1 ob1) {
        return c(te1, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> c(Te1 te1, int i, int i2, Ob1 ob1) {
        Ze1<Te1, Te1> ze1 = this.a;
        if (ze1 != null) {
            Te1 a2 = ze1.a(te1, 0, 0);
            if (a2 == null) {
                this.a.b(te1, 0, 0, te1);
            } else {
                te1 = a2;
            }
        }
        return new Af1.Ai<>(te1, new Cc1(te1, ((Integer) ob1.c(b)).intValue()));
    }

    @DexIgnore
    public boolean d(Te1 te1) {
        return true;
    }
}
