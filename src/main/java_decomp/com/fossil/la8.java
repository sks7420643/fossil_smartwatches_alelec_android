package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class La8 {
    @DexIgnore
    public Bi a;
    @DexIgnore
    public Ai b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public long a;
        @DexIgnore
        public long b;

        @DexIgnore
        public final long a() {
            return this.b;
        }

        @DexIgnore
        public final long b() {
            return this.a;
        }

        @DexIgnore
        public final void c(long j) {
            this.b = j;
        }

        @DexIgnore
        public final void d(long j) {
            this.a = j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public final int a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.a;
        }

        @DexIgnore
        public final void e(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void f(int i) {
            this.b = i;
        }

        @DexIgnore
        public final void g(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void h(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    public final String[] a() {
        Ai ai = this.b;
        if (ai != null) {
            long b2 = ai.b();
            Ai ai2 = this.b;
            if (ai2 != null) {
                List<Number> d0 = Em7.d0(new Long[]{Long.valueOf(b2), Long.valueOf(ai2.a())});
                ArrayList arrayList = new ArrayList(Im7.m(d0, 10));
                for (Number number : d0) {
                    arrayList.add(String.valueOf(number.longValue()));
                }
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    return (String[]) array;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            Wg6.n("durationConstraint");
            throw null;
        }
        Wg6.n("durationConstraint");
        throw null;
    }

    @DexIgnore
    public final String b() {
        return "duration >=? AND duration <=?";
    }

    @DexIgnore
    public final void c(Ai ai) {
        Wg6.c(ai, "<set-?>");
        this.b = ai;
    }

    @DexIgnore
    public final void d(boolean z) {
    }

    @DexIgnore
    public final void e(Bi bi) {
        Wg6.c(bi, "<set-?>");
        this.a = bi;
    }

    @DexIgnore
    public final String[] f() {
        Bi bi = this.a;
        if (bi != null) {
            int d = bi.d();
            Bi bi2 = this.a;
            if (bi2 != null) {
                int b2 = bi2.b();
                Bi bi3 = this.a;
                if (bi3 != null) {
                    int c = bi3.c();
                    Bi bi4 = this.a;
                    if (bi4 != null) {
                        List<Number> d0 = Em7.d0(new Integer[]{Integer.valueOf(d), Integer.valueOf(b2), Integer.valueOf(c), Integer.valueOf(bi4.a())});
                        ArrayList arrayList = new ArrayList(Im7.m(d0, 10));
                        for (Number number : d0) {
                            arrayList.add(String.valueOf(number.intValue()));
                        }
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            return (String[]) array;
                        }
                        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    Wg6.n("sizeConstraint");
                    throw null;
                }
                Wg6.n("sizeConstraint");
                throw null;
            }
            Wg6.n("sizeConstraint");
            throw null;
        }
        Wg6.n("sizeConstraint");
        throw null;
    }

    @DexIgnore
    public final String g() {
        return "width >= ? AND width <= ? AND height >= ? AND height <=?";
    }
}
