package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pm5 extends Em5 {
    @DexIgnore
    public Pm5(Jm5 jm5, Hm5 hm5) {
        super(jm5, hm5);
    }

    @DexIgnore
    @Override // com.fossil.Dm5
    public Cm5 a() {
        return this.a.b(137);
    }

    @DexIgnore
    public byte[] b() {
        return this.a.f(132);
    }
}
