package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.LruCache;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tm5 {
    @DexIgnore
    public static /* final */ Tm5 c; // = new Tm5();
    @DexIgnore
    public LruCache<String, String> a; // = new LruCache<>(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    @DexIgnore
    public LruCache<String, BitmapDrawable> b; // = new Ai(this, FileLogWriter.FILE_LOG_SIZE_THRESHOLD);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends LruCache<String, BitmapDrawable> {
        @DexIgnore
        public Ai(Tm5 tm5, int i) {
            super(i);
        }

        @DexIgnore
        public int a(String str, BitmapDrawable bitmapDrawable) {
            return bitmapDrawable.getBitmap().getByteCount() / 1024;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // android.util.LruCache
        public /* bridge */ /* synthetic */ int sizeOf(String str, BitmapDrawable bitmapDrawable) {
            return a(str, bitmapDrawable);
        }
    }

    @DexIgnore
    public static Tm5 d() {
        return c;
    }

    @DexIgnore
    public boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return !TextUtils.isEmpty(this.a.get(str));
    }

    @DexIgnore
    public void b() {
        this.a.evictAll();
        this.b.evictAll();
    }

    @DexIgnore
    public BitmapDrawable c(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public String e(String str) {
        return this.a.remove(str);
    }

    @DexIgnore
    public String f(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str2 = this.a.get(str);
        if (TextUtils.isEmpty(str2)) {
            try {
                str2 = Ym5.f(str);
                if (str2 != null) {
                    this.a.put(str, str2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(str2)) {
            return str2;
        }
        if (str2.contains("\\n")) {
            str2 = str2.replace("\\n", "\n");
        }
        return str2.replaceAll("\\\\", "");
    }

    @DexIgnore
    public void g(String str, String str2) {
        this.a.put(str, str2);
    }

    @DexIgnore
    public void h(String str, BitmapDrawable bitmapDrawable) {
        this.b.put(str, bitmapDrawable);
    }
}
