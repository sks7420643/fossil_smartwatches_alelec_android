package com.fossil;

import com.fossil.Dl7;
import com.mapped.Cd6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Px7<T> extends Ex7<Fx7> {
    @DexIgnore
    public /* final */ Lu7<T> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Lu7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Px7(Fx7 fx7, Lu7<? super T> lu7) {
        super(fx7);
        this.f = lu7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        w(th);
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return "ResumeAwaitOnCompletion[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.Zu7
    public void w(Throwable th) {
        Object Q = ((Fx7) this.e).Q();
        if (Nv7.a() && !(!(Q instanceof Sw7))) {
            throw new AssertionError();
        } else if (Q instanceof Vu7) {
            Lu7<T> lu7 = this.f;
            Throwable th2 = ((Vu7) Q).a;
            Dl7.Ai ai = Dl7.Companion;
            lu7.resumeWith(Dl7.constructor-impl(El7.a(th2)));
        } else {
            Lu7<T> lu72 = this.f;
            Object h = Gx7.h(Q);
            Dl7.Ai ai2 = Dl7.Companion;
            lu72.resumeWith(Dl7.constructor-impl(h));
        }
    }
}
