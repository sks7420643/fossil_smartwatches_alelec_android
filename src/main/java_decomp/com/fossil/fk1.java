package com.fossil;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fk1<T, Y> {
    @DexIgnore
    public /* final */ Map<T, Y> a; // = new LinkedHashMap(100, 0.75f, true);
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;

    @DexIgnore
    public Fk1(long j) {
        this.b = j;
    }

    @DexIgnore
    public void d() {
        m(0);
    }

    @DexIgnore
    public final void f() {
        m(this.b);
    }

    @DexIgnore
    public Y g(T t) {
        Y y;
        synchronized (this) {
            y = this.a.get(t);
        }
        return y;
    }

    @DexIgnore
    public long h() {
        long j;
        synchronized (this) {
            j = this.b;
        }
        return j;
    }

    @DexIgnore
    public int i(Y y) {
        return 1;
    }

    @DexIgnore
    public void j(T t, Y y) {
    }

    @DexIgnore
    public Y k(T t, Y y) {
        synchronized (this) {
            long i = (long) i(y);
            if (i >= this.b) {
                j(t, y);
                return null;
            }
            if (y != null) {
                this.c = i + this.c;
            }
            Y put = this.a.put(t, y);
            if (put != null) {
                this.c -= (long) i(put);
                if (!put.equals(y)) {
                    j(t, put);
                }
            }
            f();
            return put;
        }
    }

    @DexIgnore
    public Y l(T t) {
        Y remove;
        synchronized (this) {
            remove = this.a.remove(t);
            if (remove != null) {
                this.c -= (long) i(remove);
            }
        }
        return remove;
    }

    @DexIgnore
    public void m(long j) {
        synchronized (this) {
            while (this.c > j) {
                Iterator<Map.Entry<T, Y>> it = this.a.entrySet().iterator();
                Map.Entry<T, Y> next = it.next();
                Y value = next.getValue();
                this.c -= (long) i(value);
                T key = next.getKey();
                it.remove();
                j(key, value);
            }
        }
    }
}
