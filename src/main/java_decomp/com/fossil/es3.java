package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Es3 extends Vg2<Cs3> {
    @DexIgnore
    public static /* final */ Es3 c; // = new Es3();

    @DexIgnore
    public Es3() {
        super("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl");
    }

    @DexIgnore
    public static View c(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            try {
                return (View) Tg2.i(((Cs3) c.b(context)).y(Tg2.n(context), i, i2, str, i3));
            } catch (Exception e) {
                return new Bs3(context, i);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Vg2
    public final /* synthetic */ Cs3 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusOneButtonCreator");
        return queryLocalInterface instanceof Cs3 ? (Cs3) queryLocalInterface : new Ds3(iBinder);
    }
}
