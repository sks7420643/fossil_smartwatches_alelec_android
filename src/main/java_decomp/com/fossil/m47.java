package com.fossil;

import android.text.TextUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M47 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[Ci.values().length];
            b = iArr;
            try {
                iArr[Ci.CHAT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[Ci.PRIVACY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[Ci.SUPPORT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[Ci.STORE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                b[Ci.TERMS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                b[Ci.REPAIR_CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                b[Ci.CALL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                b[Ci.FAQ.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                b[Ci.DEVICE_FEATURE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                b[Ci.SUPPORT_MAIL.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                b[Ci.SOURCE_LICENSES.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                b[Ci.FEATURES.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            int[] iArr2 = new int[Bi.values().length];
            a = iArr2;
            try {
                iArr2[Bi.USING_YOUR_WATCH.ordinal()] = 1;
            } catch (NoSuchFieldError e13) {
            }
            try {
                a[Bi.LOW_BATTERY.ordinal()] = 2;
            } catch (NoSuchFieldError e14) {
            }
            try {
                a[Bi.SHOP_BATTERY.ordinal()] = 3;
            } catch (NoSuchFieldError e15) {
            }
            try {
                a[Bi.HOUR_TIME_24.ordinal()] = 4;
            } catch (NoSuchFieldError e16) {
            }
            try {
                a[Bi.ACTIVITY.ordinal()] = 5;
            } catch (NoSuchFieldError e17) {
            }
            try {
                a[Bi.ALARM.ordinal()] = 6;
            } catch (NoSuchFieldError e18) {
            }
            try {
                a[Bi.DATE.ordinal()] = 7;
            } catch (NoSuchFieldError e19) {
            }
            try {
                a[Bi.NOTIFICATIONS.ordinal()] = 8;
            } catch (NoSuchFieldError e20) {
            }
            try {
                a[Bi.Q_LINK.ordinal()] = 9;
            } catch (NoSuchFieldError e21) {
            }
            try {
                a[Bi.SECOND_TIME_ZONE.ordinal()] = 10;
            } catch (NoSuchFieldError e22) {
            }
            try {
                a[Bi.GOAL_TRACKING.ordinal()] = 11;
            } catch (NoSuchFieldError e23) {
            }
            try {
                a[Bi.CUSTOMIZE_DEVICE.ordinal()] = 12;
            } catch (NoSuchFieldError e24) {
            }
            try {
                a[Bi.STOP_WATCH.ordinal()] = 13;
            } catch (NoSuchFieldError e25) {
            }
        }
        */
    }

    @DexIgnore
    public enum Bi {
        USING_YOUR_WATCH,
        LOW_BATTERY,
        SHOP_BATTERY,
        HOUR_TIME_24,
        ACTIVITY,
        ALARM,
        DATE,
        NOTIFICATIONS,
        Q_LINK,
        SECOND_TIME_ZONE,
        GOAL_TRACKING,
        CUSTOMIZE_DEVICE,
        STOP_WATCH
    }

    @DexIgnore
    public enum Ci {
        PRIVACY,
        SUPPORT,
        STORE,
        TERMS,
        CALL,
        FEATURES,
        REPAIR_CENTER,
        DEVICE_FEATURE,
        CHAT,
        SUPPORT_MAIL,
        FAQ,
        SOURCE_LICENSES
    }

    @DexIgnore
    public static String a(Ci ci, Bi bi) {
        return b(ci, bi, null);
    }

    @DexIgnore
    public static String b(Ci ci, Bi bi, String str) {
        StringBuilder sb = new StringBuilder(H37.b.a(1));
        if (TextUtils.isEmpty(str)) {
            str = PortfolioApp.d0.J();
        }
        switch (Ai.b[ci.ordinal()]) {
            case 1:
                return "http://chat.fossil.com/chat/";
            case 2:
                sb.append("/privacy");
                break;
            case 3:
                sb.append("/support");
                break;
            case 4:
                sb.append("/store");
                break;
            case 5:
                sb.append("/terms");
                break;
            case 6:
                sb.append("/service_centers");
                break;
            case 7:
                sb.append("/call");
                break;
            case 8:
                sb.append("/faq");
                break;
            case 9:
                sb.append("/device_features/customize_device");
                break;
            case 10:
                sb.append("/support-emails");
                break;
            case 11:
                sb.append("/open-source-licenses");
                break;
            case 12:
                sb.append(DeviceHelper.o.A(str) ? "/tracker" : "/hybrid");
                if (bi == null) {
                    sb.append("/using_your_watch");
                    break;
                } else {
                    switch (Ai.a[bi.ordinal()]) {
                        case 1:
                            sb.append("/using_your_watch");
                            break;
                        case 2:
                            sb.append("/low_battery");
                            break;
                        case 3:
                            sb.append("/shop_battery");
                            break;
                        case 4:
                            sb.append("/24_hour_time");
                            break;
                        case 5:
                            sb.append("/activity");
                            break;
                        case 6:
                            sb.append("/alarm");
                            break;
                        case 7:
                            sb.append("/date");
                            break;
                        case 8:
                            sb.append("/notifications");
                            break;
                        case 9:
                            sb.append("/link");
                            break;
                        case 10:
                            sb.append("/second_time_zone");
                            break;
                        case 11:
                            sb.append("/service_centers");
                            break;
                        case 12:
                            sb.append("/customize_device");
                            break;
                        case 13:
                            sb.append("/stopwatch");
                            break;
                        default:
                            sb.append("/using_your_watch");
                            break;
                    }
                }
        }
        sb.append(String.format("?locale=%s&platform=android&serialNumber=%s&version=%s", c(), str, "2"));
        return sb.toString();
    }

    @DexIgnore
    public static String c() {
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (TextUtils.isEmpty(country)) {
            return language;
        }
        return String.format(Locale.US, "%s_%s", language, country);
    }
}
