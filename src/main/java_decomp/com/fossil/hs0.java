package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Cd6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Hs0<T> {
    @DexIgnore
    Object a(LiveData<T> liveData, Xe6<? super Dw7> xe6);

    @DexIgnore
    Object b(T t, Xe6<? super Cd6> xe6);
}
