package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Dh5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RadioButton r;
    @DexIgnore
    public /* final */ RadioButton s;
    @DexIgnore
    public /* final */ RadioButton t;
    @DexIgnore
    public /* final */ RadioButton u;
    @DexIgnore
    public /* final */ RadioGroup v;
    @DexIgnore
    public /* final */ ScrollView w;
    @DexIgnore
    public /* final */ RecyclerView x;
    @DexIgnore
    public /* final */ ConstraintLayout y;

    @DexIgnore
    public Dh5(Object obj, View view, int i, RTLImageView rTLImageView, RadioButton radioButton, RadioButton radioButton2, RadioButton radioButton3, RadioButton radioButton4, RadioGroup radioGroup, ScrollView scrollView, RecyclerView recyclerView, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = radioButton;
        this.s = radioButton2;
        this.t = radioButton3;
        this.u = radioButton4;
        this.v = radioGroup;
        this.w = scrollView;
        this.x = recyclerView;
        this.y = constraintLayout;
    }

    @DexIgnore
    @Deprecated
    public static Dh5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Dh5) ViewDataBinding.p(layoutInflater, 2131558864, viewGroup, z, obj);
    }

    @DexIgnore
    public static Dh5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
