package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum B5 {
    b(0),
    c(1),
    d(2),
    e(3);
    
    @DexIgnore
    public static /* final */ Z4 g; // = new Z4(null);

    @DexIgnore
    public B5(int i) {
    }
}
