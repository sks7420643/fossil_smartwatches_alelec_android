package com.fossil;

import com.portfolio.platform.helper.AnalyticsHelper;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wo4 implements Factory<AnalyticsHelper> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Wo4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Wo4 a(Uo4 uo4) {
        return new Wo4(uo4);
    }

    @DexIgnore
    public static AnalyticsHelper c(Uo4 uo4) {
        AnalyticsHelper c = uo4.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    public AnalyticsHelper b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
