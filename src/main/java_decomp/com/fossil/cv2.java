package com.fossil;

import com.fossil.E13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cv2 extends E13<Cv2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Cv2 zzg;
    @DexIgnore
    public static volatile Z23<Cv2> zzh;
    @DexIgnore
    public J13 zzc; // = E13.z();
    @DexIgnore
    public J13 zzd; // = E13.z();
    @DexIgnore
    public M13<Vu2> zze; // = E13.B();
    @DexIgnore
    public M13<Dv2> zzf; // = E13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Cv2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Cv2.zzg);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai B() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).f0();
            return this;
        }

        @DexIgnore
        public final Ai C(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).X(i);
            return this;
        }

        @DexIgnore
        public final Ai E(Iterable<? extends Long> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).O(iterable);
            return this;
        }

        @DexIgnore
        public final Ai G(Iterable<? extends Vu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).R(iterable);
            return this;
        }

        @DexIgnore
        public final Ai H(Iterable<? extends Dv2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).V(iterable);
            return this;
        }

        @DexIgnore
        public final Ai x() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).e0();
            return this;
        }

        @DexIgnore
        public final Ai y(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).T(i);
            return this;
        }

        @DexIgnore
        public final Ai z(Iterable<? extends Long> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Cv2) this.c).I(iterable);
            return this;
        }
    }

    /*
    static {
        Cv2 cv2 = new Cv2();
        zzg = cv2;
        E13.u(Cv2.class, cv2);
    }
    */

    @DexIgnore
    public static Ai b0() {
        return (Ai) zzg.w();
    }

    @DexIgnore
    public static Cv2 c0() {
        return zzg;
    }

    @DexIgnore
    public final Vu2 C(int i) {
        return this.zze.get(i);
    }

    @DexIgnore
    public final List<Long> D() {
        return this.zzc;
    }

    @DexIgnore
    public final void I(Iterable<? extends Long> iterable) {
        J13 j13 = this.zzc;
        if (!j13.zza()) {
            this.zzc = E13.p(j13);
        }
        Nz2.a(iterable, this.zzc);
    }

    @DexIgnore
    public final int J() {
        return this.zzc.size();
    }

    @DexIgnore
    public final Dv2 K(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final void O(Iterable<? extends Long> iterable) {
        J13 j13 = this.zzd;
        if (!j13.zza()) {
            this.zzd = E13.p(j13);
        }
        Nz2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final List<Long> P() {
        return this.zzd;
    }

    @DexIgnore
    public final void R(Iterable<? extends Vu2> iterable) {
        g0();
        Nz2.a(iterable, this.zze);
    }

    @DexIgnore
    public final int S() {
        return this.zzd.size();
    }

    @DexIgnore
    public final void T(int i) {
        g0();
        this.zze.remove(i);
    }

    @DexIgnore
    public final void V(Iterable<? extends Dv2> iterable) {
        h0();
        Nz2.a(iterable, this.zzf);
    }

    @DexIgnore
    public final List<Vu2> W() {
        return this.zze;
    }

    @DexIgnore
    public final void X(int i) {
        h0();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final int Y() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<Dv2> Z() {
        return this.zzf;
    }

    @DexIgnore
    public final int a0() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void e0() {
        this.zzc = E13.z();
    }

    @DexIgnore
    public final void f0() {
        this.zzd = E13.z();
    }

    @DexIgnore
    public final void g0() {
        M13<Vu2> m13 = this.zze;
        if (!m13.zza()) {
            this.zze = E13.q(m13);
        }
    }

    @DexIgnore
    public final void h0() {
        M13<Dv2> m13 = this.zzf;
        if (!m13.zza()) {
            this.zzf = E13.q(m13);
        }
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Cv2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzg, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zzc", "zzd", "zze", Vu2.class, "zzf", Dv2.class});
            case 4:
                return zzg;
            case 5:
                Z23<Cv2> z232 = zzh;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Cv2.class) {
                    try {
                        z23 = zzh;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzg);
                            zzh = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
