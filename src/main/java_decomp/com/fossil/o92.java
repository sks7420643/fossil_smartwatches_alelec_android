package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O92 extends BroadcastReceiver {
    @DexIgnore
    public Context a;
    @DexIgnore
    public /* final */ Q92 b;

    @DexIgnore
    public O92(Q92 q92) {
        this.b = q92;
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            if (this.a != null) {
                this.a.unregisterReceiver(this);
            }
            this.a = null;
        }
    }

    @DexIgnore
    public final void b(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.b.a();
            a();
        }
    }
}
