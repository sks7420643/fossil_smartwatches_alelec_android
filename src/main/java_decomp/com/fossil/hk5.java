package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hk5 extends Zf1 {
    @DexIgnore
    public static Bitmap d(Rd1 rd1, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        int min = Math.min(bitmap.getWidth(), bitmap.getHeight());
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, (bitmap.getWidth() - min) / 2, (bitmap.getHeight() - min) / 2, min, min);
        Bitmap c = rd1.c(min, min, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(c);
        Paint paint = new Paint();
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        paint.setShader(new BitmapShader(createBitmap, tileMode, tileMode));
        paint.setAntiAlias(true);
        float f = ((float) min) / 2.0f;
        canvas.drawCircle(f, f, f, paint);
        return c;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        messageDigest.update(Hk5.class.getName().getBytes(Mb1.a));
    }

    @DexIgnore
    @Override // com.fossil.Zf1
    public Bitmap c(Rd1 rd1, Bitmap bitmap, int i, int i2) {
        return d(rd1, bitmap);
    }
}
