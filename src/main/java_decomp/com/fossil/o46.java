package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O46 implements MembersInjector<NotificationContactsAndAppsAssignedPresenter> {
    @DexIgnore
    public static void a(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
        notificationContactsAndAppsAssignedPresenter.S();
    }
}
