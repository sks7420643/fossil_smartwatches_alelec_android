package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Do extends Lp {
    @DexIgnore
    public /* final */ Bu1 C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Do(K5 k5, I60 i60, Bu1 bu1, String str, int i) {
        super(k5, i60, Yp.J0, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = bu1;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        byte[] bArr;
        Kp1 b = this.C.b();
        if (b == null || b.d()) {
            I(this.C);
            return;
        }
        try {
            Xa xa = Xa.d;
            Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.k.b));
            if (ry1 == null) {
                ry1 = Hd0.y.d();
            }
            bArr = xa.a(1796, ry1, new Kp1[]{this.C.b()});
        } catch (Sx1 e) {
            bArr = new byte[0];
        }
        Lp.h(this, new Zj(this.w, this.x, Yp.K0, true, 1796, bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new Fn(this), new Rn(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void I(Bu1 bu1) {
        Lp.h(this, new Cn(this.w, this.x, bu1, Ob.i, null, 16), new Hm(this), new Tm(this), null, null, null, 56, null);
    }
}
