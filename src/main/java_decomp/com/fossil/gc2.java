package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gc2 {
    @DexIgnore
    public static int a; // = 129;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Gc2 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Uri f; // = new Uri.Builder().scheme("content").authority("com.google.android.gms.chimera").build();
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ComponentName c; // = null;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public Ai(String str, String str2, int i, boolean z) {
            Rc2.g(str);
            this.a = str;
            Rc2.g(str2);
            this.b = str2;
            this.d = i;
            this.e = z;
        }

        @DexIgnore
        public final ComponentName a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Intent c(Context context) {
            if (this.a == null) {
                return new Intent().setComponent(this.c);
            }
            Intent d2 = this.e ? d(context) : null;
            return d2 == null ? new Intent(this.a).setPackage(this.b) : d2;
        }

        @DexIgnore
        public final Intent d(Context context) {
            Bundle bundle;
            Intent intent = null;
            Bundle bundle2 = new Bundle();
            bundle2.putString("serviceActionBundleKey", this.a);
            try {
                bundle = context.getContentResolver().call(f, "serviceIntentCall", (String) null, bundle2);
            } catch (IllegalArgumentException e2) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                sb.append("Dynamic intent resolution failed: ");
                sb.append(valueOf);
                Log.w("ConnectionStatusConfig", sb.toString());
                bundle = null;
            }
            if (bundle != null) {
                intent = (Intent) bundle.getParcelable("serviceResponseIntentKey");
            }
            if (intent == null) {
                String valueOf2 = String.valueOf(this.a);
                Log.w("ConnectionStatusConfig", valueOf2.length() != 0 ? "Dynamic lookup for intent failed for action: ".concat(valueOf2) : new String("Dynamic lookup for intent failed for action: "));
            }
            return intent;
        }

        @DexIgnore
        public final int e() {
            return this.d;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            return Pc2.a(this.a, ai.a) && Pc2.a(this.b, ai.b) && Pc2.a(this.c, ai.c) && this.d == ai.d && this.e == ai.e;
        }

        @DexIgnore
        public final int hashCode() {
            return Pc2.b(this.a, this.b, this.c, Integer.valueOf(this.d), Boolean.valueOf(this.e));
        }

        @DexIgnore
        public final String toString() {
            String str = this.a;
            if (str != null) {
                return str;
            }
            Rc2.k(this.c);
            return this.c.flattenToString();
        }
    }

    @DexIgnore
    public static int a() {
        return a;
    }

    @DexIgnore
    public static Gc2 b(Context context) {
        synchronized (b) {
            if (c == null) {
                c = new He2(context.getApplicationContext());
            }
        }
        return c;
    }

    @DexIgnore
    public final void c(String str, String str2, int i, ServiceConnection serviceConnection, String str3, boolean z) {
        e(new Ai(str, str2, i, z), serviceConnection, str3);
    }

    @DexIgnore
    public abstract boolean d(Ai ai, ServiceConnection serviceConnection, String str);

    @DexIgnore
    public abstract void e(Ai ai, ServiceConnection serviceConnection, String str);
}
