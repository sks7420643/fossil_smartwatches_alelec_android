package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yj1 implements Mb1 {
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public Yj1(Object obj) {
        Ik1.d(obj);
        this.b = obj;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(Mb1.a));
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (obj instanceof Yj1) {
            return this.b.equals(((Yj1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
