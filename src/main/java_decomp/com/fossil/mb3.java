package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mb3 extends Gr2 implements Lb3 {
    @DexIgnore
    public Mb3() {
        super("com.google.android.gms.location.ILocationListener");
    }

    @DexIgnore
    public static Lb3 e(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationListener");
        return queryLocalInterface instanceof Lb3 ? (Lb3) queryLocalInterface : new Nb3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.Gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onLocationChanged((Location) Qr2.a(parcel, Location.CREATOR));
        return true;
    }
}
