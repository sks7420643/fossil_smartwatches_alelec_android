package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G37<T> {
    @DexIgnore
    public T a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends FragmentManager.f {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;
        @DexIgnore
        public /* final */ /* synthetic */ FragmentManager b;

        @DexIgnore
        public Ai(Fragment fragment, FragmentManager fragmentManager) {
            this.a = fragment;
            this.b = fragmentManager;
        }

        @DexIgnore
        @Override // androidx.fragment.app.FragmentManager.f
        public void n(FragmentManager fragmentManager, Fragment fragment) {
            if (Objects.equals(fragment, this.a)) {
                G37.this.a = null;
                this.b.i1(this);
            }
        }
    }

    @DexIgnore
    public G37(Fragment fragment, T t) {
        FragmentManager fragmentManager = fragment.getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.O0(new Ai(fragment, fragmentManager), false);
        }
        this.a = t;
    }

    @DexIgnore
    public T a() {
        return this.a;
    }
}
