package com.fossil;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sn0 {
    @DexIgnore
    public static /* final */ String TAG; // = "ActionProvider(support)";
    @DexIgnore
    public /* final */ Context mContext;
    @DexIgnore
    public Ai mSubUiVisibilityListener;
    @DexIgnore
    public Bi mVisibilityListener;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void onActionProviderVisibilityChanged(boolean z);
    }

    @DexIgnore
    public Sn0(Context context) {
        this.mContext = context;
    }

    @DexIgnore
    public Context getContext() {
        return this.mContext;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return false;
    }

    @DexIgnore
    public boolean isVisible() {
        return true;
    }

    @DexIgnore
    public abstract View onCreateActionView();

    @DexIgnore
    public View onCreateActionView(MenuItem menuItem) {
        return onCreateActionView();
    }

    @DexIgnore
    public boolean onPerformDefaultAction() {
        return false;
    }

    @DexIgnore
    public void onPrepareSubMenu(SubMenu subMenu) {
    }

    @DexIgnore
    public boolean overridesItemVisibility() {
        return false;
    }

    @DexIgnore
    public void refreshVisibility() {
        if (this.mVisibilityListener != null && overridesItemVisibility()) {
            this.mVisibilityListener.onActionProviderVisibilityChanged(isVisible());
        }
    }

    @DexIgnore
    public void reset() {
        this.mVisibilityListener = null;
        this.mSubUiVisibilityListener = null;
    }

    @DexIgnore
    public void setSubUiVisibilityListener(Ai ai) {
        this.mSubUiVisibilityListener = ai;
    }

    @DexIgnore
    public void setVisibilityListener(Bi bi) {
        if (!(this.mVisibilityListener == null || bi == null)) {
            Log.w(TAG, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.mVisibilityListener = bi;
    }

    @DexIgnore
    public void subUiVisibilityChanged(boolean z) {
        Ai ai = this.mSubUiVisibilityListener;
        if (ai != null) {
            ai.a(z);
        }
    }
}
