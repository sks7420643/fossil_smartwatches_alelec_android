package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ag2 {
    @DexIgnore
    public static Ag2 b; // = new Ag2();
    @DexIgnore
    public Zf2 a; // = null;

    @DexIgnore
    public static Zf2 a(Context context) {
        return b.b(context);
    }

    @DexIgnore
    public final Zf2 b(Context context) {
        Zf2 zf2;
        synchronized (this) {
            if (this.a == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                this.a = new Zf2(context);
            }
            zf2 = this.a;
        }
        return zf2;
    }
}
