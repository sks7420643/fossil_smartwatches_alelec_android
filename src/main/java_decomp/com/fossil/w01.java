package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class W01 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends W01 {
        @DexIgnore
        @Override // com.fossil.W01
        public V01 a(String str) {
            return null;
        }
    }

    @DexIgnore
    public static W01 c() {
        return new Ai();
    }

    @DexIgnore
    public abstract V01 a(String str);

    @DexIgnore
    public final V01 b(String str) {
        V01 a2 = a(str);
        return a2 == null ? V01.a(str) : a2;
    }
}
