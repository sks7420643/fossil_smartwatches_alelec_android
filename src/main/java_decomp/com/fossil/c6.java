package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C6 extends U5 {
    @DexIgnore
    public /* final */ N5 k; // = N5.c;
    @DexIgnore
    public B5 l; // = B5.b;

    @DexIgnore
    public C6(N4 n4) {
        super(V5.n, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        int i = H5.d[k5.D().ordinal()];
        if (i == 1) {
            G7 g7 = new G7(F7.b, 0, 2);
            B5 b5 = B5.b;
            k5.b.post(new Q4(k5, g7, b5, b5));
        } else if (i == 2 || i == 3) {
            G7 a2 = G7.d.a(L80.d.g(k5.A));
            if (a2.b != F7.b) {
                k5.b.post(new Q4(k5, a2, B5.d, B5.b));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void f(H7 h7) {
        S5 a2;
        k(h7);
        G7 g7 = h7.a;
        if (g7.b == F7.b) {
            a2 = this.l == B5.b ? S5.a(this.e, null, R5.b, null, 5) : S5.a(this.e, null, R5.e, null, 5);
        } else {
            S5 a3 = S5.e.a(g7);
            a2 = S5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public N5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return (h7 instanceof Y6) && ((Y6) h7).b == B5.b;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.l;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.l = ((Y6) h7).b;
    }
}
