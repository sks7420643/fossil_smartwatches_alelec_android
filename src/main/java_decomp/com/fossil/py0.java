package com.fossil;

import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Py0 {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public Runnable b;

    @DexIgnore
    public static Py0 b(ViewGroup viewGroup) {
        return (Py0) viewGroup.getTag(Ny0.transition_current_scene);
    }

    @DexIgnore
    public static void c(ViewGroup viewGroup, Py0 py0) {
        viewGroup.setTag(Ny0.transition_current_scene, py0);
    }

    @DexIgnore
    public void a() {
        Runnable runnable;
        if (b(this.a) == this && (runnable = this.b) != null) {
            runnable.run();
        }
    }
}
