package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ka3 extends Zc2 implements Z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ka3> CREATOR; // = new Va3();
    @DexIgnore
    public /* final */ Status b;
    @DexIgnore
    public /* final */ La3 c;

    @DexIgnore
    public Ka3(Status status) {
        this(status, null);
    }

    @DexIgnore
    public Ka3(Status status, La3 la3) {
        this.b = status;
        this.c = la3;
    }

    @DexIgnore
    @Override // com.fossil.Z62
    public final Status a() {
        return this.b;
    }

    @DexIgnore
    public final La3 c() {
        return this.c;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, a(), i, false);
        Bd2.t(parcel, 2, c(), i, false);
        Bd2.b(parcel, a2);
    }
}
