package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jz3 {
    @DexIgnore
    public static /* final */ int[] a; // = {Jw3.colorPrimary};
    @DexIgnore
    public static /* final */ int[] b; // = {Jw3.colorPrimaryVariant};
    @DexIgnore
    public static /* final */ int[] c; // = {16842752, Jw3.theme};
    @DexIgnore
    public static /* final */ int[] d; // = {Jw3.materialThemeOverlay};

    @DexIgnore
    public static void a(Context context) {
        e(context, a, "Theme.AppCompat");
    }

    @DexIgnore
    public static void b(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, Tw3.ThemeEnforcement, i, i2);
        boolean z = obtainStyledAttributes.getBoolean(Tw3.ThemeEnforcement_enforceMaterialTheme, false);
        obtainStyledAttributes.recycle();
        if (z) {
            TypedValue typedValue = new TypedValue();
            if (!context.getTheme().resolveAttribute(Jw3.isMaterialTheme, typedValue, true) || (typedValue.type == 18 && typedValue.data == 0)) {
                c(context);
            }
        }
        a(context);
    }

    @DexIgnore
    public static void c(Context context) {
        e(context, b, "Theme.MaterialComponents");
    }

    @DexIgnore
    public static void d(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2, int... iArr2) {
        boolean z = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, Tw3.ThemeEnforcement, i, i2);
        if (!obtainStyledAttributes.getBoolean(Tw3.ThemeEnforcement_enforceTextAppearance, false)) {
            obtainStyledAttributes.recycle();
            return;
        }
        if (iArr2 != null && iArr2.length != 0) {
            z = g(context, attributeSet, iArr, i, i2, iArr2);
        } else if (obtainStyledAttributes.getResourceId(Tw3.ThemeEnforcement_android_textAppearance, -1) != -1) {
            z = true;
        }
        obtainStyledAttributes.recycle();
        if (!z) {
            throw new IllegalArgumentException("This component requires that you specify a valid TextAppearance attribute. Update your app theme to inherit from Theme.MaterialComponents (or a descendant).");
        }
    }

    @DexIgnore
    public static void e(Context context, int[] iArr, String str) {
        if (!h(context, iArr)) {
            throw new IllegalArgumentException("The style on this component requires your app theme to be " + str + " (or a descendant).");
        }
    }

    @DexIgnore
    public static Context f(Context context, AttributeSet attributeSet, int i, int i2) {
        int j = j(context, attributeSet, i, i2);
        if (j == 0) {
            return context;
        }
        if ((context instanceof Qf0) && ((Qf0) context).c() == j) {
            return context;
        }
        Qf0 qf0 = new Qf0(context, j);
        int i3 = i(qf0, attributeSet);
        return i3 != 0 ? new Qf0(qf0, i3) : qf0;
    }

    @DexIgnore
    public static boolean g(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2, int... iArr2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr, i, i2);
        for (int i3 : iArr2) {
            if (obtainStyledAttributes.getResourceId(i3, -1) == -1) {
                obtainStyledAttributes.recycle();
                return false;
            }
        }
        obtainStyledAttributes.recycle();
        return true;
    }

    @DexIgnore
    public static boolean h(Context context, int[] iArr) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(iArr);
        for (int i = 0; i < iArr.length; i++) {
            if (!obtainStyledAttributes.hasValue(i)) {
                obtainStyledAttributes.recycle();
                return false;
            }
        }
        obtainStyledAttributes.recycle();
        return true;
    }

    @DexIgnore
    public static int i(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, c);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(1, 0);
        obtainStyledAttributes.recycle();
        return resourceId != 0 ? resourceId : resourceId2;
    }

    @DexIgnore
    public static int j(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d, i, i2);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        return resourceId;
    }

    @DexIgnore
    public static TypedArray k(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2, int... iArr2) {
        b(context, attributeSet, i, i2);
        d(context, attributeSet, iArr, i, i2, iArr2);
        return context.obtainStyledAttributes(attributeSet, iArr, i, i2);
    }

    @DexIgnore
    public static Th0 l(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2, int... iArr2) {
        b(context, attributeSet, i, i2);
        d(context, attributeSet, iArr, i, i2, iArr2);
        return Th0.v(context, attributeSet, iArr, i, i2);
    }
}
