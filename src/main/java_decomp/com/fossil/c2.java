package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class C2 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ B2 CREATOR; // = new B2(null);
    @DexIgnore
    public /* final */ Lt b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C2(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = r4.readString()
            if (r0 == 0) goto L_0x0020
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            com.fossil.Lt r1 = com.fossil.Lt.valueOf(r0)
            byte r2 = r4.readByte()
            int r0 = r4.readInt()
            if (r0 == 0) goto L_0x001e
            r0 = 1
        L_0x001a:
            r3.<init>(r1, r2, r0)
            return
        L_0x001e:
            r0 = 0
            goto L_0x001a
        L_0x0020:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.C2.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public C2(Lt lt, byte b2, boolean z) {
        this.d = true;
        this.b = lt;
        this.c = (byte) b2;
        this.d = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C2(Lt lt, byte b2, boolean z, int i) {
        this(lt, b2, (i & 4) != 0 ? true : z);
    }

    @DexIgnore
    public byte[] a() {
        return new byte[0];
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.H, Ey1.a(this.b)), Jd0.z1, Short.valueOf(Hy1.p(this.c)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
