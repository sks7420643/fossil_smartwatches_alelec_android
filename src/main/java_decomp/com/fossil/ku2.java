package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ku2 extends E13<Ku2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Ku2 zzj;
    @DexIgnore
    public static volatile Z23<Ku2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public Iu2 zzf;
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;
    @DexIgnore
    public boolean zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Ku2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Ku2.zzj);
        }

        @DexIgnore
        public /* synthetic */ Ai(Fu2 fu2) {
            this();
        }

        @DexIgnore
        public final Ai x(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Ku2) this.c).D(str);
            return this;
        }
    }

    /*
    static {
        Ku2 ku2 = new Ku2();
        zzj = ku2;
        E13.u(Ku2.class, ku2);
    }
    */

    @DexIgnore
    public static Ai N() {
        return (Ai) zzj.w();
    }

    @DexIgnore
    public final void D(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int G() {
        return this.zzd;
    }

    @DexIgnore
    public final String H() {
        return this.zze;
    }

    @DexIgnore
    public final Iu2 I() {
        Iu2 iu2 = this.zzf;
        return iu2 == null ? Iu2.N() : iu2;
    }

    @DexIgnore
    public final boolean J() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean K() {
        return this.zzh;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final boolean M() {
        return this.zzi;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Fu2.a[i - 1]) {
            case 1:
                return new Ku2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1008\u0001\u0003\u1009\u0002\u0004\u1007\u0003\u0005\u1007\u0004\u0006\u1007\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                Z23<Ku2> z232 = zzk;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Ku2.class) {
                    try {
                        z23 = zzk;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzj);
                            zzk = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
