package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xg1 implements Id1<byte[]> {
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Xg1(byte[] bArr) {
        Ik1.d(bArr);
        this.b = bArr;
    }

    @DexIgnore
    public byte[] a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public int c() {
        return this.b.length;
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Class<byte[]> d() {
        return byte[].class;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Id1
    public /* bridge */ /* synthetic */ byte[] get() {
        return a();
    }
}
