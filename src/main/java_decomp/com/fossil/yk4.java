package com.fossil;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yk4 {
    @DexIgnore
    public Ai<String, Pattern> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<K, V> {
        @DexIgnore
        public LinkedHashMap<K, V> a;
        @DexIgnore
        public int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends LinkedHashMap<K, V> {
            @DexIgnore
            public Aii(int i, float f, boolean z) {
                super(i, f, z);
            }

            @DexIgnore
            @Override // java.util.LinkedHashMap
            public boolean removeEldestEntry(Map.Entry<K, V> entry) {
                return size() > Ai.this.b;
            }
        }

        @DexIgnore
        public Ai(int i) {
            this.b = i;
            this.a = new Aii(((i * 4) / 3) + 1, 0.75f, true);
        }

        @DexIgnore
        public V b(K k) {
            V v;
            synchronized (this) {
                v = this.a.get(k);
            }
            return v;
        }

        @DexIgnore
        public void c(K k, V v) {
            synchronized (this) {
                this.a.put(k, v);
            }
        }
    }

    @DexIgnore
    public Yk4(int i) {
        this.a = new Ai<>(i);
    }

    @DexIgnore
    public Pattern a(String str) {
        Pattern b = this.a.b(str);
        if (b != null) {
            return b;
        }
        Pattern compile = Pattern.compile(str);
        this.a.c(str, compile);
        return compile;
    }
}
