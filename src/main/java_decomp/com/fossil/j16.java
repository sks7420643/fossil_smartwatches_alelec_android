package com.fossil;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface J16 extends Gq4<I16> {
    @DexIgnore
    void T(Cursor cursor);

    @DexIgnore
    Object V();  // void declaration

    @DexIgnore
    Object close();  // void declaration

    @DexIgnore
    void f6(boolean z);

    @DexIgnore
    void j5(List<J06> list, FilterQueryProvider filterQueryProvider);
}
