package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.mapped.Kc6;
import com.mapped.Lc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gk5 {
    @DexIgnore
    public static /* final */ Gk5 a; // = new Gk5();

    @DexIgnore
    public final int a() {
        return 30;
    }

    @DexIgnore
    public final int b(int i, int i2, int i3, Qh5 qh5) {
        double d;
        Wg6.c(qh5, "gender");
        double d2 = i < 18 ? 0.0d : (18 <= i && 30 >= i) ? 21.5650154d : (30 <= i && 50 >= i) ? 25.291792d : 33.3115727d;
        int i4 = Fk5.b[qh5.ordinal()];
        if (i4 == 1) {
            d = -171.5010333d;
        } else if (i4 == 2) {
            d = -159.3516885d;
        } else if (i4 == 3) {
            d = -64.4165128d;
        } else {
            throw new Kc6();
        }
        return Lr7.a(d2 + 147.7007883d + (((double) i2) * 2.3639882d) + (((double) i3) * 0.2297702d) + d);
    }

    @DexIgnore
    public final Lc6<Integer, Integer> c(Qh5 qh5, int i) {
        int i2;
        int i3 = 65;
        Wg6.c(qh5, "gender");
        int i4 = Fk5.a[qh5.ordinal()];
        int i5 = 60;
        if (i4 != 1) {
            if (i4 == 2) {
                i2 = 175;
                if (i >= 18) {
                    i3 = i < 35 ? 70 : 80;
                }
                i5 = i3;
            } else if (i4 != 3) {
                throw new Kc6();
            } else if (i < 18) {
                i2 = 170;
            } else if (i < 35) {
                i2 = 170;
                i5 = 65;
            } else {
                i2 = 170;
                i5 = 75;
            }
        } else if (i < 18) {
            i2 = 165;
            i5 = 55;
        } else if (i < 35) {
            i2 = 165;
        } else {
            i2 = 165;
            i5 = 70;
        }
        return new Lc6<>(Integer.valueOf(i2), Integer.valueOf(i5));
    }

    @DexIgnore
    public final int d(int i) {
        if (i < 18) {
            return 540;
        }
        return (18 <= i && 65 >= i) ? 480 : 450;
    }

    @DexIgnore
    public final int e() {
        return VideoUploader.RETRY_DELAY_UNIT_MS;
    }
}
