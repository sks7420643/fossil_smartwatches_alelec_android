package com.fossil;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Td7 {
    @DexIgnore
    public /* final */ HandlerThread a;
    @DexIgnore
    public /* final */ Yc7 b;
    @DexIgnore
    public /* final */ Handler c; // = new Ai(this.a.getLooper(), this);
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;
    @DexIgnore
    public long f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public long j;
    @DexIgnore
    public long k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Handler {
        @DexIgnore
        public /* final */ Td7 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message b;

            @DexIgnore
            public Aii(Ai ai, Message message) {
                this.b = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unhandled stats message." + this.b.what);
            }
        }

        @DexIgnore
        public Ai(Looper looper, Td7 td7) {
            super(looper);
            this.a = td7;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                this.a.j();
            } else if (i == 1) {
                this.a.k();
            } else if (i == 2) {
                this.a.h((long) message.arg1);
            } else if (i == 3) {
                this.a.i((long) message.arg1);
            } else if (i != 4) {
                Picasso.p.post(new Aii(this, message));
            } else {
                this.a.l((Long) message.obj);
            }
        }
    }

    @DexIgnore
    public Td7(Yc7 yc7) {
        this.b = yc7;
        HandlerThread handlerThread = new HandlerThread("Picasso-Stats", 10);
        this.a = handlerThread;
        handlerThread.start();
        Xd7.j(this.a.getLooper());
    }

    @DexIgnore
    public static long g(int i2, long j2) {
        return j2 / ((long) i2);
    }

    @DexIgnore
    public Ud7 a() {
        return new Ud7(this.b.a(), this.b.size(), this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, System.currentTimeMillis());
    }

    @DexIgnore
    public void b(Bitmap bitmap) {
        m(bitmap, 2);
    }

    @DexIgnore
    public void c(Bitmap bitmap) {
        m(bitmap, 3);
    }

    @DexIgnore
    public void d() {
        this.c.sendEmptyMessage(0);
    }

    @DexIgnore
    public void e() {
        this.c.sendEmptyMessage(1);
    }

    @DexIgnore
    public void f(long j2) {
        Handler handler = this.c;
        handler.sendMessage(handler.obtainMessage(4, Long.valueOf(j2)));
    }

    @DexIgnore
    public void h(long j2) {
        int i2 = this.m + 1;
        this.m = i2;
        long j3 = this.g + j2;
        this.g = j3;
        this.j = g(i2, j3);
    }

    @DexIgnore
    public void i(long j2) {
        this.n++;
        long j3 = this.h + j2;
        this.h = j3;
        this.k = g(this.m, j3);
    }

    @DexIgnore
    public void j() {
        this.d++;
    }

    @DexIgnore
    public void k() {
        this.e++;
    }

    @DexIgnore
    public void l(Long l2) {
        this.l++;
        long longValue = this.f + l2.longValue();
        this.f = longValue;
        this.i = g(this.l, longValue);
    }

    @DexIgnore
    public final void m(Bitmap bitmap, int i2) {
        int k2 = Xd7.k(bitmap);
        Handler handler = this.c;
        handler.sendMessage(handler.obtainMessage(i2, k2, 0));
    }
}
