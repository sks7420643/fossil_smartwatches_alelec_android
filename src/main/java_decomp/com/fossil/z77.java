package com.fossil;

import com.portfolio.platform.watchface.edit.WatchFaceEditFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z77 implements MembersInjector<WatchFaceEditFragment> {
    @DexIgnore
    public static void a(WatchFaceEditFragment watchFaceEditFragment, Po4 po4) {
        watchFaceEditFragment.h = po4;
    }
}
