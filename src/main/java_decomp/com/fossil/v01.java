package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V01 {
    @DexIgnore
    public static /* final */ String a; // = X01.f("InputMerger");

    @DexIgnore
    public static V01 a(String str) {
        try {
            return (V01) Class.forName(str).newInstance();
        } catch (Exception e) {
            X01 c = X01.c();
            String str2 = a;
            c.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    public abstract R01 b(List<R01> list);
}
