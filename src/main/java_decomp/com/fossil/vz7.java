package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vz7 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public Vz7(String str) {
        this.a = str;
    }

    @DexIgnore
    public String toString() {
        return this.a;
    }
}
