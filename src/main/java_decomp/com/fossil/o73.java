package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O73 implements Xw2<N73> {
    @DexIgnore
    public static O73 c; // = new O73();
    @DexIgnore
    public /* final */ Xw2<N73> b;

    @DexIgnore
    public O73() {
        this(Ww2.b(new Q73()));
    }

    @DexIgnore
    public O73(Xw2<N73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((N73) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ N73 zza() {
        return this.b.zza();
    }
}
