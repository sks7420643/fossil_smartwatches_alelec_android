package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V72 extends BasePendingResult<Status> {
    @DexIgnore
    public V72(R62 r62) {
        super(r62);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Z62' to match base method */
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public /* synthetic */ Status f(Status status) {
        return status;
    }
}
