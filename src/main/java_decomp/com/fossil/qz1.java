package com.fossil;

import android.util.SparseArray;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Qz1 extends Enum<Qz1> {
    @DexIgnore
    public static /* final */ SparseArray<Qz1> b;
    @DexIgnore
    public static /* final */ Qz1 zza; // = new Qz1(MessengerShareContentUtility.PREVIEW_DEFAULT, 0, 0);
    @DexIgnore
    public static /* final */ Qz1 zzb; // = new Qz1("UNMETERED_ONLY", 1, 1);
    @DexIgnore
    public static /* final */ Qz1 zzc; // = new Qz1("UNMETERED_OR_DAILY", 2, 2);
    @DexIgnore
    public static /* final */ Qz1 zzd; // = new Qz1("FAST_IF_RADIO_AWAKE", 3, 3);
    @DexIgnore
    public static /* final */ Qz1 zze; // = new Qz1("NEVER", 4, 4);
    @DexIgnore
    public static /* final */ Qz1 zzf; // = new Qz1("UNRECOGNIZED", 5, -1);

    /*
    static {
        SparseArray<Qz1> sparseArray = new SparseArray<>();
        b = sparseArray;
        sparseArray.put(0, zza);
        b.put(1, zzb);
        b.put(2, zzc);
        b.put(3, zzd);
        b.put(4, zze);
        b.put(-1, zzf);
    }
    */

    @DexIgnore
    public Qz1(String str, int i, int i2) {
    }
}
