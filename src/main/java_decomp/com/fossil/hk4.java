package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import com.mapped.Fu3;
import com.mapped.Ku3;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hk4 extends JsonWriter {
    @DexIgnore
    public static /* final */ Writer u; // = new Ai();
    @DexIgnore
    public static /* final */ Jj4 v; // = new Jj4("closed");
    @DexIgnore
    public /* final */ List<JsonElement> m; // = new ArrayList();
    @DexIgnore
    public String s;
    @DexIgnore
    public JsonElement t; // = Fj4.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Writer {
        @DexIgnore
        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Writer, java.io.Flushable
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public Hk4() {
        super(u);
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter A(String str) throws IOException {
        if (this.m.isEmpty() || this.s != null) {
            throw new IllegalStateException();
        } else if (q0() instanceof Ku3) {
            this.s = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter C() throws IOException {
        r0(Fj4.a);
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter V(long j) throws IOException {
        r0(new Jj4((Number) Long.valueOf(j)));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter X(Boolean bool) throws IOException {
        if (bool == null) {
            C();
        } else {
            r0(new Jj4(bool));
        }
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter b0(Number number) throws IOException {
        if (number == null) {
            C();
        } else {
            if (!o()) {
                double doubleValue = number.doubleValue();
                if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                    throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
                }
            }
            r0(new Jj4(number));
        }
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter c() throws IOException {
        Fu3 fu3 = new Fu3();
        r0(fu3);
        this.m.add(fu3);
        return this;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.google.gson.stream.JsonWriter, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.m.isEmpty()) {
            this.m.add(v);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter f() throws IOException {
        Ku3 ku3 = new Ku3();
        r0(ku3);
        this.m.add(ku3);
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter, java.io.Flushable
    public void flush() throws IOException {
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter g0(String str) throws IOException {
        if (str == null) {
            C();
        } else {
            r0(new Jj4(str));
        }
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter i0(boolean z) throws IOException {
        r0(new Jj4(Boolean.valueOf(z)));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter j() throws IOException {
        if (this.m.isEmpty() || this.s != null) {
            throw new IllegalStateException();
        } else if (q0() instanceof Fu3) {
            List<JsonElement> list = this.m;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter k() throws IOException {
        if (this.m.isEmpty() || this.s != null) {
            throw new IllegalStateException();
        } else if (q0() instanceof Ku3) {
            List<JsonElement> list = this.m;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonElement p0() {
        if (this.m.isEmpty()) {
            return this.t;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.m);
    }

    @DexIgnore
    public final JsonElement q0() {
        List<JsonElement> list = this.m;
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public final void r0(JsonElement jsonElement) {
        if (this.s != null) {
            if (!jsonElement.h() || l()) {
                ((Ku3) q0()).k(this.s, jsonElement);
            }
            this.s = null;
        } else if (this.m.isEmpty()) {
            this.t = jsonElement;
        } else {
            JsonElement q0 = q0();
            if (q0 instanceof Fu3) {
                ((Fu3) q0).k(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }
}
