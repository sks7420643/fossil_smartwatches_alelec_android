package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A36 implements MembersInjector<Z26> {
    @DexIgnore
    public static void a(Z26 z26, InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
        z26.k = inactivityNudgeTimePresenter;
    }

    @DexIgnore
    public static void b(Z26 z26, RemindTimePresenter remindTimePresenter) {
        z26.l = remindTimePresenter;
    }
}
