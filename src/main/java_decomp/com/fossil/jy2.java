package com.fossil;

import java.util.Collection;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Jy2<K, V> {
    @DexIgnore
    Map<K, Collection<V>> zza();
}
