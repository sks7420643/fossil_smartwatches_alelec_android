package com.fossil;

import android.os.Looper;
import com.fossil.Yb2;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J82 implements Yb2.Ci {
    @DexIgnore
    public /* final */ WeakReference<H82> a;
    @DexIgnore
    public /* final */ M62<?> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public J82(H82 h82, M62<?> m62, boolean z) {
        this.a = new WeakReference<>(h82);
        this.b = m62;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.Yb2.Ci
    public final void a(Z52 z52) {
        boolean z = false;
        H82 h82 = this.a.get();
        if (h82 != null) {
            if (Looper.myLooper() == h82.a.t.m()) {
                z = true;
            }
            Rc2.o(z, "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            h82.b.lock();
            try {
                if (h82.z(0)) {
                    if (!z52.A()) {
                        h82.v(z52, this.b, this.c);
                    }
                    if (h82.o()) {
                        h82.p();
                    }
                    h82.b.unlock();
                }
            } finally {
                h82.b.unlock();
            }
        }
    }
}
