package com.fossil;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import com.fossil.Hx3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ix3 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT < 18);

    @DexIgnore
    public static void a(Hx3 hx3, View view, FrameLayout frameLayout) {
        e(hx3, view, frameLayout);
        if (a) {
            frameLayout.setForeground(hx3);
        } else {
            view.getOverlay().add(hx3);
        }
    }

    @DexIgnore
    public static SparseArray<Hx3> b(Context context, Fz3 fz3) {
        SparseArray<Hx3> sparseArray = new SparseArray<>(fz3.size());
        for (int i = 0; i < fz3.size(); i++) {
            int keyAt = fz3.keyAt(i);
            Hx3.Ai ai = (Hx3.Ai) fz3.valueAt(i);
            if (ai != null) {
                sparseArray.put(keyAt, Hx3.e(context, ai));
            } else {
                throw new IllegalArgumentException("BadgeDrawable's savedState cannot be null");
            }
        }
        return sparseArray;
    }

    @DexIgnore
    public static Fz3 c(SparseArray<Hx3> sparseArray) {
        Fz3 fz3 = new Fz3();
        for (int i = 0; i < sparseArray.size(); i++) {
            int keyAt = sparseArray.keyAt(i);
            Hx3 valueAt = sparseArray.valueAt(i);
            if (valueAt != null) {
                fz3.put(keyAt, valueAt.k());
            } else {
                throw new IllegalArgumentException("badgeDrawable cannot be null");
            }
        }
        return fz3;
    }

    @DexIgnore
    public static void d(Hx3 hx3, View view, FrameLayout frameLayout) {
        if (hx3 != null) {
            if (a) {
                frameLayout.setForeground(null);
            } else {
                view.getOverlay().remove(hx3);
            }
        }
    }

    @DexIgnore
    public static void e(Hx3 hx3, View view, FrameLayout frameLayout) {
        Rect rect = new Rect();
        (a ? frameLayout : view).getDrawingRect(rect);
        hx3.setBounds(rect);
        hx3.w(view, frameLayout);
    }

    @DexIgnore
    public static void f(Rect rect, float f, float f2, float f3, float f4) {
        rect.set((int) (f - f3), (int) (f2 - f4), (int) (f + f3), (int) (f2 + f4));
    }
}
