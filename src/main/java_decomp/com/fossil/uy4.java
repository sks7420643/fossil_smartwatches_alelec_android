package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.List;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uy4 {
    @DexIgnore
    public static /* final */ Uy4 c; // = d.a(Hm7.h(-1739917, -1023342, -4560696, -6982195, -8812853, -10177034, -11549705, -11677471, -11684180, -8271996, -5319295, -30107, -2825897, -10929, -18611, -6190977, -7297874));
    @DexIgnore
    public static /* final */ Ai d;
    @DexIgnore
    public /* final */ Random a;
    @DexIgnore
    public /* final */ List<Integer> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Uy4 a(List<Integer> list) {
            Wg6.c(list, "colorList");
            return new Uy4(list, null);
        }

        @DexIgnore
        public final Uy4 b() {
            return Uy4.c;
        }
    }

    /*
    static {
        Ai ai = new Ai(null);
        d = ai;
        ai.a(Hm7.h(-957596, -686759, -416706, -1784274, -9977996, -10902850, -14642227, -5414233, -8366207));
    }
    */

    @DexIgnore
    public Uy4(List<Integer> list) {
        this.b = list;
        this.a = new Random(System.currentTimeMillis());
    }

    @DexIgnore
    public /* synthetic */ Uy4(List list, Qg6 qg6) {
        this(list);
    }

    @DexIgnore
    public final int b(Object obj) {
        Wg6.c(obj, "key");
        return this.b.get(Math.abs(obj.hashCode()) % this.b.size()).intValue();
    }

    @DexIgnore
    public final int c() {
        List<Integer> list = this.b;
        return list.get(this.a.nextInt(list.size())).intValue();
    }
}
