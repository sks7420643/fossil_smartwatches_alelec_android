package com.fossil;

import java.util.Collection;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Cx2<K, V> implements Jy2<K, V> {
    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Jy2) {
            return zza().equals(((Jy2) obj).zza());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return zza().hashCode();
    }

    @DexIgnore
    public String toString() {
        return zza().toString();
    }

    @DexIgnore
    @Override // com.fossil.Jy2
    public abstract Map<K, Collection<V>> zza();

    @DexIgnore
    public boolean zza(@NullableDecl Object obj) {
        for (Collection<V> collection : zza().values()) {
            if (collection.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Map<K, Collection<V>> zzb();
}
