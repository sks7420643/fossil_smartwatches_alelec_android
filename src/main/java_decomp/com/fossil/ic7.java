package com.fossil;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import com.mapped.Wg6;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ic7 extends I11 {
    @DexIgnore
    public /* final */ Map<Class<? extends ListenableWorker>, Provider<Jc7<? extends ListenableWorker>>> b;

    @DexIgnore
    public Ic7(Map<Class<? extends ListenableWorker>, Provider<Jc7<? extends ListenableWorker>>> map) {
        Wg6.c(map, "workerFactoryMap");
        this.b = map;
    }

    @DexIgnore
    @Override // com.fossil.I11
    public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
        T t;
        Provider provider;
        Wg6.c(context, "appContext");
        Wg6.c(str, "workerClassName");
        Wg6.c(workerParameters, "workerParameters");
        Iterator<T> it = this.b.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Class.forName(str).isAssignableFrom((Class) next.getKey())) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null && (provider = (Provider) t2.getValue()) != null) {
            return ((Jc7) provider.get()).a(context, workerParameters);
        }
        throw new IllegalArgumentException("could not find worker: " + str);
    }
}
