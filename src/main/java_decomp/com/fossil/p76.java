package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P76 implements MembersInjector<CommuteTimeSettingsActivity> {
    @DexIgnore
    public static void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity, CommuteTimeSettingsPresenter commuteTimeSettingsPresenter) {
        commuteTimeSettingsActivity.A = commuteTimeSettingsPresenter;
    }
}
