package com.fossil;

import android.graphics.Bitmap;
import com.fossil.Gg1;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sg1 implements Qb1<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ Gg1 a;
    @DexIgnore
    public /* final */ Od1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Gg1.Bi {
        @DexIgnore
        public /* final */ Qg1 a;
        @DexIgnore
        public /* final */ Ck1 b;

        @DexIgnore
        public Ai(Qg1 qg1, Ck1 ck1) {
            this.a = qg1;
            this.b = ck1;
        }

        @DexIgnore
        @Override // com.fossil.Gg1.Bi
        public void a(Rd1 rd1, Bitmap bitmap) throws IOException {
            IOException a2 = this.b.a();
            if (a2 != null) {
                if (bitmap != null) {
                    rd1.b(bitmap);
                }
                throw a2;
            }
        }

        @DexIgnore
        @Override // com.fossil.Gg1.Bi
        public void b() {
            this.a.b();
        }
    }

    @DexIgnore
    public Sg1(Gg1 gg1, Od1 od1) {
        this.a = gg1;
        this.b = od1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(InputStream inputStream, Ob1 ob1) throws IOException {
        return d(inputStream, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Bitmap> b(InputStream inputStream, int i, int i2, Ob1 ob1) throws IOException {
        return c(inputStream, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Bitmap> c(InputStream inputStream, int i, int i2, Ob1 ob1) throws IOException {
        boolean z;
        Qg1 qg1;
        if (inputStream instanceof Qg1) {
            qg1 = (Qg1) inputStream;
            z = false;
        } else {
            z = true;
            qg1 = new Qg1(inputStream, this.b);
        }
        Ck1 b2 = Ck1.b(qg1);
        try {
            return this.a.g(new Gk1(b2), i, i2, ob1, new Ai(qg1, b2));
        } finally {
            b2.c();
            if (z) {
                qg1.c();
            }
        }
    }

    @DexIgnore
    public boolean d(InputStream inputStream, Ob1 ob1) {
        return this.a.p(inputStream);
    }
}
