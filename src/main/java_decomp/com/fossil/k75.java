package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class K75 extends J75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362116, 1);
        Q.put(2131361851, 2);
        Q.put(2131363410, 3);
        Q.put(2131362837, 4);
        Q.put(2131361944, 5);
        Q.put(2131362598, 6);
        Q.put(2131363331, 7);
        Q.put(2131363462, 8);
        Q.put(2131361962, 9);
        Q.put(2131362605, 10);
        Q.put(2131363457, 11);
        Q.put(2131361955, 12);
        Q.put(2131362603, 13);
        Q.put(2131363298, 14);
        Q.put(2131362055, 15);
        Q.put(2131363545, 16);
        Q.put(2131363325, 17);
        Q.put(2131363548, 18);
        Q.put(2131363355, 19);
        Q.put(2131363543, 20);
        Q.put(2131363290, 21);
        Q.put(2131363315, 22);
        Q.put(2131363261, 23);
    }
    */

    @DexIgnore
    public K75(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 24, P, Q));
    }

    @DexIgnore
    public K75(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[2], (RelativeLayout) objArr[5], (RelativeLayout) objArr[12], (RelativeLayout) objArr[9], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[1], (RTLImageView) objArr[6], (RTLImageView) objArr[13], (RTLImageView) objArr[10], (LinearLayout) objArr[4], (RelativeLayout) objArr[0], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[21], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[3], (View) objArr[11], (View) objArr[8], (CustomizeWidget) objArr[20], (CustomizeWidget) objArr[16], (CustomizeWidget) objArr[18]);
        this.O = -1;
        this.A.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.O != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.O = 1;
        }
        w();
    }
}
