package com.fossil;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleCoroutineScopeImpl;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bs0 {
    @DexIgnore
    public static final Yr0 a(Lifecycle lifecycle) {
        LifecycleCoroutineScopeImpl lifecycleCoroutineScopeImpl;
        Wg6.c(lifecycle, "$this$coroutineScope");
        while (true) {
            lifecycleCoroutineScopeImpl = (LifecycleCoroutineScopeImpl) lifecycle.a.get();
            if (lifecycleCoroutineScopeImpl == null) {
                lifecycleCoroutineScopeImpl = new LifecycleCoroutineScopeImpl(lifecycle, Ux7.b(null, 1, null).plus(Bw7.c().S()));
                if (lifecycle.a.compareAndSet(null, lifecycleCoroutineScopeImpl)) {
                    lifecycleCoroutineScopeImpl.c();
                    break;
                }
            } else {
                break;
            }
        }
        return lifecycleCoroutineScopeImpl;
    }
}
