package com.fossil;

import com.mapped.Wg6;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Px1 {
    @DexIgnore
    public static final JSONArray a(Ox1[] ox1Arr) {
        Wg6.c(ox1Arr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (Ox1 ox1 : ox1Arr) {
            jSONArray.put(ox1.toJSONObject());
        }
        return jSONArray;
    }
}
