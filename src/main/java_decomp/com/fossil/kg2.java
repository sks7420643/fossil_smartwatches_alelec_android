package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kg2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Kg2> CREATOR; // = new Ng2();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Eg2 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    public Kg2(String str, IBinder iBinder, boolean z, boolean z2) {
        this.b = str;
        this.c = c(iBinder);
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public Kg2(String str, Eg2 eg2, boolean z, boolean z2) {
        this.b = str;
        this.c = eg2;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public static Eg2 c(IBinder iBinder) {
        Hg2 hg2;
        if (iBinder == null) {
            return null;
        }
        try {
            Rg2 zzb = Ke2.e(iBinder).zzb();
            byte[] bArr = zzb == null ? null : (byte[]) Tg2.i(zzb);
            if (bArr != null) {
                hg2 = new Hg2(bArr);
            } else {
                Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
                hg2 = null;
            }
            return hg2;
        } catch (RemoteException e2) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e2);
            return null;
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 1, this.b, false);
        Eg2 eg2 = this.c;
        if (eg2 == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            eg2 = null;
        } else {
            eg2.asBinder();
        }
        Bd2.m(parcel, 2, eg2, false);
        Bd2.c(parcel, 3, this.d);
        Bd2.c(parcel, 4, this.e);
        Bd2.b(parcel, a2);
    }
}
