package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Y08 {
    @DexIgnore
    public static final Y08 a = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Y08 {
        @DexIgnore
        @Override // com.fossil.Y08
        public V18 authenticate(X18 x18, Response response) {
            return null;
        }
    }

    @DexIgnore
    V18 authenticate(X18 x18, Response response) throws IOException;
}
