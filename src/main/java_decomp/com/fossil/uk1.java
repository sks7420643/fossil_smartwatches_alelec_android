package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uk1 extends Yx1 {
    @DexIgnore
    public /* final */ Vk1 b;

    @DexIgnore
    public Uk1(Vk1 vk1) {
        super(vk1);
        this.b = vk1;
    }

    @DexIgnore
    @Override // com.fossil.Yx1
    public Vk1 getErrorCode() {
        return this.b;
    }
}
