package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Ds b;
    @DexIgnore
    public /* final */ /* synthetic */ N6 c;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] d;

    @DexIgnore
    public V7(Ds ds, N6 n6, byte[] bArr) {
        this.b = ds;
        this.c = n6;
        this.d = bArr;
    }

    @DexIgnore
    public final void run() {
        this.b.a(new O7(this.c, this.d));
    }
}
