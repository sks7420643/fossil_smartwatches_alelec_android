package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N58 extends T58 {
    @DexIgnore
    public P58 option;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public N58(com.fossil.P58 r3) {
        /*
            r2 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Missing argument for option: "
            r0.append(r1)
            java.lang.String r1 = r3.getKey()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.option = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.N58.<init>(com.fossil.P58):void");
    }

    @DexIgnore
    public N58(String str) {
        super(str);
    }

    @DexIgnore
    public P58 getOption() {
        return this.option;
    }
}
