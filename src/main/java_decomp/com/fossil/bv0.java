package com.fossil;

import android.annotation.SuppressLint;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bv0 implements Runnable {
    @DexIgnore
    public static /* final */ ThreadLocal<Bv0> f; // = new ThreadLocal<>();
    @DexIgnore
    public static Comparator<Ci> g; // = new Ai();
    @DexIgnore
    public ArrayList<RecyclerView> b; // = new ArrayList<>();
    @DexIgnore
    public long c;
    @DexIgnore
    public long d;
    @DexIgnore
    public ArrayList<Ci> e; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Comparator<Ci> {
        @DexIgnore
        public int a(Ci ci, Ci ci2) {
            int i = -1;
            if ((ci.d == null) != (ci2.d == null)) {
                return ci.d == null ? 1 : -1;
            }
            boolean z = ci.a;
            if (z != ci2.a) {
                if (!z) {
                    i = 1;
                }
                return i;
            }
            int i2 = ci2.b - ci.b;
            if (i2 != 0) {
                return i2;
            }
            int i3 = ci.c - ci2.c;
            if (i3 == 0) {
                return 0;
            }
            return i3;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(Ci ci, Ci ci2) {
            return a(ci, ci2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"VisibleForTests"})
    public static class Bi implements RecyclerView.m.c {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int[] c;
        @DexIgnore
        public int d;

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.m.c
        public void a(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 >= 0) {
                int i3 = this.d * 2;
                int[] iArr = this.c;
                if (iArr == null) {
                    int[] iArr2 = new int[4];
                    this.c = iArr2;
                    Arrays.fill(iArr2, -1);
                } else if (i3 >= iArr.length) {
                    int[] iArr3 = new int[(i3 * 2)];
                    this.c = iArr3;
                    System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
                }
                int[] iArr4 = this.c;
                iArr4[i3] = i;
                iArr4[i3 + 1] = i2;
                this.d++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        @DexIgnore
        public void b() {
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.d = 0;
        }

        @DexIgnore
        public void c(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.m mVar = recyclerView.mLayout;
            if (recyclerView.mAdapter != null && mVar != null && mVar.v0()) {
                if (z) {
                    if (!recyclerView.mAdapterHelper.p()) {
                        mVar.q(recyclerView.mAdapter.getItemCount(), this);
                    }
                } else if (!recyclerView.hasPendingAdapterUpdates()) {
                    mVar.p(this.a, this.b, recyclerView.mState, this);
                }
                int i = this.d;
                if (i > mVar.m) {
                    mVar.m = i;
                    mVar.n = z;
                    recyclerView.mRecycler.K();
                }
            }
        }

        @DexIgnore
        public boolean d(int i) {
            if (this.c == null) {
                return false;
            }
            int i2 = this.d;
            for (int i3 = 0; i3 < i2 * 2; i3 += 2) {
                if (this.c[i3] == i) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public void e(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public RecyclerView d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a() {
            this.a = false;
            this.b = 0;
            this.c = 0;
            this.d = null;
            this.e = 0;
        }
    }

    @DexIgnore
    public static boolean e(RecyclerView recyclerView, int i) {
        int j = recyclerView.mChildHelper.j();
        for (int i2 = 0; i2 < j; i2++) {
            RecyclerView.ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(recyclerView.mChildHelper.i(i2));
            if (childViewHolderInt.mPosition == i && !childViewHolderInt.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        this.b.add(recyclerView);
    }

    @DexIgnore
    public final void b() {
        Ci ci;
        int i;
        int size = this.b.size();
        int i2 = 0;
        int i3 = 0;
        while (i3 < size) {
            RecyclerView recyclerView = this.b.get(i3);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.mPrefetchRegistry.c(recyclerView, false);
                i = recyclerView.mPrefetchRegistry.d + i2;
            } else {
                i = i2;
            }
            i3++;
            i2 = i;
        }
        this.e.ensureCapacity(i2);
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView recyclerView2 = this.b.get(i5);
            if (recyclerView2.getWindowVisibility() == 0) {
                Bi bi = recyclerView2.mPrefetchRegistry;
                int abs = Math.abs(bi.a) + Math.abs(bi.b);
                int i6 = i4;
                for (int i7 = 0; i7 < bi.d * 2; i7 += 2) {
                    if (i6 >= this.e.size()) {
                        ci = new Ci();
                        this.e.add(ci);
                    } else {
                        ci = this.e.get(i6);
                    }
                    int i8 = bi.c[i7 + 1];
                    ci.a = i8 <= abs;
                    ci.b = abs;
                    ci.c = i8;
                    ci.d = recyclerView2;
                    ci.e = bi.c[i7];
                    i6++;
                }
                i4 = i6;
            }
        }
        Collections.sort(this.e, g);
    }

    @DexIgnore
    public final void c(Ci ci, long j) {
        RecyclerView.ViewHolder i = i(ci.d, ci.e, ci.a ? Long.MAX_VALUE : j);
        if (i != null && i.mNestedRecyclerView != null && i.isBound() && !i.isInvalid()) {
            h(i.mNestedRecyclerView.get(), j);
        }
    }

    @DexIgnore
    public final void d(long j) {
        for (int i = 0; i < this.e.size(); i++) {
            Ci ci = this.e.get(i);
            if (ci.d != null) {
                c(ci, j);
                ci.a();
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public void f(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.c == 0) {
            this.c = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.mPrefetchRegistry.e(i, i2);
    }

    @DexIgnore
    public void g(long j) {
        b();
        d(j);
    }

    @DexIgnore
    public final void h(RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.mDataSetHasChangedAfterLayout && recyclerView.mChildHelper.j() != 0) {
                recyclerView.removeAndRecycleViews();
            }
            Bi bi = recyclerView.mPrefetchRegistry;
            bi.c(recyclerView, true);
            if (bi.d != 0) {
                try {
                    Wm0.a(RecyclerView.TRACE_NESTED_PREFETCH_TAG);
                    recyclerView.mState.f(recyclerView.mAdapter);
                    for (int i = 0; i < bi.d * 2; i += 2) {
                        i(recyclerView, bi.c[i], j);
                    }
                } finally {
                    Wm0.b();
                }
            }
        }
    }

    @DexIgnore
    public final RecyclerView.ViewHolder i(RecyclerView recyclerView, int i, long j) {
        if (e(recyclerView, i)) {
            return null;
        }
        RecyclerView.Recycler recycler = recyclerView.mRecycler;
        try {
            recyclerView.onEnterLayoutOrScroll();
            RecyclerView.ViewHolder I = recycler.I(i, false, j);
            if (I != null) {
                if (!I.isBound() || I.isInvalid()) {
                    recycler.a(I, false);
                } else {
                    recycler.B(I.itemView);
                }
            }
            return I;
        } finally {
            recyclerView.onExitLayoutOrScroll(false);
        }
    }

    @DexIgnore
    public void j(RecyclerView recyclerView) {
        this.b.remove(recyclerView);
    }

    @DexIgnore
    public void run() {
        try {
            Wm0.a(RecyclerView.TRACE_PREFETCH_TAG);
            if (!this.b.isEmpty()) {
                int size = this.b.size();
                long j = 0;
                int i = 0;
                while (i < size) {
                    RecyclerView recyclerView = this.b.get(i);
                    i++;
                    j = recyclerView.getWindowVisibility() == 0 ? Math.max(recyclerView.getDrawingTime(), j) : j;
                }
                if (j != 0) {
                    g(TimeUnit.MILLISECONDS.toNanos(j) + this.d);
                    this.c = 0;
                    Wm0.b();
                }
            }
        } finally {
            this.c = 0;
            Wm0.b();
        }
    }
}
