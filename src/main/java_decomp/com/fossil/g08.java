package com.fossil;

import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G08 implements Executor, Closeable {
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater i; // = AtomicLongFieldUpdater.newUpdater(G08.class, "parkedWorkersStack");
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater j; // = AtomicLongFieldUpdater.newUpdater(G08.class, "controlState");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater k; // = AtomicIntegerFieldUpdater.newUpdater(G08.class, "_isTerminated");
    @DexIgnore
    public static /* final */ Vz7 l; // = new Vz7("NOT_IN_STACK");
    @DexIgnore
    public volatile int _isTerminated;
    @DexIgnore
    public /* final */ J08 b;
    @DexIgnore
    public /* final */ J08 c;
    @DexIgnore
    public volatile long controlState;
    @DexIgnore
    public /* final */ AtomicReferenceArray<Ai> d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public volatile long parkedWorkersStack;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends Thread {
        @DexIgnore
        public static /* final */ AtomicIntegerFieldUpdater i; // = AtomicIntegerFieldUpdater.newUpdater(Ai.class, "workerCtl");
        @DexIgnore
        public /* final */ S08 b;
        @DexIgnore
        public Bi c;
        @DexIgnore
        public long d;
        @DexIgnore
        public long e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public volatile int indexInArray;
        @DexIgnore
        public volatile Object nextParkedWorker;
        @DexIgnore
        public volatile int workerCtl;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai() {
            setDaemon(true);
            this.b = new S08();
            this.c = Bi.DORMANT;
            this.workerCtl = 0;
            this.nextParkedWorker = G08.l;
            this.f = Rr7.b.c();
        }

        @DexIgnore
        public Ai(G08 g08, int i2) {
            this();
            n(i2);
        }

        @DexIgnore
        public final void a(int i2) {
            if (i2 != 0) {
                G08.j.addAndGet(G08.this, -2097152);
                Bi bi = this.c;
                if (bi != Bi.TERMINATED) {
                    if (Nv7.a()) {
                        if (!(bi == Bi.BLOCKING)) {
                            throw new AssertionError();
                        }
                    }
                    this.c = Bi.DORMANT;
                }
            }
        }

        @DexIgnore
        public final void b(int i2) {
            if (i2 != 0 && r(Bi.BLOCKING)) {
                G08.this.G();
            }
        }

        @DexIgnore
        public final void c(N08 n08) {
            int o = n08.c.o();
            h(o);
            b(o);
            G08.this.C(n08);
            a(o);
        }

        @DexIgnore
        public final N08 d(boolean z) {
            N08 l;
            N08 l2;
            if (z) {
                boolean z2 = j(G08.this.e * 2) == 0;
                if (z2 && (l2 = l()) != null) {
                    return l2;
                }
                N08 h2 = this.b.h();
                if (h2 != null) {
                    return h2;
                }
                if (!z2 && (l = l()) != null) {
                    return l;
                }
            } else {
                N08 l3 = l();
                if (l3 != null) {
                    return l3;
                }
            }
            return s(false);
        }

        @DexIgnore
        public final N08 e(boolean z) {
            N08 n08;
            if (p()) {
                return d(z);
            }
            if (z) {
                n08 = this.b.h();
                if (n08 == null) {
                    n08 = (N08) G08.this.c.d();
                }
            } else {
                n08 = (N08) G08.this.c.d();
            }
            return n08 == null ? s(true) : n08;
        }

        @DexIgnore
        public final int f() {
            return this.indexInArray;
        }

        @DexIgnore
        public final Object g() {
            return this.nextParkedWorker;
        }

        @DexIgnore
        public final void h(int i2) {
            boolean z = true;
            this.d = 0;
            if (this.c == Bi.PARKING) {
                if (Nv7.a()) {
                    if (i2 != 1) {
                        z = false;
                    }
                    if (!z) {
                        throw new AssertionError();
                    }
                }
                this.c = Bi.BLOCKING;
            }
        }

        @DexIgnore
        public final boolean i() {
            return this.nextParkedWorker != G08.l;
        }

        @DexIgnore
        public final int j(int i2) {
            int i3 = this.f;
            int i4 = i3 ^ (i3 << 13);
            int i5 = i4 ^ (i4 >> 17);
            int i6 = i5 ^ (i5 << 5);
            this.f = i6;
            int i7 = i2 - 1;
            return (i7 & i2) == 0 ? i6 & i7 : (i6 & Integer.MAX_VALUE) % i2;
        }

        @DexIgnore
        public final void k() {
            if (this.d == 0) {
                this.d = System.nanoTime() + G08.this.g;
            }
            LockSupport.parkNanos(G08.this.g);
            if (System.nanoTime() - this.d >= 0) {
                this.d = 0;
                t();
            }
        }

        @DexIgnore
        public final N08 l() {
            if (j(2) == 0) {
                N08 n08 = (N08) G08.this.b.d();
                return n08 != null ? n08 : (N08) G08.this.c.d();
            }
            N08 n082 = (N08) G08.this.c.d();
            return n082 == null ? (N08) G08.this.b.d() : n082;
        }

        @DexIgnore
        public final void m() {
            loop0:
            while (true) {
                boolean z = false;
                while (!G08.this.isTerminated() && this.c != Bi.TERMINATED) {
                    N08 e2 = e(this.g);
                    if (e2 != null) {
                        this.e = 0;
                        c(e2);
                    } else {
                        this.g = false;
                        if (this.e == 0) {
                            q();
                        } else if (!z) {
                            z = true;
                        } else {
                            r(Bi.PARKING);
                            Thread.interrupted();
                            LockSupport.parkNanos(this.e);
                            this.e = 0;
                        }
                    }
                }
            }
            r(Bi.TERMINATED);
        }

        @DexIgnore
        public final void n(int i2) {
            StringBuilder sb = new StringBuilder();
            sb.append(G08.this.h);
            sb.append("-worker-");
            sb.append(i2 == 0 ? "TERMINATED" : String.valueOf(i2));
            setName(sb.toString());
            this.indexInArray = i2;
        }

        @DexIgnore
        public final void o(Object obj) {
            this.nextParkedWorker = obj;
        }

        @DexIgnore
        public final boolean p() {
            boolean z;
            if (this.c != Bi.CPU_ACQUIRED) {
                G08 g08 = G08.this;
                while (true) {
                    long j = g08.controlState;
                    if (((int) ((9223367638808264704L & j) >> 42)) != 0) {
                        if (G08.j.compareAndSet(g08, j, j - 4398046511104L)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    return false;
                }
                this.c = Bi.CPU_ACQUIRED;
            }
            return true;
        }

        @DexIgnore
        public final void q() {
            if (!i()) {
                G08.this.A(this);
                return;
            }
            if (Nv7.a()) {
                if (!(this.b.f() == 0)) {
                    throw new AssertionError();
                }
            }
            this.workerCtl = -1;
            while (i() && !G08.this.isTerminated() && this.c != Bi.TERMINATED) {
                r(Bi.PARKING);
                Thread.interrupted();
                k();
            }
        }

        @DexIgnore
        public final boolean r(Bi bi) {
            Bi bi2 = this.c;
            boolean z = bi2 == Bi.CPU_ACQUIRED;
            if (z) {
                G08.j.addAndGet(G08.this, 4398046511104L);
            }
            if (bi2 != bi) {
                this.c = bi;
            }
            return z;
        }

        @DexIgnore
        public void run() {
            m();
        }

        @DexIgnore
        public final N08 s(boolean z) {
            if (Nv7.a()) {
                if (!(this.b.f() == 0)) {
                    throw new AssertionError();
                }
            }
            int l = G08.this.l();
            if (l < 2) {
                return null;
            }
            int j = j(l);
            long j2 = Long.MAX_VALUE;
            for (int i2 = 0; i2 < l; i2++) {
                int i3 = j + 1;
                j = i3 > l ? 1 : i3;
                Ai ai = G08.this.d.get(j);
                if (!(ai == null || ai == this)) {
                    if (Nv7.a()) {
                        if (!(this.b.f() == 0)) {
                            throw new AssertionError();
                        }
                    }
                    long k = z ? this.b.k(ai.b) : this.b.l(ai.b);
                    if (k == -1) {
                        return this.b.h();
                    }
                    if (k > 0) {
                        j2 = Math.min(j2, k);
                    }
                }
            }
            if (j2 == Long.MAX_VALUE) {
                j2 = 0;
            }
            this.e = j2;
            return null;
        }

        @DexIgnore
        public final void t() {
            synchronized (G08.this.d) {
                if (!G08.this.isTerminated()) {
                    if (G08.this.l() > G08.this.e) {
                        if (i.compareAndSet(this, -1, 1)) {
                            int i2 = this.indexInArray;
                            n(0);
                            G08.this.B(this, i2, 0);
                            int andDecrement = (int) (G08.j.getAndDecrement(G08.this) & 2097151);
                            if (andDecrement != i2) {
                                Ai ai = G08.this.d.get(andDecrement);
                                if (ai != null) {
                                    Ai ai2 = ai;
                                    G08.this.d.set(i2, ai2);
                                    ai2.n(i2);
                                    G08.this.B(ai2, andDecrement, i2);
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                            G08.this.d.set(andDecrement, null);
                            Cd6 cd6 = Cd6.a;
                            this.c = Bi.TERMINATED;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public enum Bi {
        CPU_ACQUIRED,
        BLOCKING,
        PARKING,
        DORMANT,
        TERMINATED
    }

    @DexIgnore
    public G08(int i2, int i3, long j2, String str) {
        boolean z = true;
        this.e = i2;
        this.f = i3;
        this.g = j2;
        this.h = str;
        if (i2 >= 1) {
            if (this.f >= this.e) {
                if (this.f <= 2097150) {
                    if (this.g <= 0 ? false : z) {
                        this.b = new J08();
                        this.c = new J08();
                        this.parkedWorkersStack = 0;
                        this.d = new AtomicReferenceArray<>(this.f + 1);
                        this.controlState = ((long) this.e) << 42;
                        this._isTerminated = 0;
                        return;
                    }
                    throw new IllegalArgumentException(("Idle worker keep alive time " + this.g + " must be positive").toString());
                }
                throw new IllegalArgumentException(("Max pool size " + this.f + " should not exceed maximal supported number of threads 2097150").toString());
            }
            throw new IllegalArgumentException(("Max pool size " + this.f + " should be greater than or equals to core pool size " + this.e).toString());
        }
        throw new IllegalArgumentException(("Core pool size " + this.e + " should be at least 1").toString());
    }

    @DexIgnore
    public static /* synthetic */ boolean P(G08 g08, long j2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            j2 = g08.controlState;
        }
        return g08.M(j2);
    }

    @DexIgnore
    public static /* synthetic */ void k(G08 g08, Runnable runnable, O08 o08, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            o08 = M08.c;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        g08.j(runnable, o08, z);
    }

    @DexIgnore
    public final boolean A(Ai ai) {
        long j2;
        int f2;
        if (ai.g() != l) {
            return false;
        }
        do {
            j2 = this.parkedWorkersStack;
            int i2 = (int) (2097151 & j2);
            f2 = ai.f();
            if (Nv7.a()) {
                if (!(f2 != 0)) {
                    throw new AssertionError();
                }
            }
            ai.o(this.d.get(i2));
        } while (!i.compareAndSet(this, j2, ((long) f2) | ((2097152 + j2) & -2097152)));
        return true;
    }

    @DexIgnore
    public final void B(Ai ai, int i2, int i3) {
        while (true) {
            long j2 = this.parkedWorkersStack;
            int i4 = (int) (2097151 & j2);
            int m = i4 == i2 ? i3 == 0 ? m(ai) : i3 : i4;
            if (m >= 0 && i.compareAndSet(this, j2, ((2097152 + j2) & -2097152) | ((long) m))) {
                return;
            }
        }
    }

    @DexIgnore
    public final void C(N08 n08) {
        Xx7 a2;
        try {
            n08.run();
            a2 = Yx7.a();
            if (a2 == null) {
                return;
            }
        } catch (Throwable th) {
            Xx7 a3 = Yx7.a();
            if (a3 != null) {
                a3.d();
            }
            throw th;
        }
        a2.d();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0071, code lost:
        if (r0 != null) goto L_0x0073;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void D(long r14) {
        /*
            r13 = this;
            r10 = 0
            r3 = 0
            r2 = 1
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r0 = com.fossil.G08.k
            boolean r0 = r0.compareAndSet(r13, r3, r2)
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            com.fossil.G08$Ai r5 = r13.h()
            java.util.concurrent.atomic.AtomicReferenceArray<com.fossil.G08$Ai> r0 = r13.d
            monitor-enter(r0)
            long r6 = r13.controlState     // Catch:{ all -> 0x00b8 }
            r8 = 2097151(0x1fffff, double:1.0361303E-317)
            long r6 = r6 & r8
            int r6 = (int) r6
            monitor-exit(r0)
            if (r2 > r6) goto L_0x0061
            r1 = r2
        L_0x001f:
            java.util.concurrent.atomic.AtomicReferenceArray<com.fossil.G08$Ai> r0 = r13.d
            java.lang.Object r0 = r0.get(r1)
            if (r0 == 0) goto L_0x005c
            com.fossil.G08$Ai r0 = (com.fossil.G08.Ai) r0
            if (r0 == r5) goto L_0x004e
        L_0x002b:
            boolean r4 = r0.isAlive()
            if (r4 == 0) goto L_0x0038
            java.util.concurrent.locks.LockSupport.unpark(r0)
            r0.join(r14)
            goto L_0x002b
        L_0x0038:
            com.fossil.G08$Bi r4 = r0.c
            boolean r7 = com.fossil.Nv7.a()
            if (r7 == 0) goto L_0x0047
            com.fossil.G08$Bi r7 = com.fossil.G08.Bi.TERMINATED
            if (r4 != r7) goto L_0x0054
            r4 = r2
        L_0x0045:
            if (r4 == 0) goto L_0x0056
        L_0x0047:
            com.fossil.S08 r0 = r0.b
            com.fossil.J08 r4 = r13.c
            r0.g(r4)
        L_0x004e:
            if (r1 == r6) goto L_0x0061
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001f
        L_0x0054:
            r4 = r3
            goto L_0x0045
        L_0x0056:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x005c:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        L_0x0061:
            com.fossil.J08 r0 = r13.c
            r0.b()
            com.fossil.J08 r0 = r13.b
            r0.b()
        L_0x006b:
            if (r5 == 0) goto L_0x007b
            com.fossil.N08 r0 = r5.e(r2)
            if (r0 == 0) goto L_0x007b
        L_0x0073:
            if (r0 == 0) goto L_0x0084
        L_0x0075:
            if (r0 == 0) goto L_0x008d
            r13.C(r0)
            goto L_0x006b
        L_0x007b:
            com.fossil.J08 r0 = r13.b
            java.lang.Object r0 = r0.d()
            com.fossil.N08 r0 = (com.fossil.N08) r0
            goto L_0x0073
        L_0x0084:
            com.fossil.J08 r0 = r13.c
            java.lang.Object r0 = r0.d()
            com.fossil.N08 r0 = (com.fossil.N08) r0
            goto L_0x0075
        L_0x008d:
            if (r5 == 0) goto L_0x0094
            com.fossil.G08$Bi r0 = com.fossil.G08.Bi.TERMINATED
            r5.r(r0)
        L_0x0094:
            boolean r0 = com.fossil.Nv7.a()
            if (r0 == 0) goto L_0x00ac
            long r0 = r13.controlState
            r4 = 9223367638808264704(0x7ffffc0000000000, double:NaN)
            long r0 = r0 & r4
            r4 = 42
            long r0 = r0 >> r4
            int r0 = (int) r0
            int r1 = r13.e
            if (r0 != r1) goto L_0x00bb
        L_0x00aa:
            if (r2 == 0) goto L_0x00b2
        L_0x00ac:
            r13.parkedWorkersStack = r10
            r13.controlState = r10
            goto L_0x000c
        L_0x00b2:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x00b8:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x00bb:
            r2 = r3
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.G08.D(long):void");
    }

    @DexIgnore
    public final void F(boolean z) {
        long addAndGet = j.addAndGet(this, 2097152);
        if (!z && !Q() && !M(addAndGet)) {
            Q();
        }
    }

    @DexIgnore
    public final void G() {
        if (!Q() && !P(this, 0, 1, null)) {
            Q();
        }
    }

    @DexIgnore
    public final N08 L(Ai ai, N08 n08, boolean z) {
        if (ai == null || ai.c == Bi.TERMINATED) {
            return n08;
        }
        if (n08.c.o() == 0 && ai.c == Bi.BLOCKING) {
            return n08;
        }
        ai.g = true;
        return ai.b.a(n08, z);
    }

    @DexIgnore
    public final boolean M(long j2) {
        if (Bs7.d(((int) (2097151 & j2)) - ((int) ((4398044413952L & j2) >> 21)), 0) < this.e) {
            int c2 = c();
            if (c2 == 1 && this.e > 1) {
                c();
            }
            if (c2 > 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean Q() {
        Ai o;
        do {
            o = o();
            if (o == null) {
                return false;
            }
        } while (!Ai.i.compareAndSet(o, -1, 0));
        LockSupport.unpark(o);
        return true;
    }

    @DexIgnore
    public final boolean b(N08 n08) {
        boolean z = true;
        if (n08.c.o() != 1) {
            z = false;
        }
        return z ? this.c.a(n08) : this.b.a(n08);
    }

    @DexIgnore
    public final int c() {
        boolean z = false;
        synchronized (this.d) {
            if (isTerminated()) {
                return -1;
            }
            long j2 = this.controlState;
            int i2 = (int) (j2 & 2097151);
            int d2 = Bs7.d(i2 - ((int) ((j2 & 4398044413952L) >> 21)), 0);
            if (d2 >= this.e) {
                return 0;
            }
            if (i2 >= this.f) {
                return 0;
            }
            int i3 = ((int) (this.controlState & 2097151)) + 1;
            if (i3 > 0 && this.d.get(i3) == null) {
                Ai ai = new Ai(this, i3);
                this.d.set(i3, ai);
                if (i3 == ((int) (j.incrementAndGet(this) & 2097151))) {
                    z = true;
                }
                if (z) {
                    ai.start();
                    return d2 + 1;
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        D(ButtonService.CONNECT_TIMEOUT);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        k(this, runnable, null, false, 6, null);
    }

    @DexIgnore
    public final N08 f(Runnable runnable, O08 o08) {
        long a2 = Q08.e.a();
        if (!(runnable instanceof N08)) {
            return new P08(runnable, a2, o08);
        }
        N08 n08 = (N08) runnable;
        n08.b = a2;
        n08.c = o08;
        return n08;
    }

    @DexIgnore
    public final Ai h() {
        Thread currentThread = Thread.currentThread();
        if (!(currentThread instanceof Ai)) {
            currentThread = null;
        }
        Ai ai = (Ai) currentThread;
        if (ai == null || !Wg6.a(G08.this, this)) {
            return null;
        }
        return ai;
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isTerminated() {
        /*
            r1 = this;
            int r0 = r1._isTerminated
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.G08.isTerminated():boolean");
    }

    @DexIgnore
    public final void j(Runnable runnable, O08 o08, boolean z) {
        Xx7 a2 = Yx7.a();
        if (a2 != null) {
            a2.h();
        }
        N08 f2 = f(runnable, o08);
        Ai h2 = h();
        N08 L = L(h2, f2, z);
        if (L == null || b(L)) {
            boolean z2 = z && h2 != null;
            if (f2.c.o() != 0) {
                F(z2);
            } else if (!z2) {
                G();
            }
        } else {
            throw new RejectedExecutionException(this.h + " was terminated");
        }
    }

    @DexIgnore
    public final int l() {
        return (int) (this.controlState & 2097151);
    }

    @DexIgnore
    public final int m(Ai ai) {
        Object g2 = ai.g();
        while (g2 != l) {
            if (g2 == null) {
                return 0;
            }
            Ai ai2 = (Ai) g2;
            int f2 = ai2.f();
            if (f2 != 0) {
                return f2;
            }
            g2 = ai2.g();
        }
        return -1;
    }

    @DexIgnore
    public final Ai o() {
        while (true) {
            long j2 = this.parkedWorkersStack;
            Ai ai = this.d.get((int) (2097151 & j2));
            if (ai == null) {
                return null;
            }
            int m = m(ai);
            if (m >= 0 && i.compareAndSet(this, j2, ((long) m) | ((2097152 + j2) & -2097152))) {
                ai.o(l);
                return ai;
            }
        }
    }

    @DexIgnore
    public String toString() {
        int i2;
        ArrayList arrayList = new ArrayList();
        int length = this.d.length();
        int i3 = 1;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i3 < length) {
            Ai ai = this.d.get(i3);
            if (ai != null) {
                int f2 = ai.b.f();
                int i9 = F08.a[ai.c.ordinal()];
                if (i9 == 1) {
                    i6++;
                    i2 = i4;
                } else if (i9 == 2) {
                    i7++;
                    arrayList.add(String.valueOf(f2) + "b");
                    i2 = i4;
                } else if (i9 == 3) {
                    i8++;
                    arrayList.add(String.valueOf(f2) + "c");
                    i2 = i4;
                } else if (i9 != 4) {
                    i2 = i9 != 5 ? i4 : i4 + 1;
                } else {
                    i5++;
                    if (f2 > 0) {
                        arrayList.add(String.valueOf(f2) + "d");
                        i2 = i4;
                    }
                }
                i3++;
                i4 = i2;
            }
            i2 = i4;
            i3++;
            i4 = i2;
        }
        long j2 = this.controlState;
        return this.h + '@' + Ov7.b(this) + "[Pool Size {core = " + this.e + ", max = " + this.f + "}, Worker States {CPU = " + i8 + ", blocking = " + i7 + ", parked = " + i6 + ", dormant = " + i5 + ", terminated = " + i4 + "}, running workers queues = " + arrayList + ", global CPU queue size = " + this.b.c() + ", global blocking queue size = " + this.c.c() + ", Control State {created workers= " + ((int) (2097151 & j2)) + ", blocking tasks = " + ((int) ((4398044413952L & j2) >> 21)) + ", CPUs acquired = " + (this.e - ((int) ((9223367638808264704L & j2) >> 42))) + "}]";
    }
}
