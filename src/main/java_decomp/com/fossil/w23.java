package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W23 {
    @DexIgnore
    public static /* final */ U23 a; // = c();
    @DexIgnore
    public static /* final */ U23 b; // = new X23();

    @DexIgnore
    public static U23 a() {
        return a;
    }

    @DexIgnore
    public static U23 b() {
        return b;
    }

    @DexIgnore
    public static U23 c() {
        try {
            return (U23) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
