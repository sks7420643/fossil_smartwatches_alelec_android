package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hw extends Ox1 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public N6 c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public /* final */ JSONObject e;

    @DexIgnore
    public /* synthetic */ Hw(long j, N6 n6, byte[] bArr, JSONObject jSONObject, int i) {
        j = (i & 1) != 0 ? System.currentTimeMillis() : j;
        n6 = (i & 2) != 0 ? null : n6;
        bArr = (i & 4) != 0 ? new byte[0] : bArr;
        jSONObject = (i & 8) != 0 ? new JSONObject() : jSONObject;
        this.b = j;
        this.c = n6;
        this.d = bArr;
        this.e = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(new JSONObject(), Jd0.Q0, Double.valueOf(Hy1.f(this.b)));
        Jd0 jd0 = Jd0.R0;
        N6 n6 = this.c;
        return Gy1.c(G80.k(G80.k(k, jd0, n6 != null ? n6.b : null), Jd0.S0, Dy1.e(this.d, null, 1, null)), this.e);
    }
}
