package com.fossil;

import android.os.Bundle;
import com.fossil.Qf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pf7 extends Df7 {
    @DexIgnore
    public Qf7 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public Pf7(Bundle bundle) {
        b(bundle);
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public boolean a() {
        Qf7 qf7 = this.c;
        if (qf7 == null) {
            return false;
        }
        return qf7.a();
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public void b(Bundle bundle) {
        super.b(bundle);
        this.d = bundle.getString("_wxapi_showmessage_req_lang");
        this.e = bundle.getString("_wxapi_showmessage_req_country");
        this.c = Qf7.Ai.a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public int c() {
        return 4;
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public void d(Bundle bundle) {
        Bundle d2 = Qf7.Ai.d(this.c);
        super.d(d2);
        bundle.putString("_wxapi_showmessage_req_lang", this.d);
        bundle.putString("_wxapi_showmessage_req_country", this.e);
        bundle.putAll(d2);
    }
}
