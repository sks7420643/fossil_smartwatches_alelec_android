package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U33 extends RuntimeException {
    @DexIgnore
    public /* final */ List<String> zza; // = null;

    @DexIgnore
    public U33(M23 m23) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
