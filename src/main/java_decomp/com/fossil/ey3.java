package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fossil.Wx3;
import com.google.android.material.internal.CheckableImageButton;
import java.util.Iterator;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ey3<S> extends Kq0 {
    @DexIgnore
    public static /* final */ Object w; // = "CONFIRM_BUTTON_TAG";
    @DexIgnore
    public static /* final */ Object x; // = "CANCEL_BUTTON_TAG";
    @DexIgnore
    public static /* final */ Object y; // = "TOGGLE_BUTTON_TAG";
    @DexIgnore
    public /* final */ LinkedHashSet<Fy3<? super S>> b; // = new LinkedHashSet<>();
    @DexIgnore
    public /* final */ LinkedHashSet<View.OnClickListener> c; // = new LinkedHashSet<>();
    @DexIgnore
    public /* final */ LinkedHashSet<DialogInterface.OnCancelListener> d; // = new LinkedHashSet<>();
    @DexIgnore
    public /* final */ LinkedHashSet<DialogInterface.OnDismissListener> e; // = new LinkedHashSet<>();
    @DexIgnore
    public int f;
    @DexIgnore
    public Zx3<S> g;
    @DexIgnore
    public Ly3<S> h;
    @DexIgnore
    public Wx3 i;
    @DexIgnore
    public Dy3<S> j;
    @DexIgnore
    public int k;
    @DexIgnore
    public CharSequence l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public TextView s;
    @DexIgnore
    public CheckableImageButton t;
    @DexIgnore
    public C04 u;
    @DexIgnore
    public Button v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnClickListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.fossil.Fy3 */
        /* JADX WARN: Multi-variable type inference failed */
        public void onClick(View view) {
            Iterator it = Ey3.this.b.iterator();
            while (it.hasNext()) {
                ((Fy3) it.next()).a(Ey3.this.H6());
            }
            Ey3.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements View.OnClickListener {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void onClick(View view) {
            Iterator it = Ey3.this.c.iterator();
            while (it.hasNext()) {
                ((View.OnClickListener) it.next()).onClick(view);
            }
            Ey3.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Ky3<S> {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.Ky3
        public void a(S s) {
            Ey3.this.N6();
            if (Ey3.this.g.T()) {
                Ey3.this.v.setEnabled(true);
            } else {
                Ey3.this.v.setEnabled(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements View.OnClickListener {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void onClick(View view) {
            Ey3.this.t.toggle();
            Ey3 ey3 = Ey3.this;
            ey3.O6(ey3.t);
            Ey3.this.L6();
        }
    }

    @DexIgnore
    public static Drawable D6(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        int[] iArr = {16842912};
        stateListDrawable.addState(iArr, Gf0.d(context, Mw3.ic_calendar_black_24dp));
        stateListDrawable.addState(new int[0], Gf0.d(context, Mw3.ic_edit_black_24dp));
        return stateListDrawable;
    }

    @DexIgnore
    public static int E6(Context context) {
        Resources resources = context.getResources();
        return resources.getDimensionPixelOffset(Lw3.mtrl_calendar_bottom_padding) + resources.getDimensionPixelSize(Lw3.mtrl_calendar_navigation_height) + resources.getDimensionPixelOffset(Lw3.mtrl_calendar_navigation_top_padding) + resources.getDimensionPixelOffset(Lw3.mtrl_calendar_navigation_bottom_padding) + resources.getDimensionPixelSize(Lw3.mtrl_calendar_days_of_week_height) + (Iy3.f * resources.getDimensionPixelSize(Lw3.mtrl_calendar_day_height)) + ((Iy3.f - 1) * resources.getDimensionPixelOffset(Lw3.mtrl_calendar_month_vertical_padding));
    }

    @DexIgnore
    public static int G6(Context context) {
        Resources resources = context.getResources();
        int dimensionPixelOffset = resources.getDimensionPixelOffset(Lw3.mtrl_calendar_content_padding);
        int i2 = Hy3.n().f;
        return (resources.getDimensionPixelOffset(Lw3.mtrl_calendar_month_horizontal_padding) * (i2 - 1)) + (dimensionPixelOffset * 2) + (resources.getDimensionPixelSize(Lw3.mtrl_calendar_day_width) * i2);
    }

    @DexIgnore
    public static boolean K6(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Nz3.c(context, Jw3.materialCalendarStyle, Dy3.class.getCanonicalName()), new int[]{16843277});
        boolean z = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        return z;
    }

    @DexIgnore
    public static long M6() {
        return Hy3.n().h;
    }

    @DexIgnore
    public String F6() {
        return this.g.j(getContext());
    }

    @DexIgnore
    public final S H6() {
        return this.g.b0();
    }

    @DexIgnore
    public final int I6(Context context) {
        int i2 = this.f;
        return i2 != 0 ? i2 : this.g.Q(context);
    }

    @DexIgnore
    public final void J6(Context context) {
        this.t.setTag(y);
        this.t.setImageDrawable(D6(context));
        Mo0.l0(this.t, null);
        O6(this.t);
        this.t.setOnClickListener(new Di());
    }

    @DexIgnore
    public final void L6() {
        this.j = Dy3.M6(this.g, I6(requireContext()), this.i);
        this.h = this.t.isChecked() ? Gy3.x6(this.g, this.i) : this.j;
        N6();
        Xq0 j2 = getChildFragmentManager().j();
        j2.r(Nw3.mtrl_calendar_frame, this.h);
        j2.j();
        this.h.v6(new Ci());
    }

    @DexIgnore
    public final void N6() {
        String F6 = F6();
        this.s.setContentDescription(String.format(getString(Rw3.mtrl_picker_announce_current_selection), F6));
        this.s.setText(F6);
    }

    @DexIgnore
    public final void O6(CheckableImageButton checkableImageButton) {
        this.t.setContentDescription(this.t.isChecked() ? checkableImageButton.getContext().getString(Rw3.mtrl_picker_toggle_to_calendar_input_mode) : checkableImageButton.getContext().getString(Rw3.mtrl_picker_toggle_to_text_input_mode));
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public final void onCancel(DialogInterface dialogInterface) {
        Iterator<DialogInterface.OnCancelListener> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().onCancel(dialogInterface);
        }
        super.onCancel(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.f = bundle.getInt("OVERRIDE_THEME_RES_ID");
        this.g = (Zx3) bundle.getParcelable("DATE_SELECTOR_KEY");
        this.i = (Wx3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.k = bundle.getInt("TITLE_TEXT_RES_ID_KEY");
        this.l = bundle.getCharSequence("TITLE_TEXT_KEY");
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public final Dialog onCreateDialog(Bundle bundle) {
        Dialog dialog = new Dialog(requireContext(), I6(requireContext()));
        Context context = dialog.getContext();
        this.m = K6(context);
        int c2 = Nz3.c(context, Jw3.colorSurface, Ey3.class.getCanonicalName());
        C04 c04 = new C04(context, null, Jw3.materialCalendarStyle, Sw3.Widget_MaterialComponents_MaterialCalendar);
        this.u = c04;
        c04.M(context);
        this.u.V(ColorStateList.valueOf(c2));
        this.u.U(Mo0.u(dialog.getWindow().getDecorView()));
        return dialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(this.m ? Pw3.mtrl_picker_fullscreen : Pw3.mtrl_picker_dialog, viewGroup);
        Context context = inflate.getContext();
        if (this.m) {
            inflate.findViewById(Nw3.mtrl_calendar_frame).setLayoutParams(new LinearLayout.LayoutParams(G6(context), -2));
        } else {
            View findViewById = inflate.findViewById(Nw3.mtrl_calendar_main_pane);
            View findViewById2 = inflate.findViewById(Nw3.mtrl_calendar_frame);
            findViewById.setLayoutParams(new LinearLayout.LayoutParams(G6(context), -1));
            findViewById2.setMinimumHeight(E6(requireContext()));
        }
        TextView textView = (TextView) inflate.findViewById(Nw3.mtrl_picker_header_selection_text);
        this.s = textView;
        Mo0.n0(textView, 1);
        this.t = (CheckableImageButton) inflate.findViewById(Nw3.mtrl_picker_header_toggle);
        TextView textView2 = (TextView) inflate.findViewById(Nw3.mtrl_picker_title_text);
        CharSequence charSequence = this.l;
        if (charSequence != null) {
            textView2.setText(charSequence);
        } else {
            textView2.setText(this.k);
        }
        J6(context);
        this.v = (Button) inflate.findViewById(Nw3.confirm_button);
        if (this.g.T()) {
            this.v.setEnabled(true);
        } else {
            this.v.setEnabled(false);
        }
        this.v.setTag(w);
        this.v.setOnClickListener(new Ai());
        Button button = (Button) inflate.findViewById(Nw3.cancel_button);
        button.setTag(x);
        button.setOnClickListener(new Bi());
        return inflate;
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public final void onDismiss(DialogInterface dialogInterface) {
        Iterator<DialogInterface.OnDismissListener> it = this.e.iterator();
        while (it.hasNext()) {
            it.next().onDismiss(dialogInterface);
        }
        ViewGroup viewGroup = (ViewGroup) getView();
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("OVERRIDE_THEME_RES_ID", this.f);
        bundle.putParcelable("DATE_SELECTOR_KEY", this.g);
        Wx3.Bi bi = new Wx3.Bi(this.i);
        if (this.j.I6() != null) {
            bi.b(this.j.I6().h);
        }
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", bi.a());
        bundle.putInt("TITLE_TEXT_RES_ID_KEY", this.k);
        bundle.putCharSequence("TITLE_TEXT_KEY", this.l);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onStart() {
        super.onStart();
        Window window = requireDialog().getWindow();
        if (this.m) {
            window.setLayout(-1, -1);
            window.setBackgroundDrawable(this.u);
        } else {
            window.setLayout(-2, -2);
            int dimensionPixelOffset = getResources().getDimensionPixelOffset(Lw3.mtrl_calendar_dialog_background_inset);
            Rect rect = new Rect(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset);
            window.setBackgroundDrawable(new InsetDrawable((Drawable) this.u, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset));
            window.getDecorView().setOnTouchListener(new Py3(requireDialog(), rect));
        }
        L6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onStop() {
        this.h.w6();
        super.onStop();
    }
}
