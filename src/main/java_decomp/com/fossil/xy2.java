package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xy2<E> extends Ay2<E> {
    @DexIgnore
    public /* final */ transient E d;
    @DexIgnore
    public transient int e;

    @DexIgnore
    public Xy2(E e2) {
        Sw2.b(e2);
        this.d = e2;
    }

    @DexIgnore
    public Xy2(E e2, int i) {
        this.d = e2;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean contains(Object obj) {
        return this.d.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final int hashCode() {
        int i = this.e;
        if (i != 0) {
            return i;
        }
        int hashCode = this.d.hashCode();
        this.e = hashCode;
        return hashCode;
    }

    @DexIgnore
    @Override // com.fossil.Tx2, com.fossil.Ay2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    public final int size() {
        return 1;
    }

    @DexIgnore
    public final String toString() {
        String obj = this.d.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final boolean zza() {
        return this.e != 0;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzb(Object[] objArr, int i) {
        objArr[i] = this.d;
        return i + 1;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Cz2<E> zzb() {
        return new Ky2(this.d);
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final Sx2<E> zzd() {
        return Sx2.zza(this.d);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return false;
    }
}
