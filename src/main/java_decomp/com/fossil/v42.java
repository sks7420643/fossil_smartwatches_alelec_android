package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V42 {
    @DexIgnore
    public static Se2 a; // = new Se2("GoogleSignInCommon", new String[0]);

    @DexIgnore
    public static L42 a(Intent intent) {
        if (intent == null || (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount"))) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.f;
        }
        return new L42(googleSignInAccount, status);
    }

    @DexIgnore
    public static Intent b(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    @DexIgnore
    public static T62<Status> c(R62 r62, Context context, boolean z) {
        a.a("Signing out", new Object[0]);
        d(context);
        return z ? U62.b(Status.f, r62) : r62.j(new W42(r62));
    }

    @DexIgnore
    public static void d(Context context) {
        B52.c(context).a();
        for (R62 r62 : R62.k()) {
            r62.p();
        }
        L72.b();
    }

    @DexIgnore
    public static Intent e(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getFallbackSignInIntent()", new Object[0]);
        Intent b = b(context, googleSignInOptions);
        b.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return b;
    }

    @DexIgnore
    public static T62<Status> f(R62 r62, Context context, boolean z) {
        a.a("Revoking access", new Object[0]);
        String e = O42.b(context).e();
        d(context);
        return z ? R42.a(e) : r62.j(new Y42(r62));
    }

    @DexIgnore
    public static Intent g(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getNoImplementationSignInIntent()", new Object[0]);
        Intent b = b(context, googleSignInOptions);
        b.setAction("com.google.android.gms.auth.NO_IMPL");
        return b;
    }
}
