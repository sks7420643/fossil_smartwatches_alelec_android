package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F91 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public F91(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || F91.class != obj.getClass()) {
            return false;
        }
        F91 f91 = (F91) obj;
        return TextUtils.equals(this.a, f91.a) && TextUtils.equals(this.b, f91.b);
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Header[name=" + this.a + ",value=" + this.b + "]";
    }
}
