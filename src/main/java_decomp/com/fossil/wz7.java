package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wz7 {
    @DexIgnore
    public static final int a() {
        return Xz7.a();
    }

    @DexIgnore
    public static final int b(String str, int i, int i2, int i3) {
        return Yz7.a(str, i, i2, i3);
    }

    @DexIgnore
    public static final long c(String str, long j, long j2, long j3) {
        return Yz7.b(str, j, j2, j3);
    }

    @DexIgnore
    public static final String d(String str) {
        return Xz7.b(str);
    }

    @DexIgnore
    public static final boolean e(String str, boolean z) {
        return Yz7.c(str, z);
    }
}
