package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.fossil.imagefilters.Format;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.K70;
import com.mapped.Kc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class G80 {
    @DexIgnore
    public static final byte a(Set<? extends K70> set) {
        byte b = (byte) 0;
        Iterator<T> it = set.iterator();
        while (it.hasNext()) {
            b = (byte) (it.next().b() | b);
        }
        return b;
    }

    @DexIgnore
    public static final SharedPreferences b(Ld0 ld0) {
        String name = ld0.name();
        Context a2 = Id0.i.a();
        if (a2 != null) {
            return a2.getSharedPreferences(name, 0);
        }
        return null;
    }

    @DexIgnore
    public static final WorkoutState c(String str) {
        if (Vt7.j(str, "started", true)) {
            return WorkoutState.START;
        }
        if (Vt7.j(str, "paused", true)) {
            return WorkoutState.PAUSE;
        }
        if (Vt7.j(str, "resumed", true)) {
            return WorkoutState.RESUME;
        }
        if (Vt7.j(str, "end", true)) {
            return WorkoutState.END;
        }
        return null;
    }

    @DexIgnore
    public static final WorkoutType d(int i) {
        WorkoutType workoutType;
        WorkoutType[] values = WorkoutType.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                workoutType = null;
                break;
            }
            workoutType = values[i2];
            if (workoutType.getValue() == i) {
                break;
            }
            i2++;
        }
        return workoutType != null ? workoutType : WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ String e(int i, int i2) {
        if ((i2 & 1) != 0) {
            i = 16;
        }
        String a2 = E.a("UUID.randomUUID().toString()");
        String substring = a2.substring(0, Math.min(i, a2.length()));
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String f(Format format) {
        int i = H80.a[format.ordinal()];
        if (i == 1) {
            return OrmLiteConfigUtil.RAW_DIR_NAME;
        }
        if (i == 2) {
            return "rle";
        }
        throw new Kc6();
    }

    @DexIgnore
    public static final JSONArray g(N6[] n6Arr) {
        JSONArray jSONArray = new JSONArray();
        for (N6 n6 : n6Arr) {
            jSONArray.put(n6.b);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray h(Zm1[] zm1Arr) {
        JSONArray jSONArray = new JSONArray();
        for (Zm1 zm1 : zm1Arr) {
            jSONArray.put(zm1.b());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray i(FitnessData[] fitnessDataArr) {
        ArrayList<Short> values;
        JSONArray jSONArray = new JSONArray();
        for (FitnessData fitnessData : fitnessDataArr) {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray2 = new JSONArray();
            ArrayList<WorkoutSession> workouts = fitnessData.getWorkouts();
            if (workouts != null) {
                for (T t : workouts) {
                    JSONObject jSONObject2 = new JSONObject();
                    Jd0 jd0 = Jd0.I4;
                    Wg6.b(t, "item");
                    k(jSONObject2, jd0, Long.valueOf(t.getId()));
                    k(jSONObject2, Jd0.X, Integer.valueOf(t.getStartTime()));
                    k(jSONObject2, Jd0.Y, Integer.valueOf(t.getEndTime()));
                    k(jSONObject2, Jd0.S5, Integer.valueOf(t.getGpsDataPoints().size()));
                    jSONArray2.put(jSONObject2);
                }
            }
            JSONArray jSONArray3 = new JSONArray();
            ArrayList<SleepSession> sleeps = fitnessData.getSleeps();
            if (sleeps != null) {
                for (T t2 : sleeps) {
                    JSONObject jSONObject3 = new JSONObject();
                    Jd0 jd02 = Jd0.X;
                    Wg6.b(t2, "item");
                    k(jSONObject3, jd02, Integer.valueOf(t2.getStartTime()));
                    k(jSONObject3, Jd0.Y, Integer.valueOf(t2.getEndTime()));
                    jSONArray3.put(jSONObject3);
                }
            }
            k(jSONObject, Jd0.f0, jSONArray2);
            k(jSONObject, Jd0.e0, jSONArray3);
            k(jSONObject, Jd0.Z, Integer.valueOf(fitnessData.getTimezoneOffsetInSecond()));
            Jd0 jd03 = Jd0.d0;
            ActiveMinute activeMinute = fitnessData.getActiveMinute();
            k(jSONObject, jd03, Integer.valueOf((activeMinute != null ? Integer.valueOf(activeMinute.getTotal()) : null).intValue()));
            Jd0 jd04 = Jd0.a0;
            Distance distance = fitnessData.getDistance();
            k(jSONObject, jd04, Double.valueOf((distance != null ? Double.valueOf(distance.getTotal()) : null).doubleValue()));
            k(jSONObject, Jd0.Y, Integer.valueOf(fitnessData.getEndTime()));
            k(jSONObject, Jd0.X, Integer.valueOf(fitnessData.getStartTime()));
            Jd0 jd05 = Jd0.c0;
            Step step = fitnessData.getStep();
            k(jSONObject, jd05, Integer.valueOf((step != null ? Integer.valueOf(step.getTotal()) : null).intValue()));
            Jd0 jd06 = Jd0.b0;
            Calorie calorie = fitnessData.getCalorie();
            k(jSONObject, jd06, Integer.valueOf((calorie != null ? Integer.valueOf(calorie.getTotal()) : null).intValue()));
            Jd0 jd07 = Jd0.g0;
            HeartRate heartrate = fitnessData.getHeartrate();
            k(jSONObject, jd07, (heartrate == null || (values = heartrate.getValues()) == null) ? JSONObject.NULL : Integer.valueOf(values.size()));
            Jd0 jd08 = Jd0.h0;
            ArrayList<Resting> resting = fitnessData.getResting();
            k(jSONObject, jd08, Integer.valueOf((resting != null ? Integer.valueOf(resting.size()) : null).intValue()));
            Jd0 jd09 = Jd0.i0;
            ArrayList<GoalTracking> goals = fitnessData.getGoals();
            k(jSONObject, jd09, Integer.valueOf((goals != null ? Integer.valueOf(goals.size()) : null).intValue()));
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray j(UUID[] uuidArr) {
        JSONArray jSONArray = new JSONArray();
        for (UUID uuid : uuidArr) {
            jSONArray.put(uuid.toString());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONObject k(JSONObject jSONObject, Jd0 jd0, Object obj) {
        JSONObject put = jSONObject.put(Ey1.a(jd0), obj);
        Wg6.b(put, "this.put(key.lowerCaseName, value)");
        return put;
    }
}
