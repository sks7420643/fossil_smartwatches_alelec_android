package com.fossil;

import com.mapped.Rm6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ax7 extends Fx7 implements Uu7 {
    @DexIgnore
    public /* final */ boolean c; // = u0();

    @DexIgnore
    public Ax7(Rm6 rm6) {
        super(true);
        T(rm6);
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public boolean M() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public boolean N() {
        return true;
    }

    @DexIgnore
    public final boolean u0() {
        Fx7 fx7;
        Qu7 P = P();
        if (!(P instanceof Ru7)) {
            P = null;
        }
        Ru7 ru7 = (Ru7) P;
        if (!(ru7 == null || (fx7 = (Fx7) ru7.e) == null)) {
            while (!fx7.M()) {
                Qu7 P2 = fx7.P();
                if (!(P2 instanceof Ru7)) {
                    P2 = null;
                }
                Ru7 ru72 = (Ru7) P2;
                if (ru72 != null) {
                    fx7 = (Fx7) ru72.e;
                    if (fx7 == null) {
                    }
                }
            }
            return true;
        }
        return false;
    }
}
