package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J11 implements D11 {
    @DexIgnore
    public /* final */ Handler a; // = Qm0.a(Looper.getMainLooper());

    @DexIgnore
    @Override // com.fossil.D11
    public void a(long j, Runnable runnable) {
        this.a.postDelayed(runnable, j);
    }

    @DexIgnore
    @Override // com.fossil.D11
    public void b(Runnable runnable) {
        this.a.removeCallbacks(runnable);
    }
}
