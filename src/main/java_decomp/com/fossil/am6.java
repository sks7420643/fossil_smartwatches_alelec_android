package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.mw5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am6 extends pv5 implements zl6, View.OnClickListener, mw5.b, t47.g {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public mw5 h;
    @DexIgnore
    public g37<p65> i;
    @DexIgnore
    public yl6 j;
    @DexIgnore
    public f67 k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ String y;
    @DexIgnore
    public HashMap z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final am6 a(Date date) {
            pq7.c(date, "date");
            am6 am6 = new am6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            am6.setArguments(bundle);
            return am6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends f67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewEmptySupport e;
        @DexIgnore
        public /* final */ /* synthetic */ am6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerViewEmptySupport recyclerViewEmptySupport, LinearLayoutManager linearLayoutManager, am6 am6, LinearLayoutManager linearLayoutManager2, p65 p65) {
            super(linearLayoutManager);
            this.e = recyclerViewEmptySupport;
            this.f = am6;
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void b(int i) {
            yl6 yl6 = this.f.j;
            if (yl6 != null) {
                yl6.r();
            }
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void c(int i, int i2) {
        }
    }

    @DexIgnore
    public am6() {
        String d = qn5.l.a().d("nonBrandSurface");
        this.t = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = qn5.l.a().d("secondaryText");
        this.u = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("primaryText");
        this.v = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("nonBrandDisableCalendarDay");
        this.w = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("nonBrandNonReachGoal");
        this.x = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("nonBrandSurface");
        this.y = d6 == null ? "#FFFFFF" : d6;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "GoalTrackingDetailFragment";
    }

    @DexIgnore
    public final void L6(p65 p65) {
        p65.F.setOnClickListener(this);
        p65.G.setOnClickListener(this);
        p65.H.setOnClickListener(this);
        if (!TextUtils.isEmpty(this.y)) {
            int parseColor = Color.parseColor(this.y);
            Drawable drawable = PortfolioApp.h0.c().getDrawable(2131230985);
            if (drawable != null) {
                drawable.setTint(parseColor);
                p65.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        p65.q.setOnClickListener(this);
        this.l = qn5.l.a().d("hybridGoalTrackingTab");
        this.m = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.s = qn5.l.a().d("onHybridGoalTrackingTab");
        p65.r.setBackgroundColor(this.t);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        RecyclerViewEmptySupport recyclerViewEmptySupport = p65.N;
        pq7.b(recyclerViewEmptySupport, "it");
        recyclerViewEmptySupport.setLayoutManager(linearLayoutManager);
        RecyclerView.m layoutManager = recyclerViewEmptySupport.getLayoutManager();
        if (layoutManager != null) {
            this.k = new b(recyclerViewEmptySupport, (LinearLayoutManager) layoutManager, this, linearLayoutManager, p65);
            recyclerViewEmptySupport.setAdapter(this.h);
            FlexibleTextView flexibleTextView = p65.O;
            pq7.b(flexibleTextView, "binding.tvNotFound");
            recyclerViewEmptySupport.setEmptyView(flexibleTextView);
            f67 f67 = this.k;
            if (f67 != null) {
                recyclerViewEmptySupport.addOnScrollListener(f67);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore
    public final void M6() {
        p65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        g37<p65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.y) != null) {
            overviewDayGoalChart.D("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(yl6 yl6) {
        pq7.c(yl6, "presenter");
        this.j = yl6;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        GoalTrackingData goalTrackingData;
        yl6 yl6;
        Bundle bundle = null;
        Serializable serializable = null;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 193266439) {
            if (hashCode == 1983205541 && str.equals("GOAL_TRACKING_ADD") && i2 == 2131362260) {
                if (intent != null) {
                    serializable = intent.getSerializableExtra("EXTRA_NUMBER_PICKER_RESULTS");
                }
                if (serializable != null) {
                    HashMap hashMap = (HashMap) serializable;
                    Integer num = (Integer) hashMap.get(2131362892);
                    Integer num2 = (Integer) hashMap.get(2131362893);
                    Integer num3 = (Integer) hashMap.get(2131362896);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("GoalTrackingDetailFragment", "onDialogFragmentResult GOAL_TRACKING_ADD: hour=" + num + ", minute=" + num2 + ", suffix=" + num3);
                    Calendar instance = Calendar.getInstance();
                    pq7.b(instance, "calendar");
                    instance.setTime(this.g);
                    if (num != null && num2 != null && num3 != null) {
                        if (num.intValue() == 12) {
                            num = 0;
                        }
                        instance.set(9, num3.intValue());
                        instance.set(10, num.intValue());
                        instance.set(12, num2.intValue());
                        Calendar instance2 = Calendar.getInstance();
                        pq7.b(instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        pq7.b(time, "Calendar.getInstance().time");
                        long time2 = time.getTime();
                        Date time3 = instance.getTime();
                        pq7.b(time3, "calendar.time");
                        if (time2 > time3.getTime()) {
                            yl6 yl62 = this.j;
                            if (yl62 != null) {
                                Date time4 = instance.getTime();
                                pq7.b(time4, "calendar.time");
                                yl62.n(time4);
                                return;
                            }
                            return;
                        }
                        FragmentManager fragmentManager = getFragmentManager();
                        if (fragmentManager != null) {
                            s37 s37 = s37.c;
                            pq7.b(fragmentManager, "this");
                            s37.B(fragmentManager);
                            return;
                        }
                        return;
                    }
                    return;
                }
                throw new il7("null cannot be cast to non-null type java.util.HashMap<kotlin.Int, kotlin.Int>");
            }
        } else if (str.equals("GOAL_TRACKING_DELETE") && i2 == 2131363373) {
            if (intent != null) {
                bundle = intent.getExtras();
            }
            if (bundle != null && (goalTrackingData = (GoalTrackingData) bundle.getSerializable("GOAL_TRACKING_DELETE_BUNDLE")) != null && (yl6 = this.j) != null) {
                yl6.o(goalTrackingData);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zl6
    public void X0(GoalTrackingSummary goalTrackingSummary) {
        p65 a2;
        int i2;
        int i3;
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailFragment", "showDayDetail - goalTrackingSummary=" + goalTrackingSummary);
        g37<p65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            pq7.b(a2, "binding");
            View n = a2.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            if (goalTrackingSummary != null) {
                i3 = goalTrackingSummary.getTotalTracked();
                i2 = goalTrackingSummary.getGoalTarget();
            } else {
                i2 = 0;
                i3 = 0;
            }
            FlexibleTextView flexibleTextView = a2.A;
            pq7.b(flexibleTextView, "binding.ftvDailyValue");
            flexibleTextView.setText(dl5.c((float) i3, 1));
            FlexibleTextView flexibleTextView2 = a2.z;
            pq7.b(flexibleTextView2, "binding.ftvDailyUnit");
            flexibleTextView2.setText("/ " + i2 + "  " + um5.c(PortfolioApp.h0.c(), 2131886745));
            int i4 = i2 > 0 ? (i3 * 100) / i2 : -1;
            if (i3 >= i2 && i2 > 0) {
                a2.C.setTextColor(this.t);
                a2.B.setTextColor(this.t);
                a2.z.setTextColor(this.t);
                a2.A.setTextColor(this.t);
                RTLImageView rTLImageView = a2.H;
                pq7.b(rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.G;
                pq7.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                pq7.b(constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView3 = a2.C;
                pq7.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setSelected(true);
                FlexibleTextView flexibleTextView4 = a2.B;
                pq7.b(flexibleTextView4, "binding.ftvDayOfMonth");
                flexibleTextView4.setSelected(true);
                View view = a2.J;
                pq7.b(view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView5 = a2.A;
                pq7.b(flexibleTextView5, "binding.ftvDailyValue");
                flexibleTextView5.setSelected(true);
                FlexibleTextView flexibleTextView6 = a2.z;
                pq7.b(flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.O;
                pq7.b(flexibleTextView7, "binding.tvNotFound");
                flexibleTextView7.setVisibility(4);
                String str = this.s;
                if (str != null) {
                    a2.C.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.J.setBackgroundColor(Color.parseColor(str));
                    a2.H.setColorFilter(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.l;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i3 > 0) {
                a2.B.setTextColor(this.v);
                a2.C.setTextColor(this.u);
                a2.z.setTextColor(this.x);
                View view2 = a2.J;
                pq7.b(view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.H;
                pq7.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.G;
                pq7.b(rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                a2.A.setTextColor(this.v);
                FlexibleTextView flexibleTextView8 = a2.O;
                pq7.b(flexibleTextView8, "binding.tvNotFound");
                flexibleTextView8.setVisibility(4);
                int i5 = this.x;
                a2.J.setBackgroundColor(i5);
                a2.H.setColorFilter(i5);
                a2.G.setColorFilter(i5);
                String str3 = this.m;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.B.setTextColor(this.v);
                a2.C.setTextColor(this.u);
                a2.A.setTextColor(this.v);
                a2.z.setTextColor(this.w);
                RTLImageView rTLImageView5 = a2.H;
                pq7.b(rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.G;
                pq7.b(rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                pq7.b(constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView9 = a2.C;
                pq7.b(flexibleTextView9, "binding.ftvDayOfWeek");
                flexibleTextView9.setSelected(false);
                FlexibleTextView flexibleTextView10 = a2.B;
                pq7.b(flexibleTextView10, "binding.ftvDayOfMonth");
                flexibleTextView10.setSelected(false);
                View view3 = a2.J;
                pq7.b(view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView11 = a2.A;
                pq7.b(flexibleTextView11, "binding.ftvDailyValue");
                flexibleTextView11.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.z;
                pq7.b(flexibleTextView12, "binding.ftvDailyUnit");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.O;
                pq7.b(flexibleTextView13, "binding.tvNotFound");
                flexibleTextView13.setVisibility(0);
                int i6 = this.x;
                a2.J.setBackgroundColor(i6);
                a2.H.setColorFilter(i6);
                a2.G.setColorFilter(i6);
                String str4 = this.m;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.L;
                pq7.b(flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView14 = a2.E;
                pq7.b(flexibleTextView14, "binding.ftvProgressValue");
                flexibleTextView14.setText(um5.c(context, 2131887328));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.L;
                pq7.b(flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView15 = a2.E;
                pq7.b(flexibleTextView15, "binding.ftvProgressValue");
                flexibleTextView15.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView16 = a2.D;
            pq7.b(flexibleTextView16, "binding.ftvGoalValue");
            hr7 hr7 = hr7.f1520a;
            String c = um5.c(context, 2131886747);
            pq7.b(c, "LanguageHelper.getString\u2026age_Title__OfNumberTimes)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView16.setText(format);
        }
    }

    @DexIgnore
    @Override // com.fossil.mw5.b
    public void Z2(GoalTrackingData goalTrackingData) {
        pq7.c(goalTrackingData, "item");
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.H(childFragmentManager, goalTrackingData);
    }

    @DexIgnore
    @Override // com.fossil.zl6
    public void d() {
        f67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.zl6
    public void j(Date date, boolean z2, boolean z3, boolean z4) {
        p65 a2;
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4);
        this.g = date;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(date);
        int i2 = instance.get(7);
        g37<p65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            FlexibleTextView flexibleTextView = a2.B;
            pq7.b(flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(instance.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.G;
                pq7.b(rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.G;
                pq7.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.H;
                pq7.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    pq7.b(flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(um5.c(getContext(), 2131886662));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.C;
                pq7.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(jl5.b.i(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.H;
            pq7.b(rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.C;
            pq7.b(flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(jl5.b.i(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.zl6
    public void n(mv5 mv5, ArrayList<String> arrayList) {
        p65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        pq7.c(mv5, "baseModel");
        pq7.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailChart - baseModel=" + mv5);
        g37<p65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.y) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayGoalChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayGoalChart, jl5.b.f(), false, 2, null);
            }
            overviewDayGoalChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.fossil.zl6
    public void n5(cu0<GoalTrackingData> cu0) {
        mw5 mw5;
        pq7.c(cu0, "goalTrackingDataList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailData - goalTrackingDataList=" + cu0);
        g37<p65> g37 = this.i;
        if (g37 != null && g37.a() != null && (mw5 = this.h) != null) {
            mw5.i(cu0);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int i2;
        int i3;
        int i4 = 12;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("GoalTrackingDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131361881:
                    Boolean p0 = lk5.p0(this.g);
                    pq7.b(p0, "DateHelper.isToday(mDate)");
                    if (p0.booleanValue()) {
                        Calendar instance = Calendar.getInstance();
                        int i5 = instance.get(10);
                        i3 = instance.get(12);
                        i2 = instance.get(9);
                        if (i5 != 0) {
                            i4 = i5;
                        }
                    } else {
                        i2 = 0;
                        i3 = 0;
                    }
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    String c = um5.c(PortfolioApp.h0.c(), 2131886102);
                    pq7.b(c, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    if (c != null) {
                        String upperCase = c.toUpperCase();
                        pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                        String c2 = um5.c(PortfolioApp.h0.c(), 2131886104);
                        pq7.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                        if (c2 != null) {
                            String upperCase2 = c2.toUpperCase();
                            pq7.b(upperCase2, "(this as java.lang.String).toUpperCase()");
                            s37.G(childFragmentManager, i4, i3, i2, new String[]{upperCase, upperCase2});
                            return;
                        }
                        throw new il7("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    yl6 yl6 = this.j;
                    if (yl6 != null) {
                        yl6.v();
                        return;
                    }
                    return;
                case 2131362735:
                    yl6 yl62 = this.j;
                    if (yl62 != null) {
                        yl62.u();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        p65 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        p65 p65 = (p65) aq0.f(layoutInflater, 2131558558, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        this.g = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.g = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        this.h = new mw5(this, new nq4());
        pq7.b(p65, "binding");
        L6(p65);
        yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.p(this.g);
        }
        this.i = new g37<>(this, p65);
        M6();
        g37<p65> g37 = this.i;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.q();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.m();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        M6();
        yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.t(this.g);
        }
        yl6 yl62 = this.j;
        if (yl62 != null) {
            yl62.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.s(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.z;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
