package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3784a;
    @DexIgnore
    public WeakReference<Activity> b;
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public d d;
    @DexIgnore
    public /* final */ Application.ActivityLifecycleCallbacks e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ GuestApiService g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            for (byte b : bArr) {
                String num = Integer.toString((b & 255) + 256, 16);
                pq7.b(num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    pq7.b(substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            pq7.b(sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String b() {
            return vm5.h;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0073  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean c(java.lang.String r8, java.lang.String r9) {
            /*
                r7 = this;
                r2 = 0
                r3 = 0
                java.lang.String r0 = "filePath"
                com.fossil.pq7.c(r8, r0)
                if (r9 != 0) goto L_0x000b
                r0 = 1
            L_0x000a:
                return r0
            L_0x000b:
                java.lang.String r0 = "MD5"
                java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ Exception -> 0x0078, all -> 0x007b }
                r1 = 2014(0x7de, float:2.822E-42)
                byte[] r4 = new byte[r1]     // Catch:{ Exception -> 0x0078, all -> 0x007b }
                java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0078, all -> 0x007b }
                r1.<init>(r8)     // Catch:{ Exception -> 0x0078, all -> 0x007b }
            L_0x001a:
                int r2 = r1.read(r4)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                r5 = -1
                if (r2 != r5) goto L_0x003f
                byte[] r0 = r0.digest()     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r2 = "digest.digest()"
                com.fossil.pq7.b(r0, r2)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r0 = r7.a(r0)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r2 = r9.toLowerCase()     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r4 = "(this as java.lang.String).toLowerCase()"
                com.fossil.pq7.b(r2, r4)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                boolean r0 = com.fossil.pq7.a(r2, r0)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                r1.close()
                goto L_0x000a
            L_0x003f:
                if (r2 <= 0) goto L_0x001a
                r5 = 0
                r0.update(r4, r5, r2)
                goto L_0x001a
            L_0x0046:
                r0 = move-exception
            L_0x0047:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0083 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x0083 }
                java.lang.String r4 = r7.b()     // Catch:{ all -> 0x0083 }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0083 }
                r5.<init>()     // Catch:{ all -> 0x0083 }
                java.lang.String r6 = "Error inside "
                r5.append(r6)     // Catch:{ all -> 0x0083 }
                java.lang.String r6 = r7.b()     // Catch:{ all -> 0x0083 }
                r5.append(r6)     // Catch:{ all -> 0x0083 }
                java.lang.String r6 = ".verifyDownloadFile - e="
                r5.append(r6)     // Catch:{ all -> 0x0083 }
                r5.append(r0)     // Catch:{ all -> 0x0083 }
                java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0083 }
                r2.e(r4, r0)     // Catch:{ all -> 0x0083 }
                if (r1 == 0) goto L_0x0076
                r1.close()
            L_0x0076:
                r0 = r3
                goto L_0x000a
            L_0x0078:
                r0 = move-exception
                r1 = r2
                goto L_0x0047
            L_0x007b:
                r0 = move-exception
                r1 = r2
            L_0x007d:
                if (r1 == 0) goto L_0x0082
                r1.close()
            L_0x0082:
                throw r0
            L_0x0083:
                r0 = move-exception
                goto L_0x007d
            L_0x0085:
                r0 = move-exception
                goto L_0x007d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vm5.a.c(java.lang.String, java.lang.String):boolean");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends AsyncTask<String, Void, Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f3785a;
        @DexIgnore
        public /* final */ wm5 b;
        @DexIgnore
        public /* final */ /* synthetic */ vm5 c;

        @DexIgnore
        public b(vm5 vm5, Context context, wm5 wm5) {
            pq7.c(context, "context");
            pq7.c(wm5, Firmware.COLUMN_DOWNLOAD_URL);
            this.c = vm5;
            this.f3785a = context;
            this.b = wm5;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            pq7.c(strArr, "links");
            if (!TextUtils.isEmpty(this.b.b())) {
                try {
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.f3785a.getFilesDir();
                    pq7.b(filesDir, "context.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    if (new File(sb2).exists() && vm5.i.c(sb2, this.b.a())) {
                        return Boolean.TRUE;
                    }
                    URLConnection openConnection = new URL(this.b.b()).openConnection();
                    openConnection.connect();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                    FileOutputStream fileOutputStream = new FileOutputStream(sb2);
                    try {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read == -1) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                break;
                            } else if (isCancelled()) {
                                Boolean bool = Boolean.FALSE;
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                bufferedInputStream.close();
                                return bool;
                            } else {
                                fileOutputStream.write(bArr, 0, read);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Throwable th) {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        bufferedInputStream.close();
                        throw th;
                    }
                    bufferedInputStream.close();
                    if (TextUtils.isEmpty(this.b.a())) {
                        FLogger.INSTANCE.getLocal().e(vm5.i.b(), "Download complete with risk cause by empty checksum");
                        return Boolean.TRUE;
                    } else if (vm5.i.c(sb2, this.b.a())) {
                        return Boolean.TRUE;
                    } else {
                        FLogger.INSTANCE.getLocal().e(vm5.i.b(), "Inconsistent checksum, retry download?");
                        return Boolean.TRUE;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b2 = vm5.i.b();
                    local.e(b2, "Error inside " + vm5.i.b() + ".onHandleIntent - e=" + e2);
                }
            }
            return Boolean.FALSE;
        }

        @DexIgnore
        /* renamed from: b */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    new e().execute(this.b.c());
                }
                d h = this.c.h();
                if (h != null) {
                    h.a(false);
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends AsyncTask<String, Void, Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3786a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ vm5 c;

        @DexIgnore
        public c(vm5 vm5, String str, boolean z) {
            pq7.c(str, "mFilePath");
            this.c = vm5;
            this.f3786a = str;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            boolean z = true;
            pq7.c(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            try {
                if (pq7.a(um5.b(), "en_US")) {
                    ym5.d(this.f3786a, this.b);
                    ym5.d(this.c.j() + "/strings.json", true);
                } else {
                    ym5.d(this.f3786a, this.b);
                }
                ym5.c();
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(vm5.i.b(), "load cache failed e=" + e);
                z = false;
            }
            return Boolean.valueOf(z);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            pq7.c(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            String str = strArr[0];
            vm5 vm5 = vm5.this;
            return Boolean.valueOf(vm5.u(vm5.m()));
        }

        @DexIgnore
        /* renamed from: b */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    vm5.this.p(new File(vm5.this.m().getFilesDir().toString() + ""));
                    vm5 vm5 = vm5.this;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = vm5.this.m().getFilesDir();
                    pq7.b(filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append(vm5.this.k());
                    vm5.f(sb.toString(), true);
                }
                d h = vm5.this.h();
                if (h != null) {
                    h.a(bool.booleanValue());
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public /* final */ /* synthetic */ vm5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, f fVar) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.this$0.b.m().getFilesDir();
                    pq7.b(filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    File file = new File(sb2);
                    if (!file.exists()) {
                        vm5 vm5 = this.this$0.b;
                        this.L$0 = iv7;
                        this.L$1 = sb2;
                        this.L$2 = file;
                        this.label = 1;
                        if (vm5.e(this) == d) {
                            return d;
                        }
                    }
                } else if (i == 1) {
                    File file2 = (File) this.L$2;
                    String str = (String) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(vm5 vm5) {
            this.b = vm5;
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            Class<?> cls;
            if (activity != null) {
                this.b.r(new WeakReference<>(activity));
                WeakReference<Activity> n = this.b.n();
                if (n != null) {
                    Activity activity2 = n.get();
                    if (pq7.a((activity2 == null || (cls = activity2.getClass()) == null) ? null : cls.getSimpleName(), this.b.l())) {
                        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(null, this), 3, null);
                        return;
                    }
                    return;
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
            pq7.c(activity, Constants.ACTIVITY);
            if (this.b.n() != null) {
                WeakReference<Activity> n = this.b.n();
                if (n == null) {
                    pq7.i();
                    throw null;
                } else if (pq7.a(n.get(), activity)) {
                    this.b.r(null);
                }
            }
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            pq7.c(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
            pq7.c(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            pq7.c(activity, Constants.ACTIVITY);
            pq7.c(bundle, "bundle");
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
            pq7.c(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            pq7.c(activity, Constants.ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.localization.LocalizationManager", f = "LocalizationManager.kt", l = {194}, m = "downloadLanguagePack")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(vm5 vm5, qn7 qn7) {
            super(qn7);
            this.this$0 = vm5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.localization.LocalizationManager$downloadLanguagePack$response$1", f = "LocalizationManager.kt", l = {194}, m = "invokeSuspend")
    public static final class h extends ko7 implements rp7<qn7<? super q88<ApiResponse<gj4>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ vm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(vm5 vm5, qn7 qn7) {
            super(1, qn7);
            this.this$0 = vm5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new h(this.this$0, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<gj4>>> qn7) {
            throw null;
            //return ((h) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                GuestApiService guestApiService = this.this$0.g;
                String str = this.this$0.f;
                this.label = 1;
                Object localizations = guestApiService.getLocalizations(str, "android", this);
                return localizations == d ? d : localizations;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = vm5.class.getSimpleName();
        pq7.b(simpleName, "LocalizationManager::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public vm5(Application application, String str, GuestApiService guestApiService) {
        pq7.c(application, "app");
        pq7.c(str, "mAppVersion");
        pq7.c(guestApiService, "mGuestApiService");
        this.f = str;
        this.g = guestApiService;
        pq7.b(application.getSharedPreferences(application.getPackageName() + ".language", 0), "app.getSharedPreferences\u2026e\", Context.MODE_PRIVATE)");
        this.f3784a = application;
        this.e = new f(this);
    }

    @DexIgnore
    public final void d(wm5 wm5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "downloadFile response=" + wm5);
        new b(this, this.f3784a, wm5).execute(new String[0]);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r8 instanceof com.fossil.vm5.g
            if (r0 == 0) goto L_0x0088
            r0 = r8
            com.fossil.vm5$g r0 = (com.fossil.vm5.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0088
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0096
            if (r3 != r6) goto L_0x008e
            java.lang.Object r0 = r0.L$0
            com.fossil.vm5 r0 = (com.fossil.vm5) r0
            com.fossil.el7.b(r1)
            r7 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x00b8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.vm5.h
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "download language success isFromCache "
            r3.append(r4)
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            boolean r4 = r0.b()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            boolean r1 = r0.b()
            if (r1 != 0) goto L_0x007e
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x007e
            java.util.List r0 = r0.get_items()
            if (r0 == 0) goto L_0x007e
            boolean r1 = r0.isEmpty()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x007e
            com.fossil.wm5 r1 = new com.fossil.wm5
            r1.<init>()
            java.lang.Object r0 = r0.get(r5)
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            r1.d(r0)
            r7.d(r1)
        L_0x007e:
            com.fossil.vm5$d r0 = r7.d
            if (r0 == 0) goto L_0x0085
            r0.a(r5)
        L_0x0085:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0087:
            return r0
        L_0x0088:
            com.fossil.vm5$g r0 = new com.fossil.vm5$g
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x008e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0096:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.fossil.vm5.h
            java.lang.String r4 = "downloadLanguagePack() called"
            r1.d(r3, r4)
            com.fossil.vm5$h r1 = new com.fossil.vm5$h
            r3 = 0
            r1.<init>(r7, r3)
            r0.L$0 = r7
            r0.label = r6
            java.lang.Object r1 = com.fossil.jq5.d(r1, r0)
            if (r1 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0087
        L_0x00b8:
            boolean r0 = r0 instanceof com.fossil.hq5
            if (r0 == 0) goto L_0x0085
            com.fossil.vm5$d r0 = r7.d
            if (r0 == 0) goto L_0x0085
            r0.a(r5)
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vm5.e(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void f(String str, boolean z) {
        pq7.c(str, "path");
        new c(this, str, z).execute(new String[0]);
    }

    @DexIgnore
    public final Application.ActivityLifecycleCallbacks g() {
        return this.e;
    }

    @DexIgnore
    public final d h() {
        return this.d;
    }

    @DexIgnore
    public final String i() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.f3784a.getFilesDir();
        pq7.b(filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append("/");
        sb.append(k());
        return sb.toString();
    }

    @DexIgnore
    public final String j() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.f3784a.getFilesDir();
        pq7.b(filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append("/");
        sb.append("language");
        sb.append("/values");
        return sb.toString();
    }

    @DexIgnore
    public final String k() {
        StringBuilder sb = new StringBuilder("");
        StringBuilder sb2 = new StringBuilder();
        File filesDir = this.f3784a.getFilesDir();
        pq7.b(filesDir, "mContext.filesDir");
        sb2.append(filesDir.getAbsolutePath());
        sb2.append("/");
        sb2.append("language");
        File file = new File(sb2.toString());
        if (file.exists()) {
            String[] list = file.list();
            pq7.b(list, "file.list()");
            Locale a2 = um5.a();
            int length = list.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                String str = list[i2];
                pq7.b(a2, "l");
                String language = a2.getLanguage();
                pq7.b(language, "l.language");
                if (wt7.v(str, language, false, 2, null)) {
                    String sb3 = sb.toString();
                    pq7.b(sb3, "path.toString()");
                    String language2 = a2.getLanguage();
                    pq7.b(language2, "l.language");
                    if (!wt7.v(sb3, language2, false, 2, null)) {
                        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb.append(a2.getLanguage());
                    }
                    String country = a2.getCountry();
                    pq7.b(country, "l.country");
                    if (wt7.v(str, country, false, 2, null)) {
                        sb.append("-r");
                        sb.append(a2.getCountry());
                        break;
                    }
                }
                i2++;
            }
        }
        sb.insert(0, "language/values");
        sb.append("/strings.json");
        FLogger.INSTANCE.getLocal().d(h, "path=" + sb.toString());
        String sb4 = sb.toString();
        pq7.b(sb4, "path.toString()");
        return sb4;
    }

    @DexIgnore
    public final String l() {
        return this.c;
    }

    @DexIgnore
    public final Context m() {
        return this.f3784a;
    }

    @DexIgnore
    public final WeakReference<Activity> n() {
        return this.b;
    }

    @DexIgnore
    public final void o(String str, String str2) {
        File file = new File(str + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Activity activity;
        pq7.c(context, "context");
        if (intent != null && !TextUtils.isEmpty(intent.getAction()) && pq7.a(intent.getAction(), "android.intent.action.LOCALE_CHANGED")) {
            String J = PortfolioApp.h0.c().J();
            if (nk5.o.x(J)) {
                PortfolioApp.h0.c().A1(J);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "onReceive locale=" + um5.b());
            WeakReference<Activity> weakReference = this.b;
            if (!(weakReference == null || (activity = weakReference.get()) == null)) {
                pq7.b(activity, "it");
                if (!activity.isFinishing()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = h;
                    local2.d(str2, "onReceive finish activity=" + activity.getLocalClassName());
                    activity.setResult(0);
                    activity.finishAffinity();
                }
            }
            PortfolioApp.h0.c().t0();
            q();
        }
    }

    @DexIgnore
    public final void p(File file) {
        pq7.c(file, "folder");
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (File file2 : listFiles) {
                pq7.b(file2, "f");
                if (file2.isDirectory()) {
                    p(file2);
                }
            }
        }
    }

    @DexIgnore
    public final void q() {
        ym5.a();
        String i2 = i();
        if (new File(i2).exists()) {
            f(i2, true);
        } else {
            f(k(), false);
        }
    }

    @DexIgnore
    public final void r(WeakReference<Activity> weakReference) {
        this.b = weakReference;
    }

    @DexIgnore
    public final vm5 s(d dVar) {
        this.d = dVar;
        return this;
    }

    @DexIgnore
    public final void t(String str) {
        pq7.c(str, "splashScreen");
        this.c = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x009a, code lost:
        if (r0 != null) goto L_0x009c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0101  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean u(android.content.Context r10) {
        /*
        // Method dump skipped, instructions count: 300
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vm5.u(android.content.Context):boolean");
    }
}
