package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L76 implements Factory<WatchAppEditViewModel> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> b;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> c;
    @DexIgnore
    public /* final */ Provider<An4> d;

    @DexIgnore
    public L76(Provider<DianaPresetRepository> provider, Provider<DianaAppSettingRepository> provider2, Provider<WatchAppRepository> provider3, Provider<An4> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static L76 a(Provider<DianaPresetRepository> provider, Provider<DianaAppSettingRepository> provider2, Provider<WatchAppRepository> provider3, Provider<An4> provider4) {
        return new L76(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static WatchAppEditViewModel c(DianaPresetRepository dianaPresetRepository, DianaAppSettingRepository dianaAppSettingRepository, WatchAppRepository watchAppRepository, An4 an4) {
        return new WatchAppEditViewModel(dianaPresetRepository, dianaAppSettingRepository, watchAppRepository, an4);
    }

    @DexIgnore
    public WatchAppEditViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
