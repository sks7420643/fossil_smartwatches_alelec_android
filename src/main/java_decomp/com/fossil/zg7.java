package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zg7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ Jg7 c;

    @DexIgnore
    public Zg7(Context context, Jg7 jg7) {
        this.b = context;
        this.c = jg7;
    }

    @DexIgnore
    public final void run() {
        try {
            Ig7.a(this.b, false, this.c);
        } catch (Throwable th) {
            Ig7.m.e(th);
        }
    }
}
