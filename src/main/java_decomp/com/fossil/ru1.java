package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ru1 extends Iw1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ru1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Ru1 a(Parcel parcel) {
            return new Ru1(parcel, null);
        }

        @DexIgnore
        public final Ru1 b(byte[] bArr) {
            try {
                Iw1 iw1 = (Iw1) Ga.d.f(bArr);
                if (iw1 instanceof Ru1) {
                    return (Ru1) iw1;
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        @DexIgnore
        public Ru1[] c(int i) {
            return new Ru1[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ru1 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ru1[] newArray(int i) {
            return c(i);
        }
    }

    @DexIgnore
    public /* synthetic */ Ru1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        j();
    }

    @DexIgnore
    public Ru1(Ry1 ry1, Yb0 yb0, Cc0[] cc0Arr, Cc0[] cc0Arr2, Cc0[] cc0Arr3, Cc0[] cc0Arr4, Cc0[] cc0Arr5, Cc0[] cc0Arr6, Cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        j();
    }

    @DexIgnore
    @Override // com.fossil.Iw1, com.fossil.Iw1, java.lang.Object
    public Ru1 clone() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Iw1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ru1)) {
            return false;
        }
        Ru1 ru1 = (Ru1) obj;
        if (!Wg6.a(getBundleId(), ru1.getBundleId())) {
            return false;
        }
        return getPackageCrc() == ru1.getPackageCrc();
    }

    @DexIgnore
    @Override // com.fossil.Iw1
    public int hashCode() {
        return (getBundleId().hashCode() * 31) + Long.valueOf(getPackageCrc()).hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
        if ((!(f().length == 0)) != false) goto L_0x0018;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void j() {
        /*
            r4 = this;
            r0 = 1
            r1 = 0
            com.fossil.Yb0 r2 = r4.g()
            com.fossil.Jw1 r2 = r2.b
            com.fossil.Jw1 r3 = com.fossil.Jw1.WATCH_APP
            if (r2 != r3) goto L_0x0029
            com.fossil.Cc0[] r2 = r4.f()
            int r2 = r2.length
            if (r2 != 0) goto L_0x001b
            r2 = r0
        L_0x0014:
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x0029
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            return
        L_0x001b:
            r2 = r1
            goto L_0x0014
        L_0x001d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Incorrect package type."
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0029:
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ru1.j():void");
    }

    @DexIgnore
    @Override // com.fossil.Iw1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.U0, Hy1.k((int) getPackageCrc(), null, 1, null)), Jd0.M4, getBundleId());
    }
}
