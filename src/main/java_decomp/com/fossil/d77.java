package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface D77 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    void b(Fb7 fb7, int i);

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    void d(Fb7 fb7);

    @DexIgnore
    Object onCancel();  // void declaration
}
