package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.M62;
import com.fossil.M62.Di;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P92<O extends M62.Di> extends D82 {
    @DexIgnore
    public /* final */ Q62<O> c;

    @DexIgnore
    public P92(Q62<O> q62) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = q62;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T i(T t) {
        this.c.d(t);
        return t;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t) {
        this.c.h(t);
        return t;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final Context l() {
        return this.c.k();
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final Looper m() {
        return this.c.m();
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void s(Da2 da2) {
    }
}
