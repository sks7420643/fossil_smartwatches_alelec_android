package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xi2 implements Parcelable.Creator<Yi2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Yi2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        IBinder iBinder = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 1) {
                Ad2.B(parcel, t);
            } else {
                iBinder = Ad2.u(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Yi2(iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Yi2[] newArray(int i) {
        return new Yi2[i];
    }
}
