package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ig0;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lx3 implements Ig0 {
    @DexIgnore
    public Cg0 b;
    @DexIgnore
    public BottomNavigationMenuView c;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<Ai> CREATOR; // = new Aii();
        @DexIgnore
        public int b;
        @DexIgnore
        public Fz3 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Parcelable.Creator<Ai> {
            @DexIgnore
            public Ai a(Parcel parcel) {
                return new Ai(parcel);
            }

            @DexIgnore
            public Ai[] b(int i) {
                return new Ai[i];
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Ai createFromParcel(Parcel parcel) {
                return a(parcel);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Ai[] newArray(int i) {
                return b(i);
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public Ai(Parcel parcel) {
            this.b = parcel.readInt();
            this.c = (Fz3) parcel.readParcelable(Ai.class.getClassLoader());
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.b);
            parcel.writeParcelable(this.c, 0);
        }
    }

    @DexIgnore
    public void a(BottomNavigationMenuView bottomNavigationMenuView) {
        this.c = bottomNavigationMenuView;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void b(Cg0 cg0, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void c(boolean z) {
        if (!this.d) {
            if (z) {
                this.c.d();
            } else {
                this.c.k();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean e(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean f(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void g(Ig0.Ai ai) {
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public int getId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void h(Context context, Cg0 cg0) {
        this.b = cg0;
        this.c.b(cg0);
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void i(Parcelable parcelable) {
        if (parcelable instanceof Ai) {
            Ai ai = (Ai) parcelable;
            this.c.j(ai.b);
            this.c.setBadgeDrawables(Ix3.b(this.c.getContext(), ai.c));
        }
    }

    @DexIgnore
    public void j(int i) {
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean k(Ng0 ng0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public Parcelable l() {
        Ai ai = new Ai();
        ai.b = this.c.getSelectedItemId();
        ai.c = Ix3.c(this.c.getBadgeDrawables());
        return ai;
    }

    @DexIgnore
    public void m(boolean z) {
        this.d = z;
    }
}
