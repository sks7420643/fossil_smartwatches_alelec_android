package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z53 implements Xw2<C63> {
    @DexIgnore
    public static Z53 c; // = new Z53();
    @DexIgnore
    public /* final */ Xw2<C63> b;

    @DexIgnore
    public Z53() {
        this(Ww2.b(new B63()));
    }

    @DexIgnore
    public Z53(Xw2<C63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((C63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((C63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ C63 zza() {
        return this.b.zza();
    }
}
