package com.fossil;

import android.content.Context;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cz3 extends Cg0 {
    @DexIgnore
    public Cz3(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // android.view.Menu, com.fossil.Cg0
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        Eg0 eg0 = (Eg0) a(i, i2, i3, charSequence);
        Ez3 ez3 = new Ez3(w(), this, eg0);
        eg0.x(ez3);
        return ez3;
    }
}
