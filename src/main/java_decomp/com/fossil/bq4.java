package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bq4 implements Factory<Xn5> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Bq4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Bq4 a(Uo4 uo4) {
        return new Bq4(uo4);
    }

    @DexIgnore
    public static Xn5 c(Uo4 uo4) {
        Xn5 I = uo4.I();
        Lk7.c(I, "Cannot return null from a non-@Nullable @Provides method");
        return I;
    }

    @DexIgnore
    public Xn5 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
