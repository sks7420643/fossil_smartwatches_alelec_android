package com.fossil;

import com.google.android.gms.common.data.DataHolder;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pb2<T> implements Qb2<T> {
    @DexIgnore
    public /* final */ DataHolder b;

    @DexIgnore
    public Pb2(DataHolder dataHolder) {
        this.b = dataHolder;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
        release();
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<T> iterator() {
        return new Rb2(this);
    }

    @DexIgnore
    @Override // com.fossil.V62
    public void release() {
        DataHolder dataHolder = this.b;
        if (dataHolder != null) {
            dataHolder.close();
        }
    }
}
