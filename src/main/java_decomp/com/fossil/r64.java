package com.fossil;

import com.fossil.M64;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R64 {
    @DexIgnore
    public Set<String> a; // = new HashSet();
    @DexIgnore
    public M64.Bi b;
    @DexIgnore
    public Fg3 c;
    @DexIgnore
    public Q64 d;

    @DexIgnore
    public R64(Fg3 fg3, M64.Bi bi) {
        this.b = bi;
        this.c = fg3;
        Q64 q64 = new Q64(this);
        this.d = q64;
        this.c.b(q64);
    }
}
