package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gu5 extends CoroutineUseCase<Di, Ei, Bi> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ Ci f; // = new Ci();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Gu5.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(Gu5.h.a(), "Inside .onReceive");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal());
            if (Gu5.this.o() && Wg6.a(Gu5.this.n(), stringExtra) && intExtra == CommunicateMode.READ_RSSI.ordinal()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = Gu5.h.a();
                local.d(a2, "onReceive - blePhase: " + intExtra + " - deviceId: " + stringExtra + " - mIsExecuted: " + Gu5.this.o());
                Gu5.this.r(false);
                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal() || intent.getExtras() == null) {
                    FLogger.INSTANCE.getLocal().e(Gu5.h.a(), "Inside .onReceive return error");
                    Gu5.this.i(new Bi());
                    return;
                }
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int i = extras.getInt("rssi", 0);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = Gu5.h.a();
                    local2.d(a3, "Inside .onReceive return rssi=" + i);
                    Gu5.this.j(new Ei(i));
                    return;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Di(String str) {
            Wg6.c(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Ei(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = Gu5.class.getSimpleName();
        Wg6.b(simpleName, "GetRssi::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return g;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Di di, Xe6 xe6) {
        return q(di, xe6);
    }

    @DexIgnore
    public final String n() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        Wg6.n("mDeviceId");
        throw null;
    }

    @DexIgnore
    public final boolean o() {
        return this.d;
    }

    @DexIgnore
    public final void p() {
        BleCommandResultManager.d.e(this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public Object q(Di di, Xe6<Object> xe6) {
        String str;
        String str2 = null;
        try {
            FLogger.INSTANCE.getLocal().d(g, "Inside .run");
            this.d = true;
            if (di == null || (str = di.a()) == null) {
                str = "";
            }
            this.e = str;
            if (PortfolioApp.get.b() != null) {
                IButtonConnectivity b = PortfolioApp.get.b();
                if (b != null) {
                    if (di != null) {
                        str2 = di.a();
                    }
                    return Ao7.f(b.deviceGetRssi(str2));
                }
                Wg6.i();
                throw null;
            }
            FLogger.INSTANCE.getLocal().e(g, "Inside .run ButtonApi is null");
            return new Bi();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.e(str3, "Inside .run caught exception=" + e2);
            return new Bi();
        }
    }

    @DexIgnore
    public final void r(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void s() {
        BleCommandResultManager.d.j(this.f, CommunicateMode.READ_RSSI);
    }
}
