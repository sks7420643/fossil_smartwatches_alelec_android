package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ye3 implements Parcelable.Creator<Ce3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ce3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        Float f = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l == 3) {
                iBinder = Ad2.u(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                f = Ad2.s(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ce3(i, iBinder, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ce3[] newArray(int i) {
        return new Ce3[i];
    }
}
