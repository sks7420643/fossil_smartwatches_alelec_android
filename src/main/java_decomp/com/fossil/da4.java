package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Da4 extends Ta4.Di {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ Long d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ Ta4.Di.Aii f;
    @DexIgnore
    public /* final */ Ta4.Di.Fii g;
    @DexIgnore
    public /* final */ Ta4.Di.Eii h;
    @DexIgnore
    public /* final */ Ta4.Di.Cii i;
    @DexIgnore
    public /* final */ Ua4<Ta4.Di.Dii> j;
    @DexIgnore
    public /* final */ int k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Bii {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Long c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Ta4.Di.Aii f;
        @DexIgnore
        public Ta4.Di.Fii g;
        @DexIgnore
        public Ta4.Di.Eii h;
        @DexIgnore
        public Ta4.Di.Cii i;
        @DexIgnore
        public Ua4<Ta4.Di.Dii> j;
        @DexIgnore
        public Integer k;

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public Bi(Ta4.Di di) {
            this.a = di.f();
            this.b = di.h();
            this.c = Long.valueOf(di.k());
            this.d = di.d();
            this.e = Boolean.valueOf(di.m());
            this.f = di.b();
            this.g = di.l();
            this.h = di.j();
            this.i = di.c();
            this.j = di.e();
            this.k = Integer.valueOf(di.g());
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di a() {
            String str = "";
            if (this.a == null) {
                str = " generator";
            }
            if (this.b == null) {
                str = str + " identifier";
            }
            if (this.c == null) {
                str = str + " startedAt";
            }
            if (this.e == null) {
                str = str + " crashed";
            }
            if (this.f == null) {
                str = str + " app";
            }
            if (this.k == null) {
                str = str + " generatorType";
            }
            if (str.isEmpty()) {
                return new Da4(this.a, this.b, this.c.longValue(), this.d, this.e.booleanValue(), this.f, this.g, this.h, this.i, this.j, this.k.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii b(Ta4.Di.Aii aii) {
            if (aii != null) {
                this.f = aii;
                return this;
            }
            throw new NullPointerException("Null app");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii c(boolean z) {
            this.e = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii d(Ta4.Di.Cii cii) {
            this.i = cii;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii e(Long l) {
            this.d = l;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii f(Ua4<Ta4.Di.Dii> ua4) {
            this.j = ua4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii g(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null generator");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii h(int i2) {
            this.k = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii i(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii k(Ta4.Di.Eii eii) {
            this.h = eii;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii l(long j2) {
            this.c = Long.valueOf(j2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Bii
        public Ta4.Di.Bii m(Ta4.Di.Fii fii) {
            this.g = fii;
            return this;
        }
    }

    @DexIgnore
    public Da4(String str, String str2, long j2, Long l, boolean z, Ta4.Di.Aii aii, Ta4.Di.Fii fii, Ta4.Di.Eii eii, Ta4.Di.Cii cii, Ua4<Ta4.Di.Dii> ua4, int i2) {
        this.a = str;
        this.b = str2;
        this.c = j2;
        this.d = l;
        this.e = z;
        this.f = aii;
        this.g = fii;
        this.h = eii;
        this.i = cii;
        this.j = ua4;
        this.k = i2;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Ta4.Di.Aii b() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Ta4.Di.Cii c() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Long d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Ua4<Ta4.Di.Dii> e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Long l;
        Ta4.Di.Fii fii;
        Ta4.Di.Eii eii;
        Ta4.Di.Cii cii;
        Ua4<Ta4.Di.Dii> ua4;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di)) {
            return false;
        }
        Ta4.Di di = (Ta4.Di) obj;
        return this.a.equals(di.f()) && this.b.equals(di.h()) && this.c == di.k() && ((l = this.d) != null ? l.equals(di.d()) : di.d() == null) && this.e == di.m() && this.f.equals(di.b()) && ((fii = this.g) != null ? fii.equals(di.l()) : di.l() == null) && ((eii = this.h) != null ? eii.equals(di.j()) : di.j() == null) && ((cii = this.i) != null ? cii.equals(di.c()) : di.c() == null) && ((ua4 = this.j) != null ? ua4.equals(di.e()) : di.e() == null) && this.k == di.g();
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public String f() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public int g() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public String h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        int hashCode = this.a.hashCode();
        int hashCode2 = this.b.hashCode();
        long j2 = this.c;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        Long l = this.d;
        int hashCode3 = l == null ? 0 : l.hashCode();
        int i4 = this.e ? 1231 : 1237;
        int hashCode4 = this.f.hashCode();
        Ta4.Di.Fii fii = this.g;
        int hashCode5 = fii == null ? 0 : fii.hashCode();
        Ta4.Di.Eii eii = this.h;
        int hashCode6 = eii == null ? 0 : eii.hashCode();
        Ta4.Di.Cii cii = this.i;
        int hashCode7 = cii == null ? 0 : cii.hashCode();
        Ua4<Ta4.Di.Dii> ua4 = this.j;
        if (ua4 != null) {
            i2 = ua4.hashCode();
        }
        return ((((((((((((((hashCode3 ^ ((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ i3) * 1000003)) * 1000003) ^ i4) * 1000003) ^ hashCode4) * 1000003) ^ hashCode5) * 1000003) ^ hashCode6) * 1000003) ^ hashCode7) * 1000003) ^ i2) * 1000003) ^ this.k;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Ta4.Di.Eii j() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public long k() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Ta4.Di.Fii l() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public boolean m() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di
    public Ta4.Di.Bii n() {
        return new Bi(this);
    }

    @DexIgnore
    public String toString() {
        return "Session{generator=" + this.a + ", identifier=" + this.b + ", startedAt=" + this.c + ", endedAt=" + this.d + ", crashed=" + this.e + ", app=" + this.f + ", user=" + this.g + ", os=" + this.h + ", device=" + this.i + ", events=" + this.j + ", generatorType=" + this.k + "}";
    }
}
