package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.fossil.Yi0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Si0 extends Ui0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Yi0.Ai {
        @DexIgnore
        public Ai(Si0 si0) {
        }

        @DexIgnore
        @Override // com.fossil.Yi0.Ai
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @DexIgnore
    @Override // com.fossil.Wi0, com.fossil.Ui0
    public void j() {
        Yi0.r = new Ai(this);
    }
}
