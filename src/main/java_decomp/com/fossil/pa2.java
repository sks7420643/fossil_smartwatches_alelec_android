package com.fossil;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pa2 {
    @DexIgnore
    public /* final */ Zi0<G72<?>, Z52> a; // = new Zi0<>();
    @DexIgnore
    public /* final */ Zi0<G72<?>, String> b; // = new Zi0<>();
    @DexIgnore
    public /* final */ Ot3<Map<G72<?>, String>> c; // = new Ot3<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public Pa2(Iterable<? extends S62<?>> iterable) {
        Iterator<? extends S62<?>> it = iterable.iterator();
        while (it.hasNext()) {
            this.a.put(((S62) it.next()).a(), null);
        }
        this.d = this.a.keySet().size();
    }

    @DexIgnore
    public final Nt3<Map<G72<?>, String>> a() {
        return this.c.a();
    }

    @DexIgnore
    public final void b(G72<?> g72, Z52 z52, String str) {
        this.a.put(g72, z52);
        this.b.put(g72, str);
        this.d--;
        if (!z52.A()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.b(new O62(this.a));
            return;
        }
        this.c.c(this.b);
    }

    @DexIgnore
    public final Set<G72<?>> c() {
        return this.a.keySet();
    }
}
