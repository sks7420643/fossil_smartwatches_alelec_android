package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public class Gy0 extends ViewGroup implements Dy0 {
    @DexIgnore
    public ViewGroup b;
    @DexIgnore
    public View c;
    @DexIgnore
    public /* final */ View d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Matrix f;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener g; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            View view;
            Mo0.b0(Gy0.this);
            Gy0 gy0 = Gy0.this;
            ViewGroup viewGroup = gy0.b;
            if (viewGroup == null || (view = gy0.c) == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            Mo0.b0(Gy0.this.b);
            Gy0 gy02 = Gy0.this;
            gy02.b = null;
            gy02.c = null;
            return true;
        }
    }

    @DexIgnore
    public Gy0(View view) {
        super(view.getContext());
        this.d = view;
        setWillNotDraw(false);
        setLayerType(2, null);
    }

    @DexIgnore
    public static Gy0 b(View view, ViewGroup viewGroup, Matrix matrix) {
        int i;
        Gy0 gy0;
        Ey0 ey0;
        Ey0 ey02;
        if (view.getParent() instanceof ViewGroup) {
            Ey0 b2 = Ey0.b(viewGroup);
            Gy0 e2 = e(view);
            if (e2 == null || (ey02 = (Ey0) e2.getParent()) == b2) {
                i = 0;
                gy0 = e2;
            } else {
                int i2 = e2.e;
                ey02.removeView(e2);
                gy0 = null;
                i = i2;
            }
            if (gy0 == null) {
                if (matrix == null) {
                    matrix = new Matrix();
                    c(view, viewGroup, matrix);
                }
                Gy0 gy02 = new Gy0(view);
                gy02.h(matrix);
                if (b2 == null) {
                    ey0 = new Ey0(viewGroup);
                } else {
                    b2.g();
                    ey0 = b2;
                }
                d(viewGroup, ey0);
                d(viewGroup, gy02);
                ey0.a(gy02);
                gy02.e = i;
                gy0 = gy02;
            } else if (matrix != null) {
                gy0.h(matrix);
            }
            gy0.e++;
            return gy0;
        }
        throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
    }

    @DexIgnore
    public static void c(View view, ViewGroup viewGroup, Matrix matrix) {
        ViewGroup viewGroup2 = (ViewGroup) view.getParent();
        matrix.reset();
        Hz0.j(viewGroup2, matrix);
        matrix.preTranslate((float) (-viewGroup2.getScrollX()), (float) (-viewGroup2.getScrollY()));
        Hz0.k(viewGroup, matrix);
    }

    @DexIgnore
    public static void d(View view, View view2) {
        Hz0.g(view2, view2.getLeft(), view2.getTop(), view2.getLeft() + view.getWidth(), view2.getTop() + view.getHeight());
    }

    @DexIgnore
    public static Gy0 e(View view) {
        return (Gy0) view.getTag(Ny0.ghost_view);
    }

    @DexIgnore
    public static void f(View view) {
        Gy0 e2 = e(view);
        if (e2 != null) {
            int i = e2.e - 1;
            e2.e = i;
            if (i <= 0) {
                ((Ey0) e2.getParent()).removeView(e2);
            }
        }
    }

    @DexIgnore
    public static void g(View view, Gy0 gy0) {
        view.setTag(Ny0.ghost_view, gy0);
    }

    @DexIgnore
    @Override // com.fossil.Dy0
    public void a(ViewGroup viewGroup, View view) {
        this.b = viewGroup;
        this.c = view;
    }

    @DexIgnore
    public void h(Matrix matrix) {
        this.f = matrix;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        g(this.d, this);
        this.d.getViewTreeObserver().addOnPreDrawListener(this.g);
        Hz0.i(this.d, 4);
        if (this.d.getParent() != null) {
            ((View) this.d.getParent()).invalidate();
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.d.getViewTreeObserver().removeOnPreDrawListener(this.g);
        Hz0.i(this.d, 0);
        g(this.d, null);
        if (this.d.getParent() != null) {
            ((View) this.d.getParent()).invalidate();
        }
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        Zx0.a(canvas, true);
        canvas.setMatrix(this.f);
        Hz0.i(this.d, 0);
        this.d.invalidate();
        Hz0.i(this.d, 4);
        drawChild(canvas, this.d, getDrawingTime());
        Zx0.a(canvas, false);
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.Dy0
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (e(this.d) == this) {
            Hz0.i(this.d, i == 0 ? 4 : 0);
        }
    }
}
