package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F97 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public F97(String str, String str2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "imageBase64");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof F97) {
                F97 f97 = (F97) obj;
                if (!Wg6.a(this.a, f97.a) || !Wg6.a(this.b, f97.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WFBackgroundPhotoDraft(id=" + this.a + ", imageBase64=" + this.b + ")";
    }
}
