package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.fossil.L56;
import com.fossil.Tq4;
import com.mapped.AppWrapper;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I56 extends C56 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public /* final */ List<AppWrapper> e; // = new ArrayList();
    @DexIgnore
    public /* final */ D56 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<String> h;
    @DexIgnore
    public /* final */ Uq4 i;
    @DexIgnore
    public /* final */ L56 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return I56.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<AppWrapper, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ I56 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(I56 i56) {
            super(1);
            this.this$0 = i56;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(AppWrapper appWrapper) {
            return Boolean.valueOf(invoke(appWrapper));
        }

        @DexIgnore
        public final boolean invoke(AppWrapper appWrapper) {
            Wg6.c(appWrapper, "it");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected != null) {
                return isSelected.booleanValue() && appWrapper.getCurrentHandGroup() == this.this$0.g;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<AppWrapper, String> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ String invoke(AppWrapper appWrapper) {
            return invoke(appWrapper);
        }

        @DexIgnore
        public final String invoke(AppWrapper appWrapper) {
            Wg6.c(appWrapper, "it");
            return String.valueOf(appWrapper.getUri());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Tq4.Di<L56.Ai, Tq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ I56 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(I56 i56) {
            this.a = i56;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Tq4.Ai ai) {
            Wg6.c(ai, "errorResponse");
            FLogger.INSTANCE.getLocal().d(I56.l.a(), "mGetApps onError");
            this.a.f.k();
        }

        @DexIgnore
        public void c(L56.Ai ai) {
            InstalledApp installedApp;
            Wg6.c(ai, "successResponse");
            FLogger.INSTANCE.getLocal().d(I56.l.a(), "mGetApps onSuccess");
            ArrayList<AppWrapper> arrayList = new ArrayList(ai.a());
            for (AppWrapper appWrapper : arrayList) {
                if (appWrapper.getUri() != null) {
                    if (this.a.h.contains(String.valueOf(appWrapper.getUri()))) {
                        InstalledApp installedApp2 = appWrapper.getInstalledApp();
                        if (installedApp2 != null) {
                            installedApp2.setSelected(true);
                        }
                        appWrapper.setCurrentHandGroup(this.a.g);
                    } else if (appWrapper.getCurrentHandGroup() == this.a.g && (installedApp = appWrapper.getInstalledApp()) != null) {
                        installedApp.setSelected(false);
                    }
                }
            }
            this.a.u().addAll(arrayList);
            this.a.f.y2(this.a.u(), this.a.g);
            this.a.f.k();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(L56.Ai ai) {
            c(ai);
        }
    }

    /*
    static {
        String simpleName = I56.class.getSimpleName();
        Wg6.b(simpleName, "NotificationHybridAppPre\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public I56(D56 d56, int i2, ArrayList<String> arrayList, Uq4 uq4, L56 l56) {
        Wg6.c(d56, "mView");
        Wg6.c(arrayList, "mListAppsSelected");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(l56, "mGetHybridApp");
        this.f = d56;
        this.g = i2;
        this.h = arrayList;
        this.i = uq4;
        this.j = l56;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (this.e.isEmpty()) {
            this.f.m();
            this.i.a(this.j, null, new Di(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.C56
    public int n() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.C56
    public void o() {
        this.f.I(new ArrayList<>(At7.t(At7.o(At7.h(Pm7.z(this.e), new Bi(this)), Ci.INSTANCE))));
    }

    @DexIgnore
    @Override // com.fossil.C56
    public void p(AppWrapper appWrapper, boolean z) {
        T t;
        int i2;
        Wg6.c(appWrapper, "appWrapper");
        FLogger.INSTANCE.getLocal().d(k, "setAppState: appWrapper=" + appWrapper + ", selected=" + z);
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getUri(), appWrapper.getUri())) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            InstalledApp installedApp = t2.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(z);
            }
            InstalledApp installedApp2 = t2.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected == null) {
                Wg6.i();
                throw null;
            } else if (isSelected.booleanValue() && t2.getCurrentHandGroup() != (i2 = this.g)) {
                t2.setCurrentHandGroup(i2);
            }
        }
    }

    @DexIgnore
    public final List<AppWrapper> u() {
        return this.e;
    }

    @DexIgnore
    public void v() {
        this.f.M5(this);
    }
}
