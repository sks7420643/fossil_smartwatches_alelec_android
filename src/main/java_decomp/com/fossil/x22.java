package com.fossil;

import android.database.Cursor;
import com.fossil.J32;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class X22 implements J32.Bi {
    @DexIgnore
    public /* final */ Map a;

    @DexIgnore
    public X22(Map map) {
        this.a = map;
    }

    @DexIgnore
    public static J32.Bi a(Map map) {
        return new X22(map);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return J32.T(this.a, (Cursor) obj);
    }
}
