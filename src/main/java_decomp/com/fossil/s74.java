package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S74 extends X64 {
    @DexIgnore
    public /* final */ Set<Class<?>> a;
    @DexIgnore
    public /* final */ Set<Class<?>> b;
    @DexIgnore
    public /* final */ Set<Class<?>> c;
    @DexIgnore
    public /* final */ Set<Class<?>> d;
    @DexIgnore
    public /* final */ Set<Class<?>> e;
    @DexIgnore
    public /* final */ B74 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Fe4 {
        @DexIgnore
        public Ai(Set<Class<?>> set, Fe4 fe4) {
        }
    }

    @DexIgnore
    public S74(A74<?> a74, B74 b74) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (K74 k74 : a74.c()) {
            if (k74.b()) {
                if (k74.d()) {
                    hashSet3.add(k74.a());
                } else {
                    hashSet.add(k74.a());
                }
            } else if (k74.d()) {
                hashSet4.add(k74.a());
            } else {
                hashSet2.add(k74.a());
            }
        }
        if (!a74.f().isEmpty()) {
            hashSet.add(Fe4.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = Collections.unmodifiableSet(hashSet3);
        this.d = Collections.unmodifiableSet(hashSet4);
        this.e = a74.f();
        this.f = b74;
    }

    @DexIgnore
    @Override // com.fossil.B74
    public <T> Mg4<T> a(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.f.a(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.B74
    public <T> Mg4<Set<T>> b(Class<T> cls) {
        if (this.d.contains(cls)) {
            return this.f.b(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.X64, com.fossil.B74
    public <T> Set<T> c(Class<T> cls) {
        if (this.c.contains(cls)) {
            return this.f.c(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.X64, com.fossil.B74
    public <T> T get(Class<T> cls) {
        if (this.a.contains(cls)) {
            T t = (T) this.f.get(cls);
            return !cls.equals(Fe4.class) ? t : (T) new Ai(this.e, t);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }
}
