package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.sina.weibo.sdk.api.ImageObject;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.Checksum;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hx1 implements Checksum {
    @DexIgnore
    public static /* final */ int[] b; // = new int[256];
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int b(int i) {
            int c = c(i);
            int i2 = 0;
            while (true) {
                i2++;
                if (i2 >= 9) {
                    return c(c);
                }
                c = (Integer.MIN_VALUE & c) != 0 ? (c << 1) ^ 517762881 : c << 1;
            }
        }

        @DexIgnore
        public final int c(int i) {
            return ((i << 31) & RecyclerView.UNDEFINED_DURATION) | ((i >>> 31) & 1) | ((i >>> 29) & 2) | ((i >>> 27) & 4) | ((i >>> 25) & 8) | ((i >>> 23) & 16) | ((i >>> 21) & 32) | ((i >>> 19) & 64) | ((i >>> 17) & 128) | ((i >>> 15) & 256) | ((i >>> 13) & 512) | ((i >>> 11) & 1024) | ((i >>> 9) & 2048) | ((i >>> 7) & 4096) | ((i >>> 5) & 8192) | ((i >>> 3) & 16384) | ((i >>> 1) & 32768) | ((i << 1) & 65536) | ((i << 3) & 131072) | ((i << 5) & 262144) | ((i << 7) & 524288) | ((i << 9) & 1048576) | ((i << 11) & ImageObject.DATA_SIZE) | ((i << 13) & 4194304) | ((i << 15) & 8388608) | ((i << 17) & 16777216) | ((i << 19) & 33554432) | ((i << 21) & 67108864) | ((i << 23) & 134217728) | ((i << 25) & SQLiteDatabase.CREATE_IF_NECESSARY) | ((i << 27) & 536870912) | ((i << 29) & 1073741824);
        }
    }

    /*
    static {
        int i = 256;
        while (true) {
            i--;
            if (i > 0) {
                b[i] = c.b(i);
            } else {
                return;
            }
        }
    }
    */

    @DexIgnore
    public final void a(byte[] bArr) {
        Wg6.c(bArr, "b");
        update(bArr, 0, bArr.length);
    }

    @DexIgnore
    public long getValue() {
        return Hy1.o(ByteBuffer.allocate(4).putInt(this.a).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
    }

    @DexIgnore
    public void reset() {
        this.a = 0;
    }

    @DexIgnore
    public void update(int i) {
        a(new byte[]{(byte) (i & 255)});
    }

    @DexIgnore
    public void update(byte[] bArr, int i, int i2) {
        Wg6.c(bArr, "b");
        int i3 = this.a;
        this.a = ((i3 << 8) & 16711680) | ((i3 >>> 24) & 255) | ((i3 >>> 8) & 65280) | ((i3 << 24) & -16777216);
        while (true) {
            i2--;
            if (i2 >= 0) {
                int i4 = this.a;
                this.a = (i4 >>> 8) ^ b[(bArr[i] ^ i4) & 255];
                i++;
            } else {
                int i5 = this.a;
                this.a = ((i5 << 8) & 16711680) | ((i5 >>> 24) & 255) | ((i5 >>> 8) & 65280) | ((i5 << 24) & -16777216);
                return;
            }
        }
    }
}
