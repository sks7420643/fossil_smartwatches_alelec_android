package com.fossil;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.M64;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V74 {
    @DexIgnore
    public /* final */ W84 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ A84 a;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService b;
        @DexIgnore
        public /* final */ /* synthetic */ Qc4 c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ W84 e;

        @DexIgnore
        public Ai(A84 a84, ExecutorService executorService, Qc4 qc4, boolean z, W84 w84) {
            this.a = a84;
            this.b = executorService;
            this.c = qc4;
            this.d = z;
            this.e = w84;
        }

        @DexIgnore
        public Void a() throws Exception {
            this.a.c(this.b, this.c);
            if (!this.d) {
                return null;
            }
            this.e.g(this.c);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore
    public V74(W84 w84) {
        this.a = w84;
    }

    @DexIgnore
    public static V74 a() {
        V74 v74 = (V74) J64.h().f(V74.class);
        if (v74 != null) {
            return v74;
        }
        throw new NullPointerException("FirebaseCrashlytics component is not present.");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v7, types: [com.fossil.F84] */
    /* JADX WARN: Type inference failed for: r1v9, types: [com.fossil.T74] */
    /* JADX WARN: Type inference failed for: r5v7, types: [com.fossil.E84, com.fossil.C84] */
    /* JADX WARN: Type inference failed for: r6v5, types: [com.fossil.D84, com.fossil.C84] */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.V74 b(com.fossil.J64 r11, com.fossil.Lg4 r12, com.fossil.W74 r13, com.fossil.M64 r14) {
        /*
            android.content.Context r8 = r11.g()
            com.fossil.H94 r2 = new com.fossil.H94
            java.lang.String r0 = r8.getPackageName()
            r2.<init>(r8, r0, r12)
            com.fossil.C94 r4 = new com.fossil.C94
            r4.<init>(r11)
            if (r13 != 0) goto L_0x0073
            com.fossil.Y74 r3 = new com.fossil.Y74
            r3.<init>()
        L_0x0019:
            com.fossil.A84 r9 = new com.fossil.A84
            r9.<init>(r11, r8, r2, r4)
            if (r14 == 0) goto L_0x0085
            com.fossil.X74 r0 = com.fossil.X74.f()
            java.lang.String r1 = "Firebase Analytics is available."
            r0.b(r1)
            com.fossil.F84 r0 = new com.fossil.F84
            r0.<init>(r14)
            com.fossil.T74 r1 = new com.fossil.T74
            r1.<init>()
            com.fossil.M64$Ai r5 = e(r14, r1)
            if (r5 == 0) goto L_0x0075
            com.fossil.X74 r5 = com.fossil.X74.f()
            java.lang.String r6 = "Firebase Analytics listener registered successfully."
            r5.b(r6)
            com.fossil.E84 r5 = new com.fossil.E84
            r5.<init>()
            com.fossil.D84 r6 = new com.fossil.D84
            r7 = 500(0x1f4, float:7.0E-43)
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MILLISECONDS
            r6.<init>(r0, r7, r10)
            r1.d(r5)
            r1.e(r6)
        L_0x0056:
            com.fossil.W84 r0 = new com.fossil.W84
            java.lang.String r1 = "Crashlytics Exception Handler"
            java.util.concurrent.ExecutorService r7 = com.fossil.F94.c(r1)
            r1 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            boolean r1 = r9.h()
            if (r1 != 0) goto L_0x0099
            com.fossil.X74 r0 = com.fossil.X74.f()
            java.lang.String r1 = "Unable to start Crashlytics."
            r0.d(r1)
            r0 = 0
        L_0x0072:
            return r0
        L_0x0073:
            r3 = r13
            goto L_0x0019
        L_0x0075:
            com.fossil.X74 r1 = com.fossil.X74.f()
            java.lang.String r5 = "Firebase Analytics listener registration failed."
            r1.b(r5)
            com.fossil.J84 r5 = new com.fossil.J84
            r5.<init>()
            r6 = r0
            goto L_0x0056
        L_0x0085:
            com.fossil.X74 r0 = com.fossil.X74.f()
            java.lang.String r1 = "Firebase Analytics is unavailable."
            r0.b(r1)
            com.fossil.J84 r5 = new com.fossil.J84
            r5.<init>()
            com.fossil.G84 r6 = new com.fossil.G84
            r6.<init>()
            goto L_0x0056
        L_0x0099:
            java.lang.String r1 = "com.google.firebase.crashlytics.startup"
            java.util.concurrent.ExecutorService r3 = com.fossil.F94.c(r1)
            com.fossil.Qc4 r4 = r9.l(r8, r11, r3)
            com.fossil.V74$Ai r1 = new com.fossil.V74$Ai
            boolean r5 = r0.n(r4)
            r2 = r9
            r6 = r0
            r1.<init>(r2, r3, r4, r5, r6)
            com.fossil.Qt3.c(r3, r1)
            com.fossil.V74 r1 = new com.fossil.V74
            r1.<init>(r0)
            r0 = r1
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.V74.b(com.fossil.J64, com.fossil.Lg4, com.fossil.W74, com.fossil.M64):com.fossil.V74");
    }

    @DexIgnore
    public static M64.Ai e(M64 m64, T74 t74) {
        M64.Ai c = m64.c("clx", t74);
        if (c == null) {
            X74.f().b("Could not register AnalyticsConnectorListener with Crashlytics origin.");
            c = m64.c(CrashDumperPlugin.NAME, t74);
            if (c != null) {
                X74.f().i("A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version.");
            }
        }
        return c;
    }

    @DexIgnore
    public void c(String str, String str2) {
        this.a.o(str, str2);
    }

    @DexIgnore
    public void d(String str) {
        this.a.p(str);
    }
}
