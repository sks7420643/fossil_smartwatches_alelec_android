package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vh7 {
    @DexIgnore
    public static Xh7 c;
    @DexIgnore
    public static Th7 d; // = Ei7.p();
    @DexIgnore
    public static JSONObject e; // = new JSONObject();
    @DexIgnore
    public Integer a; // = null;
    @DexIgnore
    public String b; // = null;

    @DexIgnore
    public Vh7(Context context) {
        try {
            a(context);
            this.a = Ei7.I(context.getApplicationContext());
            this.b = Tg7.a(context).e();
        } catch (Throwable th) {
            d.e(th);
        }
    }

    @DexIgnore
    public static Xh7 a(Context context) {
        Xh7 xh7;
        synchronized (Vh7.class) {
            try {
                if (c == null) {
                    c = new Xh7(context.getApplicationContext());
                }
                xh7 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return xh7;
    }

    @DexIgnore
    public void b(JSONObject jSONObject, Thread thread) {
        String str;
        String str2;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (c != null) {
                c.a(jSONObject2, thread);
            }
            Ji7.d(jSONObject2, "cn", this.b);
            if (this.a != null) {
                jSONObject2.put("tn", this.a);
            }
            if (thread == null) {
                str = "ev";
                str2 = jSONObject2;
            } else {
                str = "errkv";
                str2 = jSONObject2.toString();
            }
            jSONObject.put(str, str2);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.e(th);
        }
    }
}
