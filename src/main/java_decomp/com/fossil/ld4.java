package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ld4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ StackTraceElement[] c;
    @DexIgnore
    public /* final */ Ld4 d;

    @DexIgnore
    public Ld4(Throwable th, Kd4 kd4) {
        this.a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = kd4.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new Ld4(cause, kd4) : null;
    }
}
