package com.fossil;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hd7 extends InputStream {
    @DexIgnore
    public /* final */ InputStream b;
    @DexIgnore
    public long c;
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;
    @DexIgnore
    public long f;

    @DexIgnore
    public Hd7(InputStream inputStream) {
        this(inputStream, 4096);
    }

    @DexIgnore
    public Hd7(InputStream inputStream, int i) {
        this.f = -1;
        this.b = !inputStream.markSupported() ? new BufferedInputStream(inputStream, i) : inputStream;
    }

    @DexIgnore
    public void a(long j) throws IOException {
        if (this.c > this.e || j < this.d) {
            throw new IOException("Cannot reset");
        }
        this.b.reset();
        f(this.d, j);
        this.c = j;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int available() throws IOException {
        return this.b.available();
    }

    @DexIgnore
    public long b(int i) {
        long j = this.c + ((long) i);
        if (this.e < j) {
            c(j);
        }
        return this.c;
    }

    @DexIgnore
    public final void c(long j) {
        try {
            if (this.d >= this.c || this.c > this.e) {
                this.d = this.c;
                this.b.mark((int) (j - this.c));
            } else {
                this.b.reset();
                this.b.mark((int) (j - this.d));
                f(this.d, this.c);
            }
            this.e = j;
        } catch (IOException e2) {
            throw new IllegalStateException("Unable to mark: " + e2);
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    public final void f(long j, long j2) throws IOException {
        while (j < j2) {
            long skip = this.b.skip(j2 - j);
            if (skip == 0) {
                if (read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j += skip;
        }
    }

    @DexIgnore
    public void mark(int i) {
        this.f = b(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.b.markSupported();
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read() throws IOException {
        int read = this.b.read();
        if (read != -1) {
            this.c++;
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        int read = this.b.read(bArr);
        if (read != -1) {
            this.c += (long) read;
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.b.read(bArr, i, i2);
        if (read != -1) {
            this.c += (long) read;
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public void reset() throws IOException {
        a(this.f);
    }

    @DexIgnore
    @Override // java.io.InputStream
    public long skip(long j) throws IOException {
        long skip = this.b.skip(j);
        this.c += skip;
        return skip;
    }
}
