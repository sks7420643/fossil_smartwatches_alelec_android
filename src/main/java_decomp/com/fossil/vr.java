package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vr {
    @DexIgnore
    public short a; // = ((short) -1);
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ N6 f;

    @DexIgnore
    public Vr(byte[] bArr, int i, N6 n6) {
        this.d = bArr;
        this.e = i;
        this.f = n6;
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        Wg6.b(wrap, "ByteBuffer.wrap(data)");
        this.b = wrap;
        byte[] bArr2 = this.d;
        this.c = bArr2.length;
        ByteBuffer wrap2 = ByteBuffer.wrap(bArr2);
        Wg6.b(wrap2, "ByteBuffer.wrap(data)");
        this.b = wrap2;
    }

    @DexIgnore
    public final byte[] a() {
        int remaining = this.b.remaining();
        if (remaining <= 0) {
            return new byte[0];
        }
        this.a = (short) ((short) (this.a + 1));
        int min = Math.min(remaining + 1, this.e);
        byte[] copyOfRange = Arrays.copyOfRange(new byte[]{(byte) (((Wr) this).a % 256)}, 0, min);
        this.b.get(copyOfRange, 1, min - 1);
        Wg6.b(copyOfRange, "bytes");
        return copyOfRange;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public final int c() {
        return Math.min((this.a + 1) * (this.e - b()), this.c);
    }
}
