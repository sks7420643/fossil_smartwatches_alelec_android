package com.fossil;

import com.fossil.P18;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z18 {
    @DexIgnore
    public static Z18 a;

    @DexIgnore
    public abstract void a(P18.Ai ai, String str);

    @DexIgnore
    public abstract void b(P18.Ai ai, String str, String str2);

    @DexIgnore
    public abstract void c(G18 g18, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract int d(Response.a aVar);

    @DexIgnore
    public abstract boolean e(F18 f18, L28 l28);

    @DexIgnore
    public abstract Socket f(F18 f18, X08 x08, P28 p28);

    @DexIgnore
    public abstract boolean g(X08 x08, X08 x082);

    @DexIgnore
    public abstract L28 h(F18 f18, X08 x08, P28 p28, X18 x18);

    @DexIgnore
    public abstract void i(F18 f18, L28 l28);

    @DexIgnore
    public abstract M28 j(F18 f18);

    @DexIgnore
    public abstract IOException k(A18 a18, IOException iOException);
}
