package com.fossil;

import com.mapped.Ap4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hq5<T> extends Ap4<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ServerError b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public Hq5(int i, ServerError serverError, Throwable th, String str, T t) {
        super(null);
        this.a = i;
        this.b = serverError;
        this.c = th;
        this.d = str;
        this.e = t;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Hq5(int i, ServerError serverError, Throwable th, String str, Object obj, int i2, Qg6 qg6) {
        this(i, serverError, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str, (i2 & 16) == 0 ? obj : null);
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final ServerError c() {
        return this.b;
    }

    @DexIgnore
    public final Throwable d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Hq5) {
                Hq5 hq5 = (Hq5) obj;
                if (this.a != hq5.a || !Wg6.a(this.b, hq5.b) || !Wg6.a(this.c, hq5.c) || !Wg6.a(this.d, hq5.d) || !Wg6.a(this.e, hq5.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.a;
        ServerError serverError = this.b;
        int hashCode = serverError != null ? serverError.hashCode() : 0;
        Throwable th = this.c;
        int hashCode2 = th != null ? th.hashCode() : 0;
        String str = this.d;
        int hashCode3 = str != null ? str.hashCode() : 0;
        T t = this.e;
        if (t != null) {
            i = t.hashCode();
        }
        return ((((((hashCode + (i2 * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.a + ", serverError=" + this.b + ", throwable=" + this.c + ", errorItems=" + this.d + ", data=" + ((Object) this.e) + ")";
    }
}
