package com.fossil;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hz0 {
    @DexIgnore
    public static /* final */ Nz0 a;
    @DexIgnore
    public static /* final */ Property<View, Float> b; // = new Ai(Float.class, "translationAlpha");
    @DexIgnore
    public static /* final */ Property<View, Rect> c; // = new Bi(Rect.class, "clipBounds");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Property<View, Float> {
        @DexIgnore
        public Ai(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        public Float a(View view) {
            return Float.valueOf(Hz0.c(view));
        }

        @DexIgnore
        public void b(View view, Float f) {
            Hz0.h(view, f.floatValue());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ Float get(View view) {
            return a(view);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ void set(View view, Float f) {
            b(view, f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Property<View, Rect> {
        @DexIgnore
        public Bi(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        public Rect a(View view) {
            return Mo0.s(view);
        }

        @DexIgnore
        public void b(View view, Rect rect) {
            Mo0.r0(view, rect);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ Rect get(View view) {
            return a(view);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ void set(View view, Rect rect) {
            b(view, rect);
        }
    }

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            a = new Mz0();
        } else if (i >= 23) {
            a = new Lz0();
        } else if (i >= 22) {
            a = new Kz0();
        } else if (i >= 21) {
            a = new Jz0();
        } else if (i >= 19) {
            a = new Iz0();
        } else {
            a = new Nz0();
        }
    }
    */

    @DexIgnore
    public static void a(View view) {
        a.a(view);
    }

    @DexIgnore
    public static Gz0 b(View view) {
        return Build.VERSION.SDK_INT >= 18 ? new Fz0(view) : Ez0.e(view);
    }

    @DexIgnore
    public static float c(View view) {
        return a.c(view);
    }

    @DexIgnore
    public static Rz0 d(View view) {
        return Build.VERSION.SDK_INT >= 18 ? new Qz0(view) : new Pz0(view.getWindowToken());
    }

    @DexIgnore
    public static void e(View view) {
        a.d(view);
    }

    @DexIgnore
    public static void f(View view, Matrix matrix) {
        a.e(view, matrix);
    }

    @DexIgnore
    public static void g(View view, int i, int i2, int i3, int i4) {
        a.f(view, i, i2, i3, i4);
    }

    @DexIgnore
    public static void h(View view, float f) {
        a.g(view, f);
    }

    @DexIgnore
    public static void i(View view, int i) {
        a.h(view, i);
    }

    @DexIgnore
    public static void j(View view, Matrix matrix) {
        a.i(view, matrix);
    }

    @DexIgnore
    public static void k(View view, Matrix matrix) {
        a.j(view, matrix);
    }
}
