package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sb2 {
    @DexIgnore
    public /* final */ DataHolder a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public Sb2(DataHolder dataHolder, int i) {
        Rc2.k(dataHolder);
        this.a = dataHolder;
        d(i);
    }

    @DexIgnore
    public byte[] a(String str) {
        return this.a.c(str, this.b, this.c);
    }

    @DexIgnore
    public int b(String str) {
        return this.a.f(str, this.b, this.c);
    }

    @DexIgnore
    public String c(String str) {
        return this.a.A(str, this.b, this.c);
    }

    @DexIgnore
    public final void d(int i) {
        Rc2.n(i >= 0 && i < this.a.getCount());
        this.b = i;
        this.c = this.a.D(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof Sb2) {
            Sb2 sb2 = (Sb2) obj;
            return Pc2.a(Integer.valueOf(sb2.b), Integer.valueOf(this.b)) && Pc2.a(Integer.valueOf(sb2.c), Integer.valueOf(this.c)) && sb2.a == this.a;
        }
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(Integer.valueOf(this.b), Integer.valueOf(this.c), this.a);
    }
}
