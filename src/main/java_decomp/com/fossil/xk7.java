package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xk7 implements Comparable<Xk7> {
    @DexIgnore
    public static /* final */ Xk7 f; // = new Xk7(1, 3, 72);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public Xk7(int i, int i2, int i3) {
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.b = b(i, i2, i3);
    }

    @DexIgnore
    public int a(Xk7 xk7) {
        Wg6.c(xk7, FacebookRequestErrorClassification.KEY_OTHER);
        return this.b - xk7.b;
    }

    @DexIgnore
    public final int b(int i, int i2, int i3) {
        if (i >= 0 && 255 >= i && i2 >= 0 && 255 >= i2 && i3 >= 0 && 255 >= i3) {
            return (i << 16) + (i2 << 8) + i3;
        }
        throw new IllegalArgumentException(("Version components are out of range: " + i + '.' + i2 + '.' + i3).toString());
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Xk7 xk7) {
        return a(xk7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        Xk7 xk7 = (Xk7) (!(obj instanceof Xk7) ? null : obj);
        if (xk7 == null) {
            return false;
        }
        return this.b == xk7.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0007: IGET  (r1v0 int) = (r3v0 'this' com.fossil.Xk7 A[IMMUTABLE_TYPE, THIS]) com.fossil.Xk7.c int), ('.' char), (wrap: int : 0x000f: IGET  (r1v1 int) = (r3v0 'this' com.fossil.Xk7 A[IMMUTABLE_TYPE, THIS]) com.fossil.Xk7.d int), ('.' char), (wrap: int : 0x0017: IGET  (r1v2 int) = (r3v0 'this' com.fossil.Xk7 A[IMMUTABLE_TYPE, THIS]) com.fossil.Xk7.e int)] */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.c);
        sb.append('.');
        sb.append(this.d);
        sb.append('.');
        sb.append(this.e);
        return sb.toString();
    }
}
