package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bu4 {
    @DexIgnore
    public /* final */ Ft4 a;

    @DexIgnore
    public Bu4(Ft4 ft4) {
        Wg6.c(ft4, "dao");
        this.a = ft4;
    }

    @DexIgnore
    public final void a() {
        this.a.a();
    }

    @DexIgnore
    public final Long[] b(List<Dt4> list) {
        Wg6.c(list, "notifications");
        return this.a.insert(list);
    }

    @DexIgnore
    public final LiveData<List<Dt4>> c() {
        return this.a.b();
    }
}
