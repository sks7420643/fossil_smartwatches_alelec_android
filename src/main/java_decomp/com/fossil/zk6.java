package com.fossil;

import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zk6 implements MembersInjector<ActivityDetailActivity> {
    @DexIgnore
    public static void a(ActivityDetailActivity activityDetailActivity, ActivityDetailPresenter activityDetailPresenter) {
        activityDetailActivity.A = activityDetailPresenter;
    }
}
