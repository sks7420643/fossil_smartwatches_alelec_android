package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gq1 extends Lq1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Gq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gq1 createFromParcel(Parcel parcel) {
            return new Gq1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gq1[] newArray(int i) {
            return new Gq1[i];
        }
    }

    @DexIgnore
    public Gq1(byte b, Bv1 bv1) {
        super(E90.COMMUTE_TIME_ETA_MICRO_APP, b, bv1);
    }

    @DexIgnore
    public /* synthetic */ Gq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }
}
