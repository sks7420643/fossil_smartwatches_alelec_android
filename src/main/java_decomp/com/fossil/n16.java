package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N16 implements Factory<J16> {
    @DexIgnore
    public static J16 a(L16 l16) {
        J16 b = l16.b();
        Lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
