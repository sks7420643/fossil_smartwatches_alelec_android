package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.U02;
import com.fossil.V02;
import java.util.ArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B22 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ T02 b;
    @DexIgnore
    public /* final */ K22 c;
    @DexIgnore
    public /* final */ H22 d;
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public /* final */ S32 f;
    @DexIgnore
    public /* final */ T32 g;

    @DexIgnore
    public B22(Context context, T02 t02, K22 k22, H22 h22, Executor executor, S32 s32, T32 t32) {
        this.a = context;
        this.b = t02;
        this.c = k22;
        this.d = h22;
        this.e = executor;
        this.f = s32;
        this.g = t32;
    }

    @DexIgnore
    public static /* synthetic */ Object c(B22 b22, V02 v02, Iterable iterable, H02 h02, int i) {
        if (v02.c() == V02.Ai.TRANSIENT_ERROR) {
            b22.c.h0(iterable);
            b22.d.a(h02, i + 1);
            return null;
        }
        b22.c.g(iterable);
        if (v02.c() == V02.Ai.OK) {
            b22.c.s(h02, b22.g.a() + v02.b());
        }
        if (!b22.c.f0(h02)) {
            return null;
        }
        b22.d.a(h02, 1);
        return null;
    }

    @DexIgnore
    public static /* synthetic */ void e(B22 b22, H02 h02, int i, Runnable runnable) {
        try {
            S32 s32 = b22.f;
            K22 k22 = b22.c;
            k22.getClass();
            s32.a(Z12.b(k22));
            if (!b22.a()) {
                b22.f.a(A22.b(b22, h02, i));
            } else {
                b22.f(h02, i);
            }
        } catch (R32 e2) {
            b22.d.a(h02, i + 1);
        } catch (Throwable th) {
            runnable.run();
            throw th;
        }
        runnable.run();
    }

    @DexIgnore
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public void f(H02 h02, int i) {
        V02 a2;
        B12 b2 = this.b.b(h02.b());
        Iterable<Q22> iterable = (Iterable) this.f.a(X12.b(this, h02));
        if (iterable.iterator().hasNext()) {
            if (b2 == null) {
                C12.a("Uploader", "Unknown backend for %s, deleting event batch for it...", h02);
                a2 = V02.a();
            } else {
                ArrayList arrayList = new ArrayList();
                for (Q22 q22 : iterable) {
                    arrayList.add(q22.b());
                }
                U02.Ai a3 = U02.a();
                a3.b(arrayList);
                a3.c(h02.c());
                a2 = b2.a(a3.a());
            }
            this.f.a(Y12.b(this, a2, iterable, h02, i));
        }
    }

    @DexIgnore
    public void g(H02 h02, int i, Runnable runnable) {
        this.e.execute(W12.a(this, h02, i, runnable));
    }
}
