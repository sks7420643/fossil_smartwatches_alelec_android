package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wv4 {
    @DexIgnore
    public /* final */ F57.Bi a;
    @DexIgnore
    public /* final */ Uy4 b; // = Uy4.d.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ne5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ne5 ne5) {
            super(ne5.n());
            Wg6.c(ne5, "binding");
            this.a = ne5;
        }

        @DexIgnore
        public final void a(At4 at4, F57.Bi bi, Uy4 uy4) {
            Wg6.c(at4, "player");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Ne5 ne5 = this.a;
            String c = Wg6.a(PortfolioApp.get.instance().l0(), at4.c()) ? Um5.c(PortfolioApp.get.instance(), 2131886250) : Hz4.a.b(at4.b(), at4.d(), at4.e());
            FlexibleTextView flexibleTextView = ne5.v;
            Wg6.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(c);
            ImageView imageView = ne5.r;
            Wg6.b(imageView, "ivAvatar");
            Ty4.b(imageView, at4.a(), at4.b(), bi, uy4);
        }
    }

    @DexIgnore
    public Wv4(int i) {
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.a = f;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        return list.get(i) instanceof At4;
    }

    @DexIgnore
    public void b(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Object obj = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Ai ai = (Ai) (!(viewHolder instanceof Ai) ? null : viewHolder);
        Object obj2 = list.get(i);
        if (obj2 instanceof At4) {
            obj = obj2;
        }
        At4 at4 = (At4) obj;
        if (ai != null && at4 != null) {
            ai.a(at4, this.a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder c(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Ne5 z = Ne5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemFriendListBinding.in\u2026(inflater, parent, false)");
        return new Ai(z);
    }
}
