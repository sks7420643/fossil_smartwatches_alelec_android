package com.fossil;

import com.fossil.Db4;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hb4 implements Db4.Ai {
    @DexIgnore
    public static String b(String str) throws IOException {
        BufferedInputStream bufferedInputStream;
        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(str));
            try {
                String G = R84.G(bufferedInputStream);
                R84.f(bufferedInputStream);
                return G;
            } catch (Throwable th) {
                th = th;
                R84.f(bufferedInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedInputStream = null;
            R84.f(bufferedInputStream);
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Db4.Ai
    public String a(File file) throws IOException {
        return b(file.getPath());
    }
}
