package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C14<F, T> extends Z04<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ B14<F, ? extends T> function;
    @DexIgnore
    public /* final */ Z04<T> resultEquivalence;

    @DexIgnore
    public C14(B14<F, ? extends T> b14, Z04<T> z04) {
        I14.l(b14);
        this.function = b14;
        I14.l(z04);
        this.resultEquivalence = z04;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Z04<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Z04
    public boolean doEquivalent(F f, F f2) {
        return this.resultEquivalence.equivalent(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Z04<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Z04
    public int doHash(F f) {
        return this.resultEquivalence.hash(this.function.apply(f));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C14)) {
            return false;
        }
        C14 c14 = (C14) obj;
        return this.function.equals(c14.function) && this.resultEquivalence.equals(c14.resultEquivalence);
    }

    @DexIgnore
    public int hashCode() {
        return F14.b(this.function, this.resultEquivalence);
    }

    @DexIgnore
    public String toString() {
        return this.resultEquivalence + ".onResultOf(" + this.function + ")";
    }
}
