package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K26 implements Factory<J26> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;

    @DexIgnore
    public K26(Provider<QuickResponseRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static K26 a(Provider<QuickResponseRepository> provider) {
        return new K26(provider);
    }

    @DexIgnore
    public static J26 c(QuickResponseRepository quickResponseRepository) {
        return new J26(quickResponseRepository);
    }

    @DexIgnore
    public J26 b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
