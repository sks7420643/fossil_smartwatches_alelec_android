package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ap6 implements MembersInjector<Yo6> {
    @DexIgnore
    public static void a(Yo6 yo6, Kx6 kx6) {
        yo6.m = kx6;
    }

    @DexIgnore
    public static void b(Yo6 yo6, Po4 po4) {
        yo6.l = po4;
    }
}
