package com.fossil;

import android.graphics.drawable.Drawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ch1 implements Qb1<Drawable, Drawable> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(Drawable drawable, Ob1 ob1) throws IOException {
        return d(drawable, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Drawable> b(Drawable drawable, int i, int i2, Ob1 ob1) throws IOException {
        return c(drawable, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Drawable> c(Drawable drawable, int i, int i2, Ob1 ob1) {
        return Ah1.f(drawable);
    }

    @DexIgnore
    public boolean d(Drawable drawable, Ob1 ob1) {
        return true;
    }
}
