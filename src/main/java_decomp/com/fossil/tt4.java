package com.fossil;

import android.util.Base64;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tt4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Rt4 b;
    @DexIgnore
    public /* final */ ChallengeRemoteDataSource c;
    @DexIgnore
    public /* final */ An4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t.h(), t2.h());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {129}, m = "createChallenge")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, 0, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {160}, m = "editChallenge")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {74}, m = "fetchChallenge")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {31}, m = "fetchChallengesWithStatus")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {241}, m = "fetchFocusedPlayers")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, 0, 0, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {480}, m = "fetchHistoryChallenges")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {188}, m = "fetchPlayers")
    public static final class Hi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {454}, m = "fetchRecommendedChallenges")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {390}, m = "fetchSyncData")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {105}, m = "fetchWatchDisplayChallenges")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {351}, m = "joinChallenge")
    public static final class Li extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(null, 0, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {271}, m = "leaveChallenge")
    public static final class Mi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.E(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {420}, m = "pushPendingStepData")
    public static final class Ni extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.G(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {427}, m = "pushStepData")
    public static final class Oi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.H(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {298}, m = "sendInvitation")
    public static final class Pi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Pi(Tt4 tt4, Xe6 xe6) {
            super(xe6);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.I(null, null, this);
        }
    }

    @DexIgnore
    public Tt4(Rt4 rt4, ChallengeRemoteDataSource challengeRemoteDataSource, An4 an4) {
        Wg6.c(rt4, "local");
        Wg6.c(challengeRemoteDataSource, "remote");
        Wg6.c(an4, "shared");
        this.b = rt4;
        this.c = challengeRemoteDataSource;
        this.d = an4;
        String simpleName = Tt4.class.getSimpleName();
        Wg6.b(simpleName, "ChallengeRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public static /* synthetic */ Object l(Tt4 tt4, String str, String str2, String str3, Integer num, Xe6 xe6, int i, Object obj) {
        Integer num2 = null;
        String str4 = (i & 2) != 0 ? null : str2;
        String str5 = (i & 4) != 0 ? null : str3;
        if ((i & 8) == 0) {
            num2 = num;
        }
        return tt4.k(str, str4, str5, num2, xe6);
    }

    @DexIgnore
    public static /* synthetic */ Object n(Tt4 tt4, String str, String str2, Xe6 xe6, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = "joined_challenge";
        }
        return tt4.m(str, str2, xe6);
    }

    @DexIgnore
    public static /* synthetic */ Object q(Tt4 tt4, List list, int i, int i2, boolean z, Xe6 xe6, int i3, Object obj) {
        return tt4.p(list, i, i2, (i3 & 8) != 0 ? true : z, xe6);
    }

    @DexIgnore
    public static /* synthetic */ Object s(Tt4 tt4, int i, int i2, Xe6 xe6, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = 5;
        }
        if ((i3 & 2) != 0) {
            i2 = 0;
        }
        return tt4.r(i, i2, xe6);
    }

    @DexIgnore
    public final LiveData<List<Bt4>> A() {
        return this.b.p();
    }

    @DexIgnore
    public final long B(Ps4 ps4) {
        Wg6.c(ps4, "challenge");
        return this.b.r(ps4);
    }

    @DexIgnore
    public final void C(List<Ms4> list) {
        Wg6.c(list, "players");
        this.b.f();
        this.b.v(list);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(java.lang.String r7, int r8, java.lang.String r9, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Ps4>> r10) {
        /*
        // Method dump skipped, instructions count: 269
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.D(java.lang.String, int, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object E(java.lang.String r23, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Ps4>> r24) {
        /*
        // Method dump skipped, instructions count: 285
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.E(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object F(EncryptedData encryptedData, Xe6<? super Kz4<Nt4>> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "processStepData - encryptedData=" + encryptedData);
        int ordinal = encryptedData.getEncryptMethod().ordinal();
        String encodeToString = Base64.encodeToString(encryptedData.getEncryptedData(), 2);
        Wg6.b(encodeToString, "Base64.encodeToString(en\u2026ptedData, Base64.NO_WRAP)");
        return H(new Ls4(0, ordinal, encodeToString, encryptedData.getKeyType().ordinal(), String.valueOf((int) encryptedData.getSequence())), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object G(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.Tt4.Ni
            if (r0 == 0) goto L_0x002e
            r0 = r7
            com.fossil.Tt4$Ni r0 = (com.fossil.Tt4.Ni) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r3 = r1.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x003d
            if (r0 != r5) goto L_0x0035
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.Tt4 r0 = (com.fossil.Tt4) r0
            com.fossil.El7.b(r3)
        L_0x002b:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x002d:
            return r0
        L_0x002e:
            com.fossil.Tt4$Ni r0 = new com.fossil.Tt4$Ni
            r0.<init>(r6, r7)
            r1 = r0
            goto L_0x0014
        L_0x0035:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003d:
            com.fossil.El7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = r6.a
            java.lang.String r4 = "pushPendingStepData"
            r0.e(r3, r4)
            com.fossil.Rt4 r0 = r6.b
            java.util.List r3 = r0.l()
            boolean r0 = r3.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x002b
            r0 = 0
            java.lang.Object r0 = r3.get(r0)
            com.fossil.Ls4 r0 = (com.fossil.Ls4) r0
            r1.L$0 = r6
            r1.L$1 = r3
            r1.label = r5
            java.lang.Object r0 = r6.H(r0, r1)
            if (r0 != r2) goto L_0x002b
            r0 = r2
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.G(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object H(com.fossil.Ls4 r9, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Nt4>> r10) {
        /*
        // Method dump skipped, instructions count: 311
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.H(com.fossil.Ls4, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object I(java.lang.String r7, java.lang.String[] r8, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Ps4>> r9) {
        /*
        // Method dump skipped, instructions count: 260
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.I(java.lang.String, java.lang.String[], com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void J(String str, boolean z) {
        Wg6.c(str, "challengeId");
        this.d.I1(str, Boolean.valueOf(z));
    }

    @DexIgnore
    public final int K(Bt4 bt4) {
        Wg6.c(bt4, "historyChallenge");
        return this.b.w(bt4);
    }

    @DexIgnore
    public final Ps4 a(String str) {
        Wg6.c(str, "id");
        return this.b.a(str);
    }

    @DexIgnore
    public final LiveData<Ps4> b(String str) {
        Wg6.c(str, "id");
        return this.b.b(str);
    }

    @DexIgnore
    public final int c(String str) {
        Wg6.c(str, "id");
        return this.b.c(str);
    }

    @DexIgnore
    public final void d() {
        this.b.d();
    }

    @DexIgnore
    public final void e() {
        this.b.e();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.fossil.Ts4 r11, int r12, java.lang.String r13, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Ps4>> r14) {
        /*
        // Method dump skipped, instructions count: 295
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.f(com.fossil.Ts4, int, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Ps4 g(String[] strArr, Date date) {
        Wg6.c(strArr, "array");
        Wg6.c(date, "date");
        return this.b.g(strArr, date);
    }

    @DexIgnore
    public final LiveData<Ps4> h(String[] strArr, Date date) {
        Wg6.c(strArr, "array");
        Wg6.c(date, "date");
        return this.b.h(strArr, date);
    }

    @DexIgnore
    public final int i(String str) {
        Wg6.c(str, "id");
        return this.b.i(str);
    }

    @DexIgnore
    public final Ks4 j(String str) {
        Wg6.c(str, "id");
        return this.b.k(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object k(java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.Integer r12, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Ps4>> r13) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.k(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m(java.lang.String r9, java.lang.String r10, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Ps4>> r11) {
        /*
        // Method dump skipped, instructions count: 297
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.m(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object o(java.lang.String[] r8, int r9, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Ps4>>> r10) {
        /*
        // Method dump skipped, instructions count: 340
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.o(java.lang.String[], int, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object p(java.util.List<java.lang.String> r8, int r9, int r10, boolean r11, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Ks4>>> r12) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.p(java.util.List, int, int, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(int r8, int r9, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Bt4>>> r10) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.r(int, int, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object t(java.lang.String r7, java.lang.String[] r8, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Ms4>>> r9) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.t(java.lang.String, java.lang.String[], com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object u(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Mt4>>> r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            boolean r0 = r7 instanceof com.fossil.Tt4.Ii
            if (r0 == 0) goto L_0x004c
            r0 = r7
            com.fossil.Tt4$Ii r0 = (com.fossil.Tt4.Ii) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x004c
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x005a
            if (r4 != r5) goto L_0x0052
            java.lang.Object r0 = r0.L$0
            com.fossil.Tt4 r0 = (com.fossil.Tt4) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0098
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x006b
            java.util.List r1 = r0.get_items()
        L_0x003d:
            if (r1 != 0) goto L_0x006d
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x0044:
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            r3 = 2
            r1.<init>(r0, r2, r3, r2)
            r0 = r1
        L_0x004b:
            return r0
        L_0x004c:
            com.fossil.Tt4$Ii r0 = new com.fossil.Tt4$Ii
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0052:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005a:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource r1 = r6.c
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = r1.g(r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x004b
        L_0x006b:
            r1 = r2
            goto L_0x003d
        L_0x006d:
            java.util.Iterator r3 = r1.iterator()
        L_0x0071:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00c0
            java.lang.Object r0 = r3.next()
            com.fossil.Mt4 r0 = (com.fossil.Mt4) r0
            com.mapped.An4 r4 = r6.d
            com.fossil.Ps4 r5 = r0.b()
            java.lang.String r5 = r5.f()
            java.lang.Boolean r4 = r4.p0(r5)
            java.lang.String r5 = "shared.isNewChallenge(it.challenge.id)"
            com.mapped.Wg6.b(r4, r5)
            boolean r4 = r4.booleanValue()
            r0.d(r4)
            goto L_0x0071
        L_0x0098:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00b8
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r3 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00be
            java.lang.String r0 = r0.getMessage()
        L_0x00ac:
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r2.<init>(r3, r0)
            r1.<init>(r2)
            r0 = r1
            goto L_0x004b
        L_0x00b8:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        L_0x00be:
            r0 = r2
            goto L_0x00ac
        L_0x00c0:
            r0 = r1
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.u(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object v(com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Pt4>> r8) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.v(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object w(com.mapped.Xe6<? super java.util.List<com.fossil.Ps4>> r9) {
        /*
            r8 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            r7 = 1
            boolean r0 = r9 instanceof com.fossil.Tt4.Ki
            if (r0 == 0) goto L_0x004d
            r0 = r9
            com.fossil.Tt4$Ki r0 = (com.fossil.Tt4.Ki) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x004d
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x005b
            if (r4 != r7) goto L_0x0053
            java.lang.Object r0 = r0.L$0
            com.fossil.Tt4 r0 = (com.fossil.Tt4) r0
            com.fossil.El7.b(r1)
            r8 = r0
        L_0x0028:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0079
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x003d
            java.util.List r2 = r0.get_items()
        L_0x003d:
            if (r2 != 0) goto L_0x004c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r8.a
            java.lang.String r3 = "getWatchDisplayChallenges - Success with null response"
            r0.e(r1, r3)
        L_0x004c:
            return r2
        L_0x004d:
            com.fossil.Tt4$Ki r0 = new com.fossil.Tt4$Ki
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0053:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005b:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource r1 = r8.c
            r0.L$0 = r8
            r0.label = r7
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]
            r5 = 0
            java.lang.String r6 = "running"
            r4[r5] = r6
            java.lang.String r5 = "completed"
            r4[r7] = r5
            r5 = 5
            java.lang.Object r1 = r1.d(r4, r5, r0)
            if (r1 != r3) goto L_0x0028
            r2 = r3
            goto L_0x004c
        L_0x0079:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00b4
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r8.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getWatchDisplayChallenges - errorCode: "
            r4.append(r5)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r5 = "message: "
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00b2
            java.lang.String r0 = r0.getMessage()
        L_0x00a7:
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r1.e(r3, r0)
            goto L_0x004c
        L_0x00b2:
            r0 = r2
            goto L_0x00a7
        L_0x00b4:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Tt4.w(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final List<Ms4> x() {
        return this.b.m();
    }

    @DexIgnore
    public final LiveData<Bt4> y(String str) {
        Wg6.c(str, "challengeId");
        return this.b.n(str);
    }

    @DexIgnore
    public final Bt4 z(String str) {
        Wg6.c(str, "id");
        return this.b.o(str);
    }
}
