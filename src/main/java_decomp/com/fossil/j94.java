package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J94 {
    @DexIgnore
    public String a;

    @DexIgnore
    public static String b(Context context) {
        String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
        return installerPackageName == null ? "" : installerPackageName;
    }

    @DexIgnore
    public String a(Context context) {
        String str;
        synchronized (this) {
            if (this.a == null) {
                this.a = b(context);
            }
            str = "".equals(this.a) ? null : this.a;
        }
        return str;
    }
}
