package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ht4 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    @Vu3("id")
    public String b;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @Vu3("socialId")
    public String e;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_PROFILE_PIC)
    public String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ht4> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Ht4 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Ht4(parcel);
        }

        @DexIgnore
        public Ht4[] b(int i) {
            return new Ht4[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ht4 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ht4[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ht4(android.os.Parcel r7) {
        /*
            r6 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r7, r0)
            java.lang.String r1 = r7.readString()
            if (r1 == 0) goto L_0x0025
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r1, r0)
            java.lang.String r2 = r7.readString()
            java.lang.String r3 = r7.readString()
            java.lang.String r4 = r7.readString()
            java.lang.String r5 = r7.readString()
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x0025:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ht4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Ht4(String str, String str2, String str3, String str4, String str5) {
        Wg6.c(str, "id");
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ht4) {
                Ht4 ht4 = (Ht4) obj;
                if (!Wg6.a(this.b, ht4.b) || !Wg6.a(this.c, ht4.c) || !Wg6.a(this.d, ht4.d) || !Wg6.a(this.e, ht4.e) || !Wg6.a(this.f, ht4.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.f;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Owner(id=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", socialId=" + this.e + ", url=" + this.f + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
    }
}
