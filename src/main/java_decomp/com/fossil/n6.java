package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum d can be incorrect */
/* JADX WARN: Init of enum e can be incorrect */
/* JADX WARN: Init of enum f can be incorrect */
/* JADX WARN: Init of enum g can be incorrect */
/* JADX WARN: Init of enum h can be incorrect */
/* JADX WARN: Init of enum i can be incorrect */
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* JADX WARN: Init of enum o can be incorrect */
public enum N6 {
    d("unknown", r0),
    e("model_number", r0),
    f(Constants.SERIAL_NUMBER, r0),
    g(Constants.FIRMWARE_VERSION, r0),
    h("software_revision", r0),
    i("device_config", r0),
    j("file_transfer_control", r0),
    k("file_transfer_data", (byte) 0),
    l("async", r0),
    m("file_transfer_data_1", (byte) 1),
    n("authentication", r0),
    o("heart_rate", r0);
    
    @DexIgnore
    public static /* final */ L6 q; // = new L6(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        byte b2 = (byte) 255;
    }
    */

    @DexIgnore
    public N6(String str, byte b2) {
        this.b = str;
        this.c = (byte) b2;
    }

    @DexIgnore
    public final Ow a() {
        switch (M6.a[ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                return Ow.b;
            case 5:
                return Ow.c;
            case 6:
                return Ow.d;
            case 7:
            case 8:
                return Ow.e;
            case 9:
                return Ow.f;
            case 10:
                return Ow.g;
            default:
                return Ow.h;
        }
    }
}
