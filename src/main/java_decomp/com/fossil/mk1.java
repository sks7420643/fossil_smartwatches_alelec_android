package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mk1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Mk1 {
        @DexIgnore
        public volatile boolean a;

        @DexIgnore
        public Bi() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.Mk1
        public void b(boolean z) {
            this.a = z;
        }

        @DexIgnore
        @Override // com.fossil.Mk1
        public void c() {
            if (this.a) {
                throw new IllegalStateException("Already released");
            }
        }
    }

    @DexIgnore
    public Mk1() {
    }

    @DexIgnore
    public static Mk1 a() {
        return new Bi();
    }

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public abstract void c();
}
