package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mc2 implements Nc2 {
    @DexIgnore
    public /* final */ IBinder b;

    @DexIgnore
    public Mc2(IBinder iBinder) {
        this.b = iBinder;
    }

    @DexIgnore
    @Override // com.fossil.Nc2
    public final void Y(Lc2 lc2, Dc2 dc2) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
            obtain.writeStrongBinder(lc2 != null ? lc2.asBinder() : null);
            if (dc2 != null) {
                obtain.writeInt(1);
                dc2.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            this.b.transact(46, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public final IBinder asBinder() {
        return this.b;
    }
}
