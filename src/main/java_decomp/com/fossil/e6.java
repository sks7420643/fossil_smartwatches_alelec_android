package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E6 extends M5 {
    @DexIgnore
    public byte[] m; // = new byte[0];

    @DexIgnore
    public E6(N6 n6, N4 n4) {
        super(V5.f, n6, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.p(this.l);
        this.k = true;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return (h7 instanceof I7) && ((I7) h7).b == this.l;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.d;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.m = ((I7) h7).c;
    }
}
