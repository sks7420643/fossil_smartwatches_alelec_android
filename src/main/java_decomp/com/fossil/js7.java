package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ni6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Js7<T, R> extends Ni6<T, R>, Object<R> {

    @DexIgnore
    public interface Ai<T, R> extends Hs7<R>, Coroutine<T, R, Cd6> {
    }

    @DexIgnore
    Ai<T, R> getSetter();
}
