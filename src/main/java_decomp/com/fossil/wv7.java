package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Rm6;
import com.mapped.Xe6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wv7 {
    @DexIgnore
    public static /* final */ Vz7 a; // = new Vz7("UNDEFINED");
    @DexIgnore
    public static /* final */ Vz7 b; // = new Vz7("REUSABLE_CLAIMED");

    @DexIgnore
    public static final <T> void b(Xe6<? super T> xe6, Object obj) {
        boolean z;
        if (xe6 instanceof Vv7) {
            Vv7 vv7 = (Vv7) xe6;
            Object b2 = Wu7.b(obj);
            if (vv7.h.Q(vv7.getContext())) {
                vv7.e = b2;
                vv7.d = 1;
                vv7.h.M(vv7.getContext(), vv7);
                return;
            }
            Hw7 b3 = Wx7.b.b();
            if (b3.o0()) {
                vv7.e = b2;
                vv7.d = 1;
                b3.X(vv7);
                return;
            }
            b3.g0(true);
            try {
                Rm6 rm6 = (Rm6) vv7.getContext().get(Rm6.r);
                if (rm6 == null || rm6.isActive()) {
                    z = false;
                } else {
                    CancellationException k = rm6.k();
                    Dl7.Ai ai = Dl7.Companion;
                    vv7.resumeWith(Dl7.constructor-impl(El7.a(k)));
                    z = true;
                }
                if (!z) {
                    Af6 context = vv7.getContext();
                    Object c = Zz7.c(context, vv7.g);
                    try {
                        vv7.i.resumeWith(obj);
                        Cd6 cd6 = Cd6.a;
                    } finally {
                        Zz7.a(context, c);
                    }
                }
                do {
                } while (b3.r0());
            } catch (Throwable th) {
                b3.S(true);
                throw th;
            }
            b3.S(true);
            return;
        }
        xe6.resumeWith(obj);
    }

    @DexIgnore
    public static final boolean c(Vv7<? super Cd6> vv7) {
        Cd6 cd6 = Cd6.a;
        Hw7 b2 = Wx7.b.b();
        if (b2.p0()) {
            return false;
        }
        if (b2.o0()) {
            vv7.e = cd6;
            vv7.d = 1;
            b2.X(vv7);
            return true;
        }
        b2.g0(true);
        try {
            vv7.run();
            do {
            } while (b2.r0());
        } catch (Throwable th) {
            b2.S(true);
            throw th;
        }
        b2.S(true);
        return false;
    }
}
