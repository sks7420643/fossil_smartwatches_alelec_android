package com.fossil;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.customview.CircleProgressView;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ox4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public /* final */ Ki g; // = new Ki(this);
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public G37<H45> i;
    @DexIgnore
    public BCCurrentChallengeSubTabViewModel j;
    @DexIgnore
    public /* final */ TimerViewObserver k; // = new TimerViewObserver();
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ox4.m;
        }

        @DexIgnore
        public final Ox4 b() {
            return new Ox4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Ox4 ox4) {
            super(1);
            this.this$0 = ox4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            Ox4.M6(this.this$0).H();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements InterceptSwipe.j {
        @DexIgnore
        public /* final */ /* synthetic */ H45 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 b;

        @DexIgnore
        public Ci(H45 h45, Ox4 ox4) {
            this.a = h45;
            this.b = ox4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.InterceptSwipe.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.w;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.a.v;
            Wg6.b(flexibleTextView2, "ftvEmpty");
            flexibleTextView2.setVisibility(8);
            Ox4.M6(this.b).C();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Di(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            H45 h45 = (H45) Ox4.K6(this.a).a();
            if (h45 != null) {
                InterceptSwipe interceptSwipe = h45.B;
                Wg6.b(interceptSwipe, "swipeRefresh");
                Wg6.b(bool, "it");
                interceptSwipe.setRefreshing(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Ei(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Ox4 ox4 = this.a;
            Wg6.b(bool, "it");
            ox4.X6(bool.booleanValue());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Ps4> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Fi(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ox4.s.a();
            local.e(a2, "challengeLive - challenge: " + ps4 + " - " + System.currentTimeMillis());
            if (ps4 != null) {
                this.a.Y6(ps4);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Ps4 ps4) {
            a(ps4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Lc6<? extends Boolean, ? extends Ps4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Gi(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, Ps4> lc6) {
            if (lc6.getFirst().booleanValue()) {
                this.a.W6(lc6.getSecond());
            } else {
                this.a.V6(lc6.getSecond());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends Ps4> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<Gl7<? extends Ks4, ? extends String, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Hi(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Gl7<Ks4, String, Integer> gl7) {
            Ox4 ox4 = this.a;
            Wg6.b(gl7, "triple");
            ox4.Z6(gl7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends Ks4, ? extends String, ? extends Integer> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Ii(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            H45 h45 = (H45) Ox4.K6(this.a).a();
            if (h45 == null) {
                return;
            }
            if (lc6.getFirst().booleanValue()) {
                FlexibleTextView flexibleTextView = h45.w;
                Wg6.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = h45.w;
            Wg6.b(flexibleTextView2, "ftvError");
            flexibleTextView2.setVisibility(8);
            FlexibleTextView flexibleTextView3 = h45.w;
            Wg6.b(flexibleTextView3, "ftvError");
            String c = Um5.c(flexibleTextView3.getContext(), 2131886231);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, c, 1).show();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        public Ji(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            FlexibleTextView flexibleTextView;
            H45 h45 = (H45) Ox4.K6(this.a).a();
            if (h45 != null && (flexibleTextView = h45.z) != null) {
                flexibleTextView.setVisibility(Wg6.a(bool, Boolean.FALSE) ? 0 : 8);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ Ox4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ki(Ox4 ox4) {
            this.a = ox4;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Boolean valueOf = intent != null ? Boolean.valueOf(intent.getBooleanExtra("set_sync_data_extra", true)) : null;
            Ox4.M6(this.a).J(valueOf != null ? valueOf.booleanValue() : true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ H45 a;

        @DexIgnore
        public Li(H45 h45) {
            this.a = h45;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            TimerTextView timerTextView = this.a.t;
            Wg6.b(timerTextView, "ftvCountDown");
            Wg6.b(valueAnimator, "it");
            timerTextView.setText(valueAnimator.getAnimatedValue().toString());
        }
    }

    /*
    static {
        String simpleName = Ox4.class.getSimpleName();
        Wg6.b(simpleName, "BCCurrentChallengeSubTab\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Ox4 ox4) {
        G37<H45> g37 = ox4.i;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCCurrentChallengeSubTabViewModel M6(Ox4 ox4) {
        BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel = ox4.j;
        if (bCCurrentChallengeSubTabViewModel != null) {
            return bCCurrentChallengeSubTabViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        G37<H45> g37 = this.i;
        if (g37 != null) {
            H45 a2 = g37.a();
            if (a2 != null) {
                InterceptSwipe interceptSwipe = a2.B;
                Wg6.b(interceptSwipe, "swipeRefresh");
                interceptSwipe.setRefreshing(true);
                CircleProgressView circleProgressView = a2.q;
                Wg6.b(circleProgressView, "circleProgress");
                Fz4.a(circleProgressView, new Bi(this));
                a2.B.setOnRefreshListener(new Ci(a2, this));
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final IntentFilter T6() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("set_sync_data_event");
        return intentFilter;
    }

    @DexIgnore
    public final void U6() {
        BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel = this.j;
        if (bCCurrentChallengeSubTabViewModel != null) {
            bCCurrentChallengeSubTabViewModel.x().h(getViewLifecycleOwner(), new Di(this));
            BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel2 = this.j;
            if (bCCurrentChallengeSubTabViewModel2 != null) {
                bCCurrentChallengeSubTabViewModel2.v().h(getViewLifecycleOwner(), new Ei(this));
                BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel3 = this.j;
                if (bCCurrentChallengeSubTabViewModel3 != null) {
                    bCCurrentChallengeSubTabViewModel3.t().h(getViewLifecycleOwner(), new Fi(this));
                    BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel4 = this.j;
                    if (bCCurrentChallengeSubTabViewModel4 != null) {
                        bCCurrentChallengeSubTabViewModel4.z().h(getViewLifecycleOwner(), new Gi(this));
                        BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel5 = this.j;
                        if (bCCurrentChallengeSubTabViewModel5 != null) {
                            bCCurrentChallengeSubTabViewModel5.u().h(getViewLifecycleOwner(), new Hi(this));
                            BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel6 = this.j;
                            if (bCCurrentChallengeSubTabViewModel6 != null) {
                                bCCurrentChallengeSubTabViewModel6.w().h(getViewLifecycleOwner(), new Ii(this));
                                BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel7 = this.j;
                                if (bCCurrentChallengeSubTabViewModel7 != null) {
                                    bCCurrentChallengeSubTabViewModel7.y().h(getViewLifecycleOwner(), new Ji(this));
                                } else {
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(Ps4 ps4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "startDetailScreen - challenge: " + ps4);
        BCWaitingChallengeDetailActivity.a aVar = BCWaitingChallengeDetailActivity.B;
        Fragment requireParentFragment = requireParentFragment();
        Wg6.b(requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment, ps4, "joined_challenge", -1, false);
    }

    @DexIgnore
    public final void W6(Ps4 ps4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "startOverviewLeaderBoard - challenge: " + ps4);
        BCOverviewLeaderBoardActivity.A.a(this, ps4, null);
    }

    @DexIgnore
    public final void X6(boolean z) {
        G37<H45> g37 = this.i;
        if (g37 != null) {
            H45 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(8);
                if (z) {
                    Cl5.c.e();
                    FlexibleTextView flexibleTextView2 = a2.v;
                    Wg6.b(flexibleTextView2, "ftvEmpty");
                    flexibleTextView2.setVisibility(0);
                    TimerTextView timerTextView = a2.r;
                    Wg6.b(timerTextView, "ftvChallengeInfo");
                    timerTextView.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.s;
                    Wg6.b(flexibleTextView3, "ftvChallengeName");
                    flexibleTextView3.setVisibility(8);
                    FlexibleTextView flexibleTextView4 = a2.y;
                    Wg6.b(flexibleTextView4, "ftvRanking");
                    flexibleTextView4.setVisibility(8);
                    TimerTextView timerTextView2 = a2.t;
                    Wg6.b(timerTextView2, "ftvCountDown");
                    timerTextView2.setVisibility(8);
                    CircleProgressView circleProgressView = a2.q;
                    Wg6.b(circleProgressView, "circleProgress");
                    circleProgressView.setVisibility(8);
                    FlexibleTextView flexibleTextView5 = a2.x;
                    Wg6.b(flexibleTextView5, "ftvPlayerInfo");
                    flexibleTextView5.setVisibility(8);
                    FlexibleTextView flexibleTextView6 = a2.u;
                    Wg6.b(flexibleTextView6, "ftvCountPlayers");
                    flexibleTextView6.setVisibility(8);
                    FlexibleTextView flexibleTextView7 = a2.z;
                    Wg6.b(flexibleTextView7, "ftvSetSyncData");
                    flexibleTextView7.setVisibility(8);
                    TimerTextView timerTextView3 = a2.t;
                    Wg6.b(timerTextView3, "ftvCountDown");
                    timerTextView3.setTag(null);
                    a2.t.r();
                    a2.r.r();
                    TimerTextView timerTextView4 = a2.r;
                    Wg6.b(timerTextView4, "ftvChallengeInfo");
                    timerTextView4.setText("");
                    a2.q.e();
                    return;
                }
                FlexibleTextView flexibleTextView8 = a2.v;
                Wg6.b(flexibleTextView8, "ftvEmpty");
                flexibleTextView8.setVisibility(8);
                TimerTextView timerTextView5 = a2.r;
                Wg6.b(timerTextView5, "ftvChallengeInfo");
                timerTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView9 = a2.s;
                Wg6.b(flexibleTextView9, "ftvChallengeName");
                flexibleTextView9.setVisibility(0);
                FlexibleTextView flexibleTextView10 = a2.y;
                Wg6.b(flexibleTextView10, "ftvRanking");
                flexibleTextView10.setVisibility(0);
                TimerTextView timerTextView6 = a2.t;
                Wg6.b(timerTextView6, "ftvCountDown");
                timerTextView6.setVisibility(0);
                CircleProgressView circleProgressView2 = a2.q;
                Wg6.b(circleProgressView2, "circleProgress");
                circleProgressView2.setVisibility(0);
                FlexibleTextView flexibleTextView11 = a2.x;
                Wg6.b(flexibleTextView11, "ftvPlayerInfo");
                flexibleTextView11.setVisibility(0);
                FlexibleTextView flexibleTextView12 = a2.u;
                Wg6.b(flexibleTextView12, "ftvCountPlayers");
                flexibleTextView12.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Y6(Ps4 ps4) {
        String str;
        String a2;
        long j2 = 0;
        int i2 = 0;
        G37<H45> g37 = this.i;
        if (g37 != null) {
            H45 a3 = g37.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.w;
                Wg6.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a3.s;
                Wg6.b(flexibleTextView2, "ftvChallengeName");
                flexibleTextView2.setText(ps4.g());
                Integer h2 = ps4.h();
                int intValue = h2 != null ? h2.intValue() : 1;
                FlexibleTextView flexibleTextView3 = a3.u;
                Wg6.b(flexibleTextView3, "ftvCountPlayers");
                Object tag = flexibleTextView3.getTag();
                if (!(tag instanceof Integer)) {
                    tag = null;
                }
                Integer num = (Integer) tag;
                int intValue2 = num != null ? num.intValue() : 0;
                if (Wg6.a(ps4.n(), "waiting")) {
                    FlexibleTextView flexibleTextView4 = a3.u;
                    Wg6.b(flexibleTextView4, "ftvCountPlayers");
                    flexibleTextView4.setTag(Integer.valueOf(intValue));
                    FlexibleTextView flexibleTextView5 = a3.u;
                    Wg6.b(flexibleTextView5, "ftvCountPlayers");
                    flexibleTextView5.setText(String.valueOf(intValue));
                } else if (intValue > intValue2) {
                    FlexibleTextView flexibleTextView6 = a3.u;
                    Wg6.b(flexibleTextView6, "ftvCountPlayers");
                    flexibleTextView6.setTag(Integer.valueOf(intValue));
                    FlexibleTextView flexibleTextView7 = a3.u;
                    Wg6.b(flexibleTextView7, "ftvCountPlayers");
                    flexibleTextView7.setText(String.valueOf(intValue));
                }
                String k2 = ps4.k();
                if (k2 != null && k2.hashCode() == -314497661 && k2.equals("private")) {
                    a3.u.setCompoundDrawablesWithIntrinsicBounds(2131231028, 0, 0, 0);
                } else {
                    a3.u.setCompoundDrawablesWithIntrinsicBounds(2131231029, 0, 0, 0);
                }
                String r = ps4.r();
                if (r != null) {
                    int hashCode = r.hashCode();
                    if (hashCode != -1348781656) {
                        if (hashCode == -637042289 && r.equals("activity_reach_goal")) {
                            TimerTextView timerTextView = a3.t;
                            Wg6.b(timerTextView, "ftvCountDown");
                            timerTextView.setTextColor(W6.d(timerTextView.getContext(), 2131099689));
                            a3.r.setHighlightColour(2131099689);
                        }
                    } else if (r.equals("activity_best_result")) {
                        TimerTextView timerTextView2 = a3.t;
                        Wg6.b(timerTextView2, "ftvCountDown");
                        Jl5 jl5 = Jl5.b;
                        Integer d = ps4.d();
                        timerTextView2.setText(jl5.s(d != null ? d.intValue() : 0));
                        TimerTextView timerTextView3 = a3.t;
                        Wg6.b(timerTextView3, "ftvCountDown");
                        timerTextView3.setTextColor(W6.d(timerTextView3.getContext(), 2131099677));
                        a3.r.setHighlightColour(2131099677);
                    }
                }
                String n = ps4.n();
                if (n != null) {
                    int hashCode2 = n.hashCode();
                    if (hashCode2 != 1116313165) {
                        if (hashCode2 == 1550783935 && n.equals("running")) {
                            a3.r.r();
                            String r2 = ps4.r();
                            if (r2 != null) {
                                int hashCode3 = r2.hashCode();
                                if (hashCode3 != -1348781656) {
                                    if (hashCode3 == -637042289 && r2.equals("activity_reach_goal")) {
                                        TimerTextView timerTextView4 = a3.t;
                                        Wg6.b(timerTextView4, "ftvCountDown");
                                        Object tag2 = timerTextView4.getTag();
                                        if (!(tag2 instanceof Integer)) {
                                            tag2 = null;
                                        }
                                        if (((Integer) tag2) == null) {
                                            TimerTextView timerTextView5 = a3.t;
                                            Wg6.b(timerTextView5, "ftvCountDown");
                                            Integer q = ps4.q();
                                            timerTextView5.setText(q != null ? String.valueOf(q.intValue()) : null);
                                            a3.q.m(1, 1);
                                        }
                                        CircleProgressView circleProgressView = a3.q;
                                        Wg6.b(circleProgressView, "circleProgress");
                                        circleProgressView.setProgressColour(W6.d(circleProgressView.getContext(), 2131099689));
                                    }
                                } else if (r2.equals("activity_best_result")) {
                                    TimerTextView timerTextView6 = a3.t;
                                    Date e = ps4.e();
                                    timerTextView6.setTime(e != null ? e.getTime() : 0);
                                    a3.t.setDisplayType(Wy4.HOUR_MIN_SEC);
                                    a3.t.setObserver(this.k);
                                    CircleProgressView circleProgressView2 = a3.q;
                                    Wg6.b(circleProgressView2, "circleProgress");
                                    circleProgressView2.setProgressColour(W6.d(circleProgressView2.getContext(), 2131099677));
                                    a3.q.m(1, 1);
                                    Integer d2 = ps4.d();
                                    if (d2 != null) {
                                        i2 = d2.intValue();
                                    }
                                    CircleProgressView circleProgressView3 = a3.q;
                                    long j3 = (long) i2;
                                    Date e2 = ps4.e();
                                    if (e2 != null) {
                                        j2 = e2.getTime();
                                    }
                                    circleProgressView3.j(j3 * 1000, j2);
                                    CircleProgressView circleProgressView4 = a3.q;
                                    circleProgressView4.k(this.k, circleProgressView4.getIdentity());
                                }
                            }
                            TimerTextView timerTextView7 = a3.t;
                            Wg6.b(timerTextView7, "ftvCountDown");
                            Object tag3 = timerTextView7.getTag();
                            if (!(tag3 instanceof Integer)) {
                                tag3 = null;
                            }
                            if (((Integer) tag3) == null) {
                                FlexibleTextView flexibleTextView8 = a3.x;
                                Wg6.b(flexibleTextView8, "ftvPlayerInfo");
                                flexibleTextView8.setText("--");
                                TimerTextView timerTextView8 = a3.r;
                                Wg6.b(timerTextView8, "ftvChallengeInfo");
                                timerTextView8.setText("--");
                            }
                        }
                    } else if (n.equals("waiting")) {
                        TimerTextView timerTextView9 = a3.t;
                        Wg6.b(timerTextView9, "ftvCountDown");
                        timerTextView9.setTag(null);
                        String l0 = PortfolioApp.get.instance().l0();
                        Ht4 i3 = ps4.i();
                        if (Wg6.a(l0, i3 != null ? i3.b() : null)) {
                            a2 = Um5.c(PortfolioApp.get.instance(), 2131886250);
                        } else {
                            Hz4 hz4 = Hz4.a;
                            Ht4 i4 = ps4.i();
                            String a4 = i4 != null ? i4.a() : null;
                            Ht4 i5 = ps4.i();
                            String c = i5 != null ? i5.c() : null;
                            Ht4 i6 = ps4.i();
                            if (i6 == null || (str = i6.d()) == null) {
                                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                            }
                            a2 = hz4.a(a4, c, str);
                        }
                        FlexibleTextView flexibleTextView9 = a3.x;
                        Wg6.b(flexibleTextView9, "ftvPlayerInfo");
                        Jl5 jl52 = Jl5.b;
                        Wg6.b(a2, "owner");
                        Hr7 hr7 = Hr7.a;
                        FlexibleTextView flexibleTextView10 = a3.y;
                        Wg6.b(flexibleTextView10, "ftvRanking");
                        String c2 = Um5.c(flexibleTextView10.getContext(), 2131886255);
                        Wg6.b(c2, "LanguageHelper.getString\u2026ding_Text__CreatedByName)");
                        String format = String.format(c2, Arrays.copyOf(new Object[]{a2}, 1));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                        flexibleTextView9.setText(Jl5.b(jl52, a2, format, 0, 4, null));
                        FlexibleTextView flexibleTextView11 = a3.y;
                        Wg6.b(flexibleTextView11, "ftvRanking");
                        Hr7 hr72 = Hr7.a;
                        FlexibleTextView flexibleTextView12 = a3.y;
                        Wg6.b(flexibleTextView12, "ftvRanking");
                        String c3 = Um5.c(flexibleTextView12.getContext(), 2131887312);
                        Wg6.b(c3, "LanguageHelper.getString\u2026.buddy_challenge_ranking)");
                        String format2 = String.format(c3, Arrays.copyOf(new Object[]{0, Integer.valueOf(intValue)}, 2));
                        Wg6.b(format2, "java.lang.String.format(format, *args)");
                        flexibleTextView11.setText(format2);
                        CircleProgressView circleProgressView5 = a3.q;
                        Wg6.b(circleProgressView5, "circleProgress");
                        circleProgressView5.setProgressColour(W6.d(circleProgressView5.getContext(), R.color.transparent));
                        a3.q.m(0, 1);
                        a3.r.setDisplayType(Wy4.START_LEFT);
                        TimerTextView timerTextView10 = a3.r;
                        Date m2 = ps4.m();
                        if (m2 != null) {
                            j2 = m2.getTime();
                        }
                        timerTextView10.setTime(j2);
                        a3.r.setObserver(this.k);
                        if (Wg6.a("activity_reach_goal", ps4.r())) {
                            TimerTextView timerTextView11 = a3.t;
                            Wg6.b(timerTextView11, "ftvCountDown");
                            timerTextView11.setText(String.valueOf(0));
                        }
                    }
                }
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0429  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Z6(com.fossil.Gl7<com.fossil.Ks4, java.lang.String, java.lang.Integer> r19) {
        /*
        // Method dump skipped, instructions count: 1078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ox4.Z6(com.fossil.Gl7):void");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().F().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCCurrentChallengeSubTabViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.j = (BCCurrentChallengeSubTabViewModel) a2;
            getLifecycle().a(this.k);
            BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel = this.j;
            if (bCCurrentChallengeSubTabViewModel != null) {
                bCCurrentChallengeSubTabViewModel.r();
                Ct0.b(requireContext()).c(this.g, T6());
                return;
            }
            Wg6.n("viewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        H45 h45 = (H45) Aq0.f(layoutInflater, 2131558528, viewGroup, false, A6());
        this.i = new G37<>(this, h45);
        Wg6.b(h45, "binding");
        return h45.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().c(this.k);
        Ct0.b(requireContext()).e(this.g);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel = this.j;
        if (bCCurrentChallengeSubTabViewModel != null) {
            bCCurrentChallengeSubTabViewModel.B();
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel = this.j;
        if (bCCurrentChallengeSubTabViewModel != null) {
            bCCurrentChallengeSubTabViewModel.D();
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        S6();
        U6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
