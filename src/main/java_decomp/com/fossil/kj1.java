package com.fossil;

import android.graphics.Bitmap;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kj1 extends Nj1<Bitmap> {
    @DexIgnore
    public Kj1(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Nj1
    public /* bridge */ /* synthetic */ void o(Bitmap bitmap) {
        q(bitmap);
    }

    @DexIgnore
    public void q(Bitmap bitmap) {
        ((ImageView) this.b).setImageBitmap(bitmap);
    }
}
