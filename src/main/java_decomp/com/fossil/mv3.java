package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mv3 extends Ba3 implements Lv3 {
    @DexIgnore
    public Mv3() {
        super("com.google.android.gms.wearable.internal.IWearableListener");
    }

    @DexIgnore
    @Override // com.fossil.Ba3
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                O((DataHolder) Ca3.a(parcel, DataHolder.CREATOR));
                break;
            case 2:
                a1((Nv3) Ca3.a(parcel, Nv3.CREATOR));
                break;
            case 3:
                i0((Pv3) Ca3.a(parcel, Pv3.CREATOR));
                break;
            case 4:
                T1((Pv3) Ca3.a(parcel, Pv3.CREATOR));
                break;
            case 5:
                U2(parcel.createTypedArrayList(Pv3.CREATOR));
                break;
            case 6:
                k2((Uv3) Ca3.a(parcel, Uv3.CREATOR));
                break;
            case 7:
                r((Ev3) Ca3.a(parcel, Ev3.CREATOR));
                break;
            case 8:
                t0((Av3) Ca3.a(parcel, Av3.CREATOR));
                break;
            case 9:
                L0((Sv3) Ca3.a(parcel, Sv3.CREATOR));
                break;
            default:
                return false;
        }
        return true;
    }
}
