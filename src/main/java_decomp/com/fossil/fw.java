package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fw extends Tv {
    @DexIgnore
    public long L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Fw(long j, long j2, long j3, short s, K5 k5, int i, int i2) {
        super(Sv.g, s, Hs.R, k5, (i2 & 32) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.M = true;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.T2, Long.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            long o = Hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            this.L = o;
            G80.k(jSONObject, Jd0.T2, Long.valueOf(o));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        Wg6.b(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.Tv, com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(G80.k(super.z(), Jd0.Q2, Long.valueOf(this.N)), Jd0.R2, Long.valueOf(this.O)), Jd0.S2, Long.valueOf(this.P));
    }
}
