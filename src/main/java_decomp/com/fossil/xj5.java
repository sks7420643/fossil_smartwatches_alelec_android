package com.fossil;

import com.fossil.Af1;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xj5 implements Af1<Yj5, InputStream> {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ Ai b; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Xj5.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Bf1<Yj5, InputStream> {
        @DexIgnore
        public Xj5 a(Ef1 ef1) {
            Wg6.c(ef1, "multiFactory");
            return new Xj5();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Af1' to match base method */
        @Override // com.fossil.Bf1
        public /* bridge */ /* synthetic */ Af1<Yj5, InputStream> b(Ef1 ef1) {
            return a(ef1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements Wb1<InputStream> {
        @DexIgnore
        public volatile boolean b;
        @DexIgnore
        public /* final */ Yj5 c;

        @DexIgnore
        public Ci(Xj5 xj5, Yj5 yj5) {
            this.c = yj5;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
            this.b = true;
        }

        @DexIgnore
        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('+' char), (wrap: int : 0x018f: ARITH  (r1v21 int) = (wrap: int : ?: ARITH  null = (r2v1 int) - (4 int)) + (1 int))] */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x01e1  */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x01ec  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x026b  */
        @Override // com.fossil.Wb1
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void d(com.fossil.Sa1 r9, com.fossil.Wb1.Ai<? super java.io.InputStream> r10) {
            /*
            // Method dump skipped, instructions count: 630
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Xj5.Ci.d(com.fossil.Sa1, com.fossil.Wb1$Ai):void");
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    /*
    static {
        String simpleName = Xj5.class.getSimpleName();
        Wg6.b(simpleName, "NotificationLoader::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Yj5 yj5) {
        return e(yj5);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(Yj5 yj5, int i, int i2, Ob1 ob1) {
        return d(yj5, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> d(Yj5 yj5, int i, int i2, Ob1 ob1) {
        Wg6.c(yj5, "notificationModel");
        Wg6.c(ob1, "options");
        return new Af1.Ai<>(yj5, new Ci(this, yj5));
    }

    @DexIgnore
    public boolean e(Yj5 yj5) {
        Wg6.c(yj5, "notificationModel");
        return true;
    }
}
