package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zs1 extends Ox1 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Ys1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Zs1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zs1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                Parcelable readParcelable = parcel.readParcelable(Ys1.class.getClassLoader());
                if (readParcelable != null) {
                    Wg6.b(readParcelable, "parcel.readParcelable<Fi\u2026class.java.classLoader)!!");
                    return new Zs1(readString, readInt, (Ys1) readParcelable);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zs1[] newArray(int i) {
            return new Zs1[i];
        }
    }

    @DexIgnore
    public Zs1(String str, int i, Ys1 ys1) {
        this.b = str;
        this.c = i;
        this.d = ys1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Zs1) {
                Zs1 zs1 = (Zs1) obj;
                if (!Wg6.a(this.b, zs1.b) || this.c != zs1.c || !Wg6.a(this.d, zs1.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Ys1 getFitnessData() {
        return this.d;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final int getRank() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        int i2 = this.c;
        Ys1 ys1 = this.d;
        if (ys1 != null) {
            i = ys1.hashCode();
        }
        return (((hashCode * 31) + i2) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return Gy1.c(G80.k(G80.k(new JSONObject(), Jd0.H, this.b), Jd0.G4, Integer.valueOf(this.c)), this.d.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e = E.e("Player(name=");
        e.append(this.b);
        e.append(", rank=");
        e.append(this.c);
        e.append(", fitnessData=");
        e.append(this.d);
        e.append(")");
        return e.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeParcelable(this.d, i);
    }
}
