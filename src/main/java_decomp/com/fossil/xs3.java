package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.M62;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xs3 extends M62.Ai<Hs3, Gs3> {
    @DexIgnore
    /* Return type fixed from 'com.fossil.M62$Fi' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.content.Context, android.os.Looper, com.fossil.Ac2, java.lang.Object, com.fossil.R62$Bi, com.fossil.R62$Ci] */
    @Override // com.fossil.M62.Ai
    public final /* synthetic */ Hs3 c(Context context, Looper looper, Ac2 ac2, Gs3 gs3, R62.Bi bi, R62.Ci ci) {
        Gs3 gs32 = gs3;
        return new Hs3(context, looper, true, ac2, gs32 == null ? Gs3.k : gs32, bi, ci);
    }
}
