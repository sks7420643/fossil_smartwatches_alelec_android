package com.fossil;

import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class It5 implements Factory<ReconnectDeviceUseCase> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ It5 a; // = new It5();
    }

    @DexIgnore
    public static It5 a() {
        return Ai.a;
    }

    @DexIgnore
    public static ReconnectDeviceUseCase c() {
        return new ReconnectDeviceUseCase();
    }

    @DexIgnore
    public ReconnectDeviceUseCase b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
