package com.fossil;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Az1 implements A02 {
    @DexIgnore
    public static /* final */ String c; // = Uz1.a("hts/frbslgiggolai.o/0clgbthfra=snpoo", "tp:/ieaeogn.ogepscmvc/o/ac?omtjo_rt3");
    @DexIgnore
    public static /* final */ String d; // = Uz1.a("hts/frbslgigp.ogepscmv/ieo/eaybtho", "tp:/ieaeogn-agolai.o/1frlglgc/aclg");
    @DexIgnore
    public static /* final */ String e; // = Uz1.a("AzSCki82AwsLzKd5O8zo", "IayckHiZRO1EFl1aGoK");
    @DexIgnore
    public static /* final */ Set<Ty1> f; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(Ty1.b("proto"), Ty1.b("json"))));
    @DexIgnore
    public static /* final */ Az1 g; // = new Az1(d, e);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Az1(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public static Az1 c(byte[] bArr) {
        String str = new String(bArr, Charset.forName("UTF-8"));
        if (str.startsWith("1$")) {
            String[] split = str.substring(2).split(Pattern.quote("\\"), 2);
            if (split.length == 2) {
                String str2 = split[0];
                if (!str2.isEmpty()) {
                    String str3 = split[1];
                    if (str3.isEmpty()) {
                        str3 = null;
                    }
                    return new Az1(str2, str3);
                }
                throw new IllegalArgumentException("Missing endpoint in CCTDestination extras");
            }
            throw new IllegalArgumentException("Extra is not a valid encoded LegacyFlgDestination");
        }
        throw new IllegalArgumentException("Version marker missing from extras");
    }

    @DexIgnore
    @Override // com.fossil.A02
    public Set<Ty1> a() {
        return f;
    }

    @DexIgnore
    public byte[] b() {
        if (this.b == null && this.a == null) {
            return null;
        }
        String str = this.a;
        String str2 = this.b;
        if (str2 == null) {
            str2 = "";
        }
        return String.format("%s%s%s%s", "1$", str, "\\", str2).getBytes(Charset.forName("UTF-8"));
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public String e() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Zz1
    public byte[] getExtras() {
        return b();
    }

    @DexIgnore
    @Override // com.fossil.Zz1
    public String getName() {
        return "cct";
    }
}
