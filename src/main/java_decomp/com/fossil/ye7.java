package com.fossil;

import android.os.Build;
import android.os.Looper;
import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ye7 {
    @DexIgnore
    public static int a; // = 6;
    @DexIgnore
    public static Ai b; // = new Ze7();

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(String str, String str2);

        @DexIgnore
        void b(String str, String str2);

        @DexIgnore
        int c();

        @DexIgnore
        void d(String str, String str2);

        @DexIgnore
        void i(String str, String str2);
    }

    /*
    static {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("VERSION.RELEASE:[" + Build.VERSION.RELEASE);
            sb.append("] VERSION.CODENAME:[" + Build.VERSION.CODENAME);
            sb.append("] VERSION.INCREMENTAL:[" + Build.VERSION.INCREMENTAL);
            sb.append("] BOARD:[" + Build.BOARD);
            sb.append("] DEVICE:[" + Build.DEVICE);
            sb.append("] DISPLAY:[" + Build.DISPLAY);
            sb.append("] FINGERPRINT:[" + Build.FINGERPRINT);
            sb.append("] HOST:[" + Build.HOST);
            sb.append("] MANUFACTURER:[" + Build.MANUFACTURER);
            sb.append("] MODEL:[" + Build.MODEL);
            sb.append("] PRODUCT:[" + Build.PRODUCT);
            sb.append("] TAGS:[" + Build.TAGS);
            sb.append("] TYPE:[" + Build.TYPE);
            sb.append("] USER:[" + Build.USER + "]");
        } catch (Throwable th) {
            th.printStackTrace();
        }
        sb.toString();
    }
    */

    @DexIgnore
    public static void a(String str, String str2, Object... objArr) {
        Ai ai = b;
        if (ai != null && ai.c() <= 4) {
            String format = objArr == null ? str2 : String.format(str2, objArr);
            if (format == null) {
                format = "";
            }
            Ai ai2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            ai2.i(str, format);
        }
    }

    @DexIgnore
    public static void b(String str, String str2) {
        a(str, str2, null);
    }

    @DexIgnore
    public static void c(String str, String str2) {
        Ai ai = b;
        if (ai != null && ai.c() <= 3) {
            Ai ai2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            ai2.d(str, str2);
        }
    }

    @DexIgnore
    public static void d(String str, String str2) {
        Ai ai = b;
        if (ai != null && ai.c() <= 2) {
            Ai ai2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            ai2.b(str, str2);
        }
    }

    @DexIgnore
    public static void e(String str, String str2) {
        Ai ai = b;
        if (ai != null && ai.c() <= 1) {
            if (str2 == null) {
                str2 = "";
            }
            Ai ai2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            ai2.a(str, str2);
        }
    }
}
