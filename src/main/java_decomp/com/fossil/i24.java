package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I24<E> extends P24<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Queue<E> delegate;
    @DexIgnore
    public /* final */ int maxSize;

    @DexIgnore
    public I24(int i) {
        I14.f(i >= 0, "maxSize (%s) must >= 0", i);
        this.delegate = new ArrayDeque(i);
        this.maxSize = i;
    }

    @DexIgnore
    public static <E> I24<E> create(int i) {
        return new I24<>(i);
    }

    @DexIgnore
    @Override // com.fossil.L24, java.util.Collection, java.util.Queue
    @CanIgnoreReturnValue
    public boolean add(E e) {
        I14.l(e);
        if (this.maxSize != 0) {
            if (size() == this.maxSize) {
                this.delegate.remove();
            }
            this.delegate.add(e);
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.L24, java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        int size = collection.size();
        if (size < this.maxSize) {
            return standardAddAll(collection);
        }
        clear();
        return O34.a(this, O34.g(collection, size - this.maxSize));
    }

    @DexIgnore
    @Override // com.fossil.L24
    public boolean contains(Object obj) {
        Queue<E> delegate2 = delegate();
        I14.l(obj);
        return delegate2.contains(obj);
    }

    @DexIgnore
    @Override // com.fossil.L24, com.fossil.L24, com.fossil.O24, com.fossil.P24, com.fossil.P24, com.fossil.P24
    public Queue<E> delegate() {
        return this.delegate;
    }

    @DexIgnore
    @Override // com.fossil.P24, java.util.Queue
    @CanIgnoreReturnValue
    public boolean offer(E e) {
        return add(e);
    }

    @DexIgnore
    public int remainingCapacity() {
        return this.maxSize - size();
    }

    @DexIgnore
    @Override // com.fossil.L24
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        Queue<E> delegate2 = delegate();
        I14.l(obj);
        return delegate2.remove(obj);
    }
}
