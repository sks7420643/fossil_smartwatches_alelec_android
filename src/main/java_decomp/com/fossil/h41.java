package com.fossil;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H41<V> implements G64<V> {
    @DexIgnore
    public static /* final */ boolean e; // = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(H41.class.getName());
    @DexIgnore
    public static /* final */ Bi g;
    @DexIgnore
    public static /* final */ Object h; // = new Object();
    @DexIgnore
    public volatile Object b;
    @DexIgnore
    public volatile Ei c;
    @DexIgnore
    public volatile Ii d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public abstract boolean a(H41<?> h41, Ei ei, Ei ei2);

        @DexIgnore
        public abstract boolean b(H41<?> h41, Object obj, Object obj2);

        @DexIgnore
        public abstract boolean c(H41<?> h41, Ii ii, Ii ii2);

        @DexIgnore
        public abstract void d(Ii ii, Ii ii2);

        @DexIgnore
        public abstract void e(Ii ii, Thread thread);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public static /* final */ Ci c;
        @DexIgnore
        public static /* final */ Ci d;
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ Throwable b;

        /*
        static {
            if (H41.e) {
                d = null;
                c = null;
                return;
            }
            d = new Ci(false, null);
            c = new Ci(true, null);
        }
        */

        @DexIgnore
        public Ci(boolean z, Throwable th) {
            this.a = z;
            this.b = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public static /* final */ Di b; // = new Di(new Aii("Failure occurred while trying to finish a future."));
        @DexIgnore
        public /* final */ Throwable a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Throwable {
            @DexIgnore
            public Aii(String str) {
                super(str);
            }

            @DexIgnore
            public Throwable fillInStackTrace() {
                synchronized (this) {
                }
                return this;
            }
        }

        @DexIgnore
        public Di(Throwable th) {
            H41.e(th);
            this.a = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei {
        @DexIgnore
        public static /* final */ Ei d; // = new Ei(null, null);
        @DexIgnore
        public /* final */ Runnable a;
        @DexIgnore
        public /* final */ Executor b;
        @DexIgnore
        public Ei c;

        @DexIgnore
        public Ei(Runnable runnable, Executor executor) {
            this.a = runnable;
            this.b = executor;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Bi {
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<Ii, Thread> a;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<Ii, Ii> b;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<H41, Ii> c;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<H41, Ei> d;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<H41, Object> e;

        @DexIgnore
        public Fi(AtomicReferenceFieldUpdater<Ii, Thread> atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater<Ii, Ii> atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater<H41, Ii> atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater<H41, Ei> atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater<H41, Object> atomicReferenceFieldUpdater5) {
            super();
            this.a = atomicReferenceFieldUpdater;
            this.b = atomicReferenceFieldUpdater2;
            this.c = atomicReferenceFieldUpdater3;
            this.d = atomicReferenceFieldUpdater4;
            this.e = atomicReferenceFieldUpdater5;
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public boolean a(H41<?> h41, Ei ei, Ei ei2) {
            return this.d.compareAndSet(h41, ei, ei2);
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public boolean b(H41<?> h41, Object obj, Object obj2) {
            return this.e.compareAndSet(h41, obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public boolean c(H41<?> h41, Ii ii, Ii ii2) {
            return this.c.compareAndSet(h41, ii, ii2);
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public void d(Ii ii, Ii ii2) {
            this.b.lazySet(ii, ii2);
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public void e(Ii ii, Thread thread) {
            this.a.lazySet(ii, thread);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<V> implements Runnable {
        @DexIgnore
        public /* final */ H41<V> b;
        @DexIgnore
        public /* final */ G64<? extends V> c;

        @DexIgnore
        public Gi(H41<V> h41, G64<? extends V> g64) {
            this.b = h41;
            this.c = g64;
        }

        @DexIgnore
        public void run() {
            if (this.b.b == this) {
                if (H41.g.b(this.b, this, H41.j(this.c))) {
                    H41.g(this.b);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Bi {
        @DexIgnore
        public Hi() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public boolean a(H41<?> h41, Ei ei, Ei ei2) {
            synchronized (h41) {
                if (h41.c != ei) {
                    return false;
                }
                h41.c = ei2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public boolean b(H41<?> h41, Object obj, Object obj2) {
            synchronized (h41) {
                if (h41.b != obj) {
                    return false;
                }
                h41.b = obj2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public boolean c(H41<?> h41, Ii ii, Ii ii2) {
            synchronized (h41) {
                if (h41.d != ii) {
                    return false;
                }
                h41.d = ii2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public void d(Ii ii, Ii ii2) {
            ii.b = ii2;
        }

        @DexIgnore
        @Override // com.fossil.H41.Bi
        public void e(Ii ii, Thread thread) {
            ii.a = thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii {
        @DexIgnore
        public static /* final */ Ii c; // = new Ii(false);
        @DexIgnore
        public volatile Thread a;
        @DexIgnore
        public volatile Ii b;

        @DexIgnore
        public Ii() {
            H41.g.e(this, Thread.currentThread());
        }

        @DexIgnore
        public Ii(boolean z) {
        }

        @DexIgnore
        public void a(Ii ii) {
            H41.g.d(this, ii);
        }

        @DexIgnore
        public void b() {
            Thread thread = this.a;
            if (thread != null) {
                this.a = null;
                LockSupport.unpark(thread);
            }
        }
    }

    /*
    static {
        Bi hi;
        Throwable th;
        try {
            th = null;
            hi = new Fi(AtomicReferenceFieldUpdater.newUpdater(Ii.class, Thread.class, "a"), AtomicReferenceFieldUpdater.newUpdater(Ii.class, Ii.class, "b"), AtomicReferenceFieldUpdater.newUpdater(H41.class, Ii.class, "d"), AtomicReferenceFieldUpdater.newUpdater(H41.class, Ei.class, "c"), AtomicReferenceFieldUpdater.newUpdater(H41.class, Object.class, "b"));
        } catch (Throwable th2) {
            hi = new Hi();
            th = th2;
        }
        g = hi;
        if (th != null) {
            f.log(Level.SEVERE, "SafeAtomicHelper is broken!", th);
        }
    }
    */

    @DexIgnore
    public static CancellationException d(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }

    @DexIgnore
    public static <T> T e(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r4v2. Raw type applied. Possible types: com.fossil.H41<V>, com.fossil.H41 */
    /* JADX WARN: Type inference failed for: r3v1, types: [com.fossil.H41$Bi] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void g(com.fossil.H41<?> r4) {
        /*
            r0 = 0
        L_0x0001:
            r4.n()
            r4.b()
            com.fossil.H41$Ei r1 = r4.f(r0)
            r2 = r1
        L_0x000c:
            if (r2 == 0) goto L_0x0035
            com.fossil.H41$Ei r1 = r2.c
            java.lang.Runnable r0 = r2.a
            boolean r3 = r0 instanceof com.fossil.H41.Gi
            if (r3 == 0) goto L_0x002e
            com.fossil.H41$Gi r0 = (com.fossil.H41.Gi) r0
            com.fossil.H41<V> r4 = r0.b
            java.lang.Object r2 = r4.b
            if (r2 != r0) goto L_0x0033
            com.fossil.G64<? extends V> r2 = r0.c
            java.lang.Object r2 = j(r2)
            com.fossil.H41$Bi r3 = com.fossil.H41.g
            boolean r0 = r3.b(r4, r0, r2)
            if (r0 == 0) goto L_0x0033
            r0 = r1
            goto L_0x0001
        L_0x002e:
            java.util.concurrent.Executor r2 = r2.b
            h(r0, r2)
        L_0x0033:
            r2 = r1
            goto L_0x000c
        L_0x0035:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H41.g(com.fossil.H41):void");
    }

    @DexIgnore
    public static void h(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e2) {
            Logger logger = f;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, (Throwable) e2);
        }
    }

    @DexIgnore
    public static Object j(G64<?> g64) {
        if (g64 instanceof H41) {
            Object obj = ((H41) g64).b;
            if (!(obj instanceof Ci)) {
                return obj;
            }
            Ci ci = (Ci) obj;
            return ci.a ? ci.b != null ? new Ci(false, ci.b) : Ci.d : obj;
        }
        boolean isCancelled = g64.isCancelled();
        if ((!e) && isCancelled) {
            return Ci.d;
        }
        try {
            Object k = k(g64);
            if (k == null) {
                k = h;
            }
            return k;
        } catch (ExecutionException e2) {
            return new Di(e2.getCause());
        } catch (CancellationException e3) {
            if (isCancelled) {
                return new Ci(false, e3);
            }
            return new Di(new IllegalArgumentException("get() threw CancellationException, despite reporting isCancelled() == false: " + g64, e3));
        } catch (Throwable th) {
            return new Di(th);
        }
    }

    @DexIgnore
    public static <V> V k(Future<V> future) throws ExecutionException {
        V v;
        boolean z = false;
        while (true) {
            try {
                v = future.get();
                break;
            } catch (InterruptedException e2) {
                z = true;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return v;
    }

    @DexIgnore
    public final void a(StringBuilder sb) {
        try {
            Object k = k(this);
            sb.append("SUCCESS, result=[");
            sb.append(s(k));
            sb.append("]");
        } catch (ExecutionException e2) {
            sb.append("FAILURE, cause=[");
            sb.append(e2.getCause());
            sb.append("]");
        } catch (CancellationException e3) {
            sb.append("CANCELLED");
        } catch (RuntimeException e4) {
            sb.append("UNKNOWN, cause=[");
            sb.append(e4.getClass());
            sb.append(" thrown from get()]");
        }
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.G64
    public final void c(Runnable runnable, Executor executor) {
        e(runnable);
        e(executor);
        Ei ei = this.c;
        if (ei != Ei.d) {
            Ei ei2 = new Ei(runnable, executor);
            do {
                ei2.c = ei;
                if (!g.a(this, ei, ei2)) {
                    ei = this.c;
                } else {
                    return;
                }
            } while (ei != Ei.d);
        }
        h(runnable, executor);
    }

    @DexIgnore
    public final boolean cancel(boolean z) {
        Object obj = this.b;
        if (!(obj == null) && !(obj instanceof Gi)) {
            return false;
        }
        Ci ci = e ? new Ci(z, new CancellationException("Future.cancel() was called.")) : z ? Ci.c : Ci.d;
        boolean z2 = false;
        Object obj2 = obj;
        while (true) {
            if (g.b(this, obj2, ci)) {
                if (z) {
                    this.l();
                }
                g(this);
                if (!(obj2 instanceof Gi)) {
                    return true;
                }
                G64<? extends V> g64 = ((Gi) obj2).c;
                if (g64 instanceof H41) {
                    H41<V> h41 = (H41) g64;
                    obj2 = h41.b;
                    if (!(obj2 == null) && !(obj2 instanceof Gi)) {
                        return true;
                    }
                    this = h41;
                    z2 = true;
                } else {
                    g64.cancel(z);
                    return true;
                }
            } else {
                obj2 = this.b;
                if (!(obj2 instanceof Gi)) {
                    return z2;
                }
            }
        }
    }

    @DexIgnore
    public final Ei f(Ei ei) {
        Ei ei2;
        do {
            ei2 = this.c;
        } while (!g.a(this, ei2, Ei.d));
        while (ei2 != null) {
            Ei ei3 = ei2.c;
            ei2.c = ei;
            ei = ei2;
            ei2 = ei3;
        }
        return ei;
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public final V get() throws InterruptedException, ExecutionException {
        Object obj;
        if (!Thread.interrupted()) {
            Object obj2 = this.b;
            if ((obj2 != null) && (!(obj2 instanceof Gi))) {
                return i(obj2);
            }
            Ii ii = this.d;
            if (ii != Ii.c) {
                Ii ii2 = new Ii();
                do {
                    ii2.a(ii);
                    if (g.c(this, ii, ii2)) {
                        do {
                            LockSupport.park(this);
                            if (!Thread.interrupted()) {
                                obj = this.b;
                            } else {
                                o(ii2);
                                throw new InterruptedException();
                            }
                        } while (!((obj != null) & (!(obj instanceof Gi))));
                        return i(obj);
                    }
                    ii = this.d;
                } while (ii != Ii.c);
            }
            return i(this.b);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public final V get(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long nanos = timeUnit.toNanos(j);
        if (!Thread.interrupted()) {
            Object obj = this.b;
            if ((obj != null) && (!(obj instanceof Gi))) {
                return i(obj);
            }
            long nanoTime = nanos > 0 ? System.nanoTime() + nanos : 0;
            if (nanos >= 1000) {
                Ii ii = this.d;
                if (ii != Ii.c) {
                    Ii ii2 = new Ii();
                    do {
                        ii2.a(ii);
                        if (g.c(this, ii, ii2)) {
                            long j2 = nanos;
                            do {
                                LockSupport.parkNanos(this, j2);
                                if (!Thread.interrupted()) {
                                    Object obj2 = this.b;
                                    if ((obj2 != null) && (!(obj2 instanceof Gi))) {
                                        return i(obj2);
                                    }
                                    j2 = nanoTime - System.nanoTime();
                                } else {
                                    o(ii2);
                                    throw new InterruptedException();
                                }
                            } while (j2 >= 1000);
                            o(ii2);
                            nanos = j2;
                        } else {
                            ii = this.d;
                        }
                    } while (ii != Ii.c);
                }
                return i(this.b);
            }
            while (nanos > 0) {
                Object obj3 = this.b;
                if ((obj3 != null) && (!(obj3 instanceof Gi))) {
                    return i(obj3);
                }
                if (!Thread.interrupted()) {
                    nanos = nanoTime - System.nanoTime();
                } else {
                    throw new InterruptedException();
                }
            }
            String h41 = toString();
            String lowerCase = timeUnit.toString().toLowerCase(Locale.ROOT);
            String str = "Waited " + j + " " + timeUnit.toString().toLowerCase(Locale.ROOT);
            if (1000 + nanos < 0) {
                String str2 = str + " (plus ";
                long j3 = -nanos;
                long convert = timeUnit.convert(j3, TimeUnit.NANOSECONDS);
                long nanos2 = j3 - timeUnit.toNanos(convert);
                int i = (convert > 0 ? 1 : (convert == 0 ? 0 : -1));
                boolean z = i == 0 || nanos2 > 1000;
                if (i > 0) {
                    String str3 = str2 + convert + " " + lowerCase;
                    if (z) {
                        str3 = str3 + ",";
                    }
                    str2 = str3 + " ";
                }
                if (z) {
                    str2 = str2 + nanos2 + " nanoseconds ";
                }
                str = str2 + "delay)";
            }
            if (isDone()) {
                throw new TimeoutException(str + " but future completed as timeout expired");
            }
            throw new TimeoutException(str + " for " + h41);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public final V i(Object obj) throws ExecutionException {
        if (obj instanceof Ci) {
            throw d("Task was cancelled.", ((Ci) obj).b);
        } else if (obj instanceof Di) {
            throw new ExecutionException(((Di) obj).a);
        } else if (obj == h) {
            return null;
        } else {
            return obj;
        }
    }

    @DexIgnore
    public final boolean isCancelled() {
        return this.b instanceof Ci;
    }

    @DexIgnore
    public final boolean isDone() {
        Object obj = this.b;
        return (obj != null) & (!(obj instanceof Gi));
    }

    @DexIgnore
    public void l() {
    }

    @DexIgnore
    public String m() {
        Object obj = this.b;
        if (obj instanceof Gi) {
            return "setFuture=[" + s(((Gi) obj).c) + "]";
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            return "remaining delay=[" + ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS) + " ms]";
        }
    }

    @DexIgnore
    public final void n() {
        Ii ii;
        do {
            ii = this.d;
        } while (!g.c(this, ii, Ii.c));
        while (ii != null) {
            ii.b();
            ii = ii.b;
        }
    }

    @DexIgnore
    public final void o(Ii ii) {
        ii.a = null;
        while (true) {
            Ii ii2 = this.d;
            if (ii2 != Ii.c) {
                Ii ii3 = null;
                while (ii2 != null) {
                    Ii ii4 = ii2.b;
                    if (ii2.a != null) {
                        ii3 = ii2;
                    } else if (ii3 != null) {
                        ii3.b = ii4;
                        if (ii3.a == null) {
                        }
                    } else if (!g.c(this, ii2, ii4)) {
                    }
                    ii2 = ii4;
                }
                return;
            }
            return;
        }
    }

    @DexIgnore
    public boolean p(V v) {
        if (v == null) {
            v = (V) h;
        }
        if (!g.b(this, null, v)) {
            return false;
        }
        g(this);
        return true;
    }

    @DexIgnore
    public boolean q(Throwable th) {
        e(th);
        if (!g.b(this, null, new Di(th))) {
            return false;
        }
        g(this);
        return true;
    }

    @DexIgnore
    public boolean r(G64<? extends V> g64) {
        Gi gi;
        Di di;
        e(g64);
        Object obj = this.b;
        if (obj == null) {
            if (g64.isDone()) {
                if (!g.b(this, null, j(g64))) {
                    return false;
                }
                g(this);
                return true;
            }
            gi = new Gi(this, g64);
            if (g.b(this, null, gi)) {
                try {
                    g64.c(gi, I41.INSTANCE);
                } catch (Throwable th) {
                    di = Di.b;
                }
                return true;
            }
            obj = this.b;
        }
        if (obj instanceof Ci) {
            g64.cancel(((Ci) obj).a);
        }
        return false;
        g.b(this, gi, di);
        return true;
    }

    @DexIgnore
    public final String s(Object obj) {
        return obj == this ? "this future" : String.valueOf(obj);
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            sb.append("CANCELLED");
        } else if (isDone()) {
            a(sb);
        } else {
            try {
                str = m();
            } catch (RuntimeException e2) {
                str = "Exception thrown from implementation: " + e2.getClass();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            } else if (isDone()) {
                a(sb);
            } else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
