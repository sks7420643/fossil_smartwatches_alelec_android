package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Js4 {
    @DexIgnore
    @Vu3("challengeId")
    public String a;
    @DexIgnore
    @Vu3("challengeName")
    public String b;
    @DexIgnore
    @Vu3("players")
    public List<Ms4> c;

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final List<Ms4> c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Js4) {
                Js4 js4 = (Js4) obj;
                if (!Wg6.a(this.a, js4.a) || !Wg6.a(this.b, js4.b) || !Wg6.a(this.c, js4.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        List<Ms4> list = this.c;
        if (list != null) {
            i = list.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BCChallengePlayer(challengeId=" + this.a + ", challengeName=" + this.b + ", players=" + this.c + ")";
    }
}
