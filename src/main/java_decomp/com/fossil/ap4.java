package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ap4 implements Factory<ApplicationEventListener> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> A;
    @DexIgnore
    public /* final */ Provider<DianaRecommendedPresetRepository> B;
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRepository> C;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> D;
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;
    @DexIgnore
    public /* final */ Provider<An4> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> f;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> g;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> h;
    @DexIgnore
    public /* final */ Provider<com.portfolio.platform.data.source.DianaPresetRepository> i;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> j;
    @DexIgnore
    public /* final */ Provider<UserRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<Cj4> m;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> o;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> p;
    @DexIgnore
    public /* final */ Provider<ProfileRepository> q;
    @DexIgnore
    public /* final */ Provider<FriendRepository> r;
    @DexIgnore
    public /* final */ Provider<Tt4> s;
    @DexIgnore
    public /* final */ Provider<FileRepository> t;
    @DexIgnore
    public /* final */ Provider<FCMRepository> u;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> v;
    @DexIgnore
    public /* final */ Provider<FlagRepository> w;
    @DexIgnore
    public /* final */ Provider<BuddyChallengeManager> x;
    @DexIgnore
    public /* final */ Provider<ThemeRepository> y;
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> z;

    @DexIgnore
    public Ap4(Uo4 uo4, Provider<PortfolioApp> provider, Provider<An4> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<com.portfolio.platform.data.source.DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<Cj4> provider12, Provider<DianaWatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<RingStyleRepository> provider15, Provider<ProfileRepository> provider16, Provider<FriendRepository> provider17, Provider<Tt4> provider18, Provider<FileRepository> provider19, Provider<FCMRepository> provider20, Provider<WorkoutSettingRepository> provider21, Provider<FlagRepository> provider22, Provider<BuddyChallengeManager> provider23, Provider<ThemeRepository> provider24, Provider<WFAssetRepository> provider25, Provider<DianaPresetRepository> provider26, Provider<DianaRecommendedPresetRepository> provider27, Provider<WFBackgroundPhotoRepository> provider28, Provider<DianaAppSettingRepository> provider29) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
        this.p = provider15;
        this.q = provider16;
        this.r = provider17;
        this.s = provider18;
        this.t = provider19;
        this.u = provider20;
        this.v = provider21;
        this.w = provider22;
        this.x = provider23;
        this.y = provider24;
        this.z = provider25;
        this.A = provider26;
        this.B = provider27;
        this.C = provider28;
        this.D = provider29;
    }

    @DexIgnore
    public static Ap4 a(Uo4 uo4, Provider<PortfolioApp> provider, Provider<An4> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<com.portfolio.platform.data.source.DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<Cj4> provider12, Provider<DianaWatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<RingStyleRepository> provider15, Provider<ProfileRepository> provider16, Provider<FriendRepository> provider17, Provider<Tt4> provider18, Provider<FileRepository> provider19, Provider<FCMRepository> provider20, Provider<WorkoutSettingRepository> provider21, Provider<FlagRepository> provider22, Provider<BuddyChallengeManager> provider23, Provider<ThemeRepository> provider24, Provider<WFAssetRepository> provider25, Provider<DianaPresetRepository> provider26, Provider<DianaRecommendedPresetRepository> provider27, Provider<WFBackgroundPhotoRepository> provider28, Provider<DianaAppSettingRepository> provider29) {
        return new Ap4(uo4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20, provider21, provider22, provider23, provider24, provider25, provider26, provider27, provider28, provider29);
    }

    @DexIgnore
    public static ApplicationEventListener c(Uo4 uo4, PortfolioApp portfolioApp, An4 an4, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, Cj4 cj4, DianaWatchFaceRepository dianaWatchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, RingStyleRepository ringStyleRepository, ProfileRepository profileRepository, FriendRepository friendRepository, Tt4 tt4, FileRepository fileRepository, FCMRepository fCMRepository, WorkoutSettingRepository workoutSettingRepository, FlagRepository flagRepository, BuddyChallengeManager buddyChallengeManager, ThemeRepository themeRepository, WFAssetRepository wFAssetRepository, DianaPresetRepository dianaPresetRepository2, DianaRecommendedPresetRepository dianaRecommendedPresetRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        ApplicationEventListener i2 = uo4.i(portfolioApp, an4, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, deviceRepository, userRepository, alarmsRepository, cj4, dianaWatchFaceRepository, watchLocalizationRepository, ringStyleRepository, profileRepository, friendRepository, tt4, fileRepository, fCMRepository, workoutSettingRepository, flagRepository, buddyChallengeManager, themeRepository, wFAssetRepository, dianaPresetRepository2, dianaRecommendedPresetRepository, wFBackgroundPhotoRepository, dianaAppSettingRepository);
        Lk7.c(i2, "Cannot return null from a non-@Nullable @Provides method");
        return i2;
    }

    @DexIgnore
    public ApplicationEventListener b() {
        return c(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get(), this.s.get(), this.t.get(), this.u.get(), this.v.get(), this.w.get(), this.x.get(), this.y.get(), this.z.get(), this.A.get(), this.B.get(), this.C.get(), this.D.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
