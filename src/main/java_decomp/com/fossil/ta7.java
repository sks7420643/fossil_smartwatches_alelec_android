package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.faces.page.MyFaceViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ta7 implements Factory<MyFaceViewModel> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> b;
    @DexIgnore
    public /* final */ Provider<FileRepository> c;
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> d;
    @DexIgnore
    public /* final */ Provider<UserRepository> e;
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataManager> f;
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataRepository> g;

    @DexIgnore
    public Ta7(Provider<PortfolioApp> provider, Provider<DianaWatchFaceRepository> provider2, Provider<FileRepository> provider3, Provider<WFAssetRepository> provider4, Provider<UserRepository> provider5, Provider<CustomizeRealDataManager> provider6, Provider<CustomizeRealDataRepository> provider7) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static Ta7 a(Provider<PortfolioApp> provider, Provider<DianaWatchFaceRepository> provider2, Provider<FileRepository> provider3, Provider<WFAssetRepository> provider4, Provider<UserRepository> provider5, Provider<CustomizeRealDataManager> provider6, Provider<CustomizeRealDataRepository> provider7) {
        return new Ta7(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    public static MyFaceViewModel c(PortfolioApp portfolioApp, DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, WFAssetRepository wFAssetRepository, UserRepository userRepository, CustomizeRealDataManager customizeRealDataManager, CustomizeRealDataRepository customizeRealDataRepository) {
        return new MyFaceViewModel(portfolioApp, dianaWatchFaceRepository, fileRepository, wFAssetRepository, userRepository, customizeRealDataManager, customizeRealDataRepository);
    }

    @DexIgnore
    public MyFaceViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
