package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fv3 implements Parcelable.Creator<Ev3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ev3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Gv3 gv3 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                gv3 = (Gv3) Ad2.e(parcel, t, Gv3.CREATOR);
            } else if (l == 3) {
                i3 = Ad2.v(parcel, t);
            } else if (l == 4) {
                i2 = Ad2.v(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                i = Ad2.v(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ev3(gv3, i3, i2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ev3[] newArray(int i) {
        return new Ev3[i];
    }
}
