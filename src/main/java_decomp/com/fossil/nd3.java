package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nd3 extends As2 implements Md3 {
    @DexIgnore
    public Nd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    @DexIgnore
    @Override // com.fossil.Md3
    public final Ac3 F2(Rg2 rg2) throws RemoteException {
        Ac3 pd3;
        Parcel d = d();
        Es2.c(d, rg2);
        Parcel e = e(2, d);
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            pd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapFragmentDelegate");
            pd3 = queryLocalInterface instanceof Ac3 ? (Ac3) queryLocalInterface : new Pd3(readStrongBinder);
        }
        e.recycle();
        return pd3;
    }

    @DexIgnore
    @Override // com.fossil.Md3
    public final void p2(Rg2 rg2, int i) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rg2);
        d.writeInt(i);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.Md3
    public final Bc3 q0(Rg2 rg2, GoogleMapOptions googleMapOptions) throws RemoteException {
        Bc3 qd3;
        Parcel d = d();
        Es2.c(d, rg2);
        Es2.d(d, googleMapOptions);
        Parcel e = e(3, d);
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            qd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
            qd3 = queryLocalInterface instanceof Bc3 ? (Bc3) queryLocalInterface : new Qd3(readStrongBinder);
        }
        e.recycle();
        return qd3;
    }

    @DexIgnore
    @Override // com.fossil.Md3
    public final Ec3 t1(Rg2 rg2, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException {
        Ec3 gd3;
        Parcel d = d();
        Es2.c(d, rg2);
        Es2.d(d, streetViewPanoramaOptions);
        Parcel e = e(7, d);
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            gd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
            gd3 = queryLocalInterface instanceof Ec3 ? (Ec3) queryLocalInterface : new Gd3(readStrongBinder);
        }
        e.recycle();
        return gd3;
    }

    @DexIgnore
    @Override // com.fossil.Md3
    public final Yb3 zze() throws RemoteException {
        Yb3 vc3;
        Parcel e = e(4, d());
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            vc3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
            vc3 = queryLocalInterface instanceof Yb3 ? (Yb3) queryLocalInterface : new Vc3(readStrongBinder);
        }
        e.recycle();
        return vc3;
    }

    @DexIgnore
    @Override // com.fossil.Md3
    public final Fs2 zzf() throws RemoteException {
        Parcel e = e(5, d());
        Fs2 e2 = Gs2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
