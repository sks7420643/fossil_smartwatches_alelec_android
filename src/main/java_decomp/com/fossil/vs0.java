package com.fossil;

import android.app.Activity;
import android.app.Application;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vs0 {
    @DexIgnore
    public static Activity a(Fragment fragment) {
        FragmentActivity activity = fragment.getActivity();
        if (activity != null) {
            return activity;
        }
        throw new IllegalStateException("Can't create ViewModelProvider for detached fragment");
    }

    @DexIgnore
    public static Application b(Activity activity) {
        Application application = activity.getApplication();
        if (application != null) {
            return application;
        }
        throw new IllegalStateException("Your activity/fragment is not yet attached to Application. You can't request ViewModel before onCreate call.");
    }

    @DexIgnore
    public static ViewModelProvider c(Fragment fragment) {
        return d(fragment, null);
    }

    @DexIgnore
    public static ViewModelProvider d(Fragment fragment, ViewModelProvider.Factory factory) {
        Application b = b(a(fragment));
        if (factory == null) {
            factory = ViewModelProvider.a.b(b);
        }
        return new ViewModelProvider(fragment.getViewModelStore(), factory);
    }

    @DexIgnore
    public static ViewModelProvider e(FragmentActivity fragmentActivity) {
        return f(fragmentActivity, null);
    }

    @DexIgnore
    public static ViewModelProvider f(FragmentActivity fragmentActivity, ViewModelProvider.Factory factory) {
        Application b = b(fragmentActivity);
        if (factory == null) {
            factory = ViewModelProvider.a.b(b);
        }
        return new ViewModelProvider(fragmentActivity.getViewModelStore(), factory);
    }
}
