package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.H60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nm extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C;
    @DexIgnore
    public short D;
    @DexIgnore
    public long E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ ArrayList<J0> G;
    @DexIgnore
    public /* final */ Ve H;
    @DexIgnore
    public /* final */ short I;
    @DexIgnore
    public float J;
    @DexIgnore
    public /* final */ H60 K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ float N;

    @DexIgnore
    public Nm(K5 k5, I60 i60, H60 h60, boolean z, boolean z2, float f, String str) {
        super(k5, i60, Yp.m0, str, false, 16);
        this.K = h60;
        this.L = z;
        this.M = z2;
        this.N = f;
        this.C = By1.a(this.i, Hm7.c(Ow.d, Ow.e));
        this.G = new ArrayList<>();
        this.H = Hd0.y.m();
        this.I = (short) 256;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Nm(K5 k5, I60 i60, H60 h60, boolean z, boolean z2, float f, String str, int i) {
        this(k5, i60, h60, z, z2, (i & 32) != 0 ? 0.001f : f, (i & 64) != 0 ? E.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public static final /* synthetic */ void J(Nm nm) {
        int i = nm.F + 1;
        nm.F = i;
        if (i < nm.D) {
            nm.M();
        } else if (nm.M) {
            nm.l(Nr.a(nm.v, null, Zq.b, null, null, 13));
        } else {
            nm.L();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.h(this, new Tl(this.w, this.x, this.H, this.z), new Ol(this), new Am(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        JSONObject put = super.C().put(Zm1.BIOMETRIC_PROFILE.b(), this.K.c());
        Wg6.b(put, "super.optionDescription(\u2026ofile.valueDescription())");
        return G80.k(G80.k(G80.k(put, Jd0.j3, Boolean.valueOf(this.L)), Jd0.k3, Boolean.valueOf(this.M)), Jd0.A0, Hy1.l(this.I, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        Jd0 jd0 = Jd0.P2;
        Object[] array = this.G.toArray(new J0[0]);
        if (array != null) {
            return G80.k(E2, jd0, Px1.a((Ox1[]) array));
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void L() {
        Lp.i(this, new Qv(this.I, this.w, 0, 4), new Di(this), new Ri(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void M() {
        short s = (short) (this.I + this.F);
        Lp.i(this, new Vv(s, this.E, this.w, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 24), new Ej(this, s), new Qj(this), new Ck(this), null, null, 48, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
