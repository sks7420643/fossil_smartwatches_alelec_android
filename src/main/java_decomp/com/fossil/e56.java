package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Gx5;
import com.mapped.AlertDialogFragment;
import com.mapped.AppWrapper;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E56 extends BaseFragment implements D56, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public C56 g;
    @DexIgnore
    public Gx5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return E56.j;
        }

        @DexIgnore
        public final E56 b() {
            return new E56();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Gx5.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ E56 a;

        @DexIgnore
        public Bi(E56 e56) {
            this.a = e56;
        }

        @DexIgnore
        @Override // com.fossil.Gx5.Ai
        public void a(AppWrapper appWrapper, boolean z) {
            Wg6.c(appWrapper, "appWrapper");
            if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == E56.L6(this.a).n()) {
                E56.L6(this.a).p(appWrapper, z);
                return;
            }
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            String title = installedApp != null ? installedApp.getTitle() : null;
            if (title != null) {
                s37.a0(childFragmentManager, title, appWrapper.getCurrentHandGroup(), E56.L6(this.a).n(), appWrapper);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ E56 b;

        @DexIgnore
        public Ci(E56 e56) {
            this.b = e56;
        }

        @DexIgnore
        public final void onClick(View view) {
            E56.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ E56 b;
        @DexIgnore
        public /* final */ /* synthetic */ F95 c;

        @DexIgnore
        public Di(E56 e56, F95 f95) {
            this.b = e56;
            this.c = f95;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.c.t;
            Wg6.b(imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            E56.K6(this.b).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ F95 b;

        @DexIgnore
        public Ei(F95 f95) {
            this.b = f95;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                F95 f95 = this.b;
                Wg6.b(f95, "binding");
                f95.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ F95 b;

        @DexIgnore
        public Fi(F95 f95) {
            this.b = f95;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText("");
        }
    }

    /*
    static {
        String simpleName = E56.class.getSimpleName();
        Wg6.b(simpleName, "NotificationHybridAppFra\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Gx5 K6(E56 e56) {
        Gx5 gx5 = e56.h;
        if (gx5 != null) {
            return gx5;
        }
        Wg6.n("mAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ C56 L6(E56 e56) {
        C56 c56 = e56.g;
        if (c56 != null) {
            return c56;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        C56 c56 = this.g;
        if (c56 != null) {
            c56.o();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.D56
    public void I(ArrayList<String> arrayList) {
        Wg6.c(arrayList, "uriAppsSelected");
        Intent intent = new Intent();
        intent.putStringArrayListExtra("APP_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(C56 c56) {
        N6(c56);
    }

    @DexIgnore
    public void N6(C56 c56) {
        Wg6.c(c56, "presenter");
        this.g = c56;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        AppWrapper appWrapper;
        AppWrapper appWrapper2;
        Wg6.c(str, "tag");
        if (str.hashCode() != -1984760733 || !str.equals("CONFIRM_REASSIGN_APP")) {
            return;
        }
        if (i2 != 2131363373) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (appWrapper2 = (AppWrapper) extras.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                C56 c56 = this.g;
                if (c56 != null) {
                    c56.p(appWrapper2, false);
                    Gx5 gx5 = this.h;
                    if (gx5 != null) {
                        gx5.notifyDataSetChanged();
                    } else {
                        Wg6.n("mAdapter");
                        throw null;
                    }
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Bundle extras2 = intent != null ? intent.getExtras() : null;
            if (extras2 != null && (appWrapper = (AppWrapper) extras2.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                C56 c562 = this.g;
                if (c562 != null) {
                    c562.p(appWrapper, true);
                    Gx5 gx52 = this.h;
                    if (gx52 != null) {
                        gx52.notifyDataSetChanged();
                    } else {
                        Wg6.n("mAdapter");
                        throw null;
                    }
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.D56
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.D56
    public void m() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        F95 f95 = (F95) Aq0.f(layoutInflater, 2131558593, viewGroup, false, A6());
        f95.s.setOnClickListener(new Ci(this));
        f95.q.addTextChangedListener(new Di(this, f95));
        f95.q.setOnFocusChangeListener(new Ei(f95));
        f95.t.setOnClickListener(new Fi(f95));
        Gx5 gx5 = new Gx5();
        gx5.o(new Bi(this));
        this.h = gx5;
        RecyclerView recyclerView = f95.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        Gx5 gx52 = this.h;
        if (gx52 != null) {
            recyclerView.setAdapter(gx52);
            String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d)) {
                f95.u.setBackgroundColor(Color.parseColor(d));
            }
            new G37(this, f95);
            Wg6.b(f95, "binding");
            return f95.n();
        }
        Wg6.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        C56 c56 = this.g;
        if (c56 != null) {
            c56.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        C56 c56 = this.g;
        if (c56 != null) {
            c56.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.D56
    public void y2(List<AppWrapper> list, int i2) {
        Wg6.c(list, "listAppWrapper");
        Gx5 gx5 = this.h;
        if (gx5 != null) {
            gx5.n(list, i2);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }
}
