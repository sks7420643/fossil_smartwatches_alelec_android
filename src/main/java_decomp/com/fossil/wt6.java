package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.tabs.TabLayout;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wt6 extends BaseFragment implements Eu6, View.OnClickListener, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ Ai x; // = new Ai(null);
    @DexIgnore
    public G37<Fa5> g;
    @DexIgnore
    public Du6 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public GradientDrawable t; // = new GradientDrawable();
    @DexIgnore
    public GradientDrawable u; // = new GradientDrawable();
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Wt6.w;
        }

        @DexIgnore
        public final Wt6 b() {
            return new Wt6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Bi(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Ci(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).o(Ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Di(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).o(Ai5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Ei(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).q(Ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Fi(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).q(Ai5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Gi(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).n(Ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Hi(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).n(Ai5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Ii(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).p(Ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wt6 b;

        @DexIgnore
        public Ji(Wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wt6.K6(this.b).p(Ai5.IMPERIAL);
        }
    }

    /*
    static {
        String simpleName = Wt6.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "PreferredUnitFragment::class.java.simpleName!!");
            w = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Du6 K6(Wt6 wt6) {
        Du6 du6 = wt6.h;
        if (du6 != null) {
            return du6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Eu6
    public void A2(Ai5 ai5) {
        G37<Fa5> g37 = this.g;
        if (g37 != null) {
            Fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.J.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = Xt6.a[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.J.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            Wg6.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            Wg6.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.J.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            Wg6.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            Wg6.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.J.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    Wg6.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    Wg6.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Eu6
    public void F3(Ai5 ai5) {
        G37<Fa5> g37 = this.g;
        if (g37 != null) {
            Fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.I.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = Xt6.c[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.I.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            Wg6.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            Wg6.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.I.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            Wg6.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            Wg6.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.I.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    Wg6.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    Wg6.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Du6 du6) {
        M6(du6);
    }

    @DexIgnore
    public void M6(Du6 du6) {
        Wg6.c(du6, "presenter");
        I14.l(du6);
        Wg6.b(du6, "checkNotNull(presenter)");
        this.h = du6;
    }

    @DexIgnore
    @Override // com.fossil.Eu6
    public void R2(Ai5 ai5) {
        G37<Fa5> g37 = this.g;
        if (g37 != null) {
            Fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.L.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = Xt6.b[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.L.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            Wg6.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            Wg6.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.L.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            Wg6.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            Wg6.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.L.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    Wg6.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    Wg6.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0) && getActivity() != null && str.hashCode() == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).y();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Eu6
    public void m1(Ai5 ai5) {
        G37<Fa5> g37 = this.g;
        if (g37 != null) {
            Fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.K.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = Xt6.d[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.K.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            Wg6.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            Wg6.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.K.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            Wg6.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            Wg6.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.K.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    Wg6.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    Wg6.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Eu6
    public void o(int i2, String str) {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        Wg6.c(view, "v");
        if (view.getId() == 2131361851) {
            e0();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        W6.d(requireContext(), 2131100360);
        W6.d(requireContext(), 2131099829);
        this.i = Color.parseColor(ThemeManager.l.a().d("primaryButton"));
        this.j = Color.parseColor(ThemeManager.l.a().d("onPrimaryButton"));
        this.k = Color.parseColor(ThemeManager.l.a().d("primaryButtonBorder"));
        this.l = Color.parseColor(ThemeManager.l.a().d("secondaryButton"));
        this.m = Color.parseColor(ThemeManager.l.a().d("onSecondaryButton"));
        this.s = Color.parseColor(ThemeManager.l.a().d("secondaryButtonBorder"));
        Fa5 fa5 = (Fa5) Aq0.f(LayoutInflater.from(getContext()), 2131558607, null, false, A6());
        fa5.q.setOnClickListener(new Bi(this));
        View childAt = fa5.J.getChildAt(0);
        if (childAt != null) {
            View childAt2 = ((LinearLayout) childAt).getChildAt(0);
            if (childAt2 != null) {
                childAt2.setOnClickListener(new Ci(this));
            }
            View childAt3 = fa5.J.getChildAt(0);
            if (childAt3 != null) {
                View childAt4 = ((LinearLayout) childAt3).getChildAt(1);
                if (childAt4 != null) {
                    childAt4.setOnClickListener(new Di(this));
                }
                View childAt5 = fa5.L.getChildAt(0);
                if (childAt5 != null) {
                    View childAt6 = ((LinearLayout) childAt5).getChildAt(0);
                    if (childAt6 != null) {
                        childAt6.setOnClickListener(new Ei(this));
                    }
                    View childAt7 = fa5.L.getChildAt(0);
                    if (childAt7 != null) {
                        View childAt8 = ((LinearLayout) childAt7).getChildAt(1);
                        if (childAt8 != null) {
                            childAt8.setOnClickListener(new Fi(this));
                        }
                        View childAt9 = fa5.I.getChildAt(0);
                        if (childAt9 != null) {
                            View childAt10 = ((LinearLayout) childAt9).getChildAt(0);
                            if (childAt10 != null) {
                                childAt10.setOnClickListener(new Gi(this));
                            }
                            View childAt11 = fa5.I.getChildAt(0);
                            if (childAt11 != null) {
                                View childAt12 = ((LinearLayout) childAt11).getChildAt(1);
                                if (childAt12 != null) {
                                    childAt12.setOnClickListener(new Hi(this));
                                }
                                View childAt13 = fa5.K.getChildAt(0);
                                if (childAt13 != null) {
                                    View childAt14 = ((LinearLayout) childAt13).getChildAt(0);
                                    if (childAt14 != null) {
                                        childAt14.setOnClickListener(new Ii(this));
                                    }
                                    View childAt15 = fa5.K.getChildAt(0);
                                    if (childAt15 != null) {
                                        View childAt16 = ((LinearLayout) childAt15).getChildAt(1);
                                        if (childAt16 != null) {
                                            childAt16.setOnClickListener(new Ji(this));
                                        }
                                        fa5.I.H(this.m, this.j);
                                        fa5.K.H(this.m, this.j);
                                        fa5.J.H(this.m, this.j);
                                        fa5.L.H(this.m, this.j);
                                        this.g = new G37<>(this, fa5);
                                        this.t.setShape(0);
                                        this.t.setColor(this.i);
                                        this.t.setStroke(4, this.k);
                                        this.u.setShape(0);
                                        this.u.setColor(this.l);
                                        this.u.setStroke(4, this.s);
                                        Wg6.b(fa5, "binding");
                                        return fa5.n();
                                    }
                                    throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                                }
                                throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                            }
                            throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                        }
                        throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                    }
                    throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
        }
        throw new Rc6("null cannot be cast to non-null type android.widget.LinearLayout");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Du6 du6 = this.h;
        if (du6 != null) {
            du6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Du6 du6 = this.h;
        if (du6 != null) {
            du6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
