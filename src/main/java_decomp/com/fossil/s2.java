package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S2 extends C2 {
    @DexIgnore
    public static /* final */ R2 CREATOR; // = new R2(null);
    @DexIgnore
    public /* final */ Bv1 e;

    @DexIgnore
    public S2(byte b, Bv1 bv1) {
        super(Lt.j, b, false, 4);
        this.e = bv1;
    }

    @DexIgnore
    public /* synthetic */ S2(Parcel parcel, Qg6 qg6) {
        super(parcel);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            Wg6.b(createByteArray, "parcel.createByteArray()!!");
            this.e = new Bv1(createByteArray);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(S2.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            S2 s2 = (S2) obj;
            if (this.b != s2.b) {
                return false;
            }
            if (this.c != s2.c) {
                return false;
            }
            return !(Wg6.a(this.e, s2.e) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return (((hashCode * 31) + this.c) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.v3, this.e.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
