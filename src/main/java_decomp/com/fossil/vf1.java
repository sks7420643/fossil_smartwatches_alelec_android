package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vf1 implements Rb1<BitmapDrawable> {
    @DexIgnore
    public /* final */ Rd1 a;
    @DexIgnore
    public /* final */ Rb1<Bitmap> b;

    @DexIgnore
    public Vf1(Rd1 rd1, Rb1<Bitmap> rb1) {
        this.a = rd1;
        this.b = rb1;
    }

    @DexIgnore
    @Override // com.fossil.Jb1
    public /* bridge */ /* synthetic */ boolean a(Object obj, File file, Ob1 ob1) {
        return c((Id1) obj, file, ob1);
    }

    @DexIgnore
    @Override // com.fossil.Rb1
    public Ib1 b(Ob1 ob1) {
        return this.b.b(ob1);
    }

    @DexIgnore
    public boolean c(Id1<BitmapDrawable> id1, File file, Ob1 ob1) {
        return this.b.a(new Yf1(id1.get().getBitmap(), this.a), file, ob1);
    }
}
