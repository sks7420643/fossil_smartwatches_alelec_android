package com.fossil;

import com.mapped.Wg6;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M71 extends RuntimeException {
    @DexIgnore
    public /* final */ Response response;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public M71(Response response2) {
        super("HTTP " + response2.f() + ": " + response2.o());
        Wg6.c(response2, "response");
        this.response = response2;
    }

    @DexIgnore
    public final Response getResponse() {
        return this.response;
    }
}
