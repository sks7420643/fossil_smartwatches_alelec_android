package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qe {
    @DexIgnore
    public /* final */ Nd0<Lp> a; // = new Nd0<>(Le.b);
    @DexIgnore
    public /* final */ Nd0<Lp> b; // = new Nd0<>(Me.b);
    @DexIgnore
    public Hashtable<Lp, El> c; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public Nw e;

    @DexIgnore
    public Qe(Nw nw) {
        this.e = nw;
    }

    @DexIgnore
    public final ArrayList<Lp> a() {
        ArrayList<Lp> arrayList;
        synchronized (this.d) {
            arrayList = new ArrayList<>();
            arrayList.addAll(this.a);
            arrayList.addAll(this.b);
        }
        return arrayList;
    }

    @DexIgnore
    public final void b(Zq zq, Hg6<? super Lp, Boolean> hg6) {
        for (Lp lp : this.b) {
            Wg6.b(lp, "phase");
            if (hg6.invoke(lp).booleanValue()) {
                lp.k(zq);
            }
        }
    }

    @DexIgnore
    public final void c(Zq zq, Yp[] ypArr) {
        Oe oe = new Oe(ypArr);
        b(zq, oe);
        for (Lp lp : this.a) {
            Wg6.b(lp, "phase");
            if (oe.invoke(lp).booleanValue()) {
                lp.k(zq);
                this.a.remove(lp);
            }
        }
    }

    @DexIgnore
    public final void d(Nw nw) {
        synchronized (this.d) {
            this.e = nw;
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final void e(Zq zq, Yp[] ypArr) {
        b(zq, new Pe(ypArr));
    }

    @DexIgnore
    public final Lp[] f() {
        Lp[] lpArr;
        synchronized (this.d) {
            Lp[] array = this.b.toArray(new Lp[0]);
            if (array != null) {
                lpArr = array;
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return lpArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0078, code lost:
        if (r15.e.a(r0) != false) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x014a, code lost:
        if (r15.e.a(r0) != false) goto L_0x007a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g() {
        /*
        // Method dump skipped, instructions count: 410
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Qe.g():void");
    }
}
