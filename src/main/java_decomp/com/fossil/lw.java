package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Lw extends Enum<Lw> {
    @DexIgnore
    public static /* final */ Kw A; // = new Kw(null);
    @DexIgnore
    public static /* final */ Lw b;
    @DexIgnore
    public static /* final */ Lw c;
    @DexIgnore
    public static /* final */ Lw d;
    @DexIgnore
    public static /* final */ Lw e;
    @DexIgnore
    public static /* final */ Lw f;
    @DexIgnore
    public static /* final */ Lw g;
    @DexIgnore
    public static /* final */ Lw h;
    @DexIgnore
    public static /* final */ Lw i;
    @DexIgnore
    public static /* final */ Lw j;
    @DexIgnore
    public static /* final */ Lw k;
    @DexIgnore
    public static /* final */ Lw l;
    @DexIgnore
    public static /* final */ Lw m;
    @DexIgnore
    public static /* final */ Lw n;
    @DexIgnore
    public static /* final */ Lw o;
    @DexIgnore
    public static /* final */ Lw p;
    @DexIgnore
    public static /* final */ Lw q;
    @DexIgnore
    public static /* final */ Lw r;
    @DexIgnore
    public static /* final */ Lw s;
    @DexIgnore
    public static /* final */ Lw t;
    @DexIgnore
    public static /* final */ Lw u;
    @DexIgnore
    public static /* final */ Lw v;
    @DexIgnore
    public static /* final */ Lw w;
    @DexIgnore
    public static /* final */ Lw x;
    @DexIgnore
    public static /* final */ Lw y;
    @DexIgnore
    public static /* final */ /* synthetic */ Lw[] z;

    /*
    static {
        Lw lw = new Lw("SUCCESS", 0, 0);
        b = lw;
        Lw lw2 = new Lw("NOT_START", 1, 1);
        c = lw2;
        Lw lw3 = new Lw("COMMAND_ERROR", 2, 2);
        d = lw3;
        Lw lw4 = new Lw("RESPONSE_ERROR", 3, 3);
        e = lw4;
        Lw lw5 = new Lw("TIMEOUT", 4, 4);
        Lw lw6 = new Lw("EOF_TIME_OUT", 5, 5);
        f = lw6;
        Lw lw7 = new Lw("CONNECTION_DROPPED", 6, 6);
        g = lw7;
        Lw lw8 = new Lw("MISS_PACKAGE", 7, 7);
        h = lw8;
        Lw lw9 = new Lw("INVALID_DATA_LENGTH", 8, 8);
        i = lw9;
        Lw lw10 = new Lw("RECEIVED_DATA_CRC_MISS_MATCH", 9, 9);
        j = lw10;
        Lw lw11 = new Lw("INVALID_RESPONSE_LENGTH", 10, 10);
        k = lw11;
        Lw lw12 = new Lw("INVALID_RESPONSE_DATA", 11, 11);
        l = lw12;
        Lw lw13 = new Lw("REQUEST_UNSUPPORTED", 12, 12);
        m = lw13;
        Lw lw14 = new Lw("UNSUPPORTED_FILE_HANDLE", 13, 13);
        n = lw14;
        Lw lw15 = new Lw("BLUETOOTH_OFF", 14, 14);
        o = lw15;
        Lw lw16 = new Lw("WRONG_AUTHENTICATION_KEY_TYPE", 15, 15);
        p = lw16;
        Lw lw17 = new Lw("REQUEST_TIMEOUT", 16, 16);
        q = lw17;
        Lw lw18 = new Lw("RESPONSE_TIMEOUT", 17, 17);
        r = lw18;
        Lw lw19 = new Lw("EMPTY_SERVICES", 18, 18);
        s = lw19;
        Lw lw20 = new Lw("INTERRUPTED", 19, 254);
        t = lw20;
        Lw lw21 = new Lw("UNKNOWN_ERROR", 20, 255);
        u = lw21;
        Lw lw22 = new Lw("HID_PROXY_NOT_CONNECTED", 21, 256);
        v = lw22;
        Lw lw23 = new Lw("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 22, 257);
        w = lw23;
        Lw lw24 = new Lw("HID_INPUT_DEVICE_DISABLED", 23, 258);
        x = lw24;
        Lw lw25 = new Lw("HID_UNKNOWN_ERROR", 24, 511);
        y = lw25;
        z = new Lw[]{lw, lw2, lw3, lw4, lw5, lw6, lw7, lw8, lw9, lw10, lw11, lw12, lw13, lw14, lw15, lw16, lw17, lw18, lw19, lw20, lw21, lw22, lw23, lw24, lw25};
    }
    */

    @DexIgnore
    public Lw(String str, int i2, int i3) {
    }

    @DexIgnore
    public static Lw valueOf(String str) {
        return (Lw) Enum.valueOf(Lw.class, str);
    }

    @DexIgnore
    public static Lw[] values() {
        return (Lw[]) z.clone();
    }
}
