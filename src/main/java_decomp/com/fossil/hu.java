package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Hu implements Mt {
    d((byte) 0),
    e((byte) 1);
    
    @DexIgnore
    public static /* final */ Gu g; // = new Gu(null);
    @DexIgnore
    public /* final */ String b; // = Ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    public Hu(byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public String getLogName() {
        return this.b;
    }
}
