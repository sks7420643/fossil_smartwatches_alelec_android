package com.fossil;

import com.mapped.Kc6;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Op1 {
    GET,
    SET;
    
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Op1 a(T8 t8) {
            int i = Xc.a[t8.ordinal()];
            if (i == 1) {
                return Op1.GET;
            }
            if (i == 2) {
                return Op1.SET;
            }
            throw new Kc6();
        }
    }
}
