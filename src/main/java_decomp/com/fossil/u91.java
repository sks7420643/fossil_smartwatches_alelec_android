package com.fossil;

import android.os.SystemClock;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U91 {
    @DexIgnore
    public static String a; // = "Volley";
    @DexIgnore
    public static boolean b; // = Log.isLoggable("Volley", 2);
    @DexIgnore
    public static /* final */ String c; // = U91.class.getName();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public static /* final */ boolean c; // = U91.b;
        @DexIgnore
        public /* final */ List<Aii> a; // = new ArrayList();
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii {
            @DexIgnore
            public /* final */ String a;
            @DexIgnore
            public /* final */ long b;
            @DexIgnore
            public /* final */ long c;

            @DexIgnore
            public Aii(String str, long j, long j2) {
                this.a = str;
                this.b = j;
                this.c = j2;
            }
        }

        @DexIgnore
        public void a(String str, long j) {
            synchronized (this) {
                if (!this.b) {
                    this.a.add(new Aii(str, j, SystemClock.elapsedRealtime()));
                } else {
                    throw new IllegalStateException("Marker added to finished log");
                }
            }
        }

        @DexIgnore
        public void b(String str) {
            synchronized (this) {
                this.b = true;
                long c2 = c();
                if (c2 > 0) {
                    long j = this.a.get(0).c;
                    U91.b("(%-4d ms) %s", Long.valueOf(c2), str);
                    long j2 = j;
                    for (Aii aii : this.a) {
                        long j3 = aii.c;
                        U91.b("(+%-4d) [%2d] %s", Long.valueOf(j3 - j2), Long.valueOf(aii.b), aii.a);
                        j2 = j3;
                    }
                }
            }
        }

        @DexIgnore
        public final long c() {
            if (this.a.size() == 0) {
                return 0;
            }
            long j = this.a.get(0).c;
            List<Aii> list = this.a;
            return list.get(list.size() - 1).c - j;
        }

        @DexIgnore
        public void finalize() throws Throwable {
            if (!this.b) {
                b("Request on the loose");
                U91.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }
    }

    @DexIgnore
    public static String a(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClassName().equals(c)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = substring.substring(substring.lastIndexOf(36) + 1) + CodelessMatcher.CURRENT_CLASS_NAME + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    @DexIgnore
    public static void b(String str, Object... objArr) {
        Log.d(a, a(str, objArr));
    }

    @DexIgnore
    public static void c(String str, Object... objArr) {
        Log.e(a, a(str, objArr));
    }

    @DexIgnore
    public static void d(Throwable th, String str, Object... objArr) {
        Log.e(a, a(str, objArr), th);
    }

    @DexIgnore
    public static void e(String str, Object... objArr) {
        if (b) {
            Log.v(a, a(str, objArr));
        }
    }

    @DexIgnore
    public static void f(String str, Object... objArr) {
        Log.wtf(a, a(str, objArr));
    }
}
