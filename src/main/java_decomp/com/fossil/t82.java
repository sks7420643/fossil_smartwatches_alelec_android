package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.Fc2;
import com.fossil.M62;
import com.fossil.R62;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T82 extends R62 implements R92 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Fc2 d;
    @DexIgnore
    public S92 e; // = null;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ Queue<I72<?, ?>> i; // = new LinkedList();
    @DexIgnore
    public volatile boolean j;
    @DexIgnore
    public long k;
    @DexIgnore
    public long l;
    @DexIgnore
    public /* final */ B92 m;
    @DexIgnore
    public /* final */ C62 n;
    @DexIgnore
    public O92 o;
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, M62.Fi> p;
    @DexIgnore
    public Set<Scope> q;
    @DexIgnore
    public /* final */ Ac2 r;
    @DexIgnore
    public /* final */ Map<M62<?>, Boolean> s;
    @DexIgnore
    public /* final */ M62.Ai<? extends Ys3, Gs3> t;
    @DexIgnore
    public /* final */ Q72 u;
    @DexIgnore
    public /* final */ ArrayList<Wa2> v;
    @DexIgnore
    public Integer w;
    @DexIgnore
    public Set<Da2> x;
    @DexIgnore
    public /* final */ Ea2 y;
    @DexIgnore
    public /* final */ Fc2.Ai z;

    @DexIgnore
    public T82(Context context, Lock lock, Looper looper, Ac2 ac2, C62 c62, M62.Ai<? extends Ys3, Gs3> ai, Map<M62<?>, Boolean> map, List<R62.Bi> list, List<R62.Ci> list2, Map<M62.Ci<?>, M62.Fi> map2, int i2, int i3, ArrayList<Wa2> arrayList, boolean z2) {
        this.k = Df2.a() ? ButtonService.CONNECT_TIMEOUT : 120000;
        this.l = 5000;
        this.q = new HashSet();
        this.u = new Q72();
        this.w = null;
        this.x = null;
        W82 w82 = new W82(this);
        this.z = w82;
        this.g = context;
        this.b = lock;
        this.c = false;
        this.d = new Fc2(looper, w82);
        this.h = looper;
        this.m = new B92(this, looper);
        this.n = c62;
        this.f = i2;
        if (i2 >= 0) {
            this.w = Integer.valueOf(i3);
        }
        this.s = map;
        this.p = map2;
        this.v = arrayList;
        this.y = new Ea2(this.p);
        for (R62.Bi bi : list) {
            this.d.f(bi);
        }
        for (R62.Ci ci : list2) {
            this.d.g(ci);
        }
        this.r = ac2;
        this.t = ai;
    }

    @DexIgnore
    public static String J(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    @DexIgnore
    public static int x(Iterable<M62.Fi> iterable, boolean z2) {
        boolean z3 = false;
        boolean z4 = false;
        for (M62.Fi fi : iterable) {
            if (fi.v()) {
                z4 = true;
            }
            if (fi.g()) {
                z3 = true;
            }
        }
        if (z4) {
            return (!z3 || !z2) ? 1 : 2;
        }
        return 3;
    }

    @DexIgnore
    public final void B() {
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public final void C() {
        this.b.lock();
        try {
            if (D()) {
                B();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final boolean D() {
        if (!this.j) {
            return false;
        }
        this.j = false;
        this.m.removeMessages(2);
        this.m.removeMessages(1);
        O92 o92 = this.o;
        if (o92 != null) {
            o92.a();
            this.o = null;
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean E() {
        this.b.lock();
        try {
            if (this.x == null) {
                this.b.unlock();
                return false;
            }
            boolean isEmpty = this.x.isEmpty();
            this.b.unlock();
            return !isEmpty;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final String F() {
        StringWriter stringWriter = new StringWriter();
        h("", null, new PrintWriter(stringWriter), null);
        return stringWriter.toString();
    }

    @DexIgnore
    public final void I(int i2) {
        Integer num = this.w;
        if (num == null) {
            this.w = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String J = J(i2);
            String J2 = J(this.w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(J).length() + 51 + String.valueOf(J2).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(J);
            sb.append(". Mode was already set to ");
            sb.append(J2);
            throw new IllegalStateException(sb.toString());
        }
        if (this.e == null) {
            boolean z2 = false;
            boolean z3 = false;
            for (M62.Fi fi : this.p.values()) {
                if (fi.v()) {
                    z2 = true;
                }
                if (fi.g()) {
                    z3 = true;
                }
            }
            int intValue = this.w.intValue();
            if (intValue != 1) {
                if (intValue == 2 && z2) {
                    if (this.c) {
                        this.e = new Db2(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, true);
                        return;
                    } else {
                        this.e = Ya2.i(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v);
                        return;
                    }
                }
            } else if (!z2) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z3) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.c || z3) {
                this.e = new C92(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this);
            } else {
                this.e = new Db2(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, false);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.R92
    public final void a(Z52 z52) {
        if (!this.n.k(this.g, z52.c())) {
            D();
        }
        if (!this.j) {
            this.d.c(z52);
            this.d.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.R92
    public final void b(Bundle bundle) {
        while (!this.i.isEmpty()) {
            j(this.i.remove());
        }
        this.d.d(bundle);
    }

    @DexIgnore
    @Override // com.fossil.R92
    public final void c(int i2, boolean z2) {
        if (i2 == 1 && !z2 && !this.j) {
            this.j = true;
            if (this.o == null && !Df2.a()) {
                try {
                    this.o = this.n.w(this.g.getApplicationContext(), new A92(this));
                } catch (SecurityException e2) {
                }
            }
            B92 b92 = this.m;
            b92.sendMessageDelayed(b92.obtainMessage(1), this.k);
            B92 b922 = this.m;
            b922.sendMessageDelayed(b922.obtainMessage(2), this.l);
        }
        this.y.c();
        this.d.e(i2);
        this.d.a();
        if (i2 == 2) {
            B();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final Z52 d() {
        boolean z2 = true;
        Rc2.o(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.b.lock();
        try {
            if (this.f >= 0) {
                if (this.w == null) {
                    z2 = false;
                }
                Rc2.o(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(x(this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            I(this.w.intValue());
            this.d.b();
            return this.e.m();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final T62<Status> e() {
        Rc2.o(n(), "GoogleApiClient is not connected yet.");
        Rc2.o(this.w.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        V72 v72 = new V72(this);
        if (this.p.containsKey(Cd2.a)) {
            y(this, v72, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            V82 v82 = new V82(this, atomicReference, v72);
            Y82 y82 = new Y82(this, v72);
            R62.Ai ai = new R62.Ai(this.g);
            ai.a(Cd2.c);
            ai.d(v82);
            ai.e(y82);
            ai.i(this.m);
            R62 g2 = ai.g();
            atomicReference.set(g2);
            g2.f();
        }
        return v72;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void f() {
        boolean z2 = false;
        this.b.lock();
        try {
            if (this.f >= 0) {
                if (this.w != null) {
                    z2 = true;
                }
                Rc2.o(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(x(this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            u(this.w.intValue());
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void g() {
        this.b.lock();
        try {
            this.y.a();
            if (this.e != null) {
                this.e.a();
            }
            this.u.c();
            for (I72<?, ?> i72 : this.i) {
                i72.n(null);
                i72.e();
            }
            this.i.clear();
            if (this.e != null) {
                D();
                this.d.a();
                this.b.unlock();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void h(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("mContext=").println(this.g);
        printWriter.append((CharSequence) str).append("mResuming=").print(this.j);
        printWriter.append(" mWorkQueue.size()=").print(this.i.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.y.a.size());
        S92 s92 = this.e;
        if (s92 != null) {
            s92.f(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T i(T t2) {
        Rc2.b(t2.w() != null, "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.w());
        String b2 = t2.v() != null ? t2.v().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        Rc2.b(containsKey, sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                this.i.add(t2);
                return t2;
            }
            T t3 = (T) this.e.k(t2);
            this.b.unlock();
            return t3;
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t2) {
        Rc2.b(t2.w() != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.w());
        String b2 = t2.v() != null ? t2.v().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        Rc2.b(containsKey, sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.j) {
                this.i.add(t2);
                while (!this.i.isEmpty()) {
                    I72<?, ?> remove = this.i.remove();
                    this.y.b(remove);
                    remove.A(Status.h);
                }
                return t2;
            } else {
                T t3 = (T) this.e.j(t2);
                this.b.unlock();
                return t3;
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final Context l() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final Looper m() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final boolean n() {
        S92 s92 = this.e;
        return s92 != null && s92.c();
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final boolean o(T72 t72) {
        S92 s92 = this.e;
        return s92 != null && s92.g(t72);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void p() {
        S92 s92 = this.e;
        if (s92 != null) {
            s92.h();
        }
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void q(R62.Ci ci) {
        this.d.g(ci);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void r(R62.Ci ci) {
        this.d.h(ci);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public final void s(Da2 da2) {
        this.b.lock();
        try {
            if (this.x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.x.remove(da2)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!E()) {
                this.e.l();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void u(int i2) {
        boolean z2 = true;
        this.b.lock();
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z2 = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            Rc2.b(z2, sb.toString());
            I(i2);
            B();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void v() {
        g();
        f();
    }

    @DexIgnore
    public final void w() {
        this.b.lock();
        try {
            if (this.j) {
                B();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void y(R62 r62, V72 v72, boolean z2) {
        Cd2.d.a(r62).d(new X82(this, v72, z2, r62));
    }
}
