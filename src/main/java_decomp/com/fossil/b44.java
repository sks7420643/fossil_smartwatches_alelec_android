package com.fossil;

import com.fossil.C44;
import com.fossil.D44;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<K, V> extends Q14<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient M14<? extends List<V>> factory;

        @DexIgnore
        public Ai(Map<K, Collection<V>> map, M14<? extends List<V>> m14) {
            super(map);
            I14.l(m14);
            this.factory = m14;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (M14) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        @Override // com.fossil.Q14, com.fossil.Q14, com.fossil.R14
        public List<V> createCollection() {
            return (List) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<K, V> extends W14<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient M14<? extends Set<V>> factory;

        @DexIgnore
        public Bi(Map<K, Collection<V>> map, M14<? extends Set<V>> m14) {
            super(map);
            I14.l(m14);
            this.factory = m14;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (M14) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        @Override // com.fossil.R14, com.fossil.W14, com.fossil.W14
        public Set<V> createCollection() {
            return (Set) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<K, V> extends AbstractCollection<Map.Entry<K, V>> {
        @DexIgnore
        public abstract Y34<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().remove(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<K, V> extends V14<K> {
        @DexIgnore
        @Weak
        public /* final */ Y34<K, V> d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends G54<Map.Entry<K, Collection<V>>, C44.Ai<K>> {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Aiii extends D44.Bi<K> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry b;

                @DexIgnore
                public Aiii(Aii aii, Map.Entry entry) {
                    this.b = entry;
                }

                @DexIgnore
                @Override // com.fossil.C44.Ai
                public int getCount() {
                    return ((Collection) this.b.getValue()).size();
                }

                @DexIgnore
                @Override // com.fossil.C44.Ai
                public K getElement() {
                    return (K) this.b.getKey();
                }
            }

            @DexIgnore
            public Aii(Di di, Iterator it) {
                super(it);
            }

            @DexIgnore
            @Override // com.fossil.G54
            public /* bridge */ /* synthetic */ Object a(Object obj) {
                return b((Map.Entry) obj);
            }

            @DexIgnore
            public C44.Ai<K> b(Map.Entry<K, Collection<V>> entry) {
                return new Aiii(this, entry);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii extends D44.Di<K> {
            @DexIgnore
            public Bii() {
            }

            @DexIgnore
            @Override // com.fossil.D44.Di
            public C44<K> a() {
                return Di.this;
            }

            @DexIgnore
            @Override // com.fossil.D44.Di
            public boolean contains(Object obj) {
                if (obj instanceof C44.Ai) {
                    C44.Ai ai = (C44.Ai) obj;
                    Collection<V> collection = Di.this.d.asMap().get(ai.getElement());
                    if (collection != null && collection.size() == ai.getCount()) {
                        return true;
                    }
                }
                return false;
            }

            @DexIgnore
            public boolean isEmpty() {
                return Di.this.d.isEmpty();
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
            public Iterator<C44.Ai<K>> iterator() {
                return Di.this.entryIterator();
            }

            @DexIgnore
            @Override // com.fossil.D44.Di
            public boolean remove(Object obj) {
                if (obj instanceof C44.Ai) {
                    C44.Ai ai = (C44.Ai) obj;
                    Collection<V> collection = Di.this.d.asMap().get(ai.getElement());
                    if (collection != null && collection.size() == ai.getCount()) {
                        collection.clear();
                        return true;
                    }
                }
                return false;
            }

            @DexIgnore
            public int size() {
                return Di.this.distinctElements();
            }
        }

        @DexIgnore
        public Di(Y34<K, V> y34) {
            this.d = y34;
        }

        @DexIgnore
        @Override // com.fossil.V14
        public void clear() {
            this.d.clear();
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.V14
        public boolean contains(Object obj) {
            return this.d.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.V14
        public int count(Object obj) {
            Collection collection = (Collection) X34.n(this.d.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        @Override // com.fossil.V14
        public Set<C44.Ai<K>> createEntrySet() {
            return new Bii();
        }

        @DexIgnore
        @Override // com.fossil.V14
        public int distinctElements() {
            return this.d.asMap().size();
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.V14
        public Set<K> elementSet() {
            return this.d.keySet();
        }

        @DexIgnore
        @Override // com.fossil.V14
        public Iterator<C44.Ai<K>> entryIterator() {
            return new Aii(this, this.d.asMap().entrySet().iterator());
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.V14, java.lang.Iterable
        public Iterator<K> iterator() {
            return X34.h(this.d.entries().iterator());
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.V14
        public int remove(Object obj, int i) {
            A24.b(i, "occurrences");
            if (i == 0) {
                return count(obj);
            }
            Collection collection = (Collection) X34.n(this.d.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            int size = collection.size();
            if (i >= size) {
                collection.clear();
            } else {
                Iterator it = collection.iterator();
                for (int i2 = 0; i2 < i; i2++) {
                    it.next();
                    it.remove();
                }
            }
            return size;
        }
    }

    @DexIgnore
    public static boolean a(Y34<?, ?> y34, Object obj) {
        if (obj == y34) {
            return true;
        }
        if (obj instanceof Y34) {
            return y34.asMap().equals(((Y34) obj).asMap());
        }
        return false;
    }

    @DexIgnore
    public static <K, V> S34<K, V> b(Map<K, Collection<V>> map, M14<? extends List<V>> m14) {
        return new Ai(map, m14);
    }

    @DexIgnore
    public static <K, V> W44<K, V> c(Map<K, Collection<V>> map, M14<? extends Set<V>> m14) {
        return new Bi(map, m14);
    }
}
