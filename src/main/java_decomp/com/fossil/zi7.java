package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.zendesk.belvedere.Belvedere;
import java.util.Arrays;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zi7 {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public String f;
    @DexIgnore
    public Dj7 g;
    @DexIgnore
    public TreeSet<Fj7> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Context a;
        @DexIgnore
        public String b; // = "belvedere-data";
        @DexIgnore
        public int c; // = FailureCode.USER_CANCELLED_BUT_USER_DID_NOT_SELECT_ANY_DEVICE;
        @DexIgnore
        public int d; // = FailureCode.USER_CANCELLED;
        @DexIgnore
        public int e; // = 1653;
        @DexIgnore
        public boolean f; // = true;
        @DexIgnore
        public String g; // = "*/*";
        @DexIgnore
        public Dj7 h; // = new Hj7();
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public TreeSet<Fj7> j; // = new TreeSet<>(Arrays.asList(Fj7.Camera, Fj7.Gallery));

        @DexIgnore
        public Ai(Context context) {
            this.a = context;
        }

        @DexIgnore
        public Belvedere i() {
            this.h.setLoggable(this.i);
            return new Belvedere(this.a, new Zi7(this));
        }

        @DexIgnore
        public Ai j(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public Ai k(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public Ai l(Dj7 dj7) {
            if (dj7 != null) {
                this.h = dj7;
                return this;
            }
            throw new IllegalArgumentException("Invalid logger provided");
        }

        @DexIgnore
        public Ai m(boolean z) {
            this.i = z;
            return this;
        }
    }

    @DexIgnore
    public Zi7(Ai ai) {
        this.a = ai.b;
        this.b = ai.c;
        this.c = ai.d;
        this.d = ai.e;
        this.e = ai.f;
        this.f = ai.g;
        this.g = ai.h;
        this.h = ai.j;
    }

    @DexIgnore
    public boolean a() {
        return this.e;
    }

    @DexIgnore
    public Dj7 b() {
        return this.g;
    }

    @DexIgnore
    public TreeSet<Fj7> c() {
        return this.h;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public String f() {
        return this.f;
    }

    @DexIgnore
    public String g() {
        return this.a;
    }

    @DexIgnore
    public int h() {
        return this.b;
    }
}
