package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import com.fossil.It0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jt0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends It0.Bi {
        @DexIgnore
        public Ai(Context context, Bi bi) {
            super(context, bi);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
            ((Bi) this.b).b(str, new It0.Ci<>(result));
        }
    }

    @DexIgnore
    public interface Bi extends It0.Di {
        @DexIgnore
        void b(String str, It0.Ci<Parcel> ci);
    }

    @DexIgnore
    public static Object a(Context context, Bi bi) {
        return new Ai(context, bi);
    }
}
