package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P61 implements N61<Integer, Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public P61(Context context) {
        Wg6.c(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ boolean a(Integer num) {
        return c(num.intValue());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ Uri b(Integer num) {
        return d(num.intValue());
    }

    @DexIgnore
    public boolean c(int i) {
        try {
            return this.a.getResources().getResourceEntryName(i) != null;
        } catch (Resources.NotFoundException e) {
            return false;
        }
    }

    @DexIgnore
    public Uri d(int i) {
        Uri parse = Uri.parse("android.resource://" + this.a.getPackageName() + '/' + i);
        Wg6.b(parse, "Uri.parse(this)");
        return parse;
    }
}
