package com.fossil;

import android.content.Context;
import com.facebook.AccessToken;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nc4 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Nc4(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final File a() {
        return new File(new Ub4(this.a).b(), "com.crashlytics.settings.json");
    }

    @DexIgnore
    public JSONObject b() {
        FileInputStream fileInputStream;
        Exception e;
        JSONObject jSONObject;
        FileInputStream fileInputStream2 = null;
        X74.f().b("Reading cached settings...");
        try {
            File a2 = a();
            if (a2.exists()) {
                fileInputStream = new FileInputStream(a2);
                try {
                    jSONObject = new JSONObject(R84.I(fileInputStream));
                } catch (Exception e2) {
                    e = e2;
                    try {
                        X74.f().e("Failed to fetch cached settings", e);
                        R84.e(fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream2 = fileInputStream;
                        R84.e(fileInputStream2, "Error while closing settings cache file.");
                        throw th;
                    }
                }
            } else {
                X74.f().b("No cached settings found.");
                jSONObject = null;
                fileInputStream = null;
            }
            R84.e(fileInputStream, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            X74.f().e("Failed to fetch cached settings", e);
            R84.e(fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th2) {
            th = th2;
            R84.e(fileInputStream2, "Error while closing settings cache file.");
            throw th;
        }
    }

    @DexIgnore
    public void c(long j, JSONObject jSONObject) {
        FileWriter fileWriter;
        Throwable th;
        Exception e;
        X74.f().b("Writing settings to cache file...");
        if (jSONObject != null) {
            try {
                jSONObject.put(AccessToken.EXPIRES_AT_KEY, j);
                fileWriter = new FileWriter(a());
                try {
                    fileWriter.write(jSONObject.toString());
                    fileWriter.flush();
                    R84.e(fileWriter, "Failed to close settings writer.");
                } catch (Exception e2) {
                    e = e2;
                    try {
                        X74.f().e("Failed to cache settings", e);
                        R84.e(fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th2) {
                        th = th2;
                        R84.e(fileWriter, "Failed to close settings writer.");
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    R84.e(fileWriter, "Failed to close settings writer.");
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                fileWriter = null;
                X74.f().e("Failed to cache settings", e);
                R84.e(fileWriter, "Failed to close settings writer.");
            } catch (Throwable th4) {
                th = th4;
                fileWriter = null;
                R84.e(fileWriter, "Failed to close settings writer.");
                throw th;
            }
        }
    }
}
