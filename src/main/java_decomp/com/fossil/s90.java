package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Nd0;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S90 implements Parcelable.Creator<T90> {
    @DexIgnore
    public /* synthetic */ S90(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public T90 createFromParcel(Parcel parcel) {
        Object[] createTypedArray = parcel.createTypedArray(Nd0.CREATOR);
        if (createTypedArray != null) {
            Wg6.b(createTypedArray, "parcel.createTypedArray(MicroAppMapping.CREATOR)!!");
            Nd0[] nd0Arr = (Nd0[]) createTypedArray;
            Parcelable readParcelable = parcel.readParcelable(Ry1.class.getClassLoader());
            if (readParcelable != null) {
                return new T90(nd0Arr, (Ry1) readParcelable);
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public T90[] newArray(int i) {
        return new T90[i];
    }
}
