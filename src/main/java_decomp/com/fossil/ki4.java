package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.firebase.iid.FirebaseInstanceId;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ki4 {
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.HOURS.toSeconds(8);
    @DexIgnore
    public /* final */ FirebaseInstanceId a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Rf4 c;
    @DexIgnore
    public /* final */ Ef4 d;
    @DexIgnore
    public /* final */ Map<String, ArrayDeque<Ot3<Void>>> e; // = new Zi0();
    @DexIgnore
    public /* final */ ScheduledExecutorService f;
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public /* final */ Ii4 h;

    @DexIgnore
    public Ki4(FirebaseInstanceId firebaseInstanceId, Rf4 rf4, Ii4 ii4, Ef4 ef4, Context context, ScheduledExecutorService scheduledExecutorService) {
        this.a = firebaseInstanceId;
        this.c = rf4;
        this.h = ii4;
        this.d = ef4;
        this.b = context;
        this.f = scheduledExecutorService;
    }

    @DexIgnore
    public static <T> T a(Nt3<T> nt3) throws IOException {
        try {
            return (T) Qt3.b(nt3, 30, TimeUnit.SECONDS);
        } catch (ExecutionException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof IOException) {
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e2);
            }
        } catch (InterruptedException | TimeoutException e3) {
            throw new IOException("SERVICE_NOT_AVAILABLE", e3);
        }
    }

    @DexIgnore
    public static Nt3<Ki4> d(J64 j64, FirebaseInstanceId firebaseInstanceId, Rf4 rf4, Ti4 ti4, Je4 je4, Ug4 ug4, Context context, ScheduledExecutorService scheduledExecutorService) {
        return e(firebaseInstanceId, rf4, new Ef4(j64, rf4, ti4, je4, ug4), context, scheduledExecutorService);
    }

    @DexIgnore
    public static Nt3<Ki4> e(FirebaseInstanceId firebaseInstanceId, Rf4 rf4, Ef4 ef4, Context context, ScheduledExecutorService scheduledExecutorService) {
        return Qt3.c(scheduledExecutorService, new Ji4(context, scheduledExecutorService, firebaseInstanceId, rf4, ef4));
    }

    @DexIgnore
    public static boolean g() {
        return Log.isLoggable("FirebaseMessaging", 3) || (Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3));
    }

    @DexIgnore
    public static final /* synthetic */ Ki4 i(Context context, ScheduledExecutorService scheduledExecutorService, FirebaseInstanceId firebaseInstanceId, Rf4 rf4, Ef4 ef4) throws Exception {
        return new Ki4(firebaseInstanceId, rf4, Ii4.a(context, scheduledExecutorService), ef4, context, scheduledExecutorService);
    }

    @DexIgnore
    public final void b(String str) throws IOException {
        If4 if4 = (If4) a(this.a.n());
        a(this.d.j(if4.getId(), if4.a(), str));
    }

    @DexIgnore
    public final void c(String str) throws IOException {
        If4 if4 = (If4) a(this.a.n());
        a(this.d.k(if4.getId(), if4.a(), str));
    }

    @DexIgnore
    public boolean f() {
        return this.h.b() != null;
    }

    @DexIgnore
    public boolean h() {
        boolean z;
        synchronized (this) {
            z = this.g;
        }
        return z;
    }

    @DexIgnore
    public final void j(Hi4 hi4) {
        synchronized (this.e) {
            String e2 = hi4.e();
            if (this.e.containsKey(e2)) {
                ArrayDeque<Ot3<Void>> arrayDeque = this.e.get(e2);
                Ot3<Void> poll = arrayDeque.poll();
                if (poll != null) {
                    poll.c(null);
                }
                if (arrayDeque.isEmpty()) {
                    this.e.remove(e2);
                }
            }
        }
    }

    @DexIgnore
    public boolean k(Hi4 hi4) throws IOException {
        try {
            String b2 = hi4.b();
            char c2 = '\uffff';
            int hashCode = b2.hashCode();
            if (hashCode != 83) {
                if (hashCode == 85 && b2.equals("U")) {
                    c2 = 1;
                }
            } else if (b2.equals(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX)) {
                c2 = 0;
            }
            if (c2 == 0) {
                b(hi4.c());
                if (!g()) {
                    return true;
                }
                String c3 = hi4.c();
                StringBuilder sb = new StringBuilder(String.valueOf(c3).length() + 31);
                sb.append("Subscribe to topic: ");
                sb.append(c3);
                sb.append(" succeeded.");
                Log.d("FirebaseMessaging", sb.toString());
                return true;
            } else if (c2 == 1) {
                c(hi4.c());
                if (!g()) {
                    return true;
                }
                String c4 = hi4.c();
                StringBuilder sb2 = new StringBuilder(String.valueOf(c4).length() + 35);
                sb2.append("Unsubscribe from topic: ");
                sb2.append(c4);
                sb2.append(" succeeded.");
                Log.d("FirebaseMessaging", sb2.toString());
                return true;
            } else if (!g()) {
                return true;
            } else {
                String valueOf = String.valueOf(hi4);
                StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf).length() + 24);
                sb3.append("Unknown topic operation");
                sb3.append(valueOf);
                sb3.append(CodelessMatcher.CURRENT_CLASS_NAME);
                Log.d("FirebaseMessaging", sb3.toString());
                return true;
            }
        } catch (IOException e2) {
            if ("SERVICE_NOT_AVAILABLE".equals(e2.getMessage()) || "INTERNAL_SERVER_ERROR".equals(e2.getMessage())) {
                String message = e2.getMessage();
                StringBuilder sb4 = new StringBuilder(String.valueOf(message).length() + 53);
                sb4.append("Topic operation failed: ");
                sb4.append(message);
                sb4.append(". Will retry Topic operation.");
                Log.e("FirebaseMessaging", sb4.toString());
                return false;
            } else if (e2.getMessage() == null) {
                Log.e("FirebaseMessaging", "Topic operation failed without exception message. Will retry Topic operation.");
                return false;
            } else {
                throw e2;
            }
        }
    }

    @DexIgnore
    public void l(Runnable runnable, long j) {
        this.f.schedule(runnable, j, TimeUnit.SECONDS);
    }

    @DexIgnore
    public void m(boolean z) {
        synchronized (this) {
            this.g = z;
        }
    }

    @DexIgnore
    public final void n() {
        if (!h()) {
            q(0);
        }
    }

    @DexIgnore
    public void o() {
        if (f()) {
            n();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        if (k(r0) != false) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        return false;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean p() throws java.io.IOException {
        /*
            r2 = this;
        L_0x0000:
            monitor-enter(r2)
            com.fossil.Ii4 r0 = r2.h     // Catch:{ all -> 0x002b }
            com.fossil.Hi4 r0 = r0.b()     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0019
            boolean r0 = g()     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = "FirebaseMessaging"
            java.lang.String r1 = "topic sync succeeded"
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x002b }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x002b }
            r0 = 1
        L_0x0018:
            return r0
        L_0x0019:
            monitor-exit(r2)     // Catch:{ all -> 0x002b }
            boolean r1 = r2.k(r0)
            if (r1 != 0) goto L_0x0022
            r0 = 0
            goto L_0x0018
        L_0x0022:
            com.fossil.Ii4 r1 = r2.h
            r1.d(r0)
            r2.j(r0)
            goto L_0x0000
        L_0x002b:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ki4.p():boolean");
    }

    @DexIgnore
    public void q(long j) {
        l(new Li4(this, this.b, this.c, Math.min(Math.max(30L, j << 1), i)), j);
        m(true);
    }
}
