package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Au {
    c(new byte[]{5}),
    d(new byte[]{40}),
    e(new byte[]{41}),
    f(new byte[]{83});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Au(byte[] bArr) {
        this.b = bArr;
    }
}
