package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F73 implements G73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.collection.firebase_global_collection_flag_enabled", true);

    @DexIgnore
    @Override // com.fossil.G73
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
