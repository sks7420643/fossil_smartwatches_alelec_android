package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.io.Reader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class L54 {
    @DexIgnore
    public abstract Reader a() throws IOException;

    @DexIgnore
    @CanIgnoreReturnValue
    public <T> T b(R54<T> r54) throws IOException {
        I14.l(r54);
        O54 a2 = O54.a();
        try {
            Reader a3 = a();
            a2.b(a3);
            T t = (T) M54.b(a3, r54);
            a2.close();
            return t;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }
}
