package com.fossil;

import com.mapped.Oh;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gw0 {
    @DexIgnore
    public static final Dv7 a(Oh oh) {
        Wg6.c(oh, "$this$queryDispatcher");
        Map<String, Object> backingFieldMap = oh.getBackingFieldMap();
        Wg6.b(backingFieldMap, "backingFieldMap");
        Object obj = backingFieldMap.get("QueryDispatcher");
        if (obj == null) {
            Executor queryExecutor = oh.getQueryExecutor();
            Wg6.b(queryExecutor, "queryExecutor");
            obj = Pw7.a(queryExecutor);
            backingFieldMap.put("QueryDispatcher", obj);
        }
        if (obj != null) {
            return (Dv7) obj;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
    }

    @DexIgnore
    public static final Dv7 b(Oh oh) {
        Wg6.c(oh, "$this$transactionDispatcher");
        Map<String, Object> backingFieldMap = oh.getBackingFieldMap();
        Wg6.b(backingFieldMap, "backingFieldMap");
        Object obj = backingFieldMap.get("TransactionDispatcher");
        if (obj == null) {
            Executor transactionExecutor = oh.getTransactionExecutor();
            Wg6.b(transactionExecutor, "transactionExecutor");
            obj = Pw7.a(transactionExecutor);
            backingFieldMap.put("TransactionDispatcher", obj);
        }
        if (obj != null) {
            return (Dv7) obj;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
    }
}
