package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.fossil.Pd7;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qd7 {
    @DexIgnore
    public static /* final */ AtomicInteger m; // = new AtomicInteger();
    @DexIgnore
    public /* final */ Picasso a;
    @DexIgnore
    public /* final */ Pd7.Bi b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Drawable j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public Object l;

    @DexIgnore
    public Qd7(Picasso picasso, Uri uri, int i2) {
        if (!picasso.o) {
            this.a = picasso;
            this.b = new Pd7.Bi(uri, i2, picasso.l);
            return;
        }
        throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
    }

    @DexIgnore
    public final Pd7 a(long j2) {
        int andIncrement = m.getAndIncrement();
        Pd7 a2 = this.b.a();
        a2.a = andIncrement;
        a2.b = j2;
        if (this.a.n) {
            Xd7.v("Main", "created", a2.g(), a2.toString());
        }
        this.a.q(a2);
        return a2;
    }

    @DexIgnore
    public Qd7 b(int i2) {
        if (i2 == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        } else if (this.k == null) {
            this.g = i2;
            return this;
        } else {
            throw new IllegalStateException("Error image already set.");
        }
    }

    @DexIgnore
    public final Drawable c() {
        return this.f != 0 ? this.a.e.getResources().getDrawable(this.f) : this.j;
    }

    @DexIgnore
    public void d(ImageView imageView) {
        e(imageView, null);
    }

    @DexIgnore
    public void e(ImageView imageView, Zc7 zc7) {
        Bitmap n;
        long nanoTime = System.nanoTime();
        Xd7.c();
        if (imageView == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.b.b()) {
            this.a.c(imageView);
            if (this.e) {
                Nd7.d(imageView, c());
            }
        } else {
            if (this.d) {
                if (!this.b.c()) {
                    int width = imageView.getWidth();
                    int height = imageView.getHeight();
                    if (width == 0 || height == 0) {
                        if (this.e) {
                            Nd7.d(imageView, c());
                        }
                        this.a.f(imageView, new Cd7(this, imageView, zc7));
                        return;
                    }
                    this.b.d(width, height);
                } else {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
            }
            Pd7 a2 = a(nanoTime);
            String h2 = Xd7.h(a2);
            if (!Jd7.shouldReadFromMemoryCache(this.h) || (n = this.a.n(h2)) == null) {
                if (this.e) {
                    Nd7.d(imageView, c());
                }
                this.a.h(new Fd7(this.a, imageView, a2, this.h, this.i, this.g, this.k, h2, this.l, zc7, this.c));
                return;
            }
            this.a.c(imageView);
            Picasso picasso = this.a;
            Nd7.c(imageView, picasso.e, n, Picasso.LoadedFrom.MEMORY, this.c, picasso.m);
            if (this.a.n) {
                String g2 = a2.g();
                Xd7.v("Main", "completed", g2, "from " + Picasso.LoadedFrom.MEMORY);
            }
            if (zc7 != null) {
                zc7.onSuccess();
            }
        }
    }

    @DexIgnore
    public void f(Target target) {
        Bitmap n;
        Drawable drawable = null;
        long nanoTime = System.nanoTime();
        Xd7.c();
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (this.d) {
            throw new IllegalStateException("Fit cannot be used with a Target.");
        } else if (!this.b.b()) {
            this.a.d(target);
            if (this.e) {
                drawable = c();
            }
            target.onPrepareLoad(drawable);
        } else {
            Pd7 a2 = a(nanoTime);
            String h2 = Xd7.h(a2);
            if (!Jd7.shouldReadFromMemoryCache(this.h) || (n = this.a.n(h2)) == null) {
                if (this.e) {
                    drawable = c();
                }
                target.onPrepareLoad(drawable);
                this.a.h(new Vd7(this.a, target, a2, this.h, this.i, this.k, h2, this.l, this.g));
                return;
            }
            this.a.d(target);
            target.onBitmapLoaded(n, Picasso.LoadedFrom.MEMORY);
        }
    }

    @DexIgnore
    public Qd7 g() {
        this.c = true;
        return this;
    }

    @DexIgnore
    public Qd7 h(int i2) {
        if (!this.e) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        } else if (i2 == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.j == null) {
            this.f = i2;
            return this;
        } else {
            throw new IllegalStateException("Placeholder image already set.");
        }
    }

    @DexIgnore
    public Qd7 i(int i2, int i3) {
        this.b.d(i2, i3);
        return this;
    }

    @DexIgnore
    public Qd7 j(Transformation transformation) {
        this.b.e(transformation);
        return this;
    }

    @DexIgnore
    public Qd7 k() {
        this.d = false;
        return this;
    }
}
