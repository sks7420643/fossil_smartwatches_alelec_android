package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qv3 implements Parcelable.Creator<Pv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Pv3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        String str = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                str2 = Ad2.f(parcel, t);
            } else if (l == 3) {
                str = Ad2.f(parcel, t);
            } else if (l == 4) {
                i = Ad2.v(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                z = Ad2.m(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Pv3(str2, str, i, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Pv3[] newArray(int i) {
        return new Pv3[i];
    }
}
