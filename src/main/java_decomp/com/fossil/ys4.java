package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ys4 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    int b(String str);

    @DexIgnore
    LiveData<List<Xs4>> c();

    @DexIgnore
    List<Xs4> d();

    @DexIgnore
    int e();

    @DexIgnore
    List<Xs4> f();

    @DexIgnore
    Object g();  // void declaration

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    List<Xs4> i();

    @DexIgnore
    Long[] insert(List<Xs4> list);

    @DexIgnore
    Object j();  // void declaration

    @DexIgnore
    List<Xs4> k();

    @DexIgnore
    List<Xs4> l();

    @DexIgnore
    int m(String[] strArr);

    @DexIgnore
    List<Xs4> n();

    @DexIgnore
    long o(Xs4 xs4);

    @DexIgnore
    Xs4 p(String str);
}
