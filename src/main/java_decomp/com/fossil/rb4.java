package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Rb4 implements FilenameFilter {
    @DexIgnore
    public static /* final */ Rb4 a; // = new Rb4();

    @DexIgnore
    public static FilenameFilter a() {
        return a;
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return str.startsWith(Constants.EVENT);
    }
}
