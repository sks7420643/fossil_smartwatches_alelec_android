package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Iw2 implements SharedPreferences.OnSharedPreferenceChangeListener {
    @DexIgnore
    public /* final */ Jw2 a;

    @DexIgnore
    public Iw2(Jw2 jw2) {
        this.a = jw2;
    }

    @DexIgnore
    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.a.c(sharedPreferences, str);
    }
}
