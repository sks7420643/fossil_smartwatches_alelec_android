package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fq5 implements MembersInjector<SmsMmsReceiver> {
    @DexIgnore
    public static void a(SmsMmsReceiver smsMmsReceiver, DianaNotificationComponent dianaNotificationComponent) {
        smsMmsReceiver.a = dianaNotificationComponent;
    }

    @DexIgnore
    public static void b(SmsMmsReceiver smsMmsReceiver, An4 an4) {
        smsMmsReceiver.b = an4;
    }
}
