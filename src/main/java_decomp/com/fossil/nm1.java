package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.Gender;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm1 extends ym1 {
    @DexIgnore
    public static /* final */ c CREATOR; // = new c(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ short e;
    @DexIgnore
    public /* final */ short f;
    @DexIgnore
    public /* final */ b g;

    @DexIgnore
    public enum a {
        UNSPECIFIED((byte) 0),
        MALE((byte) 1),
        FEMALE((byte) 2);
        
        @DexIgnore
        public static /* final */ C0172a d; // = new C0172a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nm1$a$a")
        /* renamed from: com.fossil.nm1$a$a  reason: collision with other inner class name */
        public static final class C0172a {
            @DexIgnore
            public /* synthetic */ C0172a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }

        @DexIgnore
        public final Gender b() {
            int i = y8.f4256a[ordinal()];
            if (i == 1) {
                return Gender.UNSPECIFIED;
            }
            if (i == 2) {
                return Gender.MALE;
            }
            if (i == 3) {
                return Gender.FEMALE;
            }
            throw new al7();
        }
    }

    @DexIgnore
    public enum b {
        UNSPECIFIED((byte) 0),
        LEFT_WRIST((byte) 1),
        RIGHT_WRIST((byte) 2),
        UNSPECIFIED_WRIST((byte) 3);
        
        @DexIgnore
        public static /* final */ a d; // = new a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(kq7 kq7) {
            }

            @DexIgnore
            public final b a(byte b) throws IllegalArgumentException {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public b(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Parcelable.Creator<nm1> {
        @DexIgnore
        public /* synthetic */ c(kq7 kq7) {
        }

        @DexIgnore
        public final nm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 7) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new nm1(order.get(0), a.d.a(order.get(1)), order.getShort(2), order.getShort(4), b.d.a(order.get(6)));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 7"));
        }

        @DexIgnore
        public nm1 b(Parcel parcel) {
            return new nm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public nm1 createFromParcel(Parcel parcel) {
            return new nm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public nm1[] newArray(int i) {
            return new nm1[i];
        }
    }

    @DexIgnore
    public nm1(byte b2, a aVar, short s, short s2, b bVar) throws IllegalArgumentException {
        super(zm1.BIOMETRIC_PROFILE);
        this.c = (byte) b2;
        this.d = aVar;
        this.e = (short) s;
        this.f = (short) s2;
        this.g = bVar;
        d();
    }

    @DexIgnore
    public /* synthetic */ nm1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readByte();
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            this.d = a.valueOf(readString);
            this.e = (short) ((short) parcel.readInt());
            this.f = (short) ((short) parcel.readInt());
            String readString2 = parcel.readString();
            if (readString2 != null) {
                pq7.b(readString2, "parcel.readString()!!");
                this.g = b.valueOf(readString2);
                d();
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(7).order(ByteOrder.LITTLE_ENDIAN).put(this.c).put(this.d.a()).putShort(this.e).putShort(this.f).put(this.g.a()).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("age", Byte.valueOf(this.c));
            jSONObject.put("gender", ey1.a(this.d));
            jSONObject.put("height_in_centimeter", Short.valueOf(this.e));
            jSONObject.put("weight_in_kilogram", Short.valueOf(this.f));
            jSONObject.put("wearing_position", ey1.a(this.g));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        byte b2 = this.c;
        if (8 <= b2 && 110 >= b2) {
            short s = this.e;
            if (100 <= s && 250 >= s) {
                short s2 = this.f;
                if (35 > s2 || 250 < s2) {
                    z = false;
                }
                if (!z) {
                    StringBuilder e2 = e.e("weightInKilogram (");
                    e2.append((int) this.f);
                    e2.append(") is out of range ");
                    e2.append("[35, 250]");
                    e2.append(" (in kilogram).");
                    throw new IllegalArgumentException(e2.toString());
                }
                return;
            }
            StringBuilder e3 = e.e("heightInCentimeter (");
            e3.append((int) this.e);
            e3.append(") is out of range ");
            e3.append("[100, 250]");
            e3.append(" (in centimeter.");
            throw new IllegalArgumentException(e3.toString());
        }
        throw new IllegalArgumentException(e.c(e.e("age ("), this.c, ") is out of range [8, ", "110]."));
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(nm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            nm1 nm1 = (nm1) obj;
            if (this.c != nm1.c) {
                return false;
            }
            if (this.d != nm1.d) {
                return false;
            }
            if (this.e != nm1.e) {
                return false;
            }
            if (this.f != nm1.f) {
                return false;
            }
            return this.g == nm1.g;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BiometricProfile");
    }

    @DexIgnore
    public final byte getAge() {
        return this.c;
    }

    @DexIgnore
    public final a getGender() {
        return this.d;
    }

    @DexIgnore
    public final short getHeightInCentimeter() {
        return this.e;
    }

    @DexIgnore
    public final b getWearingPosition() {
        return this.g;
    }

    @DexIgnore
    public final short getWeightInKilogram() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        byte b2 = this.c;
        int hashCode = this.d.hashCode();
        short s = this.e;
        return (((((((b2 * 31) + hashCode) * 31) + s) * 31) + this.f) * 31) + this.g.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.e));
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.f));
        }
        if (parcel != null) {
            parcel.writeString(this.g.name());
        }
    }
}
