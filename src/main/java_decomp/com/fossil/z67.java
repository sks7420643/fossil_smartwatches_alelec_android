package com.fossil;

import androidx.lifecycle.MutableLiveData;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z67 extends Ts0 {
    @DexIgnore
    public /* final */ MutableLiveData<Date> a; // = new MutableLiveData<>();

    @DexIgnore
    public final MutableLiveData<Date> a() {
        return this.a;
    }
}
