package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vo6 extends Qo6 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ Ro6 e;

    /*
    static {
        String simpleName = Vo6.class.getSimpleName();
        Wg6.b(simpleName, "ReplaceBatteryPresenter::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public Vo6(Ro6 ro6) {
        Wg6.c(ro6, "mView");
        this.e = ro6;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(f, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(f, "presenter stop");
    }

    @DexIgnore
    public void n() {
        this.e.M5(this);
    }
}
