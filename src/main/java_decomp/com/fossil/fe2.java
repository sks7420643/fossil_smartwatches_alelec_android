package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fe2 implements Parcelable.Creator<Dc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Dc2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        B62[] b62Arr = null;
        B62[] b62Arr2 = null;
        Account account = null;
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    i = Ad2.v(parcel, t);
                    break;
                case 2:
                    i2 = Ad2.v(parcel, t);
                    break;
                case 3:
                    i3 = Ad2.v(parcel, t);
                    break;
                case 4:
                    str = Ad2.f(parcel, t);
                    break;
                case 5:
                    iBinder = Ad2.u(parcel, t);
                    break;
                case 6:
                    scopeArr = (Scope[]) Ad2.i(parcel, t, Scope.CREATOR);
                    break;
                case 7:
                    bundle = Ad2.a(parcel, t);
                    break;
                case 8:
                    account = (Account) Ad2.e(parcel, t, Account.CREATOR);
                    break;
                case 9:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 10:
                    b62Arr2 = (B62[]) Ad2.i(parcel, t, B62.CREATOR);
                    break;
                case 11:
                    b62Arr = (B62[]) Ad2.i(parcel, t, B62.CREATOR);
                    break;
                case 12:
                    z = Ad2.m(parcel, t);
                    break;
                case 13:
                    i4 = Ad2.v(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Dc2(i, i2, i3, str, iBinder, scopeArr, bundle, account, b62Arr2, b62Arr, z, i4);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Dc2[] newArray(int i) {
        return new Dc2[i];
    }
}
