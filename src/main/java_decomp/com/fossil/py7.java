package com.fossil;

import com.fossil.Lz7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Py7<E> extends Xy7 implements Wy7<E> {
    @DexIgnore
    public /* final */ Throwable e;

    @DexIgnore
    public Py7(Throwable th) {
        this.e = th;
    }

    @DexIgnore
    @Override // com.fossil.Wy7
    public /* bridge */ /* synthetic */ Object a() {
        w();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Wy7
    public void d(E e2) {
    }

    @DexIgnore
    @Override // com.fossil.Wy7
    public Vz7 e(E e2, Lz7.Ci ci) {
        Vz7 vz7 = Mu7.a;
        if (ci == null) {
            return vz7;
        }
        ci.d();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return "Closed@" + Ov7.b(this) + '[' + this.e + ']';
    }

    @DexIgnore
    public Py7<E> w() {
        return this;
    }

    @DexIgnore
    public final Throwable x() {
        Throwable th = this.e;
        return th != null ? th : new Qy7("Channel was closed");
    }
}
