package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum L43 {
    zza(S43.zzd, 1),
    zzb(S43.zzc, 5),
    zzc(S43.zzb, 0),
    zzd(S43.zzb, 0),
    zze(S43.zza, 0),
    zzf(S43.zzb, 1),
    zzg(S43.zza, 5),
    zzh(S43.zze, 0),
    zzi(S43.zzf, 2) {
    },
    zzj(S43.zzi, 3) {
    },
    zzk(S43.zzi, 2) {
    },
    zzl(S43.zzg, 2) {
    },
    zzm(S43.zza, 0),
    zzn(S43.zzh, 0),
    zzo(S43.zza, 5),
    zzp(S43.zzb, 1),
    zzq(S43.zza, 0),
    zzr(S43.zzb, 0);
    
    @DexIgnore
    public /* final */ S43 zzs;
    @DexIgnore
    public /* final */ int zzt;

    @DexIgnore
    public L43(S43 s43, int i) {
        this.zzs = s43;
        this.zzt = i;
    }

    @DexIgnore
    public final S43 zza() {
        return this.zzs;
    }

    @DexIgnore
    public final int zzb() {
        return this.zzt;
    }
}
