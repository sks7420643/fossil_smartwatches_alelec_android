package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vs extends Ss {
    @DexIgnore
    public /* final */ N6 A;
    @DexIgnore
    public /* final */ boolean B;

    @DexIgnore
    public Vs(N6 n6, boolean z, K5 k5) {
        super(Hs.f, k5);
        this.A = n6;
        this.B = z;
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new I6(this.A, this.B, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.R0, this.A.b), Jd0.F, Boolean.valueOf(this.B));
    }
}
