package com.fossil;

import com.fossil.iq4;
import com.fossil.xu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lq6 extends gq6 {
    @DexIgnore
    public /* final */ String e; // = "ProfileOptInPresenter";
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ hq6 g;
    @DexIgnore
    public /* final */ xu5 h;
    @DexIgnore
    public /* final */ UserRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lq6 f2234a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public a(lq6 lq6, boolean z) {
            this.f2234a = lq6;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f2234a.t().F4(!this.b);
            this.f2234a.t().J5();
            hq6 t = this.f2234a.t();
            int a2 = bVar.a();
            String b2 = bVar.b();
            if (b2 != null) {
                t.C(a2, b2);
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f2234a.t().J5();
            ck5.f.g().s(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lq6 f2235a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public b(lq6 lq6, boolean z) {
            this.f2235a = lq6;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f2235a.t().Y4(!this.b);
            this.f2235a.t().J5();
            hq6 t = this.f2235a.t();
            int a2 = bVar.a();
            String b2 = bVar.b();
            if (b2 != null) {
                t.C(a2, b2);
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f2235a.t().J5();
            ck5.f.g().s(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository s = this.this$0.this$0.s();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = s.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(lq6 lq6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lq6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            lq6 lq6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                lq6 lq62 = this.this$0;
                dv7 i2 = lq62.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = lq62;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
                lq6 = lq62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
                lq6 = (lq6) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lq6.f = (MFUser) g;
            MFUser mFUser = this.this$0.f;
            if (mFUser != null) {
                this.this$0.t().F4(mFUser.getDiagnosticEnabled());
                this.this$0.t().Y4(mFUser.getEmailOptIn());
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public lq6(hq6 hq6, xu5 xu5, UserRepository userRepository) {
        pq7.c(hq6, "mView");
        pq7.c(xu5, "mUpdateUser");
        pq7.c(userRepository, "mUserRepository");
        this.g = hq6;
        this.h = xu5;
        this.i = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter starts: Get user information");
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void n(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setAnonymouslySendUsageData() called with: checked = " + z);
            mFUser.setDiagnosticEnabled(z);
            this.g.P0();
            if (this.h.e(new xu5.a(mFUser), new a(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void o(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setSubcribeEmailData() called with: checked = " + z);
            mFUser.setEmailOptIn(z);
            this.g.P0();
            if (this.h.e(new xu5.a(mFUser), new b(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }

    @DexIgnore
    public final UserRepository s() {
        return this.i;
    }

    @DexIgnore
    public final hq6 t() {
        return this.g;
    }

    @DexIgnore
    public void u() {
        this.g.M5(this);
    }
}
