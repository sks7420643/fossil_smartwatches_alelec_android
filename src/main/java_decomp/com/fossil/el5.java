package com.fossil;

import android.view.View;
import com.mapped.Wg6;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class El5 implements View.OnClickListener {
    @DexIgnore
    public AtomicBoolean b; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ long c; // = 1000;
    @DexIgnore
    public /* final */ View.OnClickListener d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ El5 b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public Ai(El5 el5, View view) {
            this.b = el5;
            this.c = view;
        }

        @DexIgnore
        public final void run() {
            this.b.b.set(true);
        }
    }

    @DexIgnore
    public El5(View.OnClickListener onClickListener) {
        Wg6.c(onClickListener, "clickListener");
        this.d = onClickListener;
    }

    @DexIgnore
    public void onClick(View view) {
        if (this.b.getAndSet(false) && view != null) {
            view.postDelayed(new Ai(this, view), this.c);
            this.d.onClick(view);
        }
    }
}
