package com.fossil;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.Af1;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jf1<Data> implements Af1<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", "content")));
    @DexIgnore
    public /* final */ Ci<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Bf1<Uri, AssetFileDescriptor>, Ci<AssetFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public Ai(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.Jf1.Ci
        public Wb1<AssetFileDescriptor> a(Uri uri) {
            return new Tb1(this.a, uri);
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, AssetFileDescriptor> b(Ef1 ef1) {
            return new Jf1(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Bf1<Uri, ParcelFileDescriptor>, Ci<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public Bi(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.Jf1.Ci
        public Wb1<ParcelFileDescriptor> a(Uri uri) {
            return new Bc1(this.a, uri);
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, ParcelFileDescriptor> b(Ef1 ef1) {
            return new Jf1(this);
        }
    }

    @DexIgnore
    public interface Ci<Data> {
        @DexIgnore
        Wb1<Data> a(Uri uri);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements Bf1<Uri, InputStream>, Ci<InputStream> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public Di(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.Jf1.Ci
        public Wb1<InputStream> a(Uri uri) {
            return new Hc1(this.a, uri);
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, InputStream> b(Ef1 ef1) {
            return new Jf1(this);
        }
    }

    @DexIgnore
    public Jf1(Ci<Data> ci) {
        this.a = ci;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(Uri uri, int i, int i2, Ob1 ob1) {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<Data> c(Uri uri, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(uri), this.a.a(uri));
    }

    @DexIgnore
    public boolean d(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
