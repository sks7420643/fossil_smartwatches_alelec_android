package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P08 extends N08 {
    @DexIgnore
    public /* final */ Runnable d;

    @DexIgnore
    public P08(Runnable runnable, long j, O08 o08) {
        super(j, o08);
        this.d = runnable;
    }

    @DexIgnore
    public void run() {
        try {
            this.d.run();
        } finally {
            this.c.h();
        }
    }

    @DexIgnore
    public String toString() {
        return "Task[" + Ov7.a(this.d) + '@' + Ov7.b(this.d) + ", " + this.b + ", " + this.c + ']';
    }
}
