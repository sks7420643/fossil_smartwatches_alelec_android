package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<Background> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<Background> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore
    public final String a(Background background) {
        if (background == null) {
            return "";
        }
        String u = new Gson().u(background, new Ai().getType());
        Wg6.b(u, "Gson().toJson(background, type)");
        return u;
    }

    @DexIgnore
    public final Background b(String str) {
        Wg6.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Background) new Gson().l(str, new Bi().getType());
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> c(String str) {
        Wg6.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object l = new Gson().l(str, new Ci().getType());
        Wg6.b(l, "Gson().fromJson(json, type)");
        return (ArrayList) l;
    }

    @DexIgnore
    public final String d(ArrayList<RingStyleItem> arrayList) {
        if (Ff2.a(arrayList)) {
            return "";
        }
        String u = new Gson().u(arrayList, new Di().getType());
        Wg6.b(u, "Gson().toJson(ringStyles, type)");
        return u;
    }
}
