package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Tt {
    c((byte) 1),
    d((byte) 2),
    e((byte) 3),
    f((byte) 4),
    g((byte) 5);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Tt(byte b2) {
        this.b = (byte) b2;
    }
}
