package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W16 extends U47 implements V16 {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ Ai w; // = new Ai(null);
    @DexIgnore
    public /* final */ Zp0 k; // = new Sr4(this);
    @DexIgnore
    public G37<L95> l;
    @DexIgnore
    public U16 m;
    @DexIgnore
    public Po4 s;
    @DexIgnore
    public R16 t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return W16.v;
        }

        @DexIgnore
        public final W16 b() {
            return new W16();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ W16 b;

        @DexIgnore
        public Bi(W16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            W16.A6(this.b).n(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ W16 b;

        @DexIgnore
        public Ci(W16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            W16.A6(this.b).n(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ W16 b;

        @DexIgnore
        public Di(W16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            W16.A6(this.b).n(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ W16 b;

        @DexIgnore
        public Ei(W16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismissAllowingStateLoss();
        }
    }

    /*
    static {
        String simpleName = W16.class.getSimpleName();
        Wg6.b(simpleName, "NotificationSettingsType\u2026nt::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ U16 A6(W16 w16) {
        U16 u16 = w16.m;
        if (u16 != null) {
            return u16;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void C6() {
        G37<L95> g37 = this.l;
        if (g37 != null) {
            L95 a2 = g37.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.w;
                Wg6.b(rTLImageView, "it.ivEveryoneCheck");
                rTLImageView.setVisibility(4);
                RTLImageView rTLImageView2 = a2.x;
                Wg6.b(rTLImageView2, "it.ivFavoriteContactsCheck");
                rTLImageView2.setVisibility(4);
                RTLImageView rTLImageView3 = a2.y;
                Wg6.b(rTLImageView3, "it.ivNoOneCheck");
                rTLImageView3.setVisibility(4);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void D6(U16 u16) {
        Wg6.c(u16, "presenter");
        this.m = u16;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(U16 u16) {
        D6(u16);
    }

    @DexIgnore
    @Override // com.fossil.V16
    public void e2(int i) {
        C6();
        G37<L95> g37 = this.l;
        if (g37 != null) {
            L95 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (i == 0) {
                RTLImageView rTLImageView = a2.w;
                Wg6.b(rTLImageView, "it.ivEveryoneCheck");
                rTLImageView.setVisibility(0);
            } else if (i == 1) {
                RTLImageView rTLImageView2 = a2.x;
                Wg6.b(rTLImageView2, "it.ivFavoriteContactsCheck");
                rTLImageView2.setVisibility(0);
            } else if (i == 2) {
                RTLImageView rTLImageView3 = a2.y;
                Wg6.b(rTLImageView3, "it.ivNoOneCheck");
                rTLImageView3.setVisibility(0);
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().U1(new Y16(this)).c(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
            Po4 po4 = this.s;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(notificationCallsAndMessagesActivity, po4).a(R16.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                R16 r16 = (R16) a2;
                this.t = r16;
                U16 u16 = this.m;
                if (u16 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (r16 != null) {
                    u16.o(r16);
                } else {
                    Wg6.n("mNotificationSettingViewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        L95 l95 = (L95) Aq0.f(layoutInflater, 2131558596, viewGroup, false, this.k);
        String d = ThemeManager.l.a().d("nonBrandSurface");
        String d2 = ThemeManager.l.a().d("nonBrandDisableCalendarDay");
        if (!TextUtils.isEmpty(d)) {
            l95.q.setBackgroundColor(Color.parseColor(d));
        }
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            l95.z.setBackgroundColor(parseColor);
            l95.A.setBackgroundColor(parseColor);
            l95.B.setBackgroundColor(parseColor);
        }
        l95.r.setOnClickListener(new Bi(this));
        l95.s.setOnClickListener(new Ci(this));
        l95.t.setOnClickListener(new Di(this));
        l95.v.setOnClickListener(new Ei(this));
        this.l = new G37<>(this, l95);
        Wg6.b(l95, "binding");
        return l95.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        U16 u16 = this.m;
        if (u16 != null) {
            u16.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        U16 u16 = this.m;
        if (u16 != null) {
            u16.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.V16
    public void t(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "title");
        G37<L95> g37 = this.l;
        if (g37 != null) {
            L95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.u) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
