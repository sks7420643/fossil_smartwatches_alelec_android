package com.fossil;

import com.fossil.E13;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lu2 extends E13<Lu2, Bi> implements O23 {
    @DexIgnore
    public static /* final */ Lu2 zzh;
    @DexIgnore
    public static volatile Z23<Lu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public M13<String> zzg; // = E13.B();

    @DexIgnore
    public enum Ai implements G13 {
        zza(0),
        zzb(1),
        zzc(2),
        zzd(3),
        zze(4),
        zzf(5),
        zzg(6);
        
        @DexIgnore
        public /* final */ int zzi;

        @DexIgnore
        public Ai(int i) {
            this.zzi = i;
        }

        @DexIgnore
        public static Ai zza(int i) {
            switch (i) {
                case 0:
                    return zza;
                case 1:
                    return zzb;
                case 2:
                    return zzc;
                case 3:
                    return zzd;
                case 4:
                    return zze;
                case 5:
                    return zzf;
                case 6:
                    return zzg;
                default:
                    return null;
            }
        }

        @DexIgnore
        public static I13 zzb() {
            return Nu2.a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + Ai.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.G13
        public final int zza() {
            return this.zzi;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends E13.Ai<Lu2, Bi> implements O23 {
        @DexIgnore
        public Bi() {
            super(Lu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ Bi(Fu2 fu2) {
            this();
        }
    }

    /*
    static {
        Lu2 lu2 = new Lu2();
        zzh = lu2;
        E13.u(Lu2.class, lu2);
    }
    */

    @DexIgnore
    public static Lu2 L() {
        return zzh;
    }

    @DexIgnore
    public final boolean C() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final Ai D() {
        Ai zza = Ai.zza(this.zzd);
        return zza == null ? Ai.zza : zza;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String G() {
        return this.zze;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean I() {
        return this.zzf;
    }

    @DexIgnore
    public final List<String> J() {
        return this.zzg;
    }

    @DexIgnore
    public final int K() {
        return this.zzg.size();
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Fu2.a[i - 1]) {
            case 1:
                return new Lu2();
            case 2:
                return new Bi(null);
            case 3:
                I13 zzb = Ai.zzb();
                return E13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u1008\u0001\u0003\u1007\u0002\u0004\u001a", new Object[]{"zzc", "zzd", zzb, "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                Z23<Lu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Lu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
