package com.fossil;

import android.content.res.Configuration;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pm0 {
    @DexIgnore
    public static Rm0 a(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 24) {
            return Rm0.d(configuration.getLocales());
        }
        return Rm0.a(configuration.locale);
    }
}
