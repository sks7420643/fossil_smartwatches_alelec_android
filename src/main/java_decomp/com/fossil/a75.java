package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A75 extends Z65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d Q; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray R;
    @DexIgnore
    public long P;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        R = sparseIntArray;
        sparseIntArray.put(2131362117, 1);
        R.put(2131362666, 2);
        R.put(2131363410, 3);
        R.put(2131362089, 4);
        R.put(2131362667, 5);
        R.put(2131362402, 6);
        R.put(2131362401, 7);
        R.put(2131362783, 8);
        R.put(2131362056, 9);
        R.put(2131362574, 10);
        R.put(2131362517, 11);
        R.put(2131362516, 12);
        R.put(2131363111, 13);
        R.put(2131362482, 14);
        R.put(2131362481, 15);
        R.put(2131362492, 16);
        R.put(2131362735, 17);
        R.put(2131361890, 18);
        R.put(2131362051, 19);
        R.put(2131362189, 20);
        R.put(2131362796, 21);
        R.put(2131363455, 22);
        R.put(2131362494, 23);
        R.put(2131363023, 24);
    }
    */

    @DexIgnore
    public A75(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 25, Q, R));
    }

    @DexIgnore
    public A75(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (AppBarLayout) objArr[18], (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[1], (TodayHeartRateChart) objArr[20], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[11], (Guideline) objArr[10], (RTLImageView) objArr[2], (ImageView) objArr[5], (ImageView) objArr[17], (View) objArr[8], (LinearLayout) objArr[21], (ConstraintLayout) objArr[0], (RecyclerView) objArr[24], (View) objArr[13], (FlexibleTextView) objArr[3], (View) objArr[22]);
        this.P = -1;
        this.K.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.P = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.P != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.P = 1;
        }
        w();
    }
}
