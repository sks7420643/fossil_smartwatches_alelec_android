package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Uv extends Tv {
    @DexIgnore
    public byte[] L; // = new byte[0];
    @DexIgnore
    public byte M; // = ((byte) -1);
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public N6 P; // = N6.k;

    @DexIgnore
    public Uv(Sv sv, short s, Hs hs, K5 k5, int i) {
        super(sv, s, hs, k5, i);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final JSONObject F(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (!this.N) {
            this.N = true;
            this.E = this.v.d != Lw.b;
            byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) 8).array();
            Wg6.b(array, "ByteBuffer.allocate(1)\n \u2026                 .array()");
            Q(array);
        } else {
            this.E = true;
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final boolean G(O7 o7) {
        return o7.a == this.P;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final void I(O7 o7) {
        if (!this.N) {
            super.I(o7);
            return;
        }
        byte[] bArr = o7.b;
        this.E = true;
        this.g.add(new Hw(0, o7.a, bArr, new JSONObject(), 1));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final void J(O7 o7) {
        byte[] b = this.s ? Jx.b.b(this.y.x, this.P, o7.b) : o7.b;
        int n = Hy1.n(Hy1.p((byte) (b[0] & 63)));
        int n2 = Hy1.n(Hy1.p((byte) (((byte) (this.M + 1)) & 63)));
        if (n == n2) {
            n(this.p);
            this.M = (byte) ((byte) n2);
            this.L = Dy1.a(this.L, Dm7.k(b, 1, b.length));
            boolean z = ((byte) (b[0] & Byte.MIN_VALUE)) != ((byte) 0);
            this.O = z;
            if (z) {
                S(this.L);
                T();
                return;
            }
            return;
        }
        this.v = Mw.a(this.v, null, null, Lw.h, null, null, 27);
        this.E = true;
    }

    @DexIgnore
    public abstract void S(byte[] bArr);

    @DexIgnore
    public void T() {
    }
}
