package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I66 implements Factory<NotificationHybridEveryonePresenter> {
    @DexIgnore
    public static NotificationHybridEveryonePresenter a(C66 c66, int i, ArrayList<J06> arrayList, Uq4 uq4, Y56 y56) {
        return new NotificationHybridEveryonePresenter(c66, i, arrayList, uq4, y56);
    }
}
