package com.fossil;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.pm.PackageManager;
import com.mapped.W6;
import com.sina.weibo.sdk.statistic.LogBuilder;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pg3 extends In3 {
    @DexIgnore
    public long c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;
    @DexIgnore
    public AccountManager f;
    @DexIgnore
    public Boolean g;
    @DexIgnore
    public long h;

    @DexIgnore
    public Pg3(Pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    @Override // com.fossil.In3
    public final boolean r() {
        Calendar instance = Calendar.getInstance();
        this.c = TimeUnit.MINUTES.convert((long) (instance.get(16) + instance.get(15)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String lowerCase = locale.getLanguage().toLowerCase(Locale.ENGLISH);
        String lowerCase2 = locale.getCountry().toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder(String.valueOf(lowerCase).length() + 1 + String.valueOf(lowerCase2).length());
        sb.append(lowerCase);
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        sb.append(lowerCase2);
        this.d = sb.toString();
        return false;
    }

    @DexIgnore
    public final boolean t(Context context) {
        if (this.e == null) {
            b();
            this.e = Boolean.FALSE;
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    packageManager.getPackageInfo("com.google.android.gms", 128);
                    this.e = Boolean.TRUE;
                }
            } catch (PackageManager.NameNotFoundException e2) {
            }
        }
        return this.e.booleanValue();
    }

    @DexIgnore
    public final long u() {
        o();
        return this.c;
    }

    @DexIgnore
    public final String v() {
        o();
        return this.d;
    }

    @DexIgnore
    public final long w() {
        h();
        return this.h;
    }

    @DexIgnore
    public final void x() {
        h();
        this.g = null;
        this.h = 0;
    }

    @DexIgnore
    public final boolean y() {
        h();
        long b = zzm().b();
        if (b - this.h > LogBuilder.MAX_INTERVAL) {
            this.g = null;
        }
        Boolean bool = this.g;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (W6.a(e(), "android.permission.GET_ACCOUNTS") != 0) {
            d().J().a("Permission error checking for dasher/unicorn accounts");
            this.h = b;
            this.g = Boolean.FALSE;
            return false;
        }
        if (this.f == null) {
            this.f = AccountManager.get(e());
        }
        try {
            Account[] result = this.f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_HOSTED"}, null, null).getResult();
            if (result == null || result.length <= 0) {
                Account[] result2 = this.f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_uca"}, null, null).getResult();
                if (result2 != null && result2.length > 0) {
                    this.g = Boolean.TRUE;
                    this.h = b;
                    return true;
                }
                this.h = b;
                this.g = Boolean.FALSE;
                return false;
            }
            this.g = Boolean.TRUE;
            this.h = b;
            return true;
        } catch (AuthenticatorException | OperationCanceledException | IOException e2) {
            d().G().b("Exception checking account types", e2);
        }
    }
}
