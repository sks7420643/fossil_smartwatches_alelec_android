package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ey1 {
    @DexIgnore
    public static final String a(Enum<?> r2) {
        Wg6.c(r2, "$this$lowerCaseName");
        String name = r2.name();
        Locale b = Dx1.b();
        if (name != null) {
            String lowerCase = name.toLowerCase(b);
            Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            return lowerCase;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }
}
