package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ho1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Do1 c;
    @DexIgnore
    public /* final */ Co1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ho1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ho1 createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                Do1 valueOf = Do1.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    return new Ho1(readInt, valueOf, Co1.valueOf(readString2));
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ho1[] newArray(int i) {
            return new Ho1[i];
        }
    }

    @DexIgnore
    public Ho1(int i, Do1 do1, Co1 co1) {
        this.b = i;
        this.c = do1;
        this.d = co1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ho1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Ho1 ho1 = (Ho1) obj;
            if (this.b != ho1.b) {
                return false;
            }
            if (this.c != ho1.c) {
                return false;
            }
            return this.d == ho1.d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationEvent");
    }

    @DexIgnore
    public final Do1 getAction() {
        return this.c;
    }

    @DexIgnore
    public final Co1 getActionStatus() {
        return this.d;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Integer.valueOf(this.b).hashCode();
        return (((hashCode * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.d4, Integer.valueOf(this.b)), Jd0.v0, Ey1.a(this.c)), Jd0.w0, Ey1.a(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
