package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zy3 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    int b();

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    Bx3 d();

    @DexIgnore
    boolean e();

    @DexIgnore
    Object f();  // void declaration

    @DexIgnore
    void g(Bx3 bx3);

    @DexIgnore
    AnimatorSet h();

    @DexIgnore
    List<Animator.AnimatorListener> i();

    @DexIgnore
    void j(ExtendedFloatingActionButton.h hVar);

    @DexIgnore
    void onAnimationStart(Animator animator);
}
