package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P42 implements Parcelable.Creator<M42> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ M42 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Bundle bundle = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i2 = Ad2.v(parcel, t);
            } else if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                bundle = Ad2.a(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new M42(i2, i, bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ M42[] newArray(int i) {
        return new M42[i];
    }
}
