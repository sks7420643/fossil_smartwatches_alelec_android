package com.fossil;

import com.fossil.Aa4;
import com.fossil.Ba4;
import com.fossil.Ca4;
import com.fossil.Da4;
import com.fossil.Ea4;
import com.fossil.Ga4;
import com.fossil.Ha4;
import com.fossil.Ia4;
import com.fossil.Ja4;
import com.fossil.Pa4;
import com.fossil.Qa4;
import com.fossil.Ra4;
import com.fossil.Sa4;
import com.fossil.Z94;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ta4 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("UTF-8");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Ta4 a();

        @DexIgnore
        public abstract Ai b(String str);

        @DexIgnore
        public abstract Ai c(String str);

        @DexIgnore
        public abstract Ai d(String str);

        @DexIgnore
        public abstract Ai e(String str);

        @DexIgnore
        public abstract Ai f(Ci ci);

        @DexIgnore
        public abstract Ai g(int i);

        @DexIgnore
        public abstract Ai h(String str);

        @DexIgnore
        public abstract Ai i(Di di);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Aii {
            @DexIgnore
            public abstract Bi a();

            @DexIgnore
            public abstract Aii b(String str);

            @DexIgnore
            public abstract Aii c(String str);
        }

        @DexIgnore
        public static Aii a() {
            return new Aa4.Bi();
        }

        @DexIgnore
        public abstract String b();

        @DexIgnore
        public abstract String c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Aii {
            @DexIgnore
            public abstract Ci a();

            @DexIgnore
            public abstract Aii b(Ua4<Bii> ua4);

            @DexIgnore
            public abstract Aii c(String str);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Bii {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Aiii {
                @DexIgnore
                public abstract Bii a();

                @DexIgnore
                public abstract Aiii b(byte[] bArr);

                @DexIgnore
                public abstract Aiii c(String str);
            }

            @DexIgnore
            public static Aiii a() {
                return new Ca4.Bi();
            }

            @DexIgnore
            public abstract byte[] b();

            @DexIgnore
            public abstract String c();
        }

        @DexIgnore
        public static Aii a() {
            return new Ba4.Bi();
        }

        @DexIgnore
        public abstract Ua4<Bii> b();

        @DexIgnore
        public abstract String c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Di {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Aii {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Aiii {
                @DexIgnore
                public abstract Aii a();

                @DexIgnore
                public abstract Aiii b(String str);

                @DexIgnore
                public abstract Aiii c(String str);

                @DexIgnore
                public abstract Aiii d(String str);

                @DexIgnore
                public abstract Aiii e(String str);
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Biii {
                @DexIgnore
                public abstract String a();
            }

            @DexIgnore
            public static Aiii a() {
                return new Ea4.Bi();
            }

            @DexIgnore
            public abstract String b();

            @DexIgnore
            public abstract String c();

            @DexIgnore
            public abstract String d();

            @DexIgnore
            public abstract Biii e();

            @DexIgnore
            public abstract String f();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Bii {
            @DexIgnore
            public abstract Di a();

            @DexIgnore
            public abstract Bii b(Aii aii);

            @DexIgnore
            public abstract Bii c(boolean z);

            @DexIgnore
            public abstract Bii d(Cii cii);

            @DexIgnore
            public abstract Bii e(Long l);

            @DexIgnore
            public abstract Bii f(Ua4<Dii> ua4);

            @DexIgnore
            public abstract Bii g(String str);

            @DexIgnore
            public abstract Bii h(int i);

            @DexIgnore
            public abstract Bii i(String str);

            @DexIgnore
            public Bii j(byte[] bArr) {
                i(new String(bArr, Ta4.a));
                return this;
            }

            @DexIgnore
            public abstract Bii k(Eii eii);

            @DexIgnore
            public abstract Bii l(long j);

            @DexIgnore
            public abstract Bii m(Fii fii);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Cii {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Aiii {
                @DexIgnore
                public abstract Cii a();

                @DexIgnore
                public abstract Aiii b(int i);

                @DexIgnore
                public abstract Aiii c(int i);

                @DexIgnore
                public abstract Aiii d(long j);

                @DexIgnore
                public abstract Aiii e(String str);

                @DexIgnore
                public abstract Aiii f(String str);

                @DexIgnore
                public abstract Aiii g(String str);

                @DexIgnore
                public abstract Aiii h(long j);

                @DexIgnore
                public abstract Aiii i(boolean z);

                @DexIgnore
                public abstract Aiii j(int i);
            }

            @DexIgnore
            public static Aiii a() {
                return new Ga4.Bi();
            }

            @DexIgnore
            public abstract int b();

            @DexIgnore
            public abstract int c();

            @DexIgnore
            public abstract long d();

            @DexIgnore
            public abstract String e();

            @DexIgnore
            public abstract String f();

            @DexIgnore
            public abstract String g();

            @DexIgnore
            public abstract long h();

            @DexIgnore
            public abstract int i();

            @DexIgnore
            public abstract boolean j();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Dii {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Aiii {

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static abstract class Aiiii {
                    @DexIgnore
                    public abstract Aiii a();

                    @DexIgnore
                    public abstract Aiiii b(Boolean bool);

                    @DexIgnore
                    public abstract Aiiii c(Ua4<Bi> ua4);

                    @DexIgnore
                    public abstract Aiiii d(Biiii biiii);

                    @DexIgnore
                    public abstract Aiiii e(int i);
                }

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static abstract class Biiii {
                    @DexIgnore
                    public static Ta4$d$d$a$b$b a() {
                        return new Ja4.Bi();
                    }

                    @DexIgnore
                    public abstract Ua4<Ta4$d$d$a$b$a> b();

                    @DexIgnore
                    public abstract Ta4$d$d$a$b$c c();

                    @DexIgnore
                    public abstract Ta4$d$d$a$b$d d();

                    @DexIgnore
                    public abstract Ua4<Ta4$d$d$a$b$e> e();
                }

                @DexIgnore
                public static Aiiii a() {
                    return new Ia4.Bi();
                }

                @DexIgnore
                public abstract Boolean b();

                @DexIgnore
                public abstract Ua4<Bi> c();

                @DexIgnore
                public abstract Biiii d();

                @DexIgnore
                public abstract int e();

                @DexIgnore
                public abstract Aiiii f();
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Biii {
                @DexIgnore
                public abstract Dii a();

                @DexIgnore
                public abstract Biii b(Aiii aiii);

                @DexIgnore
                public abstract Biii c(Ciii ciii);

                @DexIgnore
                public abstract Biii d(Diii diii);

                @DexIgnore
                public abstract Biii e(long j);

                @DexIgnore
                public abstract Biii f(String str);
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Ciii {

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static abstract class Aiiii {
                    @DexIgnore
                    public abstract Ciii a();

                    @DexIgnore
                    public abstract Aiiii b(Double d);

                    @DexIgnore
                    public abstract Aiiii c(int i);

                    @DexIgnore
                    public abstract Aiiii d(long j);

                    @DexIgnore
                    public abstract Aiiii e(int i);

                    @DexIgnore
                    public abstract Aiiii f(boolean z);

                    @DexIgnore
                    public abstract Aiiii g(long j);
                }

                @DexIgnore
                public static Aiiii a() {
                    return new Pa4.Bi();
                }

                @DexIgnore
                public abstract Double b();

                @DexIgnore
                public abstract int c();

                @DexIgnore
                public abstract long d();

                @DexIgnore
                public abstract int e();

                @DexIgnore
                public abstract long f();

                @DexIgnore
                public abstract boolean g();
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Diii {

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static abstract class Aiiii {
                    @DexIgnore
                    public abstract Diii a();

                    @DexIgnore
                    public abstract Aiiii b(String str);
                }

                @DexIgnore
                public static Aiiii a() {
                    return new Qa4.Bi();
                }

                @DexIgnore
                public abstract String b();
            }

            @DexIgnore
            public static Biii a() {
                return new Ha4.Bi();
            }

            @DexIgnore
            public abstract Aiii b();

            @DexIgnore
            public abstract Ciii c();

            @DexIgnore
            public abstract Diii d();

            @DexIgnore
            public abstract long e();

            @DexIgnore
            public abstract String f();

            @DexIgnore
            public abstract Biii g();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Eii {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Aiii {
                @DexIgnore
                public abstract Eii a();

                @DexIgnore
                public abstract Aiii b(String str);

                @DexIgnore
                public abstract Aiii c(boolean z);

                @DexIgnore
                public abstract Aiii d(int i);

                @DexIgnore
                public abstract Aiii e(String str);
            }

            @DexIgnore
            public static Aiii a() {
                return new Ra4.Bi();
            }

            @DexIgnore
            public abstract String b();

            @DexIgnore
            public abstract int c();

            @DexIgnore
            public abstract String d();

            @DexIgnore
            public abstract boolean e();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Fii {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class Aiii {
                @DexIgnore
                public abstract Fii a();

                @DexIgnore
                public abstract Aiii b(String str);
            }

            @DexIgnore
            public static Aiii a() {
                return new Sa4.Bi();
            }

            @DexIgnore
            public abstract String b();
        }

        @DexIgnore
        public static Bii a() {
            Da4.Bi bi = new Da4.Bi();
            bi.c(false);
            return bi;
        }

        @DexIgnore
        public abstract Aii b();

        @DexIgnore
        public abstract Cii c();

        @DexIgnore
        public abstract Long d();

        @DexIgnore
        public abstract Ua4<Dii> e();

        @DexIgnore
        public abstract String f();

        @DexIgnore
        public abstract int g();

        @DexIgnore
        public abstract String h();

        @DexIgnore
        public byte[] i() {
            return h().getBytes(Ta4.a);
        }

        @DexIgnore
        public abstract Eii j();

        @DexIgnore
        public abstract long k();

        @DexIgnore
        public abstract Fii l();

        @DexIgnore
        public abstract boolean m();

        @DexIgnore
        public abstract Bii n();

        @DexIgnore
        public Di o(Ua4<Dii> ua4) {
            Bii n = n();
            n.f(ua4);
            return n.a();
        }

        @DexIgnore
        public Di p(long j, boolean z, String str) {
            Bii n = n();
            n.e(Long.valueOf(j));
            n.c(z);
            if (str != null) {
                Fii.Aiii a2 = Fii.a();
                a2.b(str);
                n.m(a2.a());
                n.a();
            }
            return n.a();
        }
    }

    @DexIgnore
    public enum Ei {
        INCOMPLETE,
        JAVA,
        NATIVE
    }

    @DexIgnore
    public static Ai b() {
        return new Z94.Bi();
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract Ci g();

    @DexIgnore
    public abstract int h();

    @DexIgnore
    public abstract String i();

    @DexIgnore
    public abstract Di j();

    @DexIgnore
    public Ei k() {
        return j() != null ? Ei.JAVA : g() != null ? Ei.NATIVE : Ei.INCOMPLETE;
    }

    @DexIgnore
    public abstract Ai l();

    @DexIgnore
    public Ta4 m(Ua4<Di.Dii> ua4) {
        if (j() != null) {
            Ai l = l();
            l.i(j().o(ua4));
            return l.a();
        }
        throw new IllegalStateException("Reports without sessions cannot have events added to them.");
    }

    @DexIgnore
    public Ta4 n(Ci ci) {
        Ai l = l();
        l.i(null);
        l.f(ci);
        return l.a();
    }

    @DexIgnore
    public Ta4 o(long j, boolean z, String str) {
        Ai l = l();
        if (j() != null) {
            l.i(j().p(j, z, str));
        }
        return l.a();
    }
}
