package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface A18 extends Cloneable {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        A18 d(V18 v18);
    }

    @DexIgnore
    Response a() throws IOException;

    @DexIgnore
    V18 c();

    @DexIgnore
    Object cancel();  // void declaration

    @DexIgnore
    boolean f();

    @DexIgnore
    void m(B18 b18);
}
