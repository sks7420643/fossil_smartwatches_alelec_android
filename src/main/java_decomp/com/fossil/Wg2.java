package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.fossil.Qg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wg2 implements Qg2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Activity a;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle c;
    @DexIgnore
    public /* final */ /* synthetic */ Qg2 d;

    @DexIgnore
    public Wg2(Qg2 qg2, Activity activity, Bundle bundle, Bundle bundle2) {
        this.d = qg2;
        this.a = activity;
        this.b = bundle;
        this.c = bundle2;
    }

    @DexIgnore
    @Override // com.fossil.Qg2.Ai
    public final void a(Sg2 sg2) {
        this.d.a.p(this.a, this.b, this.c);
    }

    @DexIgnore
    @Override // com.fossil.Qg2.Ai
    public final int getState() {
        return 0;
    }
}
