package com.fossil;

import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hc implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ E60 b;

    @DexIgnore
    public Hc(E60 e60) {
        this.b = e60;
    }

    @DexIgnore
    public final void run() {
        Zw zw = Zw.i;
        Zw.c.remove(this.b);
        if (Zw.i.g(this.b) && BluetoothLeAdapter.k.w() == BluetoothLeAdapter.Ci.ENABLED) {
            if (T0.c[this.b.H0().ordinal()] == 1) {
                Zw zw2 = Zw.i;
                if (Zw.d.add(this.b)) {
                    this.b.m0(new HashMap<>()).s(new Z3(this)).r(new G8(this));
                }
            }
        }
    }
}
