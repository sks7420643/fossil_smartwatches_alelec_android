package com.fossil;

import com.fossil.Af1;
import com.fossil.Ua1;
import com.fossil.Uc1;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tc1<Transcode> {
    @DexIgnore
    public /* final */ List<Af1.Ai<?>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Mb1> b; // = new ArrayList();
    @DexIgnore
    public Qa1 c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Class<?> g;
    @DexIgnore
    public Uc1.Ei h;
    @DexIgnore
    public Ob1 i;
    @DexIgnore
    public Map<Class<?>, Sb1<?>> j;
    @DexIgnore
    public Class<Transcode> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public Mb1 n;
    @DexIgnore
    public Sa1 o;
    @DexIgnore
    public Wc1 p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    @DexIgnore
    public Od1 b() {
        return this.c.b();
    }

    @DexIgnore
    public List<Mb1> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<Af1.Ai<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                Af1.Ai<?> ai = g2.get(i2);
                if (!this.b.contains(ai.a)) {
                    this.b.add(ai.a);
                }
                for (int i3 = 0; i3 < ai.b.size(); i3++) {
                    if (!this.b.contains(ai.b.get(i3))) {
                        this.b.add(ai.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    @DexIgnore
    public Be1 d() {
        return this.h.a();
    }

    @DexIgnore
    public Wc1 e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public List<Af1.Ai<?>> g() {
        if (!this.l) {
            this.l = true;
            this.a.clear();
            List i2 = this.c.h().i(this.d);
            int size = i2.size();
            for (int i3 = 0; i3 < size; i3++) {
                Af1.Ai<?> b2 = ((Af1) i2.get(i3)).b(this.d, this.e, this.f, this.i);
                if (b2 != null) {
                    this.a.add(b2);
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public <Data> Gd1<Data, ?, Transcode> h(Class<Data> cls) {
        return this.c.h().h(cls, this.g, this.k);
    }

    @DexIgnore
    public Class<?> i() {
        return this.d.getClass();
    }

    @DexIgnore
    public List<Af1<File, ?>> j(File file) throws Ua1.Ci {
        return this.c.h().i(file);
    }

    @DexIgnore
    public Ob1 k() {
        return this.i;
    }

    @DexIgnore
    public Sa1 l() {
        return this.o;
    }

    @DexIgnore
    public List<Class<?>> m() {
        return this.c.h().j(this.d.getClass(), this.g, this.k);
    }

    @DexIgnore
    public <Z> Rb1<Z> n(Id1<Z> id1) {
        return this.c.h().k(id1);
    }

    @DexIgnore
    public Mb1 o() {
        return this.n;
    }

    @DexIgnore
    public <X> Jb1<X> p(X x) throws Ua1.Ei {
        return this.c.h().m(x);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.Class<Transcode>, java.lang.Class<?> */
    public Class<?> q() {
        return (Class<Transcode>) this.k;
    }

    @DexIgnore
    public <Z> Sb1<Z> r(Class<Z> cls) {
        Sb1<Z> sb1 = (Sb1<Z>) this.j.get(cls);
        if (sb1 == null) {
            Iterator<Map.Entry<Class<?>, Sb1<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Class<?>, Sb1<?>> next = it.next();
                if (next.getKey().isAssignableFrom(cls)) {
                    sb1 = (Sb1<Z>) next.getValue();
                    break;
                }
            }
        }
        if (sb1 != null) {
            return sb1;
        }
        if (!this.j.isEmpty() || !this.q) {
            return Tf1.c();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    public int s() {
        return this.e;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Class<?> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean t(Class<?> cls) {
        return h(cls) != null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.lang.Class<R> */
    /* JADX WARN: Multi-variable type inference failed */
    public <R> void u(Qa1 qa1, Object obj, Mb1 mb1, int i2, int i3, Wc1 wc1, Class<?> cls, Class<R> cls2, Sa1 sa1, Ob1 ob1, Map<Class<?>, Sb1<?>> map, boolean z, boolean z2, Uc1.Ei ei) {
        this.c = qa1;
        this.d = obj;
        this.n = mb1;
        this.e = i2;
        this.f = i3;
        this.p = wc1;
        this.g = cls;
        this.h = ei;
        this.k = cls2;
        this.o = sa1;
        this.i = ob1;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public boolean v(Id1<?> id1) {
        return this.c.h().n(id1);
    }

    @DexIgnore
    public boolean w() {
        return this.r;
    }

    @DexIgnore
    public boolean x(Mb1 mb1) {
        List<Af1.Ai<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).a.equals(mb1)) {
                return true;
            }
        }
        return false;
    }
}
