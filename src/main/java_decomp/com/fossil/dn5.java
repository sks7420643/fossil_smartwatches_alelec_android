package com.fossil;

import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import com.portfolio.platform.manager.FileDownloadManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dn5 implements MembersInjector<FileDownloadManager> {
    @DexIgnore
    public static void a(FileDownloadManager fileDownloadManager, DownloadServiceApi downloadServiceApi) {
        fileDownloadManager.c = downloadServiceApi;
    }
}
