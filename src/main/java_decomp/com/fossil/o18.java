package com.fossil;

import java.io.IOException;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O18 {
    @DexIgnore
    public /* final */ Y18 a;
    @DexIgnore
    public /* final */ D18 b;
    @DexIgnore
    public /* final */ List<Certificate> c;
    @DexIgnore
    public /* final */ List<Certificate> d;

    @DexIgnore
    public O18(Y18 y18, D18 d18, List<Certificate> list, List<Certificate> list2) {
        this.a = y18;
        this.b = d18;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public static O18 b(SSLSession sSLSession) throws IOException {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        } else if (!"SSL_NULL_WITH_NULL_NULL".equals(cipherSuite)) {
            D18 a2 = D18.a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null");
            } else if (!"NONE".equals(protocol)) {
                Y18 forJavaName = Y18.forJavaName(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException e) {
                    certificateArr = null;
                }
                List u = certificateArr != null ? B28.u(certificateArr) : Collections.emptyList();
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                return new O18(forJavaName, a2, u, localCertificates != null ? B28.u(localCertificates) : Collections.emptyList());
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }
    }

    @DexIgnore
    public static O18 c(Y18 y18, D18 d18, List<Certificate> list, List<Certificate> list2) {
        if (y18 == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (d18 != null) {
            return new O18(y18, d18, B28.t(list), B28.t(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    @DexIgnore
    public D18 a() {
        return this.b;
    }

    @DexIgnore
    public List<Certificate> d() {
        return this.d;
    }

    @DexIgnore
    public List<Certificate> e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof O18)) {
            return false;
        }
        O18 o18 = (O18) obj;
        return this.a.equals(o18.a) && this.b.equals(o18.b) && this.c.equals(o18.c) && this.d.equals(o18.d);
    }

    @DexIgnore
    public Y18 f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() + 527) * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }
}
