package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wr7 extends Ur7 {
    @DexIgnore
    public static /* final */ Wr7 f; // = new Wr7(1, 0);
    @DexIgnore
    public static /* final */ Ai g; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Wr7 a() {
            return Wr7.f;
        }
    }

    @DexIgnore
    public Wr7(int i, int i2) {
        super(i, i2, 1);
    }

    @DexIgnore
    @Override // com.fossil.Ur7
    public boolean equals(Object obj) {
        if (obj instanceof Wr7) {
            if (!isEmpty() || !((Wr7) obj).isEmpty()) {
                Wr7 wr7 = (Wr7) obj;
                if (!(a() == wr7.a() && b() == wr7.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public Integer g() {
        return Integer.valueOf(b());
    }

    @DexIgnore
    public Integer h() {
        return Integer.valueOf(a());
    }

    @DexIgnore
    @Override // com.fossil.Ur7
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    @DexIgnore
    @Override // com.fossil.Ur7
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    @Override // com.fossil.Ur7
    public String toString() {
        return a() + ".." + b();
    }
}
