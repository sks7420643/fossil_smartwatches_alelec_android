package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nr7<T> implements Or7<Object, T> {
    @DexIgnore
    public T a;

    @DexIgnore
    @Override // com.fossil.Or7
    public void a(Object obj, Ks7<?> ks7, T t) {
        Wg6.c(ks7, "property");
        Wg6.c(t, "value");
        this.a = t;
    }

    @DexIgnore
    @Override // com.fossil.Or7
    public T b(Object obj, Ks7<?> ks7) {
        Wg6.c(ks7, "property");
        T t = this.a;
        if (t != null) {
            return t;
        }
        throw new IllegalStateException("Property " + ks7.getName() + " should be initialized before get.");
    }
}
