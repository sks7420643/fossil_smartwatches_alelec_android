package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A53 implements Xw2<Z43> {
    @DexIgnore
    public static A53 c; // = new A53();
    @DexIgnore
    public /* final */ Xw2<Z43> b;

    @DexIgnore
    public A53() {
        this(Ww2.b(new C53()));
    }

    @DexIgnore
    public A53(Xw2<Z43> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((Z43) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ Z43 zza() {
        return this.b.zza();
    }
}
