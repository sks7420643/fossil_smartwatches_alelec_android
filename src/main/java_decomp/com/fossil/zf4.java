package com.fossil;

import java.util.concurrent.ScheduledFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Zf4 implements Ht3 {
    @DexIgnore
    public /* final */ Bg4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ScheduledFuture c;

    @DexIgnore
    public Zf4(Bg4 bg4, String str, ScheduledFuture scheduledFuture) {
        this.a = bg4;
        this.b = str;
        this.c = scheduledFuture;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3 nt3) {
        this.a.g(this.b, this.c, nt3);
    }
}
