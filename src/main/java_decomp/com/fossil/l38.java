package com.fossil;

import com.fossil.F38;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L38 implements Closeable {
    @DexIgnore
    public static /* final */ Logger h; // = Logger.getLogger(G38.class.getName());
    @DexIgnore
    public /* final */ J48 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ I48 d;
    @DexIgnore
    public int e; // = 16384;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ F38.Bi g;

    @DexIgnore
    public L38(J48 j48, boolean z) {
        this.b = j48;
        this.c = z;
        I48 i48 = new I48();
        this.d = i48;
        this.g = new F38.Bi(i48);
    }

    @DexIgnore
    public static void G(J48 j48, int i) throws IOException {
        j48.v((i >>> 16) & 255);
        j48.v((i >>> 8) & 255);
        j48.v(i & 255);
    }

    @DexIgnore
    public void A(int i, D38 d38) throws IOException {
        synchronized (this) {
            if (this.f) {
                throw new IOException("closed");
            } else if (d38.httpCode != -1) {
                h(i, 4, (byte) 3, (byte) 0);
                this.b.p(d38.httpCode);
                this.b.flush();
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    @DexIgnore
    public void B(O38 o38) throws IOException {
        int i = 0;
        synchronized (this) {
            if (!this.f) {
                h(0, o38.j() * 6, (byte) 4, (byte) 0);
                while (i < 10) {
                    if (o38.g(i)) {
                        this.b.n(i == 4 ? 3 : i == 7 ? 4 : i);
                        this.b.p(o38.b(i));
                    }
                    i++;
                }
                this.b.flush();
            } else {
                throw new IOException("closed");
            }
        }
    }

    @DexIgnore
    public void C(boolean z, int i, int i2, List<E38> list) throws IOException {
        synchronized (this) {
            if (!this.f) {
                k(z, i, list);
            } else {
                throw new IOException("closed");
            }
        }
    }

    @DexIgnore
    public void D(int i, long j) throws IOException {
        synchronized (this) {
            if (this.f) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                G38.c("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
                throw null;
            } else {
                h(i, 4, (byte) 8, (byte) 0);
                this.b.p((int) j);
                this.b.flush();
            }
        }
    }

    @DexIgnore
    public final void F(int i, long j) throws IOException {
        while (j > 0) {
            int min = (int) Math.min((long) this.e, j);
            long j2 = (long) min;
            j -= j2;
            h(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.b.K(this.d, j2);
        }
    }

    @DexIgnore
    public void a(O38 o38) throws IOException {
        synchronized (this) {
            if (!this.f) {
                this.e = o38.f(this.e);
                if (o38.c() != -1) {
                    this.g.e(o38.c());
                }
                h(0, 0, (byte) 4, (byte) 1);
                this.b.flush();
            } else {
                throw new IOException("closed");
            }
        }
    }

    @DexIgnore
    public void b() throws IOException {
        synchronized (this) {
            if (this.f) {
                throw new IOException("closed");
            } else if (this.c) {
                if (h.isLoggable(Level.FINE)) {
                    h.fine(B28.r(">> CONNECTION %s", G38.a.hex()));
                }
                this.b.Z(G38.a.toByteArray());
                this.b.flush();
            }
        }
    }

    @DexIgnore
    public void c(boolean z, int i, I48 i48, int i2) throws IOException {
        synchronized (this) {
            if (!this.f) {
                byte b2 = 0;
                if (z) {
                    b2 = (byte) 1;
                }
                f(i, b2, i48, i2);
            } else {
                throw new IOException("closed");
            }
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            this.f = true;
            this.b.close();
        }
    }

    @DexIgnore
    public void f(int i, byte b2, I48 i48, int i2) throws IOException {
        h(i, i2, (byte) 0, b2);
        if (i2 > 0) {
            this.b.K(i48, (long) i2);
        }
    }

    @DexIgnore
    public void flush() throws IOException {
        synchronized (this) {
            if (!this.f) {
                this.b.flush();
            } else {
                throw new IOException("closed");
            }
        }
    }

    @DexIgnore
    public void h(int i, int i2, byte b2, byte b3) throws IOException {
        if (h.isLoggable(Level.FINE)) {
            h.fine(G38.b(false, i, i2, b2, b3));
        }
        int i3 = this.e;
        if (i2 > i3) {
            G38.c("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(i3), Integer.valueOf(i2));
            throw null;
        } else if ((Integer.MIN_VALUE & i) == 0) {
            G(this.b, i2);
            this.b.v(b2 & 255);
            this.b.v(b3 & 255);
            this.b.p(Integer.MAX_VALUE & i);
        } else {
            G38.c("reserved bit set: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public void j(int i, D38 d38, byte[] bArr) throws IOException {
        synchronized (this) {
            if (this.f) {
                throw new IOException("closed");
            } else if (d38.httpCode != -1) {
                h(0, bArr.length + 8, (byte) 7, (byte) 0);
                this.b.p(i);
                this.b.p(d38.httpCode);
                if (bArr.length > 0) {
                    this.b.Z(bArr);
                }
                this.b.flush();
            } else {
                G38.c("errorCode.httpCode == -1", new Object[0]);
                throw null;
            }
        }
    }

    @DexIgnore
    public void k(boolean z, int i, List<E38> list) throws IOException {
        if (!this.f) {
            this.g.g(list);
            long p0 = this.d.p0();
            int min = (int) Math.min((long) this.e, p0);
            long j = (long) min;
            int i2 = (p0 > j ? 1 : (p0 == j ? 0 : -1));
            byte b2 = i2 == 0 ? (byte) 4 : 0;
            if (z) {
                b2 = (byte) (b2 | 1);
            }
            h(i, min, (byte) 1, b2);
            this.b.K(this.d, j);
            if (i2 > 0) {
                F(i, p0 - j);
                return;
            }
            return;
        }
        throw new IOException("closed");
    }

    @DexIgnore
    public int l() {
        return this.e;
    }

    @DexIgnore
    public void m(boolean z, int i, int i2) throws IOException {
        byte b2 = 0;
        synchronized (this) {
            if (!this.f) {
                if (z) {
                    b2 = 1;
                }
                h(0, 8, (byte) 6, b2);
                this.b.p(i);
                this.b.p(i2);
                this.b.flush();
            } else {
                throw new IOException("closed");
            }
        }
    }

    @DexIgnore
    public void o(int i, int i2, List<E38> list) throws IOException {
        synchronized (this) {
            if (!this.f) {
                this.g.g(list);
                long p0 = this.d.p0();
                int min = (int) Math.min((long) (this.e - 4), p0);
                long j = (long) min;
                int i3 = (p0 > j ? 1 : (p0 == j ? 0 : -1));
                h(i, min + 4, (byte) 5, i3 == 0 ? (byte) 4 : 0);
                this.b.p(Integer.MAX_VALUE & i2);
                this.b.K(this.d, j);
                if (i3 > 0) {
                    F(i, p0 - j);
                }
            } else {
                throw new IOException("closed");
            }
        }
    }
}
