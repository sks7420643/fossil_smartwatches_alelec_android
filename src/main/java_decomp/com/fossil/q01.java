package com.fossil;

import android.net.Uri;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q01 {
    @DexIgnore
    public /* final */ Set<Ai> a; // = new HashSet();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Uri a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ai(Uri uri, boolean z) {
            this.a = uri;
            this.b = z;
        }

        @DexIgnore
        public Uri a() {
            return this.a;
        }

        @DexIgnore
        public boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Ai.class != obj.getClass()) {
                return false;
            }
            Ai ai = (Ai) obj;
            return this.b == ai.b && this.a.equals(ai.a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.a.hashCode() * 31) + (this.b ? 1 : 0);
        }
    }

    @DexIgnore
    public void a(Uri uri, boolean z) {
        this.a.add(new Ai(uri, z));
    }

    @DexIgnore
    public Set<Ai> b() {
        return this.a;
    }

    @DexIgnore
    public int c() {
        return this.a.size();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Q01.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((Q01) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
