package com.fossil;

import android.view.View;
import com.fossil.W67;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zb7 {
    @DexIgnore
    W67.Ci getType();

    @DexIgnore
    View getView();
}
