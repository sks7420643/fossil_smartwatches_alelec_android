package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gx7 {
    @DexIgnore
    public static /* final */ Vz7 a; // = new Vz7("COMPLETING_ALREADY");
    @DexIgnore
    public static /* final */ Vz7 b; // = new Vz7("COMPLETING_WAITING_CHILDREN");
    @DexIgnore
    public static /* final */ Vz7 c; // = new Vz7("COMPLETING_RETRY");
    @DexIgnore
    public static /* final */ Vz7 d; // = new Vz7("TOO_LATE_TO_CANCEL");
    @DexIgnore
    public static /* final */ Vz7 e; // = new Vz7("SEALED");
    @DexIgnore
    public static /* final */ Gw7 f; // = new Gw7(false);
    @DexIgnore
    public static /* final */ Gw7 g; // = new Gw7(true);

    @DexIgnore
    public static final Object g(Object obj) {
        return obj instanceof Sw7 ? new Tw7((Sw7) obj) : obj;
    }

    @DexIgnore
    public static final Object h(Object obj) {
        Sw7 sw7;
        Tw7 tw7 = (Tw7) (!(obj instanceof Tw7) ? null : obj);
        return (tw7 == null || (sw7 = tw7.a) == null) ? obj : sw7;
    }
}
