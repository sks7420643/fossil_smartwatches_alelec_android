package com.fossil;

import android.util.Log;
import com.fossil.M62;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fb2 implements Ht3<Map<G72<?>, String>> {
    @DexIgnore
    public /* final */ /* synthetic */ Db2 a;

    @DexIgnore
    public Fb2(Db2 db2) {
        this.a = db2;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3<Map<G72<?>, String>> nt3) {
        Db2.p(this.a).lock();
        try {
            if (Db2.z(this.a)) {
                if (nt3.q()) {
                    Db2.o(this.a, new Zi0(Db2.A(this.a).size()));
                    for (Bb2 bb2 : Db2.A(this.a).values()) {
                        Db2.B(this.a).put(bb2.a(), Z52.f);
                    }
                } else if (nt3.l() instanceof O62) {
                    O62 o62 = (O62) nt3.l();
                    if (Db2.C(this.a)) {
                        Db2.o(this.a, new Zi0(Db2.A(this.a).size()));
                        for (Bb2 bb22 : Db2.A(this.a).values()) {
                            G72 a2 = bb22.a();
                            Z52 connectionResult = o62.getConnectionResult((Q62<? extends M62.Di>) bb22);
                            if (Db2.r(this.a, bb22, connectionResult)) {
                                Db2.B(this.a).put(a2, new Z52(16));
                            } else {
                                Db2.B(this.a).put(a2, connectionResult);
                            }
                        }
                    } else {
                        Db2.o(this.a, o62.zaj());
                    }
                    Db2.n(this.a, Db2.D(this.a));
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", nt3.l());
                    Db2.o(this.a, Collections.emptyMap());
                    Db2.n(this.a, new Z52(8));
                }
                if (Db2.E(this.a) != null) {
                    Db2.B(this.a).putAll(Db2.E(this.a));
                    Db2.n(this.a, Db2.D(this.a));
                }
                if (Db2.F(this.a) == null) {
                    Db2.G(this.a);
                    Db2.H(this.a);
                } else {
                    Db2.s(this.a, false);
                    Db2.I(this.a).a(Db2.F(this.a));
                }
                Db2.J(this.a).signalAll();
                Db2.p(this.a).unlock();
            }
        } finally {
            Db2.p(this.a).unlock();
        }
    }
}
