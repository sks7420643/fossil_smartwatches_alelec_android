package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.yb7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z66 extends x66 {
    @DexIgnore
    public /* final */ CustomizeRealDataRepository A;
    @DexIgnore
    public /* final */ UserRepository B;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository C;
    @DexIgnore
    public /* final */ uo5 D;
    @DexIgnore
    public /* final */ s77 E;
    @DexIgnore
    public /* final */ yb7 F;
    @DexIgnore
    public LiveData<List<mo5>> e; // = new MutableLiveData();
    @DexIgnore
    public LiveData<List<CustomizeRealData>> f; // = this.A.getAllRealDataAsLiveData();
    @DexIgnore
    public /* final */ ArrayList<Complication> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WatchApp> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<mo5> i; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public mo5 j;
    @DexIgnore
    public MutableLiveData<String> k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> m; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public int n; // = 2;
    @DexIgnore
    public MFUser o;
    @DexIgnore
    public u08 p; // = w08.b(false, 1, null);
    @DexIgnore
    public boolean q;
    @DexIgnore
    public String r; // = "";
    @DexIgnore
    public boolean s;
    @DexIgnore
    public /* final */ y66 t;
    @DexIgnore
    public /* final */ WatchAppRepository u;
    @DexIgnore
    public /* final */ ComplicationRepository v;
    @DexIgnore
    public /* final */ jb6 w;
    @DexIgnore
    public /* final */ UserRepository x;
    @DexIgnore
    public /* final */ zm5 y;
    @DexIgnore
    public /* final */ FileRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$buildConfigsPerPreset$2", f = "HomeDianaCustomizePresenter.kt", l = {227}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super Map<String, List<? extends s87>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $presets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(z66 z66, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
            this.$presets = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$presets, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Map<String, List<? extends s87>>> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v19, types: [java.util.Map] */
        /* JADX WARN: Type inference failed for: r3v7, types: [java.lang.Iterable] */
        /* JADX WARN: Type inference failed for: r4v7, types: [java.util.Map] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0094  */
        /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x003e  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r16) {
            /*
            // Method dump skipped, instructions count: 218
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {Action.Music.MUSIC_END_ACTION, 207}, m = "buildUiPresets")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(z66 z66, qn7 qn7) {
            super(qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$buildUiPresets$configPerPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {208}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super Map<String, List<? extends s87>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $uiPresets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(z66 z66, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
            this.$uiPresets = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$uiPresets, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Map<String, List<? extends s87>>> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                z66 z66 = this.this$0;
                List<dp5> list = this.$uiPresets;
                this.L$0 = iv7;
                this.label = 1;
                Object e0 = z66.e0(list, this);
                return e0 == d ? d : e0;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$buildUiPresets$uiPresets$1", f = "HomeDianaCustomizePresenter.kt", l = {203}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super List<? extends dp5>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(z66 z66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super List<? extends dp5>> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v12, types: [java.util.Collection] */
        /* JADX WARN: Type inference failed for: r2v4, types: [java.util.Collection] */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0040  */
        /* JADX WARNING: Unknown variable types count: 2 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 251
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.e<yb7.d, yb7.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mo5 f4423a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ z66 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$e$a$a")
            /* renamed from: com.fossil.z66$e$a$a  reason: collision with other inner class name */
            public static final class C0297a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0297a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0297a aVar = new C0297a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((C0297a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        uo5 uo5 = this.this$0.this$0.c.D;
                        String str = this.this$0.this$0.b;
                        this.L$0 = iv7;
                        this.label = 1;
                        if (uo5.e(str, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("set new preset to watch success, delete current active ");
                    mo5 mo5 = this.this$0.f4423a;
                    sb.append(mo5 != null ? mo5.f() : null);
                    local.d("HomeDianaCustomizePresenter", sb.toString());
                    dv7 i2 = this.this$0.c.i();
                    C0297a aVar = new C0297a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(i2, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.c.t.w();
                this.this$0.c.t.O(this.this$0.c.j0());
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public e(mo5 mo5, String str, z66 z66, String str2) {
            this.f4423a = mo5;
            this.b = str;
            this.c = z66;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(yb7.b bVar) {
            pq7.c(bVar, "errorValue");
            this.c.t.w();
            int b2 = bVar.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                y66 y66 = this.c.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    y66.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.c.t.u();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(yb7.d dVar) {
            pq7.c(dVar, "responseValue");
            xw7 unused = gu7.d(this.c.k(), null, null, new a(this, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ mo5 $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    uo5 uo5 = this.this$0.this$0.D;
                    String e = this.this$0.$preset.e();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (uo5.e(e, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(mo5 mo5, qn7 qn7, z66 z66, String str) {
            super(2, qn7);
            this.$preset = mo5;
            this.this$0 = z66;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.$preset, qn7, this.this$0, this.$nextActivePresetId$inlined);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.t.O(this.this$0.j0() - 1);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED}, m = "generateSharingFaceFromPreset")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(z66 z66, qn7 qn7) {
            super(qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$generateSharingFaceFromPreset$response$1", f = "HomeDianaCustomizePresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super iq5<gj4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(z66 z66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super iq5<gj4>> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                uo5 uo5 = this.this$0.D;
                mo5 mo5 = this.this$0.j;
                if (mo5 != null) {
                    String e = mo5.e();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object h = uo5.h(e, this);
                    return h == d ? d : h;
                }
                pq7.i();
                throw null;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$onHomeTabChange$1", f = "HomeDianaCustomizePresenter.kt", l = {616, 494, 503}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $tab;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements Comparator<T> {
            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return mn7.c(Boolean.valueOf(t2.m()), Boolean.valueOf(t.m()));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements Comparator<T> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator b;

            @DexIgnore
            public b(Comparator comparator) {
                this.b = comparator;
            }

            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                int compare = this.b.compare(t, t2);
                return compare != 0 ? compare : mn7.c(t.c(), t2.c());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements Comparator<T> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator b;

            @DexIgnore
            public c(Comparator comparator) {
                this.b = comparator;
            }

            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                int compare = this.b.compare(t, t2);
                return compare != 0 ? compare : mn7.c(t.e(), t2.e());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends ko7 implements vp7<iv7, qn7<? super List<? extends mo5>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(qn7 qn7, i iVar) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(qn7, this.this$0);
                dVar.p$ = (iv7) obj;
                throw null;
                //return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends mo5>> qn7) {
                throw null;
                //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    uo5 uo5 = this.this$0.this$0.D;
                    String J = PortfolioApp.h0.c().J();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object o = uo5.o(J, this);
                    return o == d ? d : o;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(z66 z66, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
            this.$tab = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, this.$tab, qn7);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0070 A[Catch:{ all -> 0x0139 }] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00bd  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 321
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {467, 468, 469, 470, 471, 472}, m = "presetToUIPreset")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(z66 z66, qn7 qn7) {
            super(qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$reloadPresetWatchFace$1", f = "HomeDianaCustomizePresenter.kt", l = {581, 583, 585, 592, 597}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $downloadUrl;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$reloadPresetWatchFace$1$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $config;
            @DexIgnore
            public /* final */ /* synthetic */ dp5 $uiDianaPreset;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ k this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(k kVar, dp5 dp5, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = kVar;
                this.$uiDianaPreset = dp5;
                this.$config = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$uiDianaPreset, this.$config, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.t.W4(this.$uiDianaPreset, this.$config);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("HomeDianaCustomizePresenter", "update: " + this.$uiDianaPreset + " - config: " + this.$config);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(z66 z66, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
            this.$downloadUrl = str;
            this.$id = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, this.$downloadUrl, this.$id, qn7);
            kVar.p$ = (iv7) obj;
            throw null;
            //return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00f2  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0132  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 323
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.k.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$renameCurrentPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {275}, m = "invokeSuspend")
    public static final class l extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<mo5>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mo5 $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(mo5 mo5, qn7 qn7, l lVar) {
                super(2, qn7);
                this.$it = mo5;
                this.this$0 = lVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$it, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<mo5>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                iv7 iv7;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 = this.p$;
                    uo5 uo5 = this.this$0.this$0.D;
                    mo5 mo5 = this.$it;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (uo5.k(mo5, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 = (iv7) this.L$0;
                    el7.b(obj);
                } else if (i == 2) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                uo5 uo52 = this.this$0.this$0.D;
                jo5 c = lo5.c(this.$it, this.this$0.this$0.z);
                this.L$0 = iv7;
                this.label = 2;
                Object q = uo52.q(c, this);
                return q == d ? d : q;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(z66 z66, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            l lVar = new l(this.this$0, this.$presetId, this.$name, qn7);
            lVar.p$ = (iv7) obj;
            throw null;
            //return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((l) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Iterator it = this.this$0.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (ao7.a(pq7.a(((mo5) next).e(), this.$presetId)).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                mo5 mo5 = (mo5) obj2;
                mo5 a2 = mo5 != null ? lo5.a(mo5) : null;
                if (a2 != null) {
                    a2.r(this.$name);
                    a2.s(2);
                    dv7 b = bw7.b();
                    a aVar = new a(a2, null, this);
                    this.L$0 = iv7;
                    this.L$1 = a2;
                    this.L$2 = a2;
                    this.label = 1;
                    g = eu7.g(b, aVar, this);
                    if (g == d) {
                        return d;
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                mo5 mo52 = (mo5) this.L$2;
                mo5 mo53 = (mo5) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) g;
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ mo5 $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super jo5>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = mVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super jo5> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    m mVar = this.this$0;
                    return lo5.c(mVar.$it, mVar.this$0.z);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends ko7 implements vp7<iv7, qn7<? super iq5<DianaWatchFaceUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ jo5 $preset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(m mVar, jo5 jo5, qn7 qn7) {
                super(2, qn7);
                this.this$0 = mVar;
                this.$preset = jo5;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$preset, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<DianaWatchFaceUser>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.this$0.C;
                    String e = ym5.e(24);
                    pq7.b(e, "StringHelper.randomUUID(24)");
                    String c = this.$preset.c();
                    String f = this.this$0.$it.f();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object createWatchFace$default = DianaWatchFaceRepository.createWatchFace$default(dianaWatchFaceRepository, e, c, f, null, this, 8, null);
                    return createWatchFace$default == d ? d : createWatchFace$default;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ iq5 $result;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ DianaWatchFaceUser $localWatchFace;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, DianaWatchFaceUser dianaWatchFaceUser, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = cVar;
                    this.$localWatchFace = dianaWatchFaceUser;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, this.$localWatchFace, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.this$0.t.w();
                        this.this$0.this$0.this$0.t.C0(this.$localWatchFace.getId());
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(c cVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = cVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    String str;
                    Integer code;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.this$0.t.w();
                        y66 y66 = this.this$0.this$0.this$0.t;
                        ServerError c = ((hq5) this.this$0.$result).c();
                        int intValue = (c == null || (code = c.getCode()) == null) ? -1 : code.intValue();
                        ServerError c2 = ((hq5) this.this$0.$result).c();
                        if (c2 == null || (str = c2.getMessage()) == null) {
                            str = "";
                        }
                        y66.E5(intValue, str);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(m mVar, iq5 iq5, qn7 qn7) {
                super(2, qn7);
                this.this$0 = mVar;
                this.$result = iq5;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, this.$result, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x006a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 291
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.m.c.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(mo5 mo5, qn7 qn7, z66 z66) {
            super(2, qn7);
            this.$it = mo5;
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            m mVar = new m(this.$it, qn7, this.this$0);
            mVar.p$ = (iv7) obj;
            throw null;
            //return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((m) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006e  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 1
                r4 = 2
                r2 = 0
                java.lang.Object r5 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0070
                if (r0 == r6) goto L_0x004d
                if (r0 != r4) goto L_0x0045
                java.lang.Object r0 = r7.L$1
                com.fossil.jo5 r0 = (com.fossil.jo5) r0
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r0 = r8
            L_0x001b:
                r5 = r0
                com.fossil.iq5 r5 = (com.fossil.iq5) r5
                boolean r0 = r5 instanceof com.fossil.kq5
                if (r0 == 0) goto L_0x0099
                com.fossil.z66 r0 = r7.this$0
                com.fossil.y66 r0 = com.fossil.z66.V(r0)
                r0.w()
                com.fossil.z66 r0 = r7.this$0
                com.fossil.y66 r1 = com.fossil.z66.V(r0)
                com.fossil.kq5 r5 = (com.fossil.kq5) r5
                java.lang.Object r0 = r5.a()
                if (r0 == 0) goto L_0x0095
                com.portfolio.platform.data.model.watchface.DianaWatchFaceUser r0 = (com.portfolio.platform.data.model.watchface.DianaWatchFaceUser) r0
                java.lang.String r0 = r0.getId()
                r1.C0(r0)
            L_0x0042:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0044:
                return r0
            L_0x0045:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x004d:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r3 = r0
                r1 = r8
            L_0x0056:
                r0 = r1
                com.fossil.jo5 r0 = (com.fossil.jo5) r0
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.z66$m$b r6 = new com.fossil.z66$m$b
                r6.<init>(r7, r0, r2)
                r7.L$0 = r3
                r7.L$1 = r0
                r7.label = r4
                java.lang.Object r0 = com.fossil.eu7.g(r1, r6, r7)
                if (r0 != r5) goto L_0x001b
                r0 = r5
                goto L_0x0044
            L_0x0070:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.z66 r1 = r7.this$0
                com.fossil.y66 r1 = com.fossil.z66.V(r1)
                java.lang.String r3 = ""
                r1.W3(r3)
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.z66$m$a r3 = new com.fossil.z66$m$a
                r3.<init>(r7, r2)
                r7.L$0 = r0
                r7.label = r6
                java.lang.Object r1 = com.fossil.eu7.g(r1, r3, r7)
                if (r1 != r5) goto L_0x00b1
                r0 = r5
                goto L_0x0044
            L_0x0095:
                com.fossil.pq7.i()
                throw r2
            L_0x0099:
                boolean r0 = r5 instanceof com.fossil.hq5
                if (r0 == 0) goto L_0x0042
                com.fossil.z66 r0 = r7.this$0
                com.fossil.iv7 r0 = com.fossil.z66.X(r0)
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.z66$m$c r3 = new com.fossil.z66$m$c
                r3.<init>(r7, r5, r2)
                r5 = r2
                com.fossil.eu7.d(r0, r1, r2, r3, r4, r5)
                goto L_0x0042
            L_0x00b1:
                r3 = r0
                goto L_0x0056
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.m.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements iq4.e<yb7.d, yb7.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z66 f4424a;

        @DexIgnore
        public n(z66 z66) {
            this.f4424a = z66;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(yb7.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f4424a.t.w();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                y66 y66 = this.f4424a.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    y66.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (b == 1941) {
                this.f4424a.t.E5(601, "");
            } else if (b != 1942) {
                this.f4424a.t.u();
            } else {
                this.f4424a.t.E5(500, "");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(yb7.d dVar) {
            pq7.c(dVar, "responseValue");
            this.f4424a.t.w();
            this.f4424a.t.m0(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$shareCurrentWF$1", f = "HomeDianaCustomizePresenter.kt", l = {397, MFNetworkReturnCode.BAD_REQUEST}, m = "invokeSuspend")
    public static final class o extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(z66 z66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            o oVar = new o(this.this$0, qn7);
            oVar.p$ = (iv7) obj;
            throw null;
            //return oVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((o) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.t.W3("");
                mo5 mo5 = this.this$0.j;
                if (mo5 == null) {
                    pq7.i();
                    throw null;
                } else if (mo5.h() == 0) {
                    z66 z66 = this.this$0;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (z66.i0(this) == d) {
                        return d;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().e("HomeDianaCustomizePresenter", "shareCurrentWF - try to sync");
                    z66 z662 = this.this$0;
                    this.L$0 = iv7;
                    this.label = 2;
                    if (z662.q0(this) == d) {
                        return d;
                    }
                }
            } else if (i == 1 || i == 2) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1", f = "HomeDianaCustomizePresenter.kt", l = {116}, m = "invokeSuspend")
    public static final class p extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1", f = "HomeDianaCustomizePresenter.kt", l = {118, 120}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ p this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(p pVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = pVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x008b  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x00bb  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r3 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x008d
                    if (r0 == r3) goto L_0x0051
                    if (r0 != r5) goto L_0x0049
                    java.lang.Object r0 = r6.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r6.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r7)
                    r1 = r7
                    r2 = r0
                L_0x001b:
                    r0 = r1
                    java.util.Collection r0 = (java.util.Collection) r0
                    r2.addAll(r0)
                    com.fossil.z66$p r0 = r6.this$0
                    com.fossil.z66 r0 = r0.this$0
                    java.util.concurrent.CopyOnWriteArrayList r0 = com.fossil.z66.J(r0)
                    r0.clear()
                    com.fossil.z66$p r0 = r6.this$0
                    com.fossil.z66 r0 = r0.this$0
                    java.util.concurrent.CopyOnWriteArrayList r0 = com.fossil.z66.J(r0)
                    com.fossil.z66$p r1 = r6.this$0
                    com.fossil.z66 r1 = r1.this$0
                    com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r1 = com.fossil.z66.L(r1)
                    java.util.List r1 = r1.getAllRealDataRaw()
                    boolean r0 = r0.addAll(r1)
                    java.lang.Boolean r0 = com.fossil.ao7.a(r0)
                L_0x0048:
                    return r0
                L_0x0049:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0051:
                    java.lang.Object r0 = r6.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r6.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r7)
                    r3 = r0
                    r2 = r7
                L_0x005e:
                    r0 = r2
                    java.util.Collection r0 = (java.util.Collection) r0
                    r3.addAll(r0)
                    com.fossil.z66$p r0 = r6.this$0
                    com.fossil.z66 r0 = r0.this$0
                    java.util.ArrayList r0 = com.fossil.z66.A(r0)
                    r0.clear()
                    com.fossil.z66$p r0 = r6.this$0
                    com.fossil.z66 r0 = r0.this$0
                    java.util.ArrayList r0 = com.fossil.z66.A(r0)
                    com.fossil.z66$p r2 = r6.this$0
                    com.fossil.z66 r2 = r2.this$0
                    com.portfolio.platform.data.source.WatchAppRepository r2 = com.fossil.z66.W(r2)
                    r6.L$0 = r1
                    r6.L$1 = r0
                    r6.label = r5
                    java.lang.Object r1 = r2.getAllWatchAppRaw(r6)
                    if (r1 != r4) goto L_0x00bb
                    r0 = r4
                    goto L_0x0048
                L_0x008d:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r1 = r6.p$
                    com.fossil.z66$p r0 = r6.this$0
                    com.fossil.z66 r0 = r0.this$0
                    java.util.ArrayList r0 = com.fossil.z66.z(r0)
                    r0.clear()
                    com.fossil.z66$p r0 = r6.this$0
                    com.fossil.z66 r0 = r0.this$0
                    java.util.ArrayList r0 = com.fossil.z66.z(r0)
                    com.fossil.z66$p r2 = r6.this$0
                    com.fossil.z66 r2 = r2.this$0
                    com.portfolio.platform.data.source.ComplicationRepository r2 = com.fossil.z66.G(r2)
                    r6.L$0 = r1
                    r6.L$1 = r0
                    r6.label = r3
                    java.lang.Object r2 = r2.getAllComplicationRaw(r6)
                    if (r2 != r4) goto L_0x00be
                    r0 = r4
                    goto L_0x0048
                L_0x00bb:
                    r2 = r0
                    goto L_0x001b
                L_0x00be:
                    r3 = r0
                    goto L_0x005e
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.p.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<List<? extends CustomizeRealData>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ p f4425a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$p$b$a$a")
                /* renamed from: com.fossil.z66$p$b$a$a  reason: collision with other inner class name */
                public static final class C0298a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0298a(qn7 qn7, a aVar) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0298a aVar = new C0298a(qn7, this.this$0);
                        aVar.p$ = (iv7) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                        throw null;
                        //return ((C0298a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            UserRepository userRepository = this.this$0.this$0.f4425a.this$0.B;
                            this.L$0 = iv7;
                            this.label = 1;
                            Object currentUser = userRepository.getCurrentUser(this);
                            return currentUser == d ? d : currentUser;
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(List list, qn7 qn7, b bVar) {
                    super(2, qn7);
                    this.$it = list;
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.$it, qn7, this.this$0);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:24:0x00bd A[Catch:{ all -> 0x0123 }] */
                /* JADX WARNING: Removed duplicated region for block: B:30:0x00f7  */
                /* JADX WARNING: Removed duplicated region for block: B:34:0x0118  */
                @Override // com.fossil.zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                    /*
                    // Method dump skipped, instructions count: 296
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.p.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public b(p pVar) {
                this.f4425a = pVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(List<CustomizeRealData> list) {
                if (list != null) {
                    xw7 unused = gu7.d(this.f4425a.this$0.k(), null, null, new a(list, null, this), 3, null);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements ls0<String> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ p f4426a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$3$1", f = "HomeDianaCustomizePresenter.kt", l = {616, 152, 153}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$p$c$a$a")
                /* renamed from: com.fossil.z66$p$c$a$a  reason: collision with other inner class name */
                public static final class C0299a<T> implements ls0<List<? extends mo5>> {

                    @DexIgnore
                    /* renamed from: a  reason: collision with root package name */
                    public /* final */ /* synthetic */ a f4427a;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$p$c$a$a$a")
                    /* renamed from: com.fossil.z66$p$c$a$a$a  reason: collision with other inner class name */
                    public static final class C0300a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                        @DexIgnore
                        public /* final */ /* synthetic */ List $it;
                        @DexIgnore
                        public Object L$0;
                        @DexIgnore
                        public Object L$1;
                        @DexIgnore
                        public Object L$2;
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public iv7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ C0299a this$0;

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$p$c$a$a$a$a")
                        /* renamed from: com.fossil.z66$p$c$a$a$a$a  reason: collision with other inner class name */
                        public static final class C0301a<T> implements Comparator<T> {
                            @DexIgnore
                            @Override // java.util.Comparator
                            public final int compare(T t, T t2) {
                                return mn7.c(Boolean.valueOf(t2.m()), Boolean.valueOf(t.m()));
                            }
                        }

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$p$c$a$a$a$b")
                        /* renamed from: com.fossil.z66$p$c$a$a$a$b */
                        public static final class b<T> implements Comparator<T> {
                            @DexIgnore
                            public /* final */ /* synthetic */ Comparator b;

                            @DexIgnore
                            public b(Comparator comparator) {
                                this.b = comparator;
                            }

                            @DexIgnore
                            @Override // java.util.Comparator
                            public final int compare(T t, T t2) {
                                int compare = this.b.compare(t, t2);
                                return compare != 0 ? compare : mn7.c(t.c(), t2.c());
                            }
                        }

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z66$p$c$a$a$a$c")
                        /* renamed from: com.fossil.z66$p$c$a$a$a$c  reason: collision with other inner class name */
                        public static final class C0302c<T> implements Comparator<T> {
                            @DexIgnore
                            public /* final */ /* synthetic */ Comparator b;

                            @DexIgnore
                            public C0302c(Comparator comparator) {
                                this.b = comparator;
                            }

                            @DexIgnore
                            @Override // java.util.Comparator
                            public final int compare(T t, T t2) {
                                int compare = this.b.compare(t, t2);
                                return compare != 0 ? compare : mn7.c(t.e(), t2.e());
                            }
                        }

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public C0300a(List list, qn7 qn7, C0299a aVar) {
                            super(2, qn7);
                            this.$it = list;
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                            pq7.c(qn7, "completion");
                            C0300a aVar = new C0300a(this.$it, qn7, this.this$0);
                            aVar.p$ = (iv7) obj;
                            throw null;
                            //return aVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.vp7
                        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                            throw null;
                            //return ((C0300a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                        }

                        @DexIgnore
                        /* JADX WARNING: Removed duplicated region for block: B:15:0x0081 A[Catch:{ all -> 0x0185 }] */
                        /* JADX WARNING: Removed duplicated region for block: B:23:0x013d A[Catch:{ all -> 0x0185 }] */
                        /* JADX WARNING: Removed duplicated region for block: B:29:0x017b  */
                        @Override // com.fossil.zn7
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                            /*
                            // Method dump skipped, instructions count: 394
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.p.c.a.C0299a.C0300a.invokeSuspend(java.lang.Object):java.lang.Object");
                        }
                    }

                    @DexIgnore
                    public C0299a(a aVar) {
                        this.f4427a = aVar;
                    }

                    @DexIgnore
                    /* renamed from: a */
                    public final void onChanged(List<mo5> list) {
                        if (list != null) {
                            xw7 unused = gu7.d(this.f4427a.this$0.f4426a.this$0.k(), null, null, new C0300a(list, null, this), 3, null);
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, String str, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = cVar;
                    this.$it = str;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, this.$it, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:27:0x00b6  */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x00e9  */
                /* JADX WARNING: Removed duplicated region for block: B:36:0x0109  */
                @Override // com.fossil.zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                    /*
                    // Method dump skipped, instructions count: 283
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.p.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public c(p pVar) {
                this.f4426a = pVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDianaCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.f4426a.this$0.n);
                if (TextUtils.isEmpty(str) || !FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                    this.f4426a.this$0.t.r(true);
                } else {
                    xw7 unused = gu7.d(this.f4426a.this$0.k(), null, null, new a(this, str, null), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(z66 z66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            p pVar = new p(this.this$0, qn7);
            pVar.p$ = (iv7) obj;
            throw null;
            //return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((p) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.this$0.g.isEmpty() || this.this$0.h.isEmpty()) {
                    FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps");
                    dv7 h = this.this$0.h();
                    a aVar = new a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(h, aVar, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LiveData liveData = this.this$0.f;
            y66 y66 = this.this$0.t;
            if (y66 != null) {
                liveData.h((dz5) y66, new b(this));
                MutableLiveData mutableLiveData = this.this$0.k;
                y66 y662 = this.this$0.t;
                if (y662 != null) {
                    mutableLiveData.h((dz5) y662, new c(this));
                    this.this$0.w.o();
                    wq5.d.g(CommunicateMode.SET_PRESET_APPS_DATA);
                    return tl7.f3441a;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {433}, m = "syncPresetToServer")
    public static final class q extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(z66 z66, qn7 qn7) {
            super(qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$syncPresetToServer$2", f = "HomeDianaCustomizePresenter.kt", l = {436, 443, 445}, m = "invokeSuspend")
    public static final class r extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$syncPresetToServer$2$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ r this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(r rVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = rVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.t.w();
                    this.this$0.this$0.t.g2(null, 404);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(z66 z66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = z66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            r rVar = new r(this.this$0, qn7);
            rVar.p$ = (iv7) obj;
            throw null;
            //return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((r) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0180  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 399
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.r.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public z66(y66 y66, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, jb6 jb6, UserRepository userRepository, zm5 zm5, FileRepository fileRepository, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository2, DianaWatchFaceRepository dianaWatchFaceRepository, PortfolioApp portfolioApp, uo5 uo5, s77 s77, yb7 yb7) {
        pq7.c(y66, "mView");
        pq7.c(watchAppRepository, "mWatchAppRepository");
        pq7.c(complicationRepository, "mComplicationRepository");
        pq7.c(jb6, "mSetWatchAppUseCase");
        pq7.c(userRepository, "userRepository");
        pq7.c(zm5, "customizeRealDataManager");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(customizeRealDataRepository, "mCustomizeRealDataRepository");
        pq7.c(userRepository2, "mUserRepository");
        pq7.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        pq7.c(portfolioApp, "mApp");
        pq7.c(uo5, "dianaPresetRepository");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(yb7, "mSetDianaPresetUseCase");
        this.t = y66;
        this.u = watchAppRepository;
        this.v = complicationRepository;
        this.w = jb6;
        this.x = userRepository;
        this.y = zm5;
        this.z = fileRepository;
        this.A = customizeRealDataRepository;
        this.B = userRepository2;
        this.C = dianaWatchFaceRepository;
        this.D = uo5;
        this.E = s77;
        this.F = yb7;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps first time");
        this.k = portfolioApp.K();
    }

    @DexIgnore
    public final /* synthetic */ Object e0(List<dp5> list, qn7<? super Map<String, List<s87>>> qn7) {
        return eu7.g(bw7.a(), new a(this, list, null), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object f0(com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r8 = this;
            r7 = 0
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.z66.b
            if (r0 == 0) goto L_0x003c
            r0 = r9
            com.fossil.z66$b r0 = (com.fossil.z66.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003c
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0016:
            java.lang.Object r3 = r2.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0076
            if (r0 == r5) goto L_0x004b
            if (r0 != r6) goto L_0x0043
            java.lang.Object r0 = r2.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r2.L$0
            com.fossil.z66 r1 = (com.fossil.z66) r1
            com.fossil.el7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            java.util.Map r0 = (java.util.Map) r0
            com.fossil.y66 r1 = r1.t
            r1.F5(r4, r0)
        L_0x0039:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x003b:
            return r0
        L_0x003c:
            com.fossil.z66$b r0 = new com.fossil.z66$b
            r0.<init>(r8, r9)
            r2 = r0
            goto L_0x0016
        L_0x0043:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004b:
            java.lang.Object r0 = r2.L$0
            com.fossil.z66 r0 = (com.fossil.z66) r0
            com.fossil.el7.b(r3)
            r1 = r3
            r8 = r0
        L_0x0054:
            r0 = r1
            java.util.List r0 = (java.util.List) r0
            boolean r1 = r0.isEmpty()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0039
            com.fossil.dv7 r1 = com.fossil.bw7.a()
            com.fossil.z66$c r3 = new com.fossil.z66$c
            r3.<init>(r8, r0, r7)
            r2.L$0 = r8
            r2.L$1 = r0
            r2.label = r6
            java.lang.Object r2 = com.fossil.eu7.g(r1, r3, r2)
            if (r2 != r4) goto L_0x00ac
            r0 = r4
            goto L_0x003b
        L_0x0076:
            com.fossil.el7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "buildUiPresets, preset = "
            r1.append(r3)
            java.util.concurrent.CopyOnWriteArrayList<com.fossil.mo5> r3 = r8.i
            r1.append(r3)
            java.lang.String r3 = "HomeDianaCustomizePresenter"
            java.lang.String r1 = r1.toString()
            r0.d(r3, r1)
            com.fossil.dv7 r0 = com.fossil.bw7.b()
            com.fossil.z66$d r1 = new com.fossil.z66$d
            r1.<init>(r8, r7)
            r2.L$0 = r8
            r2.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r0, r1, r2)
            if (r1 != r4) goto L_0x0054
            r0 = r4
            goto L_0x003b
        L_0x00ac:
            r4 = r0
            r1 = r8
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.f0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public void g0() {
        this.t.K1();
    }

    @DexIgnore
    public final void h0(String str) {
        T t2;
        T t3;
        String str2;
        List<mo5> e2 = this.e.e();
        if (e2 != null) {
            e2.isEmpty();
        }
        Iterator<T> it = this.i.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.e(), str)) {
                t2 = next;
                break;
            }
        }
        T t4 = t2;
        if (t4 != null) {
            Iterator<T> it2 = this.i.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t3 = null;
                    break;
                }
                T next2 = it2.next();
                if (next2.m()) {
                    t3 = next2;
                    break;
                }
            }
            T t5 = t3;
            if (t5 == null || (str2 = t5.e()) == null) {
                str2 = "";
            }
            this.t.W3("");
            this.F.e(new yb7.c(t4.e()), new e(t5, str2, this, str));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i0(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            boolean r0 = r7 instanceof com.fossil.z66.g
            if (r0 == 0) goto L_0x0072
            r0 = r7
            com.fossil.z66$g r0 = (com.fossil.z66.g) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0072
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0080
            if (r4 != r5) goto L_0x0078
            java.lang.Object r0 = r0.L$0
            com.fossil.z66 r0 = (com.fossil.z66) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x00af
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "shareCurrentWF - success: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            if (r0 == 0) goto L_0x0098
            java.lang.String r1 = "shareableLink"
            com.google.gson.JsonElement r0 = r0.p(r1)
            if (r0 == 0) goto L_0x0098
            java.lang.String r0 = r0.f()
        L_0x0061:
            com.fossil.y66 r1 = r6.t
            r1.w()
            if (r0 == 0) goto L_0x009a
            com.fossil.y66 r1 = r6.t
            r2 = 200(0xc8, float:2.8E-43)
            r1.g2(r0, r2)
        L_0x006f:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0071:
            return r0
        L_0x0072:
            com.fossil.z66$g r0 = new com.fossil.z66$g
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0078:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0080:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.fossil.z66$h r4 = new com.fossil.z66$h
            r4.<init>(r6, r2)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0071
        L_0x0098:
            r0 = r2
            goto L_0x0061
        L_0x009a:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = "shareCurrentWF - sharableLink is null"
            r0.e(r1, r3)
            com.fossil.y66 r0 = r6.t
            r1 = 404(0x194, float:5.66E-43)
            r0.g2(r2, r1)
            goto L_0x006f
        L_0x00af:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x006f
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "shareCurrentWF - failure: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r1.e(r4, r3)
            com.fossil.y66 r1 = r6.t
            r1.w()
            com.fossil.y66 r1 = r6.t
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r0 = r0.a()
            r1.g2(r2, r0)
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.i0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final int j0() {
        return this.l;
    }

    @DexIgnore
    public boolean k0() {
        return this.q;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new p(this, null), 3, null);
    }

    @DexIgnore
    public void l0(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.t.c0(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        try {
            MutableLiveData<String> mutableLiveData = this.k;
            y66 y66 = this.t;
            if (y66 != null) {
                mutableLiveData.n((dz5) y66);
                this.e.n((LifecycleOwner) this.t);
                this.f.n((LifecycleOwner) this.t);
                this.w.r();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "stop fail due to " + e2);
        }
    }

    @DexIgnore
    public void m0(int i2) {
        this.n = i2;
        xw7 unused = gu7.d(k(), null, null, new i(this, i2, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void n(int i2) {
        if (this.i.size() > i2) {
            this.l = i2;
            this.j = this.i.get(i2);
        }
    }

    @DexIgnore
    public void n0() {
        FLogger.INSTANCE.getLocal().e("HomeDianaCustomizePresenter", "onUpdateFwComplete ");
        this.t.M4();
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void o() {
        if (this.i.size() < 2 || this.j == null) {
            this.t.h1();
            return;
        }
        CopyOnWriteArrayList<mo5> copyOnWriteArrayList = this.i;
        ArrayList arrayList = new ArrayList();
        for (T t2 : copyOnWriteArrayList) {
            String e2 = t2.e();
            mo5 mo5 = this.j;
            if (mo5 == null) {
                pq7.i();
                throw null;
            } else if (!pq7.a(e2, mo5.e())) {
                arrayList.add(t2);
            }
        }
        mo5 mo52 = (mo5) arrayList.get(0);
        y66 y66 = this.t;
        mo5 mo53 = this.j;
        if (mo53 != null) {
            boolean m2 = mo53.m();
            mo5 mo54 = this.j;
            if (mo54 != null) {
                y66.r0(m2, mo54.f(), mo52.f(), mo52.e());
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0150  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x02b4  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02c6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o0(com.fossil.mo5 r20, com.fossil.qn7<? super com.fossil.dp5> r21) {
        /*
        // Method dump skipped, instructions count: 758
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.o0(com.fossil.mo5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void p(String str) {
        pq7.c(str, "nextActivePresetId");
        mo5 mo5 = this.j;
        if (mo5 == null) {
            return;
        }
        if (mo5.m()) {
            h0(str);
        } else {
            xw7 unused = gu7.d(k(), null, null, new f(mo5, null, this, str), 3, null);
        }
    }

    @DexIgnore
    public void p0() {
        this.t.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.x66
    public String q() {
        String e2;
        mo5 mo5 = this.j;
        return (mo5 == null || (e2 = mo5.e()) == null) ? "" : e2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q0(com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r7 = this;
            r6 = 0
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.fossil.z66.q
            if (r0 == 0) goto L_0x002a
            r0 = r8
            com.fossil.z66$q r0 = (com.fossil.z66.q) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0038
            if (r3 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.z66 r0 = (com.fossil.z66) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0029:
            return r0
        L_0x002a:
            com.fossil.z66$q r0 = new com.fossil.z66$q
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x0030:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0038:
            com.fossil.el7.b(r2)
            com.fossil.mo5 r2 = r7.j
            if (r2 == 0) goto L_0x0072
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "syncPresetToServer - current preset: "
            r3.append(r4)
            com.fossil.mo5 r4 = r7.j
            r3.append(r4)
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            com.fossil.dv7 r2 = com.fossil.bw7.b()
            com.fossil.z66$r r3 = new com.fossil.z66$r
            r3.<init>(r7, r6)
            r0.L$0 = r7
            r0.label = r5
            java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0029
        L_0x0072:
            com.fossil.y66 r0 = r7.t
            r0.w()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "HomeDianaCustomizePresenter"
            java.lang.String r2 = "syncPresetToServer - current preset is null"
            r0.e(r1, r2)
            com.fossil.y66 r0 = r7.t
            r1 = 503(0x1f7, float:7.05E-43)
            r0.g2(r6, r1)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z66.q0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void r(String str) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "navigate to " + str);
        this.r = str;
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void s(String str, String str2) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDianaCustomizePresenter", "reloadPresetWatchFace - id: " + str + " - downloadUrl: " + str2);
        if (str2 != null) {
            xw7 unused = gu7.d(k(), bw7.b(), null, new k(this, str2, str, null), 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void t(String str, String str2) {
        pq7.c(str, "name");
        pq7.c(str2, "presetId");
        xw7 unused = gu7.d(k(), null, null, new l(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.x66
    public String u() {
        return this.r;
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void v() {
        mo5 mo5 = this.j;
        if (mo5 != null) {
            xw7 unused = gu7.d(k(), null, null, new m(mo5, null, this), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void w() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "setPresetToWatch check permission BLE");
        jn5 jn5 = jn5.b;
        y66 y66 = this.t;
        if (y66 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } else if (jn5.c(jn5, ((dz5) y66).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "setPresetToWatch currentPreset " + this.j);
            mo5 mo5 = this.j;
            if (mo5 != null) {
                y66 y662 = this.t;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886814);
                pq7.b(c2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
                y662.W3(c2);
                this.F.e(new yb7.c(mo5.e()), new n(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void x() {
        if (this.j != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("shareCurrentWF - id: ");
            mo5 mo5 = this.j;
            if (mo5 != null) {
                sb.append(mo5.e());
                local.d("HomeDianaCustomizePresenter", sb.toString());
                xw7 unused = gu7.d(k(), null, null, new o(this, null), 3, null);
                return;
            }
            pq7.i();
            throw null;
        }
        this.t.g2(null, 503);
    }

    @DexIgnore
    @Override // com.fossil.x66
    public void y(boolean z2) {
        this.q = z2;
    }
}
