package com.fossil;

import com.fossil.blesdk.adapter.BluetoothLeAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ L4 b;

    @DexIgnore
    public R7(L4 l4) {
        this.b = l4;
    }

    @DexIgnore
    public final void run() {
        this.b.a(BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED ? R5.g : R5.n);
    }
}
