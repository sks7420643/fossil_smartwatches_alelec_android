package com.fossil;

import android.util.Base64;
import com.fossil.Af1;
import com.fossil.Wb1;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Re1<Model, Data> implements Af1<Model, Data> {
    @DexIgnore
    public /* final */ Ai<Data> a;

    @DexIgnore
    public interface Ai<Data> {
        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Data b(String str) throws IllegalArgumentException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<Data> implements Wb1<Data> {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ Ai<Data> c;
        @DexIgnore
        public Data d;

        @DexIgnore
        public Bi(String str, Ai<Data> ai) {
            this.b = str;
            this.c = ai;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
            try {
                this.c.a(this.d);
            } catch (IOException e) {
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super Data> ai) {
            try {
                Data b2 = this.c.b(this.b);
                this.d = b2;
                ai.e(b2);
            } catch (IllegalArgumentException e) {
                ai.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<Data> getDataClass() {
            return this.c.getDataClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<Model> implements Bf1<Model, InputStream> {
        @DexIgnore
        public /* final */ Ai<InputStream> a; // = new Aii(this);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Ai<InputStream> {
            @DexIgnore
            public Aii(Ci ci) {
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Re1.Ai
            public /* bridge */ /* synthetic */ void a(InputStream inputStream) throws IOException {
                c(inputStream);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Re1.Ai
            public /* bridge */ /* synthetic */ InputStream b(String str) throws IllegalArgumentException {
                return d(str);
            }

            @DexIgnore
            public void c(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            @DexIgnore
            public InputStream d(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            @DexIgnore
            @Override // com.fossil.Re1.Ai
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Model, InputStream> b(Ef1 ef1) {
            return new Re1(this.a);
        }
    }

    @DexIgnore
    public Re1(Ai<Data> ai) {
        this.a = ai;
    }

    @DexIgnore
    @Override // com.fossil.Af1
    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }

    @DexIgnore
    @Override // com.fossil.Af1
    public Af1.Ai<Data> b(Model model, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(model), new Bi(model.toString(), this.a));
    }
}
