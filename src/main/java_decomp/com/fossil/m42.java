package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M42 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<M42> CREATOR; // = new P42();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public M42(int i, int i2, Bundle bundle) {
        this.b = i;
        this.c = i2;
        this.d = bundle;
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.n(parcel, 2, c());
        Bd2.e(parcel, 3, this.d, false);
        Bd2.b(parcel, a2);
    }
}
