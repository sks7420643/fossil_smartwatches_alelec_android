package com.fossil;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J extends Qq7 implements Hg6<Nr, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public J(BluetoothDevice bluetoothDevice) {
        super(1);
        this.b = bluetoothDevice;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Nr nr) {
        BluetoothLeAdapter bluetoothLeAdapter = BluetoothLeAdapter.k;
        BluetoothLeAdapter.d.remove(this.b.getAddress());
        return Cd6.a;
    }
}
