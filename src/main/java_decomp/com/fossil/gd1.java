package com.fossil;

import com.fossil.Vc1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gd1<Data, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ Mn0<List<Throwable>> a;
    @DexIgnore
    public /* final */ List<? extends Vc1<Data, ResourceType, Transcode>> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Gd1(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<Vc1<Data, ResourceType, Transcode>> list, Mn0<List<Throwable>> mn0) {
        this.a = mn0;
        Ik1.c(list);
        this.b = list;
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public Id1<Transcode> a(Xb1<Data> xb1, Ob1 ob1, int i, int i2, Vc1.Ai<ResourceType> ai) throws Dd1 {
        List<Throwable> b2 = this.a.b();
        Ik1.d(b2);
        List<Throwable> list = b2;
        try {
            return b(xb1, ob1, i, i2, ai, list);
        } finally {
            this.a.a(list);
        }
    }

    @DexIgnore
    public final Id1<Transcode> b(Xb1<Data> xb1, Ob1 ob1, int i, int i2, Vc1.Ai<ResourceType> ai, List<Throwable> list) throws Dd1 {
        Id1<Transcode> id1;
        int size = this.b.size();
        Id1<Transcode> id12 = null;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                id1 = id12;
                break;
            }
            try {
                id1 = ((Vc1) this.b.get(i3)).a(xb1, i, i2, ob1, ai);
            } catch (Dd1 e) {
                list.add(e);
                id1 = id12;
            }
            if (id1 != null) {
                break;
            }
            i3++;
            id12 = id1;
        }
        if (id1 != null) {
            return id1;
        }
        throw new Dd1(this.c, new ArrayList(list));
    }

    @DexIgnore
    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }
}
