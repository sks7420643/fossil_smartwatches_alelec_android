package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ed3 extends Ds2 implements Dd3 {
    @DexIgnore
    public Ed3() {
        super("com.google.android.gms.maps.internal.ISnapshotReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.Ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            onSnapshotReady((Bitmap) Es2.b(parcel, Bitmap.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            q1(Rg2.Ai.e(parcel.readStrongBinder()));
        }
        parcel2.writeNoException();
        return true;
    }
}
