package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Ld0 extends Enum<Ld0> {
    @DexIgnore
    public static /* final */ Ld0 b;
    @DexIgnore
    public static /* final */ Ld0 c;
    @DexIgnore
    public static /* final */ Ld0 d;
    @DexIgnore
    public static /* final */ Ld0 e;
    @DexIgnore
    public static /* final */ Ld0 f;
    @DexIgnore
    public static /* final */ /* synthetic */ Ld0[] g;

    /*
    static {
        Ld0 ld0 = new Ld0("MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE", 0);
        b = ld0;
        Ld0 ld02 = new Ld0("SDK_LOG_PREFERENCE", 1);
        c = ld02;
        Ld0 ld03 = new Ld0("MINUTE_DATA_REFERENCE", 2);
        d = ld03;
        Ld0 ld04 = new Ld0("HARDWARE_LOG_REFERENCE", 3);
        e = ld04;
        Ld0 ld05 = new Ld0("GPS_REFERENCE", 4);
        f = ld05;
        g = new Ld0[]{ld0, ld02, ld03, ld04, ld05, new Ld0("TEXT_ENCRYPTION_PREFERENCE", 5)};
    }
    */

    @DexIgnore
    public Ld0(String str, int i) {
    }

    @DexIgnore
    public static Ld0 valueOf(String str) {
        return (Ld0) Enum.valueOf(Ld0.class, str);
    }

    @DexIgnore
    public static Ld0[] values() {
        return (Ld0[]) g.clone();
    }
}
