package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I9 extends Vx1<byte[], byte[]> {
    @DexIgnore
    public static /* final */ Qx1<byte[]>[] b; // = {new C9(), new F9()};
    @DexIgnore
    public static /* final */ Rx1<byte[]>[] c; // = new Rx1[0];
    @DexIgnore
    public static /* final */ I9 d; // = new I9();

    @DexIgnore
    @Override // com.fossil.Vx1
    public Qx1<byte[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.Vx1
    public Rx1<byte[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] h(byte[] bArr) {
        return bArr;
    }
}
