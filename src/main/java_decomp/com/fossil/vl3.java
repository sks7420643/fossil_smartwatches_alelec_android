package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vl3 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ Yq3 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public Vl3(Yq3 yq3) {
        Rc2.k(yq3);
        this.a = yq3;
    }

    @DexIgnore
    public final void b() {
        this.a.b0();
        this.a.c().h();
        if (!this.b) {
            this.a.e().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.c = this.a.S().x();
            this.a.d().N().b("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.c));
            this.b = true;
        }
    }

    @DexIgnore
    public final void c() {
        this.a.b0();
        this.a.c().h();
        this.a.c().h();
        if (this.b) {
            this.a.d().N().a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.a.e().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.a.d().F().b("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        this.a.b0();
        String action = intent.getAction();
        this.a.d().N().b("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean x = this.a.S().x();
            if (this.c != x) {
                this.c = x;
                this.a.c().y(new Ul3(this, x));
                return;
            }
            return;
        }
        this.a.d().I().b("NetworkBroadcastReceiver received unknown action", action);
    }
}
