package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jv3 extends Sb2 implements Wu3 {
    @DexIgnore
    public Jv3(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    @DexIgnore
    public final String e() {
        return c("asset_key");
    }

    @DexIgnore
    @Override // com.fossil.Wu3
    public final String getId() {
        return c("asset_id");
    }
}
