package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W11 {
    @DexIgnore
    public static /* final */ String a; // = X01.f("Alarms");

    @DexIgnore
    public static void a(Context context, S11 s11, String str) {
        G31 g = s11.p().g();
        F31 b = g.b(str);
        if (b != null) {
            b(context, str, b.b);
            X01.c().a(a, String.format("Removing SystemIdInfo for workSpecId (%s)", str), new Throwable[0]);
            g.c(str);
        }
    }

    @DexIgnore
    public static void b(Context context, String str, int i) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, X11.b(context, str), 536870912);
        if (service != null && alarmManager != null) {
            X01.c().a(a, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", str, Integer.valueOf(i)), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }

    @DexIgnore
    public static void c(Context context, S11 s11, String str, long j) {
        WorkDatabase p = s11.p();
        G31 g = p.g();
        F31 b = g.b(str);
        if (b != null) {
            b(context, str, b.b);
            d(context, str, b.b, j);
            return;
        }
        int b2 = new X31(p).b();
        g.a(new F31(str, b2));
        d(context, str, b2, j);
    }

    @DexIgnore
    public static void d(Context context, String str, int i, long j) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, X11.b(context, str), 134217728);
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, j, service);
        } else {
            alarmManager.set(0, j, service);
        }
    }
}
