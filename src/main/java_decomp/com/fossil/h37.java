package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H37 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ H37 b; // = new H37();

    /*
    static {
        String simpleName = H37.class.getSimpleName();
        Wg6.b(simpleName, "BackendURLUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return b(PortfolioApp.get.instance(), i);
    }

    @DexIgnore
    public final String b(PortfolioApp portfolioApp, int i) {
        Wg6.c(portfolioApp, "app");
        int u = portfolioApp.k0().u();
        boolean z = false;
        if (u == 0 ? !Vt7.j("release", "release", true) : u == 1) {
            z = true;
        }
        FLogger.INSTANCE.getLocal().d(a, "getBackendUrl - type=" + i + ", isUsingStagingUrl= " + z);
        switch (i) {
            case 0:
                return z ? Rq4.G.l() : Rq4.G.i();
            case 1:
                return z ? Rq4.G.p() : Rq4.G.o();
            case 2:
                return z ? Rq4.G.u() : Rq4.G.t();
            case 3:
            default:
                return "";
            case 4:
                return z ? Rq4.G.g() : Rq4.G.f();
            case 5:
                return z ? Rq4.G.c() : Rq4.G.b();
            case 6:
                return z ? e(portfolioApp, Rq4.G.E()) : e(portfolioApp, Rq4.G.D());
            case 7:
                return z ? Rq4.G.y() : Rq4.G.x();
            case 8:
                return z ? Rq4.G.A() : Rq4.G.z();
        }
    }

    @DexIgnore
    public final String c(PortfolioApp portfolioApp, int i, String str) {
        Wg6.c(portfolioApp, "app");
        Wg6.c(str, "version");
        int u = portfolioApp.k0().u();
        boolean z = false;
        if (u == 0 ? !Vt7.j("release", "release", true) : u == 1) {
            z = true;
        }
        if (i != 0) {
            return b(portfolioApp, i);
        }
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != 49) {
                if (hashCode == 50 && str.equals("2")) {
                    return Rq4.G.m();
                }
            } else if (str.equals("1")) {
                return Rq4.G.l();
            }
            return Rq4.G.n();
        }
        int hashCode2 = str.hashCode();
        if (hashCode2 != 49) {
            if (hashCode2 == 50 && str.equals("2")) {
                return Rq4.G.j();
            }
        } else if (str.equals("1")) {
            return Rq4.G.i();
        }
        return Rq4.G.k();
    }

    @DexIgnore
    public final String d() {
        return PortfolioApp.get.instance().z0() ? Rq4.G.r() : Rq4.G.s();
    }

    @DexIgnore
    public final String e(PortfolioApp portfolioApp, String str) {
        String P = portfolioApp.P();
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        String language = locale.getLanguage();
        if (Wg6.a(language, "zh")) {
            Hr7 hr7 = Hr7.a;
            Locale locale2 = Locale.getDefault();
            Wg6.b(locale2, "Locale.getDefault()");
            language = String.format("%s_%s", Arrays.copyOf(new Object[]{language, locale2.getCountry()}, 2));
            Wg6.b(language, "java.lang.String.format(format, *args)");
        }
        Hr7 hr72 = Hr7.a;
        String format = String.format(str + "/%s?locale=%s", Arrays.copyOf(new Object[]{P, language}, 2));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }
}
