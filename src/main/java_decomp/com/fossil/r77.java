package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f3088a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote", f = "WFAssetRemote.kt", l = {16}, m = "fetchBackgroundTemplates")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ r77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(r77 r77, qn7 qn7) {
            super(qn7);
            this.this$0 = r77;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote$fetchBackgroundTemplates$response$1", f = "WFAssetRemote.kt", l = {16}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<ApiResponse<p77>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $sortCondition;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ r77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(r77 r77, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = r77;
            this.$sortCondition = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$sortCondition, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<p77>>> qn7) {
            throw null;
            //return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3088a;
                String str = this.$sortCondition;
                this.label = 1;
                Object fetchWFBackgroundTemplates = apiServiceV2.fetchWFBackgroundTemplates(str, 100, this);
                return fetchWFBackgroundTemplates == d ? d : fetchWFBackgroundTemplates;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote", f = "WFAssetRemote.kt", l = {41}, m = "fetchRings")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ r77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(r77 r77, qn7 qn7) {
            super(qn7);
            this.this$0 = r77;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote$fetchRings$response$1", f = "WFAssetRemote.kt", l = {41}, m = "invokeSuspend")
    public static final class d extends ko7 implements rp7<qn7<? super q88<ApiResponse<p77>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $sortQuery;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ r77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(r77 r77, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = r77;
            this.$sortQuery = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.this$0, this.$sortQuery, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<p77>>> qn7) {
            throw null;
            //return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3088a;
                String str = this.$sortQuery;
                this.label = 1;
                Object wFRings = apiServiceV2.getWFRings(str, 100, this);
                return wFRings == d ? d : wFRings;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote", f = "WFAssetRemote.kt", l = {28}, m = "fetchTickers")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ r77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(r77 r77, qn7 qn7) {
            super(qn7);
            this.this$0 = r77;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote$fetchTickers$response$1", f = "WFAssetRemote.kt", l = {28}, m = "invokeSuspend")
    public static final class f extends ko7 implements rp7<qn7<? super q88<ApiResponse<p77>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ r77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(r77 r77, qn7 qn7) {
            super(1, qn7);
            this.this$0 = r77;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new f(this.this$0, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<p77>>> qn7) {
            throw null;
            //return ((f) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3088a;
                this.label = 1;
                Object fetchWFTicker = apiServiceV2.fetchWFTicker(100, this);
                return fetchWFTicker == d ? d : fetchWFTicker;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public r77(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f3088a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.p77>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.r77.a
            if (r0 == 0) goto L_0x0044
            r0 = r9
            com.fossil.r77$a r0 = (com.fossil.r77.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.r77 r0 = (com.fossil.r77) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002d:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x006d
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.fossil.r77$a r0 = new com.fossil.r77$a
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.el7.b(r2)
            com.fossil.r77$b r0 = new com.fossil.r77$b
            java.lang.String r2 = "-metadata.priority"
            r0.<init>(r8, r2, r4)
            r1.L$0 = r8
            java.lang.String r2 = "-metadata.priority"
            r1.L$1 = r2
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x006d:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008a
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x008a:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r77.b(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.p77>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.r77.c
            if (r0 == 0) goto L_0x0044
            r0 = r9
            com.fossil.r77$c r0 = (com.fossil.r77.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.r77 r0 = (com.fossil.r77) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002d:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x006d
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.fossil.r77$c r0 = new com.fossil.r77$c
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.el7.b(r2)
            com.fossil.r77$d r0 = new com.fossil.r77$d
            java.lang.String r2 = "-metadata.priority"
            r0.<init>(r8, r2, r4)
            r1.L$0 = r8
            java.lang.String r2 = "-metadata.priority"
            r1.L$1 = r2
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x006d:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008a
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x008a:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r77.c(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.p77>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.r77.e
            if (r0 == 0) goto L_0x003f
            r0 = r9
            com.fossil.r77$e r0 = (com.fossil.r77.e) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x004d
            if (r3 != r5) goto L_0x0045
            java.lang.Object r0 = r0.L$0
            com.fossil.r77 r0 = (com.fossil.r77) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0061
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x003e:
            return r0
        L_0x003f:
            com.fossil.r77$e r0 = new com.fossil.r77$e
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0045:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004d:
            com.fossil.el7.b(r1)
            com.fossil.r77$f r1 = new com.fossil.r77$f
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x003e
        L_0x0061:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x007e
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x007e:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r77.d(com.fossil.qn7):java.lang.Object");
    }
}
