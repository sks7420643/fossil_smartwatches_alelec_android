package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L63 implements Xw2<O63> {
    @DexIgnore
    public static L63 c; // = new L63();
    @DexIgnore
    public /* final */ Xw2<O63> b;

    @DexIgnore
    public L63() {
        this(Ww2.b(new N63()));
    }

    @DexIgnore
    public L63(Xw2<O63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((O63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((O63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ O63 zza() {
        return this.b.zza();
    }
}
