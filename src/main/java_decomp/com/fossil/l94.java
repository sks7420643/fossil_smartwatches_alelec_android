package com.fossil;

import com.fossil.Ta4;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface L94 {
    @DexIgnore
    String a();

    @DexIgnore
    InputStream b();

    @DexIgnore
    Ta4.Ci.Bii c();
}
