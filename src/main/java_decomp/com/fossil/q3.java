package com.fossil;

import android.os.Build;
import com.mapped.Kc6;
import com.mapped.Na0;
import com.mapped.Qb0;
import com.mapped.Rc6;
import com.mapped.Ta0;
import com.mapped.Wa0;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q3 {
    @DexIgnore
    public static /* final */ Ry1 a; // = new Ry1(4, 0);
    @DexIgnore
    public static /* final */ Yp[] b; // = {Yp.c, Yp.A0, Yp.e, Yp.h, Yp.l, Yp.L, Yp.n, Yp.f, Yp.o0, Yp.n0, Yp.s0, Yp.t0, Yp.u0, Yp.v0, Yp.w0, Yp.r, Yp.t, Yp.s, Yp.u};
    @DexIgnore
    public static /* final */ Type[] c; // = {Br1.class, Er1.class};
    @DexIgnore
    public static /* final */ Ve[] d; // = {new Ve(12, 12, 30, 600), new Ve(18, 18, 19, 600), new Ve(24, 24, 14, 600), new Ve(48, 48, 6, 600), new Ve(72, 72, 4, 600)};
    @DexIgnore
    public static /* final */ Ve[] e; // = {new Ve(12, 12, 45, 600), new Ve(24, 24, 22, 600), new Ve(36, 36, 15, 600), new Ve(104, 112, 4, 600)};
    @DexIgnore
    public static /* final */ Q3 f; // = new Q3();

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT < 28;
    }

    @DexIgnore
    public final boolean b(Zk1 zk1, Lp lp) {
        if (f.f(zk1)) {
            return Em7.B(b, lp.y);
        }
        return true;
    }

    @DexIgnore
    public final boolean c(boolean z) {
        return !z;
    }

    @DexIgnore
    public final Ve[] d(Zk1 zk1) {
        switch (P3.a[zk1.getDeviceType().ordinal()]) {
            case 1:
            case 2:
                return d;
            case 3:
            case 4:
            case 5:
            case 6:
                return f(zk1) ? new Ve[0] : e;
            case 7:
            case 8:
            case 9:
                return new Ve[0];
            default:
                throw new Kc6();
        }
    }

    @DexIgnore
    public final long e(boolean z) {
        return z ? 0 : 30000;
    }

    @DexIgnore
    public final boolean f(Zk1 zk1) {
        if (zk1.getDeviceType().b()) {
            Ry1 ry1 = zk1.h().get(Short.valueOf(Ob.d.b));
            if (ry1 == null) {
                ry1 = Hd0.y.f();
            }
            if (ry1.compareTo(Hd0.y.x()) < 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Type[] g() {
        Object[] array = Hm7.c(Bs1.class, Or1.class, Tr1.class, Vr1.class, Na0.class, Xr1.class, Ds1.class, Ar1.class, Br1.class, Cr1.class, Er1.class, Gs1.class, Gr1.class, Hr1.class, Jr1.class, Mr1.class, Nr1.class, Hs1.class, Qr1.class, Rr1.class, Js1.class, Sr1.class, Ps1.class, Ts1.class, Ls1.class, Ta0.class, Dr1.class, Wr1.class, Zr1.class, As1.class, Is1.class, Cs1.class, Es1.class, Qs1.class, Rs1.class, Ns1.class, Kr1.class, Os1.class, Pr1.class, Vq1.class, Ur1.class, Wq1.class, Qb0.class, Yr1.class, Ms1.class, Fs1.class, Fr1.class, Wa0.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] h() {
        Object[] array = Hm7.c(Bs1.class, Or1.class, Tr1.class, Vr1.class, Na0.class, Xr1.class, Ds1.class, Ar1.class, Br1.class, Cr1.class, Er1.class, Gs1.class, Gr1.class, Hr1.class, Jr1.class, Mr1.class, Nr1.class, Hs1.class, Qr1.class, Rr1.class, Js1.class, Sr1.class, Ps1.class, Ts1.class, Ls1.class, Ta0.class, Dr1.class, Wr1.class, Zr1.class, As1.class, Is1.class, Cs1.class, Es1.class, Qs1.class, Rs1.class, Ns1.class, Kr1.class, Os1.class, Pr1.class, Vq1.class, Ur1.class, Wq1.class, Qb0.class, Yr1.class, Fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Ry1 i() {
        return a;
    }

    @DexIgnore
    public final Type[] j() {
        Object[] array = Hm7.c(Or1.class, Tr1.class, Vr1.class, Na0.class, Xr1.class, Ar1.class, Br1.class, Cr1.class, Er1.class, Gr1.class, Hr1.class, Jr1.class, Nr1.class, Od.class, Pd.class, Qd.class, Ir1.class, Yq1.class, Qr1.class, Rr1.class, Sr1.class, As1.class, Is1.class, Cs1.class, Es1.class, Rs1.class, Wr1.class, Os1.class, Vq1.class, Ur1.class, Wq1.class, Yr1.class, Fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] k() {
        Object[] array = Hm7.c(Or1.class, Tr1.class, Vr1.class, Na0.class, Xr1.class, Ar1.class, Br1.class, Cr1.class, Er1.class, Gr1.class, Hr1.class, Jr1.class, Nr1.class, Od.class, Pd.class, Qd.class, Ir1.class, Yq1.class, Qr1.class, Rr1.class, Sr1.class, As1.class, Is1.class, Cs1.class, Es1.class, Rs1.class, Wr1.class, Os1.class, Vq1.class, Ur1.class, Wq1.class, Yr1.class, Fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] l() {
        Object[] array = Hm7.c(Or1.class, Tr1.class, Vr1.class, Na0.class, Xr1.class, Ar1.class, Br1.class, Cr1.class, Er1.class, Hr1.class, Jr1.class, Nr1.class, Od.class, Pd.class, Qd.class, Ir1.class, Yq1.class, Qr1.class, Rr1.class, Sr1.class, Is1.class, Cs1.class, Es1.class, Rs1.class, Wr1.class, Os1.class, Vq1.class, Ur1.class, Wq1.class, Yr1.class, Fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] m() {
        return c;
    }
}
