package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Wn3 implements Runnable {
    @DexIgnore
    public /* final */ Un3 b;

    @DexIgnore
    public Wn3(Un3 un3) {
        this.b = un3;
    }

    @DexIgnore
    public final void run() {
        Un3 un3 = this.b;
        un3.h();
        if (un3.l().x.b()) {
            un3.d().M().a("Deferred Deep Link already retrieved. Not fetching again.");
            return;
        }
        long a2 = un3.l().y.a();
        un3.l().y.b(1 + a2);
        if (a2 >= 5) {
            un3.d().I().a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
            un3.l().x.a(true);
            return;
        }
        un3.a.u();
    }
}
