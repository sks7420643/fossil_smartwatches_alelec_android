package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.mapped.Z80;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Z80[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Yt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yt1 createFromParcel(Parcel parcel) {
            return new Yt1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yt1[] newArray(int i) {
            return new Yt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Yt1(Parcel parcel, Qg6 qg6) {
        super((X90) parcel.readParcelable(Rq1.class.getClassLoader()), (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()));
        Object[] createTypedArray = parcel.createTypedArray(Z80.CREATOR);
        if (createTypedArray != null) {
            this.d = (Z80[]) createTypedArray;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Yt1(Nt1 nt1) throws IllegalArgumentException {
        this(null, nt1, new Z80[0]);
    }

    @DexIgnore
    public Yt1(Rq1 rq1, Nt1 nt1) throws IllegalArgumentException {
        this(rq1, nt1, new Z80[0]);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Yt1(Rq1 rq1, Nt1 nt1, Z80[] z80Arr) throws IllegalArgumentException {
        super(rq1, nt1);
        boolean z = true;
        this.d = z80Arr;
        if (z80Arr.length < 1 && getDeviceMessage() == null) {
            z = false;
        }
        if (!z) {
            throw new IllegalArgumentException("weatherInfoArray must have at least 1 elements.".toString());
        }
    }

    @DexIgnore
    public Yt1(Rq1 rq1, Z80[] z80Arr) throws IllegalArgumentException {
        this(rq1, null, z80Arr);
    }

    @DexIgnore
    public Yt1(Z80[] z80Arr) throws IllegalArgumentException {
        this(null, null, z80Arr);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return G80.k(super.a(), Jd0.f2, Px1.a(this.d));
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (!(this.d.length == 0)) {
                JSONArray jSONArray = new JSONArray();
                int min = Math.min(this.d.length, 3);
                for (int i = 0; i < min; i++) {
                    jSONArray.put(this.d[i].a());
                }
                jSONObject.put("weatherApp._.config.locations", jSONArray);
            } else {
                JSONObject jSONObject2 = new JSONObject();
                Nt1 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
                jSONObject.put("weatherApp._.config.locations", jSONObject2);
            }
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        Wg6.b(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Yt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !Arrays.equals(this.d, ((Yt1) obj).d);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherWatchAppData");
    }

    @DexIgnore
    public final Z80[] getWeatherInfoArray() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        return (super.hashCode() * 31) + Arrays.hashCode(this.d);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.d, i);
        }
    }
}
