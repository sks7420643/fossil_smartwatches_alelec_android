package com.fossil;

import com.mapped.Fu3;
import com.mapped.Ku3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C97 {
    @DexIgnore
    public static final Ku3 a(List<F97> list) {
        Wg6.c(list, "$this$toPhotosJsonObject");
        Ku3 ku3 = new Ku3();
        Fu3 fu3 = new Fu3();
        for (T t : list) {
            Ku3 ku32 = new Ku3();
            ku32.n("id", t.a());
            ku32.n("image", t.b());
            fu3.k(ku32);
        }
        ku3.k(CloudLogWriter.ITEMS_PARAM, fu3);
        return ku3;
    }
}
