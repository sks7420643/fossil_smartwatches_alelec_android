package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import androidx.lifecycle.Lifecycle;
import coil.lifecycle.LifecycleCoroutineDispatcher;
import com.fossil.H81;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G71 {
    @DexIgnore
    public static /* final */ Bitmap.Config[] b; // = (Build.VERSION.SDK_INT >= 26 ? new Bitmap.Config[]{Bitmap.Config.ARGB_8888, Bitmap.Config.RGBA_F16} : new Bitmap.Config[]{Bitmap.Config.ARGB_8888});
    @DexIgnore
    public /* final */ X61 a; // = X61.a.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Ai c; // = new Ai(L61.b, Bw7.c().S());
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ Lifecycle a;
        @DexIgnore
        public /* final */ Dv7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final Ai a() {
                return Ai.c;
            }
        }

        @DexIgnore
        public Ai(Lifecycle lifecycle, Dv7 dv7) {
            Wg6.c(lifecycle, "lifecycle");
            Wg6.c(dv7, "mainDispatcher");
            this.a = lifecycle;
            this.b = dv7;
        }

        @DexIgnore
        public final Lifecycle b() {
            return this.a;
        }

        @DexIgnore
        public final Dv7 c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Lifecycle lifecycle = this.a;
            int hashCode = lifecycle != null ? lifecycle.hashCode() : 0;
            Dv7 dv7 = this.b;
            if (dv7 != null) {
                i = dv7.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "LifecycleInfo(lifecycle=" + this.a + ", mainDispatcher=" + this.b + ")";
        }
    }

    @DexIgnore
    public final boolean a(X71 x71) {
        boolean z;
        View view = null;
        Wg6.c(x71, "request");
        int i = H71.a[x71.r().ordinal()];
        if (i == 1) {
            z = false;
        } else if (i == 2) {
            z = true;
        } else if (i == 3) {
            J81 u = x71.u();
            if (!(u instanceof K81)) {
                u = null;
            }
            K81 k81 = (K81) u;
            if (k81 != null) {
                view = k81.getView();
            }
            if (view instanceof ImageView) {
                return true;
            }
            return x71.t() == null && !(x71.u() instanceof K81);
        } else {
            throw new Kc6();
        }
        return z;
    }

    @DexIgnore
    public final Lifecycle b(T71 t71) {
        if (t71.A() != null) {
            return t71.A();
        }
        if (!(t71.u() instanceof K81)) {
            return T81.b(t71.x());
        }
        Context context = ((K81) t71.u()).getView().getContext();
        Wg6.b(context, "target.view.context");
        return T81.b(context);
    }

    @DexIgnore
    public final boolean c(X71 x71, Bitmap.Config config) {
        Wg6.c(x71, "request");
        Wg6.c(config, "requestedConfig");
        if (!W81.m(config)) {
            return true;
        }
        if (!x71.b()) {
            return false;
        }
        J81 u = x71.u();
        if (u instanceof K81) {
            View view = ((K81) u).getView();
            if (view.isAttachedToWindow() && !view.isHardwareAccelerated()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean d(X71 x71, F81 f81) {
        return c(x71, x71.d()) && this.a.a(f81);
    }

    @DexIgnore
    public final boolean e(X71 x71) {
        return x71.v().isEmpty() || Em7.B(b, x71.d());
    }

    @DexIgnore
    public final Ai f(X71 x71) {
        Wg6.c(x71, "request");
        if (x71 instanceof T71) {
            Lifecycle b2 = b((T71) x71);
            return b2 != null ? new Ai(b2, LifecycleCoroutineDispatcher.f.a(Bw7.c().S(), b2)) : Ai.d.a();
        }
        throw new Kc6();
    }

    @DexIgnore
    public final X51 g(X71 x71, F81 f81, E81 e81, boolean z) {
        boolean z2 = true;
        Wg6.c(x71, "request");
        Wg6.c(f81, "size");
        Wg6.c(e81, "scale");
        Bitmap.Config d = e(x71) && d(x71, f81) ? x71.d() : Bitmap.Config.ARGB_8888;
        S71 o = z ? x71.o() : S71.DISABLED;
        if (!x71.c() || !x71.v().isEmpty() || d == Bitmap.Config.ALPHA_8) {
            z2 = false;
        }
        return new X51(d, x71.e(), e81, a(x71), z2, x71.k(), x71.p(), o, x71.g());
    }

    @DexIgnore
    public final E81 h(X71 x71, G81 g81) {
        Wg6.c(x71, "request");
        Wg6.c(g81, "sizeResolver");
        E81 s = x71.s();
        if (s != null) {
            return s;
        }
        if (g81 instanceof H81) {
            View view = ((H81) g81).getView();
            if (view instanceof ImageView) {
                return W81.j((ImageView) view);
            }
        }
        J81 u = x71.u();
        if (u instanceof K81) {
            View view2 = ((K81) u).getView();
            if (view2 instanceof ImageView) {
                return W81.j((ImageView) view2);
            }
        }
        return E81.FILL;
    }

    @DexIgnore
    public final G81 i(X71 x71, Context context) {
        Wg6.c(x71, "request");
        Wg6.c(context, "context");
        G81 t = x71.t();
        J81 u = x71.u();
        return t != null ? t : u instanceof K81 ? H81.Ai.b(H81.a, ((K81) u).getView(), false, 2, null) : new B81(context);
    }
}
