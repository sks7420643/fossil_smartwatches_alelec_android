package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zd3 {
    @DexIgnore
    public /* final */ Rg2 a;

    @DexIgnore
    public Zd3(Rg2 rg2) {
        Rc2.k(rg2);
        this.a = rg2;
    }

    @DexIgnore
    public final Rg2 a() {
        return this.a;
    }
}
