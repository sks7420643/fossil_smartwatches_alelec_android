package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oq1 extends X90 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ju1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Oq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Oq1 createFromParcel(Parcel parcel) {
            return new Oq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Oq1[] newArray(int i) {
            return new Oq1[i];
        }
    }

    @DexIgnore
    public Oq1(byte b, int i, Ju1 ju1) {
        super(E90.RING_PHONE, b, i);
        this.e = ju1;
    }

    @DexIgnore
    public /* synthetic */ Oq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = Ju1.values()[parcel.readInt()];
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Oq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.e == ((Oq1) obj).e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.RingPhoneRequest");
    }

    @DexIgnore
    public final Ju1 getAction() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }
}
