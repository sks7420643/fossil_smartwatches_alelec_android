package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gn extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Rq b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Gn(Rq rq) {
        super(1);
        this.b = rq;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        Rq rq = this.b;
        long j = rq.F;
        long j2 = rq.G;
        long j3 = j + j2;
        rq.H = j3;
        if (j3 != 0) {
            float f = (float) j;
            float f2 = (float) j3;
            rq.I = f / f2;
            rq.J = ((float) j2) / f2;
        }
        this.b.J();
        return Cd6.a;
    }
}
