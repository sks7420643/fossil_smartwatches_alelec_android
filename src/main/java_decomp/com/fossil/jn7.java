package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jn7 implements Iterator<Ql7>, Jr7 {
    @DexIgnore
    public final Ql7 a() {
        return Ql7.a(b());
    }

    @DexIgnore
    public abstract short b();

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Ql7 next() {
        return a();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
