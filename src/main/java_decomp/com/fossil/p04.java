package com.fossil;

import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class P04 extends S04 {
    @DexIgnore
    public P04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.S04
    public void a() {
        this.a.setEndIconOnClickListener(null);
        this.a.setEndIconOnLongClickListener(null);
    }
}
