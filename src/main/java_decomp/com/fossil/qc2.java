package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qc2 {
    @DexIgnore
    public static /* final */ Bi a; // = new Xd2();

    @DexIgnore
    public interface Ai<R extends Z62, T> {
        @DexIgnore
        T a(R r);
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        N62 a(Status status);
    }

    @DexIgnore
    public static <R extends Z62, T extends Y62<R>> Nt3<T> a(T62<R> t62, T t) {
        return b(t62, new Zd2(t));
    }

    @DexIgnore
    public static <R extends Z62, T> Nt3<T> b(T62<R> t62, Ai<R, T> ai) {
        Bi bi = a;
        Ot3 ot3 = new Ot3();
        t62.b(new Wd2(t62, ot3, ai, bi));
        return ot3.a();
    }

    @DexIgnore
    public static <R extends Z62> Nt3<Void> c(T62<R> t62) {
        return b(t62, new Yd2());
    }
}
