package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Px6;
import com.fossil.Qx6;
import com.fossil.Tx6;
import com.mapped.AlertDialogFragment;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rx6 extends Qv5 implements Iy6, AlertDialogFragment.Gi, AlertDialogFragment.Hi {
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public Hy6 h;
    @DexIgnore
    public Ey6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Rx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            Rx6 rx6 = new Rx6();
            rx6.setArguments(bundle);
            return rx6;
        }

        @DexIgnore
        public final String b() {
            String simpleName = Rx6.class.getSimpleName();
            Wg6.b(simpleName, "PairingFragment::class.java.simpleName");
            return simpleName;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void D0() {
        if (isActive()) {
            AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
            fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
            fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886794));
            fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886797));
            fi.b(2131363373);
            fi.h(false);
            fi.k(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void F2(String str, boolean z) {
        Wg6.c(str, "serial");
        Ey6 ey6 = this.i;
        if (ey6 instanceof Ux6) {
            ((Ux6) ey6).t3();
            return;
        }
        Ux6 a2 = Ux6.x.a(str, z, 1);
        this.i = a2;
        if (a2 != null) {
            G6(a2, "", 2131362158);
            Ey6 ey62 = this.i;
            if (ey62 != null) {
                Hy6 hy6 = this.h;
                if (hy6 != null) {
                    ey62.M5(hy6);
                } else {
                    Wg6.n("mPairingPresenter");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void G3(List<String> list) {
        Wg6.c(list, "instructions");
        Ey6 ey6 = this.i;
        if (ey6 instanceof Px6) {
            ((Px6) ey6).U6(list);
            return;
        }
        Px6.Ai ai = Px6.z;
        Hy6 hy6 = this.h;
        if (hy6 != null) {
            Px6 a2 = ai.a(list, hy6.s());
            this.i = a2;
            if (a2 != null) {
                G6(a2, "PairingAuthorizeFragment", 2131362158);
                Ey6 ey62 = this.i;
                if (ey62 != null) {
                    Hy6 hy62 = this.h;
                    if (hy62 != null) {
                        ey62.M5(hy62);
                    } else {
                        Wg6.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mPairingPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void K2(int i2, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = k.b();
        local.d(b, "showDeviceConnectFail() - errorCode = " + i2);
        if (!isActive() || getActivity() == null) {
            return;
        }
        if (str != null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String b2 = k.b();
            StringBuilder sb = new StringBuilder();
            sb.append("finish showDeviceConnectFail isOnboardingFlow ");
            Hy6 hy6 = this.h;
            if (hy6 != null) {
                sb.append(hy6.s());
                local2.d(b2, sb.toString());
                e0();
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    Wg6.b(activity, "activity!!");
                    Hy6 hy62 = this.h;
                    if (hy62 != null) {
                        aVar.b(activity, str, true, hy62.s());
                    } else {
                        Wg6.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        } else {
            e0();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String b3 = k.b();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("finish serial empty showDeviceConnectFail isOnboardingFlow ");
            Hy6 hy63 = this.h;
            if (hy63 != null) {
                sb2.append(hy63.s());
                local3.d(b3, sb2.toString());
                TroubleshootingActivity.a aVar2 = TroubleshootingActivity.B;
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    Wg6.b(activity2, "activity!!");
                    Hy6 hy64 = this.h;
                    if (hy64 != null) {
                        TroubleshootingActivity.a.c(aVar2, activity2, null, true, hy64.s(), 2, null);
                    } else {
                        Wg6.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void K6(Hy6 hy6) {
        Wg6.c(hy6, "presenter");
        this.h = hy6;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        K6((Hy6) obj);
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void P3(String str, boolean z, boolean z2) {
        Wg6.c(str, "serial");
        Ey6 ey6 = this.i;
        if (ey6 instanceof Ux6) {
            ((Ux6) ey6).A(z2);
            return;
        }
        Ux6 a2 = Ux6.x.a(str, z, z2 ? 2 : 3);
        this.i = a2;
        if (a2 != null) {
            G6(a2, "", 2131362158);
            Ey6 ey62 = this.i;
            if (ey62 != null) {
                Hy6 hy6 = this.h;
                if (hy6 != null) {
                    ey62.M5(hy6);
                } else {
                    Wg6.n("mPairingPresenter");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0022, code lost:
        if (r5.equals("SERVER_ERROR") != false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00fd, code lost:
        if (r5.equals("SERVER_MAINTENANCE") != false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        e0();
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0016  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0101  */
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void R5(java.lang.String r5, int r6, android.content.Intent r7) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Rx6.R5(java.lang.String, int, android.content.Intent):void");
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void Z0() {
        Ey6 ey6 = this.i;
        if (ey6 == null || !(ey6 instanceof Tx6)) {
            Tx6.Ai ai = Tx6.k;
            Hy6 hy6 = this.h;
            if (hy6 != null) {
                Tx6 a2 = ai.a(hy6.s());
                this.i = a2;
                if (a2 != null) {
                    G6(a2, "", 2131362158);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
        Ey6 ey62 = this.i;
        if (ey62 != null) {
            Hy6 hy62 = this.h;
            if (hy62 != null) {
                ey62.M5(hy62);
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void b4(boolean z) {
        Ey6 ey6 = this.i;
        if (ey6 instanceof Px6) {
            ((Px6) ey6).X6(z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void d0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.B;
            Wg6.b(activity, "it");
            Hy6 hy6 = this.h;
            if (hy6 != null) {
                aVar.a(activity, hy6.s());
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Wg6.b(activity, "activity!!");
            if (!activity.isFinishing()) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    Wg6.b(activity2, "activity!!");
                    if (!activity2.isDestroyed()) {
                        FragmentActivity activity3 = getActivity();
                        if (activity3 != null) {
                            activity3.finish();
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void h4(int i2, String str, String str2) {
        Wg6.c(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = k.b();
        local.d(b, "showDeviceConnectFail due to network - errorCode = " + i2);
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, i2, str2);
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Hi
    public void i1(String str) {
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void i2(String str) {
        Wg6.c(str, "serial");
        Ey6 ey6 = this.i;
        if (ey6 instanceof Px6) {
            ((Px6) ey6).V6(str);
            return;
        }
        Px6.Ai ai = Px6.z;
        Hy6 hy6 = this.h;
        if (hy6 != null) {
            Px6 b = ai.b(str, hy6.s());
            this.i = b;
            if (b != null) {
                G6(b, "PairingAuthorizeFragment", 2131362158);
                Ey6 ey62 = this.i;
                if (ey62 != null) {
                    Hy6 hy62 = this.h;
                    if (hy62 != null) {
                        ey62.M5(hy62);
                    } else {
                        Wg6.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mPairingPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            Wg6.b(activity, "it");
            aVar.a(activity, 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void l4(String str) {
        Wg6.c(str, "serial");
        TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
        Context context = getContext();
        if (context != null) {
            Wg6.b(context, "context!!");
            Hy6 hy6 = this.h;
            if (hy6 != null) {
                aVar.b(context, str, true, hy6.s());
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void o4(List<Lc6<ShineDevice, String>> list) {
        Wg6.c(list, "listDevices");
        Ey6 ey6 = this.i;
        if (ey6 == null || !(ey6 instanceof Qx6)) {
            Qx6.Ai ai = Qx6.s;
            Hy6 hy6 = this.h;
            if (hy6 != null) {
                Qx6 a2 = ai.a(hy6.s());
                a2.t2(list);
                this.i = a2;
                if (a2 != null) {
                    G6(a2, "", 2131362158);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
        Ey6 ey62 = this.i;
        if (ey62 != null) {
            Hy6 hy62 = this.h;
            if (hy62 != null) {
                ey62.M5(hy62);
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558601, viewGroup, false);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Hy6 hy6 = this.h;
        if (hy6 != null) {
            hy6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Hy6 hy6 = this.h;
        if (hy6 != null) {
            hy6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        Hy6 hy6 = this.h;
        if (hy6 != null) {
            if (hy6.q() != null) {
                Hy6 hy62 = this.h;
                if (hy62 != null) {
                    ShineDevice q = hy62.q();
                    if (q != null) {
                        bundle.putParcelable("PAIRING_DEVICE", q);
                    }
                } else {
                    Wg6.n("mPairingPresenter");
                    throw null;
                }
            }
            super.onSaveInstanceState(bundle);
            return;
        }
        Wg6.n("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle != null && bundle.containsKey("PAIRING_DEVICE")) {
            Hy6 hy6 = this.h;
            if (hy6 != null) {
                Parcelable parcelable = bundle.getParcelable("PAIRING_DEVICE");
                if (parcelable != null) {
                    hy6.w((ShineDevice) parcelable);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            Hy6 hy62 = this.h;
            if (hy62 != null) {
                hy62.z(arguments.getBoolean("IS_ONBOARDING_FLOW"));
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
        Ey6 ey6 = (Ey6) getChildFragmentManager().Y(2131362158);
        this.i = ey6;
        if (ey6 == null) {
            Tx6.Ai ai = Tx6.k;
            Hy6 hy63 = this.h;
            if (hy63 != null) {
                Tx6 a2 = ai.a(hy63.s());
                this.i = a2;
                if (a2 != null) {
                    G6(a2, "", 2131362158);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mPairingPresenter");
                throw null;
            }
        }
        Ey6 ey62 = this.i;
        if (ey62 != null) {
            Hy6 hy64 = this.h;
            if (hy64 != null) {
                ey62.M5(hy64);
                Hy6 hy65 = this.h;
                if (hy65 != null) {
                    Ey6 ey63 = this.i;
                    hy65.y((ey63 instanceof Tx6) || (ey63 instanceof Qx6));
                    E6("scan_device_view");
                    return;
                }
                Wg6.n("mPairingPresenter");
                throw null;
            }
            Wg6.n("mPairingPresenter");
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void s6(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            String J = PortfolioApp.get.instance().J();
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.v0(J, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void t2(List<Lc6<ShineDevice, String>> list) {
        Wg6.c(list, "deviceList");
        Ey6 ey6 = this.i;
        if (ey6 != null && (ey6 instanceof Qx6)) {
            ((Qx6) ey6).t2(list);
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void v2(String str, boolean z) {
        Wg6.c(str, "serial");
        Ey6 ey6 = this.i;
        if (ey6 instanceof Ux6) {
            ((Ux6) ey6).V2();
            return;
        }
        Ux6 a2 = Ux6.x.a(str, z, 0);
        this.i = a2;
        if (a2 != null) {
            G6(a2, "", 2131362158);
            Ey6 ey62 = this.i;
            if (ey62 != null) {
                Hy6 hy6 = this.h;
                if (hy6 != null) {
                    ey62.M5(hy6);
                } else {
                    Wg6.n("mPairingPresenter");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void v3(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            String J = PortfolioApp.get.instance().J();
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.w0(J, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void x4() {
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context context = getContext();
            if (context != null) {
                Wg6.b(context, "context!!");
                String J = PortfolioApp.get.instance().J();
                Hy6 hy6 = this.h;
                if (hy6 != null) {
                    aVar.b(context, J, true, hy6.s());
                } else {
                    Wg6.n("mPairingPresenter");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Iy6
    public void y4(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = k.b();
        local.d(b, "openWearOSApp(), isChineseUser=" + z);
        if (z) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.w(childFragmentManager);
            return;
        }
        try {
            Intent launchIntentForPackage = PortfolioApp.get.instance().getPackageManager().getLaunchIntentForPackage("com.google.android.wearable.app");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.startActivity(launchIntentForPackage);
            }
        } catch (Exception e) {
            try {
                FLogger.INSTANCE.getLocal().d(k.b(), "WearOS app not found, open in GG play");
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.google.android.wearable.app")));
                }
            } catch (ActivityNotFoundException e2) {
                FLogger.INSTANCE.getLocal().d(k.b(), "GG play not found, open in browser");
                FragmentActivity activity3 = getActivity();
                if (activity3 != null) {
                    activity3.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.wearable.app")));
                }
            }
        }
    }
}
