package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Wb1<T> {

    @DexIgnore
    public interface Ai<T> {
        @DexIgnore
        void b(Exception exc);

        @DexIgnore
        void e(T t);
    }

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Gb1 c();

    @DexIgnore
    Object cancel();  // void declaration

    @DexIgnore
    void d(Sa1 sa1, Ai<? super T> ai);

    @DexIgnore
    Class<T> getDataClass();
}
