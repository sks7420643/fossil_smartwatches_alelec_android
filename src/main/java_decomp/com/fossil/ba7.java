package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.FileRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba7 extends hq4 {
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public List<fb7> i; // = new ArrayList();
    @DexIgnore
    public String j; // = "";
    @DexIgnore
    public /* final */ MutableLiveData<fb7> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ js0<cl7<List<fb7>, Integer>> l; // = new js0<>();
    @DexIgnore
    public /* final */ u08 m; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ s77 n;
    @DexIgnore
    public /* final */ FileRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$1", f = "WatchFaceTemplateViewModel.kt", l = {25}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ba7 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ba7$a$a")
        @eo7(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$1$1", f = "WatchFaceTemplateViewModel.kt", l = {27, 32}, m = "invokeSuspend")
        /* renamed from: com.fossil.ba7$a$a  reason: collision with other inner class name */
        public static final class C0014a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0014a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0014a aVar = new C0014a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0014a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v19, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0040 A[LOOP:0: B:10:0x003a->B:12:0x0040, LOOP_END] */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x00ae  */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                    r9 = this;
                    r2 = 0
                    r5 = 1
                    r4 = 2
                    java.lang.Object r6 = com.fossil.yn7.d()
                    int r0 = r9.label
                    if (r0 == 0) goto L_0x0056
                    if (r0 == r5) goto L_0x0025
                    if (r0 != r4) goto L_0x001d
                    java.lang.Object r0 = r9.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r9.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r10)
                L_0x001a:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x001c:
                    return r0
                L_0x001d:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0025:
                    java.lang.Object r0 = r9.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r1 = r9.L$0
                    r3 = r1
                    com.fossil.iv7 r3 = (com.fossil.iv7) r3
                    com.fossil.el7.b(r10)
                    r1 = r0
                    r5 = r10
                L_0x0033:
                    r0 = r5
                    java.lang.Iterable r0 = (java.lang.Iterable) r0
                    java.util.Iterator r5 = r0.iterator()
                L_0x003a:
                    boolean r0 = r5.hasNext()
                    if (r0 == 0) goto L_0x0076
                    java.lang.Object r0 = r5.next()
                    com.fossil.p77 r0 = (com.fossil.p77) r0
                    com.fossil.ba7$a r7 = r9.this$0
                    com.fossil.ba7 r7 = r7.this$0
                    com.portfolio.platform.data.source.FileRepository r7 = com.fossil.ba7.o(r7)
                    com.fossil.fb7 r0 = com.fossil.dc7.a(r0, r7)
                    r1.add(r0)
                    goto L_0x003a
                L_0x0056:
                    com.fossil.el7.b(r10)
                    com.fossil.iv7 r3 = r9.p$
                    java.util.ArrayList r1 = new java.util.ArrayList
                    r1.<init>()
                    com.fossil.ba7$a r0 = r9.this$0
                    com.fossil.ba7 r0 = r0.this$0
                    com.fossil.s77 r0 = com.fossil.ba7.n(r0)
                    r9.L$0 = r3
                    r9.L$1 = r1
                    r9.label = r5
                    java.lang.Object r5 = r0.b(r9)
                    if (r5 != r6) goto L_0x0033
                    r0 = r6
                    goto L_0x001c
                L_0x0076:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.fossil.ba7$a r5 = r9.this$0
                    com.fossil.ba7 r5 = r5.this$0
                    java.lang.String r5 = com.fossil.ba7.p(r5)
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    java.lang.String r8 = "uiBackgroundTemplatesLive - assets: "
                    r7.append(r8)
                    int r8 = r1.size()
                    r7.append(r8)
                    java.lang.String r7 = r7.toString()
                    r0.e(r5, r7)
                    com.fossil.ba7$a r0 = r9.this$0
                    com.fossil.ba7 r0 = r0.this$0
                    r9.L$0 = r3
                    r9.L$1 = r1
                    r9.label = r4
                    r3 = r9
                    r5 = r2
                    java.lang.Object r0 = com.fossil.ba7.x(r0, r1, r2, r3, r4, r5)
                    if (r0 != r6) goto L_0x001a
                    r0 = r6
                    goto L_0x001c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ba7.a.C0014a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ba7 ba7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ba7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                C0014a aVar = new C0014a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$syncPickedTemplate$1", f = "WatchFaceTemplateViewModel.kt", l = {68}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $wfId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ba7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ba7 ba7, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ba7;
            this.$wfId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$wfId, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ba7 ba7 = this.this$0;
                String str = this.$wfId;
                this.L$0 = iv7;
                this.label = 1;
                if (ba7.x(ba7, null, str, this, 1, null) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel", f = "WatchFaceTemplateViewModel.kt", l = {95}, m = "updateAssetsAndCurrentIndex")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ba7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ba7 ba7, qn7 qn7) {
            super(qn7);
            this.this$0 = ba7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, null, this);
        }
    }

    @DexIgnore
    public ba7(s77 s77, FileRepository fileRepository) {
        pq7.c(s77, "assetRepository");
        pq7.c(fileRepository, "fileRepository");
        this.n = s77;
        this.o = fileRepository;
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
        String simpleName = ba7.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceTemplateViewModel::class.java.simpleName");
        this.h = simpleName;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ba7 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Object x(ba7 ba7, List list, String str, qn7 qn7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            list = null;
        }
        if ((i2 & 2) != 0) {
            str = null;
        }
        return ba7.w(list, str, qn7);
    }

    @DexIgnore
    public final LiveData<cl7<List<fb7>, Integer>> q() {
        return this.l;
    }

    @DexIgnore
    public final LiveData<fb7> r() {
        return this.k;
    }

    @DexIgnore
    public final void s(fb7 fb7) {
        pq7.c(fb7, "wf");
        this.k.l(fb7);
    }

    @DexIgnore
    public final void t(String str) {
        if (!pq7.a(this.j, str)) {
            this.j = str;
        }
    }

    @DexIgnore
    public final void u(List<fb7> list) {
        if (!pq7.a(this.i, list)) {
            this.i = list;
        }
    }

    @DexIgnore
    public final void v(String str) {
        pq7.c(str, "wfId");
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003f A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0083 A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(java.util.List<com.fossil.fb7> r11, java.lang.String r12, com.fossil.qn7<? super com.fossil.tl7> r13) {
        /*
        // Method dump skipped, instructions count: 318
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ba7.w(java.util.List, java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
