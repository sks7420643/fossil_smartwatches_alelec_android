package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A86 implements Factory<CommuteTimeSettingsDefaultAddressPresenter> {
    @DexIgnore
    public static CommuteTimeSettingsDefaultAddressPresenter a(W76 w76, An4 an4, UserRepository userRepository) {
        return new CommuteTimeSettingsDefaultAddressPresenter(w76, an4, userRepository);
    }
}
