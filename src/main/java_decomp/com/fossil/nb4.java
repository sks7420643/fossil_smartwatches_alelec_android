package com.fossil;

import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Nb4 implements FileFilter {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public Nb4(String str) {
        this.a = str;
    }

    @DexIgnore
    public static FileFilter a(String str) {
        return new Nb4(str);
    }

    @DexIgnore
    public boolean accept(File file) {
        return Sb4.t(this.a, file);
    }
}
