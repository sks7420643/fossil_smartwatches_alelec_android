package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ml extends Yl {
    @DexIgnore
    public A9 V; // = A9.b;

    @DexIgnore
    public Ml(K5 k5, I60 i60, HashMap<Hu1, Object> hashMap) {
        super(k5, i60, Yp.r0, hashMap, E.a("UUID.randomUUID().toString()"));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void f(A9 a9) {
        this.V = a9;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public A9 y() {
        return this.V;
    }
}
