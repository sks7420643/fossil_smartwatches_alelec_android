package com.fossil;

import com.fossil.P72;
import com.google.android.gms.location.LocationResult;
import com.mapped.Yv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ar2 implements P72.Bi<Yv2> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationResult a;

    @DexIgnore
    public Ar2(Zq2 zq2, LocationResult locationResult) {
        this.a = locationResult;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.P72.Bi
    public final /* synthetic */ void a(Yv2 yv2) {
        yv2.onLocationResult(this.a);
    }

    @DexIgnore
    @Override // com.fossil.P72.Bi
    public final void b() {
    }
}
