package com.fossil;

import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class co7 extends zn7 {
    @DexIgnore
    public /* final */ tn7 _context;
    @DexIgnore
    public transient qn7<Object> intercepted;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public co7(qn7<Object> qn7) {
        this(qn7, qn7 != null ? qn7.getContext() : null);
    }

    @DexIgnore
    public co7(qn7<Object> qn7, tn7 tn7) {
        super(qn7);
        this._context = tn7;
    }

    @DexIgnore
    @Override // com.fossil.zn7, com.fossil.qn7
    public tn7 getContext() {
        tn7 tn7 = this._context;
        if (tn7 != null) {
            return tn7;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final qn7<Object> intercepted() {
        qn7<Object> qn7 = this.intercepted;
        if (qn7 == null) {
            rn7 rn7 = (rn7) getContext().get(rn7.p);
            if (rn7 == null || (qn7 = rn7.c(this)) == null) {
                qn7 = this;
            }
            this.intercepted = qn7;
        }
        return qn7;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public void releaseIntercepted() {
        qn7<?> qn7 = this.intercepted;
        if (!(qn7 == null || qn7 == this)) {
            tn7.b bVar = getContext().get(rn7.p);
            if (bVar != null) {
                ((rn7) bVar).a(qn7);
            } else {
                pq7.i();
                throw null;
            }
        }
        this.intercepted = bo7.b;
    }
}
