package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ll3 {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C;
    @DexIgnore
    public String D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public /* final */ Pm3 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public String j;
    @DexIgnore
    public long k;
    @DexIgnore
    public String l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public long p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public String s;
    @DexIgnore
    public Boolean t;
    @DexIgnore
    public long u;
    @DexIgnore
    public List<String> v;
    @DexIgnore
    public String w;
    @DexIgnore
    public long x;
    @DexIgnore
    public long y;
    @DexIgnore
    public long z;

    @DexIgnore
    public Ll3(Pm3 pm3, String str) {
        Rc2.k(pm3);
        Rc2.g(str);
        this.a = pm3;
        this.b = str;
        pm3.c().h();
    }

    @DexIgnore
    public final String A() {
        this.a.c().h();
        return this.d;
    }

    @DexIgnore
    public final void B(long j2) {
        this.a.c().h();
        this.E = (this.n != j2) | this.E;
        this.n = j2;
    }

    @DexIgnore
    public final void C(String str) {
        this.a.c().h();
        this.E |= !Kr3.z0(this.e, str);
        this.e = str;
    }

    @DexIgnore
    public final String D() {
        this.a.c().h();
        return this.s;
    }

    @DexIgnore
    public final void E(long j2) {
        this.a.c().h();
        this.E = (this.u != j2) | this.E;
        this.u = j2;
    }

    @DexIgnore
    public final void F(String str) {
        this.a.c().h();
        this.E |= !Kr3.z0(this.f, str);
        this.f = str;
    }

    @DexIgnore
    public final String G() {
        this.a.c().h();
        return this.w;
    }

    @DexIgnore
    public final void H(long j2) {
        boolean z2 = true;
        Rc2.a(j2 >= 0);
        this.a.c().h();
        boolean z3 = this.E;
        if (this.g == j2) {
            z2 = false;
        }
        this.E = z3 | z2;
        this.g = j2;
    }

    @DexIgnore
    public final void I(String str) {
        this.a.c().h();
        this.E |= !Kr3.z0(this.j, str);
        this.j = str;
    }

    @DexIgnore
    public final String J() {
        this.a.c().h();
        return this.e;
    }

    @DexIgnore
    public final void K(long j2) {
        this.a.c().h();
        this.E = (this.F != j2) | this.E;
        this.F = j2;
    }

    @DexIgnore
    public final void L(String str) {
        this.a.c().h();
        this.E |= !Kr3.z0(this.l, str);
        this.l = str;
    }

    @DexIgnore
    public final String M() {
        this.a.c().h();
        return this.f;
    }

    @DexIgnore
    public final void N(long j2) {
        this.a.c().h();
        this.E = (this.G != j2) | this.E;
        this.G = j2;
    }

    @DexIgnore
    public final void O(String str) {
        this.a.c().h();
        this.E |= !Kr3.z0(this.D, str);
        this.D = str;
    }

    @DexIgnore
    public final long P() {
        this.a.c().h();
        return this.h;
    }

    @DexIgnore
    public final void Q(long j2) {
        this.a.c().h();
        this.E = (this.x != j2) | this.E;
        this.x = j2;
    }

    @DexIgnore
    public final long R() {
        this.a.c().h();
        return this.i;
    }

    @DexIgnore
    public final void S(long j2) {
        this.a.c().h();
        this.E = (this.y != j2) | this.E;
        this.y = j2;
    }

    @DexIgnore
    public final String T() {
        this.a.c().h();
        return this.j;
    }

    @DexIgnore
    public final void U(long j2) {
        this.a.c().h();
        this.E = (this.z != j2) | this.E;
        this.z = j2;
    }

    @DexIgnore
    public final long V() {
        this.a.c().h();
        return this.k;
    }

    @DexIgnore
    public final void W(long j2) {
        this.a.c().h();
        this.E = (this.A != j2) | this.E;
        this.A = j2;
    }

    @DexIgnore
    public final String X() {
        this.a.c().h();
        return this.l;
    }

    @DexIgnore
    public final void Y(long j2) {
        this.a.c().h();
        this.E = (this.C != j2) | this.E;
        this.C = j2;
    }

    @DexIgnore
    public final long Z() {
        this.a.c().h();
        return this.m;
    }

    @DexIgnore
    public final void a(long j2) {
        this.a.c().h();
        this.E = (this.h != j2) | this.E;
        this.h = j2;
    }

    @DexIgnore
    public final void a0(long j2) {
        this.a.c().h();
        this.E = (this.B != j2) | this.E;
        this.B = j2;
    }

    @DexIgnore
    public final void b(Boolean bool) {
        this.a.c().h();
        this.E |= !Kr3.a0(this.t, bool);
        this.t = bool;
    }

    @DexIgnore
    public final long b0() {
        this.a.c().h();
        return this.n;
    }

    @DexIgnore
    public final void c(String str) {
        this.a.c().h();
        this.E |= !Kr3.z0(this.c, str);
        this.c = str;
    }

    @DexIgnore
    public final void c0(long j2) {
        this.a.c().h();
        this.E = (this.p != j2) | this.E;
        this.p = j2;
    }

    @DexIgnore
    public final void d(List<String> list) {
        this.a.c().h();
        if (!Kr3.l0(this.v, list)) {
            this.E = true;
            this.v = list != null ? new ArrayList(list) : null;
        }
    }

    @DexIgnore
    public final long d0() {
        this.a.c().h();
        return this.u;
    }

    @DexIgnore
    public final void e(boolean z2) {
        this.a.c().h();
        this.E = (this.o != z2) | this.E;
        this.o = z2;
    }

    @DexIgnore
    public final boolean e0() {
        this.a.c().h();
        return this.o;
    }

    @DexIgnore
    public final boolean f() {
        this.a.c().h();
        return this.E;
    }

    @DexIgnore
    public final long f0() {
        this.a.c().h();
        return this.g;
    }

    @DexIgnore
    public final long g() {
        this.a.c().h();
        return this.C;
    }

    @DexIgnore
    public final long g0() {
        this.a.c().h();
        return this.F;
    }

    @DexIgnore
    public final long h() {
        this.a.c().h();
        return this.B;
    }

    @DexIgnore
    public final long h0() {
        this.a.c().h();
        return this.G;
    }

    @DexIgnore
    public final String i() {
        this.a.c().h();
        return this.D;
    }

    @DexIgnore
    public final void i0() {
        this.a.c().h();
        long j2 = this.g + 1;
        if (j2 > 2147483647L) {
            this.a.d().I().b("Bundle index overflow. appId", Kl3.w(this.b));
            j2 = 0;
        }
        this.E = true;
        this.g = j2;
    }

    @DexIgnore
    public final String j() {
        this.a.c().h();
        String str = this.D;
        O(null);
        return str;
    }

    @DexIgnore
    public final long j0() {
        this.a.c().h();
        return this.x;
    }

    @DexIgnore
    public final long k() {
        this.a.c().h();
        return this.p;
    }

    @DexIgnore
    public final long k0() {
        this.a.c().h();
        return this.y;
    }

    @DexIgnore
    public final boolean l() {
        this.a.c().h();
        return this.q;
    }

    @DexIgnore
    public final long l0() {
        this.a.c().h();
        return this.z;
    }

    @DexIgnore
    public final boolean m() {
        this.a.c().h();
        return this.r;
    }

    @DexIgnore
    public final long m0() {
        this.a.c().h();
        return this.A;
    }

    @DexIgnore
    public final Boolean n() {
        this.a.c().h();
        return this.t;
    }

    @DexIgnore
    public final List<String> o() {
        this.a.c().h();
        return this.v;
    }

    @DexIgnore
    public final void p() {
        this.a.c().h();
        this.E = false;
    }

    @DexIgnore
    public final void q(long j2) {
        this.a.c().h();
        this.E = (this.i != j2) | this.E;
        this.i = j2;
    }

    @DexIgnore
    public final void r(String str) {
        this.a.c().h();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.E |= !Kr3.z0(this.d, str);
        this.d = str;
    }

    @DexIgnore
    public final void s(boolean z2) {
        this.a.c().h();
        this.E = (this.q != z2) | this.E;
        this.q = z2;
    }

    @DexIgnore
    public final String t() {
        this.a.c().h();
        return this.b;
    }

    @DexIgnore
    public final void u(long j2) {
        this.a.c().h();
        this.E = (this.k != j2) | this.E;
        this.k = j2;
    }

    @DexIgnore
    public final void v(String str) {
        this.a.c().h();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.E |= !Kr3.z0(this.s, str);
        this.s = str;
    }

    @DexIgnore
    public final void w(boolean z2) {
        this.a.c().h();
        this.E = (this.r != z2) | this.E;
        this.r = z2;
    }

    @DexIgnore
    public final String x() {
        this.a.c().h();
        return this.c;
    }

    @DexIgnore
    public final void y(long j2) {
        this.a.c().h();
        this.E = (this.m != j2) | this.E;
        this.m = j2;
    }

    @DexIgnore
    public final void z(String str) {
        this.a.c().h();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.E |= !Kr3.z0(this.w, str);
        this.w = str;
    }
}
