package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pv extends Ps {
    @DexIgnore
    public /* final */ N6 G;
    @DexIgnore
    public /* final */ N6 H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public N6 L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Pv(short s, K5 k5, int i, int i2) {
        super(Hs.X, k5, (i2 & 4) != 0 ? 3 : i);
        N6 n6 = N6.j;
        this.G = n6;
        this.H = n6;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(Sv.k.b).putShort(s).putInt(0).putInt(1).array();
        Wg6.b(array, "ByteBuffer.allocate(11)\n\u2026ILE)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(Sv.k.a()).put((byte) 0).putShort(s).array();
        Wg6.b(array2, "ByteBuffer.allocate(4)\n \u2026dle)\n            .array()");
        this.J = array2;
        this.L = N6.k;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public Mt E(byte b) {
        return Kt.k.a(b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        this.E = this.v.d != Lw.b;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean G(O7 o7) {
        return o7.a == this.L;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public void J(O7 o7) {
        if ((this.s ? Jx.b.b(this.y.x, this.L, o7.b) : o7.b).length == 0) {
            this.v = Mw.a(this.v, null, null, Lw.e, null, null, 27);
        }
        this.E = true;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public N6 K() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] M() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public N6 N() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] P() {
        return this.J;
    }
}
