package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W32 implements Factory<T32> {
    @DexIgnore
    public static /* final */ W32 a; // = new W32();

    @DexIgnore
    public static W32 a() {
        return a;
    }

    @DexIgnore
    public static T32 c() {
        T32 b = U32.b();
        Lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    public T32 b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
