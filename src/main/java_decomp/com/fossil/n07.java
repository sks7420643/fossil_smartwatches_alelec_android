package com.fossil;

import com.mapped.Cj4;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N07 implements Factory<SplashPresenter> {
    @DexIgnore
    public static SplashPresenter a(J07 j07, UserRepository userRepository, MigrationHelper migrationHelper, Cj4 cj4, ThemeRepository themeRepository) {
        return new SplashPresenter(j07, userRepository, migrationHelper, cj4, themeRepository);
    }
}
