package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lw5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<PermissionData> a;
    @DexIgnore
    public Ai b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(PermissionData permissionData);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Hf5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Lw5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.D;
                    FlexibleImageButton flexibleImageButton = this.b.a.s;
                    Wg6.b(flexibleImageButton, "binding.ibInfo");
                    Context context = flexibleImageButton.getContext();
                    Wg6.b(context, "binding.ibInfo.context");
                    List list = this.b.b.a;
                    if (list != null) {
                        aVar.b(context, "", ((PermissionData) list.get(adapterPosition)).getExternalLink());
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.a;
                    if (list != null) {
                        PermissionData permissionData = (PermissionData) list.get(adapterPosition);
                        Ai ai = this.b.b.b;
                        if (ai != null) {
                            ai.a(permissionData);
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Lw5 lw5, Hf5 hf5) {
            super(hf5.n());
            Wg6.c(hf5, "binding");
            this.b = lw5;
            this.a = hf5;
            hf5.s.setOnClickListener(new Aii(this));
            String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (d != null) {
                this.a.t.setBackgroundColor(Color.parseColor(d));
                this.a.u.setColorFilter(Color.parseColor(d), PorterDuff.Mode.SRC_ATOP);
            }
            this.a.q.setOnClickListener(new Bii(this));
        }

        @DexIgnore
        public final void b(PermissionData permissionData) {
            int i = 0;
            Wg6.c(permissionData, "permissionData");
            FlexibleTextView flexibleTextView = this.a.r;
            Wg6.b(flexibleTextView, "binding.ftvDescription");
            flexibleTextView.setText(permissionData.getShortDescription());
            String type = permissionData.getType();
            int hashCode = type.hashCode();
            if (hashCode != -1168251815) {
                if (hashCode == -381825158 && type.equals("PERMISSION_REQUEST_TYPE")) {
                    FlexibleButton flexibleButton = this.a.q;
                    Wg6.b(flexibleButton, "binding.fbGrantPermission");
                    flexibleButton.setText(Um5.c(PortfolioApp.get.instance().getApplicationContext(), 2131886925));
                    FlexibleButton flexibleButton2 = this.a.q;
                    Wg6.b(flexibleButton2, "binding.fbGrantPermission");
                    flexibleButton2.setEnabled(!permissionData.isGranted());
                    ImageView imageView = this.a.u;
                    Wg6.b(imageView, "binding.ivCheck");
                    imageView.setVisibility(permissionData.isGranted() ? 0 : 4);
                }
            } else if (type.equals("PERMISSION_SETTING_TYPE")) {
                FlexibleButton flexibleButton3 = this.a.q;
                Wg6.b(flexibleButton3, "binding.fbGrantPermission");
                flexibleButton3.setText(Um5.c(PortfolioApp.get.instance().getApplicationContext(), 2131886538));
                FlexibleButton flexibleButton4 = this.a.q;
                Wg6.b(flexibleButton4, "binding.fbGrantPermission");
                flexibleButton4.setEnabled(true);
                ImageView imageView2 = this.a.u;
                Wg6.b(imageView2, "binding.ivCheck");
                imageView2.setVisibility(8);
            }
            FlexibleImageButton flexibleImageButton = this.a.s;
            Wg6.b(flexibleImageButton, "binding.ibInfo");
            if (TextUtils.isEmpty(permissionData.getExternalLink())) {
                i = 4;
            }
            flexibleImageButton.setVisibility(i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<PermissionData> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final void i(Ai ai) {
        Wg6.c(ai, "listener");
        this.b = ai;
    }

    @DexIgnore
    public final void j(List<PermissionData> list) {
        Wg6.c(list, "permissionList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        Bi bi = (Bi) viewHolder;
        List<PermissionData> list = this.a;
        if (list != null) {
            bi.b(list.get(i));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Hf5 z = Hf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemPermissionBinding.in\u2026.context), parent, false)");
        return new Bi(this, z);
    }
}
