package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O90 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore
    public O90(short s, byte b2, byte b3) {
        this.b = (short) s;
        this.c = (byte) b2;
        this.d = (byte) b3;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(this.c).putShort(this.b).put(this.d).array();
        Wg6.b(array, "ByteBuffer.allocate(4)\n \u2026ber)\n            .array()");
        this.a = array;
    }
}
