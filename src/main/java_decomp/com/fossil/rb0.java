package com.fossil;

import com.fossil.blesdk.model.network.Auth;
import com.mapped.Cd6;
import com.mapped.Il6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rb0 {
    @DexIgnore
    public Auth a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Sy7<Auth> c;
    @DexIgnore
    public /* final */ Il6 d;
    @DexIgnore
    public /* final */ Ft1 e;

    @DexIgnore
    public Rb0(Ft1 ft1) {
        this.e = ft1;
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        Wg6.b(newSingleThreadExecutor, "Executors.newSingleThreadExecutor()");
        this.d = Jv7.a(Pw7.b(newSingleThreadExecutor));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0033 A[Catch:{ all -> 0x00a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.mapped.Xe6<? super java.lang.String> r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.blesdk.model.network.AuthMaintainer
            if (r0 == 0) goto L_0x0038
            r0 = r7
            com.fossil.blesdk.model.network.AuthMaintainer r0 = (com.fossil.blesdk.model.network.AuthMaintainer) r0
            int r1 = r0.c
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.c = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.b
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.c
            if (r3 == 0) goto L_0x0058
            if (r3 == r4) goto L_0x0047
            if (r3 != r5) goto L_0x003f
            java.lang.Object r0 = r1.f
            com.fossil.blesdk.model.network.Auth r0 = (com.fossil.blesdk.model.network.Auth) r0
            java.lang.Object r0 = r1.e
            com.fossil.Rb0 r0 = (com.fossil.Rb0) r0
            com.fossil.El7.b(r2)     // Catch:{ all -> 0x00a3 }
            r0 = r2
        L_0x002f:
            com.fossil.blesdk.model.network.Auth r0 = (com.fossil.blesdk.model.network.Auth) r0     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x00a4
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x00a3 }
        L_0x0037:
            return r0
        L_0x0038:
            com.fossil.blesdk.model.network.AuthMaintainer r0 = new com.fossil.blesdk.model.network.AuthMaintainer
            r0.<init>(r6, r7)
            r1 = r0
            goto L_0x0015
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            java.lang.Object r0 = r1.e
            com.fossil.Rb0 r0 = (com.fossil.Rb0) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x004f:
            com.fossil.blesdk.model.network.Auth r0 = (com.fossil.blesdk.model.network.Auth) r0
            if (r0 == 0) goto L_0x00a4
            java.lang.String r0 = r0.a()
            goto L_0x0037
        L_0x0058:
            com.fossil.El7.b(r2)
            boolean r2 = r6.b
            if (r2 == 0) goto L_0x0076
            com.fossil.Sy7<com.fossil.blesdk.model.network.Auth> r2 = r6.c
            if (r2 == 0) goto L_0x00a4
            com.fossil.Vy7 r2 = r2.f()
            if (r2 == 0) goto L_0x00a4
            r1.e = r6
            r3 = 1
            r1.c = r3
            java.lang.Object r2 = r2.b(r1)
            if (r2 == r0) goto L_0x0037
            r0 = r2
            goto L_0x004f
        L_0x0076:
            com.fossil.blesdk.model.network.Auth r2 = r6.a
            if (r2 == 0) goto L_0x0087
            int r3 = r2.b()
            r4 = 30
            if (r3 <= r4) goto L_0x0087
            java.lang.String r0 = r2.a()
            goto L_0x0037
        L_0x0087:
            r6.b()
            com.fossil.Sy7<com.fossil.blesdk.model.network.Auth> r3 = r6.c
            if (r3 == 0) goto L_0x00a4
            com.fossil.Vy7 r3 = r3.f()
            if (r3 == 0) goto L_0x00a4
            r1.e = r6
            r1.f = r2
            r2 = 2
            r1.c = r2
            java.lang.Object r1 = com.fossil.Ny7.a(r3, r1)
            if (r1 == r0) goto L_0x0037
            r0 = r1
            goto L_0x002f
        L_0x00a3:
            r0 = move-exception
        L_0x00a4:
            r0 = 0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Rb0.a(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void b() {
        synchronized (Boolean.valueOf(this.b)) {
            if (!this.b && this.e.a() != null) {
                this.b = true;
                this.c = new Sy7<>();
                Rm6 unused = Gu7.d(this.d, null, null, new Qb0(null, this), 3, null);
            }
            Cd6 cd6 = Cd6.a;
        }
    }
}
