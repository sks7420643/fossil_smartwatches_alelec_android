package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class O94 implements Ft3 {
    @DexIgnore
    public /* final */ Q94 a;

    @DexIgnore
    public O94(Q94 q94) {
        this.a = q94;
    }

    @DexIgnore
    public static Ft3 a(Q94 q94) {
        return new O94(q94);
    }

    @DexIgnore
    @Override // com.fossil.Ft3
    public Object then(Nt3 nt3) {
        return Boolean.valueOf(this.a.i(nt3));
    }
}
