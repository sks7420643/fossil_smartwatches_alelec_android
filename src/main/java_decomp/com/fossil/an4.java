package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class An4 extends Jn4 {
    @DexIgnore
    public static void f(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            iArr[i2] = ((1 << (8 - i2)) & i) == 0 ? 1 : 2;
        }
    }

    @DexIgnore
    @Override // com.fossil.Jn4, com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (kl4 == Kl4.CODE_39) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_39, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.Jn4
    public boolean[] c(String str) {
        int length = str.length();
        if (length <= 80) {
            int[] iArr = new int[9];
            int i = length + 25;
            for (int i2 = 0; i2 < length; i2++) {
                int indexOf = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(str.charAt(i2));
                if (indexOf >= 0) {
                    f(Zm4.a[indexOf], iArr);
                    for (int i3 = 0; i3 < 9; i3++) {
                        i += iArr[i3];
                    }
                } else {
                    throw new IllegalArgumentException("Bad contents: " + str);
                }
            }
            boolean[] zArr = new boolean[i];
            f(Zm4.b, iArr);
            int b = Jn4.b(zArr, 0, iArr, true);
            int[] iArr2 = {1};
            int b2 = Jn4.b(zArr, b, iArr2, false) + b;
            for (int i4 = 0; i4 < length; i4++) {
                f(Zm4.a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(str.charAt(i4))], iArr);
                int b3 = b2 + Jn4.b(zArr, b2, iArr, true);
                b2 = b3 + Jn4.b(zArr, b3, iArr2, false);
            }
            f(Zm4.b, iArr);
            Jn4.b(zArr, b2, iArr, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
    }
}
