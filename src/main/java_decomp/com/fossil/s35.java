package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class S35 extends R35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H;
    @DexIgnore
    public long F;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H = sparseIntArray;
        sparseIntArray.put(2131362592, 1);
        H.put(2131361978, 2);
        H.put(2131362298, 3);
        H.put(2131363471, 4);
        H.put(2131361903, 5);
        H.put(2131362775, 6);
        H.put(2131362133, 7);
        H.put(2131362787, 8);
        H.put(2131362741, 9);
        H.put(2131362783, 10);
        H.put(2131362359, 11);
        H.put(2131361921, 12);
        H.put(2131362801, 13);
        H.put(2131363072, 14);
    }
    */

    @DexIgnore
    public S35(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 15, G, H));
    }

    @DexIgnore
    public S35(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleAutoCompleteTextView) objArr[5], (View) objArr[12], (FlexibleButton) objArr[2], (ImageView) objArr[7], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[11], (RTLImageView) objArr[1], (ImageView) objArr[9], (ConstraintLayout) objArr[6], (View) objArr[10], (ImageView) objArr[8], (LinearLayout) objArr[13], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[14], (View) objArr[4]);
        this.F = -1;
        this.C.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.F != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.F = 1;
        }
        w();
    }
}
