package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ib3 extends IInterface {
    @DexIgnore
    void S1(LocationResult locationResult) throws RemoteException;

    @DexIgnore
    void j1(LocationAvailability locationAvailability) throws RemoteException;
}
