package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import androidx.lifecycle.LiveData;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.mapped.Mi;
import com.mapped.Oh;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nw0 {
    @DexIgnore
    public static /* final */ String[] m; // = {"UPDATE", "DELETE", "INSERT"};
    @DexIgnore
    public /* final */ HashMap<String, Integer> a;
    @DexIgnore
    public /* final */ String[] b;
    @DexIgnore
    public Map<String, Set<String>> c;
    @DexIgnore
    public /* final */ Oh d;
    @DexIgnore
    public AtomicBoolean e; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile boolean f; // = false;
    @DexIgnore
    public volatile Mi g;
    @DexIgnore
    public Bi h;
    @DexIgnore
    public /* final */ Mw0 i;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public /* final */ Fi0<Ci, Di> j; // = new Fi0<>();
    @DexIgnore
    public Ow0 k;
    @DexIgnore
    public Runnable l; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final Set<Integer> a() {
            HashSet hashSet = new HashSet();
            Cursor query = Nw0.this.d.query(new Kx0("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (query.moveToNext()) {
                try {
                    hashSet.add(Integer.valueOf(query.getInt(0)));
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            query.close();
            if (!hashSet.isEmpty()) {
                Nw0.this.g.executeUpdateDelete();
            }
            return hashSet;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x005d  */
        /* JADX WARNING: Removed duplicated region for block: B:64:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                r2 = 0
                com.fossil.Nw0 r0 = com.fossil.Nw0.this
                com.mapped.Oh r0 = r0.d
                java.util.concurrent.locks.Lock r3 = r0.getCloseLock()
                r3.lock()     // Catch:{ IllegalStateException -> 0x008f, SQLiteException -> 0x00a1 }
                com.fossil.Nw0 r0 = com.fossil.Nw0.this     // Catch:{ IllegalStateException -> 0x008f, SQLiteException -> 0x00a1 }
                boolean r0 = r0.e()     // Catch:{ IllegalStateException -> 0x008f, SQLiteException -> 0x00a1 }
                if (r0 != 0) goto L_0x0018
                r3.unlock()
            L_0x0017:
                return
            L_0x0018:
                com.fossil.Nw0 r0 = com.fossil.Nw0.this
                java.util.concurrent.atomic.AtomicBoolean r0 = r0.e
                r1 = 1
                r4 = 0
                boolean r0 = r0.compareAndSet(r1, r4)
                if (r0 != 0) goto L_0x0028
                r3.unlock()
                goto L_0x0017
            L_0x0028:
                com.fossil.Nw0 r0 = com.fossil.Nw0.this
                com.mapped.Oh r0 = r0.d
                boolean r0 = r0.inTransaction()
                if (r0 == 0) goto L_0x0036
                r3.unlock()
                goto L_0x0017
            L_0x0036:
                com.fossil.Nw0 r0 = com.fossil.Nw0.this
                com.mapped.Oh r0 = r0.d
                boolean r0 = r0.mWriteAheadLoggingEnabled
                if (r0 == 0) goto L_0x009b
                com.fossil.Nw0 r0 = com.fossil.Nw0.this
                com.mapped.Oh r0 = r0.d
                com.mapped.Ji r0 = r0.getOpenHelper()
                com.fossil.Lx0 r4 = r0.getWritableDatabase()
                r4.beginTransaction()
                java.util.Set r0 = r5.a()     // Catch:{ all -> 0x0089 }
                r4.setTransactionSuccessful()     // Catch:{ all -> 0x00b3 }
                r4.endTransaction()     // Catch:{ IllegalStateException -> 0x00ad, SQLiteException -> 0x00b0 }
                r1 = r0
            L_0x0058:
                r3.unlock()
                if (r1 == 0) goto L_0x0017
                boolean r0 = r1.isEmpty()
                if (r0 != 0) goto L_0x0017
                com.fossil.Nw0 r0 = com.fossil.Nw0.this
                com.fossil.Fi0<com.fossil.Nw0$Ci, com.fossil.Nw0$Di> r2 = r0.j
                monitor-enter(r2)
                com.fossil.Nw0 r0 = com.fossil.Nw0.this     // Catch:{ all -> 0x0086 }
                com.fossil.Fi0<com.fossil.Nw0$Ci, com.fossil.Nw0$Di> r0 = r0.j     // Catch:{ all -> 0x0086 }
                java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x0086 }
            L_0x0070:
                boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0086 }
                if (r0 == 0) goto L_0x00a5
                java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x0086 }
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0086 }
                java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x0086 }
                com.fossil.Nw0$Di r0 = (com.fossil.Nw0.Di) r0     // Catch:{ all -> 0x0086 }
                r0.a(r1)     // Catch:{ all -> 0x0086 }
                goto L_0x0070
            L_0x0086:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0086 }
                throw r0
            L_0x0089:
                r0 = move-exception
                r1 = r0
            L_0x008b:
                r4.endTransaction()
                throw r1
            L_0x008f:
                r0 = move-exception
                r1 = r0
            L_0x0091:
                r0 = r2
            L_0x0092:
                java.lang.String r2 = "ROOM"
                java.lang.String r4 = "Cannot run invalidation tracker. Is the db closed?"
                android.util.Log.e(r2, r4, r1)     // Catch:{ all -> 0x00a8 }
                r1 = r0
                goto L_0x0058
            L_0x009b:
                java.util.Set r0 = r5.a()
                r1 = r0
                goto L_0x0058
            L_0x00a1:
                r0 = move-exception
                r1 = r0
            L_0x00a3:
                r0 = r2
                goto L_0x0092
            L_0x00a5:
                monitor-exit(r2)
                goto L_0x0017
            L_0x00a8:
                r0 = move-exception
                r3.unlock()
                throw r0
            L_0x00ad:
                r1 = move-exception
                r2 = r0
                goto L_0x0091
            L_0x00b0:
                r1 = move-exception
                r2 = r0
                goto L_0x00a3
            L_0x00b3:
                r1 = move-exception
                r2 = r0
                goto L_0x008b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Nw0.Ai.run():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ long[] a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public Bi(int i) {
            long[] jArr = new long[i];
            this.a = jArr;
            this.b = new boolean[i];
            this.c = new int[i];
            Arrays.fill(jArr, 0L);
            Arrays.fill(this.b, false);
        }

        @DexIgnore
        public int[] a() {
            synchronized (this) {
                if (!this.d || this.e) {
                    return null;
                }
                int length = this.a.length;
                for (int i = 0; i < length; i++) {
                    boolean z = this.a[i] > 0;
                    if (z != this.b[i]) {
                        this.c[i] = z ? 1 : 2;
                    } else {
                        this.c[i] = 0;
                    }
                    this.b[i] = z;
                }
                this.e = true;
                this.d = false;
                return this.c;
            }
        }

        @DexIgnore
        public boolean b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.a[i];
                    this.a[i] = 1 + j;
                    if (j == 0) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        public boolean c(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.a[i];
                    this.a[i] = j - 1;
                    if (j == 1) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        public void d() {
            synchronized (this) {
                this.e = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci {
        @DexIgnore
        public /* final */ String[] mTables;

        @DexIgnore
        public Ci(String str, String... strArr) {
            String[] strArr2 = (String[]) Arrays.copyOf(strArr, strArr.length + 1);
            this.mTables = strArr2;
            strArr2[strArr.length] = str;
        }

        @DexIgnore
        public Ci(String[] strArr) {
            this.mTables = (String[]) Arrays.copyOf(strArr, strArr.length);
        }

        @DexIgnore
        public boolean isRemote() {
            return false;
        }

        @DexIgnore
        public abstract void onInvalidated(Set<String> set);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ String[] b;
        @DexIgnore
        public /* final */ Ci c;
        @DexIgnore
        public /* final */ Set<String> d;

        @DexIgnore
        public Di(Ci ci, int[] iArr, String[] strArr) {
            this.c = ci;
            this.a = iArr;
            this.b = strArr;
            if (iArr.length == 1) {
                HashSet hashSet = new HashSet();
                hashSet.add(this.b[0]);
                this.d = Collections.unmodifiableSet(hashSet);
                return;
            }
            this.d = null;
        }

        @DexIgnore
        public void a(Set<Integer> set) {
            int length = this.a.length;
            Set<String> set2 = null;
            for (int i = 0; i < length; i++) {
                if (set.contains(Integer.valueOf(this.a[i]))) {
                    if (length == 1) {
                        set2 = this.d;
                    } else {
                        if (set2 == null) {
                            set2 = new HashSet<>(length);
                        }
                        set2.add(this.b[i]);
                    }
                }
            }
            if (set2 != null) {
                this.c.onInvalidated(set2);
            }
        }

        @DexIgnore
        public void b(String[] strArr) {
            Set<String> set = null;
            if (this.b.length == 1) {
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (strArr[i].equalsIgnoreCase(this.b[0])) {
                        set = this.d;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                HashSet hashSet = new HashSet();
                for (String str : strArr) {
                    String[] strArr2 = this.b;
                    int length2 = strArr2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        String str2 = strArr2[i2];
                        if (str2.equalsIgnoreCase(str)) {
                            hashSet.add(str2);
                            break;
                        }
                        i2++;
                    }
                }
                if (hashSet.size() > 0) {
                    set = hashSet;
                }
            }
            if (set != null) {
                this.c.onInvalidated(set);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends Ci {
        @DexIgnore
        public /* final */ Nw0 a;
        @DexIgnore
        public /* final */ WeakReference<Ci> b;

        @DexIgnore
        public Ei(Nw0 nw0, Ci ci) {
            super(ci.mTables);
            this.a = nw0;
            this.b = new WeakReference<>(ci);
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Ci ci = this.b.get();
            if (ci == null) {
                this.a.j(this);
            } else {
                ci.onInvalidated(set);
            }
        }
    }

    @DexIgnore
    public Nw0(Oh oh, Map<String, String> map, Map<String, Set<String>> map2, String... strArr) {
        this.d = oh;
        this.h = new Bi(strArr.length);
        this.a = new HashMap<>();
        this.c = map2;
        this.i = new Mw0(this.d);
        int length = strArr.length;
        this.b = new String[length];
        for (int i2 = 0; i2 < length; i2++) {
            String lowerCase = strArr[i2].toLowerCase(Locale.US);
            this.a.put(lowerCase, Integer.valueOf(i2));
            String str = map.get(strArr[i2]);
            if (str != null) {
                this.b[i2] = str.toLowerCase(Locale.US);
            } else {
                this.b[i2] = lowerCase;
            }
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String lowerCase2 = entry.getValue().toLowerCase(Locale.US);
            if (this.a.containsKey(lowerCase2)) {
                String lowerCase3 = entry.getKey().toLowerCase(Locale.US);
                HashMap<String, Integer> hashMap = this.a;
                hashMap.put(lowerCase3, hashMap.get(lowerCase2));
            }
        }
    }

    @DexIgnore
    public static void c(StringBuilder sb, String str, String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append(LocaleConverter.LOCALE_DELIMITER);
        sb.append(str2);
        sb.append("`");
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public void a(Ci ci) {
        Di f2;
        String[] k2 = k(ci.mTables);
        int[] iArr = new int[k2.length];
        int length = k2.length;
        for (int i2 = 0; i2 < length; i2++) {
            Integer num = this.a.get(k2[i2].toLowerCase(Locale.US));
            if (num != null) {
                iArr[i2] = num.intValue();
            } else {
                throw new IllegalArgumentException("There is no table with name " + k2[i2]);
            }
        }
        Di di = new Di(ci, iArr, k2);
        synchronized (this.j) {
            f2 = this.j.f(ci, di);
        }
        if (f2 == null && this.h.b(iArr)) {
            p();
        }
    }

    @DexIgnore
    public void b(Ci ci) {
        a(new Ei(this, ci));
    }

    @DexIgnore
    public <T> LiveData<T> d(String[] strArr, boolean z, Callable<T> callable) {
        return this.i.a(r(strArr), z, callable);
    }

    @DexIgnore
    public boolean e() {
        if (!this.d.isOpen()) {
            return false;
        }
        if (!this.f) {
            this.d.getOpenHelper().getWritableDatabase();
        }
        if (this.f) {
            return true;
        }
        Log.e("ROOM", "database is not initialized even though it is open");
        return false;
    }

    @DexIgnore
    public void f(Lx0 lx0) {
        synchronized (this) {
            if (this.f) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            lx0.execSQL("PRAGMA temp_store = MEMORY;");
            lx0.execSQL("PRAGMA recursive_triggers='ON';");
            lx0.execSQL("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            q(lx0);
            this.g = lx0.compileStatement("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.f = true;
        }
    }

    @DexIgnore
    public void g(String... strArr) {
        synchronized (this.j) {
            Iterator<Map.Entry<Ci, Di>> it = this.j.iterator();
            while (it.hasNext()) {
                Map.Entry<Ci, Di> next = it.next();
                if (!next.getKey().isRemote()) {
                    next.getValue().b(strArr);
                }
            }
        }
    }

    @DexIgnore
    public void h() {
        if (this.e.compareAndSet(false, true)) {
            this.d.getQueryExecutor().execute(this.l);
        }
    }

    @DexIgnore
    public void i() {
        p();
        this.l.run();
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public void j(Ci ci) {
        Di g2;
        synchronized (this.j) {
            g2 = this.j.g(ci);
        }
        if (g2 != null && this.h.c(g2.a)) {
            p();
        }
    }

    @DexIgnore
    public final String[] k(String[] strArr) {
        HashSet hashSet = new HashSet();
        for (String str : strArr) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (this.c.containsKey(lowerCase)) {
                hashSet.addAll(this.c.get(lowerCase));
            } else {
                hashSet.add(str);
            }
        }
        return (String[]) hashSet.toArray(new String[hashSet.size()]);
    }

    @DexIgnore
    public void l(Context context, String str) {
        this.k = new Ow0(context, str, this, this.d.getQueryExecutor());
    }

    @DexIgnore
    public final void m(Lx0 lx0, int i2) {
        lx0.execSQL("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i2 + ", 0)");
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        String[] strArr = m;
        for (String str2 : strArr) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            c(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i2);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            lx0.execSQL(sb.toString());
        }
    }

    @DexIgnore
    public void n() {
        Ow0 ow0 = this.k;
        if (ow0 != null) {
            ow0.a();
            this.k = null;
        }
    }

    @DexIgnore
    public final void o(Lx0 lx0, int i2) {
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        String[] strArr = m;
        for (String str2 : strArr) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            c(sb, str, str2);
            lx0.execSQL(sb.toString());
        }
    }

    @DexIgnore
    public void p() {
        if (this.d.isOpen()) {
            q(this.d.getOpenHelper().getWritableDatabase());
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void q(Lx0 lx0) {
        if (!lx0.inTransaction()) {
            while (true) {
                Lock closeLock = this.d.getCloseLock();
                closeLock.lock();
                int[] a2 = this.h.a();
                if (a2 == null) {
                    closeLock.unlock();
                    return;
                }
                int length = a2.length;
                lx0.beginTransaction();
                for (int i2 = 0; i2 < length; i2++) {
                    int i3 = a2[i2];
                    if (i3 == 1) {
                        m(lx0, i2);
                    } else if (i3 == 2) {
                        o(lx0, i2);
                    }
                }
                try {
                    lx0.setTransactionSuccessful();
                    try {
                        lx0.endTransaction();
                        this.h.d();
                        try {
                        } catch (SQLiteException | IllegalStateException e2) {
                            Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                            return;
                        }
                    } finally {
                        closeLock.unlock();
                    }
                } catch (Throwable th) {
                    lx0.endTransaction();
                    throw th;
                }
            }
        }
    }

    @DexIgnore
    public final String[] r(String[] strArr) {
        String[] k2 = k(strArr);
        for (String str : k2) {
            if (!this.a.containsKey(str.toLowerCase(Locale.US))) {
                throw new IllegalArgumentException("There is no table with name " + str);
            }
        }
        return k2;
    }
}
