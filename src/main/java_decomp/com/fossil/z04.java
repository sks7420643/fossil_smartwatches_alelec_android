package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z04<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Z04<Object> implements Serializable {
        @DexIgnore
        public static /* final */ Bi INSTANCE; // = new Bi();
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;

        @DexIgnore
        private Object readResolve() {
            return INSTANCE;
        }

        @DexIgnore
        @Override // com.fossil.Z04
        public boolean doEquivalent(Object obj, Object obj2) {
            return obj.equals(obj2);
        }

        @DexIgnore
        @Override // com.fossil.Z04
        public int doHash(Object obj) {
            return obj.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements J14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Z04<T> equivalence;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        public Ci(Z04<T> z04, T t) {
            I14.l(z04);
            this.equivalence = z04;
            this.target = t;
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean apply(T t) {
            return this.equivalence.equivalent(t, this.target);
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ci)) {
                return false;
            }
            Ci ci = (Ci) obj;
            return this.equivalence.equals(ci.equivalence) && F14.a(this.target, ci.target);
        }

        @DexIgnore
        public int hashCode() {
            return F14.b(this.equivalence, this.target);
        }

        @DexIgnore
        public String toString() {
            return this.equivalence + ".equivalentTo(" + ((Object) this.target) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Z04<Object> implements Serializable {
        @DexIgnore
        public static /* final */ Di INSTANCE; // = new Di();
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;

        @DexIgnore
        private Object readResolve() {
            return INSTANCE;
        }

        @DexIgnore
        @Override // com.fossil.Z04
        public boolean doEquivalent(Object obj, Object obj2) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Z04
        public int doHash(Object obj) {
            return System.identityHashCode(obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Z04<? super T> equivalence;
        @DexIgnore
        public /* final */ T reference;

        @DexIgnore
        public Ei(Z04<? super T> z04, T t) {
            I14.l(z04);
            this.equivalence = z04;
            this.reference = t;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Ei) {
                Ei ei = (Ei) obj;
                if (this.equivalence.equals(ei.equivalence)) {
                    return this.equivalence.equivalent(this.reference, ei.reference);
                }
            }
            return false;
        }

        @DexIgnore
        public T get() {
            return this.reference;
        }

        @DexIgnore
        public int hashCode() {
            return this.equivalence.hash(this.reference);
        }

        @DexIgnore
        public String toString() {
            return this.equivalence + ".wrap(" + ((Object) this.reference) + ")";
        }
    }

    @DexIgnore
    public static Z04<Object> equals() {
        return Bi.INSTANCE;
    }

    @DexIgnore
    public static Z04<Object> identity() {
        return Di.INSTANCE;
    }

    @DexIgnore
    public abstract boolean doEquivalent(T t, T t2);

    @DexIgnore
    public abstract int doHash(T t);

    @DexIgnore
    public final boolean equivalent(T t, T t2) {
        if (t == t2) {
            return true;
        }
        if (t == null || t2 == null) {
            return false;
        }
        return doEquivalent(t, t2);
    }

    @DexIgnore
    public final J14<T> equivalentTo(T t) {
        return new Ci(this, t);
    }

    @DexIgnore
    public final int hash(T t) {
        if (t == null) {
            return 0;
        }
        return doHash(t);
    }

    @DexIgnore
    public final <F> Z04<F> onResultOf(B14<F, ? extends T> b14) {
        return new C14(b14, this);
    }

    @DexIgnore
    public final <S extends T> Z04<Iterable<S>> pairwise() {
        return new H14(this);
    }

    @DexIgnore
    public final <S extends T> Ei<S> wrap(S s) {
        return new Ei<>(s);
    }
}
