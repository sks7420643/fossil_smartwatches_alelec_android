package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.fossil.L72;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z82 {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public Z82(int i) {
        this.a = i;
    }

    @DexIgnore
    public static Status a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (Mf2.b() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    @DexIgnore
    public abstract void b(Status status);

    @DexIgnore
    public abstract void c(L72.Ai<?> ai) throws DeadObjectException;

    @DexIgnore
    public abstract void d(A82 a82, boolean z);

    @DexIgnore
    public abstract void e(Exception exc);
}
