package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wt4 implements Factory<FCMRepository> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> b;

    @DexIgnore
    public Wt4(Provider<An4> provider, Provider<ApiServiceV2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Wt4 a(Provider<An4> provider, Provider<ApiServiceV2> provider2) {
        return new Wt4(provider, provider2);
    }

    @DexIgnore
    public static FCMRepository c(An4 an4, ApiServiceV2 apiServiceV2) {
        return new FCMRepository(an4, apiServiceV2);
    }

    @DexIgnore
    public FCMRepository b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
