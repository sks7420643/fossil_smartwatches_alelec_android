package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum T8 {
    c((byte) 0),
    d((byte) 1);
    
    @DexIgnore
    public static /* final */ S8 f; // = new S8(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public T8(byte b2) {
        this.b = (byte) b2;
    }
}
