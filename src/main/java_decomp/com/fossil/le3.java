package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Rg2;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Le3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Le3> CREATOR; // = new Df3();
    @DexIgnore
    public LatLng b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Zd3 e;
    @DexIgnore
    public float f; // = 0.5f;
    @DexIgnore
    public float g; // = 1.0f;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float l; // = 0.5f;
    @DexIgnore
    public float m; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float s; // = 1.0f;
    @DexIgnore
    public float t;

    @DexIgnore
    public Le3() {
    }

    @DexIgnore
    public Le3(LatLng latLng, String str, String str2, IBinder iBinder, float f2, float f3, boolean z, boolean z2, boolean z3, float f4, float f5, float f6, float f7, float f8) {
        this.b = latLng;
        this.c = str;
        this.d = str2;
        if (iBinder == null) {
            this.e = null;
        } else {
            this.e = new Zd3(Rg2.Ai.e(iBinder));
        }
        this.f = f2;
        this.g = f3;
        this.h = z;
        this.i = z2;
        this.j = z3;
        this.k = f4;
        this.l = f5;
        this.m = f6;
        this.s = f7;
        this.t = f8;
    }

    @DexIgnore
    public final float A() {
        return this.s;
    }

    @DexIgnore
    public final Le3 A0(float f2) {
        this.k = f2;
        return this;
    }

    @DexIgnore
    public final Le3 B0(String str) {
        this.d = str;
        return this;
    }

    @DexIgnore
    public final Le3 C0(String str) {
        this.c = str;
        return this;
    }

    @DexIgnore
    public final float D() {
        return this.f;
    }

    @DexIgnore
    public final Le3 D0(boolean z) {
        this.i = z;
        return this;
    }

    @DexIgnore
    public final Le3 E0(float f2) {
        this.t = f2;
        return this;
    }

    @DexIgnore
    public final float F() {
        return this.g;
    }

    @DexIgnore
    public final float L() {
        return this.l;
    }

    @DexIgnore
    public final Le3 c(float f2) {
        this.s = f2;
        return this;
    }

    @DexIgnore
    public final Le3 f(float f2, float f3) {
        this.f = f2;
        this.g = f3;
        return this;
    }

    @DexIgnore
    public final Le3 h(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public final Le3 k(boolean z) {
        this.j = z;
        return this;
    }

    @DexIgnore
    public final float o0() {
        return this.m;
    }

    @DexIgnore
    public final LatLng p0() {
        return this.b;
    }

    @DexIgnore
    public final float q0() {
        return this.k;
    }

    @DexIgnore
    public final String r0() {
        return this.d;
    }

    @DexIgnore
    public final String s0() {
        return this.c;
    }

    @DexIgnore
    public final float t0() {
        return this.t;
    }

    @DexIgnore
    public final Le3 u0(Zd3 zd3) {
        this.e = zd3;
        return this;
    }

    @DexIgnore
    public final Le3 v0(float f2, float f3) {
        this.l = f2;
        this.m = f3;
        return this;
    }

    @DexIgnore
    public final boolean w0() {
        return this.h;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 2, p0(), i2, false);
        Bd2.u(parcel, 3, s0(), false);
        Bd2.u(parcel, 4, r0(), false);
        Zd3 zd3 = this.e;
        Bd2.m(parcel, 5, zd3 == null ? null : zd3.a().asBinder(), false);
        Bd2.j(parcel, 6, D());
        Bd2.j(parcel, 7, F());
        Bd2.c(parcel, 8, w0());
        Bd2.c(parcel, 9, y0());
        Bd2.c(parcel, 10, x0());
        Bd2.j(parcel, 11, q0());
        Bd2.j(parcel, 12, L());
        Bd2.j(parcel, 13, o0());
        Bd2.j(parcel, 14, A());
        Bd2.j(parcel, 15, t0());
        Bd2.b(parcel, a2);
    }

    @DexIgnore
    public final boolean x0() {
        return this.j;
    }

    @DexIgnore
    public final boolean y0() {
        return this.i;
    }

    @DexIgnore
    public final Le3 z0(LatLng latLng) {
        if (latLng != null) {
            this.b = latLng;
            return this;
        }
        throw new IllegalArgumentException("latlng cannot be null - a position is required.");
    }
}
