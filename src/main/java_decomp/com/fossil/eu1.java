package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum eu1 {
    IN_PROGRESS("in_progress"),
    END("end"),
    ERROR("error"),
    SUCCESS("success");
    
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public eu1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
