package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sr0 implements Dw7 {
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ LiveData<?> c;
    @DexIgnore
    public /* final */ Js0<?> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "androidx.lifecycle.EmittedSource$dispose$1", f = "CoroutineLiveData.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Sr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Sr0 sr0, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = sr0;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.d();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "androidx.lifecycle.EmittedSource$disposeNow$2", f = "CoroutineLiveData.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Sr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Sr0 sr0, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = sr0;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.d();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public Sr0(LiveData<?> liveData, Js0<?> js0) {
        Wg6.c(liveData, "source");
        Wg6.c(js0, "mediator");
        this.c = liveData;
        this.d = js0;
    }

    @DexIgnore
    public final Object b(Xe6<? super Cd6> xe6) {
        return Eu7.g(Bw7.c().S(), new Bi(this, null), xe6);
    }

    @DexIgnore
    public final void d() {
        if (!this.b) {
            this.d.q((LiveData<S>) this.c);
            this.b = true;
        }
    }

    @DexIgnore
    @Override // com.fossil.Dw7
    public void dispose() {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.c().S()), null, null, new Ai(this, null), 3, null);
    }
}
