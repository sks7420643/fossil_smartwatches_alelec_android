package com.fossil;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nj2 {
    @DexIgnore
    public static Nj2 b;
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> a; // = new Zi0();

    @DexIgnore
    public Nj2(Context context) {
    }

    @DexIgnore
    public static Nj2 a(Context context) {
        Nj2 nj2;
        synchronized (Nj2.class) {
            try {
                if (b == null) {
                    b = new Nj2(context.getApplicationContext());
                }
                nj2 = b;
            } catch (Throwable th) {
                throw th;
            }
        }
        return nj2;
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        boolean z;
        synchronized (this) {
            Map<String, Boolean> map = this.a.get(str2);
            if (map == null) {
                map = new Zi0<>();
                this.a.put(str2, map);
            }
            z = map.put(str, Boolean.FALSE) == null;
        }
        return z;
    }

    @DexIgnore
    public final void c(String str, String str2) {
        synchronized (this) {
            Map<String, Boolean> map = this.a.get(str2);
            if (map != null) {
                if ((map.remove(str) != null) && map.isEmpty()) {
                    this.a.remove(str2);
                }
            }
        }
    }

    @DexIgnore
    public final boolean d(String str) {
        boolean containsKey;
        synchronized (this) {
            containsKey = this.a.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public final boolean e(String str, String str2) {
        synchronized (this) {
            Map<String, Boolean> map = this.a.get(str2);
            if (map == null) {
                return false;
            }
            Boolean bool = map.get(str);
            if (bool == null) {
                return false;
            }
            return bool.booleanValue();
        }
    }
}
