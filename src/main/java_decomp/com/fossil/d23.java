package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D23 implements E33 {
    @DexIgnore
    public static /* final */ N23 b; // = new B23();
    @DexIgnore
    public /* final */ N23 a;

    @DexIgnore
    public D23() {
        this(new F23(B13.a(), a()));
    }

    @DexIgnore
    public D23(N23 n23) {
        H13.f(n23, "messageInfoFactory");
        this.a = n23;
    }

    @DexIgnore
    public static N23 a() {
        try {
            return (N23) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            return b;
        }
    }

    @DexIgnore
    public static boolean b(K23 k23) {
        return k23.zza() == E13.Fi.i;
    }

    @DexIgnore
    @Override // com.fossil.E33
    public final <T> F33<T> zza(Class<T> cls) {
        H33.p(cls);
        K23 zzb = this.a.zzb(cls);
        return zzb.zzb() ? E13.class.isAssignableFrom(cls) ? S23.c(H33.B(), U03.a(), zzb.zzc()) : S23.c(H33.f(), U03.b(), zzb.zzc()) : E13.class.isAssignableFrom(cls) ? b(zzb) ? Q23.j(cls, zzb, W23.b(), V13.c(), H33.B(), U03.a(), L23.b()) : Q23.j(cls, zzb, W23.b(), V13.c(), H33.B(), null, L23.b()) : b(zzb) ? Q23.j(cls, zzb, W23.a(), V13.a(), H33.f(), U03.b(), L23.a()) : Q23.j(cls, zzb, W23.a(), V13.a(), H33.v(), null, L23.a());
    }
}
