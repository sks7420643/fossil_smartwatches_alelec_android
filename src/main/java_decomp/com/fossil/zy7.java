package com.fossil;

import com.mapped.Rc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zy7<T> {
    @DexIgnore
    public Object[] a; // = new Object[16];
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public final void a(T t) {
        Object[] objArr = this.a;
        int i = this.c;
        objArr[i] = t;
        int length = (objArr.length - 1) & (i + 1);
        this.c = length;
        if (length == this.b) {
            b();
        }
    }

    @DexIgnore
    public final void b() {
        Object[] objArr = this.a;
        int length = objArr.length;
        Object[] objArr2 = new Object[(length << 1)];
        Dm7.j(objArr, objArr2, 0, this.b, 0, 10, null);
        Object[] objArr3 = this.a;
        int length2 = objArr3.length;
        int i = this.b;
        Dm7.j(objArr3, objArr2, length2 - i, 0, i, 4, null);
        this.a = objArr2;
        this.b = 0;
        this.c = length;
    }

    @DexIgnore
    public final boolean c() {
        return this.b == this.c;
    }

    @DexIgnore
    public final T d() {
        int i = this.b;
        if (i == this.c) {
            return null;
        }
        Object[] objArr = this.a;
        T t = (T) objArr[i];
        objArr[i] = null;
        this.b = (i + 1) & (objArr.length - 1);
        if (t != null) {
            return t;
        }
        throw new Rc6("null cannot be cast to non-null type T");
    }
}
