package com.fossil;

import com.mapped.Hi6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tq7 extends Sq7 {
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ Hi6 owner;
    @DexIgnore
    public /* final */ String signature;

    @DexIgnore
    public Tq7(Hi6 hi6, String str, String str2) {
        this.owner = hi6;
        this.name = str;
        this.signature = str2;
    }

    @DexIgnore
    @Override // com.fossil.Sq7
    public Object get(Object obj) {
        return getGetter().call(obj);
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7
    public String getName() {
        return this.name;
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Hi6 getOwner() {
        return this.owner;
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public String getSignature() {
        return this.signature;
    }

    @DexIgnore
    @Override // com.fossil.Sq7
    public void set(Object obj, Object obj2) {
        getSetter().call(obj, obj2);
    }
}
