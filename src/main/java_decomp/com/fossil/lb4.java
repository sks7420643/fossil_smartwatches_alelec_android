package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lb4 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;
    @DexIgnore
    public P18 c;

    @DexIgnore
    public Lb4(int i, String str, P18 p18) {
        this.a = i;
        this.b = str;
        this.c = p18;
    }

    @DexIgnore
    public static Lb4 c(Response response) throws IOException {
        return new Lb4(response.f(), response.a() == null ? null : response.a().string(), response.l());
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public String d(String str) {
        return this.c.c(str);
    }
}
