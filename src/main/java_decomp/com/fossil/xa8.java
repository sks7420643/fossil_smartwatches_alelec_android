package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xa8 {
    @DexIgnore
    public static boolean a;

    @DexIgnore
    public static void a(Object obj, Throwable th) {
        if (a) {
            Log.e("PhotoManagerPlugin", obj == null ? "null" : obj.toString(), th);
        }
    }

    @DexIgnore
    public static void b(Object obj) {
        if (a) {
            Log.i("PhotoManagerPlugin", obj == null ? "null" : obj.toString());
        }
    }
}
