package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E82 {
    @DexIgnore
    public /* final */ G72<?> a;
    @DexIgnore
    public /* final */ Ot3<Boolean> b;

    @DexIgnore
    public final G72<?> a() {
        return this.a;
    }

    @DexIgnore
    public final Ot3<Boolean> b() {
        return this.b;
    }
}
