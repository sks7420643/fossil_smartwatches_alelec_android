package com.fossil;

import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ca7 implements Factory<WatchFaceTemplateViewModel> {
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;

    @DexIgnore
    public Ca7(Provider<WFAssetRepository> provider, Provider<FileRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Ca7 a(Provider<WFAssetRepository> provider, Provider<FileRepository> provider2) {
        return new Ca7(provider, provider2);
    }

    @DexIgnore
    public static WatchFaceTemplateViewModel c(WFAssetRepository wFAssetRepository, FileRepository fileRepository) {
        return new WatchFaceTemplateViewModel(wFAssetRepository, fileRepository);
    }

    @DexIgnore
    public WatchFaceTemplateViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
