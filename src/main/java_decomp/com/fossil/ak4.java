package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ak4<K, V> extends AbstractMap<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
    @DexIgnore
    public static /* final */ Comparator<Comparable> b; // = new Ai();
    @DexIgnore
    public Comparator<? super K> comparator;
    @DexIgnore
    public Ak4<K, V>.b entrySet;
    @DexIgnore
    public /* final */ Ei<K, V> header;
    @DexIgnore
    public Ak4<K, V>.c keySet;
    @DexIgnore
    public int modCount;
    @DexIgnore
    public Ei<K, V> root;
    @DexIgnore
    public int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Comparator<Comparable> {
        @DexIgnore
        public int a(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(Comparable comparable, Comparable comparable2) {
            return a(comparable, comparable2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends AbstractSet<Map.Entry<K, V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Ak4<K, V>.d {
            @DexIgnore
            public Aii(Bi bi) {
                super();
            }

            @DexIgnore
            public Map.Entry<K, V> b() {
                return a();
            }

            @DexIgnore
            public /* bridge */ /* synthetic */ Object next() {
                return b();
            }
        }

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void clear() {
            Ak4.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && Ak4.this.findByEntry((Map.Entry) obj) != null;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new Aii(this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            Ei<K, V> findByEntry;
            if (!(obj instanceof Map.Entry) || (findByEntry = Ak4.this.findByEntry((Map.Entry) obj)) == null) {
                return false;
            }
            Ak4.this.removeInternal(findByEntry, true);
            return true;
        }

        @DexIgnore
        public int size() {
            return Ak4.this.size;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends AbstractSet<K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Ak4<K, V>.d {
            @DexIgnore
            public Aii(Ci ci) {
                super();
            }

            @DexIgnore
            public K next() {
                return a().g;
            }
        }

        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void clear() {
            Ak4.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return Ak4.this.containsKey(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new Aii(this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return Ak4.this.removeInternalByKey(obj) != null;
        }

        @DexIgnore
        public int size() {
            return Ak4.this.size;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class Di<T> implements Iterator<T> {
        @DexIgnore
        public Ei<K, V> b;
        @DexIgnore
        public Ei<K, V> c; // = null;
        @DexIgnore
        public int d;

        @DexIgnore
        public Di() {
            Ak4 ak4 = Ak4.this;
            this.b = ak4.header.e;
            this.d = ak4.modCount;
        }

        @DexIgnore
        public final Ei<K, V> a() {
            Ei<K, V> ei = this.b;
            Ak4 ak4 = Ak4.this;
            if (ei == ak4.header) {
                throw new NoSuchElementException();
            } else if (ak4.modCount == this.d) {
                this.b = ei.e;
                this.c = ei;
                return ei;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        @DexIgnore
        public final boolean hasNext() {
            return this.b != Ak4.this.header;
        }

        @DexIgnore
        public final void remove() {
            Ei<K, V> ei = this.c;
            if (ei != null) {
                Ak4.this.removeInternal(ei, true);
                this.c = null;
                this.d = Ak4.this.modCount;
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public Ei<K, V> b;
        @DexIgnore
        public Ei<K, V> c;
        @DexIgnore
        public Ei<K, V> d;
        @DexIgnore
        public Ei<K, V> e;
        @DexIgnore
        public Ei<K, V> f;
        @DexIgnore
        public /* final */ K g;
        @DexIgnore
        public V h;
        @DexIgnore
        public int i;

        @DexIgnore
        public Ei() {
            this.g = null;
            this.f = this;
            this.e = this;
        }

        @DexIgnore
        public Ei(Ei<K, V> ei, K k, Ei<K, V> ei2, Ei<K, V> ei3) {
            this.b = ei;
            this.g = k;
            this.i = 1;
            this.e = ei2;
            this.f = ei3;
            ei3.e = this;
            ei2.f = this;
        }

        @DexIgnore
        public Ei<K, V> a() {
            for (Ei<K, V> ei = this.c; ei != null; ei = ei.c) {
                this = ei;
            }
            return this;
        }

        @DexIgnore
        public Ei<K, V> b() {
            for (Ei<K, V> ei = this.d; ei != null; ei = ei.d) {
                this = ei;
            }
            return this;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r1 = r4 instanceof java.util.Map.Entry
                r0 = 0
                if (r1 == 0) goto L_0x001c
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r1 = r3.g
                if (r1 != 0) goto L_0x001d
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x001c
            L_0x0011:
                V r1 = r3.h
                if (r1 != 0) goto L_0x0028
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x001c
            L_0x001b:
                r0 = 1
            L_0x001c:
                return r0
            L_0x001d:
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x0011
            L_0x0028:
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ak4.Ei.equals(java.lang.Object):boolean");
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            return this.g;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            return this.h;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            K k = this.g;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.h;
            if (v != null) {
                i2 = v.hashCode();
            }
            return hashCode ^ i2;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            V v2 = this.h;
            this.h = v;
            return v2;
        }

        @DexIgnore
        public String toString() {
            return ((Object) this.g) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) this.h);
        }
    }

    @DexIgnore
    public Ak4() {
        this(b);
    }

    @DexIgnore
    public Ak4(Comparator<? super K> comparator2) {
        this.size = 0;
        this.modCount = 0;
        this.header = new Ei<>();
        this.comparator = comparator2 == null ? b : comparator2;
    }

    @DexIgnore
    private Object writeReplace() throws ObjectStreamException {
        return new LinkedHashMap(this);
    }

    @DexIgnore
    public final boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public final void b(Ei<K, V> ei, boolean z) {
        while (ei != null) {
            Ei<K, V> ei2 = ei.c;
            Ei<K, V> ei3 = ei.d;
            int i = ei2 != null ? ei2.i : 0;
            int i2 = ei3 != null ? ei3.i : 0;
            int i3 = i - i2;
            if (i3 == -2) {
                Ei<K, V> ei4 = ei3.c;
                Ei<K, V> ei5 = ei3.d;
                int i4 = (ei4 != null ? ei4.i : 0) - (ei5 != null ? ei5.i : 0);
                if (i4 == -1 || (i4 == 0 && !z)) {
                    e(ei);
                } else {
                    f(ei3);
                    e(ei);
                }
                if (z) {
                    return;
                }
            } else if (i3 == 2) {
                Ei<K, V> ei6 = ei2.c;
                Ei<K, V> ei7 = ei2.d;
                int i5 = (ei6 != null ? ei6.i : 0) - (ei7 != null ? ei7.i : 0);
                if (i5 == 1 || (i5 == 0 && !z)) {
                    f(ei);
                } else {
                    e(ei2);
                    f(ei);
                }
                if (z) {
                    return;
                }
            } else if (i3 == 0) {
                ei.i = i + 1;
                if (z) {
                    return;
                }
            } else {
                ei.i = Math.max(i, i2) + 1;
                if (!z) {
                    return;
                }
            }
            ei = ei.b;
        }
    }

    @DexIgnore
    public final void c(Ei<K, V> ei, Ei<K, V> ei2) {
        Ei<K, V> ei3 = ei.b;
        ei.b = null;
        if (ei2 != null) {
            ei2.b = ei3;
        }
        if (ei3 == null) {
            this.root = ei2;
        } else if (ei3.c == ei) {
            ei3.c = ei2;
        } else {
            ei3.d = ei2;
        }
    }

    @DexIgnore
    public void clear() {
        this.root = null;
        this.size = 0;
        this.modCount++;
        Ei<K, V> ei = this.header;
        ei.f = ei;
        ei.e = ei;
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return findByObject(obj) != null;
    }

    @DexIgnore
    public final void e(Ei<K, V> ei) {
        int i = 0;
        Ei<K, V> ei2 = ei.c;
        Ei<K, V> ei3 = ei.d;
        Ei<K, V> ei4 = ei3.c;
        Ei<K, V> ei5 = ei3.d;
        ei.d = ei4;
        if (ei4 != null) {
            ei4.b = ei;
        }
        c(ei, ei3);
        ei3.c = ei;
        ei.b = ei3;
        int max = Math.max(ei2 != null ? ei2.i : 0, ei4 != null ? ei4.i : 0) + 1;
        ei.i = max;
        if (ei5 != null) {
            i = ei5.i;
        }
        ei3.i = Math.max(max, i) + 1;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        Ak4<K, V>.b bVar = this.entrySet;
        if (bVar != null) {
            return bVar;
        }
        Bi bi = new Bi();
        this.entrySet = bi;
        return bi;
    }

    @DexIgnore
    public final void f(Ei<K, V> ei) {
        int i = 0;
        Ei<K, V> ei2 = ei.c;
        Ei<K, V> ei3 = ei.d;
        Ei<K, V> ei4 = ei2.c;
        Ei<K, V> ei5 = ei2.d;
        ei.c = ei5;
        if (ei5 != null) {
            ei5.b = ei;
        }
        c(ei, ei2);
        ei2.d = ei;
        ei.b = ei2;
        int max = Math.max(ei3 != null ? ei3.i : 0, ei5 != null ? ei5.i : 0) + 1;
        ei.i = max;
        if (ei4 != null) {
            i = ei4.i;
        }
        ei2.i = Math.max(max, i) + 1;
    }

    @DexIgnore
    public Ei<K, V> find(K k, boolean z) {
        int i;
        Ei<K, V> ei;
        Comparator<? super K> comparator2 = this.comparator;
        Ei<K, V> ei2 = this.root;
        if (ei2 != null) {
            K k2 = comparator2 == b ? k : null;
            while (true) {
                int compareTo = k2 != null ? k2.compareTo(ei2.g) : comparator2.compare(k, ei2.g);
                if (compareTo == 0) {
                    return ei2;
                }
                Ei<K, V> ei3 = compareTo < 0 ? ei2.c : ei2.d;
                if (ei3 == null) {
                    i = compareTo;
                    break;
                }
                ei2 = ei3;
            }
        } else {
            i = 0;
        }
        if (!z) {
            return null;
        }
        Ei<K, V> ei4 = this.header;
        if (ei2 != null) {
            ei = new Ei<>(ei2, k, ei4, ei4.f);
            if (i < 0) {
                ei2.c = ei;
            } else {
                ei2.d = ei;
            }
            b(ei2, true);
        } else if (comparator2 != b || (k instanceof Comparable)) {
            ei = new Ei<>(ei2, k, ei4, ei4.f);
            this.root = ei;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.size++;
        this.modCount++;
        return ei;
    }

    @DexIgnore
    public Ei<K, V> findByEntry(Map.Entry<?, ?> entry) {
        Ei<K, V> findByObject = findByObject(entry.getKey());
        if (findByObject != null && a(findByObject.h, entry.getValue())) {
            return findByObject;
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public Ei<K, V> findByObject(Object obj) {
        if (obj == 0) {
            return null;
        }
        try {
            return find(obj, false);
        } catch (ClassCastException e) {
            return null;
        }
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Ei<K, V> findByObject = findByObject(obj);
        if (findByObject != null) {
            return findByObject.h;
        }
        return null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        Ak4<K, V>.c cVar = this.keySet;
        if (cVar != null) {
            return cVar;
        }
        Ci ci = new Ci();
        this.keySet = ci;
        return ci;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V put(K k, V v) {
        if (k != null) {
            Ei<K, V> find = find(k, true);
            V v2 = find.h;
            find.h = v;
            return v2;
        }
        throw new NullPointerException("key == null");
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        Ei<K, V> removeInternalByKey = removeInternalByKey(obj);
        if (removeInternalByKey != null) {
            return removeInternalByKey.h;
        }
        return null;
    }

    @DexIgnore
    public void removeInternal(Ei<K, V> ei, boolean z) {
        int i;
        int i2 = 0;
        if (z) {
            Ei<K, V> ei2 = ei.f;
            ei2.e = ei.e;
            ei.e.f = ei2;
        }
        Ei<K, V> ei3 = ei.c;
        Ei<K, V> ei4 = ei.d;
        Ei<K, V> ei5 = ei.b;
        if (ei3 == null || ei4 == null) {
            if (ei3 != null) {
                c(ei, ei3);
                ei.c = null;
            } else if (ei4 != null) {
                c(ei, ei4);
                ei.d = null;
            } else {
                c(ei, null);
            }
            b(ei5, false);
            this.size--;
            this.modCount++;
            return;
        }
        Ei<K, V> b2 = ei3.i > ei4.i ? ei3.b() : ei4.a();
        removeInternal(b2, false);
        Ei<K, V> ei6 = ei.c;
        if (ei6 != null) {
            i = ei6.i;
            b2.c = ei6;
            ei6.b = b2;
            ei.c = null;
        } else {
            i = 0;
        }
        Ei<K, V> ei7 = ei.d;
        if (ei7 != null) {
            i2 = ei7.i;
            b2.d = ei7;
            ei7.b = b2;
            ei.d = null;
        }
        b2.i = Math.max(i, i2) + 1;
        c(ei, b2);
    }

    @DexIgnore
    public Ei<K, V> removeInternalByKey(Object obj) {
        Ei<K, V> findByObject = findByObject(obj);
        if (findByObject != null) {
            removeInternal(findByObject, true);
        }
        return findByObject;
    }

    @DexIgnore
    public int size() {
        return this.size;
    }
}
