package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class De3 {
    @DexIgnore
    public /* final */ Is2 a;

    @DexIgnore
    public De3(Is2 is2) {
        Rc2.k(is2);
        this.a = is2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void c(LatLng latLng) {
        try {
            this.a.setCenter(latLng);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        try {
            this.a.b(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void e(int i) {
        try {
            this.a.setFillColor(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof De3)) {
            return false;
        }
        try {
            return this.a.b0(((De3) obj).a);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void f(double d) {
        try {
            this.a.setRadius(d);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void g(int i) {
        try {
            this.a.setStrokeColor(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void h(float f) {
        try {
            this.a.setStrokeWidth(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void j(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }
}
