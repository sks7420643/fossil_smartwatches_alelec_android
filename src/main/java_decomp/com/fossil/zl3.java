package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zl3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ Xl3 e;

    @DexIgnore
    public Zl3(Xl3 xl3, String str, boolean z) {
        this.e = xl3;
        Rc2.g(str);
        this.a = str;
        this.b = z;
    }

    @DexIgnore
    public final void a(boolean z) {
        SharedPreferences.Editor edit = this.e.B().edit();
        edit.putBoolean(this.a, z);
        edit.apply();
        this.d = z;
    }

    @DexIgnore
    public final boolean b() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.B().getBoolean(this.a, this.b);
        }
        return this.d;
    }
}
