package com.fossil;

import com.mapped.Cd6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ru7 extends Zw7<Fx7> implements Qu7 {
    @DexIgnore
    public /* final */ Su7 f;

    @DexIgnore
    public Ru7(Fx7 fx7, Su7 su7) {
        super(fx7);
        this.f = su7;
    }

    @DexIgnore
    @Override // com.fossil.Qu7
    public boolean c(Throwable th) {
        return ((Fx7) this.e).y(th);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        w(th);
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return "ChildHandle[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.Zu7
    public void w(Throwable th) {
        this.f.m((Nx7) this.e);
    }
}
