package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum L90 {
    c("req_chl_info"),
    d("req_chl_list"),
    e("sync_pkg");
    
    @DexIgnore
    public static /* final */ K90 g; // = new K90(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public L90(String str) {
        this.b = str;
    }
}
