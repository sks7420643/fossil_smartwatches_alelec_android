package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pg0 extends ViewGroup {
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public ActionMenuView d;
    @DexIgnore
    public Rg0 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Ro0 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements So0 {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void a(View view) {
            this.a = true;
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void b(View view) {
            if (!this.a) {
                Pg0 pg0 = Pg0.this;
                pg0.g = null;
                Pg0.super.setVisibility(this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void c(View view) {
            Pg0.super.setVisibility(0);
            this.a = false;
        }

        @DexIgnore
        public Ai d(Ro0 ro0, int i) {
            Pg0.this.g = ro0;
            this.b = i;
            return this;
        }
    }

    @DexIgnore
    public Pg0(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public Pg0(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = new Ai();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(Le0.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.c = context;
        } else {
            this.c = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    @DexIgnore
    public static int d(int i2, int i3, boolean z) {
        return z ? i2 - i3 : i2 + i3;
    }

    @DexIgnore
    public int c(View view, int i2, int i3, int i4) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i2, RecyclerView.UNDEFINED_DURATION), i3);
        return Math.max(0, (i2 - view.getMeasuredWidth()) - i4);
    }

    @DexIgnore
    public int e(View view, int i2, int i3, int i4, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = ((i4 - measuredHeight) / 2) + i3;
        if (z) {
            view.layout(i2 - measuredWidth, i5, i2, measuredHeight + i5);
        } else {
            view.layout(i2, i5, i2 + measuredWidth, measuredHeight + i5);
        }
        return z ? -measuredWidth : measuredWidth;
    }

    @DexIgnore
    public Ro0 f(int i2, long j) {
        Ro0 ro0 = this.g;
        if (ro0 != null) {
            ro0.b();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            Ro0 c2 = Mo0.c(this);
            c2.a(1.0f);
            c2.d(j);
            Ai ai = this.b;
            ai.d(c2, i2);
            c2.f(ai);
            return c2;
        }
        Ro0 c3 = Mo0.c(this);
        c3.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        c3.d(j);
        Ai ai2 = this.b;
        ai2.d(c3, i2);
        c3.f(ai2);
        return c3;
    }

    @DexIgnore
    public int getAnimatedVisibility() {
        return this.g != null ? this.b.b : getVisibility();
    }

    @DexIgnore
    public int getContentHeight() {
        return this.f;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, Ue0.ActionBar, Le0.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(Ue0.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        Rg0 rg0 = this.e;
        if (rg0 != null) {
            rg0.I(configuration);
        }
    }

    @DexIgnore
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.i = false;
        }
        if (!this.i) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.i = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.i = false;
        }
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.h = false;
        }
        if (!this.h) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.h = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.h = false;
        }
        return true;
    }

    @DexIgnore
    public abstract void setContentHeight(int i2);

    @DexIgnore
    public void setVisibility(int i2) {
        if (i2 != getVisibility()) {
            Ro0 ro0 = this.g;
            if (ro0 != null) {
                ro0.b();
            }
            super.setVisibility(i2);
        }
    }
}
