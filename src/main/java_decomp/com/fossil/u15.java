package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class U15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ Zc5 s;
    @DexIgnore
    public /* final */ Zc5 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public U15(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, Zc5 zc5, Zc5 zc52, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = zc5;
        x(zc5);
        this.t = zc52;
        x(zc52);
        this.u = linearLayout;
    }
}
