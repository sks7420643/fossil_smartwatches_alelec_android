package com.fossil;

import com.mapped.Qg6;
import com.mapped.Rc6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q6 {
    @DexIgnore
    public /* synthetic */ Q6(Qg6 qg6) {
    }

    @DexIgnore
    public final R6[] a(int i) {
        R6[] values = R6.values();
        ArrayList arrayList = new ArrayList();
        for (R6 r6 : values) {
            if ((r6.b & i) != 0) {
                arrayList.add(r6);
            }
        }
        Object[] array = arrayList.toArray(new R6[0]);
        if (array != null) {
            return (R6[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
