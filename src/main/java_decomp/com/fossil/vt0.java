package com.fossil;

import com.fossil.Bu0;
import com.mapped.Xe;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vt0<Key, Value> extends Xe<Key, Value> {
    @DexIgnore
    public abstract void dispatchLoadAfter(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai);

    @DexIgnore
    public abstract void dispatchLoadBefore(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai);

    @DexIgnore
    public abstract void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, Bu0.Ai<Value> ai);

    @DexIgnore
    public abstract Key getKey(int i, Value value);

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isContiguous() {
        return true;
    }

    @DexIgnore
    public boolean supportsPageDropping() {
        return true;
    }
}
