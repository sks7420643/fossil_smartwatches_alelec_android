package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U8 implements Parcelable.Creator<V8> {
    @DexIgnore
    public /* synthetic */ U8(Qg6 qg6) {
    }

    @DexIgnore
    public final V8 a(byte[] bArr) throws IllegalArgumentException {
        if (bArr.length == 3) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            byte b = order.get(0);
            T8 a2 = T8.f.a(b);
            if (a2 != null) {
                return new V8(a2, order.getShort(1));
            }
            throw new IllegalArgumentException("Invalid action: " + ((int) b) + '.');
        }
        throw new IllegalArgumentException(E.c(E.e("Invalid data length ("), bArr.length, "), ", "require 3."));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public V8 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            return new V8(T8.valueOf(readString), (short) parcel.readInt());
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public V8[] newArray(int i) {
        return new V8[i];
    }
}
