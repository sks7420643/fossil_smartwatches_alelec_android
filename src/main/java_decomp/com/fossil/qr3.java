package com.fossil;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qr3 {
    @DexIgnore
    public /* final */ Pm3 a;

    @DexIgnore
    public Qr3(Pm3 pm3) {
        this.a = pm3;
    }

    @DexIgnore
    public final void a() {
        this.a.c().h();
        if (e()) {
            if (d()) {
                this.a.z().A.b(null);
                Bundle bundle = new Bundle();
                bundle.putString("source", "(not set)");
                bundle.putString("medium", "(not set)");
                bundle.putString("_cis", "intent");
                bundle.putLong("_cc", 1);
                this.a.E().Q("auto", "_cmpx", bundle);
            } else {
                String a2 = this.a.z().A.a();
                if (TextUtils.isEmpty(a2)) {
                    this.a.d().G().a("Cache still valid but referrer not found");
                } else {
                    long a3 = this.a.z().B.a() / 3600000;
                    Uri parse = Uri.parse(a2);
                    Bundle bundle2 = new Bundle();
                    Pair pair = new Pair(parse.getPath(), bundle2);
                    for (String str : parse.getQueryParameterNames()) {
                        bundle2.putString(str, parse.getQueryParameter(str));
                    }
                    ((Bundle) pair.second).putLong("_cc", (a3 - 1) * 3600000);
                    this.a.E().Q((String) pair.first, "_cmp", (Bundle) pair.second);
                }
                this.a.z().A.b(null);
            }
            this.a.z().B.b(0);
        }
    }

    @DexIgnore
    public final void b(String str, Bundle bundle) {
        String str2;
        this.a.c().h();
        if (!this.a.o()) {
            if (bundle == null || bundle.isEmpty()) {
                str2 = null;
            } else {
                if (str == null || str.isEmpty()) {
                    str = "auto";
                }
                Uri.Builder builder = new Uri.Builder();
                builder.path(str);
                for (String str3 : bundle.keySet()) {
                    builder.appendQueryParameter(str3, bundle.getString(str3));
                }
                str2 = builder.build().toString();
            }
            if (!TextUtils.isEmpty(str2)) {
                this.a.z().A.b(str2);
                this.a.z().B.b(this.a.zzm().b());
            }
        }
    }

    @DexIgnore
    public final void c() {
        if (e() && d()) {
            this.a.z().A.b(null);
        }
    }

    @DexIgnore
    public final boolean d() {
        return e() && this.a.zzm().b() - this.a.z().B.a() > this.a.w().p(null, Xg3.S0);
    }

    @DexIgnore
    public final boolean e() {
        return this.a.z().B.a() > 0;
    }
}
