package com.fossil;

import android.text.TextUtils;
import com.fossil.Jh4;
import com.fossil.Kh4;
import com.fossil.Vg4;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tg4 implements Ug4 {
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public static /* final */ ThreadFactory m; // = new Ai();
    @DexIgnore
    public /* final */ J64 a;
    @DexIgnore
    public /* final */ Ih4 b;
    @DexIgnore
    public /* final */ Eh4 c;
    @DexIgnore
    public /* final */ Bh4 d;
    @DexIgnore
    public /* final */ Dh4 e;
    @DexIgnore
    public /* final */ Zg4 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ ExecutorService h;
    @DexIgnore
    public /* final */ ExecutorService i;
    @DexIgnore
    public String j;
    @DexIgnore
    public /* final */ List<Ah4> k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format("firebase-installations-executor-%d", Integer.valueOf(this.a.getAndIncrement())));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Bi {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[Kh4.Bi.values().length];
            b = iArr;
            try {
                iArr[Kh4.Bi.OK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[Kh4.Bi.BAD_CONFIG.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[Kh4.Bi.AUTH_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            int[] iArr2 = new int[Jh4.Bi.values().length];
            a = iArr2;
            try {
                iArr2[Jh4.Bi.OK.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Jh4.Bi.BAD_CONFIG.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
        }
        */
    }

    @DexIgnore
    public Tg4(J64 j64, Ti4 ti4, Je4 je4) {
        this(new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), m), j64, new Ih4(j64.g(), ti4, je4), new Eh4(j64), new Bh4(), new Dh4(j64), new Zg4());
    }

    @DexIgnore
    public Tg4(ExecutorService executorService, J64 j64, Ih4 ih4, Eh4 eh4, Bh4 bh4, Dh4 dh4, Zg4 zg4) {
        this.g = new Object();
        this.j = null;
        this.k = new ArrayList();
        this.a = j64;
        this.b = ih4;
        this.c = eh4;
        this.d = bh4;
        this.e = dh4;
        this.f = zg4;
        this.h = executorService;
        this.i = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), m);
    }

    @DexIgnore
    public static Tg4 k() {
        return l(J64.h());
    }

    @DexIgnore
    public static Tg4 l(J64 j64) {
        Rc2.b(j64 != null, "Null is not a valid value of FirebaseApp.");
        return (Tg4) j64.f(Ug4.class);
    }

    @DexIgnore
    @Override // com.fossil.Ug4
    public Nt3<Yg4> a(boolean z) {
        t();
        Nt3<Yg4> c2 = c();
        this.h.execute(Pg4.a(this, z));
        return c2;
    }

    @DexIgnore
    public final Nt3<Yg4> c() {
        Ot3 ot3 = new Ot3();
        Xg4 xg4 = new Xg4(this.d, ot3);
        synchronized (this.g) {
            this.k.add(xg4);
        }
        return ot3.a();
    }

    @DexIgnore
    public final Void d() throws Vg4, IOException {
        this.j = null;
        Fh4 m2 = m();
        if (m2.k()) {
            try {
                this.b.e(i(), m2.d(), o(), m2.f());
            } catch (K64 e2) {
                throw new Vg4("Failed to delete a Firebase Installation.", Vg4.Ai.BAD_CONFIG);
            }
        }
        p(m2.r());
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Ug4
    public Nt3<Void> delete() {
        return Qt3.c(this.h, Qg4.a(this));
    }

    @DexIgnore
    public final void e(boolean z) {
        Fh4 n = n();
        if (z) {
            n = n.p();
        }
        x(n);
        this.i.execute(Sg4.a(this, z));
    }

    @DexIgnore
    public final String f() {
        String str = this.j;
        if (str != null) {
            return str;
        }
        Fh4 n = n();
        this.i.execute(Rg4.a(this));
        return n.d();
    }

    @DexIgnore
    public final void g(boolean z) {
        Fh4 fh4;
        Fh4 m2 = m();
        try {
            if (m2.i() || m2.l()) {
                fh4 = v(m2);
            } else if (z || this.d.b(m2)) {
                fh4 = h(m2);
            } else {
                return;
            }
            p(fh4);
            if (fh4.k()) {
                this.j = fh4.d();
            }
            if (fh4.i()) {
                w(fh4, new Vg4(Vg4.Ai.BAD_CONFIG));
            } else if (fh4.j()) {
                w(fh4, new IOException("cleared fid due to auth error"));
            } else {
                x(fh4);
            }
        } catch (IOException e2) {
            w(m2, e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ug4
    public Nt3<String> getId() {
        t();
        Ot3 ot3 = new Ot3();
        ot3.e(f());
        return ot3.a();
    }

    @DexIgnore
    public final Fh4 h(Fh4 fh4) throws IOException {
        Kh4 f2 = this.b.f(i(), fh4.d(), o(), fh4.f());
        int i2 = Bi.b[f2.b().ordinal()];
        if (i2 == 1) {
            return fh4.o(f2.c(), f2.d(), this.d.a());
        }
        if (i2 == 2) {
            return fh4.q("BAD CONFIG");
        }
        if (i2 == 3) {
            this.j = null;
            return fh4.r();
        }
        throw new IOException();
    }

    @DexIgnore
    public String i() {
        return this.a.j().b();
    }

    @DexIgnore
    public String j() {
        return this.a.j().c();
    }

    @DexIgnore
    public final Fh4 m() {
        Fh4 c2;
        synchronized (l) {
            Og4 a2 = Og4.a(this.a.g(), "generatefid.lock");
            try {
                c2 = this.c.c();
            } finally {
                if (a2 != null) {
                    a2.b();
                }
            }
        }
        return c2;
    }

    @DexIgnore
    public final Fh4 n() {
        Fh4 c2;
        synchronized (l) {
            Og4 a2 = Og4.a(this.a.g(), "generatefid.lock");
            try {
                c2 = this.c.c();
                if (c2.j()) {
                    String u = u(c2);
                    Eh4 eh4 = this.c;
                    c2 = c2.t(u);
                    eh4.a(c2);
                }
            } finally {
                if (a2 != null) {
                    a2.b();
                }
            }
        }
        return c2;
    }

    @DexIgnore
    public String o() {
        return this.a.j().e();
    }

    @DexIgnore
    public final void p(Fh4 fh4) {
        synchronized (l) {
            Og4 a2 = Og4.a(this.a.g(), "generatefid.lock");
            try {
                this.c.a(fh4);
            } finally {
                if (a2 != null) {
                    a2.b();
                }
            }
        }
    }

    @DexIgnore
    public final void t() {
        Rc2.g(j());
        Rc2.g(o());
        Rc2.g(i());
        Rc2.b(Bh4.d(j()), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        Rc2.b(Bh4.c(i()), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }

    @DexIgnore
    public final String u(Fh4 fh4) {
        if ((!this.a.i().equals("CHIME_ANDROID_SDK") && !this.a.q()) || !fh4.m()) {
            return this.f.a();
        }
        String f2 = this.e.f();
        return TextUtils.isEmpty(f2) ? this.f.a() : f2;
    }

    @DexIgnore
    public final Fh4 v(Fh4 fh4) throws IOException {
        Jh4 d2 = this.b.d(i(), fh4.d(), o(), j(), fh4.d().length() == 11 ? this.e.i() : null);
        int i2 = Bi.a[d2.e().ordinal()];
        if (i2 == 1) {
            return fh4.s(d2.c(), d2.d(), this.d.a(), d2.b().c(), d2.b().d());
        }
        if (i2 == 2) {
            return fh4.q("BAD CONFIG");
        }
        throw new IOException();
    }

    @DexIgnore
    public final void w(Fh4 fh4, Exception exc) {
        synchronized (this.g) {
            Iterator<Ah4> it = this.k.iterator();
            while (it.hasNext()) {
                if (it.next().a(fh4, exc)) {
                    it.remove();
                }
            }
        }
    }

    @DexIgnore
    public final void x(Fh4 fh4) {
        synchronized (this.g) {
            Iterator<Ah4> it = this.k.iterator();
            while (it.hasNext()) {
                if (it.next().b(fh4)) {
                    it.remove();
                }
            }
        }
    }
}
