package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kx4 implements MembersInjector<Ix4> {
    @DexIgnore
    public static void a(Ix4 ix4, Po4 po4) {
        ix4.i = po4;
    }
}
