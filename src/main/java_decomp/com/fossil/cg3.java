package com.fossil;

import com.fossil.Qb3;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cg3 extends Oc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Ii b;

    @DexIgnore
    public Cg3(Qb3 qb3, Qb3.Ii ii) {
        this.b = ii;
    }

    @DexIgnore
    @Override // com.fossil.Nc3
    public final void onMapLongClick(LatLng latLng) {
        this.b.onMapLongClick(latLng);
    }
}
