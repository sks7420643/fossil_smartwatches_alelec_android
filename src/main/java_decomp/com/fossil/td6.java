package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Td6 implements Factory<DashboardActiveTimePresenter> {
    @DexIgnore
    public static DashboardActiveTimePresenter a(Qd6 qd6, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        return new DashboardActiveTimePresenter(qd6, summariesRepository, fitnessDataRepository, userRepository, u04);
    }
}
