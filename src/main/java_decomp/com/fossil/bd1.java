package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bd1 {
    @DexIgnore
    public Ad1 a(Object obj, Mb1 mb1, int i, int i2, Map<Class<?>, Sb1<?>> map, Class<?> cls, Class<?> cls2, Ob1 ob1) {
        return new Ad1(obj, mb1, i, i2, map, cls, cls2, ob1);
    }
}
