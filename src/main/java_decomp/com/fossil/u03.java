package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U03 {
    @DexIgnore
    public static /* final */ S03<?> a; // = new R03();
    @DexIgnore
    public static /* final */ S03<?> b; // = c();

    @DexIgnore
    public static S03<?> a() {
        return a;
    }

    @DexIgnore
    public static S03<?> b() {
        S03<?> s03 = b;
        if (s03 != null) {
            return s03;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    @DexIgnore
    public static S03<?> c() {
        try {
            return (S03) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
