package com.fossil;

import com.fossil.Ix1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ro extends Mj {
    @DexIgnore
    public byte[] E;
    @DexIgnore
    public long F;
    @DexIgnore
    public float G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public int K;
    @DexIgnore
    public N6 L;
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ float R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ro(K5 k5, I60 i60, Yp yp, boolean z, short s, float f, String str, boolean z2, int i) {
        super(k5, i60, yp, s, (i & 64) != 0 ? E.a("UUID.randomUUID().toString()") : str, (i & 128) != 0 ? true : z2);
        f = (i & 32) != 0 ? 0.001f : f;
        this.Q = z;
        this.R = f;
        this.E = new byte[0];
        this.J = true;
        this.L = N6.d;
    }

    @DexIgnore
    public static final /* synthetic */ void J(Ro ro, long j, long j2) {
        long N2 = ro.N();
        if (1 <= j && N2 >= j) {
            long b = Ix1.a.b(ro.E, Ix1.Ai.CRC32);
            ro.G(j);
            if (j2 != b) {
                K5 k5 = ro.w;
                I60 i60 = ro.x;
                String str = k5.x;
                short s = ro.D;
                byte[] bArr = ro.E;
                Lp.h(ro, new Oi(k5, i60, new J0(str, J0.CREATOR.b(s), J0.CREATOR.a(s), bArr, Hy1.o(bArr.length), Ix1.a.b(ro.E, Ix1.Ai.CRC32), 0, false), ro.z), new Tn(ro), new Fo(ro), null, null, null, 56, null);
            } else if (j == ro.N()) {
                Lp.j(ro, Hs.n, null, 2, null);
            } else {
                ro.Q();
            }
        } else {
            ro.G(0);
            ro.Q();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        if (!(!(this.E.length == 0))) {
            l(Nr.a(this.v, null, Zq.b, null, null, 13));
        } else if (this.Q) {
            Lp.j(this, Hs.m, null, 2, null);
        } else {
            Lp.j(this, Hs.o, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Mj
    public JSONObject C() {
        return G80.k(G80.k(G80.k(super.C(), Jd0.I, Integer.valueOf(this.E.length)), Jd0.J, Long.valueOf(Ix1.a.b(this.E, Ix1.Ai.CRC32))), Jd0.x0, Boolean.valueOf(this.Q));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void D() {
        try {
            this.E = M();
            this.F = 0;
            this.H = 0;
            this.I = 0;
            this.J = true;
            this.K = 0;
            this.L = N6.d;
        } catch (Sx1 e) {
            D90.i.i(e);
            l(Nr.a(this.v, null, Zq.q, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(G80.k(super.E(), Jd0.o4, Long.valueOf(Math.max(this.N - this.M, 0L))), Jd0.p4, Long.valueOf(Math.max(this.P - this.O, 0L)));
    }

    @DexIgnore
    public final void G(long j) {
        this.H = this.I;
        this.I = j;
        this.F = j;
    }

    @DexIgnore
    public final void H(Oi oi) {
        G(oi.D);
        long j = this.I;
        this.F = j;
        float N2 = (((float) j) * 1.0f) / ((float) N());
        if (Math.abs(N2 - this.G) > this.R || N2 == 1.0f) {
            this.G = N2;
            d(N2);
        }
        Lp.j(this, Hs.m, null, 2, null);
    }

    @DexIgnore
    public final Zs L() {
        if (this.N == 0) {
            this.N = System.currentTimeMillis();
        }
        if (this.O == 0) {
            this.O = System.currentTimeMillis();
        }
        long N2 = N();
        long j = this.I;
        short s = this.D;
        byte[] k = Dm7.k(this.E, (int) j, (int) N());
        K5 k5 = this.w;
        Zs zs = new Zs(s, new Wr(k, Math.min(k5.m, k5.s), this.L), this.w);
        Wl wl = new Wl(this, N2 - j);
        if (!zs.t) {
            zs.o.add(wl);
        }
        zs.o(new Jm(this));
        zs.c(new Vm(this));
        Zs zs2 = zs;
        zs2.s = w();
        return zs2;
    }

    @DexIgnore
    public abstract byte[] M();

    @DexIgnore
    public final long N() {
        return Hy1.o(this.E.length);
    }

    @DexIgnore
    public Mv O() {
        return new Mv(this.D, Hs.n, this.w, 0, 8);
    }

    @DexIgnore
    public void P() {
        l(Nr.a(this.v, null, Zq.b, null, null, 13));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0032, code lost:
        if (r2 < 3) goto L_0x0008;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Q() {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            boolean r2 = r6.J
            if (r2 == 0) goto L_0x0012
            r6.J = r0
        L_0x0008:
            r0 = 1
        L_0x0009:
            if (r0 == 0) goto L_0x0035
            com.fossil.Hs r0 = com.fossil.Hs.m
            r2 = 2
            com.fossil.Lp.j(r6, r0, r1, r2, r1)
        L_0x0011:
            return
        L_0x0012:
            long r2 = r6.I
            long r4 = r6.H
            long r2 = r2 - r4
            float r2 = (float) r2
            r3 = 1065353216(0x3f800000, float:1.0)
            float r2 = r2 * r3
            long r4 = r6.N()
            float r3 = (float) r4
            float r2 = r2 / r3
            r3 = 1008981770(0x3c23d70a, float:0.01)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x002b
            r6.K = r0
            goto L_0x0008
        L_0x002b:
            int r2 = r6.K
            int r2 = r2 + 1
            r6.K = r2
            r3 = 3
            if (r2 >= r3) goto L_0x0009
            goto L_0x0008
        L_0x0035:
            com.fossil.Nr r0 = r6.v
            com.fossil.Zq r2 = com.fossil.Zq.h
            r5 = 13
            r3 = r1
            r4 = r1
            com.fossil.Nr r0 = com.fossil.Nr.a(r0, r1, r2, r3, r4, r5)
            r6.l(r0)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ro.Q():void");
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Fs b(Hs hs) {
        int i = Lk.a[hs.ordinal()];
        if (i == 1) {
            Hv hv = new Hv(this.D, this.w, 0, 4);
            hv.o(new Xk(this));
            return hv;
        } else if (i == 2) {
            if (this.M == 0) {
                this.M = System.currentTimeMillis();
            }
            Kv kv = new Kv(this.I, N() - this.I, N(), this.D, this.w, 0, 32);
            kv.o(new Kl(this));
            return kv;
        } else if (i == 3) {
            Mv O2 = O();
            O2.o(new Hn(this));
            return O2;
        } else if (i != 4) {
            return null;
        } else {
            return L();
        }
    }
}
