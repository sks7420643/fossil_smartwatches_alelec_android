package com.fossil;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B33 {
    @DexIgnore
    public static /* final */ B33 c; // = new B33();
    @DexIgnore
    public /* final */ E33 a; // = new D23();
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, F33<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static B33 a() {
        return c;
    }

    @DexIgnore
    public final <T> F33<T> b(Class<T> cls) {
        H13.f(cls, "messageType");
        F33<T> f33 = (F33<T>) this.b.get(cls);
        if (f33 != null) {
            return f33;
        }
        F33<T> zza = this.a.zza(cls);
        H13.f(cls, "messageType");
        H13.f(zza, "schema");
        F33<T> f332 = (F33<T>) this.b.putIfAbsent(cls, zza);
        return f332 != null ? f332 : zza;
    }

    @DexIgnore
    public final <T> F33<T> c(T t) {
        return b(t.getClass());
    }
}
