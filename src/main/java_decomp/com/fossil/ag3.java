package com.fossil;

import com.fossil.Qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ag3 extends Sd3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Bi b;

    @DexIgnore
    public Ag3(Qb3 qb3, Qb3.Bi bi) {
        this.b = bi;
    }

    @DexIgnore
    @Override // com.fossil.Rd3
    public final void onCameraIdle() {
        this.b.onCameraIdle();
    }
}
