package com.fossil;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vj1 implements Mb1 {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Mb1 c;

    @DexIgnore
    public Vj1(int i, Mb1 mb1) {
        this.b = i;
        this.c = mb1;
    }

    @DexIgnore
    public static Mb1 c(Context context) {
        return new Vj1(context.getResources().getConfiguration().uiMode & 48, Wj1.c(context));
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        this.c.a(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (!(obj instanceof Vj1)) {
            return false;
        }
        Vj1 vj1 = (Vj1) obj;
        return this.b == vj1.b && this.c.equals(vj1.c);
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        return Jk1.n(this.c, this.b);
    }
}
