package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public List<AddressWrapper> a;
    @DexIgnore
    public Bi b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ Cw5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                int adapterPosition = this.b.getAdapterPosition();
                if (this.b.getAdapterPosition() != -1 && (bi = this.b.d.b) != null) {
                    List list = this.b.d.a;
                    if (list != null) {
                        bi.a((AddressWrapper) list.get(adapterPosition));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Cw5 cw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.d = cw5;
            view.setOnClickListener(new Aii(this));
            View findViewById = view.findViewById(2131362546);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131362386);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                    View findViewById3 = view.findViewById(2131362726);
                    if (findViewById3 != null) {
                        this.c = (ImageView) findViewById3;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final ImageView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(AddressWrapper addressWrapper);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<AddressWrapper> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final AddressWrapper i(int i) {
        if (i == -1) {
            return null;
        }
        List<AddressWrapper> list = this.a;
        if (list == null) {
            Wg6.i();
            throw null;
        } else if (i > list.size()) {
            return null;
        } else {
            List<AddressWrapper> list2 = this.a;
            if (list2 != null) {
                return list2.get(i);
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void j(Ai ai, int i) {
        Wg6.c(ai, "holder");
        List<AddressWrapper> list = this.a;
        if (list != null) {
            AddressWrapper addressWrapper = list.get(i);
            ai.c().setText(addressWrapper.getName());
            ai.a().setText(addressWrapper.getAddress());
            int i2 = Dw5.a[addressWrapper.getType().ordinal()];
            if (i2 == 1) {
                ai.b().setImageDrawable(W6.f(PortfolioApp.get.instance(), 2131230987));
                ai.c().setText(Um5.c(PortfolioApp.get.instance(), 2131886354));
            } else if (i2 == 2) {
                ai.b().setImageDrawable(W6.f(PortfolioApp.get.instance(), 2131230989));
                ai.c().setText(Um5.c(PortfolioApp.get.instance(), 2131886356));
            } else if (i2 == 3) {
                ai.b().setImageDrawable(W6.f(PortfolioApp.get.instance(), 2131230988));
                ai.c().setText(addressWrapper.getName());
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ai k(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558656, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Ai(this, inflate);
    }

    @DexIgnore
    public final void l(List<AddressWrapper> list) {
        Wg6.c(list, "addressList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(Bi bi) {
        Wg6.c(bi, "listener");
        this.b = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        j(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return k(viewGroup, i);
    }
}
