package com.fossil;

import com.fossil.R62;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D82 extends R62 {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public D82(String str) {
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.R62
    public Z52 d() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public T62<Status> e() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public void f() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public void g() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public void h(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public boolean n() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public void q(R62.Ci ci) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.R62
    public void r(R62.Ci ci) {
        throw new UnsupportedOperationException(this.b);
    }
}
