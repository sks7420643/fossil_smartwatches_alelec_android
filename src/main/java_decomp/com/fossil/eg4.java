package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import com.fossil.Dg4;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Eg4 implements Runnable {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ PowerManager.WakeLock c;
    @DexIgnore
    public /* final */ FirebaseInstanceId d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends BroadcastReceiver {
        @DexIgnore
        public Eg4 a;

        @DexIgnore
        public Ai(Eg4 eg4) {
            this.a = eg4;
        }

        @DexIgnore
        public void a() {
            if (FirebaseInstanceId.u()) {
                Log.d("FirebaseInstanceId", "Connectivity change received registered");
            }
            this.a.b().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Eg4 eg4 = this.a;
            if (eg4 != null && eg4.d()) {
                if (FirebaseInstanceId.u()) {
                    Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
                }
                this.a.d.h(this.a, 0);
                this.a.b().unregisterReceiver(this);
                this.a = null;
            }
        }
    }

    @DexIgnore
    public Eg4(FirebaseInstanceId firebaseInstanceId, long j) {
        this.d = firebaseInstanceId;
        this.b = j;
        PowerManager.WakeLock newWakeLock = ((PowerManager) b().getSystemService("power")).newWakeLock(1, "fiid-sync");
        this.c = newWakeLock;
        newWakeLock.setReferenceCounted(false);
    }

    @DexIgnore
    public Context b() {
        return this.d.j().g();
    }

    @DexIgnore
    public final void c(String str) {
        if ("[DEFAULT]".equals(this.d.j().i())) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(this.d.j().i());
                Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Invoking onNewToken for app: ".concat(valueOf) : new String("Invoking onNewToken for app: "));
            }
            Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
            intent.putExtra("token", str);
            Cg4.h(b(), intent);
        }
    }

    @DexIgnore
    public boolean d() {
        ConnectivityManager connectivityManager = (ConnectivityManager) b().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public boolean e() throws IOException {
        Dg4.Ai s = this.d.s();
        if (!this.d.K(s)) {
            return true;
        }
        try {
            String e = this.d.e();
            if (e == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if (s != null && (s == null || e.equals(s.a))) {
                return true;
            }
            c(e);
            return true;
        } catch (IOException e2) {
            if (Ef4.f(e2.getMessage())) {
                String message = e2.getMessage();
                StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 52);
                sb.append("Token retrieval failed: ");
                sb.append(message);
                sb.append(". Will retry token retrieval");
                Log.w("FirebaseInstanceId", sb.toString());
                return false;
            } else if (e2.getMessage() == null) {
                Log.w("FirebaseInstanceId", "Token retrieval failed without exception message. Will retry token retrieval");
                return false;
            } else {
                throw e2;
            }
        } catch (SecurityException e3) {
            Log.w("FirebaseInstanceId", "Token retrieval failed with SecurityException. Will retry token retrieval");
            return false;
        }
    }

    @DexIgnore
    @SuppressLint({"Wakelock"})
    public void run() {
        if (Cg4.b().e(b())) {
            this.c.acquire();
        }
        try {
            this.d.G(true);
            if (!this.d.w()) {
                this.d.G(false);
                if (Cg4.b().e(b())) {
                    this.c.release();
                }
            } else if (!Cg4.b().d(b()) || d()) {
                if (e()) {
                    this.d.G(false);
                } else {
                    this.d.J(this.b);
                }
                if (Cg4.b().e(b())) {
                    this.c.release();
                }
            } else {
                new Ai(this).a();
                if (Cg4.b().e(b())) {
                    this.c.release();
                }
            }
        } catch (IOException e) {
            String message = e.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 93);
            sb.append("Topic sync or token retrieval failed on hard failure exceptions: ");
            sb.append(message);
            sb.append(". Won't retry the operation.");
            Log.e("FirebaseInstanceId", sb.toString());
            this.d.G(false);
            if (Cg4.b().e(b())) {
                this.c.release();
            }
        } catch (Throwable th) {
            if (Cg4.b().e(b())) {
                this.c.release();
            }
            throw th;
        }
    }
}
