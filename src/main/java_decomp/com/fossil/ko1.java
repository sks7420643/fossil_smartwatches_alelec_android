package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ko1 {
    SILENT((byte) 1),
    IMPORTANT((byte) 2),
    PRE_EXISTING((byte) 4),
    ALLOW_USER_POSITIVE_ACTION((byte) 8),
    ALLOW_USER_NEGATIVE_ACTION((byte) 16),
    ALLOW_USER_REPLY_ACTION((byte) 32);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final ko1[] a(String[] strArr) {
            ko1 ko1;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                ko1[] values = ko1.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        ko1 = null;
                        break;
                    }
                    ko1 = values[i];
                    if (pq7.a(ey1.a(ko1), str) || pq7.a(ko1.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (ko1 != null) {
                    arrayList.add(ko1);
                }
            }
            Object[] array = arrayList.toArray(new ko1[0]);
            if (array != null) {
                return (ko1[]) array;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public ko1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
