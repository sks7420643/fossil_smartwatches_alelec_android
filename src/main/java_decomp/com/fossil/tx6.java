package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentActivity;
import com.fossil.X37;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tx6 extends Ey6 implements Gq4 {
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public G37<Ba5> h;
    @DexIgnore
    public Hy6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Tx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            Tx6 tx6 = new Tx6();
            tx6.setArguments(bundle);
            return tx6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity b;

        @DexIgnore
        public Bi(FragmentActivity fragmentActivity) {
            this.b = fragmentActivity;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b, 2130772009);
            FlexibleTextView flexibleTextView = (FlexibleTextView) this.b.findViewById(2131362546);
            if (flexibleTextView != null) {
                flexibleTextView.startAnimation(loadAnimation);
            }
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) this.b.findViewById(2131362405);
            if (flexibleTextView2 != null) {
                flexibleTextView2.startAnimation(loadAnimation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Tx6 b;

        @DexIgnore
        public Ci(Tx6 tx6) {
            this.b = tx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Tx6.K6(this.b).r(1);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Hy6 K6(Tx6 tx6) {
        Hy6 hy6 = tx6.i;
        if (hy6 != null) {
            return hy6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public void L6(Hy6 hy6) {
        Wg6.c(hy6, "presenter");
        this.i = hy6;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        L6((Hy6) obj);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        FragmentActivity activity;
        if (z || (activity = getActivity()) == null) {
            return null;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(activity, 2130772005);
        if (loadAnimation == null) {
            return loadAnimation;
        }
        loadAnimation.setAnimationListener(new Bi(activity));
        return loadAnimation;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Ba5 ba5 = (Ba5) Aq0.f(layoutInflater, 2131558605, viewGroup, false, A6());
        this.h = new G37<>(this, ba5);
        Wg6.b(ba5, "binding");
        return ba5.n();
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        RTLImageView rTLImageView;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Ba5> g37 = this.h;
        if (g37 != null) {
            Ba5 a2 = g37.a();
            if (!(a2 == null || (rTLImageView = a2.s) == null)) {
                rTLImageView.setOnClickListener(new Ci(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                G37<Ba5> g372 = this.h;
                if (g372 != null) {
                    Ba5 a3 = g372.a();
                    if (a3 != null && (dashBar = a3.u) != null) {
                        X37.Ai ai = X37.a;
                        Wg6.b(dashBar, "this");
                        ai.d(dashBar, z, 500);
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
