package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class K24<E> implements Iterable<E> {
    @DexIgnore
    public /* final */ G14<Iterable<E>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends K24<E> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Iterable iterable, Iterable iterable2) {
            super(iterable);
            this.c = iterable2;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<E> iterator() {
            return this.c.iterator();
        }
    }

    @DexIgnore
    public K24() {
        this.b = G14.absent();
    }

    @DexIgnore
    public K24(Iterable<E> iterable) {
        I14.l(iterable);
        this.b = G14.fromNullable(this == iterable ? null : iterable);
    }

    @DexIgnore
    public static <E> K24<E> b(Iterable<E> iterable) {
        return iterable instanceof K24 ? (K24) iterable : new Ai(iterable, iterable);
    }

    @DexIgnore
    public final K24<E> a(J14<? super E> j14) {
        return b(O34.d(c(), j14));
    }

    @DexIgnore
    public final Iterable<E> c() {
        return this.b.or((G14<Iterable<E>>) this);
    }

    @DexIgnore
    public final H34<E> d() {
        return H34.copyOf(c());
    }

    @DexIgnore
    public String toString() {
        return O34.j(c());
    }
}
