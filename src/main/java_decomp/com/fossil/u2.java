package com.fossil;

import android.os.Parcel;
import com.mapped.L70;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U2 extends C2 {
    @DexIgnore
    public static /* final */ T2 CREATOR; // = new T2(null);
    @DexIgnore
    public /* final */ L70 e;

    @DexIgnore
    public U2(byte b, L70 l70) {
        super(Lt.g, b, false, 4);
        this.e = l70;
    }

    @DexIgnore
    public /* synthetic */ U2(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.e = L70.valueOf(readString);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
