package com.fossil;

import com.portfolio.platform.buddy_challenge.analytic.BCAnalytic;
import com.portfolio.platform.helper.AnalyticsHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zr4 implements Factory<BCAnalytic> {
    @DexIgnore
    public /* final */ Provider<AnalyticsHelper> a;
    @DexIgnore
    public /* final */ Provider<Tt4> b;

    @DexIgnore
    public Zr4(Provider<AnalyticsHelper> provider, Provider<Tt4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Zr4 a(Provider<AnalyticsHelper> provider, Provider<Tt4> provider2) {
        return new Zr4(provider, provider2);
    }

    @DexIgnore
    public static BCAnalytic c(AnalyticsHelper analyticsHelper, Tt4 tt4) {
        return new BCAnalytic(analyticsHelper, tt4);
    }

    @DexIgnore
    public BCAnalytic b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
