package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Vy4 {
    EDIT,
    ADD_FRIENDS,
    LEAVE,
    ABOUT,
    LEADER_BOARD
}
