package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mf extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Bi b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Mf(Bi bi) {
        super(1);
        this.b = bi;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.b.H;
        ArrayList<J0> arrayList = ((Iv) fs).X;
        ArrayList arrayList2 = new ArrayList();
        for (T t : arrayList) {
            if (t.g > 0) {
                arrayList2.add(t);
            }
        }
        copyOnWriteArrayList.addAll(arrayList2);
        Bi bi = this.b;
        int i = 0;
        for (J0 j0 : bi.H) {
            i = ((int) j0.g) + i;
        }
        bi.J = Hy1.o(i);
        this.b.Z();
        this.b.Y();
        return Cd6.a;
    }
}
