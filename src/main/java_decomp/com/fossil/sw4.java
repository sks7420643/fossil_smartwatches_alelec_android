package com.fossil;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Xw4;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sw4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public G37<F35> g;
    @DexIgnore
    public BCFindFriendsViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public Xw4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Sw4.s;
        }

        @DexIgnore
        public final Sw4 b(String str) {
            Sw4 sw4 = new Sw4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_history_id_extra", str);
            sw4.setArguments(bundle);
            return sw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 b;

        @DexIgnore
        public Bi(Sw4 sw4) {
            this.b = sw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            Sw4.Q6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 b;

        @DexIgnore
        public Ci(Sw4 sw4) {
            this.b = sw4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Sw4.Q6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ F35 a;
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 b;

        @DexIgnore
        public Di(F35 f35, Sw4 sw4) {
            this.a = f35;
            this.b = sw4;
        }

        @DexIgnore
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            Wg6.c(textView, "v");
            if (i != 3) {
                return false;
            }
            Sw4 sw4 = this.b;
            FlexibleEditText flexibleEditText = this.a.s;
            Wg6.b(flexibleEditText, "etSearch");
            sw4.U6(flexibleEditText);
            Sw4.Q6(this.b).s();
            Sw4.Q6(this.b).u(textView.getText().toString());
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 b;

        @DexIgnore
        public Ei(Sw4 sw4) {
            this.b = sw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity;
            if (!(this.b.k == null || !this.b.l || (activity = this.b.getActivity()) == null)) {
                activity.setResult(-1);
            }
            FragmentActivity activity2 = this.b.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Xw4.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 a;

        @DexIgnore
        public Fi(Sw4 sw4) {
            this.a = sw4;
        }

        @DexIgnore
        @Override // com.fossil.Xw4.Bi
        public void a(Xs4 xs4, int i) {
            Wg6.c(xs4, "friend");
            Sw4.Q6(this.a).v(xs4, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnFocusChangeListener {
        @DexIgnore
        public static /* final */ Gi b; // = new Gi();

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                Xr4.a.f(PortfolioApp.get.instance());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 a;

        @DexIgnore
        public Hi(Sw4 sw4) {
            this.a = sw4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Wg6.b(bool, "it");
            if (bool.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Kz4<List<Xs4>>> {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 a;

        @DexIgnore
        public Ii(Sw4 sw4) {
            this.a = sw4;
        }

        @DexIgnore
        public final void a(Kz4<List<Xs4>> kz4) {
            FlexibleTextView flexibleTextView;
            ProgressBar progressBar;
            F35 f35 = (F35) Sw4.N6(this.a).a();
            if (!(f35 == null || (progressBar = f35.x) == null)) {
                progressBar.setVisibility(8);
            }
            F35 f352 = (F35) Sw4.N6(this.a).a();
            if (!(f352 == null || (flexibleTextView = f352.u) == null)) {
                flexibleTextView.setVisibility(8);
            }
            List<Xs4> c = kz4.c();
            if (c == null) {
                ServerError a2 = kz4.a();
                F35 f353 = (F35) Sw4.N6(this.a).a();
                if (f353 != null) {
                    FlexibleTextView flexibleTextView2 = f353.t;
                    Wg6.b(flexibleTextView2, "ftvNotFound");
                    flexibleTextView2.setText(String.valueOf(a2));
                }
            } else if (c.isEmpty()) {
                F35 f354 = (F35) Sw4.N6(this.a).a();
                if (f354 != null) {
                    if (this.a.k == null) {
                        FlexibleEditText flexibleEditText = f354.s;
                        Wg6.b(flexibleEditText, "etSearch");
                        String valueOf = String.valueOf(flexibleEditText.getText());
                        Hr7 hr7 = Hr7.a;
                        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886286);
                        Wg6.b(c2, "LanguageHelper.getString\u2026or__NothingFoundForInput)");
                        String format = String.format(c2, Arrays.copyOf(new Object[]{valueOf}, 1));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                        FlexibleTextView flexibleTextView3 = f354.t;
                        Wg6.b(flexibleTextView3, "ftvNotFound");
                        flexibleTextView3.setText(format);
                    } else {
                        FlexibleTextView flexibleTextView4 = f354.t;
                        Wg6.b(flexibleTextView4, "ftvNotFound");
                        flexibleTextView4.setText("");
                    }
                    FlexibleTextView flexibleTextView5 = f354.t;
                    Wg6.b(flexibleTextView5, "ftvNotFound");
                    flexibleTextView5.setVisibility(0);
                }
            } else {
                Xw4 xw4 = this.a.j;
                if (xw4 != null) {
                    xw4.l(c);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Kz4<List<Xs4>> kz4) {
            a(kz4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<Lc6<? extends Kz4<Boolean>, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 a;

        @DexIgnore
        public Ji(Sw4 sw4) {
            this.a = sw4;
        }

        @DexIgnore
        public final void a(Lc6<Kz4<Boolean>, Integer> lc6) {
            String str;
            Boolean c = lc6.getFirst().c();
            int intValue = lc6.getSecond().intValue();
            if (c != null) {
                Sw4 sw4 = this.a;
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886332);
                Wg6.b(c2, "LanguageHelper.getString\u2026Label__FriendRequestSent)");
                sw4.T6(c2);
                Xw4 xw4 = this.a.j;
                if (xw4 != null) {
                    xw4.m(intValue);
                    return;
                }
                return;
            }
            ServerError a2 = lc6.getFirst().a();
            Integer code = a2 != null ? a2.getCode() : null;
            if (code != null && code.intValue() == 404001) {
                this.a.l = true;
                Xw4 xw42 = this.a.j;
                if (xw42 != null) {
                    xw42.m(intValue);
                }
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                String c3 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                Wg6.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c4 = Um5.c(PortfolioApp.get.instance(), 2131886299);
                Wg6.b(c4, "LanguageHelper.getString\u2026t__ThisAccountWasDeleted)");
                s37.E(childFragmentManager, c3, c4);
            } else if (code != null && code.intValue() == 409001) {
                Sw4 sw42 = this.a;
                String c5 = Um5.c(PortfolioApp.get.instance(), 2131886332);
                Wg6.b(c5, "LanguageHelper.getString\u2026Label__FriendRequestSent)");
                sw42.T6(c5);
                Xw4 xw43 = this.a.j;
                if (xw43 != null) {
                    xw43.m(intValue);
                }
            } else {
                Jl5 jl5 = Jl5.b;
                ServerError a3 = lc6.getFirst().a();
                Integer code2 = a3 != null ? a3.getCode() : null;
                ServerError a4 = lc6.getFirst().a();
                if (a4 == null || (str = a4.getMessage()) == null) {
                    str = "";
                }
                String e = jl5.e(code2, str);
                S37 s372 = S37.c;
                FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager2, "childFragmentManager");
                s372.g(code, e, childFragmentManager2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Kz4<Boolean>, ? extends Integer> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<Yy4> {
        @DexIgnore
        public /* final */ /* synthetic */ Sw4 a;

        @DexIgnore
        public Ki(Sw4 sw4) {
            this.a = sw4;
        }

        @DexIgnore
        public final void a(Yy4 yy4) {
            F35 f35;
            if (yy4 != null) {
                int i = Tw4.a[yy4.ordinal()];
                if (i == 1) {
                    F35 f352 = (F35) Sw4.N6(this.a).a();
                    if (f352 != null) {
                        f352.s.setText("");
                        ProgressBar progressBar = f352.x;
                        Wg6.b(progressBar, "prg");
                        progressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView = f352.u;
                        Wg6.b(flexibleTextView, "ftvSearch");
                        flexibleTextView.setVisibility(8);
                    }
                    Xw4 xw4 = this.a.j;
                    if (xw4 != null) {
                        xw4.h();
                    }
                } else if (i == 2) {
                    F35 f353 = (F35) Sw4.N6(this.a).a();
                    if (f353 != null) {
                        FlexibleTextView flexibleTextView2 = f353.t;
                        Wg6.b(flexibleTextView2, "ftvNotFound");
                        flexibleTextView2.setVisibility(8);
                        FlexibleEditText flexibleEditText = f353.s;
                        Wg6.b(flexibleEditText, "etSearch");
                        if (TextUtils.isEmpty(String.valueOf(flexibleEditText.getText()))) {
                            RTLImageView rTLImageView = f353.q;
                            Wg6.b(rTLImageView, "btnSearchClear");
                            rTLImageView.setVisibility(8);
                            return;
                        }
                        RTLImageView rTLImageView2 = f353.q;
                        Wg6.b(rTLImageView2, "btnSearchClear");
                        rTLImageView2.setVisibility(0);
                    }
                } else if (i == 3 && (f35 = (F35) Sw4.N6(this.a).a()) != null) {
                    FlexibleTextView flexibleTextView3 = f35.t;
                    Wg6.b(flexibleTextView3, "ftvNotFound");
                    flexibleTextView3.setVisibility(8);
                    ProgressBar progressBar2 = f35.x;
                    Wg6.b(progressBar2, "prg");
                    progressBar2.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = f35.u;
                    Wg6.b(flexibleTextView4, "ftvSearch");
                    flexibleTextView4.setVisibility(0);
                    Xw4 xw42 = this.a.j;
                    if (xw42 != null) {
                        xw42.h();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Yy4 yy4) {
            a(yy4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<Lc6<? extends String, ? extends String>> {
        @DexIgnore
        public static /* final */ Li a; // = new Li();

        @DexIgnore
        public final void a(Lc6<String, String> lc6) {
            Xr4.a.i(lc6.getFirst(), lc6.getSecond(), PortfolioApp.get.instance());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends String, ? extends String> lc6) {
            a(lc6);
        }
    }

    /*
    static {
        String simpleName = Sw4.class.getSimpleName();
        Wg6.b(simpleName, "BCFindFriendsFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 N6(Sw4 sw4) {
        G37<F35> g37 = sw4.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCFindFriendsViewModel Q6(Sw4 sw4) {
        BCFindFriendsViewModel bCFindFriendsViewModel = sw4.h;
        if (bCFindFriendsViewModel != null) {
            return bCFindFriendsViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T6(String str) {
        LayoutInflater layoutInflater = getLayoutInflater();
        Wg6.b(layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558835, activity != null ? (ViewGroup) activity.findViewById(2131362178) : null);
        View findViewById = inflate.findViewById(2131362386);
        Wg6.b(findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public final void U6(View view) {
        Object systemService = view.getContext().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).hideSoftInputFromWindow(view.getWindowToken(), 0);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public final void V6() {
        G37<F35> g37 = this.g;
        if (g37 != null) {
            F35 a2 = g37.a();
            if (a2 != null) {
                if (this.k != null) {
                    ConstraintLayout constraintLayout = a2.r;
                    Wg6.b(constraintLayout, "cSearch");
                    constraintLayout.setVisibility(8);
                    View view = a2.w;
                    Wg6.b(view, "line");
                    view.setVisibility(0);
                }
                RTLImageView rTLImageView = a2.q;
                Wg6.b(rTLImageView, "btnSearchClear");
                rTLImageView.setVisibility(8);
                a2.q.setOnClickListener(new Bi(this));
                a2.s.setOnFocusChangeListener(Gi.b);
                a2.s.addTextChangedListener(new Ci(this));
                a2.s.setOnEditorActionListener(new Di(a2, this));
                a2.v.setOnClickListener(new Ei(this));
                Xw4 xw4 = new Xw4();
                this.j = xw4;
                xw4.k(new Fi(this));
                RecyclerView recyclerView = a2.z;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                recyclerView.setAdapter(this.j);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W6() {
        BCFindFriendsViewModel bCFindFriendsViewModel = this.h;
        if (bCFindFriendsViewModel != null) {
            bCFindFriendsViewModel.m().h(getViewLifecycleOwner(), new Hi(this));
            BCFindFriendsViewModel bCFindFriendsViewModel2 = this.h;
            if (bCFindFriendsViewModel2 != null) {
                bCFindFriendsViewModel2.n().h(getViewLifecycleOwner(), new Ii(this));
                BCFindFriendsViewModel bCFindFriendsViewModel3 = this.h;
                if (bCFindFriendsViewModel3 != null) {
                    bCFindFriendsViewModel3.p().h(getViewLifecycleOwner(), new Ji(this));
                    BCFindFriendsViewModel bCFindFriendsViewModel4 = this.h;
                    if (bCFindFriendsViewModel4 != null) {
                        bCFindFriendsViewModel4.o().h(getViewLifecycleOwner(), new Ki(this));
                        BCFindFriendsViewModel bCFindFriendsViewModel5 = this.h;
                        if (bCFindFriendsViewModel5 != null) {
                            bCFindFriendsViewModel5.l().h(getViewLifecycleOwner(), Li.a);
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.k = arguments != null ? arguments.getString("challenge_history_id_extra") : null;
        PortfolioApp.get.instance().getIface().T().a(this);
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCFindFriendsViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ndsViewModel::class.java)");
            this.h = (BCFindFriendsViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        F35 f35 = (F35) Aq0.f(layoutInflater, 2131558512, viewGroup, false, A6());
        this.g = new G37<>(this, f35);
        Wg6.b(f35, "binding");
        return f35.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_friend_add", activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        String str = this.k;
        if (str != null) {
            BCFindFriendsViewModel bCFindFriendsViewModel = this.h;
            if (bCFindFriendsViewModel == null) {
                Wg6.n("viewModel");
                throw null;
            } else if (str != null) {
                bCFindFriendsViewModel.q(str);
            } else {
                Wg6.i();
                throw null;
            }
        }
        V6();
        W6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
