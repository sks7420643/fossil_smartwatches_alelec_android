package com.fossil;

import com.fossil.Lz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fz1 extends Lz1 {
    @DexIgnore
    public /* final */ Lz1.Bi a;
    @DexIgnore
    public /* final */ Bz1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Lz1.Ai {
        @DexIgnore
        public Lz1.Bi a;
        @DexIgnore
        public Bz1 b;

        @DexIgnore
        @Override // com.fossil.Lz1.Ai
        public Lz1.Ai a(Bz1 bz1) {
            this.b = bz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Lz1.Ai
        public Lz1.Ai b(Lz1.Bi bi) {
            this.a = bi;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Lz1.Ai
        public Lz1 c() {
            return new Fz1(this.a, this.b, null);
        }
    }

    @DexIgnore
    public /* synthetic */ Fz1(Lz1.Bi bi, Bz1 bz1, Ai ai) {
        this.a = bi;
        this.b = bz1;
    }

    @DexIgnore
    @Override // com.fossil.Lz1
    public Bz1 b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Lz1
    public Lz1.Bi c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Lz1)) {
            return false;
        }
        Lz1.Bi bi = this.a;
        if (bi != null ? bi.equals(((Fz1) obj).a) : ((Fz1) obj).a == null) {
            Bz1 bz1 = this.b;
            if (bz1 == null) {
                if (((Fz1) obj).b == null) {
                    z = true;
                    return z;
                }
            } else if (bz1.equals(((Fz1) obj).b)) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Lz1.Bi bi = this.a;
        int hashCode = bi == null ? 0 : bi.hashCode();
        Bz1 bz1 = this.b;
        if (bz1 != null) {
            i = bz1.hashCode();
        }
        return ((hashCode ^ 1000003) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "ClientInfo{clientType=" + this.a + ", androidClientInfo=" + this.b + "}";
    }
}
