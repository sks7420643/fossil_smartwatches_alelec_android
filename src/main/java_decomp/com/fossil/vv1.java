package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Vv1 {
    WEATHER("weatherSSE"),
    HEART_RATE("hrSSE"),
    STEPS("stepsSSE"),
    DATE("dateSSE"),
    CHANCE_OF_RAIN("chanceOfRainSSE"),
    SECOND_TIMEZONE("timeZone2SSE"),
    ACTIVE_MINUTES("activeMinutesSSE"),
    CALORIES("caloriesSSE"),
    BATTERY("batterySSE");
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Vv1 a(String str) {
            Vv1[] values = Vv1.values();
            for (Vv1 vv1 : values) {
                if (Wg6.a(vv1.a(), str)) {
                    return vv1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Vv1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
