package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Va1<TranscodeType> extends Yi1<Va1<TranscodeType>> implements Cloneable {
    @DexIgnore
    public /* final */ Context G;
    @DexIgnore
    public /* final */ Wa1 H;
    @DexIgnore
    public /* final */ Class<TranscodeType> I;
    @DexIgnore
    public /* final */ Qa1 J;
    @DexIgnore
    public Xa1<?, ? super TranscodeType> K;
    @DexIgnore
    public Object L;
    @DexIgnore
    public List<Ej1<TranscodeType>> M;
    @DexIgnore
    public Va1<TranscodeType> N;
    @DexIgnore
    public Va1<TranscodeType> O;
    @DexIgnore
    public Float P;
    @DexIgnore
    public boolean Q; // = true;
    @DexIgnore
    public boolean R;
    @DexIgnore
    public boolean S;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[Sa1.values().length];
            b = iArr;
            try {
                iArr[Sa1.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[Sa1.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[Sa1.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[Sa1.IMMEDIATE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            int[] iArr2 = new int[ImageView.ScaleType.values().length];
            a = iArr2;
            try {
                iArr2[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[ImageView.ScaleType.FIT_START.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[ImageView.ScaleType.CENTER.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
        }
        */
    }

    /*
    static {
        Fj1 fj1 = (Fj1) ((Fj1) ((Fj1) new Fj1().l(Wc1.b)).e0(Sa1.LOW)).m0(true);
    }
    */

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public Va1(Oa1 oa1, Wa1 wa1, Class<TranscodeType> cls, Context context) {
        this.H = wa1;
        this.I = cls;
        this.G = context;
        this.K = wa1.o(cls);
        this.J = oa1.i();
        B0(wa1.m());
        u0(wa1.n());
    }

    @DexIgnore
    public final Sa1 A0(Sa1 sa1) {
        int i = Ai.b[sa1.ordinal()];
        if (i == 1) {
            return Sa1.NORMAL;
        }
        if (i == 2) {
            return Sa1.HIGH;
        }
        if (i == 3 || i == 4) {
            return Sa1.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + B());
    }

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public final void B0(List<Ej1<Object>> list) {
        for (Ej1<Object> ej1 : list) {
            t0(ej1);
        }
    }

    @DexIgnore
    public <Y extends Qj1<TranscodeType>> Y C0(Y y) {
        E0(y, null, Dk1.b());
        return y;
    }

    @DexIgnore
    public final <Y extends Qj1<TranscodeType>> Y D0(Y y, Ej1<TranscodeType> ej1, Yi1<?> yi1, Executor executor) {
        Ik1.d(y);
        if (this.R) {
            Bj1 v0 = v0(y, ej1, yi1, executor);
            Bj1 i = y.i();
            if (!v0.g(i) || G0(yi1, i)) {
                this.H.l(y);
                y.d(v0);
                this.H.z(y, v0);
            } else {
                Ik1.d(i);
                if (!i.isRunning()) {
                    i.f();
                }
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    @DexIgnore
    public <Y extends Qj1<TranscodeType>> Y E0(Y y, Ej1<TranscodeType> ej1, Executor executor) {
        D0(y, ej1, this, executor);
        return y;
    }

    @DexIgnore
    public Rj1<ImageView, TranscodeType> F0(ImageView imageView) {
        Yi1<?> yi1;
        Jk1.b();
        Ik1.d(imageView);
        if (!T() && R() && imageView.getScaleType() != null) {
            switch (Ai.a[imageView.getScaleType().ordinal()]) {
                case 1:
                    yi1 = i().W();
                    break;
                case 2:
                    yi1 = i().X();
                    break;
                case 3:
                case 4:
                case 5:
                    yi1 = i().Y();
                    break;
                case 6:
                    yi1 = i().X();
                    break;
            }
            Rj1<ImageView, TranscodeType> a2 = this.J.a(imageView, this.I);
            D0(a2, null, yi1, Dk1.b());
            return a2;
        }
        yi1 = this;
        Rj1<ImageView, TranscodeType> a22 = this.J.a(imageView, this.I);
        D0(a22, null, yi1, Dk1.b());
        return a22;
    }

    @DexIgnore
    public final boolean G0(Yi1<?> yi1, Bj1 bj1) {
        return !yi1.M() && bj1.j();
    }

    @DexIgnore
    public Va1<TranscodeType> H0(Ej1<TranscodeType> ej1) {
        this.M = null;
        return t0(ej1);
    }

    @DexIgnore
    public Va1<TranscodeType> I0(Bitmap bitmap) {
        O0(bitmap);
        return u0(Fj1.u0(Wc1.a));
    }

    @DexIgnore
    public Va1<TranscodeType> J0(Uri uri) {
        O0(uri);
        return this;
    }

    @DexIgnore
    public Va1<TranscodeType> K0(File file) {
        O0(file);
        return this;
    }

    @DexIgnore
    public Va1<TranscodeType> L0(Integer num) {
        O0(num);
        return u0(Fj1.v0(Vj1.c(this.G)));
    }

    @DexIgnore
    public Va1<TranscodeType> M0(Object obj) {
        O0(obj);
        return this;
    }

    @DexIgnore
    public Va1<TranscodeType> N0(String str) {
        O0(str);
        return this;
    }

    @DexIgnore
    public final Va1<TranscodeType> O0(Object obj) {
        this.L = obj;
        this.R = true;
        return this;
    }

    @DexIgnore
    public final Bj1 P0(Object obj, Qj1<TranscodeType> qj1, Ej1<TranscodeType> ej1, Yi1<?> yi1, Cj1 cj1, Xa1<?, ? super TranscodeType> xa1, Sa1 sa1, int i, int i2, Executor executor) {
        Context context = this.G;
        Qa1 qa1 = this.J;
        return Hj1.x(context, qa1, obj, this.L, this.I, yi1, i, i2, sa1, qj1, ej1, this.M, cj1, qa1.f(), xa1.e(), executor);
    }

    @DexIgnore
    public Aj1<TranscodeType> Q0() {
        return R0(RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    }

    @DexIgnore
    public Aj1<TranscodeType> R0(int i, int i2) {
        Dj1 dj1 = new Dj1(i, i2);
        E0(dj1, dj1, Dk1.a());
        return dj1;
    }

    @DexIgnore
    @Override // com.fossil.Yi1, java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return y0();
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 d(Yi1 yi1) {
        return u0(yi1);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 i() {
        return y0();
    }

    @DexIgnore
    public Va1<TranscodeType> t0(Ej1<TranscodeType> ej1) {
        if (ej1 != null) {
            if (this.M == null) {
                this.M = new ArrayList();
            }
            this.M.add(ej1);
        }
        return this;
    }

    @DexIgnore
    public Va1<TranscodeType> u0(Yi1<?> yi1) {
        Ik1.d(yi1);
        return (Va1) super.d(yi1);
    }

    @DexIgnore
    public final Bj1 v0(Qj1<TranscodeType> qj1, Ej1<TranscodeType> ej1, Yi1<?> yi1, Executor executor) {
        return w0(new Object(), qj1, ej1, null, this.K, yi1.B(), yi1.x(), yi1.w(), yi1, executor);
    }

    @DexIgnore
    public final Bj1 w0(Object obj, Qj1<TranscodeType> qj1, Ej1<TranscodeType> ej1, Cj1 cj1, Xa1<?, ? super TranscodeType> xa1, Sa1 sa1, int i, int i2, Yi1<?> yi1, Executor executor) {
        Zi1 zi1;
        Zi1 zi12;
        if (this.O != null) {
            Zi1 zi13 = new Zi1(obj, cj1);
            zi1 = zi13;
            zi12 = zi13;
        } else {
            zi1 = cj1;
            zi12 = null;
        }
        Bj1 x0 = x0(obj, qj1, ej1, zi1, xa1, sa1, i, i2, yi1, executor);
        if (zi12 == null) {
            return x0;
        }
        int x = this.O.x();
        int w = this.O.w();
        if (Jk1.s(i, i2) && !this.O.U()) {
            x = yi1.x();
            w = yi1.w();
        }
        Va1<TranscodeType> va1 = this.O;
        zi12.p(x0, va1.w0(obj, qj1, ej1, zi12, va1.K, va1.B(), x, w, this.O, executor));
        return zi12;
    }

    @DexIgnore
    public final Bj1 x0(Object obj, Qj1<TranscodeType> qj1, Ej1<TranscodeType> ej1, Cj1 cj1, Xa1<?, ? super TranscodeType> xa1, Sa1 sa1, int i, int i2, Yi1<?> yi1, Executor executor) {
        int i3;
        int i4;
        Va1<TranscodeType> va1 = this.N;
        if (va1 != null) {
            if (!this.S) {
                Xa1<?, ? super TranscodeType> xa12 = va1.Q ? xa1 : va1.K;
                Sa1 B = this.N.N() ? this.N.B() : A0(sa1);
                int x = this.N.x();
                int w = this.N.w();
                if (!Jk1.s(i, i2) || this.N.U()) {
                    i3 = w;
                    i4 = x;
                } else {
                    int x2 = yi1.x();
                    i3 = yi1.w();
                    i4 = x2;
                }
                Ij1 ij1 = new Ij1(obj, cj1);
                Bj1 P0 = P0(obj, qj1, ej1, yi1, ij1, xa1, sa1, i, i2, executor);
                this.S = true;
                Va1<TranscodeType> va12 = this.N;
                Bj1 w0 = va12.w0(obj, qj1, ej1, ij1, xa12, B, i4, i3, va12, executor);
                this.S = false;
                ij1.o(P0, w0);
                return ij1;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.P == null) {
            return P0(obj, qj1, ej1, yi1, cj1, xa1, sa1, i, i2, executor);
        } else {
            Ij1 ij12 = new Ij1(obj, cj1);
            ij12.o(P0(obj, qj1, ej1, yi1, ij12, xa1, sa1, i, i2, executor), P0(obj, qj1, ej1, yi1.i().l0(this.P.floatValue()), ij12, xa1, A0(sa1), i, i2, executor));
            return ij12;
        }
    }

    @DexIgnore
    public Va1<TranscodeType> y0() {
        Va1<TranscodeType> va1 = (Va1) super.i();
        va1.K = va1.K.d();
        return va1;
    }

    @DexIgnore
    public Va1<TranscodeType> z0(Va1<TranscodeType> va1) {
        this.O = va1;
        return this;
    }
}
