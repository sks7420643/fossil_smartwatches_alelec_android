package com.fossil;

import com.mapped.Wg6;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qr7 extends Pr7 {
    @DexIgnore
    public /* final */ Ai c; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends ThreadLocal<Random> {
        @DexIgnore
        public Random a() {
            return new Random();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ Random initialValue() {
            return a();
        }
    }

    @DexIgnore
    @Override // com.fossil.Pr7
    public Random d() {
        Object obj = this.c.get();
        Wg6.b(obj, "implStorage.get()");
        return (Random) obj;
    }
}
