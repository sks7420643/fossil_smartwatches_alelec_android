package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O10 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ B20 b;
    @DexIgnore
    public /* final */ /* synthetic */ Lp c;
    @DexIgnore
    public /* final */ /* synthetic */ float d;

    @DexIgnore
    public O10(B20 b20, Lp lp, float f) {
        this.b = b20;
        this.c = lp;
        this.d = f;
    }

    @DexIgnore
    public final void run() {
        E60.p0(this.b.c, Ky1.DEBUG, Ey1.a(this.c.y), "Progress: %.4f.", Float.valueOf(this.d));
        this.b.b.y(this.d);
    }
}
