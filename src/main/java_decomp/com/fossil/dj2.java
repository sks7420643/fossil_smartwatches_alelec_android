package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Pc2;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dj2 extends Zc2 implements Z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Dj2> CREATOR; // = new Ej2();
    @DexIgnore
    public /* final */ List<Uh2> b;
    @DexIgnore
    public /* final */ Status c;

    @DexIgnore
    public Dj2(List<Uh2> list, Status status) {
        this.b = Collections.unmodifiableList(list);
        this.c = status;
    }

    @DexIgnore
    @Override // com.fossil.Z62
    public Status a() {
        return this.c;
    }

    @DexIgnore
    public List<Uh2> c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof Dj2)) {
                return false;
            }
            Dj2 dj2 = (Dj2) obj;
            if (!(this.c.equals(dj2.c) && Pc2.a(this.b, dj2.b))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(this.c, this.b);
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a("status", this.c);
        c2.a("dataSources", this.b);
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.y(parcel, 1, c(), false);
        Bd2.t(parcel, 2, a(), i, false);
        Bd2.b(parcel, a2);
    }
}
