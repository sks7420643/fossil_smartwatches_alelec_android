package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ge4 {
    @DexIgnore
    <T> void a(Class<T> cls, Ee4<? super T> ee4);

    @DexIgnore
    <T> void b(Class<T> cls, Executor executor, Ee4<? super T> ee4);
}
