package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G57 {
    @DexIgnore
    public CharSequence a;
    @DexIgnore
    public CharSequence b;
    @DexIgnore
    public CharSequence c;

    @DexIgnore
    public CharSequence a() {
        return this.b;
    }

    @DexIgnore
    public CharSequence b(boolean z) {
        return z ? this.a : this.c;
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        this.b = charSequence;
    }

    @DexIgnore
    public void d(CharSequence charSequence) {
        this.a = charSequence;
    }

    @DexIgnore
    public void e(CharSequence charSequence) {
        this.c = charSequence;
    }
}
