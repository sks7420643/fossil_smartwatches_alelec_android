package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fd1 {
    @DexIgnore
    public /* final */ Map<Mb1, Yc1<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Mb1, Yc1<?>> b; // = new HashMap();

    @DexIgnore
    public Yc1<?> a(Mb1 mb1, boolean z) {
        return b(z).get(mb1);
    }

    @DexIgnore
    public final Map<Mb1, Yc1<?>> b(boolean z) {
        return z ? this.b : this.a;
    }

    @DexIgnore
    public void c(Mb1 mb1, Yc1<?> yc1) {
        b(yc1.p()).put(mb1, yc1);
    }

    @DexIgnore
    public void d(Mb1 mb1, Yc1<?> yc1) {
        Map<Mb1, Yc1<?>> b2 = b(yc1.p());
        if (yc1.equals(b2.get(mb1))) {
            b2.remove(mb1);
        }
    }
}
