package com.fossil;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Wg6;
import com.mapped.Zf;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ka7 extends Iv0<Bi, Di> {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public /* final */ Ai a;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void i6(String str);

        @DexIgnore
        void s3(List<Bi> list);

        @DexIgnore
        void u6(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ DianaWatchFaceUser a;
        @DexIgnore
        public /* final */ List<S87> b;
        @DexIgnore
        public /* final */ Bitmap c;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends com.fossil.S87> */
        /* JADX WARN: Multi-variable type inference failed */
        public Bi(DianaWatchFaceUser dianaWatchFaceUser, List<? extends S87> list, Bitmap bitmap) {
            Wg6.c(dianaWatchFaceUser, "watchFace");
            Wg6.c(list, MessengerShareContentUtility.ELEMENTS);
            this.a = dianaWatchFaceUser;
            this.b = list;
            this.c = bitmap;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.c;
        }

        @DexIgnore
        public final List<S87> b() {
            return this.b;
        }

        @DexIgnore
        public final DianaWatchFaceUser c() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!Wg6.a(this.a, bi.a) || !Wg6.a(this.b, bi.b) || !Wg6.a(this.c, bi.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            DianaWatchFaceUser dianaWatchFaceUser = this.a;
            int hashCode = dianaWatchFaceUser != null ? dianaWatchFaceUser.hashCode() : 0;
            List<S87> list = this.b;
            int hashCode2 = list != null ? list.hashCode() : 0;
            Bitmap bitmap = this.c;
            if (bitmap != null) {
                i = bitmap.hashCode();
            }
            return (((hashCode * 31) + hashCode2) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ViewHolderModel(watchFace=" + this.a + ", elements=" + this.b + ", background=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Zf.Di<Bi> {
        @DexIgnore
        public boolean a(Bi bi, Bi bi2) {
            Wg6.c(bi, "oldItem");
            Wg6.c(bi2, "newItem");
            return Wg6.a(bi.c(), bi2.c());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Zf.Di
        public /* bridge */ /* synthetic */ boolean areContentsTheSame(Bi bi, Bi bi2) {
            return a(bi, bi2);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Zf.Di
        public /* bridge */ /* synthetic */ boolean areItemsTheSame(Bi bi, Bi bi2) {
            return b(bi, bi2);
        }

        @DexIgnore
        public boolean b(Bi bi, Bi bi2) {
            Wg6.c(bi, "oldItem");
            Wg6.c(bi2, "newItem");
            return Wg6.a(bi.c().getId(), bi2.c().getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ke5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ka7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser c;

            @DexIgnore
            public Aii(Di di, DianaWatchFaceUser dianaWatchFaceUser) {
                this.b = di;
                this.c = dianaWatchFaceUser;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.a.i6(this.c.getId());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser c;

            @DexIgnore
            public Bii(Di di, DianaWatchFaceUser dianaWatchFaceUser) {
                this.b = di;
                this.c = dianaWatchFaceUser;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.a.u6(this.c.getId());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Ka7 ka7, Ke5 ke5) {
            super(ke5.b());
            Wg6.c(ke5, "binding");
            this.b = ka7;
            this.a = ke5;
        }

        @DexIgnore
        public final void a(Bi bi) {
            Wg6.c(bi, "viewHolderModel");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = Ka7.b;
            local.d(str, "bind viewHolderModel=" + bi);
            DianaWatchFaceUser c = bi.c();
            FlexibleTextView flexibleTextView = this.a.e;
            Wg6.b(flexibleTextView, "binding.tvTitle");
            flexibleTextView.setText(c.getName());
            this.a.b.setOnClickListener(new Aii(this, c));
            this.a.c.setOnClickListener(new Bii(this, c));
            if (c.getPreviewURL().length() > 0) {
                this.a.g.P(c.getPreviewURL());
            } else {
                this.a.g.Q(bi.b(), bi.a());
            }
        }
    }

    /*
    static {
        String simpleName = Ka7.class.getSimpleName();
        Wg6.b(simpleName, "WatchFaceListAdapter::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ka7(Ai ai) {
        super(new Ci());
        Wg6.c(ai, "mEvent");
        this.a = ai;
    }

    @DexIgnore
    public void i(Di di, int i) {
        Wg6.c(di, "holder");
        Object item = getItem(i);
        Wg6.b(item, "getItem(position)");
        di.a((Bi) item);
    }

    @DexIgnore
    public Di j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Ke5 c = Ke5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(c, "ItemFacesBinding.inflate\u2026.context), parent, false)");
        return new Di(this, c);
    }

    @DexIgnore
    public final void k(List<Bi> list) {
        Wg6.c(list, "updatedList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "setData updatedList.size=" + list.size());
        submitList(list);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        i((Di) viewHolder, i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }

    @DexIgnore
    @Override // com.fossil.Iv0
    public void onCurrentListChanged(List<Bi> list, List<Bi> list2) {
        Wg6.c(list, "previousList");
        Wg6.c(list2, "currentList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onCurrentListChanged previous size: " + list.size() + " current size: " + list2.size());
        this.a.s3(list2);
    }
}
