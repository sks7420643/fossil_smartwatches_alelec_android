package com.fossil;

import com.fossil.Ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vv extends Uv {
    @DexIgnore
    public byte[] Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public long S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public /* final */ boolean W;
    @DexIgnore
    public /* final */ byte[] X;
    @DexIgnore
    public byte[] Y;
    @DexIgnore
    public /* final */ long Z;
    @DexIgnore
    public /* final */ float a0;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Vv(short r7, long r8, com.fossil.K5 r10, int r11, float r12, int r13) {
        /*
            r6 = this;
            r0 = r13 & 8
            if (r0 == 0) goto L_0x0074
            r5 = 3
        L_0x0005:
            r0 = r13 & 16
            if (r0 == 0) goto L_0x000c
            r12 = 981668463(0x3a83126f, float:0.001)
        L_0x000c:
            com.fossil.Sv r1 = com.fossil.Sv.k
            com.fossil.Hs r3 = com.fossil.Hs.W
            r0 = r6
            r2 = r7
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            r6.Z = r8
            r6.a0 = r12
            r0 = 0
            byte[] r0 = new byte[r0]
            r6.Q = r0
            r0 = 65535(0xffff, float:9.1834E-41)
            r6.U = r0
            r0 = 11
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.allocate(r0)
            java.nio.ByteOrder r1 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r0 = r0.order(r1)
            com.fossil.Sv r1 = com.fossil.Sv.k
            byte r1 = r1.b
            java.nio.ByteBuffer r0 = r0.put(r1)
            java.nio.ByteBuffer r0 = r0.putShort(r7)
            int r1 = r6.T
            java.nio.ByteBuffer r0 = r0.putInt(r1)
            int r1 = r6.U
            java.nio.ByteBuffer r0 = r0.putInt(r1)
            byte[] r0 = r0.array()
            java.lang.String r1 = "ByteBuffer.allocate(11)\n\u2026gth)\n            .array()"
            com.mapped.Wg6.b(r0, r1)
            r6.X = r0
            r0 = 1
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.allocate(r0)
            java.nio.ByteOrder r1 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r0 = r0.order(r1)
            com.fossil.Sv r1 = com.fossil.Sv.k
            byte r1 = r1.a()
            java.nio.ByteBuffer r0 = r0.put(r1)
            byte[] r0 = r0.array()
            java.lang.String r1 = "ByteBuffer.allocate(1)\n \u2026e())\n            .array()"
            com.mapped.Wg6.b(r0, r1)
            r6.Y = r0
            return
        L_0x0074:
            r5 = r11
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vv.<init>(short, long, com.fossil.K5, int, float, int):void");
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(G80.k(super.A(), Jd0.J, Long.valueOf(Ix1.a.b(this.Q, Ix1.Ai.CRC32))), Jd0.d1, Integer.valueOf(this.Q.length));
    }

    @DexIgnore
    @Override // com.fossil.Ps, com.fossil.Tv
    public byte[] M() {
        return this.X;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.W;
    }

    @DexIgnore
    @Override // com.fossil.Ps, com.fossil.Tv
    public byte[] P() {
        return this.Y;
    }

    @DexIgnore
    @Override // com.fossil.Tv
    public void Q(byte[] bArr) {
        this.Y = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Uv
    public void S(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.R = order.getShort(0);
        this.S = Hy1.o(order.getInt(bArr.length - 4));
        if (!(this.R == R() || this.S == Ix1.a.a(bArr, 0, bArr.length - 4, Ix1.Ai.CRC32))) {
            this.v = Mw.a(this.v, null, null, Lw.l, null, null, 27);
        }
        this.Q = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Uv
    public void T() {
        float length = (((float) this.Q.length) * 1.0f) / ((float) this.Z);
        if (length - this.V > this.a0 || length == 1.0f) {
            this.V = length;
            e(length);
        }
    }

    @DexIgnore
    @Override // com.fossil.Tv, com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(G80.k(super.z(), Jd0.i3, Long.valueOf(this.Z)), Jd0.c1, Integer.valueOf(this.T)), Jd0.d1, Integer.valueOf(this.U));
    }
}
