package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xs6 implements MembersInjector<Ws6> {
    @DexIgnore
    public static void a(Ws6 ws6, Po4 po4) {
        ws6.g = po4;
    }
}
