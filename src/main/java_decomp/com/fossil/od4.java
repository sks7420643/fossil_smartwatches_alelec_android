package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Od4 implements D74 {
    @DexIgnore
    public static /* final */ Od4 a; // = new Od4();

    @DexIgnore
    public static D74 b() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.D74
    public Object a(B74 b74) {
        return M02.f((Context) b74.get(Context.class));
    }
}
