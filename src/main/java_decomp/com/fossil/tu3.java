package com.fossil;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tu3 extends Q62<Object> {

    @DexIgnore
    public interface Ai extends Parcelable {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public abstract void a(Ai ai, int i, int i2);

        @DexIgnore
        public abstract void b(Ai ai);

        @DexIgnore
        public abstract void c(Ai ai, int i, int i2);

        @DexIgnore
        public abstract void d(Ai ai, int i, int i2);
    }
}
