package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bc0 implements Parcelable.Creator<Cc0> {
    @DexIgnore
    public /* synthetic */ Bc0(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Cc0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray == null) {
                createByteArray = new byte[0];
            }
            return new Cc0(readString, createByteArray);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Cc0[] newArray(int i) {
        return new Cc0[i];
    }
}
