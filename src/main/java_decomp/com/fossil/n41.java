package com.fossil;

import android.net.Uri;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N41 {
    @DexIgnore
    public Uri a;
    @DexIgnore
    public Uri b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Uri a;

        @DexIgnore
        public Ai(String str, String str2, Uri uri, String str3) {
            this.a = uri;
        }
    }

    @DexIgnore
    public N41(Uri uri, List<Ai> list, Uri uri2) {
        this.a = uri;
        if (list == null) {
            Collections.emptyList();
        }
        this.b = uri2;
    }
}
