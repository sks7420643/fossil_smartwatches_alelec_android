package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S13<K> implements Map.Entry<K, Object> {
    @DexIgnore
    public Map.Entry<K, Q13> b;

    @DexIgnore
    public S13(Map.Entry<K, Q13> entry) {
        this.b = entry;
    }

    @DexIgnore
    public final Q13 a() {
        return this.b.getValue();
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.b.getKey();
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.b.getValue() == null) {
            return null;
        }
        Q13.e();
        throw null;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof M23) {
            return this.b.getValue().a((M23) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
