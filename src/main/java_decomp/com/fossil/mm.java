package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mm extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Af b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Mm(Af af) {
        super(1);
        this.b = af;
    }

    @DexIgnore
    public final void a(Fs fs) {
        Aw aw = (Aw) fs;
        Af af = this.b;
        byte[] bArr = af.N;
        long j = aw.N;
        byte[] k = Dm7.k(bArr, (int) j, (int) Math.min(af.D, j + aw.O));
        Af af2 = this.b;
        long j2 = aw.N;
        short s = af2.P;
        K5 k5 = af2.w;
        Lp.i(af2, new Dw(s, new Wr(k, Math.min(k5.m, k5.s), N6.k), af2.w), new Uo(af2, k), new Hp(af2), new Ho(af2, k, j2), null, null, 48, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Fs fs) {
        a(fs);
        return Cd6.a;
    }
}
