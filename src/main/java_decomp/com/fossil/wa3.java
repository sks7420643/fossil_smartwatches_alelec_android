package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wa3 implements Parcelable.Creator<La3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ La3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    z6 = Ad2.m(parcel, t);
                    break;
                case 2:
                    z5 = Ad2.m(parcel, t);
                    break;
                case 3:
                    z4 = Ad2.m(parcel, t);
                    break;
                case 4:
                    z3 = Ad2.m(parcel, t);
                    break;
                case 5:
                    z2 = Ad2.m(parcel, t);
                    break;
                case 6:
                    z = Ad2.m(parcel, t);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new La3(z6, z5, z4, z3, z2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ La3[] newArray(int i) {
        return new La3[i];
    }
}
