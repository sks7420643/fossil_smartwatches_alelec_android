package com.fossil;

import android.animation.Animator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M67 implements Animator.AnimatorListener {
    @DexIgnore
    public void onAnimationCancel(Animator animator) {
    }

    @DexIgnore
    public void onAnimationRepeat(Animator animator) {
    }

    @DexIgnore
    public void onAnimationStart(Animator animator) {
    }
}
