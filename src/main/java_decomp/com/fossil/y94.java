package com.fossil;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.dumpapp.plugins.FilesDumperPlugin;
import com.fossil.Ta4;
import com.fossil.Ta4$d$d$a$b$e;
import com.fossil.wearables.fsl.countdown.CountDown;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Explore;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y94 implements Wd4 {
    @DexIgnore
    public static /* final */ Wd4 a; // = new Y94();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Sd4<Ta4.Bi> {
        @DexIgnore
        public static /* final */ Ai a; // = new Ai();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Bi) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Bi bi, Td4 td4) throws IOException {
            td4.f("key", bi.b());
            td4.f("value", bi.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Sd4<Ta4> {
        @DexIgnore
        public static /* final */ Bi a; // = new Bi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4) obj, td4);
        }

        @DexIgnore
        public void b(Ta4 ta4, Td4 td4) throws IOException {
            td4.f("sdkVersion", ta4.i());
            td4.f("gmpAppId", ta4.e());
            td4.c("platform", ta4.h());
            td4.f("installationUuid", ta4.f());
            td4.f("buildVersion", ta4.c());
            td4.f("displayVersion", ta4.d());
            td4.f(Constants.SESSION, ta4.j());
            td4.f("ndkPayload", ta4.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Sd4<Ta4.Ci> {
        @DexIgnore
        public static /* final */ Ci a; // = new Ci();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Ci) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Ci ci, Td4 td4) throws IOException {
            td4.f(FilesDumperPlugin.NAME, ci.b());
            td4.f("orgId", ci.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Sd4<Ta4.Ci.Bii> {
        @DexIgnore
        public static /* final */ Di a; // = new Di();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Ci.Bii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Ci.Bii bii, Td4 td4) throws IOException {
            td4.f("filename", bii.c());
            td4.f("contents", bii.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Sd4<Ta4.Di.Aii> {
        @DexIgnore
        public static /* final */ Ei a; // = new Ei();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Aii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Aii aii, Td4 td4) throws IOException {
            td4.f("identifier", aii.c());
            td4.f("version", aii.f());
            td4.f("displayVersion", aii.b());
            td4.f("organization", aii.e());
            td4.f("installationUuid", aii.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Sd4<Ta4.Di.Aii.Biii> {
        @DexIgnore
        public static /* final */ Fi a; // = new Fi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Aii.Biii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Aii.Biii biii, Td4 td4) throws IOException {
            td4.f("clsId", biii.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Sd4<Ta4.Di.Cii> {
        @DexIgnore
        public static /* final */ Gi a; // = new Gi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Cii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Cii cii, Td4 td4) throws IOException {
            td4.c("arch", cii.b());
            td4.f(DeviceRequestsHelper.DEVICE_INFO_MODEL, cii.f());
            td4.c("cores", cii.c());
            td4.b("ram", cii.h());
            td4.b("diskSpace", cii.d());
            td4.a("simulator", cii.j());
            td4.c("state", cii.i());
            td4.f("manufacturer", cii.e());
            td4.f("modelClass", cii.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements Sd4<Ta4.Di> {
        @DexIgnore
        public static /* final */ Hi a; // = new Hi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di di, Td4 td4) throws IOException {
            td4.f("generator", di.f());
            td4.f("identifier", di.i());
            td4.b("startedAt", di.k());
            td4.f(CountDown.COLUMN_ENDED_AT, di.d());
            td4.a("crashed", di.m());
            td4.f("app", di.b());
            td4.f("user", di.l());
            td4.f("os", di.j());
            td4.f("device", di.c());
            td4.f("events", di.e());
            td4.c("generatorType", di.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements Sd4<Ta4.Di.Dii.Aiii> {
        @DexIgnore
        public static /* final */ Ii a; // = new Ii();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Dii.Aiii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Dii.Aiii aiii, Td4 td4) throws IOException {
            td4.f("execution", aiii.d());
            td4.f("customAttributes", aiii.c());
            td4.f(Explore.COLUMN_BACKGROUND, aiii.b());
            td4.c("uiOrientation", aiii.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements Sd4<Ta4$d$d$a$b$a> {
        @DexIgnore
        public static /* final */ Ji a; // = new Ji();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4$d$d$a$b$a) obj, td4);
        }

        @DexIgnore
        public void b(Ta4$d$d$a$b$a ta4$d$d$a$b$a, Td4 td4) throws IOException {
            td4.b("baseAddress", ta4$d$d$a$b$a.b());
            td4.b("size", ta4$d$d$a$b$a.d());
            td4.f("name", ta4$d$d$a$b$a.c());
            td4.f("uuid", ta4$d$d$a$b$a.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements Sd4<Ta4.Di.Dii.Aiii.Biiii> {
        @DexIgnore
        public static /* final */ Ki a; // = new Ki();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Dii.Aiii.Biiii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Dii.Aiii.Biiii biiii, Td4 td4) throws IOException {
            td4.f("threads", biiii.e());
            td4.f("exception", biiii.c());
            td4.f("signal", biiii.d());
            td4.f("binaries", biiii.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements Sd4<Ta4$d$d$a$b$c> {
        @DexIgnore
        public static /* final */ Li a; // = new Li();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4$d$d$a$b$c) obj, td4);
        }

        @DexIgnore
        public void b(Ta4$d$d$a$b$c ta4$d$d$a$b$c, Td4 td4) throws IOException {
            td4.f("type", ta4$d$d$a$b$c.f());
            td4.f("reason", ta4$d$d$a$b$c.e());
            td4.f("frames", ta4$d$d$a$b$c.c());
            td4.f("causedBy", ta4$d$d$a$b$c.b());
            td4.c("overflowCount", ta4$d$d$a$b$c.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements Sd4<Ta4$d$d$a$b$d> {
        @DexIgnore
        public static /* final */ Mi a; // = new Mi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4$d$d$a$b$d) obj, td4);
        }

        @DexIgnore
        public void b(Ta4$d$d$a$b$d ta4$d$d$a$b$d, Td4 td4) throws IOException {
            td4.f("name", ta4$d$d$a$b$d.d());
            td4.f("code", ta4$d$d$a$b$d.c());
            td4.b("address", ta4$d$d$a$b$d.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements Sd4<Ta4$d$d$a$b$e> {
        @DexIgnore
        public static /* final */ Ni a; // = new Ni();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4$d$d$a$b$e) obj, td4);
        }

        @DexIgnore
        public void b(Ta4$d$d$a$b$e ta4$d$d$a$b$e, Td4 td4) throws IOException {
            td4.f("name", ta4$d$d$a$b$e.d());
            td4.c("importance", ta4$d$d$a$b$e.c());
            td4.f("frames", ta4$d$d$a$b$e.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements Sd4<Ta4$d$d$a$b$e.b> {
        @DexIgnore
        public static /* final */ Oi a; // = new Oi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4$d$d$a$b$e.b) obj, td4);
        }

        @DexIgnore
        public void b(Ta4$d$d$a$b$e.b bVar, Td4 td4) throws IOException {
            td4.b("pc", bVar.e());
            td4.f("symbol", bVar.f());
            td4.f("file", bVar.b());
            td4.b(Constants.JSON_KEY_OFFSET, bVar.d());
            td4.c("importance", bVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi implements Sd4<Ta4.Di.Dii.Ciii> {
        @DexIgnore
        public static /* final */ Pi a; // = new Pi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Dii.Ciii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Dii.Ciii ciii, Td4 td4) throws IOException {
            td4.f(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, ciii.b());
            td4.c("batteryVelocity", ciii.c());
            td4.a("proximityOn", ciii.g());
            td4.c("orientation", ciii.e());
            td4.b("ramUsed", ciii.f());
            td4.b("diskUsed", ciii.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi implements Sd4<Ta4.Di.Dii> {
        @DexIgnore
        public static /* final */ Qi a; // = new Qi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Dii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Dii dii, Td4 td4) throws IOException {
            td4.b("timestamp", dii.e());
            td4.f("type", dii.f());
            td4.f("app", dii.b());
            td4.f("device", dii.c());
            td4.f("log", dii.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri implements Sd4<Ta4.Di.Dii.Diii> {
        @DexIgnore
        public static /* final */ Ri a; // = new Ri();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Dii.Diii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Dii.Diii diii, Td4 td4) throws IOException {
            td4.f("content", diii.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si implements Sd4<Ta4.Di.Eii> {
        @DexIgnore
        public static /* final */ Si a; // = new Si();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Eii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Eii eii, Td4 td4) throws IOException {
            td4.c("platform", eii.c());
            td4.f("version", eii.d());
            td4.f("buildVersion", eii.b());
            td4.a("jailbroken", eii.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti implements Sd4<Ta4.Di.Fii> {
        @DexIgnore
        public static /* final */ Ti a; // = new Ti();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Ta4.Di.Fii) obj, td4);
        }

        @DexIgnore
        public void b(Ta4.Di.Fii fii, Td4 td4) throws IOException {
            td4.f("identifier", fii.b());
        }
    }

    @DexIgnore
    @Override // com.fossil.Wd4
    public void a(Xd4<?> xd4) {
        xd4.a(Ta4.class, Bi.a);
        xd4.a(Z94.class, Bi.a);
        xd4.a(Ta4.Di.class, Hi.a);
        xd4.a(Da4.class, Hi.a);
        xd4.a(Ta4.Di.Aii.class, Ei.a);
        xd4.a(Ea4.class, Ei.a);
        xd4.a(Ta4.Di.Aii.Biii.class, Fi.a);
        xd4.a(Fa4.class, Fi.a);
        xd4.a(Ta4.Di.Fii.class, Ti.a);
        xd4.a(Sa4.class, Ti.a);
        xd4.a(Ta4.Di.Eii.class, Si.a);
        xd4.a(Ra4.class, Si.a);
        xd4.a(Ta4.Di.Cii.class, Gi.a);
        xd4.a(Ga4.class, Gi.a);
        xd4.a(Ta4.Di.Dii.class, Qi.a);
        xd4.a(Ha4.class, Qi.a);
        xd4.a(Ta4.Di.Dii.Aiii.class, Ii.a);
        xd4.a(Ia4.class, Ii.a);
        xd4.a(Ta4.Di.Dii.Aiii.Biiii.class, Ki.a);
        xd4.a(Ja4.class, Ki.a);
        xd4.a(Ta4$d$d$a$b$e.class, Ni.a);
        xd4.a(Na4.class, Ni.a);
        xd4.a(Ta4$d$d$a$b$e.b.class, Oi.a);
        xd4.a(Oa4.class, Oi.a);
        xd4.a(Ta4$d$d$a$b$c.class, Li.a);
        xd4.a(La4.class, Li.a);
        xd4.a(Ta4$d$d$a$b$d.class, Mi.a);
        xd4.a(Ma4.class, Mi.a);
        xd4.a(Ta4$d$d$a$b$a.class, Ji.a);
        xd4.a(Ka4.class, Ji.a);
        xd4.a(Ta4.Bi.class, Ai.a);
        xd4.a(Aa4.class, Ai.a);
        xd4.a(Ta4.Di.Dii.Ciii.class, Pi.a);
        xd4.a(Pa4.class, Pi.a);
        xd4.a(Ta4.Di.Dii.Diii.class, Ri.a);
        xd4.a(Qa4.class, Ri.a);
        xd4.a(Ta4.Ci.class, Ci.a);
        xd4.a(Ba4.class, Ci.a);
        xd4.a(Ta4.Ci.Bii.class, Di.a);
        xd4.a(Ca4.class, Di.a);
    }
}
