package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class R32 extends RuntimeException {
    @DexIgnore
    public R32(String str, Throwable th) {
        super(str, th);
    }
}
