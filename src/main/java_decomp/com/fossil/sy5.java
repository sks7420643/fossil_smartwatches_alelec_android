package com.fossil;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sy5 extends Animation implements Animation.AnimationListener {
    @DexIgnore
    public /* final */ ImageView b;
    @DexIgnore
    public /* final */ CropOverlayView c;
    @DexIgnore
    public /* final */ float[] d; // = new float[8];
    @DexIgnore
    public /* final */ float[] e; // = new float[8];
    @DexIgnore
    public /* final */ RectF f; // = new RectF();
    @DexIgnore
    public /* final */ RectF g; // = new RectF();
    @DexIgnore
    public /* final */ float[] h; // = new float[9];
    @DexIgnore
    public /* final */ float[] i; // = new float[9];
    @DexIgnore
    public /* final */ RectF j; // = new RectF();
    @DexIgnore
    public /* final */ float[] k; // = new float[8];
    @DexIgnore
    public /* final */ float[] l; // = new float[9];

    @DexIgnore
    public Sy5(ImageView imageView, CropOverlayView cropOverlayView) {
        this.b = imageView;
        this.c = cropOverlayView;
        setDuration(300);
        setFillAfter(true);
        setInterpolator(new AccelerateDecelerateInterpolator());
        setAnimationListener(this);
    }

    @DexIgnore
    public void applyTransformation(float f2, Transformation transformation) {
        float[] fArr;
        int i2 = 0;
        RectF rectF = this.j;
        RectF rectF2 = this.f;
        float f3 = rectF2.left;
        RectF rectF3 = this.g;
        rectF.left = f3 + ((rectF3.left - f3) * f2);
        float f4 = rectF2.top;
        rectF.top = f4 + ((rectF3.top - f4) * f2);
        float f5 = rectF2.right;
        rectF.right = f5 + ((rectF3.right - f5) * f2);
        float f6 = rectF2.bottom;
        rectF.bottom = f6 + ((rectF3.bottom - f6) * f2);
        this.c.setCropWindowRect(rectF);
        int i3 = 0;
        while (true) {
            fArr = this.k;
            if (i3 >= fArr.length) {
                break;
            }
            float[] fArr2 = this.d;
            fArr[i3] = ((this.e[i3] - fArr2[i3]) * f2) + fArr2[i3];
            i3++;
        }
        this.c.s(fArr, this.b.getWidth(), this.b.getHeight());
        while (true) {
            float[] fArr3 = this.l;
            if (i2 < fArr3.length) {
                float[] fArr4 = this.h;
                fArr3[i2] = ((this.i[i2] - fArr4[i2]) * f2) + fArr4[i2];
                i2++;
            } else {
                Matrix imageMatrix = this.b.getImageMatrix();
                imageMatrix.setValues(this.l);
                this.b.setImageMatrix(imageMatrix);
                this.b.invalidate();
                this.c.invalidate();
                return;
            }
        }
    }

    @DexIgnore
    public void d(float[] fArr, Matrix matrix) {
        System.arraycopy(fArr, 0, this.e, 0, 8);
        this.g.set(this.c.getCropWindowRect());
        matrix.getValues(this.i);
    }

    @DexIgnore
    public void e(float[] fArr, Matrix matrix) {
        reset();
        System.arraycopy(fArr, 0, this.d, 0, 8);
        this.f.set(this.c.getCropWindowRect());
        matrix.getValues(this.h);
    }

    @DexIgnore
    public void onAnimationEnd(Animation animation) {
        this.b.clearAnimation();
    }

    @DexIgnore
    public void onAnimationRepeat(Animation animation) {
    }

    @DexIgnore
    public void onAnimationStart(Animation animation) {
    }
}
