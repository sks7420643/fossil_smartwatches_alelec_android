package com.fossil;

import android.content.SharedPreferences;
import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Am3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ Xl3 e;

    @DexIgnore
    public Am3(Xl3 xl3, String str, long j) {
        this.e = xl3;
        Rc2.g(str);
        Rc2.a(j > 0);
        this.a = String.valueOf(str).concat(":start");
        this.b = String.valueOf(str).concat(":count");
        this.c = String.valueOf(str).concat(":value");
        this.d = j;
    }

    @DexIgnore
    public final Pair<String, Long> a() {
        long abs;
        this.e.h();
        this.e.h();
        long d2 = d();
        if (d2 == 0) {
            c();
            abs = 0;
        } else {
            abs = Math.abs(d2 - this.e.zzm().b());
        }
        long j = this.d;
        if (abs < j) {
            return null;
        }
        if (abs > (j << 1)) {
            c();
            return null;
        }
        String string = this.e.B().getString(this.c, null);
        long j2 = this.e.B().getLong(this.b, 0);
        c();
        return (string == null || j2 <= 0) ? Xl3.D : new Pair<>(string, Long.valueOf(j2));
    }

    @DexIgnore
    public final void b(String str, long j) {
        this.e.h();
        if (d() == 0) {
            c();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.e.B().getLong(this.b, 0);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = this.e.B().edit();
            edit.putString(this.c, str);
            edit.putLong(this.b, 1);
            edit.apply();
            return;
        }
        long j3 = 1 + j2;
        boolean z = (this.e.k().G0().nextLong() & Long.MAX_VALUE) < Long.MAX_VALUE / j3;
        SharedPreferences.Editor edit2 = this.e.B().edit();
        if (z) {
            edit2.putString(this.c, str);
        }
        edit2.putLong(this.b, j3);
        edit2.apply();
    }

    @DexIgnore
    public final void c() {
        this.e.h();
        long b2 = this.e.zzm().b();
        SharedPreferences.Editor edit = this.e.B().edit();
        edit.remove(this.b);
        edit.remove(this.c);
        edit.putLong(this.a, b2);
        edit.apply();
    }

    @DexIgnore
    public final long d() {
        return this.e.B().getLong(this.a, 0);
    }
}
