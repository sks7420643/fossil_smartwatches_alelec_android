package com.fossil;

import com.mapped.MapPickerViewModel;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hv6 implements Factory<MapPickerViewModel> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<GetUserLocation> b;
    @DexIgnore
    public /* final */ Provider<GetAddress> c;

    @DexIgnore
    public Hv6(Provider<PortfolioApp> provider, Provider<GetUserLocation> provider2, Provider<GetAddress> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Hv6 a(Provider<PortfolioApp> provider, Provider<GetUserLocation> provider2, Provider<GetAddress> provider3) {
        return new Hv6(provider, provider2, provider3);
    }

    @DexIgnore
    public static MapPickerViewModel c(PortfolioApp portfolioApp, GetUserLocation getUserLocation, GetAddress getAddress) {
        return new MapPickerViewModel(portfolioApp, getUserLocation, getAddress);
    }

    @DexIgnore
    public MapPickerViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
