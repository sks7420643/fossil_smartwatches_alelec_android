package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G97 {
    @DexIgnore
    @Vu3("id")
    public String a;
    @DexIgnore
    @Vu3(Firmware.COLUMN_DOWNLOAD_URL)
    public String b;
    @DexIgnore
    @Vu3("checksum")
    public String c;
    @DexIgnore
    @Vu3("uid")
    public String d;
    @DexIgnore
    @Vu3("createdAt")
    public String e;
    @DexIgnore
    @Vu3("updatedAt")
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public int h;

    @DexIgnore
    public G97(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i) {
        Wg6.c(str, "id");
        Wg6.c(str2, Firmware.COLUMN_DOWNLOAD_URL);
        Wg6.c(str3, "checkSum");
        Wg6.c(str4, "uid");
        Wg6.c(str5, "createdAt");
        Wg6.c(str6, "updatedAt");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
        this.h = i;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public final String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof G97) {
                G97 g97 = (G97) obj;
                if (!Wg6.a(this.a, g97.a) || !Wg6.a(this.b, g97.b) || !Wg6.a(this.c, g97.c) || !Wg6.a(this.d, g97.d) || !Wg6.a(this.e, g97.e) || !Wg6.a(this.f, g97.f) || !Wg6.a(this.g, g97.g) || this.h != g97.h) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int f() {
        return this.h;
    }

    @DexIgnore
    public final String g() {
        return this.d;
    }

    @DexIgnore
    public final String h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.f;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.g;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i) * 31) + this.h;
    }

    @DexIgnore
    public final void i(int i) {
        this.h = i;
    }

    @DexIgnore
    public String toString() {
        return "WFBackgroundPhoto(id=" + this.a + ", downloadUrl=" + this.b + ", checkSum=" + this.c + ", uid=" + this.d + ", createdAt=" + this.e + ", updatedAt=" + this.f + ", localPath=" + this.g + ", pinType=" + this.h + ")";
    }
}
