package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintHelper;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Constraints;
import androidx.constraintlayout.widget.Guideline;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fk0 {
    @DexIgnore
    public static /* final */ int[] b; // = {0, 4, 8};
    @DexIgnore
    public static SparseIntArray c;
    @DexIgnore
    public HashMap<Integer, Bi> a; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public int A;
        @DexIgnore
        public int B;
        @DexIgnore
        public int C;
        @DexIgnore
        public int D;
        @DexIgnore
        public int E;
        @DexIgnore
        public int F;
        @DexIgnore
        public int G;
        @DexIgnore
        public int H;
        @DexIgnore
        public int I;
        @DexIgnore
        public int J;
        @DexIgnore
        public int K;
        @DexIgnore
        public int L;
        @DexIgnore
        public int M;
        @DexIgnore
        public int N;
        @DexIgnore
        public int O;
        @DexIgnore
        public int P;
        @DexIgnore
        public float Q;
        @DexIgnore
        public float R;
        @DexIgnore
        public int S;
        @DexIgnore
        public int T;
        @DexIgnore
        public float U;
        @DexIgnore
        public boolean V;
        @DexIgnore
        public float W;
        @DexIgnore
        public float X;
        @DexIgnore
        public float Y;
        @DexIgnore
        public float Z;
        @DexIgnore
        public boolean a;
        @DexIgnore
        public float a0;
        @DexIgnore
        public int b;
        @DexIgnore
        public float b0;
        @DexIgnore
        public int c;
        @DexIgnore
        public float c0;
        @DexIgnore
        public int d;
        @DexIgnore
        public float d0;
        @DexIgnore
        public int e;
        @DexIgnore
        public float e0;
        @DexIgnore
        public int f;
        @DexIgnore
        public float f0;
        @DexIgnore
        public float g;
        @DexIgnore
        public float g0;
        @DexIgnore
        public int h;
        @DexIgnore
        public boolean h0;
        @DexIgnore
        public int i;
        @DexIgnore
        public boolean i0;
        @DexIgnore
        public int j;
        @DexIgnore
        public int j0;
        @DexIgnore
        public int k;
        @DexIgnore
        public int k0;
        @DexIgnore
        public int l;
        @DexIgnore
        public int l0;
        @DexIgnore
        public int m;
        @DexIgnore
        public int m0;
        @DexIgnore
        public int n;
        @DexIgnore
        public int n0;
        @DexIgnore
        public int o;
        @DexIgnore
        public int o0;
        @DexIgnore
        public int p;
        @DexIgnore
        public float p0;
        @DexIgnore
        public int q;
        @DexIgnore
        public float q0;
        @DexIgnore
        public int r;
        @DexIgnore
        public boolean r0;
        @DexIgnore
        public int s;
        @DexIgnore
        public int s0;
        @DexIgnore
        public int t;
        @DexIgnore
        public int t0;
        @DexIgnore
        public float u;
        @DexIgnore
        public int[] u0;
        @DexIgnore
        public float v;
        @DexIgnore
        public String v0;
        @DexIgnore
        public String w;
        @DexIgnore
        public int x;
        @DexIgnore
        public int y;
        @DexIgnore
        public float z;

        @DexIgnore
        public Bi() {
            this.a = false;
            this.e = -1;
            this.f = -1;
            this.g = -1.0f;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = -1;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = 0.5f;
            this.v = 0.5f;
            this.w = null;
            this.x = -1;
            this.y = 0;
            this.z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.A = -1;
            this.B = -1;
            this.C = -1;
            this.D = -1;
            this.E = -1;
            this.F = -1;
            this.G = -1;
            this.H = -1;
            this.I = -1;
            this.J = 0;
            this.K = -1;
            this.L = -1;
            this.M = -1;
            this.N = -1;
            this.O = -1;
            this.P = -1;
            this.Q = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.R = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.S = 0;
            this.T = 0;
            this.U = 1.0f;
            this.V = false;
            this.W = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.X = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.Y = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.Z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.a0 = 1.0f;
            this.b0 = 1.0f;
            this.c0 = Float.NaN;
            this.d0 = Float.NaN;
            this.e0 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f0 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.g0 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.h0 = false;
            this.i0 = false;
            this.j0 = 0;
            this.k0 = 0;
            this.l0 = -1;
            this.m0 = -1;
            this.n0 = -1;
            this.o0 = -1;
            this.p0 = 1.0f;
            this.q0 = 1.0f;
            this.r0 = false;
            this.s0 = -1;
            this.t0 = -1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
            return e();
        }

        @DexIgnore
        public void d(ConstraintLayout.LayoutParams layoutParams) {
            layoutParams.d = this.h;
            layoutParams.e = this.i;
            layoutParams.f = this.j;
            layoutParams.g = this.k;
            layoutParams.h = this.l;
            layoutParams.i = this.m;
            layoutParams.j = this.n;
            layoutParams.k = this.o;
            layoutParams.l = this.p;
            layoutParams.p = this.q;
            layoutParams.q = this.r;
            layoutParams.r = this.s;
            layoutParams.s = this.t;
            ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin = this.D;
            ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin = this.E;
            ((ViewGroup.MarginLayoutParams) layoutParams).topMargin = this.F;
            ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin = this.G;
            layoutParams.x = this.P;
            layoutParams.y = this.O;
            layoutParams.z = this.u;
            layoutParams.A = this.v;
            layoutParams.m = this.x;
            layoutParams.n = this.y;
            layoutParams.o = this.z;
            layoutParams.B = this.w;
            layoutParams.P = this.A;
            layoutParams.Q = this.B;
            layoutParams.E = this.Q;
            layoutParams.D = this.R;
            layoutParams.G = this.T;
            layoutParams.F = this.S;
            layoutParams.S = this.h0;
            layoutParams.T = this.i0;
            layoutParams.H = this.j0;
            layoutParams.I = this.k0;
            layoutParams.L = this.l0;
            layoutParams.M = this.m0;
            layoutParams.J = this.n0;
            layoutParams.K = this.o0;
            layoutParams.N = this.p0;
            layoutParams.O = this.q0;
            layoutParams.R = this.C;
            layoutParams.c = this.g;
            layoutParams.a = this.e;
            layoutParams.b = this.f;
            ((ViewGroup.MarginLayoutParams) layoutParams).width = this.b;
            ((ViewGroup.MarginLayoutParams) layoutParams).height = this.c;
            if (Build.VERSION.SDK_INT >= 17) {
                layoutParams.setMarginStart(this.I);
                layoutParams.setMarginEnd(this.H);
            }
            layoutParams.a();
        }

        @DexIgnore
        public Bi e() {
            Bi bi = new Bi();
            bi.a = this.a;
            bi.b = this.b;
            bi.c = this.c;
            bi.e = this.e;
            bi.f = this.f;
            bi.g = this.g;
            bi.h = this.h;
            bi.i = this.i;
            bi.j = this.j;
            bi.k = this.k;
            bi.l = this.l;
            bi.m = this.m;
            bi.n = this.n;
            bi.o = this.o;
            bi.p = this.p;
            bi.q = this.q;
            bi.r = this.r;
            bi.s = this.s;
            bi.t = this.t;
            bi.u = this.u;
            bi.v = this.v;
            bi.w = this.w;
            bi.A = this.A;
            bi.B = this.B;
            bi.u = this.u;
            bi.u = this.u;
            bi.u = this.u;
            bi.u = this.u;
            bi.u = this.u;
            bi.C = this.C;
            bi.D = this.D;
            bi.E = this.E;
            bi.F = this.F;
            bi.G = this.G;
            bi.H = this.H;
            bi.I = this.I;
            bi.J = this.J;
            bi.K = this.K;
            bi.L = this.L;
            bi.M = this.M;
            bi.N = this.N;
            bi.O = this.O;
            bi.P = this.P;
            bi.Q = this.Q;
            bi.R = this.R;
            bi.S = this.S;
            bi.T = this.T;
            bi.U = this.U;
            bi.V = this.V;
            bi.W = this.W;
            bi.X = this.X;
            bi.Y = this.Y;
            bi.Z = this.Z;
            bi.a0 = this.a0;
            bi.b0 = this.b0;
            bi.c0 = this.c0;
            bi.d0 = this.d0;
            bi.e0 = this.e0;
            bi.f0 = this.f0;
            bi.g0 = this.g0;
            bi.h0 = this.h0;
            bi.i0 = this.i0;
            bi.j0 = this.j0;
            bi.k0 = this.k0;
            bi.l0 = this.l0;
            bi.m0 = this.m0;
            bi.n0 = this.n0;
            bi.o0 = this.o0;
            bi.p0 = this.p0;
            bi.q0 = this.q0;
            bi.s0 = this.s0;
            bi.t0 = this.t0;
            int[] iArr = this.u0;
            if (iArr != null) {
                bi.u0 = Arrays.copyOf(iArr, iArr.length);
            }
            bi.x = this.x;
            bi.y = this.y;
            bi.z = this.z;
            bi.r0 = this.r0;
            return bi;
        }

        @DexIgnore
        public final void f(int i2, ConstraintLayout.LayoutParams layoutParams) {
            this.d = i2;
            this.h = layoutParams.d;
            this.i = layoutParams.e;
            this.j = layoutParams.f;
            this.k = layoutParams.g;
            this.l = layoutParams.h;
            this.m = layoutParams.i;
            this.n = layoutParams.j;
            this.o = layoutParams.k;
            this.p = layoutParams.l;
            this.q = layoutParams.p;
            this.r = layoutParams.q;
            this.s = layoutParams.r;
            this.t = layoutParams.s;
            this.u = layoutParams.z;
            this.v = layoutParams.A;
            this.w = layoutParams.B;
            this.x = layoutParams.m;
            this.y = layoutParams.n;
            this.z = layoutParams.o;
            this.A = layoutParams.P;
            this.B = layoutParams.Q;
            this.C = layoutParams.R;
            this.g = layoutParams.c;
            this.e = layoutParams.a;
            this.f = layoutParams.b;
            this.b = ((ViewGroup.MarginLayoutParams) layoutParams).width;
            this.c = ((ViewGroup.MarginLayoutParams) layoutParams).height;
            this.D = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
            this.E = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
            this.F = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
            this.G = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
            this.Q = layoutParams.E;
            this.R = layoutParams.D;
            this.T = layoutParams.G;
            this.S = layoutParams.F;
            boolean z2 = layoutParams.S;
            this.h0 = z2;
            this.i0 = layoutParams.T;
            this.j0 = layoutParams.H;
            this.k0 = layoutParams.I;
            this.h0 = z2;
            this.l0 = layoutParams.L;
            this.m0 = layoutParams.M;
            this.n0 = layoutParams.J;
            this.o0 = layoutParams.K;
            this.p0 = layoutParams.N;
            this.q0 = layoutParams.O;
            if (Build.VERSION.SDK_INT >= 17) {
                this.H = layoutParams.getMarginEnd();
                this.I = layoutParams.getMarginStart();
            }
        }

        @DexIgnore
        public final void g(int i2, Constraints.LayoutParams layoutParams) {
            f(i2, layoutParams);
            this.U = layoutParams.m0;
            this.X = layoutParams.p0;
            this.Y = layoutParams.q0;
            this.Z = layoutParams.r0;
            this.a0 = layoutParams.s0;
            this.b0 = layoutParams.t0;
            this.c0 = layoutParams.u0;
            this.d0 = layoutParams.v0;
            this.e0 = layoutParams.w0;
            this.f0 = layoutParams.x0;
            this.g0 = layoutParams.y0;
            this.W = layoutParams.o0;
            this.V = layoutParams.n0;
        }

        @DexIgnore
        public final void h(ConstraintHelper constraintHelper, int i2, Constraints.LayoutParams layoutParams) {
            g(i2, layoutParams);
            if (constraintHelper instanceof Barrier) {
                this.t0 = 1;
                Barrier barrier = (Barrier) constraintHelper;
                this.s0 = barrier.getType();
                this.u0 = barrier.getReferencedIds();
            }
        }
    }

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        c = sparseIntArray;
        sparseIntArray.append(Hk0.ConstraintSet_layout_constraintLeft_toLeftOf, 25);
        c.append(Hk0.ConstraintSet_layout_constraintLeft_toRightOf, 26);
        c.append(Hk0.ConstraintSet_layout_constraintRight_toLeftOf, 29);
        c.append(Hk0.ConstraintSet_layout_constraintRight_toRightOf, 30);
        c.append(Hk0.ConstraintSet_layout_constraintTop_toTopOf, 36);
        c.append(Hk0.ConstraintSet_layout_constraintTop_toBottomOf, 35);
        c.append(Hk0.ConstraintSet_layout_constraintBottom_toTopOf, 4);
        c.append(Hk0.ConstraintSet_layout_constraintBottom_toBottomOf, 3);
        c.append(Hk0.ConstraintSet_layout_constraintBaseline_toBaselineOf, 1);
        c.append(Hk0.ConstraintSet_layout_editor_absoluteX, 6);
        c.append(Hk0.ConstraintSet_layout_editor_absoluteY, 7);
        c.append(Hk0.ConstraintSet_layout_constraintGuide_begin, 17);
        c.append(Hk0.ConstraintSet_layout_constraintGuide_end, 18);
        c.append(Hk0.ConstraintSet_layout_constraintGuide_percent, 19);
        c.append(Hk0.ConstraintSet_android_orientation, 27);
        c.append(Hk0.ConstraintSet_layout_constraintStart_toEndOf, 32);
        c.append(Hk0.ConstraintSet_layout_constraintStart_toStartOf, 33);
        c.append(Hk0.ConstraintSet_layout_constraintEnd_toStartOf, 10);
        c.append(Hk0.ConstraintSet_layout_constraintEnd_toEndOf, 9);
        c.append(Hk0.ConstraintSet_layout_goneMarginLeft, 13);
        c.append(Hk0.ConstraintSet_layout_goneMarginTop, 16);
        c.append(Hk0.ConstraintSet_layout_goneMarginRight, 14);
        c.append(Hk0.ConstraintSet_layout_goneMarginBottom, 11);
        c.append(Hk0.ConstraintSet_layout_goneMarginStart, 15);
        c.append(Hk0.ConstraintSet_layout_goneMarginEnd, 12);
        c.append(Hk0.ConstraintSet_layout_constraintVertical_weight, 40);
        c.append(Hk0.ConstraintSet_layout_constraintHorizontal_weight, 39);
        c.append(Hk0.ConstraintSet_layout_constraintHorizontal_chainStyle, 41);
        c.append(Hk0.ConstraintSet_layout_constraintVertical_chainStyle, 42);
        c.append(Hk0.ConstraintSet_layout_constraintHorizontal_bias, 20);
        c.append(Hk0.ConstraintSet_layout_constraintVertical_bias, 37);
        c.append(Hk0.ConstraintSet_layout_constraintDimensionRatio, 5);
        c.append(Hk0.ConstraintSet_layout_constraintLeft_creator, 75);
        c.append(Hk0.ConstraintSet_layout_constraintTop_creator, 75);
        c.append(Hk0.ConstraintSet_layout_constraintRight_creator, 75);
        c.append(Hk0.ConstraintSet_layout_constraintBottom_creator, 75);
        c.append(Hk0.ConstraintSet_layout_constraintBaseline_creator, 75);
        c.append(Hk0.ConstraintSet_android_layout_marginLeft, 24);
        c.append(Hk0.ConstraintSet_android_layout_marginRight, 28);
        c.append(Hk0.ConstraintSet_android_layout_marginStart, 31);
        c.append(Hk0.ConstraintSet_android_layout_marginEnd, 8);
        c.append(Hk0.ConstraintSet_android_layout_marginTop, 34);
        c.append(Hk0.ConstraintSet_android_layout_marginBottom, 2);
        c.append(Hk0.ConstraintSet_android_layout_width, 23);
        c.append(Hk0.ConstraintSet_android_layout_height, 21);
        c.append(Hk0.ConstraintSet_android_visibility, 22);
        c.append(Hk0.ConstraintSet_android_alpha, 43);
        c.append(Hk0.ConstraintSet_android_elevation, 44);
        c.append(Hk0.ConstraintSet_android_rotationX, 45);
        c.append(Hk0.ConstraintSet_android_rotationY, 46);
        c.append(Hk0.ConstraintSet_android_rotation, 60);
        c.append(Hk0.ConstraintSet_android_scaleX, 47);
        c.append(Hk0.ConstraintSet_android_scaleY, 48);
        c.append(Hk0.ConstraintSet_android_transformPivotX, 49);
        c.append(Hk0.ConstraintSet_android_transformPivotY, 50);
        c.append(Hk0.ConstraintSet_android_translationX, 51);
        c.append(Hk0.ConstraintSet_android_translationY, 52);
        c.append(Hk0.ConstraintSet_android_translationZ, 53);
        c.append(Hk0.ConstraintSet_layout_constraintWidth_default, 54);
        c.append(Hk0.ConstraintSet_layout_constraintHeight_default, 55);
        c.append(Hk0.ConstraintSet_layout_constraintWidth_max, 56);
        c.append(Hk0.ConstraintSet_layout_constraintHeight_max, 57);
        c.append(Hk0.ConstraintSet_layout_constraintWidth_min, 58);
        c.append(Hk0.ConstraintSet_layout_constraintHeight_min, 59);
        c.append(Hk0.ConstraintSet_layout_constraintCircle, 61);
        c.append(Hk0.ConstraintSet_layout_constraintCircleRadius, 62);
        c.append(Hk0.ConstraintSet_layout_constraintCircleAngle, 63);
        c.append(Hk0.ConstraintSet_android_id, 38);
        c.append(Hk0.ConstraintSet_layout_constraintWidth_percent, 69);
        c.append(Hk0.ConstraintSet_layout_constraintHeight_percent, 70);
        c.append(Hk0.ConstraintSet_chainUseRtl, 71);
        c.append(Hk0.ConstraintSet_barrierDirection, 72);
        c.append(Hk0.ConstraintSet_constraint_referenced_ids, 73);
        c.append(Hk0.ConstraintSet_barrierAllowsGoneWidgets, 74);
    }
    */

    @DexIgnore
    public static int i(TypedArray typedArray, int i, int i2) {
        int resourceId = typedArray.getResourceId(i, i2);
        return resourceId == -1 ? typedArray.getInt(i, -1) : resourceId;
    }

    @DexIgnore
    public void a(ConstraintLayout constraintLayout) {
        b(constraintLayout);
        constraintLayout.setConstraintSet(null);
    }

    @DexIgnore
    public void b(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        HashSet hashSet = new HashSet(this.a.keySet());
        for (int i = 0; i < childCount; i++) {
            View childAt = constraintLayout.getChildAt(i);
            int id = childAt.getId();
            if (id != -1) {
                if (this.a.containsKey(Integer.valueOf(id))) {
                    hashSet.remove(Integer.valueOf(id));
                    Bi bi = this.a.get(Integer.valueOf(id));
                    if (childAt instanceof Barrier) {
                        bi.t0 = 1;
                    }
                    int i2 = bi.t0;
                    if (i2 != -1 && i2 == 1) {
                        Barrier barrier = (Barrier) childAt;
                        barrier.setId(id);
                        barrier.setType(bi.s0);
                        barrier.setAllowsGoneWidget(bi.r0);
                        int[] iArr = bi.u0;
                        if (iArr != null) {
                            barrier.setReferencedIds(iArr);
                        } else {
                            String str = bi.v0;
                            if (str != null) {
                                int[] f = f(barrier, str);
                                bi.u0 = f;
                                barrier.setReferencedIds(f);
                            }
                        }
                    }
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
                    bi.d(layoutParams);
                    childAt.setLayoutParams(layoutParams);
                    childAt.setVisibility(bi.J);
                    if (Build.VERSION.SDK_INT >= 17) {
                        childAt.setAlpha(bi.U);
                        childAt.setRotation(bi.X);
                        childAt.setRotationX(bi.Y);
                        childAt.setRotationY(bi.Z);
                        childAt.setScaleX(bi.a0);
                        childAt.setScaleY(bi.b0);
                        if (!Float.isNaN(bi.c0)) {
                            childAt.setPivotX(bi.c0);
                        }
                        if (!Float.isNaN(bi.d0)) {
                            childAt.setPivotY(bi.d0);
                        }
                        childAt.setTranslationX(bi.e0);
                        childAt.setTranslationY(bi.f0);
                        if (Build.VERSION.SDK_INT >= 21) {
                            childAt.setTranslationZ(bi.g0);
                            if (bi.V) {
                                childAt.setElevation(bi.W);
                            }
                        }
                    }
                }
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            Bi bi2 = this.a.get(num);
            int i3 = bi2.t0;
            if (i3 != -1 && i3 == 1) {
                Barrier barrier2 = new Barrier(constraintLayout.getContext());
                barrier2.setId(num.intValue());
                int[] iArr2 = bi2.u0;
                if (iArr2 != null) {
                    barrier2.setReferencedIds(iArr2);
                } else {
                    String str2 = bi2.v0;
                    if (str2 != null) {
                        int[] f2 = f(barrier2, str2);
                        bi2.u0 = f2;
                        barrier2.setReferencedIds(f2);
                    }
                }
                barrier2.setType(bi2.s0);
                ConstraintLayout.LayoutParams r = constraintLayout.r();
                barrier2.f();
                bi2.d(r);
                constraintLayout.addView(barrier2, r);
            }
            if (bi2.a) {
                View guideline = new Guideline(constraintLayout.getContext());
                guideline.setId(num.intValue());
                ConstraintLayout.LayoutParams r2 = constraintLayout.r();
                bi2.d(r2);
                constraintLayout.addView(guideline, r2);
            }
        }
    }

    @DexIgnore
    public void c(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        this.a.clear();
        for (int i = 0; i < childCount; i++) {
            View childAt = constraintLayout.getChildAt(i);
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.a.containsKey(Integer.valueOf(id))) {
                    this.a.put(Integer.valueOf(id), new Bi());
                }
                Bi bi = this.a.get(Integer.valueOf(id));
                bi.f(id, layoutParams);
                bi.J = childAt.getVisibility();
                if (Build.VERSION.SDK_INT >= 17) {
                    bi.U = childAt.getAlpha();
                    bi.X = childAt.getRotation();
                    bi.Y = childAt.getRotationX();
                    bi.Z = childAt.getRotationY();
                    bi.a0 = childAt.getScaleX();
                    bi.b0 = childAt.getScaleY();
                    float pivotX = childAt.getPivotX();
                    float pivotY = childAt.getPivotY();
                    if (!(((double) pivotX) == 0.0d && ((double) pivotY) == 0.0d)) {
                        bi.c0 = pivotX;
                        bi.d0 = pivotY;
                    }
                    bi.e0 = childAt.getTranslationX();
                    bi.f0 = childAt.getTranslationY();
                    if (Build.VERSION.SDK_INT >= 21) {
                        bi.g0 = childAt.getTranslationZ();
                        if (bi.V) {
                            bi.W = childAt.getElevation();
                        }
                    }
                }
                if (childAt instanceof Barrier) {
                    Barrier barrier = (Barrier) childAt;
                    bi.r0 = barrier.g();
                    bi.u0 = barrier.getReferencedIds();
                    bi.s0 = barrier.getType();
                }
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    @DexIgnore
    public void d(Constraints constraints) {
        int childCount = constraints.getChildCount();
        this.a.clear();
        for (int i = 0; i < childCount; i++) {
            View childAt = constraints.getChildAt(i);
            Constraints.LayoutParams layoutParams = (Constraints.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.a.containsKey(Integer.valueOf(id))) {
                    this.a.put(Integer.valueOf(id), new Bi());
                }
                Bi bi = this.a.get(Integer.valueOf(id));
                if (childAt instanceof ConstraintHelper) {
                    bi.h((ConstraintHelper) childAt, id, layoutParams);
                }
                bi.g(id, layoutParams);
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    @DexIgnore
    public void e(int i, int i2, int i3, int i4) {
        if (!this.a.containsKey(Integer.valueOf(i))) {
            this.a.put(Integer.valueOf(i), new Bi());
        }
        Bi bi = this.a.get(Integer.valueOf(i));
        switch (i2) {
            case 1:
                if (i4 == 1) {
                    bi.h = i3;
                    bi.i = -1;
                    return;
                } else if (i4 == 2) {
                    bi.i = i3;
                    bi.h = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("left to " + k(i4) + " undefined");
                }
            case 2:
                if (i4 == 1) {
                    bi.j = i3;
                    bi.k = -1;
                    return;
                } else if (i4 == 2) {
                    bi.k = i3;
                    bi.j = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + k(i4) + " undefined");
                }
            case 3:
                if (i4 == 3) {
                    bi.l = i3;
                    bi.m = -1;
                    bi.p = -1;
                    return;
                } else if (i4 == 4) {
                    bi.m = i3;
                    bi.l = -1;
                    bi.p = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + k(i4) + " undefined");
                }
            case 4:
                if (i4 == 4) {
                    bi.o = i3;
                    bi.n = -1;
                    bi.p = -1;
                    return;
                } else if (i4 == 3) {
                    bi.n = i3;
                    bi.o = -1;
                    bi.p = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + k(i4) + " undefined");
                }
            case 5:
                if (i4 == 5) {
                    bi.p = i3;
                    bi.o = -1;
                    bi.n = -1;
                    bi.l = -1;
                    bi.m = -1;
                    return;
                }
                throw new IllegalArgumentException("right to " + k(i4) + " undefined");
            case 6:
                if (i4 == 6) {
                    bi.r = i3;
                    bi.q = -1;
                    return;
                } else if (i4 == 7) {
                    bi.q = i3;
                    bi.r = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + k(i4) + " undefined");
                }
            case 7:
                if (i4 == 7) {
                    bi.t = i3;
                    bi.s = -1;
                    return;
                } else if (i4 == 6) {
                    bi.s = i3;
                    bi.t = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + k(i4) + " undefined");
                }
            default:
                throw new IllegalArgumentException(k(i2) + " to " + k(i4) + " unknown");
        }
    }

    @DexIgnore
    public final int[] f(View view, String str) {
        int i;
        Object t;
        String[] split = str.split(",");
        Context context = view.getContext();
        int[] iArr = new int[split.length];
        int i2 = 0;
        int i3 = 0;
        while (i3 < split.length) {
            String trim = split[i3].trim();
            try {
                i = Gk0.class.getField(trim).getInt(null);
            } catch (Exception e) {
                i = 0;
            }
            if (i == 0) {
                i = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            iArr[i2] = (i != 0 || !view.isInEditMode() || !(view.getParent() instanceof ConstraintLayout) || (t = ((ConstraintLayout) view.getParent()).t(0, trim)) == null || !(t instanceof Integer)) ? i : ((Integer) t).intValue();
            i3++;
            i2++;
        }
        return i2 != split.length ? Arrays.copyOf(iArr, i2) : iArr;
    }

    @DexIgnore
    public final Bi g(Context context, AttributeSet attributeSet) {
        Bi bi = new Bi();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, Hk0.ConstraintSet);
        j(bi, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        return bi;
    }

    @DexIgnore
    public void h(Context context, int i) {
        XmlResourceParser xml = context.getResources().getXml(i);
        try {
            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                if (eventType == 0) {
                    xml.getName();
                } else if (eventType == 2) {
                    String name = xml.getName();
                    Bi g = g(context, Xml.asAttributeSet(xml));
                    if (name.equalsIgnoreCase("Guideline")) {
                        g.a = true;
                    }
                    this.a.put(Integer.valueOf(g.d), g);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void j(Bi bi, TypedArray typedArray) {
        int indexCount = typedArray.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = typedArray.getIndex(i);
            int i2 = c.get(index);
            switch (i2) {
                case 1:
                    bi.p = i(typedArray, index, bi.p);
                    break;
                case 2:
                    bi.G = typedArray.getDimensionPixelSize(index, bi.G);
                    break;
                case 3:
                    bi.o = i(typedArray, index, bi.o);
                    break;
                case 4:
                    bi.n = i(typedArray, index, bi.n);
                    break;
                case 5:
                    bi.w = typedArray.getString(index);
                    break;
                case 6:
                    bi.A = typedArray.getDimensionPixelOffset(index, bi.A);
                    break;
                case 7:
                    bi.B = typedArray.getDimensionPixelOffset(index, bi.B);
                    break;
                case 8:
                    bi.H = typedArray.getDimensionPixelSize(index, bi.H);
                    break;
                case 9:
                    bi.t = i(typedArray, index, bi.t);
                    break;
                case 10:
                    bi.s = i(typedArray, index, bi.s);
                    break;
                case 11:
                    bi.N = typedArray.getDimensionPixelSize(index, bi.N);
                    break;
                case 12:
                    bi.O = typedArray.getDimensionPixelSize(index, bi.O);
                    break;
                case 13:
                    bi.K = typedArray.getDimensionPixelSize(index, bi.K);
                    break;
                case 14:
                    bi.M = typedArray.getDimensionPixelSize(index, bi.M);
                    break;
                case 15:
                    bi.P = typedArray.getDimensionPixelSize(index, bi.P);
                    break;
                case 16:
                    bi.L = typedArray.getDimensionPixelSize(index, bi.L);
                    break;
                case 17:
                    bi.e = typedArray.getDimensionPixelOffset(index, bi.e);
                    break;
                case 18:
                    bi.f = typedArray.getDimensionPixelOffset(index, bi.f);
                    break;
                case 19:
                    bi.g = typedArray.getFloat(index, bi.g);
                    break;
                case 20:
                    bi.u = typedArray.getFloat(index, bi.u);
                    break;
                case 21:
                    bi.c = typedArray.getLayoutDimension(index, bi.c);
                    break;
                case 22:
                    int i3 = typedArray.getInt(index, bi.J);
                    bi.J = i3;
                    bi.J = b[i3];
                    break;
                case 23:
                    bi.b = typedArray.getLayoutDimension(index, bi.b);
                    break;
                case 24:
                    bi.D = typedArray.getDimensionPixelSize(index, bi.D);
                    break;
                case 25:
                    bi.h = i(typedArray, index, bi.h);
                    break;
                case 26:
                    bi.i = i(typedArray, index, bi.i);
                    break;
                case 27:
                    bi.C = typedArray.getInt(index, bi.C);
                    break;
                case 28:
                    bi.E = typedArray.getDimensionPixelSize(index, bi.E);
                    break;
                case 29:
                    bi.j = i(typedArray, index, bi.j);
                    break;
                case 30:
                    bi.k = i(typedArray, index, bi.k);
                    break;
                case 31:
                    bi.I = typedArray.getDimensionPixelSize(index, bi.I);
                    break;
                case 32:
                    bi.q = i(typedArray, index, bi.q);
                    break;
                case 33:
                    bi.r = i(typedArray, index, bi.r);
                    break;
                case 34:
                    bi.F = typedArray.getDimensionPixelSize(index, bi.F);
                    break;
                case 35:
                    bi.m = i(typedArray, index, bi.m);
                    break;
                case 36:
                    bi.l = i(typedArray, index, bi.l);
                    break;
                case 37:
                    bi.v = typedArray.getFloat(index, bi.v);
                    break;
                case 38:
                    bi.d = typedArray.getResourceId(index, bi.d);
                    break;
                case 39:
                    bi.R = typedArray.getFloat(index, bi.R);
                    break;
                case 40:
                    bi.Q = typedArray.getFloat(index, bi.Q);
                    break;
                case 41:
                    bi.S = typedArray.getInt(index, bi.S);
                    break;
                case 42:
                    bi.T = typedArray.getInt(index, bi.T);
                    break;
                case 43:
                    bi.U = typedArray.getFloat(index, bi.U);
                    break;
                case 44:
                    bi.V = true;
                    bi.W = typedArray.getDimension(index, bi.W);
                    break;
                case 45:
                    bi.Y = typedArray.getFloat(index, bi.Y);
                    break;
                case 46:
                    bi.Z = typedArray.getFloat(index, bi.Z);
                    break;
                case 47:
                    bi.a0 = typedArray.getFloat(index, bi.a0);
                    break;
                case 48:
                    bi.b0 = typedArray.getFloat(index, bi.b0);
                    break;
                case 49:
                    bi.c0 = typedArray.getFloat(index, bi.c0);
                    break;
                case 50:
                    bi.d0 = typedArray.getFloat(index, bi.d0);
                    break;
                case 51:
                    bi.e0 = typedArray.getDimension(index, bi.e0);
                    break;
                case 52:
                    bi.f0 = typedArray.getDimension(index, bi.f0);
                    break;
                case 53:
                    bi.g0 = typedArray.getDimension(index, bi.g0);
                    break;
                default:
                    switch (i2) {
                        case 60:
                            bi.X = typedArray.getFloat(index, bi.X);
                            continue;
                        case 61:
                            bi.x = i(typedArray, index, bi.x);
                            continue;
                        case 62:
                            bi.y = typedArray.getDimensionPixelSize(index, bi.y);
                            continue;
                        case 63:
                            bi.z = typedArray.getFloat(index, bi.z);
                            continue;
                        default:
                            switch (i2) {
                                case 69:
                                    bi.p0 = typedArray.getFloat(index, 1.0f);
                                    continue;
                                case 70:
                                    bi.q0 = typedArray.getFloat(index, 1.0f);
                                    continue;
                                case 71:
                                    Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                                    continue;
                                case 72:
                                    bi.s0 = typedArray.getInt(index, bi.s0);
                                    continue;
                                case 73:
                                    bi.v0 = typedArray.getString(index);
                                    continue;
                                case 74:
                                    bi.r0 = typedArray.getBoolean(index, bi.r0);
                                    continue;
                                case 75:
                                    Log.w("ConstraintSet", "unused attribute 0x" + Integer.toHexString(index) + "   " + c.get(index));
                                    continue;
                                default:
                                    Log.w("ConstraintSet", "Unknown attribute 0x" + Integer.toHexString(index) + "   " + c.get(index));
                                    continue;
                                    continue;
                            }
                    }
            }
        }
    }

    @DexIgnore
    public final String k(int i) {
        switch (i) {
            case 1:
                return ViewHierarchy.DIMENSION_LEFT_KEY;
            case 2:
                return "right";
            case 3:
                return ViewHierarchy.DIMENSION_TOP_KEY;
            case 4:
                return "bottom";
            case 5:
                return "baseline";
            case 6:
                return VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE;
            case 7:
                return "end";
            default:
                return "undefined";
        }
    }
}
