package com.fossil;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.j57;
import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.NumberPickerLarge;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UseSparseArrays"})
public class t47 extends kq0 implements DialogInterface.OnKeyListener {
    @DexIgnore
    public HashMap<Integer, Integer> A;
    @DexIgnore
    public Intent B;
    @DexIgnore
    public g C;
    @DexIgnore
    public h D;
    @DexIgnore
    public String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public ArrayList<Integer> g;
    @DexIgnore
    public HashMap<Integer, Integer> h;
    @DexIgnore
    public HashMap<Integer, Bitmap> i;
    @DexIgnore
    public HashMap<Integer, String> j;
    @DexIgnore
    public HashMap<Integer, String> k;
    @DexIgnore
    public DashbarData l;
    @DexIgnore
    public HashMap<Integer, SpannableString> m;
    @DexIgnore
    public ArrayList<Integer> s;
    @DexIgnore
    public ArrayList<Integer> t;
    @DexIgnore
    public ArrayList<Integer> u;
    @DexIgnore
    public ArrayList<Integer> v;
    @DexIgnore
    public HashMap<Integer, List<Serializable>> w;
    @DexIgnore
    public ArrayList<ln0<Integer, Integer>> x;
    @DexIgnore
    public HashMap<Integer, Boolean> y;
    @DexIgnore
    public HashMap<Integer, Integer> z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ View f3363a;

        @DexIgnore
        public a(View view) {
            this.f3363a = view;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            oa1.v(t47.this).t(str2).d(new fj1().l(wc1.c)).F0((ImageView) this.f3363a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public b(View view, int i) {
            this.b = view;
            this.c = i;
        }

        @DexIgnore
        public void onClick(View view) {
            t47.this.dismiss();
            t47 t47 = t47.this;
            HashMap<Integer, Boolean> hashMap = t47.y;
            if (hashMap != null) {
                t47.B.putExtra("EXTRA_SWITCH_RESULTS", hashMap);
            }
            t47 t472 = t47.this;
            HashMap<Integer, Integer> hashMap2 = t472.z;
            if (hashMap2 != null) {
                t472.B.putExtra("EXTRA_NUMBER_PICKER_RESULTS", hashMap2);
            }
            t47 t473 = t47.this;
            HashMap<Integer, Integer> hashMap3 = t473.A;
            if (hashMap3 != null) {
                t473.B.putExtra("EXTRA_RADIO_GROUPS_RESULTS", hashMap3);
            }
            if (t47.this.v != null) {
                HashMap hashMap4 = new HashMap();
                Iterator<Integer> it = t47.this.v.iterator();
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    hashMap4.put(Integer.valueOf(intValue), ((FlexibleEditText) this.b.findViewById(intValue)).getText().toString());
                }
                t47.this.B.putExtra("EXTRA_EDIT_TEXT_RESULTS", hashMap4);
            }
            t47 t474 = t47.this;
            g gVar = t474.C;
            if (gVar != null) {
                gVar.R5(t474.b, this.c, t474.B);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public c(View view, int i) {
            this.b = view;
            this.c = i;
        }

        @DexIgnore
        public void onClick(View view) {
            t47 t47 = t47.this;
            HashMap<Integer, Boolean> hashMap = t47.y;
            if (hashMap != null) {
                t47.B.putExtra("EXTRA_SWITCH_RESULTS", hashMap);
            }
            t47 t472 = t47.this;
            HashMap<Integer, Integer> hashMap2 = t472.z;
            if (hashMap2 != null) {
                t472.B.putExtra("EXTRA_NUMBER_PICKER_RESULTS", hashMap2);
            }
            t47 t473 = t47.this;
            HashMap<Integer, Integer> hashMap3 = t473.A;
            if (hashMap3 != null) {
                t473.B.putExtra("EXTRA_RADIO_GROUPS_RESULTS", hashMap3);
            }
            if (t47.this.v != null) {
                HashMap hashMap4 = new HashMap();
                Iterator<Integer> it = t47.this.v.iterator();
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    hashMap4.put(Integer.valueOf(intValue), ((FlexibleEditText) this.b.findViewById(intValue)).getText().toString());
                }
                t47.this.B.putExtra("EXTRA_EDIT_TEXT_RESULTS", hashMap4);
            }
            t47 t474 = t47.this;
            g gVar = t474.C;
            if (gVar != null) {
                gVar.R5(t474.b, this.c, t474.B);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            t47 t47 = t47.this;
            if (t47.y == null) {
                t47.y = new HashMap<>();
            }
            t47.this.y.put(Integer.valueOf(compoundButton.getId()), Boolean.valueOf(z));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements NumberPickerLarge.h {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPickerLarge.h
        public void a(NumberPickerLarge numberPickerLarge, int i, int i2) {
            t47 t47 = t47.this;
            if (t47.z == null) {
                t47.z = new HashMap<>();
            }
            t47.this.z.put(Integer.valueOf(numberPickerLarge.getId()), Integer.valueOf(i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3366a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d; // = true;
        @DexIgnore
        public /* final */ ArrayList<Integer> e; // = new ArrayList<>();
        @DexIgnore
        public /* final */ HashMap<Integer, Integer> f; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, Bitmap> g; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, String> h; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, String> i; // = new HashMap<>();
        @DexIgnore
        public DashbarData j;
        @DexIgnore
        public /* final */ HashMap<Integer, SpannableString> k; // = new HashMap<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> l; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> m; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> n; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> o; // = new ArrayList<>();
        @DexIgnore
        public /* final */ HashMap<Integer, List<Serializable>> p; // = new HashMap<>();
        @DexIgnore
        public /* final */ ArrayList<ln0<Integer, Integer>> q; // = new ArrayList<>();

        @DexIgnore
        public f(int i2) {
            this.f3366a = i2;
        }

        @DexIgnore
        public f a(DashbarData dashbarData) {
            this.j = dashbarData;
            return this;
        }

        @DexIgnore
        public f b(int i2) {
            this.n.add(Integer.valueOf(i2));
            return this;
        }

        @DexIgnore
        public f c(int i2, int i3, int i4, int i5) {
            d(i2, i3, i4, i5, null, null);
            return this;
        }

        @DexIgnore
        public f d(int i2, int i3, int i4, int i5, NumberPickerLarge.f fVar, String[] strArr) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(Integer.valueOf(i3));
            arrayList.add(Integer.valueOf(i4));
            arrayList.add(Integer.valueOf(i5));
            if (fVar != null) {
                arrayList.add(fVar);
            }
            if (strArr != null) {
                arrayList.add(strArr);
            }
            this.p.put(Integer.valueOf(i2), arrayList);
            return this;
        }

        @DexIgnore
        public f e(int i2, String str) {
            this.i.put(Integer.valueOf(i2), str);
            return this;
        }

        @DexIgnore
        public t47 f(String str) {
            return g(str, null);
        }

        @DexIgnore
        public t47 g(String str, Bundle bundle) {
            return t47.w6(str, bundle, this.f3366a, this.b, this.c, this.e, this.f, this.g, this.h, this.i, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.d, this.j);
        }

        @DexIgnore
        public f h(boolean z) {
            this.d = z;
            return this;
        }

        @DexIgnore
        public f i(int i2) {
            this.b = i2;
            return this;
        }

        @DexIgnore
        public f j(boolean z) {
            this.c = z;
            return this;
        }

        @DexIgnore
        public t47 k(FragmentManager fragmentManager, String str) {
            return l(fragmentManager, str, 1, 2131951629);
        }

        @DexIgnore
        public t47 l(FragmentManager fragmentManager, String str, int i2, int i3) {
            t47 f2 = f(str);
            f2.setStyle(i2, i3);
            f2.show(fragmentManager, str);
            return f2;
        }

        @DexIgnore
        public t47 m(FragmentManager fragmentManager, String str, Bundle bundle) {
            return n(fragmentManager, str, bundle, 1, 2131951629);
        }

        @DexIgnore
        public t47 n(FragmentManager fragmentManager, String str, Bundle bundle, int i2, int i3) {
            t47 g2 = g(str, bundle);
            g2.setStyle(i2, i3);
            g2.show(fragmentManager, str);
            return g2;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void R5(String str, int i, Intent intent);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void i1(String str);
    }

    @DexIgnore
    public static t47 w6(String str, Bundle bundle, int i2, int i3, boolean z2, ArrayList<Integer> arrayList, HashMap<Integer, Integer> hashMap, HashMap<Integer, Bitmap> hashMap2, HashMap<Integer, String> hashMap3, HashMap<Integer, String> hashMap4, HashMap<Integer, SpannableString> hashMap5, ArrayList<Integer> arrayList2, ArrayList<Integer> arrayList3, ArrayList<Integer> arrayList4, ArrayList<Integer> arrayList5, HashMap<Integer, List<Serializable>> hashMap6, ArrayList<ln0<Integer, Integer>> arrayList6, boolean z3, DashbarData dashbarData) {
        t47 t47 = new t47();
        Bundle bundle2 = new Bundle();
        bundle2.putString("ARGUMENTS_TAG", str);
        bundle2.putBundle("ARGUMENTS_BUNDLE", bundle);
        bundle2.putInt("ARGUMENTS_LAYOUT_ID", i2);
        bundle2.putInt("ARGUMENTS_STATUS_BAR_COLOR_ID", i3);
        bundle2.putBoolean("ARGUMENTS_STATUS_BAR_DARK_ICON", z2);
        bundle2.putSerializable("ARGUMENTS_STATUS_BAR_FLAGS", arrayList);
        bundle2.putSerializable("ARGUMENTS_IMAGE_VIEWS", hashMap);
        bundle2.putSerializable("ARGUMENTS_BLUR_IMAGE_VIEWS", hashMap2);
        bundle2.putSerializable("ARGUMENTS_DEVICE_IMAGE_VIEWS", hashMap3);
        bundle2.putSerializable("ARGUMENTS_TEXT_VIEWS", hashMap4);
        bundle2.putSerializable("ARGUMENTS_TEXT_VIEWS_SPANNABLE", hashMap5);
        bundle2.putSerializable("ARGUMENTS_DISMISS_VIEWS", arrayList4);
        bundle2.putSerializable("ARGUMENTS_ACTION_VIEWS", arrayList2);
        bundle2.putSerializable("ARGUMENTS_SWITCH_VIEWS", arrayList3);
        bundle2.putSerializable("ARGUMENTS_EDIT_TEXT_VIEWS", arrayList5);
        bundle2.putSerializable("ARGUMENTS_NUMBER_PICKERS", hashMap6);
        bundle2.putSerializable("ARGUMENTS_RADIO_GROUPS", arrayList6);
        bundle2.putBoolean("ARGUMENTS_ALLOW_BACK_PRESS", z3);
        bundle2.putParcelable("ARGUMENTS_DASH_BAR_DATA", dashbarData);
        t47.setArguments(bundle2);
        return t47;
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void dismiss() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            xq0 j2 = fragmentManager.j();
            j2.q(this);
            j2.i();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            if (parentFragment instanceof g) {
                this.C = (g) parentFragment;
            }
            if (parentFragment instanceof h) {
                this.D = (h) parentFragment;
            }
        }
        if (this.C == null && (context instanceof g)) {
            this.C = (g) context;
        }
        if (this.D == null && (context instanceof h)) {
            this.D = (h) context;
        }
    }

    @DexIgnore
    public boolean onBackPressed() {
        dismiss();
        return true;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
        this.B = new Intent();
        Bundle arguments = getArguments();
        this.b = arguments.getString("ARGUMENTS_TAG");
        Bundle bundle2 = arguments.getBundle("ARGUMENTS_BUNDLE");
        if (bundle2 != null) {
            this.B.putExtras(bundle2);
        }
        this.c = arguments.getInt("ARGUMENTS_LAYOUT_ID");
        this.d = arguments.getInt("ARGUMENTS_STATUS_BAR_COLOR_ID");
        this.e = arguments.getBoolean("ARGUMENTS_STATUS_BAR_DARK_ICON");
        this.g = (ArrayList) arguments.getSerializable("ARGUMENTS_STATUS_BAR_FLAGS");
        this.h = (HashMap) arguments.getSerializable("ARGUMENTS_IMAGE_VIEWS");
        this.i = (HashMap) arguments.getSerializable("ARGUMENTS_BLUR_IMAGE_VIEWS");
        this.j = (HashMap) arguments.getSerializable("ARGUMENTS_DEVICE_IMAGE_VIEWS");
        this.k = (HashMap) arguments.getSerializable("ARGUMENTS_TEXT_VIEWS");
        this.m = (HashMap) arguments.getSerializable("ARGUMENTS_TEXT_VIEWS_SPANNABLE");
        this.s = (ArrayList) arguments.getSerializable("ARGUMENTS_ACTION_VIEWS");
        this.t = (ArrayList) arguments.getSerializable("ARGUMENTS_DISMISS_VIEWS");
        this.u = (ArrayList) arguments.getSerializable("ARGUMENTS_SWITCH_VIEWS");
        this.v = (ArrayList) arguments.getSerializable("ARGUMENTS_EDIT_TEXT_VIEWS");
        this.w = (HashMap) arguments.getSerializable("ARGUMENTS_NUMBER_PICKERS");
        this.x = (ArrayList) arguments.getSerializable("ARGUMENTS_RADIO_GROUPS");
        this.f = arguments.getBoolean("ARGUMENTS_ALLOW_BACK_PRESS", true);
        this.l = (DashbarData) arguments.getParcelable("ARGUMENTS_DASH_BAR_DATA");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Window window;
        if (Build.VERSION.SDK_INT >= 21 && (window = getDialog().getWindow()) != null) {
            if (this.d != 0) {
                window.addFlags(RecyclerView.UNDEFINED_DURATION);
                window.setStatusBarColor(gl0.d(PortfolioApp.d0, this.d));
            }
            ArrayList<Integer> arrayList = this.g;
            if (arrayList != null && !arrayList.isEmpty()) {
                Iterator<Integer> it = this.g.iterator();
                while (it.hasNext()) {
                    window.addFlags(it.next().intValue());
                }
            }
            if (Build.VERSION.SDK_INT >= 23 && this.e) {
                window.getDecorView().setSystemUiVisibility(8192);
            }
        }
        View inflate = layoutInflater.inflate(this.c, viewGroup);
        View findViewById = inflate.findViewById(2131363010);
        if (findViewById != null) {
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                findViewById.setBackgroundColor(Color.parseColor(d2));
            }
        }
        return inflate;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onDetach() {
        super.onDetach();
        this.C = null;
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void onDismiss(DialogInterface dialogInterface) {
        FLogger.INSTANCE.getLocal().d("AlertDialogFragment", "onDismiss");
        h hVar = this.D;
        if (hVar != null) {
            hVar.i1(getTag());
        }
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        if (!this.f) {
            return true;
        }
        if (keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d("AlertDialogFragment", "onKey KEYCODE_BACK");
        return onBackPressed();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(2131363010);
        if (findViewById != null) {
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                findViewById.setBackgroundColor(Color.parseColor(d2));
            }
        }
        HashMap<Integer, Integer> hashMap = this.h;
        if (hashMap != null) {
            for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
                View findViewById2 = view.findViewById(entry.getKey().intValue());
                if (findViewById2 == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("AlertDialogFragment", "Set ImageViews - view is null on mTag = " + this.b);
                } else {
                    ((ImageView) findViewById2).setImageResource(entry.getValue().intValue());
                }
            }
        }
        if (this.i != null) {
            Context context = getContext();
            for (Map.Entry<Integer, Bitmap> entry2 : this.i.entrySet()) {
                View findViewById3 = view.findViewById(entry2.getKey().intValue());
                if (findViewById3 == null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("AlertDialogFragment", "Set mBlurImageViews - view is null on mTag = " + this.b);
                } else {
                    j57.b a2 = j57.a(context);
                    a2.b(25);
                    a2.c(2);
                    a2.a(entry2.getValue()).a((ImageView) findViewById3);
                }
            }
        }
        HashMap<Integer, String> hashMap2 = this.j;
        if (hashMap2 != null) {
            for (Map.Entry<Integer, String> entry3 : hashMap2.entrySet()) {
                View findViewById4 = view.findViewById(entry3.getKey().intValue());
                if (findViewById4 == null) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.e("AlertDialogFragment", "Set mDeviceImageViews - view is null on mTag = " + this.b);
                } else {
                    String value = entry3.getValue();
                    CloudImageHelper.getInstance().with().setSerialNumber(value).setSerialPrefix(nk5.o.m(value)).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder((ImageView) findViewById4, nk5.o.i(value, nk5.b.LARGE)).setImageCallback(new a(findViewById4)).download();
                }
            }
        }
        HashMap<Integer, String> hashMap3 = this.k;
        if (hashMap3 != null) {
            for (Map.Entry<Integer, String> entry4 : hashMap3.entrySet()) {
                View findViewById5 = view.findViewById(entry4.getKey().intValue());
                if (findViewById5 == null) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    local4.e("AlertDialogFragment", "Set TextViews - view is null on mTag = " + this.b);
                } else {
                    ((TextView) findViewById5).setText(entry4.getValue());
                }
            }
        }
        DashbarData dashbarData = this.l;
        if (dashbarData != null) {
            DashBar dashBar = (DashBar) view.findViewById(dashbarData.getViewId());
            if (dashBar == null) {
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                local5.e("AlertDialogFragment", "Set DashBar - view is null on mTag = " + this.b);
            } else {
                dashBar.setProgress(this.l.getStartProgress());
                ObjectAnimator ofInt = ObjectAnimator.ofInt(dashBar, "progress", this.l.getStartProgress(), this.l.getEndProgress());
                ofInt.setDuration(500L);
                ofInt.start();
            }
        }
        HashMap<Integer, SpannableString> hashMap4 = this.m;
        if (hashMap4 != null) {
            for (Map.Entry<Integer, SpannableString> entry5 : hashMap4.entrySet()) {
                View findViewById6 = view.findViewById(entry5.getKey().intValue());
                if (findViewById6 == null) {
                    ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                    local6.e("AlertDialogFragment", "Set TextViews - view is null on mTag = " + this.b);
                } else {
                    TextView textView = (TextView) findViewById6;
                    textView.setText(entry5.getValue());
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setHighlightColor(0);
                }
            }
        }
        ArrayList<Integer> arrayList = this.t;
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator<Integer> it = this.t.iterator();
            while (it.hasNext()) {
                int intValue = it.next().intValue();
                if (view.findViewById(intValue) == null) {
                    ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                    local7.e("AlertDialogFragment", "Set action - view is null on mTag = " + this.b);
                } else {
                    view.findViewById(intValue).setOnClickListener(new b(view, intValue));
                }
            }
        }
        ArrayList<Integer> arrayList2 = this.s;
        if (arrayList2 != null) {
            Iterator<Integer> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                int intValue2 = it2.next().intValue();
                if (view.findViewById(intValue2) == null) {
                    ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                    local8.e("AlertDialogFragment", "Set action - view is null on mTag = " + this.b);
                } else {
                    view.findViewById(intValue2).setOnClickListener(new c(view, intValue2));
                }
            }
        }
        ArrayList<Integer> arrayList3 = this.u;
        if (arrayList3 != null) {
            Iterator<Integer> it3 = arrayList3.iterator();
            while (it3.hasNext()) {
                int intValue3 = it3.next().intValue();
                View findViewById7 = view.findViewById(intValue3);
                if (findViewById7 == null) {
                    ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                    local9.e("AlertDialogFragment", "Set SwitchViews - view is null on mTag = " + this.b);
                } else {
                    if (this.y == null) {
                        this.y = new HashMap<>();
                    }
                    this.y.put(Integer.valueOf(intValue3), Boolean.FALSE);
                    ((SwitchCompat) findViewById7).setOnCheckedChangeListener(new d());
                }
            }
        }
        ArrayList<Integer> arrayList4 = this.v;
        if (arrayList4 != null) {
            Iterator<Integer> it4 = arrayList4.iterator();
            while (it4.hasNext()) {
                if (((FlexibleEditText) view.findViewById(it4.next().intValue())) == null) {
                    ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                    local10.e("AlertDialogFragment", "Set EditTextViews - view is null on mTag = " + this.b);
                }
            }
        }
        HashMap<Integer, List<Serializable>> hashMap5 = this.w;
        if (hashMap5 != null) {
            for (Map.Entry<Integer, List<Serializable>> entry6 : hashMap5.entrySet()) {
                View findViewById8 = view.findViewById(entry6.getKey().intValue());
                if (findViewById8 == null) {
                    ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                    local11.e("AlertDialogFragment", "Set NumberPickers - view is null on mTag = " + this.b);
                } else {
                    NumberPickerLarge numberPickerLarge = (NumberPickerLarge) findViewById8;
                    numberPickerLarge.setOnValueChangedListener(new e());
                    List<Serializable> value2 = entry6.getValue();
                    int intValue4 = ((Integer) value2.get(2)).intValue();
                    numberPickerLarge.setMinValue(((Integer) value2.get(0)).intValue());
                    numberPickerLarge.setMaxValue(((Integer) value2.get(1)).intValue());
                    numberPickerLarge.setValue(intValue4);
                    if (value2.size() > 3) {
                        numberPickerLarge.setFormatter((NumberPickerLarge.f) value2.get(3));
                    }
                    if (value2.size() > 4) {
                        numberPickerLarge.setDisplayedValues((String[]) value2.get(4));
                    }
                    if (this.z == null) {
                        this.z = new HashMap<>();
                    }
                    this.z.put(entry6.getKey(), Integer.valueOf(intValue4));
                }
            }
        }
        ArrayList<ln0<Integer, Integer>> arrayList5 = this.x;
        if (arrayList5 != null) {
            Iterator<ln0<Integer, Integer>> it5 = arrayList5.iterator();
            while (it5.hasNext()) {
                ln0<Integer, Integer> next = it5.next();
                F f2 = next.f2221a;
                S s2 = next.b;
                if (!(f2 == null || s2 == null)) {
                    RadioGroup radioGroup = (RadioGroup) view.findViewById(f2.intValue());
                    if (radioGroup == null) {
                        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                        local12.e("AlertDialogFragment", "Set RadioGroup - radioGroup is null on mTag = " + this.b);
                    } else {
                        RadioButton radioButton = (RadioButton) view.findViewById(s2.intValue());
                        if (radioButton != null) {
                            radioButton.setChecked(true);
                        } else {
                            ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                            local13.e("AlertDialogFragment", "Set RadioGroup - defaultRadioButtonChecked is null on mTag = " + this.b + ", do not set check default");
                        }
                        if (this.A == null) {
                            this.A = new HashMap<>();
                        }
                        this.A.put(f2, s2);
                        radioGroup.setOnCheckedChangeListener(new s47(this, f2));
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void setupDialog(Dialog dialog, int i2) {
        dialog.requestWindowFeature(1);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().getDecorView().setSystemUiVisibility(3328);
            dialog.setOnKeyListener(this);
        }
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public void show(FragmentManager fragmentManager, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlertDialogFragment", "show - tag: " + str);
        xq0 j2 = fragmentManager.j();
        Fragment Z = fragmentManager.Z(str);
        if (Z != null) {
            j2.q(Z);
        }
        j2.f(null);
        j2.d(this, str);
        j2.i();
    }

    @DexIgnore
    public /* synthetic */ void v6(Integer num, RadioGroup radioGroup, int i2) {
        if (this.A == null) {
            this.A = new HashMap<>();
        }
        this.A.put(num, Integer.valueOf(i2));
    }
}
