package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ko2 extends Tn2 implements Lo2 {
    @DexIgnore
    public Ko2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
    }

    @DexIgnore
    @Override // com.fossil.Lo2
    public final void V2(Wi2 wi2) throws RemoteException {
        Parcel d = d();
        Qo2.b(d, wi2);
        e(3, d);
    }
}
