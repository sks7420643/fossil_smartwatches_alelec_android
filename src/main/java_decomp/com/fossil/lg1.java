package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lg1 {
    @DexIgnore
    public static /* final */ File f; // = new File("/proc/self/fd");
    @DexIgnore
    public static volatile Lg1 g;
    @DexIgnore
    public /* final */ boolean a; // = d();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = true;

    @DexIgnore
    public Lg1() {
        if (Build.VERSION.SDK_INT >= 28) {
            this.b = 20000;
            this.c = 0;
            return;
        }
        this.b = 700;
        this.c = 128;
    }

    @DexIgnore
    public static Lg1 a() {
        if (g == null) {
            synchronized (Lg1.class) {
                try {
                    if (g == null) {
                        g = new Lg1();
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return g;
    }

    @DexIgnore
    public static boolean d() {
        String str = Build.MODEL;
        if (str == null || str.length() < 7) {
            return true;
        }
        String substring = Build.MODEL.substring(0, 7);
        char c2 = '\uffff';
        switch (substring.hashCode()) {
            case -1398613787:
                if (substring.equals("SM-A520")) {
                    c2 = 6;
                    break;
                }
                break;
            case -1398431166:
                if (substring.equals("SM-G930")) {
                    c2 = 5;
                    break;
                }
                break;
            case -1398431161:
                if (substring.equals("SM-G935")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1398431073:
                if (substring.equals("SM-G960")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1398431068:
                if (substring.equals("SM-G965")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1398343746:
                if (substring.equals("SM-J720")) {
                    c2 = 1;
                    break;
                }
                break;
            case -1398222624:
                if (substring.equals("SM-N935")) {
                    c2 = 0;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return Build.VERSION.SDK_INT != 26;
            default:
                return true;
        }
    }

    @DexIgnore
    public final boolean b() {
        boolean z;
        synchronized (this) {
            boolean z2 = true;
            int i = this.d + 1;
            this.d = i;
            if (i >= 50) {
                this.d = 0;
                int length = f.list().length;
                if (length >= this.b) {
                    z2 = false;
                }
                this.e = z2;
                if (!z2 && Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + this.b);
                }
            }
            z = this.e;
        }
        return z;
    }

    @DexIgnore
    public boolean c(int i, int i2, boolean z, boolean z2) {
        int i3;
        return z && this.a && Build.VERSION.SDK_INT >= 26 && !z2 && i >= (i3 = this.c) && i2 >= i3 && b();
    }

    @DexIgnore
    @TargetApi(26)
    public boolean e(int i, int i2, BitmapFactory.Options options, boolean z, boolean z2) {
        boolean c2 = c(i, i2, z, z2);
        if (c2) {
            options.inPreferredConfig = Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return c2;
    }
}
