package com.fossil;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.R62;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fc2 implements Handler.Callback {
    @DexIgnore
    public /* final */ Ai a;
    @DexIgnore
    public /* final */ ArrayList<R62.Bi> b; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<R62.Bi> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<R62.Ci> d; // = new ArrayList<>();
    @DexIgnore
    public volatile boolean e; // = false;
    @DexIgnore
    public /* final */ AtomicInteger f; // = new AtomicInteger(0);
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ Object i; // = new Object();

    @DexIgnore
    public interface Ai {
        @DexIgnore
        boolean c();

        @DexIgnore
        Bundle y();
    }

    @DexIgnore
    public Fc2(Looper looper, Ai ai) {
        this.a = ai;
        this.h = new Ol2(looper, this);
    }

    @DexIgnore
    public final void a() {
        this.e = false;
        this.f.incrementAndGet();
    }

    @DexIgnore
    public final void b() {
        this.e = true;
    }

    @DexIgnore
    public final void c(Z52 z52) {
        Rc2.e(this.h, "onConnectionFailure must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            ArrayList arrayList = new ArrayList(this.d);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                R62.Ci ci = (R62.Ci) obj;
                if (this.e && this.f.get() == i2) {
                    if (this.d.contains(ci)) {
                        ci.n(z52);
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void d(Bundle bundle) {
        boolean z = true;
        Rc2.e(this.h, "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.i) {
            Rc2.n(!this.g);
            this.h.removeMessages(1);
            this.g = true;
            if (this.c.size() != 0) {
                z = false;
            }
            Rc2.n(z);
            ArrayList arrayList = new ArrayList(this.b);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                R62.Bi bi = (R62.Bi) obj;
                if (!this.e || !this.a.c() || this.f.get() != i2) {
                    break;
                } else if (!this.c.contains(bi)) {
                    bi.e(bundle);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    @DexIgnore
    public final void e(int i2) {
        Rc2.e(this.h, "onUnintentionalDisconnection must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            this.g = true;
            ArrayList arrayList = new ArrayList(this.b);
            int i3 = this.f.get();
            int size = arrayList.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList.get(i4);
                i4++;
                R62.Bi bi = (R62.Bi) obj;
                if (!this.e || this.f.get() != i3) {
                    break;
                } else if (this.b.contains(bi)) {
                    bi.d(i2);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    @DexIgnore
    public final void f(R62.Bi bi) {
        Rc2.k(bi);
        synchronized (this.i) {
            if (this.b.contains(bi)) {
                String valueOf = String.valueOf(bi);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.b.add(bi);
            }
        }
        if (this.a.c()) {
            Handler handler = this.h;
            handler.sendMessage(handler.obtainMessage(1, bi));
        }
    }

    @DexIgnore
    public final void g(R62.Ci ci) {
        Rc2.k(ci);
        synchronized (this.i) {
            if (this.d.contains(ci)) {
                String valueOf = String.valueOf(ci);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.d.add(ci);
            }
        }
    }

    @DexIgnore
    public final void h(R62.Ci ci) {
        Rc2.k(ci);
        synchronized (this.i) {
            if (!this.d.remove(ci)) {
                String valueOf = String.valueOf(ci);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 1) {
            R62.Bi bi = (R62.Bi) message.obj;
            synchronized (this.i) {
                if (this.e && this.a.c() && this.b.contains(bi)) {
                    bi.e(this.a.y());
                }
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i2);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }
}
