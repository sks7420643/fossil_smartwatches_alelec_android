package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dd1 extends Exception {
    @DexIgnore
    public static /* final */ StackTraceElement[] b; // = new StackTraceElement[0];
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ List<Throwable> causes;
    @DexIgnore
    public Class<?> dataClass;
    @DexIgnore
    public Gb1 dataSource;
    @DexIgnore
    public String detailMessage;
    @DexIgnore
    public Exception exception;
    @DexIgnore
    public Mb1 key;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Appendable {
        @DexIgnore
        public /* final */ Appendable b;
        @DexIgnore
        public boolean c; // = true;

        @DexIgnore
        public Ai(Appendable appendable) {
            this.b = appendable;
        }

        @DexIgnore
        public final CharSequence a(CharSequence charSequence) {
            return charSequence == null ? "" : charSequence;
        }

        @DexIgnore
        @Override // java.lang.Appendable
        public Appendable append(char c2) throws IOException {
            boolean z = false;
            if (this.c) {
                this.c = false;
                this.b.append("  ");
            }
            if (c2 == '\n') {
                z = true;
            }
            this.c = z;
            this.b.append(c2);
            return this;
        }

        @DexIgnore
        @Override // java.lang.Appendable
        public Appendable append(CharSequence charSequence) throws IOException {
            CharSequence a2 = a(charSequence);
            append(a2, 0, a2.length());
            return this;
        }

        @DexIgnore
        @Override // java.lang.Appendable
        public Appendable append(CharSequence charSequence, int i, int i2) throws IOException {
            boolean z = false;
            CharSequence a2 = a(charSequence);
            if (this.c) {
                this.c = false;
                this.b.append("  ");
            }
            if (a2.length() > 0 && a2.charAt(i2 - 1) == '\n') {
                z = true;
            }
            this.c = z;
            this.b.append(a2, i, i2);
            return this;
        }
    }

    @DexIgnore
    public Dd1(String str) {
        this(str, Collections.emptyList());
    }

    @DexIgnore
    public Dd1(String str, Throwable th) {
        this(str, Collections.singletonList(th));
    }

    @DexIgnore
    public Dd1(String str, List<Throwable> list) {
        this.detailMessage = str;
        setStackTrace(b);
        this.causes = list;
    }

    @DexIgnore
    public static void b(List<Throwable> list, Appendable appendable) {
        try {
            c(list, appendable);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @DexIgnore
    public static void c(List<Throwable> list, Appendable appendable) throws IOException {
        int size = list.size();
        int i = 0;
        while (i < size) {
            int i2 = i + 1;
            appendable.append("Cause (").append(String.valueOf(i2)).append(" of ").append(String.valueOf(size)).append("): ");
            Throwable th = list.get(i);
            if (th instanceof Dd1) {
                ((Dd1) th).e(appendable);
            } else {
                d(th, appendable);
            }
            i = i2;
        }
    }

    @DexIgnore
    public static void d(Throwable th, Appendable appendable) {
        try {
            appendable.append(th.getClass().toString()).append(": ").append(th.getMessage()).append('\n');
        } catch (IOException e) {
            throw new RuntimeException(th);
        }
    }

    @DexIgnore
    public final void a(Throwable th, List<Throwable> list) {
        if (th instanceof Dd1) {
            for (Throwable th2 : ((Dd1) th).getCauses()) {
                a(th2, list);
            }
            return;
        }
        list.add(th);
    }

    @DexIgnore
    public final void e(Appendable appendable) {
        d(this, appendable);
        b(getCauses(), new Ai(appendable));
    }

    @DexIgnore
    public Throwable fillInStackTrace() {
        return this;
    }

    @DexIgnore
    public List<Throwable> getCauses() {
        return this.causes;
    }

    @DexIgnore
    public String getMessage() {
        StringBuilder sb = new StringBuilder(71);
        sb.append(this.detailMessage);
        sb.append(this.dataClass != null ? ", " + this.dataClass : "");
        sb.append(this.dataSource != null ? ", " + this.dataSource : "");
        sb.append(this.key != null ? ", " + this.key : "");
        List<Throwable> rootCauses = getRootCauses();
        if (rootCauses.isEmpty()) {
            return sb.toString();
        }
        if (rootCauses.size() == 1) {
            sb.append("\nThere was 1 cause:");
        } else {
            sb.append("\nThere were ");
            sb.append(rootCauses.size());
            sb.append(" causes:");
        }
        for (Throwable th : rootCauses) {
            sb.append('\n');
            sb.append(th.getClass().getName());
            sb.append('(');
            sb.append(th.getMessage());
            sb.append(')');
        }
        sb.append("\n call GlideException#logRootCauses(String) for more detail");
        return sb.toString();
    }

    @DexIgnore
    public Exception getOrigin() {
        return this.exception;
    }

    @DexIgnore
    public List<Throwable> getRootCauses() {
        ArrayList arrayList = new ArrayList();
        a(this, arrayList);
        return arrayList;
    }

    @DexIgnore
    public void logRootCauses(String str) {
        List<Throwable> rootCauses = getRootCauses();
        int size = rootCauses.size();
        int i = 0;
        while (i < size) {
            StringBuilder sb = new StringBuilder();
            sb.append("Root cause (");
            int i2 = i + 1;
            sb.append(i2);
            sb.append(" of ");
            sb.append(size);
            sb.append(")");
            Log.i(str, sb.toString(), rootCauses.get(i));
            i = i2;
        }
    }

    @DexIgnore
    public void printStackTrace() {
        printStackTrace(System.err);
    }

    @DexIgnore
    @Override // java.lang.Throwable
    public void printStackTrace(PrintStream printStream) {
        e(printStream);
    }

    @DexIgnore
    @Override // java.lang.Throwable
    public void printStackTrace(PrintWriter printWriter) {
        e(printWriter);
    }

    @DexIgnore
    public void setLoggingDetails(Mb1 mb1, Gb1 gb1) {
        setLoggingDetails(mb1, gb1, null);
    }

    @DexIgnore
    public void setLoggingDetails(Mb1 mb1, Gb1 gb1, Class<?> cls) {
        this.key = mb1;
        this.dataSource = gb1;
        this.dataClass = cls;
    }

    @DexIgnore
    public void setOrigin(Exception exc) {
        this.exception = exc;
    }
}
