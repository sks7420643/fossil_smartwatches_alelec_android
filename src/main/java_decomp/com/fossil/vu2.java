package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vu2 extends E13<Vu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Vu2 zzf;
    @DexIgnore
    public static volatile Z23<Vu2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public long zze;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Vu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Vu2.zzf);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Vu2) this.c).C(i);
            return this;
        }

        @DexIgnore
        public final Ai y(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Vu2) this.c).D(j);
            return this;
        }
    }

    /*
    static {
        Vu2 vu2 = new Vu2();
        zzf = vu2;
        E13.u(Vu2.class, vu2);
    }
    */

    @DexIgnore
    public static Ai L() {
        return (Ai) zzf.w();
    }

    @DexIgnore
    public final void C(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final void D(long j) {
        this.zzc |= 2;
        this.zze = j;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int I() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long K() {
        return this.zze;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Vu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                Z23<Vu2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Vu2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
