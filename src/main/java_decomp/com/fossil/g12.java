package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class G12 implements Runnable {
    @DexIgnore
    public /* final */ I12 b;
    @DexIgnore
    public /* final */ H02 c;
    @DexIgnore
    public /* final */ Zy1 d;
    @DexIgnore
    public /* final */ C02 e;

    @DexIgnore
    public G12(I12 i12, H02 h02, Zy1 zy1, C02 c02) {
        this.b = i12;
        this.c = h02;
        this.d = zy1;
        this.e = c02;
    }

    @DexIgnore
    public static Runnable a(I12 i12, H02 h02, Zy1 zy1, C02 c02) {
        return new G12(i12, h02, zy1, c02);
    }

    @DexIgnore
    public void run() {
        I12.c(this.b, this.c, this.d, this.e);
    }
}
