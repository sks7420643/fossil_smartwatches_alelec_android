package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lo6 extends Go6 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ Ho6 e;

    /*
    static {
        String simpleName = Lo6.class.getSimpleName();
        Wg6.b(simpleName, "AboutPresenter::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public Lo6(Ho6 ho6) {
        Wg6.c(ho6, "mView");
        this.e = ho6;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(f, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(f, "presenter stop");
    }

    @DexIgnore
    public void n() {
        this.e.M5(this);
    }
}
