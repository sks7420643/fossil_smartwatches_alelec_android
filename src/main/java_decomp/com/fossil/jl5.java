package com.fossil;

import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AppHelper;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jl5 {
    @DexIgnore
    public static /* final */ PortfolioApp a; // = PortfolioApp.get.instance();
    @DexIgnore
    public static /* final */ Jl5 b; // = new Jl5();

    @DexIgnore
    public static /* synthetic */ Spannable b(Jl5 jl5, String str, String str2, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 2131099703;
        }
        return jl5.a(str, str2, i);
    }

    @DexIgnore
    public final Spannable a(String str, String str2, int i) {
        int G;
        Wg6.c(str, "text");
        Wg6.c(str2, MessengerShareContentUtility.WEBVIEW_RATIO_FULL);
        SpannableString spannableString = new SpannableString(str2);
        if (i != 0 && (G = Wt7.G(str2, str, 0, false, 6, null)) >= 0) {
            spannableString.setSpan(new StyleSpan(1), G, str.length() + G, 33);
            spannableString.setSpan(new ForegroundColorSpan(W6.d(PortfolioApp.get.instance(), i)), G, str.length() + G, 33);
        }
        return spannableString;
    }

    @DexIgnore
    public final String c(Integer num, String str) {
        Wg6.c(str, "errorMessage");
        if (num != null && num.intValue() == 601) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886794);
            Wg6.b(c, "LanguageHelper.getString\u2026ourInternetConnectionAnd)");
            return c;
        } else if (num != null && num.intValue() == 408) {
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886832);
            Wg6.b(c2, "LanguageHelper.getString\u2026asAProblemProcessingThat)");
            return c2;
        } else if ((num != null && num.intValue() == 504) || ((num != null && num.intValue() == 503) || (num != null && num.intValue() == 500))) {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886882);
            Wg6.b(c3, "LanguageHelper.getString\u2026tlyUndergoingMaintenance)");
            return c3;
        } else if (num != null && num.intValue() == 429) {
            String c4 = Um5.c(PortfolioApp.get.instance(), 2131886987);
            Wg6.b(c4, "LanguageHelper.getString\u2026_PleaseTryAgainAfterAFew)");
            return c4;
        } else {
            if (TextUtils.isEmpty(str)) {
                str = Um5.c(PortfolioApp.get.instance(), 2131886832);
            }
            Wg6.b(str, "if (TextUtils.isEmpty(er\u2026   errorMessage\n        }");
            return str;
        }
    }

    @DexIgnore
    public final String d() {
        String f = AppHelper.g.c().f();
        Hr7 hr7 = Hr7.a;
        String format = String.format("%s/%s (Linux; U; Android %s; %s; Build/%s)", Arrays.copyOf(new Object[]{PortfolioApp.get.instance().Q(), f, Build.VERSION.RELEASE, Build.MODEL, f}, 5));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String e(Integer num, String str) {
        Wg6.c(str, "errorMes");
        if (num != null && num.intValue() == 400701) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886292);
            Wg6.b(c, "LanguageHelper.getString\u2026UsersFriendRequestsAreAt)");
            return c;
        } else if ((num != null && num.intValue() == 400700) || (num != null && num.intValue() == 400707)) {
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886293);
            Wg6.b(c2, "LanguageHelper.getString\u2026LimitReachedPleaseReview)");
            return c2;
        } else if (num != null && num.intValue() == 404001) {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886299);
            Wg6.b(c3, "LanguageHelper.getString\u2026t__ThisAccountWasDeleted)");
            return c3;
        } else if (num != null && num.intValue() == 400709) {
            String c4 = Um5.c(PortfolioApp.get.instance(), 2131886302);
            Wg6.b(c4, "LanguageHelper.getString\u2026t__YouAreNoLongerFriends)");
            return c4;
        } else if (num != null && num.intValue() == 404701) {
            String c5 = Um5.c(PortfolioApp.get.instance(), 2131886304);
            Wg6.b(c5, "LanguageHelper.getString\u2026ge_Friends_not_available)");
            return c5;
        } else if (num == null || num.intValue() != 400705) {
            return c(num, str);
        } else {
            String c6 = Um5.c(PortfolioApp.get.instance(), 2131887490);
            Wg6.b(c6, "LanguageHelper.getString\u2026_friend_to_blocked_users)");
            return c6;
        }
    }

    @DexIgnore
    public final ArrayList<String> f() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList.add(Um5.c(PortfolioApp.get.instance(), 2131886667));
        arrayList.add(Um5.c(PortfolioApp.get.instance(), 2131886670));
        arrayList.add(Um5.c(PortfolioApp.get.instance(), 2131886669));
        arrayList.add(Um5.c(PortfolioApp.get.instance(), 2131886671));
        arrayList.add(Um5.c(PortfolioApp.get.instance(), 2131886668));
        return arrayList;
    }

    @DexIgnore
    public final SpannableString g(String str, String str2, float f) {
        Wg6.c(str, "bigText");
        Wg6.c(str2, "smallText");
        SpannableString spannableString = new SpannableString(str + str2);
        spannableString.setSpan(new RelativeSizeSpan(f), str.length(), str.length() + str2.length(), 0);
        return spannableString;
    }

    @DexIgnore
    public final String h(int i) {
        switch (i) {
            case 1:
                String c = Um5.c(PortfolioApp.get.instance(), 2131886137);
                Wg6.b(c, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__S)");
                return c;
            case 2:
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886136);
                Wg6.b(c2, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__M)");
                return c2;
            case 3:
                String c3 = Um5.c(PortfolioApp.get.instance(), 2131886139);
                Wg6.b(c3, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__T)");
                return c3;
            case 4:
                String c4 = Um5.c(PortfolioApp.get.instance(), 2131886141);
                Wg6.b(c4, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__W)");
                return c4;
            case 5:
                String c5 = Um5.c(PortfolioApp.get.instance(), 2131886140);
                Wg6.b(c5, "LanguageHelper.getString\u2026RepeatEnabled_Label__T_1)");
                return c5;
            case 6:
                String c6 = Um5.c(PortfolioApp.get.instance(), 2131886135);
                Wg6.b(c6, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__F)");
                return c6;
            case 7:
                String c7 = Um5.c(PortfolioApp.get.instance(), 2131886138);
                Wg6.b(c7, "LanguageHelper.getString\u2026RepeatEnabled_Label__S_1)");
                return c7;
            default:
                return "";
        }
    }

    @DexIgnore
    public final String i(int i) {
        switch (i) {
            case 1:
                String c = Um5.c(PortfolioApp.get.instance(), 2131886684);
                Wg6.b(c, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sun)");
                return c;
            case 2:
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886681);
                Wg6.b(c2, "LanguageHelper.getString\u2026ain_StepsToday_Text__Mon)");
                return c2;
            case 3:
                String c3 = Um5.c(PortfolioApp.get.instance(), 2131886687);
                Wg6.b(c3, "LanguageHelper.getString\u2026in_StepsToday_Text__Tues)");
                return c3;
            case 4:
                String c4 = Um5.c(PortfolioApp.get.instance(), 2131886688);
                Wg6.b(c4, "LanguageHelper.getString\u2026ain_StepsToday_Text__Wed)");
                return c4;
            case 5:
                String c5 = Um5.c(PortfolioApp.get.instance(), 2131886685);
                Wg6.b(c5, "LanguageHelper.getString\u2026in_StepsToday_Text__Thur)");
                return c5;
            case 6:
                String c6 = Um5.c(PortfolioApp.get.instance(), 2131886680);
                Wg6.b(c6, "LanguageHelper.getString\u2026ain_StepsToday_Text__Fri)");
                return c6;
            case 7:
                String c7 = Um5.c(PortfolioApp.get.instance(), 2131886683);
                Wg6.b(c7, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sat)");
                return c7;
            default:
                return "";
        }
    }

    @DexIgnore
    public final char j(String str) {
        if (TextUtils.isEmpty(str)) {
            return '#';
        }
        if (str != null) {
            char upperCase = Character.toUpperCase(str.charAt(0));
            if (Character.isAlphabetic(upperCase)) {
                return upperCase;
            }
            return '#';
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final String k(String str) {
        Wg6.c(str, LogBuilder.KEY_TIME);
        String string = PortfolioApp.get.instance().getString(2131886080);
        Wg6.b(string, "PortfolioApp.instance.getString(R.string.AM)");
        if (Vt7.i(str, string, false, 2, null)) {
            StringBuilder sb = new StringBuilder();
            String substring = str.substring(0, str.length() - 2);
            Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(Um5.c(PortfolioApp.get.instance(), 2131886763));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        String substring2 = str.substring(0, str.length() - 2);
        Wg6.b(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb2.append(substring2);
        sb2.append(Um5.c(PortfolioApp.get.instance(), 2131886766));
        return sb2.toString();
    }

    @DexIgnore
    public final String l(int i, float f) {
        float h = Dl5.h(((float) 100) * f, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getSleepDaySummaryText", "roundedPercent : " + h);
        StringBuilder sb = new StringBuilder();
        Hr7 hr7 = Hr7.a;
        String string = PortfolioApp.get.instance().getString(i);
        Wg6.b(string, "PortfolioApp.instance.getString(stringId)");
        String format = String.format(string, Arrays.copyOf(new Object[]{String.valueOf(h)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(" %");
        return sb.toString();
    }

    @DexIgnore
    public final String m(int i) {
        String e = Dl5.e(i);
        Wg6.b(e, "NumberHelper.formatNumber(steps)");
        return e;
    }

    @DexIgnore
    public final String n(long j) {
        String format;
        if (j == 0) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886313);
            Wg6.b(c, "LanguageHelper.getString\u2026erBoard_Label__NotSynced)");
            return c;
        }
        if (Xy4.a.b() - j < 3600000) {
            long b2 = (Xy4.a.b() - j) / 60000;
            if (b2 == 0) {
                format = Um5.c(PortfolioApp.get.instance(), 2131886314);
            } else {
                Hr7 hr7 = Hr7.a;
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886205);
                Wg6.b(c2, "LanguageHelper.getString\u2026lenge_Label__StartInMins)");
                String format2 = String.format(c2, Arrays.copyOf(new Object[]{String.valueOf(b2)}, 1));
                Wg6.b(format2, "java.lang.String.format(format, *args)");
                Hr7 hr72 = Hr7.a;
                String c3 = Um5.c(PortfolioApp.get.instance(), 2131886315);
                Wg6.b(c3, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                format = String.format(c3, Arrays.copyOf(new Object[]{format2}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
            }
        } else {
            long b3 = (Xy4.a.b() - j) / 3600000;
            long j2 = (long) 24;
            if (b3 < j2) {
                Hr7 hr73 = Hr7.a;
                String c4 = Um5.c(PortfolioApp.get.instance(), 2131886203);
                Wg6.b(c4, "LanguageHelper.getString\u2026enge_Label__StartInHours)");
                String format3 = String.format(c4, Arrays.copyOf(new Object[]{String.valueOf(b3)}, 1));
                Wg6.b(format3, "java.lang.String.format(format, *args)");
                Hr7 hr74 = Hr7.a;
                String c5 = Um5.c(PortfolioApp.get.instance(), 2131886315);
                Wg6.b(c5, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                format = String.format(c5, Arrays.copyOf(new Object[]{format3}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
            } else {
                Hr7 hr75 = Hr7.a;
                String c6 = Um5.c(PortfolioApp.get.instance(), 2131886202);
                Wg6.b(c6, "LanguageHelper.getString\u2026lenge_Label__StartInDays)");
                String format4 = String.format(c6, Arrays.copyOf(new Object[]{String.valueOf(b3 / j2)}, 1));
                Wg6.b(format4, "java.lang.String.format(format, *args)");
                Hr7 hr76 = Hr7.a;
                String c7 = Um5.c(PortfolioApp.get.instance(), 2131886315);
                Wg6.b(c7, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                format = String.format(c7, Arrays.copyOf(new Object[]{format4}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
            }
        }
        Wg6.b(format, "if ((ExactTime.milliseco\u2026)\n            }\n        }");
        return format;
    }

    @DexIgnore
    public final CharSequence o(int i) {
        String d = Dl5.d(i / 60);
        Wg6.b(d, "NumberHelper.formatBigNumber(hours)");
        String c = Um5.c(a, 2131887116);
        Wg6.b(c, "LanguageHelper.getString\u2026_SetGoalsSleep_Label__Hr)");
        String c2 = Um5.c(a, 2131887117);
        Wg6.b(c2, "LanguageHelper.getString\u2026SetGoalsSleep_Label__Min)");
        CharSequence concat = TextUtils.concat(g(d, c, 0.7f), g(' ' + Dl5.d(i % 60), c2, 0.7f));
        Wg6.b(concat, "TextUtils.concat(activeH\u2026ing, remainMinutesString)");
        return concat;
    }

    @DexIgnore
    public final String p(int i) {
        Hr7 hr7 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131886326);
        Wg6.b(c, "LanguageHelper.getString\u2026r_List_Subtitle__Friends)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String q(int i) {
        Hr7 hr7 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131886327);
        Wg6.b(c, "LanguageHelper.getString\u2026r_List_Subtitle__Members)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String r(int i) {
        Hr7 hr7 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131886328);
        Wg6.b(c, "LanguageHelper.getString\u2026itle__PendingInvitations)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String s(int i) {
        int i2 = i / 3600;
        int i3 = i - ((i2 * 60) * 60);
        int i4 = i3 / 60;
        Hr7 hr7 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131887314);
        Wg6.b(c, "LanguageHelper.getString\u2026_time_hour_minute_second)");
        String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2), Dl5.a(i4), Dl5.a(i3 - (i4 * 60))}, 3));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }
}
