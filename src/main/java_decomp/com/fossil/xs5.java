package com.fossil;

import com.mapped.Er4;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xs5 extends Er4 {
    @DexIgnore
    public String d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Xs5(String str, String str2, String str3) {
        super(str, str2);
        Wg6.c(str, "tagName");
        Wg6.c(str2, "title");
        Wg6.c(str3, "text");
        this.d = str3;
    }

    @DexIgnore
    public final String f() {
        return this.d;
    }

    @DexIgnore
    public final void g(String str) {
        Wg6.c(str, "<set-?>");
        this.d = str;
    }
}
