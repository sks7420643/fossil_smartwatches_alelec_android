package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uw6 implements Factory<UpdateFirmwarePresenter> {
    @DexIgnore
    public static UpdateFirmwarePresenter a(Qw6 qw6, DeviceRepository deviceRepository, UserRepository userRepository, Cj4 cj4, An4 an4, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        return new UpdateFirmwarePresenter(qw6, deviceRepository, userRepository, cj4, an4, updateFirmwareUsecase, downloadFirmwareByDeviceModelUsecase);
    }
}
