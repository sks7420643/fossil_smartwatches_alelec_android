package com.fossil;

import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vv4 implements Factory<BCMemberInChallengeViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;
    @DexIgnore
    public /* final */ Provider<FriendRepository> b;

    @DexIgnore
    public Vv4(Provider<Tt4> provider, Provider<FriendRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Vv4 a(Provider<Tt4> provider, Provider<FriendRepository> provider2) {
        return new Vv4(provider, provider2);
    }

    @DexIgnore
    public static BCMemberInChallengeViewModel c(Tt4 tt4, FriendRepository friendRepository) {
        return new BCMemberInChallengeViewModel(tt4, friendRepository);
    }

    @DexIgnore
    public BCMemberInChallengeViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
