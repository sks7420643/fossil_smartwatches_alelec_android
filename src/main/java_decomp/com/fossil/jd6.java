package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jd6 implements Factory<Lj6> {
    @DexIgnore
    public static Lj6 a(Dd6 dd6) {
        Lj6 f = dd6.f();
        Lk7.c(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
