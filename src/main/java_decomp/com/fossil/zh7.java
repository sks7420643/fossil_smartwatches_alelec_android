package com.fossil;

import android.util.Base64;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zh7 {
    @DexIgnore
    public static byte[] a() {
        return Base64.decode("MDNhOTc2NTExZTJjYmUzYTdmMjY4MDhmYjdhZjNjMDU=", 0);
    }

    @DexIgnore
    public static byte[] b(byte[] bArr) {
        return c(bArr, a());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r5v5, resolved type: byte */
    /* JADX WARN: Multi-variable type inference failed */
    public static byte[] c(byte[] bArr, byte[] bArr2) {
        int[] iArr = new int[256];
        int[] iArr2 = new int[256];
        int length = bArr2.length;
        if (length <= 0 || length > 256) {
            throw new IllegalArgumentException("key must be between 1 and 256 bytes");
        }
        for (int i = 0; i < 256; i++) {
            iArr[i] = i;
            iArr2[i] = bArr2[i % length];
        }
        int i2 = 0;
        for (int i3 = 0; i3 < 256; i3++) {
            i2 = (i2 + iArr[i3] + iArr2[i3]) & 255;
            int i4 = iArr[i3];
            iArr[i3] = iArr[i2];
            iArr[i2] = i4;
        }
        byte[] bArr3 = new byte[bArr.length];
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i5 < bArr.length) {
            int i8 = (i6 + 1) & 255;
            i7 = (iArr[i8] + i7) & 255;
            int i9 = iArr[i8];
            iArr[i8] = iArr[i7];
            iArr[i7] = i9;
            bArr3[i5] = (byte) ((byte) (iArr[(iArr[i8] + iArr[i7]) & 255] ^ bArr[i5]));
            i5++;
            i6 = i8;
        }
        return bArr3;
    }

    @DexIgnore
    public static byte[] d(byte[] bArr) {
        return e(bArr, a());
    }

    @DexIgnore
    public static byte[] e(byte[] bArr, byte[] bArr2) {
        return c(bArr, bArr2);
    }
}
