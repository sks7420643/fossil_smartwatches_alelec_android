package com.fossil;

import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pe extends Qq7 implements Hg6<Lp, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ Yp[] b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Pe(Yp[] ypArr) {
        super(1);
        this.b = ypArr;
    }

    @DexIgnore
    public final boolean a(Lp lp) {
        return !Em7.B(this.b, lp.y);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Boolean invoke(Lp lp) {
        return Boolean.valueOf(a(lp));
    }
}
