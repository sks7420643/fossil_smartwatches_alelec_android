package com.fossil;

import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ub7 {
    @DexIgnore
    public /* final */ Rb7 a;
    @DexIgnore
    public /* final */ List<Qb7> b;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends com.fossil.Qb7> */
    /* JADX WARN: Multi-variable type inference failed */
    public Ub7(Rb7 rb7, List<? extends Qb7> list) {
        Wg6.c(list, "movingObjects");
        this.a = rb7;
        this.b = list;
    }

    @DexIgnore
    public final Rb7 a() {
        return this.a;
    }

    @DexIgnore
    public final List<Qb7> b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ub7) {
                Ub7 ub7 = (Ub7) obj;
                if (!Wg6.a(this.a, ub7.a) || !Wg6.a(this.b, ub7.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Rb7 rb7 = this.a;
        int hashCode = rb7 != null ? rb7.hashCode() : 0;
        List<Qb7> list = this.b;
        if (list != null) {
            i = list.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UIThemeData(background=" + this.a + ", movingObjects=" + this.b + ")";
    }
}
