package com.fossil;

import java.io.File;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Pb4 implements Comparator {
    @DexIgnore
    public static /* final */ Pb4 b; // = new Pb4();

    @DexIgnore
    public static Comparator a() {
        return b;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return Sb4.z((File) obj, (File) obj2);
    }
}
