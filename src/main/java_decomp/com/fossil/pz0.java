package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pz0 implements Rz0 {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public Pz0(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Pz0) && ((Pz0) obj).a.equals(this.a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
