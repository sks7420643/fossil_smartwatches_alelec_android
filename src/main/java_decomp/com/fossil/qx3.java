package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.G04;
import com.google.android.material.card.MaterialCardView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qx3 {
    @DexIgnore
    public static /* final */ int[] t; // = {16842912};
    @DexIgnore
    public static /* final */ double u; // = Math.cos(Math.toRadians(45.0d));
    @DexIgnore
    public /* final */ MaterialCardView a;
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public /* final */ C04 c;
    @DexIgnore
    public /* final */ C04 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Drawable h;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public G04 l;
    @DexIgnore
    public ColorStateList m;
    @DexIgnore
    public Drawable n;
    @DexIgnore
    public LayerDrawable o;
    @DexIgnore
    public C04 p;
    @DexIgnore
    public C04 q;
    @DexIgnore
    public boolean r; // = false;
    @DexIgnore
    public boolean s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends InsetDrawable {
        @DexIgnore
        public Ai(Qx3 qx3, Drawable drawable, int i, int i2, int i3, int i4) {
            super(drawable, i, i2, i3, i4);
        }

        @DexIgnore
        public boolean getPadding(Rect rect) {
            return false;
        }
    }

    @DexIgnore
    public Qx3(MaterialCardView materialCardView, AttributeSet attributeSet, int i2, int i3) {
        this.a = materialCardView;
        C04 c04 = new C04(materialCardView.getContext(), attributeSet, i2, i3);
        this.c = c04;
        c04.M(materialCardView.getContext());
        this.c.a0(-12303292);
        G04.Bi v = this.c.C().v();
        TypedArray obtainStyledAttributes = materialCardView.getContext().obtainStyledAttributes(attributeSet, Tw3.CardView, i2, Sw3.CardView);
        if (obtainStyledAttributes.hasValue(Tw3.CardView_cardCornerRadius)) {
            v.o(obtainStyledAttributes.getDimension(Tw3.CardView_cardCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        }
        this.d = new C04();
        L(v.m());
        Resources resources = materialCardView.getResources();
        this.e = resources.getDimensionPixelSize(Lw3.mtrl_card_checked_icon_margin);
        this.f = resources.getDimensionPixelSize(Lw3.mtrl_card_checked_icon_size);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public boolean A() {
        return this.s;
    }

    @DexIgnore
    public void B(TypedArray typedArray) {
        ColorStateList a2 = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialCardView_strokeColor);
        this.m = a2;
        if (a2 == null) {
            this.m = ColorStateList.valueOf(-1);
        }
        this.g = typedArray.getDimensionPixelSize(Tw3.MaterialCardView_strokeWidth, 0);
        boolean z = typedArray.getBoolean(Tw3.MaterialCardView_android_checkable, false);
        this.s = z;
        this.a.setLongClickable(z);
        this.k = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialCardView_checkedIconTint);
        G(Oz3.d(this.a.getContext(), typedArray, Tw3.MaterialCardView_checkedIcon));
        ColorStateList a3 = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialCardView_rippleColor);
        this.j = a3;
        if (a3 == null) {
            this.j = ColorStateList.valueOf(Vx3.c(this.a, Jw3.colorControlHighlight));
        }
        ColorStateList a4 = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialCardView_cardForegroundColor);
        C04 c04 = this.d;
        if (a4 == null) {
            a4 = ColorStateList.valueOf(0);
        }
        c04.V(a4);
        W();
        T();
        X();
        this.a.setBackgroundInternal(y(this.c));
        Drawable o2 = this.a.isClickable() ? o() : this.d;
        this.h = o2;
        this.a.setForeground(y(o2));
    }

    @DexIgnore
    public void C(int i2, int i3) {
        int i4;
        int i5;
        if (this.o != null) {
            int i6 = this.e;
            int i7 = this.f;
            int i8 = (i2 - i6) - i7;
            if (Mo0.z(this.a) == 1) {
                i4 = i8;
                i5 = i6;
            } else {
                i4 = i6;
                i5 = i8;
            }
            this.o.setLayerInset(2, i5, this.e, i4, (i3 - i6) - i7);
        }
    }

    @DexIgnore
    public void D(boolean z) {
        this.r = z;
    }

    @DexIgnore
    public void E(ColorStateList colorStateList) {
        this.c.V(colorStateList);
    }

    @DexIgnore
    public void F(boolean z) {
        this.s = z;
    }

    @DexIgnore
    public void G(Drawable drawable) {
        this.i = drawable;
        if (drawable != null) {
            Drawable r2 = Am0.r(drawable.mutate());
            this.i = r2;
            Am0.o(r2, this.k);
        }
        if (this.o != null) {
            this.o.setDrawableByLayerId(Nw3.mtrl_card_checked_layer_id, f());
        }
    }

    @DexIgnore
    public void H(ColorStateList colorStateList) {
        this.k = colorStateList;
        Drawable drawable = this.i;
        if (drawable != null) {
            Am0.o(drawable, colorStateList);
        }
    }

    @DexIgnore
    public void I(float f2) {
        L(this.l.w(f2));
        this.h.invalidateSelf();
        if (Q() || P()) {
            S();
        }
        if (Q()) {
            V();
        }
    }

    @DexIgnore
    public void J(float f2) {
        this.c.W(f2);
        C04 c04 = this.d;
        if (c04 != null) {
            c04.W(f2);
        }
        C04 c042 = this.q;
        if (c042 != null) {
            c042.W(f2);
        }
    }

    @DexIgnore
    public void K(ColorStateList colorStateList) {
        this.j = colorStateList;
        W();
    }

    @DexIgnore
    public void L(G04 g04) {
        this.l = g04;
        this.c.setShapeAppearanceModel(g04);
        C04 c04 = this.d;
        if (c04 != null) {
            c04.setShapeAppearanceModel(g04);
        }
        C04 c042 = this.q;
        if (c042 != null) {
            c042.setShapeAppearanceModel(g04);
        }
        C04 c043 = this.p;
        if (c043 != null) {
            c043.setShapeAppearanceModel(g04);
        }
    }

    @DexIgnore
    public void M(ColorStateList colorStateList) {
        if (this.m != colorStateList) {
            this.m = colorStateList;
            X();
        }
    }

    @DexIgnore
    public void N(int i2) {
        if (i2 != this.g) {
            this.g = i2;
            X();
        }
    }

    @DexIgnore
    public void O(int i2, int i3, int i4, int i5) {
        this.b.set(i2, i3, i4, i5);
        S();
    }

    @DexIgnore
    public final boolean P() {
        return this.a.getPreventCornerOverlap() && !e();
    }

    @DexIgnore
    public final boolean Q() {
        return this.a.getPreventCornerOverlap() && e() && this.a.getUseCompatPadding();
    }

    @DexIgnore
    public void R() {
        Drawable drawable = this.h;
        Drawable o2 = this.a.isClickable() ? o() : this.d;
        this.h = o2;
        if (drawable != o2) {
            U(o2);
        }
    }

    @DexIgnore
    public void S() {
        int a2 = (int) ((P() || Q() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) - q());
        MaterialCardView materialCardView = this.a;
        Rect rect = this.b;
        materialCardView.m(rect.left + a2, rect.top + a2, rect.right + a2, a2 + rect.bottom);
    }

    @DexIgnore
    public void T() {
        this.c.U(this.a.getCardElevation());
    }

    @DexIgnore
    public final void U(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 23 || !(this.a.getForeground() instanceof InsetDrawable)) {
            this.a.setForeground(y(drawable));
        } else {
            ((InsetDrawable) this.a.getForeground()).setDrawable(drawable);
        }
    }

    @DexIgnore
    public void V() {
        if (!z()) {
            this.a.setBackgroundInternal(y(this.c));
        }
        this.a.setForeground(y(this.h));
    }

    @DexIgnore
    public final void W() {
        Drawable drawable;
        if (!Tz3.a || (drawable = this.n) == null) {
            C04 c04 = this.p;
            if (c04 != null) {
                c04.V(this.j);
                return;
            }
            return;
        }
        ((RippleDrawable) drawable).setColor(this.j);
    }

    @DexIgnore
    public void X() {
        this.d.e0((float) this.g, this.m);
    }

    @DexIgnore
    public final float a() {
        return Math.max(Math.max(b(this.l.q(), this.c.F()), b(this.l.s(), this.c.G())), Math.max(b(this.l.k(), this.c.s()), b(this.l.i(), this.c.r())));
    }

    @DexIgnore
    public final float b(Zz3 zz3, float f2) {
        return zz3 instanceof F04 ? (float) ((1.0d - u) * ((double) f2)) : zz3 instanceof A04 ? f2 / 2.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float c() {
        return (Q() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + this.a.getMaxCardElevation();
    }

    @DexIgnore
    public final float d() {
        return (Q() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + (this.a.getMaxCardElevation() * 1.5f);
    }

    @DexIgnore
    public final boolean e() {
        return Build.VERSION.SDK_INT >= 21 && this.c.P();
    }

    @DexIgnore
    public final Drawable f() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = this.i;
        if (drawable != null) {
            stateListDrawable.addState(t, drawable);
        }
        return stateListDrawable;
    }

    @DexIgnore
    public final Drawable g() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        C04 i2 = i();
        this.p = i2;
        i2.V(this.j);
        int[] iArr = {16842919};
        stateListDrawable.addState(iArr, this.p);
        return stateListDrawable;
    }

    @DexIgnore
    public final Drawable h() {
        if (!Tz3.a) {
            return g();
        }
        this.q = i();
        return new RippleDrawable(this.j, null, this.q);
    }

    @DexIgnore
    public final C04 i() {
        return new C04(this.l);
    }

    @DexIgnore
    public void j() {
        Drawable drawable = this.n;
        if (drawable != null) {
            Rect bounds = drawable.getBounds();
            int i2 = bounds.bottom;
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i2 - 1);
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i2);
        }
    }

    @DexIgnore
    public C04 k() {
        return this.c;
    }

    @DexIgnore
    public ColorStateList l() {
        return this.c.w();
    }

    @DexIgnore
    public Drawable m() {
        return this.i;
    }

    @DexIgnore
    public ColorStateList n() {
        return this.k;
    }

    @DexIgnore
    public final Drawable o() {
        if (this.n == null) {
            this.n = h();
        }
        if (this.o == null) {
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{this.n, this.d, f()});
            this.o = layerDrawable;
            layerDrawable.setId(2, Nw3.mtrl_card_checked_layer_id);
        }
        return this.o;
    }

    @DexIgnore
    public float p() {
        return this.c.F();
    }

    @DexIgnore
    public final float q() {
        return (!this.a.getPreventCornerOverlap() || (Build.VERSION.SDK_INT >= 21 && !this.a.getUseCompatPadding())) ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : (float) ((1.0d - u) * ((double) this.a.getCardViewRadius()));
    }

    @DexIgnore
    public float r() {
        return this.c.x();
    }

    @DexIgnore
    public ColorStateList s() {
        return this.j;
    }

    @DexIgnore
    public G04 t() {
        return this.l;
    }

    @DexIgnore
    public int u() {
        ColorStateList colorStateList = this.m;
        if (colorStateList == null) {
            return -1;
        }
        return colorStateList.getDefaultColor();
    }

    @DexIgnore
    public ColorStateList v() {
        return this.m;
    }

    @DexIgnore
    public int w() {
        return this.g;
    }

    @DexIgnore
    public Rect x() {
        return this.b;
    }

    @DexIgnore
    public final Drawable y(Drawable drawable) {
        int i2;
        int i3;
        if ((Build.VERSION.SDK_INT < 21) || this.a.getUseCompatPadding()) {
            i2 = (int) Math.ceil((double) d());
            i3 = (int) Math.ceil((double) c());
        } else {
            i2 = 0;
            i3 = 0;
        }
        return new Ai(this, drawable, i3, i2, i3, i2);
    }

    @DexIgnore
    public boolean z() {
        return this.r;
    }
}
