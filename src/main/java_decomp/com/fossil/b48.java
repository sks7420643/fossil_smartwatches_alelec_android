package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B48 implements HostnameVerifier {
    @DexIgnore
    public static /* final */ B48 a; // = new B48();

    @DexIgnore
    public static List<String> a(X509Certificate x509Certificate) {
        List<String> b = b(x509Certificate, 7);
        List<String> b2 = b(x509Certificate, 2);
        ArrayList arrayList = new ArrayList(b.size() + b2.size());
        arrayList.addAll(b);
        arrayList.addAll(b2);
        return arrayList;
    }

    @DexIgnore
    public static List<String> b(X509Certificate x509Certificate, int i) {
        Integer num;
        String str;
        ArrayList arrayList = new ArrayList();
        try {
            Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
            if (subjectAlternativeNames == null) {
                return Collections.emptyList();
            }
            for (List<?> list : subjectAlternativeNames) {
                if (!(list == null || list.size() < 2 || (num = (Integer) list.get(0)) == null || num.intValue() != i || (str = (String) list.get(1)) == null)) {
                    arrayList.add(str);
                }
            }
            return arrayList;
        } catch (CertificateParsingException e) {
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public boolean c(String str, X509Certificate x509Certificate) {
        return B28.J(str) ? f(str, x509Certificate) : e(str, x509Certificate);
    }

    @DexIgnore
    public boolean d(String str, String str2) {
        if (str == null || str.length() == 0 || str.startsWith(CodelessMatcher.CURRENT_CLASS_NAME) || str.endsWith("..") || str2 == null || str2.length() == 0 || str2.startsWith(CodelessMatcher.CURRENT_CLASS_NAME) || str2.endsWith("..")) {
            return false;
        }
        if (!str.endsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
            str = str + '.';
        }
        if (!str2.endsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
            str2 = str2 + '.';
        }
        String lowerCase = str2.toLowerCase(Locale.US);
        if (!lowerCase.contains(G78.ANY_MARKER)) {
            return str.equals(lowerCase);
        }
        if (!lowerCase.startsWith("*.") || lowerCase.indexOf(42, 1) != -1 || str.length() < lowerCase.length() || "*.".equals(lowerCase)) {
            return false;
        }
        String substring = lowerCase.substring(1);
        if (!str.endsWith(substring)) {
            return false;
        }
        int length = str.length() - substring.length();
        return length <= 0 || str.lastIndexOf(46, length + -1) == -1;
    }

    @DexIgnore
    public final boolean e(String str, X509Certificate x509Certificate) {
        String lowerCase = str.toLowerCase(Locale.US);
        for (String str2 : b(x509Certificate, 2)) {
            if (d(lowerCase, str2)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean f(String str, X509Certificate x509Certificate) {
        List<String> b = b(x509Certificate, 7);
        int size = b.size();
        for (int i = 0; i < size; i++) {
            if (str.equalsIgnoreCase(b.get(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean verify(String str, SSLSession sSLSession) {
        try {
            return c(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
        } catch (SSLException e) {
            return false;
        }
    }
}
