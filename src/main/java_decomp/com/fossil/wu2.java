package com.fossil;

import com.fossil.E13;
import com.fossil.Yu2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wu2 extends E13<Wu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Wu2 zzi;
    @DexIgnore
    public static volatile Z23<Wu2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public M13<Yu2> zzd; // = E13.B();
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public long zzg;
    @DexIgnore
    public int zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Wu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Wu2.zzi);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai B(Yu2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).O((Yu2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai C(Yu2 yu2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).O(yu2);
            return this;
        }

        @DexIgnore
        public final Ai E(Iterable<? extends Yu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).P(iterable);
            return this;
        }

        @DexIgnore
        public final Ai G(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).Q(str);
            return this;
        }

        @DexIgnore
        public final Yu2 H(int i) {
            return ((Wu2) this.c).C(i);
        }

        @DexIgnore
        public final List<Yu2> I() {
            return Collections.unmodifiableList(((Wu2) this.c).D());
        }

        @DexIgnore
        public final int J() {
            return ((Wu2) this.c).R();
        }

        @DexIgnore
        public final Ai K(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).S(i);
            return this;
        }

        @DexIgnore
        public final Ai M(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).T(j);
            return this;
        }

        @DexIgnore
        public final Ai N() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).f0();
            return this;
        }

        @DexIgnore
        public final String O() {
            return ((Wu2) this.c).V();
        }

        @DexIgnore
        public final long P() {
            return ((Wu2) this.c).X();
        }

        @DexIgnore
        public final long Q() {
            return ((Wu2) this.c).Z();
        }

        @DexIgnore
        public final Ai x(int i, Yu2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).E(i, (Yu2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai y(int i, Yu2 yu2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).E(i, yu2);
            return this;
        }

        @DexIgnore
        public final Ai z(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Wu2) this.c).G(j);
            return this;
        }
    }

    /*
    static {
        Wu2 wu2 = new Wu2();
        zzi = wu2;
        E13.u(Wu2.class, wu2);
    }
    */

    @DexIgnore
    public static Ai c0() {
        return (Ai) zzi.w();
    }

    @DexIgnore
    public final Yu2 C(int i) {
        return this.zzd.get(i);
    }

    @DexIgnore
    public final List<Yu2> D() {
        return this.zzd;
    }

    @DexIgnore
    public final void E(int i, Yu2 yu2) {
        yu2.getClass();
        e0();
        this.zzd.set(i, yu2);
    }

    @DexIgnore
    public final void G(long j) {
        this.zzc |= 2;
        this.zzf = j;
    }

    @DexIgnore
    public final void O(Yu2 yu2) {
        yu2.getClass();
        e0();
        this.zzd.add(yu2);
    }

    @DexIgnore
    public final void P(Iterable<? extends Yu2> iterable) {
        e0();
        Nz2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final void Q(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zze = str;
    }

    @DexIgnore
    public final int R() {
        return this.zzd.size();
    }

    @DexIgnore
    public final void S(int i) {
        e0();
        this.zzd.remove(i);
    }

    @DexIgnore
    public final void T(long j) {
        this.zzc |= 4;
        this.zzg = j;
    }

    @DexIgnore
    public final String V() {
        return this.zze;
    }

    @DexIgnore
    public final boolean W() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long X() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean Y() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long Z() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean a0() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int b0() {
        return this.zzh;
    }

    @DexIgnore
    public final void e0() {
        M13<Yu2> m13 = this.zzd;
        if (!m13.zza()) {
            this.zzd = E13.q(m13);
        }
    }

    @DexIgnore
    public final void f0() {
        this.zzd = E13.B();
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Wu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002\u1008\u0000\u0003\u1002\u0001\u0004\u1002\u0002\u0005\u1004\u0003", new Object[]{"zzc", "zzd", Yu2.class, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                Z23<Wu2> z232 = zzj;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Wu2.class) {
                    try {
                        z23 = zzj;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzi);
                            zzj = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
