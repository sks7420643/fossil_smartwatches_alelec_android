package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ox2 {
    @DexIgnore
    public static int a(int i, int i2, int i3) {
        return (i & i3) | (i2 & i3);
    }

    @DexIgnore
    public static int b(Object obj, int i) {
        return obj instanceof byte[] ? ((byte[]) obj)[i] & 255 : obj instanceof short[] ? ((short[]) obj)[i] & 65535 : ((int[]) obj)[i];
    }

    @DexIgnore
    public static int c(@NullableDecl Object obj, @NullableDecl Object obj2, int i, Object obj3, int[] iArr, Object[] objArr, @NullableDecl Object[] objArr2) {
        int i2;
        int i3;
        int b = Qx2.b(obj);
        int i4 = b & i;
        int b2 = b(obj3, i4);
        if (b2 == 0) {
            return -1;
        }
        int i5 = -1;
        int i6 = b2;
        while (true) {
            i2 = i6 - 1;
            i3 = iArr[i2];
            if ((i3 & i) != (b & i) || !Qw2.a(obj, objArr[i2]) || (objArr2 != null && !Qw2.a(obj2, objArr2[i2]))) {
                i6 = i3 & i;
                if (i6 == 0) {
                    return -1;
                }
                i5 = i2;
            }
        }
        int i7 = i3 & i;
        if (i5 == -1) {
            e(obj3, i4, i7);
        } else {
            iArr[i5] = a(iArr[i5], i7, i);
        }
        return i2;
    }

    @DexIgnore
    public static Object d(int i) {
        if (i >= 2 && i <= 1073741824 && Integer.highestOneBit(i) == i) {
            return i <= 256 ? new byte[i] : i <= 65536 ? new short[i] : new int[i];
        }
        StringBuilder sb = new StringBuilder(52);
        sb.append("must be power of 2 between 2^1 and 2^30: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public static void e(Object obj, int i, int i2) {
        if (obj instanceof byte[]) {
            ((byte[]) obj)[i] = (byte) ((byte) i2);
        } else if (obj instanceof short[]) {
            ((short[]) obj)[i] = (short) ((short) i2);
        } else {
            ((int[]) obj)[i] = i2;
        }
    }

    @DexIgnore
    public static int f(int i) {
        return (i < 32 ? 4 : 2) * (i + 1);
    }
}
