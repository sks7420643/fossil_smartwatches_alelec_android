package com.fossil;

import io.flutter.util.Predicate;
import io.flutter.view.AccessibilityBridge;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Rk7 implements Predicate {
    @DexIgnore
    public /* final */ /* synthetic */ AccessibilityBridge.SemanticsNode a;

    @DexIgnore
    public /* synthetic */ Rk7(AccessibilityBridge.SemanticsNode semanticsNode) {
        this.a = semanticsNode;
    }

    @DexIgnore
    @Override // io.flutter.util.Predicate
    public final boolean test(Object obj) {
        return AccessibilityBridge.a(this.a, (AccessibilityBridge.SemanticsNode) obj);
    }
}
