package com.fossil;

import android.os.Bundle;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cw6 extends Yv6 {
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public /* final */ Zv6 f;
    @DexIgnore
    public /* final */ ResetPasswordUseCase g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<ResetPasswordUseCase.Ci, ResetPasswordUseCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ Cw6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Cw6 cw6) {
            this.a = cw6;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(ResetPasswordUseCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(ResetPasswordUseCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.q().h();
            int a2 = bi.a();
            if (a2 == 400005) {
                Zv6 q = this.a.q();
                String c = Um5.c(PortfolioApp.get.instance(), 2131886985);
                Wg6.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
                q.r5(c);
                this.a.q().P4(false);
            } else if (a2 != 404001) {
                Zv6 q2 = this.a.q();
                int a3 = bi.a();
                String b = bi.b();
                if (b == null) {
                    b = "";
                }
                q2.o(a3, b);
                this.a.q().P4(false);
            } else {
                Zv6 q3 = this.a.q();
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886980);
                Wg6.b(c2, "LanguageHelper.getString\u2026xt__EmailIsNotRegistered)");
                q3.r5(c2);
                this.a.q().P4(true);
            }
        }

        @DexIgnore
        public void c(ResetPasswordUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.q().h();
            this.a.q().m3();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(ResetPasswordUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore
    public Cw6(Zv6 zv6, ResetPasswordUseCase resetPasswordUseCase) {
        Wg6.c(zv6, "mView");
        Wg6.c(resetPasswordUseCase, "mResetPasswordUseCase");
        this.f = zv6;
        this.g = resetPasswordUseCase;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        this.f.n6(this.e);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Yv6
    public void n(String str) {
        Wg6.c(str, Constants.EMAIL);
        this.e = str;
        if (str.length() == 0) {
            this.f.H2();
        } else {
            this.f.S0();
        }
    }

    @DexIgnore
    @Override // com.fossil.Yv6
    public void o(String str) {
        Wg6.c(str, Constants.EMAIL);
        if (r()) {
            this.f.i();
            this.g.e(new ResetPasswordUseCase.Ai(str), new Ai(this));
        }
    }

    @DexIgnore
    public final void p(String str) {
        Wg6.c(str, Constants.EMAIL);
        this.e = str;
    }

    @DexIgnore
    public final Zv6 q() {
        return this.f;
    }

    @DexIgnore
    public final boolean r() {
        if (this.e.length() == 0) {
            this.f.r5("");
            return false;
        } else if (!B47.a(this.e)) {
            Zv6 zv6 = this.f;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886985);
            Wg6.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            zv6.r5(c);
            return false;
        } else {
            this.f.r5("");
            return true;
        }
    }

    @DexIgnore
    public final void s(String str, Bundle bundle) {
        Wg6.c(str, "key");
        Wg6.c(bundle, "outState");
        bundle.putString(str, this.e);
    }

    @DexIgnore
    public void t() {
        this.f.M5(this);
    }
}
