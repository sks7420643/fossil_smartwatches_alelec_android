package com.fossil;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pe7 extends Qe7 {
    @DexIgnore
    public Pe7(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final void b(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to Settings.System");
            Settings.System.putString(this.a.getContentResolver(), Se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final boolean c() {
        return Se7.d(this.a, "android.permission.WRITE_SETTINGS");
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final String d() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from Settings.System");
            string = Settings.System.getString(this.a.getContentResolver(), Se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="));
        }
        return string;
    }
}
