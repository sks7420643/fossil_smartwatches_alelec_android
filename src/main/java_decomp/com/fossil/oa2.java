package com.fossil;

import android.util.Log;
import android.util.SparseArray;
import com.fossil.R62;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oa2 extends Ra2 {
    @DexIgnore
    public /* final */ SparseArray<Ai> g; // = new SparseArray<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai implements R62.Ci {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ R62 c;
        @DexIgnore
        public /* final */ R62.Ci d;

        @DexIgnore
        public Ai(int i, R62 r62, R62.Ci ci) {
            this.b = i;
            this.c = r62;
            this.d = ci;
            r62.q(this);
        }

        @DexIgnore
        @Override // com.fossil.R72
        public final void n(Z52 z52) {
            String valueOf = String.valueOf(z52);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            Oa2.this.n(z52, this.b);
        }
    }

    @DexIgnore
    public Oa2(O72 o72) {
        super(o72);
        this.b.b1("AutoManageHelper", this);
    }

    @DexIgnore
    public static Oa2 q(N72 n72) {
        O72 d = LifecycleCallback.d(n72);
        Oa2 oa2 = (Oa2) d.S2("AutoManageHelper", Oa2.class);
        return oa2 != null ? oa2 : new Oa2(d);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.g.size(); i++) {
            Ai t = t(i);
            if (t != null) {
                printWriter.append((CharSequence) str).append("GoogleApiClient #").print(t.b);
                printWriter.println(":");
                t.c.h(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        boolean z = this.c;
        String valueOf = String.valueOf(this.g);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (this.d.get() == null) {
            for (int i = 0; i < this.g.size(); i++) {
                Ai t = t(i);
                if (t != null) {
                    t.c.f();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void k() {
        super.k();
        for (int i = 0; i < this.g.size(); i++) {
            Ai t = t(i);
            if (t != null) {
                t.c.g();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ra2
    public final void m(Z52 z52, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        Ai ai = this.g.get(i);
        if (ai != null) {
            r(i);
            R62.Ci ci = ai.d;
            if (ci != null) {
                ci.n(z52);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ra2
    public final void o() {
        for (int i = 0; i < this.g.size(); i++) {
            Ai t = t(i);
            if (t != null) {
                t.c.f();
            }
        }
    }

    @DexIgnore
    public final void r(int i) {
        Ai ai = this.g.get(i);
        this.g.remove(i);
        if (ai != null) {
            ai.c.r(ai);
            ai.c.g();
        }
    }

    @DexIgnore
    public final void s(int i, R62 r62, R62.Ci ci) {
        Rc2.l(r62, "GoogleApiClient instance cannot be null");
        boolean z = this.g.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        Rc2.o(z, sb.toString());
        Qa2 qa2 = this.d.get();
        boolean z2 = this.c;
        String valueOf = String.valueOf(qa2);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.g.put(i, new Ai(i, r62, ci));
        if (this.c && qa2 == null) {
            String valueOf2 = String.valueOf(r62);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            r62.f();
        }
    }

    @DexIgnore
    public final Ai t(int i) {
        if (this.g.size() <= i) {
            return null;
        }
        SparseArray<Ai> sparseArray = this.g;
        return sparseArray.get(sparseArray.keyAt(i));
    }
}
