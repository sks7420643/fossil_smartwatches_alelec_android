package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B62 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<B62> CREATOR; // = new Cg2();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    @Deprecated
    public /* final */ int c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public B62(String str, int i, long j) {
        this.b = str;
        this.c = i;
        this.d = j;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof B62) {
            B62 b62 = (B62) obj;
            return ((c() != null && c().equals(b62.c())) || (c() == null && b62.c() == null)) && f() == b62.f();
        }
    }

    @DexIgnore
    public long f() {
        long j = this.d;
        return j == -1 ? (long) this.c : j;
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(c(), Long.valueOf(f()));
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a("name", c());
        c2.a("version", Long.valueOf(f()));
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 1, c(), false);
        Bd2.n(parcel, 2, this.c);
        Bd2.r(parcel, 3, f());
        Bd2.b(parcel, a2);
    }
}
