package com.fossil;

import com.mapped.Jf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ko7 extends Jf6 implements Mq7<Object> {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public Ko7(int i) {
        this(i, null);
    }

    @DexIgnore
    public Ko7(int i, Xe6<Object> xe6) {
        super(xe6);
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.Mq7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public String toString() {
        if (getCompletion() != null) {
            return super.toString();
        }
        String h = Er7.h(this);
        Wg6.b(h, "Reflection.renderLambdaToString(this)");
        return h;
    }
}
