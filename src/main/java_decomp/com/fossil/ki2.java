package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ki2 implements Parcelable.Creator<DataSet> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataSet createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        ArrayList arrayList2 = null;
        Uh2 uh2 = null;
        int i = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                uh2 = (Uh2) Ad2.e(parcel, t, Uh2.CREATOR);
            } else if (l == 1000) {
                i = Ad2.v(parcel, t);
            } else if (l == 3) {
                Ad2.x(parcel, t, arrayList, Ki2.class.getClassLoader());
            } else if (l == 4) {
                arrayList2 = Ad2.j(parcel, t, Uh2.CREATOR);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                z = Ad2.m(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new DataSet(i, uh2, arrayList, arrayList2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataSet[] newArray(int i) {
        return new DataSet[i];
    }
}
