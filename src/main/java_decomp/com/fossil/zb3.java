package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zb3 extends IInterface {
    @DexIgnore
    void B(Vd3 vd3) throws RemoteException;

    @DexIgnore
    void D2(Rc3 rc3) throws RemoteException;

    @DexIgnore
    Fc3 F1() throws RemoteException;

    @DexIgnore
    void H(Jc3 jc3) throws RemoteException;

    @DexIgnore
    void H2(Nc3 nc3) throws RemoteException;

    @DexIgnore
    void I0(Dd3 dd3, Rg2 rg2) throws RemoteException;

    @DexIgnore
    void J(LatLngBounds latLngBounds) throws RemoteException;

    @DexIgnore
    void J0(Hc3 hc3) throws RemoteException;

    @DexIgnore
    boolean N1() throws RemoteException;

    @DexIgnore
    Ls2 N2(Le3 le3) throws RemoteException;

    @DexIgnore
    Is2 Q(Ee3 ee3) throws RemoteException;

    @DexIgnore
    void R(Yc3 yc3) throws RemoteException;

    @DexIgnore
    boolean R0() throws RemoteException;

    @DexIgnore
    void R2(Tc3 tc3) throws RemoteException;

    @DexIgnore
    void S2(Wc3 wc3) throws RemoteException;

    @DexIgnore
    void U0(Rg2 rg2, Kd3 kd3) throws RemoteException;

    @DexIgnore
    void X(Lc3 lc3) throws RemoteException;

    @DexIgnore
    void Y0(float f) throws RemoteException;

    @DexIgnore
    void a0(int i, int i2, int i3, int i4) throws RemoteException;

    @DexIgnore
    void a2(Td3 td3) throws RemoteException;

    @DexIgnore
    Cc3 b2() throws RemoteException;

    @DexIgnore
    void clear() throws RemoteException;

    @DexIgnore
    void e1(float f) throws RemoteException;

    @DexIgnore
    void h2(Xd3 xd3) throws RemoteException;

    @DexIgnore
    void j2(Rg2 rg2) throws RemoteException;

    @DexIgnore
    void o0(Rg2 rg2) throws RemoteException;

    @DexIgnore
    CameraPosition p0() throws RemoteException;

    @DexIgnore
    void setBuildingsEnabled(boolean z) throws RemoteException;

    @DexIgnore
    boolean setIndoorEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMapType(int i) throws RemoteException;

    @DexIgnore
    void setMyLocationEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setTrafficEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void u1() throws RemoteException;

    @DexIgnore
    boolean v0(Je3 je3) throws RemoteException;

    @DexIgnore
    Os2 w1(Oe3 oe3) throws RemoteException;

    @DexIgnore
    void x1(Rd3 rd3) throws RemoteException;

    @DexIgnore
    Rs2 x2(Qe3 qe3) throws RemoteException;

    @DexIgnore
    float z() throws RemoteException;

    @DexIgnore
    float z2() throws RemoteException;
}
