package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Mg1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Mg1 {
        @DexIgnore
        public /* final */ Dc1 a;
        @DexIgnore
        public /* final */ Od1 b;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> c;

        @DexIgnore
        public Ai(InputStream inputStream, List<ImageHeaderParser> list, Od1 od1) {
            Ik1.d(od1);
            this.b = od1;
            Ik1.d(list);
            this.c = list;
            this.a = new Dc1(inputStream, od1);
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public int a() throws IOException {
            return Lb1.b(this.c, this.a.d(), this.b);
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public Bitmap b(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeStream(this.a.d(), null, options);
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public void c() {
            this.a.c();
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public ImageHeaderParser.ImageType d() throws IOException {
            return Lb1.e(this.c, this.a.d(), this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Mg1 {
        @DexIgnore
        public /* final */ Od1 a;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> b;
        @DexIgnore
        public /* final */ Fc1 c;

        @DexIgnore
        public Bi(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, Od1 od1) {
            Ik1.d(od1);
            this.a = od1;
            Ik1.d(list);
            this.b = list;
            this.c = new Fc1(parcelFileDescriptor);
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public int a() throws IOException {
            return Lb1.a(this.b, this.c, this.a);
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public Bitmap b(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.c.d().getFileDescriptor(), null, options);
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public void c() {
        }

        @DexIgnore
        @Override // com.fossil.Mg1
        public ImageHeaderParser.ImageType d() throws IOException {
            return Lb1.d(this.b, this.c, this.a);
        }
    }

    @DexIgnore
    int a() throws IOException;

    @DexIgnore
    Bitmap b(BitmapFactory.Options options) throws IOException;

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    ImageHeaderParser.ImageType d() throws IOException;
}
