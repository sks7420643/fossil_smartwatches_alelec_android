package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hd4 implements Kd4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Kd4[] b;
    @DexIgnore
    public /* final */ Id4 c;

    @DexIgnore
    public Hd4(int i, Kd4... kd4Arr) {
        this.a = i;
        this.b = kd4Arr;
        this.c = new Id4(i);
    }

    @DexIgnore
    @Override // com.fossil.Kd4
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.a) {
            return stackTraceElementArr;
        }
        Kd4[] kd4Arr = this.b;
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (Kd4 kd4 : kd4Arr) {
            if (stackTraceElementArr2.length <= this.a) {
                break;
            }
            stackTraceElementArr2 = kd4.a(stackTraceElementArr);
        }
        if (stackTraceElementArr2.length > this.a) {
            stackTraceElementArr2 = this.c.a(stackTraceElementArr2);
        }
        return stackTraceElementArr2;
    }
}
