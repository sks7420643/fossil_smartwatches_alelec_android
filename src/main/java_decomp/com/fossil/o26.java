package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O26 implements Factory<QuickResponseViewModel> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;
    @DexIgnore
    public /* final */ Provider<SetReplyMessageMappingUseCase> b;

    @DexIgnore
    public O26(Provider<QuickResponseRepository> provider, Provider<SetReplyMessageMappingUseCase> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static O26 a(Provider<QuickResponseRepository> provider, Provider<SetReplyMessageMappingUseCase> provider2) {
        return new O26(provider, provider2);
    }

    @DexIgnore
    public static QuickResponseViewModel c(QuickResponseRepository quickResponseRepository, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase) {
        return new QuickResponseViewModel(quickResponseRepository, setReplyMessageMappingUseCase);
    }

    @DexIgnore
    public QuickResponseViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
