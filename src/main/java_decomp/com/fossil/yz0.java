package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yz0 implements Interpolator {
    @DexIgnore
    public float[] a;
    @DexIgnore
    public float[] b;

    @DexIgnore
    public Yz0(Context context, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        this(context.getResources(), context.getTheme(), attributeSet, xmlPullParser);
    }

    @DexIgnore
    public Yz0(Resources resources, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        TypedArray k = Ol0.k(resources, theme, attributeSet, Sz0.l);
        d(k, xmlPullParser);
        k.recycle();
    }

    @DexIgnore
    public final void a(float f, float f2, float f3, float f4) {
        Path path = new Path();
        path.moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        path.cubicTo(f, f2, f3, f4, 1.0f, 1.0f);
        b(path);
    }

    @DexIgnore
    public final void b(Path path) {
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float length = pathMeasure.getLength();
        int min = Math.min(3000, ((int) (length / 0.002f)) + 1);
        if (min > 0) {
            this.a = new float[min];
            this.b = new float[min];
            float[] fArr = new float[2];
            for (int i = 0; i < min; i++) {
                pathMeasure.getPosTan((((float) i) * length) / ((float) (min - 1)), fArr, null);
                this.a[i] = fArr[0];
                this.b[i] = fArr[1];
            }
            if (((double) Math.abs(this.a[0])) <= 1.0E-5d && ((double) Math.abs(this.b[0])) <= 1.0E-5d) {
                int i2 = min - 1;
                if (((double) Math.abs(this.a[i2] - 1.0f)) <= 1.0E-5d && ((double) Math.abs(this.b[i2] - 1.0f)) <= 1.0E-5d) {
                    float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    int i3 = 0;
                    int i4 = 0;
                    while (i4 < min) {
                        float[] fArr2 = this.a;
                        float f2 = fArr2[i3];
                        if (f2 >= f) {
                            fArr2[i4] = f2;
                            i4++;
                            i3++;
                            f = f2;
                        } else {
                            throw new IllegalArgumentException("The Path cannot loop back on itself, x :" + f2);
                        }
                    }
                    if (pathMeasure.nextContour()) {
                        throw new IllegalArgumentException("The Path should be continuous, can't have 2+ contours");
                    }
                    return;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("The Path must start at (0,0) and end at (1,1) start: ");
            sb.append(this.a[0]);
            sb.append(",");
            sb.append(this.b[0]);
            sb.append(" end:");
            int i5 = min - 1;
            sb.append(this.a[i5]);
            sb.append(",");
            sb.append(this.b[i5]);
            throw new IllegalArgumentException(sb.toString());
        }
        throw new IllegalArgumentException("The Path has a invalid length " + length);
    }

    @DexIgnore
    public final void c(float f, float f2) {
        Path path = new Path();
        path.moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        path.quadTo(f, f2, 1.0f, 1.0f);
        b(path);
    }

    @DexIgnore
    public final void d(TypedArray typedArray, XmlPullParser xmlPullParser) {
        if (Ol0.j(xmlPullParser, "pathData")) {
            String i = Ol0.i(typedArray, xmlPullParser, "pathData", 4);
            Path e = Rl0.e(i);
            if (e != null) {
                b(e);
                return;
            }
            throw new InflateException("The path is null, which is created from " + i);
        } else if (!Ol0.j(xmlPullParser, "controlX1")) {
            throw new InflateException("pathInterpolator requires the controlX1 attribute");
        } else if (Ol0.j(xmlPullParser, "controlY1")) {
            float f = Ol0.f(typedArray, xmlPullParser, "controlX1", 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f2 = Ol0.f(typedArray, xmlPullParser, "controlY1", 1, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            boolean j = Ol0.j(xmlPullParser, "controlX2");
            if (j != Ol0.j(xmlPullParser, "controlY2")) {
                throw new InflateException("pathInterpolator requires both controlX2 and controlY2 for cubic Beziers.");
            } else if (!j) {
                c(f, f2);
            } else {
                a(f, f2, Ol0.f(typedArray, xmlPullParser, "controlX2", 2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES), Ol0.f(typedArray, xmlPullParser, "controlY2", 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            }
        } else {
            throw new InflateException("pathInterpolator requires the controlY1 attribute");
        }
    }

    @DexIgnore
    public float getInterpolation(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        if (f >= 1.0f) {
            return 1.0f;
        }
        int length = this.a.length - 1;
        int i = 0;
        while (length - i > 1) {
            int i2 = (i + length) / 2;
            if (f < this.a[i2]) {
                length = i2;
            } else {
                i = i2;
            }
        }
        float[] fArr = this.a;
        float f2 = fArr[length] - fArr[i];
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return this.b[i];
        }
        float[] fArr2 = this.b;
        float f3 = fArr2[i];
        return (((f - fArr[i]) / f2) * (fArr2[length] - f3)) + f3;
    }
}
