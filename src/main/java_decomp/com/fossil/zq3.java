package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zq3 extends Wq3 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public Zq3(Yq3 yq3) {
        super(yq3);
        this.b.s(this);
    }

    @DexIgnore
    public final boolean q() {
        return this.c;
    }

    @DexIgnore
    public final void r() {
        if (!q()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void s() {
        if (!this.c) {
            t();
            this.b.e0();
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean t();
}
