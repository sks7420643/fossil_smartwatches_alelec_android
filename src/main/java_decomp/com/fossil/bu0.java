package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bu0<T> {
    @DexIgnore
    public static /* final */ Bu0 e; // = new Bu0(Collections.emptyList(), 0);
    @DexIgnore
    public static /* final */ Bu0 f; // = new Bu0(Collections.emptyList(), 0);
    @DexIgnore
    public /* final */ List<T> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<T> {
        @DexIgnore
        public abstract void a(int i, Bu0<T> bu0);
    }

    @DexIgnore
    public Bu0(List<T> list, int i) {
        this.a = list;
        this.b = 0;
        this.c = 0;
        this.d = i;
    }

    @DexIgnore
    public Bu0(List<T> list, int i, int i2, int i3) {
        this.a = list;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public static <T> Bu0<T> a() {
        return e;
    }

    @DexIgnore
    public static <T> Bu0<T> b() {
        return f;
    }

    @DexIgnore
    public boolean c() {
        return this == f;
    }

    @DexIgnore
    public String toString() {
        return "Result " + this.b + ", " + this.a + ", " + this.c + ", offset " + this.d;
    }
}
