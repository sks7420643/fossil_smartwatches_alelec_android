package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Wi0 {
    @DexIgnore
    void a(Vi0 vi0, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    @DexIgnore
    void b(Vi0 vi0, float f);

    @DexIgnore
    float c(Vi0 vi0);

    @DexIgnore
    float d(Vi0 vi0);

    @DexIgnore
    void e(Vi0 vi0);

    @DexIgnore
    void f(Vi0 vi0, float f);

    @DexIgnore
    float g(Vi0 vi0);

    @DexIgnore
    ColorStateList h(Vi0 vi0);

    @DexIgnore
    void i(Vi0 vi0);

    @DexIgnore
    Object j();  // void declaration

    @DexIgnore
    float k(Vi0 vi0);

    @DexIgnore
    float l(Vi0 vi0);

    @DexIgnore
    void m(Vi0 vi0);

    @DexIgnore
    void n(Vi0 vi0, ColorStateList colorStateList);

    @DexIgnore
    void o(Vi0 vi0, float f);
}
