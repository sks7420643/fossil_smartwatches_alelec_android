package com.fossil;

import com.fossil.I22;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class L22 {
    @DexIgnore
    public static /* final */ L22 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract L22 a();

        @DexIgnore
        public abstract Ai b(int i);

        @DexIgnore
        public abstract Ai c(long j);

        @DexIgnore
        public abstract Ai d(int i);

        @DexIgnore
        public abstract Ai e(int i);

        @DexIgnore
        public abstract Ai f(long j);
    }

    /*
    static {
        Ai a2 = a();
        a2.f(10485760);
        a2.d(200);
        a2.b(10000);
        a2.c(604800000);
        a2.e(81920);
        a = a2.a();
    }
    */

    @DexIgnore
    public static Ai a() {
        return new I22.Bi();
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public abstract long f();
}
