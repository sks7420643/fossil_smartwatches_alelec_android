package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodChannel;
import java.io.ByteArrayOutputStream;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wa8 {
    @DexIgnore
    public static /* final */ Wa8 a; // = new Wa8();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ua8 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(int i, Hg6 hg6, int i2, int i3, int i4, int i5) {
            super(i4, i5);
            this.f = i;
            this.g = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Qj1
        public /* bridge */ /* synthetic */ void b(Object obj, Tj1 tj1) {
            c((Bitmap) obj, tj1);
        }

        @DexIgnore
        @Override // com.fossil.Ua8
        public void c(Bitmap bitmap, Tj1<? super Bitmap> tj1) {
            Wg6.c(bitmap, "resource");
            super.c(bitmap, tj1);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(this.f == 1 ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            this.g.invoke(byteArrayOutputStream.toByteArray());
        }

        @DexIgnore
        @Override // com.fossil.Qj1
        public void j(Drawable drawable) {
            this.g.invoke(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ua8 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(int i, Ya8 ya8, int i2, int i3, int i4, int i5) {
            super(i4, i5);
            this.f = i;
            this.g = ya8;
        }

        @DexIgnore
        @Override // com.fossil.Qj1
        public /* bridge */ /* synthetic */ void b(Object obj, Tj1 tj1) {
            c((Bitmap) obj, tj1);
        }

        @DexIgnore
        @Override // com.fossil.Ua8
        public void c(Bitmap bitmap, Tj1<? super Bitmap> tj1) {
            Wg6.c(bitmap, "resource");
            super.c(bitmap, tj1);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(this.f == 1 ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            this.g.c(byteArrayOutputStream.toByteArray());
        }

        @DexIgnore
        @Override // com.fossil.Va8, com.fossil.Qj1
        public void f(Drawable drawable) {
            this.g.c(null);
        }

        @DexIgnore
        @Override // com.fossil.Qj1
        public void j(Drawable drawable) {
            this.g.c(null);
        }
    }

    @DexIgnore
    public final void a(Context context, Bitmap bitmap, int i, int i2, int i3, Hg6<? super byte[], Cd6> hg6) {
        Wg6.c(context, "ctx");
        Wg6.c(hg6, Constants.CALLBACK);
        Oa1.t(context).e().I0(bitmap).C0(new Ai(i3, hg6, i, i2, i, i2));
    }

    @DexIgnore
    public final void b(Context context, String str, int i, int i2, int i3, MethodChannel.Result result) {
        Wg6.c(context, "ctx");
        Wg6.c(str, "path");
        Oa1.t(context).e().K0(new File(str)).C0(new Bi(i3, new Ya8(result), i, i2, i, i2));
    }
}
