package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X01 {
    @DexIgnore
    public static X01 a; // = null;
    @DexIgnore
    public static /* final */ int b; // = 20;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends X01 {
        @DexIgnore
        public int c;

        @DexIgnore
        public Ai(int i) {
            super(i);
            this.c = i;
        }

        @DexIgnore
        @Override // com.fossil.X01
        public void a(String str, String str2, Throwable... thArr) {
            if (this.c > 3) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.d(str, str2);
            } else {
                Log.d(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.X01
        public void b(String str, String str2, Throwable... thArr) {
            if (this.c > 6) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.e(str, str2);
            } else {
                Log.e(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.X01
        public void d(String str, String str2, Throwable... thArr) {
            if (this.c > 4) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.i(str, str2);
            } else {
                Log.i(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.X01
        public void g(String str, String str2, Throwable... thArr) {
            if (this.c > 2) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.v(str, str2);
            } else {
                Log.v(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.X01
        public void h(String str, String str2, Throwable... thArr) {
            if (this.c > 5) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.w(str, str2);
            } else {
                Log.w(str, str2, thArr[0]);
            }
        }
    }

    @DexIgnore
    public X01(int i) {
    }

    @DexIgnore
    public static X01 c() {
        X01 x01;
        synchronized (X01.class) {
            try {
                if (a == null) {
                    a = new Ai(3);
                }
                x01 = a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return x01;
    }

    @DexIgnore
    public static void e(X01 x01) {
        synchronized (X01.class) {
            try {
                a = x01;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static String f(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        int i = b;
        if (length >= i) {
            sb.append(str.substring(0, i));
        } else {
            sb.append(str);
        }
        return sb.toString();
    }

    @DexIgnore
    public abstract void a(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void b(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void d(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void g(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void h(String str, String str2, Throwable... thArr);
}
