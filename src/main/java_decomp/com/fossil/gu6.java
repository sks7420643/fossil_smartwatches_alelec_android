package com.fossil;

import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gu6 implements Factory<SetWorkoutSettingUseCase> {
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> a;

    @DexIgnore
    public Gu6(Provider<WorkoutSettingRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Gu6 a(Provider<WorkoutSettingRepository> provider) {
        return new Gu6(provider);
    }

    @DexIgnore
    public static SetWorkoutSettingUseCase c(WorkoutSettingRepository workoutSettingRepository) {
        return new SetWorkoutSettingUseCase(workoutSettingRepository);
    }

    @DexIgnore
    public SetWorkoutSettingUseCase b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
