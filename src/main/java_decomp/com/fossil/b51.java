package com.fossil;

import android.content.Context;
import com.fossil.A18;
import com.fossil.Y41;
import com.mapped.Gg6;
import com.mapped.Wg6;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B51 {
    @DexIgnore
    public A18.Ai a;
    @DexIgnore
    public Y41 b;
    @DexIgnore
    public double c;
    @DexIgnore
    public double d; // = Y81.a.f();
    @DexIgnore
    public Z41 e; // = new Z41(null, null, null, false, false, null, null, null, 255, null);
    @DexIgnore
    public /* final */ Context f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Gg6<OkHttpClient> {
        @DexIgnore
        public /* final */ /* synthetic */ B51 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(B51 b51) {
            super(0);
            this.this$0 = b51;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final OkHttpClient invoke() {
            OkHttpClient.b bVar = new OkHttpClient.b();
            bVar.e(R81.b(this.this$0.f));
            OkHttpClient d = bVar.d();
            Wg6.b(d, "OkHttpClient.Builder()\n \u2026xt))\n            .build()");
            return d;
        }
    }

    @DexIgnore
    public B51(Context context) {
        Wg6.c(context, "context");
        this.f = context;
        this.c = Y81.a.d(context);
    }

    @DexIgnore
    public final A51 b() {
        long b2 = Y81.a.b(this.f, this.c);
        long j = (long) (this.d * ((double) b2));
        G51 a2 = G51.a.a(j);
        S61 s61 = new S61(a2);
        C71 a3 = C71.a.a(s61, (int) (b2 - j));
        Context context = this.f;
        Z41 z41 = this.e;
        A18.Ai ai = this.a;
        if (ai == null) {
            ai = c();
        }
        Y41 y41 = this.b;
        if (y41 == null) {
            Y41.Bi bi = Y41.e;
            y41 = new Y41.Ai().d();
        }
        return new C51(context, z41, a2, s61, a3, ai, y41);
    }

    @DexIgnore
    public final A18.Ai c() {
        return W81.p(new Ai(this));
    }
}
