package com.fossil;

import android.content.ContentResolver;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ep4 implements Factory<ContentResolver> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Ep4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Ep4 a(Uo4 uo4) {
        return new Ep4(uo4);
    }

    @DexIgnore
    public static ContentResolver c(Uo4 uo4) {
        ContentResolver l = uo4.l();
        Lk7.c(l, "Cannot return null from a non-@Nullable @Provides method");
        return l;
    }

    @DexIgnore
    public ContentResolver b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
