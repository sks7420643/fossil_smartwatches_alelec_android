package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dx6 implements Factory<ProfileSetupPresenter> {
    @DexIgnore
    public static ProfileSetupPresenter a(Zw6 zw6, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository, DeviceRepository deviceRepository, ServerSettingRepository serverSettingRepository, FlagRepository flagRepository, An4 an4) {
        return new ProfileSetupPresenter(zw6, getRecommendedGoalUseCase, userRepository, deviceRepository, serverSettingRepository, flagRepository, an4);
    }
}
