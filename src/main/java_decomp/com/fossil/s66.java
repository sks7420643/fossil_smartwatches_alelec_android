package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Zc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S66 extends BaseFragment {
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<H55> h;
    @DexIgnore
    public Zc6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final S66 a(String str) {
            Wg6.c(str, "watchAppId");
            S66 s66 = new S66();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCHAPP_ID", str);
            s66.setArguments(bundle);
            return s66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ S66 b;

        @DexIgnore
        public Bi(S66 s66) {
            this.b = s66;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Zc6.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ S66 a;
        @DexIgnore
        public /* final */ /* synthetic */ H55 b;

        @DexIgnore
        public Ci(S66 s66, H55 h55) {
            this.a = s66;
            this.b = h55;
        }

        @DexIgnore
        public final void a(Zc6.Ai ai) {
            Integer a2 = ai.a();
            if (a2 != null) {
                ((Va1) ((Va1) Oa1.v(this.a).r(Integer.valueOf(a2.intValue())).l(Wc1.a)).m0(true)).F0(this.b.r);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Zc6.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        H55 h55 = (H55) Aq0.f(layoutInflater, 2131558541, viewGroup, false, A6());
        PortfolioApp.get.instance().getIface().U0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(Zc6.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ialViewModel::class.java)");
            this.i = (Zc6) a2;
            h55.q.setOnClickListener(new Bi(this));
            Bundle arguments = getArguments();
            if (arguments != null) {
                Zc6 zc6 = this.i;
                if (zc6 != null) {
                    String string = arguments.getString("EXTRA_WATCHAPP_ID");
                    if (string != null) {
                        Wg6.b(string, "it.getString(EXTRA_WATCHAPP_ID)!!");
                        zc6.d(string);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mViewModel");
                    throw null;
                }
            }
            Zc6 zc62 = this.i;
            if (zc62 != null) {
                zc62.c().h(getViewLifecycleOwner(), new Ci(this, h55));
                G37<H55> g37 = new G37<>(this, h55);
                this.h = g37;
                if (g37 != null) {
                    H55 a3 = g37.a();
                    if (a3 != null) {
                        Wg6.b(a3, "mBinding.get()!!");
                        return a3.n();
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
