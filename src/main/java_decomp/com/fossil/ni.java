package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ni extends Lp {
    @DexIgnore
    public /* final */ HashMap<Dr, Object> C;

    @DexIgnore
    public Ni(K5 k5, I60 i60, HashMap<Dr, Object> hashMap) {
        super(k5, i60, Yp.i0, null, false, 24);
        this.C = hashMap;
    }

    @DexIgnore
    public static final /* synthetic */ void G(Ni ni) {
        K5 k5 = ni.w;
        Long l = (Long) ni.C.get(Dr.b);
        Lp.i(ni, new Yr(k5, l != null ? l.longValue() : 60000), Rr.b, We.b, null, new Kf(ni), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.h(this, new Mk(this.w, this.x, this.z), new Yf(this), new Mg(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void I(Nr nr) {
        if (nr.c == Zq.b) {
            l(nr);
            return;
        }
        this.v = nr;
        Lp.i(this, new Ms(this.w), Zg.b, Mh.b, null, new Zh(this), null, 40, null);
    }
}
