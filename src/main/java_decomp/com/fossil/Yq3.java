package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.Av2;
import com.fossil.Ev2;
import com.fossil.Wu2;
import com.fossil.Yu2;
import com.fossil.Zu2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yq3 implements Ln3 {
    @DexIgnore
    public static volatile Yq3 x;
    @DexIgnore
    public Jm3 a;
    @DexIgnore
    public Ol3 b;
    @DexIgnore
    public Kg3 c;
    @DexIgnore
    public Vl3 d;
    @DexIgnore
    public Uq3 e;
    @DexIgnore
    public Pr3 f;
    @DexIgnore
    public /* final */ Gr3 g;
    @DexIgnore
    public Vo3 h;
    @DexIgnore
    public /* final */ Pm3 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public long l;
    @DexIgnore
    public List<Runnable> m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public FileLock s;
    @DexIgnore
    public FileChannel t;
    @DexIgnore
    public List<Long> u;
    @DexIgnore
    public List<Long> v;
    @DexIgnore
    public long w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai implements Mg3 {
        @DexIgnore
        public Av2 a;
        @DexIgnore
        public List<Long> b;
        @DexIgnore
        public List<Wu2> c;
        @DexIgnore
        public long d;

        @DexIgnore
        public Ai(Yq3 yq3) {
        }

        @DexIgnore
        public /* synthetic */ Ai(Yq3 yq3, Br3 br3) {
            this(yq3);
        }

        @DexIgnore
        public static long c(Wu2 wu2) {
            return ((wu2.X() / 1000) / 60) / 60;
        }

        @DexIgnore
        @Override // com.fossil.Mg3
        public final void a(Av2 av2) {
            Rc2.k(av2);
            this.a = av2;
        }

        @DexIgnore
        @Override // com.fossil.Mg3
        public final boolean b(long j, Wu2 wu2) {
            Rc2.k(wu2);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.b == null) {
                this.b = new ArrayList();
            }
            if (this.c.size() > 0 && c(this.c.get(0)) != c(wu2)) {
                return false;
            }
            long l = this.d + ((long) wu2.l());
            if (l >= ((long) Math.max(0, Xg3.i.a(null).intValue()))) {
                return false;
            }
            this.d = l;
            this.c.add(wu2);
            this.b.add(Long.valueOf(j));
            return this.c.size() < Math.max(1, Xg3.j.a(null).intValue());
        }
    }

    @DexIgnore
    public Yq3(Er3 er3) {
        this(er3, null);
    }

    @DexIgnore
    public Yq3(Er3 er3, Pm3 pm3) {
        this.j = false;
        Rc2.k(er3);
        this.i = Pm3.a(er3.a, null, null);
        this.w = -1;
        Gr3 gr3 = new Gr3(this);
        gr3.s();
        this.g = gr3;
        Ol3 ol3 = new Ol3(this);
        ol3.s();
        this.b = ol3;
        Jm3 jm3 = new Jm3(this);
        jm3.s();
        this.a = jm3;
        this.i.c().y(new Br3(this, er3));
    }

    @DexIgnore
    public static void K(Zq3 zq3) {
        if (zq3 == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!zq3.q()) {
            String valueOf = String.valueOf(zq3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public static Yq3 g(Context context) {
        Rc2.k(context);
        Rc2.k(context.getApplicationContext());
        if (x == null) {
            synchronized (Yq3.class) {
                try {
                    if (x == null) {
                        x = new Yq3(new Er3(context));
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return x;
    }

    @DexIgnore
    public static void k(Wu2.Ai ai, int i2, String str) {
        List<Yu2> I = ai.I();
        for (int i3 = 0; i3 < I.size(); i3++) {
            if ("_err".equals(I.get(i3).O())) {
                return;
            }
        }
        Yu2.Ai f0 = Yu2.f0();
        f0.E("_err");
        f0.z(Long.valueOf((long) i2).longValue());
        Yu2.Ai f02 = Yu2.f0();
        f02.E("_ev");
        f02.H(str);
        ai.C((Yu2) ((E13) f0.h()));
        ai.C((Yu2) ((E13) f02.h()));
    }

    @DexIgnore
    public static void l(Wu2.Ai ai, String str) {
        List<Yu2> I = ai.I();
        for (int i2 = 0; i2 < I.size(); i2++) {
            if (str.equals(I.get(i2).O())) {
                ai.K(i2);
                return;
            }
        }
    }

    @DexIgnore
    public static void m(Av2.Ai ai) {
        ai.O(Long.MAX_VALUE);
        ai.T(Long.MIN_VALUE);
        for (int i2 = 0; i2 < ai.M(); i2++) {
            Wu2 N = ai.N(i2);
            if (N.X() < ai.f0()) {
                ai.O(N.X());
            }
            if (N.X() > ai.j0()) {
                ai.T(N.X());
            }
        }
    }

    @DexIgnore
    public final void A(boolean z) {
        l0();
    }

    @DexIgnore
    public final boolean B(int i2, FileChannel fileChannel) {
        i0();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.d().F().a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            if (this.i.w().s(Xg3.z0) && Build.VERSION.SDK_INT <= 19) {
                fileChannel.position(0L);
            }
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() == 4) {
                return true;
            }
            this.i.d().F().b("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            return true;
        } catch (IOException e2) {
            this.i.d().F().b("Failed to write to channel", e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean C(Wu2.Ai ai, Wu2.Ai ai2) {
        String str = null;
        Rc2.a("_e".equals(ai.O()));
        Y();
        Yu2 y = Gr3.y((Wu2) ((E13) ai.h()), "_sc");
        String U = y == null ? null : y.U();
        Y();
        Yu2 y2 = Gr3.y((Wu2) ((E13) ai2.h()), "_pc");
        if (y2 != null) {
            str = y2.U();
        }
        if (str == null || !str.equals(U)) {
            return false;
        }
        I(ai, ai2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:112:0x038e, code lost:
        r15.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x01ae, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:493:0x1024, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:494:0x1027, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:495:0x1028, code lost:
        r15 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:498:0x102f, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:499:0x1030, code lost:
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0313, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0314, code lost:
        r5 = r15;
        r6 = r14;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x038e  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03d3  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0436  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x043e  */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x0691  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x06e3  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01ae  */
    /* JADX WARNING: Removed duplicated region for block: B:492:0x1014  */
    /* JADX WARNING: Removed duplicated region for block: B:493:0x1024 A[ExcHandler: all (th java.lang.Throwable), PHI: r15 
      PHI: (r15v7 android.database.Cursor) = (r15v9 android.database.Cursor), (r15v0 android.database.Cursor), (r15v0 android.database.Cursor) binds: [B:57:0x01e0, B:9:0x003c, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x003c] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:494:0x1027 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:14:0x0070] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean D(java.lang.String r37, long r38) {
        /*
        // Method dump skipped, instructions count: 4182
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yq3.D(java.lang.String, long):boolean");
    }

    @DexIgnore
    public final void E() {
        i0();
        if (this.p || this.q || this.r) {
            this.i.d().N().d("Not stopping services. fetch, network, upload", Boolean.valueOf(this.p), Boolean.valueOf(this.q), Boolean.valueOf(this.r));
            return;
        }
        this.i.d().N().a("Stopping uploading service(s)");
        List<Runnable> list = this.m;
        if (list != null) {
            for (Runnable runnable : list) {
                runnable.run();
            }
            this.m.clear();
        }
    }

    @DexIgnore
    public final boolean F() {
        FileLock fileLock;
        i0();
        if (!this.i.w().s(Xg3.m0) || (fileLock = this.s) == null || !fileLock.isValid()) {
            try {
                FileChannel channel = new RandomAccessFile(new File(this.i.e().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
                this.t = channel;
                FileLock tryLock = channel.tryLock();
                this.s = tryLock;
                if (tryLock != null) {
                    this.i.d().N().a("Storage concurrent access okay");
                    return true;
                }
                this.i.d().F().a("Storage concurrent data access panic");
                return false;
            } catch (FileNotFoundException e2) {
                this.i.d().F().b("Failed to acquire storage lock", e2);
            } catch (IOException e3) {
                this.i.d().F().b("Failed to access storage lock file", e3);
            } catch (OverlappingFileLockException e4) {
                this.i.d().I().b("Storage lock already acquired", e4);
            }
        } else {
            this.i.d().N().a("Storage concurrent access okay");
            return true;
        }
    }

    @DexIgnore
    public final Zr3 G() {
        return this.i.w();
    }

    @DexIgnore
    public final Boolean H(Ll3 ll3) {
        try {
            if (ll3.V() != -2147483648L) {
                if (ll3.V() == ((long) Ag2.a(this.i.e()).e(ll3.t(), 0).versionCode)) {
                    return Boolean.TRUE;
                }
            } else {
                String str = Ag2.a(this.i.e()).e(ll3.t(), 0).versionName;
                if (ll3.T() != null && ll3.T().equals(str)) {
                    return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    @DexIgnore
    public final void I(Wu2.Ai ai, Wu2.Ai ai2) {
        Rc2.a("_e".equals(ai.O()));
        Y();
        Yu2 y = Gr3.y((Wu2) ((E13) ai.h()), "_et");
        if (y.X() && y.Y() > 0) {
            long Y = y.Y();
            Y();
            Yu2 y2 = Gr3.y((Wu2) ((E13) ai2.h()), "_et");
            Y().H(ai2, "_et", Long.valueOf((y2 == null || y2.Y() <= 0) ? Y : y2.Y() + Y));
            Y().H(ai, "_fr", 1L);
        }
    }

    @DexIgnore
    public final void J(Vg3 vg3, Or3 or3) {
        if (J73.a() && this.i.w().s(Xg3.O0)) {
            Pl3 b2 = Pl3.b(vg3);
            this.i.F().L(b2.d, U().A0(or3.b));
            this.i.F().U(b2, this.i.w().n(or3.b));
            vg3 = b2.a();
        }
        o(vg3, or3);
    }

    @DexIgnore
    public final void L(Fr3 fr3, Or3 or3) {
        i0();
        b0();
        if (V(or3)) {
            if (!or3.i) {
                P(or3);
            } else if (!"_npa".equals(fr3.c) || or3.y == null) {
                this.i.d().M().b("Removing user property", this.i.G().z(fr3.c));
                U().u0();
                try {
                    P(or3);
                    U().k0(or3.b, fr3.c);
                    U().u();
                    this.i.d().M().b("User property removed", this.i.G().z(fr3.c));
                } finally {
                    U().z0();
                }
            } else {
                this.i.d().M().a("Falling back to manifest metadata value for ad personalization");
                u(new Fr3("_npa", this.i.zzm().b(), Long.valueOf(or3.y.booleanValue() ? 1 : 0), "auto"), or3);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0373  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x04ab  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01ff  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void M(com.fossil.Or3 r13) {
        /*
        // Method dump skipped, instructions count: 1230
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yq3.M(com.fossil.Or3):void");
    }

    @DexIgnore
    public final void N(Xr3 xr3) {
        Or3 h2 = h(xr3.b);
        if (h2 != null) {
            O(xr3, h2);
        }
    }

    @DexIgnore
    public final void O(Xr3 xr3, Or3 or3) {
        Rc2.k(xr3);
        Rc2.g(xr3.b);
        Rc2.k(xr3.d);
        Rc2.g(xr3.d.c);
        i0();
        b0();
        if (V(or3)) {
            if (!or3.i) {
                P(or3);
                return;
            }
            U().u0();
            try {
                P(or3);
                Xr3 o0 = U().o0(xr3.b, xr3.d.c);
                if (o0 != null) {
                    this.i.d().M().c("Removing conditional user property", xr3.b, this.i.G().z(xr3.d.c));
                    U().q0(xr3.b, xr3.d.c);
                    if (o0.f) {
                        U().k0(xr3.b, xr3.d.c);
                    }
                    if (xr3.l != null) {
                        Bundle bundle = null;
                        if (xr3.l.c != null) {
                            bundle = xr3.l.c.k();
                        }
                        R(this.i.F().D(xr3.b, xr3.l.b, bundle, o0.c, xr3.l.e, true, false), or3);
                    }
                } else {
                    this.i.d().I().c("Conditional user property doesn't exist", Kl3.w(xr3.b), this.i.G().z(xr3.d.c));
                }
                U().u();
            } finally {
                U().z0();
            }
        }
    }

    @DexIgnore
    public final Ll3 P(Or3 or3) {
        i0();
        b0();
        Rc2.k(or3);
        Rc2.g(or3.b);
        Ll3 i0 = U().i0(or3.b);
        String w2 = this.i.z().w(or3.b);
        if (!R63.a() || !this.i.w().s(Xg3.u0)) {
            return f(or3, i0, w2);
        }
        if (i0 == null) {
            i0 = new Ll3(this.i, or3.b);
            i0.c(this.i.F().M0());
            i0.C(w2);
        } else if (!w2.equals(i0.J())) {
            i0.C(w2);
            i0.c(this.i.F().M0());
        }
        i0.r(or3.c);
        i0.v(or3.x);
        if (I73.a() && this.i.w().B(i0.t(), Xg3.o0)) {
            i0.z(or3.B);
        }
        if (!TextUtils.isEmpty(or3.l)) {
            i0.F(or3.l);
        }
        long j2 = or3.f;
        if (j2 != 0) {
            i0.y(j2);
        }
        if (!TextUtils.isEmpty(or3.d)) {
            i0.I(or3.d);
        }
        i0.u(or3.k);
        String str = or3.e;
        if (str != null) {
            i0.L(str);
        }
        i0.B(or3.g);
        i0.e(or3.i);
        if (!TextUtils.isEmpty(or3.h)) {
            i0.O(or3.h);
        }
        if (!this.i.w().s(Xg3.M0)) {
            i0.c0(or3.m);
        }
        i0.s(or3.u);
        i0.w(or3.v);
        i0.b(or3.y);
        i0.E(or3.z);
        if (!i0.f()) {
            return i0;
        }
        U().M(i0);
        return i0;
    }

    @DexIgnore
    public final Jm3 Q() {
        K(this.a);
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02e6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void R(com.fossil.Vg3 r33, com.fossil.Or3 r34) {
        /*
        // Method dump skipped, instructions count: 2668
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yq3.R(com.fossil.Vg3, com.fossil.Or3):void");
    }

    @DexIgnore
    public final Ol3 S() {
        K(this.b);
        return this.b;
    }

    @DexIgnore
    public final String T(Or3 or3) {
        try {
            return (String) this.i.c().v(new Cr3(this, or3)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            this.i.d().F().c("Failed to get app instance id. appId", Kl3.w(or3.b), e2);
            return null;
        }
    }

    @DexIgnore
    public final Kg3 U() {
        K(this.c);
        return this.c;
    }

    @DexIgnore
    public final boolean V(Or3 or3) {
        return (!I73.a() || !this.i.w().B(or3.b, Xg3.o0)) ? !TextUtils.isEmpty(or3.c) || !TextUtils.isEmpty(or3.x) : !TextUtils.isEmpty(or3.c) || !TextUtils.isEmpty(or3.B) || !TextUtils.isEmpty(or3.x);
    }

    @DexIgnore
    public final Pr3 W() {
        K(this.f);
        return this.f;
    }

    @DexIgnore
    public final Vo3 X() {
        K(this.h);
        return this.h;
    }

    @DexIgnore
    public final Gr3 Y() {
        K(this.g);
        return this.g;
    }

    @DexIgnore
    public final Il3 Z() {
        return this.i.G();
    }

    @DexIgnore
    public final int a(FileChannel fileChannel) {
        i0();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.d().F().a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read == 4) {
                allocate.flip();
                return allocate.getInt();
            } else if (read == -1) {
                return 0;
            } else {
                this.i.d().I().b("Unexpected data length. Bytes read", Integer.valueOf(read));
                return 0;
            }
        } catch (IOException e2) {
            this.i.d().F().b("Failed to read from channel", e2);
            return 0;
        }
    }

    @DexIgnore
    public final Kr3 a0() {
        return this.i.F();
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public final Yr3 b() {
        return this.i.b();
    }

    @DexIgnore
    public final void b0() {
        if (!this.j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public final Im3 c() {
        return this.i.c();
    }

    @DexIgnore
    public final void c0() {
        Ll3 i0;
        String str;
        List<Pair<Av2, Long>> list;
        i0();
        b0();
        this.r = true;
        try {
            this.i.b();
            Boolean a0 = this.i.O().a0();
            if (a0 == null) {
                this.i.d().I().a("Upload data called on the client side before use of service was decided");
            } else if (a0.booleanValue()) {
                this.i.d().F().a("Upload called in the client side when service should be used");
                this.r = false;
                E();
            } else if (this.l > 0) {
                l0();
                this.r = false;
                E();
            } else {
                i0();
                if (this.u != null) {
                    this.i.d().N().a("Uploading requested multiple times");
                    this.r = false;
                    E();
                } else if (!S().x()) {
                    this.i.d().N().a("Network not connected, ignoring upload request");
                    l0();
                    this.r = false;
                    E();
                } else {
                    long b2 = this.i.zzm().b();
                    int u2 = this.i.w().u(null, Xg3.Q);
                    long N = Zr3.N();
                    for (int i2 = 0; i2 < u2 && D(null, b2 - N); i2++) {
                    }
                    long a2 = this.i.z().e.a();
                    if (a2 != 0) {
                        this.i.d().M().b("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(b2 - a2)));
                    }
                    String w2 = U().w();
                    if (!TextUtils.isEmpty(w2)) {
                        if (this.w == -1) {
                            this.w = U().X();
                        }
                        List<Pair<Av2, Long>> H = U().H(w2, this.i.w().u(w2, Xg3.g), Math.max(0, this.i.w().u(w2, Xg3.h)));
                        if (!H.isEmpty()) {
                            Iterator<Pair<Av2, Long>> it = H.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    str = null;
                                    break;
                                }
                                Av2 av2 = (Av2) it.next().first;
                                if (!TextUtils.isEmpty(av2.a0())) {
                                    str = av2.a0();
                                    break;
                                }
                            }
                            if (str != null) {
                                int i3 = 0;
                                while (true) {
                                    if (i3 >= H.size()) {
                                        break;
                                    }
                                    Av2 av22 = (Av2) H.get(i3).first;
                                    if (!(TextUtils.isEmpty(av22.a0()) || av22.a0().equals(str))) {
                                        list = H.subList(0, i3);
                                        break;
                                    }
                                    i3++;
                                }
                            }
                            list = H;
                            Zu2.Ai H2 = Zu2.H();
                            int size = list.size();
                            ArrayList arrayList = new ArrayList(list.size());
                            boolean F = this.i.w().F(w2);
                            for (int i4 = 0; i4 < size; i4++) {
                                Av2.Ai ai = (Av2.Ai) ((Av2) list.get(i4).first).x();
                                arrayList.add((Long) list.get(i4).second);
                                ai.l0(this.i.w().C());
                                ai.B(b2);
                                this.i.b();
                                ai.Q(false);
                                if (!F) {
                                    ai.G0();
                                }
                                if (this.i.w().B(w2, Xg3.Z)) {
                                    ai.B0(Y().v(((Av2) ((E13) ai.h())).c()));
                                }
                                H2.x(ai);
                            }
                            String C = this.i.d().B(2) ? Y().C((Zu2) ((E13) H2.h())) : null;
                            Y();
                            byte[] c2 = ((Zu2) ((E13) H2.h())).c();
                            String a3 = Xg3.q.a(null);
                            try {
                                URL url = new URL(a3);
                                Rc2.a(!arrayList.isEmpty());
                                if (this.u != null) {
                                    this.i.d().F().a("Set uploading progress before finishing the previous upload");
                                } else {
                                    this.u = new ArrayList(arrayList);
                                }
                                this.i.z().f.b(b2);
                                String str2 = "?";
                                if (size > 0) {
                                    str2 = H2.y(0).H2();
                                }
                                this.i.d().N().d("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(c2.length), C);
                                this.q = true;
                                Ol3 S = S();
                                Ar3 ar3 = new Ar3(this, w2);
                                S.h();
                                S.r();
                                Rc2.k(url);
                                Rc2.k(c2);
                                Rc2.k(ar3);
                                S.c().B(new Sl3(S, w2, url, c2, null, ar3));
                            } catch (MalformedURLException e2) {
                                this.i.d().F().c("Failed to parse upload URL. Not uploading. appId", Kl3.w(w2), a3);
                            }
                        }
                    } else {
                        this.w = -1;
                        String F2 = U().F(b2 - Zr3.N());
                        if (!TextUtils.isEmpty(F2) && (i0 = U().i0(F2)) != null) {
                            q(i0);
                        }
                    }
                    this.r = false;
                    E();
                }
            }
        } finally {
            this.r = false;
            E();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public final Kl3 d() {
        return this.i.d();
    }

    @DexIgnore
    public final void d0() {
        i0();
        b0();
        if (!this.k) {
            this.k = true;
            if (F()) {
                int a2 = a(this.t);
                int G = this.i.Q().G();
                i0();
                if (a2 > G) {
                    this.i.d().F().c("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(G));
                } else if (a2 >= G) {
                } else {
                    if (B(G, this.t)) {
                        this.i.d().N().c("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(G));
                    } else {
                        this.i.d().F().c("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(G));
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public final Context e() {
        return this.i.e();
    }

    @DexIgnore
    public final void e0() {
        this.o++;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Ll3 f(com.fossil.Or3 r9, com.fossil.Ll3 r10, java.lang.String r11) {
        /*
        // Method dump skipped, instructions count: 406
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yq3.f(com.fossil.Or3, com.fossil.Ll3, java.lang.String):com.fossil.Ll3");
    }

    @DexIgnore
    public final Pm3 f0() {
        return this.i;
    }

    @DexIgnore
    public final Vl3 g0() {
        Vl3 vl3 = this.d;
        if (vl3 != null) {
            return vl3;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    @DexIgnore
    public final Or3 h(String str) {
        Ll3 i0 = U().i0(str);
        if (i0 == null || TextUtils.isEmpty(i0.T())) {
            this.i.d().M().b("No app data available; dropping", str);
            return null;
        }
        Boolean H = H(i0);
        if (H == null || H.booleanValue()) {
            return new Or3(str, i0.A(), i0.T(), i0.V(), i0.X(), i0.Z(), i0.b0(), (String) null, i0.e0(), false, i0.M(), i0.k(), 0L, 0, i0.l(), i0.m(), false, i0.D(), i0.n(), i0.d0(), i0.o(), (!I73.a() || !this.i.w().B(str, Xg3.o0)) ? null : i0.G());
        }
        this.i.d().F().b("App version does not match; dropping. appId", Kl3.w(str));
        return null;
    }

    @DexIgnore
    public final Uq3 h0() {
        K(this.e);
        return this.e;
    }

    @DexIgnore
    public final void i() {
        this.i.c().h();
        U().C0();
        if (this.i.z().e.a() == 0) {
            this.i.z().e.b(this.i.zzm().b());
        }
        l0();
    }

    @DexIgnore
    public final void i0() {
        this.i.c().h();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void j(int i2, Throwable th, byte[] bArr, String str) {
        i0();
        b0();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.q = false;
                E();
                throw th2;
            }
        }
        List<Long> list = this.u;
        this.u = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                this.i.z().e.b(this.i.zzm().b());
                this.i.z().f.b(0);
                l0();
                this.i.d().N().c("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                U().u0();
                try {
                    for (Long l2 : list) {
                        try {
                            Kg3 U = U();
                            long longValue = l2.longValue();
                            U.h();
                            U.r();
                            try {
                                if (U.v().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                    throw new SQLiteException("Deleted fewer rows from queue than expected");
                                }
                            } catch (SQLiteException e2) {
                                U.d().F().b("Failed to delete a bundle in a queue table", e2);
                                throw e2;
                            }
                        } catch (SQLiteException e3) {
                            if (this.v == null || !this.v.contains(l2)) {
                                throw e3;
                            }
                        }
                    }
                    U().u();
                    U().z0();
                    this.v = null;
                    if (!S().x() || !k0()) {
                        this.w = -1;
                        l0();
                    } else {
                        c0();
                    }
                    this.l = 0;
                } catch (Throwable th3) {
                    U().z0();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.i.d().F().b("Database error while trying to delete uploaded bundles", e4);
                this.l = this.i.zzm().c();
                this.i.d().N().b("Disable upload, time", Long.valueOf(this.l));
            }
        } else {
            this.i.d().N().c("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            this.i.z().f.b(this.i.zzm().b());
            if (!(i2 == 503 || i2 == 429)) {
                z = false;
            }
            if (z) {
                this.i.z().g.b(this.i.zzm().b());
            }
            U().O(list);
            l0();
        }
        this.q = false;
        E();
    }

    @DexIgnore
    public final long j0() {
        long b2 = this.i.zzm().b();
        Xl3 z = this.i.z();
        z.o();
        z.h();
        long a2 = z.i.a();
        if (a2 == 0) {
            a2 = 1 + ((long) z.k().G0().nextInt(DateTimeConstants.MILLIS_PER_DAY));
            z.i.b(a2);
        }
        return ((((a2 + b2) / 1000) / 60) / 60) / 24;
    }

    @DexIgnore
    public final boolean k0() {
        i0();
        b0();
        return U().F0() || !TextUtils.isEmpty(U().w());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01f2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void l0() {
        /*
        // Method dump skipped, instructions count: 691
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yq3.l0():void");
    }

    @DexIgnore
    public final void n(Av2.Ai ai, long j2, boolean z) {
        String str = z ? "_se" : "_lte";
        Hr3 n0 = U().n0(ai.x0(), str);
        Hr3 hr3 = (n0 == null || n0.e == null) ? new Hr3(ai.x0(), "auto", str, this.i.zzm().b(), Long.valueOf(j2)) : new Hr3(ai.x0(), "auto", str, this.i.zzm().b(), Long.valueOf(((Long) n0.e).longValue() + j2));
        Ev2.Ai Z = Ev2.Z();
        Z.B(str);
        Z.z(this.i.zzm().b());
        Z.E(((Long) hr3.e).longValue());
        Ev2 ev2 = (Ev2) ((E13) Z.h());
        boolean z2 = false;
        int u2 = Gr3.u(ai, str);
        if (u2 >= 0) {
            ai.z(u2, ev2);
            z2 = true;
        }
        if (!z2) {
            ai.G(ev2);
        }
        if (j2 > 0) {
            U().R(hr3);
            this.i.d().N().c("Updated engagement user property. scope, value", z ? "session-scoped" : "lifetime", hr3.e);
        }
    }

    @DexIgnore
    public final void o(Vg3 vg3, Or3 or3) {
        List<Xr3> J;
        List<Xr3> J2;
        List<Xr3> J3;
        List<String> list;
        Rc2.k(or3);
        Rc2.g(or3.b);
        i0();
        b0();
        String str = or3.b;
        long j2 = vg3.e;
        Y();
        if (Gr3.R(vg3, or3)) {
            if (!or3.i) {
                P(or3);
                return;
            }
            if (this.i.w().B(str, Xg3.c0) && (list = or3.A) != null) {
                if (list.contains(vg3.b)) {
                    Bundle k2 = vg3.c.k();
                    k2.putLong("ga_safelisted", 1);
                    vg3 = new Vg3(vg3.b, new Ug3(k2), vg3.d, vg3.e);
                } else {
                    this.i.d().M().d("Dropping non-safelisted event. appId, event name, origin", str, vg3.b, vg3.d);
                    return;
                }
            }
            U().u0();
            try {
                Kg3 U = U();
                Rc2.g(str);
                U.h();
                U.r();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 < 0) {
                    U.d().I().c("Invalid time querying timed out conditional properties", Kl3.w(str), Long.valueOf(j2));
                    J = Collections.emptyList();
                } else {
                    J = U.J("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (Xr3 xr3 : J) {
                    if (xr3 != null) {
                        this.i.d().N().d("User property timed out", xr3.b, this.i.G().z(xr3.d.c), xr3.d.c());
                        if (xr3.h != null) {
                            R(new Vg3(xr3.h, j2), or3);
                        }
                        U().q0(str, xr3.d.c);
                    }
                }
                Kg3 U2 = U();
                Rc2.g(str);
                U2.h();
                U2.r();
                if (i2 < 0) {
                    U2.d().I().c("Invalid time querying expired conditional properties", Kl3.w(str), Long.valueOf(j2));
                    J2 = Collections.emptyList();
                } else {
                    J2 = U2.J("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(J2.size());
                for (Xr3 xr32 : J2) {
                    if (xr32 != null) {
                        this.i.d().N().d("User property expired", xr32.b, this.i.G().z(xr32.d.c), xr32.d.c());
                        U().k0(str, xr32.d.c);
                        if (xr32.l != null) {
                            arrayList.add(xr32.l);
                        }
                        U().q0(str, xr32.d.c);
                    }
                }
                int size = arrayList.size();
                int i3 = 0;
                while (i3 < size) {
                    Object obj = arrayList.get(i3);
                    i3++;
                    R(new Vg3((Vg3) obj, j2), or3);
                }
                Kg3 U3 = U();
                String str2 = vg3.b;
                Rc2.g(str);
                Rc2.g(str2);
                U3.h();
                U3.r();
                if (i2 < 0) {
                    U3.d().I().d("Invalid time querying triggered conditional properties", Kl3.w(str), U3.j().v(str2), Long.valueOf(j2));
                    J3 = Collections.emptyList();
                } else {
                    J3 = U3.J("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList2 = new ArrayList(J3.size());
                for (Xr3 xr33 : J3) {
                    if (xr33 != null) {
                        Fr3 fr3 = xr33.d;
                        Hr3 hr3 = new Hr3(xr33.b, xr33.c, fr3.c, j2, fr3.c());
                        if (U().R(hr3)) {
                            this.i.d().N().d("User property triggered", xr33.b, this.i.G().z(hr3.c), hr3.e);
                        } else {
                            this.i.d().F().d("Too many active user properties, ignoring", Kl3.w(xr33.b), this.i.G().z(hr3.c), hr3.e);
                        }
                        if (xr33.j != null) {
                            arrayList2.add(xr33.j);
                        }
                        xr33.d = new Fr3(hr3);
                        xr33.f = true;
                        U().S(xr33);
                    }
                }
                R(vg3, or3);
                int size2 = arrayList2.size();
                int i4 = 0;
                while (i4 < size2) {
                    Object obj2 = arrayList2.get(i4);
                    i4++;
                    R(new Vg3((Vg3) obj2, j2), or3);
                }
                U().u();
            } finally {
                U().z0();
            }
        }
    }

    @DexIgnore
    public final void p(Vg3 vg3, String str) {
        Ll3 i0 = U().i0(str);
        if (i0 == null || TextUtils.isEmpty(i0.T())) {
            this.i.d().M().b("No app data available; dropping event", str);
            return;
        }
        Boolean H = H(i0);
        if (H == null) {
            if (!"_ui".equals(vg3.b)) {
                this.i.d().I().b("Could not find package. appId", Kl3.w(str));
            }
        } else if (!H.booleanValue()) {
            this.i.d().F().b("App version does not match; dropping event. appId", Kl3.w(str));
            return;
        }
        J(vg3, new Or3(str, i0.A(), i0.T(), i0.V(), i0.X(), i0.Z(), i0.b0(), (String) null, i0.e0(), false, i0.M(), i0.k(), 0L, 0, i0.l(), i0.m(), false, i0.D(), i0.n(), i0.d0(), i0.o(), (!I73.a() || !this.i.w().B(i0.t(), Xg3.o0)) ? null : i0.G()));
    }

    @DexIgnore
    public final void q(Ll3 ll3) {
        Zi0 zi0;
        i0();
        if (!I73.a() || !this.i.w().B(ll3.t(), Xg3.o0)) {
            if (TextUtils.isEmpty(ll3.A()) && TextUtils.isEmpty(ll3.D())) {
                z(ll3.t(), 204, null, null, null);
                return;
            }
        } else if (TextUtils.isEmpty(ll3.A()) && TextUtils.isEmpty(ll3.G()) && TextUtils.isEmpty(ll3.D())) {
            z(ll3.t(), 204, null, null, null);
            return;
        }
        String q2 = this.i.w().q(ll3);
        try {
            URL url = new URL(q2);
            this.i.d().N().b("Fetching remote configuration", ll3.t());
            Ru2 u2 = Q().u(ll3.t());
            String z = Q().z(ll3.t());
            if (u2 == null || TextUtils.isEmpty(z)) {
                zi0 = null;
            } else {
                zi0 = new Zi0();
                zi0.put("If-Modified-Since", z);
            }
            this.p = true;
            Ol3 S = S();
            String t2 = ll3.t();
            Dr3 dr3 = new Dr3(this);
            S.h();
            S.r();
            Rc2.k(url);
            Rc2.k(dr3);
            S.c().B(new Sl3(S, t2, url, null, zi0, dr3));
        } catch (MalformedURLException e2) {
            this.i.d().F().c("Failed to parse config URL. Not fetching. appId", Kl3.w(ll3.t()), q2);
        }
    }

    @DexIgnore
    public final void s(Zq3 zq3) {
        this.n++;
    }

    @DexIgnore
    public final void t(Er3 er3) {
        this.i.c().h();
        Kg3 kg3 = new Kg3(this);
        kg3.s();
        this.c = kg3;
        this.i.w().r(this.a);
        Pr3 pr3 = new Pr3(this);
        pr3.s();
        this.f = pr3;
        Vo3 vo3 = new Vo3(this);
        vo3.s();
        this.h = vo3;
        Uq3 uq3 = new Uq3(this);
        uq3.s();
        this.e = uq3;
        this.d = new Vl3(this);
        if (this.n != this.o) {
            this.i.d().F().c("Not all upload components initialized", Integer.valueOf(this.n), Integer.valueOf(this.o));
        }
        this.j = true;
    }

    @DexIgnore
    public final void u(Fr3 fr3, Or3 or3) {
        int i2 = 0;
        i0();
        b0();
        if (V(or3)) {
            if (!or3.i) {
                P(or3);
                return;
            }
            int w0 = this.i.F().w0(fr3.c);
            if (w0 != 0) {
                this.i.F();
                String G = Kr3.G(fr3.c, 24, true);
                String str = fr3.c;
                if (str != null) {
                    i2 = str.length();
                }
                this.i.F().V(or3.b, w0, "_ev", G, i2);
                return;
            }
            int o0 = this.i.F().o0(fr3.c, fr3.c());
            if (o0 != 0) {
                this.i.F();
                String G2 = Kr3.G(fr3.c, 24, true);
                Object c2 = fr3.c();
                if (c2 != null && ((c2 instanceof String) || (c2 instanceof CharSequence))) {
                    i2 = String.valueOf(c2).length();
                }
                this.i.F().V(or3.b, o0, "_ev", G2, i2);
                return;
            }
            Object x0 = this.i.F().x0(fr3.c, fr3.c());
            if (x0 != null) {
                if ("_sid".equals(fr3.c)) {
                    long j2 = fr3.d;
                    String str2 = fr3.g;
                    long j3 = 0;
                    Hr3 n0 = U().n0(or3.b, "_sno");
                    if (n0 != null) {
                        Object obj = n0.e;
                        if (obj instanceof Long) {
                            j3 = ((Long) obj).longValue();
                            u(new Fr3("_sno", j2, Long.valueOf(j3 + 1), str2), or3);
                        }
                    }
                    if (n0 != null) {
                        this.i.d().I().b("Retrieved last session number from database does not contain a valid (long) value", n0.e);
                    }
                    Rg3 C = U().C(or3.b, "_s");
                    if (C != null) {
                        long j4 = C.c;
                        this.i.d().N().b("Backfill the session number. Last used session number", Long.valueOf(j4));
                        j3 = j4;
                    }
                    u(new Fr3("_sno", j2, Long.valueOf(j3 + 1), str2), or3);
                }
                Hr3 hr3 = new Hr3(or3.b, fr3.g, fr3.c, fr3.d, x0);
                this.i.d().N().c("Setting user property", this.i.G().z(hr3.c), x0);
                U().u0();
                try {
                    P(or3);
                    boolean R = U().R(hr3);
                    U().u();
                    if (!R) {
                        this.i.d().F().c("Too many unique user properties are set. Ignoring user property", this.i.G().z(hr3.c), hr3.e);
                        this.i.F().V(or3.b, 9, null, null, 0);
                    }
                } finally {
                    U().z0();
                }
            }
        }
    }

    @DexIgnore
    public final void v(Or3 or3) {
        if (this.u != null) {
            ArrayList arrayList = new ArrayList();
            this.v = arrayList;
            arrayList.addAll(this.u);
        }
        Kg3 U = U();
        String str = or3.b;
        Rc2.g(str);
        U.h();
        U.r();
        try {
            SQLiteDatabase v2 = U.v();
            String[] strArr = new String[1];
            strArr[0] = str;
            int delete = v2.delete("default_event_params", "app_id=?", strArr) + v2.delete("apps", "app_id=?", strArr) + 0 + v2.delete("events", "app_id=?", strArr) + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("queue", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr) + v2.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                U.d().N().c("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            U.d().F().c("Error resetting analytics data. appId, error", Kl3.w(str), e2);
        }
        if (or3.i) {
            M(or3);
        }
    }

    @DexIgnore
    public final void w(Xr3 xr3) {
        Or3 h2 = h(xr3.b);
        if (h2 != null) {
            x(xr3, h2);
        }
    }

    @DexIgnore
    public final void x(Xr3 xr3, Or3 or3) {
        boolean z = true;
        Rc2.k(xr3);
        Rc2.g(xr3.b);
        Rc2.k(xr3.c);
        Rc2.k(xr3.d);
        Rc2.g(xr3.d.c);
        i0();
        b0();
        if (V(or3)) {
            if (!or3.i) {
                P(or3);
                return;
            }
            Xr3 xr32 = new Xr3(xr3);
            xr32.f = false;
            U().u0();
            try {
                Xr3 o0 = U().o0(xr32.b, xr32.d.c);
                if (o0 != null && !o0.c.equals(xr32.c)) {
                    this.i.d().I().d("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.i.G().z(xr32.d.c), xr32.c, o0.c);
                }
                if (o0 != null && o0.f) {
                    xr32.c = o0.c;
                    xr32.e = o0.e;
                    xr32.i = o0.i;
                    xr32.g = o0.g;
                    xr32.j = o0.j;
                    xr32.f = o0.f;
                    xr32.d = new Fr3(xr32.d.c, o0.d.d, xr32.d.c(), o0.d.g);
                    z = false;
                } else if (TextUtils.isEmpty(xr32.g)) {
                    xr32.d = new Fr3(xr32.d.c, xr32.e, xr32.d.c(), xr32.d.g);
                    xr32.f = true;
                } else {
                    z = false;
                }
                if (xr32.f) {
                    Fr3 fr3 = xr32.d;
                    Hr3 hr3 = new Hr3(xr32.b, xr32.c, fr3.c, fr3.d, fr3.c());
                    if (U().R(hr3)) {
                        this.i.d().M().d("User property updated immediately", xr32.b, this.i.G().z(hr3.c), hr3.e);
                    } else {
                        this.i.d().F().d("(2)Too many active user properties, ignoring", Kl3.w(xr32.b), this.i.G().z(hr3.c), hr3.e);
                    }
                    if (z && xr32.j != null) {
                        R(new Vg3(xr32.j, xr32.e), or3);
                    }
                }
                if (U().S(xr32)) {
                    this.i.d().M().d("Conditional property added", xr32.b, this.i.G().z(xr32.d.c), xr32.d.c());
                } else {
                    this.i.d().F().d("Too many conditional properties, ignoring", Kl3.w(xr32.b), this.i.G().z(xr32.d.c), xr32.d.c());
                }
                U().u();
            } finally {
                U().z0();
            }
        }
    }

    @DexIgnore
    public final void y(Runnable runnable) {
        i0();
        if (this.m == null) {
            this.m = new ArrayList();
        }
        this.m.add(runnable);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void z(String str, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        boolean z = true;
        i0();
        b0();
        Rc2.g(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.p = false;
                E();
                throw th2;
            }
        }
        this.i.d().N().b("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        U().u0();
        try {
            Ll3 i0 = U().i0(str);
            boolean z2 = (i2 == 200 || i2 == 204 || i2 == 304) && th == null;
            if (i0 == null) {
                this.i.d().I().b("App does not exist in onConfigFetched. appId", Kl3.w(str));
            } else if (z2 || i2 == 404) {
                List<String> list = map != null ? map.get("Last-Modified") : null;
                String str2 = (list == null || list.size() <= 0) ? null : list.get(0);
                if (i2 == 404 || i2 == 304) {
                    if (Q().u(str) == null && !Q().y(str, null, null)) {
                        U().z0();
                        this.p = false;
                        E();
                        return;
                    }
                } else if (!Q().y(str, bArr, str2)) {
                    U().z0();
                    this.p = false;
                    E();
                    return;
                }
                i0.K(this.i.zzm().b());
                U().M(i0);
                if (i2 == 404) {
                    this.i.d().K().b("Config not found. Using empty config. appId", str);
                } else {
                    this.i.d().N().c("Successfully fetched config. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                }
                if (!S().x() || !k0()) {
                    l0();
                } else {
                    c0();
                }
            } else {
                i0.N(this.i.zzm().b());
                U().M(i0);
                this.i.d().N().c("Fetching config failed. code, error", Integer.valueOf(i2), th);
                Q().B(str);
                this.i.z().f.b(this.i.zzm().b());
                if (!(i2 == 503 || i2 == 429)) {
                    z = false;
                }
                if (z) {
                    this.i.z().g.b(this.i.zzm().b());
                }
                l0();
            }
            U().u();
            U().z0();
            this.p = false;
            E();
        } catch (Throwable th3) {
            U().z0();
            throw th3;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public final Ef2 zzm() {
        return this.i.zzm();
    }
}
