package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xi7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ Jg7 c;
    @DexIgnore
    public /* final */ /* synthetic */ Lg7 d;

    @DexIgnore
    public Xi7(Context context, Jg7 jg7, Lg7 lg7) {
        this.b = context;
        this.c = jg7;
        this.d = lg7;
    }

    @DexIgnore
    public final void run() {
        try {
            Kg7 kg7 = new Kg7(this.b, Ig7.a(this.b, false, this.c), this.d.a, this.c);
            kg7.i().c = this.d.c;
            new Ch7(kg7).b();
        } catch (Throwable th) {
            Ig7.m.e(th);
            Ig7.f(this.b, th);
        }
    }
}
