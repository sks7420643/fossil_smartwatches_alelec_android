package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D90 extends Z80 {
    @DexIgnore
    public static long h; // = 60000;
    @DexIgnore
    public static /* final */ D90 i; // = new D90();

    @DexIgnore
    public D90() {
        super("sdk_log", 102400, 20971520, "sdk_log", "sdklog", new Zw1("", "", ""), 1800, new B90(), Ld0.c, true);
    }

    @DexIgnore
    @Override // com.fossil.Z80
    public long e() {
        return h;
    }

    @DexIgnore
    public final void i(Exception exc) {
    }
}
