package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yu4 implements Factory<BCInviteFriendViewModel> {
    @DexIgnore
    public /* final */ Provider<FriendRepository> a;
    @DexIgnore
    public /* final */ Provider<Tt4> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Yu4(Provider<FriendRepository> provider, Provider<Tt4> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Yu4 a(Provider<FriendRepository> provider, Provider<Tt4> provider2, Provider<An4> provider3) {
        return new Yu4(provider, provider2, provider3);
    }

    @DexIgnore
    public static BCInviteFriendViewModel c(FriendRepository friendRepository, Tt4 tt4, An4 an4) {
        return new BCInviteFriendViewModel(friendRepository, tt4, an4);
    }

    @DexIgnore
    public BCInviteFriendViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
