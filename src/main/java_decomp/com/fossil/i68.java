package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i68 {
    @DexIgnore
    public static boolean a(CharSequence charSequence) {
        int length;
        if (!(charSequence == null || (length = charSequence.length()) == 0)) {
            for (int i = 0; i < length; i++) {
                if (!Character.isWhitespace(charSequence.charAt(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean b(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    @DexIgnore
    public static String c(String str, int i, char c) {
        if (str == null) {
            return null;
        }
        int length = i - str.length();
        return length > 0 ? length > 8192 ? d(str, i, String.valueOf(c)) : e(c, length).concat(str) : str;
    }

    @DexIgnore
    public static String d(String str, int i, String str2) {
        if (str == null) {
            return null;
        }
        if (b(str2)) {
            str2 = " ";
        }
        int length = str2.length();
        int length2 = i - str.length();
        if (length2 <= 0) {
            return str;
        }
        if (length == 1 && length2 <= 8192) {
            return c(str, i, str2.charAt(0));
        }
        if (length2 == length) {
            return str2.concat(str);
        }
        if (length2 < length) {
            return str2.substring(0, length2).concat(str);
        }
        char[] cArr = new char[length2];
        char[] charArray = str2.toCharArray();
        for (int i2 = 0; i2 < length2; i2++) {
            cArr[i2] = (char) charArray[i2 % length];
        }
        return new String(cArr).concat(str);
    }

    @DexIgnore
    public static String e(char c, int i) {
        if (i <= 0) {
            return "";
        }
        char[] cArr = new char[i];
        for (int i2 = i - 1; i2 >= 0; i2--) {
            cArr[i2] = (char) c;
        }
        return new String(cArr);
    }

    @DexIgnore
    public static String f(String str, int i) {
        if (str == null) {
            return null;
        }
        if (i <= 0) {
            return "";
        }
        int length = str.length();
        if (i == 1 || length == 0) {
            return str;
        }
        if (length == 1 && i <= 8192) {
            return e(str.charAt(0), i);
        }
        int i2 = length * i;
        if (length == 1) {
            return e(str.charAt(0), i);
        }
        if (length != 2) {
            StringBuilder sb = new StringBuilder(i2);
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(str);
            }
            return sb.toString();
        }
        char charAt = str.charAt(0);
        char charAt2 = str.charAt(1);
        char[] cArr = new char[i2];
        for (int i4 = (i * 2) - 2; i4 >= 0; i4 = (i4 - 1) - 1) {
            cArr[i4] = (char) charAt;
            cArr[i4 + 1] = (char) charAt2;
        }
        return new String(cArr);
    }

    @DexIgnore
    public static String[] g(String str, String str2) {
        return h(str, str2, -1, false);
    }

    @DexIgnore
    public static String[] h(String str, String str2, int i, boolean z) {
        boolean z2;
        boolean z3;
        int i2;
        int i3;
        int i4;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length == 0) {
            return e68.b;
        }
        ArrayList arrayList = new ArrayList();
        if (str2 == null) {
            int i5 = 1;
            int i6 = 0;
            z2 = false;
            boolean z8 = false;
            i3 = 0;
            while (i3 < length) {
                if (Character.isWhitespace(str.charAt(i3))) {
                    if (z8 || z) {
                        if (i5 == i) {
                            i3 = length;
                            z7 = false;
                        } else {
                            z7 = true;
                        }
                        arrayList.add(str.substring(i6, i3));
                        i5++;
                        z6 = false;
                        z2 = z7;
                    } else {
                        z6 = z8;
                    }
                    int i7 = i3 + 1;
                    i6 = i7;
                    z8 = z6;
                    i3 = i7;
                } else {
                    i3++;
                    z2 = false;
                    z8 = true;
                }
            }
            i2 = i6;
            z3 = z8;
        } else {
            if (str2.length() == 1) {
                char charAt = str2.charAt(0);
                int i8 = 1;
                int i9 = 0;
                boolean z9 = false;
                z2 = false;
                i4 = 0;
                while (i4 < length) {
                    if (str.charAt(i4) == charAt) {
                        if (z9 || z) {
                            if (i8 == i) {
                                z5 = false;
                                i4 = length;
                            } else {
                                z5 = true;
                            }
                            arrayList.add(str.substring(i9, i4));
                            i8++;
                            z4 = false;
                            z2 = z5;
                        } else {
                            z4 = z9;
                        }
                        int i10 = i4 + 1;
                        i9 = i10;
                        z9 = z4;
                        i4 = i10;
                    } else {
                        i4++;
                        z9 = true;
                        z2 = false;
                    }
                }
                z3 = z9;
                i2 = i9;
            } else {
                int i11 = 1;
                int i12 = 0;
                boolean z10 = false;
                z3 = false;
                i4 = 0;
                while (i4 < length) {
                    if (str2.indexOf(str.charAt(i4)) >= 0) {
                        if (z3 || z) {
                            if (i11 == i) {
                                z10 = false;
                                i4 = length;
                            } else {
                                z10 = true;
                            }
                            arrayList.add(str.substring(i12, i4));
                            i11++;
                            z3 = false;
                        }
                        int i13 = i4 + 1;
                        i12 = i13;
                        i4 = i13;
                    } else {
                        i4++;
                        z10 = false;
                        z3 = true;
                    }
                }
                z2 = z10;
                i2 = i12;
            }
            i3 = i4;
        }
        if (z3 || (z && z2)) {
            arrayList.add(str.substring(i2, i3));
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    @DexIgnore
    public static String i(String str, int i, int i2) {
        int i3 = 0;
        if (str == null) {
            return null;
        }
        int length = i2 < 0 ? str.length() + i2 : i2;
        if (i < 0) {
            i += str.length();
        }
        if (length > str.length()) {
            length = str.length();
        }
        if (i > length) {
            return "";
        }
        if (i < 0) {
            i = 0;
        }
        if (length >= 0) {
            i3 = length;
        }
        return str.substring(i, i3);
    }
}
