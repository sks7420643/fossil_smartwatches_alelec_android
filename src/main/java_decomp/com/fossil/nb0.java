package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nb0 implements Parcelable.Creator<Ob0> {
    @DexIgnore
    public /* synthetic */ Nb0(Qg6 qg6) {
    }

    @DexIgnore
    public Ob0 a(Parcel parcel) {
        return new Ob0(parcel, (Qg6) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ob0 createFromParcel(Parcel parcel) {
        return new Ob0(parcel, (Qg6) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ob0[] newArray(int i) {
        return new Ob0[i];
    }
}
