package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.It0;
import com.fossil.Jt0;
import com.fossil.Kt0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ht0 extends Service {
    @DexIgnore
    public static /* final */ boolean g; // = Log.isLoggable("MBServiceCompat", 3);
    @DexIgnore
    public Gi b;
    @DexIgnore
    public /* final */ Zi0<IBinder, Fi> c; // = new Zi0<>();
    @DexIgnore
    public Fi d;
    @DexIgnore
    public /* final */ Qi e; // = new Qi();
    @DexIgnore
    public MediaSessionCompat.Token f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Mi<List<MediaBrowserCompat.MediaItem>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fi f;
        @DexIgnore
        public /* final */ /* synthetic */ String g;
        @DexIgnore
        public /* final */ /* synthetic */ Bundle h;
        @DexIgnore
        public /* final */ /* synthetic */ Bundle i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Object obj, Fi fi, String str, Bundle bundle, Bundle bundle2) {
            super(obj);
            this.f = fi;
            this.g = str;
            this.h = bundle;
            this.i = bundle2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ht0.Mi
        public /* bridge */ /* synthetic */ void d(List<MediaBrowserCompat.MediaItem> list) {
            h(list);
        }

        @DexIgnore
        public void h(List<MediaBrowserCompat.MediaItem> list) {
            if (Ht0.this.c.get(this.f.c.asBinder()) == this.f) {
                if ((a() & 1) != 0) {
                    list = Ht0.this.b(list, this.h);
                }
                try {
                    this.f.c.a(this.g, list, this.h, this.i);
                } catch (RemoteException e) {
                    Log.w("MBServiceCompat", "Calling onLoadChildren() failed for id=" + this.g + " package=" + this.f.a);
                }
            } else if (Ht0.g) {
                Log.d("MBServiceCompat", "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + this.f.a + " id=" + this.g);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Mi<MediaBrowserCompat.MediaItem> {
        @DexIgnore
        public /* final */ /* synthetic */ He0 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Ht0 ht0, Object obj, He0 he0) {
            super(obj);
            this.f = he0;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ht0.Mi
        public /* bridge */ /* synthetic */ void d(MediaBrowserCompat.MediaItem mediaItem) {
            h(mediaItem);
        }

        @DexIgnore
        public void h(MediaBrowserCompat.MediaItem mediaItem) {
            if ((a() & 2) != 0) {
                this.f.b(-1, null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("media_item", mediaItem);
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Mi<List<MediaBrowserCompat.MediaItem>> {
        @DexIgnore
        public /* final */ /* synthetic */ He0 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Ht0 ht0, Object obj, He0 he0) {
            super(obj);
            this.f = he0;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ht0.Mi
        public /* bridge */ /* synthetic */ void d(List<MediaBrowserCompat.MediaItem> list) {
            h(list);
        }

        @DexIgnore
        public void h(List<MediaBrowserCompat.MediaItem> list) {
            if ((a() & 4) != 0 || list == null) {
                this.f.b(-1, null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArray("search_results", (Parcelable[]) list.toArray(new MediaBrowserCompat.MediaItem[0]));
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Mi<Bundle> {
        @DexIgnore
        public /* final */ /* synthetic */ He0 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Ht0 ht0, Object obj, He0 he0) {
            super(obj);
            this.f = he0;
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Mi
        public void c(Bundle bundle) {
            this.f.b(-1, bundle);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ht0.Mi
        public /* bridge */ /* synthetic */ void d(Bundle bundle) {
            h(bundle);
        }

        @DexIgnore
        public void h(Bundle bundle) {
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei {
        @DexIgnore
        public abstract Bundle a();

        @DexIgnore
        public abstract String b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements IBinder.DeathRecipient {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Bundle b;
        @DexIgnore
        public /* final */ Oi c;
        @DexIgnore
        public /* final */ HashMap<String, List<Ln0<IBinder, Bundle>>> d; // = new HashMap<>();
        @DexIgnore
        public Ei e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public void run() {
                Fi fi = Fi.this;
                Ht0.this.c.remove(fi.c.asBinder());
            }
        }

        @DexIgnore
        public Fi(String str, int i, int i2, Bundle bundle, Oi oi) {
            this.a = str;
            new Lt0(str, i, i2);
            this.b = bundle;
            this.c = oi;
        }

        @DexIgnore
        public void binderDied() {
            Ht0.this.e.post(new Aii());
        }
    }

    @DexIgnore
    public interface Gi {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        IBinder d(Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi implements Gi, It0.Di {
        @DexIgnore
        public /* final */ List<Bundle> a; // = new ArrayList();
        @DexIgnore
        public Object b;
        @DexIgnore
        public Messenger c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Mi<List<MediaBrowserCompat.MediaItem>> {
            @DexIgnore
            public /* final */ /* synthetic */ It0.Ci f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Hi hi, Object obj, It0.Ci ci) {
                super(obj);
                this.f = ci;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ht0.Mi
            public /* bridge */ /* synthetic */ void d(List<MediaBrowserCompat.MediaItem> list) {
                h(list);
            }

            @DexIgnore
            public void h(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    ArrayList arrayList2 = new ArrayList();
                    for (MediaBrowserCompat.MediaItem mediaItem : list) {
                        Parcel obtain = Parcel.obtain();
                        mediaItem.writeToParcel(obtain, 0);
                        arrayList2.add(obtain);
                    }
                    arrayList = arrayList2;
                } else {
                    arrayList = null;
                }
                this.f.b(arrayList);
            }
        }

        @DexIgnore
        public Hi() {
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Gi
        public void a() {
            Object a2 = It0.a(Ht0.this, this);
            this.b = a2;
            It0.c(a2);
        }

        @DexIgnore
        @Override // com.fossil.It0.Di
        public void c(String str, It0.Ci<List<Parcel>> ci) {
            Ht0.this.f(str, new Aii(this, str, ci));
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Gi
        public IBinder d(Intent intent) {
            return It0.b(this.b, intent);
        }

        @DexIgnore
        @Override // com.fossil.It0.Di
        public It0.Ai f(String str, int i, Bundle bundle) {
            Bundle bundle2;
            if (bundle == null || bundle.getInt("extra_client_version", 0) == 0) {
                bundle2 = null;
            } else {
                bundle.remove("extra_client_version");
                this.c = new Messenger(Ht0.this.e);
                Bundle bundle3 = new Bundle();
                bundle3.putInt("extra_service_version", 2);
                Vk0.b(bundle3, "extra_messenger", this.c.getBinder());
                MediaSessionCompat.Token token = Ht0.this.f;
                if (token != null) {
                    Be0 c2 = token.c();
                    Vk0.b(bundle3, "extra_session_binder", c2 == null ? null : c2.asBinder());
                    bundle2 = bundle3;
                } else {
                    this.a.add(bundle3);
                    bundle2 = bundle3;
                }
            }
            Ht0 ht0 = Ht0.this;
            ht0.d = new Fi(str, -1, i, bundle, null);
            Ei e = Ht0.this.e(str, i, bundle);
            Ht0.this.d = null;
            if (e == null) {
                return null;
            }
            if (bundle2 == null) {
                bundle2 = e.a();
            } else if (e.a() != null) {
                bundle2.putAll(e.a());
            }
            return new It0.Ai(e.b(), bundle2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii extends Hi implements Jt0.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Mi<MediaBrowserCompat.MediaItem> {
            @DexIgnore
            public /* final */ /* synthetic */ It0.Ci f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ii ii, Object obj, It0.Ci ci) {
                super(obj);
                this.f = ci;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ht0.Mi
            public /* bridge */ /* synthetic */ void d(MediaBrowserCompat.MediaItem mediaItem) {
                h(mediaItem);
            }

            @DexIgnore
            public void h(MediaBrowserCompat.MediaItem mediaItem) {
                if (mediaItem == null) {
                    this.f.b(null);
                    return;
                }
                Parcel obtain = Parcel.obtain();
                mediaItem.writeToParcel(obtain, 0);
                this.f.b(obtain);
            }
        }

        @DexIgnore
        public Ii() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Hi, com.fossil.Ht0.Gi
        public void a() {
            Object a2 = Jt0.a(Ht0.this, this);
            this.b = a2;
            It0.c(a2);
        }

        @DexIgnore
        @Override // com.fossil.Jt0.Bi
        public void b(String str, It0.Ci<Parcel> ci) {
            Ht0.this.h(str, new Aii(this, str, ci));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji extends Ii implements Kt0.Ci {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Mi<List<MediaBrowserCompat.MediaItem>> {
            @DexIgnore
            public /* final */ /* synthetic */ Kt0.Bi f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ji ji, Object obj, Kt0.Bi bi) {
                super(obj);
                this.f = bi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ht0.Mi
            public /* bridge */ /* synthetic */ void d(List<MediaBrowserCompat.MediaItem> list) {
                h(list);
            }

            @DexIgnore
            public void h(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    ArrayList arrayList2 = new ArrayList();
                    for (MediaBrowserCompat.MediaItem mediaItem : list) {
                        Parcel obtain = Parcel.obtain();
                        mediaItem.writeToParcel(obtain, 0);
                        arrayList2.add(obtain);
                    }
                    arrayList = arrayList2;
                } else {
                    arrayList = null;
                }
                this.f.b(arrayList, a());
            }
        }

        @DexIgnore
        public Ji() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Ii, com.fossil.Ht0.Hi, com.fossil.Ht0.Gi
        public void a() {
            Object a2 = Kt0.a(Ht0.this, this);
            this.b = a2;
            It0.c(a2);
        }

        @DexIgnore
        @Override // com.fossil.Kt0.Ci
        public void e(String str, Kt0.Bi bi, Bundle bundle) {
            Ht0.this.g(str, new Aii(this, str, bi), bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ki extends Ji {
        @DexIgnore
        public Ki(Ht0 ht0) {
            super();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Li implements Gi {
        @DexIgnore
        public Messenger a;

        @DexIgnore
        public Li() {
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Gi
        public void a() {
            this.a = new Messenger(Ht0.this.e);
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Gi
        public IBinder d(Intent intent) {
            if ("android.media.browse.MediaBrowserService".equals(intent.getAction())) {
                return this.a.getBinder();
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Mi<T> {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;

        @DexIgnore
        public Mi(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public int a() {
            return this.e;
        }

        @DexIgnore
        public boolean b() {
            return this.b || this.c || this.d;
        }

        @DexIgnore
        public void c(Bundle bundle) {
            throw new UnsupportedOperationException("It is not supported to send an error for " + this.a);
        }

        @DexIgnore
        public abstract void d(T t);

        @DexIgnore
        public void e(Bundle bundle) {
            if (this.c || this.d) {
                throw new IllegalStateException("sendError() called when either sendResult() or sendError() had already been called for: " + this.a);
            }
            this.d = true;
            c(bundle);
        }

        @DexIgnore
        public void f(T t) {
            if (this.c || this.d) {
                throw new IllegalStateException("sendResult() called when either sendResult() or sendError() had already been called for: " + this.a);
            }
            this.c = true;
            d(t);
        }

        @DexIgnore
        public void g(int i) {
            this.e = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ni {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ int d;
            @DexIgnore
            public /* final */ /* synthetic */ int e;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle f;

            @DexIgnore
            public Aii(Oi oi, String str, int i, int i2, Bundle bundle) {
                this.b = oi;
                this.c = str;
                this.d = i;
                this.e = i2;
                this.f = bundle;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.b.asBinder();
                Ht0.this.c.remove(asBinder);
                Fi fi = new Fi(this.c, this.d, this.e, this.f, this.b);
                Ht0 ht0 = Ht0.this;
                ht0.d = fi;
                Ei e2 = ht0.e(this.c, this.e, this.f);
                fi.e = e2;
                Ht0 ht02 = Ht0.this;
                ht02.d = null;
                if (e2 == null) {
                    Log.i("MBServiceCompat", "No root for client " + this.c + " from service " + Aii.class.getName());
                    try {
                        this.b.b();
                    } catch (RemoteException e3) {
                        Log.w("MBServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + this.c);
                    }
                } else {
                    try {
                        ht02.c.put(asBinder, fi);
                        asBinder.linkToDeath(fi, 0);
                        if (Ht0.this.f != null) {
                            this.b.c(fi.e.b(), Ht0.this.f, fi.e.a());
                        }
                    } catch (RemoteException e4) {
                        Log.w("MBServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + this.c);
                        Ht0.this.c.remove(asBinder);
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;

            @DexIgnore
            public Bii(Oi oi) {
                this.b = oi;
            }

            @DexIgnore
            public void run() {
                Fi remove = Ht0.this.c.remove(this.b.asBinder());
                if (remove != null) {
                    remove.c.asBinder().unlinkToDeath(remove, 0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Cii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ IBinder d;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle e;

            @DexIgnore
            public Cii(Oi oi, String str, IBinder iBinder, Bundle bundle) {
                this.b = oi;
                this.c = str;
                this.d = iBinder;
                this.e = bundle;
            }

            @DexIgnore
            public void run() {
                Fi fi = Ht0.this.c.get(this.b.asBinder());
                if (fi == null) {
                    Log.w("MBServiceCompat", "addSubscription for callback that isn't registered id=" + this.c);
                    return;
                }
                Ht0.this.a(this.c, fi, this.d, this.e);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Dii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ IBinder d;

            @DexIgnore
            public Dii(Oi oi, String str, IBinder iBinder) {
                this.b = oi;
                this.c = str;
                this.d = iBinder;
            }

            @DexIgnore
            public void run() {
                Fi fi = Ht0.this.c.get(this.b.asBinder());
                if (fi == null) {
                    Log.w("MBServiceCompat", "removeSubscription for callback that isn't registered id=" + this.c);
                } else if (!Ht0.this.p(this.c, fi, this.d)) {
                    Log.w("MBServiceCompat", "removeSubscription called for " + this.c + " which is not subscribed");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Eii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ He0 d;

            @DexIgnore
            public Eii(Oi oi, String str, He0 he0) {
                this.b = oi;
                this.c = str;
                this.d = he0;
            }

            @DexIgnore
            public void run() {
                Fi fi = Ht0.this.c.get(this.b.asBinder());
                if (fi == null) {
                    Log.w("MBServiceCompat", "getMediaItem for callback that isn't registered id=" + this.c);
                    return;
                }
                Ht0.this.n(this.c, fi, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Fii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ int d;
            @DexIgnore
            public /* final */ /* synthetic */ int e;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle f;

            @DexIgnore
            public Fii(Oi oi, String str, int i, int i2, Bundle bundle) {
                this.b = oi;
                this.c = str;
                this.d = i;
                this.e = i2;
                this.f = bundle;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.b.asBinder();
                Ht0.this.c.remove(asBinder);
                Fi fi = new Fi(this.c, this.d, this.e, this.f, this.b);
                Ht0.this.c.put(asBinder, fi);
                try {
                    asBinder.linkToDeath(fi, 0);
                } catch (RemoteException e2) {
                    Log.w("MBServiceCompat", "IBinder is already dead.");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Gii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;

            @DexIgnore
            public Gii(Oi oi) {
                this.b = oi;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.b.asBinder();
                Fi remove = Ht0.this.c.remove(asBinder);
                if (remove != null) {
                    asBinder.unlinkToDeath(remove, 0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Hii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle d;
            @DexIgnore
            public /* final */ /* synthetic */ He0 e;

            @DexIgnore
            public Hii(Oi oi, String str, Bundle bundle, He0 he0) {
                this.b = oi;
                this.c = str;
                this.d = bundle;
                this.e = he0;
            }

            @DexIgnore
            public void run() {
                Fi fi = Ht0.this.c.get(this.b.asBinder());
                if (fi == null) {
                    Log.w("MBServiceCompat", "search for callback that isn't registered query=" + this.c);
                    return;
                }
                Ht0.this.o(this.c, this.d, fi, this.e);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Iii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Oi b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle d;
            @DexIgnore
            public /* final */ /* synthetic */ He0 e;

            @DexIgnore
            public Iii(Oi oi, String str, Bundle bundle, He0 he0) {
                this.b = oi;
                this.c = str;
                this.d = bundle;
                this.e = he0;
            }

            @DexIgnore
            public void run() {
                Fi fi = Ht0.this.c.get(this.b.asBinder());
                if (fi == null) {
                    Log.w("MBServiceCompat", "sendCustomAction for callback that isn't registered action=" + this.c + ", extras=" + this.d);
                    return;
                }
                Ht0.this.l(this.c, this.d, fi, this.e);
            }
        }

        @DexIgnore
        public Ni() {
        }

        @DexIgnore
        public void a(String str, IBinder iBinder, Bundle bundle, Oi oi) {
            Ht0.this.e.a(new Cii(oi, str, iBinder, bundle));
        }

        @DexIgnore
        public void b(String str, int i, int i2, Bundle bundle, Oi oi) {
            if (Ht0.this.c(str, i2)) {
                Ht0.this.e.a(new Aii(oi, str, i, i2, bundle));
                return;
            }
            throw new IllegalArgumentException("Package/uid mismatch: uid=" + i2 + " package=" + str);
        }

        @DexIgnore
        public void c(Oi oi) {
            Ht0.this.e.a(new Bii(oi));
        }

        @DexIgnore
        public void d(String str, He0 he0, Oi oi) {
            if (!TextUtils.isEmpty(str) && he0 != null) {
                Ht0.this.e.a(new Eii(oi, str, he0));
            }
        }

        @DexIgnore
        public void e(Oi oi, String str, int i, int i2, Bundle bundle) {
            Ht0.this.e.a(new Fii(oi, str, i, i2, bundle));
        }

        @DexIgnore
        public void f(String str, IBinder iBinder, Oi oi) {
            Ht0.this.e.a(new Dii(oi, str, iBinder));
        }

        @DexIgnore
        public void g(String str, Bundle bundle, He0 he0, Oi oi) {
            if (!TextUtils.isEmpty(str) && he0 != null) {
                Ht0.this.e.a(new Hii(oi, str, bundle, he0));
            }
        }

        @DexIgnore
        public void h(String str, Bundle bundle, He0 he0, Oi oi) {
            if (!TextUtils.isEmpty(str) && he0 != null) {
                Ht0.this.e.a(new Iii(oi, str, bundle, he0));
            }
        }

        @DexIgnore
        public void i(Oi oi) {
            Ht0.this.e.a(new Gii(oi));
        }
    }

    @DexIgnore
    public interface Oi {
        @DexIgnore
        void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException;

        @DexIgnore
        IBinder asBinder();

        @DexIgnore
        void b() throws RemoteException;

        @DexIgnore
        void c(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Pi implements Oi {
        @DexIgnore
        public /* final */ Messenger a;

        @DexIgnore
        public Pi(Messenger messenger) {
            this.a = messenger;
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Oi
        public void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException {
            Bundle bundle3 = new Bundle();
            bundle3.putString("data_media_item_id", str);
            bundle3.putBundle("data_options", bundle);
            bundle3.putBundle("data_notify_children_changed_options", bundle2);
            if (list != null) {
                bundle3.putParcelableArrayList("data_media_item_list", list instanceof ArrayList ? (ArrayList) list : new ArrayList<>(list));
            }
            d(3, bundle3);
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Oi
        public IBinder asBinder() {
            return this.a.getBinder();
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Oi
        public void b() throws RemoteException {
            d(2, null);
        }

        @DexIgnore
        @Override // com.fossil.Ht0.Oi
        public void c(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putInt("extra_service_version", 2);
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putParcelable("data_media_session_token", token);
            bundle2.putBundle("data_root_hints", bundle);
            d(1, bundle2);
        }

        @DexIgnore
        public final void d(int i, Bundle bundle) throws RemoteException {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 2;
            obtain.setData(bundle);
            this.a.send(obtain);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Qi extends Handler {
        @DexIgnore
        public /* final */ Ni a; // = new Ni();

        @DexIgnore
        public Qi() {
        }

        @DexIgnore
        public void a(Runnable runnable) {
            if (Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }

        @DexIgnore
        public void handleMessage(Message message) {
            Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    Bundle bundle = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle);
                    this.a.b(data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle, new Pi(message.replyTo));
                    return;
                case 2:
                    this.a.c(new Pi(message.replyTo));
                    return;
                case 3:
                    Bundle bundle2 = data.getBundle("data_options");
                    MediaSessionCompat.a(bundle2);
                    this.a.a(data.getString("data_media_item_id"), Vk0.a(data, "data_callback_token"), bundle2, new Pi(message.replyTo));
                    return;
                case 4:
                    this.a.f(data.getString("data_media_item_id"), Vk0.a(data, "data_callback_token"), new Pi(message.replyTo));
                    return;
                case 5:
                    this.a.d(data.getString("data_media_item_id"), (He0) data.getParcelable("data_result_receiver"), new Pi(message.replyTo));
                    return;
                case 6:
                    Bundle bundle3 = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle3);
                    this.a.e(new Pi(message.replyTo), data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle3);
                    return;
                case 7:
                    this.a.i(new Pi(message.replyTo));
                    return;
                case 8:
                    Bundle bundle4 = data.getBundle("data_search_extras");
                    MediaSessionCompat.a(bundle4);
                    this.a.g(data.getString("data_search_query"), bundle4, (He0) data.getParcelable("data_result_receiver"), new Pi(message.replyTo));
                    return;
                case 9:
                    Bundle bundle5 = data.getBundle("data_custom_action_extras");
                    MediaSessionCompat.a(bundle5);
                    this.a.h(data.getString("data_custom_action"), bundle5, (He0) data.getParcelable("data_result_receiver"), new Pi(message.replyTo));
                    return;
                default:
                    Log.w("MBServiceCompat", "Unhandled message: " + message + "\n  Service version: 2\n  Client version: " + message.arg1);
                    return;
            }
        }

        @DexIgnore
        public boolean sendMessageAtTime(Message message, long j) {
            Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            data.putInt("data_calling_pid", Binder.getCallingPid());
            return super.sendMessageAtTime(message, j);
        }
    }

    @DexIgnore
    public void a(String str, Fi fi, IBinder iBinder, Bundle bundle) {
        List<Ln0<IBinder, Bundle>> list = fi.d.get(str);
        ArrayList arrayList = list == null ? new ArrayList() : list;
        for (Ln0<IBinder, Bundle> ln0 : arrayList) {
            if (iBinder == ln0.a && Gt0.a(bundle, ln0.b)) {
                return;
            }
        }
        arrayList.add(new Ln0<>(iBinder, bundle));
        fi.d.put(str, arrayList);
        m(str, fi, bundle, null);
        j(str, bundle);
    }

    @DexIgnore
    public List<MediaBrowserCompat.MediaItem> b(List<MediaBrowserCompat.MediaItem> list, Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i == -1 && i2 == -1) {
            return list;
        }
        int i3 = i2 * i;
        int i4 = i3 + i2;
        if (i < 0 || i2 < 1 || i3 >= list.size()) {
            return Collections.emptyList();
        }
        if (i4 > list.size()) {
            i4 = list.size();
        }
        return list.subList(i3, i4);
    }

    @DexIgnore
    public boolean c(String str, int i) {
        if (str == null) {
            return false;
        }
        for (String str2 : getPackageManager().getPackagesForUid(i)) {
            if (str2.equals(str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void d(String str, Bundle bundle, Mi<Bundle> mi) {
        mi.e(null);
    }

    @DexIgnore
    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    public abstract Ei e(String str, int i, Bundle bundle);

    @DexIgnore
    public abstract void f(String str, Mi<List<MediaBrowserCompat.MediaItem>> mi);

    @DexIgnore
    public void g(String str, Mi<List<MediaBrowserCompat.MediaItem>> mi, Bundle bundle) {
        mi.g(1);
        f(str, mi);
    }

    @DexIgnore
    public void h(String str, Mi<MediaBrowserCompat.MediaItem> mi) {
        mi.g(2);
        mi.f(null);
    }

    @DexIgnore
    public void i(String str, Bundle bundle, Mi<List<MediaBrowserCompat.MediaItem>> mi) {
        mi.g(4);
        mi.f(null);
    }

    @DexIgnore
    public void j(String str, Bundle bundle) {
    }

    @DexIgnore
    public void k(String str) {
    }

    @DexIgnore
    public void l(String str, Bundle bundle, Fi fi, He0 he0) {
        Di di = new Di(this, str, he0);
        d(str, bundle, di);
        if (!di.b()) {
            throw new IllegalStateException("onCustomAction must call detach() or sendResult() or sendError() before returning for action=" + str + " extras=" + bundle);
        }
    }

    @DexIgnore
    public void m(String str, Fi fi, Bundle bundle, Bundle bundle2) {
        Ai ai = new Ai(str, fi, str, bundle, bundle2);
        if (bundle == null) {
            f(str, ai);
        } else {
            g(str, ai, bundle);
        }
        if (!ai.b()) {
            throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + fi.a + " id=" + str);
        }
    }

    @DexIgnore
    public void n(String str, Fi fi, He0 he0) {
        Bi bi = new Bi(this, str, he0);
        h(str, bi);
        if (!bi.b()) {
            throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
        }
    }

    @DexIgnore
    public void o(String str, Bundle bundle, Fi fi, He0 he0) {
        Ci ci = new Ci(this, str, he0);
        i(str, bundle, ci);
        if (!ci.b()) {
            throw new IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + str);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.b.d(intent);
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.b = new Ki(this);
        } else if (i >= 26) {
            this.b = new Ji();
        } else if (i >= 23) {
            this.b = new Ii();
        } else if (i >= 21) {
            this.b = new Hi();
        } else {
            this.b = new Li();
        }
        this.b.a();
    }

    @DexIgnore
    public boolean p(String str, Fi fi, IBinder iBinder) {
        boolean z = false;
        if (iBinder == null) {
            try {
                return fi.d.remove(str) != null;
            } finally {
                k(str);
            }
        } else {
            List<Ln0<IBinder, Bundle>> list = fi.d.get(str);
            if (list != null) {
                Iterator<Ln0<IBinder, Bundle>> it = list.iterator();
                boolean z2 = false;
                while (it.hasNext()) {
                    if (iBinder == it.next().a) {
                        it.remove();
                        z2 = true;
                    }
                }
                if (list.size() == 0) {
                    fi.d.remove(str);
                    z = z2;
                } else {
                    z = z2;
                }
            }
            k(str);
            return z;
        }
    }
}
