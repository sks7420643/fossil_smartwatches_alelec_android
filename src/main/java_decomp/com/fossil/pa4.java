package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pa4 extends Ta4.Di.Dii.Ciii {
    @DexIgnore
    public /* final */ Double a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Dii.Ciii.Aiiii {
        @DexIgnore
        public Double a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Long f;

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii a() {
            String str = "";
            if (this.b == null) {
                str = " batteryVelocity";
            }
            if (this.c == null) {
                str = str + " proximityOn";
            }
            if (this.d == null) {
                str = str + " orientation";
            }
            if (this.e == null) {
                str = str + " ramUsed";
            }
            if (this.f == null) {
                str = str + " diskUsed";
            }
            if (str.isEmpty()) {
                return new Pa4(this.a, this.b.intValue(), this.c.booleanValue(), this.d.intValue(), this.e.longValue(), this.f.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii.Aiiii b(Double d2) {
            this.a = d2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii.Aiiii c(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii.Aiiii d(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii.Aiiii e(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii.Aiiii f(boolean z) {
            this.c = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Ciii.Aiiii
        public Ta4.Di.Dii.Ciii.Aiiii g(long j) {
            this.e = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public Pa4(Double d2, int i, boolean z, int i2, long j, long j2) {
        this.a = d2;
        this.b = i;
        this.c = z;
        this.d = i2;
        this.e = j;
        this.f = j2;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Ciii
    public Double b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Ciii
    public int c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Ciii
    public long d() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Ciii
    public int e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di.Dii.Ciii)) {
            return false;
        }
        Ta4.Di.Dii.Ciii ciii = (Ta4.Di.Dii.Ciii) obj;
        Double d2 = this.a;
        if (d2 != null ? d2.equals(ciii.b()) : ciii.b() == null) {
            if (this.b == ciii.c() && this.c == ciii.g() && this.d == ciii.e() && this.e == ciii.f() && this.f == ciii.d()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Ciii
    public long f() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Ciii
    public boolean g() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        Double d2 = this.a;
        int hashCode = d2 == null ? 0 : d2.hashCode();
        int i = this.b;
        int i2 = this.c ? 1231 : 1237;
        int i3 = this.d;
        long j = this.e;
        long j2 = this.f;
        return ((((((((((hashCode ^ 1000003) * 1000003) ^ i) * 1000003) ^ i2) * 1000003) ^ i3) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2));
    }

    @DexIgnore
    public String toString() {
        return "Device{batteryLevel=" + this.a + ", batteryVelocity=" + this.b + ", proximityOn=" + this.c + ", orientation=" + this.d + ", ramUsed=" + this.e + ", diskUsed=" + this.f + "}";
    }
}
