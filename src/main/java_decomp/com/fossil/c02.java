package com.fossil;

import com.fossil.Vz1;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class C02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public final Ai a(String str, int i) {
            e().put(str, String.valueOf(i));
            return this;
        }

        @DexIgnore
        public final Ai b(String str, long j) {
            e().put(str, String.valueOf(j));
            return this;
        }

        @DexIgnore
        public final Ai c(String str, String str2) {
            e().put(str, str2);
            return this;
        }

        @DexIgnore
        public abstract C02 d();

        @DexIgnore
        public abstract Map<String, String> e();

        @DexIgnore
        public abstract Ai f(Map<String, String> map);

        @DexIgnore
        public abstract Ai g(Integer num);

        @DexIgnore
        public abstract Ai h(B02 b02);

        @DexIgnore
        public abstract Ai i(long j);

        @DexIgnore
        public abstract Ai j(String str);

        @DexIgnore
        public abstract Ai k(long j);
    }

    @DexIgnore
    public static Ai a() {
        Vz1.Bi bi = new Vz1.Bi();
        bi.f(new HashMap());
        return bi;
    }

    @DexIgnore
    public final String b(String str) {
        String str2 = c().get(str);
        return str2 == null ? "" : str2;
    }

    @DexIgnore
    public abstract Map<String, String> c();

    @DexIgnore
    public abstract Integer d();

    @DexIgnore
    public abstract B02 e();

    @DexIgnore
    public abstract long f();

    @DexIgnore
    public final int g(String str) {
        String str2 = c().get(str);
        if (str2 == null) {
            return 0;
        }
        return Integer.valueOf(str2).intValue();
    }

    @DexIgnore
    public final long h(String str) {
        String str2 = c().get(str);
        if (str2 == null) {
            return 0;
        }
        return Long.valueOf(str2).longValue();
    }

    @DexIgnore
    public final Map<String, String> i() {
        return Collections.unmodifiableMap(c());
    }

    @DexIgnore
    public abstract String j();

    @DexIgnore
    public abstract long k();

    @DexIgnore
    public Ai l() {
        Vz1.Bi bi = new Vz1.Bi();
        bi.j(j());
        bi.g(d());
        bi.h(e());
        bi.i(f());
        bi.k(k());
        bi.f(new HashMap(c()));
        return bi;
    }
}
