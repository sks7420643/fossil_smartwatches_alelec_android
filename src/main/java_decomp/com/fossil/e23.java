package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E23<K, V> {
    @DexIgnore
    public static <K, V> int a(H23<K, V> h23, K k, V v) {
        return T03.b(h23.a, 1, k) + T03.b(h23.c, 2, v);
    }

    @DexIgnore
    public static <K, V> void b(L03 l03, H23<K, V> h23, K k, V v) throws IOException {
        T03.f(l03, h23.a, 1, k);
        T03.f(l03, h23.c, 2, v);
    }
}
