package com.fossil;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Je0 {
    @DexIgnore
    public CopyOnWriteArrayList<Ie0> mCancellables; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public boolean mEnabled;

    @DexIgnore
    public Je0(boolean z) {
        this.mEnabled = z;
    }

    @DexIgnore
    public void addCancellable(Ie0 ie0) {
        this.mCancellables.add(ie0);
    }

    @DexIgnore
    public abstract void handleOnBackPressed();

    @DexIgnore
    public final boolean isEnabled() {
        return this.mEnabled;
    }

    @DexIgnore
    public final void remove() {
        Iterator<Ie0> it = this.mCancellables.iterator();
        while (it.hasNext()) {
            it.next().cancel();
        }
    }

    @DexIgnore
    public void removeCancellable(Ie0 ie0) {
        this.mCancellables.remove(ie0);
    }

    @DexIgnore
    public final void setEnabled(boolean z) {
        this.mEnabled = z;
    }
}
