package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zv extends Mu {
    @DexIgnore
    public Zv(K5 k5) {
        super(Fu.u, Hs.S, k5, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void i(P7 p7) {
        if (p7.a != F5.b) {
            m(Mw.a(this.v, null, null, Lw.u, null, null, 27));
        } else if (p7.b == 19 || this.D) {
            m(this.v);
        } else {
            m(Mw.a(this.v, null, null, Lw.g, null, null, 27));
        }
    }
}
