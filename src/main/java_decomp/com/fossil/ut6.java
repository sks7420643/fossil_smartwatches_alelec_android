package com.fossil;

import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ut6 implements MembersInjector<PreferredUnitActivity> {
    @DexIgnore
    public static void a(PreferredUnitActivity preferredUnitActivity, PreferredUnitPresenter preferredUnitPresenter) {
        preferredUnitActivity.A = preferredUnitPresenter;
    }
}
