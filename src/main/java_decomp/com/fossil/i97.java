package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.Firmware;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I97 implements H97 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<G97> b;
    @DexIgnore
    public /* final */ Vh c;
    @DexIgnore
    public /* final */ Vh d;
    @DexIgnore
    public /* final */ Vh e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<G97> {
        @DexIgnore
        public Ai(I97 i97, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, G97 g97) {
            if (g97.d() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, g97.d());
            }
            if (g97.c() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, g97.c());
            }
            if (g97.a() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, g97.a());
            }
            if (g97.g() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, g97.g());
            }
            if (g97.b() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, g97.b());
            }
            if (g97.h() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, g97.h());
            }
            if (g97.e() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, g97.e());
            }
            mi.bindLong(8, (long) g97.f());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, G97 g97) {
            a(mi, g97);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `wf_background_photo` (`id`,`downloadUrl`,`checkSum`,`uid`,`createdAt`,`updatedAt`,`localPath`,`pinType`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(I97 i97, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM wf_background_photo WHERE id =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(I97 i97, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE wf_background_photo SET pinType = 3 WHERE id =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Vh {
        @DexIgnore
        public Di(I97 i97, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM wf_background_photo";
        }
    }

    @DexIgnore
    public I97(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
        this.c = new Bi(this, oh);
        this.d = new Ci(this, oh);
        this.e = new Di(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.H97
    public void a() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.e.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public long c(G97 g97) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(g97);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public List<G97> d() {
        Rh f = Rh.f("SELECT * FROM wf_background_photo WHERE pinType = 3", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = Dx0.c(b2, "checkSum");
            int c5 = Dx0.c(b2, "uid");
            int c6 = Dx0.c(b2, "createdAt");
            int c7 = Dx0.c(b2, "updatedAt");
            int c8 = Dx0.c(b2, "localPath");
            int c9 = Dx0.c(b2, "pinType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new G97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public List<G97> e() {
        Rh f = Rh.f("SELECT * FROM wf_background_photo WHERE pinType = 1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = Dx0.c(b2, "checkSum");
            int c5 = Dx0.c(b2, "uid");
            int c6 = Dx0.c(b2, "createdAt");
            int c7 = Dx0.c(b2, "updatedAt");
            int c8 = Dx0.c(b2, "localPath");
            int c9 = Dx0.c(b2, "pinType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new G97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public List<G97> f() {
        Rh f = Rh.f("SELECT * FROM wf_background_photo WHERE pinType <> 3", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = Dx0.c(b2, "checkSum");
            int c5 = Dx0.c(b2, "uid");
            int c6 = Dx0.c(b2, "createdAt");
            int c7 = Dx0.c(b2, "updatedAt");
            int c8 = Dx0.c(b2, "localPath");
            int c9 = Dx0.c(b2, "pinType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new G97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public G97 g(String str) {
        G97 g97 = null;
        Rh f = Rh.f("SELECT * FROM wf_background_photo WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = Dx0.c(b2, "checkSum");
            int c5 = Dx0.c(b2, "uid");
            int c6 = Dx0.c(b2, "createdAt");
            int c7 = Dx0.c(b2, "updatedAt");
            int c8 = Dx0.c(b2, "localPath");
            int c9 = Dx0.c(b2, "pinType");
            if (b2.moveToFirst()) {
                g97 = new G97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9));
            }
            return g97;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public void h(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.d.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.H97
    public Long[] insert(List<G97> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }
}
