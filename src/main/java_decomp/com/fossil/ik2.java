package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ik2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ Intent c;
    @DexIgnore
    public /* final */ /* synthetic */ Hk2 d;

    @DexIgnore
    public Ik2(Hk2 hk2, Intent intent, Intent intent2) {
        this.d = hk2;
        this.b = intent;
        this.c = intent2;
    }

    @DexIgnore
    public final void run() {
        this.d.handleIntent(this.b);
        Hk2.a(this.d, this.c);
    }
}
