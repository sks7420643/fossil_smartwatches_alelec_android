package com.fossil;

import android.content.Context;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R67 extends W67 {
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k; // = 1.0f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public R67(Context context) {
        super(context);
        Wg6.c(context, "context");
    }

    @DexIgnore
    public int getOrderIndex() {
        return this.i;
    }

    @DexIgnore
    public int getPreviousIndex() {
        return this.j;
    }

    @DexIgnore
    public void setOrderIndex(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public void setPreviousIndex(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void t(float f) {
        float f2 = this.k * f;
        setScaleX(f2);
        setScaleY(f2);
    }

    @DexIgnore
    public final void u(float f) {
        this.k = getScaleX();
    }

    @DexIgnore
    public final void v(float f) {
    }
}
