package com.fossil;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.Xq0;
import java.io.PrintWriter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iq0 extends Xq0 implements FragmentManager.h {
    @DexIgnore
    public /* final */ FragmentManager r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public int t;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Iq0(androidx.fragment.app.FragmentManager r3) {
        /*
            r2 = this;
            com.fossil.Nq0 r1 = r3.h0()
            androidx.fragment.app.FragmentHostCallback<?> r0 = r3.o
            if (r0 == 0) goto L_0x0019
            android.content.Context r0 = r0.e()
            java.lang.ClassLoader r0 = r0.getClassLoader()
        L_0x0010:
            r2.<init>(r1, r0)
            r0 = -1
            r2.t = r0
            r2.r = r3
            return
        L_0x0019:
            r0 = 0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iq0.<init>(androidx.fragment.app.FragmentManager):void");
    }

    @DexIgnore
    public static boolean G(Xq0.Ai ai) {
        Fragment fragment = ai.b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    @DexIgnore
    public void A() {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            Xq0.Ai ai = this.a.get(i);
            Fragment fragment = ai.b;
            if (fragment != null) {
                fragment.setNextTransition(this.f);
            }
            switch (ai.a) {
                case 1:
                    fragment.setNextAnim(ai.c);
                    this.r.a1(fragment, false);
                    this.r.d(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + ai.a);
                case 3:
                    fragment.setNextAnim(ai.d);
                    this.r.Q0(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(ai.d);
                    this.r.q0(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(ai.c);
                    this.r.a1(fragment, false);
                    this.r.f1(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(ai.d);
                    this.r.r(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(ai.c);
                    this.r.a1(fragment, false);
                    this.r.i(fragment);
                    break;
                case 8:
                    this.r.d1(fragment);
                    break;
                case 9:
                    this.r.d1(null);
                    break;
                case 10:
                    this.r.c1(fragment, ai.h);
                    break;
            }
            if (!(this.p || ai.a == 1 || fragment == null)) {
                this.r.B0(fragment);
            }
        }
        if (!this.p) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.C0(fragmentManager.n, true);
        }
    }

    @DexIgnore
    public void B(boolean z) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            Xq0.Ai ai = this.a.get(size);
            Fragment fragment = ai.b;
            if (fragment != null) {
                fragment.setNextTransition(FragmentManager.W0(this.f));
            }
            switch (ai.a) {
                case 1:
                    fragment.setNextAnim(ai.f);
                    this.r.a1(fragment, true);
                    this.r.Q0(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + ai.a);
                case 3:
                    fragment.setNextAnim(ai.e);
                    this.r.d(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(ai.e);
                    this.r.f1(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(ai.f);
                    this.r.a1(fragment, true);
                    this.r.q0(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(ai.e);
                    this.r.i(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(ai.f);
                    this.r.a1(fragment, true);
                    this.r.r(fragment);
                    break;
                case 8:
                    this.r.d1(null);
                    break;
                case 9:
                    this.r.d1(fragment);
                    break;
                case 10:
                    this.r.c1(fragment, ai.g);
                    break;
            }
            if (!(this.p || ai.a == 3 || fragment == null)) {
                this.r.B0(fragment);
            }
        }
        if (!this.p && z) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.C0(fragmentManager.n, true);
        }
    }

    @DexIgnore
    public Fragment C(ArrayList<Fragment> arrayList, Fragment fragment) {
        boolean z;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                return fragment;
            }
            Xq0.Ai ai = this.a.get(i2);
            int i3 = ai.a;
            if (i3 != 1) {
                if (i3 == 2) {
                    Fragment fragment2 = ai.b;
                    int i4 = fragment2.mContainerId;
                    int size = arrayList.size() - 1;
                    boolean z2 = false;
                    int i5 = i2;
                    Fragment fragment3 = fragment;
                    while (size >= 0) {
                        Fragment fragment4 = arrayList.get(size);
                        if (fragment4.mContainerId != i4) {
                            z = z2;
                        } else if (fragment4 == fragment2) {
                            z = true;
                        } else {
                            if (fragment4 == fragment3) {
                                this.a.add(i5, new Xq0.Ai(9, fragment4));
                                i5++;
                                fragment3 = null;
                            }
                            Xq0.Ai ai2 = new Xq0.Ai(3, fragment4);
                            ai2.c = ai.c;
                            ai2.e = ai.e;
                            ai2.d = ai.d;
                            ai2.f = ai.f;
                            this.a.add(i5, ai2);
                            arrayList.remove(fragment4);
                            i5++;
                            z = z2;
                        }
                        size--;
                        z2 = z;
                    }
                    if (z2) {
                        this.a.remove(i5);
                        i2 = i5 - 1;
                        fragment = fragment3;
                    } else {
                        ai.a = 1;
                        arrayList.add(fragment2);
                        i2 = i5;
                        fragment = fragment3;
                    }
                } else if (i3 == 3 || i3 == 6) {
                    arrayList.remove(ai.b);
                    Fragment fragment5 = ai.b;
                    if (fragment5 == fragment) {
                        this.a.add(i2, new Xq0.Ai(9, fragment5));
                        i2++;
                        fragment = null;
                    }
                } else if (i3 != 7) {
                    if (i3 == 8) {
                        this.a.add(i2, new Xq0.Ai(9, fragment));
                        i2++;
                        fragment = ai.b;
                    }
                }
                i = i2 + 1;
            }
            arrayList.add(ai.b);
            i = i2 + 1;
        }
    }

    @DexIgnore
    public String D() {
        return this.i;
    }

    @DexIgnore
    public boolean E(int i) {
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = this.a.get(i2).b;
            int i3 = fragment != null ? fragment.mContainerId : 0;
            if (i3 != 0 && i3 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean F(ArrayList<Iq0> arrayList, int i, int i2) {
        int i3;
        if (i2 == i) {
            return false;
        }
        int size = this.a.size();
        int i4 = -1;
        int i5 = 0;
        while (i5 < size) {
            Fragment fragment = this.a.get(i5).b;
            int i6 = fragment != null ? fragment.mContainerId : 0;
            if (i6 == 0 || i6 == i4) {
                i3 = i4;
            } else {
                for (int i7 = i; i7 < i2; i7++) {
                    Iq0 iq0 = arrayList.get(i7);
                    int size2 = iq0.a.size();
                    for (int i8 = 0; i8 < size2; i8++) {
                        Fragment fragment2 = iq0.a.get(i8).b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i6) {
                            return true;
                        }
                    }
                }
                i3 = i6;
            }
            i5++;
            i4 = i3;
        }
        return false;
    }

    @DexIgnore
    public boolean H() {
        for (int i = 0; i < this.a.size(); i++) {
            if (G(this.a.get(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void I() {
        if (this.q != null) {
            for (int i = 0; i < this.q.size(); i++) {
                this.q.get(i).run();
            }
            this.q = null;
        }
    }

    @DexIgnore
    public void J(Fragment.OnStartEnterTransitionListener onStartEnterTransitionListener) {
        for (int i = 0; i < this.a.size(); i++) {
            Xq0.Ai ai = this.a.get(i);
            if (G(ai)) {
                ai.b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }

    @DexIgnore
    public Fragment K(ArrayList<Fragment> arrayList, Fragment fragment) {
        Fragment fragment2;
        int size = this.a.size() - 1;
        Fragment fragment3 = fragment;
        while (size >= 0) {
            Xq0.Ai ai = this.a.get(size);
            int i = ai.a;
            if (i != 1) {
                if (i != 3) {
                    switch (i) {
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            fragment2 = null;
                            break;
                        case 9:
                            fragment2 = ai.b;
                            break;
                        case 10:
                            ai.h = ai.g;
                            fragment2 = fragment3;
                            break;
                        default:
                            fragment2 = fragment3;
                            break;
                    }
                    size--;
                    fragment3 = fragment2;
                }
                arrayList.add(ai.b);
                fragment2 = fragment3;
                size--;
                fragment3 = fragment2;
            }
            arrayList.remove(ai.b);
            fragment2 = fragment3;
            size--;
            fragment3 = fragment2;
        }
        return fragment3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentManager.h
    public boolean a(ArrayList<Iq0> arrayList, ArrayList<Boolean> arrayList2) {
        if (FragmentManager.s0(2)) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(Boolean.FALSE);
        if (!this.g) {
            return true;
        }
        this.r.b(this);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public int h() {
        return x(false);
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public int i() {
        return x(true);
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public void j() {
        m();
        this.r.S(this, false);
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public void k() {
        m();
        this.r.S(this, true);
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public Xq0 l(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.l(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot detach Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public void n(int i, Fragment fragment, String str, int i2) {
        super.n(i, fragment, str, i2);
        fragment.mFragmentManager = this.r;
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public Xq0 o(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.o(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot hide Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public boolean p() {
        return this.a.isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public Xq0 q(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.q(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot remove Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public Xq0 t(Fragment fragment, Lifecycle.State state) {
        if (fragment.mFragmentManager != this.r) {
            throw new IllegalArgumentException("Cannot setMaxLifecycle for Fragment not attached to FragmentManager " + this.r);
        } else if (state.isAtLeast(Lifecycle.State.CREATED)) {
            super.t(fragment, state);
            return this;
        } else {
            throw new IllegalArgumentException("Cannot set maximum Lifecycle below " + Lifecycle.State.CREATED);
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.t >= 0) {
            sb.append(" #");
            sb.append(this.t);
        }
        if (this.i != null) {
            sb.append(" ");
            sb.append(this.i);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.Xq0
    public Xq0 v(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.v(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot show Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    public void w(int i) {
        if (this.g) {
            if (FragmentManager.s0(2)) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                Xq0.Ai ai = this.a.get(i2);
                Fragment fragment = ai.b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i;
                    if (FragmentManager.s0(2)) {
                        Log.v("FragmentManager", "Bump nesting of " + ai.b + " to " + ai.b.mBackStackNesting);
                    }
                }
            }
        }
    }

    @DexIgnore
    public int x(boolean z) {
        if (!this.s) {
            if (FragmentManager.s0(2)) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new Jn0("FragmentManager"));
                y("  ", printWriter);
                printWriter.close();
            }
            this.s = true;
            if (this.g) {
                this.t = this.r.g();
            } else {
                this.t = -1;
            }
            this.r.P(this, z);
            return this.t;
        }
        throw new IllegalStateException("commit already called");
    }

    @DexIgnore
    public void y(String str, PrintWriter printWriter) {
        z(str, printWriter, true);
    }

    @DexIgnore
    public void z(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.i);
            printWriter.print(" mIndex=");
            printWriter.print(this.t);
            printWriter.print(" mCommitted=");
            printWriter.println(this.s);
            if (this.f != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f));
            }
            if (!(this.b == 0 && this.c == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.b));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.c));
            }
            if (!(this.d == 0 && this.e == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.d));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.e));
            }
            if (!(this.j == 0 && this.k == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.j));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.k);
            }
            if (!(this.l == 0 && this.m == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.l));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.m);
            }
        }
        if (!this.a.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                Xq0.Ai ai = this.a.get(i);
                switch (ai.a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        str2 = "cmd=" + ai.a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(ai.b);
                if (z) {
                    if (!(ai.c == 0 && ai.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(ai.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(ai.d));
                    }
                    if (ai.e != 0 || ai.f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(ai.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(ai.f));
                    }
                }
            }
        }
    }
}
