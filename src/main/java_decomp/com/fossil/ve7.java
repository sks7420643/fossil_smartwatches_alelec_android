package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ve7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Bundle d;
    }

    @DexIgnore
    public static boolean a(Context context, Ai ai) {
        String str;
        if (context == null) {
            str = "send fail, invalid argument";
        } else if (Af7.a(ai.b)) {
            str = "send fail, action is null";
        } else {
            String str2 = null;
            if (!Af7.a(ai.a)) {
                str2 = ai.a + ".permission.MM_MESSAGE";
            }
            Intent intent = new Intent(ai.b);
            Bundle bundle = ai.d;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 587268097);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", ai.c);
            intent.putExtra("_mmessage_checksum", We7.a(ai.c, 587268097, packageName));
            context.sendBroadcast(intent, str2);
            Ye7.e("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str2);
            return true;
        }
        Ye7.b("MicroMsg.SDK.MMessage", str);
        return false;
    }
}
