package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Wg6;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ol7 implements Collection<Nl7>, Jr7 {
    @DexIgnore
    public /* final */ long[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends In7 {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ long[] c;

        @DexIgnore
        public Ai(long[] jArr) {
            Wg6.c(jArr, "array");
            this.c = jArr;
        }

        @DexIgnore
        @Override // com.fossil.In7
        public long b() {
            int i = this.b;
            long[] jArr = this.c;
            if (i < jArr.length) {
                this.b = i + 1;
                long j = jArr[i];
                Nl7.e(j);
                return j;
            }
            throw new NoSuchElementException(String.valueOf(this.b));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.length;
        }
    }

    @DexIgnore
    public static boolean b(long[] jArr, long j) {
        return Em7.A(jArr, j);
    }

    @DexIgnore
    public static boolean c(long[] jArr, Collection<Nl7> collection) {
        boolean z;
        Wg6.c(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof Nl7) || !Em7.A(jArr, t.j())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean e(long[] jArr, Object obj) {
        return (obj instanceof Ol7) && Wg6.a(jArr, ((Ol7) obj).m());
    }

    @DexIgnore
    public static int g(long[] jArr) {
        return jArr.length;
    }

    @DexIgnore
    public static int h(long[] jArr) {
        if (jArr != null) {
            return Arrays.hashCode(jArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean i(long[] jArr) {
        return jArr.length == 0;
    }

    @DexIgnore
    public static In7 k(long[] jArr) {
        return new Ai(jArr);
    }

    @DexIgnore
    public static String l(long[] jArr) {
        return "ULongArray(storage=" + Arrays.toString(jArr) + ")";
    }

    @DexIgnore
    public boolean a(long j) {
        return b(this.b, j);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(Nl7 nl7) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends Nl7> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof Nl7) {
            return a(((Nl7) obj).j());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return c(this.b, collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return e(this.b, obj);
    }

    @DexIgnore
    public int f() {
        return g(this.b);
    }

    @DexIgnore
    public int hashCode() {
        return h(this.b);
    }

    @DexIgnore
    public boolean isEmpty() {
        return i(this.b);
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator<Nl7> iterator() {
        return j();
    }

    @DexIgnore
    public In7 j() {
        return k(this.b);
    }

    @DexIgnore
    public final /* synthetic */ long[] m() {
        return this.b;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return f();
    }

    @DexIgnore
    public Object[] toArray() {
        return Jq7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) Jq7.b(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return l(this.b);
    }
}
