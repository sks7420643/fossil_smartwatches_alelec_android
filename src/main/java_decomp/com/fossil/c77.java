package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C77 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ D77 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Bg5 a;
        @DexIgnore
        public /* final */ D77 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Bg5 bg5, D77 d77) {
            super(bg5.n());
            Wg6.c(bg5, "binding");
            Wg6.c(d77, "photoListener");
            this.a = bg5;
            this.b = d77;
        }

        @DexIgnore
        public final void b() {
            this.a.q.setOnClickListener(new Aii(this));
        }
    }

    @DexIgnore
    public C77(int i, D77 d77) {
        Wg6.c(d77, "photoListener");
        this.a = i;
        this.b = d77;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Ai ai = (Ai) (!(viewHolder instanceof Ai) ? null : viewHolder);
        if (ai != null) {
            ai.b();
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Bg5 z = Bg5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemThemeBackgroundAddBi\u2026(inflater, parent, false)");
        return new Ai(z, this.b);
    }
}
