package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uu1 extends Vu1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ short d; // = Hy1.c(Fq7.a);
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Uu1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uu1 createFromParcel(Parcel parcel) {
            return new Uu1(Hy1.p(parcel.readByte()));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uu1[] newArray(int i) {
            return new Uu1[i];
        }
    }

    @DexIgnore
    public Uu1(short s) throws IllegalArgumentException {
        this.c = (short) s;
        if (!(s >= 0 && d >= s)) {
            StringBuilder e = E.e("goalId(");
            e.append((int) this.c);
            e.append(") is out of range ");
            e.append("[0, ");
            throw new IllegalArgumentException(E.b(e, d, "]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.Vu1
    public int b() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.Vu1
    public byte[] c() {
        ByteBuffer allocate = ByteBuffer.allocate(3);
        Wg6.b(allocate, "ByteBuffer.allocate(GOAL\u2026ACKING_TYPE_FRAME_LENGTH)");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put(Ba0.c.b);
        allocate.put((byte) 1);
        allocate.put((byte) this.c);
        byte[] array = allocate.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final short getGoalId() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(new JSONObject(), Jd0.N3, Short.valueOf(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) this.c);
    }
}
