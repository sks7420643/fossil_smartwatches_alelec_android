package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wi1 {
    @DexIgnore
    public /* final */ List<String> a; // = new ArrayList();
    @DexIgnore
    public /* final */ Map<String, List<Ai<?, ?>>> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<T, R> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ Qb1<T, R> c;

        @DexIgnore
        public Ai(Class<T> cls, Class<R> cls2, Qb1<T, R> qb1) {
            this.a = cls;
            this.b = cls2;
            this.c = qb1;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public <T, R> void a(String str, Qb1<T, R> qb1, Class<T> cls, Class<R> cls2) {
        synchronized (this) {
            c(str).add(new Ai<>(cls, cls2, qb1));
        }
    }

    @DexIgnore
    public <T, R> List<Qb1<T, R>> b(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (String str : this.a) {
                List<Ai<?, ?>> list = this.b.get(str);
                if (list != null) {
                    for (Ai<?, ?> ai : list) {
                        if (ai.a(cls, cls2)) {
                            arrayList.add(ai.c);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<Ai<?, ?>> c(String str) {
        List<Ai<?, ?>> list;
        synchronized (this) {
            if (!this.a.contains(str)) {
                this.a.add(str);
            }
            list = this.b.get(str);
            if (list == null) {
                list = new ArrayList<>();
                this.b.put(str, list);
            }
        }
        return list;
    }

    @DexIgnore
    public <T, R> List<Class<R>> d(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (String str : this.a) {
                List<Ai<?, ?>> list = this.b.get(str);
                if (list != null) {
                    for (Ai<?, ?> ai : list) {
                        if (ai.a(cls, cls2) && !arrayList.contains(ai.b)) {
                            arrayList.add(ai.b);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void e(List<String> list) {
        synchronized (this) {
            ArrayList<String> arrayList = new ArrayList(this.a);
            this.a.clear();
            for (String str : list) {
                this.a.add(str);
            }
            for (String str2 : arrayList) {
                if (!list.contains(str2)) {
                    this.a.add(str2);
                }
            }
        }
    }
}
