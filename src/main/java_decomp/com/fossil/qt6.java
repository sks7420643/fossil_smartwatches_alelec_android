package com.fossil;

import com.mapped.UserCustomizeThemeFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qt6 implements MembersInjector<UserCustomizeThemeFragment> {
    @DexIgnore
    public static void a(UserCustomizeThemeFragment userCustomizeThemeFragment, Po4 po4) {
        userCustomizeThemeFragment.g = po4;
    }
}
