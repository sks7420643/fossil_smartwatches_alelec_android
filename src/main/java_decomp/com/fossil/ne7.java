package com.fossil;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ne7 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = "0";
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public static Ne7 b(String str) {
        Ne7 ne7 = new Ne7();
        if (Se7.f(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    ne7.a = jSONObject.getString("ui");
                }
                if (!jSONObject.isNull("mc")) {
                    ne7.b = jSONObject.getString("mc");
                }
                if (!jSONObject.isNull("mid")) {
                    ne7.c = jSONObject.getString("mid");
                }
                if (!jSONObject.isNull("ts")) {
                    ne7.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return ne7;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            Se7.c(jSONObject, "ui", this.a);
            Se7.c(jSONObject, "mc", this.b);
            Se7.c(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final String toString() {
        return c().toString();
    }
}
