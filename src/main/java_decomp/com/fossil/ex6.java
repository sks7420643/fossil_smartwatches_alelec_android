package com.fossil;

import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ex6 implements MembersInjector<ProfileSetupPresenter> {
    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, AnalyticsHelper analyticsHelper) {
        profileSetupPresenter.h = analyticsHelper;
    }

    @DexIgnore
    public static void b(ProfileSetupPresenter profileSetupPresenter, GetUser getUser) {
        profileSetupPresenter.g = getUser;
    }

    @DexIgnore
    public static void c(ProfileSetupPresenter profileSetupPresenter, SignUpEmailUseCase signUpEmailUseCase) {
        profileSetupPresenter.e = signUpEmailUseCase;
    }

    @DexIgnore
    public static void d(ProfileSetupPresenter profileSetupPresenter, SignUpSocialUseCase signUpSocialUseCase) {
        profileSetupPresenter.f = signUpSocialUseCase;
    }

    @DexIgnore
    public static void e(ProfileSetupPresenter profileSetupPresenter) {
        profileSetupPresenter.k0();
    }
}
