package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C81 extends F81 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public C81(int i, int i2) {
        super(null);
        this.a = i;
        this.b = i2;
        if (!(i > 0 && i2 > 0)) {
            throw new IllegalArgumentException("Width and height must be > 0.".toString());
        }
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C81) {
                C81 c81 = (C81) obj;
                if (!(this.a == c81.a && this.b == c81.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "PixelSize(width=" + this.a + ", height=" + this.b + ")";
    }
}
