package com.fossil;

import com.fossil.Qg2;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xg2 implements Ug2<T> {
    @DexIgnore
    public /* final */ /* synthetic */ Qg2 a;

    @DexIgnore
    public Xg2(Qg2 qg2) {
        this.a = qg2;
    }

    @DexIgnore
    @Override // com.fossil.Ug2
    public final void a(T t) {
        this.a.a = t;
        Iterator it = this.a.c.iterator();
        while (it.hasNext()) {
            ((Qg2.Ai) it.next()).a(this.a.a);
        }
        this.a.c.clear();
        this.a.b = null;
    }
}
