package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vf7 {
    @DexIgnore
    public static Sf7 a(Context context, String str, boolean z) {
        Ye7.e("MicroMsg.PaySdk.WXFactory", "createWXAPI, appId = " + str + ", checkSignature = " + z);
        return new Ag7(context, str, z);
    }
}
