package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oh0 {
    @DexIgnore
    public static /* final */ ThreadLocal<TypedValue> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ int[] b; // = {-16842910};
    @DexIgnore
    public static /* final */ int[] c; // = {16842908};
    @DexIgnore
    public static /* final */ int[] d; // = {16842919};
    @DexIgnore
    public static /* final */ int[] e; // = {16842912};
    @DexIgnore
    public static /* final */ int[] f; // = new int[0];
    @DexIgnore
    public static /* final */ int[] g; // = new int[1];

    @DexIgnore
    public static void a(View view, Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Ue0.AppCompatTheme);
        try {
            if (!obtainStyledAttributes.hasValue(Ue0.AppCompatTheme_windowActionBar)) {
                Log.e("ThemeUtils", "View " + view.getClass() + " is an AppCompat widget that can only be used with a Theme.AppCompat theme (or descendant).");
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public static int b(Context context, int i) {
        ColorStateList e2 = e(context, i);
        if (e2 != null && e2.isStateful()) {
            return e2.getColorForState(b, e2.getDefaultColor());
        }
        TypedValue f2 = f();
        context.getTheme().resolveAttribute(16842803, f2, true);
        return d(context, i, f2.getFloat());
    }

    @DexIgnore
    public static int c(Context context, int i) {
        int[] iArr = g;
        iArr[0] = i;
        Th0 u = Th0.u(context, null, iArr);
        try {
            return u.b(0, 0);
        } finally {
            u.w();
        }
    }

    @DexIgnore
    public static int d(Context context, int i, float f2) {
        int c2 = c(context, i);
        return Pl0.h(c2, Math.round(((float) Color.alpha(c2)) * f2));
    }

    @DexIgnore
    public static ColorStateList e(Context context, int i) {
        int[] iArr = g;
        iArr[0] = i;
        Th0 u = Th0.u(context, null, iArr);
        try {
            return u.c(0);
        } finally {
            u.w();
        }
    }

    @DexIgnore
    public static TypedValue f() {
        TypedValue typedValue = a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        a.set(typedValue2);
        return typedValue2;
    }
}
