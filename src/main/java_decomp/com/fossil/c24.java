package com.fossil;

import java.io.Serializable;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C24<T> extends I44<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Comparator<T> comparator;

    @DexIgnore
    public C24(Comparator<T> comparator2) {
        I14.l(comparator2);
        this.comparator = comparator2;
    }

    @DexIgnore
    @Override // com.fossil.I44, java.util.Comparator
    public int compare(T t, T t2) {
        return this.comparator.compare(t, t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C24) {
            return this.comparator.equals(((C24) obj).comparator);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.comparator.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.comparator.toString();
    }
}
