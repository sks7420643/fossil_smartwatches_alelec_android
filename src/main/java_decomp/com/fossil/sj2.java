package com.fossil;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import java.util.MissingFormatArgumentException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sj2 {
    @DexIgnore
    public static Sj2 d;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public /* final */ AtomicInteger c; // = new AtomicInteger((int) SystemClock.elapsedRealtime());

    @DexIgnore
    public Sj2(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static Sj2 a(Context context) {
        Sj2 sj2;
        synchronized (Sj2.class) {
            try {
                if (d == null) {
                    d = new Sj2(context);
                }
                sj2 = d;
            } catch (Throwable th) {
                throw th;
            }
        }
        return sj2;
    }

    @DexIgnore
    public static String b(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    @DexIgnore
    public final String c(Bundle bundle, String str) {
        String b2 = b(bundle, str);
        if (!TextUtils.isEmpty(b2)) {
            return b2;
        }
        String valueOf = String.valueOf(str);
        String b3 = b(bundle, "_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf));
        if (TextUtils.isEmpty(b3)) {
            return null;
        }
        Resources resources = this.a.getResources();
        int identifier = resources.getIdentifier(b3, LegacyTokenHelper.TYPE_STRING, this.a.getPackageName());
        if (identifier == 0) {
            String valueOf2 = String.valueOf(str);
            String substring = ("_loc_key".length() != 0 ? valueOf2.concat("_loc_key") : new String(valueOf2)).substring(6);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 49 + String.valueOf(b3).length());
            sb.append(substring);
            sb.append(" resource not found: ");
            sb.append(b3);
            sb.append(" Default value will be used.");
            Log.w("GcmNotification", sb.toString());
            return null;
        }
        String valueOf3 = String.valueOf(str);
        String b4 = b(bundle, "_loc_args".length() != 0 ? valueOf3.concat("_loc_args") : new String(valueOf3));
        if (TextUtils.isEmpty(b4)) {
            return resources.getString(identifier);
        }
        try {
            JSONArray jSONArray = new JSONArray(b4);
            int length = jSONArray.length();
            Object[] objArr = new String[length];
            for (int i = 0; i < length; i++) {
                objArr[i] = jSONArray.opt(i);
            }
            return resources.getString(identifier, objArr);
        } catch (JSONException e) {
            String valueOf4 = String.valueOf(str);
            String substring2 = ("_loc_args".length() != 0 ? valueOf4.concat("_loc_args") : new String(valueOf4)).substring(6);
            StringBuilder sb2 = new StringBuilder(String.valueOf(substring2).length() + 41 + String.valueOf(b4).length());
            sb2.append("Malformed ");
            sb2.append(substring2);
            sb2.append(": ");
            sb2.append(b4);
            sb2.append("  Default value will be used.");
            Log.w("GcmNotification", sb2.toString());
            return null;
        } catch (MissingFormatArgumentException e2) {
            StringBuilder sb3 = new StringBuilder(String.valueOf(b3).length() + 58 + String.valueOf(b4).length());
            sb3.append("Missing format argument for ");
            sb3.append(b3);
            sb3.append(": ");
            sb3.append(b4);
            sb3.append(" Default value will be used.");
            Log.w("GcmNotification", sb3.toString(), e2);
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x022d  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0262  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d(android.os.Bundle r14) {
        /*
        // Method dump skipped, instructions count: 698
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sj2.d(android.os.Bundle):boolean");
    }

    @DexIgnore
    public final Bundle e() {
        ApplicationInfo applicationInfo;
        Bundle bundle;
        try {
            applicationInfo = this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        return (applicationInfo == null || (bundle = applicationInfo.metaData) == null) ? Bundle.EMPTY : bundle;
    }
}
