package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sy1<T> extends Uy1<T> {
    @DexIgnore
    public /* final */ Integer a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Vy1 c;

    @DexIgnore
    public Sy1(Integer num, T t, Vy1 vy1) {
        this.a = num;
        if (t != null) {
            this.b = t;
            if (vy1 != null) {
                this.c = vy1;
                return;
            }
            throw new NullPointerException("Null priority");
        }
        throw new NullPointerException("Null payload");
    }

    @DexIgnore
    @Override // com.fossil.Uy1
    public Integer a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Uy1
    public T b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Uy1
    public Vy1 c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Uy1)) {
            return false;
        }
        Uy1 uy1 = (Uy1) obj;
        Integer num = this.a;
        if (num != null ? num.equals(uy1.a()) : uy1.a() == null) {
            if (this.b.equals(uy1.b()) && this.c.equals(uy1.c())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.a;
        return (((((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Event{code=" + this.a + ", payload=" + ((Object) this.b) + ", priority=" + this.c + "}";
    }
}
