package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y68 implements Comparable<Y68> {
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public Di d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Ci {
        @DexIgnore
        public static /* final */ BigInteger c; // = new BigInteger("0");
        @DexIgnore
        public static /* final */ Bi d; // = new Bi();
        @DexIgnore
        public /* final */ BigInteger b;

        @DexIgnore
        public Bi() {
            this.b = c;
        }

        @DexIgnore
        public Bi(String str) {
            this.b = new BigInteger(str);
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public int compareTo(Ci ci) {
            if (ci == null) {
                return !c.equals(this.b);
            }
            int type = ci.getType();
            if (type == 0) {
                return this.b.compareTo(((Bi) ci).b);
            }
            if (type == 1 || type == 2) {
                return 1;
            }
            throw new RuntimeException("invalid item: " + ci.getClass());
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public int getType() {
            return 0;
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public boolean isNull() {
            return c.equals(this.b);
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        int compareTo(Ci ci);

        @DexIgnore
        int getType();

        @DexIgnore
        boolean isNull();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di extends ArrayList<Ci> implements Ci {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public int compareTo(Ci ci) {
            int compareTo;
            if (ci != null) {
                int type = ci.getType();
                if (type == 0) {
                    return -1;
                }
                if (type == 1) {
                    return 1;
                }
                if (type == 2) {
                    Iterator it = iterator();
                    Iterator it2 = ((Di) ci).iterator();
                    do {
                        if (!it.hasNext() && !it2.hasNext()) {
                            return 0;
                        }
                        Ci ci2 = it.hasNext() ? (Ci) it.next() : null;
                        Ci ci3 = it2.hasNext() ? (Ci) it2.next() : null;
                        if (ci2 != null) {
                            compareTo = ci2.compareTo(ci3);
                            continue;
                        } else if (ci3 == null) {
                            compareTo = 0;
                            continue;
                        } else {
                            compareTo = ci3.compareTo(ci2) * -1;
                            continue;
                        }
                    } while (compareTo == 0);
                    return compareTo;
                }
                throw new RuntimeException("invalid item: " + ci.getClass());
            } else if (size() == 0) {
                return 0;
            } else {
                return ((Ci) get(0)).compareTo(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public int getType() {
            return 2;
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public boolean isNull() {
            return size() == 0;
        }

        @DexIgnore
        public void normalize() {
            for (int size = size() - 1; size >= 0; size--) {
                Ci ci = (Ci) get(size);
                if (ci.isNull()) {
                    remove(size);
                } else if (!(ci instanceof Di)) {
                    return;
                }
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator it = iterator();
            while (it.hasNext()) {
                Ci ci = (Ci) it.next();
                if (sb.length() > 0) {
                    sb.append(ci instanceof Di ? '-' : '.');
                }
                sb.append(ci);
            }
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei implements Ci {
        @DexIgnore
        public static /* final */ String[] c;
        @DexIgnore
        public static /* final */ List<String> d;
        @DexIgnore
        public static /* final */ Properties e;
        @DexIgnore
        public static /* final */ String f; // = String.valueOf(d.indexOf(""));
        @DexIgnore
        public String b;

        /*
        static {
            String[] strArr = {"alpha", "beta", "milestone", "rc", "snapshot", "", "sp"};
            c = strArr;
            d = Arrays.asList(strArr);
            Properties properties = new Properties();
            e = properties;
            properties.put("ga", "");
            e.put("final", "");
            e.put("cr", "rc");
        }
        */

        @DexIgnore
        public Ei(String str, boolean z) {
            if (z && str.length() == 1) {
                char charAt = str.charAt(0);
                if (charAt == 'a') {
                    str = "alpha";
                } else if (charAt == 'b') {
                    str = "beta";
                } else if (charAt == 'm') {
                    str = "milestone";
                }
            }
            this.b = e.getProperty(str, str);
        }

        @DexIgnore
        public static String a(String str) {
            int indexOf = d.indexOf(str);
            if (indexOf != -1) {
                return String.valueOf(indexOf);
            }
            return d.size() + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + str;
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public int compareTo(Ci ci) {
            if (ci == null) {
                return a(this.b).compareTo(f);
            }
            int type = ci.getType();
            if (type == 0) {
                return -1;
            }
            if (type == 1) {
                return a(this.b).compareTo(a(((Ei) ci).b));
            }
            if (type == 2) {
                return -1;
            }
            throw new RuntimeException("invalid item: " + ci.getClass());
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public int getType() {
            return 1;
        }

        @DexIgnore
        @Override // com.fossil.Y68.Ci
        public boolean isNull() {
            return a(this.b).compareTo(f) == 0;
        }

        @DexIgnore
        public String toString() {
            return this.b;
        }
    }

    @DexIgnore
    public Y68(String str) {
        c(str);
    }

    @DexIgnore
    public static Ci b(boolean z, String str) {
        return z ? new Bi(str) : new Ei(str, false);
    }

    @DexIgnore
    public int a(Y68 y68) {
        return this.d.compareTo(y68.d);
    }

    @DexIgnore
    public final void c(String str) {
        this.b = str;
        this.d = new Di();
        String lowerCase = str.toLowerCase(Locale.ENGLISH);
        Di di = this.d;
        Stack stack = new Stack();
        stack.push(di);
        boolean z = false;
        int i = 0;
        for (int i2 = 0; i2 < lowerCase.length(); i2++) {
            char charAt = lowerCase.charAt(i2);
            if (charAt == '.') {
                if (i2 == i) {
                    di.add(Bi.d);
                } else {
                    di.add(b(z, lowerCase.substring(i, i2)));
                }
                i = i2 + 1;
            } else if (charAt == '-') {
                if (i2 == i) {
                    di.add(Bi.d);
                } else {
                    di.add(b(z, lowerCase.substring(i, i2)));
                }
                i = i2 + 1;
                Di di2 = new Di();
                di.add(di2);
                stack.push(di2);
                di = di2;
            } else if (Character.isDigit(charAt)) {
                if (!z && i2 > i) {
                    di.add(new Ei(lowerCase.substring(i, i2), true));
                    Di di3 = new Di();
                    di.add(di3);
                    stack.push(di3);
                    i = i2;
                    di = di3;
                }
                z = true;
            } else {
                if (z && i2 > i) {
                    di.add(b(true, lowerCase.substring(i, i2)));
                    Di di4 = new Di();
                    di.add(di4);
                    stack.push(di4);
                    i = i2;
                    di = di4;
                }
                z = false;
            }
        }
        if (lowerCase.length() > i) {
            di.add(b(z, lowerCase.substring(i)));
        }
        while (!stack.isEmpty()) {
            ((Di) stack.pop()).normalize();
        }
        this.c = this.d.toString();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Y68 y68) {
        return a(y68);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Y68) && this.c.equals(((Y68) obj).c);
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.b;
    }
}
