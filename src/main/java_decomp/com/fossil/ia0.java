package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ia0 extends Va0 {
    @DexIgnore
    public static /* final */ Ha0 CREATOR; // = new Ha0(null);
    @DexIgnore
    public R90[] c;
    @DexIgnore
    public /* final */ V90 d;

    @DexIgnore
    public /* synthetic */ Ia0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = new R90[0];
        this.d = V90.c;
        Object[] readArray = parcel.readArray(R90.class.getClassLoader());
        if (readArray != null) {
            this.c = (R90[]) readArray;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.microapp.animation.HandAnimation>");
    }

    @DexIgnore
    public Ia0(R90[] r90Arr) {
        super(Aa0.k);
        this.c = new R90[0];
        this.d = V90.c;
        this.c = r90Arr;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate((this.c.length * 3) + 2).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.d.b);
        byte b = (byte) 0;
        order.put(b);
        R90[] r90Arr = this.c;
        for (R90 r90 : r90Arr) {
            b = (byte) (b | r90.b.b);
            ByteBuffer order2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
            Wg6.b(order2, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
            order2.put((byte) (((byte) (((byte) (((byte) (r90.c.b << 6)) | ((byte) (r90.d.b << 5)))) | 0)) | r90.e.b));
            order2.putShort(r90.f);
            byte[] array = order2.array();
            Wg6.b(array, "dataBuffer.array()");
            order.put(array);
        }
        order.put(1, b);
        byte[] array2 = order.array();
        Wg6.b(array2, "byteBuffer.array()");
        return array2;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ia0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.c, ((Ia0) obj).c);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.AnimationInstr");
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Va0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (R90 r90 : this.c) {
            jSONArray.put(r90.toJSONObject());
        }
        return G80.k(super.toJSONObject(), Jd0.S3, jSONArray);
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
