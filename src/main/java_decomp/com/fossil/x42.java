package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X42 extends Q42 {
    @DexIgnore
    public /* final */ /* synthetic */ W42 b;

    @DexIgnore
    public X42(W42 w42) {
        this.b = w42;
    }

    @DexIgnore
    @Override // com.fossil.Q42, com.fossil.E52
    public final void A(Status status) throws RemoteException {
        this.b.j(status);
    }
}
