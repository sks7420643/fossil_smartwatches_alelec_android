package com.fossil;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.d26;
import com.fossil.g26;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o16 extends i16 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public /* final */ List<j06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<j06> f; // = new ArrayList();
    @DexIgnore
    public /* final */ j16 g;
    @DexIgnore
    public /* final */ uq4 h;
    @DexIgnore
    public /* final */ d26 i;
    @DexIgnore
    public /* final */ g26 j;
    @DexIgnore
    public /* final */ LoaderManager k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return o16.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.d<g26.b, tq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o16 f2612a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(o16 o16) {
            this.f2612a = o16;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(tq4.a aVar) {
            FLogger.INSTANCE.getLocal().d(o16.m.a(), ".Inside mSaveContactGroupsNotification onError");
            this.f2612a.g.close();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(g26.b bVar) {
            FLogger.INSTANCE.getLocal().d(o16.m.a(), ".Inside mSaveContactGroupsNotification onSuccess");
            this.f2612a.g.close();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1", f = "NotificationContactsPresenter.kt", l = {49}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$1", f = "NotificationContactsPresenter.kt", l = {50}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements iq4.e<d26.c, d26.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f2613a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1", f = "NotificationContactsPresenter.kt", l = {60}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ d26.c $responseValue;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.o16$c$b$a$a")
                @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.o16$c$b$a$a  reason: collision with other inner class name */
                public static final class C0177a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0177a(a aVar, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0177a aVar = new C0177a(this.this$0, qn7);
                        aVar.p$ = (iv7) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                        throw null;
                        //return ((C0177a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        yn7.d();
                        if (this.label == 0) {
                            el7.b(obj);
                            for (T t : this.this$0.$responseValue.a()) {
                                List<Contact> contacts = t.getContacts();
                                pq7.b(contacts, "it.contacts");
                                if (!contacts.isEmpty()) {
                                    Contact contact = t.getContacts().get(0);
                                    j06 j06 = new j06(contact, null, 2, null);
                                    j06.setAdded(true);
                                    Contact contact2 = j06.getContact();
                                    if (contact2 != null) {
                                        pq7.b(contact, "contact");
                                        contact2.setDbRowId(contact.getDbRowId());
                                    }
                                    Contact contact3 = j06.getContact();
                                    if (contact3 != null) {
                                        pq7.b(contact, "contact");
                                        contact3.setUseSms(contact.isUseSms());
                                    }
                                    Contact contact4 = j06.getContact();
                                    if (contact4 != null) {
                                        pq7.b(contact, "contact");
                                        contact4.setUseCall(contact.isUseCall());
                                    }
                                    pq7.b(contact, "contact");
                                    List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                                    pq7.b(phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                        pq7.b(phoneNumber, "contact.phoneNumbers[0]");
                                        if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                            j06.setHasPhoneNumber(true);
                                            PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                            pq7.b(phoneNumber2, "contact.phoneNumbers[0]");
                                            j06.setPhoneNumber(phoneNumber2.getNumber());
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a2 = o16.m.a();
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                            PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                            pq7.b(phoneNumber3, "contact.phoneNumbers[0]");
                                            sb.append(phoneNumber3.getNumber());
                                            local.d(a2, sb.toString());
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = o16.m.a();
                                    local2.d(a3, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                                    this.this$0.this$0.f2613a.this$0.v().add(j06);
                                }
                            }
                            return ao7.a(this.this$0.this$0.f2613a.this$0.w().addAll(pm7.j0(this.this$0.this$0.f2613a.this$0.v())));
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, d26.c cVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                    this.$responseValue = cVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, this.$responseValue, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        dv7 h = this.this$0.f2613a.this$0.h();
                        C0177a aVar = new C0177a(this, null);
                        this.L$0 = iv7;
                        this.label = 1;
                        if (eu7.g(h, aVar, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.f2613a.this$0.g.j5(this.this$0.f2613a.this$0.v(), w37.f3877a.a());
                    this.this$0.f2613a.this$0.k.d(0, new Bundle(), this.this$0.f2613a.this$0);
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(c cVar) {
                this.f2613a = cVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(d26.a aVar) {
                pq7.c(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(o16.m.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(d26.c cVar) {
                pq7.c(cVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(o16.m.a(), "GetAllContactGroup onSuccess");
                xw7 unused = gu7.d(this.f2613a.this$0.k(), null, null, new a(this, cVar, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(o16 o16, qn7 qn7) {
            super(2, qn7);
            this.this$0 = o16;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (!PortfolioApp.h0.c().k0().s0()) {
                    dv7 h = this.this$0.h();
                    a aVar = new a(null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(h, aVar, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.v().isEmpty()) {
                this.this$0.i.e(null, new b(this));
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = o16.class.getSimpleName();
        pq7.b(simpleName, "NotificationContactsPres\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public o16(j16 j16, uq4 uq4, d26 d26, g26 g26, LoaderManager loaderManager) {
        pq7.c(j16, "mView");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(d26, "mGetAllContactGroup");
        pq7.c(g26, "mSaveContactGroupsNotification");
        pq7.c(loaderManager, "mLoaderManager");
        this.g = j16;
        this.h = uq4;
        this.i = d26;
        this.j = g26;
        this.k = loaderManager;
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public at0<Cursor> d(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = has_phone_number!=0 AND mimetype=?");
        return new zs0(PortfolioApp.h0.c(), ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void g(at0<Cursor> at0) {
        pq7.c(at0, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.g.V();
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        jn5 jn5 = jn5.b;
        j16 j16 = this.g;
        if (j16 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsFragment");
        } else if (jn5.c(jn5, ((k16) j16).getContext(), jn5.a.NOTIFICATION_GET_CONTACTS, false, true, false, null, 52, null)) {
            xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    @Override // com.fossil.i16
    public void n() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List V = pm7.V(this.f, this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : V) {
            Contact contact = ((j06) obj).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            if (((List) entry.getValue()).size() == 1) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<j06> arrayList3 = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            mm7.s(arrayList3, (List) entry2.getValue());
        }
        for (j06 j06 : arrayList3) {
            Iterator<T> it = this.f.iterator();
            boolean z = false;
            while (it.hasNext()) {
                Contact contact2 = it.next().getContact();
                Integer valueOf2 = contact2 != null ? Integer.valueOf(contact2.getContactId()) : null;
                Contact contact3 = j06.getContact();
                if (pq7.a(valueOf2, contact3 != null ? Integer.valueOf(contact3.getContactId()) : null)) {
                    arrayList.add(j06);
                    z = true;
                }
            }
            if (!z) {
                arrayList2.add(j06);
            }
        }
        this.h.a(this.j, new g26.a(arrayList, arrayList2), new b(this));
    }

    @DexIgnore
    @Override // com.fossil.i16
    public void o() {
        FLogger.INSTANCE.getLocal().d(l, "onListContactWrapperChanged");
        List V = pm7.V(this.f, this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : V) {
            Contact contact = ((j06) obj).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            if (((List) entry.getValue()).size() == 1) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            mm7.s(arrayList, (List) entry2.getValue());
        }
        if (arrayList.isEmpty()) {
            this.g.f6(false);
        } else {
            this.g.f6(true);
        }
    }

    @DexIgnore
    public final List<j06> v() {
        return this.e;
    }

    @DexIgnore
    public final List<j06> w() {
        return this.f;
    }

    @DexIgnore
    /* renamed from: x */
    public void a(at0<Cursor> at0, Cursor cursor) {
        pq7.c(at0, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.g.T(cursor);
    }

    @DexIgnore
    public void y() {
        this.g.M5(this);
    }
}
