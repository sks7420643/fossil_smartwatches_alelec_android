package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dn3 implements Callable<byte[]> {
    @DexIgnore
    public /* final */ /* synthetic */ Vg3 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 c;

    @DexIgnore
    public Dn3(Qm3 qm3, Vg3 vg3, String str) {
        this.c = qm3;
        this.a = vg3;
        this.b = str;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ byte[] call() throws Exception {
        this.c.b.d0();
        this.c.b.X().u(this.a, this.b);
        throw null;
    }
}
