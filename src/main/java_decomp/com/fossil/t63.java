package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T63 implements U63 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.service.use_appinfo_modified", false);

    @DexIgnore
    @Override // com.fossil.U63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.U63
    public final boolean zzb() {
        return a.o().booleanValue();
    }
}
