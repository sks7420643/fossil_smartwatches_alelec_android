package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Op4 implements Factory<Vn5> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Op4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Op4 a(Uo4 uo4) {
        return new Op4(uo4);
    }

    @DexIgnore
    public static Vn5 c(Uo4 uo4) {
        Vn5 v = uo4.v();
        Lk7.c(v, "Cannot return null from a non-@Nullable @Provides method");
        return v;
    }

    @DexIgnore
    public Vn5 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
