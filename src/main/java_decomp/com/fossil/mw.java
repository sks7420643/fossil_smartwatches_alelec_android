package com.fossil;

import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mw extends Ox1 {
    @DexIgnore
    public static /* final */ Iw g; // = new Iw(null);
    @DexIgnore
    public /* final */ Hs b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Lw d;
    @DexIgnore
    public /* final */ S5 e;
    @DexIgnore
    public /* final */ Mt f;

    @DexIgnore
    public Mw(Hs hs, String str, Lw lw, S5 s5, Mt mt) {
        this.b = hs;
        this.c = str;
        this.d = lw;
        this.e = s5;
        this.f = mt;
    }

    @DexIgnore
    public /* synthetic */ Mw(Hs hs, String str, Lw lw, S5 s5, Mt mt, int i) {
        hs = (i & 1) != 0 ? Hs.b : hs;
        str = (i & 2) != 0 ? "" : str;
        s5 = (i & 8) != 0 ? new S5(null, R5.b, null, 5) : s5;
        mt = (i & 16) != 0 ? null : mt;
        this.b = hs;
        this.c = str;
        this.d = lw;
        this.e = s5;
        this.f = mt;
    }

    @DexIgnore
    public static /* synthetic */ Mw a(Mw mw, Hs hs, String str, Lw lw, S5 s5, Mt mt, int i) {
        return mw.a((i & 1) != 0 ? mw.b : hs, (i & 2) != 0 ? mw.c : str, (i & 4) != 0 ? mw.d : lw, (i & 8) != 0 ? mw.e : s5, (i & 16) != 0 ? mw.f : mt);
    }

    @DexIgnore
    public final Mw a(Hs hs, String str, Lw lw, S5 s5, Mt mt) {
        return new Mw(hs, str, lw, s5, mt);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Mw) {
                Mw mw = (Mw) obj;
                if (!Wg6.a(this.b, mw.b) || !Wg6.a(this.c, mw.c) || !Wg6.a(this.d, mw.d) || !Wg6.a(this.e, mw.e) || !Wg6.a(this.f, mw.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Hs hs = this.b;
        int hashCode = hs != null ? hs.hashCode() : 0;
        String str = this.c;
        int hashCode2 = str != null ? str.hashCode() : 0;
        Lw lw = this.d;
        int hashCode3 = lw != null ? lw.hashCode() : 0;
        S5 s5 = this.e;
        int hashCode4 = s5 != null ? s5.hashCode() : 0;
        Mt mt = this.f;
        if (mt != null) {
            i = mt.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(G80.k(jSONObject, Jd0.q, Ey1.a(this.b)), Jd0.O0, Ey1.a(this.d));
            Mt mt = this.f;
            if (mt != null && !mt.a()) {
                G80.k(jSONObject, Jd0.l3, this.f.getLogName());
            }
            if (this.e.c != R5.b) {
                G80.k(jSONObject, Jd0.l4, this.e.toJSONObject());
            }
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e2 = E.e("Result(requestId=");
        e2.append(this.b);
        e2.append(", requestUuid=");
        e2.append(this.c);
        e2.append(", resultCode=");
        e2.append(this.d);
        e2.append(", commandResult=");
        e2.append(this.e);
        e2.append(", responseStatus=");
        e2.append(this.f);
        e2.append(")");
        return e2.toString();
    }
}
