package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Collection;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xk1 extends Wk1 {
    @DexIgnore
    public /* final */ HashMap<String, Wk1> b; // = new HashMap<>();

    @DexIgnore
    @Override // com.fossil.Wk1
    public boolean a(E60 e60) {
        for (Wk1 wk1 : this.b.values()) {
            if (!wk1.a(e60)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Xk1 setDeviceTypes(Al1[] al1Arr) {
        HashMap<String, Wk1> hashMap = this.b;
        String simpleName = L.class.getSimpleName();
        Wg6.b(simpleName, "DeviceTypesScanFilter::class.java.simpleName");
        hashMap.put(simpleName, new L(al1Arr));
        return this;
    }

    @DexIgnore
    public final Xk1 setSerialNumberPattern(String str) {
        HashMap<String, Wk1> hashMap = this.b;
        String simpleName = M.class.getSimpleName();
        Wg6.b(simpleName, "SerialNumberPatternScanF\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new M(str));
        return this;
    }

    @DexIgnore
    public final Xk1 setSerialNumberPrefixes(String[] strArr) {
        HashMap<String, Wk1> hashMap = this.b;
        String simpleName = N.class.getSimpleName();
        Wg6.b(simpleName, "SerialNumberPrefixesScan\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new N(strArr));
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        Jd0 jd0 = Jd0.M1;
        Collection<Wk1> values = this.b.values();
        Wg6.b(values, "deviceFilters.values");
        Object[] array = values.toArray(new Wk1[0]);
        if (array != null) {
            return G80.k(jSONObject, jd0, Px1.a((Ox1[]) array));
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
