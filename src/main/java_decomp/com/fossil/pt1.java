package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ry1 d;
    @DexIgnore
    public /* final */ Yu1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Pt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Pt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Lq1.class.getClassLoader());
            if (readParcelable != null) {
                Lq1 lq1 = (Lq1) readParcelable;
                Nt1 nt1 = (Nt1) parcel.readParcelable(Nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(Ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new Pt1(lq1, nt1, (Ry1) readParcelable2, Yu1.values()[parcel.readInt()]);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Pt1[] newArray(int i) {
            return new Pt1[i];
        }
    }

    @DexIgnore
    public Pt1(Lq1 lq1, Nt1 nt1, Ry1 ry1, Yu1 yu1) {
        super(lq1, nt1);
        this.e = yu1;
        this.d = ry1;
    }

    @DexIgnore
    public Pt1(Lq1 lq1, Ry1 ry1, Yu1 yu1) {
        super(lq1, null);
        this.e = yu1;
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return G80.k(G80.k(super.a(), Jd0.t3, this.d.toString()), Jd0.c4, Ey1.a(this.e));
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        try {
            I9 i9 = I9.d;
            X90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9.a(s, ry1, new Kb0(((Lq1) deviceRequest).c(), new Ry1(this.d.getMajor(), this.d.getMinor()), this.e.a()).a());
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (Sx1 e2) {
            D90.i.i(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Pt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Pt1 pt1 = (Pt1) obj;
            if (!Wg6.a(this.d, pt1.d)) {
                return false;
            }
            return this.e == pt1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.MicroAppErrorData");
    }

    @DexIgnore
    public final Yu1 getErrorType() {
        return this.e;
    }

    @DexIgnore
    public final Ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e.ordinal());
        }
    }
}
