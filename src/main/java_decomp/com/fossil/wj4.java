package com.fossil;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wj4 {
    @DexIgnore
    public /* final */ Map<Type, Aj4<?>> a;
    @DexIgnore
    public /* final */ Lk4 b; // = Lk4.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Bk4<T> {
        @DexIgnore
        public Ai(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new ConcurrentHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Bk4<T> {
        @DexIgnore
        public Bi(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new TreeMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Bk4<T> {
        @DexIgnore
        public Ci(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new LinkedHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Bk4<T> {
        @DexIgnore
        public Di(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new Ak4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei implements Bk4<T> {
        @DexIgnore
        public /* final */ Fk4 a; // = Fk4.b();
        @DexIgnore
        public /* final */ /* synthetic */ Class b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public Ei(Wj4 wj4, Class cls, Type type) {
            this.b = cls;
            this.c = type;
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            try {
                return (T) this.a.c(this.b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.c + ". Registering an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements Bk4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Aj4 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public Fi(Wj4 wj4, Aj4 aj4, Type type) {
            this.a = aj4;
            this.b = type;
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) this.a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements Bk4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Aj4 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public Gi(Wj4 wj4, Aj4 aj4, Type type) {
            this.a = aj4;
            this.b = type;
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) this.a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi implements Bk4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor a;

        @DexIgnore
        public Hi(Wj4 wj4, Constructor constructor) {
            this.a = constructor;
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            try {
                return (T) this.a.newInstance(null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii implements Bk4<T> {
        @DexIgnore
        public Ii(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new TreeSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji implements Bk4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;

        @DexIgnore
        public Ji(Wj4 wj4, Type type) {
            this.a = type;
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            Type type = this.a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return (T) EnumSet.noneOf((Class) type2);
                }
                throw new Ej4("Invalid EnumSet type: " + this.a.toString());
            }
            throw new Ej4("Invalid EnumSet type: " + this.a.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ki implements Bk4<T> {
        @DexIgnore
        public Ki(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new LinkedHashSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Li implements Bk4<T> {
        @DexIgnore
        public Li(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new ArrayDeque();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Mi implements Bk4<T> {
        @DexIgnore
        public Mi(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new ArrayList();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ni implements Bk4<T> {
        @DexIgnore
        public Ni(Wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.Bk4
        public T a() {
            return (T) new ConcurrentSkipListMap();
        }
    }

    @DexIgnore
    public Wj4(Map<Type, Aj4<?>> map) {
        this.a = map;
    }

    @DexIgnore
    public <T> Bk4<T> a(TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        Aj4<?> aj4 = this.a.get(type);
        if (aj4 != null) {
            return new Fi(this, aj4, type);
        }
        Aj4<?> aj42 = this.a.get(rawType);
        if (aj42 != null) {
            return new Gi(this, aj42, type);
        }
        Bk4<T> b2 = b(rawType);
        if (b2 != null) {
            return b2;
        }
        Bk4<T> c = c(type, rawType);
        return c == null ? d(type, rawType) : c;
    }

    @DexIgnore
    public final <T> Bk4<T> b(Class<? super T> cls) {
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.b.b(declaredConstructor);
            }
            return new Hi(this, declaredConstructor);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @DexIgnore
    public final <T> Bk4<T> c(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new Ii(this) : EnumSet.class.isAssignableFrom(cls) ? new Ji(this, type) : Set.class.isAssignableFrom(cls) ? new Ki(this) : Queue.class.isAssignableFrom(cls) ? new Li(this) : new Mi(this);
        }
        if (Map.class.isAssignableFrom(cls)) {
            return ConcurrentNavigableMap.class.isAssignableFrom(cls) ? new Ni(this) : ConcurrentMap.class.isAssignableFrom(cls) ? new Ai(this) : SortedMap.class.isAssignableFrom(cls) ? new Bi(this) : (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) ? new Di(this) : new Ci(this);
        }
        return null;
    }

    @DexIgnore
    public final <T> Bk4<T> d(Type type, Class<? super T> cls) {
        return new Ei(this, cls, type);
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }
}
