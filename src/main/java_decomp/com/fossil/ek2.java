package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ek2 {
    @DexIgnore
    public static Ek2 a;

    @DexIgnore
    public static Ek2 b() {
        Ek2 ek2;
        synchronized (Ek2.class) {
            try {
                if (a == null) {
                    a = new Ak2();
                }
                ek2 = a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ek2;
    }

    @DexIgnore
    public abstract Fk2<Boolean> a(String str, boolean z);
}
