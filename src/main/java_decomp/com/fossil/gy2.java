package com.fossil;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gy2<E> extends Fy2<E> implements Az2<E>, NavigableSet<E> {
    @DexIgnore
    public transient Gy2<E> d;
    @DexIgnore
    public /* final */ transient Comparator<? super E> zza;

    @DexIgnore
    public Gy2(Comparator<? super E> comparator) {
        this.zza = comparator;
    }

    @DexIgnore
    public static <E> Vy2<E> zza(Comparator<? super E> comparator) {
        return Ly2.zza.equals(comparator) ? (Vy2<E>) Vy2.zzb : new Vy2<>(Sx2.zza(), comparator);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E ceiling(E e) {
        return (E) Iy2.a((Gy2) tailSet(e, true), null);
    }

    @DexIgnore
    @Override // com.fossil.Az2, java.util.SortedSet
    public Comparator<? super E> comparator() {
        return this.zza;
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public /* synthetic */ Iterator descendingIterator() {
        return zzj();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet descendingSet() {
        Gy2<E> gy2 = this.d;
        if (gy2 != null) {
            return gy2;
        }
        Gy2<E> zzi = zzi();
        this.d = zzi;
        zzi.d = this;
        return zzi;
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E first() {
        return (E) ((Cz2) iterator()).next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E floor(E e) {
        return (E) Hy2.a((Cz2) ((Gy2) headSet(e, true)).descendingIterator(), null);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet headSet(Object obj, boolean z) {
        Sw2.b(obj);
        return zza((Gy2<E>) obj, z);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet headSet(Object obj) {
        return (Gy2) headSet(obj, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E higher(E e) {
        return (E) Iy2.a((Gy2) tailSet(e, false), null);
    }

    @DexIgnore
    @Override // com.fossil.Tx2, com.fossil.Ay2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.util.NavigableSet, java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E last() {
        return (E) ((Cz2) descendingIterator()).next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E lower(E e) {
        return (E) Hy2.a((Cz2) ((Gy2) headSet(e, false)).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet subSet(Object obj, boolean z, Object obj2, boolean z2) {
        Sw2.b(obj);
        Sw2.b(obj2);
        if (this.zza.compare(obj, obj2) <= 0) {
            return zza(obj, z, obj2, z2);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet subSet(Object obj, Object obj2) {
        return (Gy2) subSet(obj, true, obj2, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet tailSet(Object obj, boolean z) {
        Sw2.b(obj);
        return zzb((Gy2<E>) obj, z);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet tailSet(Object obj) {
        return (Gy2) tailSet(obj, true);
    }

    @DexIgnore
    public final int zza(Object obj, Object obj2) {
        return this.zza.compare(obj, obj2);
    }

    @DexIgnore
    public abstract Gy2<E> zza(E e, boolean z);

    @DexIgnore
    public abstract Gy2<E> zza(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    public abstract Gy2<E> zzb(E e, boolean z);

    @DexIgnore
    public abstract Gy2<E> zzi();

    @DexIgnore
    public abstract Cz2<E> zzj();
}
