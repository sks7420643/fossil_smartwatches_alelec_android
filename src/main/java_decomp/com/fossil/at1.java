package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class At1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ G90 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<At1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public At1 createFromParcel(Parcel parcel) {
            G90 g90 = G90.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = F90.a[g90.ordinal()];
            if (i == 1) {
                return I90.CREATOR.a(parcel);
            }
            if (i == 2) {
                return Ct1.CREATOR.a(parcel);
            }
            if (i == 3) {
                return Bt1.CREATOR.a(parcel);
            }
            throw new Kc6();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public At1[] newArray(int i) {
            return new At1[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public At1(Parcel parcel) {
        this(G90.values()[parcel.readInt()], parcel.readInt() != 1 ? false : true);
    }

    @DexIgnore
    public At1(G90 g90, boolean z) {
        this.b = g90;
        this.c = z;
    }

    @DexIgnore
    public /* synthetic */ At1(G90 g90, boolean z, int i) {
        z = (i & 2) != 0 ? false : z;
        this.b = g90;
        this.c = z;
    }

    @DexIgnore
    public boolean a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            At1 at1 = (At1) obj;
            return this.b == at1.b && this.c == at1.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + Boolean.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("enable_goal_ring", this.c);
        Wg6.b(put, "JSONObject()\n           \u2026 mEnablePercentageCircle)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }
}
