package com.fossil;

import com.fossil.A91;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.j256.ormlite.stmt.query.SimpleComparison;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ba1 {
    @DexIgnore
    public static String a(long j) {
        return b().format(new Date(j));
    }

    @DexIgnore
    public static SimpleDateFormat b() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }

    @DexIgnore
    public static A91.Ai c(J91 j91) {
        boolean z;
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = j91.c;
        String str = map.get(ConfigItem.KEY_DATE);
        long e = str != null ? e(str) : 0;
        String str2 = map.get(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
        boolean z2 = false;
        if (str2 != null) {
            String[] split = str2.split(",", 0);
            z2 = false;
            j = 0;
            long j6 = 0;
            for (String str3 : split) {
                String trim = str3.trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j = Long.parseLong(trim.substring(8));
                    } catch (Exception e2) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    j6 = Long.parseLong(trim.substring(23));
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    z2 = true;
                }
            }
            z = true;
            j2 = j6;
        } else {
            z = false;
            j = 0;
            j2 = 0;
        }
        String str4 = map.get("Expires");
        long e3 = str4 != null ? e(str4) : 0;
        String str5 = map.get("Last-Modified");
        long e4 = str5 != null ? e(str5) : 0;
        String str6 = map.get("ETag");
        if (z) {
            j3 = currentTimeMillis + (j * 1000);
            if (z2) {
                j5 = j3;
            } else {
                Long.signum(j2);
                j5 = (1000 * j2) + j3;
            }
            j4 = j5;
        } else if (e <= 0 || e3 < e) {
            j3 = 0;
            j4 = 0;
        } else {
            long j7 = (e3 - e) + currentTimeMillis;
            j3 = j7;
            j4 = j7;
        }
        A91.Ai ai = new A91.Ai();
        ai.a = j91.b;
        ai.b = str6;
        ai.f = j3;
        ai.e = j4;
        ai.c = e;
        ai.d = e4;
        ai.g = map;
        ai.h = j91.d;
        return ai;
    }

    @DexIgnore
    public static String d(Map<String, String> map, String str) {
        String str2 = map.get("Content-Type");
        if (str2 == null) {
            return str;
        }
        String[] split = str2.split(";", 0);
        for (int i = 1; i < split.length; i++) {
            String[] split2 = split[i].trim().split(SimpleComparison.EQUAL_TO_OPERATION, 0);
            if (split2.length == 2 && split2[0].equals("charset")) {
                return split2[1];
            }
        }
        return str;
    }

    @DexIgnore
    public static long e(String str) {
        try {
            return b().parse(str).getTime();
        } catch (ParseException e) {
            U91.d(e, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    @DexIgnore
    public static List<F91> f(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            arrayList.add(new F91(entry.getKey(), entry.getValue()));
        }
        return arrayList;
    }

    @DexIgnore
    public static Map<String, String> g(List<F91> list) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (F91 f91 : list) {
            treeMap.put(f91.a(), f91.b());
        }
        return treeMap;
    }
}
