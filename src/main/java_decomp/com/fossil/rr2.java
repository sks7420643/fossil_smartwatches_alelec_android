package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rr2 extends Ec2<Uq2> {
    @DexIgnore
    public /* final */ String E;
    @DexIgnore
    public /* final */ Mr2<Uq2> F; // = new Sr2(this);

    @DexIgnore
    public Rr2(Context context, Looper looper, R62.Bi bi, R62.Ci ci, String str, Ac2 ac2) {
        super(context, looper, 23, ac2, bi, ci);
        this.E = str;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public Bundle F() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.E);
        return bundle;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public String p() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof Uq2 ? (Uq2) queryLocalInterface : new Vq2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2, com.fossil.Yb2
    public int s() {
        return 11925000;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public String x() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }
}
