package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.Ve0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U78 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<U78> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public Object i;
    @DexIgnore
    public Context j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<U78> {
        @DexIgnore
        public U78 a(Parcel parcel) {
            return new U78(parcel, null);
        }

        @DexIgnore
        public U78[] b(int i) {
            return new U78[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ U78 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ U78[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public U78(Parcel parcel) {
        this.b = parcel.readInt();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.h = parcel.readInt();
    }

    @DexIgnore
    public /* synthetic */ U78(Parcel parcel, Ai ai) {
        this(parcel);
    }

    @DexIgnore
    public static U78 a(Intent intent, Activity activity) {
        U78 u78 = (U78) intent.getParcelableExtra("extra_app_settings");
        u78.c(activity);
        return u78;
    }

    @DexIgnore
    public int b() {
        return this.h;
    }

    @DexIgnore
    public final void c(Object obj) {
        this.i = obj;
        if (obj instanceof Activity) {
            this.j = (Activity) obj;
        } else if (obj instanceof Fragment) {
            this.j = ((Fragment) obj).getContext();
        } else {
            throw new IllegalStateException("Unknown object: " + obj);
        }
    }

    @DexIgnore
    public Ve0 d(DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        int i2 = this.b;
        Ve0.Ai ai = i2 > 0 ? new Ve0.Ai(this.j, i2) : new Ve0.Ai(this.j);
        ai.d(false);
        ai.o(this.d);
        ai.g(this.c);
        ai.l(this.e, onClickListener);
        ai.h(this.f, onClickListener2);
        return ai.q();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
    }
}
