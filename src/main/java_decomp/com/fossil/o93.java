package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O93 implements P93 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.upload.file_lock_state_check", false);

    @DexIgnore
    @Override // com.fossil.P93
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
