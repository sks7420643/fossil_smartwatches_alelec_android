package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mx4 implements Factory<BCCreateSubTabViewModel> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<Tt4> b;
    @DexIgnore
    public /* final */ Provider<FriendRepository> c;

    @DexIgnore
    public Mx4(Provider<An4> provider, Provider<Tt4> provider2, Provider<FriendRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Mx4 a(Provider<An4> provider, Provider<Tt4> provider2, Provider<FriendRepository> provider3) {
        return new Mx4(provider, provider2, provider3);
    }

    @DexIgnore
    public static BCCreateSubTabViewModel c(An4 an4, Tt4 tt4, FriendRepository friendRepository) {
        return new BCCreateSubTabViewModel(an4, tt4, friendRepository);
    }

    @DexIgnore
    public BCCreateSubTabViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
