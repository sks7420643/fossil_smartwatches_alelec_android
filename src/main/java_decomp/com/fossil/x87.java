package com.fossil;

import android.graphics.Bitmap;
import com.mapped.WatchFaceStickerStorage;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X87 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public X87(String str, String str2) {
        Wg6.c(str, "name");
        Wg6.c(str2, "url");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final Bitmap a() {
        return WatchFaceStickerStorage.c.c(this.a);
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof X87) {
                X87 x87 = (X87) obj;
                if (!Wg6.a(this.a, x87.a) || !Wg6.a(this.b, x87.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "StickerInfo(name=" + this.a + ", url=" + this.b + ")";
    }
}
