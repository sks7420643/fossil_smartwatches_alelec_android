package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lb1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Gi {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream a;

        @DexIgnore
        public Ai(InputStream inputStream) {
            this.a = inputStream;
        }

        @DexIgnore
        @Override // com.fossil.Lb1.Gi
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            try {
                return imageHeaderParser.b(this.a);
            } finally {
                this.a.reset();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Gi {
        @DexIgnore
        public /* final */ /* synthetic */ ByteBuffer a;

        @DexIgnore
        public Bi(ByteBuffer byteBuffer) {
            this.a = byteBuffer;
        }

        @DexIgnore
        @Override // com.fossil.Lb1.Gi
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            return imageHeaderParser.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Gi {
        @DexIgnore
        public /* final */ /* synthetic */ Fc1 a;
        @DexIgnore
        public /* final */ /* synthetic */ Od1 b;

        @DexIgnore
        public Ci(Fc1 fc1, Od1 od1) {
            this.a = fc1;
            this.b = od1;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0028 A[SYNTHETIC, Splitter:B:12:0x0028] */
        @Override // com.fossil.Lb1.Gi
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.bumptech.glide.load.ImageHeaderParser.ImageType a(com.bumptech.glide.load.ImageHeaderParser r5) throws java.io.IOException {
            /*
                r4 = this;
                r1 = 0
                com.fossil.Qg1 r0 = new com.fossil.Qg1     // Catch:{ all -> 0x0031 }
                java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0031 }
                com.fossil.Fc1 r3 = r4.a     // Catch:{ all -> 0x0031 }
                android.os.ParcelFileDescriptor r3 = r3.d()     // Catch:{ all -> 0x0031 }
                java.io.FileDescriptor r3 = r3.getFileDescriptor()     // Catch:{ all -> 0x0031 }
                r2.<init>(r3)     // Catch:{ all -> 0x0031 }
                com.fossil.Od1 r3 = r4.b     // Catch:{ all -> 0x0031 }
                r0.<init>(r2, r3)     // Catch:{ all -> 0x0031 }
                com.bumptech.glide.load.ImageHeaderParser$ImageType r1 = r5.b(r0)     // Catch:{ all -> 0x0024 }
                r0.close()     // Catch:{ IOException -> 0x0034 }
            L_0x001e:
                com.fossil.Fc1 r0 = r4.a
                r0.d()
                return r1
            L_0x0024:
                r2 = move-exception
                r1 = r0
            L_0x0026:
                if (r1 == 0) goto L_0x002b
                r1.close()     // Catch:{ IOException -> 0x0036 }
            L_0x002b:
                com.fossil.Fc1 r0 = r4.a
                r0.d()
                throw r2
            L_0x0031:
                r0 = move-exception
                r2 = r0
                goto L_0x0026
            L_0x0034:
                r0 = move-exception
                goto L_0x001e
            L_0x0036:
                r0 = move-exception
                goto L_0x002b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lb1.Ci.a(com.bumptech.glide.load.ImageHeaderParser):com.bumptech.glide.load.ImageHeaderParser$ImageType");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Fi {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream a;
        @DexIgnore
        public /* final */ /* synthetic */ Od1 b;

        @DexIgnore
        public Di(InputStream inputStream, Od1 od1) {
            this.a = inputStream;
            this.b = od1;
        }

        @DexIgnore
        @Override // com.fossil.Lb1.Fi
        public int a(ImageHeaderParser imageHeaderParser) throws IOException {
            try {
                return imageHeaderParser.c(this.a, this.b);
            } finally {
                this.a.reset();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei implements Fi {
        @DexIgnore
        public /* final */ /* synthetic */ Fc1 a;
        @DexIgnore
        public /* final */ /* synthetic */ Od1 b;

        @DexIgnore
        public Ei(Fc1 fc1, Od1 od1) {
            this.a = fc1;
            this.b = od1;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x002a A[SYNTHETIC, Splitter:B:12:0x002a] */
        @Override // com.fossil.Lb1.Fi
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int a(com.bumptech.glide.load.ImageHeaderParser r5) throws java.io.IOException {
            /*
                r4 = this;
                r2 = 0
                com.fossil.Qg1 r1 = new com.fossil.Qg1     // Catch:{ all -> 0x0026 }
                java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0026 }
                com.fossil.Fc1 r3 = r4.a     // Catch:{ all -> 0x0026 }
                android.os.ParcelFileDescriptor r3 = r3.d()     // Catch:{ all -> 0x0026 }
                java.io.FileDescriptor r3 = r3.getFileDescriptor()     // Catch:{ all -> 0x0026 }
                r0.<init>(r3)     // Catch:{ all -> 0x0026 }
                com.fossil.Od1 r3 = r4.b     // Catch:{ all -> 0x0026 }
                r1.<init>(r0, r3)     // Catch:{ all -> 0x0026 }
                com.fossil.Od1 r0 = r4.b     // Catch:{ all -> 0x0037 }
                int r0 = r5.c(r1, r0)     // Catch:{ all -> 0x0037 }
                r1.close()     // Catch:{ IOException -> 0x0033 }
            L_0x0020:
                com.fossil.Fc1 r1 = r4.a
                r1.d()
                return r0
            L_0x0026:
                r0 = move-exception
                r1 = r2
            L_0x0028:
                if (r1 == 0) goto L_0x002d
                r1.close()     // Catch:{ IOException -> 0x0035 }
            L_0x002d:
                com.fossil.Fc1 r1 = r4.a
                r1.d()
                throw r0
            L_0x0033:
                r1 = move-exception
                goto L_0x0020
            L_0x0035:
                r1 = move-exception
                goto L_0x002d
            L_0x0037:
                r0 = move-exception
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lb1.Ei.a(com.bumptech.glide.load.ImageHeaderParser):int");
        }
    }

    @DexIgnore
    public interface Fi {
        @DexIgnore
        int a(ImageHeaderParser imageHeaderParser) throws IOException;
    }

    @DexIgnore
    public interface Gi {
        @DexIgnore
        ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException;
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, Fc1 fc1, Od1 od1) throws IOException {
        return c(list, new Ei(fc1, od1));
    }

    @DexIgnore
    public static int b(List<ImageHeaderParser> list, InputStream inputStream, Od1 od1) throws IOException {
        if (inputStream == null) {
            return -1;
        }
        if (!inputStream.markSupported()) {
            inputStream = new Qg1(inputStream, od1);
        }
        inputStream.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
        return c(list, new Di(inputStream, od1));
    }

    @DexIgnore
    public static int c(List<ImageHeaderParser> list, Fi fi) throws IOException {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            int a2 = fi.a(list.get(i));
            if (a2 != -1) {
                return a2;
            }
        }
        return -1;
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType d(List<ImageHeaderParser> list, Fc1 fc1, Od1 od1) throws IOException {
        return g(list, new Ci(fc1, od1));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType e(List<ImageHeaderParser> list, InputStream inputStream, Od1 od1) throws IOException {
        if (inputStream == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        if (!inputStream.markSupported()) {
            inputStream = new Qg1(inputStream, od1);
        }
        inputStream.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
        return g(list, new Ai(inputStream));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType f(List<ImageHeaderParser> list, ByteBuffer byteBuffer) throws IOException {
        return byteBuffer == null ? ImageHeaderParser.ImageType.UNKNOWN : g(list, new Bi(byteBuffer));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType g(List<ImageHeaderParser> list, Gi gi) throws IOException {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ImageHeaderParser.ImageType a2 = gi.a(list.get(i));
            if (a2 != ImageHeaderParser.ImageType.UNKNOWN) {
                return a2;
            }
        }
        return ImageHeaderParser.ImageType.UNKNOWN;
    }
}
