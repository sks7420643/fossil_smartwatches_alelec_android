package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum tl1 {
    HOUR((byte) 1),
    MINUTE((byte) 2),
    SUB_EYE((byte) 3);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public tl1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
