package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Zq {
    b(0),
    c(1),
    d(2),
    e(3),
    f(4),
    g(5),
    h(6),
    i(7),
    j(8),
    k(9),
    l(10),
    m(11),
    n(12),
    o(13),
    p(14),
    q(15),
    r(16),
    s(17),
    t(18),
    u(19),
    v(20),
    w(21),
    x(22),
    y(23),
    z(24),
    A(25),
    B(26),
    C(27),
    D(28),
    E(29),
    F(257),
    G(254);
    
    @DexIgnore
    public static /* final */ Lq I; // = new Lq(null);

    @DexIgnore
    public Zq(int i2) {
    }
}
