package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yv7<T> extends N08 {
    @DexIgnore
    public int d;

    @DexIgnore
    public Yv7(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
    }

    @DexIgnore
    public abstract Xe6<T> c();

    @DexIgnore
    public final Throwable d(Object obj) {
        Vu7 vu7 = (Vu7) (!(obj instanceof Vu7) ? null : obj);
        if (vu7 != null) {
            return vu7.a;
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public <T> T h(Object obj) {
        return obj;
    }

    @DexIgnore
    public final void i(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                Tk7.a(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            String str = "Fatal exception in coroutines machinery for " + this + ". Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (th != null) {
                Fv7.a(c().getContext(), new Mv7(str, th));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public abstract Object j();

    @DexIgnore
    public final void run() {
        Object r1;
        Object r12;
        O08 o08 = this.c;
        try {
            Xe6<T> c = c();
            if (c != null) {
                Vv7 vv7 = (Vv7) c;
                Xe6<T> xe6 = vv7.i;
                Af6 context = xe6.getContext();
                Object j = j();
                Object c2 = Zz7.c(context, vv7.g);
                try {
                    Throwable d2 = d(j);
                    Rm6 rm6 = Zv7.b(this.d) ? (Rm6) context.get(Rm6.r) : null;
                    if (d2 == null && rm6 != null && !rm6.isActive()) {
                        CancellationException k = rm6.k();
                        a(j, k);
                        Dl7.Ai ai = Dl7.Companion;
                        xe6.resumeWith(Dl7.constructor-impl(El7.a(Nv7.d() ? !(xe6 instanceof Do7) ? k : Uz7.j(k, (Do7) xe6) : k)));
                    } else if (d2 != null) {
                        Dl7.Ai ai2 = Dl7.Companion;
                        xe6.resumeWith(Dl7.constructor-impl(El7.a(d2)));
                    } else {
                        T h = h(j);
                        Dl7.Ai ai3 = Dl7.Companion;
                        xe6.resumeWith(Dl7.constructor-impl(h));
                    }
                    Cd6 cd6 = Cd6.a;
                    try {
                        Dl7.Ai ai4 = Dl7.Companion;
                        o08.h();
                        r12 = Dl7.constructor-impl(Cd6.a);
                    } catch (Throwable th) {
                        Dl7.Ai ai5 = Dl7.Companion;
                        r12 = Dl7.constructor-impl(El7.a(th));
                    }
                    i(null, Dl7.exceptionOrNull-impl(r12));
                    return;
                } finally {
                    Zz7.a(context, c2);
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
            }
        } catch (Throwable th2) {
            Dl7.Ai ai6 = Dl7.Companion;
            r1 = Dl7.constructor-impl(El7.a(th2));
        }
        i(th, Dl7.exceptionOrNull-impl(r1));
    }
}
