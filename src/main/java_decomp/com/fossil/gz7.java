package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.BufferedReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gz7 {
    @DexIgnore
    public static /* final */ Gz7 a; // = new Gz7();

    @DexIgnore
    public final <S> S a(String str, ClassLoader classLoader, Class<S> cls) {
        Class<?> cls2 = Class.forName(str, false, classLoader);
        if (cls.isAssignableFrom(cls2)) {
            return cls.cast(cls2.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        }
        throw new IllegalArgumentException(("Expected service of class " + cls + ", but found " + cls2).toString());
    }

    @DexIgnore
    public final <S> List<S> b(Class<S> cls, ClassLoader classLoader) {
        try {
            return d(cls, classLoader);
        } catch (Throwable th) {
            return Pm7.h0(ServiceLoader.load(cls, classLoader));
        }
    }

    @DexIgnore
    public final List<MainDispatcherFactory> c() {
        MainDispatcherFactory mainDispatcherFactory;
        MainDispatcherFactory mainDispatcherFactory2;
        if (!Hz7.a()) {
            return b(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
        try {
            ArrayList arrayList = new ArrayList(2);
            try {
                mainDispatcherFactory = (MainDispatcherFactory) MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.android.AndroidDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } catch (ClassNotFoundException e) {
                mainDispatcherFactory = null;
            }
            if (mainDispatcherFactory != null) {
                arrayList.add(mainDispatcherFactory);
            }
            try {
                mainDispatcherFactory2 = (MainDispatcherFactory) MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.test.internal.TestMainDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } catch (ClassNotFoundException e2) {
                mainDispatcherFactory2 = null;
            }
            if (mainDispatcherFactory2 == null) {
                return arrayList;
            }
            arrayList.add(mainDispatcherFactory2);
            return arrayList;
        } catch (Throwable th) {
            return b(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
    }

    @DexIgnore
    public final <S> List<S> d(Class<S> cls, ClassLoader classLoader) {
        ArrayList<URL> list = Collections.list(classLoader.getResources("META-INF/services/" + cls.getName()));
        Wg6.b(list, "java.util.Collections.list(this)");
        ArrayList arrayList = new ArrayList();
        for (URL url : list) {
            Mm7.s(arrayList, a.e(url));
        }
        Set<String> l0 = Pm7.l0(arrayList);
        if (!l0.isEmpty()) {
            ArrayList arrayList2 = new ArrayList(Im7.m(l0, 10));
            for (String str : l0) {
                arrayList2.add(a.a(str, classLoader, cls));
            }
            return arrayList2;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004e, code lost:
        com.fossil.So7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0054, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0058, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0059, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005a, code lost:
        com.fossil.Tk7.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005d, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0078, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0079, code lost:
        com.fossil.So7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007c, code lost:
        throw r2;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> e(java.net.URL r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 2
            r3 = 0
            java.lang.String r0 = r7.toString()
            java.lang.String r1 = "jar"
            boolean r1 = com.fossil.Vt7.s(r0, r1, r5, r4, r3)
            if (r1 == 0) goto L_0x005e
            java.lang.String r1 = "jar:file:"
            java.lang.String r1 = com.fossil.Wt7.j0(r0, r1, r3, r4, r3)
            r2 = 33
            java.lang.String r1 = com.fossil.Wt7.o0(r1, r2, r3, r4, r3)
            java.lang.String r2 = "!/"
            java.lang.String r0 = com.fossil.Wt7.j0(r0, r2, r3, r4, r3)
            java.util.jar.JarFile r2 = new java.util.jar.JarFile
            r2.<init>(r1, r5)
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x0052 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0052 }
            java.util.zip.ZipEntry r4 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x0052 }
            r4.<init>(r0)     // Catch:{ all -> 0x0052 }
            java.io.InputStream r0 = r2.getInputStream(r4)     // Catch:{ all -> 0x0052 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r0, r4)     // Catch:{ all -> 0x0052 }
            r1.<init>(r3)     // Catch:{ all -> 0x0052 }
            com.fossil.Gz7 r0 = com.fossil.Gz7.a     // Catch:{ all -> 0x004b }
            java.util.List r0 = r0.f(r1)     // Catch:{ all -> 0x004b }
            r3 = 0
            com.fossil.So7.a(r1, r3)
            r2.close()     // Catch:{ all -> 0x0049 }
        L_0x0048:
            return r0
        L_0x0049:
            r0 = move-exception
            throw r0
        L_0x004b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004d }
        L_0x004d:
            r3 = move-exception
            com.fossil.So7.a(r1, r0)
            throw r3
        L_0x0052:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0054 }
        L_0x0054:
            r1 = move-exception
            r2.close()     // Catch:{ all -> 0x0059 }
            throw r1
        L_0x0059:
            r1 = move-exception
            com.fossil.Tk7.a(r0, r1)
            throw r0
        L_0x005e:
            java.io.BufferedReader r1 = new java.io.BufferedReader
            java.io.InputStreamReader r0 = new java.io.InputStreamReader
            java.io.InputStream r2 = r7.openStream()
            r0.<init>(r2)
            r1.<init>(r0)
            com.fossil.Gz7 r0 = com.fossil.Gz7.a     // Catch:{ all -> 0x0076 }
            java.util.List r0 = r0.f(r1)     // Catch:{ all -> 0x0076 }
            com.fossil.So7.a(r1, r3)
            goto L_0x0048
        L_0x0076:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0078 }
        L_0x0078:
            r2 = move-exception
            com.fossil.So7.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Gz7.e(java.net.URL):java.util.List");
    }

    @DexIgnore
    public final List<String> f(BufferedReader bufferedReader) {
        boolean z;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return Pm7.h0(linkedHashSet);
            }
            String p0 = Wt7.p0(readLine, "#", null, 2, null);
            if (p0 != null) {
                String obj = Wt7.u0(p0).toString();
                int i = 0;
                while (true) {
                    if (i >= obj.length()) {
                        z = true;
                        break;
                    }
                    char charAt = obj.charAt(i);
                    if (!(charAt == '.' || Character.isJavaIdentifierPart(charAt))) {
                        z = false;
                        break;
                    }
                    i++;
                }
                if (z) {
                    if (obj.length() > 0) {
                        linkedHashSet.add(obj);
                    }
                } else {
                    throw new IllegalArgumentException(("Illegal service provider class name: " + obj).toString());
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }
}
