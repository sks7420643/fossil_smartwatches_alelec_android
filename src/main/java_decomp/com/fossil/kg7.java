package com.fossil;

import android.content.Context;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kg7 extends Ng7 {
    @DexIgnore
    public Lg7 m;
    @DexIgnore
    public long n; // = -1;

    @DexIgnore
    public Kg7(Context context, int i, String str, Jg7 jg7) {
        super(context, i, jg7);
        Lg7 lg7 = new Lg7();
        this.m = lg7;
        lg7.a = str;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public Og7 a() {
        return Og7.d;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public boolean b(JSONObject jSONObject) {
        String str;
        jSONObject.put("ei", this.m.a);
        long j = this.n;
        if (j > 0) {
            jSONObject.put("du", j);
        }
        Object obj = this.m.b;
        if (obj == null) {
            j();
            obj = this.m.c;
            str = "kv";
        } else {
            str = "ar";
        }
        jSONObject.put(str, obj);
        return true;
    }

    @DexIgnore
    public Lg7 i() {
        return this.m;
    }

    @DexIgnore
    public final void j() {
        Properties w;
        String str = this.m.a;
        if (!(str == null || (w = Ig7.w(str)) == null || w.size() <= 0)) {
            JSONObject jSONObject = this.m.c;
            if (jSONObject == null || jSONObject.length() == 0) {
                this.m.c = new JSONObject(w);
                return;
            }
            for (Map.Entry entry : w.entrySet()) {
                try {
                    this.m.c.put(entry.getKey().toString(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
