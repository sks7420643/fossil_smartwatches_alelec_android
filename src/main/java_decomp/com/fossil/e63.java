package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E63 implements Xw2<D63> {
    @DexIgnore
    public static E63 c; // = new E63();
    @DexIgnore
    public /* final */ Xw2<D63> b;

    @DexIgnore
    public E63() {
        this(Ww2.b(new G63()));
    }

    @DexIgnore
    public E63(Xw2<D63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((D63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((D63) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((D63) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((D63) c.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((D63) c.zza()).zze();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ D63 zza() {
        return this.b.zza();
    }
}
