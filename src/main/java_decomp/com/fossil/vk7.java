package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vk7<T> implements Yk7<T>, Serializable {
    @DexIgnore
    public /* final */ T value;

    @DexIgnore
    public Vk7(T t) {
        this.value = t;
    }

    @DexIgnore
    @Override // com.fossil.Yk7
    public T getValue() {
        return this.value;
    }

    @DexIgnore
    public boolean isInitialized() {
        return true;
    }

    @DexIgnore
    public String toString() {
        return String.valueOf(getValue());
    }
}
