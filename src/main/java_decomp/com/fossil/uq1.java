package com.fossil;

import android.location.Location;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.fitness.WorkoutSessionManager;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uq1 extends X90 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ Location[] f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Uq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uq1 createFromParcel(Parcel parcel) {
            return new Uq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uq1[] newArray(int i) {
            return new Uq1[i];
        }
    }

    @DexIgnore
    public Uq1(byte b, int i, long j) {
        super(E90.WORKOUT_ROUTE_IMAGE, b, i);
        this.e = j;
        ArrayList<GpsDataPoint> gpsRoute = WorkoutSessionManager.getGpsRoute(j);
        Wg6.b(gpsRoute, "WorkoutSessionManager.getGpsRoute(sessionId)");
        ArrayList arrayList = new ArrayList(Im7.m(gpsRoute, 10));
        for (T t : gpsRoute) {
            Wg6.b(t, "gpsDataPoint");
            Location location = new Location("gpsDataPoint");
            location.setLongitude(t.getLongitude());
            location.setLatitude(t.getLatitude());
            location.setTime((long) (t.getTimestamp() * 1000));
            location.setAltitude(t.getAltitude());
            location.setAccuracy(t.getHorizontalAccuracy());
            if (Build.VERSION.SDK_INT >= 26) {
                location.setVerticalAccuracyMeters(t.getVerticalAccuracy());
            }
            location.setSpeed((float) t.getSpeed());
            location.setBearing((float) t.getHeading());
            arrayList.add(location);
        }
        Object[] array = arrayList.toArray(new Location[0]);
        if (array != null) {
            this.f = (Location[]) array;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public /* synthetic */ Uq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = parcel.readLong();
        Location[] locationArr = (Location[]) parcel.createTypedArray(Location.CREATOR);
        this.f = locationArr == null ? new Location[0] : locationArr;
    }

    @DexIgnore
    public final Location[] c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Uq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.e == ((Uq1) obj).e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + Long.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.H5, Long.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.f, i);
        }
    }
}
