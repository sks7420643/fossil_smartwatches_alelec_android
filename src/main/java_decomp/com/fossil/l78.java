package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class L78 extends O78 implements E78 {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 9044267456635152283L;

    @DexIgnore
    @Override // com.fossil.O78
    public void debug(G78 g78, String str) {
        debug(str);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void debug(G78 g78, String str, Object obj) {
        debug(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void debug(G78 g78, String str, Object obj, Object obj2) {
        debug(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void debug(G78 g78, String str, Throwable th) {
        debug(str, th);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void debug(G78 g78, String str, Object... objArr) {
        debug(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void debug(String str);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void debug(String str, Object obj);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void debug(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void debug(String str, Throwable th);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void debug(String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.O78
    public void error(G78 g78, String str) {
        error(str);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void error(G78 g78, String str, Object obj) {
        error(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void error(G78 g78, String str, Object obj, Object obj2) {
        error(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void error(G78 g78, String str, Throwable th) {
        error(str, th);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void error(G78 g78, String str, Object... objArr) {
        error(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void error(String str);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void error(String str, Object obj);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void error(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void error(String str, Throwable th);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void error(String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.O78
    public /* bridge */ /* synthetic */ String getName() {
        return super.getName();
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void info(G78 g78, String str) {
        info(str);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void info(G78 g78, String str, Object obj) {
        info(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void info(G78 g78, String str, Object obj, Object obj2) {
        info(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void info(G78 g78, String str, Throwable th) {
        info(str, th);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void info(G78 g78, String str, Object... objArr) {
        info(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void info(String str);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void info(String str, Object obj);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void info(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void info(String str, Throwable th);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void info(String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ boolean isDebugEnabled();

    @DexIgnore
    @Override // com.fossil.O78
    public boolean isDebugEnabled(G78 g78) {
        return isDebugEnabled();
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ boolean isErrorEnabled();

    @DexIgnore
    @Override // com.fossil.O78
    public boolean isErrorEnabled(G78 g78) {
        return isErrorEnabled();
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ boolean isInfoEnabled();

    @DexIgnore
    @Override // com.fossil.O78
    public boolean isInfoEnabled(G78 g78) {
        return isInfoEnabled();
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ boolean isTraceEnabled();

    @DexIgnore
    @Override // com.fossil.O78
    public boolean isTraceEnabled(G78 g78) {
        return isTraceEnabled();
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ boolean isWarnEnabled();

    @DexIgnore
    @Override // com.fossil.O78
    public boolean isWarnEnabled(G78 g78) {
        return isWarnEnabled();
    }

    @DexIgnore
    public String toString() {
        return getClass().getName() + "(" + getName() + ")";
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void trace(G78 g78, String str) {
        trace(str);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void trace(G78 g78, String str, Object obj) {
        trace(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void trace(G78 g78, String str, Object obj, Object obj2) {
        trace(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void trace(G78 g78, String str, Throwable th) {
        trace(str, th);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void trace(G78 g78, String str, Object... objArr) {
        trace(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void trace(String str);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void trace(String str, Object obj);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void trace(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void trace(String str, Throwable th);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void trace(String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.O78
    public void warn(G78 g78, String str) {
        warn(str);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void warn(G78 g78, String str, Object obj) {
        warn(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void warn(G78 g78, String str, Object obj, Object obj2) {
        warn(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void warn(G78 g78, String str, Throwable th) {
        warn(str, th);
    }

    @DexIgnore
    @Override // com.fossil.O78
    public void warn(G78 g78, String str, Object... objArr) {
        warn(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void warn(String str);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void warn(String str, Object obj);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void warn(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.O78, com.fossil.E78
    public abstract /* synthetic */ void warn(String str, Throwable th);

    @DexIgnore
    @Override // com.fossil.O78
    public abstract /* synthetic */ void warn(String str, Object... objArr);
}
