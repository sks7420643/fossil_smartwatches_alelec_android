package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.Dl7;
import com.mapped.Rc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iy7 {
    /*
    static {
        Object r0;
        try {
            Dl7.Ai ai = Dl7.Companion;
            r0 = Dl7.constructor-impl(new Gy7(a(Looper.getMainLooper(), true), "Main"));
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            r0 = Dl7.constructor-impl(El7.a(th));
        }
        if (Dl7.isFailure-impl(r0)) {
            r0 = null;
        }
        Hy7 hy7 = (Hy7) r0;
    }
    */

    @DexIgnore
    public static final Handler a(Looper looper, boolean z) {
        int i;
        if (!z || (i = Build.VERSION.SDK_INT) < 16) {
            return new Handler(looper);
        }
        if (i >= 28) {
            Object invoke = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, looper);
            if (invoke != null) {
                return (Handler) invoke;
            }
            throw new Rc6("null cannot be cast to non-null type android.os.Handler");
        }
        try {
            return (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
        } catch (NoSuchMethodException e) {
            return new Handler(looper);
        }
    }
}
