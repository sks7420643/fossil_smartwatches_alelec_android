package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U48 implements C58 {
    @DexIgnore
    public /* final */ I48 b;
    @DexIgnore
    public X48 c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f;
    @DexIgnore
    public /* final */ K48 g;

    @DexIgnore
    public U48(K48 k48) {
        Wg6.c(k48, "upstream");
        this.g = k48;
        I48 t = k48.t();
        this.b = t;
        X48 x48 = t.b;
        this.c = x48;
        this.d = x48 != null ? x48.b : -1;
    }

    @DexIgnore
    @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.e = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        if (r2 == r6.b) goto L_0x0029;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0076  */
    @Override // com.fossil.C58
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long d0(com.fossil.I48 r9, long r10) {
        /*
            r8 = this;
            r4 = 0
            r7 = 0
            r1 = 1
            r0 = 0
            java.lang.String r2 = "sink"
            com.mapped.Wg6.c(r9, r2)
            int r3 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r3 < 0) goto L_0x002f
            r2 = r1
        L_0x000f:
            if (r2 == 0) goto L_0x008e
            boolean r2 = r8.e
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x0082
            com.fossil.X48 r2 = r8.c
            if (r2 == 0) goto L_0x0029
            com.fossil.I48 r6 = r8.b
            com.fossil.X48 r6 = r6.b
            if (r2 != r6) goto L_0x002a
            int r2 = r8.d
            if (r6 == 0) goto L_0x0031
            int r6 = r6.b
            if (r2 != r6) goto L_0x002a
        L_0x0029:
            r0 = r1
        L_0x002a:
            if (r0 == 0) goto L_0x0076
            if (r3 != 0) goto L_0x0035
        L_0x002e:
            return r4
        L_0x002f:
            r2 = r0
            goto L_0x000f
        L_0x0031:
            com.mapped.Wg6.i()
            throw r7
        L_0x0035:
            com.fossil.K48 r0 = r8.g
            long r2 = r8.f
            r4 = 1
            long r2 = r2 + r4
            boolean r0 = r0.R(r2)
            if (r0 != 0) goto L_0x0045
            r4 = -1
            goto L_0x002e
        L_0x0045:
            com.fossil.X48 r0 = r8.c
            if (r0 != 0) goto L_0x0057
            com.fossil.I48 r0 = r8.b
            com.fossil.X48 r0 = r0.b
            if (r0 == 0) goto L_0x0057
            r8.c = r0
            if (r0 == 0) goto L_0x0072
            int r0 = r0.b
            r8.d = r0
        L_0x0057:
            com.fossil.I48 r0 = r8.b
            long r0 = r0.p0()
            long r2 = r8.f
            long r0 = r0 - r2
            long r4 = java.lang.Math.min(r10, r0)
            com.fossil.I48 r0 = r8.b
            long r2 = r8.f
            r1 = r9
            r0.C(r1, r2, r4)
            long r0 = r8.f
            long r0 = r0 + r4
            r8.f = r0
            goto L_0x002e
        L_0x0072:
            com.mapped.Wg6.i()
            throw r7
        L_0x0076:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Peek source is invalid because upstream source was used"
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0082:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "closed"
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x008e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "byteCount < 0: "
            r0.append(r1)
            r0.append(r10)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U48.d0(com.fossil.I48, long):long");
    }

    @DexIgnore
    @Override // com.fossil.C58
    public D58 e() {
        return this.g.e();
    }
}
