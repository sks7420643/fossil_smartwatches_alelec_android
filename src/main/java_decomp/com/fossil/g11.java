package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"AddedAbstractMethod"})
public abstract class G11 {
    @DexIgnore
    public static G11 e(Context context) {
        return S11.l(context);
    }

    @DexIgnore
    public static void f(Context context, O01 o01) {
        S11.f(context, o01);
    }

    @DexIgnore
    public final A11 a(H11 h11) {
        return b(Collections.singletonList(h11));
    }

    @DexIgnore
    public abstract A11 b(List<? extends H11> list);

    @DexIgnore
    public A11 c(String str, S01 s01, Z01 z01) {
        return d(str, s01, Collections.singletonList(z01));
    }

    @DexIgnore
    public abstract A11 d(String str, S01 s01, List<Z01> list);
}
