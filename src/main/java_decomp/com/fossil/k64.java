package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class K64 extends Exception {
    @DexIgnore
    @Deprecated
    public K64() {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public K64(String str) {
        super(str);
        Rc2.h(str, "Detail message must not be empty");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public K64(String str, Throwable th) {
        super(str, th);
        Rc2.h(str, "Detail message must not be empty");
    }
}
