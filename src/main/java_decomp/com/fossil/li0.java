package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Li0 {
    @DexIgnore
    public /* final */ Rd0 a;

    @DexIgnore
    public Li0(Rd0 rd0) {
        this.a = rd0;
    }

    @DexIgnore
    public IBinder a() {
        return this.a.asBinder();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Li0)) {
            return false;
        }
        return ((Li0) obj).a().equals(this.a.asBinder());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }
}
