package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.hq4;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListViewModel;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma7 extends qv5 {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public po4 h;
    @DexIgnore
    public WatchFaceListViewModel i;
    @DexIgnore
    public oa7 j;
    @DexIgnore
    public tg5 k;
    @DexIgnore
    public String l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ma7 a(String str, String str2, boolean z) {
            pq7.c(str, "watchFaceId");
            pq7.c(str2, "type");
            ma7 ma7 = new ma7();
            ma7.setArguments(nm0.a(new cl7("WATCH_FACE_ID", str), new cl7("WATCH_FACE_ID_TYPE", str2), new cl7("sharing_flow_extra", Boolean.valueOf(z))));
            return ma7;
        }

        @DexIgnore
        public final ma7 b(String str, String str2) {
            pq7.c(str, "id");
            pq7.c(str2, "token");
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCH_FACE_SHARED_ID", str);
            bundle.putString("EXTRA_WATCH_FACE_SHARED_TOKEN", str2);
            bundle.putBoolean("sharing_flow_extra", true);
            ma7 ma7 = new ma7();
            ma7.setArguments(bundle);
            return ma7;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2336a;

        @DexIgnore
        public b(ma7 ma7) {
            this.f2336a = ma7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            boolean z = true;
            int i = 0;
            int intValue = t.intValue();
            FLogger.INSTANCE.getLocal().d("WatchFaceListFragment", "watchFaceListSize=" + intValue);
            boolean z2 = intValue != 0;
            tg5 tg5 = this.f2336a.k;
            if (tg5 != null) {
                ViewPager2 viewPager2 = tg5.j;
                pq7.b(viewPager2, "mBinding!!.viewPager");
                int currentItem = viewPager2.getCurrentItem();
                tg5 tg52 = this.f2336a.k;
                if (tg52 != null) {
                    ViewPager2 viewPager22 = tg52.j;
                    pq7.b(viewPager22, "mBinding!!.viewPager");
                    viewPager22.setVisibility(z2 ? 0 : 8);
                    tg5 tg53 = this.f2336a.k;
                    if (tg53 != null) {
                        RTLImageView rTLImageView = tg53.e;
                        pq7.b(rTLImageView, "mBinding!!.fbPreview");
                        if (!z2 || currentItem != 0) {
                            z = false;
                        }
                        rTLImageView.setVisibility(z ? 0 : 8);
                        tg5 tg54 = this.f2336a.k;
                        if (tg54 != null) {
                            Group group = tg54.c;
                            pq7.b(group, "mBinding!!.emptyGroup");
                            if (!(!z2)) {
                                i = 8;
                            }
                            group.setVisibility(i);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2337a;

        @DexIgnore
        public c(ma7 ma7) {
            this.f2337a = ma7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            Boolean bool = (Boolean) t.a();
            if (bool != null) {
                boolean booleanValue = bool.booleanValue();
                tg5 tg5 = this.f2337a.k;
                if (tg5 != null) {
                    ViewPager2 viewPager2 = tg5.j;
                    pq7.b(viewPager2, "mBinding!!.viewPager");
                    if (viewPager2.getCurrentItem() == 0) {
                        tg5 tg52 = this.f2337a.k;
                        if (tg52 != null) {
                            ViewPager2 viewPager22 = tg52.j;
                            pq7.b(viewPager22, "mBinding!!.viewPager");
                            viewPager22.setUserInputEnabled(booleanValue);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    return;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2338a;

        @DexIgnore
        public d(ma7 ma7) {
            this.f2338a = ma7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceListFragment", "update new id " + ((String) t2));
            this.f2338a.l = t2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2339a;

        @DexIgnore
        public e(ma7 ma7) {
            this.f2339a = ma7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            String str = (String) t.a();
            if (str != null) {
                ma7.O6(this.f2339a).x(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2340a;

        @DexIgnore
        public f(ma7 ma7) {
            this.f2340a = ma7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            String str = (String) t.a();
            if (str != null) {
                ma7.O6(this.f2340a).y(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ma7 b;

        @DexIgnore
        public g(ma7 ma7) {
            this.b = ma7;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ma7 b;

        @DexIgnore
        public h(ma7 ma7) {
            this.b = ma7;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceListFragment", "preview watch face id " + this.b.l);
            if (jn5.c(jn5.b, this.b.getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
                ma7.O6(this.b).E(this.b.l);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ma7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.faces.WatchFaceListFragment$onActivityCreated$12$1", f = "WatchFaceListFragment.kt", l = {160}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object D;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WatchFaceListViewModel O6 = ma7.O6(this.this$0.b);
                    this.L$0 = iv7;
                    this.label = 1;
                    D = O6.D(this);
                    if (D == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    D = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (!((Boolean) D).booleanValue()) {
                    WatchFaceEditActivity.a aVar = WatchFaceEditActivity.A;
                    Context requireContext = this.this$0.b.requireContext();
                    pq7.b(requireContext, "requireContext()");
                    aVar.b(requireContext, this.this$0.b.m);
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public i(ma7 ma7) {
            this.b = ma7;
        }

        @DexIgnore
        public final void onClick(View view) {
            xw7 unused = gu7.d(ds0.a(this.b), null, null, new a(this, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ma7 b;

        @DexIgnore
        public j(ma7 ma7) {
            this.b = ma7;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchFaceGalleryActivity.a aVar = WatchFaceGalleryActivity.B;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<hq4.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2341a;

        @DexIgnore
        public k(ma7 ma7) {
            this.f2341a = ma7;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.c cVar) {
            if (!cVar.a().isEmpty()) {
                ma7 ma7 = this.f2341a;
                Object[] array = cVar.a().toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    ma7.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2342a;

        @DexIgnore
        public l(ma7 ma7) {
            this.f2342a = ma7;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
            if (this.f2342a.isActive()) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                FragmentActivity requireActivity = this.f2342a.requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<hq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2343a;

        @DexIgnore
        public m(ma7 ma7) {
            this.f2343a = ma7;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.a aVar) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f2343a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.B0(childFragmentManager, aVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2344a;

        @DexIgnore
        public n(ma7 ma7) {
            this.f2344a = ma7;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceListFragment", "update new id " + str);
            oa7 N6 = ma7.N6(this.f2344a);
            pq7.b(str, "it");
            N6.t(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2345a;

        @DexIgnore
        public o(ma7 ma7) {
            this.f2345a = ma7;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            HomeActivity.a aVar = HomeActivity.B;
            FragmentActivity requireActivity = this.f2345a.requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            pq7.b(str, "presetId");
            aVar.c(requireActivity, str, this.f2345a.m);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2346a;

        @DexIgnore
        public p(ma7 ma7) {
            this.f2346a = ma7;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar.a()) {
                this.f2346a.b();
            }
            if (bVar.b()) {
                this.f2346a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ma7 f2347a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public q(ma7 ma7) {
            this.f2347a = ma7;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            int i2 = 0;
            super.c(i);
            tg5 tg5 = this.f2347a.k;
            if (tg5 != null) {
                RTLImageView rTLImageView = tg5.e;
                pq7.b(rTLImageView, "mBinding!!.fbPreview");
                if (!(i == 0)) {
                    i2 = 8;
                }
                rTLImageView.setVisibility(i2);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public static final /* synthetic */ oa7 N6(ma7 ma7) {
        oa7 oa7 = ma7.j;
        if (oa7 != null) {
            return oa7;
        }
        pq7.n("mSharedViewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceListViewModel O6(ma7 ma7) {
        WatchFaceListViewModel watchFaceListViewModel = ma7.i;
        if (watchFaceListViewModel != null) {
            return watchFaceListViewModel;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceListFragment";
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        String str;
        String str2;
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().q0().b(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(WatchFaceListViewModel.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026istViewModel::class.java)");
            this.i = (WatchFaceListViewModel) a2;
            FragmentActivity requireActivity = requireActivity();
            if (requireActivity != null) {
                this.j = ((WatchFaceListActivity) requireActivity).M();
                Bundle arguments = getArguments();
                if (arguments != null) {
                    String string = arguments.getString("EXTRA_WATCH_FACE_SHARED_ID");
                    if (string == null) {
                        string = "";
                    }
                    pq7.b(string, "it.getString(EXTRA_WATCH_FACE_SHARED_ID) ?: \"\"");
                    String string2 = arguments.getString("EXTRA_WATCH_FACE_SHARED_TOKEN");
                    if (string2 == null) {
                        string2 = "";
                    }
                    pq7.b(string2, "it.getString(EXTRA_WATCH_FACE_SHARED_TOKEN) ?: \"\"");
                    this.m = arguments.getBoolean("sharing_flow_extra");
                    WatchFaceListViewModel watchFaceListViewModel = this.i;
                    if (watchFaceListViewModel != null) {
                        watchFaceListViewModel.z(string, string2);
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceListFragment", "onActivityCreated sharingFlow " + this.m);
                Bundle arguments2 = getArguments();
                if (arguments2 == null || (str = arguments2.getString("WATCH_FACE_ID")) == null) {
                    str = "";
                }
                pq7.b(str, "arguments?.getString(Con\u2026ants.WATCH_FACE_ID) ?: \"\"");
                Bundle arguments3 = getArguments();
                if (arguments3 == null || (str2 = arguments3.getString("WATCH_FACE_ID_TYPE")) == null) {
                    str2 = "";
                }
                pq7.b(str2, "arguments?.getString(Con\u2026WATCH_FACE_ID_TYPE) ?: \"\"");
                va7 va7 = new va7(this, str, str2);
                q qVar = new q(this);
                tg5 tg5 = this.k;
                if (tg5 != null) {
                    ViewPager2 viewPager2 = tg5.j;
                    viewPager2.setOverScrollMode(2);
                    viewPager2.setUserInputEnabled(false);
                    viewPager2.setAdapter(va7);
                    viewPager2.g(qVar);
                    WatchFaceListViewModel watchFaceListViewModel2 = this.i;
                    if (watchFaceListViewModel2 != null) {
                        LiveData<Integer> C = watchFaceListViewModel2.C();
                        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
                        pq7.b(viewLifecycleOwner, "viewLifecycleOwner");
                        C.h(viewLifecycleOwner, new b(this));
                        WatchFaceListViewModel watchFaceListViewModel3 = this.i;
                        if (watchFaceListViewModel3 != null) {
                            watchFaceListViewModel3.l().h(getViewLifecycleOwner(), new k(this));
                            WatchFaceListViewModel watchFaceListViewModel4 = this.i;
                            if (watchFaceListViewModel4 != null) {
                                watchFaceListViewModel4.m().h(getViewLifecycleOwner(), new l(this));
                                WatchFaceListViewModel watchFaceListViewModel5 = this.i;
                                if (watchFaceListViewModel5 != null) {
                                    watchFaceListViewModel5.h().h(getViewLifecycleOwner(), new m(this));
                                    WatchFaceListViewModel watchFaceListViewModel6 = this.i;
                                    if (watchFaceListViewModel6 != null) {
                                        watchFaceListViewModel6.B().h(getViewLifecycleOwner(), new n(this));
                                        WatchFaceListViewModel watchFaceListViewModel7 = this.i;
                                        if (watchFaceListViewModel7 != null) {
                                            watchFaceListViewModel7.A().h(getViewLifecycleOwner(), new o(this));
                                            WatchFaceListViewModel watchFaceListViewModel8 = this.i;
                                            if (watchFaceListViewModel8 != null) {
                                                watchFaceListViewModel8.j().h(getViewLifecycleOwner(), new p(this));
                                                tg5 tg52 = this.k;
                                                if (tg52 != null) {
                                                    tg52.g.setOnClickListener(new g(this));
                                                    tg5 tg53 = this.k;
                                                    if (tg53 != null) {
                                                        tg53.e.setOnClickListener(new h(this));
                                                        tg5 tg54 = this.k;
                                                        if (tg54 != null) {
                                                            tg54.d.setOnClickListener(new i(this));
                                                            tg5 tg55 = this.k;
                                                            if (tg55 != null) {
                                                                tg55.b.setOnClickListener(new j(this));
                                                                oa7 oa7 = this.j;
                                                                if (oa7 != null) {
                                                                    LiveData<u37<Boolean>> s2 = oa7.s();
                                                                    LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                                                                    pq7.b(viewLifecycleOwner2, "viewLifecycleOwner");
                                                                    s2.h(viewLifecycleOwner2, new c(this));
                                                                    oa7 oa72 = this.j;
                                                                    if (oa72 != null) {
                                                                        LiveData<String> q2 = oa72.q();
                                                                        LifecycleOwner viewLifecycleOwner3 = getViewLifecycleOwner();
                                                                        pq7.b(viewLifecycleOwner3, "viewLifecycleOwner");
                                                                        q2.h(viewLifecycleOwner3, new d(this));
                                                                        oa7 oa73 = this.j;
                                                                        if (oa73 != null) {
                                                                            LiveData<u37<String>> p2 = oa73.p();
                                                                            LifecycleOwner viewLifecycleOwner4 = getViewLifecycleOwner();
                                                                            pq7.b(viewLifecycleOwner4, "viewLifecycleOwner");
                                                                            p2.h(viewLifecycleOwner4, new e(this));
                                                                            oa7 oa74 = this.j;
                                                                            if (oa74 != null) {
                                                                                LiveData<u37<String>> r = oa74.r();
                                                                                LifecycleOwner viewLifecycleOwner5 = getViewLifecycleOwner();
                                                                                pq7.b(viewLifecycleOwner5, "viewLifecycleOwner");
                                                                                r.h(viewLifecycleOwner5, new f(this));
                                                                                return;
                                                                            }
                                                                            pq7.n("mSharedViewModel");
                                                                            throw null;
                                                                        }
                                                                        pq7.n("mSharedViewModel");
                                                                        throw null;
                                                                    }
                                                                    pq7.n("mSharedViewModel");
                                                                    throw null;
                                                                }
                                                                pq7.n("mSharedViewModel");
                                                                throw null;
                                                            }
                                                            pq7.i();
                                                            throw null;
                                                        }
                                                        pq7.i();
                                                        throw null;
                                                    }
                                                    pq7.i();
                                                    throw null;
                                                }
                                                pq7.i();
                                                throw null;
                                            }
                                            pq7.n("mViewModel");
                                            throw null;
                                        }
                                        pq7.n("mViewModel");
                                        throw null;
                                    }
                                    pq7.n("mViewModel");
                                    throw null;
                                }
                                pq7.n("mViewModel");
                                throw null;
                            }
                            pq7.n("mViewModel");
                            throw null;
                        }
                        pq7.n("mViewModel");
                        throw null;
                    }
                    pq7.n("mViewModel");
                    throw null;
                }
                pq7.i();
                throw null;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.faces.WatchFaceListActivity");
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        tg5 c2 = tg5.c(layoutInflater);
        this.k = c2;
        if (c2 != null) {
            ConstraintLayout b2 = c2.b();
            pq7.b(b2, "mBinding!!.root");
            return b2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
