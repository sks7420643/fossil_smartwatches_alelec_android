package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qh6 {
    @DexIgnore
    public /* final */ Kh6 a;
    @DexIgnore
    public /* final */ Bi6 b;
    @DexIgnore
    public /* final */ Vh6 c;

    @DexIgnore
    public Qh6(Kh6 kh6, Bi6 bi6, Vh6 vh6) {
        Wg6.c(kh6, "mGoalTrackingOverviewDayView");
        Wg6.c(bi6, "mGoalTrackingOverviewWeekView");
        Wg6.c(vh6, "mGoalTrackingOverviewMonthView");
        this.a = kh6;
        this.b = bi6;
        this.c = vh6;
    }

    @DexIgnore
    public final Kh6 a() {
        return this.a;
    }

    @DexIgnore
    public final Vh6 b() {
        return this.c;
    }

    @DexIgnore
    public final Bi6 c() {
        return this.b;
    }
}
