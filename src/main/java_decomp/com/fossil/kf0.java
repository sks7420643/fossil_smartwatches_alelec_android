package com.fossil;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.StateSet;
import com.fossil.If0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public class Kf0 extends If0 {
    @DexIgnore
    public Ai s;
    @DexIgnore
    public boolean t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends If0.Ci {
        @DexIgnore
        public int[][] J;

        @DexIgnore
        public Ai(Ai ai, Kf0 kf0, Resources resources) {
            super(ai, kf0, resources);
            if (ai != null) {
                this.J = ai.J;
            } else {
                this.J = new int[f()][];
            }
        }

        @DexIgnore
        public int A(int[] iArr) {
            int[][] iArr2 = this.J;
            int h = h();
            for (int i = 0; i < h; i++) {
                if (StateSet.stateSetMatches(iArr2[i], iArr)) {
                    return i;
                }
            }
            return -1;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new Kf0(this, null);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new Kf0(this, resources);
        }

        @DexIgnore
        @Override // com.fossil.If0.Ci
        public void o(int i, int i2) {
            super.o(i, i2);
            int[][] iArr = new int[i2][];
            System.arraycopy(this.J, 0, iArr, 0, i);
            this.J = iArr;
        }

        @DexIgnore
        @Override // com.fossil.If0.Ci
        public void r() {
            int[][] iArr = this.J;
            int[][] iArr2 = new int[iArr.length][];
            for (int length = iArr.length - 1; length >= 0; length--) {
                int[][] iArr3 = this.J;
                iArr2[length] = iArr3[length] != null ? (int[]) iArr3[length].clone() : null;
            }
            this.J = iArr2;
        }

        @DexIgnore
        public int z(int[] iArr, Drawable drawable) {
            int a2 = a(drawable);
            this.J[a2] = iArr;
            return a2;
        }
    }

    @DexIgnore
    public Kf0(Ai ai) {
        if (ai != null) {
            h(ai);
        }
    }

    @DexIgnore
    public Kf0(Ai ai, Resources resources) {
        h(new Ai(ai, this, resources));
        onStateChange(getState());
    }

    @DexIgnore
    @Override // com.fossil.If0
    public void applyTheme(Resources.Theme theme) {
        super.applyTheme(theme);
        onStateChange(getState());
    }

    @DexIgnore
    @Override // com.fossil.If0
    public /* bridge */ /* synthetic */ If0.Ci b() {
        return j();
    }

    @DexIgnore
    @Override // com.fossil.If0
    public void h(If0.Ci ci) {
        super.h(ci);
        if (ci instanceof Ai) {
            this.s = (Ai) ci;
        }
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public Ai j() {
        return new Ai(this.s, this, null);
    }

    @DexIgnore
    public int[] k(AttributeSet attributeSet) {
        int i;
        int attributeCount = attributeSet.getAttributeCount();
        int[] iArr = new int[attributeCount];
        int i2 = 0;
        int i3 = 0;
        while (i3 < attributeCount) {
            int attributeNameResource = attributeSet.getAttributeNameResource(i3);
            if (attributeNameResource == 0 || attributeNameResource == 16842960 || attributeNameResource == 16843161) {
                i = i2;
            } else {
                if (!attributeSet.getAttributeBooleanValue(i3, false)) {
                    attributeNameResource = -attributeNameResource;
                }
                iArr[i2] = attributeNameResource;
                i = i2 + 1;
            }
            i3++;
            i2 = i;
        }
        return StateSet.trimStateSet(iArr, i2);
    }

    @DexIgnore
    @Override // com.fossil.If0
    public Drawable mutate() {
        if (!this.t) {
            super.mutate();
            this.s.r();
            this.t = true;
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.If0
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        int A = this.s.A(iArr);
        if (A < 0) {
            A = this.s.A(StateSet.WILD_CARD);
        }
        return g(A) || onStateChange;
    }
}
