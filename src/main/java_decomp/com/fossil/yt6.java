package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yt6 {
    @DexIgnore
    public /* final */ Eu6 a;

    @DexIgnore
    public Yt6(Eu6 eu6) {
        Wg6.c(eu6, "view");
        I14.o(eu6, "view cannot be null!", new Object[0]);
        Wg6.b(eu6, "checkNotNull(view, \"view cannot be null!\")");
        this.a = eu6;
    }

    @DexIgnore
    public final Eu6 a() {
        return this.a;
    }
}
