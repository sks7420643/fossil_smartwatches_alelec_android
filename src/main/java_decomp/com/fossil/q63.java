package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q63 implements Xw2<P63> {
    @DexIgnore
    public static Q63 c; // = new Q63();
    @DexIgnore
    public /* final */ Xw2<P63> b;

    @DexIgnore
    public Q63() {
        this(Ww2.b(new S63()));
    }

    @DexIgnore
    public Q63(Xw2<P63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((P63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((P63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ P63 zza() {
        return this.b.zza();
    }
}
