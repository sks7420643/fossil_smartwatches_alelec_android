package com.fossil;

import com.fossil.E14;
import com.fossil.V34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U34 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public V34.Pi d;
    @DexIgnore
    public V34.Pi e;
    @DexIgnore
    public Z04<Object> f;

    @DexIgnore
    @CanIgnoreReturnValue
    public U34 a(int i) {
        boolean z = true;
        I14.u(this.c == -1, "concurrency level was already set to %s", this.c);
        if (i <= 0) {
            z = false;
        }
        I14.d(z);
        this.c = i;
        return this;
    }

    @DexIgnore
    public int b() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    @DexIgnore
    public int c() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    @DexIgnore
    public Z04<Object> d() {
        return (Z04) E14.a(this.f, e().defaultEquivalence());
    }

    @DexIgnore
    public V34.Pi e() {
        return (V34.Pi) E14.a(this.d, V34.Pi.STRONG);
    }

    @DexIgnore
    public V34.Pi f() {
        return (V34.Pi) E14.a(this.e, V34.Pi.STRONG);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public U34 g(int i) {
        boolean z = true;
        I14.u(this.b == -1, "initial capacity was already set to %s", this.b);
        if (i < 0) {
            z = false;
        }
        I14.d(z);
        this.b = i;
        return this;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public U34 h(Z04<Object> z04) {
        I14.v(this.f == null, "key equivalence was already set to %s", this.f);
        I14.l(z04);
        this.f = z04;
        this.a = true;
        return this;
    }

    @DexIgnore
    public <K, V> ConcurrentMap<K, V> i() {
        return !this.a ? new ConcurrentHashMap(c(), 0.75f, b()) : V34.create(this);
    }

    @DexIgnore
    public U34 j(V34.Pi pi) {
        I14.v(this.d == null, "Key strength was already set to %s", this.d);
        I14.l(pi);
        this.d = pi;
        if (pi != V34.Pi.STRONG) {
            this.a = true;
        }
        return this;
    }

    @DexIgnore
    public U34 k(V34.Pi pi) {
        I14.v(this.e == null, "Value strength was already set to %s", this.e);
        I14.l(pi);
        this.e = pi;
        if (pi != V34.Pi.STRONG) {
            this.a = true;
        }
        return this;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public U34 l() {
        j(V34.Pi.WEAK);
        return this;
    }

    @DexIgnore
    public String toString() {
        E14.Bi b2 = E14.b(this);
        int i = this.b;
        if (i != -1) {
            b2.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            b2.a("concurrencyLevel", i2);
        }
        V34.Pi pi = this.d;
        if (pi != null) {
            b2.b("keyStrength", Y04.b(pi.toString()));
        }
        V34.Pi pi2 = this.e;
        if (pi2 != null) {
            b2.b("valueStrength", Y04.b(pi2.toString()));
        }
        if (this.f != null) {
            b2.f("keyEquivalence");
        }
        return b2.toString();
    }
}
