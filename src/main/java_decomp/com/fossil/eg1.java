package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eg1 implements ImageHeaderParser {
    @DexIgnore
    public static /* final */ byte[] a; // = "Exif\u0000\u0000".getBytes(Charset.forName("UTF-8"));
    @DexIgnore
    public static /* final */ int[] b; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ci {
        @DexIgnore
        public /* final */ ByteBuffer a;

        @DexIgnore
        public Ai(ByteBuffer byteBuffer) {
            this.a = byteBuffer;
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public int a() throws Ci.Aii {
            return (c() << 8) | c();
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public int b(byte[] bArr, int i) {
            int min = Math.min(i, this.a.remaining());
            if (min == 0) {
                return -1;
            }
            this.a.get(bArr, 0, min);
            return min;
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public short c() throws Ci.Aii {
            if (this.a.remaining() >= 1) {
                return (short) (this.a.get() & 255);
            }
            throw new Ci.Aii();
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public long skip(long j) {
            int min = (int) Math.min((long) this.a.remaining(), j);
            ByteBuffer byteBuffer = this.a;
            byteBuffer.position(byteBuffer.position() + min);
            return (long) min;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ ByteBuffer a;

        @DexIgnore
        public Bi(byte[] bArr, int i) {
            this.a = (ByteBuffer) ByteBuffer.wrap(bArr).order(ByteOrder.BIG_ENDIAN).limit(i);
        }

        @DexIgnore
        public short a(int i) {
            if (c(i, 2)) {
                return this.a.getShort(i);
            }
            return -1;
        }

        @DexIgnore
        public int b(int i) {
            if (c(i, 4)) {
                return this.a.getInt(i);
            }
            return -1;
        }

        @DexIgnore
        public final boolean c(int i, int i2) {
            return this.a.remaining() - i >= i2;
        }

        @DexIgnore
        public int d() {
            return this.a.remaining();
        }

        @DexIgnore
        public void e(ByteOrder byteOrder) {
            this.a.order(byteOrder);
        }
    }

    @DexIgnore
    public interface Ci {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends IOException {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 1;

            @DexIgnore
            public Aii() {
                super("Unexpectedly reached end of a file");
            }
        }

        @DexIgnore
        int a() throws IOException;

        @DexIgnore
        int b(byte[] bArr, int i) throws IOException;

        @DexIgnore
        short c() throws IOException;

        @DexIgnore
        long skip(long j) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Ci {
        @DexIgnore
        public /* final */ InputStream a;

        @DexIgnore
        public Di(InputStream inputStream) {
            this.a = inputStream;
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public int a() throws IOException {
            return (c() << 8) | c();
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public int b(byte[] bArr, int i) throws IOException {
            int i2 = 0;
            int i3 = 0;
            while (i3 < i) {
                i2 = this.a.read(bArr, i3, i - i3);
                if (i2 == -1) {
                    break;
                }
                i3 += i2;
            }
            if (i3 != 0 || i2 != -1) {
                return i3;
            }
            throw new Ci.Aii();
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public short c() throws IOException {
            int read = this.a.read();
            if (read != -1) {
                return (short) read;
            }
            throw new Ci.Aii();
        }

        @DexIgnore
        @Override // com.fossil.Eg1.Ci
        public long skip(long j) throws IOException {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.a.skip(j2);
                if (skip <= 0) {
                    if (this.a.read() == -1) {
                        break;
                    }
                    skip = 1;
                }
                j2 -= skip;
            }
            return j - j2;
        }
    }

    @DexIgnore
    public static int d(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    @DexIgnore
    public static boolean g(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    @DexIgnore
    public static int j(Bi bi) {
        ByteOrder byteOrder;
        short a2 = bi.a(6);
        if (a2 == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else if (a2 != 19789) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unknown endianness = " + ((int) a2));
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else {
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        bi.e(byteOrder);
        int b2 = bi.b(10) + 6;
        short a3 = bi.a(b2);
        for (int i = 0; i < a3; i++) {
            int d = d(b2, i);
            short a4 = bi.a(d);
            if (a4 == 274) {
                short a5 = bi.a(d + 2);
                if (a5 >= 1 && a5 <= 12) {
                    int b3 = bi.b(d + 4);
                    if (b3 >= 0) {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got tagIndex=" + i + " tagType=" + ((int) a4) + " formatCode=" + ((int) a5) + " componentCount=" + b3);
                        }
                        int i2 = b3 + b[a5];
                        if (i2 <= 4) {
                            int i3 = d + 8;
                            if (i3 < 0 || i3 > bi.d()) {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + ((int) a4));
                                }
                            } else if (i2 >= 0 && i2 + i3 <= bi.d()) {
                                return bi.a(i3);
                            } else {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + ((int) a4));
                                }
                            }
                        } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + ((int) a5));
                        }
                    } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Got invalid format code = " + ((int) a5));
                }
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public ImageHeaderParser.ImageType a(ByteBuffer byteBuffer) throws IOException {
        Ik1.d(byteBuffer);
        return f(new Ai(byteBuffer));
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public ImageHeaderParser.ImageType b(InputStream inputStream) throws IOException {
        Ik1.d(inputStream);
        return f(new Di(inputStream));
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public int c(InputStream inputStream, Od1 od1) throws IOException {
        Ik1.d(inputStream);
        Di di = new Di(inputStream);
        Ik1.d(od1);
        return e(di, od1);
    }

    @DexIgnore
    public final int e(Ci ci, Od1 od1) throws IOException {
        try {
            int a2 = ci.a();
            if (!g(a2)) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + a2);
                }
                return -1;
            }
            int i = i(ci);
            if (i == -1) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
                }
                return -1;
            }
            byte[] bArr = (byte[]) od1.g(i, byte[].class);
            try {
                return k(ci, bArr, i);
            } finally {
                od1.f(bArr);
            }
        } catch (Ci.Aii e) {
            return -1;
        }
    }

    @DexIgnore
    public final ImageHeaderParser.ImageType f(Ci ci) throws IOException {
        try {
            int a2 = ci.a();
            if (a2 == 65496) {
                return ImageHeaderParser.ImageType.JPEG;
            }
            int c = (a2 << 8) | ci.c();
            if (c == 4671814) {
                return ImageHeaderParser.ImageType.GIF;
            }
            int c2 = (c << 8) | ci.c();
            if (c2 == -1991225785) {
                ci.skip(21);
                try {
                    return ci.c() >= 3 ? ImageHeaderParser.ImageType.PNG_A : ImageHeaderParser.ImageType.PNG;
                } catch (Ci.Aii e) {
                    return ImageHeaderParser.ImageType.PNG;
                }
            } else if (c2 != 1380533830) {
                return ImageHeaderParser.ImageType.UNKNOWN;
            } else {
                ci.skip(4);
                if (((ci.a() << 16) | ci.a()) != 1464156752) {
                    return ImageHeaderParser.ImageType.UNKNOWN;
                }
                int a3 = (ci.a() << 16) | ci.a();
                if ((a3 & -256) != 1448097792) {
                    return ImageHeaderParser.ImageType.UNKNOWN;
                }
                int i = a3 & 255;
                if (i == 88) {
                    ci.skip(4);
                    return (ci.c() & 16) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
                } else if (i != 76) {
                    return ImageHeaderParser.ImageType.WEBP;
                } else {
                    ci.skip(4);
                    return (ci.c() & 8) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
                }
            }
        } catch (Ci.Aii e2) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
    }

    @DexIgnore
    public final boolean h(byte[] bArr, int i) {
        boolean z = bArr != null && i > a.length;
        if (z) {
            int i2 = 0;
            while (true) {
                byte[] bArr2 = a;
                if (i2 >= bArr2.length) {
                    break;
                } else if (bArr[i2] != bArr2[i2]) {
                    return false;
                } else {
                    i2++;
                }
            }
        }
        return z;
    }

    @DexIgnore
    public final int i(Ci ci) throws IOException {
        short c;
        int a2;
        long j;
        long skip;
        do {
            short c2 = ci.c();
            if (c2 == 255) {
                c = ci.c();
                if (c == 218) {
                    return -1;
                }
                if (c != 217) {
                    a2 = ci.a() - 2;
                    if (c == 225) {
                        return a2;
                    }
                    j = (long) a2;
                    skip = ci.skip(j);
                } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                    return -1;
                } else {
                    Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                    return -1;
                }
            } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            } else {
                Log.d("DfltImageHeaderParser", "Unknown segmentId=" + ((int) c2));
                return -1;
            }
        } while (skip == j);
        if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
            return -1;
        }
        Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + ((int) c) + ", wanted to skip: " + a2 + ", but actually skipped: " + skip);
        return -1;
    }

    @DexIgnore
    public final int k(Ci ci, byte[] bArr, int i) throws IOException {
        int b2 = ci.b(bArr, i);
        if (b2 != i) {
            if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            }
            Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + b2);
            return -1;
        } else if (h(bArr, i)) {
            return j(new Bi(bArr, i));
        } else {
            if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            }
            Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            return -1;
        }
    }
}
