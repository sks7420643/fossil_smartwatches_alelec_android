package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Em1 extends Hv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ HashSet<Dm1> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Em1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Em1 a(Parcel parcel) {
            return new Em1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Em1 createFromParcel(Parcel parcel) {
            return new Em1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Em1[] newArray(int i) {
            return new Em1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Em1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.b = new HashSet<>();
        ArrayList createTypedArrayList = parcel.createTypedArrayList(Dm1.CREATOR);
        if (createTypedArrayList != null) {
            this.b.addAll(createTypedArrayList);
        }
    }

    @DexIgnore
    public Em1(Dm1 dm1, Dm1 dm12, Dm1 dm13, Dm1 dm14) {
        this.b = new HashSet<>();
        dm1.a(new Dt1(0, 62));
        dm12.a(new Dt1(90, 62));
        dm13.a(new Dt1(180, 62));
        dm14.a(new Dt1(270, 62));
        this.b.add(dm1);
        this.b.add(dm12);
        this.b.add(dm13);
        this.b.add(dm14);
    }

    @DexIgnore
    public Em1(Dm1[] dm1Arr) {
        HashSet<Dm1> hashSet = new HashSet<>();
        this.b = hashSet;
        Mm7.t(hashSet, dm1Arr);
    }

    @DexIgnore
    @Override // com.fossil.Hv1
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = this.b.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().toJSONObject());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.comps", jSONArray);
        Wg6.b(put, "JSONObject().put(UIScrip\u2026ationAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final HashSet<Dm1> c() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Em1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.b, ((Em1) obj).b) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.ComplicationConfig");
    }

    @DexIgnore
    public final Dm1 getBottomFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 180 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Dm1 getLeftFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 270 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Dm1 getRightFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 90 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Dm1 getTopFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    @Override // com.fossil.Hv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(Pm7.h0(this.b));
        }
    }
}
