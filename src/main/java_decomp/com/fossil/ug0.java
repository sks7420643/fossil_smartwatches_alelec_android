package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import com.fossil.Jh0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ug0 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode b; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static Ug0 c;
    @DexIgnore
    public Jh0 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Jh0.Ei {
        @DexIgnore
        public /* final */ int[] a; // = {Pe0.abc_textfield_search_default_mtrl_alpha, Pe0.abc_textfield_default_mtrl_alpha, Pe0.abc_ab_share_pack_mtrl_alpha};
        @DexIgnore
        public /* final */ int[] b; // = {Pe0.abc_ic_commit_search_api_mtrl_alpha, Pe0.abc_seekbar_tick_mark_material, Pe0.abc_ic_menu_share_mtrl_alpha, Pe0.abc_ic_menu_copy_mtrl_am_alpha, Pe0.abc_ic_menu_cut_mtrl_alpha, Pe0.abc_ic_menu_selectall_mtrl_alpha, Pe0.abc_ic_menu_paste_mtrl_am_alpha};
        @DexIgnore
        public /* final */ int[] c; // = {Pe0.abc_textfield_activated_mtrl_alpha, Pe0.abc_textfield_search_activated_mtrl_alpha, Pe0.abc_cab_background_top_mtrl_alpha, Pe0.abc_text_cursor_material, Pe0.abc_text_select_handle_left_mtrl_dark, Pe0.abc_text_select_handle_middle_mtrl_dark, Pe0.abc_text_select_handle_right_mtrl_dark, Pe0.abc_text_select_handle_left_mtrl_light, Pe0.abc_text_select_handle_middle_mtrl_light, Pe0.abc_text_select_handle_right_mtrl_light};
        @DexIgnore
        public /* final */ int[] d; // = {Pe0.abc_popup_background_mtrl_mult, Pe0.abc_cab_background_internal_bg, Pe0.abc_menu_hardkey_panel_mtrl_mult};
        @DexIgnore
        public /* final */ int[] e; // = {Pe0.abc_tab_indicator_material, Pe0.abc_textfield_search_material};
        @DexIgnore
        public /* final */ int[] f; // = {Pe0.abc_btn_check_material, Pe0.abc_btn_radio_material, Pe0.abc_btn_check_material_anim, Pe0.abc_btn_radio_material_anim};

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0063  */
        /* JADX WARNING: Removed duplicated region for block: B:6:0x0019  */
        @Override // com.fossil.Jh0.Ei
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.content.Context r8, int r9, android.graphics.drawable.Drawable r10) {
            /*
                r7 = this;
                r5 = 1
                r6 = 0
                r3 = -1
                android.graphics.PorterDuff$Mode r0 = com.fossil.Ug0.a()
                int[] r1 = r7.a
                boolean r2 = r7.f(r1, r9)
                r1 = 16842801(0x1010031, float:2.3693695E-38)
                if (r2 == 0) goto L_0x0035
                int r1 = com.fossil.Le0.colorControlNormal
            L_0x0014:
                r2 = r3
                r4 = r1
            L_0x0016:
                r1 = r5
            L_0x0017:
                if (r1 == 0) goto L_0x0063
                boolean r1 = com.fossil.Dh0.a(r10)
                if (r1 == 0) goto L_0x0023
                android.graphics.drawable.Drawable r10 = r10.mutate()
            L_0x0023:
                int r1 = com.fossil.Oh0.c(r8, r4)
                android.graphics.PorterDuffColorFilter r0 = com.fossil.Ug0.e(r1, r0)
                r10.setColorFilter(r0)
                if (r2 == r3) goto L_0x0033
                r10.setAlpha(r2)
            L_0x0033:
                r0 = r5
            L_0x0034:
                return r0
            L_0x0035:
                int[] r2 = r7.c
                boolean r2 = r7.f(r2, r9)
                if (r2 == 0) goto L_0x0040
                int r1 = com.fossil.Le0.colorControlActivated
                goto L_0x0014
            L_0x0040:
                int[] r2 = r7.d
                boolean r2 = r7.f(r2, r9)
                if (r2 == 0) goto L_0x004b
                android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
                goto L_0x0014
            L_0x004b:
                int r2 = com.fossil.Pe0.abc_list_divider_mtrl_alpha
                if (r9 != r2) goto L_0x005b
                r4 = 16842800(0x1010030, float:2.3693693E-38)
                r1 = 1109603123(0x42233333, float:40.8)
                int r1 = java.lang.Math.round(r1)
                r2 = r1
                goto L_0x0016
            L_0x005b:
                int r2 = com.fossil.Pe0.abc_dialog_material_background
                if (r9 == r2) goto L_0x0014
                r1 = r6
                r2 = r3
                r4 = r6
                goto L_0x0017
            L_0x0063:
                r0 = r6
                goto L_0x0034
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ug0.Ai.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
        }

        @DexIgnore
        @Override // com.fossil.Jh0.Ei
        public PorterDuff.Mode b(int i) {
            if (i == Pe0.abc_switch_thumb_material) {
                return PorterDuff.Mode.MULTIPLY;
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.Jh0.Ei
        public Drawable c(Jh0 jh0, Context context, int i) {
            if (i != Pe0.abc_cab_background_top_material) {
                return null;
            }
            return new LayerDrawable(new Drawable[]{jh0.j(context, Pe0.abc_cab_background_internal_bg), jh0.j(context, Pe0.abc_cab_background_top_mtrl_alpha)});
        }

        @DexIgnore
        @Override // com.fossil.Jh0.Ei
        public ColorStateList d(Context context, int i) {
            if (i == Pe0.abc_edit_text_material) {
                return Gf0.c(context, Ne0.abc_tint_edittext);
            }
            if (i == Pe0.abc_switch_track_mtrl_alpha) {
                return Gf0.c(context, Ne0.abc_tint_switch_track);
            }
            if (i == Pe0.abc_switch_thumb_material) {
                return k(context);
            }
            if (i == Pe0.abc_btn_default_mtrl_shape) {
                return j(context);
            }
            if (i == Pe0.abc_btn_borderless_material) {
                return g(context);
            }
            if (i == Pe0.abc_btn_colored_material) {
                return i(context);
            }
            if (i == Pe0.abc_spinner_mtrl_am_alpha || i == Pe0.abc_spinner_textfield_background_material) {
                return Gf0.c(context, Ne0.abc_tint_spinner);
            }
            if (f(this.b, i)) {
                return Oh0.e(context, Le0.colorControlNormal);
            }
            if (f(this.e, i)) {
                return Gf0.c(context, Ne0.abc_tint_default);
            }
            if (f(this.f, i)) {
                return Gf0.c(context, Ne0.abc_tint_btn_checkable);
            }
            if (i == Pe0.abc_seekbar_thumb_material) {
                return Gf0.c(context, Ne0.abc_tint_seek_thumb);
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.Jh0.Ei
        public boolean e(Context context, int i, Drawable drawable) {
            if (i == Pe0.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                l(layerDrawable.findDrawableByLayerId(16908288), Oh0.c(context, Le0.colorControlNormal), Ug0.b);
                l(layerDrawable.findDrawableByLayerId(16908303), Oh0.c(context, Le0.colorControlNormal), Ug0.b);
                l(layerDrawable.findDrawableByLayerId(16908301), Oh0.c(context, Le0.colorControlActivated), Ug0.b);
                return true;
            } else if (i != Pe0.abc_ratingbar_material && i != Pe0.abc_ratingbar_indicator_material && i != Pe0.abc_ratingbar_small_material) {
                return false;
            } else {
                LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                l(layerDrawable2.findDrawableByLayerId(16908288), Oh0.b(context, Le0.colorControlNormal), Ug0.b);
                l(layerDrawable2.findDrawableByLayerId(16908303), Oh0.c(context, Le0.colorControlActivated), Ug0.b);
                l(layerDrawable2.findDrawableByLayerId(16908301), Oh0.c(context, Le0.colorControlActivated), Ug0.b);
                return true;
            }
        }

        @DexIgnore
        public final boolean f(int[] iArr, int i) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final ColorStateList g(Context context) {
            return h(context, 0);
        }

        @DexIgnore
        public final ColorStateList h(Context context, int i) {
            int c2 = Oh0.c(context, Le0.colorControlHighlight);
            int b2 = Oh0.b(context, Le0.colorButtonNormal);
            int[] iArr = Oh0.b;
            int[] iArr2 = Oh0.d;
            int e2 = Pl0.e(c2, i);
            int[] iArr3 = Oh0.c;
            int e3 = Pl0.e(c2, i);
            return new ColorStateList(new int[][]{iArr, iArr2, iArr3, Oh0.f}, new int[]{b2, e2, e3, i});
        }

        @DexIgnore
        public final ColorStateList i(Context context) {
            return h(context, Oh0.c(context, Le0.colorAccent));
        }

        @DexIgnore
        public final ColorStateList j(Context context) {
            return h(context, Oh0.c(context, Le0.colorButtonNormal));
        }

        @DexIgnore
        public final ColorStateList k(Context context) {
            int[][] iArr = new int[3][];
            int[] iArr2 = new int[3];
            ColorStateList e2 = Oh0.e(context, Le0.colorSwitchThumbNormal);
            if (e2 == null || !e2.isStateful()) {
                iArr[0] = Oh0.b;
                iArr2[0] = Oh0.b(context, Le0.colorSwitchThumbNormal);
                iArr[1] = Oh0.e;
                iArr2[1] = Oh0.c(context, Le0.colorControlActivated);
                iArr[2] = Oh0.f;
                iArr2[2] = Oh0.c(context, Le0.colorSwitchThumbNormal);
            } else {
                iArr[0] = Oh0.b;
                iArr2[0] = e2.getColorForState(iArr[0], 0);
                iArr[1] = Oh0.e;
                iArr2[1] = Oh0.c(context, Le0.colorControlActivated);
                iArr[2] = Oh0.f;
                iArr2[2] = e2.getDefaultColor();
            }
            return new ColorStateList(iArr, iArr2);
        }

        @DexIgnore
        public final void l(Drawable drawable, int i, PorterDuff.Mode mode) {
            if (Dh0.a(drawable)) {
                drawable = drawable.mutate();
            }
            if (mode == null) {
                mode = Ug0.b;
            }
            drawable.setColorFilter(Ug0.e(i, mode));
        }
    }

    @DexIgnore
    public static Ug0 b() {
        Ug0 ug0;
        synchronized (Ug0.class) {
            try {
                if (c == null) {
                    h();
                }
                ug0 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ug0;
    }

    @DexIgnore
    public static PorterDuffColorFilter e(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter l;
        synchronized (Ug0.class) {
            try {
                l = Jh0.l(i, mode);
            } catch (Throwable th) {
                throw th;
            }
        }
        return l;
    }

    @DexIgnore
    public static void h() {
        synchronized (Ug0.class) {
            try {
                if (c == null) {
                    Ug0 ug0 = new Ug0();
                    c = ug0;
                    ug0.a = Jh0.h();
                    c.a.u(new Ai());
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static void i(Drawable drawable, Rh0 rh0, int[] iArr) {
        Jh0.w(drawable, rh0, iArr);
    }

    @DexIgnore
    public Drawable c(Context context, int i) {
        Drawable j;
        synchronized (this) {
            j = this.a.j(context, i);
        }
        return j;
    }

    @DexIgnore
    public Drawable d(Context context, int i, boolean z) {
        Drawable k;
        synchronized (this) {
            k = this.a.k(context, i, z);
        }
        return k;
    }

    @DexIgnore
    public ColorStateList f(Context context, int i) {
        ColorStateList m;
        synchronized (this) {
            m = this.a.m(context, i);
        }
        return m;
    }

    @DexIgnore
    public void g(Context context) {
        synchronized (this) {
            this.a.s(context);
        }
    }
}
