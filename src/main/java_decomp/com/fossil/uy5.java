package com.fossil;

import android.graphics.RectF;
import com.fossil.Vy5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uy5 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();
    @DexIgnore
    public /* final */ RectF b; // = new RectF();
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k; // = 1.0f;
    @DexIgnore
    public float l; // = 1.0f;

    @DexIgnore
    public static boolean l(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f6 && f3 > f5 && f3 < f7;
    }

    @DexIgnore
    public static boolean m(float f2, float f3, float f4, float f5, float f6) {
        return Math.abs(f2 - f4) <= f6 && Math.abs(f3 - f5) <= f6;
    }

    @DexIgnore
    public static boolean n(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f5 && Math.abs(f3 - f6) <= f7;
    }

    @DexIgnore
    public static boolean o(float f2, float f3, float f4, float f5, float f6, float f7) {
        return Math.abs(f2 - f4) <= f7 && f3 > f5 && f3 < f6;
    }

    @DexIgnore
    public final boolean a() {
        return !u();
    }

    @DexIgnore
    public float b() {
        return Math.min(this.f, this.j / this.l);
    }

    @DexIgnore
    public float c() {
        return Math.min(this.e, this.i / this.k);
    }

    @DexIgnore
    public float d() {
        return Math.max(this.d, this.h / this.l);
    }

    @DexIgnore
    public float e() {
        return Math.max(this.c, this.g / this.k);
    }

    @DexIgnore
    public Vy5 f(float f2, float f3, float f4, CropImageView.c cVar) {
        Vy5.Bi g2 = cVar == CropImageView.c.OVAL ? g(f2, f3) : i(f2, f3, f4);
        if (g2 != null) {
            return new Vy5(g2, this, f2, f3);
        }
        return null;
    }

    @DexIgnore
    public final Vy5.Bi g(float f2, float f3) {
        float width = this.a.width() / 6.0f;
        RectF rectF = this.a;
        float f4 = rectF.left;
        float height = rectF.height() / 6.0f;
        float f5 = this.a.top;
        float f6 = f5 + height;
        float f7 = (height * 5.0f) + f5;
        return f2 < f4 + width ? f3 < f6 ? Vy5.Bi.TOP_LEFT : f3 < f7 ? Vy5.Bi.LEFT : Vy5.Bi.BOTTOM_LEFT : f2 < (width * 5.0f) + f4 ? f3 < f6 ? Vy5.Bi.TOP : f3 < f7 ? Vy5.Bi.CENTER : Vy5.Bi.BOTTOM : f3 < f6 ? Vy5.Bi.TOP_RIGHT : f3 < f7 ? Vy5.Bi.RIGHT : Vy5.Bi.BOTTOM_RIGHT;
    }

    @DexIgnore
    public RectF h() {
        this.b.set(this.a);
        return this.b;
    }

    @DexIgnore
    public final Vy5.Bi i(float f2, float f3, float f4) {
        RectF rectF = this.a;
        if (m(f2, f3, rectF.left, rectF.top, f4)) {
            return Vy5.Bi.TOP_LEFT;
        }
        RectF rectF2 = this.a;
        if (m(f2, f3, rectF2.right, rectF2.top, f4)) {
            return Vy5.Bi.TOP_RIGHT;
        }
        RectF rectF3 = this.a;
        if (m(f2, f3, rectF3.left, rectF3.bottom, f4)) {
            return Vy5.Bi.BOTTOM_LEFT;
        }
        RectF rectF4 = this.a;
        if (m(f2, f3, rectF4.right, rectF4.bottom, f4)) {
            return Vy5.Bi.BOTTOM_RIGHT;
        }
        RectF rectF5 = this.a;
        if (l(f2, f3, rectF5.left, rectF5.top, rectF5.right, rectF5.bottom) && a()) {
            return Vy5.Bi.CENTER;
        }
        RectF rectF6 = this.a;
        if (n(f2, f3, rectF6.left, rectF6.right, rectF6.top, f4)) {
            return Vy5.Bi.TOP;
        }
        RectF rectF7 = this.a;
        if (n(f2, f3, rectF7.left, rectF7.right, rectF7.bottom, f4)) {
            return Vy5.Bi.BOTTOM;
        }
        RectF rectF8 = this.a;
        if (o(f2, f3, rectF8.left, rectF8.top, rectF8.bottom, f4)) {
            return Vy5.Bi.LEFT;
        }
        RectF rectF9 = this.a;
        if (o(f2, f3, rectF9.right, rectF9.top, rectF9.bottom, f4)) {
            return Vy5.Bi.RIGHT;
        }
        RectF rectF10 = this.a;
        if (!l(f2, f3, rectF10.left, rectF10.top, rectF10.right, rectF10.bottom) || a()) {
            return null;
        }
        return Vy5.Bi.CENTER;
    }

    @DexIgnore
    public float j() {
        return this.l;
    }

    @DexIgnore
    public float k() {
        return this.k;
    }

    @DexIgnore
    public void p(float f2, float f3, float f4, float f5) {
        this.e = f2;
        this.f = f3;
        this.k = f4;
        this.l = f5;
    }

    @DexIgnore
    public void q(Ty5 ty5) {
        this.c = (float) ty5.D;
        this.d = (float) ty5.E;
        this.g = (float) ty5.F;
        this.h = (float) ty5.G;
        this.i = (float) ty5.H;
        this.j = (float) ty5.I;
    }

    @DexIgnore
    public void r(int i2, int i3) {
        this.i = (float) i2;
        this.j = (float) i3;
    }

    @DexIgnore
    public void s(int i2, int i3) {
        this.g = (float) i2;
        this.h = (float) i3;
    }

    @DexIgnore
    public void t(RectF rectF) {
        this.a.set(rectF);
    }

    @DexIgnore
    public boolean u() {
        return this.a.width() >= 100.0f && this.a.height() >= 100.0f;
    }
}
