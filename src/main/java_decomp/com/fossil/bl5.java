package com.fossil;

import android.os.Build;
import com.mapped.PermissionUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bl5 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = Hm7.c(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue());
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = Hm7.c(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue());
    @DexIgnore
    public static /* final */ Bl5 c; // = new Bl5();

    @DexIgnore
    public final String a(String str) {
        Wg6.c(str, "microAppId");
        if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886533);
            Wg6.b(c2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
            return c2;
        } else if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886369);
            Wg6.b(c3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return c3;
        } else if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.getValue())) {
            String c4 = Um5.c(PortfolioApp.get.instance(), 2131887172);
            Wg6.b(c4, "LanguageHelper.getString\u2026naProfile_List__SetGoals)");
            return c4;
        } else if (!Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return "";
        } else {
            String c5 = Um5.c(PortfolioApp.get.instance(), 2131886423);
            Wg6.b(c5, "LanguageHelper.getString\u2026ingtone_Title__RingPhone)");
            return c5;
        }
    }

    @DexIgnore
    public final List<String> b(String str) {
        Wg6.c(str, "microAppId");
        if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            int i = Build.VERSION.SDK_INT;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppHelper", "android.os.Build.VERSION.SDK_INT=" + i);
            if (i >= 29) {
                return Hm7.c(InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION);
            }
            return Hm7.c(InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE);
        } else if (!Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return new ArrayList();
        } else {
            return Hm7.c(InAppPermission.NOTIFICATION_ACCESS);
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        Wg6.c(str, "microAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        Wg6.c(str, "microAppId");
        return a.contains(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        Wg6.c(str, "microAppId");
        List<String> b2 = b(str);
        String[] a2 = PermissionUtils.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppHelper", "isPermissionGrantedForMicroApp " + str + " granted=" + a2 + " required=" + b2);
        Iterator<T> it = b2.iterator();
        while (it.hasNext()) {
            if (!Em7.B(a2, it.next())) {
                return false;
            }
        }
        return true;
    }
}
