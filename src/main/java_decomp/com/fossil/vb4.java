package com.fossil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vb4 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public volatile int b; // = 0;

    @DexIgnore
    public Vb4(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    public static Vb4 a(byte[] bArr) {
        return b(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static Vb4 b(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return new Vb4(bArr2);
    }

    @DexIgnore
    public static Vb4 c(String str) {
        try {
            return new Vb4(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.", e);
        }
    }

    @DexIgnore
    public void d(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.a, i, bArr, i2, i3);
    }

    @DexIgnore
    public InputStream e() {
        return new ByteArrayInputStream(this.a);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Vb4)) {
            return false;
        }
        byte[] bArr = this.a;
        int length = bArr.length;
        byte[] bArr2 = ((Vb4) obj).a;
        if (length != bArr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int f() {
        return this.a.length;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        if (i == 0) {
            byte[] bArr = this.a;
            int length = bArr.length;
            i = length;
            for (byte b2 : bArr) {
                i = (i * 31) + b2;
            }
            if (i == 0) {
                i = 1;
            }
            this.b = i;
        }
        return i;
    }
}
