package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Us4 {
    @DexIgnore
    public static final boolean a(Ts4 ts4) {
        Wg6.c(ts4, "$this$validate");
        if (Wg6.a(ts4.k(), "activity_best_result")) {
            if ((ts4.e().length() > 0) && ts4.e().length() <= 32 && ts4.b() >= 3600 && ts4.b() <= 259200) {
                return true;
            }
        }
        if (Wg6.a(ts4.k(), "activity_reach_goal")) {
            if ((ts4.e().length() > 0) && ts4.e().length() <= 32 && ts4.i() >= 1000 && ts4.i() <= 300000) {
                return true;
            }
        }
        return false;
    }
}
