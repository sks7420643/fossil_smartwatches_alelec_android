package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ U93 b;
    @DexIgnore
    public /* final */ /* synthetic */ Vg3 c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService e;

    @DexIgnore
    public Mp3(AppMeasurementDynamiteService appMeasurementDynamiteService, U93 u93, Vg3 vg3, String str) {
        this.e = appMeasurementDynamiteService;
        this.b = u93;
        this.c = vg3;
        this.d = str;
    }

    @DexIgnore
    public final void run() {
        this.e.b.O().H(this.b, this.c, this.d);
    }
}
