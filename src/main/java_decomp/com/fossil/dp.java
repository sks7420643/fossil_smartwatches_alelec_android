package com.fossil;

import com.fossil.fitness.FitnessData;
import com.mapped.Cd6;
import com.mapped.Hg6;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dp extends Qq7 implements Hg6<Lp, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Rq b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Dp(Rq rq) {
        super(1);
        this.b = rq;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Lp lp) {
        Rq rq = this.b;
        FitnessData[] fitnessDataArr = ((Mi) lp).V;
        if (fitnessDataArr == null) {
            fitnessDataArr = new FitnessData[0];
        }
        rq.D = fitnessDataArr;
        if (Q3.f.f(this.b.x.a())) {
            Rq rq2 = this.b;
            rq2.l(Nr.a(rq2.v, null, Zq.b, null, null, 13));
        } else {
            Rq rq3 = this.b;
            if (rq3.G > 0) {
                Lp.h(rq3, new Cj(rq3.w, rq3.x, (HashMap) null, rq3.z, 4), new Sn(rq3), new Eo(rq3), new Qo(rq3), null, null, 48, null);
            } else {
                rq3.l(Nr.a(rq3.v, null, Zq.b, null, null, 13));
            }
        }
        return Cd6.a;
    }
}
