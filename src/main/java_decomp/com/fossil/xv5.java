package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.X37;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.TimeUtils;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xv5 extends BaseFragment implements Zw6 {
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Yw6 g;
    @DexIgnore
    public G37<Pa5> h;
    @DexIgnore
    public Sv5 i;
    @DexIgnore
    public Kx6 j;
    @DexIgnore
    public Z67 k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public /* final */ String m; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Xv5 a() {
            return new Xv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 a;

        @DexIgnore
        public Bi(Xv5 xv5) {
            this.a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Xv5.K6(this.a).y(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 a;

        @DexIgnore
        public Ci(Xv5 xv5) {
            this.a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Xv5.K6(this.a).x(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 a;

        @DexIgnore
        public Di(Xv5 xv5) {
            this.a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Xv5.K6(this.a).w(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 a;

        @DexIgnore
        public Ei(Xv5 xv5) {
            this.a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Xv5.K6(this.a).v(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Fi(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xv5.K6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Gi(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Xv5.K6(this.b).r(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Hi(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Xv5.K6(this.b).s(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Ii(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Xv5.K6(this.b).u(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Ji(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pa5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 c;

        @DexIgnore
        public Ki(Pa5 pa5, Xv5 xv5) {
            this.b = pa5;
            this.c = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.v;
            Wg6.b(flexibleTextInputEditText, "binding.etBirthday");
            Editable text = flexibleTextInputEditText.getText();
            if (text == null || text.length() == 0) {
                Xv5.K6(this.c).A();
            } else {
                Xv5.K6(this.c).z();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Li(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xv5.K6(this.b).t(Qh5.MALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Mi(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xv5.K6(this.b).t(Qh5.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 b;

        @DexIgnore
        public Ni(Xv5 xv5) {
            this.b = xv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xv5.K6(this.b).t(Qh5.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 a;

        @DexIgnore
        public Oi(Xv5 xv5) {
            this.a = xv5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Xv5.K6(this.a).q(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi<T> implements Ls0<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ Xv5 a;

        @DexIgnore
        public Pi(Xv5 xv5) {
            this.a = xv5;
        }

        @DexIgnore
        public final void a(Date date) {
            if (date != null) {
                Calendar o = Xv5.K6(this.a).o();
                o.setTime(date);
                Xv5.K6(this.a).p(date, o);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Date date) {
            a(date);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Yw6 K6(Xv5 xv5) {
        Yw6 yw6 = xv5.g;
        if (yw6 != null) {
            return yw6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ProfileSetupFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void H4(Spanned spanned) {
        Wg6.c(spanned, "message");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.H;
                    Wg6.b(flexibleTextView, "binding.ftvThree");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.H.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void I3(SignUpEmailAuth signUpEmailAuth) {
        Wg6.c(signUpEmailAuth, "auth");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    Wg6.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(Um5.c(PortfolioApp.get.instance(), 2131886968));
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setVisibility(8);
                    RTLImageView rTLImageView = a2.O;
                    Wg6.b(rTLImageView, "it.ivCheckedEmail");
                    rTLImageView.setVisibility(8);
                    if (!TextUtils.isEmpty(signUpEmailAuth.getEmail())) {
                        a2.w.setText(signUpEmailAuth.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        Wg6.b(flexibleTextInputEditText, "it.etEmail");
                        flexibleTextInputEditText.setKeyListener(null);
                        FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                        Wg6.b(flexibleTextInputLayout2, "it.inputEmail");
                        flexibleTextInputLayout2.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        Wg6.b(flexibleTextInputLayout3, "it.inputEmail");
                        flexibleTextInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void K4(boolean z, String str) {
        Wg6.c(str, "message");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.J;
                    Wg6.b(flexibleTextInputLayout, "it.inputBirthday");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.J;
                    Wg6.b(flexibleTextInputLayout2, "it.inputBirthday");
                    flexibleTextInputLayout2.setError(str);
                    RTLImageView rTLImageView = a2.N;
                    Wg6.b(rTLImageView, "it.ivCheckedBirthday");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void L3(boolean z) {
        RTLImageView rTLImageView;
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null && (rTLImageView = a2.P) != null) {
                    Wg6.b(rTLImageView, "it");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void L6(TextView textView) {
        Drawable f = W6.f(PortfolioApp.get.instance(), 2131230836);
        textView.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099707));
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setBackground(f);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Yw6 yw6) {
        M6(yw6);
    }

    @DexIgnore
    public void M6(Yw6 yw6) {
        Wg6.c(yw6, "presenter");
        this.g = yw6;
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void Q1() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            OnboardingHeightWeightActivity.a aVar = OnboardingHeightWeightActivity.B;
            Wg6.b(activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void Q3(Spanned spanned) {
        Wg6.c(spanned, "message");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.D;
                    Wg6.b(flexibleTextView, "binding.ftvFive");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.D.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void T5(int i2, String str) {
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void Y2(Spanned spanned) {
        Wg6.c(spanned, "message");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.E;
                    Wg6.b(flexibleTextView, "binding.ftvFour");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.E.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void Z5(String str) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        Wg6.c(str, "birthdate");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null && (flexibleTextInputEditText = a2.v) != null) {
                    flexibleTextInputEditText.setText(str);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void c6() {
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    Wg6.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.z;
                    Wg6.b(flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.z;
                    Wg6.b(flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(false);
                    a2.z.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void e3(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    Wg6.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(Um5.c(PortfolioApp.get.instance(), 2131886968));
                    a2.x.setText(signUpSocialAuth.getFirstName());
                    a2.y.setText(signUpSocialAuth.getLastName());
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setVisibility(0);
                    if (!TextUtils.isEmpty(signUpSocialAuth.getEmail())) {
                        a2.w.setText(signUpSocialAuth.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        Wg6.b(flexibleTextInputEditText, "it.etEmail");
                        flexibleTextInputEditText.setKeyListener(null);
                        FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                        Wg6.b(flexibleTextInputLayout2, "it.inputEmail");
                        flexibleTextInputLayout2.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        Wg6.b(flexibleTextInputLayout3, "it.inputEmail");
                        flexibleTextInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void f() {
        DashBar dashBar;
        G37<Pa5> g37 = this.h;
        if (g37 != null) {
            Pa5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.S) != null) {
                X37.Ai ai = X37.a;
                Wg6.b(dashBar, "this");
                ai.f(dashBar, 500);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void h() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void i() {
        if (isActive()) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886961);
            Wg6.b(c, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            H6(c);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void k3() {
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    Wg6.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.z;
                    Wg6.b(flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.z;
                    Wg6.b(flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(true);
                    a2.z.d("flexible_button_primary");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void n0(boolean z, boolean z2, String str) {
        int i2;
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                    Wg6.b(flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                    RTLImageView rTLImageView = a2.O;
                    Wg6.b(rTLImageView, "it.ivCheckedEmail");
                    if (z) {
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        Wg6.b(flexibleTextInputLayout3, "it.inputEmail");
                        if (flexibleTextInputLayout3.getVisibility() == 0) {
                            i2 = 0;
                            rTLImageView.setVisibility(i2);
                            return;
                        }
                    }
                    i2 = 8;
                    rTLImageView.setVisibility(i2);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void n2(MFUser mFUser) {
        Date date;
        Wg6.c(mFUser, "user");
        if (isActive()) {
            c6();
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    Wg6.b(flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(Um5.c(PortfolioApp.get.instance(), 2131886951));
                    p4(Qh5.Companion.a(mFUser.getGender()));
                    if (!Vt7.l(mFUser.getEmail())) {
                        a2.w.setText(mFUser.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        Wg6.b(flexibleTextInputEditText, "it.etEmail");
                        L6(flexibleTextInputEditText);
                    }
                    if (!Vt7.l(mFUser.getFirstName())) {
                        a2.x.setText(mFUser.getFirstName());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.x;
                        Wg6.b(flexibleTextInputEditText2, "it.etFirstName");
                        L6(flexibleTextInputEditText2);
                    }
                    if (!Vt7.l(mFUser.getLastName())) {
                        a2.y.setText(mFUser.getLastName());
                        FlexibleTextInputEditText flexibleTextInputEditText3 = a2.y;
                        Wg6.b(flexibleTextInputEditText3, "it.etLastName");
                        L6(flexibleTextInputEditText3);
                    }
                    if (!Vt7.l(mFUser.getBirthday())) {
                        FlexibleTextInputEditText flexibleTextInputEditText4 = a2.v;
                        Wg6.b(flexibleTextInputEditText4, "it.etBirthday");
                        L6(flexibleTextInputEditText4);
                        try {
                            SimpleDateFormat simpleDateFormat = TimeUtils.a.get();
                            if (simpleDateFormat != null) {
                                date = simpleDateFormat.parse(mFUser.getBirthday());
                                Yw6 yw6 = this.g;
                                if (yw6 != null) {
                                    Calendar o = yw6.o();
                                    o.setTime(date);
                                    Yw6 yw62 = this.g;
                                    if (yw62 == null) {
                                        Wg6.n("mPresenter");
                                        throw null;
                                    } else if (date != null) {
                                        yw62.p(date, o);
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.e("ProfileSetupFragment", "toOffsetDateTime - e=" + e);
                            date = new Date();
                        }
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<Pa5> g37 = new G37<>(this, (Pa5) Aq0.f(layoutInflater, 2131558612, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            Pa5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Yw6 yw6 = this.g;
        if (yw6 != null) {
            yw6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Yw6 yw6 = this.g;
        if (yw6 != null) {
            yw6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Ts0 a2 = Vs0.e(requireActivity()).a(Z67.class);
        Wg6.b(a2, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
        this.k = (Z67) a2;
        Sv5 sv5 = (Sv5) getChildFragmentManager().Z(Sv5.u.a());
        this.i = sv5;
        if (sv5 == null) {
            this.i = Sv5.u.b();
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        Sv5 sv52 = this.i;
        if (sv52 != null) {
            iface.C1(new Ix6(sv52)).b(this);
            Z67 z67 = this.k;
            if (z67 != null) {
                z67.a().h(getViewLifecycleOwner(), new Pi(this));
                G37<Pa5> g37 = this.h;
                if (g37 != null) {
                    Pa5 a3 = g37.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.m)) {
                            Integer valueOf = Integer.valueOf(Color.parseColor(this.m));
                            this.l = valueOf;
                            if (valueOf != null) {
                                int intValue = valueOf.intValue();
                                FlexibleCheckBox flexibleCheckBox = a3.u;
                                Wg6.b(flexibleCheckBox, "binding.cbTwo");
                                flexibleCheckBox.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox2 = a3.u;
                                Wg6.b(flexibleCheckBox2, "binding.cbTwo");
                                flexibleCheckBox2.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox3 = a3.s;
                                Wg6.b(flexibleCheckBox3, "binding.cbOne");
                                flexibleCheckBox3.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox4 = a3.s;
                                Wg6.b(flexibleCheckBox4, "binding.cbOne");
                                flexibleCheckBox4.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox5 = a3.t;
                                Wg6.b(flexibleCheckBox5, "binding.cbThree");
                                flexibleCheckBox5.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox6 = a3.t;
                                Wg6.b(flexibleCheckBox6, "binding.cbThree");
                                flexibleCheckBox6.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox7 = a3.q;
                                Wg6.b(flexibleCheckBox7, "binding.cbFive");
                                flexibleCheckBox7.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox8 = a3.q;
                                Wg6.b(flexibleCheckBox8, "binding.cbFive");
                                flexibleCheckBox8.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox9 = a3.r;
                                Wg6.b(flexibleCheckBox9, "binding.cbFour");
                                flexibleCheckBox9.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox10 = a3.r;
                                Wg6.b(flexibleCheckBox10, "binding.cbFour");
                                flexibleCheckBox10.setButtonTintList(ColorStateList.valueOf(intValue));
                            }
                        }
                        a3.w.addTextChangedListener(new Gi(this));
                        a3.x.addTextChangedListener(new Hi(this));
                        a3.y.addTextChangedListener(new Ii(this));
                        a3.R.setOnClickListener(new Ji(this));
                        a3.v.setOnClickListener(new Ki(a3, this));
                        a3.B.setOnClickListener(new Li(this));
                        a3.A.setOnClickListener(new Mi(this));
                        a3.C.setOnClickListener(new Ni(this));
                        a3.u.setOnCheckedChangeListener(new Oi(this));
                        a3.s.setOnCheckedChangeListener(new Bi(this));
                        a3.t.setOnCheckedChangeListener(new Ci(this));
                        a3.r.setOnCheckedChangeListener(new Di(this));
                        a3.q.setOnCheckedChangeListener(new Ei(this));
                        a3.z.setOnClickListener(new Fi(this));
                        FlexibleTextView flexibleTextView = a3.G;
                        Wg6.b(flexibleTextView, "binding.ftvOne");
                        flexibleTextView.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView2 = a3.H;
                        Wg6.b(flexibleTextView2, "binding.ftvThree");
                        flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView3 = a3.E;
                        Wg6.b(flexibleTextView3, "binding.ftvFour");
                        flexibleTextView3.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView4 = a3.D;
                        Wg6.b(flexibleTextView4, "binding.ftvFive");
                        flexibleTextView4.setMovementMethod(new LinkMovementMethod());
                        if (Wr4.a.a().i()) {
                            FlexibleCheckBox flexibleCheckBox11 = a3.s;
                            Wg6.b(flexibleCheckBox11, "binding.cbOne");
                            flexibleCheckBox11.setVisibility(8);
                            FlexibleTextView flexibleTextView5 = a3.G;
                            Wg6.b(flexibleTextView5, "binding.ftvOne");
                            flexibleTextView5.setVisibility(8);
                            FlexibleCheckBox flexibleCheckBox12 = a3.u;
                            Wg6.b(flexibleCheckBox12, "binding.cbTwo");
                            flexibleCheckBox12.setVisibility(8);
                            FlexibleTextView flexibleTextView6 = a3.I;
                            Wg6.b(flexibleTextView6, "binding.ftvTwo");
                            flexibleTextView6.setVisibility(8);
                            FlexibleCheckBox flexibleCheckBox13 = a3.t;
                            Wg6.b(flexibleCheckBox13, "binding.cbThree");
                            flexibleCheckBox13.setVisibility(0);
                            FlexibleTextView flexibleTextView7 = a3.H;
                            Wg6.b(flexibleTextView7, "binding.ftvThree");
                            flexibleTextView7.setVisibility(0);
                            FlexibleCheckBox flexibleCheckBox14 = a3.r;
                            Wg6.b(flexibleCheckBox14, "binding.cbFour");
                            flexibleCheckBox14.setVisibility(0);
                            FlexibleTextView flexibleTextView8 = a3.E;
                            Wg6.b(flexibleTextView8, "binding.ftvFour");
                            flexibleTextView8.setVisibility(0);
                            FlexibleCheckBox flexibleCheckBox15 = a3.q;
                            Wg6.b(flexibleCheckBox15, "binding.cbFive");
                            flexibleCheckBox15.setVisibility(0);
                            FlexibleTextView flexibleTextView9 = a3.D;
                            Wg6.b(flexibleTextView9, "binding.ftvFive");
                            flexibleTextView9.setVisibility(0);
                        }
                    }
                    E6("profile_setup_view");
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mUserBirthDayViewModel");
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void p3(boolean z) {
        RTLImageView rTLImageView;
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null && (rTLImageView = a2.Q) != null) {
                    Wg6.b(rTLImageView, "it");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void p4(Qh5 qh5) {
        Wg6.c(qh5, "gender");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    PortfolioApp instance = PortfolioApp.get.instance();
                    int d = W6.d(instance, 2131099968);
                    int d2 = W6.d(instance, 2131100360);
                    Drawable f = W6.f(instance, 2131230836);
                    Drawable f2 = W6.f(instance, 2131230830);
                    a2.C.setTextColor(d);
                    a2.A.setTextColor(d);
                    a2.B.setTextColor(d);
                    FlexibleButton flexibleButton = a2.C;
                    Wg6.b(flexibleButton, "it.fbOther");
                    flexibleButton.setBackground(f);
                    FlexibleButton flexibleButton2 = a2.A;
                    Wg6.b(flexibleButton2, "it.fbFemale");
                    flexibleButton2.setBackground(f);
                    FlexibleButton flexibleButton3 = a2.B;
                    Wg6.b(flexibleButton3, "it.fbMale");
                    flexibleButton3.setBackground(f);
                    a2.B.d("flexible_button_secondary");
                    a2.A.d("flexible_button_secondary");
                    a2.C.d("flexible_button_secondary");
                    int i2 = Yv5.a[qh5.ordinal()];
                    if (i2 == 1) {
                        a2.B.setTextColor(d2);
                        FlexibleButton flexibleButton4 = a2.B;
                        Wg6.b(flexibleButton4, "it.fbMale");
                        flexibleButton4.setBackground(f2);
                        a2.B.d("flexible_button_primary");
                    } else if (i2 == 2) {
                        a2.A.setTextColor(d2);
                        FlexibleButton flexibleButton5 = a2.A;
                        Wg6.b(flexibleButton5, "it.fbFemale");
                        flexibleButton5.setBackground(f2);
                        a2.A.d("flexible_button_primary");
                    } else if (i2 == 3) {
                        a2.C.setTextColor(d2);
                        FlexibleButton flexibleButton6 = a2.C;
                        Wg6.b(flexibleButton6, "it.fbOther");
                        flexibleButton6.setBackground(f2);
                        a2.C.d("flexible_button_primary");
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void w0(Spanned spanned) {
        Wg6.c(spanned, "message");
        if (isActive()) {
            G37<Pa5> g37 = this.h;
            if (g37 != null) {
                Pa5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.G;
                    Wg6.b(flexibleTextView, "binding.ftvOne");
                    flexibleTextView.setText(spanned);
                    Integer num = this.l;
                    if (num != null) {
                        a2.G.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw6
    public void z0(Bundle bundle) {
        Wg6.c(bundle, "data");
        Sv5 sv5 = this.i;
        if (sv5 != null) {
            sv5.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            sv5.show(childFragmentManager, Sv5.u.a());
        }
    }
}
