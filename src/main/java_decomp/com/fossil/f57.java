package com.fossil;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F57 extends ShapeDrawable {
    @DexIgnore
    public /* final */ Paint a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ RectShape e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Ci, Di, Bi {
        @DexIgnore
        public String a; // = "";
        @DexIgnore
        public int b; // = -7829368;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public Typeface f; // = Typeface.create("sans-serif-light", 0);
        @DexIgnore
        public RectShape g; // = new RectShape();
        @DexIgnore
        public int h; // = -1;
        @DexIgnore
        public int i; // = -1;
        @DexIgnore
        public boolean j; // = false;
        @DexIgnore
        public boolean k; // = false;
        @DexIgnore
        public float l;

        @DexIgnore
        @Override // com.fossil.F57.Di
        public F57 a(String str, int i2) {
            k();
            return e(str, i2);
        }

        @DexIgnore
        @Override // com.fossil.F57.Ci
        public Di b() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Ci
        public Ci c(int i2) {
            this.d = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Ci
        public Ci d(int i2) {
            this.i = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Bi
        public F57 e(String str, int i2) {
            this.b = i2;
            this.a = str;
            return new F57(this);
        }

        @DexIgnore
        @Override // com.fossil.F57.Di
        public Bi f() {
            this.g = new OvalShape();
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Di
        public Ci g() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Ci
        public Ci h(int i2) {
            this.e = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Ci
        public Ci i(int i2) {
            this.h = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F57.Ci
        public Ci j(Typeface typeface) {
            this.f = typeface;
            return this;
        }

        @DexIgnore
        public Bi k() {
            this.g = new RectShape();
            return this;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        F57 e(String str, int i);
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Di b();

        @DexIgnore
        Ci c(int i);

        @DexIgnore
        Ci d(int i);

        @DexIgnore
        Ci h(int i);

        @DexIgnore
        Ci i(int i);

        @DexIgnore
        Ci j(Typeface typeface);
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        F57 a(String str, int i);

        @DexIgnore
        Bi f();

        @DexIgnore
        Ci g();
    }

    @DexIgnore
    public F57(Ai ai) {
        super(ai.g);
        this.e = ai.g;
        this.f = ai.e;
        this.g = ai.d;
        this.i = ai.l;
        this.c = ai.k ? ai.a.toUpperCase() : ai.a;
        this.d = ai.b;
        this.h = ai.i;
        Paint paint = new Paint();
        this.a = paint;
        paint.setColor(ai.h);
        this.a.setAntiAlias(true);
        this.a.setFakeBoldText(ai.j);
        this.a.setStyle(Paint.Style.FILL);
        this.a.setTypeface(ai.f);
        this.a.setTextAlign(Paint.Align.CENTER);
        this.a.setStrokeWidth((float) ai.c);
        this.j = ai.c;
        Paint paint2 = new Paint();
        this.b = paint2;
        paint2.setAntiAlias(true);
        this.b.setColor(c(this.d));
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth((float) this.j);
        Paint paint3 = getPaint();
        paint3.setAntiAlias(true);
        paint3.setColor(this.d);
    }

    @DexIgnore
    public static Di a() {
        return new Ai();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        RectF rectF = new RectF(getBounds());
        int i2 = this.j;
        rectF.inset((float) (i2 / 2), (float) (i2 / 2));
        RectShape rectShape = this.e;
        if (rectShape instanceof OvalShape) {
            canvas.drawOval(rectF, this.b);
        } else if (rectShape instanceof RoundRectShape) {
            float f2 = this.i;
            canvas.drawRoundRect(rectF, f2, f2, this.b);
        } else {
            canvas.drawRect(rectF, this.b);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        return Color.rgb((int) (((float) Color.red(i2)) * 0.9f), (int) (((float) Color.green(i2)) * 0.9f), (int) (((float) Color.blue(i2)) * 0.9f));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        if (this.j > 0) {
            b(canvas);
        }
        int save = canvas.save();
        canvas.translate((float) bounds.left, (float) bounds.top);
        int i2 = this.g;
        if (i2 < 0) {
            i2 = bounds.width();
        }
        int i3 = this.f;
        if (i3 < 0) {
            i3 = bounds.height();
        }
        int i4 = this.h;
        if (i4 < 0) {
            i4 = Math.min(i2, i3) / 2;
        }
        this.a.setTextSize((float) i4);
        canvas.drawText(this.c, (float) (i2 / 2), ((float) (i3 / 2)) - ((this.a.descent() + (this.a.ascent() / 2.0f)) + (this.a.ascent() / 6.0f)), this.a);
        canvas.restoreToCount(save);
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.g;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.a.setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
    }
}
