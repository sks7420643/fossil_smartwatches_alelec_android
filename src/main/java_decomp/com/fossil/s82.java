package com.fossil;

import android.os.Bundle;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S82 implements R62.Bi, R62.Ci {
    @DexIgnore
    public /* final */ /* synthetic */ H82 b;

    @DexIgnore
    public S82(H82 h82) {
        this.b = h82;
    }

    @DexIgnore
    public /* synthetic */ S82(H82 h82, K82 k82) {
        this(h82);
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void e(Bundle bundle) {
        if (this.b.r.l()) {
            this.b.b.lock();
            try {
                if (this.b.k != null) {
                    this.b.k.e(new Q82(this.b));
                    this.b.b.unlock();
                }
            } finally {
                this.b.b.unlock();
            }
        } else {
            this.b.k.e(new Q82(this.b));
        }
    }

    @DexIgnore
    @Override // com.fossil.R72
    public final void n(Z52 z52) {
        this.b.b.lock();
        try {
            if (this.b.C(z52)) {
                this.b.r();
                this.b.p();
            } else {
                this.b.D(z52);
            }
        } finally {
            this.b.b.unlock();
        }
    }
}
