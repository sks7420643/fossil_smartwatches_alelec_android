package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q75 extends P75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d T;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        ViewDataBinding.d dVar = new ViewDataBinding.d(28);
        T = dVar;
        dVar.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{2131558843});
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131362361, 3);
        U.put(2131362360, 4);
        U.put(2131362779, 5);
        U.put(2131361966, 6);
        U.put(2131363026, 7);
        U.put(2131362499, 8);
        U.put(2131362800, 9);
        U.put(2131362369, 10);
        U.put(2131362767, 11);
        U.put(2131362370, 12);
        U.put(2131363459, 13);
        U.put(2131362416, 14);
        U.put(2131362519, 15);
        U.put(2131363167, 16);
        U.put(2131363460, 17);
        U.put(2131362104, 18);
        U.put(2131362522, 19);
        U.put(2131362824, 20);
        U.put(2131362523, 21);
        U.put(2131361913, 22);
        U.put(2131362520, 23);
        U.put(2131362823, 24);
        U.put(2131362521, 25);
        U.put(2131361914, 26);
        U.put(2131362490, 27);
    }
    */

    @DexIgnore
    public Q75(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 28, T, U));
    }

    @DexIgnore
    public Q75(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 1, (View) objArr[22], (View) objArr[26], (FlexibleButton) objArr[6], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[18], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[21], (Lg5) objArr[2], (RTLImageView) objArr[11], (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[9], (LinearLayout) objArr[24], (LinearLayout) objArr[20], (ConstraintLayout) objArr[0], (RecyclerView) objArr[7], (FlexibleSwitchCompat) objArr[16], (View) objArr[13], (View) objArr[17]);
        this.S = -1;
        this.t.setTag(null);
        this.N.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.S = 0;
        }
        ViewDataBinding.i(this.H);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.H.o() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return false;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            long r2 = r6.S     // Catch:{ all -> 0x0017 }
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0017 }
        L_0x000b:
            return r0
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0017 }
            com.fossil.Lg5 r1 = r6.H
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            r0 = 0
            goto L_0x000b
        L_0x0017:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q75.o():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.S = 2;
        }
        this.H.q();
        w();
    }
}
