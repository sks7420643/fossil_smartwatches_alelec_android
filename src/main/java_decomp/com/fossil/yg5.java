package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yg5 {
    @DexIgnore
    public /* final */ ConstraintLayout a;
    @DexIgnore
    public /* final */ ConstraintLayout b;
    @DexIgnore
    public /* final */ ScrollView c;
    @DexIgnore
    public /* final */ RecyclerView d;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat e;
    @DexIgnore
    public /* final */ FlexibleTextView f;
    @DexIgnore
    public /* final */ FlexibleTextView g;
    @DexIgnore
    public /* final */ View h;
    @DexIgnore
    public /* final */ ConstraintLayout i;

    @DexIgnore
    public Yg5(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ScrollView scrollView, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view, ConstraintLayout constraintLayout3) {
        this.a = constraintLayout;
        this.b = constraintLayout2;
        this.c = scrollView;
        this.d = recyclerView;
        this.e = flexibleSwitchCompat;
        this.f = flexibleTextView;
        this.g = flexibleTextView2;
        this.h = view;
        this.i = constraintLayout3;
    }

    @DexIgnore
    public static Yg5 a(View view) {
        int i2 = 2131363293;
        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(2131362056);
        if (constraintLayout != null) {
            ScrollView scrollView = (ScrollView) view.findViewById(2131363010);
            if (scrollView != null) {
                RecyclerView recyclerView = (RecyclerView) view.findViewById(2131363053);
                if (recyclerView != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = (FlexibleSwitchCompat) view.findViewById(2131363171);
                    if (flexibleSwitchCompat != null) {
                        FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363293);
                        if (flexibleTextView != null) {
                            i2 = 2131363390;
                            FlexibleTextView flexibleTextView2 = (FlexibleTextView) view.findViewById(2131363390);
                            if (flexibleTextView2 != null) {
                                i2 = 2131363468;
                                View findViewById = view.findViewById(2131363468);
                                if (findViewById != null) {
                                    ConstraintLayout constraintLayout2 = (ConstraintLayout) view;
                                    return new Yg5(constraintLayout2, constraintLayout, scrollView, recyclerView, flexibleSwitchCompat, flexibleTextView, flexibleTextView2, findViewById, constraintLayout2);
                                }
                            }
                        }
                    } else {
                        i2 = 2131363171;
                    }
                } else {
                    i2 = 2131363053;
                }
            } else {
                i2 = 2131363010;
            }
        } else {
            i2 = 2131362056;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
    }

    @DexIgnore
    public static Yg5 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    @DexIgnore
    public static Yg5 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558861, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.a;
    }
}
