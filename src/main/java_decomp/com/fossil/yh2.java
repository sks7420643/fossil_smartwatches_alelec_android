package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yh2 {
    @DexIgnore
    public static /* final */ Wh2 A; // = Wh2.A("body_temperature_measurement_location");
    @DexIgnore
    public static /* final */ Wh2 B; // = Wh2.A("cervical_mucus_texture");
    @DexIgnore
    public static /* final */ Wh2 C; // = Wh2.A("cervical_mucus_amount");
    @DexIgnore
    public static /* final */ Wh2 D; // = Wh2.A("cervical_position");
    @DexIgnore
    public static /* final */ Wh2 E; // = Wh2.A("cervical_dilation");
    @DexIgnore
    public static /* final */ Wh2 F; // = Wh2.A("cervical_firmness");
    @DexIgnore
    public static /* final */ Wh2 G; // = Wh2.A("menstrual_flow");
    @DexIgnore
    public static /* final */ Wh2 H; // = Wh2.A("ovulation_test_result");
    @DexIgnore
    public static /* final */ Wh2 a; // = Wh2.D("blood_pressure_systolic");
    @DexIgnore
    public static /* final */ Wh2 b; // = Wh2.D("blood_pressure_systolic_average");
    @DexIgnore
    public static /* final */ Wh2 c; // = Wh2.D("blood_pressure_systolic_min");
    @DexIgnore
    public static /* final */ Wh2 d; // = Wh2.D("blood_pressure_systolic_max");
    @DexIgnore
    public static /* final */ Wh2 e; // = Wh2.D("blood_pressure_diastolic");
    @DexIgnore
    public static /* final */ Wh2 f; // = Wh2.D("blood_pressure_diastolic_average");
    @DexIgnore
    public static /* final */ Wh2 g; // = Wh2.D("blood_pressure_diastolic_min");
    @DexIgnore
    public static /* final */ Wh2 h; // = Wh2.D("blood_pressure_diastolic_max");
    @DexIgnore
    public static /* final */ Wh2 i; // = Wh2.A("body_position");
    @DexIgnore
    public static /* final */ Wh2 j; // = Wh2.A("blood_pressure_measurement_location");
    @DexIgnore
    public static /* final */ Wh2 k; // = Wh2.D("blood_glucose_level");
    @DexIgnore
    public static /* final */ Wh2 l; // = Wh2.A("temporal_relation_to_meal");
    @DexIgnore
    public static /* final */ Wh2 m; // = Wh2.A("temporal_relation_to_sleep");
    @DexIgnore
    public static /* final */ Wh2 n; // = Wh2.A("blood_glucose_specimen_source");
    @DexIgnore
    public static /* final */ Wh2 o; // = Wh2.D("oxygen_saturation");
    @DexIgnore
    public static /* final */ Wh2 p; // = Wh2.D("oxygen_saturation_average");
    @DexIgnore
    public static /* final */ Wh2 q; // = Wh2.D("oxygen_saturation_min");
    @DexIgnore
    public static /* final */ Wh2 r; // = Wh2.D("oxygen_saturation_max");
    @DexIgnore
    public static /* final */ Wh2 s; // = Wh2.D("supplemental_oxygen_flow_rate");
    @DexIgnore
    public static /* final */ Wh2 t; // = Wh2.D("supplemental_oxygen_flow_rate_average");
    @DexIgnore
    public static /* final */ Wh2 u; // = Wh2.D("supplemental_oxygen_flow_rate_min");
    @DexIgnore
    public static /* final */ Wh2 v; // = Wh2.D("supplemental_oxygen_flow_rate_max");
    @DexIgnore
    public static /* final */ Wh2 w; // = Wh2.A("oxygen_therapy_administration_mode");
    @DexIgnore
    public static /* final */ Wh2 x; // = Wh2.A("oxygen_saturation_system");
    @DexIgnore
    public static /* final */ Wh2 y; // = Wh2.A("oxygen_saturation_measurement_method");
    @DexIgnore
    public static /* final */ Wh2 z; // = Wh2.D("body_temperature");
}
