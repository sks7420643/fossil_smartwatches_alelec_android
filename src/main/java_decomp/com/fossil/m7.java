package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.mapped.Rc6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M7 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ K5 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public M7(K5 k5) {
        this.a = k5;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == -1323520325 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED")) {
            Serializable serializableExtra = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE");
            if (serializableExtra != null) {
                BluetoothLeAdapter.Ci ci = (BluetoothLeAdapter.Ci) serializableExtra;
                Serializable serializableExtra2 = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE");
                if (serializableExtra2 != null) {
                    this.a.r((BluetoothLeAdapter.Ci) serializableExtra2);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
        }
    }
}
