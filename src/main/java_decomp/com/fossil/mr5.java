package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Mr5 {
    @DexIgnore
    void a(Kr5 kr5);

    @DexIgnore
    void b(int i, Vh5 vh5);

    @DexIgnore
    void c(Kr5 kr5);
}
