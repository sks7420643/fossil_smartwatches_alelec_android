package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.mapped.Lc3;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vn5 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public GoogleSignInOptions a;
    @DexIgnore
    public J42 b;
    @DexIgnore
    public /* final */ String c; // = "23412525";
    @DexIgnore
    public WeakReference<BaseActivity> d;
    @DexIgnore
    public Yn5 e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<TResult> implements Ht3<Void> {
        @DexIgnore
        public static /* final */ Ai a; // = new Ai();

        @DexIgnore
        @Override // com.fossil.Ht3
        public final void onComplete(Nt3<Void> nt3) {
            Wg6.c(nt3, "it");
            FLogger.INSTANCE.getLocal().d(Vn5.g, "Log out google account completely");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Lc3 {
        @DexIgnore
        public static /* final */ Bi a; // = new Bi();

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            Wg6.c(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = Vn5.g;
            local.d(str, "Could not log out google account, error = " + exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    /*
    static {
        String canonicalName = Vn5.class.getCanonicalName();
        if (canonicalName != null) {
            Wg6.b(canonicalName, "MFLoginGoogleManager::class.java.canonicalName!!");
            g = canonicalName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public final void b() {
        J42 j42;
        FLogger.INSTANCE.getLocal().d(g, "connectNewAccount");
        WeakReference<BaseActivity> weakReference = this.d;
        if (weakReference != null) {
            if (weakReference == null) {
                Wg6.i();
                throw null;
            } else if (!(weakReference.get() == null || (j42 = this.b) == null)) {
                if (j42 != null) {
                    Intent s = j42.s();
                    Wg6.b(s, "googleSignInClient!!.signInIntent");
                    WeakReference<BaseActivity> weakReference2 = this.d;
                    if (weakReference2 != null) {
                        BaseActivity baseActivity = weakReference2.get();
                        if (baseActivity != null) {
                            baseActivity.startActivityForResult(s, 922);
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
        }
        Yn5 yn5 = this.e;
        if (yn5 != null) {
            yn5.b(600, null, "");
        }
    }

    @DexIgnore
    public final void c() {
        f(this.d);
        b();
    }

    @DexIgnore
    public final void d() {
        f(this.d);
    }

    @DexIgnore
    public final void e(L42 l42) {
        int i = 2;
        if (l42 == null) {
            d();
            Yn5 yn5 = this.e;
            if (yn5 != null) {
                yn5.b(600, null, "");
            }
        } else if (l42.c()) {
            GoogleSignInAccount b2 = l42.b();
            if (b2 == null) {
                Yn5 yn52 = this.e;
                if (yn52 != null) {
                    yn52.b(600, null, "");
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(g, "Step 1: Login using google success");
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String f2 = b2.f();
                if (f2 == null) {
                    f2 = "";
                }
                signUpSocialAuth.setEmail(f2);
                FLogger.INSTANCE.getLocal().d(g, "Google user email is " + b2.f());
                String k = b2.k();
                if (k == null) {
                    k = "";
                }
                signUpSocialAuth.setFirstName(k);
                FLogger.INSTANCE.getLocal().d(g, "Google user first name is " + b2.k());
                String h = b2.h();
                if (h == null) {
                    h = "";
                }
                signUpSocialAuth.setLastName(h);
                FLogger.INSTANCE.getLocal().d(g, "Google user last name is " + b2.h());
                String F = b2.F();
                if (F == null) {
                    F = "";
                }
                signUpSocialAuth.setToken(F);
                signUpSocialAuth.setClientId(AppHelper.g.a(""));
                signUpSocialAuth.setService("google");
                Yn5 yn53 = this.e;
                if (yn53 != null) {
                    yn53.a(signUpSocialAuth);
                }
                this.f = 0;
                d();
            }
        } else {
            Status a2 = l42.a();
            Wg6.b(a2, "result.status");
            int f3 = a2.f();
            FLogger.INSTANCE.getLocal().d(g, "login result code from google: " + f3);
            if (f3 == 12502) {
                int i2 = this.f;
                if (i2 < 2) {
                    this.f = i2 + 1;
                    b();
                    return;
                }
                this.f = 0;
                FLogger.INSTANCE.getLocal().d(g, "why the login session always end up here, bug from Google!!");
                d();
                Yn5 yn54 = this.e;
                if (yn54 != null) {
                    yn54.b(600, null, "");
                    return;
                }
                return;
            }
            if (f3 != 12501) {
                i = 600;
            }
            d();
            Yn5 yn55 = this.e;
            if (yn55 != null) {
                yn55.b(i, null, "");
            }
        }
    }

    @DexIgnore
    public final void f(WeakReference<Activity> weakReference) {
        if ((weakReference != null ? weakReference.get() : null) != null && H42.b(weakReference.get()) != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "inside .logOut(), googleSignInClient=" + this.b);
            J42 j42 = this.b;
            if (j42 == null) {
                return;
            }
            if (j42 != null) {
                Nt3<Void> t = j42.t();
                if (t != null) {
                    t.b(Ai.a);
                }
                if (t != null) {
                    t.d(Bi.a);
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void g(WeakReference<BaseActivity> weakReference, Yn5 yn5) {
        String str;
        Wg6.c(weakReference, Constants.ACTIVITY);
        Wg6.c(yn5, Constants.CALLBACK);
        Access c2 = SoLibraryLoader.f().c(PortfolioApp.get.instance());
        if (c2 == null || (str = c2.getA()) == null) {
            str = this.c;
        }
        if (str != null) {
            String obj = Wt7.u0(str).toString();
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(GoogleSignInOptions.v);
            aVar.d(obj);
            aVar.g(obj);
            aVar.b();
            aVar.e();
            GoogleSignInOptions a2 = aVar.a();
            Wg6.b(a2, "GoogleSignInOptions.Buil\u2026\n                .build()");
            this.a = a2;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            GoogleSignInOptions googleSignInOptions = this.a;
            if (googleSignInOptions != null) {
                this.b = H42.a(applicationContext, googleSignInOptions);
                this.e = yn5;
                this.d = weakReference;
                c();
                return;
            }
            Wg6.n("gso");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final boolean h(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Inside .onActivityResult requestCode=" + i + ", resultCode=" + i2);
        if (i != 922) {
            return true;
        }
        if (intent != null) {
            L42 a2 = D42.f.a(intent);
            e(a2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = g;
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult googleSignInResult=");
            Wg6.b(a2, Constants.RESULT);
            sb.append(a2.a());
            local2.d(str2, sb.toString());
            return true;
        }
        e(null);
        return true;
    }
}
