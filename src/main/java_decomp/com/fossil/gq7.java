package com.fossil;

import com.mapped.Hi6;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gq7 implements Ds7, Serializable {
    @DexIgnore
    public static /* final */ Object NO_RECEIVER; // = Ai.b;
    @DexIgnore
    public /* final */ Object receiver;
    @DexIgnore
    public transient Ds7 reflected;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Serializable {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        private Object readResolve() throws ObjectStreamException {
            return b;
        }
    }

    @DexIgnore
    public Gq7() {
        this(NO_RECEIVER);
    }

    @DexIgnore
    public Gq7(Object obj) {
        this.receiver = obj;
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public Object call(Object... objArr) {
        return getReflected().call(objArr);
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public Object callBy(Map map) {
        return getReflected().callBy(map);
    }

    @DexIgnore
    public Ds7 compute() {
        Ds7 ds7 = this.reflected;
        if (ds7 != null) {
            return ds7;
        }
        Ds7 computeReflected = computeReflected();
        this.reflected = computeReflected;
        return computeReflected;
    }

    @DexIgnore
    public abstract Ds7 computeReflected();

    @DexIgnore
    @Override // com.fossil.Cs7
    public List<Annotation> getAnnotations() {
        return getReflected().getAnnotations();
    }

    @DexIgnore
    public Object getBoundReceiver() {
        return this.receiver;
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public String getName() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    public Hi6 getOwner() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public List<Object> getParameters() {
        return getReflected().getParameters();
    }

    @DexIgnore
    public Ds7 getReflected() {
        Ds7 compute = compute();
        if (compute != this) {
            return compute;
        }
        throw new Fp7();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public Ns7 getReturnType() {
        return getReflected().getReturnType();
    }

    @DexIgnore
    public String getSignature() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public List<Object> getTypeParameters() {
        return getReflected().getTypeParameters();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public Os7 getVisibility() {
        return getReflected().getVisibility();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public boolean isAbstract() {
        return getReflected().isAbstract();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public boolean isFinal() {
        return getReflected().isFinal();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public boolean isOpen() {
        return getReflected().isOpen();
    }

    @DexIgnore
    @Override // com.fossil.Ds7
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }
}
