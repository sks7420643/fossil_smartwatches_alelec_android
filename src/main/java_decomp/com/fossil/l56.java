package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.Tq4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.mapped.AppWrapper;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.helper.AppHelper;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L56 extends Tq4<Tq4.Bi, Ai, Tq4.Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ Context d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Tq4.Ci {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public Ai(List<AppWrapper> list) {
            Wg6.c(list, "apps");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = V36.class.getSimpleName();
        Wg6.b(simpleName, "GetApps::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public L56(Context context) {
        Wg6.c(context, "mContext");
        this.d = context;
    }

    @DexIgnore
    @Override // com.fossil.Tq4
    public void a(Tq4.Bi bi) {
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase GetHybridApps");
        List<AppFilter> allAppFilters = Mn5.p.a().c().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.Bi> it = AppHelper.g.g().iterator();
        while (it.hasNext()) {
            AppHelper.Bi next = it.next();
            if (TextUtils.isEmpty(next.b()) || !Vt7.j(next.b(), this.d.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), Boolean.FALSE);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    Wg6.b(next2, "appFilter");
                    if (Wg6.a(next2.getType(), installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        Lm7.q(linkedList);
        b().onSuccess(new Ai(linkedList));
    }
}
