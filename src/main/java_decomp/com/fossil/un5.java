package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.util.Arrays;
import java.util.StringTokenizer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Un5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public CallbackManager a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Un5.b;
        }

        @DexIgnore
        public final String[] b(String str) {
            Wg6.c(str, "fullName");
            String[] strArr = {"", ""};
            StringTokenizer stringTokenizer = new StringTokenizer(str);
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                Wg6.b(nextToken, "tokenizer.nextToken()");
                strArr[0] = nextToken;
            }
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken2 = stringTokenizer.nextToken();
                Wg6.b(nextToken2, "tokenizer.nextToken()");
                strArr[1] = nextToken2;
            }
            return strArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements GraphRequest.GraphJSONObjectCallback {
        @DexIgnore
        public /* final */ /* synthetic */ Yn5 a;
        @DexIgnore
        public /* final */ /* synthetic */ AccessToken b;

        @DexIgnore
        public Bi(Yn5 yn5, AccessToken accessToken) {
            this.a = yn5;
            this.b = accessToken;
        }

        @DexIgnore
        @Override // com.facebook.GraphRequest.GraphJSONObjectCallback
        public final void onCompleted(JSONObject jSONObject, GraphResponse graphResponse) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Un5.c.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .fetchGraphDataFacebook response=");
            sb.append(graphResponse);
            sb.append(", error=");
            Wg6.b(graphResponse, "response");
            sb.append(graphResponse.getError());
            local.d(a2, sb.toString());
            if (graphResponse.getError() != null) {
                this.a.b(600, null, "");
            } else if (jSONObject != null) {
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String optString = jSONObject.optString(Constants.EMAIL);
                Wg6.b(optString, "me.optString(\"email\")");
                signUpSocialAuth.setEmail(optString);
                String optString2 = jSONObject.optString("name");
                String optString3 = jSONObject.optString("first_name");
                String optString4 = jSONObject.optString("last_name");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = Un5.c.a();
                local2.d(a3, "Facebook email is " + jSONObject.optString(Constants.EMAIL) + " facebook name " + jSONObject.optString("name"));
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = Un5.c.a();
                local3.d(a4, "Facebook first name = " + optString3 + " last name = " + optString4);
                if (!TextUtils.isEmpty(optString3) && !TextUtils.isEmpty(optString4)) {
                    Wg6.b(optString3, Constants.PROFILE_KEY_FIRST_NAME);
                    signUpSocialAuth.setFirstName(optString3);
                    Wg6.b(optString4, Constants.PROFILE_KEY_LAST_NAME);
                    signUpSocialAuth.setLastName(optString4);
                } else if (!TextUtils.isEmpty(optString2)) {
                    Ai ai = Un5.c;
                    Wg6.b(optString2, "name");
                    String[] b2 = ai.b(optString2);
                    signUpSocialAuth.setFirstName(b2[0]);
                    signUpSocialAuth.setLastName(b2[1]);
                }
                String token = this.b.getToken();
                Wg6.b(token, "token.token");
                signUpSocialAuth.setToken(token);
                signUpSocialAuth.setService(Constants.FACEBOOK);
                this.a.a(signUpSocialAuth);
            } else {
                this.a.b(600, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements FacebookCallback<LoginResult> {
        @DexIgnore
        public /* final */ /* synthetic */ Un5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Yn5 b;

        @DexIgnore
        public Ci(Un5 un5, Yn5 yn5) {
            this.a = un5;
            this.b = yn5;
        }

        @DexIgnore
        public void a(LoginResult loginResult) {
            Wg6.c(loginResult, "loginResult");
            AccessToken accessToken = loginResult.getAccessToken();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Un5.c.a();
            local.d(a2, "Step 1: Login using facebook success token=" + accessToken);
            Un5 un5 = this.a;
            Wg6.b(accessToken, Constants.PROFILE_KEY_ACCESS_TOKEN);
            un5.b(accessToken, this.b);
        }

        @DexIgnore
        @Override // com.facebook.FacebookCallback
        public void onCancel() {
            FLogger.INSTANCE.getLocal().e(Un5.c.a(), "loginWithEmail facebook is cancel");
            this.b.b(2, null, "");
        }

        @DexIgnore
        @Override // com.facebook.FacebookCallback
        public void onError(FacebookException facebookException) {
            Wg6.c(facebookException, "e");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Un5.c.a();
            local.e(a2, "loginWithEmail facebook fail " + facebookException.getMessage());
            this.b.b(600, null, "");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.facebook.FacebookCallback
        public /* bridge */ /* synthetic */ void onSuccess(LoginResult loginResult) {
            a(loginResult);
        }
    }

    /*
    static {
        String canonicalName = Un5.class.getCanonicalName();
        if (canonicalName != null) {
            Wg6.b(canonicalName, "MFLoginFacebookManager::class.java.canonicalName!!");
            b = canonicalName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public final void b(AccessToken accessToken, Yn5 yn5) {
        Wg6.c(accessToken, "token");
        Wg6.c(yn5, Constants.CALLBACK);
        GraphRequest newMeRequest = GraphRequest.newMeRequest(accessToken, new Bi(yn5, accessToken));
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id, name, email, first_name, last_name");
        Wg6.b(newMeRequest, "request");
        newMeRequest.setParameters(bundle);
        newMeRequest.executeAsync();
    }

    @DexIgnore
    public final void c(Activity activity, Yn5 yn5) {
        Wg6.c(activity, "activityContext");
        Wg6.c(yn5, Constants.CALLBACK);
        this.a = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList(Constants.EMAIL, "user_photos", "public_profile"));
        LoginManager instance = LoginManager.getInstance();
        CallbackManager callbackManager = this.a;
        if (callbackManager != null) {
            instance.registerCallback(callbackManager, new Ci(this, yn5));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final boolean d(int i, int i2, Intent intent) {
        Wg6.c(intent, "data");
        CallbackManager callbackManager = this.a;
        if (callbackManager == null) {
            return false;
        }
        if (callbackManager != null) {
            boolean onActivityResult = callbackManager.onActivityResult(i, i2, intent);
            this.a = null;
            return onActivityResult;
        }
        Wg6.i();
        throw null;
    }
}
