package com.fossil;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bs4 extends Handler {
    @DexIgnore
    public /* final */ WeakReference<Hs4> a;
    @DexIgnore
    public long b;
    @DexIgnore
    public TimeInterpolator c; // = new AccelerateDecelerateInterpolator();
    @DexIgnore
    public long d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Bs4(Context context, Hs4 hs4) {
        super(context.getMainLooper());
        Wg6.c(context, "context");
        Wg6.c(hs4, "iContinuousAnimator");
        this.a = new WeakReference<>(hs4);
    }

    @DexIgnore
    public final boolean a(Hs4 hs4) {
        float min = Math.min(((float) (System.currentTimeMillis() - this.b)) / hs4.getAnimationDuration(), 1.0f);
        hs4.setCurrentValue((this.c.getInterpolation(min) * (hs4.getTo() - hs4.getFrom())) + hs4.getFrom());
        return min >= ((float) 1);
    }

    @DexIgnore
    public final void b(Message message, Hs4 hs4) {
        hs4.setFrom(hs4.getTo());
        Object obj = message.obj;
        if (obj != null) {
            hs4.setTo(((float[]) obj)[0]);
            hs4.setCurrentValue(hs4.getTo());
            hs4.setAnimationState(Ds4.IDLE);
            hs4.a();
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.FloatArray");
    }

    @DexIgnore
    public final void c(Message message, Hs4 hs4) {
        Object obj = message.obj;
        if (obj != null) {
            hs4.setFrom(((float[]) obj)[0]);
            Object obj2 = message.obj;
            if (obj2 != null) {
                hs4.setTo(((float[]) obj2)[1]);
                this.b = System.currentTimeMillis();
                hs4.setAnimationState(Ds4.ANIMATING);
                sendEmptyMessageDelayed(Cs4.TICK.ordinal(), ((long) hs4.getFrameDelay()) - (SystemClock.uptimeMillis() - this.d));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.FloatArray");
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.FloatArray");
    }

    @DexIgnore
    public void handleMessage(Message message) {
        Wg6.c(message, "msg");
        Hs4 hs4 = this.a.get();
        if (hs4 != null) {
            Cs4 cs4 = Cs4.values()[message.what];
            Cs4 cs42 = Cs4.TICK;
            if (cs4 == cs42) {
                removeMessages(cs42.ordinal());
            }
            this.d = SystemClock.uptimeMillis();
            int i = As4.c[hs4.getAnimationState().ordinal()];
            if (i == 1) {
                int i2 = As4.a[cs4.ordinal()];
                if (i2 == 1) {
                    b(message, hs4);
                } else if (i2 == 2) {
                    c(message, hs4);
                } else if (i2 == 3) {
                    removeMessages(Cs4.TICK.ordinal());
                }
            } else if (i == 2) {
                int i3 = As4.b[cs4.ordinal()];
                if (i3 == 1) {
                    b(message, hs4);
                } else if (i3 == 2) {
                    this.b = System.currentTimeMillis();
                    hs4.setFrom(hs4.getCurrentValue());
                    Object obj = message.obj;
                    if (obj != null) {
                        hs4.setTo(((float[]) obj)[1]);
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlin.FloatArray");
                } else if (i3 == 3) {
                    if (a(hs4)) {
                        hs4.setAnimationState(Ds4.IDLE);
                        hs4.setCurrentValue(hs4.getTo());
                    }
                    sendEmptyMessageDelayed(Cs4.TICK.ordinal(), ((long) hs4.getFrameDelay()) - (SystemClock.uptimeMillis() - this.d));
                    hs4.a();
                }
            }
        }
    }
}
