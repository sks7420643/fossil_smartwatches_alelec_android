package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X33<T, B> {
    @DexIgnore
    public abstract B a();

    @DexIgnore
    public abstract void b(B b, int i, long j);

    @DexIgnore
    public abstract void c(B b, int i, Xz2 xz2);

    @DexIgnore
    public abstract void d(T t, R43 r43) throws IOException;

    @DexIgnore
    public abstract void e(Object obj, T t);

    @DexIgnore
    public abstract T f(Object obj);

    @DexIgnore
    public abstract void g(T t, R43 r43) throws IOException;

    @DexIgnore
    public abstract void h(Object obj, B b);

    @DexIgnore
    public abstract T i(T t, T t2);

    @DexIgnore
    public abstract void j(Object obj);

    @DexIgnore
    public abstract int k(T t);

    @DexIgnore
    public abstract int l(T t);
}
