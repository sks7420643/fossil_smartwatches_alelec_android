package com.fossil;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Pn6;
import com.mapped.AlertDialogFragment;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutEditActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nn6 extends U47 implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ Ai v; // = new Ai(null);
    @DexIgnore
    public Pn6 k;
    @DexIgnore
    public /* final */ Zp0 l; // = new Sr4(this);
    @DexIgnore
    public String m;
    @DexIgnore
    public Po4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Nn6.u;
        }

        @DexIgnore
        public final Nn6 b(String str) {
            Wg6.c(str, "workoutSessionId");
            Nn6 nn6 = new Nn6();
            nn6.setArguments(Nm0.a(new Lc6("WORKOUT_ID_KEY", str)));
            return nn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Nn6 b;

        @DexIgnore
        public Bi(Nn6 nn6) {
            this.b = nn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.b.getDialog();
            if (dialog != null) {
                Nn6 nn6 = this.b;
                Wg6.b(dialog, "it");
                nn6.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Nn6 b;

        @DexIgnore
        public Ci(Nn6 nn6) {
            this.b = nn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                WorkoutEditActivity.a aVar = WorkoutEditActivity.A;
                Wg6.b(activity, "it");
                String str = this.b.m;
                if (str != null) {
                    aVar.a(activity, str);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            Dialog dialog = this.b.getDialog();
            if (dialog != null) {
                Nn6 nn6 = this.b;
                Wg6.b(dialog, "it");
                nn6.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Nn6 b;

        @DexIgnore
        public Di(Nn6 nn6) {
            this.b = nn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.t(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Pn6.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Nn6 a;

        @DexIgnore
        public Ei(Nn6 nn6) {
            this.a = nn6;
        }

        @DexIgnore
        public final void a(Pn6.Ai ai) {
            if (ai.a()) {
                Dialog dialog = this.a.getDialog();
                if (dialog != null) {
                    Nn6 nn6 = this.a;
                    Wg6.b(dialog, "it");
                    nn6.onDismiss(dialog);
                }
            } else if (ai.b() > 0) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.O(childFragmentManager, ai.b(), ai.c());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Pn6.Ai ai) {
            a(ai);
        }
    }

    /*
    static {
        String simpleName = Nn6.class.getSimpleName();
        Wg6.b(simpleName, "WorkoutMenuFragment::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i, Intent intent) {
        Wg6.c(str, "tag");
        if (!(str.length() == 0)) {
            if (str.hashCode() != -479587351 || !str.equals("DELETE_WORKOUT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((BaseActivity) activity).R5(str, i, intent);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i == 2131363373) {
                Pn6 pn6 = this.k;
                if (pn6 != null) {
                    pn6.q();
                } else {
                    Wg6.n("mViewModel");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().a0().a(this);
        Po4 po4 = this.s;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(Pn6.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026enuViewModel::class.java)");
            this.k = (Pn6) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                String string = arguments.getString("WORKOUT_ID_KEY");
                if (string == null) {
                    string = "";
                }
                this.m = string;
                Pn6 pn6 = this.k;
                if (pn6 == null) {
                    Wg6.n("mViewModel");
                    throw null;
                } else if (string != null) {
                    pn6.s(string);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        } else {
            Wg6.n("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Pc5 pc5 = (Pc5) Aq0.f(layoutInflater, 2131558644, viewGroup, false, this.l);
        pc5.t.setOnClickListener(new Bi(this));
        new G37(this, pc5);
        pc5.r.setOnClickListener(new Ci(this));
        pc5.q.setOnClickListener(new Di(this));
        Pn6 pn6 = this.k;
        if (pn6 != null) {
            pn6.r().h(getViewLifecycleOwner(), new Ei(this));
            Wg6.b(pc5, "binding");
            return pc5.n();
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
