package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Tj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ak0 extends Ck0 {
    @DexIgnore
    public Tj0 c;
    @DexIgnore
    public Ak0 d;
    @DexIgnore
    public float e;
    @DexIgnore
    public Ak0 f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public Ak0 i;
    @DexIgnore
    public Bk0 j; // = null;
    @DexIgnore
    public int k; // = 1;
    @DexIgnore
    public Bk0 l; // = null;
    @DexIgnore
    public int m; // = 1;

    @DexIgnore
    public Ak0(Tj0 tj0) {
        this.c = tj0;
    }

    @DexIgnore
    @Override // com.fossil.Ck0
    public void e() {
        super.e();
        this.d = null;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.j = null;
        this.k = 1;
        this.l = null;
        this.m = 1;
        this.f = null;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = null;
        this.h = 0;
    }

    @DexIgnore
    @Override // com.fossil.Ck0
    public void f() {
        Ak0 ak0;
        Ak0 ak02;
        Ak0 ak03;
        Ak0 ak04;
        Ak0 ak05;
        Ak0 ak06;
        float f2;
        float f3;
        float f4;
        float f5;
        Ak0 ak07;
        if (this.b != 1 && this.h != 4) {
            Bk0 bk0 = this.j;
            if (bk0 != null) {
                if (bk0.b == 1) {
                    this.e = bk0.c * ((float) this.k);
                } else {
                    return;
                }
            }
            Bk0 bk02 = this.l;
            if (bk02 != null) {
                if (bk02.b == 1) {
                    float f6 = bk02.c;
                } else {
                    return;
                }
            }
            if (this.h == 1 && ((ak07 = this.d) == null || ak07.b == 1)) {
                Ak0 ak08 = this.d;
                if (ak08 == null) {
                    this.f = this;
                    this.g = this.e;
                } else {
                    this.f = ak08.f;
                    this.g = ak08.g + this.e;
                }
                b();
            } else if (this.h == 2 && (ak04 = this.d) != null && ak04.b == 1 && (ak05 = this.i) != null && (ak06 = ak05.d) != null && ak06.b == 1) {
                if (Kj0.x() != null) {
                    Kj0.x().v++;
                }
                this.f = this.d.f;
                Ak0 ak09 = this.i;
                ak09.f = ak09.d.f;
                Tj0.Di di = this.c.c;
                boolean z = di != Tj0.Di.RIGHT ? di == Tj0.Di.BOTTOM : true;
                if (z) {
                    f2 = this.d.g;
                    f3 = this.i.d.g;
                } else {
                    f2 = this.i.d.g;
                    f3 = this.d.g;
                }
                float f7 = f2 - f3;
                Tj0 tj0 = this.c;
                Tj0.Di di2 = tj0.c;
                if (di2 == Tj0.Di.LEFT || di2 == Tj0.Di.RIGHT) {
                    f5 = f7 - ((float) this.c.b.D());
                    f4 = this.c.b.V;
                } else {
                    f5 = f7 - ((float) tj0.b.r());
                    f4 = this.c.b.W;
                }
                int d2 = this.c.d();
                int d3 = this.i.c.d();
                if (this.c.i() == this.i.c.i()) {
                    f4 = 0.5f;
                    d2 = 0;
                    d3 = 0;
                }
                float f8 = (float) d2;
                float f9 = (float) d3;
                float f10 = (f5 - f8) - f9;
                if (z) {
                    Ak0 ak010 = this.i;
                    ak010.g = f9 + ak010.d.g + (f10 * f4);
                    this.g = (this.d.g - f8) - (f10 * (1.0f - f4));
                } else {
                    this.g = f8 + this.d.g + (f10 * f4);
                    Ak0 ak011 = this.i;
                    ak011.g = (ak011.d.g - f9) - (f10 * (1.0f - f4));
                }
                b();
                this.i.b();
            } else if (this.h == 3 && (ak0 = this.d) != null && ak0.b == 1 && (ak02 = this.i) != null && (ak03 = ak02.d) != null && ak03.b == 1) {
                if (Kj0.x() != null) {
                    Kj0.x().w++;
                }
                Ak0 ak012 = this.d;
                this.f = ak012.f;
                Ak0 ak013 = this.i;
                Ak0 ak014 = ak013.d;
                ak013.f = ak014.f;
                this.g = ak012.g + this.e;
                ak013.g = ak014.g + ak013.e;
                b();
                this.i.b();
            } else if (this.h == 5) {
                this.c.b.U();
            }
        }
    }

    @DexIgnore
    public void g(Kj0 kj0) {
        Oj0 g2 = this.c.g();
        Ak0 ak0 = this.f;
        if (ak0 == null) {
            kj0.f(g2, (int) (this.g + 0.5f));
        } else {
            kj0.e(g2, kj0.r(ak0.c), (int) (this.g + 0.5f), 6);
        }
    }

    @DexIgnore
    public void h(int i2, Ak0 ak0, int i3) {
        this.h = i2;
        this.d = ak0;
        this.e = (float) i3;
        ak0.a(this);
    }

    @DexIgnore
    public void i(Ak0 ak0, int i2) {
        this.d = ak0;
        this.e = (float) i2;
        ak0.a(this);
    }

    @DexIgnore
    public void j(Ak0 ak0, int i2, Bk0 bk0) {
        this.d = ak0;
        ak0.a(this);
        this.j = bk0;
        this.k = i2;
        bk0.a(this);
    }

    @DexIgnore
    public float k() {
        return this.g;
    }

    @DexIgnore
    public void l(Ak0 ak0, float f2) {
        if (this.b == 0 || !(this.f == ak0 || this.g == f2)) {
            this.f = ak0;
            this.g = f2;
            if (this.b == 1) {
                c();
            }
            b();
        }
    }

    @DexIgnore
    public String m(int i2) {
        return i2 == 1 ? "DIRECT" : i2 == 2 ? "CENTER" : i2 == 3 ? "MATCH" : i2 == 4 ? "CHAIN" : i2 == 5 ? "BARRIER" : "UNCONNECTED";
    }

    @DexIgnore
    public void n(Ak0 ak0, float f2) {
        this.i = ak0;
    }

    @DexIgnore
    public void o(Ak0 ak0, int i2, Bk0 bk0) {
        this.i = ak0;
        this.l = bk0;
        this.m = i2;
    }

    @DexIgnore
    public void p(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void q() {
        Tj0 i2 = this.c.i();
        if (i2 != null) {
            if (i2.i() == this.c) {
                this.h = 4;
                i2.f().h = 4;
            }
            int d2 = this.c.d();
            Tj0.Di di = this.c.c;
            if (di == Tj0.Di.RIGHT || di == Tj0.Di.BOTTOM) {
                d2 = -d2;
            }
            i(i2.f(), d2);
        }
    }

    @DexIgnore
    public String toString() {
        if (this.b != 1) {
            return "{ " + this.c + " UNRESOLVED} type: " + m(this.h);
        } else if (this.f == this) {
            return "[" + this.c + ", RESOLVED: " + this.g + "]  type: " + m(this.h);
        } else {
            return "[" + this.c + ", RESOLVED: " + this.f + ":" + this.g + "] type: " + m(this.h);
        }
    }
}
