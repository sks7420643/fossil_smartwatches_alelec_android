package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ku7<T> extends qn7<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* synthetic */ Object a(ku7 ku7, Object obj, Object obj2, int i, Object obj3) {
            if (obj3 == null) {
                if ((i & 2) != 0) {
                    obj2 = null;
                }
                return ku7.b(obj, obj2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryResume");
        }
    }

    @DexIgnore
    Object b(T t, Object obj);

    @DexIgnore
    void e(rp7<? super Throwable, tl7> rp7);

    @DexIgnore
    void f(dv7 dv7, T t);

    @DexIgnore
    void g(Object obj);

    @DexIgnore
    boolean isActive();
}
