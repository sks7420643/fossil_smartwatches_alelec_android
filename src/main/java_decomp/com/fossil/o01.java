package com.fossil;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O01 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ I11 c;
    @DexIgnore
    public /* final */ W01 d;
    @DexIgnore
    public /* final */ D11 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Executor a;
        @DexIgnore
        public I11 b;
        @DexIgnore
        public W01 c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public D11 e;
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = Integer.MAX_VALUE;
        @DexIgnore
        public int i; // = 20;

        @DexIgnore
        public O01 a() {
            return new O01(this);
        }

        @DexIgnore
        public Ai b(I11 i11) {
            this.b = i11;
            return this;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        O01 a();
    }

    @DexIgnore
    public O01(Ai ai) {
        Executor executor = ai.a;
        if (executor == null) {
            this.a = a();
        } else {
            this.a = executor;
        }
        Executor executor2 = ai.d;
        if (executor2 == null) {
            this.b = a();
        } else {
            this.b = executor2;
        }
        I11 i11 = ai.b;
        if (i11 == null) {
            this.c = I11.c();
        } else {
            this.c = i11;
        }
        W01 w01 = ai.c;
        if (w01 == null) {
            this.d = W01.c();
        } else {
            this.d = w01;
        }
        D11 d11 = ai.e;
        if (d11 == null) {
            this.e = new J11();
        } else {
            this.e = d11;
        }
        this.f = ai.f;
        this.g = ai.g;
        this.h = ai.h;
        this.i = ai.i;
    }

    @DexIgnore
    public final Executor a() {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    @DexIgnore
    public Executor b() {
        return this.a;
    }

    @DexIgnore
    public W01 c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.h;
    }

    @DexIgnore
    public int e() {
        return Build.VERSION.SDK_INT == 23 ? this.i / 2 : this.i;
    }

    @DexIgnore
    public int f() {
        return this.g;
    }

    @DexIgnore
    public int g() {
        return this.f;
    }

    @DexIgnore
    public D11 h() {
        return this.e;
    }

    @DexIgnore
    public Executor i() {
        return this.b;
    }

    @DexIgnore
    public I11 j() {
        return this.c;
    }
}
