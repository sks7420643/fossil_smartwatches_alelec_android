package com.fossil;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fh6 extends ch6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<GoalTrackingSummary> f;
    @DexIgnore
    public /* final */ dh6 g;
    @DexIgnore
    public /* final */ GoalTrackingRepository h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ no4 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1", f = "DashboardGoalTrackingPresenter.kt", l = {55, 62}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fh6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fh6$a$a")
        /* renamed from: com.fossil.fh6$a$a  reason: collision with other inner class name */
        public static final class C0085a implements fl5.a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1131a;

            @DexIgnore
            public C0085a(a aVar) {
                this.f1131a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.fl5.a
            public final void e(fl5.g gVar) {
                pq7.c(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardGoalTrackingPresenter", "onStatusChange status=" + gVar);
                if (gVar.b()) {
                    this.f1131a.this$0.w().d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<cu0<GoalTrackingSummary>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1132a;

            @DexIgnore
            public b(a aVar) {
                this.f1132a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<GoalTrackingSummary> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
                local.d("DashboardGoalTrackingPresenter", sb.toString());
                if (cu0 != null) {
                    this.f1132a.this$0.w().q(cu0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$user$1", f = "DashboardGoalTrackingPresenter.kt", l = {55}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.i;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fh6 fh6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fh6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r8 = 2
                r5 = 1
                java.lang.Object r4 = com.fossil.yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x0098
                if (r0 == r5) goto L_0x0058
                if (r0 != r8) goto L_0x0050
                java.lang.Object r0 = r9.L$4
                com.fossil.fh6 r0 = (com.fossil.fh6) r0
                java.lang.Object r1 = r9.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r9.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r9.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r9.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r10)
                r2 = r0
                r1 = r10
            L_0x0027:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.fossil.fh6.v(r2, r0)
                com.fossil.fh6 r0 = r9.this$0
                com.fossil.dh6 r0 = r0.w()
                com.fossil.fh6 r1 = r9.this$0
                com.portfolio.platform.data.Listing r1 = com.fossil.fh6.t(r1)
                if (r1 == 0) goto L_0x004d
                androidx.lifecycle.LiveData r1 = r1.getPagedList()
                if (r1 == 0) goto L_0x004d
                if (r0 == 0) goto L_0x00b8
                com.fossil.eh6 r0 = (com.fossil.eh6) r0
                com.fossil.fh6$a$b r2 = new com.fossil.fh6$a$b
                r2.<init>(r9)
                r1.h(r0, r2)
            L_0x004d:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x004f:
                return r0
            L_0x0050:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0058:
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
                r2 = r0
                r1 = r10
            L_0x0061:
                r0 = r1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                if (r0 == 0) goto L_0x004d
                java.lang.String r1 = r0.getCreatedAt()
                java.util.Date r1 = com.fossil.lk5.q0(r1)
                com.fossil.fh6 r3 = r9.this$0
                com.portfolio.platform.data.source.GoalTrackingRepository r5 = com.fossil.fh6.s(r3)
                java.lang.String r6 = "createdDate"
                com.fossil.pq7.b(r1, r6)
                com.fossil.fh6 r6 = r9.this$0
                com.fossil.no4 r6 = com.fossil.fh6.r(r6)
                com.fossil.fh6$a$a r7 = new com.fossil.fh6$a$a
                r7.<init>(r9)
                r9.L$0 = r2
                r9.L$1 = r0
                r9.L$2 = r0
                r9.L$3 = r1
                r9.L$4 = r3
                r9.label = r8
                java.lang.Object r1 = r5.getSummariesPaging(r1, r6, r7, r9)
                if (r1 != r4) goto L_0x00b5
                r0 = r4
                goto L_0x004f
            L_0x0098:
                com.fossil.el7.b(r10)
                com.fossil.iv7 r0 = r9.p$
                com.fossil.fh6 r1 = r9.this$0
                com.fossil.dv7 r1 = com.fossil.fh6.q(r1)
                com.fossil.fh6$a$c r2 = new com.fossil.fh6$a$c
                r3 = 0
                r2.<init>(r9, r3)
                r9.L$0 = r0
                r9.label = r5
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r9)
                if (r1 != r4) goto L_0x00c0
                r0 = r4
                goto L_0x004f
            L_0x00b5:
                r2 = r3
                goto L_0x0027
            L_0x00b8:
                com.fossil.il7 r0 = new com.fossil.il7
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment"
                r0.<init>(r1)
                throw r0
            L_0x00c0:
                r2 = r0
                goto L_0x0061
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.fh6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public fh6(dh6 dh6, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, no4 no4) {
        pq7.c(dh6, "mView");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(no4, "mAppExecutors");
        this.g = dh6;
        this.h = goalTrackingRepository;
        this.i = userRepository;
        this.j = no4;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!lk5.p0(this.e).booleanValue()) {
            this.e = new Date();
            Listing<GoalTrackingSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.ch6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.ch6
    public void o() {
        LiveData<cu0<GoalTrackingSummary>> pagedList;
        try {
            dh6 dh6 = this.g;
            Listing<GoalTrackingSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (dh6 != null) {
                    pagedList.n((eh6) dh6);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("DashboardGoalTrackingPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.ch6
    public void p() {
        gp7<tl7> retry;
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "retry all failed request");
        Listing<GoalTrackingSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public final dh6 w() {
        return this.g;
    }

    @DexIgnore
    public void x() {
        this.g.M5(this);
    }
}
