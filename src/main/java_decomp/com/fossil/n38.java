package com.fossil;

import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface N38 {
    @DexIgnore
    public static final N38 a = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements N38 {
        @DexIgnore
        @Override // com.fossil.N38
        public boolean a(int i, List<E38> list) {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.N38
        public boolean b(int i, List<E38> list, boolean z) {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.N38
        public void c(int i, D38 d38) {
        }

        @DexIgnore
        @Override // com.fossil.N38
        public boolean d(int i, K48 k48, int i2, boolean z) throws IOException {
            k48.skip((long) i2);
            return true;
        }
    }

    @DexIgnore
    boolean a(int i, List<E38> list);

    @DexIgnore
    boolean b(int i, List<E38> list, boolean z);

    @DexIgnore
    void c(int i, D38 d38);

    @DexIgnore
    boolean d(int i, K48 k48, int i2, boolean z) throws IOException;
}
