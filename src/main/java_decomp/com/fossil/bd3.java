package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bd3 extends Ds2 implements Ad3 {
    @DexIgnore
    public Bd3() {
        super("com.google.android.gms.maps.internal.IOnStreetViewPanoramaReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.Ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        Dc3 fd3;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            fd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaDelegate");
            fd3 = queryLocalInterface instanceof Dc3 ? (Dc3) queryLocalInterface : new Fd3(readStrongBinder);
        }
        b1(fd3);
        parcel2.writeNoException();
        return true;
    }
}
