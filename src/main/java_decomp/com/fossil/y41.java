package com.fossil;

import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y41 {
    @DexIgnore
    public static /* final */ Bi e; // = new Bi(null);
    @DexIgnore
    public /* final */ List<Lc6<Class<? extends Object>, N61<? extends Object, ?>>> a;
    @DexIgnore
    public /* final */ List<Lc6<Class<? extends Object>, O61<? extends Object, ?>>> b;
    @DexIgnore
    public /* final */ List<Lc6<Class<? extends Object>, E61<? extends Object>>> c;
    @DexIgnore
    public /* final */ List<U51> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ List<Lc6<Class<? extends Object>, N61<? extends Object, ?>>> a;
        @DexIgnore
        public /* final */ List<Lc6<Class<? extends Object>, O61<? extends Object, ?>>> b;
        @DexIgnore
        public /* final */ List<Lc6<Class<? extends Object>, E61<? extends Object>>> c;
        @DexIgnore
        public /* final */ List<U51> d;

        @DexIgnore
        public Ai() {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = new ArrayList();
            this.d = new ArrayList();
        }

        @DexIgnore
        public Ai(Y41 y41) {
            Wg6.c(y41, "registry");
            this.a = Pm7.j0(y41.c());
            this.b = Pm7.j0(y41.d());
            this.c = Pm7.j0(y41.b());
            this.d = Pm7.j0(y41.a());
        }

        @DexIgnore
        public final Ai a(U51 u51) {
            Wg6.c(u51, "decoder");
            this.d.add(u51);
            return this;
        }

        @DexIgnore
        public final <T> Ai b(Class<T> cls, E61<T> e61) {
            Wg6.c(cls, "type");
            Wg6.c(e61, "fetcher");
            this.c.add(Hl7.a(cls, e61));
            return this;
        }

        @DexIgnore
        public final <T> Ai c(Class<T> cls, N61<T, ?> n61) {
            Wg6.c(cls, "type");
            Wg6.c(n61, "mapper");
            this.a.add(Hl7.a(cls, n61));
            return this;
        }

        @DexIgnore
        public final Y41 d() {
            return new Y41(Pm7.h0(this.a), Pm7.h0(this.b), Pm7.h0(this.c), Pm7.h0(this.d), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.List<? extends com.mapped.Lc6<? extends java.lang.Class<? extends java.lang.Object>, ? extends com.fossil.N61<? extends java.lang.Object, ?>>> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.mapped.Lc6<? extends java.lang.Class<? extends java.lang.Object>, ? extends com.fossil.O61<? extends java.lang.Object, ?>>> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends com.mapped.Lc6<? extends java.lang.Class<? extends java.lang.Object>, ? extends com.fossil.E61<? extends java.lang.Object>>> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends com.fossil.U51> */
    /* JADX WARN: Multi-variable type inference failed */
    public Y41(List<? extends Lc6<? extends Class<? extends Object>, ? extends N61<? extends Object, ?>>> list, List<? extends Lc6<? extends Class<? extends Object>, ? extends O61<? extends Object, ?>>> list2, List<? extends Lc6<? extends Class<? extends Object>, ? extends E61<? extends Object>>> list3, List<? extends U51> list4) {
        this.a = list;
        this.b = list2;
        this.c = list3;
        this.d = list4;
    }

    @DexIgnore
    public /* synthetic */ Y41(List list, List list2, List list3, List list4, Qg6 qg6) {
        this(list, list2, list3, list4);
    }

    @DexIgnore
    public final List<U51> a() {
        return this.d;
    }

    @DexIgnore
    public final List<Lc6<Class<? extends Object>, E61<? extends Object>>> b() {
        return this.c;
    }

    @DexIgnore
    public final List<Lc6<Class<? extends Object>, N61<? extends Object, ?>>> c() {
        return this.a;
    }

    @DexIgnore
    public final List<Lc6<Class<? extends Object>, O61<? extends Object, ?>>> d() {
        return this.b;
    }

    @DexIgnore
    public final Ai e() {
        return new Ai(this);
    }

    @DexIgnore
    public final <T> U51 f(T t, K48 k48, String str) {
        U51 u51;
        Wg6.c(t, "data");
        Wg6.c(k48, "source");
        List<U51> list = this.d;
        int size = list.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                u51 = null;
                break;
            }
            U51 u512 = list.get(i);
            if (u512.b(k48, str)) {
                u51 = u512;
                break;
            }
            i++;
        }
        U51 u513 = u51;
        if (u513 != null) {
            return u513;
        }
        throw new IllegalStateException(("Unable to decode data. No decoder supports: " + ((Object) t)).toString());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[LOOP:0: B:1:0x000d->B:19:0x004f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0037 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> com.fossil.E61<T> g(T r9) {
        /*
            r8 = this;
            r3 = 0
            java.lang.String r0 = "data"
            com.mapped.Wg6.c(r9, r0)
            java.util.List<com.mapped.Lc6<java.lang.Class<? extends java.lang.Object>, com.fossil.E61<? extends java.lang.Object>>> r5 = r8.c
            int r6 = r5.size()
            r4 = r3
        L_0x000d:
            if (r4 >= r6) goto L_0x0053
            java.lang.Object r1 = r5.get(r4)
            r0 = r1
            com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
            java.lang.Object r2 = r0.component1()
            java.lang.Class r2 = (java.lang.Class) r2
            java.lang.Object r0 = r0.component2()
            com.fossil.E61 r0 = (com.fossil.E61) r0
            java.lang.Class r7 = r9.getClass()
            boolean r2 = r2.isAssignableFrom(r7)
            if (r2 == 0) goto L_0x004d
            if (r0 == 0) goto L_0x0045
            boolean r0 = r0.a(r9)
            if (r0 == 0) goto L_0x004d
            r0 = 1
        L_0x0035:
            if (r0 == 0) goto L_0x004f
            r0 = r1
        L_0x0038:
            com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
            if (r0 == 0) goto L_0x005d
            java.lang.Object r0 = r0.getSecond()
            if (r0 == 0) goto L_0x0055
            com.fossil.E61 r0 = (com.fossil.E61) r0
            return r0
        L_0x0045:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type coil.fetch.Fetcher<kotlin.Any>"
            r0.<init>(r1)
            throw r0
        L_0x004d:
            r0 = r3
            goto L_0x0035
        L_0x004f:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x000d
        L_0x0053:
            r0 = 0
            goto L_0x0038
        L_0x0055:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type coil.fetch.Fetcher<T>"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unable to fetch data. No fetcher supports: "
            r0.append(r1)
            r0.append(r9)
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Y41.g(java.lang.Object):com.fossil.E61");
    }
}
