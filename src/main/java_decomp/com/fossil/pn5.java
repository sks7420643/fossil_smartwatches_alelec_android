package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.helper.AppHelper;
import java.io.FileNotFoundException;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pn5 implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static Pn5 n;
    @DexIgnore
    public static /* final */ Ai o; // = new Ai(null);
    @DexIgnore
    public MediaPlayer a;
    @DexIgnore
    public AudioManager b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public long i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public AudioAttributes k;
    @DexIgnore
    public /* final */ Runnable l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Pn5 a() {
            Pn5 b;
            synchronized (this) {
                if (Pn5.o.b() == null) {
                    Pn5.o.c(new Pn5(null));
                }
                b = Pn5.o.b();
                if (b == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return b;
        }

        @DexIgnore
        public final Pn5 b() {
            return Pn5.n;
        }

        @DexIgnore
        public final void c(Pn5 pn5) {
            Pn5.n = pn5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements Runnable {
        @DexIgnore
        public /* final */ Ringtone b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ /* synthetic */ Pn5 d;

        @DexIgnore
        public Bi(Pn5 pn5, Ringtone ringtone, long j) {
            Wg6.c(ringtone, "mRingtone");
            this.d = pn5;
            this.b = ringtone;
            this.c = j;
        }

        @DexIgnore
        public void run() {
            this.d.m(this.b, Integer.MAX_VALUE, this.c, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements Runnable {
        @DexIgnore
        public /* final */ Ringtone b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ Pn5 d;

        @DexIgnore
        public Ci(Pn5 pn5, Ringtone ringtone, int i) {
            Wg6.c(ringtone, "mRingtone");
            this.d = pn5;
            this.b = ringtone;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            this.d.m(this.b, this.c, 0, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Pn5 b;

        @DexIgnore
        public Di(Pn5 pn5) {
            this.b = pn5;
        }

        @DexIgnore
        public final void run() {
            if (this.b.a != null) {
                MediaPlayer mediaPlayer = this.b.a;
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                    if (this.b.d) {
                        this.b.o();
                        this.b.b.setStreamVolume(4, this.b.f, 1);
                    }
                    FLogger.INSTANCE.getLocal().d(Pn5.m, "On play ringtone complete");
                    this.b.c = false;
                    return;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = Pn5.class.getSimpleName();
        Wg6.b(simpleName, "SoundManager::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public Pn5() {
        this.i = Long.MAX_VALUE;
        this.j = new Handler(PortfolioApp.get.instance().getMainLooper());
        this.a = new MediaPlayer();
        Object systemService = PortfolioApp.get.instance().getApplicationContext().getSystemService("audio");
        if (systemService != null) {
            AudioManager audioManager = (AudioManager) systemService;
            this.b = audioManager;
            this.e = audioManager.getStreamMaxVolume(4);
            AudioAttributes build = new AudioAttributes.Builder().setUsage(4).build();
            Wg6.b(build, "AudioAttributes.Builder(\u2026utes.USAGE_ALARM).build()");
            this.k = build;
            this.l = new Di(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.media.AudioManager");
    }

    @DexIgnore
    public /* synthetic */ Pn5(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void j(Ringtone ringtone) {
        if (ringtone != null) {
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            p();
            new Thread(new Ci(this, ringtone, 1)).start();
        }
    }

    @DexIgnore
    public final void k(Ringtone ringtone) {
        Wg6.c(ringtone, Constants.RINGTONE);
        l(ringtone, 2);
    }

    @DexIgnore
    public final void l(Ringtone ringtone, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "Inside " + m + ".playRingtone - ringtone=" + ringtone);
        if (ringtone != null) {
            this.d = true;
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            if (this.c) {
                FLogger.INSTANCE.getLocal().d(m, "On button stop ringtone event");
                o();
                return;
            }
            FLogger.INSTANCE.getLocal().d(m, "On button play ringtone event");
            new Thread(new Ci(this, ringtone, i2)).start();
        }
    }

    @DexIgnore
    public final void m(Ringtone ringtone, int i2, long j2, boolean z) {
        T t;
        Wg6.c(ringtone, Constants.RINGTONE);
        try {
            MediaPlayer mediaPlayer = this.a;
            if (mediaPlayer != null) {
                mediaPlayer.setOnPreparedListener(this);
                MediaPlayer mediaPlayer2 = this.a;
                if (mediaPlayer2 != null) {
                    mediaPlayer2.setOnCompletionListener(this);
                    if (i2 >= Integer.MAX_VALUE) {
                        this.h = true;
                        this.g = 0;
                        this.i = j2;
                    } else {
                        this.h = false;
                        this.i = 0;
                        this.g = i2;
                    }
                    MediaPlayer mediaPlayer3 = this.a;
                    if (mediaPlayer3 != null) {
                        mediaPlayer3.reset();
                        if (TextUtils.isEmpty(ringtone.getRingtoneId())) {
                            AssetFileDescriptor openFd = PortfolioApp.get.instance().getAssets().openFd("ringtones/" + ringtone.getRingtoneName() + CodelessMatcher.CURRENT_CLASS_NAME + Constants.MP3_EXTENSION);
                            Wg6.b(openFd, "PortfolioApp.instance.as\u2026me + \".\" + MP3_EXTENSION)");
                            MediaPlayer mediaPlayer4 = this.a;
                            if (mediaPlayer4 != null) {
                                mediaPlayer4.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
                                openFd.close();
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Iterator<T> it = AppHelper.g.e().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    t = null;
                                    break;
                                }
                                T next = it.next();
                                if (Wg6.a(next.getRingtoneName(), ringtone.getRingtoneName())) {
                                    t = next;
                                    break;
                                }
                            }
                            if (t != null) {
                                try {
                                    MediaPlayer mediaPlayer5 = this.a;
                                    if (mediaPlayer5 != null) {
                                        mediaPlayer5.setDataSource(PortfolioApp.get.instance(), Uri.parse(MediaStore.Audio.Media.INTERNAL_CONTENT_URI + '/' + ringtone.getRingtoneId()));
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                } catch (Exception e2) {
                                    FLogger.INSTANCE.getLocal().d(m, "playRingtoneFromAsset exeption=" + e2);
                                    MediaPlayer mediaPlayer6 = this.a;
                                    if (mediaPlayer6 != null) {
                                        mediaPlayer6.setDataSource(PortfolioApp.get.instance(), Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + '/' + ringtone.getRingtoneId()));
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                }
                            }
                        }
                        MediaPlayer mediaPlayer7 = this.a;
                        if (mediaPlayer7 != null) {
                            mediaPlayer7.setAudioAttributes(this.k);
                            MediaPlayer mediaPlayer8 = this.a;
                            if (mediaPlayer8 != null) {
                                mediaPlayer8.prepare();
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } catch (FileNotFoundException e3) {
            FLogger.INSTANCE.getLocal().e(m, "Error Inside " + m + ".playRingtoneFromAsset - Cant find ringtone, play default instead, ex=" + e3);
            Ringtone ringtone2 = new Ringtone(Constants.RINGTONE_DEFAULT, "");
            if (!z) {
                m(ringtone2, i2, j2, true);
                return;
            }
            FLogger.INSTANCE.getLocal().e(m, "Error Inside " + m + ".playRingtoneFromAsset - Cant find ringtone, play default instead");
        } catch (Exception e4) {
            FLogger.INSTANCE.getLocal().e(m, "Error when playing ringtone " + e4);
        }
    }

    @DexIgnore
    public final void n(Ringtone ringtone, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "Inside " + m + ".playRingtoneInfinitive - ringtone=" + ringtone);
        if (ringtone != null) {
            this.d = true;
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            if (!this.c) {
                FLogger.INSTANCE.getLocal().d(m, "On button play ringtone event");
                new Thread(new Bi(this, ringtone, j2)).start();
            }
        }
    }

    @DexIgnore
    public final void o() {
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(m, "On release media event");
            if (this.c) {
                p();
            }
            if (!this.d) {
                if (this.a != null) {
                    MediaPlayer mediaPlayer = this.a;
                    if (mediaPlayer != null) {
                        mediaPlayer.reset();
                        MediaPlayer mediaPlayer2 = this.a;
                        if (mediaPlayer2 != null) {
                            mediaPlayer2.release();
                            this.a = null;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                this.d = false;
                this.c = false;
            }
        }
    }

    @DexIgnore
    public void onCompletion(MediaPlayer mediaPlayer) {
        Wg6.c(mediaPlayer, "mp");
        if (this.g > 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "current loop is" + this.g);
            this.g = this.g + -1;
            MediaPlayer mediaPlayer2 = this.a;
            if (mediaPlayer2 != null) {
                mediaPlayer2.seekTo(0);
                MediaPlayer mediaPlayer3 = this.a;
                if (mediaPlayer3 != null) {
                    mediaPlayer3.start();
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            MediaPlayer mediaPlayer4 = this.a;
            if (mediaPlayer4 != null) {
                mediaPlayer4.stop();
                if (this.d) {
                    o();
                    this.b.setStreamVolume(4, this.f, 1);
                }
                FLogger.INSTANCE.getLocal().d(m, "On play ringtone complete");
                this.c = false;
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void onPrepared(MediaPlayer mediaPlayer) {
        Wg6.c(mediaPlayer, "mp");
        this.f = this.b.getStreamVolume(4);
        if (this.d) {
            this.b.setStreamVolume(4, this.e, 1);
        }
        if (!this.h) {
            this.j.postDelayed(this.l, (long) (mediaPlayer.getDuration() * this.g));
        } else {
            long j2 = this.i;
            if (j2 < Long.MAX_VALUE) {
                this.j.postDelayed(this.l, j2);
            }
            MediaPlayer mediaPlayer2 = this.a;
            if (mediaPlayer2 != null) {
                mediaPlayer2.setLooping(true);
            } else {
                Wg6.i();
                throw null;
            }
        }
        MediaPlayer mediaPlayer3 = this.a;
        if (mediaPlayer3 != null) {
            mediaPlayer3.start();
            this.c = true;
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void p() {
        MediaPlayer mediaPlayer = this.a;
        if (mediaPlayer != null) {
            if (mediaPlayer == null) {
                Wg6.i();
                throw null;
            } else if (mediaPlayer.isPlaying()) {
                FLogger.INSTANCE.getLocal().d(m, "On stop playing ringtone");
                MediaPlayer mediaPlayer2 = this.a;
                if (mediaPlayer2 != null) {
                    mediaPlayer2.stop();
                    this.j.removeCallbacks(this.l);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
        if (this.d) {
            this.b.setStreamVolume(4, this.f, 1);
            this.d = false;
        }
        this.c = false;
    }
}
