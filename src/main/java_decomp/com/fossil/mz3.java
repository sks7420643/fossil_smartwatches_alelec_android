package com.fossil;

import android.graphics.Typeface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mz3 extends Rz3 {
    @DexIgnore
    public /* final */ Typeface a;
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Typeface typeface);
    }

    @DexIgnore
    public Mz3(Ai ai, Typeface typeface) {
        this.a = typeface;
        this.b = ai;
    }

    @DexIgnore
    @Override // com.fossil.Rz3
    public void a(int i) {
        d(this.a);
    }

    @DexIgnore
    @Override // com.fossil.Rz3
    public void b(Typeface typeface, boolean z) {
        d(typeface);
    }

    @DexIgnore
    public void c() {
        this.c = true;
    }

    @DexIgnore
    public final void d(Typeface typeface) {
        if (!this.c) {
            this.b.a(typeface);
        }
    }
}
