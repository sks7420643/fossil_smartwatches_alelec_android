package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.fossil.Oa1;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qa1 extends ContextWrapper {
    @DexIgnore
    public static /* final */ Xa1<?, ?> k; // = new Na1();
    @DexIgnore
    public /* final */ Od1 a;
    @DexIgnore
    public /* final */ Ua1 b;
    @DexIgnore
    public /* final */ Oj1 c;
    @DexIgnore
    public /* final */ Oa1.Ai d;
    @DexIgnore
    public /* final */ List<Ej1<Object>> e;
    @DexIgnore
    public /* final */ Map<Class<?>, Xa1<?, ?>> f;
    @DexIgnore
    public /* final */ Xc1 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public Fj1 j;

    @DexIgnore
    public Qa1(Context context, Od1 od1, Ua1 ua1, Oj1 oj1, Oa1.Ai ai, Map<Class<?>, Xa1<?, ?>> map, List<Ej1<Object>> list, Xc1 xc1, boolean z, int i2) {
        super(context.getApplicationContext());
        this.a = od1;
        this.b = ua1;
        this.c = oj1;
        this.d = ai;
        this.e = list;
        this.f = map;
        this.g = xc1;
        this.h = z;
        this.i = i2;
    }

    @DexIgnore
    public <X> Rj1<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    @DexIgnore
    public Od1 b() {
        return this.a;
    }

    @DexIgnore
    public List<Ej1<Object>> c() {
        return this.e;
    }

    @DexIgnore
    public Fj1 d() {
        Fj1 fj1;
        synchronized (this) {
            if (this.j == null) {
                this.j = (Fj1) this.d.build().V();
            }
            fj1 = this.j;
        }
        return fj1;
    }

    @DexIgnore
    public <T> Xa1<?, T> e(Class<T> cls) {
        Xa1<?, T> xa1 = (Xa1<?, T>) this.f.get(cls);
        if (xa1 == null) {
            Xa1<?, ?> xa12 = xa1;
            for (Map.Entry<Class<?>, Xa1<?, ?>> entry : this.f.entrySet()) {
                if (entry.getKey().isAssignableFrom(cls)) {
                    xa12 = entry.getValue();
                }
            }
            xa1 = xa12;
        }
        return xa1 == null ? (Xa1<?, T>) k : xa1;
    }

    @DexIgnore
    public Xc1 f() {
        return this.g;
    }

    @DexIgnore
    public int g() {
        return this.i;
    }

    @DexIgnore
    public Ua1 h() {
        return this.b;
    }

    @DexIgnore
    public boolean i() {
        return this.h;
    }
}
