package com.fossil;

import java.io.IOException;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X88<T> implements E88<T, RequestBody> {
    @DexIgnore
    public static /* final */ X88<Object> a; // = new X88<>();
    @DexIgnore
    public static /* final */ R18 b; // = R18.d("text/plain; charset=UTF-8");

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.E88
    public /* bridge */ /* synthetic */ RequestBody a(Object obj) throws IOException {
        return b(obj);
    }

    @DexIgnore
    public RequestBody b(T t) throws IOException {
        return RequestBody.d(b, String.valueOf(t));
    }
}
