package com.fossil;

import com.mapped.Kc6;
import com.mapped.Wg6;
import java.util.zip.CRC32;
import java.util.zip.Checksum;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ix1 {
    @DexIgnore
    public static /* final */ Ix1 a; // = new Ix1();

    @DexIgnore
    public enum Ai {
        CRC32,
        CRC32C
    }

    @DexIgnore
    public final long a(byte[] bArr, int i, int i2, Ai ai) {
        Wg6.c(bArr, "data");
        Wg6.c(ai, "crcType");
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            return 0;
        }
        Checksum c = c(ai);
        c.update(bArr, i, i2);
        return c.getValue();
    }

    @DexIgnore
    public final long b(byte[] bArr, Ai ai) {
        Wg6.c(bArr, "data");
        Wg6.c(ai, "crcType");
        Checksum c = c(ai);
        c.update(bArr, 0, bArr.length);
        return c.getValue();
    }

    @DexIgnore
    public final Checksum c(Ai ai) {
        int i = Jx1.a[ai.ordinal()];
        if (i == 1) {
            return new CRC32();
        }
        if (i == 2) {
            return new Hx1();
        }
        throw new Kc6();
    }
}
