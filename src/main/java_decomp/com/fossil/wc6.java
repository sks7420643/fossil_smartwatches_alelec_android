package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wc6 implements Factory<SearchMicroAppPresenter> {
    @DexIgnore
    public static SearchMicroAppPresenter a(Rc6 rc6, MicroAppRepository microAppRepository, An4 an4, PortfolioApp portfolioApp) {
        return new SearchMicroAppPresenter(rc6, microAppRepository, an4, portfolioApp);
    }
}
