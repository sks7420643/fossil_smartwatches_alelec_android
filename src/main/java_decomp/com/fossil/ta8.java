package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;
import com.mapped.Qg6;
import com.mapped.Wg6;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ta8 extends ContentObserver {
    @DexIgnore
    public MethodChannel a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ta8(Handler handler) {
        super(handler);
        Wg6.c(handler, "handler");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ta8(Handler handler, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new Handler() : handler);
    }

    @DexIgnore
    public void onChange(boolean z) {
        super.onChange(z);
        MethodChannel methodChannel = this.a;
        if (methodChannel != null) {
            methodChannel.invokeMethod("change", 1);
        }
    }
}
