package com.fossil;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import com.fossil.A21;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y11 {
    @DexIgnore
    public static /* final */ String e; // = X01.f("ConstraintsCmdHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ A21 c;
    @DexIgnore
    public /* final */ H21 d;

    @DexIgnore
    public Y11(Context context, int i, A21 a21) {
        this.a = context;
        this.b = i;
        this.c = a21;
        this.d = new H21(this.a, a21.f(), null);
    }

    @DexIgnore
    public void a() {
        List<O31> i = this.c.g().p().j().i();
        ConstraintProxy.a(this.a, i);
        this.d.d(i);
        ArrayList<O31> arrayList = new ArrayList(i.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (O31 o31 : i) {
            String str = o31.a;
            if (currentTimeMillis >= o31.a() && (!o31.b() || this.d.c(str))) {
                arrayList.add(o31);
            }
        }
        for (O31 o312 : arrayList) {
            String str2 = o312.a;
            Intent b2 = X11.b(this.a, str2);
            X01.c().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", str2), new Throwable[0]);
            A21 a21 = this.c;
            a21.k(new A21.Bi(a21, b2, this.b));
        }
        this.d.e();
    }
}
