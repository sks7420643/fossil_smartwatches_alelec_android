package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G53 implements Xw2<F53> {
    @DexIgnore
    public static G53 c; // = new G53();
    @DexIgnore
    public /* final */ Xw2<F53> b;

    @DexIgnore
    public G53() {
        this(Ww2.b(new I53()));
    }

    @DexIgnore
    public G53(Xw2<F53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static long A() {
        return ((F53) c.zza()).u();
    }

    @DexIgnore
    public static long B() {
        return ((F53) c.zza()).w();
    }

    @DexIgnore
    public static long C() {
        return ((F53) c.zza()).b();
    }

    @DexIgnore
    public static long D() {
        return ((F53) c.zza()).h();
    }

    @DexIgnore
    public static long E() {
        return ((F53) c.zza()).l();
    }

    @DexIgnore
    public static long F() {
        return ((F53) c.zza()).o();
    }

    @DexIgnore
    public static long G() {
        return ((F53) c.zza()).i();
    }

    @DexIgnore
    public static long a() {
        return ((F53) c.zza()).k();
    }

    @DexIgnore
    public static long b() {
        return ((F53) c.zza()).t();
    }

    @DexIgnore
    public static long c() {
        return ((F53) c.zza()).v();
    }

    @DexIgnore
    public static long d() {
        return ((F53) c.zza()).r();
    }

    @DexIgnore
    public static long e() {
        return ((F53) c.zza()).s();
    }

    @DexIgnore
    public static long f() {
        return ((F53) c.zza()).m();
    }

    @DexIgnore
    public static String g() {
        return ((F53) c.zza()).q();
    }

    @DexIgnore
    public static long h() {
        return ((F53) c.zza()).j();
    }

    @DexIgnore
    public static long i() {
        return ((F53) c.zza()).zza();
    }

    @DexIgnore
    public static long j() {
        return ((F53) c.zza()).zzb();
    }

    @DexIgnore
    public static String k() {
        return ((F53) c.zza()).zzc();
    }

    @DexIgnore
    public static String l() {
        return ((F53) c.zza()).zzd();
    }

    @DexIgnore
    public static long m() {
        return ((F53) c.zza()).zze();
    }

    @DexIgnore
    public static long n() {
        return ((F53) c.zza()).zzf();
    }

    @DexIgnore
    public static long o() {
        return ((F53) c.zza()).zzg();
    }

    @DexIgnore
    public static long p() {
        return ((F53) c.zza()).zzh();
    }

    @DexIgnore
    public static long q() {
        return ((F53) c.zza()).g();
    }

    @DexIgnore
    public static long r() {
        return ((F53) c.zza()).a();
    }

    @DexIgnore
    public static long s() {
        return ((F53) c.zza()).p();
    }

    @DexIgnore
    public static long t() {
        return ((F53) c.zza()).zzl();
    }

    @DexIgnore
    public static long u() {
        return ((F53) c.zza()).zzm();
    }

    @DexIgnore
    public static long v() {
        return ((F53) c.zza()).e();
    }

    @DexIgnore
    public static long w() {
        return ((F53) c.zza()).n();
    }

    @DexIgnore
    public static long x() {
        return ((F53) c.zza()).f();
    }

    @DexIgnore
    public static long y() {
        return ((F53) c.zza()).c();
    }

    @DexIgnore
    public static long z() {
        return ((F53) c.zza()).d();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ F53 zza() {
        return this.b.zza();
    }
}
