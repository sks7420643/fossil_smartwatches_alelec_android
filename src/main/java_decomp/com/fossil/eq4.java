package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eq4 extends BaseActivity {
    /*
    static {
        Wg6.b(Eq4.class.getSimpleName(), "BaseDeepLinkHandleActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        F();
        Intent intent = getIntent();
        Uri data = intent != null ? intent.getData() : null;
        if (data != null) {
            String lastPathSegment = data.getLastPathSegment();
            if (lastPathSegment != null) {
                B77 b77 = B77.a;
                Wg6.b(lastPathSegment, "it");
                b77.f(lastPathSegment);
            }
            boolean contains = data.getPathSegments().contains("customized");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.d(r, "onCreate uri " + data + " open my faces: " + contains);
            t();
            finish();
            if (contains) {
                WatchFaceListActivity.C.b(this, data);
            } else {
                WatchFaceGalleryActivity.B.b(this, data);
            }
        }
    }
}
