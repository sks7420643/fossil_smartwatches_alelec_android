package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A16 implements MembersInjector<Z06> {
    @DexIgnore
    public static void a(Z06 z06, A26 a26) {
        z06.m = a26;
    }

    @DexIgnore
    public static void b(Z06 z06, Po4 po4) {
        z06.t = po4;
    }
}
