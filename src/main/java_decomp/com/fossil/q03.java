package com.fossil;

import com.fossil.E13;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q03 {
    @DexIgnore
    public static volatile Q03 b;
    @DexIgnore
    public static volatile Q03 c;
    @DexIgnore
    public static /* final */ Q03 d; // = new Q03(true);
    @DexIgnore
    public /* final */ Map<Ai, E13.Di<?, ?>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ai(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (!(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            return this.a == ai.a && this.b == ai.b;
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    @DexIgnore
    public Q03() {
        this.a = new HashMap();
    }

    @DexIgnore
    public Q03(boolean z) {
        this.a = Collections.emptyMap();
    }

    @DexIgnore
    public static Q03 a() {
        Q03 q03 = b;
        if (q03 == null) {
            synchronized (Q03.class) {
                try {
                    q03 = b;
                    if (q03 == null) {
                        q03 = d;
                        b = q03;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return q03;
    }

    @DexIgnore
    public static Q03 c() {
        Q03 q03 = c;
        if (q03 == null) {
            synchronized (Q03.class) {
                try {
                    q03 = c;
                    if (q03 == null) {
                        q03 = D13.b(Q03.class);
                        c = q03;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return q03;
    }

    @DexIgnore
    public final <ContainingType extends M23> E13.Di<ContainingType, ?> b(ContainingType containingtype, int i) {
        return (E13.Di<ContainingType, ?>) this.a.get(new Ai(containingtype, i));
    }
}
