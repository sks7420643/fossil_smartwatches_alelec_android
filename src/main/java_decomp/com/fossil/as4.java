package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class As4 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;

    /*
    static {
        int[] iArr = new int[Cs4.values().length];
        a = iArr;
        iArr[Cs4.SET_VALUE.ordinal()] = 1;
        a[Cs4.SET_VALUE_ANIMATED.ordinal()] = 2;
        a[Cs4.TICK.ordinal()] = 3;
        int[] iArr2 = new int[Cs4.values().length];
        b = iArr2;
        iArr2[Cs4.SET_VALUE.ordinal()] = 1;
        b[Cs4.SET_VALUE_ANIMATED.ordinal()] = 2;
        b[Cs4.TICK.ordinal()] = 3;
        int[] iArr3 = new int[Ds4.values().length];
        c = iArr3;
        iArr3[Ds4.IDLE.ordinal()] = 1;
        c[Ds4.ANIMATING.ordinal()] = 2;
    }
    */
}
