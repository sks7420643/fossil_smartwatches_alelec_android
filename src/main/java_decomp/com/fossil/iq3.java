package com.fossil;

import android.app.job.JobParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Iq3 implements Runnable {
    @DexIgnore
    public /* final */ Gq3 b;
    @DexIgnore
    public /* final */ Kl3 c;
    @DexIgnore
    public /* final */ JobParameters d;

    @DexIgnore
    public Iq3(Gq3 gq3, Kl3 kl3, JobParameters jobParameters) {
        this.b = gq3;
        this.c = kl3;
        this.d = jobParameters;
    }

    @DexIgnore
    public final void run() {
        this.b.e(this.c, this.d);
    }
}
