package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.mapped.Td0;
import com.mapped.Ud0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class Nc {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] g;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] h;

    /*
    static {
        int[] iArr = new int[Ob.values().length];
        a = iArr;
        iArr[Ob.f.ordinal()] = 1;
        a[Ob.n.ordinal()] = 2;
        a[Ob.e.ordinal()] = 3;
        a[Ob.p.ordinal()] = 4;
        a[Ob.l.ordinal()] = 5;
        a[Ob.u.ordinal()] = 6;
        a[Ob.d.ordinal()] = 7;
        a[Ob.g.ordinal()] = 8;
        a[Ob.h.ordinal()] = 9;
        a[Ob.i.ordinal()] = 10;
        a[Ob.k.ordinal()] = 11;
        a[Ob.m.ordinal()] = 12;
        a[Ob.o.ordinal()] = 13;
        a[Ob.y.ordinal()] = 14;
        a[Ob.j.ordinal()] = 15;
        a[Ob.q.ordinal()] = 16;
        a[Ob.r.ordinal()] = 17;
        a[Ob.x.ordinal()] = 18;
        a[Ob.s.ordinal()] = 19;
        a[Ob.t.ordinal()] = 20;
        a[Ob.v.ordinal()] = 21;
        a[Ob.w.ordinal()] = 22;
        int[] iArr2 = new int[Ud0.values().length];
        b = iArr2;
        iArr2[Ud0.ETA.ordinal()] = 1;
        b[Ud0.TRAVEL.ordinal()] = 2;
        int[] iArr3 = new int[Td0.values().length];
        c = iArr3;
        iArr3[Td0.COMMUTE_TIME.ordinal()] = 1;
        c[Td0.RING_PHONE.ordinal()] = 2;
        int[] iArr4 = new int[Lt.values().length];
        d = iArr4;
        iArr4[Lt.c.ordinal()] = 1;
        d[Lt.d.ordinal()] = 2;
        d[Lt.i.ordinal()] = 3;
        d[Lt.e.ordinal()] = 4;
        d[Lt.f.ordinal()] = 5;
        d[Lt.g.ordinal()] = 6;
        d[Lt.h.ordinal()] = 7;
        d[Lt.j.ordinal()] = 8;
        d[Lt.l.ordinal()] = 9;
        d[Lt.k.ordinal()] = 10;
        d[Lt.m.ordinal()] = 11;
        d[Lt.n.ordinal()] = 12;
        int[] iArr5 = new int[Du1.values().length];
        e = iArr5;
        iArr5[Du1.START.ordinal()] = 1;
        e[Du1.STOP.ordinal()] = 2;
        int[] iArr6 = new int[L90.values().length];
        f = iArr6;
        iArr6[L90.c.ordinal()] = 1;
        f[L90.d.ordinal()] = 2;
        f[L90.e.ordinal()] = 3;
        int[] iArr7 = new int[WorkoutState.values().length];
        g = iArr7;
        iArr7[WorkoutState.START.ordinal()] = 1;
        g[WorkoutState.RESUME.ordinal()] = 2;
        g[WorkoutState.PAUSE.ordinal()] = 3;
        g[WorkoutState.END.ordinal()] = 4;
        int[] iArr8 = new int[N90.values().length];
        h = iArr8;
        iArr8[N90.c.ordinal()] = 1;
        h[N90.d.ordinal()] = 2;
    }
    */
}
