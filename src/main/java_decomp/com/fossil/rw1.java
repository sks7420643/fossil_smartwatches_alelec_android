package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Rw1 {
    STEP("stepsBar"),
    CALORIES("caloriesBar"),
    ACTIVE_MINUTE("activeMinutesBar");
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Rw1 a(String str) {
            Rw1[] values = Rw1.values();
            for (Rw1 rw1 : values) {
                if (Wg6.a(rw1.a(), str)) {
                    return rw1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Rw1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
