package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wa0 implements Parcelable.Creator<Xa0> {
    @DexIgnore
    public /* synthetic */ Wa0(Qg6 qg6) {
    }

    @DexIgnore
    public Xa0 a(Parcel parcel) {
        return new Xa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Xa0 createFromParcel(Parcel parcel) {
        return new Xa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Xa0[] newArray(int i) {
        return new Xa0[i];
    }
}
