package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.fossil.F57;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zu4 extends RecyclerView.g<Bi> {
    @DexIgnore
    public /* final */ List<Ot4> a; // = new ArrayList();
    @DexIgnore
    public /* final */ F57.Bi b;
    @DexIgnore
    public /* final */ Uy4 c;
    @DexIgnore
    public /* final */ Ai d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void G5(Ot4 ot4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Zf5 a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Zu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;
            @DexIgnore
            public /* final */ /* synthetic */ Ot4 c;

            @DexIgnore
            public Aii(Bi bi, Ot4 ot4, F57.Bi bi2, Uy4 uy4) {
                this.b = bi;
                this.c = ot4;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.getAdapterPosition() != -1) {
                    Ot4 ot4 = this.c;
                    if (view != null) {
                        ot4.g(((FlexibleCheckBox) view).isChecked());
                        this.b.c.d.G5(this.c);
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleCheckBox");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Zu4 zu4, Zf5 zf5, View view) {
            super(view);
            Wg6.c(zf5, "binding");
            Wg6.c(view, "root");
            this.c = zu4;
            this.a = zf5;
            this.b = view;
        }

        @DexIgnore
        public void a(Ot4 ot4, F57.Bi bi, Uy4 uy4) {
            Wg6.c(ot4, "friend");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Zf5 zf5 = this.a;
            String b2 = Hz4.a.b(ot4.a(), ot4.c(), ot4.e());
            FlexibleTextView flexibleTextView = zf5.u;
            Wg6.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(b2);
            FlexibleCheckBox flexibleCheckBox = zf5.q;
            Wg6.b(flexibleCheckBox, "cbInvite");
            flexibleCheckBox.setChecked(ot4.f());
            ImageView imageView = zf5.s;
            Wg6.b(imageView, "ivAvatar");
            Ty4.b(imageView, ot4.d(), b2, bi, uy4);
            zf5.q.setOnClickListener(new Aii(this, ot4, bi, uy4));
        }
    }

    @DexIgnore
    public Zu4(Ai ai) {
        Wg6.c(ai, "listener");
        this.d = ai;
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.b = f;
        this.c = Uy4.d.b();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return i;
    }

    @DexIgnore
    public void h(Bi bi, int i) {
        Wg6.c(bi, "holder");
        bi.a(this.a.get(i), this.b, this.c);
    }

    @DexIgnore
    public Bi i(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Zf5 z = Zf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemSuggestingFriendLayo\u2026tInflater, parent, false)");
        View n = z.n();
        Wg6.b(n, "itemFriendInviteListBinding.root");
        return new Bi(this, z, n);
    }

    @DexIgnore
    public final void j(List<Ot4> list) {
        Wg6.c(list, NativeProtocol.AUDIENCE_FRIENDS);
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        h(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return i(viewGroup, i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void setHasStableIds(boolean z) {
        super.setHasStableIds(true);
    }
}
