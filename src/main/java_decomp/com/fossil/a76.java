package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.usecase.SetPresetUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A76 implements Factory<HomeDianaCustomizePresenter> {
    @DexIgnore
    public static HomeDianaCustomizePresenter a(Y66 y66, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, SetWatchAppUseCase setWatchAppUseCase, UserRepository userRepository, CustomizeRealDataManager customizeRealDataManager, FileRepository fileRepository, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository2, DianaWatchFaceRepository dianaWatchFaceRepository, PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository, WFAssetRepository wFAssetRepository, SetPresetUseCase setPresetUseCase) {
        return new HomeDianaCustomizePresenter(y66, watchAppRepository, complicationRepository, setWatchAppUseCase, userRepository, customizeRealDataManager, fileRepository, customizeRealDataRepository, userRepository2, dianaWatchFaceRepository, portfolioApp, dianaPresetRepository, wFAssetRepository, setPresetUseCase);
    }
}
