package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R90 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Q90 CREATOR; // = new Q90(null);
    @DexIgnore
    public /* final */ Ca0 b;
    @DexIgnore
    public /* final */ W90 c;
    @DexIgnore
    public /* final */ Da0 d;
    @DexIgnore
    public /* final */ Ea0 e;
    @DexIgnore
    public /* final */ short f;

    @DexIgnore
    public R90(Ca0 ca0, W90 w90, Da0 da0, Ea0 ea0, short s) {
        this.b = ca0;
        this.c = w90;
        this.d = da0;
        this.e = ea0;
        this.f = (short) s;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(R90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            R90 r90 = (R90) obj;
            return this.b == r90.b && this.c == r90.c && this.d == r90.d && this.e == r90.e && this.f == r90.f;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.animation.HandAnimation");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return (((((((hashCode * 31) + hashCode2) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.X0, Ey1.a(this.b)), Jd0.Z0, Ey1.a(this.c)), Jd0.W0, Ey1.a(this.d)), Jd0.a1, Ey1.a(this.e)), Jd0.R3, Short.valueOf(this.f));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
