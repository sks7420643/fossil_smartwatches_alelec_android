package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Np3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Xo3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 c;

    @DexIgnore
    public Np3(Fp3 fp3, Xo3 xo3) {
        this.c = fp3;
        this.b = xo3;
    }

    @DexIgnore
    public final void run() {
        Cl3 cl3 = this.c.d;
        if (cl3 == null) {
            this.c.d().F().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.b == null) {
                cl3.N0(0, null, null, this.c.e().getPackageName());
            } else {
                cl3.N0(this.b.c, this.b.a, this.b.b, this.c.e().getPackageName());
            }
            this.c.e0();
        } catch (RemoteException e) {
            this.c.d().F().b("Failed to send current screen to the service", e);
        }
    }
}
