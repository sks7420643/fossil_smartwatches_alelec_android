package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fy4 implements Factory<BCFriendTabViewModel> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<FriendRepository> b;

    @DexIgnore
    public Fy4(Provider<An4> provider, Provider<FriendRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Fy4 a(Provider<An4> provider, Provider<FriendRepository> provider2) {
        return new Fy4(provider, provider2);
    }

    @DexIgnore
    public static BCFriendTabViewModel c(An4 an4, FriendRepository friendRepository) {
        return new BCFriendTabViewModel(an4, friendRepository);
    }

    @DexIgnore
    public BCFriendTabViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
