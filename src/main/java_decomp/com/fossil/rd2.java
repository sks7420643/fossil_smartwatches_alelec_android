package com.fossil;

import android.app.Activity;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rd2 extends Cc2 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ Activity c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;

    @DexIgnore
    public Rd2(Intent intent, Activity activity, int i) {
        this.b = intent;
        this.c = activity;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.Cc2
    public final void c() {
        Intent intent = this.b;
        if (intent != null) {
            this.c.startActivityForResult(intent, this.d);
        }
    }
}
