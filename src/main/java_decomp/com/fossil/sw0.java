package com.fossil;

import android.database.Cursor;
import com.fossil.mx0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sw0 extends mx0.a {
    @DexIgnore
    public hw0 b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public a(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(lx0 lx0);

        @DexIgnore
        public abstract void dropAllTables(lx0 lx0);

        @DexIgnore
        public abstract void onCreate(lx0 lx0);

        @DexIgnore
        public abstract void onOpen(lx0 lx0);

        @DexIgnore
        public abstract void onPostMigrate(lx0 lx0);

        @DexIgnore
        public abstract void onPreMigrate(lx0 lx0);

        @DexIgnore
        public abstract b onValidateSchema(lx0 lx0);

        @DexIgnore
        @Deprecated
        public void validateMigration(lx0 lx0) {
            throw new UnsupportedOperationException("validateMigration is deprecated");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f3320a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(boolean z, String str) {
            this.f3320a = z;
            this.b = str;
        }
    }

    @DexIgnore
    public sw0(hw0 hw0, a aVar, String str, String str2) {
        super(aVar.version);
        this.b = hw0;
        this.c = aVar;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public static boolean j(lx0 lx0) {
        boolean z = false;
        Cursor query = lx0.query("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            if (query.moveToFirst() && query.getInt(0) == 0) {
                z = true;
            }
            return z;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static boolean k(lx0 lx0) {
        boolean z = false;
        Cursor query = lx0.query("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            if (query.moveToFirst() && query.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.mx0.a
    public void b(lx0 lx0) {
        super.b(lx0);
    }

    @DexIgnore
    @Override // com.fossil.mx0.a
    public void d(lx0 lx0) {
        boolean j = j(lx0);
        this.c.createAllTables(lx0);
        if (!j) {
            b onValidateSchema = this.c.onValidateSchema(lx0);
            if (!onValidateSchema.f3320a) {
                throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
            }
        }
        l(lx0);
        this.c.onCreate(lx0);
    }

    @DexIgnore
    @Override // com.fossil.mx0.a
    public void e(lx0 lx0, int i, int i2) {
        g(lx0, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.mx0.a
    public void f(lx0 lx0) {
        super.f(lx0);
        h(lx0);
        this.c.onOpen(lx0);
        this.b = null;
    }

    @DexIgnore
    @Override // com.fossil.mx0.a
    public void g(lx0 lx0, int i, int i2) {
        boolean z;
        List<ax0> c2;
        hw0 hw0 = this.b;
        if (hw0 == null || (c2 = hw0.d.c(i, i2)) == null) {
            z = false;
        } else {
            this.c.onPreMigrate(lx0);
            for (ax0 ax0 : c2) {
                ax0.migrate(lx0);
            }
            b onValidateSchema = this.c.onValidateSchema(lx0);
            if (onValidateSchema.f3320a) {
                this.c.onPostMigrate(lx0);
                l(lx0);
                z = true;
            } else {
                throw new IllegalStateException("Migration didn't properly handle: " + onValidateSchema.b);
            }
        }
        if (!z) {
            hw0 hw02 = this.b;
            if (hw02 == null || hw02.a(i, i2)) {
                throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
            }
            this.c.dropAllTables(lx0);
            this.c.createAllTables(lx0);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void h(lx0 lx0) {
        if (k(lx0)) {
            String str = null;
            Cursor query = lx0.query(new kx0("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (query.moveToFirst()) {
                    str = query.getString(0);
                }
                query.close();
                if (!this.d.equals(str) && !this.e.equals(str)) {
                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        } else {
            b onValidateSchema = this.c.onValidateSchema(lx0);
            if (onValidateSchema.f3320a) {
                this.c.onPostMigrate(lx0);
                l(lx0);
                return;
            }
            throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
        }
    }

    @DexIgnore
    public final void i(lx0 lx0) {
        lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    public final void l(lx0 lx0) {
        i(lx0);
        lx0.execSQL(rw0.a(this.d));
    }
}
