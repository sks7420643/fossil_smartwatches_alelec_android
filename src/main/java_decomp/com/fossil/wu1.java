package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ vu1 c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ byte f;
    @DexIgnore
    public /* final */ short g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ short i;
    @DexIgnore
    public /* final */ byte j;
    @DexIgnore
    public /* final */ zu1 k;
    @DexIgnore
    public /* final */ av1 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wu1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public wu1 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                pq7.b(createByteArray, "parcel.createByteArray()!!");
                Parcelable readParcelable = parcel.readParcelable(vu1.class.getClassLoader());
                if (readParcelable != null) {
                    return new wu1(createByteArray, (vu1) readParcelable);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public wu1[] newArray(int i) {
            return new wu1[i];
        }
    }

    @DexIgnore
    public wu1(byte[] bArr, vu1 vu1) {
        boolean z = true;
        this.b = bArr;
        this.c = vu1;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.d = order.get(0);
        this.e = order.get(1);
        this.f = order.get(2);
        this.g = order.getShort(3);
        this.h = order.getShort(7) <= 0 ? false : z;
        this.i = hy1.p(order.get(9));
        this.j = order.get(10);
        this.k = p90.b.a(this.g);
        this.l = p90.b.b(this.g);
    }

    @DexIgnore
    public final short a() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(wu1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            wu1 wu1 = (wu1) obj;
            return Arrays.equals(this.b, wu1.b) && !(pq7.a(this.c, wu1.c) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration");
    }

    @DexIgnore
    public final vu1 getCustomization() {
        return this.c;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.b;
    }

    @DexIgnore
    public final boolean getHasCustomization() {
        return this.h;
    }

    @DexIgnore
    public final byte getMajorVersion() {
        return this.d;
    }

    @DexIgnore
    public final zu1 getMicroAppId() {
        return this.k;
    }

    @DexIgnore
    public final av1 getMicroAppVariantId() {
        return this.l;
    }

    @DexIgnore
    public final byte getMicroAppVersion() {
        return this.f;
    }

    @DexIgnore
    public final byte getMinorVersion() {
        return this.e;
    }

    @DexIgnore
    public final byte getVariationNumber() {
        return this.j;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.b);
        vu1 vu1 = this.c;
        return (vu1 != null ? vu1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.F3, this.k.name()), jd0.t3, Byte.valueOf(this.f)), jd0.G3, Byte.valueOf(this.e)), jd0.H3, Byte.valueOf(this.d)), jd0.I3, Byte.valueOf(this.j)), jd0.J3, this.l), jd0.K3, Short.valueOf(this.i)), jd0.L3, Boolean.valueOf(this.h)), jd0.M3, Long.valueOf(ix1.f1688a.b(this.b, ix1.a.CRC32)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i2);
        }
    }
}
