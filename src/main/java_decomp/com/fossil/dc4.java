package com.fossil;

import com.fossil.Ec4;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dc4 implements Ec4 {
    @DexIgnore
    public /* final */ File a;

    @DexIgnore
    public Dc4(File file) {
        this.a = file;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public Map<String, String> a() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public String b() {
        return this.a.getName();
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public File c() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public File[] d() {
        return this.a.listFiles();
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public String e() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public Ec4.Ai getType() {
        return Ec4.Ai.NATIVE;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public void remove() {
        File[] d = d();
        for (File file : d) {
            X74.f().b("Removing native report file at " + file.getPath());
            file.delete();
        }
        X74.f().b("Removing native report directory at " + this.a);
        this.a.delete();
    }
}
