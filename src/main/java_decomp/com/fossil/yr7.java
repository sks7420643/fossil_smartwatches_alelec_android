package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yr7 extends Vm7 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public Yr7(long j, long j2, long j3) {
        this.e = j3;
        this.b = j2;
        boolean z = true;
        int i = (j > j2 ? 1 : (j == j2 ? 0 : -1));
        if (j3 <= 0 ? i < 0 : i > 0) {
            z = false;
        }
        this.c = z;
        this.d = !z ? this.b : j;
    }

    @DexIgnore
    @Override // com.fossil.Vm7
    public long b() {
        long j = this.d;
        if (j != this.b) {
            this.d = this.e + j;
        } else if (this.c) {
            this.c = false;
        } else {
            throw new NoSuchElementException();
        }
        return j;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.c;
    }
}
