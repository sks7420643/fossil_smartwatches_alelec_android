package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.xq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b06 extends qv5 implements a06, aw5, View.OnClickListener {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<n75> h;
    @DexIgnore
    public zz5 i;
    @DexIgnore
    public xq4 j;
    @DexIgnore
    public xz5 k;
    @DexIgnore
    public s36 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final b06 a() {
            return new b06();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements xq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ b06 f376a;

        @DexIgnore
        public b(b06 b06) {
            this.f376a = b06;
        }

        @DexIgnore
        @Override // com.fossil.xq4.a
        public void a(Alarm alarm) {
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            b06.L6(this.f376a).o(alarm, !alarm.isActive());
        }

        @DexIgnore
        @Override // com.fossil.xq4.a
        public void b(Alarm alarm) {
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            b06.L6(this.f376a).q(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public c(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                pq7.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public d(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public e(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            p47.l(view);
            NotificationAppsActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public f(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationCallsAndMessagesActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public g(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            b06.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public h(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            xz5 xz5 = this.b.k;
            if (xz5 != null) {
                xz5.D6(0);
            }
            xz5 xz52 = this.b.k;
            if (xz52 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                xz52.show(childFragmentManager, xz5.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b06 b;

        @DexIgnore
        public i(b06 b06) {
            this.b = b06;
        }

        @DexIgnore
        public final void onClick(View view) {
            xz5 xz5 = this.b.k;
            if (xz5 != null) {
                xz5.D6(1);
            }
            xz5 xz52 = this.b.k;
            if (xz52 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                xz52.show(childFragmentManager, xz5.u.a());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ zz5 L6(b06 b06) {
        zz5 zz5 = b06.i;
        if (zz5 != null) {
            return zz5;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HomeAlertsFragment";
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void F(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            g37<n75> g37 = this.h;
            if (g37 != null) {
                n75 a2 = g37.a();
                if (a2 != null && (rTLImageView2 = a2.G) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<n75> g372 = this.h;
        if (g372 != null) {
            n75 a3 = g372.a();
            if (a3 != null && (rTLImageView = a3.G) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void J0(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        pq7.c(spannableString, LogBuilder.KEY_TIME);
        g37<n75> g37 = this.h;
        if (g37 != null) {
            n75 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.B) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(zz5 zz5) {
        pq7.c(zz5, "presenter");
        this.i = zz5;
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void U() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.Z(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void W() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "notifyListAlarm()");
        xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.notifyDataSetChanged();
        } else {
            pq7.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        if (z) {
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void l0(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        pq7.c(str, "deviceId");
        pq7.c(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.B;
        Context requireContext = requireContext();
        pq7.b(requireContext, "requireContext()");
        aVar.a(requireContext, str, arrayList, alarm);
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void n3(boolean z) {
        Context requireContext;
        int i2;
        g37<n75> g37 = this.h;
        if (g37 != null) {
            n75 a2 = g37.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.P;
                pq7.b(switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                ConstraintLayout constraintLayout = a2.s;
                pq7.b(constraintLayout, "it.clScheduledTimeContainer");
                constraintLayout.setVisibility(z ? 0 : 8);
                FlexibleTextView flexibleTextView = a2.z;
                if (z) {
                    requireContext = requireContext();
                    i2 = 2131099968;
                } else {
                    requireContext = requireContext();
                    i2 = 2131100358;
                }
                flexibleTextView.setTextColor(gl0.d(requireContext, i2));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void o1(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "notificationAppOverView");
        g37<n75> g37 = this.h;
        if (g37 != null) {
            n75 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.v) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
        if (valueOf != null && valueOf.intValue() == 2131361966) {
            zz5 zz5 = this.i;
            if (zz5 != null) {
                zz5.q(null);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else if (valueOf != null && valueOf.intValue() == 2131362767) {
            zz5 zz52 = this.i;
            if (zz52 != null) {
                zz52.n();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        n75 n75 = (n75) aq0.f(layoutInflater, 2131558571, viewGroup, false, A6());
        xz5 xz5 = (xz5) getChildFragmentManager().Z(xz5.u.a());
        this.k = xz5;
        if (xz5 == null) {
            this.k = xz5.u.b();
        }
        String d2 = qn5.l.a().d("onPrimaryButton");
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            Drawable drawable = PortfolioApp.h0.c().getDrawable(2131230986);
            if (drawable != null) {
                drawable.setTint(parseColor);
                n75.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        n75.G.setOnClickListener(this);
        FlexibleButton flexibleButton = n75.q;
        pq7.b(flexibleButton, "binding.btnAdd");
        nl5.a(flexibleButton, this);
        n75.M.setOnClickListener(new d(this));
        n75.I.setOnClickListener(new e(this));
        n75.J.setOnClickListener(new f(this));
        n75.P.setOnClickListener(new g(this));
        n75.L.setOnClickListener(new h(this));
        n75.K.setOnClickListener(new i(this));
        lg5 lg5 = n75.F;
        if (lg5 != null) {
            ConstraintLayout constraintLayout = lg5.q;
            pq7.b(constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            String d3 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d3)) {
                lg5.q.setBackgroundColor(Color.parseColor(d3));
            }
            lg5.t.setImageResource(2131230818);
            FlexibleTextView flexibleTextView = lg5.r;
            pq7.b(flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(um5.c(getContext(), 2131887050));
            lg5.s.setOnClickListener(new c(this));
        }
        xq4 xq4 = new xq4();
        xq4.o(new b(this));
        this.j = xq4;
        RecyclerView recyclerView = n75.O;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        xq4 xq42 = this.j;
        if (xq42 != null) {
            recyclerView.setAdapter(xq42);
            this.h = new g37<>(this, n75);
            ro4 M = PortfolioApp.h0.c().M();
            xz5 xz52 = this.k;
            if (xz52 != null) {
                M.I0(new d06(xz52)).a(this);
                g37<n75> g37 = this.h;
                if (g37 != null) {
                    n75 a2 = g37.a();
                    if (a2 != null) {
                        pq7.b(a2, "mBinding.get()!!");
                        return a2.n();
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimeContract.View");
        }
        pq7.n("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        zz5 zz5 = this.i;
        if (zz5 != null) {
            if (zz5 != null) {
                zz5.m();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
        super.onPause();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        zz5 zz5 = this.i;
        if (zz5 != null) {
            if (zz5 != null) {
                zz5.l();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("alert_view");
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void p0(List<Alarm> list) {
        pq7.c(list, "alarms");
        xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.n(list);
        } else {
            pq7.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void q2(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        pq7.c(spannableString, LogBuilder.KEY_TIME);
        g37<n75> g37 = this.h;
        if (g37 != null) {
            n75 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.D) != null) {
                flexibleTextView.setText(spannableString.toString());
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void r(boolean z) {
        ConstraintLayout constraintLayout;
        g37<n75> g37 = this.h;
        if (g37 != null) {
            n75 a2 = g37.a();
            if (a2 != null && (constraintLayout = a2.r) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a06
    public void v0() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.r0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
