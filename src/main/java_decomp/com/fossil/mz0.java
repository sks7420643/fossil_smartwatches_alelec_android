package com.fossil;

import android.graphics.Matrix;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mz0 extends Lz0 {
    @DexIgnore
    @Override // com.fossil.Iz0, com.fossil.Nz0
    public float c(View view) {
        return view.getTransitionAlpha();
    }

    @DexIgnore
    @Override // com.fossil.Jz0, com.fossil.Nz0
    public void e(View view, Matrix matrix) {
        view.setAnimationMatrix(matrix);
    }

    @DexIgnore
    @Override // com.fossil.Kz0, com.fossil.Nz0
    public void f(View view, int i, int i2, int i3, int i4) {
        view.setLeftTopRightBottom(i, i2, i3, i4);
    }

    @DexIgnore
    @Override // com.fossil.Iz0, com.fossil.Nz0
    public void g(View view, float f) {
        view.setTransitionAlpha(f);
    }

    @DexIgnore
    @Override // com.fossil.Lz0, com.fossil.Nz0
    public void h(View view, int i) {
        view.setTransitionVisibility(i);
    }

    @DexIgnore
    @Override // com.fossil.Jz0, com.fossil.Nz0
    public void i(View view, Matrix matrix) {
        view.transformMatrixToGlobal(matrix);
    }

    @DexIgnore
    @Override // com.fossil.Jz0, com.fossil.Nz0
    public void j(View view, Matrix matrix) {
        view.transformMatrixToLocal(matrix);
    }
}
