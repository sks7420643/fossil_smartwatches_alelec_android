package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U62 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<R extends Z62> extends BasePendingResult<R> {
        @DexIgnore
        public /* final */ R q;

        @DexIgnore
        public Ai(R62 r62, R r) {
            super(r62);
            this.q = r;
        }

        @DexIgnore
        @Override // com.google.android.gms.common.api.internal.BasePendingResult
        public final R f(Status status) {
            return this.q;
        }
    }

    @DexIgnore
    public static <R extends Z62> T62<R> a(R r, R62 r62) {
        Rc2.l(r, "Result must not be null");
        Rc2.b(!r.a().D(), "Status code must not be SUCCESS");
        Ai ai = new Ai(r62, r);
        ai.j(r);
        return ai;
    }

    @DexIgnore
    public static T62<Status> b(Status status, R62 r62) {
        Rc2.l(status, "Result must not be null");
        V72 v72 = new V72(r62);
        v72.j(status);
        return v72;
    }
}
