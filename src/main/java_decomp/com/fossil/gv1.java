package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gv1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Hv1[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Gv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gv1 createFromParcel(Parcel parcel) {
            Parcelable[] readParcelableArray = parcel.readParcelableArray(Hv1.class.getClassLoader());
            if (readParcelableArray != null) {
                return new Gv1((Hv1[]) readParcelableArray);
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.preset.DevicePresetItem>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gv1[] newArray(int i) {
            return new Gv1[i];
        }
    }

    @DexIgnore
    public Gv1(Hv1[] hv1Arr) {
        this.b = hv1Arr;
        a();
    }

    @DexIgnore
    public final void a() {
        ArrayList arrayList = new ArrayList();
        for (Em1 em1 : Dm7.n(this.b, Em1.class)) {
            arrayList.addAll(em1.c());
        }
        Hv1[] hv1Arr = this.b;
        for (Hv1 hv1 : hv1Arr) {
            if (hv1 instanceof Sl1) {
                Mm7.w(((Sl1) hv1).c(), new Tb0(arrayList));
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final Hv1[] getPresetItems() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (Hv1 hv1 : this.b) {
            jSONArray.put(hv1.toJSONObject());
        }
        return G80.k(new JSONObject(), Jd0.g3, jSONArray);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelableArray(this.b, i);
        }
    }
}
