package com.fossil;

import android.os.RemoteException;
import com.fossil.M62;
import com.fossil.M62.Bi;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class W72<A extends M62.Bi, ResultT> {
    @DexIgnore
    public /* final */ B62[] a; // = null;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public abstract void a(A a2, Ot3<ResultT> ot3) throws RemoteException;

    @DexIgnore
    public boolean b() {
        return this.b;
    }

    @DexIgnore
    public final B62[] c() {
        return this.a;
    }
}
