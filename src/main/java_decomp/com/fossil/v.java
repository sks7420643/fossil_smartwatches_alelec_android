package com.fossil;

import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V extends Hh<J0> {
    @DexIgnore
    public V(Z z, Oh oh) {
        super(oh);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
    @Override // com.mapped.Hh
    public void bind(Mi mi, J0 j0) {
        J0 j02 = j0;
        mi.bindLong(1, (long) j02.b);
        String str = j02.c;
        if (str == null) {
            mi.bindNull(2);
        } else {
            mi.bindString(2, str);
        }
        mi.bindLong(3, (long) j02.d);
        mi.bindLong(4, (long) j02.e);
        byte[] bArr = j02.f;
        if (bArr == null) {
            mi.bindNull(5);
        } else {
            mi.bindBlob(5, bArr);
        }
        mi.bindLong(6, j02.g);
        mi.bindLong(7, j02.h);
        mi.bindLong(8, j02.a());
        mi.bindLong(9, j02.c() ? 1 : 0);
    }

    @DexIgnore
    @Override // com.mapped.Vh
    public String createQuery() {
        return "INSERT OR REPLACE INTO `DeviceFile` (`id`,`deviceMacAddress`,`fileType`,`fileIndex`,`rawData`,`fileLength`,`fileCrc`,`createdTimeStamp`,`isCompleted`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
    }
}
