package com.fossil;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.gv6;
import com.fossil.jn5;
import com.fossil.qb3;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev6 extends qv5 implements qb3.b, qb3.d {
    @DexIgnore
    public static /* final */ b m; // = new b(null);
    @DexIgnore
    public g37<l85> h;
    @DexIgnore
    public gv6 i;
    @DexIgnore
    public qb3 j;
    @DexIgnore
    public po4 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements qb3.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l85 f995a;
        @DexIgnore
        public /* final */ /* synthetic */ LatLng b;

        @DexIgnore
        public a(l85 l85, LatLng latLng) {
            this.f995a = l85;
            this.b = latLng;
        }

        @DexIgnore
        @Override // com.fossil.qb3.a
        public void onCancel() {
            FlexibleImageButton flexibleImageButton = this.f995a.r;
            pq7.b(flexibleImageButton, "binding.fbCurrentLocation");
            flexibleImageButton.setEnabled(true);
        }

        @DexIgnore
        @Override // com.fossil.qb3.a
        public void onFinish() {
            FlexibleImageButton flexibleImageButton = this.f995a.r;
            pq7.b(flexibleImageButton, "binding.fbCurrentLocation");
            flexibleImageButton.setEnabled(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ev6 a(double d, double d2, String str) {
            pq7.c(str, "address");
            ev6 ev6 = new ev6();
            Bundle bundle = new Bundle();
            bundle.putDouble("latitude", d);
            bundle.putDouble("longitude", d2);
            bundle.putString("address", str);
            ev6.setArguments(bundle);
            return ev6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sb3 {
        @DexIgnore
        public /* final */ /* synthetic */ ev6 b;

        @DexIgnore
        public c(ev6 ev6) {
            this.b = ev6;
        }

        @DexIgnore
        @Override // com.fossil.sb3
        public final void onMapReady(qb3 qb3) {
            this.b.j = qb3;
            qb3 qb32 = this.b.j;
            if (qb32 != null) {
                qb32.y(this.b);
            }
            qb3 qb33 = this.b.j;
            if (qb33 != null) {
                qb33.A(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev6 b;

        @DexIgnore
        public d(ev6 ev6) {
            this.b = ev6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev6 b;

        @DexIgnore
        public e(ev6 ev6) {
            this.b = ev6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ev6.M6(this.b).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l85 b;
        @DexIgnore
        public /* final */ /* synthetic */ ev6 c;

        @DexIgnore
        public f(l85 l85, ev6 ev6) {
            this.b = l85;
            this.c = ev6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Intent intent = new Intent();
            FlexibleTextView flexibleTextView = this.b.u;
            pq7.b(flexibleTextView, "binding.tvTitle");
            String obj = flexibleTextView.getText().toString();
            Bundle bundle = new Bundle();
            Location m = ev6.M6(this.c).m();
            if (m != null) {
                bundle.putParcelable(PlaceFields.LOCATION, m);
                bundle.putString("address", obj);
            }
            intent.putExtra(Constants.RESULT, bundle);
            FragmentActivity activity = this.c.getActivity();
            if (activity != null) {
                activity.setResult(100, intent);
            }
            FragmentActivity activity2 = this.c.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<gv6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ev6 f996a;

        @DexIgnore
        public g(ev6 ev6) {
            this.f996a = ev6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gv6.a aVar) {
            if (aVar != null) {
                Integer c = aVar.c();
                if (c != null) {
                    this.f996a.o(c.intValue(), "");
                }
                if (!(aVar.d() == null || aVar.e() == null)) {
                    this.f996a.E1(aVar.d(), aVar.e());
                }
                if (aVar.a() != null) {
                    ev6 ev6 = this.f996a;
                    String a2 = aVar.a();
                    if (a2 != null) {
                        ev6.L2(a2);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                Boolean b = aVar.b();
                if (b != null) {
                    b.booleanValue();
                    this.f996a.Q6();
                }
                Boolean g = aVar.g();
                if (g != null) {
                    if (g.booleanValue()) {
                        this.f996a.i();
                    } else {
                        this.f996a.h();
                    }
                }
                Boolean f = aVar.f();
                if (f != null) {
                    f.booleanValue();
                    Boolean f2 = aVar.f();
                    if (f2 == null) {
                        pq7.i();
                        throw null;
                    } else if (!f2.booleanValue()) {
                        this.f996a.j1();
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ gv6 M6(ev6 ev6) {
        gv6 gv6 = ev6.i;
        if (gv6 != null) {
            return gv6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void E1(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        g37<l85> g37 = this.h;
        if (g37 != null) {
            l85 a2 = g37.a();
            if (a2 != null && d2 != null && d3 != null) {
                ImageView imageView = a2.s;
                pq7.b(imageView, "binding.imgLocationPinUp");
                imageView.setVisibility(0);
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                qb3 qb3 = this.j;
                if (qb3 != null) {
                    FlexibleImageButton flexibleImageButton = a2.r;
                    pq7.b(flexibleImageButton, "binding.fbCurrentLocation");
                    flexibleImageButton.setEnabled(false);
                    qb3.f(pb3.d(latLng, 16.0f), new a(a2, latLng));
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void L2(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showAddress: address = " + str);
        g37<l85> g37 = this.h;
        if (g37 != null) {
            l85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "it.tvTitle");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        g37<l85> g37 = this.h;
        if (g37 != null) {
            l85 a2 = g37.a();
            if (a2 != null) {
                a2.q.d("flexible_button_primary");
                FlexibleButton flexibleButton = a2.q;
                pq7.b(flexibleButton, "it.btConfirm");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.q;
                pq7.b(flexibleButton2, "it.btConfirm");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.q;
                pq7.b(flexibleButton3, "it.btConfirm");
                flexibleButton3.setFocusable(true);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        gv6 gv6 = this.i;
        if (gv6 != null) {
            gv6.i().h(getViewLifecycleOwner(), new g(this));
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "hide Dialog Loading");
        a();
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "show Dialog Loading");
        b();
    }

    @DexIgnore
    public final void j1() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showLocationPermissionError");
    }

    @DexIgnore
    public final void o(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showErrorDialog");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        if (i2 == 111 && i3 == 2 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.qb3.b
    public void onCameraIdle() {
        ImageView imageView;
        g37<l85> g37 = this.h;
        if (g37 != null) {
            l85 a2 = g37.a();
            if (!(a2 == null || (imageView = a2.s) == null)) {
                imageView.setImageResource(2131231099);
            }
            le3 le3 = new le3();
            qb3 qb3 = this.j;
            CameraPosition h2 = qb3 != null ? qb3.h() : null;
            if (h2 != null) {
                le3.z0(h2.b);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("onCameraIdle: lat= ");
                pq7.b(le3, "markerOptions");
                sb.append(le3.p0().b);
                sb.append(", long= ");
                sb.append(le3.p0().c);
                local.d("MapPickerFragment", sb.toString());
                if (le3.p0().b != 0.0d && le3.p0().c != 0.0d) {
                    gv6 gv6 = this.i;
                    if (gv6 != null) {
                        gv6.g(le3.p0().b, le3.p0().c);
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qb3.d
    public void onCameraMoveStarted(int i2) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCameraMoveStarted");
        g37<l85> g37 = this.h;
        if (g37 != null) {
            l85 a2 = g37.a();
            if (a2 != null) {
                a2.s.setImageResource(2131231098);
                FlexibleTextView flexibleTextView = a2.u;
                pq7.b(flexibleTextView, "it.tvTitle");
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(getContext(), 2131886552);
                pq7.b(c2, "LanguageHelper.getString\u2026on_DropPin_Text__Loading)");
                String format = String.format(c2, Arrays.copyOf(new Object[0], 0));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.d("flexible_button_disabled");
                FlexibleButton flexibleButton = a2.q;
                pq7.b(flexibleButton, "it.btConfirm");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.q;
                pq7.b(flexibleButton2, "it.btConfirm");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.q;
                pq7.b(flexibleButton3, "it.btConfirm");
                flexibleButton3.setFocusable(false);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FragmentManager supportFragmentManager;
        pq7.c(layoutInflater, "inflater");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCreateView");
        l85 l85 = (l85) aq0.e(LayoutInflater.from(getContext()), 2131558583, viewGroup, false);
        PortfolioApp.h0.c().M().x().a(this);
        po4 po4 = this.k;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(gv6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026kerViewModel::class.java)");
            this.i = (gv6) a2;
            l85.q.d("flexible_button_disabled");
            FragmentActivity activity = getActivity();
            SupportMapFragment supportMapFragment = (SupportMapFragment) ((activity == null || (supportFragmentManager = activity.getSupportFragmentManager()) == null) ? null : supportFragmentManager.Y(2131362251));
            if (supportMapFragment != null) {
                supportMapFragment.v6(new c(this));
            }
            R6();
            this.h = new g37<>(this, l85);
            pq7.b(l85, "binding");
            return l85.n();
        }
        pq7.n("appViewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onResume");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        double d2;
        double d3;
        String string;
        pq7.c(view, "view");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onViewCreated");
        super.onViewCreated(view, bundle);
        g37<l85> g37 = this.h;
        if (g37 != null) {
            l85 a2 = g37.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                a2.q.setOnClickListener(new f(a2, this));
                String str = "";
                if (getArguments() != null) {
                    double d4 = requireArguments().containsKey("latitude") ? requireArguments().getDouble("latitude") : 0.0d;
                    d2 = requireArguments().containsKey("longitude") ? requireArguments().getDouble("longitude") : 0.0d;
                    if (requireArguments().containsKey("address") && (string = requireArguments().getString("address")) != null) {
                        str = string;
                    }
                    d3 = d4;
                } else {
                    d2 = 0.0d;
                    d3 = 0.0d;
                }
                if ((d3 == 0.0d || d2 == 0.0d) && TextUtils.isEmpty(str)) {
                    gv6 gv6 = this.i;
                    if (gv6 == null) {
                        pq7.n("mViewModel");
                        throw null;
                    } else if (!gv6.d()) {
                    } else {
                        if (!jn5.c(jn5.b, requireActivity(), jn5.a.FIND_DEVICE, false, true, false, null, 52, null)) {
                            FLogger.INSTANCE.getLocal().d("MapPickerFragment", "No permission granted");
                            return;
                        }
                        gv6 gv62 = this.i;
                        if (gv62 != null) {
                            gv62.j();
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("MapPickerFragment", "initStartLocation lat " + d3 + " long " + d2 + " address " + str);
                    gv6 gv63 = this.i;
                    if (gv63 != null) {
                        gv63.l(d3, d2, str);
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
