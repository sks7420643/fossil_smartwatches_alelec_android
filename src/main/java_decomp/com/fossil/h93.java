package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H93 implements D93 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.integration.disable_firebase_instance_id", false);

    @DexIgnore
    @Override // com.fossil.D93
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.D93
    public final boolean zzb() {
        return a.o().booleanValue();
    }
}
