package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av5 extends iq4<b, c, a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f344a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(int i, String str) {
            pq7.c(str, "errorMessage");
            this.f344a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f344a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ MFUser f345a;

        @DexIgnore
        public c(MFUser mFUser) {
            this.f345a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.f345a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase", f = "DownloadUserInfoUseCase.kt", l = {22}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ av5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(av5 av5, qn7 qn7) {
            super(qn7);
            this.this$0 = av5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = av5.class.getSimpleName();
        pq7.b(simpleName, "DownloadUserInfoUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public av5(UserRepository userRepository) {
        pq7.c(userRepository, "userRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.av5.b r7, com.fossil.qn7<java.lang.Object> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.fossil.av5.d
            if (r0 == 0) goto L_0x0041
            r0 = r8
            com.fossil.av5$d r0 = (com.fossil.av5.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0041
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0050
            if (r0 != r5) goto L_0x0048
            java.lang.Object r0 = r1.L$1
            com.fossil.av5$b r0 = (com.fossil.av5.b) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.av5 r0 = (com.fossil.av5) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002c:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.av5$c r1 = new com.fossil.av5$c
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            r1.<init>(r0)
            r0 = r1
        L_0x0040:
            return r0
        L_0x0041:
            com.fossil.av5$d r0 = new com.fossil.av5$d
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0048:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.av5.e
            java.lang.String r4 = "running UseCase"
            r0.d(r2, r4)
            com.portfolio.platform.data.source.UserRepository r0 = r6.d
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r0 = r0.loadUserInfo(r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0040
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0090
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x008d
            java.lang.String r0 = r0.getMessage()
            if (r0 == 0) goto L_0x008d
        L_0x0086:
            com.fossil.av5$a r1 = new com.fossil.av5$a
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x0040
        L_0x008d:
            java.lang.String r0 = ""
            goto L_0x0086
        L_0x0090:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.av5.k(com.fossil.av5$b, com.fossil.qn7):java.lang.Object");
    }
}
