package com.fossil;

import com.fossil.M62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gs3 implements M62.Di.Cii, M62.Di {
    @DexIgnore
    public static /* final */ Gs3 k; // = new Gs3(false, false, null, false, null, null, false, null, null);
    @DexIgnore
    public /* final */ boolean b; // = false;
    @DexIgnore
    public /* final */ boolean c; // = false;
    @DexIgnore
    public /* final */ String d; // = null;
    @DexIgnore
    public /* final */ boolean e; // = false;
    @DexIgnore
    public /* final */ String f; // = null;
    @DexIgnore
    public /* final */ String g; // = null;
    @DexIgnore
    public /* final */ boolean h; // = false;
    @DexIgnore
    public /* final */ Long i; // = null;
    @DexIgnore
    public /* final */ Long j; // = null;

    @DexIgnore
    public Gs3(boolean z, boolean z2, String str, boolean z3, String str2, String str3, boolean z4, Long l, Long l2) {
    }

    @DexIgnore
    public final Long a() {
        return this.i;
    }

    @DexIgnore
    public final String c() {
        return this.f;
    }

    @DexIgnore
    public final String d() {
        return this.g;
    }

    @DexIgnore
    public final Long e() {
        return this.j;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Gs3)) {
            return false;
        }
        Gs3 gs3 = (Gs3) obj;
        return this.b == gs3.b && this.c == gs3.c && Pc2.a(this.d, gs3.d) && this.e == gs3.e && this.h == gs3.h && Pc2.a(this.f, gs3.f) && Pc2.a(this.g, gs3.g) && Pc2.a(this.i, gs3.i) && Pc2.a(this.j, gs3.j);
    }

    @DexIgnore
    public final String f() {
        return this.d;
    }

    @DexIgnore
    public final boolean g() {
        return this.e;
    }

    @DexIgnore
    public final boolean h() {
        return this.c;
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(Boolean.valueOf(this.b), Boolean.valueOf(this.c), this.d, Boolean.valueOf(this.e), Boolean.valueOf(this.h), this.f, this.g, this.i, this.j);
    }

    @DexIgnore
    public final boolean i() {
        return this.b;
    }

    @DexIgnore
    public final boolean j() {
        return this.h;
    }
}
