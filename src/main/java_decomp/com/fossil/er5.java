package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Er5 implements Factory<BuddyChallengeManager> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<Tt4> b;
    @DexIgnore
    public /* final */ Provider<FriendRepository> c;
    @DexIgnore
    public /* final */ Provider<An4> d;

    @DexIgnore
    public Er5(Provider<PortfolioApp> provider, Provider<Tt4> provider2, Provider<FriendRepository> provider3, Provider<An4> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static Er5 a(Provider<PortfolioApp> provider, Provider<Tt4> provider2, Provider<FriendRepository> provider3, Provider<An4> provider4) {
        return new Er5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static BuddyChallengeManager c(PortfolioApp portfolioApp, Tt4 tt4, FriendRepository friendRepository, An4 an4) {
        return new BuddyChallengeManager(portfolioApp, tt4, friendRepository, an4);
    }

    @DexIgnore
    public BuddyChallengeManager b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
