package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nf5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;

    @DexIgnore
    public Nf5(Object obj, View view, int i, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleTextView;
    }

    @DexIgnore
    @Deprecated
    public static Nf5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Nf5) ViewDataBinding.p(layoutInflater, 2131558709, viewGroup, z, obj);
    }

    @DexIgnore
    public static Nf5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
