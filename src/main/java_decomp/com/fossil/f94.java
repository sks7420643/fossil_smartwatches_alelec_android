package com.fossil;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F94 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicLong b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends N84 {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable b;

            @DexIgnore
            public Aii(Ai ai, Runnable runnable) {
                this.b = runnable;
            }

            @DexIgnore
            @Override // com.fossil.N84
            public void a() {
                this.b.run();
            }
        }

        @DexIgnore
        public Ai(String str, AtomicLong atomicLong) {
            this.a = str;
            this.b = atomicLong;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new Aii(this, runnable));
            newThread.setName(this.a + this.b.getAndIncrement());
            return newThread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends N84 {
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ TimeUnit e;

        @DexIgnore
        public Bi(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.b = str;
            this.c = executorService;
            this.d = j;
            this.e = timeUnit;
        }

        @DexIgnore
        @Override // com.fossil.N84
        public void a() {
            try {
                X74 f = X74.f();
                f.b("Executing shutdown hook for " + this.b);
                this.c.shutdown();
                if (!this.c.awaitTermination(this.d, this.e)) {
                    X74 f2 = X74.f();
                    f2.b(this.b + " did not shut down in the allocated time. Requesting immediate shutdown.");
                    this.c.shutdownNow();
                }
            } catch (InterruptedException e2) {
                X74.f().b(String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", this.b));
                this.c.shutdownNow();
            }
        }
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService) {
        b(str, executorService, 2, TimeUnit.SECONDS);
    }

    @DexIgnore
    public static final void b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        Bi bi = new Bi(str, executorService, j, timeUnit);
        runtime.addShutdownHook(new Thread(bi, "Crashlytics Shutdown Hook for " + str));
    }

    @DexIgnore
    public static ExecutorService c(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(d(str));
        a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    @DexIgnore
    public static final ThreadFactory d(String str) {
        return new Ai(str, new AtomicLong(1));
    }
}
