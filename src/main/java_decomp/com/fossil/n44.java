package com.fossil;

import com.fossil.B34;
import com.fossil.C34;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N44<K, V> extends T24<K, V> {
    @DexIgnore
    public static /* final */ N44<Object, Object> EMPTY; // = new N44<>(null, null, A34.EMPTY_ENTRY_ARRAY, 0, 0);
    @DexIgnore
    public static /* final */ double MAX_LOAD_FACTOR; // = 1.2d;
    @DexIgnore
    public /* final */ transient B34<K, V>[] f;
    @DexIgnore
    public /* final */ transient B34<K, V>[] g;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] h;
    @DexIgnore
    public /* final */ transient int i;
    @DexIgnore
    public /* final */ transient int j;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient T24<V, K> k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends T24<V, K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Aii extends C34<V, K> {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Aiii extends S24<Map.Entry<V, K>> {
                @DexIgnore
                public Aiii() {
                }

                @DexIgnore
                @Override // com.fossil.S24
                public U24<Map.Entry<V, K>> delegateCollection() {
                    return Aii.this;
                }

                @DexIgnore
                @Override // java.util.List
                public Map.Entry<V, K> get(int i) {
                    Map.Entry entry = N44.this.h[i];
                    return X34.e(entry.getValue(), entry.getKey());
                }
            }

            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.fossil.H34
            public Y24<Map.Entry<V, K>> createAsList() {
                return new Aiii();
            }

            @DexIgnore
            @Override // com.fossil.C34, com.fossil.H34
            public int hashCode() {
                return N44.this.j;
            }

            @DexIgnore
            @Override // com.fossil.C34, com.fossil.H34
            public boolean isHashCodeFast() {
                return true;
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
            public H54<Map.Entry<V, K>> iterator() {
                return asList().iterator();
            }

            @DexIgnore
            @Override // com.fossil.C34
            public A34<V, K> map() {
                return Bi.this;
            }
        }

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.A34
        public H34<Map.Entry<V, K>> createEntrySet() {
            return new Aii();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.A34
        public K get(Object obj) {
            if (!(obj == null || N44.this.g == null)) {
                for (B34 b34 = N44.this.g[R24.b(obj.hashCode()) & N44.this.i]; b34 != null; b34 = b34.getNextInValueBucket()) {
                    if (obj.equals(b34.getValue())) {
                        return (K) b34.getKey();
                    }
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.T24, com.fossil.T24
        public T24<K, V> inverse() {
            return N44.this;
        }

        @DexIgnore
        @Override // com.fossil.A34
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        public int size() {
            return inverse().size();
        }

        @DexIgnore
        @Override // com.fossil.T24, com.fossil.A34
        public Object writeReplace() {
            return new Ci(N44.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;
        @DexIgnore
        public /* final */ T24<K, V> forward;

        @DexIgnore
        public Ci(T24<K, V> t24) {
            this.forward = t24;
        }

        @DexIgnore
        public Object readResolve() {
            return this.forward.inverse();
        }
    }

    @DexIgnore
    public N44(B34<K, V>[] b34Arr, B34<K, V>[] b34Arr2, Map.Entry<K, V>[] entryArr, int i2, int i3) {
        this.f = b34Arr;
        this.g = b34Arr2;
        this.h = entryArr;
        this.i = i2;
        this.j = i3;
    }

    @DexIgnore
    public static void b(Object obj, Map.Entry<?, ?> entry, B34<?, ?> b34) {
        while (b34 != null) {
            A34.checkNoConflict(!obj.equals(b34.getValue()), "value", entry, b34);
            b34 = b34.getNextInValueBucket();
        }
    }

    @DexIgnore
    public static <K, V> N44<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> N44<K, V> fromEntryArray(int i2, Map.Entry<K, V>[] entryArr) {
        I14.p(i2, entryArr.length);
        int a2 = R24.a(i2, 1.2d);
        int i3 = a2 - 1;
        B34[] createEntryArray = B34.createEntryArray(a2);
        B34[] createEntryArray2 = B34.createEntryArray(a2);
        Map.Entry<K, V>[] createEntryArray3 = i2 == entryArr.length ? entryArr : B34.createEntryArray(i2);
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            Map.Entry<K, V> entry = entryArr[i5];
            K key = entry.getKey();
            V value = entry.getValue();
            A24.a(key, value);
            int hashCode = key.hashCode();
            int hashCode2 = value.hashCode();
            int b = R24.b(hashCode) & i3;
            int b2 = R24.b(hashCode2) & i3;
            B34 b34 = createEntryArray[b];
            P44.checkNoConflictInKeyBucket(key, entry, b34);
            B34 b342 = createEntryArray2[b2];
            b(value, entry, b342);
            B34 b343 = (b342 == null && b34 == null) ? (entry instanceof B34) && ((B34) entry).isReusable() ? (B34) entry : new B34(key, value) : new B34.Ai(key, value, b34, b342);
            createEntryArray[b] = b343;
            createEntryArray2[b2] = b343;
            createEntryArray3[i5] = b343;
            i4 += hashCode ^ hashCode2;
        }
        return new N44<>(createEntryArray, createEntryArray2, createEntryArray3, i3, i4);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H34<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? H34.of() : new C34.Bi(this, this.h);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34
    public V get(Object obj) {
        B34<K, V>[] b34Arr = this.f;
        if (b34Arr == null) {
            return null;
        }
        return (V) P44.get(obj, b34Arr, this.i);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public int hashCode() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.T24, com.fossil.T24
    public T24<V, K> inverse() {
        if (isEmpty()) {
            return T24.of();
        }
        T24<V, K> t24 = this.k;
        if (t24 != null) {
            return t24;
        }
        Bi bi = new Bi();
        this.k = bi;
        return bi;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.h.length;
    }
}
