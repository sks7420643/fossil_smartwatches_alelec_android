package com.fossil;

import com.fossil.Af1;
import com.fossil.Sc1;
import com.fossil.Wb1;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jd1 implements Sc1, Wb1.Ai<Object> {
    @DexIgnore
    public /* final */ Sc1.Ai b;
    @DexIgnore
    public /* final */ Tc1<?> c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public Mb1 f;
    @DexIgnore
    public List<Af1<File, ?>> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public volatile Af1.Ai<?> i;
    @DexIgnore
    public File j;
    @DexIgnore
    public Kd1 k;

    @DexIgnore
    public Jd1(Tc1<?> tc1, Sc1.Ai ai) {
        this.c = tc1;
        this.b = ai;
    }

    @DexIgnore
    public final boolean a() {
        return this.h < this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.Wb1.Ai
    public void b(Exception exc) {
        this.b.a(this.k, exc, this.i.c, Gb1.RESOURCE_DISK_CACHE);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 138
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    @DexIgnore
    @Override // com.fossil.Sc1
    public boolean c() {
        /*
        // Method dump skipped, instructions count: 302
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Jd1.c():boolean");
    }

    @DexIgnore
    @Override // com.fossil.Sc1
    public void cancel() {
        Af1.Ai<?> ai = this.i;
        if (ai != null) {
            ai.c.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.Wb1.Ai
    public void e(Object obj) {
        this.b.e(this.f, obj, this.i.c, Gb1.RESOURCE_DISK_CACHE, this.k);
    }
}
