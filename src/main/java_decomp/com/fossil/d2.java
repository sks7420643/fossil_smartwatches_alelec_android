package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D2 implements Parcelable.Creator<E2> {
    @DexIgnore
    public /* synthetic */ D2(Qg6 qg6) {
    }

    @DexIgnore
    public E2 a(Parcel parcel) {
        return new E2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public E2 createFromParcel(Parcel parcel) {
        return new E2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public E2[] newArray(int i) {
        return new E2[i];
    }
}
