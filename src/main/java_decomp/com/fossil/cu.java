package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Cu {
    c(new byte[]{8}),
    d(new byte[]{9}),
    e(new byte[]{10});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Cu(byte[] bArr) {
        this.b = bArr;
    }
}
