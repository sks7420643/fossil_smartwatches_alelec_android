package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.M47;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pp6 extends BaseFragment implements Op6, View.OnClickListener, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public G37<J75> g;
    @DexIgnore
    public Np6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Pp6.j;
        }

        @DexIgnore
        public final Pp6 b() {
            return new Pp6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Bi(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Ci(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String N6;
            if (Wr4.a.a().f()) {
                N6 = M47.a(M47.Ci.FAQ, null);
                Wg6.b(N6, "URLHelper.buildStaticPag\u2026per.StaticPage.FAQ, null)");
            } else {
                N6 = this.b.N6("https://support.fossil.com/hc/%s/categories/360000064626-Smartwatch-FAQ");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Pp6.k.a();
            local.d(a2, "FAQ URL = " + N6);
            this.b.P6(N6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Di(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String format;
            if (Wr4.a.a().f()) {
                format = M47.a(M47.Ci.REPAIR_CENTER, null);
                Wg6.b(format, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
            } else {
                Hr7 hr7 = Hr7.a;
                Locale a2 = Um5.a();
                Wg6.b(a2, "LanguageHelper.getLocale()");
                format = String.format("https://c.fossil.com/web/service_centers", Arrays.copyOf(new Object[]{a2.getLanguage()}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
            }
            FLogger.INSTANCE.getLocal().d(Pp6.k.a(), "Repair Center URL = https://c.fossil.com/web/service_centers");
            this.b.P6(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Ei(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (Wr4.a.a().n()) {
                String a2 = M47.a(M47.Ci.REPAIR_CENTER, null);
                Pp6 pp6 = this.b;
                Wg6.b(a2, "url");
                pp6.P6(a2);
                return;
            }
            this.b.M6().o();
            this.b.M6().p("Contact Us - From app [Fossil] - [Android]");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Fi(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String N6 = this.b.N6("https://support.fossil.com/hc/%s?wearablesChat=true");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Pp6.k.a();
            local.d(a2, "Chat URL = " + N6);
            this.b.P6(N6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Gi(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String format;
            if (Wr4.a.a().f()) {
                format = M47.a(M47.Ci.CALL, null);
                Wg6.b(format, "URLHelper.buildStaticPag\u2026er.StaticPage.CALL, null)");
            } else {
                Hr7 hr7 = Hr7.a;
                Locale a2 = Um5.a();
                Wg6.b(a2, "LanguageHelper.getLocale()");
                format = String.format("https://c.fossil.com/web/call", Arrays.copyOf(new Object[]{a2.getLanguage()}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = Pp6.k.a();
            local.d(a3, "Call Us URL = " + format);
            this.b.P6(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pp6 b;

        @DexIgnore
        public Hi(Pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                DeleteAccountActivity.a aVar = DeleteAccountActivity.B;
                FragmentActivity requireActivity = this.b.requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    /*
    static {
        String simpleName = Pp6.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "HelpFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Np6 np6) {
        O6(np6);
    }

    @DexIgnore
    public final Np6 M6() {
        Np6 np6 = this.h;
        if (np6 != null) {
            return np6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final String N6(String str) {
        Wg6.c(str, "featureLink");
        Locale a2 = Um5.a();
        Wg6.b(a2, "LanguageHelper.getLocale()");
        String language = a2.getLanguage();
        Locale a3 = Um5.a();
        Wg6.b(a3, "LanguageHelper.getLocale()");
        String country = a3.getCountry();
        if (Wg6.a(language, "zh")) {
            if (Wg6.a(country, "tw")) {
                Hr7 hr7 = Hr7.a;
                Locale locale = Locale.US;
                Wg6.b(locale, "Locale.US");
                language = String.format(locale, "%s-%s", Arrays.copyOf(new Object[]{language, "tw"}, 2));
                Wg6.b(language, "java.lang.String.format(locale, format, *args)");
            } else {
                Hr7 hr72 = Hr7.a;
                Locale locale2 = Locale.US;
                Wg6.b(locale2, "Locale.US");
                language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, "cn"}, 2));
                Wg6.b(language, "java.lang.String.format(locale, format, *args)");
            }
        }
        Hr7 hr73 = Hr7.a;
        String format = String.format(str, Arrays.copyOf(new Object[]{language}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public void O6(Np6 np6) {
        Wg6.c(np6, "presenter");
        I14.l(np6);
        Wg6.b(np6, "Preconditions.checkNotNull(presenter)");
        this.h = np6;
    }

    @DexIgnore
    public final void P6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), j);
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = Wt6.x.a();
        local.d(a2, "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.R5(str, i2, intent);
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Op6
    public void j0(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        Wg6.c(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            Np6 np6 = this.h;
            if (np6 != null) {
                np6.n();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        Wg6.c(view, "v");
        if (view.getId() == 2131361851) {
            e0();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        J75 j75 = (J75) Aq0.f(LayoutInflater.from(getContext()), R.layout.fragment_help, null, false, A6());
        this.g = new G37<>(this, j75);
        Wg6.b(j75, "binding");
        return j75.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Np6 np6 = this.h;
        if (np6 != null) {
            np6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Np6 np6 = this.h;
        if (np6 != null) {
            np6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        String P = PortfolioApp.get.instance().P();
        G37<J75> g37 = this.g;
        if (g37 != null) {
            J75 a2 = g37.a();
            if (a2 != null) {
                String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
                if (!TextUtils.isEmpty(d)) {
                    int parseColor = Color.parseColor(d);
                    a2.J.setBackgroundColor(parseColor);
                    a2.K.setBackgroundColor(parseColor);
                }
                FlexibleTextView flexibleTextView = a2.B;
                Wg6.b(flexibleTextView, "binding.tvAppVersion");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131887105);
                Wg6.b(c, "LanguageHelper.getString\u2026w_Text__AppVersionNumber)");
                String format = String.format(c, Arrays.copyOf(new Object[]{P}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.setOnClickListener(new Bi(this, P));
                a2.r.setOnClickListener(new Ci(this, P));
                a2.s.setOnClickListener(new Di(this, P));
                a2.M.setOnClickListener(new Ei(this, P));
                a2.N.setOnClickListener(new Fi(this, P));
                a2.L.setOnClickListener(new Gi(this, P));
                a2.E.setOnClickListener(new Hi(this, P));
                if (!Wr4.a.a().c()) {
                    FlexibleTextView flexibleTextView2 = a2.H;
                    Wg6.b(flexibleTextView2, "binding.tvLiveChat");
                    flexibleTextView2.setVisibility(8);
                    CustomizeWidget customizeWidget = a2.N;
                    Wg6.b(customizeWidget, "binding.wcLiveChat");
                    customizeWidget.setVisibility(8);
                }
                if (!Wr4.a.a().a()) {
                    FlexibleTextView flexibleTextView3 = a2.D;
                    Wg6.b(flexibleTextView3, "binding.tvContacts");
                    flexibleTextView3.setVisibility(4);
                    ConstraintLayout constraintLayout = a2.u;
                    Wg6.b(constraintLayout, "binding.clContacts");
                    constraintLayout.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
