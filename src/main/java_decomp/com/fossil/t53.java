package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T53 implements Xw2<W53> {
    @DexIgnore
    public static T53 c; // = new T53();
    @DexIgnore
    public /* final */ Xw2<W53> b;

    @DexIgnore
    public T53() {
        this(Ww2.b(new V53()));
    }

    @DexIgnore
    public T53(Xw2<W53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((W53) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ W53 zza() {
        return this.b.zza();
    }
}
