package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Pc2;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yo2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Yo2> CREATOR; // = new Ap2();
    @DexIgnore
    public /* final */ List<DataType> b;

    @DexIgnore
    public Yo2(List<DataType> list) {
        this.b = list;
    }

    @DexIgnore
    public final List<DataType> c() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    public final String toString() {
        Pc2.Ai c = Pc2.c(this);
        c.a("dataTypes", this.b);
        return c.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.y(parcel, 1, Collections.unmodifiableList(this.b), false);
        Bd2.b(parcel, a2);
    }
}
