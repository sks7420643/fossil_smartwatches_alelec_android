package com.fossil;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.DragEvent;
import android.view.View;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R66 implements View.OnDragListener {
    @DexIgnore
    public Handler a;
    @DexIgnore
    public Ai b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(String str);

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        void c(String str);

        @DexIgnore
        boolean d(String str);

        @DexIgnore
        boolean e(View view, String str);
    }

    @DexIgnore
    public R66(Ai ai) {
        this.b = ai;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.DragEvent r3) {
        /*
            r2 = this;
            android.content.ClipDescription r0 = r3.getClipDescription()
            if (r0 == 0) goto L_0x0015
            java.lang.CharSequence r0 = r0.getLabel()
            java.lang.String r0 = r0.toString()
            int r1 = r0.hashCode()
            switch(r1) {
                case -1787753871: goto L_0x003f;
                case 137912893: goto L_0x0036;
                case 333273330: goto L_0x0026;
                case 672879678: goto L_0x0016;
                default: goto L_0x0015;
            }
        L_0x0015:
            return
        L_0x0016:
            java.lang.String r1 = "COMPLICATION"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
        L_0x001e:
            com.fossil.R66$Ai r1 = r2.b
            if (r1 == 0) goto L_0x0015
            r1.c(r0)
            goto L_0x0015
        L_0x0026:
            java.lang.String r1 = "SWAP_PRESET_COMPLICATION"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
        L_0x002e:
            com.fossil.R66$Ai r1 = r2.b
            if (r1 == 0) goto L_0x0015
            r1.c(r0)
            goto L_0x0015
        L_0x0036:
            java.lang.String r1 = "SWAP_PRESET_WATCH_APP"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
            goto L_0x002e
        L_0x003f:
            java.lang.String r1 = "WATCH_APP"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
            goto L_0x001e
            switch-data {-1787753871->0x003f, 137912893->0x0036, 333273330->0x0026, 672879678->0x0016, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.R66.a(android.view.DragEvent):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(android.view.DragEvent r4) {
        /*
            r3 = this;
            android.content.ClipDescription r0 = r4.getClipDescription()
            if (r0 == 0) goto L_0x0015
            java.lang.CharSequence r0 = r0.getLabel()
            java.lang.String r0 = r0.toString()
            int r1 = r0.hashCode()
            switch(r1) {
                case -1787753871: goto L_0x0047;
                case 137912893: goto L_0x003e;
                case 333273330: goto L_0x0026;
                case 672879678: goto L_0x0016;
                default: goto L_0x0015;
            }
        L_0x0015:
            return
        L_0x0016:
            java.lang.String r1 = "COMPLICATION"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
        L_0x001e:
            com.fossil.R66$Ai r1 = r3.b
            if (r1 == 0) goto L_0x0015
            r1.a(r0)
            goto L_0x0015
        L_0x0026:
            java.lang.String r1 = "SWAP_PRESET_COMPLICATION"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
        L_0x002e:
            android.os.Handler r1 = r3.a
            if (r1 == 0) goto L_0x0036
            r2 = 0
            r1.removeCallbacksAndMessages(r2)
        L_0x0036:
            com.fossil.R66$Ai r1 = r3.b
            if (r1 == 0) goto L_0x0015
            r1.a(r0)
            goto L_0x0015
        L_0x003e:
            java.lang.String r1 = "SWAP_PRESET_WATCH_APP"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
            goto L_0x002e
        L_0x0047:
            java.lang.String r1 = "WATCH_APP"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0015
            goto L_0x001e
            switch-data {-1787753871->0x0047, 137912893->0x003e, 333273330->0x0026, 672879678->0x0016, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.R66.b(android.view.DragEvent):void");
    }

    @DexIgnore
    public final boolean c(View view, DragEvent dragEvent) {
        ClipDescription description;
        Intent intent;
        Ai ai;
        Handler handler = this.a;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        ClipData clipData = dragEvent.getClipData();
        if (clipData == null || clipData.getItemCount() == 0 || (description = clipData.getDescription()) == null) {
            return false;
        }
        String obj = description.getLabel().toString();
        switch (obj.hashCode()) {
            case -1787753871:
                if (!obj.equals("WATCH_APP")) {
                    return false;
                }
                return d(view, clipData);
            case 137912893:
                if (!obj.equals("SWAP_PRESET_WATCH_APP")) {
                    return false;
                }
                try {
                    ClipData.Item itemAt = clipData.getItemAt(0);
                    Wg6.b(itemAt, "clipData.getItemAt(0)");
                    intent = itemAt.getIntent();
                    if (intent == null && (ai = this.b) != null) {
                        String stringExtra = intent.getStringExtra("KEY_POSITION");
                        Wg6.b(stringExtra, "it.getStringExtra(KEY_POSITION)");
                        return ai.d(stringExtra);
                    }
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("CustomizeDragListener", "processDrop - e=" + e);
                    return false;
                }
            case 333273330:
                if (!obj.equals("SWAP_PRESET_COMPLICATION")) {
                    return false;
                }
                ClipData.Item itemAt2 = clipData.getItemAt(0);
                Wg6.b(itemAt2, "clipData.getItemAt(0)");
                intent = itemAt2.getIntent();
                return intent == null ? false : false;
            case 672879678:
                if (!obj.equals("COMPLICATION")) {
                    return false;
                }
                return d(view, clipData);
            default:
                return false;
        }
    }

    @DexIgnore
    public final boolean d(View view, ClipData clipData) {
        String str;
        Intent intent;
        ClipData.Item itemAt = clipData.getItemAt(0);
        if (itemAt == null || (intent = itemAt.getIntent()) == null || (str = intent.getStringExtra("KEY_ID")) == null) {
            str = "";
        }
        if (itemAt == null) {
            return false;
        }
        Ai ai = this.b;
        return ai != null ? ai.e(view, str) : false;
    }

    @DexIgnore
    public boolean onDrag(View view, DragEvent dragEvent) {
        Ai ai;
        Integer num = null;
        if (dragEvent != null) {
            int action = dragEvent.getAction();
            ClipData clipData = dragEvent.getClipData();
            ClipDescription clipDescription = dragEvent.getClipDescription();
            CharSequence label = clipDescription != null ? clipDescription.getLabel() : null;
            if (clipData != null) {
                int i = 0;
                int itemCount = clipData.getItemCount() - 1;
                if (itemCount >= 0) {
                    while (true) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("onDrag - v=");
                        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
                        sb.append(", action=");
                        sb.append(action);
                        sb.append(", labelDesc=");
                        sb.append(label);
                        sb.append(", item=");
                        sb.append(clipData.getItemAt(i));
                        local.d("CustomizeDragListener", sb.toString());
                        if (i == itemCount) {
                            break;
                        }
                        i++;
                    }
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("onDrag - v=");
                if (view != null) {
                    num = Integer.valueOf(view.getId());
                }
                sb2.append(num);
                sb2.append(", action=");
                sb2.append(action);
                sb2.append(", labelDesc=");
                sb2.append(label);
                local2.d("CustomizeDragListener", sb2.toString());
            }
            if (action != 1) {
                if (action != 3) {
                    if (action != 4) {
                        if (action == 5) {
                            a(dragEvent);
                        } else if (action == 6) {
                            b(dragEvent);
                        }
                    } else if (!dragEvent.getResult() && (ai = this.b) != null) {
                        ai.b();
                    }
                } else if (view != null) {
                    c(view, dragEvent);
                }
            } else if (this.a == null) {
                this.a = new Handler(Looper.getMainLooper());
            }
        }
        return true;
    }
}
