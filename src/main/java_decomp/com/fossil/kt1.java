package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ry1 d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Kt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kt1 createFromParcel(Parcel parcel) {
            boolean z = true;
            Parcelable readParcelable = parcel.readParcelable(Hq1.class.getClassLoader());
            if (readParcelable != null) {
                Wg6.b(readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                Hq1 hq1 = (Hq1) readParcelable;
                Nt1 nt1 = (Nt1) parcel.readParcelable(Nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(Ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    Ry1 ry1 = (Ry1) readParcelable2;
                    int readInt = parcel.readInt();
                    if (parcel.readInt() != 1) {
                        z = false;
                    }
                    return new Kt1(hq1, nt1, ry1, readInt, z);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kt1[] newArray(int i) {
            return new Kt1[i];
        }
    }

    @DexIgnore
    public Kt1(Hq1 hq1, Nt1 nt1, Ry1 ry1, int i, boolean z) {
        super(hq1, nt1);
        this.e = z;
        this.f = i;
        this.d = ry1;
    }

    @DexIgnore
    public Kt1(Hq1 hq1, Ry1 ry1, int i) {
        super(hq1, null);
        this.e = true;
        this.f = i;
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return G80.k(G80.k(G80.k(super.a(), Jd0.t3, this.d.toString()), Jd0.w3, Boolean.valueOf(this.e)), Jd0.x3, Integer.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        try {
            I9 i9 = I9.d;
            X90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9.a(s, ry1, new Ib0(((Lq1) deviceRequest).c(), new Ry1(this.d.getMajor(), this.d.getMinor()), this.e, this.f).a());
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (Sx1 e2) {
            D90.i.i(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Kt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Kt1 kt1 = (Kt1) obj;
            if (!Wg6.a(this.d, kt1.d)) {
                return false;
            }
            if (this.e != kt1.e) {
                return false;
            }
            return this.f == kt1.f;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeTravelMicroAppData");
    }

    @DexIgnore
    public final Ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    public final boolean getShipHandsToTwelve() {
        return this.e;
    }

    @DexIgnore
    public final int getTravelTimeInMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.d.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Boolean.valueOf(this.e).hashCode()) * 31) + Integer.valueOf(this.f).hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(this.e ? 1 : 0);
        }
    }
}
