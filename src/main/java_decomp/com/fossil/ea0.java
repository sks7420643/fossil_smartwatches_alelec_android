package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Ea0 extends Enum<Ea0> {
    @DexIgnore
    public static /* final */ Ea0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ Ea0[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Ea0 ea0 = new Ea0("FULL", 0, (byte) 0);
        c = ea0;
        d = new Ea0[]{ea0, new Ea0("HALF", 1, (byte) 1), new Ea0("QUARTER", 2, (byte) 2), new Ea0("EIGHTH", 3, (byte) 3), new Ea0("SIXTEENTH", 4, (byte) 4)};
    }
    */

    @DexIgnore
    public Ea0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Ea0 valueOf(String str) {
        return (Ea0) Enum.valueOf(Ea0.class, str);
    }

    @DexIgnore
    public static Ea0[] values() {
        return (Ea0[]) d.clone();
    }
}
