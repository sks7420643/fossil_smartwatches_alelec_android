package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A40 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ R40 b;
    @DexIgnore
    public /* final */ /* synthetic */ Lp c;
    @DexIgnore
    public /* final */ /* synthetic */ float d;

    @DexIgnore
    public A40(R40 r40, Lp lp, float f) {
        this.b = r40;
        this.c = lp;
        this.d = f;
    }

    @DexIgnore
    public final void run() {
        E60.p0(this.b.c, Ky1.DEBUG, Ey1.a(this.c.y), "Progress: %.4f.", Float.valueOf(this.d));
        this.b.b.y(this.d);
    }
}
