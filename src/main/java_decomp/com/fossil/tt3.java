package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tt3<TResult, TContinuationResult> implements Hu3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Ft3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ Lu3<TContinuationResult> c;

    @DexIgnore
    public Tt3(Executor executor, Ft3<TResult, TContinuationResult> ft3, Lu3<TContinuationResult> lu3) {
        this.a = executor;
        this.b = ft3;
        this.c = lu3;
    }

    @DexIgnore
    @Override // com.fossil.Hu3
    public final void a(Nt3<TResult> nt3) {
        this.a.execute(new Vt3(this, nt3));
    }
}
