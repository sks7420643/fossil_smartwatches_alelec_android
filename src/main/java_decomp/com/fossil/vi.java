package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vi extends Lp {
    @DexIgnore
    public Zk1 C;

    @DexIgnore
    public Vi(K5 k5, I60 i60, String str) {
        super(k5, i60, Yp.e, str, false, 16);
        this.C = new Zk1(k5.C(), k5.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.i(this, new Lu(this.w), new Uh(this), new Hi(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(super.E(), Jd0.b, this.C.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.C;
    }
}
