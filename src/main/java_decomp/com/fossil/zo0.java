package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zo0 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Zo0 a;

        @DexIgnore
        public Ai(Zo0 zo0) {
            this.a = zo0;
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            Yo0 a2 = this.a.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.D0();
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List<Yo0> b = this.a.b(str, i);
            if (b == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = b.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(b.get(i2).D0());
            }
            return arrayList;
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.a.e(i, i2, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Ai {
        @DexIgnore
        public Bi(Zo0 zo0) {
            super(zo0);
        }

        @DexIgnore
        public AccessibilityNodeInfo findFocus(int i) {
            Yo0 c = this.a.c(i);
            if (c == null) {
                return null;
            }
            return c.D0();
        }
    }

    @DexIgnore
    public Zo0() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            this.a = new Bi(this);
        } else if (i >= 16) {
            this.a = new Ai(this);
        } else {
            this.a = null;
        }
    }

    @DexIgnore
    public Zo0(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public Yo0 a(int i) {
        return null;
    }

    @DexIgnore
    public List<Yo0> b(String str, int i) {
        return null;
    }

    @DexIgnore
    public Yo0 c(int i) {
        return null;
    }

    @DexIgnore
    public Object d() {
        return this.a;
    }

    @DexIgnore
    public boolean e(int i, int i2, Bundle bundle) {
        return false;
    }
}
