package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eu5 implements Factory<GetUserLocation> {
    @DexIgnore
    public /* final */ Provider<LocationSource> a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;

    @DexIgnore
    public Eu5(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Eu5 a(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        return new Eu5(provider, provider2);
    }

    @DexIgnore
    public static GetUserLocation c(LocationSource locationSource, PortfolioApp portfolioApp) {
        return new GetUserLocation(locationSource, portfolioApp);
    }

    @DexIgnore
    public GetUserLocation b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
