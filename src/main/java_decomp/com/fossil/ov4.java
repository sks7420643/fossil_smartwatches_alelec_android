package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ov4 implements MembersInjector<Nv4> {
    @DexIgnore
    public static void a(Nv4 nv4, Po4 po4) {
        nv4.i = po4;
    }
}
