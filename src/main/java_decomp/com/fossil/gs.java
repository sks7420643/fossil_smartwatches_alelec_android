package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gs extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Es CREATOR; // = new Es(null);
    @DexIgnore
    public /* final */ N6 b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public Gs(N6 n6, long j, byte[] bArr) {
        this.b = n6;
        this.c = j;
        this.d = bArr;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.D2, this.b.b), Jd0.Q0, Double.valueOf(Hy1.f(this.c))), Jd0.Y2, Dy1.e(this.d, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeLong(this.c);
        parcel.writeByteArray(this.d);
    }
}
