package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.fossil.Dl7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface H81<T extends View> extends G81 {
    @DexIgnore
    public static final Ai a = Ai.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ Ai a; // = new Ai();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements H81<T> {
            @DexIgnore
            public /* final */ T b;
            @DexIgnore
            public /* final */ boolean c;
            @DexIgnore
            public /* final */ /* synthetic */ View d;

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: android.view.View */
            /* JADX WARN: Multi-variable type inference failed */
            public Aii(View view, boolean z) {
                this.d = view;
                this.b = view;
                this.c = z;
            }

            @DexIgnore
            @Override // com.fossil.H81
            public boolean a() {
                return this.c;
            }

            @DexIgnore
            @Override // com.fossil.G81
            public Object b(Xe6<? super F81> xe6) {
                return Bi.h(this, xe6);
            }

            @DexIgnore
            /* JADX WARN: Type inference failed for: r0v0, types: [T, T extends android.view.View] */
            @Override // com.fossil.H81
            public T getView() {
                return this.b;
            }
        }

        @DexIgnore
        public static /* synthetic */ H81 b(Ai ai, View view, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = true;
            }
            return ai.a(view, z);
        }

        @DexIgnore
        public final <T extends View> H81<T> a(T t, boolean z) {
            Wg6.c(t, "view");
            return new Aii(t, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public /* final */ /* synthetic */ ViewTreeObserver c;
            @DexIgnore
            public /* final */ /* synthetic */ Lk6 d;
            @DexIgnore
            public /* final */ /* synthetic */ H81 e;

            @DexIgnore
            public Aii(ViewTreeObserver viewTreeObserver, Lk6 lk6, H81 h81) {
                this.c = viewTreeObserver;
                this.d = lk6;
                this.e = h81;
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (!this.b) {
                    this.b = true;
                    H81 h81 = this.e;
                    ViewTreeObserver viewTreeObserver = this.c;
                    Wg6.b(viewTreeObserver, "viewTreeObserver");
                    Bi.g(h81, viewTreeObserver, this);
                    C81 c81 = new C81(Bs7.d(Bi.f(this.e, false), 1), Bs7.d(Bi.e(this.e, false), 1));
                    Lk6 lk6 = this.d;
                    Dl7.Ai ai = Dl7.Companion;
                    lk6.resumeWith(Dl7.constructor-impl(c81));
                }
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Qq7 implements Hg6<Throwable, Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Aii $preDrawListener;
            @DexIgnore
            public /* final */ /* synthetic */ ViewTreeObserver $viewTreeObserver;
            @DexIgnore
            public /* final */ /* synthetic */ H81 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(ViewTreeObserver viewTreeObserver, Aii aii, H81 h81) {
                super(1);
                this.$viewTreeObserver = viewTreeObserver;
                this.$preDrawListener = aii;
                this.this$0 = h81;
            }

            @DexIgnore
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
                invoke(th);
                return Cd6.a;
            }

            @DexIgnore
            public final void invoke(Throwable th) {
                H81 h81 = this.this$0;
                ViewTreeObserver viewTreeObserver = this.$viewTreeObserver;
                Wg6.b(viewTreeObserver, "viewTreeObserver");
                Bi.g(h81, viewTreeObserver, this.$preDrawListener);
            }
        }

        @DexIgnore
        public static <T extends View> int d(H81<T> h81, int i, int i2, int i3, boolean z) {
            int i4 = i - i3;
            if (i4 > 0) {
                return i4;
            }
            int i5 = i2 - i3;
            if (i5 > 0) {
                return i5;
            }
            if (z || i != -2) {
                return -1;
            }
            if (Q81.c.a() && Q81.c.b() <= 4) {
                Log.println(4, "ViewSizeResolver", "A View's width and/or height is set to WRAP_CONTENT. Falling back to the size of the display.");
            }
            Context context = h81.getView().getContext();
            Wg6.b(context, "view.context");
            Resources resources = context.getResources();
            Wg6.b(resources, "view.context.resources");
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            return Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
        }

        @DexIgnore
        public static <T extends View> int e(H81<T> h81, boolean z) {
            ViewGroup.LayoutParams layoutParams = h81.getView().getLayoutParams();
            return d(h81, layoutParams != null ? layoutParams.height : -1, h81.getView().getHeight(), h81.a() ? h81.getView().getPaddingTop() + h81.getView().getPaddingBottom() : 0, z);
        }

        @DexIgnore
        public static <T extends View> int f(H81<T> h81, boolean z) {
            ViewGroup.LayoutParams layoutParams = h81.getView().getLayoutParams();
            return d(h81, layoutParams != null ? layoutParams.width : -1, h81.getView().getWidth(), h81.a() ? h81.getView().getPaddingLeft() + h81.getView().getPaddingRight() : 0, z);
        }

        @DexIgnore
        public static <T extends View> void g(H81<T> h81, ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnPreDrawListener onPreDrawListener) {
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            } else {
                h81.getView().getViewTreeObserver().removeOnPreDrawListener(onPreDrawListener);
            }
        }

        @DexIgnore
        public static <T extends View> Object h(H81<T> h81, Xe6<? super F81> xe6) {
            boolean isLayoutRequested = h81.getView().isLayoutRequested();
            int f = f(h81, isLayoutRequested);
            int e = e(h81, isLayoutRequested);
            if (f > 0 && e > 0) {
                return new C81(f, e);
            }
            Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
            ViewTreeObserver viewTreeObserver = h81.getView().getViewTreeObserver();
            Aii aii = new Aii(viewTreeObserver, lu7, h81);
            viewTreeObserver.addOnPreDrawListener(aii);
            lu7.e(new Bii(viewTreeObserver, aii, h81));
            Object t = lu7.t();
            if (t != Yn7.d()) {
                return t;
            }
            Go7.c(xe6);
            return t;
        }
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    T getView();
}
