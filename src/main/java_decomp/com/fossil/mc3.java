package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mc3 extends Ds2 implements Lc3 {
    @DexIgnore
    public Mc3() {
        super("com.google.android.gms.maps.internal.IOnMapLoadedCallback");
    }

    @DexIgnore
    @Override // com.fossil.Ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onMapLoaded();
        parcel2.writeNoException();
        return true;
    }
}
