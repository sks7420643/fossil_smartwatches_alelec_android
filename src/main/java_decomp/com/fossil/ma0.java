package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ma0 extends Va0 {
    @DexIgnore
    public static /* final */ La0 CREATOR; // = new La0(null);
    @DexIgnore
    public /* final */ double c;

    @DexIgnore
    public Ma0(double d) {
        super(Aa0.j);
        this.c = d;
    }

    @DexIgnore
    public /* synthetic */ Ma0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readDouble();
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort((short) ((int) ((this.c * ((double) 1000)) / 100.0d)));
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ma0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Ma0) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.DelayInstr");
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int hashCode() {
        return (int) this.c;
    }

    @DexIgnore
    @Override // com.fossil.Va0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.W3, Double.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.c);
        }
    }
}
