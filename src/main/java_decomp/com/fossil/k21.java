package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class K21<T> implements E21<T> {
    @DexIgnore
    public /* final */ List<String> a; // = new ArrayList();
    @DexIgnore
    public T b;
    @DexIgnore
    public T21<T> c;
    @DexIgnore
    public Ai d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(List<String> list);

        @DexIgnore
        void b(List<String> list);
    }

    @DexIgnore
    public K21(T21<T> t21) {
        this.c = t21;
    }

    @DexIgnore
    @Override // com.fossil.E21
    public void a(T t) {
        this.b = t;
        h(this.d, t);
    }

    @DexIgnore
    public abstract boolean b(O31 o31);

    @DexIgnore
    public abstract boolean c(T t);

    @DexIgnore
    public boolean d(String str) {
        T t = this.b;
        return t != null && c(t) && this.a.contains(str);
    }

    @DexIgnore
    public void e(Iterable<O31> iterable) {
        this.a.clear();
        for (O31 o31 : iterable) {
            if (b(o31)) {
                this.a.add(o31.a);
            }
        }
        if (this.a.isEmpty()) {
            this.c.c(this);
        } else {
            this.c.a(this);
        }
        h(this.d, this.b);
    }

    @DexIgnore
    public void f() {
        if (!this.a.isEmpty()) {
            this.a.clear();
            this.c.c(this);
        }
    }

    @DexIgnore
    public void g(Ai ai) {
        if (this.d != ai) {
            this.d = ai;
            h(ai, this.b);
        }
    }

    @DexIgnore
    public final void h(Ai ai, T t) {
        if (!this.a.isEmpty() && ai != null) {
            if (t == null || c(t)) {
                ai.b(this.a);
            } else {
                ai.a(this.a);
            }
        }
    }
}
