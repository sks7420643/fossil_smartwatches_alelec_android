package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rh1 implements Th1<Drawable, byte[]> {
    @DexIgnore
    public /* final */ Rd1 a;
    @DexIgnore
    public /* final */ Th1<Bitmap, byte[]> b;
    @DexIgnore
    public /* final */ Th1<Hh1, byte[]> c;

    @DexIgnore
    public Rh1(Rd1 rd1, Th1<Bitmap, byte[]> th1, Th1<Hh1, byte[]> th12) {
        this.a = rd1;
        this.b = th1;
        this.c = th12;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Id1<android.graphics.drawable.Drawable> */
    /* JADX WARN: Multi-variable type inference failed */
    public static Id1<Hh1> b(Id1<Drawable> id1) {
        return id1;
    }

    @DexIgnore
    @Override // com.fossil.Th1
    public Id1<byte[]> a(Id1<Drawable> id1, Ob1 ob1) {
        Drawable drawable = id1.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(Yf1.f(((BitmapDrawable) drawable).getBitmap(), this.a), ob1);
        }
        if (!(drawable instanceof Hh1)) {
            return null;
        }
        Th1<Hh1, byte[]> th1 = this.c;
        b(id1);
        return th1.a(id1, ob1);
    }
}
