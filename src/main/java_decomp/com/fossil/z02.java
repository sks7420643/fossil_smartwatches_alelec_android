package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.util.Log;
import com.google.android.datatransport.runtime.backends.TransportBackendDiscovery;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z02 implements T02 {
    @DexIgnore
    public /* final */ Ai a;
    @DexIgnore
    public /* final */ X02 b;
    @DexIgnore
    public /* final */ Map<String, B12> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public Map<String, String> b; // = null;

        @DexIgnore
        public Ai(Context context) {
            this.a = context;
        }

        @DexIgnore
        public static Bundle d(Context context) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("BackendRegistry", "Context has no PackageManager.");
                    return null;
                }
                ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, TransportBackendDiscovery.class), 128);
                if (serviceInfo != null) {
                    return serviceInfo.metaData;
                }
                Log.w("BackendRegistry", "TransportBackendDiscovery has no service info.");
                return null;
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("BackendRegistry", "Application info not found.");
                return null;
            }
        }

        @DexIgnore
        public final Map<String, String> a(Context context) {
            Bundle d = d(context);
            if (d == null) {
                Log.w("BackendRegistry", "Could not retrieve metadata, returning empty list of transport backends.");
                return Collections.emptyMap();
            }
            HashMap hashMap = new HashMap();
            for (String str : d.keySet()) {
                Object obj = d.get(str);
                if ((obj instanceof String) && str.startsWith("backend:")) {
                    String[] split = ((String) obj).split(",", -1);
                    for (String str2 : split) {
                        String trim = str2.trim();
                        if (!trim.isEmpty()) {
                            hashMap.put(trim, str.substring(8));
                        }
                    }
                }
            }
            return hashMap;
        }

        @DexIgnore
        public S02 b(String str) {
            String str2 = c().get(str);
            if (str2 == null) {
                return null;
            }
            try {
                return (S02) Class.forName(str2).asSubclass(S02.class).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassNotFoundException e) {
                Log.w("BackendRegistry", String.format("Class %s is not found.", str2), e);
            } catch (IllegalAccessException e2) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e2);
            } catch (InstantiationException e3) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e3);
            } catch (NoSuchMethodException e4) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e4);
            } catch (InvocationTargetException e5) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e5);
            }
            return null;
        }

        @DexIgnore
        public final Map<String, String> c() {
            if (this.b == null) {
                this.b = a(this.a);
            }
            return this.b;
        }
    }

    @DexIgnore
    public Z02(Context context, X02 x02) {
        this(new Ai(context), x02);
    }

    @DexIgnore
    public Z02(Ai ai, X02 x02) {
        this.c = new HashMap();
        this.a = ai;
        this.b = x02;
    }

    @DexIgnore
    @Override // com.fossil.T02
    public B12 b(String str) {
        synchronized (this) {
            if (this.c.containsKey(str)) {
                return this.c.get(str);
            }
            S02 b2 = this.a.b(str);
            if (b2 == null) {
                return null;
            }
            B12 create = b2.create(this.b.a(str));
            this.c.put(str, create);
            return create;
        }
    }
}
