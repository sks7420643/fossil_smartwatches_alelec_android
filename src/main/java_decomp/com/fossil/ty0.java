package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ty0 {
    @DexIgnore
    public static Transition a; // = new AutoTransition();
    @DexIgnore
    public static ThreadLocal<WeakReference<Zi0<ViewGroup, ArrayList<Transition>>>> b; // = new ThreadLocal<>();
    @DexIgnore
    public static ArrayList<ViewGroup> c; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
        @DexIgnore
        public Transition b;
        @DexIgnore
        public ViewGroup c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Sy0 {
            @DexIgnore
            public /* final */ /* synthetic */ Zi0 a;

            @DexIgnore
            public Aii(Zi0 zi0) {
                this.a = zi0;
            }

            @DexIgnore
            @Override // androidx.transition.Transition.f
            public void c(Transition transition) {
                ((ArrayList) this.a.get(Ai.this.c)).remove(transition);
                transition.d0(this);
            }
        }

        @DexIgnore
        public Ai(Transition transition, ViewGroup viewGroup) {
            this.b = transition;
            this.c = viewGroup;
        }

        @DexIgnore
        public final void a() {
            this.c.getViewTreeObserver().removeOnPreDrawListener(this);
            this.c.removeOnAttachStateChangeListener(this);
        }

        @DexIgnore
        public boolean onPreDraw() {
            a();
            if (Ty0.c.remove(this.c)) {
                Zi0<ViewGroup, ArrayList<Transition>> b2 = Ty0.b();
                ArrayList<Transition> arrayList = b2.get(this.c);
                ArrayList arrayList2 = null;
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                    b2.put(this.c, arrayList);
                } else if (arrayList.size() > 0) {
                    arrayList2 = new ArrayList(arrayList);
                }
                arrayList.add(this.b);
                this.b.d(new Aii(b2));
                this.b.r(this.c, false);
                if (arrayList2 != null) {
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        ((Transition) it.next()).f0(this.c);
                    }
                }
                this.b.c0(this.c);
            }
            return true;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            a();
            Ty0.c.remove(this.c);
            ArrayList<Transition> arrayList = Ty0.b().get(this.c);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator<Transition> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().f0(this.c);
                }
            }
            this.b.s(true);
        }
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, Transition transition) {
        if (!c.contains(viewGroup) && Mo0.Q(viewGroup)) {
            c.add(viewGroup);
            if (transition == null) {
                transition = a;
            }
            Transition t = transition.t();
            d(viewGroup, t);
            Py0.c(viewGroup, null);
            c(viewGroup, t);
        }
    }

    @DexIgnore
    public static Zi0<ViewGroup, ArrayList<Transition>> b() {
        Zi0<ViewGroup, ArrayList<Transition>> zi0;
        WeakReference<Zi0<ViewGroup, ArrayList<Transition>>> weakReference = b.get();
        if (weakReference != null && (zi0 = weakReference.get()) != null) {
            return zi0;
        }
        Zi0<ViewGroup, ArrayList<Transition>> zi02 = new Zi0<>();
        b.set(new WeakReference<>(zi02));
        return zi02;
    }

    @DexIgnore
    public static void c(ViewGroup viewGroup, Transition transition) {
        if (transition != null && viewGroup != null) {
            Ai ai = new Ai(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(ai);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(ai);
        }
    }

    @DexIgnore
    public static void d(ViewGroup viewGroup, Transition transition) {
        ArrayList<Transition> arrayList = b().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<Transition> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().b0(viewGroup);
            }
        }
        if (transition != null) {
            transition.r(viewGroup, true);
        }
        Py0 b2 = Py0.b(viewGroup);
        if (b2 != null) {
            b2.a();
        }
    }
}
