package com.fossil;

import java.io.IOException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class El4 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = Integer.MAX_VALUE;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i; // = 64;

    @DexIgnore
    public El4(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        this.c = i3 + i2;
        this.e = i2;
    }

    @DexIgnore
    public static El4 d(byte[] bArr) {
        return e(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static El4 e(byte[] bArr, int i2, int i3) {
        return new El4(bArr, i2, i3);
    }

    @DexIgnore
    public void a(int i2) throws Gl4 {
        if (this.f != i2) {
            throw Gl4.invalidEndTag();
        }
    }

    @DexIgnore
    public int b() {
        return this.e - this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.e == this.c;
    }

    @DexIgnore
    public void f(int i2) {
        this.g = i2;
        r();
    }

    @DexIgnore
    public int g(int i2) throws Gl4 {
        if (i2 >= 0) {
            int i3 = this.e + i2;
            int i4 = this.g;
            if (i3 <= i4) {
                this.g = i3;
                r();
                return i4;
            }
            throw Gl4.truncatedMessage();
        }
        throw Gl4.negativeSize();
    }

    @DexIgnore
    public boolean h() throws IOException {
        return o() != 0;
    }

    @DexIgnore
    public int i() throws IOException {
        return o();
    }

    @DexIgnore
    public void j(Hl4 hl4) throws IOException {
        int o = o();
        if (this.h < this.i) {
            int g2 = g(o);
            this.h++;
            hl4.b(this);
            a(0);
            this.h--;
            f(g2);
            return;
        }
        throw Gl4.recursionLimitExceeded();
    }

    @DexIgnore
    public byte k() throws IOException {
        int i2 = this.e;
        if (i2 != this.c) {
            byte[] bArr = this.a;
            this.e = i2 + 1;
            return bArr[i2];
        }
        throw Gl4.truncatedMessage();
    }

    @DexIgnore
    public byte[] l(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = this.g;
            if (i3 + i2 > i4) {
                v(i4 - i3);
                throw Gl4.truncatedMessage();
            } else if (i2 <= this.c - i3) {
                byte[] bArr = new byte[i2];
                System.arraycopy(this.a, i3, bArr, 0, i2);
                this.e += i2;
                return bArr;
            } else {
                throw Gl4.truncatedMessage();
            }
        } else {
            throw Gl4.negativeSize();
        }
    }

    @DexIgnore
    public int m() throws IOException {
        return (k() & 255) | ((k() & 255) << 8) | ((k() & 255) << 16) | ((k() & 255) << 24);
    }

    @DexIgnore
    public long n() throws IOException {
        byte k = k();
        return ((((long) k()) & 255) << 8) | (((long) k) & 255) | ((((long) k()) & 255) << 16) | ((((long) k()) & 255) << 24) | ((((long) k()) & 255) << 32) | ((((long) k()) & 255) << 40) | ((((long) k()) & 255) << 48) | ((((long) k()) & 255) << 56);
    }

    @DexIgnore
    public int o() throws IOException {
        int i2;
        byte k = k();
        if (k >= 0) {
            return k;
        }
        int i3 = k & Byte.MAX_VALUE;
        byte k2 = k();
        if (k2 >= 0) {
            i2 = k2 << 7;
        } else {
            i3 |= (k2 & Byte.MAX_VALUE) << 7;
            byte k3 = k();
            if (k3 >= 0) {
                i2 = k3 << DateTimeFieldType.HOUR_OF_HALFDAY;
            } else {
                i3 |= (k3 & Byte.MAX_VALUE) << 14;
                byte k4 = k();
                if (k4 >= 0) {
                    i2 = k4 << DateTimeFieldType.SECOND_OF_MINUTE;
                } else {
                    byte k5 = k();
                    int i4 = ((k4 & Byte.MAX_VALUE) << 21) | i3 | (k5 << 28);
                    if (k5 >= 0) {
                        return i4;
                    }
                    for (int i5 = 0; i5 < 5; i5++) {
                        if (k() >= 0) {
                            return i4;
                        }
                    }
                    throw Gl4.malformedVarint();
                }
            }
        }
        return i2 | i3;
    }

    @DexIgnore
    public String p() throws IOException {
        int o = o();
        if (o > this.c - this.e || o <= 0) {
            return new String(l(o), "UTF-8");
        }
        String str = new String(this.a, this.e, o, "UTF-8");
        this.e = o + this.e;
        return str;
    }

    @DexIgnore
    public int q() throws IOException {
        int i2 = 0;
        if (c()) {
            this.f = 0;
        } else {
            i2 = o();
            this.f = i2;
            if (i2 == 0) {
                throw Gl4.invalidTag();
            }
        }
        return i2;
    }

    @DexIgnore
    public final void r() {
        int i2 = this.c + this.d;
        this.c = i2;
        int i3 = this.g;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.d = i4;
            this.c = i2 - i4;
            return;
        }
        this.d = 0;
    }

    @DexIgnore
    public void s(int i2) {
        int i3 = this.e;
        int i4 = this.b;
        if (i2 > i3 - i4) {
            int i5 = this.e;
            int i6 = this.b;
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i2);
            sb.append(" is beyond current ");
            sb.append(i5 - i6);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0) {
            this.e = i4 + i2;
        } else {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    @DexIgnore
    public boolean t(int i2) throws IOException {
        int c2 = Jl4.c(i2);
        if (c2 == 0) {
            i();
            return true;
        } else if (c2 == 1) {
            n();
            return true;
        } else if (c2 == 2) {
            v(o());
            return true;
        } else if (c2 == 3) {
            u();
            a(Jl4.d(Jl4.b(i2), 4));
            return true;
        } else if (c2 == 4) {
            return false;
        } else {
            if (c2 == 5) {
                m();
                return true;
            }
            throw Gl4.invalidWireType();
        }
    }

    @DexIgnore
    public void u() throws IOException {
        int q;
        do {
            q = q();
            if (q == 0) {
                return;
            }
        } while (t(q));
    }

    @DexIgnore
    public void v(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = this.g;
            if (i3 + i2 > i4) {
                v(i4 - i3);
                throw Gl4.truncatedMessage();
            } else if (i2 <= this.c - i3) {
                this.e = i3 + i2;
            } else {
                throw Gl4.truncatedMessage();
            }
        } else {
            throw Gl4.negativeSize();
        }
    }
}
