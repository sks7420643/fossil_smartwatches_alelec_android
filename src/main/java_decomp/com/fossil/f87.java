package com.fossil;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.h87;
import com.fossil.hw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f87 extends pv5 {
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public h87 h;
    @DexIgnore
    public qg5 i;
    @DexIgnore
    public hw5 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final f87 a() {
            return new f87();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements hw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1072a;

        @DexIgnore
        public b(f87 f87) {
            this.f1072a = f87;
        }

        @DexIgnore
        @Override // com.fossil.hw5.b
        public void a(String str) {
            pq7.c(str, "complicationId");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "onItemClicked, complicationId = " + str);
            f87.K6(this.f1072a).t(str);
            f87.L6(this.f1072a).p(str);
            gc7 d = hc7.c.d(this.f1072a);
            if (d != null) {
                d.y(str);
            }
        }

        @DexIgnore
        @Override // com.fossil.hw5.b
        public void b(String str) {
            pq7.c(str, "complicationId");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "onItemWarningClicked, complicationId = " + str);
            jn5.c(jn5.b, this.f1072a.getContext(), hl5.f1493a.b(str), false, false, false, null, 60, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ f87 b;

        @DexIgnore
        public c(f87 f87) {
            this.b = f87;
        }

        @DexIgnore
        public final void onClick(View view) {
            f87.L6(this.b).z();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<List<? extends Complication>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1073a;

        @DexIgnore
        public d(f87 f87) {
            this.f1073a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<Complication> list) {
            hw5 K6 = f87.K6(this.f1073a);
            pq7.b(list, "it");
            K6.n(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<h87.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1074a;

        @DexIgnore
        public e(f87 f87) {
            this.f1074a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h87.c cVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "settingViewLiveData changed, value = " + cVar);
            this.f1074a.S6(cVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<h87.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1075a;

        @DexIgnore
        public f(f87 f87) {
            this.f1075a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h87.b bVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "settingScreenNavigationLiveData changed, value = " + bVar);
            String b = bVar.b();
            String a2 = bVar.a();
            int hashCode = a2.hashCode();
            if (hashCode != -829740640) {
                if (hashCode == 134170930 && a2.equals("second-timezone")) {
                    SearchSecondTimezoneActivity.B.a(this.f1075a, b);
                }
            } else if (a2.equals("commute-time")) {
                CommuteTimeSettingsActivity.B.a(this.f1075a, b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<Complication> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1076a;

        @DexIgnore
        public g(f87 f87) {
            this.f1076a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Complication complication) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "selectedComplicationLiveData, value = " + complication);
            f87 f87 = this.f1076a;
            pq7.b(complication, "it");
            f87.T6(complication);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<eb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1077a;

        @DexIgnore
        public h(f87 f87) {
            this.f1077a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(eb7 eb7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "watchFaceComplicationsLive, value = " + eb7);
            this.f1077a.R6(eb7.b().a());
            f87.K6(this.f1077a).u(eb7.b().b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<Rect> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1078a;

        @DexIgnore
        public i(f87 f87) {
            this.f1078a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Rect rect) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "editorRectLive, value = " + rect);
            hw5 K6 = f87.K6(this.f1078a);
            pq7.b(rect, "it");
            K6.r(rect);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1079a;

        @DexIgnore
        public j(f87 f87) {
            this.f1079a = f87;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
            FLogger.INSTANCE.getLocal().d("WatchFaceComplicationFragment", "complicationDroppedLive");
            f87.K6(this.f1079a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1080a;

        @DexIgnore
        public k(f87 f87) {
            this.f1080a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            pq7.b(bool, "it");
            if (bool.booleanValue()) {
                f87.K6(this.f1080a).notifyDataSetChanged();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<List<? extends DianaAppSetting>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f87 f1081a;

        @DexIgnore
        public l(f87 f87) {
            this.f1081a = f87;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DianaAppSetting> list) {
            gc7 d = hc7.c.d(this.f1081a);
            if (d != null) {
                pq7.b(list, "it");
                d.t(list);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ hw5 K6(f87 f87) {
        hw5 hw5 = f87.j;
        if (hw5 != null) {
            return hw5;
        }
        pq7.n("complicationAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ h87 L6(f87 f87) {
        h87 h87 = f87.h;
        if (h87 != null) {
            return h87;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceComplicationFragment";
    }

    @DexIgnore
    public final void P6() {
        hw5 hw5 = new hw5(null, null, 3, null);
        hw5.s(new b(this));
        this.j = hw5;
        qg5 qg5 = this.i;
        if (qg5 != null) {
            RecyclerView recyclerView = qg5.f;
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
            hw5 hw52 = this.j;
            if (hw52 != null) {
                recyclerView.setAdapter(hw52);
                qg5.j.setOnClickListener(new c(this));
                return;
            }
            pq7.n("complicationAdapter");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        LiveData<Object> f2;
        LiveData<Rect> h2;
        LiveData<eb7> q;
        h87 h87 = this.h;
        if (h87 != null) {
            h87.v().h(getViewLifecycleOwner(), new d(this));
            h87 h872 = this.h;
            if (h872 != null) {
                h872.y().h(getViewLifecycleOwner(), new e(this));
                h87 h873 = this.h;
                if (h873 != null) {
                    h873.x().h(getViewLifecycleOwner(), new f(this));
                    h87 h874 = this.h;
                    if (h874 != null) {
                        h874.w().h(getViewLifecycleOwner(), new g(this));
                        gc7 d2 = hc7.c.d(this);
                        if (!(d2 == null || (q = d2.q()) == null)) {
                            q.h(getViewLifecycleOwner(), new h(this));
                        }
                        gc7 d3 = hc7.c.d(this);
                        if (!(d3 == null || (h2 = d3.h()) == null)) {
                            h2.h(getViewLifecycleOwner(), new i(this));
                        }
                        gc7 d4 = hc7.c.d(this);
                        if (!(d4 == null || (f2 = d4.f()) == null)) {
                            f2.h(getViewLifecycleOwner(), new j(this));
                        }
                        h87 h875 = this.h;
                        if (h875 != null) {
                            h875.r().h(getViewLifecycleOwner(), new k(this));
                            h87 h876 = this.h;
                            if (h876 != null) {
                                h876.u().h(getViewLifecycleOwner(), new l(this));
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void R6(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationFragment", "showSelectedComplication, complicationId = " + str);
        hw5 hw5 = this.j;
        if (hw5 != null) {
            hw5.t(str);
            if (str != null) {
                h87 h87 = this.h;
                if (h87 != null) {
                    h87.p(str);
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                qg5 qg5 = this.i;
                if (qg5 != null) {
                    ConstraintLayout constraintLayout = qg5.b;
                    pq7.b(constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(4);
                    FlexibleTextView flexibleTextView = qg5.j;
                    pq7.b(flexibleTextView, "it.tvComplicationSetting");
                    flexibleTextView.setVisibility(4);
                    FlexibleTextView flexibleTextView2 = qg5.l;
                    pq7.b(flexibleTextView2, "it.tvSelectedComplication");
                    flexibleTextView2.setVisibility(4);
                    FlexibleTextView flexibleTextView3 = qg5.h;
                    pq7.b(flexibleTextView3, "it.tvComplicationDetail");
                    flexibleTextView3.setVisibility(4);
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
        } else {
            pq7.n("complicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void S6(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationFragment", "showSettingData, setting = " + str);
        qg5 qg5 = this.i;
        if (qg5 == null) {
            pq7.n("mBinding");
            throw null;
        } else if (!vt7.l(str)) {
            ConstraintLayout constraintLayout = qg5.b;
            pq7.b(constraintLayout, "it.clComplicationSetting");
            constraintLayout.setVisibility(0);
            FlexibleTextView flexibleTextView = qg5.j;
            pq7.b(flexibleTextView, "it.tvComplicationSetting");
            flexibleTextView.setVisibility(0);
            qg5.j.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            FlexibleTextView flexibleTextView2 = qg5.j;
            pq7.b(flexibleTextView2, "it.tvComplicationSetting");
            FragmentActivity activity = getActivity();
            Resources resources = activity != null ? activity.getResources() : null;
            if (resources != null) {
                flexibleTextView2.setCompoundDrawablePadding((int) resources.getDimension(2131165420));
                qg5.j.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2131231049, 0);
                ImageView imageView = qg5.d;
                pq7.b(imageView, "it.ivComplicationSetting");
                imageView.setVisibility(4);
                FlexibleTextView flexibleTextView3 = qg5.j;
                pq7.b(flexibleTextView3, "it.tvComplicationSetting");
                flexibleTextView3.setText(str);
                return;
            }
            pq7.i();
            throw null;
        } else {
            ConstraintLayout constraintLayout2 = qg5.b;
            pq7.b(constraintLayout2, "it.clComplicationSetting");
            constraintLayout2.setVisibility(4);
            FlexibleTextView flexibleTextView4 = qg5.j;
            pq7.b(flexibleTextView4, "it.tvComplicationSetting");
            flexibleTextView4.setVisibility(4);
        }
    }

    @DexIgnore
    public final void T6(Complication complication) {
        qg5 qg5 = this.i;
        if (qg5 != null) {
            FlexibleTextView flexibleTextView = qg5.l;
            pq7.b(flexibleTextView, "binding.tvSelectedComplication");
            flexibleTextView.setVisibility(0);
            FlexibleTextView flexibleTextView2 = qg5.h;
            pq7.b(flexibleTextView2, "binding.tvComplicationDetail");
            flexibleTextView2.setVisibility(0);
            FlexibleTextView flexibleTextView3 = qg5.l;
            pq7.b(flexibleTextView3, "binding.tvSelectedComplication");
            flexibleTextView3.setText(um5.d(PortfolioApp.h0.c(), complication.getNameKey(), complication.getName()));
            FlexibleTextView flexibleTextView4 = qg5.h;
            pq7.b(flexibleTextView4, "binding.tvComplicationDetail");
            flexibleTextView4.setText(um5.d(PortfolioApp.h0.c(), complication.getDescriptionKey(), complication.getDescription()));
            hw5 hw5 = this.j;
            if (hw5 != null) {
                int l2 = hw5.l(complication.getComplicationId());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceComplicationFragment", "updateDetailComplication complicationId=" + complication.getComplicationId() + " scrollTo " + l2);
                if (l2 >= 0) {
                    qg5.f.scrollToPosition(l2);
                    return;
                }
                return;
            }
            pq7.n("complicationAdapter");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().G0().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            po4 po4 = this.g;
            if (po4 != null) {
                ts0 a2 = vs0.f(watchFaceEditActivity, po4).a(h87.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026ionViewModel::class.java)");
                this.h = (h87) a2;
                P6();
                Q6();
                return;
            }
            pq7.n("viewModelFactory");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        CommuteTimeSetting commuteTimeSetting;
        super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationFragment", "onActivityResult requestCode " + i2);
        if (i2 != 100) {
            if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                S6(commuteTimeSetting.getAddress());
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            S6(secondTimezoneSetting.getTimeZoneName());
            gc7 d2 = hc7.c.d(this);
            if (d2 != null) {
                d2.y("second-timezone");
            }
            h87 h87 = this.h;
            if (h87 != null) {
                h87.A("second-timezone", secondTimezoneSetting);
            } else {
                pq7.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        qg5 c2 = qg5.c(layoutInflater);
        pq7.b(c2, "WatchFaceComplicationFra\u2026Binding.inflate(inflater)");
        this.i = c2;
        if (c2 != null) {
            ConstraintLayout b2 = c2.b();
            pq7.b(b2, "mBinding.root");
            return b2;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        h87 h87 = this.h;
        if (h87 != null) {
            h87.q();
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
