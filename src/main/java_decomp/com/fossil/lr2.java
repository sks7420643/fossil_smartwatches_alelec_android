package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lr2 implements Parcelable.Creator<Kr2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Kr2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 1;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder3 = null;
        Ir2 ir2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    i = Ad2.v(parcel, t);
                    break;
                case 2:
                    ir2 = (Ir2) Ad2.e(parcel, t, Ir2.CREATOR);
                    break;
                case 3:
                    iBinder3 = Ad2.u(parcel, t);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) Ad2.e(parcel, t, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = Ad2.u(parcel, t);
                    break;
                case 6:
                    iBinder = Ad2.u(parcel, t);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Kr2(i, ir2, iBinder3, pendingIntent, iBinder2, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Kr2[] newArray(int i) {
        return new Kr2[i];
    }
}
