package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W63 implements Xw2<V63> {
    @DexIgnore
    public static W63 c; // = new W63();
    @DexIgnore
    public /* final */ Xw2<V63> b;

    @DexIgnore
    public W63() {
        this(Ww2.b(new Y63()));
    }

    @DexIgnore
    public W63(Xw2<V63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((V63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((V63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ V63 zza() {
        return this.b.zza();
    }
}
