package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rd6 extends pv5 implements qd6, zw5, aw5 {
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public g37<j55> g;
    @DexIgnore
    public pd6 h;
    @DexIgnore
    public tw5 i;
    @DexIgnore
    public ActiveTimeOverviewFragment j;
    @DexIgnore
    public f67 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final rd6 a() {
            return new rd6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends f67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;
        @DexIgnore
        public /* final */ /* synthetic */ rd6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, rd6 rd6, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = recyclerView;
            this.f = rd6;
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void b(int i) {
            rd6.K6(this.f).p();
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void c(int i, int i2) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ pd6 K6(rd6 rd6) {
        pd6 pd6 = rd6.h;
        if (pd6 != null) {
            return pd6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "DashboardActiveTimeFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final j55 L6() {
        g37<j55> g37 = this.g;
        if (g37 != null) {
            return g37.a();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(pd6 pd6) {
        pq7.c(pd6, "presenter");
        this.h = pd6;
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void Q(Date date) {
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeFragment", "onDayClicked - date=" + date);
        Context context = getContext();
        if (context != null) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.C;
            pq7.b(context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        j55 L6;
        RecyclerView recyclerView;
        View view;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("DashboardActiveTimeFragment visible=");
        sb.append(z);
        sb.append(", tracer=");
        sb.append(C6());
        sb.append(", isRunning=");
        vl5 C6 = C6();
        sb.append(C6 != null ? Boolean.valueOf(C6.f()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z) {
            vl5 C62 = C6();
            if (C62 != null) {
                C62.i();
            }
            if (isVisible() && this.g != null && (L6 = L6()) != null && (recyclerView = L6.q) != null) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
                if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    recyclerView.smoothScrollToPosition(0);
                    f67 f67 = this.k;
                    if (f67 != null) {
                        f67.d();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        vl5 C63 = C6();
        if (C63 != null) {
            C63.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.qd6
    public void d() {
        f67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<j55> g37 = new g37<>(this, (j55) aq0.f(layoutInflater, 2131558542, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            j55 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimeFragment", "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        pd6 pd6 = this.h;
        if (pd6 != null) {
            pd6.o();
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pd6 pd6 = this.h;
        if (pd6 != null) {
            pd6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        pd6 pd6 = this.h;
        if (pd6 != null) {
            pd6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ActiveTimeOverviewFragment activeTimeOverviewFragment = (ActiveTimeOverviewFragment) getChildFragmentManager().Z("ActiveTimeOverviewFragment");
        this.j = activeTimeOverviewFragment;
        if (activeTimeOverviewFragment == null) {
            this.j = new ActiveTimeOverviewFragment();
        }
        sw5 sw5 = new sw5();
        PortfolioApp c = PortfolioApp.h0.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        ActiveTimeOverviewFragment activeTimeOverviewFragment2 = this.j;
        if (activeTimeOverviewFragment2 != null) {
            this.i = new tw5(sw5, c, this, childFragmentManager, activeTimeOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            j55 L6 = L6();
            if (!(L6 == null || (recyclerView2 = L6.q) == null)) {
                pq7.b(recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                tw5 tw5 = this.i;
                if (tw5 != null) {
                    recyclerView2.setAdapter(tw5);
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        b bVar = new b(recyclerView2, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                        this.k = bVar;
                        if (bVar != null) {
                            recyclerView2.addOnScrollListener(bVar);
                            recyclerView2.setItemViewCacheSize(0);
                            bd6 bd6 = new bd6(linearLayoutManager.q2());
                            Drawable f = gl0.f(recyclerView2.getContext(), 2131230856);
                            if (f != null) {
                                pq7.b(f, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                bd6.h(f);
                                recyclerView2.addItemDecoration(bd6);
                                pd6 pd6 = this.h;
                                if (pd6 != null) {
                                    pd6.n();
                                } else {
                                    pq7.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    pq7.n("mDashboardActiveTimeAdapter");
                    throw null;
                }
            }
            j55 L62 = L6();
            if (!(L62 == null || (recyclerView = L62.q) == null)) {
                pq7.b(recyclerView, "recyclerView");
                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof pv0) {
                    ((pv0) itemAnimator).setSupportsChangeAnimations(false);
                }
            }
            E6("active_minutes_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ts0 a2 = vs0.e(activity).a(y67.class);
                pq7.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                y67 y67 = (y67) a2;
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qd6
    public void q(cu0<ActivitySummary> cu0) {
        tw5 tw5 = this.i;
        if (tw5 != null) {
            tw5.x(cu0);
        } else {
            pq7.n("mDashboardActiveTimeAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void q0(Date date, Date date2) {
        pq7.c(date, "startWeekDate");
        pq7.c(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeFragment", "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
