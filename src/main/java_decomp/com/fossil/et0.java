package com.fossil;

import android.annotation.TargetApi;
import android.media.AudioAttributes;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@TargetApi(21)
public class Et0 implements Dt0 {
    @DexIgnore
    public AudioAttributes a;
    @DexIgnore
    public int b; // = -1;

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Et0)) {
            return false;
        }
        return this.a.equals(((Et0) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "AudioAttributesCompat: audioattributes=" + this.a;
    }
}
