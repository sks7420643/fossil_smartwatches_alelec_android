package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Td0;
import com.mapped.Ud0;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bv1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public byte e;
    @DexIgnore
    public byte f;
    @DexIgnore
    public byte g;
    @DexIgnore
    public byte h;
    @DexIgnore
    public /* final */ byte i;
    @DexIgnore
    public byte j;
    @DexIgnore
    public /* final */ Td0 k; // = P90.b.a(this.d);
    @DexIgnore
    public /* final */ Ud0 l; // = P90.b.b(this.d);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Bv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bv1 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                Wg6.b(createByteArray, "parcel.createByteArray()!!");
                return new Bv1(createByteArray);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bv1[] newArray(int i) {
            return new Bv1[i];
        }
    }

    @DexIgnore
    public Bv1(byte[] bArr) {
        this.b = bArr;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.c = order.get(0);
        this.d = order.getShort(1);
        this.e = order.get(3);
        this.f = order.get(4);
        this.g = order.get(5);
        this.h = order.get(6);
        this.i = order.get(7);
        this.j = order.get(8);
    }

    @DexIgnore
    public final byte[] a() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Bv1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((Bv1) obj).b);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppEvent");
    }

    @DexIgnore
    public final Td0 getMicroAppId() {
        return this.k;
    }

    @DexIgnore
    public final byte getRequestId() {
        return this.i;
    }

    @DexIgnore
    public final Ud0 getVariant() {
        return this.l;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.b);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.F3, this.k.name()), Jd0.J3, this.l), Jd0.t3, Byte.valueOf(this.c)), Jd0.I3, Byte.valueOf(this.e)), Jd0.O3, Byte.valueOf(this.f)), Jd0.P3, Byte.valueOf(this.g)), Jd0.O2, Byte.valueOf(this.h)), Jd0.q, Byte.valueOf(this.i)), Jd0.v3, Byte.valueOf(this.j));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
