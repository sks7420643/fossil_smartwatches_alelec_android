package com.fossil;

import com.mapped.Qg6;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vu7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(Vu7.class, "_handled");
    @DexIgnore
    public volatile int _handled;
    @DexIgnore
    public /* final */ Throwable a;

    @DexIgnore
    public Vu7(Throwable th, boolean z) {
        this.a = th;
        this._handled = z ? 1 : 0;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Vu7(Throwable th, boolean z, int i, Qg6 qg6) {
        this(th, (i & 2) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r1 = this;
            int r0 = r1._handled
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vu7.a():boolean");
    }

    @DexIgnore
    public final boolean b() {
        return b.compareAndSet(this, 0, 1);
    }

    @DexIgnore
    public String toString() {
        return Ov7.a(this) + '[' + this.a + ']';
    }
}
