package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.H70;
import com.mapped.I70;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class An1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ H70 c;
    @DexIgnore
    public /* final */ Mn1 d;
    @DexIgnore
    public /* final */ Pn1 e;
    @DexIgnore
    public /* final */ I70 f;
    @DexIgnore
    public /* final */ On1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<An1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final An1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                int i = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                H70 a2 = H70.d.a((i >> 0) & 1);
                Mn1 a3 = Mn1.d.a((i >> 1) & 1);
                Pn1 a4 = Pn1.d.a((i >> 2) & 1);
                I70 a5 = I70.d.a((i >> 3) & 1);
                On1 a6 = On1.d.a((i >> 4) & 1);
                if (a2 != null && a3 != null && a4 != null && a5 != null && a6 != null) {
                    return new An1(a2, a3, a4, a5, a6);
                }
                throw new IllegalArgumentException("Invalid data properties");
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public An1 b(Parcel parcel) {
            return new An1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public An1 createFromParcel(Parcel parcel) {
            return new An1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public An1[] newArray(int i) {
            return new An1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ An1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.c = H70.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                Wg6.b(readString2, "parcel.readString()!!");
                this.d = Mn1.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    Wg6.b(readString3, "parcel.readString()!!");
                    this.e = Pn1.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        Wg6.b(readString4, "parcel.readString()!!");
                        this.f = I70.valueOf(readString4);
                        String readString5 = parcel.readString();
                        if (readString5 != null) {
                            Wg6.b(readString5, "parcel.readString()!!");
                            this.g = On1.valueOf(readString5);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public An1(H70 h70, Mn1 mn1, Pn1 pn1, I70 i70, On1 on1) {
        super(Zm1.DISPLAY_UNIT);
        this.c = h70;
        this.d = mn1;
        this.e = pn1;
        this.f = i70;
        this.g = on1;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((this.c.a() << 0) | 0 | (this.d.a() << 1) | (this.e.a() << 2) | (this.f.a() << 3) | (this.g.a() << 4)).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(G80.k(G80.k(G80.k(G80.k(jSONObject, Jd0.T1, Ey1.a(this.c)), Jd0.U1, Ey1.a(this.d)), Jd0.V1, Ey1.a(this.e)), Jd0.W1, Ey1.a(this.f)), Jd0.X1, Ey1.a(this.g));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(An1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            An1 an1 = (An1) obj;
            if (this.c != an1.c) {
                return false;
            }
            if (this.d != an1.d) {
                return false;
            }
            if (this.e != an1.e) {
                return false;
            }
            if (this.f != an1.f) {
                return false;
            }
            return this.g == an1.g;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DisplayUnitConfig");
    }

    @DexIgnore
    public final Mn1 getCaloriesUnit() {
        return this.d;
    }

    @DexIgnore
    public final On1 getDateFormat() {
        return this.g;
    }

    @DexIgnore
    public final Pn1 getDistanceUnit() {
        return this.e;
    }

    @DexIgnore
    public final H70 getTemperatureUnit() {
        return this.c;
    }

    @DexIgnore
    public final I70 getTimeFormat() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + this.f.hashCode()) * 31) + this.g.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
        if (parcel != null) {
            parcel.writeString(this.g.name());
        }
    }
}
