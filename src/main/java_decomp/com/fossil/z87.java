package com.fossil;

import android.graphics.Typeface;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Map<String, Typeface> f4432a; // = new LinkedHashMap();
    @DexIgnore
    public static /* final */ yk7 b; // = zk7.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ yk7 c; // = zk7.a(b.INSTANCE);
    @DexIgnore
    public static /* final */ z87 d; // = new z87();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<String> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final String invoke() {
            z87 z87 = z87.d;
            return (String) pm7.G(z87.f4432a.keySet());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements gp7<Typeface> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final Typeface invoke() {
            z87 z87 = z87.d;
            return (Typeface) pm7.G(z87.f4432a.values());
        }
    }

    @DexIgnore
    public final void b() {
        f4432a.clear();
    }

    @DexIgnore
    public final String c() {
        return (String) b.getValue();
    }

    @DexIgnore
    public final Typeface d() {
        return (Typeface) c.getValue();
    }

    @DexIgnore
    public final boolean e() {
        return !f4432a.isEmpty();
    }

    @DexIgnore
    public final List<Typeface> f() {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<String, Typeface> entry : f4432a.entrySet()) {
            arrayList.add(entry.getValue());
        }
        return arrayList;
    }

    @DexIgnore
    public final void g() {
        PortfolioApp c2 = PortfolioApp.h0.c();
        String[] list = c2.getAssets().list("theme/fonts");
        if (list != null) {
            for (String str : list) {
                String[] list2 = c2.getAssets().list("theme/fonts/" + str);
                if (list2 != null) {
                    for (String str2 : list2) {
                        Typeface createFromAsset = Typeface.createFromAsset(c2.getAssets(), "theme/fonts/" + str + '/' + str2);
                        pq7.b(str2, "fontFile");
                        int G = wt7.G(str2, CodelessMatcher.CURRENT_CLASS_NAME, 0, false, 6, null);
                        if (str2 != null) {
                            String substring = str2.substring(0, G);
                            pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            Map<String, Typeface> map = f4432a;
                            pq7.b(createFromAsset, "typeface");
                            map.put(substring, createFromAsset);
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    continue;
                }
            }
        }
        FLogger.INSTANCE.getLocal().d("WatchFaceFontStorage", "init " + f4432a);
    }

    @DexIgnore
    public final String h(Typeface typeface) {
        for (Map.Entry<String, Typeface> entry : f4432a.entrySet()) {
            String key = entry.getKey();
            if (pq7.a(f4432a.get(key), typeface)) {
                return key;
            }
        }
        return c();
    }

    @DexIgnore
    public final Typeface i(String str) {
        Typeface typeface = f4432a.get(str);
        return typeface != null ? typeface : d();
    }
}
