package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oq2 extends Sq2 {
    @DexIgnore
    public /* final */ J72<Status> b;

    @DexIgnore
    public Oq2(J72<Status> j72) {
        this.b = j72;
    }

    @DexIgnore
    @Override // com.fossil.Rq2
    public final void M(Pq2 pq2) {
        this.b.a(pq2.a());
    }
}
