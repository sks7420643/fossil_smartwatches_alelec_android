package com.fossil;

import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRemote;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ap5 implements Factory<DianaRecommendedPresetRepository> {
    @DexIgnore
    public /* final */ Provider<DianaRecommendedPresetRemote> a;

    @DexIgnore
    public Ap5(Provider<DianaRecommendedPresetRemote> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Ap5 a(Provider<DianaRecommendedPresetRemote> provider) {
        return new Ap5(provider);
    }

    @DexIgnore
    public static DianaRecommendedPresetRepository c(DianaRecommendedPresetRemote dianaRecommendedPresetRemote) {
        return new DianaRecommendedPresetRepository(dianaRecommendedPresetRemote);
    }

    @DexIgnore
    public DianaRecommendedPresetRepository b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
