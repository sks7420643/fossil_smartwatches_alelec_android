package com.fossil;

import com.fossil.Gh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jh4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Jh4 a();

        @DexIgnore
        public abstract Ai b(Kh4 kh4);

        @DexIgnore
        public abstract Ai c(String str);

        @DexIgnore
        public abstract Ai d(String str);

        @DexIgnore
        public abstract Ai e(Bi bi);

        @DexIgnore
        public abstract Ai f(String str);
    }

    @DexIgnore
    public enum Bi {
        OK,
        BAD_CONFIG
    }

    @DexIgnore
    public static Ai a() {
        return new Gh4.Bi();
    }

    @DexIgnore
    public abstract Kh4 b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract Bi e();

    @DexIgnore
    public abstract String f();
}
