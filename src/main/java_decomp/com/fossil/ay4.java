package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ay4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Object> a; // = new ArrayList();
    @DexIgnore
    public Zy4 b; // = new Zy4(1);
    @DexIgnore
    public Iy4 c;
    @DexIgnore
    public Hy4 d;

    /*
    static {
        Wg6.b(Ay4.class.getSimpleName(), "AllFriendListAdapter::class.java.simpleName");
    }
    */

    @DexIgnore
    public Ay4(Oy5 oy5) {
        Wg6.c(oy5, "listener");
        this.c = new Iy4(2, oy5);
        this.d = new Hy4(3);
    }

    @DexIgnore
    public final void g() {
        this.a.clear();
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.b(this.a, i)) {
            return this.b.a();
        }
        if (this.c.b(this.a, i)) {
            return this.c.a();
        }
        if (this.d.b(this.a, i)) {
            return this.d.a();
        }
        throw new IllegalArgumentException("No delegate for this position : " + this.a.get(i));
    }

    @DexIgnore
    public final Object h(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public final void i(List<? extends Object> list) {
        Wg6.c(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            this.b.c(this.a, i, viewHolder);
        } else if (itemViewType == 2) {
            this.c.c(this.a, i, viewHolder);
        } else if (itemViewType == 3) {
            this.d.c(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == this.b.a()) {
            return this.b.d(viewGroup);
        }
        if (i == this.c.a()) {
            return this.c.d(viewGroup);
        }
        if (i == this.d.a()) {
            return this.d.d(viewGroup);
        }
        throw new IllegalArgumentException("No support for this viewType: " + i);
    }
}
