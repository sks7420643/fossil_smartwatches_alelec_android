package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ve3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ve3> CREATOR; // = new Jf3();
    @DexIgnore
    public static /* final */ Ve3 c; // = new Ve3(0);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public Ve3(int i) {
        this.b = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ve3)) {
            return false;
        }
        return this.b == ((Ve3) obj).b;
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(Integer.valueOf(this.b));
    }

    @DexIgnore
    public final String toString() {
        String str;
        int i = this.b;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", Integer.valueOf(i));
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", str);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 2, this.b);
        Bd2.b(parcel, a2);
    }
}
