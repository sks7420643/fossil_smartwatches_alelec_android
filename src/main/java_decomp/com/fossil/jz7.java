package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jz7 extends Lz7 {
    @DexIgnore
    @Override // com.fossil.Lz7
    public boolean q() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public final boolean r() {
        throw new IllegalStateException("head cannot be removed".toString());
    }

    @DexIgnore
    public final boolean w() {
        return l() == this;
    }
}
