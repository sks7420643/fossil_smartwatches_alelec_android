package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Av3 extends Zc2 implements Qu3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Av3> CREATOR; // = new Bv3();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ List<Pv3> c;

    @DexIgnore
    public Av3(String str, List<Pv3> list) {
        this.b = str;
        this.c = list;
        Rc2.k(str);
        Rc2.k(this.c);
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Av3.class != obj.getClass()) {
            return false;
        }
        Av3 av3 = (Av3) obj;
        String str = this.b;
        if (str == null ? av3.b != null : !str.equals(av3.b)) {
            return false;
        }
        List<Pv3> list = this.c;
        List<Pv3> list2 = av3.c;
        if (list != null) {
            if (list.equals(list2)) {
                return true;
            }
        } else if (list2 == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        List<Pv3> list = this.c;
        if (list != null) {
            i = list.hashCode();
        }
        return ((hashCode + 31) * 31) + i;
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18 + String.valueOf(valueOf).length());
        sb.append("CapabilityInfo{");
        sb.append(str);
        sb.append(", ");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 2, c(), false);
        Bd2.y(parcel, 3, this.c, false);
        Bd2.b(parcel, a2);
    }
}
