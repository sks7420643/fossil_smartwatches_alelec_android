package com.fossil;

import com.mapped.C;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yn6 {
    @DexIgnore
    public /* final */ Date a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public Yn6(Date date, long j) {
        Wg6.c(date, "date");
        this.a = date;
        this.b = j;
    }

    @DexIgnore
    public final Date a() {
        return this.a;
    }

    @DexIgnore
    public final long b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Yn6) {
                Yn6 yn6 = (Yn6) obj;
                if (!Wg6.a(this.a, yn6.a) || this.b != yn6.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.a;
        return ((date != null ? date.hashCode() : 0) * 31) + C.a(this.b);
    }

    @DexIgnore
    public String toString() {
        return "ActivityDailyBest(date=" + this.a + ", value=" + this.b + ")";
    }
}
