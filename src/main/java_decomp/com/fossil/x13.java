package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X13 extends V13 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public X13() {
        super();
    }

    @DexIgnore
    public static <L> List<L> e(Object obj, long j, int i) {
        List<L> f = f(obj, j);
        if (f.isEmpty()) {
            List<L> t13 = f instanceof W13 ? new T13(i) : (!(f instanceof Y23) || !(f instanceof M13)) ? new ArrayList<>(i) : ((M13) f).zza(i);
            E43.j(obj, j, t13);
            return t13;
        } else if (c.isAssignableFrom(f.getClass())) {
            ArrayList arrayList = new ArrayList(f.size() + i);
            arrayList.addAll(f);
            E43.j(obj, j, arrayList);
            return arrayList;
        } else if (f instanceof Y33) {
            T13 t132 = new T13(f.size() + i);
            t132.addAll((Y33) f);
            E43.j(obj, j, t132);
            return t132;
        } else if (!(f instanceof Y23) || !(f instanceof M13)) {
            return f;
        } else {
            M13 m13 = (M13) f;
            if (m13.zza()) {
                return f;
            }
            M13 zza = m13.zza(f.size() + i);
            E43.j(obj, j, zza);
            return zza;
        }
    }

    @DexIgnore
    public static <E> List<E> f(Object obj, long j) {
        return (List) E43.F(obj, j);
    }

    @DexIgnore
    @Override // com.fossil.V13
    public final <E> void b(Object obj, Object obj2, long j) {
        List f = f(obj2, j);
        List e = e(obj, j, f.size());
        int size = e.size();
        int size2 = f.size();
        if (size > 0 && size2 > 0) {
            e.addAll(f);
        }
        if (size <= 0) {
            e = f;
        }
        E43.j(obj, j, e);
    }

    @DexIgnore
    @Override // com.fossil.V13
    public final void d(Object obj, long j) {
        Object unmodifiableList;
        List list = (List) E43.F(obj, j);
        if (list instanceof W13) {
            unmodifiableList = ((W13) list).zze();
        } else if (c.isAssignableFrom(list.getClass())) {
            return;
        } else {
            if (!(list instanceof Y23) || !(list instanceof M13)) {
                unmodifiableList = Collections.unmodifiableList(list);
            } else {
                M13 m13 = (M13) list;
                if (m13.zza()) {
                    m13.zzb();
                    return;
                }
                return;
            }
        }
        E43.j(obj, j, unmodifiableList);
    }
}
