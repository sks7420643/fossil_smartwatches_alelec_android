package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hd2 extends Dd2 {
    @DexIgnore
    public /* final */ J72<Status> b;

    @DexIgnore
    public Hd2(J72<Status> j72) {
        this.b = j72;
    }

    @DexIgnore
    @Override // com.fossil.Ld2
    public final void u0(int i) throws RemoteException {
        this.b.a(new Status(i));
    }
}
