package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qg3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Ln3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Ng3 c;

    @DexIgnore
    public Qg3(Ng3 ng3, Ln3 ln3) {
        this.c = ng3;
        this.b = ln3;
    }

    @DexIgnore
    public final void run() {
        this.b.b();
        if (Yr3.a()) {
            this.b.c().y(this);
            return;
        }
        boolean d = this.c.d();
        this.c.c = 0;
        if (d) {
            this.c.b();
        }
    }
}
