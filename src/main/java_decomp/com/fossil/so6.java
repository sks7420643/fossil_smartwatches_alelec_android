package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.M47;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class So6 extends BaseFragment implements Ro6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public G37<Xa5> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return So6.i;
        }

        @DexIgnore
        public final So6 b() {
            return new So6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ So6 b;

        @DexIgnore
        public Bi(So6 so6) {
            this.b = so6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = M47.a(M47.Ci.FEATURES, M47.Bi.SHOP_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = So6.j.a();
            local.d(a3, "Purchase Battery URL = " + a2);
            So6 so6 = this.b;
            Wg6.b(a2, "url");
            so6.N6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ So6 b;

        @DexIgnore
        public Ci(So6 so6) {
            this.b = so6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = M47.a(M47.Ci.FEATURES, M47.Bi.LOW_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = So6.j.a();
            local.d(a3, "Need Help URL = " + a2);
            So6 so6 = this.b;
            Wg6.b(a2, "url");
            so6.N6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ So6 b;

        @DexIgnore
        public Di(So6 so6) {
            this.b = so6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    /*
    static {
        String simpleName = So6.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "ReplaceBatteryFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Qo6 qo6) {
        M6(qo6);
    }

    @DexIgnore
    public void M6(Qo6 qo6) {
        Wg6.c(qo6, "presenter");
        I14.l(qo6);
        Wg6.b(qo6, "Preconditions.checkNotNull(presenter)");
    }

    @DexIgnore
    public final void N6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), i);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Xa5 xa5 = (Xa5) Aq0.f(LayoutInflater.from(getContext()), 2131558618, null, false, A6());
        this.g = new G37<>(this, xa5);
        Wg6.b(xa5, "binding");
        return xa5.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Xa5> g37 = this.g;
        if (g37 != null) {
            Xa5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new Bi(this));
                a2.r.setOnClickListener(new Ci(this));
                a2.q.setOnClickListener(new Di(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
