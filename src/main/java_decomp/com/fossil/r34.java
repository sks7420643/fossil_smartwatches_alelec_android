package com.fossil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R34<E> extends S14<E> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    public R34() {
        super(new LinkedHashMap());
    }

    @DexIgnore
    public R34(int i) {
        super(X34.k(i));
    }

    @DexIgnore
    public static <E> R34<E> create() {
        return new R34<>();
    }

    @DexIgnore
    public static <E> R34<E> create(int i) {
        return new R34<>(i);
    }

    @DexIgnore
    public static <E> R34<E> create(Iterable<? extends E> iterable) {
        R34<E> create = create(D44.e(iterable));
        O34.a(create, iterable);
        return create;
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int c = V44.c(objectInputStream);
        setBackingMap(new LinkedHashMap());
        V44.b(this, objectInputStream, c);
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        V44.e(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.S14, com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ int add(Object obj, int i) {
        return super.add(obj, i);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ boolean add(Object obj) {
        return super.add(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.V14
    public /* bridge */ /* synthetic */ boolean addAll(Collection collection) {
        return super.addAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.S14, com.fossil.V14
    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    @DexIgnore
    @Override // com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ boolean contains(Object obj) {
        return super.contains(obj);
    }

    @DexIgnore
    @Override // com.fossil.S14, com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ int count(Object obj) {
        return super.count(obj);
    }

    @DexIgnore
    @Override // com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ Set elementSet() {
        return super.elementSet();
    }

    @DexIgnore
    @Override // com.fossil.S14, com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ Set entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    @Override // com.fossil.V14
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.V14
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.V14
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.S14, com.fossil.V14, java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    @DexIgnore
    @Override // com.fossil.S14, com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ int remove(Object obj, int i) {
        return super.remove(obj, i);
    }

    @DexIgnore
    @Override // com.fossil.V14
    public /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.V14
    public /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.V14
    public /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.S14, com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ int setCount(Object obj, int i) {
        return super.setCount(obj, i);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.C44, com.fossil.V14
    public /* bridge */ /* synthetic */ boolean setCount(Object obj, int i, int i2) {
        return super.setCount(obj, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.S14, com.fossil.V14
    public /* bridge */ /* synthetic */ int size() {
        return super.size();
    }

    @DexIgnore
    @Override // com.fossil.V14
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }
}
