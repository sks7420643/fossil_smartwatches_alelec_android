package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4528a;
    @DexIgnore
    public /* final */ xt4 b;
    @DexIgnore
    public /* final */ yt4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {315}, m = "block")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {290}, m = "cancelFriendRequest")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {393, 395}, m = "completePendingConfirmedFriends")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {229}, m = "fetchBlockedFriends")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {207}, m = "fetchFriends")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {177}, m = "fetchReceivedRequestFriends")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {130, 132}, m = "fetchSentRequestFriends")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {66}, m = "respondRequest")
    public static final class h extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(false, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {26}, m = "searchProfile")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {46}, m = "sendRequest")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {259}, m = "unFriend")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {350}, m = "unblock")
    public static final class l extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(zt4 zt4, qn7 qn7) {
            super(qn7);
            this.this$0 = zt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore
    public zt4(xt4 xt4, yt4 yt4, on5 on5) {
        pq7.c(xt4, "local");
        pq7.c(yt4, "remote");
        pq7.c(on5, "shared");
        this.b = xt4;
        this.c = yt4;
        String simpleName = zt4.class.getSimpleName();
        pq7.b(simpleName, "FriendRepository::class.java.simpleName");
        this.f4528a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.xs4 r10, com.fossil.qn7<? super com.fossil.kz4<java.lang.String>> r11) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.a(com.fossil.xs4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.xs4 r11, com.fossil.qn7<? super com.fossil.kz4<java.lang.String>> r12) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.b(com.fossil.xs4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void c() {
        this.b.a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.d(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final int e() {
        return this.b.b();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.xs4>>> r8) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.f(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.xs4>>> r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            boolean r0 = r7 instanceof com.fossil.zt4.e
            if (r0 == 0) goto L_0x0073
            r0 = r7
            com.fossil.zt4$e r0 = (com.fossil.zt4.e) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0073
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0081
            if (r4 != r5) goto L_0x0079
            java.lang.Object r0 = r0.L$0
            com.fossil.zt4 r0 = (com.fossil.zt4) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x009b
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x0094
            java.util.List r0 = r0.get_items()
        L_0x003d:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r6.f4528a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "fetchFriends - data: "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            if (r0 == 0) goto L_0x0069
            com.fossil.xt4 r1 = r6.b
            r1.q(r0)
            com.fossil.xt4 r1 = r6.b
            java.util.List r1 = r1.j()
            r6.o(r1, r0)
        L_0x0069:
            if (r0 == 0) goto L_0x0096
        L_0x006b:
            com.fossil.kz4 r1 = new com.fossil.kz4
            r3 = 2
            r1.<init>(r0, r2, r3, r2)
            r0 = r1
        L_0x0072:
            return r0
        L_0x0073:
            com.fossil.zt4$e r0 = new com.fossil.zt4$e
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0079:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0081:
            com.fossil.el7.b(r1)
            com.fossil.yt4 r1 = r6.c
            r0.L$0 = r6
            r0.label = r5
            java.lang.String r4 = "friend"
            java.lang.Object r1 = r1.d(r4, r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0072
        L_0x0094:
            r0 = r2
            goto L_0x003d
        L_0x0096:
            java.util.List r0 = com.fossil.hm7.e()
            goto L_0x006b
        L_0x009b:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00da
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r6.f4528a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "fetchFriends - "
            r4.append(r5)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            int r1 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00cf
            java.lang.String r2 = r0.getMessage()
        L_0x00cf:
            com.fossil.kz4 r0 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>(r1, r2)
            r0.<init>(r3)
            goto L_0x0072
        L_0x00da:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.g(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h(com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.xs4>>> r7) {
        /*
        // Method dump skipped, instructions count: 248
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.h(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.xs4>>> r14) {
        /*
        // Method dump skipped, instructions count: 438
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.i(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final xs4 j(String str) {
        pq7.c(str, "id");
        return this.b.h(str);
    }

    @DexIgnore
    public final List<xs4> k() {
        return this.b.k();
    }

    @DexIgnore
    public final List<xs4> l() {
        return this.b.j();
    }

    @DexIgnore
    public final LiveData<List<xs4>> m() {
        return this.b.l();
    }

    @DexIgnore
    public final long n(xs4 xs4) {
        pq7.c(xs4, "friend");
        return this.b.p(xs4);
    }

    @DexIgnore
    public final void o(List<xs4> list, List<xs4> list2) {
        List V = pm7.V(list, list2);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : V) {
            String d2 = ((xs4) obj).d();
            Object obj2 = linkedHashMap.get(d2);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(d2, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            if (((List) entry.getValue()).size() == 1) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<xs4> arrayList = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            mm7.s(arrayList, (List) entry2.getValue());
        }
        ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
        for (xs4 xs4 : arrayList) {
            arrayList2.add(xs4.d());
        }
        xt4 xt4 = this.b;
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            xt4.g((String[]) array);
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object p(boolean r13, com.fossil.xs4 r14, com.fossil.qn7<? super com.fossil.kz4<com.fossil.xs4>> r15) {
        /*
        // Method dump skipped, instructions count: 569
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.p(boolean, com.fossil.xs4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object q(java.lang.String r7, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.xs4>>> r8) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r8 instanceof com.fossil.zt4.i
            if (r0 == 0) goto L_0x0054
            r0 = r8
            com.fossil.zt4$i r0 = (com.fossil.zt4.i) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0054
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0063
            if (r4 != r5) goto L_0x005b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.zt4 r1 = (com.fossil.zt4) r1
            com.fossil.el7.b(r2)
            r7 = r0
        L_0x002d:
            r0 = r2
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r2 = r0 instanceof com.fossil.kq5
            if (r2 == 0) goto L_0x007f
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x0076
            java.util.List r0 = r0.get_items()
            r1 = r0
        L_0x0043:
            if (r1 != 0) goto L_0x0078
            com.fossil.kz4 r0 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            java.lang.String r3 = "code: 200 but null response"
            r1.<init>(r2, r3)
            r0.<init>(r1)
        L_0x0053:
            return r0
        L_0x0054:
            com.fossil.zt4$i r0 = new com.fossil.zt4$i
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.el7.b(r2)
            com.fossil.yt4 r2 = r6.c
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r2 = r2.e(r7, r1)
            if (r2 == r0) goto L_0x0053
            r1 = r6
            goto L_0x002d
        L_0x0076:
            r1 = r3
            goto L_0x0043
        L_0x0078:
            com.fossil.kz4 r0 = new com.fossil.kz4
            r2 = 2
            r0.<init>(r1, r3, r2, r3)
            goto L_0x0053
        L_0x007f:
            boolean r2 = r0 instanceof com.fossil.hq5
            if (r2 == 0) goto L_0x00c7
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r1 = r1.f4528a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "searchProfile - "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = " - "
            r4.append(r5)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r1, r4)
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00cd
            java.lang.String r0 = r0.getMessage()
        L_0x00bb:
            com.fossil.kz4 r1 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>(r2, r0)
            r1.<init>(r3)
            r0 = r1
            goto L_0x0053
        L_0x00c7:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        L_0x00cd:
            r0 = r3
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.q(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(com.fossil.xs4 r8, com.fossil.qn7<? super com.fossil.kz4<java.lang.Boolean>> r9) {
        /*
            r7 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            r6 = 1
            boolean r0 = r9 instanceof com.fossil.zt4.j
            if (r0 == 0) goto L_0x0043
            r0 = r9
            com.fossil.zt4$j r0 = (com.fossil.zt4.j) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0052
            if (r4 != r6) goto L_0x004a
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            com.fossil.xs4 r0 = (com.fossil.xs4) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.zt4 r1 = (com.fossil.zt4) r1
            com.fossil.el7.b(r2)
            r8 = r0
        L_0x0031:
            r0 = r2
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r2 = r0 instanceof com.fossil.kq5
            if (r2 == 0) goto L_0x006f
            com.fossil.kz4 r0 = new com.fossil.kz4
            java.lang.Boolean r1 = com.fossil.ao7.a(r6)
            r2 = 2
            r0.<init>(r1, r3, r2, r3)
        L_0x0042:
            return r0
        L_0x0043:
            com.fossil.zt4$j r0 = new com.fossil.zt4$j
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x004a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0052:
            com.fossil.el7.b(r2)
            java.lang.String r2 = r8.d()
            com.fossil.yt4 r4 = r7.c
            java.lang.String r5 = r8.i()
            r1.L$0 = r7
            r1.L$1 = r8
            r1.L$2 = r2
            r1.label = r6
            java.lang.Object r2 = r4.g(r2, r5, r1)
            if (r2 == r0) goto L_0x0042
            r1 = r7
            goto L_0x0031
        L_0x006f:
            boolean r2 = r0 instanceof com.fossil.hq5
            if (r2 == 0) goto L_0x00cd
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r1 = r1.f4528a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "sendRequest - "
            r4.append(r5)
            java.lang.String r5 = r8.d()
            r4.append(r5)
            java.lang.String r5 = " - "
            r4.append(r5)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r1, r4)
            com.portfolio.platform.data.model.ServerError r1 = r0.c()
            if (r1 == 0) goto L_0x00c8
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x00c8
            int r1 = r1.intValue()
        L_0x00b1:
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00d3
            java.lang.String r0 = r0.getMessage()
        L_0x00bb:
            com.fossil.kz4 r2 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>(r1, r0)
            r2.<init>(r3)
            r0 = r2
            goto L_0x0042
        L_0x00c8:
            int r1 = r0.a()
            goto L_0x00b1
        L_0x00cd:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        L_0x00d3:
            r0 = r3
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.r(com.fossil.xs4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(com.fossil.xs4 r11, com.fossil.qn7<? super com.fossil.kz4<java.lang.String>> r12) {
        /*
        // Method dump skipped, instructions count: 256
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.s(com.fossil.xs4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object t(com.fossil.xs4 r10, com.fossil.qn7<? super com.fossil.kz4<java.lang.String>> r11) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zt4.t(com.fossil.xs4, com.fossil.qn7):java.lang.Object");
    }
}
