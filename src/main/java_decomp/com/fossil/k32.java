package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K32 implements Factory<J32> {
    @DexIgnore
    public /* final */ Provider<T32> a;
    @DexIgnore
    public /* final */ Provider<T32> b;
    @DexIgnore
    public /* final */ Provider<L22> c;
    @DexIgnore
    public /* final */ Provider<P32> d;

    @DexIgnore
    public K32(Provider<T32> provider, Provider<T32> provider2, Provider<L22> provider3, Provider<P32> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static K32 a(Provider<T32> provider, Provider<T32> provider2, Provider<L22> provider3, Provider<P32> provider4) {
        return new K32(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public J32 b() {
        return new J32(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
