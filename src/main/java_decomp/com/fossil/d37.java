package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d37 extends iq4<a, c, b> {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ q27 h;
    @DexIgnore
    public /* final */ PortfolioApp i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f731a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, "deviceId");
            this.f731a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f731a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f732a;

        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMesagge");
            this.f732a = i;
        }

        @DexIgnore
        public final int a() {
            return this.f732a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f733a;

        @DexIgnore
        public c(String str) {
            this.f733a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f733a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {74}, m = "getLocalSecretKey")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ d37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(d37 d37, qn7 qn7) {
            super(qn7);
            this.this$0 = d37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {90}, m = "getServerSecretKey")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ d37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(d37 d37, qn7 qn7) {
            super(qn7);
            this.this$0 = d37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {33, 37, 38}, m = "run")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ d37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(d37 d37, qn7 qn7) {
            super(qn7);
            this.this$0 = d37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public d37(DeviceRepository deviceRepository, UserRepository userRepository, q27 q27, PortfolioApp portfolioApp) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(q27, "mDecryptValueKeyStoreUseCase");
        pq7.c(portfolioApp, "mApp");
        this.f = deviceRepository;
        this.g = userRepository;
        this.h = q27;
        this.i = portfolioApp;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "VerifySecretKeyUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object m(com.fossil.qn7<? super java.lang.String> r9) {
        /*
            r8 = this;
            r2 = 0
            r7 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.d37.d
            if (r0 == 0) goto L_0x0055
            r0 = r9
            com.fossil.d37$d r0 = (com.fossil.d37.d) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0055
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0063
            if (r4 != r7) goto L_0x005b
            java.lang.Object r0 = r0.L$0
            com.fossil.d37 r0 = (com.fossil.d37) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.fossil.iq4$c r0 = (com.fossil.iq4.c) r0
            boolean r1 = r0 instanceof com.fossil.q27.c
            if (r1 == 0) goto L_0x008f
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Get local key success "
            r2.append(r3)
            com.fossil.q27$c r0 = (com.fossil.q27.c) r0
            java.lang.String r3 = r0.a()
            r2.append(r3)
            java.lang.String r3 = "VerifySecretKeyUseCase"
            java.lang.String r2 = r2.toString()
            r1.d(r3, r2)
            java.lang.String r0 = r0.a()
        L_0x0054:
            return r0
        L_0x0055:
            com.fossil.d37$d r0 = new com.fossil.d37$d
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = "VerifySecretKeyUseCase"
            java.lang.String r5 = "getLocalSecretKey"
            r1.d(r4, r5)
            com.fossil.q27 r1 = r8.h
            java.lang.String r4 = r8.e
            if (r4 == 0) goto L_0x0093
            com.fossil.q27$a r5 = new com.fossil.q27$a
            com.fossil.bi5 r6 = new com.fossil.bi5
            r6.<init>()
            r5.<init>(r4, r6)
            r0.L$0 = r8
            r0.label = r7
            java.lang.Object r0 = com.fossil.jq4.a(r1, r5, r0)
            if (r0 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0054
        L_0x008f:
            boolean r0 = r0 instanceof com.fossil.q27.b
            r0 = r2
            goto L_0x0054
        L_0x0093:
            com.fossil.pq7.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.d37.m(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object n(com.fossil.qn7<? super java.lang.String> r7) {
        /*
            r6 = this;
            r2 = 0
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.d37.e
            if (r0 == 0) goto L_0x0055
            r0 = r7
            com.fossil.d37$e r0 = (com.fossil.d37.e) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0055
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0063
            if (r4 != r5) goto L_0x005b
            java.lang.Object r0 = r0.L$0
            com.fossil.d37 r0 = (com.fossil.d37) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getServerSecretKey "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "VerifySecretKeyUseCase"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x007a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x0076
            java.lang.String r0 = (java.lang.String) r0
        L_0x0054:
            return r0
        L_0x0055:
            com.fossil.d37$e r0 = new com.fossil.d37$e
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.el7.b(r1)
            com.portfolio.platform.data.source.DeviceRepository r1 = r6.f
            java.lang.String r4 = r6.d
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r1.getDeviceSecretKey(r4, r0)
            if (r0 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0054
        L_0x0076:
            com.fossil.pq7.i()
            throw r2
        L_0x007a:
            boolean r0 = r0 instanceof com.fossil.hq5
            if (r0 == 0) goto L_0x0080
            r0 = r2
            goto L_0x0054
        L_0x0080:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.d37.n(com.fossil.qn7):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008e, code lost:
        if (r1.j(new com.fossil.d37.c(r5)) == null) goto L_0x0157;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* renamed from: o */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.d37.a r11, com.fossil.qn7<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 362
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.d37.k(com.fossil.d37$a, com.fossil.qn7):java.lang.Object");
    }
}
