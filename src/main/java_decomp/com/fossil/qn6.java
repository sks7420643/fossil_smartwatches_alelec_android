package com.fossil;

import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qn6 implements Factory<Pn6> {
    @DexIgnore
    public /* final */ Provider<WorkoutSessionRepository> a;

    @DexIgnore
    public Qn6(Provider<WorkoutSessionRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Qn6 a(Provider<WorkoutSessionRepository> provider) {
        return new Qn6(provider);
    }

    @DexIgnore
    public static Pn6 c(WorkoutSessionRepository workoutSessionRepository) {
        return new Pn6(workoutSessionRepository);
    }

    @DexIgnore
    public Pn6 b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
