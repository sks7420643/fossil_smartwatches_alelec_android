package com.fossil;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O74 implements Ge4, Fe4 {
    @DexIgnore
    public /* final */ Map<Class<?>, ConcurrentHashMap<Ee4<Object>, Executor>> a; // = new HashMap();
    @DexIgnore
    public Queue<De4<?>> b; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public O74(Executor executor) {
        this.c = executor;
    }

    @DexIgnore
    @Override // com.fossil.Ge4
    public <T> void a(Class<T> cls, Ee4<? super T> ee4) {
        b(cls, this.c, ee4);
    }

    @DexIgnore
    @Override // com.fossil.Ge4
    public <T> void b(Class<T> cls, Executor executor, Ee4<? super T> ee4) {
        synchronized (this) {
            R74.b(cls);
            R74.b(ee4);
            R74.b(executor);
            if (!this.a.containsKey(cls)) {
                this.a.put(cls, new ConcurrentHashMap<>());
            }
            this.a.get(cls).put(ee4, executor);
        }
    }

    @DexIgnore
    public void c() {
        Queue<De4<?>> queue = null;
        synchronized (this) {
            if (this.b != null) {
                queue = this.b;
                this.b = null;
            }
        }
        if (queue != null) {
            for (De4<?> de4 : queue) {
                f(de4);
            }
        }
    }

    @DexIgnore
    public final Set<Map.Entry<Ee4<Object>, Executor>> d(De4<?> de4) {
        Set<Map.Entry<Ee4<Object>, Executor>> emptySet;
        synchronized (this) {
            ConcurrentHashMap<Ee4<Object>, Executor> concurrentHashMap = this.a.get(de4.b());
            emptySet = concurrentHashMap == null ? Collections.emptySet() : concurrentHashMap.entrySet();
        }
        return emptySet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r2.hasNext() == false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r0 = r2.next();
        r0.getValue().execute(com.fossil.N74.a(r0, r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r2 = d(r4).iterator();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f(com.fossil.De4<?> r4) {
        /*
            r3 = this;
            com.fossil.R74.b(r4)
            monitor-enter(r3)
            java.util.Queue<com.fossil.De4<?>> r0 = r3.b     // Catch:{ all -> 0x0032 }
            if (r0 == 0) goto L_0x000f
            java.util.Queue<com.fossil.De4<?>> r0 = r3.b     // Catch:{ all -> 0x0032 }
            r0.add(r4)     // Catch:{ all -> 0x0032 }
            monitor-exit(r3)     // Catch:{ all -> 0x0032 }
        L_0x000e:
            return
        L_0x000f:
            monitor-exit(r3)     // Catch:{ all -> 0x0032 }
            java.util.Set r0 = r3.d(r4)
            java.util.Iterator r2 = r0.iterator()
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x000e
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getValue()
            java.util.concurrent.Executor r1 = (java.util.concurrent.Executor) r1
            java.lang.Runnable r0 = com.fossil.N74.a(r0, r4)
            r1.execute(r0)
            goto L_0x0018
        L_0x0032:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.O74.f(com.fossil.De4):void");
    }
}
