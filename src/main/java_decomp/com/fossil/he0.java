package com.fossil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.Ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public class He0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<He0> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ boolean b; // = false;
    @DexIgnore
    public /* final */ Handler c; // = null;
    @DexIgnore
    public Ge0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Parcelable.Creator<He0> {
        @DexIgnore
        public He0 a(Parcel parcel) {
            return new He0(parcel);
        }

        @DexIgnore
        public He0[] b(int i) {
            return new He0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ He0 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ He0[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Ge0.Ai {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.Ge0
        public void T2(int i, Bundle bundle) {
            He0 he0 = He0.this;
            Handler handler = he0.c;
            if (handler != null) {
                handler.post(new Ci(i, bundle));
            } else {
                he0.a(i, bundle);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Bundle c;

        @DexIgnore
        public Ci(int i, Bundle bundle) {
            this.b = i;
            this.c = bundle;
        }

        @DexIgnore
        public void run() {
            He0.this.a(this.b, this.c);
        }
    }

    @DexIgnore
    public He0(Parcel parcel) {
        this.d = Ge0.Ai.d(parcel.readStrongBinder());
    }

    @DexIgnore
    public void a(int i, Bundle bundle) {
    }

    @DexIgnore
    public void b(int i, Bundle bundle) {
        if (this.b) {
            Handler handler = this.c;
            if (handler != null) {
                handler.post(new Ci(i, bundle));
            } else {
                a(i, bundle);
            }
        } else {
            Ge0 ge0 = this.d;
            if (ge0 != null) {
                try {
                    ge0.T2(i, bundle);
                } catch (RemoteException e) {
                }
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.d == null) {
                this.d = new Bi();
            }
            parcel.writeStrongBinder(this.d.asBinder());
        }
    }
}
