package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Xz7 {
    @DexIgnore
    public static /* final */ int a; // = Runtime.getRuntime().availableProcessors();

    @DexIgnore
    public static final int a() {
        return a;
    }

    @DexIgnore
    public static final String b(String str) {
        try {
            return System.getProperty(str);
        } catch (SecurityException e) {
            return null;
        }
    }
}
