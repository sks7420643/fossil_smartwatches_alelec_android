package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oj6 implements Factory<DashboardSleepPresenter> {
    @DexIgnore
    public static DashboardSleepPresenter a(Lj6 lj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        return new DashboardSleepPresenter(lj6, sleepSummariesRepository, sleepSessionsRepository, fitnessDataRepository, userRepository, u04);
    }
}
