package com.fossil;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lt7 {
    @DexIgnore
    public static final Ht7 e(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new It7(matcher, charSequence);
    }

    @DexIgnore
    public static final Ht7 f(Matcher matcher, CharSequence charSequence) {
        if (!matcher.matches()) {
            return null;
        }
        return new It7(matcher, charSequence);
    }

    @DexIgnore
    public static final Wr7 g(MatchResult matchResult) {
        return Bs7.m(matchResult.start(), matchResult.end());
    }

    @DexIgnore
    public static final int h(Iterable<? extends Gt7> iterable) {
        int i = 0;
        for (Gt7 gt7 : iterable) {
            i = gt7.getValue() | i;
        }
        return i;
    }
}
