package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.InstalledApp;
import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qj5 implements Mb1 {
    @DexIgnore
    public /* final */ InstalledApp b;

    @DexIgnore
    public Qj5(InstalledApp installedApp) {
        Wg6.c(installedApp, "installedApp");
        this.b = installedApp;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        Wg6.c(messageDigest, "messageDigest");
        String identifier = this.b.getIdentifier();
        Wg6.b(identifier, "installedApp.identifier");
        Charset charset = Mb1.a;
        Wg6.b(charset, "Key.CHARSET");
        if (identifier != null) {
            byte[] bytes = identifier.getBytes(charset);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final InstalledApp c() {
        return this.b;
    }
}
