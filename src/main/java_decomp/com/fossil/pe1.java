package com.fossil;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pe1 implements Jb1<ByteBuffer> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.io.File, com.fossil.Ob1] */
    @Override // com.fossil.Jb1
    public /* bridge */ /* synthetic */ boolean a(ByteBuffer byteBuffer, File file, Ob1 ob1) {
        return c(byteBuffer, file, ob1);
    }

    @DexIgnore
    public boolean c(ByteBuffer byteBuffer, File file, Ob1 ob1) {
        try {
            Zj1.e(byteBuffer, file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("ByteBufferEncoder", 3)) {
                Log.d("ByteBufferEncoder", "Failed to write data", e);
            }
            return false;
        }
    }
}
