package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.Ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mg0 extends Gg0 implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, Ig0, View.OnKeyListener {
    @DexIgnore
    public static /* final */ int B; // = Re0.abc_popup_menu_item_layout;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ Cg0 d;
    @DexIgnore
    public /* final */ Bg0 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ Ih0 j;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener k; // = new Ai();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener l; // = new Bi();
    @DexIgnore
    public PopupWindow.OnDismissListener m;
    @DexIgnore
    public View s;
    @DexIgnore
    public View t;
    @DexIgnore
    public Ig0.Ai u;
    @DexIgnore
    public ViewTreeObserver v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (Mg0.this.a() && !Mg0.this.j.w()) {
                View view = Mg0.this.t;
                if (view == null || !view.isShown()) {
                    Mg0.this.dismiss();
                } else {
                    Mg0.this.j.show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements View.OnAttachStateChangeListener {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = Mg0.this.v;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    Mg0.this.v = view.getViewTreeObserver();
                }
                Mg0 mg0 = Mg0.this;
                mg0.v.removeGlobalOnLayoutListener(mg0.k);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore
    public Mg0(Context context, Cg0 cg0, View view, int i2, int i3, boolean z2) {
        this.c = context;
        this.d = cg0;
        this.f = z2;
        this.e = new Bg0(cg0, LayoutInflater.from(context), this.f, B);
        this.h = i2;
        this.i = i3;
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(Oe0.abc_config_prefDialogWidth));
        this.s = view;
        this.j = new Ih0(this.c, null, this.h, this.i);
        cg0.c(this, context);
    }

    @DexIgnore
    public final boolean A() {
        View view;
        if (a()) {
            return true;
        }
        if (this.w || (view = this.s) == null) {
            return false;
        }
        this.t = view;
        this.j.F(this);
        this.j.G(this);
        this.j.E(true);
        View view2 = this.t;
        boolean z2 = this.v == null;
        ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
        this.v = viewTreeObserver;
        if (z2) {
            viewTreeObserver.addOnGlobalLayoutListener(this.k);
        }
        view2.addOnAttachStateChangeListener(this.l);
        this.j.y(view2);
        this.j.B(this.z);
        if (!this.x) {
            this.y = Gg0.p(this.e, null, this.c, this.g);
            this.x = true;
        }
        this.j.A(this.y);
        this.j.D(2);
        this.j.C(o());
        this.j.show();
        ListView j2 = this.j.j();
        j2.setOnKeyListener(this);
        if (this.A && this.d.z() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.c).inflate(Re0.abc_popup_menu_header_item_layout, (ViewGroup) j2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.d.z());
            }
            frameLayout.setEnabled(false);
            j2.addHeaderView(frameLayout, null, false);
        }
        this.j.o(this.e);
        this.j.show();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public boolean a() {
        return !this.w && this.j.a();
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void b(Cg0 cg0, boolean z2) {
        if (cg0 == this.d) {
            dismiss();
            Ig0.Ai ai = this.u;
            if (ai != null) {
                ai.b(cg0, z2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void c(boolean z2) {
        this.x = false;
        Bg0 bg0 = this.e;
        if (bg0 != null) {
            bg0.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public void dismiss() {
        if (a()) {
            this.j.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void g(Ig0.Ai ai) {
        this.u = ai;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void i(Parcelable parcelable) {
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public ListView j() {
        return this.j.j();
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean k(Ng0 ng0) {
        if (ng0.hasVisibleItems()) {
            Hg0 hg0 = new Hg0(this.c, ng0, this.t, this.f, this.h, this.i);
            hg0.j(this.u);
            hg0.g(Gg0.y(ng0));
            hg0.i(this.m);
            this.m = null;
            this.d.e(false);
            int c2 = this.j.c();
            int n = this.j.n();
            if ((Gravity.getAbsoluteGravity(this.z, Mo0.z(this.s)) & 7) == 5) {
                c2 += this.s.getWidth();
            }
            if (hg0.n(c2, n)) {
                Ig0.Ai ai = this.u;
                if (ai != null) {
                    ai.c(ng0);
                }
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public Parcelable l() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void m(Cg0 cg0) {
    }

    @DexIgnore
    public void onDismiss() {
        this.w = true;
        this.d.close();
        ViewTreeObserver viewTreeObserver = this.v;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.v = this.t.getViewTreeObserver();
            }
            this.v.removeGlobalOnLayoutListener(this.k);
            this.v = null;
        }
        this.t.removeOnAttachStateChangeListener(this.l);
        PopupWindow.OnDismissListener onDismissListener = this.m;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void q(View view) {
        this.s = view;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void s(boolean z2) {
        this.e.d(z2);
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public void show() {
        if (!A()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void t(int i2) {
        this.z = i2;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void u(int i2) {
        this.j.e(i2);
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void v(PopupWindow.OnDismissListener onDismissListener) {
        this.m = onDismissListener;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void w(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void x(int i2) {
        this.j.k(i2);
    }
}
