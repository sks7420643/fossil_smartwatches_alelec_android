package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fz0 implements Gz0 {
    @DexIgnore
    public /* final */ ViewOverlay a;

    @DexIgnore
    public Fz0(View view) {
        this.a = view.getOverlay();
    }

    @DexIgnore
    @Override // com.fossil.Gz0
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Gz0
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }
}
