package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ft1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Ft1(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ft1)) {
            return false;
        }
        Ft1 ft1 = (Ft1) obj;
        if (!Wg6.a(this.a, ft1.a)) {
            return false;
        }
        return !(Wg6.a(this.b, ft1.b) ^ true);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        String str = this.b;
        return (str != null ? str.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    public String toString() {
        StringBuilder e = E.e("UserCredential(userId=");
        e.append(this.a);
        e.append(", refreshToken=");
        e.append(this.b);
        e.append(")");
        return e.toString();
    }
}
