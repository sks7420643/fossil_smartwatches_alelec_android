package com.fossil;

import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ij5 {
    @DexIgnore
    public static final UserDisplayUnit a(MFUser mFUser) {
        String str = null;
        if (mFUser == null) {
            return null;
        }
        MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
        String temperature = unitGroup != null ? unitGroup.getTemperature() : null;
        UserDisplayUnit.TemperatureUnit temperatureUnit = Wg6.a(temperature, Ai5.METRIC.getValue()) ? UserDisplayUnit.TemperatureUnit.C : Wg6.a(temperature, Ai5.IMPERIAL.getValue()) ? UserDisplayUnit.TemperatureUnit.F : UserDisplayUnit.TemperatureUnit.C;
        MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
        if (unitGroup2 != null) {
            str = unitGroup2.getDistance();
        }
        return new UserDisplayUnit(temperatureUnit, Wg6.a(str, Ai5.METRIC.getValue()) ? UserDisplayUnit.DistanceUnit.KM : Wg6.a(str, Ai5.IMPERIAL.getValue()) ? UserDisplayUnit.DistanceUnit.MILE : UserDisplayUnit.DistanceUnit.KM);
    }

    @DexIgnore
    public static final String b(String str, String str2) {
        Wg6.c(str2, DatabaseFieldConfigLoader.FIELD_NAME_DEFAULT_VALUE);
        return str == null || Vt7.l(str) ? str2 : str;
    }
}
