package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iq7 implements Es7<Object>, Hq7 {
    @DexIgnore
    public static /* final */ Map<Class<? extends Uk7<?>>, Integer> c;
    @DexIgnore
    public static /* final */ HashMap<String, String> d;
    @DexIgnore
    public static /* final */ HashMap<String, String> e;
    @DexIgnore
    public static /* final */ HashMap<String, String> f;
    @DexIgnore
    public static /* final */ Map<String, String> g;
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public /* final */ Class<?> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x003f, code lost:
            if (r0 != null) goto L_0x0041;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.String a(java.lang.Class<?> r6) {
            /*
                r5 = this;
                r4 = 2
                r1 = 0
                java.lang.String r0 = "jClass"
                com.mapped.Wg6.c(r6, r0)
                boolean r0 = r6.isAnonymousClass()
                java.lang.String r2 = "Array"
                if (r0 == 0) goto L_0x0011
                r0 = r1
            L_0x0010:
                return r0
            L_0x0011:
                boolean r0 = r6.isLocalClass()
                if (r0 == 0) goto L_0x0076
                java.lang.String r2 = r6.getSimpleName()
                java.lang.reflect.Method r0 = r6.getEnclosingMethod()
                if (r0 == 0) goto L_0x004f
                java.lang.String r3 = "name"
                com.mapped.Wg6.b(r2, r3)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r0 = r0.getName()
                r3.append(r0)
                java.lang.String r0 = "$"
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                java.lang.String r0 = com.fossil.Wt7.j0(r2, r0, r1, r4, r1)
                if (r0 == 0) goto L_0x004f
            L_0x0041:
                if (r0 != 0) goto L_0x0010
                java.lang.String r0 = "name"
                com.mapped.Wg6.b(r2, r0)
                r0 = 36
                java.lang.String r0 = com.fossil.Wt7.i0(r2, r0, r1, r4, r1)
                goto L_0x0010
            L_0x004f:
                java.lang.reflect.Constructor r0 = r6.getEnclosingConstructor()
                if (r0 == 0) goto L_0x0074
                java.lang.String r3 = "name"
                com.mapped.Wg6.b(r2, r3)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r0 = r0.getName()
                r3.append(r0)
                java.lang.String r0 = "$"
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                java.lang.String r0 = com.fossil.Wt7.j0(r2, r0, r1, r4, r1)
                goto L_0x0041
            L_0x0074:
                r0 = r1
                goto L_0x0041
            L_0x0076:
                boolean r0 = r6.isArray()
                if (r0 == 0) goto L_0x00b2
                java.lang.Class r0 = r6.getComponentType()
                java.lang.String r3 = "componentType"
                com.mapped.Wg6.b(r0, r3)
                boolean r3 = r0.isPrimitive()
                if (r3 == 0) goto L_0x00c8
                java.util.Map r3 = com.fossil.Iq7.c()
                java.lang.String r0 = r0.getName()
                java.lang.Object r0 = r3.get(r0)
                java.lang.String r0 = (java.lang.String) r0
                if (r0 == 0) goto L_0x00c8
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                r1.append(r0)
                java.lang.String r0 = "Array"
                r1.append(r0)
                java.lang.String r1 = r1.toString()
                r0 = r1
            L_0x00ad:
                if (r0 != 0) goto L_0x0010
                r0 = r2
                goto L_0x0010
            L_0x00b2:
                java.util.Map r0 = com.fossil.Iq7.c()
                java.lang.String r1 = r6.getName()
                java.lang.Object r0 = r0.get(r1)
                java.lang.String r0 = (java.lang.String) r0
                if (r0 != 0) goto L_0x0010
                java.lang.String r0 = r6.getSimpleName()
                goto L_0x0010
            L_0x00c8:
                r0 = r1
                goto L_0x00ad
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iq7.Ai.a(java.lang.Class):java.lang.String");
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v40, resolved type: java.util.HashMap<java.lang.String, java.lang.String> */
    /* JADX WARN: Multi-variable type inference failed */
    /*
    static {
        int i = 0;
        List h2 = Hm7.h(Gg6.class, Hg6.class, Coroutine.class, Wp7.class, Xp7.class, Yp7.class, Zp7.class, Aq7.class, Bq7.class, Cq7.class, Hp7.class, Ip7.class, Jp7.class, Kp7.class, Lp7.class, Mp7.class, Np7.class, Op7.class, Pp7.class, Qp7.class, Sp7.class, Tp7.class, Up7.class);
        ArrayList arrayList = new ArrayList(Im7.m(h2, 10));
        for (Object obj : h2) {
            if (i >= 0) {
                arrayList.add(Hl7.a((Class) obj, Integer.valueOf(i)));
                i++;
            } else {
                Hm7.l();
                throw null;
            }
        }
        c = Zm7.n(arrayList);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("boolean", "kotlin.Boolean");
        hashMap.put(LegacyTokenHelper.TYPE_CHAR, "kotlin.Char");
        hashMap.put(LegacyTokenHelper.TYPE_BYTE, "kotlin.Byte");
        hashMap.put(LegacyTokenHelper.TYPE_SHORT, "kotlin.Short");
        hashMap.put(LegacyTokenHelper.TYPE_INTEGER, "kotlin.Int");
        hashMap.put(LegacyTokenHelper.TYPE_FLOAT, "kotlin.Float");
        hashMap.put(LegacyTokenHelper.TYPE_LONG, "kotlin.Long");
        hashMap.put(LegacyTokenHelper.TYPE_DOUBLE, "kotlin.Double");
        d = hashMap;
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("java.lang.Boolean", "kotlin.Boolean");
        hashMap2.put("java.lang.Character", "kotlin.Char");
        hashMap2.put("java.lang.Byte", "kotlin.Byte");
        hashMap2.put("java.lang.Short", "kotlin.Short");
        hashMap2.put("java.lang.Integer", "kotlin.Int");
        hashMap2.put("java.lang.Float", "kotlin.Float");
        hashMap2.put("java.lang.Long", "kotlin.Long");
        hashMap2.put("java.lang.Double", "kotlin.Double");
        e = hashMap2;
        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("java.lang.Object", "kotlin.Any");
        hashMap3.put("java.lang.String", "kotlin.String");
        hashMap3.put("java.lang.CharSequence", "kotlin.CharSequence");
        hashMap3.put("java.lang.Throwable", "kotlin.Throwable");
        hashMap3.put("java.lang.Cloneable", "kotlin.Cloneable");
        hashMap3.put("java.lang.Number", "kotlin.Number");
        hashMap3.put("java.lang.Comparable", "kotlin.Comparable");
        hashMap3.put("java.lang.Enum", "kotlin.Enum");
        hashMap3.put("java.lang.annotation.Annotation", "kotlin.Annotation");
        hashMap3.put("java.lang.Iterable", "kotlin.collections.Iterable");
        hashMap3.put("java.util.Iterator", "kotlin.collections.Iterator");
        hashMap3.put("java.util.Collection", "kotlin.collections.Collection");
        hashMap3.put("java.util.List", "kotlin.collections.List");
        hashMap3.put("java.util.Set", "kotlin.collections.Set");
        hashMap3.put("java.util.ListIterator", "kotlin.collections.ListIterator");
        hashMap3.put("java.util.Map", "kotlin.collections.Map");
        hashMap3.put("java.util.Map$Entry", "kotlin.collections.Map.Entry");
        hashMap3.put("kotlin.jvm.internal.StringCompanionObject", "kotlin.String.Companion");
        hashMap3.put("kotlin.jvm.internal.EnumCompanionObject", "kotlin.Enum.Companion");
        hashMap3.putAll(d);
        hashMap3.putAll(e);
        Collection<String> values = d.values();
        Wg6.b(values, "primitiveFqNames.values");
        for (T t : values) {
            StringBuilder sb = new StringBuilder();
            sb.append("kotlin.jvm.internal.");
            Wg6.b(t, "kotlinName");
            sb.append(Wt7.l0(t, '.', null, 2, null));
            sb.append("CompanionObject");
            String sb2 = sb.toString();
            Lc6 a2 = Hl7.a(sb2, ((String) t) + ".Companion");
            hashMap3.put(a2.getFirst(), a2.getSecond());
        }
        for (Map.Entry<Class<? extends Uk7<?>>, Integer> entry : c.entrySet()) {
            int intValue = entry.getValue().intValue();
            String name = entry.getKey().getName();
            hashMap3.put(name, "kotlin.Function" + intValue);
        }
        f = hashMap3;
        LinkedHashMap linkedHashMap = new LinkedHashMap(Ym7.b(hashMap3.size()));
        for (Map.Entry entry2 : hashMap3.entrySet()) {
            linkedHashMap.put(entry2.getKey(), Wt7.l0((String) entry2.getValue(), '.', null, 2, null));
        }
        g = linkedHashMap;
    }
    */

    @DexIgnore
    public Iq7(Class<?> cls) {
        Wg6.c(cls, "jClass");
        this.b = cls;
    }

    @DexIgnore
    @Override // com.fossil.Es7
    public String a() {
        return h.a(b());
    }

    @DexIgnore
    @Override // com.fossil.Hq7
    public Class<?> b() {
        return this.b;
    }

    @DexIgnore
    public final Void d() {
        throw new Fp7();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Iq7) && Wg6.a(Ep7.b(this), Ep7.b((Es7) obj));
    }

    @DexIgnore
    @Override // com.fossil.Cs7
    public List<Annotation> getAnnotations() {
        d();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return Ep7.b(this).hashCode();
    }

    @DexIgnore
    public String toString() {
        return b().toString() + " (Kotlin reflection is not available)";
    }
}
