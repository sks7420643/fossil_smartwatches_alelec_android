package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N extends Wk1 {
    @DexIgnore
    public /* final */ String[] b;

    @DexIgnore
    public N(String[] strArr) {
        this.b = strArr;
    }

    @DexIgnore
    @Override // com.fossil.Wk1
    public boolean a(E60 e60) {
        if (this.b.length == 0) {
            return true;
        }
        String serialNumber = e60.u.getSerialNumber();
        for (String str : this.b) {
            if (Vt7.s(serialNumber, str, false, 2, null)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.H1, "serial_number_prefixes"), Jd0.I1, Ay1.b(this.b));
    }
}
