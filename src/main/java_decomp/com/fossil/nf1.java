package com.fossil;

import android.content.Context;
import android.net.Uri;
import com.fossil.Af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nf1 implements Af1<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Bf1<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public Ai(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, InputStream> b(Ef1 ef1) {
            return new Nf1(this.a);
        }
    }

    @DexIgnore
    public Nf1(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(Uri uri, int i, int i2, Ob1 ob1) {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> c(Uri uri, int i, int i2, Ob1 ob1) {
        if (Jc1.d(i, i2)) {
            return new Af1.Ai<>(new Yj1(uri), Kc1.e(this.a, uri));
        }
        return null;
    }

    @DexIgnore
    public boolean d(Uri uri) {
        return Jc1.a(uri);
    }
}
