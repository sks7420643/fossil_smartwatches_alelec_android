package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mr3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ U93 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService e;

    @DexIgnore
    public Mr3(AppMeasurementDynamiteService appMeasurementDynamiteService, U93 u93, String str, String str2) {
        this.e = appMeasurementDynamiteService;
        this.b = u93;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    public final void run() {
        this.e.b.O().I(this.b, this.c, this.d);
    }
}
