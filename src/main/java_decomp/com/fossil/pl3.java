package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pl3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public long c;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public Pl3(String str, String str2, Bundle bundle, long j) {
        this.a = str;
        this.b = str2;
        this.d = bundle == null ? new Bundle() : bundle;
        this.c = j;
    }

    @DexIgnore
    public static Pl3 b(Vg3 vg3) {
        return new Pl3(vg3.b, vg3.d, vg3.c.k(), vg3.e);
    }

    @DexIgnore
    public final Vg3 a() {
        return new Vg3(this.a, new Ug3(new Bundle(this.d)), this.b, this.c);
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String str2 = this.a;
        String valueOf = String.valueOf(this.d);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }
}
