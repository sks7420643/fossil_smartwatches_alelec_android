package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vh1<Z> implements Th1<Z, Z> {
    @DexIgnore
    public static /* final */ Vh1<?> a; // = new Vh1<>();

    @DexIgnore
    public static <Z> Th1<Z, Z> b() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.Th1
    public Id1<Z> a(Id1<Z> id1, Ob1 ob1) {
        return id1;
    }
}
