package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rt5 implements Factory<SwitchActiveDeviceUseCase> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<Cj4> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<An4> e;

    @DexIgnore
    public Rt5(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<Cj4> provider3, Provider<PortfolioApp> provider4, Provider<An4> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static Rt5 a(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<Cj4> provider3, Provider<PortfolioApp> provider4, Provider<An4> provider5) {
        return new Rt5(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static SwitchActiveDeviceUseCase c(UserRepository userRepository, DeviceRepository deviceRepository, Cj4 cj4, PortfolioApp portfolioApp, An4 an4) {
        return new SwitchActiveDeviceUseCase(userRepository, deviceRepository, cj4, portfolioApp, an4);
    }

    @DexIgnore
    public SwitchActiveDeviceUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
