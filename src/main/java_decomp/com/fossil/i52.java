package com.fossil;

import android.content.Context;
import android.os.Binder;
import com.fossil.R62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I52 extends D52 {
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public I52(Context context) {
        this.b = context;
    }

    @DexIgnore
    @Override // com.fossil.C52
    public final void a() {
        e();
        O42 b2 = O42.b(this.b);
        GoogleSignInAccount c = b2.c();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.v;
        if (c != null) {
            googleSignInOptions = b2.d();
        }
        R62.Ai ai = new R62.Ai(this.b);
        ai.b(D42.e, googleSignInOptions);
        R62 g = ai.g();
        try {
            if (g.d().A()) {
                if (c != null) {
                    D42.f.b(g);
                } else {
                    g.e();
                }
            }
        } finally {
            g.g();
        }
    }

    @DexIgnore
    public final void e() {
        if (!H62.i(this.b, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.C52
    public final void p() {
        e();
        B52.c(this.b).a();
    }
}
