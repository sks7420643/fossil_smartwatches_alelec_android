package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xo3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public Xo3(String str, String str2, long j) {
        this(str, str2, j, false, 0);
    }

    @DexIgnore
    public Xo3(String str, String str2, long j, boolean z, long j2) {
        this.a = str;
        this.b = str2;
        this.c = j;
        this.d = false;
        this.e = z;
        this.f = j2;
    }
}
