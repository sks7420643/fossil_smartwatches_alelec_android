package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ps extends Ns {
    @DexIgnore
    public /* final */ byte[] A; // = new byte[0];
    @DexIgnore
    public byte[] B; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] C; // = new byte[0];
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ boolean F; // = true;

    @DexIgnore
    public Ps(Hs hs, K5 k5, int i) {
        super(hs, k5, i);
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public final U5 D() {
        ByteBuffer put = ByteBuffer.allocate(M().length + L().length).order(ByteOrder.LITTLE_ENDIAN).put(M()).put(L());
        N6 K = K();
        byte[] array = put.array();
        Wg6.b(array, "byteBuffer.array()");
        return new J6(K, array, this.y.z);
    }

    @DexIgnore
    public abstract Mt E(byte b);

    @DexIgnore
    public JSONObject F(byte[] bArr) {
        this.E = true;
        return new JSONObject();
    }

    @DexIgnore
    public boolean G(O7 o7) {
        return false;
    }

    @DexIgnore
    public final boolean H(O7 o7) {
        if (o7.a != N() || o7.b.length < P().length) {
            return false;
        }
        return Arrays.equals(P(), Dm7.k(o7.b, 0, P().length));
    }

    @DexIgnore
    public void I(O7 o7) {
        JSONObject F2;
        byte[] bArr = o7.b;
        JSONObject jSONObject = new JSONObject();
        if (O()) {
            Mt E2 = E(bArr[P().length]);
            Mw a2 = Mw.a(this.v, null, null, Mw.g.b(E2).d, null, E2, 11);
            this.v = a2;
            if (a2.d == Lw.b) {
                F2 = F(Dm7.k(bArr, P().length + 1, bArr.length));
            } else {
                this.E = true;
                F2 = jSONObject;
            }
        } else {
            F2 = F(Dm7.k(bArr, P().length, bArr.length));
        }
        this.g.add(new Hw(0, o7.a, bArr, F2, 1));
    }

    @DexIgnore
    public void J(O7 o7) {
    }

    @DexIgnore
    public abstract N6 K();

    @DexIgnore
    public byte[] L() {
        return this.C;
    }

    @DexIgnore
    public byte[] M() {
        return this.A;
    }

    @DexIgnore
    public abstract N6 N();

    @DexIgnore
    public boolean O() {
        return this.F;
    }

    @DexIgnore
    public byte[] P() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final void g(U5 u5) {
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final void s(O7 o7) {
        if (H(o7)) {
            I(o7);
        } else if (G(o7)) {
            J(o7);
        }
        if (this.D && this.E) {
            m(this.v);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final void v(U5 u5) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.D = true;
        A90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        A90 a902 = this.f;
        if (!(a902 == null || (jSONObject2 = a902.n) == null)) {
            G80.k(jSONObject2, Jd0.k, Ey1.a(Lw.b));
        }
        if (this.v.d == Lw.c) {
            Mw a2 = Mw.a(this.v, null, null, Mw.g.a(u5.e).d, u5.e, null, 19);
            this.v = a2;
            Lw lw = a2.d;
            Lw lw2 = Lw.b;
        }
        A90 a903 = this.f;
        if (a903 != null) {
            a903.j = true;
        }
        A90 a904 = this.f;
        if (!(a904 == null || (jSONObject = a904.n) == null)) {
            G80.k(jSONObject, Jd0.k, Ey1.a(Lw.b));
        }
        C();
        if (this.E) {
            m(this.v);
        }
    }
}
