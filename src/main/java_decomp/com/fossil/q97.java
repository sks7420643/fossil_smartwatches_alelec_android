package com.fossil;

import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q97 implements Factory<P97> {
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRepository> a;

    @DexIgnore
    public Q97(Provider<WFBackgroundPhotoRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Q97 a(Provider<WFBackgroundPhotoRepository> provider) {
        return new Q97(provider);
    }

    @DexIgnore
    public static P97 c(WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        return new P97(wFBackgroundPhotoRepository);
    }

    @DexIgnore
    public P97 b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
