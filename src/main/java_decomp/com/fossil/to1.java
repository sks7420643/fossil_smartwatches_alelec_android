package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.O80;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class To1 extends O80 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<To1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public To1 a(Parcel parcel) {
            return new To1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public To1 createFromParcel(Parcel parcel) {
            return new To1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public To1[] newArray(int i) {
            return new To1[i];
        }
    }

    @DexIgnore
    public To1() {
        super(Ap1.EMPTY, null, null, 6);
    }

    @DexIgnore
    public /* synthetic */ To1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public To1(Vw1 vw1) {
        super(Ap1.EMPTY, vw1, null, 4);
    }
}
