package com.fossil;

import com.fossil.Tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F26 extends Tq4<Ai, Bi, Tq4.Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Tq4.Bi {
        @DexIgnore
        public /* final */ ContactGroup a;

        @DexIgnore
        public Ai(ContactGroup contactGroup) {
            Wg6.c(contactGroup, "contactGroup");
            this.a = contactGroup;
        }

        @DexIgnore
        public final ContactGroup a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Ci {
        @DexIgnore
        public Bi(boolean z) {
        }
    }

    /*
    static {
        String simpleName = F26.class.getSimpleName();
        Wg6.b(simpleName, "RemoveContactGroup::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public F26(NotificationsRepository notificationsRepository) {
        Wg6.c(notificationsRepository, "notificationsRepository");
        I14.o(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        Wg6.b(notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Ai ai) {
        f(ai);
    }

    @DexIgnore
    public void f(Ai ai) {
        Wg6.c(ai, "requestValues");
        g(ai.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .RemoveContactGroup done");
        b().onSuccess(new Bi(true));
    }

    @DexIgnore
    public final void g(ContactGroup contactGroup) {
        Contact contact = contactGroup.getContacts().get(0);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        StringBuilder sb = new StringBuilder();
        sb.append("Removed contact = ");
        Wg6.b(contact, "contact");
        sb.append(contact.getFirstName());
        sb.append(" row id = ");
        sb.append(contact.getDbRowId());
        local.d(str, sb.toString());
        ArrayList arrayList = new ArrayList();
        for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
            Wg6.b(phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            arrayList.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
        }
        this.d.removeContact(contact);
        this.d.removeContactGroup(contactGroup);
        h(arrayList);
    }

    @DexIgnore
    public final void h(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(phoneFavoritesContact);
        }
    }
}
