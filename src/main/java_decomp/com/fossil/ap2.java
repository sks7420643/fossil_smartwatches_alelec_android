package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ap2 implements Parcelable.Creator<Yo2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Yo2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 1) {
                Ad2.B(parcel, t);
            } else {
                arrayList = Ad2.j(parcel, t, DataType.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Yo2(arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Yo2[] newArray(int i) {
        return new Yo2[i];
    }
}
