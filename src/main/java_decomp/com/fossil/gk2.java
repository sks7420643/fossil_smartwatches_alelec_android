package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.mapped.W6;
import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gk2 {
    @DexIgnore
    public SharedPreferences a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public /* final */ Map<String, Object> c;

    @DexIgnore
    public Gk2(Context context) {
        this(context, new Ok2());
    }

    @DexIgnore
    public Gk2(Context context, Ok2 ok2) {
        this.c = new Zi0();
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        File file = new File(W6.i(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !a()) {
                    Log.i("InstanceID/Store", "App restored, clearing state");
                    Zj2.d(this.b, this);
                }
            } catch (IOException e) {
                if (Log.isLoggable("InstanceID/Store", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("InstanceID/Store", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        return this.a.getAll().isEmpty();
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this) {
            SharedPreferences.Editor edit = this.a.edit();
            for (String str2 : this.a.getAll().keySet()) {
                if (str2.startsWith(str)) {
                    edit.remove(str2);
                }
            }
            edit.commit();
        }
    }

    @DexIgnore
    public final void c(String str) {
        synchronized (this) {
            this.c.remove(str);
        }
        Ok2.a(this.b, str);
        b(String.valueOf(str).concat("|"));
    }

    @DexIgnore
    public final void d() {
        synchronized (this) {
            this.c.clear();
            Ok2.b(this.b);
            this.a.edit().clear().commit();
        }
    }
}
