package com.fossil;

import com.fossil.Kf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Qf4 implements Runnable {
    @DexIgnore
    public /* final */ Kf4.Bi b;
    @DexIgnore
    public /* final */ Kf4.Ei c;

    @DexIgnore
    public Qf4(Kf4.Bi bi, Kf4.Ei ei) {
        this.b = bi;
        this.c = ei;
    }

    @DexIgnore
    public final void run() {
        this.b.f(this.c);
    }
}
