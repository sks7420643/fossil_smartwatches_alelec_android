package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K03 extends J03 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public K03(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.e = Integer.MAX_VALUE;
        this.a = i2 + i;
        this.c = i;
        this.d = i;
    }

    @DexIgnore
    public final int d(int i) throws L13 {
        if (i >= 0) {
            int e2 = e() + i;
            int i2 = this.e;
            if (e2 <= i2) {
                this.e = e2;
                f();
                return i2;
            }
            throw L13.zza();
        }
        throw L13.zzb();
    }

    @DexIgnore
    public final int e() {
        return this.c - this.d;
    }

    @DexIgnore
    public final void f() {
        int i = this.a + this.b;
        this.a = i;
        int i2 = i - this.d;
        int i3 = this.e;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.b = i4;
            this.a = i - i4;
            return;
        }
        this.b = 0;
    }
}
