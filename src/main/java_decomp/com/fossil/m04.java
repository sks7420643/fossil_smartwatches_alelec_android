package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M04 extends Rp0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<M04> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, Bundle> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.ClassLoaderCreator<M04> {
        @DexIgnore
        public M04 a(Parcel parcel) {
            return new M04(parcel, null, null);
        }

        @DexIgnore
        public M04 b(Parcel parcel, ClassLoader classLoader) {
            return new M04(parcel, classLoader, null);
        }

        @DexIgnore
        public M04[] c(int i) {
            return new M04[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.ClassLoaderCreator
        public /* bridge */ /* synthetic */ M04 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return b(parcel, classLoader);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return c(i);
        }
    }

    @DexIgnore
    public M04(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.d = new SimpleArrayMap<>(readInt);
        for (int i = 0; i < readInt; i++) {
            this.d.put(strArr[i], bundleArr[i]);
        }
    }

    @DexIgnore
    public /* synthetic */ M04(Parcel parcel, ClassLoader classLoader, Ai ai) {
        this(parcel, classLoader);
    }

    @DexIgnore
    public M04(Parcelable parcelable) {
        super(parcelable);
        this.d = new SimpleArrayMap<>();
    }

    @DexIgnore
    public String toString() {
        return "ExtendableSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " states=" + this.d + "}";
    }

    @DexIgnore
    @Override // com.fossil.Rp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        int size = this.d.size();
        parcel.writeInt(size);
        String[] strArr = new String[size];
        Bundle[] bundleArr = new Bundle[size];
        for (int i2 = 0; i2 < size; i2++) {
            strArr[i2] = this.d.j(i2);
            bundleArr[i2] = this.d.n(i2);
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }
}
