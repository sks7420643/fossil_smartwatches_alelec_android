package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Base64;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yh implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Mi b;
    @DexIgnore
    public /* final */ /* synthetic */ File c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList e;

    @DexIgnore
    public Yh(Mi mi, File file, int i, ArrayList arrayList) {
        this.b = mi;
        this.c = file;
        this.d = i;
        this.e = arrayList;
    }

    @DexIgnore
    public final void run() {
        Boolean bool;
        byte[] b2;
        Context a2 = Id0.i.a();
        ActivityManager activityManager = (ActivityManager) (a2 != null ? a2.getSystemService(Constants.ACTIVITY) : null);
        if (activityManager != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            bool = Boolean.valueOf(memoryInfo.lowMemory);
        } else {
            bool = null;
        }
        if (Wg6.a(bool, Boolean.FALSE) && (b2 = Jy1.a.b(this.c)) != null) {
            W80 w80 = W80.i;
            V80 v80 = V80.j;
            Mi mi = this.b;
            w80.d(new A90("gps_log", v80, mi.w.x, Ey1.a(mi.y), this.b.z, true, null, null, null, G80.k(new JSONObject(), Jd0.Y2, Base64.encodeToString(b2, 2)), 448));
        }
        this.c.delete();
        if (this.d == this.e.size() - 1) {
            Z80.c(W80.i, 0, 1, null);
        }
    }
}
