package com.fossil;

import com.fossil.B34;
import com.fossil.C34;
import com.fossil.H34;
import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P44<K, V> extends A34<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] f;
    @DexIgnore
    public /* final */ transient B34<K, V>[] g;
    @DexIgnore
    public /* final */ transient int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<K, V> extends H34.Bi<K> {
        @DexIgnore
        @Weak
        public /* final */ P44<K, V> map;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii<K> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ A34<K, ?> map;

            @DexIgnore
            public Aii(A34<K, ?> a34) {
                this.map = a34;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.keySet();
            }
        }

        @DexIgnore
        public Ai(P44<K, V> p44) {
            this.map = p44;
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean contains(Object obj) {
            return this.map.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.H34.Bi
        public K get(int i) {
            return (K) this.map.f[i].getKey();
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        @Override // com.fossil.U24, com.fossil.H34
        public Object writeReplace() {
            return new Aii(this.map);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<K, V> extends Y24<V> {
        @DexIgnore
        @Weak
        public /* final */ P44<K, V> map;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii<V> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ A34<?, V> map;

            @DexIgnore
            public Aii(A34<?, V> a34) {
                this.map = a34;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.values();
            }
        }

        @DexIgnore
        public Bi(P44<K, V> p44) {
            this.map = p44;
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            return (V) this.map.f[i].getValue();
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        @Override // com.fossil.U24, com.fossil.Y24
        public Object writeReplace() {
            return new Aii(this.map);
        }
    }

    @DexIgnore
    public P44(Map.Entry<K, V>[] entryArr, B34<K, V>[] b34Arr, int i) {
        this.f = entryArr;
        this.g = b34Arr;
        this.h = i;
    }

    @DexIgnore
    public static void checkNoConflictInKeyBucket(Object obj, Map.Entry<?, ?> entry, B34<?, ?> b34) {
        while (b34 != null) {
            A34.checkNoConflict(!obj.equals(b34.getKey()), "key", entry, b34);
            b34 = b34.getNextInKeyBucket();
        }
    }

    @DexIgnore
    public static <K, V> P44<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> P44<K, V> fromEntryArray(int i, Map.Entry<K, V>[] entryArr) {
        I14.p(i, entryArr.length);
        B34[] createEntryArray = i == entryArr.length ? entryArr : B34.createEntryArray(i);
        int a2 = R24.a(i, 1.2d);
        B34[] createEntryArray2 = B34.createEntryArray(a2);
        int i2 = a2 - 1;
        for (int i3 = 0; i3 < i; i3++) {
            Map.Entry<K, V> entry = entryArr[i3];
            K key = entry.getKey();
            V value = entry.getValue();
            A24.a(key, value);
            int b = R24.b(key.hashCode()) & i2;
            B34 b34 = createEntryArray2[b];
            B34 b342 = b34 == null ? (entry instanceof B34) && ((B34) entry).isReusable() ? (B34) entry : new B34(key, value) : new B34.Bi(key, value, b34);
            createEntryArray2[b] = b342;
            createEntryArray[i3] = b342;
            checkNoConflictInKeyBucket(key, b342, b34);
        }
        return new P44<>(createEntryArray, createEntryArray2, i2);
    }

    @DexIgnore
    public static <V> V get(Object obj, B34<?, V>[] b34Arr, int i) {
        if (obj == null) {
            return null;
        }
        for (B34<?, V> b34 = b34Arr[R24.b(obj.hashCode()) & i]; b34 != null; b34 = b34.getNextInKeyBucket()) {
            if (obj.equals(b34.getKey())) {
                return b34.getValue();
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H34<Map.Entry<K, V>> createEntrySet() {
        return new C34.Bi(this, this.f);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H34<K> createKeySet() {
        return new Ai(this);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public U24<V> createValues() {
        return new Bi(this);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34
    public V get(Object obj) {
        return (V) get(obj, this.g, this.h);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.f.length;
    }
}
