package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import androidx.lifecycle.Lifecycle;
import coil.memory.RequestDelegate;
import com.fossil.A18;
import com.fossil.G71;
import com.fossil.S81;
import com.fossil.X71;
import com.fossil.Y41;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Ve6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C51 implements A51, S81 {
    @DexIgnore
    public /* final */ Il6 b; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.c().S()));
    @DexIgnore
    public /* final */ CoroutineExceptionHandler c; // = new Ai(CoroutineExceptionHandler.q);
    @DexIgnore
    public /* final */ T61 d; // = new T61(this, this.m);
    @DexIgnore
    public /* final */ G71 e; // = new G71();
    @DexIgnore
    public /* final */ V51 f; // = new V51(this.l);
    @DexIgnore
    public /* final */ N71 g; // = new N71(this.j);
    @DexIgnore
    public /* final */ Y41 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Context j;
    @DexIgnore
    public /* final */ Z41 k;
    @DexIgnore
    public /* final */ G51 l;
    @DexIgnore
    public /* final */ S61 m;
    @DexIgnore
    public /* final */ C71 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ve6 implements CoroutineExceptionHandler {
        @DexIgnore
        public Ai(Af6.Ci ci) {
            super(ci);
        }

        @DexIgnore
        @Override // kotlinx.coroutines.CoroutineExceptionHandler
        public void handleException(Af6 af6, Throwable th) {
            Wg6.c(af6, "context");
            Wg6.c(th, "exception");
            X81.a("RealImageLoader", th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public F81 a;
        @DexIgnore
        public /* final */ Il6 b;
        @DexIgnore
        public /* final */ G81 c;
        @DexIgnore
        public /* final */ I71 d;
        @DexIgnore
        public /* final */ X71 e;

        @DexIgnore
        public Bi(Il6 il6, G81 g81, I71 i71, X71 x71) {
            Wg6.c(il6, "scope");
            Wg6.c(g81, "sizeResolver");
            Wg6.c(i71, "targetDelegate");
            Wg6.c(x71, "request");
            this.b = il6;
            this.c = g81;
            this.d = i71;
            this.e = x71;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "coil.RealImageLoader$execute$2", f = "RealImageLoader.kt", l = {258}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Drawable>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Object $data;
        @DexIgnore
        public /* final */ /* synthetic */ X71 $request;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ C51 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Hg6<Throwable, Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ RequestDelegate $requestDelegate;
            @DexIgnore
            public /* final */ /* synthetic */ I71 $targetDelegate;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "coil.RealImageLoader$execute$2$1$1", f = "RealImageLoader.kt", l = {251}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable $throwable;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Throwable th, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$throwable = th;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$throwable, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        this.this$0.$requestDelegate.a();
                        Throwable th = this.$throwable;
                        if (th == null) {
                            return Cd6.a;
                        }
                        if (th instanceof CancellationException) {
                            if (Q81.c.a() && Q81.c.b() <= 4) {
                                Log.println(4, "RealImageLoader", "\ud83c\udfd7  Cancelled - " + this.this$0.this$0.$data);
                            }
                            X71.Ai m = this.this$0.this$0.$request.m();
                            if (m != null) {
                                m.onCancel(this.this$0.this$0.$data);
                            }
                            return Cd6.a;
                        }
                        if (Q81.c.a() && Q81.c.b() <= 4) {
                            Log.println(4, "RealImageLoader", "\ud83d\udea8 Failed - " + this.this$0.this$0.$data + " - " + this.$throwable);
                        }
                        Drawable j = this.$throwable instanceof V71 ? this.this$0.this$0.$request.j() : this.this$0.this$0.$request.i();
                        Aii aii = this.this$0;
                        I71 i71 = aii.$targetDelegate;
                        N81 w = aii.this$0.$request.w();
                        this.L$0 = il6;
                        this.L$1 = j;
                        this.label = 1;
                        if (i71.f(j, w, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Drawable drawable = (Drawable) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    X71.Ai m2 = this.this$0.this$0.$request.m();
                    if (m2 != null) {
                        m2.b(this.this$0.this$0.$data, this.$throwable);
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, RequestDelegate requestDelegate, I71 i71) {
                super(1);
                this.this$0 = ci;
                this.$requestDelegate = requestDelegate;
                this.$targetDelegate = i71;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
                invoke(th);
                return Cd6.a;
            }

            @DexIgnore
            public final void invoke(Throwable th) {
                Rm6 unused = Gu7.d(this.this$0.this$0.b, Bw7.c().S(), null, new Aiii(this, th, null), 2, null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "coil.RealImageLoader$execute$2$deferred$1", f = "RealImageLoader.kt", l = {503, 548, 568, 215, 578, 230}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Drawable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Lifecycle $lifecycle;
            @DexIgnore
            public /* final */ /* synthetic */ I71 $targetDelegate;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$13;
            @DexIgnore
            public Object L$14;
            @DexIgnore
            public Object L$15;
            @DexIgnore
            public Object L$16;
            @DexIgnore
            public Object L$17;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ci ci, Lifecycle lifecycle, I71 i71, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$lifecycle = lifecycle;
                this.$targetDelegate = i71;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$lifecycle, this.$targetDelegate, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Drawable> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v59, resolved type: java.lang.Object */
            /* JADX DEBUG: Multi-variable search result rejected for r12v7, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:108:0x06d2  */
            /* JADX WARNING: Removed duplicated region for block: B:109:0x06da  */
            /* JADX WARNING: Removed duplicated region for block: B:12:0x00f3  */
            /* JADX WARNING: Removed duplicated region for block: B:138:0x082e  */
            /* JADX WARNING: Removed duplicated region for block: B:139:0x0832  */
            /* JADX WARNING: Removed duplicated region for block: B:148:0x0904  */
            /* JADX WARNING: Removed duplicated region for block: B:170:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:171:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x01ec  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x027f  */
            /* JADX WARNING: Removed duplicated region for block: B:45:0x03ba  */
            /* JADX WARNING: Removed duplicated region for block: B:52:0x03f9  */
            /* JADX WARNING: Removed duplicated region for block: B:54:0x03fc  */
            /* JADX WARNING: Removed duplicated region for block: B:57:0x041e  */
            /* JADX WARNING: Removed duplicated region for block: B:64:0x04de  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x006c  */
            /* JADX WARNING: Removed duplicated region for block: B:95:0x066e  */
            /* JADX WARNING: Removed duplicated region for block: B:97:0x067e  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r31) {
                /*
                // Method dump skipped, instructions count: 2350
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.C51.Ci.Bii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(C51 c51, X71 x71, Object obj, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = c51;
            this.$request = x71;
            this.$data = obj;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$request, this.$data, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Drawable> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.n();
                G71.Ai f = this.this$0.e.f(this.$request);
                Lifecycle b = f.b();
                Dv7 c = f.c();
                I71 b2 = this.this$0.d.b(this.$request);
                Rl6<? extends Drawable> a2 = Eu7.a(il6, c, Lv7.LAZY, new Bii(this, b, b2, null));
                RequestDelegate a3 = this.this$0.d.a(this.$request, b2, b, c, a2);
                a2.A(new Aii(this, a3, b2));
                this.L$0 = il6;
                this.L$1 = b;
                this.L$2 = c;
                this.L$3 = b2;
                this.L$4 = a2;
                this.L$5 = a3;
                this.label = 1;
                Object l = a2.l(this);
                return l == d ? d : l;
            } else if (i == 1) {
                RequestDelegate requestDelegate = (RequestDelegate) this.L$5;
                Rl6 rl6 = (Rl6) this.L$4;
                I71 i71 = (I71) this.L$3;
                Dv7 dv7 = (Dv7) this.L$2;
                Lifecycle lifecycle = (Lifecycle) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "coil.RealImageLoader$load$job$1", f = "RealImageLoader.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ T71 $request;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ C51 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(C51 c51, T71 t71, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = c51;
            this.$request = t71;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$request, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                C51 c51 = this.this$0;
                Object y = this.$request.y();
                T71 t71 = this.$request;
                this.L$0 = il6;
                this.label = 1;
                if (c51.p(y, t71, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public C51(Context context, Z41 z41, G51 g51, S61 s61, C71 c71, A18.Ai ai, Y41 y41) {
        Wg6.c(context, "context");
        Wg6.c(z41, Constants.DEFAULTS);
        Wg6.c(g51, "bitmapPool");
        Wg6.c(s61, "referenceCounter");
        Wg6.c(c71, "memoryCache");
        Wg6.c(ai, "callFactory");
        Wg6.c(y41, "registry");
        this.j = context;
        this.k = z41;
        this.l = g51;
        this.m = s61;
        this.s = c71;
        Y41.Ai e2 = y41.e();
        e2.c(String.class, new R61());
        e2.c(Uri.class, new M61());
        e2.c(Uri.class, new Q61(this.j));
        e2.c(Integer.class, new P61(this.j));
        e2.b(Uri.class, new H61(ai));
        e2.b(Q18.class, new I61(ai));
        e2.b(File.class, new F61());
        e2.b(Uri.class, new Y51(this.j));
        e2.b(Uri.class, new A61(this.j));
        e2.b(Uri.class, new J61(this.j, this.f));
        e2.b(Drawable.class, new B61(this.j, this.f));
        e2.b(Bitmap.class, new Z51(this.j));
        e2.a(new P51(this.j));
        this.h = e2.d();
        this.j.registerComponentCallbacks(this);
    }

    @DexIgnore
    @Override // com.fossil.A51
    public Z41 a() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.A51
    public Z71 b(T71 t71) {
        Wg6.c(t71, "request");
        Rm6 rm6 = Gu7.d(this.b, this.c, null, new Di(this, t71, null), 2, null);
        return t71.u() instanceof K81 ? new A81(W81.i(((K81) t71.u()).getView()).d(rm6), (K81) t71.u()) : new R71(rm6);
    }

    @DexIgnore
    public final void n() {
        if (!(!this.i)) {
            throw new IllegalStateException("The image loader is shutdown!".toString());
        }
    }

    @DexIgnore
    public void o() {
        onTrimMemory(80);
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        S81.Ai.a(this, configuration);
    }

    @DexIgnore
    public void onLowMemory() {
        S81.Ai.b(this);
    }

    @DexIgnore
    @Override // com.fossil.S81
    public void onTrimMemory(int i2) {
        this.s.a(i2);
        this.l.a(i2);
    }

    @DexIgnore
    public final /* synthetic */ Object p(Object obj, X71 x71, Xe6<? super Drawable> xe6) {
        return Eu7.g(Bw7.c().S(), new Ci(this, x71, obj, null), xe6);
    }

    @DexIgnore
    public final boolean q(BitmapDrawable bitmapDrawable, boolean z, F81 f81, E81 e81, X71 x71) {
        boolean z2 = true;
        Wg6.c(bitmapDrawable, "cached");
        Wg6.c(f81, "size");
        Wg6.c(e81, "scale");
        Wg6.c(x71, "request");
        Bitmap bitmap = bitmapDrawable.getBitmap();
        if (f81 instanceof C81) {
            Wg6.b(bitmap, "bitmap");
            C81 c81 = (C81) f81;
            double c2 = T51.c(bitmap.getWidth(), bitmap.getHeight(), c81.d(), c81.c(), e81);
            if (c2 != 1.0d && !this.e.a(x71)) {
                return false;
            }
            if (c2 > 1.0d && z) {
                return false;
            }
        }
        G71 g71 = this.e;
        Wg6.b(bitmap, "bitmap");
        Bitmap.Config config = bitmap.getConfig();
        Wg6.b(config, "bitmap.config");
        if (!g71.c(x71, config)) {
            return false;
        }
        if (x71.c() && bitmap.getConfig() == Bitmap.Config.RGB_565) {
            return true;
        }
        if (W81.q(bitmap.getConfig()) != W81.q(x71.d())) {
            z2 = false;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.A51
    public void shutdown() {
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                Jv7.d(this.b, null, 1, null);
                this.j.unregisterComponentCallbacks(this);
                this.g.d();
                o();
            }
        }
    }
}
