package com.fossil;

import com.mapped.A;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ib7 implements Gb7 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Integer e;
    @DexIgnore
    public Nb7 f;
    @DexIgnore
    public Jb7 g;
    @DexIgnore
    public Lb7 h;
    @DexIgnore
    public int i;

    @DexIgnore
    public Ib7(byte[] bArr, String str, boolean z, String str2, Integer num, Nb7 nb7, Jb7 jb7, Lb7 lb7, int i2) {
        Wg6.c(bArr, "ringByteData");
        Wg6.c(str, "nameOfRing");
        Wg6.c(nb7, "colour");
        Wg6.c(jb7, "metric");
        Wg6.c(lb7, "type");
        this.a = bArr;
        this.b = str;
        this.c = z;
        this.d = str2;
        this.e = num;
        this.f = nb7;
        this.g = jb7;
        this.h = lb7;
        this.i = i2;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public int a() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public Jb7 b() {
        return this.g;
    }

    @DexIgnore
    public final Nb7 c() {
        return this.f;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ib7.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Ib7 ib7 = (Ib7) obj;
            if (!Wg6.a(this.b, ib7.b)) {
                return false;
            }
            if (this.c != ib7.c) {
                return false;
            }
            if (!Wg6.a(this.d, ib7.d)) {
                return false;
            }
            if (!Wg6.a(this.e, ib7.e)) {
                return false;
            }
            if (this.f != ib7.f) {
                return false;
            }
            if (!Wg6.a(b(), ib7.b())) {
                return false;
            }
            if (getType() != ib7.getType()) {
                return false;
            }
            return a() == ib7.a();
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFComplicationData");
    }

    @DexIgnore
    public final String f() {
        return this.b;
    }

    @DexIgnore
    public final Integer g() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public Lb7 getType() {
        return this.h;
    }

    @DexIgnore
    public final byte[] h() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        int hashCode = this.b.hashCode();
        int a2 = A.a(this.c);
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        Integer num = this.e;
        if (num != null) {
            i2 = num.intValue();
        }
        return ((((((((((hashCode2 + (((hashCode * 31) + a2) * 31)) * 31) + i2) * 31) + this.f.hashCode()) * 31) + b().hashCode()) * 31) + getType().hashCode()) * 31) + a();
    }

    @DexIgnore
    public final void i(String str) {
        this.d = str;
    }

    @DexIgnore
    public final void j(Integer num) {
        this.e = num;
    }

    @DexIgnore
    public final void k(byte[] bArr) {
        Wg6.c(bArr, "<set-?>");
        this.a = bArr;
    }

    @DexIgnore
    public String toString() {
        return "WFComplicationData(ringByteData=" + Arrays.toString(this.a) + ", nameOfRing=" + this.b + ", enableRingGoal=" + this.c + ", location=" + this.d + ", offsetMin=" + this.e + ", colour=" + this.f + ", metric=" + b() + ", type=" + getType() + ", index=" + a() + ")";
    }
}
