package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ro0 {
    @DexIgnore
    public WeakReference<View> a;
    @DexIgnore
    public Runnable b; // = null;
    @DexIgnore
    public Runnable c; // = null;
    @DexIgnore
    public int d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ So0 a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public Ai(Ro0 ro0, So0 so0, View view) {
            this.a = so0;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a.a(this.b);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b(this.b);
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.c(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ Uo0 a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public Bi(Ro0 ro0, Uo0 uo0, View view) {
            this.a = uo0;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements So0 {
        @DexIgnore
        public Ro0 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ci(Ro0 ro0) {
            this.a = ro0;
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void a(View view) {
            Object tag = view.getTag(2113929216);
            So0 so0 = tag instanceof So0 ? (So0) tag : null;
            if (so0 != null) {
                so0.a(view);
            }
        }

        @DexIgnore
        @Override // com.fossil.So0
        @SuppressLint({"WrongConstant"})
        public void b(View view) {
            int i = this.a.d;
            if (i > -1) {
                view.setLayerType(i, null);
                this.a.d = -1;
            }
            if (Build.VERSION.SDK_INT >= 16 || !this.b) {
                Ro0 ro0 = this.a;
                Runnable runnable = ro0.c;
                if (runnable != null) {
                    ro0.c = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                So0 so0 = tag instanceof So0 ? (So0) tag : null;
                if (so0 != null) {
                    so0.b(view);
                }
                this.b = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void c(View view) {
            this.b = false;
            if (this.a.d > -1) {
                view.setLayerType(2, null);
            }
            Ro0 ro0 = this.a;
            Runnable runnable = ro0.b;
            if (runnable != null) {
                ro0.b = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            So0 so0 = tag instanceof So0 ? (So0) tag : null;
            if (so0 != null) {
                so0.c(view);
            }
        }
    }

    @DexIgnore
    public Ro0(View view) {
        this.a = new WeakReference<>(view);
    }

    @DexIgnore
    public Ro0 a(float f) {
        View view = this.a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    @DexIgnore
    public void b() {
        View view = this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    @DexIgnore
    public long c() {
        View view = this.a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    @DexIgnore
    public Ro0 d(long j) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    @DexIgnore
    public Ro0 e(Interpolator interpolator) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    @DexIgnore
    public Ro0 f(So0 so0) {
        View view = this.a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                g(view, so0);
            } else {
                view.setTag(2113929216, so0);
                g(view, new Ci(this));
            }
        }
        return this;
    }

    @DexIgnore
    public final void g(View view, So0 so0) {
        if (so0 != null) {
            view.animate().setListener(new Ai(this, so0, view));
        } else {
            view.animate().setListener(null);
        }
    }

    @DexIgnore
    public Ro0 h(long j) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    @DexIgnore
    public Ro0 i(Uo0 uo0) {
        View view = this.a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            Bi bi = null;
            if (uo0 != null) {
                bi = new Bi(this, uo0, view);
            }
            view.animate().setUpdateListener(bi);
        }
        return this;
    }

    @DexIgnore
    public void j() {
        View view = this.a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    @DexIgnore
    public Ro0 k(float f) {
        View view = this.a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }
}
