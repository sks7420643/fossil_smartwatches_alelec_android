package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ej2 implements Parcelable.Creator<Dj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Dj2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Status status = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                arrayList = Ad2.j(parcel, t, Uh2.CREATOR);
            } else if (l != 2) {
                Ad2.B(parcel, t);
            } else {
                status = (Status) Ad2.e(parcel, t, Status.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Dj2(arrayList, status);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Dj2[] newArray(int i) {
        return new Dj2[i];
    }
}
