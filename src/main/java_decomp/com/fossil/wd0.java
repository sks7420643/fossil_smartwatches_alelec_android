package com.fossil;

import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import com.fossil.Vd0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wd0 {

    @DexIgnore
    public interface Ai extends Vd0.Di {
        @DexIgnore
        void a(String str, Bundle bundle);

        @DexIgnore
        void b(String str, List<?> list, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<T extends Ai> extends Vd0.Ei<T> {
        @DexIgnore
        public Bi(T t) {
            super(t);
        }

        @DexIgnore
        @Override // android.media.browse.MediaBrowser.SubscriptionCallback
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((Ai) this.a).b(str, list, bundle);
        }

        @DexIgnore
        public void onError(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((Ai) this.a).a(str, bundle);
        }
    }

    @DexIgnore
    public static Object a(Ai ai) {
        return new Bi(ai);
    }
}
