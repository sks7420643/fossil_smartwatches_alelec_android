package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L14<T> extends G14<T> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ T reference;

    @DexIgnore
    public L14(T t) {
        this.reference = t;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public Set<T> asSet() {
        return Collections.singleton(this.reference);
    }

    @DexIgnore
    @Override // com.fossil.G14
    public boolean equals(Object obj) {
        if (obj instanceof L14) {
            return this.reference.equals(((L14) obj).reference);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public T get() {
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public boolean isPresent() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public G14<T> or(G14<? extends T> g14) {
        I14.l(g14);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public T or(M14<? extends T> m14) {
        I14.l(m14);
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public T or(T t) {
        I14.m(t, "use Optional.orNull() instead of Optional.or(null)");
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public T orNull() {
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.G14
    public String toString() {
        return "Optional.of(" + ((Object) this.reference) + ")";
    }

    @DexIgnore
    @Override // com.fossil.G14
    public <V> G14<V> transform(B14<? super T, V> b14) {
        V apply = b14.apply(this.reference);
        I14.m(apply, "the Function passed to Optional.transform() must not return null.");
        return new L14(apply);
    }
}
