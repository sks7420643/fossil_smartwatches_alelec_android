package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gb5 extends Fb5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d T; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131362117, 1);
        U.put(2131362666, 2);
        U.put(2131363410, 3);
        U.put(2131362089, 4);
        U.put(2131362667, 5);
        U.put(2131362402, 6);
        U.put(2131362401, 7);
        U.put(2131362783, 8);
        U.put(2131362399, 9);
        U.put(2131362397, 10);
        U.put(2131362400, 11);
        U.put(2131362398, 12);
        U.put(2131362735, 13);
        U.put(2131362109, 14);
        U.put(2131362091, 15);
        U.put(2131362925, 16);
        U.put(2131362505, 17);
        U.put(2131362445, 18);
        U.put(2131362064, 19);
        U.put(2131363056, 20);
        U.put(2131362162, 21);
        U.put(2131362031, 22);
        U.put(2131363455, 23);
        U.put(2131362724, 24);
        U.put(2131362453, 25);
        U.put(2131363040, 26);
        U.put(2131362452, 27);
    }
    */

    @DexIgnore
    public Gb5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 28, T, U));
    }

    @DexIgnore
    public Gb5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[22], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[15], (LinearLayout) objArr[14], (ConstraintLayout) objArr[1], (TabLayout) objArr[21], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[17], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (ImageView) objArr[24], (RTLImageView) objArr[13], (View) objArr[8], (FlexibleProgressBar) objArr[16], (ConstraintLayout) objArr[0], (ViewPager2) objArr[26], (ViewPager2) objArr[20], (FlexibleTextView) objArr[3], (View) objArr[23]);
        this.S = -1;
        this.N.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.S = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.S != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.S = 1;
        }
        w();
    }
}
