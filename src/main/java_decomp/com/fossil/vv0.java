package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vv0 extends Uv0 {
    @DexIgnore
    public int d;
    @DexIgnore
    public Ci e;
    @DexIgnore
    public Bi f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[Bi.values().length];
            b = iArr;
            try {
                iArr[Bi.PIXEL_LA.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[Bi.PIXEL_RGB.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[Bi.PIXEL_RGBA.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            int[] iArr2 = new int[Ci.values().length];
            a = iArr2;
            try {
                iArr2[Ci.FLOAT_32.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Ci.FLOAT_64.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[Ci.SIGNED_8.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[Ci.SIGNED_16.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[Ci.SIGNED_32.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[Ci.SIGNED_64.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[Ci.UNSIGNED_8.ordinal()] = 7;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[Ci.UNSIGNED_16.ordinal()] = 8;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[Ci.UNSIGNED_32.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[Ci.UNSIGNED_64.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                a[Ci.BOOLEAN.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
        }
        */
    }

    @DexIgnore
    public enum Bi {
        USER(0),
        PIXEL_L(7),
        PIXEL_A(8),
        PIXEL_LA(9),
        PIXEL_RGB(10),
        PIXEL_RGBA(11),
        PIXEL_DEPTH(12),
        PIXEL_YUV(13);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public Bi(int i) {
            this.mID = i;
        }
    }

    @DexIgnore
    public enum Ci {
        NONE(0, 0),
        FLOAT_32(2, 4),
        FLOAT_64(3, 8),
        SIGNED_8(4, 1),
        SIGNED_16(5, 2),
        SIGNED_32(6, 4),
        SIGNED_64(7, 8),
        UNSIGNED_8(8, 1),
        UNSIGNED_16(9, 2),
        UNSIGNED_32(10, 4),
        UNSIGNED_64(11, 8),
        BOOLEAN(12, 1),
        UNSIGNED_5_6_5(13, 2),
        UNSIGNED_5_5_5_1(14, 2),
        UNSIGNED_4_4_4_4(15, 2),
        MATRIX_4X4(16, 64),
        MATRIX_3X3(17, 36),
        MATRIX_2X2(18, 16),
        RS_ELEMENT(1000),
        RS_TYPE(1001),
        RS_ALLOCATION(1002),
        RS_SAMPLER(1003),
        RS_SCRIPT(1004);
        
        @DexIgnore
        public int mID;
        @DexIgnore
        public int mSize;

        @DexIgnore
        public Ci(int i) {
            this.mID = i;
            this.mSize = 4;
            if (RenderScript.G == 8) {
                this.mSize = 32;
            }
        }

        @DexIgnore
        public Ci(int i, int i2) {
            this.mID = i;
            this.mSize = i2;
        }
    }

    @DexIgnore
    public Vv0(long j, RenderScript renderScript, Ci ci, Bi bi, boolean z, int i) {
        super(j, renderScript);
        if (ci == Ci.UNSIGNED_5_6_5 || ci == Ci.UNSIGNED_4_4_4_4 || ci == Ci.UNSIGNED_5_5_5_1) {
            this.d = ci.mSize;
        } else if (i == 3) {
            this.d = ci.mSize * 4;
        } else {
            this.d = ci.mSize * i;
        }
        this.e = ci;
        this.f = bi;
        this.g = z;
        this.h = i;
    }

    @DexIgnore
    public static Vv0 f(RenderScript renderScript) {
        if (renderScript.n == null) {
            renderScript.n = l(renderScript, Ci.UNSIGNED_8, Bi.PIXEL_A);
        }
        return renderScript.n;
    }

    @DexIgnore
    public static Vv0 g(RenderScript renderScript) {
        if (renderScript.p == null) {
            renderScript.p = l(renderScript, Ci.UNSIGNED_4_4_4_4, Bi.PIXEL_RGBA);
        }
        return renderScript.p;
    }

    @DexIgnore
    public static Vv0 h(RenderScript renderScript) {
        if (renderScript.q == null) {
            renderScript.q = l(renderScript, Ci.UNSIGNED_8, Bi.PIXEL_RGBA);
        }
        return renderScript.q;
    }

    @DexIgnore
    public static Vv0 i(RenderScript renderScript) {
        if (renderScript.o == null) {
            renderScript.o = l(renderScript, Ci.UNSIGNED_5_6_5, Bi.PIXEL_RGB);
        }
        return renderScript.o;
    }

    @DexIgnore
    public static Vv0 j(RenderScript renderScript) {
        if (renderScript.m == null) {
            renderScript.m = m(renderScript, Ci.UNSIGNED_8);
        }
        return renderScript.m;
    }

    @DexIgnore
    public static Vv0 k(RenderScript renderScript) {
        if (renderScript.r == null) {
            renderScript.r = n(renderScript, Ci.UNSIGNED_8, 4);
        }
        return renderScript.r;
    }

    @DexIgnore
    public static Vv0 l(RenderScript renderScript, Ci ci, Bi bi) {
        if (bi != Bi.PIXEL_L && bi != Bi.PIXEL_A && bi != Bi.PIXEL_LA && bi != Bi.PIXEL_RGB && bi != Bi.PIXEL_RGBA && bi != Bi.PIXEL_DEPTH && bi != Bi.PIXEL_YUV) {
            throw new Yv0("Unsupported DataKind");
        } else if (ci != Ci.UNSIGNED_8 && ci != Ci.UNSIGNED_16 && ci != Ci.UNSIGNED_5_6_5 && ci != Ci.UNSIGNED_4_4_4_4 && ci != Ci.UNSIGNED_5_5_5_1) {
            throw new Yv0("Unsupported DataType");
        } else if (ci == Ci.UNSIGNED_5_6_5 && bi != Bi.PIXEL_RGB) {
            throw new Yv0("Bad kind and type combo");
        } else if (ci == Ci.UNSIGNED_5_5_5_1 && bi != Bi.PIXEL_RGBA) {
            throw new Yv0("Bad kind and type combo");
        } else if (ci == Ci.UNSIGNED_4_4_4_4 && bi != Bi.PIXEL_RGBA) {
            throw new Yv0("Bad kind and type combo");
        } else if (ci != Ci.UNSIGNED_16 || bi == Bi.PIXEL_DEPTH) {
            int i = Ai.b[bi.ordinal()];
            int i2 = i != 1 ? i != 2 ? i != 3 ? 1 : 4 : 3 : 2;
            return new Vv0(renderScript.s((long) ci.mID, bi.mID, true, i2), renderScript, ci, bi, true, i2);
        } else {
            throw new Yv0("Bad kind and type combo");
        }
    }

    @DexIgnore
    public static Vv0 m(RenderScript renderScript, Ci ci) {
        Bi bi = Bi.USER;
        return new Vv0(renderScript.s((long) ci.mID, bi.mID, false, 1), renderScript, ci, bi, false, 1);
    }

    @DexIgnore
    public static Vv0 n(RenderScript renderScript, Ci ci, int i) {
        if (i < 2 || i > 4) {
            throw new Yv0("Vector size out of range 2-4.");
        }
        switch (Ai.a[ci.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                Bi bi = Bi.USER;
                return new Vv0(renderScript.s((long) ci.mID, bi.mID, false, i), renderScript, ci, bi, false, i);
            default:
                throw new Yv0("Cannot create vector of non-primitive type.");
        }
    }

    @DexIgnore
    public int o() {
        return this.d;
    }

    @DexIgnore
    public long p(RenderScript renderScript) {
        return renderScript.x((long) this.e.mID, this.f.mID, this.g, this.h);
    }

    @DexIgnore
    public boolean q(Vv0 vv0) {
        Ci ci;
        if (equals(vv0)) {
            return true;
        }
        return this.d == vv0.d && (ci = this.e) != Ci.NONE && ci == vv0.e && this.h == vv0.h;
    }
}
