package com.fossil;

import com.fossil.Bu0;
import com.fossil.Eu0;
import com.mapped.Cf;
import com.mapped.Xe;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Iu0<T> extends Cf<T> implements Eu0.Ai {
    @DexIgnore
    public /* final */ Gu0<T> u;
    @DexIgnore
    public Bu0.Ai<T> v; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Bu0.Ai<T> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Bu0.Ai
        public void a(int i, Bu0<T> bu0) {
            boolean z = true;
            if (bu0.c()) {
                Iu0.this.m();
            } else if (Iu0.this.t()) {
            } else {
                if (i == 0 || i == 3) {
                    List<T> list = bu0.a;
                    if (Iu0.this.f.l() == 0) {
                        Iu0 iu0 = Iu0.this;
                        iu0.f.s(bu0.b, list, bu0.c, bu0.d, iu0.e.a, iu0);
                    } else {
                        Iu0 iu02 = Iu0.this;
                        iu02.f.F(bu0.d, list, iu02.g, iu02.e.d, iu02.i, iu02);
                    }
                    Iu0 iu03 = Iu0.this;
                    if (iu03.d != null) {
                        boolean z2 = iu03.f.size() == 0;
                        boolean z3 = !z2 && bu0.b == 0 && bu0.d == 0;
                        int size = Iu0.this.size();
                        if (z2 || (!(i == 0 && bu0.c == 0) && (i != 3 || bu0.d + Iu0.this.e.a < size))) {
                            z = false;
                        }
                        Iu0.this.l(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("unexpected resultType" + i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public Bi(int i) {
            this.b = i;
        }

        @DexIgnore
        public void run() {
            if (!Iu0.this.t()) {
                Iu0 iu0 = Iu0.this;
                int i = iu0.e.a;
                if (iu0.u.isInvalid()) {
                    Iu0.this.m();
                    return;
                }
                int i2 = this.b * i;
                int min = Math.min(i, Iu0.this.f.size() - i2);
                Iu0 iu02 = Iu0.this;
                iu02.u.dispatchLoadRange(3, i2, min, iu02.b, iu02.v);
            }
        }
    }

    @DexIgnore
    public Iu0(Gu0<T> gu0, Executor executor, Executor executor2, Cf.Ci<T> ci, Cf.Fi fi, int i) {
        super(new Eu0(), executor, executor2, ci, fi);
        this.u = gu0;
        int i2 = this.e.a;
        this.g = i;
        if (gu0.isInvalid()) {
            m();
            return;
        }
        int max = Math.max(this.e.e / i2, 2) * i2;
        this.u.dispatchLoadInitial(true, Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, this.b, this.v);
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void a() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void b(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void c(int i) {
        y(0, i);
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void d(int i) {
        this.c.execute(new Bi(i));
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void e(int i, int i2) {
        x(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void f(int i, int i2) {
        z(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void g() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void h(int i, int i2) {
        x(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Eu0.Ai
    public void i(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public void o(Cf<T> cf, Cf.Ei ei) {
        int i;
        Eu0<T> eu0 = cf.f;
        if (eu0.isEmpty() || this.f.size() != eu0.size()) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i2 = this.e.a;
        int h = this.f.h() / i2;
        int l = this.f.l();
        int i3 = 0;
        while (i3 < l) {
            int i4 = i3 + h;
            int i5 = 0;
            while (i5 < this.f.l()) {
                int i6 = i4 + i5;
                if (!this.f.p(i2, i6) || eu0.p(i2, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                ei.a(i4 * i2, i2 * i5);
                i = (i5 - 1) + i3;
            } else {
                i = i3;
            }
            i3 = i + 1;
        }
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public Xe<?, T> p() {
        return this.u;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public Object q() {
        return Integer.valueOf(this.g);
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public boolean s() {
        return false;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public void w(int i) {
        Eu0<T> eu0 = this.f;
        Cf.Fi fi = this.e;
        eu0.b(i, fi.b, fi.a, this);
    }
}
