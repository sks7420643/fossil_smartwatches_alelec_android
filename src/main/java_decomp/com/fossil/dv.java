package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Dv extends Ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ N6 I;
    @DexIgnore
    public /* final */ N6 J;
    @DexIgnore
    public /* final */ Iu K;
    @DexIgnore
    public /* final */ short L;

    @DexIgnore
    public Dv(Iu iu, short s, Hs hs, K5 k5, int i) {
        super(hs, k5, i);
        this.K = iu;
        this.L = (short) s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.b).putShort(this.L).array();
        Wg6.b(array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.a()).putShort(this.L).array();
        Wg6.b(array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        N6 n6 = N6.j;
        this.I = n6;
        this.J = n6;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final Mt E(byte b) {
        return Ku.i.a(b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final byte[] P() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final long a(O7 o7) {
        if (o7.a == N6.j) {
            byte[] bArr = o7.b;
            if (bArr.length >= 9) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                short s = order.getShort(1);
                byte b2 = order.get(3);
                byte b3 = order.get(4);
                long o = Hy1.o(order.getInt(5));
                if (b == Iu.k.a() && this.L == s && b2 == Ku.d.c && b3 == this.K.b && o > 0) {
                    return o;
                }
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.A0, Hy1.l(this.L, null, 1, null));
    }
}
