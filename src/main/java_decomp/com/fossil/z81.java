package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z81 extends T91 {
    @DexIgnore
    public Intent mResolutionIntent;

    @DexIgnore
    public Z81() {
    }

    @DexIgnore
    public Z81(Intent intent) {
        this.mResolutionIntent = intent;
    }

    @DexIgnore
    public Z81(J91 j91) {
        super(j91);
    }

    @DexIgnore
    public Z81(String str) {
        super(str);
    }

    @DexIgnore
    public Z81(String str, Exception exc) {
        super(str, exc);
    }

    @DexIgnore
    public String getMessage() {
        return this.mResolutionIntent != null ? "User needs to (re)enter credentials." : super.getMessage();
    }

    @DexIgnore
    public Intent getResolutionIntent() {
        return this.mResolutionIntent;
    }
}
