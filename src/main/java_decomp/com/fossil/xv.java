package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xv extends Uv {
    @DexIgnore
    public short Q;
    @DexIgnore
    public long R;
    @DexIgnore
    public /* final */ byte[] S;
    @DexIgnore
    public byte[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Xv(short s, K5 k5, int i, int i2) {
        super(Sv.j, s, Hs.V, k5, (i2 & 4) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(Sv.j.b).array();
        Wg6.b(array, "ByteBuffer.allocate(1)\n \u2026ode)\n            .array()");
        this.S = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(Sv.j.a()).array();
        Wg6.b(array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.T = array2;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(G80.k(super.A(), Jd0.h3, Short.valueOf(this.Q)), Jd0.i3, Long.valueOf(this.R));
    }

    @DexIgnore
    @Override // com.fossil.Ps, com.fossil.Tv
    public byte[] M() {
        return this.S;
    }

    @DexIgnore
    @Override // com.fossil.Ps, com.fossil.Tv
    public byte[] P() {
        return this.T;
    }

    @DexIgnore
    @Override // com.fossil.Tv
    public void Q(byte[] bArr) {
        this.T = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Uv
    public void S(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.wrap(received\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.Q = Hy1.p(order.get(0));
        this.R = Hy1.o(order.getInt(1));
    }

    @DexIgnore
    @Override // com.fossil.Tv, com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.A0, Hy1.l(this.K, null, 1, null));
    }
}
