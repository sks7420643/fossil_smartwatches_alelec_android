package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dl4 extends Hl4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public Dl4() {
        c();
    }

    @DexIgnore
    @Override // com.fossil.Hl4
    public /* bridge */ /* synthetic */ Hl4 b(El4 el4) throws IOException {
        d(el4);
        return this;
    }

    @DexIgnore
    public Dl4 c() {
        this.a = "";
        this.b = "";
        return this;
    }

    @DexIgnore
    public Dl4 d(El4 el4) throws IOException {
        while (true) {
            int q = el4.q();
            if (q == 0) {
                break;
            } else if (q == 18) {
                this.a = el4.p();
            } else if (q == 26) {
                this.b = el4.p();
            } else if (q == 50) {
                el4.p();
            } else if (!Jl4.e(el4, q)) {
                break;
            }
        }
        return this;
    }
}
