package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y46 implements Factory<NotificationDialLandingPresenter> {
    @DexIgnore
    public static NotificationDialLandingPresenter a(S46 s46, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        return new NotificationDialLandingPresenter(s46, notificationsLoader, loaderManager);
    }
}
