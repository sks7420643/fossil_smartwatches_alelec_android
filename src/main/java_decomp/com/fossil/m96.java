package com.fossil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.o96;
import com.fossil.q96;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m96 extends qv5 implements q96.c, CropImageView.i, CropImageView.g, FragmentManager.g, t47.g {
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public o96 h;
    @DexIgnore
    public g37<b65> i;
    @DexIgnore
    public Uri j;
    @DexIgnore
    public q96 k;
    @DexIgnore
    public /* final */ ArrayList<FilterType> l;
    @DexIgnore
    public FilterType m;
    @DexIgnore
    public po4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final m96 a(Uri uri) {
            pq7.c(uri, "uri");
            m96 m96 = new m96();
            Bundle bundle = new Bundle();
            bundle.putParcelable("MAGE_URI_ARG", uri);
            m96.setArguments(bundle);
            return m96;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<o96.d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m96 f2330a;

        @DexIgnore
        public b(m96 m96) {
            this.f2330a = m96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(o96.d dVar) {
            FragmentActivity activity;
            FlexibleTextView flexibleTextView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EditPhotoFragment", "Crop image: result = " + dVar);
            this.f2330a.a();
            b65 S6 = this.f2330a.S6();
            if (!(S6 == null || (flexibleTextView = S6.w) == null)) {
                flexibleTextView.setClickable(true);
            }
            if (dVar.b() && (activity = this.f2330a.getActivity()) != null) {
                this.f2330a.a();
                if (dVar.b()) {
                    FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Receive saveThemeResult Success");
                    Intent intent = new Intent();
                    intent.putExtra("WATCH_FACE_ID", dVar.a());
                    activity.setResult(-1, intent);
                    activity.finish();
                    return;
                }
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Receive saveThemeResult Fail");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<o96.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m96 f2331a;

        @DexIgnore
        public c(m96 m96) {
            this.f2331a = m96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(o96.b bVar) {
            List<FilterResult> a2 = bVar.a();
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(a2));
            this.f2331a.a();
            if (!a2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (FilterResult filterResult : bVar.a()) {
                    Bitmap preview = filterResult.getPreview();
                    pq7.b(preview, "filter.preview");
                    Bitmap s = ry5.s(preview, false);
                    pq7.b(s, "circleBitmap");
                    FilterType type = filterResult.getType();
                    pq7.b(type, "filter.type");
                    arrayList.add(new q96.a(s, type));
                }
                this.f2331a.m = ((q96.a) arrayList.get(0)).b();
                q96 q96 = this.f2331a.k;
                if (q96 != null) {
                    q96.l(arrayList);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<o96.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m96 f2332a;

        @DexIgnore
        public d(m96 m96) {
            this.f2332a = m96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(o96.a aVar) {
            CropImageView cropImageView;
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(aVar.a()));
            FilterResult a2 = aVar.a();
            b65 S6 = this.f2332a.S6();
            if (!(S6 == null || (cropImageView = S6.q) == null)) {
                cropImageView.q(a2.getPreview());
            }
            m96 m96 = this.f2332a;
            FilterType type = a2.getType();
            pq7.b(type, "it.type");
            m96.m = type;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m96 b;

        @DexIgnore
        public e(m96 m96, b65 b65) {
            this.b = m96;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m96 b;

        @DexIgnore
        public f(m96 m96, b65 b65) {
            this.b = m96;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.j != null) {
                this.b.Q6();
            } else {
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Can not crop image cause of null imageURI");
            }
        }
    }

    @DexIgnore
    public m96() {
        ArrayList<FilterType> arrayList = new ArrayList<>();
        arrayList.add(FilterType.ATKINSON_DITHERING);
        arrayList.add(FilterType.BURKES_DITHERING);
        arrayList.add(FilterType.DIRECT_MAPPING);
        arrayList.add(FilterType.JAJUNI_DITHERING);
        arrayList.add(FilterType.ORDERED_DITHERING);
        arrayList.add(FilterType.SIERRA_DITHERING);
        this.l = arrayList;
        FilterType filterType = arrayList.get(0);
        pq7.b(filterType, "mFilterList[0]");
        this.m = filterType;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.g
    public void E3(Rect rect) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onCropOverlayReleased()");
        U6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.i
    public void K3(CropImageView cropImageView, Uri uri, Exception exc) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onSetImageUriComplete, uri = " + uri + ", error = " + exc);
        if (isActive()) {
            if (exc == null) {
                U6();
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("EditPhotoFragment", "error = " + exc);
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.X(childFragmentManager);
        }
    }

    @DexIgnore
    public final void P6() {
        o96 o96 = this.h;
        if (o96 != null) {
            o96.h().h(getViewLifecycleOwner(), new b(this));
            o96 o962 = this.h;
            if (o962 != null) {
                o962.f().h(getViewLifecycleOwner(), new c(this));
                o96 o963 = this.h;
                if (o963 != null) {
                    o963.e().h(getViewLifecycleOwner(), new d(this));
                } else {
                    pq7.n("mViewModel");
                    throw null;
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "cropImage()");
        o96.c R6 = R6();
        if (R6 != null) {
            b();
            o96 o96 = this.h;
            if (o96 != null) {
                o96.d(R6, this.m);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onDialogFragmentResult tag = " + str);
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() == 407101428 && str.equals("PROCESS_IMAGE_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        super.R5(str, i2, intent);
    }

    @DexIgnore
    public final o96.c R6() {
        CropImageView cropImageView;
        ConstraintLayout constraintLayout;
        b65 S6 = S6();
        if (!(S6 == null || (constraintLayout = S6.r) == null)) {
            constraintLayout.invalidate();
        }
        b65 S62 = S6();
        if (!(S62 == null || (cropImageView = S62.q) == null)) {
            pq7.b(cropImageView, "it");
            if (cropImageView.getInitializeBitmap() != null) {
                cropImageView.clearAnimation();
                float[] fArr = new float[8];
                float[] cropPoints = cropImageView.getCropPoints();
                pq7.b(cropPoints, "it.cropPoints");
                int i2 = 0;
                for (float f2 : cropPoints) {
                    fArr[i2] = f2 / ((float) cropImageView.getLoadedSampleSize());
                    i2++;
                }
                Bitmap initializeBitmap = cropImageView.getInitializeBitmap();
                pq7.b(initializeBitmap, "it.initializeBitmap");
                int rotatedDegrees = cropImageView.getRotatedDegrees();
                boolean k2 = cropImageView.k();
                Object obj = cropImageView.getAspectRatio().first;
                pq7.b(obj, "it.aspectRatio.first");
                int intValue = ((Number) obj).intValue();
                Object obj2 = cropImageView.getAspectRatio().second;
                pq7.b(obj2, "it.aspectRatio.second");
                return new o96.c(initializeBitmap, fArr, rotatedDegrees, k2, intValue, ((Number) obj2).intValue(), cropImageView.l(), cropImageView.m());
            }
        }
        return null;
    }

    @DexIgnore
    public final b65 S6() {
        g37<b65> g37 = this.i;
        if (g37 != null) {
            return g37.a();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void T6(Bundle bundle) {
        ArrayList<n66> arrayList = null;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "initialize()");
        if (bundle.containsKey("MAGE_URI_ARG")) {
            Bundle arguments = getArguments();
            this.j = arguments != null ? (Uri) arguments.getParcelable("MAGE_URI_ARG") : null;
        }
        if (bundle.containsKey("COMPLICATIONS_ARG")) {
            o96 o96 = this.h;
            if (o96 != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getParcelableArrayList("COMPLICATIONS_ARG");
                }
                o96.i(arrayList);
                return;
            }
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void U6() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "updateFilterList()");
        o96.c R6 = R6();
        if (R6 != null) {
            o96 o96 = this.h;
            if (o96 != null) {
                o96.c(R6, this.l);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.q96.c
    public void k4(q96.a aVar) {
        b65 S6;
        CropImageView cropImageView;
        Bitmap initializeBitmap;
        pq7.c(aVar, "imageFilter");
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(aVar.b()));
        if (this.m != aVar.b() && (S6 = S6()) != null && (cropImageView = S6.q) != null && (initializeBitmap = cropImageView.getInitializeBitmap()) != null) {
            o96 o96 = this.h;
            if (o96 != null) {
                o96.b(initializeBitmap, aVar.b());
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null) {
            T6(bundle);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentManager.g
    public void onBackStackChanged() {
        CropImageView cropImageView;
        CropImageView cropImageView2;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onBackStackChanged()");
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        if (childFragmentManager.d0() == 0) {
            g37<b65> g37 = this.i;
            if (g37 != null) {
                b65 a2 = g37.a();
                if (a2 != null && (cropImageView2 = a2.q) != null) {
                    cropImageView2.setOnSetImageUriCompleteListener(this);
                    cropImageView2.setOnSetCropOverlayReleasedListener(this);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<b65> g372 = this.i;
        if (g372 != null) {
            b65 a3 = g372.a();
            if (a3 != null && (cropImageView = a3.q) != null) {
                cropImageView.e();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().v0().a(this);
        FragmentActivity requireActivity = requireActivity();
        po4 po4 = this.s;
        if (po4 != null) {
            ts0 a2 = vs0.f(requireActivity, po4).a(o96.class);
            pq7.b(a2, "ViewModelProviders.of(re\u2026otoViewModel::class.java)");
            this.h = (o96) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                pq7.b(arguments, "it");
                T6(arguments);
            }
            getChildFragmentManager().e(this);
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        b65 b65 = (b65) aq0.f(LayoutInflater.from(getContext()), 2131558551, null, true, A6());
        this.i = new g37<>(this, b65);
        this.k = new q96(null, this, 1, null);
        g37<b65> g37 = this.i;
        if (g37 != null) {
            b65 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "fragmentBinding.tvCancel");
                nl5.a(flexibleTextView, new e(this, b65));
                FlexibleTextView flexibleTextView2 = a2.w;
                pq7.b(flexibleTextView2, "fragmentBinding.tvPreview");
                nl5.a(flexibleTextView2, new f(this, b65));
                if (this.j != null) {
                    a2.q.setOnSetImageUriCompleteListener(this);
                    CropImageView cropImageView = a2.q;
                    if (cropImageView != null) {
                        cropImageView.setOnSetCropOverlayReleasedListener(this);
                    }
                    b();
                    a2.q.w(this.j, this.m);
                } else {
                    a();
                }
                RecyclerView recyclerView = b65.u;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator(null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.k);
            }
            P6();
            pq7.b(b65, "binding");
            return b65.n();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        getChildFragmentManager().R0(this);
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        CropImageView cropImageView;
        g37<b65> g37 = this.i;
        if (g37 != null) {
            b65 a2 = g37.a();
            if (!(a2 == null || (cropImageView = a2.q) == null)) {
                cropImageView.e();
            }
            q96 q96 = this.k;
            if (q96 != null) {
                q96.m(null);
            }
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("MAGE_URI_ARG", this.j);
        o96 o96 = this.h;
        if (o96 != null) {
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", o96.g());
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
