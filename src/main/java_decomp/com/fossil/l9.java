package com.fossil;

import com.fossil.blesdk.device.data.notification.NotificationFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L9 extends Tx1<NotificationFilter[]> {
    @DexIgnore
    public L9() {
        super(null, 1, null);
    }

    @DexIgnore
    @Override // com.fossil.Qx1
    public byte[] b(Object obj) {
        return R9.d.h((NotificationFilter[]) obj);
    }
}
