package com.fossil;

import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Ku extends Enum<Ku> implements Mt {
    @DexIgnore
    public static /* final */ Ku d;
    @DexIgnore
    public static /* final */ Ku e;
    @DexIgnore
    public static /* final */ Ku f;
    @DexIgnore
    public static /* final */ Ku g;
    @DexIgnore
    public static /* final */ /* synthetic */ Ku[] h;
    @DexIgnore
    public static /* final */ Ju i; // = new Ju(null);
    @DexIgnore
    public /* final */ String b; // = Ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        Ku ku = new Ku("SUCCESS", 0, (byte) 0);
        d = ku;
        Ku ku2 = new Ku("INVALID_OPERATION_DATA", 1, (byte) 1);
        Ku ku3 = new Ku("OPERATION_IN_PROGRESS", 2, (byte) 2);
        Ku ku4 = new Ku("MISS_PACKET", 3, (byte) 3);
        Ku ku5 = new Ku("SOCKET_BUSY", 4, (byte) 4);
        Ku ku6 = new Ku("VERIFICATION_FAIL", 5, (byte) 5);
        Ku ku7 = new Ku("OVERFLOW", 6, (byte) 6);
        Ku ku8 = new Ku("SIZE_OVER_LIMIT", 7, (byte) 7);
        e = ku8;
        Ku ku9 = new Ku("FIRMWARE_INTERNAL_ERROR", 8, (byte) 128);
        f = ku9;
        Ku ku10 = new Ku("FIRMWARE_INTERNAL_ERROR_NOT_OPEN", 9, (byte) 129);
        Ku ku11 = new Ku("FIRMWARE_INTERNAL_ERROR_ACCESS_ERROR", 10, (byte) 130);
        Ku ku12 = new Ku("FIRMWARE_INTERNAL_ERROR_NOT_FOUND", 11, (byte) 131);
        Ku ku13 = new Ku("FIRMWARE_INTERNAL_ERROR_NOT_VALID", 12, (byte) 132);
        Ku ku14 = new Ku("FIRMWARE_INTERNAL_ERROR_ALREADY_CREATE", 13, (byte) 133);
        Ku ku15 = new Ku("FIRMWARE_INTERNAL_ERROR_NOT_ENOUGH_MEMORY", 14, (byte) 134);
        Ku ku16 = new Ku("FIRMWARE_INTERNAL_ERROR_NOT_IMPLEMENTED", 15, (byte) 135);
        Ku ku17 = new Ku("FIRMWARE_INTERNAL_ERROR_NOT_SUPPORT", 16, (byte) 136);
        g = ku17;
        h = new Ku[]{ku, ku2, ku3, ku4, ku5, ku6, ku7, ku8, ku9, ku10, ku11, ku12, ku13, ku14, ku15, ku16, ku17, new Ku("FIRMWARE_INTERNAL_ERROR_SOCKET_BUSY", 17, (byte) 137), new Ku("FIRMWARE_INTERNAL_ERROR_SOCKET_ALREADY_OPEN", 18, (byte) 138), new Ku("FIRMWARE_INTERNAL_ERROR_INPUT_DATA_INVALID", 19, (byte) 139), new Ku("FIRMWARE_INTERNAL_NOT_AUTHENTICATE", 20, (byte) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL), new Ku("FIRMWARE_INTERNAL_SIZE_OVER_LIMIT", 21, (byte) 141)};
    }
    */

    @DexIgnore
    public Ku(String str, int i2, byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    public static Ku valueOf(String str) {
        return (Ku) Enum.valueOf(Ku.class, str);
    }

    @DexIgnore
    public static Ku[] values() {
        return (Ku[]) h.clone();
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public String getLogName() {
        return this.b;
    }
}
