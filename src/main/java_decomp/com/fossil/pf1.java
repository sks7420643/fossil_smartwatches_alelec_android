package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import com.fossil.Af1;
import com.fossil.Wb1;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pf1<DataT> implements Af1<Uri, DataT> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Af1<File, DataT> b;
    @DexIgnore
    public /* final */ Af1<Uri, DataT> c;
    @DexIgnore
    public /* final */ Class<DataT> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<DataT> implements Bf1<Uri, DataT> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Class<DataT> b;

        @DexIgnore
        public Ai(Context context, Class<DataT> cls) {
            this.a = context;
            this.b = cls;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public final Af1<Uri, DataT> b(Ef1 ef1) {
            return new Pf1(this.a, ef1.d(File.class, this.b), ef1.d(Uri.class, this.b), this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ai<ParcelFileDescriptor> {
        @DexIgnore
        public Bi(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ai<InputStream> {
        @DexIgnore
        public Ci(Context context) {
            super(context, InputStream.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<DataT> implements Wb1<DataT> {
        @DexIgnore
        public static /* final */ String[] l; // = {"_data"};
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ Af1<File, DataT> c;
        @DexIgnore
        public /* final */ Af1<Uri, DataT> d;
        @DexIgnore
        public /* final */ Uri e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ Ob1 h;
        @DexIgnore
        public /* final */ Class<DataT> i;
        @DexIgnore
        public volatile boolean j;
        @DexIgnore
        public volatile Wb1<DataT> k;

        @DexIgnore
        public Di(Context context, Af1<File, DataT> af1, Af1<Uri, DataT> af12, Uri uri, int i2, int i3, Ob1 ob1, Class<DataT> cls) {
            this.b = context.getApplicationContext();
            this.c = af1;
            this.d = af12;
            this.e = uri;
            this.f = i2;
            this.g = i3;
            this.h = ob1;
            this.i = cls;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
            Wb1<DataT> wb1 = this.k;
            if (wb1 != null) {
                wb1.a();
            }
        }

        @DexIgnore
        public final Af1.Ai<DataT> b() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.c.b(g(this.e), this.f, this.g, this.h);
            }
            return this.d.b(f() ? MediaStore.setRequireOriginal(this.e) : this.e, this.f, this.g, this.h);
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
            this.j = true;
            Wb1<DataT> wb1 = this.k;
            if (wb1 != null) {
                wb1.cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super DataT> ai) {
            try {
                Wb1<DataT> e2 = e();
                if (e2 == null) {
                    ai.b(new IllegalArgumentException("Failed to build fetcher for: " + this.e));
                    return;
                }
                this.k = e2;
                if (this.j) {
                    cancel();
                } else {
                    e2.d(sa1, ai);
                }
            } catch (FileNotFoundException e3) {
                ai.b(e3);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: com.fossil.Wb1<Data>, com.fossil.Wb1<DataT> */
        public final Wb1<DataT> e() throws FileNotFoundException {
            Af1.Ai<DataT> b2 = b();
            if (b2 != null) {
                return (Wb1<Data>) b2.c;
            }
            return null;
        }

        @DexIgnore
        public final boolean f() {
            return this.b.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.io.File g(android.net.Uri r8) throws java.io.FileNotFoundException {
            /*
                r7 = this;
                r6 = 0
                android.content.Context r0 = r7.b     // Catch:{ all -> 0x0069 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ all -> 0x0069 }
                java.lang.String[] r2 = com.fossil.Pf1.Di.l     // Catch:{ all -> 0x0069 }
                r3 = 0
                r4 = 0
                r5 = 0
                r1 = r8
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0069 }
                if (r1 == 0) goto L_0x0052
                boolean r0 = r1.moveToFirst()     // Catch:{ all -> 0x004b }
                if (r0 == 0) goto L_0x0052
                java.lang.String r0 = "_data"
                int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x004b }
                java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x004b }
                boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x004b }
                if (r2 != 0) goto L_0x0034
                java.io.File r2 = new java.io.File     // Catch:{ all -> 0x004b }
                r2.<init>(r0)     // Catch:{ all -> 0x004b }
                if (r1 == 0) goto L_0x0033
                r1.close()
            L_0x0033:
                return r2
            L_0x0034:
                java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "File path was empty in media store for: "
                r2.append(r3)
                r2.append(r8)
                java.lang.String r2 = r2.toString()
                r0.<init>(r2)
                throw r0
            L_0x004b:
                r0 = move-exception
            L_0x004c:
                if (r1 == 0) goto L_0x0051
                r1.close()
            L_0x0051:
                throw r0
            L_0x0052:
                java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Failed to media store entry for: "
                r2.append(r3)
                r2.append(r8)
                java.lang.String r2 = r2.toString()
                r0.<init>(r2)
                throw r0
            L_0x0069:
                r0 = move-exception
                r1 = r6
                goto L_0x004c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Pf1.Di.g(android.net.Uri):java.io.File");
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<DataT> getDataClass() {
            return this.i;
        }
    }

    @DexIgnore
    public Pf1(Context context, Af1<File, DataT> af1, Af1<Uri, DataT> af12, Class<DataT> cls) {
        this.a = context.getApplicationContext();
        this.b = af1;
        this.c = af12;
        this.d = cls;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(Uri uri, int i, int i2, Ob1 ob1) {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<DataT> c(Uri uri, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(uri), new Di(this.a, this.b, this.c, uri, i, i2, ob1, this.d));
    }

    @DexIgnore
    public boolean d(Uri uri) {
        return Build.VERSION.SDK_INT >= 29 && Jc1.b(uri);
    }
}
