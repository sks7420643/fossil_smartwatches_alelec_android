package com.fossil;

import com.fossil.Qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rf3 extends Uc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Ki b;

    @DexIgnore
    public Rf3(Qb3 qb3, Qb3.Ki ki) {
        this.b = ki;
    }

    @DexIgnore
    @Override // com.fossil.Tc3
    public final void U1(Ls2 ls2) {
        this.b.onMarkerDragStart(new Ke3(ls2));
    }

    @DexIgnore
    @Override // com.fossil.Tc3
    public final void q2(Ls2 ls2) {
        this.b.onMarkerDragEnd(new Ke3(ls2));
    }

    @DexIgnore
    @Override // com.fossil.Tc3
    public final void t2(Ls2 ls2) {
        this.b.onMarkerDrag(new Ke3(ls2));
    }
}
