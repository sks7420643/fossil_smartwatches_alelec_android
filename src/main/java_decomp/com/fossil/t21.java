package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T21<T> {
    @DexIgnore
    public static /* final */ String f; // = X01.f("ConstraintTracker");
    @DexIgnore
    public /* final */ K41 a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public /* final */ Set<E21<T>> d; // = new LinkedHashSet();
    @DexIgnore
    public T e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List b;

        @DexIgnore
        public Ai(List list) {
            this.b = list;
        }

        @DexIgnore
        public void run() {
            for (E21 e21 : this.b) {
                e21.a(T21.this.e);
            }
        }
    }

    @DexIgnore
    public T21(Context context, K41 k41) {
        this.b = context.getApplicationContext();
        this.a = k41;
    }

    @DexIgnore
    public void a(E21<T> e21) {
        synchronized (this.c) {
            if (this.d.add(e21)) {
                if (this.d.size() == 1) {
                    this.e = b();
                    X01.c().a(f, String.format("%s: initial state = %s", getClass().getSimpleName(), this.e), new Throwable[0]);
                    e();
                }
                e21.a(this.e);
            }
        }
    }

    @DexIgnore
    public abstract T b();

    @DexIgnore
    public void c(E21<T> e21) {
        synchronized (this.c) {
            if (this.d.remove(e21) && this.d.isEmpty()) {
                f();
            }
        }
    }

    @DexIgnore
    public void d(T t) {
        synchronized (this.c) {
            if (this.e != t && (this.e == null || !this.e.equals(t))) {
                this.e = t;
                this.a.a().execute(new Ai(new ArrayList(this.d)));
            }
        }
    }

    @DexIgnore
    public abstract void e();

    @DexIgnore
    public abstract void f();
}
