package com.fossil;

import com.fossil.Dl7;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jq4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<P, E> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 a;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase b;

        @DexIgnore
        public Ai(Lk6 lk6, CoroutineUseCase coroutineUseCase, CoroutineUseCase.Bi bi) {
            this.a = lk6;
            this.b = coroutineUseCase;
        }

        @DexIgnore
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Object obj) {
            b((CoroutineUseCase.Ai) obj);
        }

        @DexIgnore
        public void b(E e) {
            Wg6.c(e, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String h = this.b.h();
            local.d(h, "fail continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                Lk6 lk6 = this.a;
                Dl7.Ai ai = Dl7.Companion;
                lk6.resumeWith(Dl7.constructor-impl(e));
            }
        }

        @DexIgnore
        public void c(P p) {
            Wg6.c(p, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String h = this.b.h();
            local.d(h, "success continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                Lk6 lk6 = this.a;
                Dl7.Ai ai = Dl7.Companion;
                lk6.resumeWith(Dl7.constructor-impl(p));
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Object obj) {
            c((CoroutineUseCase.Di) obj);
        }
    }

    @DexIgnore
    public static final <R extends CoroutineUseCase.Bi, P extends CoroutineUseCase.Di, E extends CoroutineUseCase.Ai> Object a(CoroutineUseCase<? super R, P, E> coroutineUseCase, R r, Xe6<? super CoroutineUseCase.Ci> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        coroutineUseCase.e(r, new Ai(lu7, coroutineUseCase, r));
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }
}
