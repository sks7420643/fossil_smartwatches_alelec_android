package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y61 extends X61 {
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public Y61(boolean z) {
        super(null);
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.X61
    public boolean a(F81 f81) {
        Wg6.c(f81, "size");
        return this.b;
    }
}
