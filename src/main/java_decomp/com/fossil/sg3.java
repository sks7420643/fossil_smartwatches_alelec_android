package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sg3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ Ug3 f;

    @DexIgnore
    public Sg3(Pm3 pm3, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        Ug3 ug3;
        Rc2.g(str2);
        Rc2.g(str3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        if (j2 != 0 && j2 > j) {
            pm3.d().I().b("Event created with reverse previous/current timestamps. appId", Kl3.w(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            ug3 = new Ug3(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator<String> it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (next == null) {
                    pm3.d().F().a("Param name can't be null");
                    it.remove();
                } else {
                    Object F = pm3.F().F(next, bundle2.get(next));
                    if (F == null) {
                        pm3.d().I().b("Param value can't be null", pm3.G().y(next));
                        it.remove();
                    } else {
                        pm3.F().M(bundle2, next, F);
                    }
                }
            }
            ug3 = new Ug3(bundle2);
        }
        this.f = ug3;
    }

    @DexIgnore
    public Sg3(Pm3 pm3, String str, String str2, String str3, long j, long j2, Ug3 ug3) {
        Rc2.g(str2);
        Rc2.g(str3);
        Rc2.k(ug3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        if (j2 != 0 && j2 > j) {
            pm3.d().I().c("Event created with reverse previous/current timestamps. appId, name", Kl3.w(str2), Kl3.w(str3));
        }
        this.f = ug3;
    }

    @DexIgnore
    public final Sg3 a(Pm3 pm3, long j) {
        return new Sg3(pm3, this.c, this.a, this.b, this.d, j, this.f);
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }
}
