package com.fossil;

import androidx.lifecycle.ViewModelProvider;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Po4 implements ViewModelProvider.Factory {
    @DexIgnore
    public /* final */ Map<Class<? extends Ts0>, Provider<Ts0>> a;

    @DexIgnore
    public Po4(Map<Class<? extends Ts0>, Provider<Ts0>> map) {
        Wg6.c(map, "creators");
        this.a = map;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.Factory
    public <T extends Ts0> T create(Class<T> cls) {
        T t;
        Wg6.c(cls, "modelClass");
        Provider<Ts0> provider = this.a.get(cls);
        if (provider == null) {
            Iterator<T> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (cls.isAssignableFrom((Class) next.getKey())) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            provider = t2 != null ? (Provider) t2.getValue() : null;
        }
        if (provider != null) {
            try {
                Ts0 ts0 = provider.get();
                if (ts0 != null) {
                    return (T) ts0;
                }
                throw new Rc6("null cannot be cast to non-null type T");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("unknown model class; " + cls);
        }
    }
}
