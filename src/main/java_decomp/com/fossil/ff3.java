package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ff3 implements Parcelable.Creator<Oe3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Oe3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        int i2 = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i3 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    arrayList2 = Ad2.j(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    Ad2.x(parcel, t, arrayList, Ff3.class.getClassLoader());
                    break;
                case 4:
                    f = Ad2.r(parcel, t);
                    break;
                case 5:
                    i = Ad2.v(parcel, t);
                    break;
                case 6:
                    i2 = Ad2.v(parcel, t);
                    break;
                case 7:
                    f2 = Ad2.r(parcel, t);
                    break;
                case 8:
                    z = Ad2.m(parcel, t);
                    break;
                case 9:
                    z2 = Ad2.m(parcel, t);
                    break;
                case 10:
                    z3 = Ad2.m(parcel, t);
                    break;
                case 11:
                    i3 = Ad2.v(parcel, t);
                    break;
                case 12:
                    arrayList3 = Ad2.j(parcel, t, Me3.CREATOR);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Oe3(arrayList2, arrayList, f, i, i2, f2, z, z2, z3, i3, arrayList3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Oe3[] newArray(int i) {
        return new Oe3[i];
    }
}
