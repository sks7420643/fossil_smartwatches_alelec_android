package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U90 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ O90 b;

    @DexIgnore
    public U90(O90 o90) {
        this.b = o90;
        byte[] array = ByteBuffer.allocate(o90.a.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a).put((byte) 0).array();
        Wg6.b(array, "ByteBuffer.allocate(micr\u2026x00)\n            .array()");
        this.a = array;
    }
}
