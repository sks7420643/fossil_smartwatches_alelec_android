package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import com.fossil.Yo0;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rn0 {
    @DexIgnore
    public static /* final */ View.AccessibilityDelegate c; // = new View.AccessibilityDelegate();
    @DexIgnore
    public /* final */ View.AccessibilityDelegate a;
    @DexIgnore
    public /* final */ View.AccessibilityDelegate b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends View.AccessibilityDelegate {
        @DexIgnore
        public /* final */ Rn0 a;

        @DexIgnore
        public Ai(Rn0 rn0) {
            this.a = rn0;
        }

        @DexIgnore
        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            return this.a.a(view, accessibilityEvent);
        }

        @DexIgnore
        public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
            Zo0 b = this.a.b(view);
            if (b != null) {
                return (AccessibilityNodeProvider) b.d();
            }
            return null;
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.a.f(view, accessibilityEvent);
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            Yo0 E0 = Yo0.E0(accessibilityNodeInfo);
            E0.v0(Mo0.T(view));
            E0.n0(Mo0.O(view));
            E0.s0(Mo0.o(view));
            this.a.g(view, E0);
            E0.f(accessibilityNodeInfo.getText(), view);
            List<Yo0.Ai> c = Rn0.c(view);
            for (int i = 0; i < c.size(); i++) {
                E0.b(c.get(i));
            }
        }

        @DexIgnore
        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.a.h(view, accessibilityEvent);
        }

        @DexIgnore
        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return this.a.i(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            return this.a.j(view, i, bundle);
        }

        @DexIgnore
        public void sendAccessibilityEvent(View view, int i) {
            this.a.l(view, i);
        }

        @DexIgnore
        public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
            this.a.m(view, accessibilityEvent);
        }
    }

    @DexIgnore
    public Rn0() {
        this(c);
    }

    @DexIgnore
    public Rn0(View.AccessibilityDelegate accessibilityDelegate) {
        this.a = accessibilityDelegate;
        this.b = new Ai(this);
    }

    @DexIgnore
    public static List<Yo0.Ai> c(View view) {
        List<Yo0.Ai> list = (List) view.getTag(Pk0.tag_accessibility_actions);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public Zo0 b(View view) {
        AccessibilityNodeProvider accessibilityNodeProvider;
        if (Build.VERSION.SDK_INT < 16 || (accessibilityNodeProvider = this.a.getAccessibilityNodeProvider(view)) == null) {
            return null;
        }
        return new Zo0(accessibilityNodeProvider);
    }

    @DexIgnore
    public View.AccessibilityDelegate d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e(ClickableSpan clickableSpan, View view) {
        if (clickableSpan == null) {
            return false;
        }
        ClickableSpan[] q = Yo0.q(view.createAccessibilityNodeInfo().getText());
        int i = 0;
        while (q != null && i < q.length) {
            if (clickableSpan.equals(q[i])) {
                return true;
            }
            i++;
        }
        return false;
    }

    @DexIgnore
    public void f(View view, AccessibilityEvent accessibilityEvent) {
        this.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void g(View view, Yo0 yo0) {
        this.a.onInitializeAccessibilityNodeInfo(view, yo0.D0());
    }

    @DexIgnore
    public void h(View view, AccessibilityEvent accessibilityEvent) {
        this.a.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public boolean i(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    @DexIgnore
    public boolean j(View view, int i, Bundle bundle) {
        boolean z;
        List<Yo0.Ai> c2 = c(view);
        int i2 = 0;
        while (true) {
            if (i2 >= c2.size()) {
                z = false;
                break;
            }
            Yo0.Ai ai = c2.get(i2);
            if (ai.b() == i) {
                z = ai.d(view, bundle);
                break;
            }
            i2++;
        }
        if (!z && Build.VERSION.SDK_INT >= 16) {
            z = this.a.performAccessibilityAction(view, i, bundle);
        }
        return (z || i != Pk0.accessibility_action_clickable_span) ? z : k(bundle.getInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", -1), view);
    }

    @DexIgnore
    public final boolean k(int i, View view) {
        WeakReference weakReference;
        SparseArray sparseArray = (SparseArray) view.getTag(Pk0.tag_accessibility_clickable_spans);
        if (!(sparseArray == null || (weakReference = (WeakReference) sparseArray.get(i)) == null)) {
            ClickableSpan clickableSpan = (ClickableSpan) weakReference.get();
            if (e(clickableSpan, view)) {
                clickableSpan.onClick(view);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void l(View view, int i) {
        this.a.sendAccessibilityEvent(view, i);
    }

    @DexIgnore
    public void m(View view, AccessibilityEvent accessibilityEvent) {
        this.a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
}
