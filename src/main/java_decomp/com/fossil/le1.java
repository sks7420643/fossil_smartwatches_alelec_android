package com.fossil;

import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Le1 implements ExecutorService {
    @DexIgnore
    public static /* final */ long c; // = TimeUnit.SECONDS.toMillis(10);
    @DexIgnore
    public static volatile int d;
    @DexIgnore
    public /* final */ ExecutorService b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public Ci d; // = Ci.b;
        @DexIgnore
        public String e;
        @DexIgnore
        public long f;

        @DexIgnore
        public Ai(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public Le1 a() {
            if (!TextUtils.isEmpty(this.e)) {
                ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(this.b, this.c, this.f, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new Bi(this.e, this.d, this.a));
                if (this.f != 0) {
                    threadPoolExecutor.allowCoreThreadTimeOut(true);
                }
                return new Le1(threadPoolExecutor);
            }
            throw new IllegalArgumentException("Name must be non-null and non-empty, but given: " + this.e);
        }

        @DexIgnore
        public Ai b(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        public Ai c(int i) {
            this.b = i;
            this.c = i;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ThreadFactory {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Ci b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Thread {
            @DexIgnore
            public Aii(Runnable runnable, String str) {
                super(runnable, str);
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(9);
                if (Bi.this.c) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    Bi.this.b.a(th);
                }
            }
        }

        @DexIgnore
        public Bi(String str, Ci ci, boolean z) {
            this.a = str;
            this.b = ci;
            this.c = z;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Aii aii;
            synchronized (this) {
                aii = new Aii(runnable, "glide-" + this.a + "-thread-" + this.d);
                this.d = this.d + 1;
            }
            return aii;
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        public static final Ci a = new Aii();
        @DexIgnore
        public static final Ci b = a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Ci {
            @DexIgnore
            @Override // com.fossil.Le1.Ci
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        @DexIgnore
        void a(Throwable th);
    }

    @DexIgnore
    public Le1(ExecutorService executorService) {
        this.b = executorService;
    }

    @DexIgnore
    public static int a() {
        if (d == 0) {
            d = Math.min(4, Me1.a());
        }
        return d;
    }

    @DexIgnore
    public static Ai b() {
        int i = a() >= 4 ? 2 : 1;
        Ai ai = new Ai(true);
        ai.c(i);
        ai.b("animation");
        return ai;
    }

    @DexIgnore
    public static Le1 c() {
        return b().a();
    }

    @DexIgnore
    public static Ai d() {
        Ai ai = new Ai(true);
        ai.c(1);
        ai.b("disk-cache");
        return ai;
    }

    @DexIgnore
    public static Le1 e() {
        return d().a();
    }

    @DexIgnore
    public static Ai f() {
        Ai ai = new Ai(false);
        ai.c(a());
        ai.b("source");
        return ai;
    }

    @DexIgnore
    public static Le1 g() {
        return f().a();
    }

    @DexIgnore
    public static Le1 h() {
        return new Le1(new ThreadPoolExecutor(0, Integer.MAX_VALUE, c, TimeUnit.MILLISECONDS, new SynchronousQueue(), new Bi("source-unlimited", Ci.b, false)));
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.b.awaitTermination(j, timeUnit);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.b.invokeAll(collection);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException {
        return this.b.invokeAll(collection, j, timeUnit);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return (T) this.b.invokeAny(collection);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return (T) this.b.invokeAny(collection, j, timeUnit);
    }

    @DexIgnore
    public boolean isShutdown() {
        return this.b.isShutdown();
    }

    @DexIgnore
    public boolean isTerminated() {
        return this.b.isTerminated();
    }

    @DexIgnore
    public void shutdown() {
        this.b.shutdown();
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public List<Runnable> shutdownNow() {
        return this.b.shutdownNow();
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public Future<?> submit(Runnable runnable) {
        return this.b.submit(runnable);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.b.submit(runnable, t);
    }

    @DexIgnore
    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Callable<T> callable) {
        return this.b.submit(callable);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
