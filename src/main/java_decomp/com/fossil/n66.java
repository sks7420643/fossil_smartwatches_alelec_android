package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N66 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator CREATOR; // = new Ai();
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "in");
            return new N66(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object[] newArray(int i) {
            return new N66[i];
        }
    }

    @DexIgnore
    public N66() {
        this(null, null, null, null, null, 31, null);
    }

    @DexIgnore
    public N66(String str, String str2, String str3, String str4, String str5) {
        Wg6.c(str, "id");
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ N66(String str, String str2, String str3, String str4, String str5, int i, Qg6 qg6) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? "" : str3, (i & 8) != 0 ? "" : str4, (i & 16) != 0 ? "" : str5);
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof N66) {
                N66 n66 = (N66) obj;
                if (!Wg6.a(this.b, n66.b) || !Wg6.a(this.c, n66.c) || !Wg6.a(this.d, n66.d) || !Wg6.a(this.e, n66.e) || !Wg6.a(this.f, n66.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.f;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "PresetConfig(id=" + this.b + ", icon=" + this.c + ", name=" + this.d + ", position=" + this.e + ", value=" + this.f + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
    }
}
