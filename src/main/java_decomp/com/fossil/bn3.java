package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Vg3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 d;

    @DexIgnore
    public Bn3(Qm3 qm3, Vg3 vg3, Or3 or3) {
        this.d = qm3;
        this.b = vg3;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        Vg3 Y2 = this.d.Y2(this.b, this.c);
        this.d.b.d0();
        this.d.b.o(Y2, this.c);
    }
}
