package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xd1 implements Rd1 {
    @DexIgnore
    public static /* final */ Bitmap.Config j; // = Bitmap.Config.ARGB_8888;
    @DexIgnore
    public /* final */ Yd1 a;
    @DexIgnore
    public /* final */ Set<Bitmap.Config> b;
    @DexIgnore
    public /* final */ Ai c;
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Bitmap bitmap);

        @DexIgnore
        void b(Bitmap bitmap);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ai {
        @DexIgnore
        @Override // com.fossil.Xd1.Ai
        public void a(Bitmap bitmap) {
        }

        @DexIgnore
        @Override // com.fossil.Xd1.Ai
        public void b(Bitmap bitmap) {
        }
    }

    @DexIgnore
    public Xd1(long j2) {
        this(j2, l(), k());
    }

    @DexIgnore
    public Xd1(long j2, Yd1 yd1, Set<Bitmap.Config> set) {
        this.d = j2;
        this.a = yd1;
        this.b = set;
        this.c = new Bi();
    }

    @DexIgnore
    @TargetApi(26)
    public static void f(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE) {
            throw new IllegalArgumentException("Cannot create a mutable Bitmap with config: " + config + ". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
        }
    }

    @DexIgnore
    public static Bitmap g(int i2, int i3, Bitmap.Config config) {
        if (config == null) {
            config = j;
        }
        return Bitmap.createBitmap(i2, i3, config);
    }

    @DexIgnore
    @TargetApi(26)
    public static Set<Bitmap.Config> k() {
        HashSet hashSet = new HashSet(Arrays.asList(Bitmap.Config.values()));
        if (Build.VERSION.SDK_INT >= 19) {
            hashSet.add(null);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            hashSet.remove(Bitmap.Config.HARDWARE);
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public static Yd1 l() {
        return Build.VERSION.SDK_INT >= 19 ? new Ae1() : new Pd1();
    }

    @DexIgnore
    @TargetApi(19)
    public static void o(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    @DexIgnore
    public static void p(Bitmap bitmap) {
        bitmap.setHasAlpha(true);
        o(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Rd1
    @SuppressLint({"InlinedApi"})
    public void a(int i2) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "trimMemory, level=" + i2);
        }
        if (i2 >= 40 || (Build.VERSION.SDK_INT >= 23 && i2 >= 20)) {
            d();
        } else if (i2 >= 20 || i2 == 15) {
            q(n() / 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Rd1
    public void b(Bitmap bitmap) {
        synchronized (this) {
            if (bitmap == null) {
                throw new NullPointerException("Bitmap must not be null");
            } else if (bitmap.isRecycled()) {
                throw new IllegalStateException("Cannot pool recycled bitmap");
            } else if (!bitmap.isMutable() || ((long) this.a.e(bitmap)) > this.d || !this.b.contains(bitmap.getConfig())) {
                if (Log.isLoggable("LruBitmapPool", 2)) {
                    Log.v("LruBitmapPool", "Reject bitmap from pool, bitmap: " + this.a.d(bitmap) + ", is mutable: " + bitmap.isMutable() + ", is allowed config: " + this.b.contains(bitmap.getConfig()));
                }
                bitmap.recycle();
            } else {
                int e2 = this.a.e(bitmap);
                this.a.b(bitmap);
                this.c.b(bitmap);
                this.h++;
                this.e = ((long) e2) + this.e;
                if (Log.isLoggable("LruBitmapPool", 2)) {
                    Log.v("LruBitmapPool", "Put bitmap in pool=" + this.a.d(bitmap));
                }
                h();
                j();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Rd1
    public Bitmap c(int i2, int i3, Bitmap.Config config) {
        Bitmap m = m(i2, i3, config);
        if (m == null) {
            return g(i2, i3, config);
        }
        m.eraseColor(0);
        return m;
    }

    @DexIgnore
    @Override // com.fossil.Rd1
    public void d() {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "clearMemory");
        }
        q(0);
    }

    @DexIgnore
    @Override // com.fossil.Rd1
    public Bitmap e(int i2, int i3, Bitmap.Config config) {
        Bitmap m = m(i2, i3, config);
        return m == null ? g(i2, i3, config) : m;
    }

    @DexIgnore
    public final void h() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            i();
        }
    }

    @DexIgnore
    public final void i() {
        Log.v("LruBitmapPool", "Hits=" + this.f + ", misses=" + this.g + ", puts=" + this.h + ", evictions=" + this.i + ", currentSize=" + this.e + ", maxSize=" + this.d + "\nStrategy=" + this.a);
    }

    @DexIgnore
    public final void j() {
        q(this.d);
    }

    @DexIgnore
    public final Bitmap m(int i2, int i3, Bitmap.Config config) {
        Bitmap c2;
        synchronized (this) {
            f(config);
            c2 = this.a.c(i2, i3, config != null ? config : j);
            if (c2 == null) {
                if (Log.isLoggable("LruBitmapPool", 3)) {
                    Log.d("LruBitmapPool", "Missing bitmap=" + this.a.a(i2, i3, config));
                }
                this.g++;
            } else {
                this.f++;
                this.e -= (long) this.a.e(c2);
                this.c.a(c2);
                p(c2);
            }
            if (Log.isLoggable("LruBitmapPool", 2)) {
                Log.v("LruBitmapPool", "Get bitmap=" + this.a.a(i2, i3, config));
            }
            h();
        }
        return c2;
    }

    @DexIgnore
    public long n() {
        return this.d;
    }

    @DexIgnore
    public final void q(long j2) {
        synchronized (this) {
            while (this.e > j2) {
                Bitmap removeLast = this.a.removeLast();
                if (removeLast == null) {
                    if (Log.isLoggable("LruBitmapPool", 5)) {
                        Log.w("LruBitmapPool", "Size mismatch, resetting");
                        i();
                    }
                    this.e = 0;
                    return;
                }
                this.c.a(removeLast);
                this.e -= (long) this.a.e(removeLast);
                this.i++;
                if (Log.isLoggable("LruBitmapPool", 3)) {
                    Log.d("LruBitmapPool", "Evicting bitmap=" + this.a.d(removeLast));
                }
                h();
                removeLast.recycle();
            }
        }
    }
}
