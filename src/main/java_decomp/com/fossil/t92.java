package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T92 {
    @DexIgnore
    public /* final */ Z82 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Q62<?> c;

    @DexIgnore
    public T92(Z82 z82, int i, Q62<?> q62) {
        this.a = z82;
        this.b = i;
        this.c = q62;
    }
}
