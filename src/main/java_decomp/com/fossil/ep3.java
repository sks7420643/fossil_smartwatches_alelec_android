package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ep3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ Ap3 c;

    @DexIgnore
    public Ep3(Ap3 ap3, long j) {
        this.c = ap3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        this.c.o().v(this.b);
        this.c.e = null;
    }
}
