package com.fossil;

import android.graphics.Color;
import com.mapped.Kc6;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum O87 {
    BLACK(Color.parseColor("#000000")),
    WHITE(Color.parseColor("#FFFFFF")),
    LIGHT_GRAY(Color.parseColor("#AAAAAA")),
    DARK_GRAY(Color.parseColor("#555555"));
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public static /* final */ O87 c; // = WHITE;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final O87 a() {
            return O87.c;
        }
    }

    @DexIgnore
    public O87(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final Nb7 toWFThemeColour() {
        int i = P87.a[ordinal()];
        if (i == 1) {
            return Nb7.BLACK;
        }
        if (i == 2) {
            return Nb7.WHITE;
        }
        if (i == 3) {
            return Nb7.LIGHT_GREY;
        }
        if (i == 4) {
            return Nb7.DARK_GREY;
        }
        throw new Kc6();
    }
}
