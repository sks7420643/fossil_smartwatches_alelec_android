package com.fossil;

import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ly7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(Ly7.class, Object.class, "onCloseHandler");
    @DexIgnore
    public /* final */ Jz7 a; // = new Jz7();
    @DexIgnore
    public volatile Object onCloseHandler; // = null;

    @DexIgnore
    public boolean c(Throwable th) {
        boolean z;
        Py7<?> py7 = new Py7<>(th);
        Lz7 lz7 = this.a;
        while (true) {
            Lz7 n = lz7.n();
            z = true;
            if (!(n instanceof Py7)) {
                if (n.g(py7, lz7)) {
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (!z) {
            Lz7 n2 = this.a.n();
            if (n2 != null) {
                py7 = (Py7) n2;
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.channels.Closed<*>");
            }
        }
        i(py7);
        if (z) {
            j(th);
        }
        return z;
    }

    @DexIgnore
    public final int d() {
        Jz7 jz7 = this.a;
        Object l = jz7.l();
        if (l != null) {
            int i = 0;
            Lz7 lz7 = (Lz7) l;
            while (!Wg6.a(lz7, jz7)) {
                int i2 = lz7 instanceof Lz7 ? i + 1 : i;
                lz7 = lz7.m();
                i = i2;
            }
            return i;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public String e() {
        return "";
    }

    @DexIgnore
    public final Py7<?> f() {
        Lz7 n = this.a.n();
        if (!(n instanceof Py7)) {
            n = null;
        }
        Py7<?> py7 = (Py7) n;
        if (py7 == null) {
            return null;
        }
        i(py7);
        return py7;
    }

    @DexIgnore
    public final Jz7 g() {
        return this.a;
    }

    @DexIgnore
    public final String h() {
        String str;
        Lz7 m = this.a.m();
        if (m == this.a) {
            return "EmptyQueue";
        }
        if (m instanceof Py7) {
            str = m.toString();
        } else if (m instanceof Uy7) {
            str = "ReceiveQueued";
        } else if (m instanceof Xy7) {
            str = "SendQueued";
        } else {
            str = "UNEXPECTED:" + m;
        }
        Lz7 n = this.a.n();
        if (n == m) {
            return str;
        }
        String str2 = str + ",queueSize=" + d();
        if (!(n instanceof Py7)) {
            return str2;
        }
        return str2 + ",closedForSend=" + n;
    }

    @DexIgnore
    public final void i(Py7<?> py7) {
        Object b2 = Iz7.b(null, 1, null);
        while (true) {
            Lz7 n = py7.n();
            if (!(n instanceof Uy7)) {
                n = null;
            }
            Uy7 uy7 = (Uy7) n;
            if (uy7 == null) {
                break;
            } else if (!uy7.r()) {
                uy7.o();
            } else {
                b2 = Iz7.c(b2, uy7);
            }
        }
        if (b2 != null) {
            if (!(b2 instanceof ArrayList)) {
                ((Uy7) b2).w(py7);
            } else if (b2 != null) {
                ArrayList arrayList = (ArrayList) b2;
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    ((Uy7) arrayList.get(size)).w(py7);
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<E> /* = java.util.ArrayList<E> */");
            }
        }
        k(py7);
    }

    @DexIgnore
    public final void j(Throwable th) {
        Object obj;
        Object obj2 = this.onCloseHandler;
        if (obj2 != null && obj2 != (obj = Ky7.c) && b.compareAndSet(this, obj2, obj)) {
            Ir7.d(obj2, 1);
            ((Hg6) obj2).invoke(th);
        }
    }

    @DexIgnore
    public void k(Lz7 lz7) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        r0 = null;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.Wy7<E> l() {
        /*
            r3 = this;
            com.fossil.Jz7 r2 = r3.a
        L_0x0002:
            java.lang.Object r0 = r2.l()
            if (r0 == 0) goto L_0x002b
            com.fossil.Lz7 r0 = (com.fossil.Lz7) r0
            if (r0 != r2) goto L_0x0010
        L_0x000c:
            r0 = 0
        L_0x000d:
            com.fossil.Wy7 r0 = (com.fossil.Wy7) r0
            return r0
        L_0x0010:
            boolean r1 = r0 instanceof com.fossil.Wy7
            if (r1 == 0) goto L_0x000c
            r1 = r0
            com.fossil.Wy7 r1 = (com.fossil.Wy7) r1
            boolean r1 = r1 instanceof com.fossil.Py7
            if (r1 == 0) goto L_0x0021
            boolean r1 = r0.q()
            if (r1 == 0) goto L_0x000d
        L_0x0021:
            com.fossil.Lz7 r1 = r0.t()
            if (r1 == 0) goto L_0x000d
            r1.p()
            goto L_0x0002
        L_0x002b:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
        */
        //  java.lang.String r1 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
        /*
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ly7.l():com.fossil.Wy7");
    }

    @DexIgnore
    public String toString() {
        return Ov7.a(this) + '@' + Ov7.b(this) + '{' + h() + '}' + e();
    }
}
