package com.fossil;

import android.content.ContentResolver;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mc1 {
    @DexIgnore
    public static /* final */ Ic1 f; // = new Ic1();
    @DexIgnore
    public /* final */ Ic1 a;
    @DexIgnore
    public /* final */ Lc1 b;
    @DexIgnore
    public /* final */ Od1 c;
    @DexIgnore
    public /* final */ ContentResolver d;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> e;

    @DexIgnore
    public Mc1(List<ImageHeaderParser> list, Ic1 ic1, Lc1 lc1, Od1 od1, ContentResolver contentResolver) {
        this.a = ic1;
        this.b = lc1;
        this.c = od1;
        this.d = contentResolver;
        this.e = list;
    }

    @DexIgnore
    public Mc1(List<ImageHeaderParser> list, Lc1 lc1, Od1 od1, ContentResolver contentResolver) {
        this(list, f, lc1, od1, contentResolver);
    }

    @DexIgnore
    public int a(Uri uri) {
        InputStream inputStream = null;
        try {
            inputStream = this.d.openInputStream(uri);
            int b2 = Lb1.b(this.e, inputStream, this.c);
            if (inputStream == null) {
                return b2;
            }
            try {
                inputStream.close();
                return b2;
            } catch (IOException e2) {
                return b2;
            }
        } catch (IOException | NullPointerException e3) {
            if (Log.isLoggable("ThumbStreamOpener", 3)) {
                Log.d("ThumbStreamOpener", "Failed to open uri: " + uri, e3);
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                }
            }
            return -1;
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                }
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002b A[Catch:{ all -> 0x0047 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String b(android.net.Uri r6) {
        /*
            r5 = this;
            r2 = 0
            com.fossil.Lc1 r0 = r5.b     // Catch:{ SecurityException -> 0x0020, all -> 0x004f }
            android.database.Cursor r1 = r0.a(r6)     // Catch:{ SecurityException -> 0x0020, all -> 0x004f }
            if (r1 == 0) goto L_0x001a
            boolean r0 = r1.moveToFirst()     // Catch:{ SecurityException -> 0x0051 }
            if (r0 == 0) goto L_0x001a
            r0 = 0
            java.lang.String r2 = r1.getString(r0)     // Catch:{ SecurityException -> 0x0051 }
            if (r1 == 0) goto L_0x0019
            r1.close()
        L_0x0019:
            return r2
        L_0x001a:
            if (r1 == 0) goto L_0x0019
            r1.close()
            goto L_0x0019
        L_0x0020:
            r0 = move-exception
            r1 = r2
        L_0x0022:
            java.lang.String r3 = "ThumbStreamOpener"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ all -> 0x0047 }
            if (r3 == 0) goto L_0x0041
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0047 }
            r3.<init>()     // Catch:{ all -> 0x0047 }
            java.lang.String r4 = "Failed to query for thumbnail for Uri: "
            r3.append(r4)     // Catch:{ all -> 0x0047 }
            r3.append(r6)     // Catch:{ all -> 0x0047 }
            java.lang.String r4 = "ThumbStreamOpener"
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0047 }
            android.util.Log.d(r4, r3, r0)     // Catch:{ all -> 0x0047 }
        L_0x0041:
            if (r1 == 0) goto L_0x0019
            r1.close()
            goto L_0x0019
        L_0x0047:
            r0 = move-exception
            r2 = r1
        L_0x0049:
            if (r2 == 0) goto L_0x004e
            r2.close()
        L_0x004e:
            throw r0
        L_0x004f:
            r0 = move-exception
            goto L_0x0049
        L_0x0051:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Mc1.b(android.net.Uri):java.lang.String");
    }

    @DexIgnore
    public final boolean c(File file) {
        return this.a.a(file) && 0 < this.a.c(file);
    }

    @DexIgnore
    public InputStream d(Uri uri) throws FileNotFoundException {
        String b2 = b(uri);
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        File b3 = this.a.b(b2);
        if (!c(b3)) {
            return null;
        }
        Uri fromFile = Uri.fromFile(b3);
        try {
            return this.d.openInputStream(fromFile);
        } catch (NullPointerException e2) {
            throw ((FileNotFoundException) new FileNotFoundException("NPE opening uri: " + uri + " -> " + fromFile).initCause(e2));
        }
    }
}
