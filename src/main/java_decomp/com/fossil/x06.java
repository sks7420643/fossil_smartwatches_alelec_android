package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X06 extends Fq4 {
    @DexIgnore
    public abstract void n();

    @DexIgnore
    public abstract boolean o();

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public abstract void q(ContactGroup contactGroup);

    @DexIgnore
    public abstract void r(boolean z);

    @DexIgnore
    public abstract void s();

    @DexIgnore
    public abstract void t(R16 r16);

    @DexIgnore
    public abstract void u(boolean z);

    @DexIgnore
    public abstract void v(boolean z);

    @DexIgnore
    public abstract void w(boolean z);
}
