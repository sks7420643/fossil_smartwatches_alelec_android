package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ Xr3 d;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 e;
    @DexIgnore
    public /* final */ /* synthetic */ Xr3 f;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 g;

    @DexIgnore
    public Tp3(Fp3 fp3, boolean z, boolean z2, Xr3 xr3, Or3 or3, Xr3 xr32) {
        this.g = fp3;
        this.b = z;
        this.c = z2;
        this.d = xr3;
        this.e = or3;
        this.f = xr32;
    }

    @DexIgnore
    public final void run() {
        Cl3 cl3 = this.g.d;
        if (cl3 == null) {
            this.g.d().F().a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.b) {
            this.g.M(cl3, this.c ? null : this.d, this.e);
        } else {
            try {
                if (TextUtils.isEmpty(this.f.b)) {
                    cl3.s(this.d, this.e);
                } else {
                    cl3.D1(this.d);
                }
            } catch (RemoteException e2) {
                this.g.d().F().b("Failed to send conditional user property to the service", e2);
            }
        }
        this.g.e0();
    }
}
