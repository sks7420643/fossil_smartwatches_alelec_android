package com.fossil;

import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Kj0;
import com.fossil.Oj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hj0 implements Kj0.Ai {
    @DexIgnore
    public Oj0 a; // = null;
    @DexIgnore
    public float b; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Gj0 d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public Hj0(Ij0 ij0) {
        this.d = new Gj0(this, ij0);
    }

    @DexIgnore
    @Override // com.fossil.Kj0.Ai
    public void a(Oj0 oj0) {
        int i = oj0.d;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.d.l(oj0, f);
    }

    @DexIgnore
    @Override // com.fossil.Kj0.Ai
    public Oj0 b(Kj0 kj0, boolean[] zArr) {
        return this.d.g(zArr, null);
    }

    @DexIgnore
    @Override // com.fossil.Kj0.Ai
    public void c(Kj0.Ai ai) {
        if (ai instanceof Hj0) {
            Hj0 hj0 = (Hj0) ai;
            this.a = null;
            this.d.c();
            int i = 0;
            while (true) {
                Gj0 gj0 = hj0.d;
                if (i < gj0.a) {
                    this.d.a(gj0.h(i), hj0.d.i(i), true);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Kj0.Ai
    public void clear() {
        this.d.c();
        this.a = null;
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public Hj0 d(Kj0 kj0, int i) {
        this.d.l(kj0.p(i, "ep"), 1.0f);
        this.d.l(kj0.p(i, UserDataStore.EMAIL), -1.0f);
        return this;
    }

    @DexIgnore
    public Hj0 e(Oj0 oj0, int i) {
        this.d.l(oj0, (float) i);
        return this;
    }

    @DexIgnore
    public boolean f(Kj0 kj0) {
        boolean z;
        Oj0 b2 = this.d.b(kj0);
        if (b2 == null) {
            z = true;
        } else {
            v(b2);
            z = false;
        }
        if (this.d.a == 0) {
            this.e = true;
        }
        return z;
    }

    @DexIgnore
    public Hj0 g(Oj0 oj0, Oj0 oj02, int i, float f, Oj0 oj03, Oj0 oj04, int i2) {
        if (oj02 == oj03) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj04, 1.0f);
            this.d.l(oj02, -2.0f);
        } else if (f == 0.5f) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj03, -1.0f);
            this.d.l(oj04, 1.0f);
            if (i > 0 || i2 > 0) {
                this.b = (float) ((-i) + i2);
            }
        } else if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
            this.b = (float) i;
        } else if (f >= 1.0f) {
            this.d.l(oj03, -1.0f);
            this.d.l(oj04, 1.0f);
            this.b = (float) i2;
        } else {
            float f2 = 1.0f - f;
            this.d.l(oj0, f2 * 1.0f);
            this.d.l(oj02, f2 * -1.0f);
            this.d.l(oj03, -1.0f * f);
            this.d.l(oj04, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.b = (((float) (-i)) * f2) + (((float) i2) * f);
            }
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Kj0.Ai
    public Oj0 getKey() {
        return this.a;
    }

    @DexIgnore
    public Hj0 h(Oj0 oj0, int i) {
        this.a = oj0;
        float f = (float) i;
        oj0.e = f;
        this.b = f;
        this.e = true;
        return this;
    }

    @DexIgnore
    public Hj0 i(Oj0 oj0, Oj0 oj02, Oj0 oj03, float f) {
        this.d.l(oj0, -1.0f);
        this.d.l(oj02, 1.0f - f);
        this.d.l(oj03, f);
        return this;
    }

    @DexIgnore
    public Hj0 j(Oj0 oj0, Oj0 oj02, Oj0 oj03, Oj0 oj04, float f) {
        this.d.l(oj0, -1.0f);
        this.d.l(oj02, 1.0f);
        this.d.l(oj03, f);
        this.d.l(oj04, -f);
        return this;
    }

    @DexIgnore
    public Hj0 k(float f, float f2, float f3, Oj0 oj0, Oj0 oj02, Oj0 oj03, Oj0 oj04) {
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f == f3) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj04, 1.0f);
            this.d.l(oj03, -1.0f);
        } else if (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
        } else if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.l(oj03, 1.0f);
            this.d.l(oj04, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj04, f4);
            this.d.l(oj03, -f4);
        }
        return this;
    }

    @DexIgnore
    public Hj0 l(Oj0 oj0, int i) {
        if (i < 0) {
            this.b = (float) (i * -1);
            this.d.l(oj0, 1.0f);
        } else {
            this.b = (float) i;
            this.d.l(oj0, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public Hj0 m(Oj0 oj0, Oj0 oj02, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
        } else {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public Hj0 n(Oj0 oj0, Oj0 oj02, Oj0 oj03, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
            this.d.l(oj03, 1.0f);
        } else {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj03, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public Hj0 o(Oj0 oj0, Oj0 oj02, Oj0 oj03, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.l(oj0, -1.0f);
            this.d.l(oj02, 1.0f);
            this.d.l(oj03, -1.0f);
        } else {
            this.d.l(oj0, 1.0f);
            this.d.l(oj02, -1.0f);
            this.d.l(oj03, 1.0f);
        }
        return this;
    }

    @DexIgnore
    public Hj0 p(Oj0 oj0, Oj0 oj02, Oj0 oj03, Oj0 oj04, float f) {
        this.d.l(oj03, 0.5f);
        this.d.l(oj04, 0.5f);
        this.d.l(oj0, -0.5f);
        this.d.l(oj02, -0.5f);
        this.b = -f;
        return this;
    }

    @DexIgnore
    public void q() {
        float f = this.b;
        if (f < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b = f * -1.0f;
            this.d.j();
        }
    }

    @DexIgnore
    public boolean r() {
        Oj0 oj0 = this.a;
        return oj0 != null && (oj0.g == Oj0.Ai.UNRESTRICTED || this.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public boolean s(Oj0 oj0) {
        return this.d.d(oj0);
    }

    @DexIgnore
    public boolean t() {
        return this.a == null && this.b == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.d.a == 0;
    }

    @DexIgnore
    public String toString() {
        return x();
    }

    @DexIgnore
    public Oj0 u(Oj0 oj0) {
        return this.d.g(null, oj0);
    }

    @DexIgnore
    public void v(Oj0 oj0) {
        Oj0 oj02 = this.a;
        if (oj02 != null) {
            this.d.l(oj02, -1.0f);
            this.a = null;
        }
        float m = this.d.m(oj0, true) * -1.0f;
        this.a = oj0;
        if (m != 1.0f) {
            this.b /= m;
            this.d.e(m);
        }
    }

    @DexIgnore
    public void w() {
        this.a = null;
        this.d.c();
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String x() {
        /*
        // Method dump skipped, instructions count: 258
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Hj0.x():java.lang.String");
    }
}
