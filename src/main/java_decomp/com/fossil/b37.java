package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B37 implements Factory<SetNotificationUseCase> {
    @DexIgnore
    public /* final */ Provider<V36> a;
    @DexIgnore
    public /* final */ Provider<D26> b;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<An4> f;

    @DexIgnore
    public B37(Provider<V36> provider, Provider<D26> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<An4> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static B37 a(Provider<V36> provider, Provider<D26> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<An4> provider6) {
        return new B37(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static SetNotificationUseCase c(V36 v36, D26 d26, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, An4 an4) {
        return new SetNotificationUseCase(v36, d26, notificationSettingsDatabase, notificationsRepository, deviceRepository, an4);
    }

    @DexIgnore
    public SetNotificationUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
