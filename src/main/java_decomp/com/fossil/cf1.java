package com.fossil;

import com.fossil.Ua1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cf1 {
    @DexIgnore
    public /* final */ Ef1 a;
    @DexIgnore
    public /* final */ Ai b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Map<Class<?>, Aii<?>> a; // = new HashMap();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii<Model> {
            @DexIgnore
            public /* final */ List<Af1<Model, ?>> a;

            @DexIgnore
            public Aii(List<Af1<Model, ?>> list) {
                this.a = list;
            }
        }

        @DexIgnore
        public void a() {
            this.a.clear();
        }

        @DexIgnore
        public <Model> List<Af1<Model, ?>> b(Class<Model> cls) {
            Aii<?> aii = this.a.get(cls);
            if (aii == null) {
                return null;
            }
            return aii.a;
        }

        @DexIgnore
        public <Model> void c(Class<Model> cls, List<Af1<Model, ?>> list) {
            if (this.a.put(cls, new Aii<>(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }
    }

    @DexIgnore
    public Cf1(Ef1 ef1) {
        this.b = new Ai();
        this.a = ef1;
    }

    @DexIgnore
    public Cf1(Mn0<List<Throwable>> mn0) {
        this(new Ef1(mn0));
    }

    @DexIgnore
    public static <A> Class<A> b(A a2) {
        return (Class<A>) a2.getClass();
    }

    @DexIgnore
    public <Model, Data> void a(Class<Model> cls, Class<Data> cls2, Bf1<? extends Model, ? extends Data> bf1) {
        synchronized (this) {
            this.a.b(cls, cls2, bf1);
            this.b.a();
        }
    }

    @DexIgnore
    public List<Class<?>> c(Class<?> cls) {
        List<Class<?>> g;
        synchronized (this) {
            g = this.a.g(cls);
        }
        return g;
    }

    @DexIgnore
    public <A> List<Af1<A, ?>> d(A a2) {
        List<Af1<A, ?>> e = e(b(a2));
        if (!e.isEmpty()) {
            int size = e.size();
            List<Af1<A, ?>> emptyList = Collections.emptyList();
            boolean z = true;
            int i = 0;
            while (i < size) {
                Af1<A, ?> af1 = e.get(i);
                if (af1.a(a2)) {
                    if (z) {
                        emptyList = new ArrayList<>(size - i);
                        z = false;
                    }
                    emptyList.add(af1);
                }
                i++;
                z = z;
            }
            if (!emptyList.isEmpty()) {
                return emptyList;
            }
            throw new Ua1.Ci(a2, e);
        }
        throw new Ua1.Ci(a2);
    }

    @DexIgnore
    public final <A> List<Af1<A, ?>> e(Class<A> cls) {
        List<Af1<A, ?>> b2;
        synchronized (this) {
            b2 = this.b.b(cls);
            if (b2 == null) {
                b2 = Collections.unmodifiableList(this.a.e(cls));
                this.b.c(cls, b2);
            }
        }
        return b2;
    }
}
