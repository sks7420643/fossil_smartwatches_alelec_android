package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface Su3 {
    @DexIgnore
    void a(Ru3 ru3);

    @DexIgnore
    void b(Ru3 ru3, int i, int i2);

    @DexIgnore
    void c(Ru3 ru3, int i, int i2);

    @DexIgnore
    void d(Ru3 ru3, int i, int i2);
}
