package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ae3 {
    @DexIgnore
    public static Fs2 a;

    @DexIgnore
    public static Zd3 a() {
        try {
            return new Zd3(f().g());
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Zd3 b(float f) {
        try {
            return new Zd3(f().O1(f));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Zd3 c(String str) {
        try {
            return new Zd3(f().zza(str));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Zd3 d(Bitmap bitmap) {
        try {
            return new Zd3(f().zza(bitmap));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static void e(Fs2 fs2) {
        if (a == null) {
            Rc2.k(fs2);
            a = fs2;
        }
    }

    @DexIgnore
    public static Fs2 f() {
        Fs2 fs2 = a;
        Rc2.l(fs2, "IBitmapDescriptorFactory is not initialized");
        return fs2;
    }
}
