package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import com.mapped.W6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yg1 {
    @DexIgnore
    public static volatile boolean a; // = true;

    @DexIgnore
    public static Drawable a(Context context, int i, Resources.Theme theme) {
        return c(context, context, i, theme);
    }

    @DexIgnore
    public static Drawable b(Context context, Context context2, int i) {
        return c(context, context2, i, null);
    }

    @DexIgnore
    public static Drawable c(Context context, Context context2, int i, Resources.Theme theme) {
        try {
            if (a) {
                return e(context2, i, theme);
            }
        } catch (NoClassDefFoundError e) {
            a = false;
        } catch (IllegalStateException e2) {
            if (!context.getPackageName().equals(context2.getPackageName())) {
                return W6.f(context2, i);
            }
            throw e2;
        } catch (Resources.NotFoundException e3) {
        }
        if (theme == null) {
            theme = context2.getTheme();
        }
        return d(context2, i, theme);
    }

    @DexIgnore
    public static Drawable d(Context context, int i, Resources.Theme theme) {
        return Nl0.a(context.getResources(), i, theme);
    }

    @DexIgnore
    public static Drawable e(Context context, int i, Resources.Theme theme) {
        if (theme != null) {
            context = new Qf0(context, theme);
        }
        return Gf0.d(context, i);
    }
}
