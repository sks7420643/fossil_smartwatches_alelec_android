package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jo1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public long b;
    @DexIgnore
    public byte c;
    @DexIgnore
    public String d;
    @DexIgnore
    public short e;
    @DexIgnore
    public lo1 f;
    @DexIgnore
    public cv1 g;
    @DexIgnore
    public no1 h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jo1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jo1 createFromParcel(Parcel parcel) {
            return new jo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jo1[] newArray(int i) {
            return new jo1[i];
        }
    }

    /*
    static {
        hy1.d(fq7.f1179a);
        hy1.c(fq7.f1179a);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ jo1(android.os.Parcel r3, com.fossil.kq7 r4) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0053
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            r2.<init>(r0)
            long r0 = r3.readLong()
            r2.b = r0
            byte r0 = r3.readByte()
            r2.c = r0
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0050
        L_0x0020:
            r2.m11setSender(r0)
            int r0 = r3.readInt()
            short r0 = (short) r0
            r2.m10setPriority(r0)
            java.lang.Class<com.fossil.lo1> r0 = com.fossil.lo1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r3.readParcelable(r0)
            com.fossil.lo1 r0 = (com.fossil.lo1) r0
            r2.f = r0
            java.io.Serializable r0 = r3.readSerializable()
            com.fossil.no1 r0 = (com.fossil.no1) r0
            r2.h = r0
            java.lang.Class<com.fossil.cv1> r0 = com.fossil.cv1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r3.readParcelable(r0)
            com.fossil.cv1 r0 = (com.fossil.cv1) r0
            r2.g = r0
            return
        L_0x0050:
            java.lang.String r0 = ""
            goto L_0x0020
        L_0x0053:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jo1.<init>(android.os.Parcel, com.fossil.kq7):void");
    }

    @DexIgnore
    public jo1(String str) {
        this.i = str;
        ix1 ix1 = ix1.f1688a;
        Charset c2 = hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            this.b = ix1.b(dm7.p(bytes, (byte) 0), ix1.a.CRC32);
            this.d = "";
            this.e = (short) 255;
            return;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public jo1(String str, lo1 lo1, no1 no1) {
        this(str);
        this.f = lo1;
        this.h = no1;
    }

    @DexIgnore
    public final byte[] a() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte b2 = tb.d.b;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.b).array();
        pq7.b(array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        byteArrayOutputStream.write(a(b2, array));
        byteArrayOutputStream.write(a(tb.e.b, new byte[]{this.c}));
        if (this.d.length() > 0) {
            byte b3 = tb.c.b;
            String a2 = iy1.a(this.d);
            Charset c2 = hd0.y.c();
            if (a2 != null) {
                byte[] bytes = a2.getBytes(c2);
                pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                byteArrayOutputStream.write(a(b3, bytes));
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        short s = this.e;
        if (s != ((short) -1)) {
            byteArrayOutputStream.write(a(tb.g.b, new byte[]{(byte) s}));
        }
        lo1 lo1 = this.f;
        if (lo1 != null) {
            byteArrayOutputStream.write(a(tb.h.b, lo1.a()));
        }
        no1 no1 = this.h;
        if (no1 != null) {
            byteArrayOutputStream.write(a(tb.i.b, new byte[]{no1.a()}));
        }
        cv1 cv1 = this.g;
        if (cv1 != null) {
            byteArrayOutputStream.write(a(tb.f.b, cv1.b()));
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byte[] array2 = ByteBuffer.allocate(byteArray.length + 2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) byteArray.length).put(byteArray).array();
        pq7.b(array2, "ByteBuffer\n             \u2026\n                .array()");
        return array2;
    }

    @DexIgnore
    public final byte[] a(byte b2, byte[] bArr) {
        if (bArr.length + 2 > 100) {
            return new byte[0];
        }
        byte[] array = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN).put(b2).put((byte) bArr.length).put(bArr).array();
        pq7.b(array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(jo1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            jo1 jo1 = (jo1) obj;
            return this.b == jo1.b && this.c == jo1.c && !(pq7.a(this.d, jo1.d) ^ true) && this.e == jo1.e && !(pq7.a(this.f, jo1.f) ^ true) && this.h == jo1.h && !(pq7.a(this.g, jo1.g) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.b;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.i;
    }

    @DexIgnore
    public final lo1 getHandMovingConfig() {
        return this.f;
    }

    @DexIgnore
    public final cv1 getIconConfig() {
        return this.g;
    }

    @DexIgnore
    public final short getPriority() {
        return this.e;
    }

    @DexIgnore
    public final String getSender() {
        return this.d;
    }

    @DexIgnore
    public final no1 getVibePattern() {
        return this.h;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((Long.valueOf(this.b).hashCode() * 31) + this.c) * 31) + this.d.hashCode()) * 31) + this.e;
        lo1 lo1 = this.f;
        if (lo1 != null) {
            hashCode = (hashCode * 31) + lo1.hashCode();
        }
        no1 no1 = this.h;
        if (no1 != null) {
            hashCode = (hashCode * 31) + no1.hashCode();
        }
        cv1 cv1 = this.g;
        return cv1 != null ? (hashCode * 31) + cv1.hashCode() : hashCode;
    }

    @DexIgnore
    public final jo1 setHandMovingConfig(lo1 lo1) {
        this.f = lo1;
        return this;
    }

    @DexIgnore
    public final jo1 setIconConfig(cv1 cv1) {
        this.g = cv1;
        return this;
    }

    @DexIgnore
    public final jo1 setPriority(short s) {
        m10setPriority(s);
        return this;
    }

    @DexIgnore
    /* renamed from: setPriority  reason: collision with other method in class */
    public final void m10setPriority(short s) {
        short d2 = hy1.d(fq7.f1179a);
        short c2 = hy1.c(fq7.f1179a);
        if (d2 > s || c2 < s) {
            s = -1;
        }
        this.e = (short) s;
    }

    @DexIgnore
    public final jo1 setSender(String str) {
        this.d = iy1.e(str, 97, null, null, 6, null);
        return this;
    }

    @DexIgnore
    /* renamed from: setSender  reason: collision with other method in class */
    public final void m11setSender(String str) {
        this.d = iy1.e(str, 97, null, null, 6, null);
    }

    @DexIgnore
    public final jo1 setVibePatternConfig(no1 no1) {
        this.h = no1;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = null;
        JSONObject jSONObject2 = new JSONObject();
        try {
            g80.k(jSONObject2, jd0.X2, this.i);
            g80.k(jSONObject2, jd0.W2, hy1.k((int) this.b, null, 1, null));
            g80.k(jSONObject2, jd0.T, Byte.valueOf(this.c));
            g80.k(jSONObject2, jd0.i, this.d);
            g80.k(jSONObject2, jd0.Q, Short.valueOf(this.e));
            jd0 jd0 = jd0.B2;
            lo1 lo1 = this.f;
            g80.k(jSONObject2, jd0, lo1 != null ? lo1.toJSONObject() : null);
            jd0 jd02 = jd0.C2;
            no1 no1 = this.h;
            g80.k(jSONObject2, jd02, no1 != null ? Byte.valueOf(no1.a()) : null);
            jd0 jd03 = jd0.b3;
            cv1 cv1 = this.g;
            if (cv1 != null) {
                jSONObject = cv1.toJSONObject();
            }
            g80.k(jSONObject2, jd03, jSONObject);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject2;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.i);
        }
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.f, i2);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.h);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.g, i2);
        }
    }
}
