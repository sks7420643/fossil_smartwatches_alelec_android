package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mq1 extends X90 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ WorkoutType e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Mq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Mq1 createFromParcel(Parcel parcel) {
            return new Mq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Mq1[] newArray(int i) {
            return new Mq1[i];
        }
    }

    @DexIgnore
    public Mq1(byte b, int i, WorkoutType workoutType, long j, boolean z) {
        super(E90.WORKOUT_RESUME, b, i);
        this.e = workoutType;
        this.f = j;
        this.g = z;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Mq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        boolean z = true;
        this.e = G80.d(parcel.readByte());
        this.f = parcel.readLong();
        this.g = parcel.readInt() != 1 ? false : z;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Mq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Mq1 mq1 = (Mq1) obj;
            if (this.e != mq1.e) {
                return false;
            }
            if (this.f != mq1.f) {
                return false;
            }
            return this.g == mq1.g;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.ResumeWorkoutRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.f;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.e.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Long.valueOf(this.f).hashCode()) * 31) + Boolean.valueOf(this.g).hashCode();
    }

    @DexIgnore
    public final boolean isRequiredGPS() {
        return this.g;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(super.toJSONObject(), Jd0.G5, Ey1.a(this.e)), Jd0.H5, Long.valueOf(this.f)), Jd0.F5, Boolean.valueOf(this.g));
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.e.getValue());
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(this.g ? 1 : 0);
        }
    }
}
