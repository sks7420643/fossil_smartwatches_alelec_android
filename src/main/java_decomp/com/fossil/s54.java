package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.LinkedList;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S54 {
    @DexIgnore
    public /* final */ Readable a;
    @DexIgnore
    public /* final */ Reader b;
    @DexIgnore
    public /* final */ CharBuffer c;
    @DexIgnore
    public /* final */ char[] d;
    @DexIgnore
    public /* final */ Queue<String> e; // = new LinkedList();
    @DexIgnore
    public /* final */ Q54 f; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Q54 {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Q54
        public void d(String str, String str2) {
            S54.this.e.add(str);
        }
    }

    @DexIgnore
    public S54(Readable readable) {
        CharBuffer a2 = M54.a();
        this.c = a2;
        this.d = a2.array();
        I14.l(readable);
        this.a = readable;
        this.b = readable instanceof Reader ? (Reader) readable : null;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public String b() throws IOException {
        int read;
        while (true) {
            if (this.e.peek() != null) {
                break;
            }
            this.c.clear();
            Reader reader = this.b;
            if (reader != null) {
                char[] cArr = this.d;
                read = reader.read(cArr, 0, cArr.length);
            } else {
                read = this.a.read(this.c);
            }
            if (read == -1) {
                this.f.b();
                break;
            }
            this.f.a(this.d, 0, read);
        }
        return this.e.poll();
    }
}
