package com.fossil;

import com.fossil.Zk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class W60 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Zk1.Bi.values().length];
        a = iArr;
        iArr[Zk1.Bi.SERIAL_NUMBER.ordinal()] = 1;
        a[Zk1.Bi.HARDWARE_REVISION.ordinal()] = 2;
        a[Zk1.Bi.FIRMWARE_VERSION.ordinal()] = 3;
        a[Zk1.Bi.MODEL_NUMBER.ordinal()] = 4;
        a[Zk1.Bi.HEART_RATE_SERIAL_NUMBER.ordinal()] = 5;
        a[Zk1.Bi.BOOTLOADER_VERSION.ordinal()] = 6;
        a[Zk1.Bi.WATCH_APP_VERSION.ordinal()] = 7;
        a[Zk1.Bi.FONT_VERSION.ordinal()] = 8;
        a[Zk1.Bi.LUTS_VERSION.ordinal()] = 9;
        a[Zk1.Bi.SUPPORTED_FILES_VERSION.ordinal()] = 10;
        a[Zk1.Bi.CURRENT_FILES_VERSION.ordinal()] = 11;
        a[Zk1.Bi.BOND_REQUIREMENT.ordinal()] = 12;
        a[Zk1.Bi.SUPPORTED_DEVICE_CONFIGS.ordinal()] = 13;
        a[Zk1.Bi.DEVICE_SECURITY_VERSION.ordinal()] = 14;
        a[Zk1.Bi.SOCKET_INFO.ordinal()] = 15;
        a[Zk1.Bi.LOCALE.ordinal()] = 16;
        a[Zk1.Bi.LOCALE_VERSION.ordinal()] = 17;
        a[Zk1.Bi.MICRO_APP_SYSTEM_VERSION.ordinal()] = 18;
        a[Zk1.Bi.UNKNOWN.ordinal()] = 19;
    }
    */
}
