package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W93 extends Ss2 implements U93 {
    @DexIgnore
    public W93(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    @Override // com.fossil.U93
    public final void c(Bundle bundle) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, bundle);
        i(1, d);
    }
}
