package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilNotificationImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Le5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageButton s;
    @DexIgnore
    public /* final */ FossilNotificationImageView t;
    @DexIgnore
    public /* final */ View u;

    @DexIgnore
    public Le5(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, ImageButton imageButton, FossilNotificationImageView fossilNotificationImageView, View view2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = imageButton;
        this.t = fossilNotificationImageView;
        this.u = view2;
    }

    @DexIgnore
    @Deprecated
    public static Le5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Le5) ViewDataBinding.p(layoutInflater, 2131558686, viewGroup, z, obj);
    }

    @DexIgnore
    public static Le5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
