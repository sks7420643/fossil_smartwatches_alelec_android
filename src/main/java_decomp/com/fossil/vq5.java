package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vq5 implements Factory<Uq5> {
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Vq5(Provider<AuthApiGuestService> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Vq5 a(Provider<AuthApiGuestService> provider, Provider<An4> provider2) {
        return new Vq5(provider, provider2);
    }

    @DexIgnore
    public static Uq5 c(AuthApiGuestService authApiGuestService, An4 an4) {
        return new Uq5(authApiGuestService, an4);
    }

    @DexIgnore
    public Uq5 b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
