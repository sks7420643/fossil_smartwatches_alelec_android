package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mapped.Jh6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R26 extends U47 implements Q26 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public /* final */ Zp0 k; // = new Sr4(this);
    @DexIgnore
    public G37<Z55> l;
    @DexIgnore
    public P26 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return R26.t;
        }

        @DexIgnore
        public final R26 b() {
            return new R26();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Z55 a;
        @DexIgnore
        public /* final */ /* synthetic */ R26 b;

        @DexIgnore
        public Bi(Z55 z55, R26 r26, boolean z) {
            this.a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            P26 A6 = R26.A6(this.b);
            NumberPicker numberPicker2 = this.a.t;
            Wg6.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.a.u;
            Wg6.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(i2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Z55 a;
        @DexIgnore
        public /* final */ /* synthetic */ R26 b;

        @DexIgnore
        public Ci(Z55 z55, R26 r26, boolean z) {
            this.a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            P26 A6 = R26.A6(this.b);
            NumberPicker numberPicker2 = this.a.v;
            Wg6.b(numberPicker2, "binding.numberPickerTwo");
            A6.p(String.valueOf(i2), String.valueOf(numberPicker2.getValue()), false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Z55 a;
        @DexIgnore
        public /* final */ /* synthetic */ R26 b;

        @DexIgnore
        public Di(Z55 z55, R26 r26, boolean z) {
            this.a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            P26 A6 = R26.A6(this.b);
            NumberPicker numberPicker2 = this.a.v;
            Wg6.b(numberPicker2, "binding.numberPickerTwo");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.a.u;
            Wg6.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(i2), String.valueOf(value), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Z55 a;
        @DexIgnore
        public /* final */ /* synthetic */ R26 b;

        @DexIgnore
        public Ei(Z55 z55, R26 r26, boolean z) {
            this.a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            P26 A6 = R26.A6(this.b);
            NumberPicker numberPicker2 = this.a.t;
            Wg6.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.a.v;
            Wg6.b(numberPicker3, "binding.numberPickerTwo");
            int value2 = numberPicker3.getValue();
            if (i2 != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(value2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ R26 b;

        @DexIgnore
        public Fi(R26 r26) {
            this.b = r26;
        }

        @DexIgnore
        public final void onClick(View view) {
            R26.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z55 b;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 c;

        @DexIgnore
        public Gi(Z55 z55, Jh6 jh6) {
            this.b = z55;
            this.c = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.b.w;
            Wg6.b(constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).f();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.E(3);
                        Z55 z55 = this.b;
                        Wg6.b(z55, "it");
                        View n = z55.n();
                        Wg6.b(n, "it.root");
                        n.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new Rc6("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = R26.class.getSimpleName();
        Wg6.b(simpleName, "InactivityNudgeTimeFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ P26 A6(R26 r26) {
        P26 p26 = r26.m;
        if (p26 != null) {
            return p26;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void B5(boolean z) {
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                Wg6.b(numberPicker, "binding.numberPickerTwo");
                numberPicker.setMinValue(0);
                NumberPicker numberPicker2 = a2.v;
                Wg6.b(numberPicker2, "binding.numberPickerTwo");
                numberPicker2.setMaxValue(59);
                a2.v.setOnValueChangedListener(new Bi(a2, this, z));
                if (z) {
                    NumberPicker numberPicker3 = a2.t;
                    Wg6.b(numberPicker3, "binding.numberPickerOne");
                    numberPicker3.setMinValue(0);
                    NumberPicker numberPicker4 = a2.t;
                    Wg6.b(numberPicker4, "binding.numberPickerOne");
                    numberPicker4.setMaxValue(23);
                    a2.t.setOnValueChangedListener(new Ci(a2, this, z));
                    NumberPicker numberPicker5 = a2.u;
                    Wg6.b(numberPicker5, "binding.numberPickerThree");
                    numberPicker5.setVisibility(8);
                    return;
                }
                NumberPicker numberPicker6 = a2.t;
                Wg6.b(numberPicker6, "binding.numberPickerOne");
                numberPicker6.setMinValue(1);
                NumberPicker numberPicker7 = a2.t;
                Wg6.b(numberPicker7, "binding.numberPickerOne");
                numberPicker7.setMaxValue(12);
                a2.t.setOnValueChangedListener(new Di(a2, this, z));
                String c = Um5.c(PortfolioApp.get.instance(), 2131886102);
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886104);
                NumberPicker numberPicker8 = a2.u;
                Wg6.b(numberPicker8, "binding.numberPickerThree");
                numberPicker8.setVisibility(0);
                NumberPicker numberPicker9 = a2.u;
                Wg6.b(numberPicker9, "binding.numberPickerThree");
                numberPicker9.setMinValue(0);
                NumberPicker numberPicker10 = a2.u;
                Wg6.b(numberPicker10, "binding.numberPickerThree");
                numberPicker10.setMaxValue(1);
                a2.u.setDisplayedValues(new String[]{c, c2});
                a2.u.setOnValueChangedListener(new Ei(a2, this, z));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void C6(int i) {
        P26 p26 = this.m;
        if (p26 != null) {
            p26.o(i);
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void D6(P26 p26) {
        Wg6.c(p26, "presenter");
        this.m = p26;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(P26 p26) {
        D6(p26);
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void Y0(String str) {
        Wg6.c(str, "description");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager requireFragmentManager = requireFragmentManager();
            Wg6.b(requireFragmentManager, "requireFragmentManager()");
            s37.q(requireFragmentManager, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void h6(int i, boolean z) {
        int i2 = 0;
        int i3 = i / 60;
        if (!z) {
            if (i3 >= 12) {
                i2 = 1;
                i3 -= 12;
            }
            if (i3 == 0) {
                i3 = 12;
            }
        }
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                Wg6.b(numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.v;
                Wg6.b(numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i % 60);
                NumberPicker numberPicker3 = a2.u;
                Wg6.b(numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Z55 z55 = (Z55) Aq0.f(layoutInflater, 2131558550, viewGroup, false, this.k);
        String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d)) {
            z55.w.setBackgroundColor(Color.parseColor(d));
        }
        z55.q.setOnClickListener(new Fi(this));
        this.l = new G37<>(this, z55);
        Wg6.b(z55, "binding");
        return z55.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        P26 p26 = this.m;
        if (p26 != null) {
            p26.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        P26 p26 = this.m;
        if (p26 != null) {
            p26.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null) {
                Jh6 jh6 = new Jh6();
                jh6.element = null;
                jh6.element = (T) new Gi(a2, jh6);
                Wg6.b(a2, "it");
                View n = a2.n();
                Wg6.b(n, "it.root");
                n.getViewTreeObserver().addOnGlobalLayoutListener(jh6.element);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void t(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "title");
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
