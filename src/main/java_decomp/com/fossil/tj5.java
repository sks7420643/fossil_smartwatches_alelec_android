package com.fossil;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tj5 {
    @DexIgnore
    public static Wj5 a(Context context) {
        return (Wj5) Oa1.t(context);
    }

    @DexIgnore
    public static Wj5 b(View view) {
        return (Wj5) Oa1.u(view);
    }

    @DexIgnore
    public static Wj5 c(Fragment fragment) {
        return (Wj5) Oa1.v(fragment);
    }
}
