package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ UUID[] d;
    @DexIgnore
    public /* final */ /* synthetic */ N6[] e;

    @DexIgnore
    public S4(K5 k5, G7 g7, UUID[] uuidArr, N6[] n6Arr) {
        this.b = k5;
        this.c = g7;
        this.d = uuidArr;
        this.e = n6Arr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.g.f();
        this.b.z.g.c(new C7(this.c, this.d, this.e));
    }
}
