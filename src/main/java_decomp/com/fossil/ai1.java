package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.fossil.Yh1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ai1 implements Yh1 {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Yh1.Ai c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ BroadcastReceiver f; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends BroadcastReceiver {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Ai1 ai1 = Ai1.this;
            boolean z = ai1.d;
            ai1.d = ai1.c(context);
            if (z != Ai1.this.d) {
                if (Log.isLoggable("ConnectivityMonitor", 3)) {
                    Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + Ai1.this.d);
                }
                Ai1 ai12 = Ai1.this;
                ai12.c.a(ai12.d);
            }
        }
    }

    @DexIgnore
    public Ai1(Context context, Yh1.Ai ai) {
        this.b = context.getApplicationContext();
        this.c = ai;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public boolean c(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        Ik1.d(connectivityManager);
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (RuntimeException e2) {
            if (Log.isLoggable("ConnectivityMonitor", 5)) {
                Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e2);
            }
            return true;
        }
    }

    @DexIgnore
    public final void e() {
        if (!this.e) {
            this.d = c(this.b);
            try {
                this.b.registerReceiver(this.f, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.e = true;
            } catch (SecurityException e2) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register", e2);
                }
            }
        }
    }

    @DexIgnore
    public final void g() {
        if (this.e) {
            this.b.unregisterReceiver(this.f);
            this.e = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStart() {
        e();
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStop() {
        g();
    }
}
