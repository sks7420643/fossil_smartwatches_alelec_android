package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt7 implements Serializable {
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public Set<? extends mt7> _options;
    @DexIgnore
    public /* final */ Pattern nativePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final int b(int i) {
            return (i & 2) != 0 ? i | 64 : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Serializable {
        @DexIgnore
        public static /* final */ a Companion; // = new a(null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int flags;
        @DexIgnore
        public /* final */ String pattern;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(kq7 kq7) {
                this();
            }
        }

        @DexIgnore
        public b(String str, int i) {
            pq7.c(str, "pattern");
            this.pattern = str;
            this.flags = i;
        }

        @DexIgnore
        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.pattern, this.flags);
            pq7.b(compile, "Pattern.compile(pattern, flags)");
            return new jt7(compile);
        }

        @DexIgnore
        public final int getFlags() {
            return this.flags;
        }

        @DexIgnore
        public final String getPattern() {
            return this.pattern;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements gp7<ht7> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $input;
        @DexIgnore
        public /* final */ /* synthetic */ int $startIndex;
        @DexIgnore
        public /* final */ /* synthetic */ jt7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(jt7 jt7, CharSequence charSequence, int i) {
            super(0);
            this.this$0 = jt7;
            this.$input = charSequence;
            this.$startIndex = i;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final ht7 invoke() {
            throw null;
            //return this.this$0.find(this.$input, this.$startIndex);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class d extends nq7 implements rp7<ht7, ht7> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        @Override // com.fossil.gq7, com.fossil.ds7
        public final String getName() {
            return "next";
        }

        @DexIgnore
        @Override // com.fossil.gq7
        public final fs7 getOwner() {
            return er7.b(ht7.class);
        }

        @DexIgnore
        @Override // com.fossil.gq7
        public final String getSignature() {
            return "next()Lkotlin/text/MatchResult;";
        }

        @DexIgnore
        public final ht7 invoke(ht7 ht7) {
            pq7.c(ht7, "p1");
            return ht7.next();
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public jt7(java.lang.String r3) {
        /*
            r2 = this;
            java.lang.String r0 = "pattern"
            com.fossil.pq7.c(r3, r0)
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r3)
            java.lang.String r1 = "Pattern.compile(pattern)"
            com.fossil.pq7.b(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt7.<init>(java.lang.String):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public jt7(java.lang.String r3, com.fossil.mt7 r4) {
        /*
            r2 = this;
            java.lang.String r0 = "pattern"
            com.fossil.pq7.c(r3, r0)
            java.lang.String r0 = "option"
            com.fossil.pq7.c(r4, r0)
            com.fossil.jt7$a r0 = com.fossil.jt7.Companion
            int r1 = r4.getValue()
            int r0 = com.fossil.jt7.a.a(r0, r1)
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r3, r0)
            java.lang.String r1 = "Pattern.compile(pattern,\u2026nicodeCase(option.value))"
            com.fossil.pq7.b(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt7.<init>(java.lang.String, com.fossil.mt7):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public jt7(java.lang.String r3, java.util.Set<? extends com.fossil.mt7> r4) {
        /*
            r2 = this;
            java.lang.String r0 = "pattern"
            com.fossil.pq7.c(r3, r0)
            java.lang.String r0 = "options"
            com.fossil.pq7.c(r4, r0)
            com.fossil.jt7$a r0 = com.fossil.jt7.Companion
            int r1 = com.fossil.lt7.d(r4)
            int r0 = com.fossil.jt7.a.a(r0, r1)
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r3, r0)
            java.lang.String r1 = "Pattern.compile(pattern,\u2026odeCase(options.toInt()))"
            com.fossil.pq7.b(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt7.<init>(java.lang.String, java.util.Set):void");
    }

    @DexIgnore
    public jt7(Pattern pattern) {
        pq7.c(pattern, "nativePattern");
        this.nativePattern = pattern;
    }

    @DexIgnore
    public static /* synthetic */ ht7 find$default(jt7 jt7, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return jt7.find(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ ts7 findAll$default(jt7 jt7, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return jt7.findAll(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ List split$default(jt7 jt7, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return jt7.split(charSequence, i);
    }

    @DexIgnore
    private final Object writeReplace() {
        String pattern = this.nativePattern.pattern();
        pq7.b(pattern, "nativePattern.pattern()");
        return new b(pattern, this.nativePattern.flags());
    }

    @DexIgnore
    public final boolean containsMatchIn(CharSequence charSequence) {
        pq7.c(charSequence, "input");
        return this.nativePattern.matcher(charSequence).find();
    }

    @DexIgnore
    public final ht7 find(CharSequence charSequence, int i) {
        pq7.c(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        pq7.b(matcher, "nativePattern.matcher(input)");
        return lt7.e(matcher, i, charSequence);
    }

    @DexIgnore
    public final ts7<ht7> findAll(CharSequence charSequence, int i) {
        pq7.c(charSequence, "input");
        return ys7.d(new c(this, charSequence, i), d.INSTANCE);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.Set<? extends com.fossil.mt7>, java.util.Set<com.fossil.mt7> */
    public final Set<mt7> getOptions() {
        Set set = this._options;
        if (set != null) {
            return set;
        }
        int flags = this.nativePattern.flags();
        EnumSet allOf = EnumSet.allOf(mt7.class);
        mm7.x(allOf, new kt7(flags));
        Set<mt7> unmodifiableSet = Collections.unmodifiableSet(allOf);
        pq7.b(unmodifiableSet, "Collections.unmodifiable\u2026mask == it.value }\n    })");
        this._options = unmodifiableSet;
        return unmodifiableSet;
    }

    @DexIgnore
    public final String getPattern() {
        String pattern = this.nativePattern.pattern();
        pq7.b(pattern, "nativePattern.pattern()");
        return pattern;
    }

    @DexIgnore
    public final ht7 matchEntire(CharSequence charSequence) {
        pq7.c(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        pq7.b(matcher, "nativePattern.matcher(input)");
        return lt7.f(matcher, charSequence);
    }

    @DexIgnore
    public final boolean matches(CharSequence charSequence) {
        pq7.c(charSequence, "input");
        return this.nativePattern.matcher(charSequence).matches();
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, rp7<? super ht7, ? extends CharSequence> rp7) {
        int i = 0;
        pq7.c(charSequence, "input");
        pq7.c(rp7, "transform");
        ht7 find$default = find$default(this, charSequence, 0, 2, null);
        if (find$default == null) {
            return charSequence.toString();
        }
        int length = charSequence.length();
        StringBuilder sb = new StringBuilder(length);
        ht7 ht7 = find$default;
        while (ht7 != null) {
            sb.append(charSequence, i, ht7.a().h().intValue());
            sb.append((CharSequence) rp7.invoke(ht7));
            i = ht7.a().g().intValue() + 1;
            ht7 next = ht7.next();
            if (i >= length || next == null) {
                if (i < length) {
                    sb.append(charSequence, i, length);
                }
                String sb2 = sb.toString();
                pq7.b(sb2, "sb.toString()");
                return sb2;
            }
            ht7 = next;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, String str) {
        pq7.c(charSequence, "input");
        pq7.c(str, "replacement");
        String replaceAll = this.nativePattern.matcher(charSequence).replaceAll(str);
        pq7.b(replaceAll, "nativePattern.matcher(in\u2026).replaceAll(replacement)");
        return replaceAll;
    }

    @DexIgnore
    public final String replaceFirst(CharSequence charSequence, String str) {
        pq7.c(charSequence, "input");
        pq7.c(str, "replacement");
        String replaceFirst = this.nativePattern.matcher(charSequence).replaceFirst(str);
        pq7.b(replaceFirst, "nativePattern.matcher(in\u2026replaceFirst(replacement)");
        return replaceFirst;
    }

    @DexIgnore
    public final List<String> split(CharSequence charSequence, int i) {
        int i2 = 10;
        pq7.c(charSequence, "input");
        if (i >= 0) {
            Matcher matcher = this.nativePattern.matcher(charSequence);
            if (!matcher.find() || i == 1) {
                return gm7.b(charSequence.toString());
            }
            if (i > 0) {
                i2 = bs7.g(i, 10);
            }
            ArrayList arrayList = new ArrayList(i2);
            int i3 = i - 1;
            int i4 = 0;
            do {
                arrayList.add(charSequence.subSequence(i4, matcher.start()).toString());
                i4 = matcher.end();
                if (i3 >= 0 && arrayList.size() == i3) {
                    break;
                }
            } while (matcher.find());
            arrayList.add(charSequence.subSequence(i4, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    @DexIgnore
    public final Pattern toPattern() {
        return this.nativePattern;
    }

    @DexIgnore
    public String toString() {
        String pattern = this.nativePattern.toString();
        pq7.b(pattern, "nativePattern.toString()");
        return pattern;
    }
}
