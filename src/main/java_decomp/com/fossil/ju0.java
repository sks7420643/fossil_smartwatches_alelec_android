package com.fossil;

import com.mapped.V3;
import com.mapped.Xe;
import com.mapped.Ye;
import java.util.IdentityHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ju0<K, A, B> extends Ye<K, B> {
    @DexIgnore
    public /* final */ Ye<K, A> a;
    @DexIgnore
    public /* final */ V3<List<A>, List<B>> b;
    @DexIgnore
    public /* final */ IdentityHashMap<B, K> c; // = new IdentityHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Ye.Ci<A> {
        @DexIgnore
        public /* final */ /* synthetic */ Ye.Ci a;

        @DexIgnore
        public Ai(Ye.Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        @Override // com.mapped.Ye.Ai
        public void a(List<A> list) {
            this.a.a(Ju0.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Ye.Ai<A> {
        @DexIgnore
        public /* final */ /* synthetic */ Ye.Ai a;

        @DexIgnore
        public Bi(Ye.Ai ai) {
            this.a = ai;
        }

        @DexIgnore
        @Override // com.mapped.Ye.Ai
        public void a(List<A> list) {
            this.a.a(Ju0.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Ye.Ai<A> {
        @DexIgnore
        public /* final */ /* synthetic */ Ye.Ai a;

        @DexIgnore
        public Ci(Ye.Ai ai) {
            this.a = ai;
        }

        @DexIgnore
        @Override // com.mapped.Ye.Ai
        public void a(List<A> list) {
            this.a.a(Ju0.this.a(list));
        }
    }

    @DexIgnore
    public Ju0(Ye<K, A> ye, V3<List<A>, List<B>> v3) {
        this.a = ye;
        this.b = v3;
    }

    @DexIgnore
    public List<B> a(List<A> list) {
        List<B> convert = Xe.convert(this.b, list);
        synchronized (this.c) {
            for (int i = 0; i < convert.size(); i++) {
                this.c.put(convert.get(i), this.a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void addInvalidatedCallback(Xe.Ci ci) {
        this.a.addInvalidatedCallback(ci);
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public K getKey(B b2) {
        K k;
        synchronized (this.c) {
            k = this.c.get(b2);
        }
        return k;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadAfter(Ye.Fi<K> fi, Ye.Ai<B> ai) {
        this.a.loadAfter(fi, new Bi(ai));
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadBefore(Ye.Fi<K> fi, Ye.Ai<B> ai) {
        this.a.loadBefore(fi, new Ci(ai));
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadInitial(Ye.Ei<K> ei, Ye.Ci<B> ci) {
        this.a.loadInitial(ei, new Ai(ci));
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void removeInvalidatedCallback(Xe.Ci ci) {
        this.a.removeInvalidatedCallback(ci);
    }
}
