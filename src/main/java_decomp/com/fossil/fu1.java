package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Fu1 {
    NOT_ENCRYPTED((byte) 0),
    AES_CTR((byte) 1),
    AES_CBC_PKCS5((byte) 2),
    XOR((byte) 3);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Fu1 a(byte b) {
            Fu1[] values = Fu1.values();
            for (Fu1 fu1 : values) {
                if (fu1.a() == b) {
                    return fu1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Fu1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
