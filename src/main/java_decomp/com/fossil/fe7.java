package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fe7 extends De7 {
    @DexIgnore
    public /* final */ Map<String, Object> a;
    @DexIgnore
    public /* final */ Ai b; // = new Ai(this);
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Je7 {
        @DexIgnore
        public Object a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Object d;

        @DexIgnore
        public Ai(Fe7 fe7) {
        }

        @DexIgnore
        @Override // com.fossil.Je7
        public void error(String str, String str2, Object obj) {
            this.b = str;
            this.c = str2;
            this.d = obj;
        }

        @DexIgnore
        @Override // com.fossil.Je7
        public void success(Object obj) {
            this.a = obj;
        }
    }

    @DexIgnore
    public Fe7(Map<String, Object> map, boolean z) {
        this.a = map;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.Ie7
    public <T> T a(String str) {
        return (T) this.a.get(str);
    }

    @DexIgnore
    @Override // com.fossil.Ee7, com.fossil.Ie7
    public boolean c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.De7
    public Je7 i() {
        return this.b;
    }

    @DexIgnore
    public String j() {
        return (String) this.a.get("method");
    }

    @DexIgnore
    public Map<String, Object> k() {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        hashMap2.put("code", this.b.b);
        hashMap2.put("message", this.b.c);
        hashMap2.put("data", this.b.d);
        hashMap.put("error", hashMap2);
        return hashMap;
    }

    @DexIgnore
    public Map<String, Object> l() {
        HashMap hashMap = new HashMap();
        hashMap.put(Constants.RESULT, this.b.a);
        return hashMap;
    }

    @DexIgnore
    public void m(MethodChannel.Result result) {
        Ai ai = this.b;
        result.error(ai.b, ai.c, ai.d);
    }

    @DexIgnore
    public void n(List<Map<String, Object>> list) {
        if (!c()) {
            list.add(k());
        }
    }

    @DexIgnore
    public void o(List<Map<String, Object>> list) {
        if (!c()) {
            list.add(l());
        }
    }
}
