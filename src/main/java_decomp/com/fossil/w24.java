package com.fossil;

import com.fossil.A34;
import java.io.Serializable;
import java.lang.Enum;
import java.util.EnumMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W24<K extends Enum<K>, V> extends A34.Ci<K, V> {
    @DexIgnore
    public /* final */ transient EnumMap<K, V> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<K extends Enum<K>, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumMap<K, V> delegate;

        @DexIgnore
        public Bi(EnumMap<K, V> enumMap) {
            this.delegate = enumMap;
        }

        @DexIgnore
        public Object readResolve() {
            return new W24(this.delegate);
        }
    }

    @DexIgnore
    public W24(EnumMap<K, V> enumMap) {
        this.f = enumMap;
        I14.d(!enumMap.isEmpty());
    }

    @DexIgnore
    public static <K extends Enum<K>, V> A34<K, V> asImmutable(EnumMap<K, V> enumMap) {
        int size = enumMap.size();
        if (size == 0) {
            return A34.of();
        }
        if (size != 1) {
            return new W24(enumMap);
        }
        Map.Entry entry = (Map.Entry) O34.f(enumMap.entrySet());
        return A34.of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean containsKey(Object obj) {
        return this.f.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.A34.Ci
    public H54<Map.Entry<K, V>> entryIterator() {
        return X34.r(this.f.entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof W24) {
            obj = ((W24) obj).f;
        }
        return this.f.equals(obj);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34
    public V get(Object obj) {
        return this.f.get(obj);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H54<K> keyIterator() {
        return P34.x(this.f.keySet().iterator());
    }

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    @Override // com.fossil.A34
    public Object writeReplace() {
        return new Bi(this.f);
    }
}
