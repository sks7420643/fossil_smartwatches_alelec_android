package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vz5 implements Factory<Sn6> {
    @DexIgnore
    public static Sn6 a(Oz5 oz5) {
        Sn6 g = oz5.g();
        Lk7.c(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }
}
