package com.fossil;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zw0 implements Executor {
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ ArrayDeque<Runnable> c; // = new ArrayDeque<>();
    @DexIgnore
    public Runnable d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable b;

        @DexIgnore
        public Ai(Runnable runnable) {
            this.b = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.b.run();
            } finally {
                Zw0.this.a();
            }
        }
    }

    @DexIgnore
    public Zw0(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            Runnable poll = this.c.poll();
            this.d = poll;
            if (poll != null) {
                this.b.execute(poll);
            }
        }
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        synchronized (this) {
            this.c.offer(new Ai(runnable));
            if (this.d == null) {
                a();
            }
        }
    }
}
