package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ex4 implements MembersInjector<Dx4> {
    @DexIgnore
    public static void a(Dx4 dx4, Po4 po4) {
        dx4.h = po4;
    }
}
