package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sg0 extends DataSetObservable {
    @DexIgnore
    public static /* final */ String n; // = Sg0.class.getSimpleName();
    @DexIgnore
    public static /* final */ Object o; // = new Object();
    @DexIgnore
    public static /* final */ Map<String, Sg0> p; // = new HashMap();
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ List<Ai> b; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Di> c; // = new ArrayList();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public Intent f;
    @DexIgnore
    public Bi g; // = new Ci();
    @DexIgnore
    public int h; // = 50;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public Ei m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Comparable<Ai> {
        @DexIgnore
        public /* final */ ResolveInfo b;
        @DexIgnore
        public float c;

        @DexIgnore
        public Ai(ResolveInfo resolveInfo) {
            this.b = resolveInfo;
        }

        @DexIgnore
        public int a(Ai ai) {
            return Float.floatToIntBits(ai.c) - Float.floatToIntBits(this.c);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // java.lang.Comparable
        public /* bridge */ /* synthetic */ int compareTo(Ai ai) {
            return a(ai);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (Ai.class != obj.getClass()) {
                return false;
            }
            return Float.floatToIntBits(this.c) == Float.floatToIntBits(((Ai) obj).c);
        }

        @DexIgnore
        public int hashCode() {
            return Float.floatToIntBits(this.c) + 31;
        }

        @DexIgnore
        public String toString() {
            return "[resolveInfo:" + this.b.toString() + "; weight:" + new BigDecimal((double) this.c) + "]";
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(Intent intent, List<Ai> list, List<Di> list2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Bi {
        @DexIgnore
        public /* final */ Map<ComponentName, Ai> a; // = new HashMap();

        @DexIgnore
        @Override // com.fossil.Sg0.Bi
        public void a(Intent intent, List<Ai> list, List<Di> list2) {
            float f;
            Map<ComponentName, Ai> map = this.a;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Ai ai = list.get(i);
                ai.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                ActivityInfo activityInfo = ai.b.activityInfo;
                map.put(new ComponentName(activityInfo.packageName, activityInfo.name), ai);
            }
            float f2 = 1.0f;
            for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
                Di di = list2.get(size2);
                Ai ai2 = map.get(di.a);
                if (ai2 != null) {
                    ai2.c = (di.c * f2) + ai2.c;
                    f = 0.95f * f2;
                } else {
                    f = f2;
                }
                f2 = f;
            }
            Collections.sort(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ ComponentName a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ float c;

        @DexIgnore
        public Di(ComponentName componentName, long j, float f) {
            this.a = componentName;
            this.b = j;
            this.c = f;
        }

        @DexIgnore
        public Di(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (Di.class != obj.getClass()) {
                return false;
            }
            Di di = (Di) obj;
            ComponentName componentName = this.a;
            if (componentName == null) {
                if (di.a != null) {
                    return false;
                }
            } else if (!componentName.equals(di.a)) {
                return false;
            }
            if (this.b != di.b) {
                return false;
            }
            return Float.floatToIntBits(this.c) == Float.floatToIntBits(di.c);
        }

        @DexIgnore
        public int hashCode() {
            ComponentName componentName = this.a;
            int hashCode = componentName == null ? 0 : componentName.hashCode();
            long j = this.b;
            return ((((hashCode + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Float.floatToIntBits(this.c);
        }

        @DexIgnore
        public String toString() {
            return "[; activity:" + this.a + "; time:" + this.b + "; weight:" + new BigDecimal((double) this.c) + "]";
        }
    }

    @DexIgnore
    public interface Ei {
        @DexIgnore
        boolean a(Sg0 sg0, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Fi extends AsyncTask<Object, Void, Void> {
        @DexIgnore
        public Fi() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0077, code lost:
            if (r3 != null) goto L_0x0079;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r3.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x009c, code lost:
            if (r3 == null) goto L_0x007c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00be, code lost:
            if (r3 == null) goto L_0x007c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e0, code lost:
            if (r3 == null) goto L_0x007c;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void a(java.lang.Object... r13) {
            /*
            // Method dump skipped, instructions count: 268
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sg0.Fi.a(java.lang.Object[]):java.lang.Void");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ Void doInBackground(Object[] objArr) {
            return a(objArr);
        }
    }

    @DexIgnore
    public Sg0(Context context, String str) {
        this.d = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.e = str;
            return;
        }
        this.e = str + ".xml";
    }

    @DexIgnore
    public static Sg0 d(Context context, String str) {
        Sg0 sg0;
        synchronized (o) {
            sg0 = p.get(str);
            if (sg0 == null) {
                sg0 = new Sg0(context, str);
                p.put(str, sg0);
            }
        }
        return sg0;
    }

    @DexIgnore
    public final boolean a(Di di) {
        boolean add = this.c.add(di);
        if (add) {
            this.k = true;
            l();
            k();
            r();
            notifyChanged();
        }
        return add;
    }

    @DexIgnore
    public Intent b(int i2) {
        synchronized (this.a) {
            if (this.f == null) {
                return null;
            }
            c();
            Ai ai = this.b.get(i2);
            ComponentName componentName = new ComponentName(ai.b.activityInfo.packageName, ai.b.activityInfo.name);
            Intent intent = new Intent(this.f);
            intent.setComponent(componentName);
            if (this.m != null) {
                if (this.m.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new Di(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    @DexIgnore
    public final void c() {
        boolean j2 = j();
        boolean m2 = m();
        l();
        if (j2 || m2) {
            r();
            notifyChanged();
        }
    }

    @DexIgnore
    public ResolveInfo e(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.a) {
            c();
            resolveInfo = this.b.get(i2).b;
        }
        return resolveInfo;
    }

    @DexIgnore
    public int f() {
        int size;
        synchronized (this.a) {
            c();
            size = this.b.size();
        }
        return size;
    }

    @DexIgnore
    public int g(ResolveInfo resolveInfo) {
        synchronized (this.a) {
            c();
            List<Ai> list = this.b;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (list.get(i2).b == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public ResolveInfo h() {
        synchronized (this.a) {
            c();
            if (this.b.isEmpty()) {
                return null;
            }
            return this.b.get(0).b;
        }
    }

    @DexIgnore
    public int i() {
        int size;
        synchronized (this.a) {
            c();
            size = this.c.size();
        }
        return size;
    }

    @DexIgnore
    public final boolean j() {
        if (!this.l || this.f == null) {
            return false;
        }
        this.l = false;
        this.b.clear();
        List<ResolveInfo> queryIntentActivities = this.d.getPackageManager().queryIntentActivities(this.f, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.b.add(new Ai(queryIntentActivities.get(i2)));
        }
        return true;
    }

    @DexIgnore
    public final void k() {
        if (!this.j) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.k) {
            this.k = false;
            if (!TextUtils.isEmpty(this.e)) {
                new Fi().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ArrayList(this.c), this.e);
            }
        }
    }

    @DexIgnore
    public final void l() {
        int size = this.c.size() - this.h;
        if (size > 0) {
            this.k = true;
            for (int i2 = 0; i2 < size; i2++) {
                this.c.remove(0);
            }
        }
    }

    @DexIgnore
    public final boolean m() {
        if (!this.i || !this.k || TextUtils.isEmpty(this.e)) {
            return false;
        }
        this.i = false;
        this.j = true;
        n();
        return true;
    }

    @DexIgnore
    public final void n() {
        try {
            FileInputStream openFileInput = this.d.openFileInput(this.e);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if ("historical-records".equals(newPullParser.getName())) {
                    List<Di> list = this.c;
                    list.clear();
                    while (true) {
                        int next = newPullParser.next();
                        if (next == 1) {
                            if (openFileInput == null) {
                                return;
                            }
                        } else if (!(next == 3 || next == 4)) {
                            if ("historical-record".equals(newPullParser.getName())) {
                                list.add(new Di(newPullParser.getAttributeValue(null, Constants.ACTIVITY), Long.parseLong(newPullParser.getAttributeValue(null, LogBuilder.KEY_TIME)), Float.parseFloat(newPullParser.getAttributeValue(null, Constants.PROFILE_KEY_UNITS_WEIGHT))));
                            } else {
                                throw new XmlPullParserException("Share records file not well-formed.");
                            }
                        }
                    }
                    try {
                        openFileInput.close();
                    } catch (IOException e2) {
                    }
                } else {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
            } catch (XmlPullParserException e3) {
                String str = n;
                Log.e(str, "Error reading historical recrod file: " + this.e, e3);
                if (openFileInput == null) {
                }
            } catch (IOException e4) {
                String str2 = n;
                Log.e(str2, "Error reading historical recrod file: " + this.e, e4);
                if (openFileInput == null) {
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e5) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e6) {
        }
    }

    @DexIgnore
    public void o(int i2) {
        synchronized (this.a) {
            c();
            Ai ai = this.b.get(i2);
            Ai ai2 = this.b.get(0);
            a(new Di(new ComponentName(ai.b.activityInfo.packageName, ai.b.activityInfo.name), System.currentTimeMillis(), ai2 != null ? (ai2.c - ai.c) + 5.0f : 1.0f));
        }
    }

    @DexIgnore
    public void p(Intent intent) {
        synchronized (this.a) {
            if (this.f != intent) {
                this.f = intent;
                this.l = true;
                c();
            }
        }
    }

    @DexIgnore
    public void q(Ei ei) {
        synchronized (this.a) {
            this.m = ei;
        }
    }

    @DexIgnore
    public final boolean r() {
        if (this.g == null || this.f == null || this.b.isEmpty() || this.c.isEmpty()) {
            return false;
        }
        this.g.a(this.f, this.b, Collections.unmodifiableList(this.c));
        return true;
    }
}
