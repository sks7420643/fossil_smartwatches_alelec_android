package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw4 extends ts0 {
    @DexIgnore
    public static /* final */ String i;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Boolean> f3845a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<yy4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<kz4<List<xs4>>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<kz4<Boolean>, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<String, String>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Stack<String> f; // = new Stack<>();
    @DexIgnore
    public /* final */ zt4 g;
    @DexIgnore
    public /* final */ tt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$addFriendRequestedToLocal$1", f = "BCFindFriendsViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(vw4 vw4, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vw4;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$friend, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                if (this.this$0.g.j(this.$friend.d()) == null) {
                    this.$friend.k(1);
                } else {
                    this.$friend.k(0);
                }
                this.this$0.g.n(this.$friend);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1", f = "BCFindFriendsViewModel.kt", l = {85}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vw4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1$1", f = "BCFindFriendsViewModel.kt", l = {91}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vw4$b$a$a")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1$1$1", f = "BCFindFriendsViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.vw4$b$a$a  reason: collision with other inner class name */
            public static final class C0266a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $friends;
                @DexIgnore
                public /* final */ /* synthetic */ List $players;
                @DexIgnore
                public /* final */ /* synthetic */ List $suggestedFriends;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0266a(a aVar, List list, List list2, List list3, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$players = list;
                    this.$friends = list2;
                    this.$suggestedFriends = list3;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0266a aVar = new C0266a(this.this$0, this.$players, this.$friends, this.$suggestedFriends, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((C0266a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object obj2;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        for (ms4 ms4 : this.$players) {
                            if ((!pq7.a(ms4.d(), PortfolioApp.h0.c().l0())) && (!pq7.a(ms4.p(), ao7.a(true))) && (!pq7.a(ms4.k(), "left_after_start"))) {
                                Iterator it = this.$friends.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        obj2 = null;
                                        break;
                                    }
                                    Object next = it.next();
                                    if (ao7.a(pq7.a(ms4.d(), ((xs4) next).d())).booleanValue()) {
                                        obj2 = next;
                                        break;
                                    }
                                }
                                if (((xs4) obj2) == null) {
                                    this.$suggestedFriends.add(new xs4(ms4.d(), ms4.i(), ms4.c(), ms4.e(), null, ms4.g(), true, 0, 1));
                                }
                            }
                        }
                        this.this$0.this$0.this$0.c.l(new kz4(this.$suggestedFriends, null, 2, null));
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                List<ms4> x;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    bt4 z = this.this$0.this$0.h.z(this.this$0.$historyId);
                    List<xs4> k = this.this$0.this$0.g.k();
                    ArrayList arrayList = new ArrayList();
                    if (z == null || (x = z.k()) == null) {
                        x = this.this$0.this$0.h.x();
                    }
                    dv7 a2 = bw7.a();
                    C0266a aVar = new C0266a(this, x, k, arrayList, null);
                    this.L$0 = iv7;
                    this.L$1 = z;
                    this.L$2 = k;
                    this.L$3 = arrayList;
                    this.L$4 = x;
                    this.label = 1;
                    if (eu7.g(a2, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    List list = (List) this.L$4;
                    List list2 = (List) this.L$3;
                    List list3 = (List) this.L$2;
                    bt4 bt4 = (bt4) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vw4 vw4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vw4;
            this.$historyId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$historyId, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.f3845a.l(ao7.a(true));
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f3845a.l(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$searchPeople$1", f = "BCFindFriendsViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $keyword;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vw4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$searchPeople$1$result$1", f = "BCFindFriendsViewModel.kt", l = {60}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<List<xs4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<List<xs4>>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    zt4 zt4 = this.this$0.this$0.g;
                    String str = this.this$0.$keyword;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object q = zt4.q(str, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(vw4 vw4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vw4;
            this.$keyword = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$keyword, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = vw4.i;
                local.e(str, "searchPeople " + this.$keyword);
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) g;
            if (this.this$0.f.contains(this.$keyword)) {
                this.this$0.c.l(kz4);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$sendFriendRequest$1", f = "BCFindFriendsViewModel.kt", l = {121}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public /* final */ /* synthetic */ int $position;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vw4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$sendFriendRequest$1$result$1", f = "BCFindFriendsViewModel.kt", l = {122}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<Boolean>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<Boolean>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    zt4 zt4 = this.this$0.this$0.g;
                    xs4 xs4 = this.this$0.$friend;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object r = zt4.r(xs4, this);
                    return r == d ? d : r;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(vw4 vw4, xs4 xs4, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vw4;
            this.$friend = xs4;
            this.$position = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$friend, this.$position, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                FLogger.INSTANCE.getLocal().d(vw4.i, "sendFriendRequest");
                this.this$0.f3845a.l(ao7.a(true));
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) g;
            this.this$0.f3845a.l(ao7.a(false));
            if (kz4.c() != null && ((Boolean) kz4.c()).booleanValue()) {
                this.this$0.e.l(hl7.a(PortfolioApp.h0.c().l0(), this.$friend.d()));
                this.this$0.j(this.$friend);
            }
            this.this$0.d.l(hl7.a(kz4, ao7.e(this.$position)));
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = vw4.class.getSimpleName();
        pq7.b(simpleName, "BCFindFriendsViewModel::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public vw4(zt4 zt4, tt4 tt4) {
        pq7.c(zt4, "mSocialFriendRepository");
        pq7.c(tt4, "challengeRepository");
        this.g = zt4;
        this.h = tt4;
    }

    @DexIgnore
    public final void j(xs4 xs4) {
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new a(this, xs4, null), 2, null);
    }

    @DexIgnore
    public final void k() {
        if (!this.f.isEmpty()) {
            this.f.pop();
        }
    }

    @DexIgnore
    public final LiveData<cl7<String, String>> l() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> m() {
        return this.f3845a;
    }

    @DexIgnore
    public final LiveData<kz4<List<xs4>>> n() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<yy4> o() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<cl7<kz4<Boolean>, Integer>> p() {
        return this.d;
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, "historyId");
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    public final void r() {
        k();
        this.b.l(yy4.CANCEL);
    }

    @DexIgnore
    public final void s() {
        this.b.l(yy4.SEARCH);
    }

    @DexIgnore
    public final void t() {
        this.b.l(yy4.TYPING);
    }

    @DexIgnore
    public final void u(String str) {
        pq7.c(str, "keyword");
        k();
        this.f.push(str);
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void v(xs4 xs4, int i2) {
        pq7.c(xs4, "friend");
        xw7 unused = gu7.d(us0.a(this), null, null, new d(this, xs4, i2, null), 3, null);
    }
}
