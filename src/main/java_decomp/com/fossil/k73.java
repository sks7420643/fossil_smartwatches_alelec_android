package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K73 implements H73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.ga.ga_app_id", false);

    @DexIgnore
    @Override // com.fossil.H73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.H73
    public final boolean zzb() {
        return a.o().booleanValue();
    }
}
