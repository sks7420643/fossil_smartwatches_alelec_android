package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Sv {
    c((byte) 7),
    d((byte) 11),
    e((byte) 13),
    f(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    g(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    h(DateTimeFieldType.MINUTE_OF_DAY),
    i(DateTimeFieldType.SECOND_OF_DAY),
    j((byte) 5),
    k((byte) 1),
    l((byte) 3);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Sv(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.b + 1);
    }
}
