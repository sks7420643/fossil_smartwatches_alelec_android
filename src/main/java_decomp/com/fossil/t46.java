package com.fossil;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Jh6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.view.NotificationSummaryDialView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T46 extends BaseFragment implements S46 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public R46 g;
    @DexIgnore
    public G37<D95> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return T46.j;
        }

        @DexIgnore
        public final T46 b() {
            return new T46();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ T46 b;

        @DexIgnore
        public Bi(T46 t46) {
            this.b = t46;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements NotificationSummaryDialView.a {
        @DexIgnore
        public /* final */ /* synthetic */ T46 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(T46 t46) {
            this.a = t46;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NotificationSummaryDialView.a
        public void a(int i) {
            NotificationContactsAndAppsAssignedActivity.B.a(this.a, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ D95 b;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 c;

        @DexIgnore
        public Di(D95 d95, Jh6 jh6) {
            this.b = d95;
            this.c = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            this.b.v.requestLayout();
            NotificationSummaryDialView notificationSummaryDialView = this.b.v;
            Wg6.b(notificationSummaryDialView, "binding.nsdv");
            notificationSummaryDialView.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
        }
    }

    /*
    static {
        String simpleName = T46.class.getSimpleName();
        Wg6.b(simpleName, "NotificationDialLandingF\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public void L6(R46 r46) {
        Wg6.c(r46, "presenter");
        this.g = r46;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(R46 r46) {
        L6(r46);
    }

    @DexIgnore
    @Override // com.fossil.S46
    public void k5(SparseArray<List<BaseFeatureModel>> sparseArray) {
        NotificationSummaryDialView notificationSummaryDialView;
        Wg6.c(sparseArray, "data");
        G37<D95> g37 = this.h;
        if (g37 != null) {
            D95 a2 = g37.a();
            if (a2 != null && (notificationSummaryDialView = a2.v) != null) {
                notificationSummaryDialView.setDataAsync(sparseArray);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        D95 d95 = (D95) Aq0.f(layoutInflater, 2131558592, viewGroup, false, A6());
        d95.u.setOnClickListener(new Bi(this));
        d95.v.setOnItemClickListener(new Ci(this));
        Jh6 jh6 = new Jh6();
        jh6.element = null;
        jh6.element = (T) new Di(d95, jh6);
        NotificationSummaryDialView notificationSummaryDialView = d95.v;
        Wg6.b(notificationSummaryDialView, "binding.nsdv");
        notificationSummaryDialView.getViewTreeObserver().addOnGlobalLayoutListener(jh6.element);
        this.h = new G37<>(this, d95);
        Wg6.b(d95, "binding");
        return d95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        R46 r46 = this.g;
        if (r46 != null) {
            r46.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        R46 r46 = this.g;
        if (r46 != null) {
            r46.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
