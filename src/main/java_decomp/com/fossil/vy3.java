package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vy3 implements Zy3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ExtendedFloatingActionButton b;
    @DexIgnore
    public /* final */ ArrayList<Animator.AnimatorListener> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Uy3 d;
    @DexIgnore
    public Bx3 e;
    @DexIgnore
    public Bx3 f;

    @DexIgnore
    public Vy3(ExtendedFloatingActionButton extendedFloatingActionButton, Uy3 uy3) {
        this.b = extendedFloatingActionButton;
        this.a = extendedFloatingActionButton.getContext();
        this.d = uy3;
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public void a() {
        this.d.b();
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public Bx3 d() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public void f() {
        this.d.b();
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public final void g(Bx3 bx3) {
        this.f = bx3;
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public AnimatorSet h() {
        return k(l());
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public final List<Animator.AnimatorListener> i() {
        return this.c;
    }

    @DexIgnore
    public AnimatorSet k(Bx3 bx3) {
        ArrayList arrayList = new ArrayList();
        if (bx3.j("opacity")) {
            arrayList.add(bx3.f("opacity", this.b, View.ALPHA));
        }
        if (bx3.j("scale")) {
            arrayList.add(bx3.f("scale", this.b, View.SCALE_Y));
            arrayList.add(bx3.f("scale", this.b, View.SCALE_X));
        }
        if (bx3.j("width")) {
            arrayList.add(bx3.f("width", this.b, ExtendedFloatingActionButton.H));
        }
        if (bx3.j("height")) {
            arrayList.add(bx3.f("height", this.b, ExtendedFloatingActionButton.I));
        }
        AnimatorSet animatorSet = new AnimatorSet();
        Vw3.a(animatorSet, arrayList);
        return animatorSet;
    }

    @DexIgnore
    public final Bx3 l() {
        Bx3 bx3 = this.f;
        if (bx3 != null) {
            return bx3;
        }
        if (this.e == null) {
            this.e = Bx3.d(this.a, b());
        }
        Bx3 bx32 = this.e;
        Pn0.d(bx32);
        return bx32;
    }

    @DexIgnore
    @Override // com.fossil.Zy3
    public void onAnimationStart(Animator animator) {
        this.d.c(animator);
    }
}
