package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bt1 extends At1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Bt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Bt1 a(Parcel parcel) {
            return new Bt1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bt1 createFromParcel(Parcel parcel) {
            return new Bt1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bt1[] newArray(int i) {
            return new Bt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Bt1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Bt1(boolean z) {
        super(G90.d, z);
    }

    @DexIgnore
    @Override // com.fossil.At1
    public /* bridge */ /* synthetic */ boolean a() {
        return isPercentageCircleEnable();
    }

    @DexIgnore
    public boolean isPercentageCircleEnable() {
        return super.a();
    }
}
