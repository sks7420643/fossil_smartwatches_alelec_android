package com.fossil;

import java.util.ArrayDeque;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iu3<TResult> {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public Queue<Hu3<TResult>> b;
    @DexIgnore
    public boolean c;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        r1 = r2.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0 = r2.b.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        if (r0 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001e, code lost:
        r2.c = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0021, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0026, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0027, code lost:
        r0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.Nt3<TResult> r3) {
        /*
            r2 = this;
            java.lang.Object r1 = r2.a
            monitor-enter(r1)
            java.util.Queue<com.fossil.Hu3<TResult>> r0 = r2.b     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x000b
            boolean r0 = r2.c     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
        L_0x000c:
            return
        L_0x000d:
            r0 = 1
            r2.c = r0     // Catch:{ all -> 0x002b }
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
        L_0x0011:
            java.lang.Object r1 = r2.a
            monitor-enter(r1)
            java.util.Queue<com.fossil.Hu3<TResult>> r0 = r2.b     // Catch:{ all -> 0x0023 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0023 }
            com.fossil.Hu3 r0 = (com.fossil.Hu3) r0     // Catch:{ all -> 0x0023 }
            if (r0 != 0) goto L_0x0026
            r0 = 0
            r2.c = r0     // Catch:{ all -> 0x0023 }
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x000c
        L_0x0023:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            throw r0
        L_0x0026:
            monitor-exit(r1)
            r0.a(r3)
            goto L_0x0011
        L_0x002b:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iu3.a(com.fossil.Nt3):void");
    }

    @DexIgnore
    public final void b(Hu3<TResult> hu3) {
        synchronized (this.a) {
            if (this.b == null) {
                this.b = new ArrayDeque();
            }
            this.b.add(hu3);
        }
    }
}
