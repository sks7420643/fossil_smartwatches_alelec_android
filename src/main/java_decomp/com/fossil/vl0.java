package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.Kl0;
import com.fossil.Zm0;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vl0 extends Tl0 {
    @DexIgnore
    public /* final */ Class<?> g;
    @DexIgnore
    public /* final */ Constructor<?> h;
    @DexIgnore
    public /* final */ Method i;
    @DexIgnore
    public /* final */ Method j;
    @DexIgnore
    public /* final */ Method k;
    @DexIgnore
    public /* final */ Method l;
    @DexIgnore
    public /* final */ Method m;

    @DexIgnore
    public Vl0() {
        Method method;
        Method method2;
        Method method3;
        Method method4;
        Method method5;
        Constructor<?> constructor;
        Class<?> cls;
        try {
            cls = y();
            constructor = z(cls);
            method5 = v(cls);
            method4 = w(cls);
            method3 = A(cls);
            method2 = u(cls);
            method = x(cls);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e("TypefaceCompatApi26Impl", "Unable to collect necessary methods for class " + e.getClass().getName(), e);
            method = null;
            method2 = null;
            method3 = null;
            method4 = null;
            method5 = null;
            constructor = null;
            cls = null;
        }
        this.g = cls;
        this.h = constructor;
        this.i = method5;
        this.j = method4;
        this.k = method3;
        this.l = method2;
        this.m = method;
    }

    @DexIgnore
    private Object o() {
        try {
            return this.h.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            return null;
        }
    }

    @DexIgnore
    public Method A(Class<?> cls) throws NoSuchMethodException {
        return cls.getMethod("freeze", new Class[0]);
    }

    @DexIgnore
    @Override // com.fossil.Yl0, com.fossil.Tl0
    public Typeface b(Context context, Kl0.Bi bi, Resources resources, int i2) {
        if (!t()) {
            return super.b(context, bi, resources, i2);
        }
        Object o = o();
        if (o == null) {
            return null;
        }
        Kl0.Ci[] a2 = bi.a();
        for (Kl0.Ci ci : a2) {
            if (!q(context, o, ci.a(), ci.c(), ci.e(), ci.f() ? 1 : 0, FontVariationAxis.fromFontVariationSettings(ci.d()))) {
                p(o);
                return null;
            }
        }
        if (!s(o)) {
            return null;
        }
        return l(o);
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:48:0x007a */
    @Override // com.fossil.Yl0, com.fossil.Tl0
    public Typeface c(Context context, CancellationSignal cancellationSignal, Zm0.Fi[] fiArr, int i2) {
        if (fiArr.length < 1) {
            return null;
        }
        if (!t()) {
            Zm0.Fi h2 = h(fiArr, i2);
            try {
                ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(h2.c(), "r", cancellationSignal);
                if (openFileDescriptor == null) {
                    if (openFileDescriptor != null) {
                        openFileDescriptor.close();
                    }
                    return null;
                }
                try {
                    Typeface build = new Typeface.Builder(openFileDescriptor.getFileDescriptor()).setWeight(h2.d()).setItalic(h2.e()).build();
                    if (openFileDescriptor == null) {
                        return build;
                    }
                    openFileDescriptor.close();
                    return build;
                } catch (Throwable th) {
                    th.addSuppressed(th);
                }
            } catch (IOException e) {
                return null;
            }
        } else {
            Map<Uri, ByteBuffer> i3 = Zm0.i(context, fiArr, cancellationSignal);
            Object o = o();
            if (o == null) {
                return null;
            }
            int length = fiArr.length;
            int i4 = 0;
            boolean z = false;
            while (i4 < length) {
                Zm0.Fi fi = fiArr[i4];
                ByteBuffer byteBuffer = i3.get(fi.c());
                if (byteBuffer != null) {
                    if (!r(o, byteBuffer, fi.b(), fi.d(), fi.e() ? 1 : 0)) {
                        p(o);
                        return null;
                    }
                    z = true;
                }
                i4++;
                z = z;
            }
            if (!z) {
                p(o);
                return null;
            } else if (!s(o)) {
                return null;
            } else {
                Typeface l2 = l(o);
                if (l2 == null) {
                    return null;
                }
                return Typeface.create(l2, i2);
            }
        }
        throw th;
    }

    @DexIgnore
    @Override // com.fossil.Yl0
    public Typeface e(Context context, Resources resources, int i2, String str, int i3) {
        if (!t()) {
            return super.e(context, resources, i2, str, i3);
        }
        Object o = o();
        if (o == null) {
            return null;
        }
        if (!q(context, o, str, 0, -1, -1, null)) {
            p(o);
            return null;
        } else if (s(o)) {
            return l(o);
        } else {
            return null;
        }
    }

    @DexIgnore
    public Typeface l(Object obj) {
        try {
            Object newInstance = Array.newInstance(this.g, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) this.m.invoke(null, newInstance, -1, -1);
        } catch (IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }

    @DexIgnore
    public final void p(Object obj) {
        try {
            this.l.invoke(obj, new Object[0]);
        } catch (IllegalAccessException | InvocationTargetException e) {
        }
    }

    @DexIgnore
    public final boolean q(Context context, Object obj, String str, int i2, int i3, int i4, FontVariationAxis[] fontVariationAxisArr) {
        try {
            return ((Boolean) this.i.invoke(obj, context.getAssets(), str, 0, Boolean.FALSE, Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), fontVariationAxisArr)).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            return false;
        }
    }

    @DexIgnore
    public final boolean r(Object obj, ByteBuffer byteBuffer, int i2, int i3, int i4) {
        try {
            return ((Boolean) this.j.invoke(obj, byteBuffer, Integer.valueOf(i2), null, Integer.valueOf(i3), Integer.valueOf(i4))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            return false;
        }
    }

    @DexIgnore
    public final boolean s(Object obj) {
        try {
            return ((Boolean) this.k.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            return false;
        }
    }

    @DexIgnore
    public final boolean t() {
        if (this.i == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return this.i != null;
    }

    @DexIgnore
    public Method u(Class<?> cls) throws NoSuchMethodException {
        return cls.getMethod("abortCreation", new Class[0]);
    }

    @DexIgnore
    public Method v(Class<?> cls) throws NoSuchMethodException {
        Class<?> cls2 = Integer.TYPE;
        Class<?> cls3 = Boolean.TYPE;
        Class<?> cls4 = Integer.TYPE;
        return cls.getMethod("addFontFromAssetManager", AssetManager.class, String.class, cls2, cls3, cls4, cls4, cls4, FontVariationAxis[].class);
    }

    @DexIgnore
    public Method w(Class<?> cls) throws NoSuchMethodException {
        Class<?> cls2 = Integer.TYPE;
        return cls.getMethod("addFontFromBuffer", ByteBuffer.class, cls2, FontVariationAxis[].class, cls2, cls2);
    }

    @DexIgnore
    public Method x(Class<?> cls) throws NoSuchMethodException {
        Class<?> cls2 = Array.newInstance(cls, 1).getClass();
        Class cls3 = Integer.TYPE;
        Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", cls2, cls3, cls3);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    @DexIgnore
    public Class<?> y() throws ClassNotFoundException {
        return Class.forName("android.graphics.FontFamily");
    }

    @DexIgnore
    public Constructor<?> z(Class<?> cls) throws NoSuchMethodException {
        return cls.getConstructor(new Class[0]);
    }
}
