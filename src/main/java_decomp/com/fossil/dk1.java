package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dk1 {
    @DexIgnore
    public static /* final */ Executor a; // = new Ai();
    @DexIgnore
    public static /* final */ Executor b; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            runnable.run();
        }
    }

    @DexIgnore
    public static Executor a() {
        return b;
    }

    @DexIgnore
    public static Executor b() {
        return a;
    }
}
