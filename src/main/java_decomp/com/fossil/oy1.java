package com.fossil;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oy1<V> extends qy1<V> {
    @DexIgnore
    public CopyOnWriteArrayList<rp7<Float, tl7>> h; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $action;
        @DexIgnore
        public /* final */ /* synthetic */ float $progress$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(rp7 rp7, qn7 qn7, float f) {
            super(2, qn7);
            this.$action = rp7;
            this.$progress$inlined = f;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$action, qn7, this.$progress$inlined);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.$action.invoke(ao7.d(this.$progress$inlined));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    @Override // com.fossil.py1
    public void j() {
        super.j();
        this.h.clear();
    }

    @DexIgnore
    public oy1<V> t(rp7<? super tl7, tl7> rp7) {
        pq7.c(rp7, "actionOnFinal");
        qy1<V> p = super.p(rp7);
        if (p != null) {
            return (oy1) p;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    /* renamed from: u */
    public oy1<V> q(rp7<? super yx1, tl7> rp7) {
        pq7.c(rp7, "actionOnCancel");
        qy1<V> q = super.k(rp7);
        if (q != null) {
            return (oy1) q;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    /* renamed from: v */
    public oy1<V> r(rp7<? super yx1, tl7> rp7) {
        pq7.c(rp7, "actionOnError");
        qy1<V> r = super.l(rp7);
        if (r != null) {
            return (oy1) r;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    public final oy1<V> w(rp7<? super Float, tl7> rp7) {
        pq7.c(rp7, "actionOnProgressChange");
        if (!g()) {
            this.h.add(rp7);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: x */
    public oy1<V> s(rp7<? super V, tl7> rp7) {
        pq7.c(rp7, "actionOnSuccess");
        qy1<V> s = super.m(rp7);
        if (s != null) {
            return (oy1) s;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    public final void y(float f) {
        synchronized (this) {
            Iterator<T> it = this.h.iterator();
            while (it.hasNext()) {
                try {
                    xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(it.next(), null, f), 3, null);
                } catch (Exception e) {
                }
            }
        }
    }
}
