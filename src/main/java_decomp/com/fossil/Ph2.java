package com.fossil;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ph2 implements K42 {
    @DexIgnore
    public /* final */ Set<Scope> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Set<Scope> a;

        @DexIgnore
        public Ai() {
            this.a = new HashSet();
        }

        @DexIgnore
        public final Ai a(DataType dataType, int i) {
            Rc2.b(i == 0 || i == 1, "valid access types are FitnessOptions.ACCESS_READ or FitnessOptions.ACCESS_WRITE");
            if (i == 0 && dataType.k() != null) {
                this.a.add(new Scope(dataType.k()));
            } else if (i == 1 && dataType.A() != null) {
                this.a.add(new Scope(dataType.A()));
            }
            return this;
        }

        @DexIgnore
        public final Ph2 b() {
            return new Ph2(this);
        }
    }

    @DexIgnore
    public Ph2(Ai ai) {
        this.a = Lj2.a(ai.a);
    }

    @DexIgnore
    public static Ai b() {
        return new Ai();
    }

    @DexIgnore
    @Override // com.fossil.K42
    public final List<Scope> a() {
        return new ArrayList(this.a);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ph2)) {
            return false;
        }
        return this.a.equals(((Ph2) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(this.a);
    }
}
