package com.fossil;

import com.fossil.Bl1;
import com.mapped.Yb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yb implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Yc b;
    @DexIgnore
    public /* final */ /* synthetic */ Lp c;

    @DexIgnore
    public Yb(Yc yc, Lp lp) {
        this.b = yc;
        this.c = lp;
    }

    @DexIgnore
    public final void run() {
        Yc yc = this.b;
        Yb0 yb0 = yc.b;
        Nr nr = this.c.v;
        Yx1 yx1 = nr.e;
        if (yx1 == null) {
            yx1 = Bl1.d.a(nr, Zm7.i(E.d(yc.c, Bl1.Bi.b)));
        }
        yb0.n(yx1);
    }
}
