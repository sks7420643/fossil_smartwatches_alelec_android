package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V2 implements Parcelable.Creator<W2> {
    @DexIgnore
    public /* synthetic */ V2(Qg6 qg6) {
    }

    @DexIgnore
    public W2 a(Parcel parcel) {
        return new W2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public W2 createFromParcel(Parcel parcel) {
        return new W2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public W2[] newArray(int i) {
        return new W2[i];
    }
}
