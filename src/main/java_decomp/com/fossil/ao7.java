package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ao7 {
    @DexIgnore
    public static final Boolean a(boolean z) {
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public static final Character b(char c) {
        return new Character(c);
    }

    @DexIgnore
    public static final Double c(double d) {
        return new Double(d);
    }

    @DexIgnore
    public static final Float d(float f) {
        return new Float(f);
    }

    @DexIgnore
    public static final Integer e(int i) {
        return new Integer(i);
    }

    @DexIgnore
    public static final Long f(long j) {
        return new Long(j);
    }

    @DexIgnore
    public static final Short g(short s) {
        return new Short(s);
    }
}
