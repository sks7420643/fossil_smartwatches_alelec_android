package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gs5 implements Factory<WorkoutTetherGpsManager> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<LocationSource> b;

    @DexIgnore
    public Gs5(Provider<PortfolioApp> provider, Provider<LocationSource> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Gs5 a(Provider<PortfolioApp> provider, Provider<LocationSource> provider2) {
        return new Gs5(provider, provider2);
    }

    @DexIgnore
    public static WorkoutTetherGpsManager c(PortfolioApp portfolioApp, LocationSource locationSource) {
        return new WorkoutTetherGpsManager(portfolioApp, locationSource);
    }

    @DexIgnore
    public WorkoutTetherGpsManager b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
