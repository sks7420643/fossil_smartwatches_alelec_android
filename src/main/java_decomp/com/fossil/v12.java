package com.fossil;

import android.app.job.JobInfo;
import com.fossil.S12;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V12 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public T32 a;
        @DexIgnore
        public Map<Vy1, Bi> b; // = new HashMap();

        @DexIgnore
        public Ai a(Vy1 vy1, Bi bi) {
            this.b.put(vy1, bi);
            return this;
        }

        @DexIgnore
        public V12 b() {
            if (this.a == null) {
                throw new NullPointerException("missing required property: clock");
            } else if (this.b.keySet().size() >= Vy1.values().length) {
                Map<Vy1, Bi> map = this.b;
                this.b = new HashMap();
                return V12.c(this.a, map);
            } else {
                throw new IllegalStateException("Not all priorities have been configured");
            }
        }

        @DexIgnore
        public Ai c(T32 t32) {
            this.a = t32;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class Aii {
            @DexIgnore
            public abstract Bi a();

            @DexIgnore
            public abstract Aii b(long j);

            @DexIgnore
            public abstract Aii c(Set<Ci> set);

            @DexIgnore
            public abstract Aii d(long j);
        }

        @DexIgnore
        public static Aii a() {
            S12.Bi bi = new S12.Bi();
            bi.c(Collections.emptySet());
            return bi;
        }

        @DexIgnore
        public abstract long b();

        @DexIgnore
        public abstract Set<Ci> c();

        @DexIgnore
        public abstract long d();
    }

    @DexIgnore
    public enum Ci {
        NETWORK_UNMETERED,
        DEVICE_IDLE,
        DEVICE_CHARGING
    }

    @DexIgnore
    public static Ai a() {
        return new Ai();
    }

    @DexIgnore
    public static V12 c(T32 t32, Map<Vy1, Bi> map) {
        return new R12(t32, map);
    }

    @DexIgnore
    public static V12 e(T32 t32) {
        Ai a2 = a();
        Vy1 vy1 = Vy1.DEFAULT;
        Bi.Aii a3 = Bi.a();
        a3.b(30000);
        a3.d(LogBuilder.MAX_INTERVAL);
        a2.a(vy1, a3.a());
        Vy1 vy12 = Vy1.HIGHEST;
        Bi.Aii a4 = Bi.a();
        a4.b(1000);
        a4.d(LogBuilder.MAX_INTERVAL);
        a2.a(vy12, a4.a());
        Vy1 vy13 = Vy1.VERY_LOW;
        Bi.Aii a5 = Bi.a();
        a5.b(LogBuilder.MAX_INTERVAL);
        a5.d(LogBuilder.MAX_INTERVAL);
        a5.c(h(Ci.NETWORK_UNMETERED, Ci.DEVICE_IDLE));
        a2.a(vy13, a5.a());
        a2.c(t32);
        return a2.b();
    }

    @DexIgnore
    public static <T> Set<T> h(T... tArr) {
        return Collections.unmodifiableSet(new HashSet(Arrays.asList(tArr)));
    }

    @DexIgnore
    public JobInfo.Builder b(JobInfo.Builder builder, Vy1 vy1, long j, int i) {
        builder.setMinimumLatency(f(vy1, j, i));
        i(builder, g().get(vy1).c());
        return builder;
    }

    @DexIgnore
    public abstract T32 d();

    @DexIgnore
    public long f(Vy1 vy1, long j, int i) {
        long a2 = d().a();
        Bi bi = g().get(vy1);
        return Math.min(Math.max(((long) Math.pow(2.0d, (double) (i - 1))) * bi.b(), j - a2), bi.d());
    }

    @DexIgnore
    public abstract Map<Vy1, Bi> g();

    @DexIgnore
    public final void i(JobInfo.Builder builder, Set<Ci> set) {
        if (set.contains(Ci.NETWORK_UNMETERED)) {
            builder.setRequiredNetworkType(2);
        } else {
            builder.setRequiredNetworkType(1);
        }
        if (set.contains(Ci.DEVICE_CHARGING)) {
            builder.setRequiresCharging(true);
        }
        if (set.contains(Ci.DEVICE_IDLE)) {
            builder.setRequiresDeviceIdle(true);
        }
    }
}
