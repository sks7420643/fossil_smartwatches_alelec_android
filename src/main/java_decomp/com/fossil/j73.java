package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J73 implements Xw2<M73> {
    @DexIgnore
    public static J73 c; // = new J73();
    @DexIgnore
    public /* final */ Xw2<M73> b;

    @DexIgnore
    public J73() {
        this(Ww2.b(new L73()));
    }

    @DexIgnore
    public J73(Xw2<M73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((M73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((M73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((M73) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((M73) c.zza()).zzd();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ M73 zza() {
        return this.b.zza();
    }
}
