package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.ListMenuItemView;
import androidx.appcompat.widget.ListPopupWindow;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ih0 extends ListPopupWindow implements Hh0 {
    @DexIgnore
    public static Method P;
    @DexIgnore
    public Hh0 O;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Eh0 {
        @DexIgnore
        public /* final */ int u;
        @DexIgnore
        public /* final */ int v;
        @DexIgnore
        public Hh0 w;
        @DexIgnore
        public MenuItem x;

        @DexIgnore
        public Ai(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.u = 22;
                this.v = 21;
                return;
            }
            this.u = 21;
            this.v = 22;
        }

        @DexIgnore
        @Override // com.fossil.Eh0
        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i;
            Bg0 bg0;
            int pointToPosition;
            int i2;
            if (this.w != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i = headerViewListAdapter.getHeadersCount();
                    bg0 = (Bg0) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i = 0;
                    bg0 = (Bg0) adapter;
                }
                Eg0 c = (motionEvent.getAction() == 10 || (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) == -1 || (i2 = pointToPosition - i) < 0 || i2 >= bg0.getCount()) ? null : bg0.c(i2);
                MenuItem menuItem = this.x;
                if (menuItem != c) {
                    Cg0 b = bg0.b();
                    if (menuItem != null) {
                        this.w.g(b, menuItem);
                    }
                    this.x = c;
                    if (c != null) {
                        this.w.d(b, c);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        @DexIgnore
        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i == this.u) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i != this.v) {
                return super.onKeyDown(i, keyEvent);
            } else {
                setSelection(-1);
                ((Bg0) getAdapter()).b().e(false);
                return true;
            }
        }

        @DexIgnore
        public void setHoverListener(Hh0 hh0) {
            this.w = hh0;
        }

        @DexIgnore
        @Override // android.widget.AbsListView, com.fossil.Eh0
        public /* bridge */ /* synthetic */ void setSelector(Drawable drawable) {
            super.setSelector(drawable);
        }
    }

    /*
    static {
        try {
            if (Build.VERSION.SDK_INT <= 28) {
                P = PopupWindow.class.getDeclaredMethod("setTouchModal", Boolean.TYPE);
            }
        } catch (NoSuchMethodException e) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }
    */

    @DexIgnore
    public Ih0(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    public void M(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.K.setEnterTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void N(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.K.setExitTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void O(Hh0 hh0) {
        this.O = hh0;
    }

    @DexIgnore
    public void P(boolean z) {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = P;
            if (method != null) {
                try {
                    method.invoke(this.K, Boolean.valueOf(z));
                } catch (Exception e) {
                    Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
                }
            }
        } else {
            this.K.setTouchModal(z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hh0
    public void d(Cg0 cg0, MenuItem menuItem) {
        Hh0 hh0 = this.O;
        if (hh0 != null) {
            hh0.d(cg0, menuItem);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hh0
    public void g(Cg0 cg0, MenuItem menuItem) {
        Hh0 hh0 = this.O;
        if (hh0 != null) {
            hh0.g(cg0, menuItem);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ListPopupWindow
    public Eh0 r(Context context, boolean z) {
        Ai ai = new Ai(context, z);
        ai.setHoverListener(this);
        return ai;
    }
}
