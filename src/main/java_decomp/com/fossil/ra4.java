package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ra4 extends Ta4.Di.Eii {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Eii.Aiii {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Boolean d;

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Eii.Aiii
        public Ta4.Di.Eii a() {
            String str = "";
            if (this.a == null) {
                str = " platform";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (this.c == null) {
                str = str + " buildVersion";
            }
            if (this.d == null) {
                str = str + " jailbroken";
            }
            if (str.isEmpty()) {
                return new Ra4(this.a.intValue(), this.b, this.c, this.d.booleanValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Eii.Aiii
        public Ta4.Di.Eii.Aiii b(String str) {
            if (str != null) {
                this.c = str;
                return this;
            }
            throw new NullPointerException("Null buildVersion");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Eii.Aiii
        public Ta4.Di.Eii.Aiii c(boolean z) {
            this.d = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Eii.Aiii
        public Ta4.Di.Eii.Aiii d(int i) {
            this.a = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Eii.Aiii
        public Ta4.Di.Eii.Aiii e(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null version");
        }
    }

    @DexIgnore
    public Ra4(int i, String str, String str2, boolean z) {
        this.a = i;
        this.b = str;
        this.c = str2;
        this.d = z;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Eii
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Eii
    public int c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Eii
    public String d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Eii
    public boolean e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di.Eii)) {
            return false;
        }
        Ta4.Di.Eii eii = (Ta4.Di.Eii) obj;
        return this.a == eii.c() && this.b.equals(eii.d()) && this.c.equals(eii.b()) && this.d == eii.e();
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a;
        int hashCode = this.b.hashCode();
        return (this.d ? 1231 : 1237) ^ ((((((i ^ 1000003) * 1000003) ^ hashCode) * 1000003) ^ this.c.hashCode()) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "OperatingSystem{platform=" + this.a + ", version=" + this.b + ", buildVersion=" + this.c + ", jailbroken=" + this.d + "}";
    }
}
