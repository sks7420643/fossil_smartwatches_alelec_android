package com.fossil;

import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pn4 implements Ql4 {
    @DexIgnore
    public static Bm4 b(Un4 un4, String str, int i, int i2, int i3, int i4) throws Rl4 {
        boolean z;
        un4.e(str, i);
        byte[][] b = un4.f().b(1, 4);
        if ((i3 > i2) ^ (b[0].length < b.length)) {
            b = d(b);
            z = true;
        } else {
            z = false;
        }
        int length = i2 / b[0].length;
        int length2 = i3 / b.length;
        if (length >= length2) {
            length = length2;
        }
        if (length <= 1) {
            return c(b, i4);
        }
        byte[][] b2 = un4.f().b(length, length << 2);
        return c(z ? d(b2) : b2, i4);
    }

    @DexIgnore
    public static Bm4 c(byte[][] bArr, int i) {
        int i2 = i * 2;
        Bm4 bm4 = new Bm4(bArr[0].length + i2, i2 + bArr.length);
        bm4.e();
        int j = (bm4.j() - i) - 1;
        int i3 = 0;
        while (i3 < bArr.length) {
            for (int i4 = 0; i4 < bArr[0].length; i4++) {
                if (bArr[i3][i4] == 1) {
                    bm4.n(i4 + i, j);
                }
            }
            i3++;
            j--;
        }
        return bm4;
    }

    @DexIgnore
    public static byte[][] d(byte[][] bArr) {
        byte[][] bArr2 = (byte[][]) Array.newInstance(Byte.TYPE, bArr[0].length, bArr.length);
        for (int i = 0; i < bArr.length; i++) {
            int length = bArr.length;
            for (int i2 = 0; i2 < bArr[0].length; i2++) {
                bArr2[i2][(length - i) - 1] = (byte) bArr[i][i2];
            }
        }
        return bArr2;
    }

    @DexIgnore
    @Override // com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        int i3;
        int i4 = 2;
        if (kl4 == Kl4.PDF_417) {
            Un4 un4 = new Un4();
            if (map != null) {
                if (map.containsKey(Ml4.PDF417_COMPACT)) {
                    un4.h(Boolean.valueOf(map.get(Ml4.PDF417_COMPACT).toString()).booleanValue());
                }
                if (map.containsKey(Ml4.PDF417_COMPACTION)) {
                    un4.i(Sn4.valueOf(map.get(Ml4.PDF417_COMPACTION).toString()));
                }
                if (map.containsKey(Ml4.PDF417_DIMENSIONS)) {
                    Tn4 tn4 = (Tn4) map.get(Ml4.PDF417_DIMENSIONS);
                    un4.j(tn4.a(), tn4.c(), tn4.b(), tn4.d());
                }
                int parseInt = map.containsKey(Ml4.MARGIN) ? Integer.parseInt(map.get(Ml4.MARGIN).toString()) : 30;
                if (map.containsKey(Ml4.ERROR_CORRECTION)) {
                    i4 = Integer.parseInt(map.get(Ml4.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(Ml4.CHARACTER_SET)) {
                    un4.k(Charset.forName(map.get(Ml4.CHARACTER_SET).toString()));
                }
                i3 = parseInt;
            } else {
                i3 = 30;
            }
            return b(un4, str, i4, i, i2, i3);
        }
        throw new IllegalArgumentException("Can only encode PDF_417, but got " + kl4);
    }
}
