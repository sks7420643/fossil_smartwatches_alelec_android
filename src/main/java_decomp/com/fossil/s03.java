package com.fossil;

import com.fossil.V03;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class S03<T extends V03<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract T03<T> b(Object obj);

    @DexIgnore
    public abstract Object c(Q03 q03, M23 m23, int i);

    @DexIgnore
    public abstract void d(R43 r43, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract boolean e(M23 m23);

    @DexIgnore
    public abstract T03<T> f(Object obj);

    @DexIgnore
    public abstract void g(Object obj);
}
