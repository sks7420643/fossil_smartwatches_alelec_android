package com.fossil;

import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ir7 {
    @DexIgnore
    public static Iterable a(Object obj) {
        if (!(obj instanceof Jr7)) {
            return e(obj);
        }
        l(obj, "kotlin.collections.MutableIterable");
        throw null;
    }

    @DexIgnore
    public static List b(Object obj) {
        if (!(obj instanceof Jr7)) {
            return f(obj);
        }
        l(obj, "kotlin.collections.MutableList");
        throw null;
    }

    @DexIgnore
    public static Map c(Object obj) {
        if (!(obj instanceof Jr7)) {
            return g(obj);
        }
        l(obj, "kotlin.collections.MutableMap");
        throw null;
    }

    @DexIgnore
    public static Object d(Object obj, int i) {
        if (obj == null || i(obj, i)) {
            return obj;
        }
        l(obj, "kotlin.jvm.functions.Function" + i);
        throw null;
    }

    @DexIgnore
    public static Iterable e(Object obj) {
        try {
            return (Iterable) obj;
        } catch (ClassCastException e) {
            k(e);
            throw null;
        }
    }

    @DexIgnore
    public static List f(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            k(e);
            throw null;
        }
    }

    @DexIgnore
    public static Map g(Object obj) {
        try {
            return (Map) obj;
        } catch (ClassCastException e) {
            k(e);
            throw null;
        }
    }

    @DexIgnore
    public static int h(Object obj) {
        if (obj instanceof Mq7) {
            return ((Mq7) obj).getArity();
        }
        if (obj instanceof Gg6) {
            return 0;
        }
        if (obj instanceof Hg6) {
            return 1;
        }
        if (obj instanceof Coroutine) {
            return 2;
        }
        if (obj instanceof Wp7) {
            return 3;
        }
        if (obj instanceof Xp7) {
            return 4;
        }
        if (obj instanceof Yp7) {
            return 5;
        }
        if (obj instanceof Zp7) {
            return 6;
        }
        if (obj instanceof Aq7) {
            return 7;
        }
        if (obj instanceof Bq7) {
            return 8;
        }
        if (obj instanceof Cq7) {
            return 9;
        }
        if (obj instanceof Hp7) {
            return 10;
        }
        if (obj instanceof Ip7) {
            return 11;
        }
        if (obj instanceof Jp7) {
            return 12;
        }
        if (obj instanceof Kp7) {
            return 13;
        }
        if (obj instanceof Lp7) {
            return 14;
        }
        if (obj instanceof Mp7) {
            return 15;
        }
        if (obj instanceof Np7) {
            return 16;
        }
        if (obj instanceof Op7) {
            return 17;
        }
        if (obj instanceof Pp7) {
            return 18;
        }
        if (obj instanceof Qp7) {
            return 19;
        }
        if (obj instanceof Sp7) {
            return 20;
        }
        if (obj instanceof Tp7) {
            return 21;
        }
        return obj instanceof Up7 ? 22 : -1;
    }

    @DexIgnore
    public static boolean i(Object obj, int i) {
        return (obj instanceof Uk7) && h(obj) == i;
    }

    @DexIgnore
    public static <T extends Throwable> T j(T t) {
        Wg6.g(t, Ir7.class.getName());
        return t;
    }

    @DexIgnore
    public static ClassCastException k(ClassCastException classCastException) {
        j(classCastException);
        throw classCastException;
    }

    @DexIgnore
    public static void l(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        m(name + " cannot be cast to " + str);
        throw null;
    }

    @DexIgnore
    public static void m(String str) {
        k(new ClassCastException(str));
        throw null;
    }
}
