package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import com.fossil.Kw0;
import com.fossil.Lw0;
import com.fossil.Nw0;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ow0 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ Nw0 d;
    @DexIgnore
    public /* final */ Nw0.Ci e;
    @DexIgnore
    public Lw0 f;
    @DexIgnore
    public /* final */ Executor g;
    @DexIgnore
    public /* final */ Kw0 h; // = new Ai();
    @DexIgnore
    public /* final */ AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ServiceConnection j; // = new Bi();
    @DexIgnore
    public /* final */ Runnable k; // = new Ci();
    @DexIgnore
    public /* final */ Runnable l; // = new Di();
    @DexIgnore
    public /* final */ Runnable m; // = new Ei();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Kw0.Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String[] b;

            @DexIgnore
            public Aii(String[] strArr) {
                this.b = strArr;
            }

            @DexIgnore
            public void run() {
                Ow0.this.d.g(this.b);
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Kw0
        public void T(String[] strArr) {
            Ow0.this.g.execute(new Aii(strArr));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements ServiceConnection {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Ow0.this.f = Lw0.Ai.d(iBinder);
            Ow0 ow0 = Ow0.this;
            ow0.g.execute(ow0.k);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            Ow0 ow0 = Ow0.this;
            ow0.g.execute(ow0.l);
            Ow0.this.f = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void run() {
            try {
                Lw0 lw0 = Ow0.this.f;
                if (lw0 != null) {
                    Ow0.this.c = lw0.l0(Ow0.this.h, Ow0.this.b);
                    Ow0.this.d.a(Ow0.this.e);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Runnable {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void run() {
            Ow0 ow0 = Ow0.this;
            ow0.d.j(ow0.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei implements Runnable {
        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public void run() {
            Ow0 ow0 = Ow0.this;
            ow0.d.j(ow0.e);
            try {
                Lw0 lw0 = Ow0.this.f;
                if (lw0 != null) {
                    lw0.M2(Ow0.this.h, Ow0.this.c);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e);
            }
            Ow0 ow02 = Ow0.this;
            ow02.a.unbindService(ow02.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Nw0.Ci {
        @DexIgnore
        public Fi(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            if (!Ow0.this.i.get()) {
                try {
                    Lw0 lw0 = Ow0.this.f;
                    if (lw0 != null) {
                        lw0.E2(Ow0.this.c, (String[]) set.toArray(new String[0]));
                    }
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public Ow0(Context context, String str, Nw0 nw0, Executor executor) {
        this.a = context.getApplicationContext();
        this.b = str;
        this.d = nw0;
        this.g = executor;
        this.e = new Fi((String[]) nw0.a.keySet().toArray(new String[0]));
        this.a.bindService(new Intent(this.a, MultiInstanceInvalidationService.class), this.j, 1);
    }

    @DexIgnore
    public void a() {
        if (this.i.compareAndSet(false, true)) {
            this.g.execute(this.m);
        }
    }
}
