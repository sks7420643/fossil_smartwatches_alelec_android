package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jm5 {
    @DexIgnore
    public HashMap<Integer, Object> a;

    @DexIgnore
    public Jm5() {
        this.a = null;
        this.a = new HashMap<>();
    }

    @DexIgnore
    public void a(Cm5 cm5, int i) {
        if (cm5 == null) {
            throw null;
        } else if (i == 129 || i == 130 || i == 151) {
            ArrayList arrayList = (ArrayList) this.a.get(Integer.valueOf(i));
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(cm5);
            this.a.put(Integer.valueOf(i), arrayList);
        } else {
            throw new RuntimeException("Invalid header field!");
        }
    }

    @DexIgnore
    public Cm5 b(int i) {
        return (Cm5) this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public Cm5[] c(int i) {
        ArrayList arrayList = (ArrayList) this.a.get(Integer.valueOf(i));
        if (arrayList == null) {
            return null;
        }
        return (Cm5[]) arrayList.toArray(new Cm5[arrayList.size()]);
    }

    @DexIgnore
    public long d(int i) {
        Long l = (Long) this.a.get(Integer.valueOf(i));
        if (l == null) {
            return -1;
        }
        return l.longValue();
    }

    @DexIgnore
    public int e(int i) {
        Integer num = (Integer) this.a.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    @DexIgnore
    public byte[] f(int i) {
        return (byte[]) this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void g(Cm5 cm5, int i) {
        if (cm5 == null) {
            throw null;
        } else if (i == 137 || i == 147 || i == 150 || i == 154 || i == 160 || i == 164 || i == 166 || i == 181 || i == 182) {
            this.a.put(Integer.valueOf(i), cm5);
        } else {
            throw new RuntimeException("Invalid header field!");
        }
    }

    @DexIgnore
    public void h(long j, int i) {
        if (i == 133 || i == 142 || i == 157 || i == 159 || i == 161 || i == 173 || i == 175 || i == 179 || i == 135 || i == 136) {
            this.a.put(Integer.valueOf(i), Long.valueOf(j));
            return;
        }
        throw new RuntimeException("Invalid header field!");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x010f, code lost:
        if (r7 <= 255) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0115, code lost:
        if (r7 < 192) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0117, code lost:
        if (r7 > 255) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0125, code lost:
        if (r7 <= 255) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x012b, code lost:
        if (r7 < 192) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x012d, code lost:
        if (r7 > 255) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0084, code lost:
        if (r7 <= 255) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x008c, code lost:
        if (r7 < 192) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x008e, code lost:
        if (r7 > 255) goto L_0x0090;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i(int r7, int r8) throws com.fossil.Wl5 {
        /*
        // Method dump skipped, instructions count: 340
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Jm5.i(int, int):void");
    }

    @DexIgnore
    public void j(byte[] bArr, int i) {
        if (bArr != null) {
            if (!(i == 131 || i == 132 || i == 138 || i == 139 || i == 152 || i == 158 || i == 189 || i == 190)) {
                switch (i) {
                    case 183:
                    case 184:
                    case 185:
                        break;
                    default:
                        throw new RuntimeException("Invalid header field!");
                }
            }
            this.a.put(Integer.valueOf(i), bArr);
            return;
        }
        throw null;
    }
}
