package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nh6 implements Factory<GoalTrackingOverviewDayPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewDayPresenter a(Kh6 kh6, An4 an4, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewDayPresenter(kh6, an4, goalTrackingRepository);
    }
}
