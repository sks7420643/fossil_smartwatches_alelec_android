package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Us6 implements Factory<CustomizeHeartRateChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Us6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Us6 a(Provider<ThemeRepository> provider) {
        return new Us6(provider);
    }

    @DexIgnore
    public static CustomizeHeartRateChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeHeartRateChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeHeartRateChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
