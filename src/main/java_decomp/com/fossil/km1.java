package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Km1 extends Dm1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Km1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Km1 a(Parcel parcel) {
            return new Km1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Km1 createFromParcel(Parcel parcel) {
            return new Km1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Km1[] newArray(int i) {
            return new Km1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Km1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Km1(Ct1 ct1) {
        super(Fm1.SECOND_TIMEZONE, ct1, null, null, 12);
    }

    @DexIgnore
    public Km1(Ct1 ct1, Dt1 dt1, Et1 et1) {
        super(Fm1.SECOND_TIMEZONE, ct1, dt1, et1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Km1(Ct1 ct1, Dt1 dt1, Et1 et1, int i, Qg6 qg6) {
        this(ct1, dt1, (i & 4) != 0 ? new Et1(Et1.CREATOR.a()) : et1);
    }

    @DexIgnore
    public final Ct1 getDataConfig() {
        At1 at1 = this.c;
        if (at1 != null) {
            return (Ct1) at1;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }
}
