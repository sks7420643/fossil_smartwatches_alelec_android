package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rq5 implements Factory<AuthenticationInterceptor> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Rq5(Provider<PortfolioApp> provider, Provider<AuthApiGuestService> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Rq5 a(Provider<PortfolioApp> provider, Provider<AuthApiGuestService> provider2, Provider<An4> provider3) {
        return new Rq5(provider, provider2, provider3);
    }

    @DexIgnore
    public static AuthenticationInterceptor c(PortfolioApp portfolioApp, AuthApiGuestService authApiGuestService, An4 an4) {
        return new AuthenticationInterceptor(portfolioApp, authApiGuestService, an4);
    }

    @DexIgnore
    public AuthenticationInterceptor b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
