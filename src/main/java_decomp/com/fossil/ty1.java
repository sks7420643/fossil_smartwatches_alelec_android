package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ty1 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public Ty1(String str) {
        if (str != null) {
            this.a = str;
            return;
        }
        throw new NullPointerException("name is null");
    }

    @DexIgnore
    public static Ty1 b(String str) {
        return new Ty1(str);
    }

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ty1)) {
            return false;
        }
        return this.a.equals(((Ty1) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Encoding{name=\"" + this.a + "\"}";
    }
}
