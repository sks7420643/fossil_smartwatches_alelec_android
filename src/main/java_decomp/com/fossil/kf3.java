package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kf3 implements Parcelable.Creator<We3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ We3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        LatLngBounds latLngBounds = null;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                latLng4 = (LatLng) Ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 3) {
                latLng3 = (LatLng) Ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 4) {
                latLng2 = (LatLng) Ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 5) {
                latLng = (LatLng) Ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l != 6) {
                Ad2.B(parcel, t);
            } else {
                latLngBounds = (LatLngBounds) Ad2.e(parcel, t, LatLngBounds.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new We3(latLng4, latLng3, latLng2, latLng, latLngBounds);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ We3[] newArray(int i) {
        return new We3[i];
    }
}
