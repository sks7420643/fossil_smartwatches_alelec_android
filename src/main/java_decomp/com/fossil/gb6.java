package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gb6 implements Factory<WatchAppSearchPresenter> {
    @DexIgnore
    public static WatchAppSearchPresenter a(Bb6 bb6, WatchAppRepository watchAppRepository, An4 an4) {
        return new WatchAppSearchPresenter(bb6, watchAppRepository, an4);
    }
}
