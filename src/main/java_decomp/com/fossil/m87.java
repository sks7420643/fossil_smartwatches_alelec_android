package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m87 extends hq4 {
    @DexIgnore
    public /* final */ LiveData<List<p77>> h; // = or0.c(bw7.b(), 0, new a(this, null), 2, null);
    @DexIgnore
    public /* final */ s77 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel$ringsLiveData$1", f = "WatchFaceRingViewModel.kt", l = {12, 12}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<hs0<List<? extends p77>>, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ m87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(m87 m87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = m87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (hs0) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(hs0<List<? extends p77>> hs0, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0038
                if (r0 == r2) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                com.fossil.el7.b(r6)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$1
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                java.lang.Object r1 = r5.L$0
                com.fossil.hs0 r1 = (com.fossil.hs0) r1
                com.fossil.el7.b(r6)
                r2 = r1
            L_0x002c:
                r5.L$0 = r2
                r5.label = r4
                java.lang.Object r0 = r0.b(r6, r5)
                if (r0 != r3) goto L_0x0015
                r0 = r3
                goto L_0x0017
            L_0x0038:
                com.fossil.el7.b(r6)
                com.fossil.hs0 r1 = r5.p$
                com.fossil.m87 r0 = r5.this$0
                com.fossil.s77 r0 = com.fossil.m87.n(r0)
                r5.L$0 = r1
                r5.L$1 = r1
                r5.label = r2
                java.lang.Object r6 = r0.f(r5)
                if (r6 != r3) goto L_0x0051
                r0 = r3
                goto L_0x0017
            L_0x0051:
                r0 = r1
                r2 = r1
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.m87.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public m87(s77 s77) {
        pq7.c(s77, "mRepository");
        this.i = s77;
    }

    @DexIgnore
    public final LiveData<List<p77>> o() {
        return this.h;
    }
}
