package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Eu {
    c(new byte[]{2});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Eu(byte[] bArr) {
        this.b = bArr;
    }
}
