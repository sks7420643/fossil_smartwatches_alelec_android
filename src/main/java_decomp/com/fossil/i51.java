package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I51 implements J51 {
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public /* final */ N51<Bi, Bitmap> b; // = new N51<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Bitmap.Config c;

        @DexIgnore
        public Bi(int i, int i2, Bitmap.Config config) {
            Wg6.c(config, "config");
            this.a = i;
            this.b = i2;
            this.c = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!(this.a == bi.a && this.b == bi.b && Wg6.a(this.c, bi.c))) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a;
            int i2 = this.b;
            Bitmap.Config config = this.c;
            return (config != null ? config.hashCode() : 0) + (((i * 31) + i2) * 31);
        }

        @DexIgnore
        public String toString() {
            Ai ai = I51.c;
            int i = this.a;
            int i2 = this.b;
            Bitmap.Config config = this.c;
            return '[' + i + " x " + i2 + "], " + config;
        }
    }

    @DexIgnore
    @Override // com.fossil.J51
    public String a(int i, int i2, Bitmap.Config config) {
        Wg6.c(config, "config");
        return '[' + i + " x " + i2 + "], " + config;
    }

    @DexIgnore
    @Override // com.fossil.J51
    public void b(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        N51<Bi, Bitmap> n51 = this.b;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        Wg6.b(config, "bitmap.config");
        n51.f(new Bi(width, height, config), bitmap);
    }

    @DexIgnore
    @Override // com.fossil.J51
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        Wg6.c(config, "config");
        return this.b.a(new Bi(i, i2, config));
    }

    @DexIgnore
    @Override // com.fossil.J51
    public String d(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        Wg6.b(config, "bitmap.config");
        return '[' + width + " x " + height + "], " + config;
    }

    @DexIgnore
    @Override // com.fossil.J51
    public Bitmap removeLast() {
        return this.b.e();
    }

    @DexIgnore
    public String toString() {
        return "AttributeStrategy: groupedMap=" + this.b;
    }
}
