package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X48 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public X48 f;
    @DexIgnore
    public X48 g;

    @DexIgnore
    public X48() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    @DexIgnore
    public X48(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        Wg6.c(bArr, "data");
        this.a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public final void a() {
        int i = 0;
        if (this.g != this) {
            X48 x48 = this.g;
            if (x48 == null) {
                Wg6.i();
                throw null;
            } else if (x48.e) {
                int i2 = this.c - this.b;
                if (x48 != null) {
                    int i3 = x48.c;
                    if (x48 != null) {
                        if (!x48.d) {
                            if (x48 != null) {
                                i = x48.b;
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        if (i2 <= i + (8192 - i3)) {
                            X48 x482 = this.g;
                            if (x482 != null) {
                                f(x482, i2);
                                b();
                                Y48.c.a(this);
                                return;
                            }
                            Wg6.i();
                            throw null;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
        } else {
            throw new IllegalStateException("cannot compact".toString());
        }
    }

    @DexIgnore
    public final X48 b() {
        X48 x48 = this.f;
        if (x48 == this) {
            x48 = null;
        }
        X48 x482 = this.g;
        if (x482 != null) {
            x482.f = this.f;
            X48 x483 = this.f;
            if (x483 != null) {
                x483.g = x482;
                this.f = null;
                this.g = null;
                return x48;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final X48 c(X48 x48) {
        Wg6.c(x48, "segment");
        x48.g = this;
        x48.f = this.f;
        X48 x482 = this.f;
        if (x482 != null) {
            x482.g = x48;
            this.f = x48;
            return x48;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final X48 d() {
        this.d = true;
        return new X48(this.a, this.b, this.c, true, false);
    }

    @DexIgnore
    public final X48 e(int i) {
        X48 x48;
        if (i > 0 && i <= this.c - this.b) {
            if (i >= 1024) {
                x48 = d();
            } else {
                X48 b2 = Y48.c.b();
                byte[] bArr = this.a;
                byte[] bArr2 = b2.a;
                int i2 = this.b;
                Dm7.i(bArr, bArr2, 0, i2, i2 + i, 2, null);
                x48 = b2;
            }
            x48.c = x48.b + i;
            this.b += i;
            X48 x482 = this.g;
            if (x482 != null) {
                x482.c(x48);
                return x48;
            }
            Wg6.i();
            throw null;
        }
        throw new IllegalArgumentException("byteCount out of range".toString());
    }

    @DexIgnore
    public final void f(X48 x48, int i) {
        Wg6.c(x48, "sink");
        if (x48.e) {
            int i2 = x48.c;
            if (i2 + i > 8192) {
                if (!x48.d) {
                    int i3 = x48.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = x48.a;
                        Dm7.i(bArr, bArr, 0, i3, i2, 2, null);
                        x48.c -= x48.b;
                        x48.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            byte[] bArr2 = this.a;
            byte[] bArr3 = x48.a;
            int i4 = x48.c;
            int i5 = this.b;
            Dm7.g(bArr2, bArr3, i4, i5, i5 + i);
            x48.c += i;
            this.b += i;
            return;
        }
        throw new IllegalStateException("only owner can write".toString());
    }
}
