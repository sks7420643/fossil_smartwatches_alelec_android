package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y51 implements E61<Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Y51(Context context) {
        Wg6.c(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return e(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Uri uri) {
        return f(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ Object c(G51 g51, Uri uri, F81 f81, X51 x51, Xe6 xe6) {
        return d(g51, uri, f81, x51, xe6);
    }

    @DexIgnore
    public Object d(G51 g51, Uri uri, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        List<String> pathSegments = uri.getPathSegments();
        Wg6.b(pathSegments, "data.pathSegments");
        String N = Pm7.N(Pm7.D(pathSegments, 1), "/", null, null, 0, null, null, 62, null);
        InputStream open = this.a.getAssets().open(N);
        Wg6.b(open, "context.assets.open(path)");
        K48 d = S48.d(S48.l(open));
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        Wg6.b(singleton, "MimeTypeMap.getSingleton()");
        return new K61(d, W81.g(singleton, N), Q51.DISK);
    }

    @DexIgnore
    public boolean e(Uri uri) {
        Wg6.c(uri, "data");
        return Wg6.a(uri.getScheme(), "file") && Wg6.a(W81.f(uri), "android_asset");
    }

    @DexIgnore
    public String f(Uri uri) {
        Wg6.c(uri, "data");
        String uri2 = uri.toString();
        Wg6.b(uri2, "data.toString()");
        return uri2;
    }
}
