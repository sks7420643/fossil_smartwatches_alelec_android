package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Arrays;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ts4 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String[] j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public Date l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ts4> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Ts4 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Ts4(parcel);
        }

        @DexIgnore
        public Ts4[] b(int i) {
            return new Ts4[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ts4 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ts4[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public Ts4() {
        this(null, null, null, null, null, 0, 0, null, null, false, null, 2047, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ts4(android.os.Parcel r15) {
        /*
            r14 = this;
            r11 = 0
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r15, r0)
            java.lang.String r1 = r15.readString()
            java.lang.String r2 = r15.readString()
            if (r2 == 0) goto L_0x007f
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r2, r0)
            java.lang.String r3 = r15.readString()
            java.lang.String r4 = r15.readString()
            if (r4 == 0) goto L_0x007b
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r4, r0)
            java.lang.String r5 = r15.readString()
            if (r5 == 0) goto L_0x0077
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r5, r0)
            int r6 = r15.readInt()
            int r7 = r15.readInt()
            java.lang.String r8 = r15.readString()
            if (r8 == 0) goto L_0x0073
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r8, r0)
            java.lang.String[] r9 = r15.createStringArray()
            if (r9 == 0) goto L_0x006f
            java.lang.String r0 = "parcel.createStringArray()!!"
            com.mapped.Wg6.b(r9, r0)
            java.lang.Class r0 = java.lang.Boolean.TYPE
            java.lang.ClassLoader r0 = r0.getClassLoader()
            java.lang.Object r0 = r15.readValue(r0)
            if (r0 == 0) goto L_0x0067
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r10 = r0.booleanValue()
            r12 = 1024(0x400, float:1.435E-42)
            r0 = r14
            r13 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        L_0x0067:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Boolean"
            r0.<init>(r1)
            throw r0
        L_0x006f:
            com.mapped.Wg6.i()
            throw r11
        L_0x0073:
            com.mapped.Wg6.i()
            throw r11
        L_0x0077:
            com.mapped.Wg6.i()
            throw r11
        L_0x007b:
            com.mapped.Wg6.i()
            throw r11
        L_0x007f:
            com.mapped.Wg6.i()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ts4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Ts4(String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6, String[] strArr, boolean z, Date date) {
        Wg6.c(str2, "name");
        Wg6.c(str4, "type");
        Wg6.c(str5, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        Wg6.c(str6, SampleRaw.COLUMN_START_TIME);
        Wg6.c(strArr, "ids");
        Wg6.c(date, "startTrackingDate");
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = i2;
        this.h = i3;
        this.i = str6;
        this.j = strArr;
        this.k = z;
        this.l = date;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ts4(String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6, String[] strArr, boolean z, Date date, int i4, Qg6 qg6) {
        this((i4 & 1) != 0 ? null : str, (i4 & 2) != 0 ? "" : str2, (i4 & 4) == 0 ? str3 : null, (i4 & 8) != 0 ? "activity_best_result" : str4, (i4 & 16) != 0 ? "public_with_friend" : str5, (i4 & 32) != 0 ? 0 : i2, (i4 & 64) != 0 ? 0 : i3, (i4 & 128) == 0 ? str6 : "", (i4 & 256) != 0 ? new String[0] : strArr, (i4 & 512) == 0 ? z : false, (i4 & 1024) != 0 ? new Date() : date);
    }

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final int b() {
        return this.h;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final String[] d() {
        return this.j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ts4) {
                Ts4 ts4 = (Ts4) obj;
                if (!Wg6.a(this.b, ts4.b) || !Wg6.a(this.c, ts4.c) || !Wg6.a(this.d, ts4.d) || !Wg6.a(this.e, ts4.e) || !Wg6.a(this.f, ts4.f) || this.g != ts4.g || this.h != ts4.h || !Wg6.a(this.i, ts4.i) || !Wg6.a(this.j, ts4.j) || this.k != ts4.k || !Wg6.a(this.l, ts4.l)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.f;
    }

    @DexIgnore
    public final String g() {
        return this.i;
    }

    @DexIgnore
    public final Date h() {
        return this.l;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.f;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        int i3 = this.g;
        int i4 = this.h;
        String str6 = this.i;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String[] strArr = this.j;
        int hashCode7 = strArr != null ? Arrays.hashCode(strArr) : 0;
        boolean z = this.k;
        if (z) {
            z = true;
        }
        Date date = this.l;
        if (date != null) {
            i2 = date.hashCode();
        }
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        int i7 = z ? 1 : 0;
        return (((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i3) * 31) + i4) * 31) + hashCode6) * 31) + hashCode7) * 31) + i5) * 31) + i2;
    }

    @DexIgnore
    public final int i() {
        return this.g;
    }

    @DexIgnore
    public final String k() {
        return this.e;
    }

    @DexIgnore
    public final boolean m() {
        return this.k;
    }

    @DexIgnore
    public final void n(String str) {
        this.d = str;
    }

    @DexIgnore
    public final void p(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void q(String[] strArr) {
        Wg6.c(strArr, "<set-?>");
        this.j = strArr;
    }

    @DexIgnore
    public final void r(boolean z) {
        this.k = z;
    }

    @DexIgnore
    public final void s(String str) {
        Wg6.c(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "<set-?>");
        this.f = str;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeDraft(id=" + this.b + ", name=" + this.c + ", des=" + this.d + ", type=" + this.e + ", privacy=" + this.f + ", target=" + this.g + ", duration=" + this.h + ", startTime=" + this.i + ", ids=" + Arrays.toString(this.j) + ", isInviteAll=" + this.k + ", startTrackingDate=" + this.l + ")";
    }

    @DexIgnore
    public final void u(String str) {
        Wg6.c(str, "<set-?>");
        this.i = str;
    }

    @DexIgnore
    public final void v(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
        parcel.writeString(this.i);
        parcel.writeStringArray(this.j);
        parcel.writeValue(Boolean.valueOf(this.k));
    }
}
