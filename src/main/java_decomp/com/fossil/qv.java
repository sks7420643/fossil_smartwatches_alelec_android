package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qv extends Ps {
    @DexIgnore
    public /* final */ N6 G;
    @DexIgnore
    public /* final */ N6 H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Qv(short s, K5 k5, int i, int i2) {
        super(Hs.Y, k5, (i2 & 4) != 0 ? 3 : i);
        this.K = (short) s;
        N6 n6 = N6.j;
        this.G = n6;
        this.H = n6;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(Sv.l.b).putShort(this.K).array();
        Wg6.b(array, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(Sv.l.a()).putShort(this.K).array();
        Wg6.b(array2, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.J = array2;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public Mt E(byte b) {
        return Kt.k.a(b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public N6 K() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] M() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public N6 N() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] P() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.A0, Hy1.l(this.K, null, 1, null));
    }
}
