package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import com.mapped.Fd;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ur0 extends Fd {
    @DexIgnore
    void onCreate(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void onDestroy(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void onPause(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void onResume(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void onStart(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void onStop(LifecycleOwner lifecycleOwner);
}
