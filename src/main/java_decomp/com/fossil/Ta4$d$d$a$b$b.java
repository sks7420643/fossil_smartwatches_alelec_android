package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ta4$d$d$a$b$b {
    @DexIgnore
    public abstract Ta4.Di.Dii.Aiii.Biiii a();

    @DexIgnore
    public abstract Ta4$d$d$a$b$b b(Ua4<Ta4$d$d$a$b$a> ua4);

    @DexIgnore
    public abstract Ta4$d$d$a$b$b c(Ta4$d$d$a$b$c ta4$d$d$a$b$c);

    @DexIgnore
    public abstract Ta4$d$d$a$b$b d(Ta4$d$d$a$b$d ta4$d$d$a$b$d);

    @DexIgnore
    public abstract Ta4$d$d$a$b$b e(Ua4<Ta4$d$d$a$b$e> ua4);
}
