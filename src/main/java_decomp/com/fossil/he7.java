package com.fossil;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class He7 extends De7 {
    @DexIgnore
    public /* final */ MethodCall a;
    @DexIgnore
    public /* final */ Ai b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Je7 {
        @DexIgnore
        public /* final */ MethodChannel.Result a;

        @DexIgnore
        public Ai(He7 he7, MethodChannel.Result result) {
            this.a = result;
        }

        @DexIgnore
        @Override // com.fossil.Je7
        public void error(String str, String str2, Object obj) {
            this.a.error(str, str2, obj);
        }

        @DexIgnore
        @Override // com.fossil.Je7
        public void success(Object obj) {
            this.a.success(obj);
        }
    }

    @DexIgnore
    public He7(MethodCall methodCall, MethodChannel.Result result) {
        this.a = methodCall;
        this.b = new Ai(this, result);
    }

    @DexIgnore
    @Override // com.fossil.Ie7
    public <T> T a(String str) {
        return (T) this.a.argument(str);
    }

    @DexIgnore
    @Override // com.fossil.De7
    public Je7 i() {
        return this.b;
    }
}
