package com.fossil;

import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dp5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public List<Oo5> e;
    @DexIgnore
    public Ub7 f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;

    @DexIgnore
    public Dp5(String str, boolean z, boolean z2, String str2, List<Oo5> list, Ub7 ub7, String str3, String str4) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        this.a = str;
        this.b = z;
        this.c = z2;
        this.d = str2;
        this.e = list;
        this.f = ub7;
        this.g = str3;
        this.h = str4;
    }

    @DexIgnore
    public final List<Oo5> a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.h;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Dp5) {
                Dp5 dp5 = (Dp5) obj;
                if (!Wg6.a(this.a, dp5.a) || this.b != dp5.b || this.c != dp5.c || !Wg6.a(this.d, dp5.d) || !Wg6.a(this.e, dp5.e) || !Wg6.a(this.f, dp5.f) || !Wg6.a(this.g, dp5.g) || !Wg6.a(this.h, dp5.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Ub7 f() {
        return this.f;
    }

    @DexIgnore
    public final boolean g() {
        return this.b;
    }

    @DexIgnore
    public final boolean h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1;
        int i2 = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        boolean z2 = this.c;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        String str2 = this.d;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        List<Oo5> list = this.e;
        int hashCode3 = list != null ? list.hashCode() : 0;
        Ub7 ub7 = this.f;
        int hashCode4 = ub7 != null ? ub7.hashCode() : 0;
        String str3 = this.g;
        int hashCode5 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.h;
        if (str4 != null) {
            i2 = str4.hashCode();
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((hashCode * 31) + i3) * 31) + i) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "UIDianaPreset(id=" + this.a + ", isActive=" + this.b + ", isEditable=" + this.c + ", name=" + this.d + ", buttons=" + this.e + ", uiThemeData=" + this.f + ", previewUrl=" + this.g + ", downloadUrl=" + this.h + ")";
    }
}
