package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.G72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uh2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Uh2> CREATOR; // = new Mi2();
    @DexIgnore
    public static /* final */ int[] j; // = new int[0];
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ G72 e;
    @DexIgnore
    public /* final */ Ii2 f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ int[] h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public DataType a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public String c;
        @DexIgnore
        public G72 d;
        @DexIgnore
        public Ii2 e;
        @DexIgnore
        public String f; // = "";
        @DexIgnore
        public int[] g;

        @DexIgnore
        public final Uh2 a() {
            boolean z = true;
            Rc2.o(this.a != null, "Must set data type");
            if (this.b < 0) {
                z = false;
            }
            Rc2.o(z, "Must set data source type");
            return new Uh2(this);
        }

        @DexIgnore
        public final Ai b(Context context) {
            c(context.getPackageName());
            return this;
        }

        @DexIgnore
        public final Ai c(String str) {
            this.e = Ii2.f(str);
            return this;
        }

        @DexIgnore
        public final Ai d(DataType dataType) {
            this.a = dataType;
            return this;
        }

        @DexIgnore
        public final Ai e(G72 g72) {
            this.d = g72;
            return this;
        }

        @DexIgnore
        @Deprecated
        public final Ai f(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        public final Ai g(int i) {
            this.b = i;
            return this;
        }
    }

    @DexIgnore
    public Uh2(Ai ai) {
        this.b = ai.a;
        this.d = ai.b;
        this.c = ai.c;
        this.e = ai.d;
        this.f = ai.e;
        this.g = ai.f;
        this.i = q0();
        this.h = ai.g;
    }

    @DexIgnore
    public Uh2(DataType dataType, String str, int i2, G72 g72, Ii2 ii2, String str2, int[] iArr) {
        this.b = dataType;
        this.d = i2;
        this.c = str;
        this.e = g72;
        this.f = ii2;
        this.g = str2;
        this.i = q0();
        this.h = iArr == null ? j : iArr;
    }

    @DexIgnore
    public String A() {
        return this.i;
    }

    @DexIgnore
    public String D() {
        return this.g;
    }

    @DexIgnore
    public int F() {
        return this.d;
    }

    @DexIgnore
    public final String L() {
        return this.d != 0 ? "derived" : OrmLiteConfigUtil.RAW_DIR_NAME;
    }

    @DexIgnore
    @Deprecated
    public int[] c() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Uh2)) {
            return false;
        }
        return this.i.equals(((Uh2) obj).i);
    }

    @DexIgnore
    public DataType f() {
        return this.b;
    }

    @DexIgnore
    public G72 h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return this.i.hashCode();
    }

    @DexIgnore
    @Deprecated
    public String k() {
        return this.c;
    }

    @DexIgnore
    public final String o0() {
        String concat;
        String str;
        int i2 = this.d;
        String str2 = i2 != 0 ? i2 != 1 ? "?" : "d" : "r";
        String D = this.b.D();
        Ii2 ii2 = this.f;
        String str3 = "";
        if (ii2 == null) {
            concat = "";
        } else if (ii2.equals(Ii2.c)) {
            concat = ":gms";
        } else {
            String valueOf = String.valueOf(this.f.c());
            concat = valueOf.length() != 0 ? ":".concat(valueOf) : new String(":");
        }
        G72 g72 = this.e;
        if (g72 != null) {
            String f2 = g72.f();
            String A = this.e.A();
            StringBuilder sb = new StringBuilder(String.valueOf(f2).length() + 2 + String.valueOf(A).length());
            sb.append(":");
            sb.append(f2);
            sb.append(":");
            sb.append(A);
            str = sb.toString();
        } else {
            str = "";
        }
        String str4 = this.g;
        if (str4 != null) {
            String valueOf2 = String.valueOf(str4);
            str3 = valueOf2.length() != 0 ? ":".concat(valueOf2) : new String(":");
        }
        StringBuilder sb2 = new StringBuilder(str2.length() + 1 + String.valueOf(D).length() + String.valueOf(concat).length() + String.valueOf(str).length() + String.valueOf(str3).length());
        sb2.append(str2);
        sb2.append(":");
        sb2.append(D);
        sb2.append(concat);
        sb2.append(str);
        sb2.append(str3);
        return sb2.toString();
    }

    @DexIgnore
    public final Ii2 p0() {
        return this.f;
    }

    @DexIgnore
    public final String q0() {
        StringBuilder sb = new StringBuilder();
        sb.append(L());
        sb.append(":");
        sb.append(this.b.f());
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f.c());
        }
        if (this.e != null) {
            sb.append(":");
            sb.append(this.e.h());
        }
        if (this.g != null) {
            sb.append(":");
            sb.append(this.g);
        }
        return sb.toString();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("DataSource{");
        sb.append(L());
        if (this.c != null) {
            sb.append(":");
            sb.append(this.c);
        }
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f);
        }
        if (this.e != null) {
            sb.append(":");
            sb.append(this.e);
        }
        if (this.g != null) {
            sb.append(":");
            sb.append(this.g);
        }
        sb.append(":");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, f(), i2, false);
        Bd2.u(parcel, 2, k(), false);
        Bd2.n(parcel, 3, F());
        Bd2.t(parcel, 4, h(), i2, false);
        Bd2.t(parcel, 5, this.f, i2, false);
        Bd2.u(parcel, 6, D(), false);
        Bd2.o(parcel, 8, c(), false);
        Bd2.b(parcel, a2);
    }
}
