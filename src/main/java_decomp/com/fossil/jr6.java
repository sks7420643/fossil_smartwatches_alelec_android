package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jr6 implements Factory<CustomizeActiveCaloriesChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Jr6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Jr6 a(Provider<ThemeRepository> provider) {
        return new Jr6(provider);
    }

    @DexIgnore
    public static CustomizeActiveCaloriesChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeActiveCaloriesChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeActiveCaloriesChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
