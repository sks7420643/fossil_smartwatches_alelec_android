package com.fossil;

import com.mapped.Af6;
import com.mapped.Xe6;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xv7<T> extends Tz7<T> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater f; // = AtomicIntegerFieldUpdater.newUpdater(Xv7.class, "_decision");
    @DexIgnore
    public volatile int _decision; // = 0;

    @DexIgnore
    public Xv7(Af6 af6, Xe6<? super T> xe6) {
        super(af6, xe6);
    }

    @DexIgnore
    public final Object A0() {
        if (C0()) {
            return Yn7.d();
        }
        Object h = Gx7.h(Q());
        if (!(h instanceof Vu7)) {
            return h;
        }
        throw ((Vu7) h).a;
    }

    @DexIgnore
    public final boolean B0() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!f.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean C0() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!f.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Tz7, com.fossil.Fx7
    public void p(Object obj) {
        u0(obj);
    }

    @DexIgnore
    @Override // com.fossil.Tz7, com.fossil.Au7
    public void u0(Object obj) {
        if (!B0()) {
            Wv7.b(Xn7.c(this.e), Wu7.a(obj, this.e));
        }
    }
}
