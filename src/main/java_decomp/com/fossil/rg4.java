package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Rg4 implements Runnable {
    @DexIgnore
    public /* final */ Tg4 b;

    @DexIgnore
    public Rg4(Tg4 tg4) {
        this.b = tg4;
    }

    @DexIgnore
    public static Runnable a(Tg4 tg4) {
        return new Rg4(tg4);
    }

    @DexIgnore
    public void run() {
        this.b.g(false);
    }
}
