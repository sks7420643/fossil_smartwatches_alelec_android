package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qe7 {
    @DexIgnore
    public Context a; // = null;

    @DexIgnore
    public Qe7(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void a(Ne7 ne7) {
        if (ne7 != null) {
            String ne72 = ne7.toString();
            if (c()) {
                b(Se7.i(ne72));
            }
        }
    }

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public final Ne7 e() {
        String h = c() ? Se7.h(d()) : null;
        if (h != null) {
            return Ne7.b(h);
        }
        return null;
    }
}
