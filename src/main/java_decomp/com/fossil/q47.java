package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.facebook.GraphRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q47 implements Tf7 {
    @DexIgnore
    public static volatile Q47 e;
    @DexIgnore
    public Ai b;
    @DexIgnore
    public Sf7 c;
    @DexIgnore
    public String d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        Object c();  // void declaration

        @DexIgnore
        void d(String str);
    }

    @DexIgnore
    public static Q47 i() {
        Q47 q47;
        synchronized (Q47.class) {
            try {
                if (e == null) {
                    synchronized (Q47.class) {
                        try {
                            if (e == null) {
                                e = new Q47();
                            }
                        } catch (Throwable th) {
                            throw th;
                        }
                    }
                }
                q47 = e;
            } finally {
            }
        }
        return q47;
    }

    @DexIgnore
    @Override // com.fossil.Tf7
    public void a(Df7 df7) {
    }

    @DexIgnore
    @Override // com.fossil.Tf7
    public void b(Ef7 ef7) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, onResp...");
        int i = ef7.a;
        if (i == -4) {
            f(ef7);
        } else if (i == -2) {
            e(ef7);
        } else if (i != 0) {
            Ai ai = this.b;
            if (ai != null) {
                ai.a();
            }
        } else {
            g(ef7);
        }
    }

    @DexIgnore
    public void c(Intent intent, Tf7 tf7) {
        Sf7 sf7 = this.c;
        if (sf7 != null) {
            sf7.a(intent, tf7);
        }
    }

    @DexIgnore
    public void d(Context context) {
        Sf7 a2 = Vf7.a(context, this.d, false);
        this.c = a2;
        a2.c(this.d);
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, initialize");
    }

    @DexIgnore
    public final void e(Ef7 ef7) {
        if (ef7.b() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize canceled!");
            Ai ai = this.b;
            if (ai != null) {
                ai.c();
            }
        }
    }

    @DexIgnore
    public final void f(Ef7 ef7) {
        if (ef7.b() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize denied!");
            Ai ai = this.b;
            if (ai != null) {
                ai.a();
            }
        }
    }

    @DexIgnore
    public final void g(Ef7 ef7) {
        if (ef7.b() == 1) {
            Nf7 nf7 = (Nf7) ef7;
            if (nf7.d.equals("com.fossil.wearables.fossil.tag_wechat_login")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = GraphRequest.TAG;
                local.d(str, "Wechat authorize succeed, data = " + nf7.c + ", openid: " + nf7.b);
                Ai ai = this.b;
                if (ai != null) {
                    ai.d(nf7.c);
                }
            }
        }
    }

    @DexIgnore
    public void h(String str) {
        this.d = str;
    }

    @DexIgnore
    public void j(Ai ai) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, login...");
        this.b = ai;
        if (!this.c.b()) {
            this.b.b();
            return;
        }
        Mf7 mf7 = new Mf7();
        mf7.c = "snsapi_userinfo";
        mf7.d = "com.fossil.wearables.fossil.tag_wechat_login";
        if (!this.c.d(mf7)) {
            this.b.c();
        }
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, start authorize...");
    }
}
