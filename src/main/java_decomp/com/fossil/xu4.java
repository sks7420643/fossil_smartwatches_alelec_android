package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu4 extends ts0 {
    @DexIgnore
    public static /* final */ String q;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<cl7<Boolean, Boolean>> f4178a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<List<ot4>, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<gs4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Date>> f; // = new MutableLiveData<>();
    @DexIgnore
    public ts4 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public List<String> i; // = new ArrayList();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<List<ot4>, String>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<ps4, Long, String>> k; // = new MutableLiveData<>();
    @DexIgnore
    public List<ot4> l;
    @DexIgnore
    public String m; // = "now";
    @DexIgnore
    public /* final */ zt4 n;
    @DexIgnore
    public /* final */ tt4 o;
    @DexIgnore
    public /* final */ on5 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1", f = "BCInviteFriendViewModel.kt", l = {287, 296}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xu4$a$a")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1$friendsWrapper$1", f = "BCInviteFriendViewModel.kt", l = {287}, m = "invokeSuspend")
        /* renamed from: com.fossil.xu4$a$a  reason: collision with other inner class name */
        public static final class C0288a extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends xs4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0288a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0288a aVar = new C0288a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends xs4>>> qn7) {
                throw null;
                //return ((C0288a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    zt4 zt4 = this.this$0.this$0.n;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object g = zt4.g(this);
                    return g == d ? d : g;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1$localFriends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<xs4>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.n.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xu4 xu4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0089  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00c7  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 250
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xu4.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1", f = "BCInviteFriendViewModel.kt", l = {310, 311, 312, 313, 319, 325, 328}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1$friends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<xs4>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.n.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xu4$b$b")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1$suggestedFriends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.xu4$b$b  reason: collision with other inner class name */
        public static final class C0289b extends ko7 implements vp7<iv7, qn7<? super List<? extends ot4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $combinationFriends;
            @DexIgnore
            public /* final */ /* synthetic */ List $friends;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0289b(List list, List list2, qn7 qn7) {
                super(2, qn7);
                this.$friends = list;
                this.$combinationFriends = list2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0289b bVar = new C0289b(this.$friends, this.$combinationFriends, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends ot4>> qn7) {
                throw null;
                //return ((C0289b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return py4.m(this.$friends, this.$combinationFriends);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xu4 xu4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r5v19, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r5v23, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x00e7  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0148  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0180  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x01cf  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x020a  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x022b  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0298  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x029c  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x02f3  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0344  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x0359  */
        /* JADX WARNING: Unknown variable types count: 2 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r21) {
            /*
            // Method dump skipped, instructions count: 886
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xu4.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel", f = "BCInviteFriendViewModel.kt", l = {335, 335}, m = "loadInvitedPlayers")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xu4 xu4, qn7 qn7) {
            super(qn7);
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.A(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super rv7<? extends cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xu4$d$a$a")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1$joined$1", f = "BCInviteFriendViewModel.kt", l = {338}, m = "invokeSuspend")
            /* renamed from: com.fossil.xu4$d$a$a  reason: collision with other inner class name */
            public static final class C0290a extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0290a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0290a aVar = new C0290a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ms4>>> qn7) {
                    throw null;
                    //return ((C0290a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        tt4 p = this.this$0.this$0.this$0.p();
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = iv7;
                            this.label = 1;
                            Object t = p.t(str, new String[]{"waiting"}, this);
                            return t == d ? d : t;
                        }
                        pq7.i();
                        throw null;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1$pending$1", f = "BCInviteFriendViewModel.kt", l = {337}, m = "invokeSuspend")
            public static final class b extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ms4>>> qn7) {
                    throw null;
                    //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        tt4 p = this.this$0.this$0.this$0.p();
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = iv7;
                            this.label = 1;
                            Object t = p.t(str, new String[]{"invited"}, this);
                            return t == d ? d : t;
                        }
                        pq7.i();
                        throw null;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    return hl7.a(gu7.b(iv7, null, null, new b(this, null), 3, null), gu7.b(iv7, null, null, new C0290a(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xu4 xu4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$challengeId, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super rv7<? extends cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>>> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return gu7.b(this.p$, bw7.b(), null, new a(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    if (this.this$0.this$0.h) {
                        cl7 cl7 = (cl7) this.this$0.this$0.d.e();
                        List<ot4> list = cl7 != null ? (List) cl7.getFirst() : null;
                        if (!(list == null || list.isEmpty())) {
                            ts4 a2 = xu4.a(this.this$0.this$0);
                            ArrayList arrayList = new ArrayList(im7.m(list, 10));
                            for (ot4 ot4 : list) {
                                arrayList.add(ot4.b());
                            }
                            Object[] array = arrayList.toArray(new String[0]);
                            if (array != null) {
                                a2.q((String[]) array);
                            } else {
                                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = xu4.q;
                    local.e(str, "onInviteMoreFriends - launch - ids: " + xu4.a(this.this$0.this$0).d());
                    tt4 p = this.this$0.this$0.p();
                    e eVar = this.this$0;
                    String str2 = eVar.$it;
                    String[] d2 = xu4.a(eVar.this$0).d();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object I = p.I(str2, d2, this);
                    return I == d ? d : I;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, qn7 qn7, xu4 xu4) {
            super(2, qn7);
            this.$it = str;
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$it, qn7, this.this$0);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.f4178a.l(hl7.a(null, ao7.a(true)));
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) g;
            this.this$0.f4178a.l(hl7.a(null, ao7.a(false)));
            if (kz4.c() != null) {
                this.this$0.c.l(hl7.a(ao7.a(true), null));
            } else {
                this.this$0.c.l(hl7.a(ao7.a(false), kz4.a()));
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onSearching$1", f = "BCInviteFriendViewModel.kt", l = {232}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $key;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onSearching$1$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $searchingFriends;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$searchingFriends = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$searchingFriends, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.j.l(hl7.a(this.$searchingFriends, this.this$0.$key));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(xu4 xu4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
            this.$key = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$key, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:31:0x008c  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x004a A[SYNTHETIC] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                r11 = 0
                r4 = 0
                r5 = 1
                java.lang.Object r6 = com.fossil.yn7.d()
                int r0 = r12.label
                if (r0 == 0) goto L_0x0023
                if (r0 != r5) goto L_0x001b
                java.lang.Object r0 = r12.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r12.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r13)
            L_0x0018:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                com.fossil.el7.b(r13)
                com.fossil.iv7 r7 = r12.p$
                com.fossil.xu4 r0 = r12.this$0
                java.util.List r0 = com.fossil.xu4.c(r0)
                if (r0 == 0) goto L_0x0036
                boolean r0 = r0.isEmpty()
                if (r0 == 0) goto L_0x0090
            L_0x0036:
                r0 = r5
            L_0x0037:
                if (r0 != 0) goto L_0x0018
                com.fossil.xu4 r0 = r12.this$0
                java.util.List r0 = com.fossil.xu4.c(r0)
                if (r0 == 0) goto L_0x00af
                java.util.ArrayList r8 = new java.util.ArrayList
                r8.<init>()
                java.util.Iterator r9 = r0.iterator()
            L_0x004a:
                boolean r0 = r9.hasNext()
                if (r0 == 0) goto L_0x0097
                java.lang.Object r1 = r9.next()
                r0 = r1
                com.fossil.ot4 r0 = (com.fossil.ot4) r0
                java.lang.String r2 = r0.a()
                java.lang.String r3 = ""
                if (r2 == 0) goto L_0x0092
            L_0x005f:
                java.lang.String r10 = r12.$key
                boolean r2 = com.fossil.wt7.u(r2, r10, r5)
                if (r2 != 0) goto L_0x0081
                java.lang.String r2 = r0.c()
                if (r2 == 0) goto L_0x00b3
            L_0x006d:
                java.lang.String r3 = r12.$key
                boolean r2 = com.fossil.wt7.u(r2, r3, r5)
                if (r2 != 0) goto L_0x0081
                java.lang.String r0 = r0.e()
                java.lang.String r2 = r12.$key
                boolean r0 = com.fossil.wt7.u(r0, r2, r5)
                if (r0 == 0) goto L_0x0095
            L_0x0081:
                r0 = r5
            L_0x0082:
                java.lang.Boolean r0 = com.fossil.ao7.a(r0)
                boolean r0 = r0.booleanValue()
                if (r0 == 0) goto L_0x004a
                r8.add(r1)
                goto L_0x004a
            L_0x0090:
                r0 = r4
                goto L_0x0037
            L_0x0092:
                java.lang.String r2 = ""
                goto L_0x005f
            L_0x0095:
                r0 = r4
                goto L_0x0082
            L_0x0097:
                com.fossil.jx7 r0 = com.fossil.bw7.c()
                com.fossil.xu4$f$a r1 = new com.fossil.xu4$f$a
                r1.<init>(r12, r8, r11)
                r12.L$0 = r7
                r12.L$1 = r8
                r12.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r0, r1, r12)
                if (r0 != r6) goto L_0x0018
                r0 = r6
                goto L_0x001a
            L_0x00af:
                com.fossil.pq7.i()
                throw r11
            L_0x00b3:
                r2 = r3
                goto L_0x006d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xu4.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onStartLater$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(xu4 xu4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.f.l(qy4.f3046a.b());
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onStartNow$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(xu4 xu4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.e.l(qy4.f3046a.h());
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1", f = "BCInviteFriendViewModel.kt", l = {261, 272}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xu4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ps4 $challenge;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, ps4 ps4, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
                this.$challenge = ps4;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$challenge, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Long f;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.p().e();
                    String p = this.$challenge.p();
                    if (p != null && (f = ao7.f(PortfolioApp.h0.c().m1(p))) != null) {
                        return f;
                    }
                    this.this$0.this$0.v().m0(ao7.a(true));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1$result$1", f = "BCInviteFriendViewModel.kt", l = {261}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, int i, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    tt4 p = this.this$0.this$0.p();
                    ts4 a2 = xu4.a(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object f = p.f(a2, i2, str, this);
                    return f == d ? d : f;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(xu4 xu4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, qn7);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006d  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0131  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 333
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xu4.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = xu4.class.getSimpleName();
        pq7.b(simpleName, "BCInviteFriendViewModel::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public xu4(zt4 zt4, tt4 tt4, on5 on5) {
        pq7.c(zt4, "friendRepository");
        pq7.c(tt4, "challengeRepository");
        pq7.c(on5, "shared");
        this.n = zt4;
        this.o = tt4;
        this.p = on5;
    }

    @DexIgnore
    public static final /* synthetic */ ts4 a(xu4 xu4) {
        ts4 ts4 = xu4.g;
        if (ts4 != null) {
            return ts4;
        }
        pq7.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object A(java.lang.String r8, com.fossil.qn7<? super com.fossil.cl7<? extends com.fossil.rv7<com.fossil.kz4<java.util.List<com.fossil.ms4>>>, ? extends com.fossil.rv7<com.fossil.kz4<java.util.List<com.fossil.ms4>>>>> r9) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.xu4.c
            if (r0 == 0) goto L_0x002f
            r0 = r9
            com.fossil.xu4$c r0 = (com.fossil.xu4.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002f
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x005b
            if (r0 == r5) goto L_0x003e
            if (r0 != r6) goto L_0x0036
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$0
            com.fossil.xu4 r0 = (com.fossil.xu4) r0
            com.fossil.el7.b(r2)
        L_0x002e:
            return r2
        L_0x002f:
            com.fossil.xu4$c r0 = new com.fossil.xu4$c
            r0.<init>(r7, r9)
            r3 = r0
            goto L_0x0015
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.fossil.xu4 r1 = (com.fossil.xu4) r1
            com.fossil.el7.b(r2)
            r8 = r0
        L_0x004a:
            r0 = r2
            com.fossil.rv7 r0 = (com.fossil.rv7) r0
            r3.L$0 = r1
            r3.L$1 = r8
            r3.label = r6
            java.lang.Object r2 = r0.l(r3)
            if (r2 != r4) goto L_0x002e
            r2 = r4
            goto L_0x002e
        L_0x005b:
            com.fossil.el7.b(r2)
            com.fossil.xu4$d r0 = new com.fossil.xu4$d
            r1 = 0
            r0.<init>(r7, r8, r1)
            r3.L$0 = r7
            r3.L$1 = r8
            r3.label = r5
            java.lang.Object r2 = com.fossil.ux7.c(r0, r3)
            if (r2 != r4) goto L_0x0072
            r2 = r4
            goto L_0x002e
        L_0x0072:
            r1 = r7
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xu4.A(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void B() {
        List<ot4> list = this.l;
        if (list != null) {
            this.d.l(hl7.a(list, null));
        }
        this.l = null;
    }

    @DexIgnore
    public final void C() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        StringBuilder sb = new StringBuilder();
        sb.append("onInviteMoreFriends - ids: ");
        ts4 ts4 = this.g;
        if (ts4 != null) {
            sb.append(ts4.d());
            sb.append(" - cachedInvitedAll: ");
            sb.append(this.h);
            local.e(str, sb.toString());
            ts4 ts42 = this.g;
            if (ts42 != null) {
                if (!(ts42.d().length == 0) || this.h) {
                    ts4 ts43 = this.g;
                    if (ts43 != null) {
                        String c2 = ts43.c();
                        if (c2 != null) {
                            xw7 unused = gu7.d(us0.a(this), null, null, new e(c2, null, this), 3, null);
                            return;
                        }
                        return;
                    }
                    pq7.n("cachedDraft");
                    throw null;
                }
                this.c.l(hl7.a(Boolean.TRUE, null));
                return;
            }
            pq7.n("cachedDraft");
            throw null;
        }
        pq7.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void D(boolean z) {
        if (this.h != z) {
            cl7<List<ot4>, ServerError> e2 = this.d.e();
            List<ot4> first = e2 != null ? e2.getFirst() : null;
            this.h = z;
            if (z) {
                if (first != null) {
                    Iterator<T> it = first.iterator();
                    while (it.hasNext()) {
                        it.next().g(true);
                    }
                }
                ts4 ts4 = this.g;
                if (ts4 != null) {
                    ts4.r(true);
                } else {
                    pq7.n("cachedDraft");
                    throw null;
                }
            } else {
                if (this.i.isEmpty()) {
                    if (first != null) {
                        Iterator<T> it2 = first.iterator();
                        while (it2.hasNext()) {
                            it2.next().g(false);
                        }
                    }
                } else if (first != null) {
                    for (T t : first) {
                        t.g(this.i.contains(t.b()));
                    }
                }
                ts4 ts42 = this.g;
                if (ts42 != null) {
                    ts42.r(false);
                } else {
                    pq7.n("cachedDraft");
                    throw null;
                }
            }
            if (first != null) {
                this.d.l(hl7.a(first, null));
            }
        }
    }

    @DexIgnore
    public final void E(ot4 ot4) {
        int i2 = 0;
        pq7.c(ot4, "friend");
        if (!ot4.f() && this.h) {
            this.h = false;
            ts4 ts4 = this.g;
            if (ts4 != null) {
                ts4.r(false);
                this.b.l(Boolean.FALSE);
                cl7<List<ot4>, ServerError> e2 = this.d.e();
                List<ot4> first = e2 != null ? e2.getFirst() : null;
                if (first != null) {
                    this.i.clear();
                    Iterator<T> it = first.iterator();
                    while (it.hasNext()) {
                        this.i.add(it.next().b());
                    }
                }
            } else {
                pq7.n("cachedDraft");
                throw null;
            }
        }
        if (ot4.f()) {
            if (!this.i.contains(ot4.b())) {
                this.i.add(ot4.b());
            }
        } else {
            this.i.remove(ot4.b());
        }
        ts4 ts42 = this.g;
        if (ts42 != null) {
            Object[] array = this.i.toArray(new String[0]);
            if (array != null) {
                ts42.q((String[]) array);
                List<ot4> list = this.l;
                if (list != null) {
                    Iterator<ot4> it2 = list.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            i2 = -1;
                            break;
                        } else if (pq7.a(it2.next().b(), ot4.b())) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        list.get(i2).g(ot4.f());
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = q;
                StringBuilder sb = new StringBuilder();
                sb.append("cachedDraft: ");
                ts4 ts43 = this.g;
                if (ts43 != null) {
                    sb.append(ts43);
                    local.e(str, sb.toString());
                    return;
                }
                pq7.n("cachedDraft");
                throw null;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        pq7.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void F() {
        ts4 ts4 = this.g;
        if (ts4 != null) {
            if (ts4.c() != null) {
                o();
            } else {
                n();
            }
        } else {
            pq7.n("cachedDraft");
            throw null;
        }
    }

    @DexIgnore
    public final void G(String str) {
        pq7.c(str, "key");
        cl7<List<ot4>, ServerError> e2 = this.d.e();
        this.l = e2 != null ? e2.getFirst() : null;
        xw7 unused = gu7.d(us0.a(this), bw7.a(), null, new f(this, str, null), 2, null);
    }

    @DexIgnore
    public final void H() {
        this.m = "later";
        xw7 unused = gu7.d(us0.a(this), bw7.a(), null, new g(this, null), 2, null);
    }

    @DexIgnore
    public final void I() {
        this.m = "now";
        xw7 unused = gu7.d(us0.a(this), bw7.a(), null, new h(this, null), 2, null);
    }

    @DexIgnore
    public final void J(Date date) {
        pq7.c(date, "date");
        SimpleDateFormat simpleDateFormat = lk5.p.get();
        String format = simpleDateFormat != null ? simpleDateFormat.format(date) : null;
        if (format != null) {
            K(format);
        }
    }

    @DexIgnore
    public final void K(String str) {
        ts4 ts4 = this.g;
        if (ts4 != null) {
            ts4.u(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = q;
            StringBuilder sb = new StringBuilder();
            sb.append("startChallenge - draft: ");
            ts4 ts42 = this.g;
            if (ts42 != null) {
                sb.append(ts42);
                local.e(str2, sb.toString());
                this.f4178a.l(hl7.a(null, Boolean.TRUE));
                xw7 unused = gu7.d(us0.a(this), null, null, new i(this, null), 3, null);
                return;
            }
            pq7.n("cachedDraft");
            throw null;
        }
        pq7.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void n() {
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final void o() {
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final tt4 p() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<gl7<ps4, Long, String>> q() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Date>> s() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, Boolean>> t() {
        return this.f4178a;
    }

    @DexIgnore
    public final LiveData<cl7<List<ot4>, String>> u() {
        return this.j;
    }

    @DexIgnore
    public final on5 v() {
        return this.p;
    }

    @DexIgnore
    public final LiveData<List<gs4>> w() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> x() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<cl7<List<ot4>, ServerError>> y() {
        return this.d;
    }

    @DexIgnore
    public final void z(ts4 ts4) {
        boolean z = false;
        String str = null;
        this.g = ts4 != null ? ts4 : new ts4(null, null, null, null, null, 0, 0, null, null, false, null, 2047, null);
        if (ts4 != null) {
            str = ts4.c();
        }
        if (str != null) {
            z = true;
        }
        if (z) {
            o();
        } else {
            n();
        }
    }
}
