package com.fossil;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C18 {
    @DexIgnore
    public static /* final */ C18 c; // = new Ai().a();
    @DexIgnore
    public /* final */ Set<Bi> a;
    @DexIgnore
    public /* final */ A48 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ List<Bi> a; // = new ArrayList();

        @DexIgnore
        public C18 a() {
            return new C18(new LinkedHashSet(this.a), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ L48 d;

        @DexIgnore
        public boolean a(String str) {
            if (!this.a.startsWith("*.")) {
                return str.equals(this.b);
            }
            int indexOf = str.indexOf(46);
            if ((str.length() - indexOf) - 1 != this.b.length()) {
                return false;
            }
            String str2 = this.b;
            return str.regionMatches(false, indexOf + 1, str2, 0, str2.length());
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof Bi) {
                Bi bi = (Bi) obj;
                return this.a.equals(bi.a) && this.c.equals(bi.c) && this.d.equals(bi.d);
            }
        }

        @DexIgnore
        public int hashCode() {
            return ((((this.a.hashCode() + 527) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.c + this.d.base64();
        }
    }

    @DexIgnore
    public C18(Set<Bi> set, A48 a48) {
        this.a = set;
        this.b = a48;
    }

    @DexIgnore
    public static String c(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + e((X509Certificate) certificate).base64();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    @DexIgnore
    public static L48 d(X509Certificate x509Certificate) {
        return L48.of(x509Certificate.getPublicKey().getEncoded()).sha1();
    }

    @DexIgnore
    public static L48 e(X509Certificate x509Certificate) {
        return L48.of(x509Certificate.getPublicKey().getEncoded()).sha256();
    }

    @DexIgnore
    public void a(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List<Bi> b2 = b(str);
        if (!b2.isEmpty()) {
            A48 a48 = this.b;
            if (a48 != null) {
                list = a48.a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = b2.size();
                int i2 = 0;
                L48 l48 = null;
                L48 l482 = null;
                while (i2 < size2) {
                    Bi bi = b2.get(i2);
                    if (bi.c.equals("sha256/")) {
                        if (l48 == null) {
                            l48 = e(x509Certificate);
                        }
                        if (bi.d.equals(l48)) {
                            return;
                        }
                    } else if (bi.c.equals("sha1/")) {
                        if (l482 == null) {
                            l482 = d(x509Certificate);
                        }
                        if (bi.d.equals(l482)) {
                            return;
                        }
                    } else {
                        throw new AssertionError("unsupported hashAlgorithm: " + bi.c);
                    }
                    i2++;
                    l48 = l48;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Certificate pinning failure!");
            sb.append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                sb.append("\n    ");
                sb.append(c(x509Certificate2));
                sb.append(": ");
                sb.append(x509Certificate2.getSubjectDN().getName());
            }
            sb.append("\n  Pinned certificates for ");
            sb.append(str);
            sb.append(":");
            int size4 = b2.size();
            for (int i4 = 0; i4 < size4; i4++) {
                sb.append("\n    ");
                sb.append(b2.get(i4));
            }
            throw new SSLPeerUnverifiedException(sb.toString());
        }
    }

    @DexIgnore
    public List<Bi> b(String str) {
        List<Bi> emptyList = Collections.emptyList();
        List<Bi> list = emptyList;
        for (Bi bi : this.a) {
            if (bi.a(str)) {
                if (list.isEmpty()) {
                    list = new ArrayList<>();
                }
                list.add(bi);
            }
        }
        return list;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C18) {
            C18 c18 = (C18) obj;
            return B28.q(this.b, c18.b) && this.a.equals(c18.a);
        }
    }

    @DexIgnore
    public C18 f(A48 a48) {
        return B28.q(this.b, a48) ? this : new C18(this.a, a48);
    }

    @DexIgnore
    public int hashCode() {
        A48 a48 = this.b;
        return ((a48 != null ? a48.hashCode() : 0) * 31) + this.a.hashCode();
    }
}
