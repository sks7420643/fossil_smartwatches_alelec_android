package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I76 implements Factory<WatchAppEditPresenter> {
    @DexIgnore
    public static WatchAppEditPresenter a(E76 e76, SetWatchAppUseCase setWatchAppUseCase, An4 an4) {
        return new WatchAppEditPresenter(e76, setWatchAppUseCase, an4);
    }
}
