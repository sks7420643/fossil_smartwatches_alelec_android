package com.fossil;

import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Zd0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dv1 {
    @DexIgnore
    public ArrayList<Zd0> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<Zd0, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Mo1 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Mo1 mo1) {
            super(1);
            this.b = mo1;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public Boolean invoke(Zd0 zd0) {
            return Boolean.valueOf(this.b == zd0.getNotificationType());
        }
    }

    @DexIgnore
    public final Zd0[] a() {
        Object[] array = this.a.toArray(new Zd0[0]);
        if (array != null) {
            return (Zd0[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Dv1 b(Mo1 mo1, Ev1 ev1) {
        synchronized (this) {
            Mm7.w(this.a, new Ai(mo1));
            if (ev1 != null) {
                if (!(ev1.getReplyMessages().length == 0)) {
                    this.a.add(new Zd0(mo1, ev1));
                }
            }
        }
        return this;
    }
}
