package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ab0 implements Parcelable.Creator<Bb0> {
    @DexIgnore
    public /* synthetic */ Ab0(Qg6 qg6) {
    }

    @DexIgnore
    public Bb0 a(Parcel parcel) {
        return new Bb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Bb0 createFromParcel(Parcel parcel) {
        return new Bb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Bb0[] newArray(int i) {
        return new Bb0[i];
    }
}
