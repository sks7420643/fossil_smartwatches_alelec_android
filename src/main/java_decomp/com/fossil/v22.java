package com.fossil;

import android.database.Cursor;
import com.fossil.J32;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class V22 implements J32.Bi {
    @DexIgnore
    public /* final */ J32 a;
    @DexIgnore
    public /* final */ List b;
    @DexIgnore
    public /* final */ H02 c;

    @DexIgnore
    public V22(J32 j32, List list, H02 h02) {
        this.a = j32;
        this.b = list;
        this.c = h02;
    }

    @DexIgnore
    public static J32.Bi a(J32 j32, List list, H02 h02) {
        return new V22(j32, list, h02);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return J32.S(this.a, this.b, this.c, (Cursor) obj);
    }
}
