package com.fossil;

import android.content.Context;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gf2 {
    @DexIgnore
    public static boolean a(Context context, Throwable th) {
        return b(context, th, 536870912);
    }

    @DexIgnore
    public static boolean b(Context context, Throwable th, int i) {
        try {
            Rc2.k(context);
            Rc2.k(th);
        } catch (Exception e) {
            Log.e("CrashUtils", "Error adding exception to DropBox!", e);
        }
        return false;
    }
}
