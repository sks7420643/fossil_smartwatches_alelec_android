package com.fossil;

import android.net.Uri;
import com.mapped.Wg6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M61 implements N61<Uri, File> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return c(uri);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ File b(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    public boolean c(Uri uri) {
        Wg6.c(uri, "data");
        if (Wg6.a(uri.getScheme(), "file")) {
            String f = W81.f(uri);
            if (f != null && (Wg6.a(f, "android_asset") ^ true)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public File d(Uri uri) {
        Wg6.c(uri, "data");
        return Lm0.a(uri);
    }
}
