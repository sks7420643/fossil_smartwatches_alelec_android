package com.fossil;

import java.util.concurrent.ScheduledFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Jg4 implements Ht3 {
    @DexIgnore
    public /* final */ ScheduledFuture a;

    @DexIgnore
    public Jg4(ScheduledFuture scheduledFuture) {
        this.a = scheduledFuture;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3 nt3) {
        this.a.cancel(false);
    }
}
