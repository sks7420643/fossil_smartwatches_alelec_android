package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 e;
    @DexIgnore
    public /* final */ /* synthetic */ U93 f;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 g;

    @DexIgnore
    public Xp3(Fp3 fp3, String str, String str2, boolean z, Or3 or3, U93 u93) {
        this.g = fp3;
        this.b = str;
        this.c = str2;
        this.d = z;
        this.e = or3;
        this.f = u93;
    }

    @DexIgnore
    public final void run() {
        Bundle bundle = new Bundle();
        try {
            Cl3 cl3 = this.g.d;
            if (cl3 == null) {
                this.g.d().F().c("Failed to get user properties; not connected to service", this.b, this.c);
                return;
            }
            Bundle C = Kr3.C(cl3.o1(this.b, this.c, this.d, this.e));
            this.g.e0();
            this.g.k().P(this.f, C);
        } catch (RemoteException e2) {
            this.g.d().F().c("Failed to get user properties; remote exception", this.b, e2);
        } finally {
            this.g.k().P(this.f, bundle);
        }
    }
}
