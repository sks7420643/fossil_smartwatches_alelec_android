package com.fossil;

import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Fm2 {
    @DexIgnore
    void a(Dm2 dm2, Throwable th, Object obj);

    @DexIgnore
    Object b(Dm2 dm2, Message message);

    @DexIgnore
    void c(Dm2 dm2, Message message, Object obj);

    @DexIgnore
    void d(Dm2 dm2, Message message, long j);
}
