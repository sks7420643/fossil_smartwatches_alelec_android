package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ns extends Fs {
    @DexIgnore
    public Ns(Hs hs, K5 k5, int i) {
        super(hs, k5, i);
    }

    @DexIgnore
    public abstract U5 D();

    @DexIgnore
    @Override // com.fossil.Fs
    public final U5 w() {
        return D();
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final void y() {
    }
}
