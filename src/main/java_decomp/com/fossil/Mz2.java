package com.fossil;

import com.fossil.Mz2;
import com.fossil.Nz2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mz2<MessageType extends Nz2<MessageType, BuilderType>, BuilderType extends Mz2<MessageType, BuilderType>> implements P23 {
    @DexIgnore
    @Override // com.fossil.P23
    public final /* synthetic */ P23 A(byte[] bArr) throws L13 {
        p(bArr, 0, bArr.length);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Mz2<MessageType extends com.fossil.Nz2<MessageType, BuilderType>, BuilderType extends com.fossil.Mz2<MessageType, BuilderType>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.P23
    public final /* synthetic */ P23 F(M23 m23) {
        if (d().getClass().isInstance(m23)) {
            o((Nz2) m23);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    @DexIgnore
    @Override // com.fossil.P23
    public final /* synthetic */ P23 k(byte[] bArr, Q03 q03) throws L13 {
        q(bArr, 0, bArr.length, q03);
        return this;
    }

    @DexIgnore
    public abstract BuilderType o(MessageType messagetype);

    @DexIgnore
    public abstract BuilderType p(byte[] bArr, int i, int i2) throws L13;

    @DexIgnore
    public abstract BuilderType q(byte[] bArr, int i, int i2, Q03 q03) throws L13;
}
