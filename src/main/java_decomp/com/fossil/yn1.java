package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yn1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Yn1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yn1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                byte readByte = parcel.readByte();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        Wg6.b(readString3, "parcel.readString()!!");
                        String readString4 = parcel.readString();
                        if (readString4 != null) {
                            Wg6.b(readString4, "parcel.readString()!!");
                            return new Yn1(readString, readByte, readString2, readString3, readString4);
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yn1[] newArray(int i) {
            return new Yn1[i];
        }
    }

    @DexIgnore
    public Yn1(String str, byte b2, String str2, String str3, String str4) {
        this.b = str;
        this.c = (byte) b2;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    @DexIgnore
    public final byte[] a() {
        ByteBuffer allocate = ByteBuffer.allocate(6);
        Wg6.b(allocate, "ByteBuffer.allocate(HEADER_LENGTH.toInt())");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        String str = this.b;
        Charset c2 = Hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            byte min = (byte) Math.min(bytes.length + 1, 50);
            String str2 = this.d;
            Charset c3 = Hd0.y.c();
            if (str2 != null) {
                byte[] bytes2 = str2.getBytes(c3);
                Wg6.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte min2 = (byte) Math.min(bytes2.length + 1, 50);
                String str3 = this.e;
                Charset c4 = Hd0.y.c();
                if (str3 != null) {
                    byte[] bytes3 = str3.getBytes(c4);
                    Wg6.b(bytes3, "(this as java.lang.String).getBytes(charset)");
                    byte min3 = (byte) Math.min(bytes3.length + 1, 50);
                    String str4 = this.f;
                    Charset c5 = Hd0.y.c();
                    if (str4 != null) {
                        byte[] bytes4 = str4.getBytes(c5);
                        Wg6.b(bytes4, "(this as java.lang.String).getBytes(charset)");
                        byte min4 = (byte) Math.min(bytes4.length + 1, 50);
                        short s = (short) (min + 7 + min2 + min3 + min4);
                        allocate.putShort(s);
                        allocate.put(min);
                        allocate.put(min2);
                        allocate.put(min3);
                        allocate.put(min4);
                        ByteBuffer allocate2 = ByteBuffer.allocate(s);
                        Wg6.b(allocate2, "ByteBuffer.allocate(totalLen.toInt())");
                        allocate2.order(ByteOrder.LITTLE_ENDIAN);
                        allocate2.put(allocate.array());
                        ByteBuffer allocate3 = ByteBuffer.allocate(s - 6);
                        Wg6.b(allocate3, "ByteBuffer.allocate(totalLen - HEADER_LENGTH)");
                        allocate3.order(ByteOrder.LITTLE_ENDIAN);
                        allocate3.put(this.c);
                        String str5 = this.b;
                        Charset c6 = Hd0.y.c();
                        if (str5 != null) {
                            byte[] bytes5 = str5.getBytes(c6);
                            Wg6.b(bytes5, "(this as java.lang.String).getBytes(charset)");
                            allocate3.put(Arrays.copyOfRange(bytes5, 0, min - 1)).put((byte) 0);
                            String str6 = this.d;
                            Charset c7 = Hd0.y.c();
                            if (str6 != null) {
                                byte[] bytes6 = str6.getBytes(c7);
                                Wg6.b(bytes6, "(this as java.lang.String).getBytes(charset)");
                                allocate3.put(Arrays.copyOfRange(bytes6, 0, min2 - 1)).put((byte) 0);
                                String str7 = this.e;
                                Charset c8 = Hd0.y.c();
                                if (str7 != null) {
                                    byte[] bytes7 = str7.getBytes(c8);
                                    Wg6.b(bytes7, "(this as java.lang.String).getBytes(charset)");
                                    allocate3.put(Arrays.copyOfRange(bytes7, 0, min3 - 1)).put((byte) 0);
                                    String str8 = this.f;
                                    Charset c9 = Hd0.y.c();
                                    if (str8 != null) {
                                        byte[] bytes8 = str8.getBytes(c9);
                                        Wg6.b(bytes8, "(this as java.lang.String).getBytes(charset)");
                                        allocate3.put(Arrays.copyOfRange(bytes8, 0, min4 - 1)).put((byte) 0);
                                        allocate2.put(allocate3.array());
                                        byte[] array = allocate2.array();
                                        Wg6.b(array, "trackInfoData.array()");
                                        return array;
                                    }
                                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                                }
                                throw new Rc6("null cannot be cast to non-null type java.lang.String");
                            }
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Yn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Yn1 yn1 = (Yn1) obj;
            if (!Wg6.a(this.b, yn1.b)) {
                return false;
            }
            if (this.c != yn1.c) {
                return false;
            }
            if (!Wg6.a(this.d, yn1.d)) {
                return false;
            }
            if (!Wg6.a(this.e, yn1.e)) {
                return false;
            }
            return !(Wg6.a(this.f, yn1.f) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.TrackInfo");
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.f;
    }

    @DexIgnore
    public final String getAppName() {
        return this.b;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.e;
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.d;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        byte b2 = this.c;
        int hashCode2 = this.d.hashCode();
        return (((((((hashCode * 31) + b2) * 31) + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.g, this.b), Jd0.w, Byte.valueOf(this.c)), Jd0.h, this.d), Jd0.x, this.e), Jd0.y, this.f);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f);
        }
    }
}
