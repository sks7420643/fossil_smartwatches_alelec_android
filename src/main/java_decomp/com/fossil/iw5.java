package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.An4;
import com.mapped.TimeUtils;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public /* final */ An4 a;
    @DexIgnore
    public /* final */ ArrayList<HomeProfilePresenter.Bi> b;
    @DexIgnore
    public /* final */ Wa1 c;
    @DexIgnore
    public Bi d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ImageView a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ ImageView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ CardView h;
        @DexIgnore
        public /* final */ /* synthetic */ Iw5 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                if (this.b.i.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (bi = this.b.i.d) != null) {
                    bi.M0(((HomeProfilePresenter.Bi) this.b.i.b.get(this.b.getAdapterPosition())).c());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Bii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                Wg6.c(str, "serial");
                Wg6.c(str2, "filePath");
                this.a.i.c.t(str2).F0(this.a.a);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Iw5 iw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.i = iw5;
            this.a = (ImageView) view.findViewById(2131362691);
            this.b = (ImageView) view.findViewById(2131362750);
            this.c = (FlexibleTextView) view.findViewById(2131363319);
            this.d = (FlexibleTextView) view.findViewById(2131363347);
            this.e = (ConstraintLayout) view.findViewById(2131362056);
            this.f = (ImageView) view.findViewById(2131362752);
            this.g = (FlexibleTextView) view.findViewById(2131363405);
            this.h = (CardView) view.findViewById(2131361994);
            this.f.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0076: INVOKE  (r2v13 int) = (r8v0 com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$Bi) type: VIRTUAL call: com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.Bi.a():int), ('%' char)] */
        @SuppressLint({"SetTextI18n"})
        public final void b(HomeProfilePresenter.Bi bi) {
            Wg6.c(bi, "device");
            FlexibleTextView flexibleTextView = this.c;
            Wg6.b(flexibleTextView, "mTvDeviceName");
            flexibleTextView.setText(L47.d(bi.b()));
            if (bi.d()) {
                CardView cardView = this.h;
                Wg6.b(cardView, "mCardView");
                cardView.setRadius(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                CardView cardView2 = this.h;
                Wg6.b(cardView2, "mCardView");
                cardView2.setCardElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.b.setImageResource(2131230960);
                ConstraintLayout constraintLayout = this.e;
                Wg6.b(constraintLayout, "mClContainer");
                constraintLayout.setAlpha(1.0f);
                ConstraintLayout constraintLayout2 = this.e;
                Wg6.b(constraintLayout2, "mClContainer");
                constraintLayout2.setBackground(W6.f(this.i.e, 2131230833));
                if (bi.a() >= 0) {
                    FlexibleTextView flexibleTextView2 = this.g;
                    Wg6.b(flexibleTextView2, "mTvDeviceStatus");
                    StringBuilder sb = new StringBuilder();
                    sb.append(bi.a());
                    sb.append('%');
                    flexibleTextView2.setText(sb.toString());
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, W6.f(this.i.e, DeviceHelper.o.c(bi.a())), (Drawable) null);
                } else {
                    FlexibleTextView flexibleTextView3 = this.g;
                    Wg6.b(flexibleTextView3, "mTvDeviceStatus");
                    flexibleTextView3.setText("");
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                Fk0 fk0 = new Fk0();
                ImageView imageView = this.f;
                Wg6.b(imageView, "mIvSetting");
                imageView.setVisibility(0);
                fk0.c(this.e);
                FlexibleTextView flexibleTextView4 = this.d;
                Wg6.b(flexibleTextView4, "mTvLastSyncedTime");
                int id = flexibleTextView4.getId();
                ImageView imageView2 = this.f;
                Wg6.b(imageView2, "mIvSetting");
                fk0.e(id, 7, imageView2.getId(), 6);
                fk0.a(this.e);
                if (!bi.e()) {
                    FlexibleTextView flexibleTextView5 = this.g;
                    Wg6.b(flexibleTextView5, "mTvDeviceStatus");
                    flexibleTextView5.setText(Um5.c(this.i.e, 2131887173));
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                FlexibleTextView flexibleTextView6 = this.d;
                Wg6.b(flexibleTextView6, "mTvLastSyncedTime");
                flexibleTextView6.setText(this.i.l(bi.c(), true));
            } else {
                CardView cardView3 = this.h;
                Wg6.b(cardView3, "mCardView");
                cardView3.setRadius(2.0f);
                CardView cardView4 = this.h;
                Wg6.b(cardView4, "mCardView");
                cardView4.setCardElevation(2.0f);
                this.b.setImageResource(2131230959);
                ConstraintLayout constraintLayout3 = this.e;
                Wg6.b(constraintLayout3, "mClContainer");
                constraintLayout3.setAlpha(0.7f);
                ConstraintLayout constraintLayout4 = this.e;
                Wg6.b(constraintLayout4, "mClContainer");
                constraintLayout4.setBackground(W6.f(this.i.e, 2131230858));
                FlexibleTextView flexibleTextView7 = this.g;
                Wg6.b(flexibleTextView7, "mTvDeviceStatus");
                flexibleTextView7.setText(Um5.c(this.i.e, 2131887173));
                this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.c.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                FlexibleTextView flexibleTextView8 = this.d;
                Wg6.b(flexibleTextView8, "mTvLastSyncedTime");
                flexibleTextView8.setText(this.i.l(bi.c(), false));
            }
            String d2 = ThemeManager.l.a().d("nonBrandSurface");
            if (!TextUtils.isEmpty(d2)) {
                this.h.setBackgroundColor(Color.parseColor(d2));
            }
            CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(bi.c()).setSerialPrefix(DeviceHelper.o.m(bi.c())).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView3 = this.a;
            Wg6.b(imageView3, "mIvDevice");
            type.setPlaceHolder(imageView3, DeviceHelper.o.i(bi.c(), DeviceHelper.Bi.SMALL)).setImageCallback(new Bii(this)).download();
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void M0(String str);
    }

    @DexIgnore
    public Iw5(ArrayList<HomeProfilePresenter.Bi> arrayList, Wa1 wa1, Bi bi, PortfolioApp portfolioApp) {
        Wg6.c(arrayList, "mData");
        Wg6.c(wa1, "mRequestManager");
        Wg6.c(portfolioApp, "mApp");
        this.b = arrayList;
        this.c = wa1;
        this.d = bi;
        this.e = portfolioApp;
        this.a = portfolioApp.k0();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final String l(String str, boolean z) {
        String h;
        if (!z || !this.e.A0(str)) {
            long C = this.a.C(str);
            if (C == 0) {
                h = "";
            } else if (System.currentTimeMillis() - C < 60000) {
                Hr7 hr7 = Hr7.a;
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131887191);
                Wg6.b(c2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                h = String.format(c2, Arrays.copyOf(new Object[]{1}, 1));
                Wg6.b(h, "java.lang.String.format(format, *args)");
            } else {
                h = TimeUtils.h(C);
            }
            String string = this.e.getString(2131887175, new Object[]{h});
            Wg6.b(string, "mApp.getString(R.string.\u2026ayTime, lastSyncTimeText)");
            return string;
        }
        String string2 = this.e.getString(2131886784);
        Wg6.b(string2, "mApp.getString(R.string.\u2026ded_Text__SyncInProgress)");
        return string2;
    }

    @DexIgnore
    public void m(Ai ai, int i) {
        Wg6.c(ai, "holder");
        if (getItemCount() > i && i != -1) {
            HomeProfilePresenter.Bi bi = this.b.get(i);
            Wg6.b(bi, "mData[position]");
            ai.b(bi);
        }
    }

    @DexIgnore
    public Ai n(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558699, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026le_device, parent, false)");
        return new Ai(this, inflate);
    }

    @DexIgnore
    public final void o(ArrayList<HomeProfilePresenter.Bi> arrayList) {
        Wg6.c(arrayList, "data");
        this.b.clear();
        this.b.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        m(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return n(viewGroup, i);
    }

    @DexIgnore
    public final void p() {
        if (getItemCount() > 0) {
            notifyItemChanged(0);
        }
    }
}
