package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f658a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource", f = "NotificationRemoteDataSource.kt", l = {16}, m = "notifications")
    public static final class a extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ cu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(cu4 cu4, qn7 qn7) {
            super(qn7);
            this.this$0 = cu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$notifications$response$1", f = "NotificationRemoteDataSource.kt", l = {16}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<ApiResponse<dt4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ cu4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(cu4 cu4, int i, qn7 qn7) {
            super(1, qn7);
            this.this$0 = cu4;
            this.$limit = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$limit, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<dt4>>> qn7) {
            throw null;
            //return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f658a;
                Integer e = ao7.e(this.$limit);
                this.label = 1;
                Object notifications = apiServiceV2.notifications(e, this);
                return notifications == d ? d : notifications;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public cu4(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f658a = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object c(cu4 cu4, int i, qn7 qn7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 100;
        }
        return cu4.b(i, qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(int r9, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.dt4>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.fossil.cu4.a
            if (r0 == 0) goto L_0x0041
            r0 = r10
            com.fossil.cu4$a r0 = (com.fossil.cu4.a) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0041
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x004f
            if (r4 != r5) goto L_0x0047
            int r2 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.fossil.cu4 r0 = (com.fossil.cu4) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x002a:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0065
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0040:
            return r0
        L_0x0041:
            com.fossil.cu4$a r0 = new com.fossil.cu4$a
            r0.<init>(r8, r10)
            goto L_0x0014
        L_0x0047:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004f:
            com.fossil.el7.b(r1)
            com.fossil.cu4$b r1 = new com.fossil.cu4$b
            r1.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x002a
            r0 = r2
            goto L_0x0040
        L_0x0065:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x007f
            r2 = r0
            com.fossil.hq5 r2 = (com.fossil.hq5) r2
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 28
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0040
        L_0x007f:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cu4.b(int, com.fossil.qn7):java.lang.Object");
    }
}
