package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.Ie1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class He1 extends Fk1<Mb1, Id1<?>> implements Ie1 {
    @DexIgnore
    public Ie1.Ai d;

    @DexIgnore
    public He1(long j) {
        super(j);
    }

    @DexIgnore
    @Override // com.fossil.Ie1
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            d();
        } else if (i >= 20 || i == 15) {
            m(h() / 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ie1
    public /* bridge */ /* synthetic */ Id1 b(Mb1 mb1, Id1 id1) {
        return (Id1) super.k(mb1, id1);
    }

    @DexIgnore
    @Override // com.fossil.Ie1
    public /* bridge */ /* synthetic */ Id1 c(Mb1 mb1) {
        return (Id1) super.l(mb1);
    }

    @DexIgnore
    @Override // com.fossil.Ie1
    public void e(Ie1.Ai ai) {
        this.d = ai;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Fk1
    public /* bridge */ /* synthetic */ int i(Id1<?> id1) {
        return n(id1);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.Fk1
    public /* bridge */ /* synthetic */ void j(Mb1 mb1, Id1<?> id1) {
        o(mb1, id1);
    }

    @DexIgnore
    public int n(Id1<?> id1) {
        return id1 == null ? super.i(null) : id1.c();
    }

    @DexIgnore
    public void o(Mb1 mb1, Id1<?> id1) {
        Ie1.Ai ai = this.d;
        if (ai != null && id1 != null) {
            ai.a(id1);
        }
    }
}
