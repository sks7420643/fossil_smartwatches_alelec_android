package com.fossil;

import com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroViewModel;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Su4 implements Factory<BCCreateChallengeIntroViewModel> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Su4 a; // = new Su4();
    }

    @DexIgnore
    public static Su4 a() {
        return Ai.a;
    }

    @DexIgnore
    public static BCCreateChallengeIntroViewModel c() {
        return new BCCreateChallengeIntroViewModel();
    }

    @DexIgnore
    public BCCreateChallengeIntroViewModel b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
