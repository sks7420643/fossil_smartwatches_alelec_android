package com.fossil;

import com.mapped.N70;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ws extends Rs {
    @DexIgnore
    public /* final */ N70 A;

    @DexIgnore
    public Ws(N70 n70, K5 k5) {
        super(Hs.C, k5);
        this.A = n70;
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        N6 n6 = N6.l;
        N70 n70 = this.A;
        ByteBuffer order = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(4).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(Ot.c.b);
        order.put(Lt.g.b);
        order.put(n70.getAction().a());
        order.put(n70.getActionStatus().a());
        byte[] array = order.array();
        Wg6.b(array, "musicEventData.array()");
        return new J6(n6, array, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.u0, this.A.toJSONObject());
    }
}
