package com.fossil;

import com.mapped.An4;
import com.mapped.U04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zq5 implements MembersInjector<MFDeviceService> {
    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ActivitiesRepository activitiesRepository) {
        mFDeviceService.e = activitiesRepository;
    }

    @DexIgnore
    public static void b(MFDeviceService mFDeviceService, AnalyticsHelper analyticsHelper) {
        mFDeviceService.w = analyticsHelper;
    }

    @DexIgnore
    public static void c(MFDeviceService mFDeviceService, PortfolioApp portfolioApp) {
        mFDeviceService.x = portfolioApp;
    }

    @DexIgnore
    public static void d(MFDeviceService mFDeviceService, U04 u04) {
        mFDeviceService.v = u04;
    }

    @DexIgnore
    public static void e(MFDeviceService mFDeviceService, BuddyChallengeManager buddyChallengeManager) {
        mFDeviceService.E = buddyChallengeManager;
    }

    @DexIgnore
    public static void f(MFDeviceService mFDeviceService, DeviceRepository deviceRepository) {
        mFDeviceService.d = deviceRepository;
    }

    @DexIgnore
    public static void g(MFDeviceService mFDeviceService, S27 s27) {
        mFDeviceService.A = s27;
    }

    @DexIgnore
    public static void h(MFDeviceService mFDeviceService, FitnessDataRepository fitnessDataRepository) {
        mFDeviceService.t = fitnessDataRepository;
    }

    @DexIgnore
    public static void i(MFDeviceService mFDeviceService, FitnessHelper fitnessHelper) {
        mFDeviceService.C = fitnessHelper;
    }

    @DexIgnore
    public static void j(MFDeviceService mFDeviceService, GoalTrackingRepository goalTrackingRepository) {
        mFDeviceService.u = goalTrackingRepository;
    }

    @DexIgnore
    public static void k(MFDeviceService mFDeviceService, HeartRateSampleRepository heartRateSampleRepository) {
        mFDeviceService.l = heartRateSampleRepository;
    }

    @DexIgnore
    public static void l(MFDeviceService mFDeviceService, HeartRateSummaryRepository heartRateSummaryRepository) {
        mFDeviceService.m = heartRateSummaryRepository;
    }

    @DexIgnore
    public static void m(MFDeviceService mFDeviceService, LinkStreamingManager linkStreamingManager) {
        mFDeviceService.y = linkStreamingManager;
    }

    @DexIgnore
    public static void n(MFDeviceService mFDeviceService, MicroAppRepository microAppRepository) {
        mFDeviceService.k = microAppRepository;
    }

    @DexIgnore
    public static void o(MFDeviceService mFDeviceService, MusicControlComponent musicControlComponent) {
        mFDeviceService.G = musicControlComponent;
    }

    @DexIgnore
    public static void p(MFDeviceService mFDeviceService, HybridPresetRepository hybridPresetRepository) {
        mFDeviceService.j = hybridPresetRepository;
    }

    @DexIgnore
    public static void q(MFDeviceService mFDeviceService, An4 an4) {
        mFDeviceService.c = an4;
    }

    @DexIgnore
    public static void r(MFDeviceService mFDeviceService, SleepSessionsRepository sleepSessionsRepository) {
        mFDeviceService.g = sleepSessionsRepository;
    }

    @DexIgnore
    public static void s(MFDeviceService mFDeviceService, SleepSummariesRepository sleepSummariesRepository) {
        mFDeviceService.h = sleepSummariesRepository;
    }

    @DexIgnore
    public static void t(MFDeviceService mFDeviceService, SummariesRepository summariesRepository) {
        mFDeviceService.f = summariesRepository;
    }

    @DexIgnore
    public static void u(MFDeviceService mFDeviceService, ThirdPartyRepository thirdPartyRepository) {
        mFDeviceService.z = thirdPartyRepository;
    }

    @DexIgnore
    public static void v(MFDeviceService mFDeviceService, UserRepository userRepository) {
        mFDeviceService.i = userRepository;
    }

    @DexIgnore
    public static void w(MFDeviceService mFDeviceService, VerifySecretKeyUseCase verifySecretKeyUseCase) {
        mFDeviceService.B = verifySecretKeyUseCase;
    }

    @DexIgnore
    public static void x(MFDeviceService mFDeviceService, Rl5 rl5) {
        mFDeviceService.D = rl5;
    }

    @DexIgnore
    public static void y(MFDeviceService mFDeviceService, WorkoutSessionRepository workoutSessionRepository) {
        mFDeviceService.s = workoutSessionRepository;
    }

    @DexIgnore
    public static void z(MFDeviceService mFDeviceService, WorkoutTetherGpsManager workoutTetherGpsManager) {
        mFDeviceService.F = workoutTetherGpsManager;
    }
}
