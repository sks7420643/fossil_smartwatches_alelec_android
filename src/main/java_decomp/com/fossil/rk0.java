package com.fossil;

import android.app.Activity;
import android.app.SharedElementCallback;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.View;
import androidx.core.app.SharedElementCallback;
import com.mapped.W6;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rk0 extends W6 {
    @DexIgnore
    public static Ci c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ String[] b;
        @DexIgnore
        public /* final */ /* synthetic */ Activity c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        public Ai(String[] strArr, Activity activity, int i) {
            this.b = strArr;
            this.c = activity;
            this.d = i;
        }

        @DexIgnore
        public void run() {
            int[] iArr = new int[this.b.length];
            PackageManager packageManager = this.c.getPackageManager();
            String packageName = this.c.getPackageName();
            int length = this.b.length;
            for (int i = 0; i < length; i++) {
                iArr[i] = packageManager.checkPermission(this.b[i], packageName);
            }
            ((Bi) this.c).onRequestPermissionsResult(this.d, this.b, iArr);
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        boolean a(Activity activity, int i, int i2, Intent intent);

        @DexIgnore
        boolean b(Activity activity, String[] strArr, int i);
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        void validateRequestPermissionsRequestCode(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends SharedElementCallback {
        @DexIgnore
        public /* final */ androidx.core.app.SharedElementCallback a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements SharedElementCallback.a {
            @DexIgnore
            public /* final */ /* synthetic */ SharedElementCallback.OnSharedElementsReadyListener a;

            @DexIgnore
            public Aii(Ei ei, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
                this.a = onSharedElementsReadyListener;
            }

            @DexIgnore
            @Override // androidx.core.app.SharedElementCallback.a
            public void a() {
                this.a.onSharedElementsReady();
            }
        }

        @DexIgnore
        public Ei(androidx.core.app.SharedElementCallback sharedElementCallback) {
            this.a = sharedElementCallback;
        }

        @DexIgnore
        public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
            return this.a.b(view, matrix, rectF);
        }

        @DexIgnore
        public View onCreateSnapshotView(Context context, Parcelable parcelable) {
            return this.a.c(context, parcelable);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onMapSharedElements(List<String> list, Map<String, View> map) {
            this.a.d(list, map);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onRejectSharedElements(List<View> list) {
            this.a.e(list);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
            this.a.f(list, list2, list3);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
            this.a.g(list, list2, list3);
        }

        @DexIgnore
        @Override // android.app.SharedElementCallback
        public void onSharedElementsArrived(List<String> list, List<View> list2, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            this.a.h(list, list2, new Aii(this, onSharedElementsReadyListener));
        }
    }

    @DexIgnore
    public static void A(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.startPostponedEnterTransition();
        }
    }

    @DexIgnore
    public static void p(Activity activity) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    public static void q(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    public static Ci r() {
        return c;
    }

    @DexIgnore
    public static void s(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.postponeEnterTransition();
        }
    }

    @DexIgnore
    public static void t(Activity activity) {
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
        } else if (!Tk0.i(activity)) {
            activity.recreate();
        }
    }

    @DexIgnore
    public static void u(Activity activity, String[] strArr, int i) {
        Ci ci = c;
        if (ci != null && ci.b(activity, strArr, i)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof Di) {
                ((Di) activity).validateRequestPermissionsRequestCode(i);
            }
            activity.requestPermissions(strArr, i);
        } else if (activity instanceof Bi) {
            new Handler(Looper.getMainLooper()).post(new Ai(strArr, activity, i));
        }
    }

    @DexIgnore
    public static void v(Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setEnterSharedElementCallback(sharedElementCallback != null ? new Ei(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    public static void w(Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setExitSharedElementCallback(sharedElementCallback != null ? new Ei(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    public static boolean x(Activity activity, String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return activity.shouldShowRequestPermissionRationale(str);
        }
        return false;
    }

    @DexIgnore
    public static void y(Activity activity, Intent intent, int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    @DexIgnore
    public static void z(Activity activity, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws IntentSender.SendIntentException {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
        } else {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
        }
    }
}
