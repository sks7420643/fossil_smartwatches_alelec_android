package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rg1 implements Qb1<Uri, Bitmap> {
    @DexIgnore
    public /* final */ Bh1 a;
    @DexIgnore
    public /* final */ Rd1 b;

    @DexIgnore
    public Rg1(Bh1 bh1, Rd1 rd1) {
        this.a = bh1;
        this.b = rd1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(Uri uri, Ob1 ob1) throws IOException {
        return d(uri, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Bitmap> b(Uri uri, int i, int i2, Ob1 ob1) throws IOException {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Bitmap> c(Uri uri, int i, int i2, Ob1 ob1) {
        Id1<Drawable> c = this.a.c(uri, i, i2, ob1);
        if (c == null) {
            return null;
        }
        return Hg1.a(this.b, c.get(), i, i2);
    }

    @DexIgnore
    public boolean d(Uri uri, Ob1 ob1) {
        return "android.resource".equals(uri.getScheme());
    }
}
