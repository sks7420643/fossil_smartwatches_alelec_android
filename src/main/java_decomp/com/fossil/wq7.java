package com.fossil;

import com.fossil.ls7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wq7 extends yq7 implements ls7 {
    @DexIgnore
    public wq7() {
    }

    @DexIgnore
    public wq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public ds7 computeReflected() {
        er7.f(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get();

    @DexIgnore
    @Override // com.fossil.ls7
    public Object getDelegate() {
        return ((ls7) getReflected()).getDelegate();
    }

    @DexIgnore
    @Override // com.fossil.yq7, com.fossil.ls7
    public ls7.a getGetter() {
        return ((ls7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.gp7
    public Object invoke() {
        return get();
    }
}
