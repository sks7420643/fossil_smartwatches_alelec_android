package com.fossil;

import com.fossil.O28;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P28 {
    @DexIgnore
    public /* final */ X08 a;
    @DexIgnore
    public O28.Ai b;
    @DexIgnore
    public X18 c;
    @DexIgnore
    public /* final */ F18 d;
    @DexIgnore
    public /* final */ A18 e;
    @DexIgnore
    public /* final */ M18 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ O28 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public L28 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public S28 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends WeakReference<P28> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public Ai(P28 p28, Object obj) {
            super(p28);
            this.a = obj;
        }
    }

    @DexIgnore
    public P28(F18 f18, X08 x08, A18 a18, M18 m18, Object obj) {
        this.d = f18;
        this.a = x08;
        this.e = a18;
        this.f = m18;
        this.h = new O28(x08, p(), a18, m18);
        this.g = obj;
    }

    @DexIgnore
    public void a(L28 l28, boolean z) {
        if (this.j == null) {
            this.j = l28;
            this.k = z;
            l28.n.add(new Ai(this, this.g));
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public void b() {
        S28 s28;
        L28 l28;
        synchronized (this.d) {
            this.m = true;
            s28 = this.n;
            l28 = this.j;
        }
        if (s28 != null) {
            s28.cancel();
        } else if (l28 != null) {
            l28.d();
        }
    }

    @DexIgnore
    public S28 c() {
        S28 s28;
        synchronized (this.d) {
            s28 = this.n;
        }
        return s28;
    }

    @DexIgnore
    public L28 d() {
        L28 l28;
        synchronized (this) {
            l28 = this.j;
        }
        return l28;
    }

    @DexIgnore
    public final Socket e(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (z3) {
            this.n = null;
        }
        if (z2) {
            this.l = true;
        }
        L28 l28 = this.j;
        if (l28 == null) {
            return null;
        }
        if (z) {
            l28.k = true;
        }
        if (this.n != null) {
            return null;
        }
        if (!this.l && !this.j.k) {
            return null;
        }
        l(this.j);
        if (this.j.n.isEmpty()) {
            this.j.o = System.nanoTime();
            if (Z18.a.e(this.d, this.j)) {
                socket = this.j.r();
                this.j = null;
                return socket;
            }
        }
        socket = null;
        this.j = null;
        return socket;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.L28 f(int r13, int r14, int r15, int r16, boolean r17) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.P28.f(int, int, int, int, boolean):com.fossil.L28");
    }

    @DexIgnore
    public final L28 g(int i2, int i3, int i4, int i5, boolean z, boolean z2) throws IOException {
        L28 f2;
        while (true) {
            f2 = f(i2, i3, i4, i5, z);
            synchronized (this.d) {
                if (f2.l != 0 || f2.o()) {
                    if (f2.n(z2)) {
                        break;
                    }
                    j();
                } else {
                    break;
                }
            }
        }
        return f2;
    }

    @DexIgnore
    public boolean h() {
        O28.Ai ai;
        return this.c != null || ((ai = this.b) != null && ai.b()) || this.h.c();
    }

    @DexIgnore
    public S28 i(OkHttpClient okHttpClient, Interceptor.Chain chain, boolean z) {
        try {
            S28 p = g(chain.f(), chain.a(), chain.b(), okHttpClient.B(), okHttpClient.J(), z).p(okHttpClient, chain, this);
            synchronized (this.d) {
                this.n = p;
            }
            return p;
        } catch (IOException e2) {
            throw new N28(e2);
        }
    }

    @DexIgnore
    public void j() {
        L28 l28;
        Socket e2;
        synchronized (this.d) {
            l28 = this.j;
            e2 = e(true, false, false);
            if (this.j != null) {
                l28 = null;
            }
        }
        B28.h(e2);
        if (l28 != null) {
            this.f.h(this.e, l28);
        }
    }

    @DexIgnore
    public void k() {
        L28 l28;
        Socket e2;
        synchronized (this.d) {
            l28 = this.j;
            e2 = e(false, true, false);
            if (this.j != null) {
                l28 = null;
            }
        }
        B28.h(e2);
        if (l28 != null) {
            Z18.a.k(this.e, null);
            this.f.h(this.e, l28);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final void l(L28 l28) {
        int size = l28.n.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (l28.n.get(i2).get() == this) {
                l28.n.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public Socket m(L28 l28) {
        if (this.n == null && this.j.n.size() == 1) {
            Socket e2 = e(true, false, false);
            this.j = l28;
            l28.n.add(this.j.n.get(0));
            return e2;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final Socket n() {
        L28 l28 = this.j;
        if (l28 == null || !l28.k) {
            return null;
        }
        return e(false, false, true);
    }

    @DexIgnore
    public X18 o() {
        return this.c;
    }

    @DexIgnore
    public final M28 p() {
        return Z18.a.j(this.d);
    }

    @DexIgnore
    public void q(IOException iOException) {
        Socket e2;
        boolean z = false;
        L28 l28 = null;
        synchronized (this.d) {
            if (iOException instanceof P38) {
                D38 d38 = ((P38) iOException).errorCode;
                if (d38 == D38.REFUSED_STREAM) {
                    int i2 = this.i + 1;
                    this.i = i2;
                    if (i2 > 1) {
                        this.c = null;
                    }
                    L28 l282 = this.j;
                    e2 = e(z, false, true);
                    if (this.j == null && this.k) {
                        l28 = l282;
                    }
                } else {
                    if (d38 != D38.CANCEL) {
                        this.c = null;
                    }
                    L28 l2822 = this.j;
                    e2 = e(z, false, true);
                    l28 = l2822;
                }
            } else {
                if (this.j != null && (!this.j.o() || (iOException instanceof C38))) {
                    if (this.j.l == 0) {
                        if (!(this.c == null || iOException == null)) {
                            this.h.a(this.c, iOException);
                        }
                        this.c = null;
                    }
                }
                L28 l28222 = this.j;
                e2 = e(z, false, true);
                l28 = l28222;
            }
            z = true;
            L28 l282222 = this.j;
            e2 = e(z, false, true);
            l28 = l282222;
        }
        B28.h(e2);
        if (l28 != null) {
            this.f.h(this.e, l28);
        }
    }

    @DexIgnore
    public void r(boolean z, S28 s28, long j2, IOException iOException) {
        L28 l28;
        Socket e2;
        boolean z2;
        this.f.p(this.e, j2);
        synchronized (this.d) {
            if (s28 != null) {
                if (s28 == this.n) {
                    if (!z) {
                        this.j.l++;
                    }
                    l28 = this.j;
                    e2 = e(z, false, true);
                    if (this.j != null) {
                        l28 = null;
                    }
                    z2 = this.l;
                }
            }
            throw new IllegalStateException("expected " + this.n + " but was " + s28);
        }
        B28.h(e2);
        if (l28 != null) {
            this.f.h(this.e, l28);
        }
        if (iOException != null) {
            this.f.b(this.e, Z18.a.k(this.e, iOException));
        } else if (z2) {
            Z18.a.k(this.e, null);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public String toString() {
        L28 d2 = d();
        return d2 != null ? d2.toString() : this.a.toString();
    }
}
