package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pa0 implements Parcelable.Creator<Qa0> {
    @DexIgnore
    public /* synthetic */ Pa0(Qg6 qg6) {
    }

    @DexIgnore
    public Qa0 a(Parcel parcel) {
        return new Qa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Qa0 createFromParcel(Parcel parcel) {
        return new Qa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Qa0[] newArray(int i) {
        return new Qa0[i];
    }
}
