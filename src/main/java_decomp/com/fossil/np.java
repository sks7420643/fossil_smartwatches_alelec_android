package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.LinkedHashSet;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Np extends Lp {
    @DexIgnore
    public static /* final */ Rm D; // = new Rm(null);
    @DexIgnore
    public /* final */ Gv1 C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Np(K5 k5, I60 i60, Gv1 gv1, String str, int i) {
        super(k5, i60, Yp.a0, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = gv1;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        boolean z = false;
        if (!(this.C.getPresetItems().length == 0)) {
            Rl1[] H = H();
            if (H.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    Rm rm = D;
                    Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.k.b));
                    if (ry1 == null) {
                        ry1 = Hd0.y.d();
                    }
                    Lp.h(this, new Zj(this.w, this.x, Yp.F, true, 1792, rm.a(H, 1792, ry1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new Dn(this), new Pn(this), new Bo(this), null, null, 48, null);
                } catch (Sx1 e) {
                    D90.i.i(e);
                    l(Nr.a(this.v, null, Zq.q, null, null, 13));
                }
            } else {
                I();
            }
        } else {
            l(Nr.a(this.v, null, Zq.b, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.g3, this.C.toJSONObject());
    }

    @DexIgnore
    public final Rl1[] H() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Hv1[] presetItems = this.C.getPresetItems();
        for (Hv1 hv1 : presetItems) {
            if (hv1 instanceof Sl1) {
                linkedHashSet.addAll(((Sl1) hv1).c());
            }
        }
        Object[] array = Pm7.C(linkedHashSet).toArray(new Rl1[0]);
        if (array != null) {
            return (Rl1[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void I() {
        JSONObject jSONObject;
        K5 k5 = this.w;
        I60 i60 = this.x;
        Yp yp = Yp.b0;
        try {
            Ry1 ry1 = i60.a().h().get(Short.valueOf(Ob.k.b));
            Ry1 d = ry1 != null ? ry1 : Hd0.y.d();
            Wg6.b(d, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
            JSONObject jSONObject2 = new JSONObject();
            Hv1[] presetItems = this.C.getPresetItems();
            for (Hv1 hv1 : presetItems) {
                hv1.a(d);
                jSONObject2 = Gy1.c(jSONObject2, hv1.a());
            }
            jSONObject = new JSONObject().put("push", new JSONObject().put("set", jSONObject2));
            Wg6.b(jSONObject, "JSONObject().put(UIScrip\u2026T, assignmentJsonObject))");
        } catch (JSONException e) {
            D90.i.i(e);
            jSONObject = new JSONObject();
        }
        Lp.h(this, new Rp(k5, i60, yp, jSONObject, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 48), new No(this), new Ap(this), null, null, null, 56, null);
    }
}
