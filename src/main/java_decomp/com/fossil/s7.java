package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S7 extends Qq7 implements Hg6<U5, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ U5 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public S7(K5 k5, U5 u5) {
        super(1);
        this.b = k5;
        this.c = u5;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(U5 u5) {
        K5 k5 = this.b;
        U5 u52 = this.c;
        Iterator<Bs> it = k5.i.iterator();
        while (it.hasNext()) {
            try {
                k5.b.post(new X7(it.next(), u52));
            } catch (Exception e) {
                D90.i.i(e);
            }
        }
        return Cd6.a;
    }
}
