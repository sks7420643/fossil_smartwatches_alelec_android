package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qc7 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Method b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public boolean d; // = true;

    @DexIgnore
    public Qc7(Object obj, Method method) {
        if (obj == null) {
            throw new NullPointerException("EventProducer target cannot be null.");
        } else if (method != null) {
            this.a = obj;
            this.b = method;
            method.setAccessible(true);
            this.c = ((method.hashCode() + 31) * 31) + obj.hashCode();
        } else {
            throw new NullPointerException("EventProducer method cannot be null.");
        }
    }

    @DexIgnore
    public void a() {
        this.d = false;
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public Object c() throws InvocationTargetException {
        if (this.d) {
            try {
                return this.b.invoke(this.a, new Object[0]);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            } catch (InvocationTargetException e2) {
                if (e2.getCause() instanceof Error) {
                    throw ((Error) e2.getCause());
                }
                throw e2;
            }
        } else {
            throw new IllegalStateException(toString() + " has been invalidated and can no longer produce events.");
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (Qc7.class != obj.getClass()) {
            return false;
        }
        Qc7 qc7 = (Qc7) obj;
        return this.b.equals(qc7.b) && this.a == qc7.a;
    }

    @DexIgnore
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        return "[EventProducer " + this.b + "]";
    }
}
