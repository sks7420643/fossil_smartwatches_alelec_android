package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Rm6;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vw7 extends Zw7<Rm6> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater g; // = AtomicIntegerFieldUpdater.newUpdater(Vw7.class, "_invoked");
    @DexIgnore
    public volatile int _invoked; // = 0;
    @DexIgnore
    public /* final */ Hg6<Throwable, Cd6> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Hg6<? super java.lang.Throwable, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Vw7(Rm6 rm6, Hg6<? super Throwable, Cd6> hg6) {
        super(rm6);
        this.f = hg6;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        w(th);
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return "InvokeOnCancelling[" + Ov7.a(this) + '@' + Ov7.b(this) + ']';
    }

    @DexIgnore
    @Override // com.fossil.Zu7
    public void w(Throwable th) {
        if (g.compareAndSet(this, 0, 1)) {
            this.f.invoke(th);
        }
    }
}
