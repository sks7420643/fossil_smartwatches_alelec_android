package com.fossil;

import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class St2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ R93 i;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public St2(Zs2 zs2, String str, String str2, boolean z, R93 r93) {
        super(zs2);
        this.j = zs2;
        this.f = str;
        this.g = str2;
        this.h = z;
        this.i = r93;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        this.j.h.getUserProperties(this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void b() {
        this.i.c(null);
    }
}
