package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L15 extends K15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362546, 1);
        y.put(2131363459, 2);
        y.put(2131362980, 3);
        y.put(2131362686, 4);
        y.put(2131361976, 5);
    }
    */

    @DexIgnore
    public L15(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 6, x, y));
    }

    @DexIgnore
    public L15(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[5], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[1], (RTLImageView) objArr[4], (RecyclerView) objArr[3], (View) objArr[2]);
        this.w = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.w != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.w = 1;
        }
        w();
    }
}
