package com.fossil;

import com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.NotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eu4 implements Factory<NotificationRepository> {
    @DexIgnore
    public /* final */ Provider<Bu4> a;
    @DexIgnore
    public /* final */ Provider<NotificationRemoteDataSource> b;

    @DexIgnore
    public Eu4(Provider<Bu4> provider, Provider<NotificationRemoteDataSource> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Eu4 a(Provider<Bu4> provider, Provider<NotificationRemoteDataSource> provider2) {
        return new Eu4(provider, provider2);
    }

    @DexIgnore
    public static NotificationRepository c(Bu4 bu4, NotificationRemoteDataSource notificationRemoteDataSource) {
        return new NotificationRepository(bu4, notificationRemoteDataSource);
    }

    @DexIgnore
    public NotificationRepository b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
