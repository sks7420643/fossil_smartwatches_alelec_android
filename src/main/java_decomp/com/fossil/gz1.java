package com.fossil;

import com.fossil.Mz1;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gz1 extends Mz1 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ Integer b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ Pz1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Mz1.Ai {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Long c;
        @DexIgnore
        public byte[] d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Long f;
        @DexIgnore
        public Pz1 g;

        @DexIgnore
        @Override // com.fossil.Mz1.Ai
        public Mz1.Ai a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Mz1.Ai
        public Mz1.Ai b(Pz1 pz1) {
            this.g = pz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Mz1.Ai
        public Mz1.Ai c(Integer num) {
            this.b = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Mz1.Ai
        public Mz1 d() {
            String str = "";
            if (this.a == null) {
                str = " eventTimeMs";
            }
            if (this.c == null) {
                str = str + " eventUptimeMs";
            }
            if (this.f == null) {
                str = str + " timezoneOffsetSeconds";
            }
            if (str.isEmpty()) {
                return new Gz1(this.a.longValue(), this.b, this.c.longValue(), this.d, this.e, this.f.longValue(), this.g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Mz1.Ai
        public Mz1.Ai e(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Mz1.Ai
        public Mz1.Ai f(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public Mz1.Ai g(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        public Mz1.Ai h(byte[] bArr) {
            this.d = bArr;
            return this;
        }
    }

    @DexIgnore
    public /* synthetic */ Gz1(long j, Integer num, long j2, byte[] bArr, String str, long j3, Pz1 pz1, Ai ai) {
        this.a = j;
        this.b = num;
        this.c = j2;
        this.d = bArr;
        this.e = str;
        this.f = j3;
        this.g = pz1;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public Integer c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public long d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public long e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        Integer num;
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Mz1)) {
            return false;
        }
        Mz1 mz1 = (Mz1) obj;
        if (this.a == mz1.d() && ((num = this.b) != null ? num.equals(((Gz1) mz1).b) : ((Gz1) mz1).b == null) && this.c == mz1.e()) {
            if (Arrays.equals(this.d, mz1 instanceof Gz1 ? ((Gz1) mz1).d : mz1.g()) && ((str = this.e) != null ? str.equals(((Gz1) mz1).e) : ((Gz1) mz1).e == null) && this.f == mz1.i()) {
                Pz1 pz1 = this.g;
                if (pz1 == null) {
                    if (((Gz1) mz1).g == null) {
                        z = true;
                        return z;
                    }
                } else if (pz1.equals(((Gz1) mz1).g)) {
                    z = true;
                    return z;
                }
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public Pz1 f() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public byte[] g() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public String h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        long j = this.a;
        int i2 = (int) (j ^ (j >>> 32));
        Integer num = this.b;
        int hashCode = num == null ? 0 : num.hashCode();
        long j2 = this.c;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        int hashCode2 = Arrays.hashCode(this.d);
        String str = this.e;
        int hashCode3 = str == null ? 0 : str.hashCode();
        long j3 = this.f;
        int i4 = (int) (j3 ^ (j3 >>> 32));
        Pz1 pz1 = this.g;
        if (pz1 != null) {
            i = pz1.hashCode();
        }
        return ((((((((((hashCode ^ ((i2 ^ 1000003) * 1000003)) * 1000003) ^ i3) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ i4) * 1000003) ^ i;
    }

    @DexIgnore
    @Override // com.fossil.Mz1
    public long i() {
        return this.f;
    }

    @DexIgnore
    public String toString() {
        return "LogEvent{eventTimeMs=" + this.a + ", eventCode=" + this.b + ", eventUptimeMs=" + this.c + ", sourceExtension=" + Arrays.toString(this.d) + ", sourceExtensionJsonProto3=" + this.e + ", timezoneOffsetSeconds=" + this.f + ", networkConnectionInfo=" + this.g + "}";
    }
}
