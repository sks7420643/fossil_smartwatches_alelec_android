package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xa3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Xa3> CREATOR; // = new Ya3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public Xa3(int i, int i2, long j, long j2) {
        this.b = i;
        this.c = i2;
        this.d = j;
        this.e = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && Xa3.class == obj.getClass()) {
            Xa3 xa3 = (Xa3) obj;
            return this.b == xa3.b && this.c == xa3.c && this.d == xa3.d && this.e == xa3.e;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(Integer.valueOf(this.c), Integer.valueOf(this.b), Long.valueOf(this.e), Long.valueOf(this.d));
    }

    @DexIgnore
    public final String toString() {
        return "NetworkLocationStatus: Wifi status: " + this.b + " Cell status: " + this.c + " elapsed time NS: " + this.e + " system time ms: " + this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.n(parcel, 2, this.c);
        Bd2.r(parcel, 3, this.d);
        Bd2.r(parcel, 4, this.e);
        Bd2.b(parcel, a2);
    }
}
