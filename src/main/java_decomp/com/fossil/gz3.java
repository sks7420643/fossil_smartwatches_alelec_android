package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.StateSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gz3 {
    @DexIgnore
    public /* final */ ArrayList<Bi> a; // = new ArrayList<>();
    @DexIgnore
    public Bi b; // = null;
    @DexIgnore
    public ValueAnimator c; // = null;
    @DexIgnore
    public /* final */ Animator.AnimatorListener d; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends AnimatorListenerAdapter {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Gz3 gz3 = Gz3.this;
            if (gz3.c == animator) {
                gz3.c = null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ ValueAnimator b;

        @DexIgnore
        public Bi(int[] iArr, ValueAnimator valueAnimator) {
            this.a = iArr;
            this.b = valueAnimator;
        }
    }

    @DexIgnore
    public void a(int[] iArr, ValueAnimator valueAnimator) {
        Bi bi = new Bi(iArr, valueAnimator);
        valueAnimator.addListener(this.d);
        this.a.add(bi);
    }

    @DexIgnore
    public final void b() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.c = null;
        }
    }

    @DexIgnore
    public void c() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.end();
            this.c = null;
        }
    }

    @DexIgnore
    public void d(int[] iArr) {
        Bi bi;
        int size = this.a.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                bi = null;
                break;
            }
            bi = this.a.get(i);
            if (StateSet.stateSetMatches(bi.a, iArr)) {
                break;
            }
            i++;
        }
        Bi bi2 = this.b;
        if (bi != bi2) {
            if (bi2 != null) {
                b();
            }
            this.b = bi;
            if (bi != null) {
                e(bi);
            }
        }
    }

    @DexIgnore
    public final void e(Bi bi) {
        ValueAnimator valueAnimator = bi.b;
        this.c = valueAnimator;
        valueAnimator.start();
    }
}
