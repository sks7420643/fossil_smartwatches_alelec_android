package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.R62;
import com.fossil.Yb2;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hs3 extends Ec2<Ms3> implements Ys3 {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ Ac2 F;
    @DexIgnore
    public /* final */ Bundle G;
    @DexIgnore
    public Integer H;

    @DexIgnore
    public Hs3(Context context, Looper looper, boolean z, Ac2 ac2, Bundle bundle, R62.Bi bi, R62.Ci ci) {
        super(context, looper, 44, ac2, bi, ci);
        this.E = true;
        this.F = ac2;
        this.G = bundle;
        this.H = ac2.f();
    }

    @DexIgnore
    public Hs3(Context context, Looper looper, boolean z, Ac2 ac2, Gs3 gs3, R62.Bi bi, R62.Ci ci) {
        this(context, looper, true, ac2, t0(ac2), bi, ci);
    }

    @DexIgnore
    public static Bundle t0(Ac2 ac2) {
        Gs3 k = ac2.k();
        Integer f = ac2.f();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", ac2.a());
        if (f != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", f.intValue());
        }
        if (k != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", k.i());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", k.h());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", k.f());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", k.g());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", k.c());
            bundle.putString("com.google.android.gms.signin.internal.logSessionId", k.d());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", k.j());
            if (k.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", k.a().longValue());
            }
            if (k.e() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", k.e().longValue());
            }
        }
        return bundle;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public Bundle F() {
        if (!E().getPackageName().equals(this.F.i())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.i());
        }
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ys3
    public final void b() {
        l(new Yb2.Di());
    }

    @DexIgnore
    @Override // com.fossil.Ys3
    public final void d(Jc2 jc2, boolean z) {
        try {
            ((Ms3) I()).F0(jc2, this.H.intValue(), z);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys3
    public final void e(Ks3 ks3) {
        Rc2.l(ks3, "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.F.c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = O42.b(E()).c();
            }
            ((Ms3) I()).Q1(new Ss3(new Sc2(c, this.H.intValue(), googleSignInAccount)), ks3);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                ks3.Q2(new Us3(8));
            } catch (RemoteException e2) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys3
    public final void o() {
        try {
            ((Ms3) I()).K(this.H.intValue());
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public String p() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        return queryLocalInterface instanceof Ms3 ? (Ms3) queryLocalInterface : new Ps3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2, com.fossil.Yb2
    public int s() {
        return H62.a;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public boolean v() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public String x() {
        return "com.google.android.gms.signin.service.START";
    }
}
