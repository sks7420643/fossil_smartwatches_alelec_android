package com.fossil;

import com.fossil.Ta4;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class P94 implements Comparator {
    @DexIgnore
    public static /* final */ P94 b; // = new P94();

    @DexIgnore
    public static Comparator a() {
        return b;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((Ta4.Bi) obj).b().compareTo(((Ta4.Bi) obj2).b());
    }
}
