package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ie7 extends Je7 {
    @DexIgnore
    <T> T a(String str);

    @DexIgnore
    Be7 b();

    @DexIgnore
    boolean c();

    @DexIgnore
    Boolean d();
}
