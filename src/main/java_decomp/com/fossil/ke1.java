package com.fossil;

import com.fossil.Kk1;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ke1 {
    @DexIgnore
    public /* final */ Fk1<Mb1, String> a; // = new Fk1<>(1000);
    @DexIgnore
    public /* final */ Mn0<Bi> b; // = Kk1.d(10, new Ai(this));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Kk1.Di<Bi> {
        @DexIgnore
        public Ai(Ke1 ke1) {
        }

        @DexIgnore
        public Bi a() {
            try {
                return new Bi(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Kk1.Di
        public /* bridge */ /* synthetic */ Bi create() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Kk1.Fi {
        @DexIgnore
        public /* final */ MessageDigest b;
        @DexIgnore
        public /* final */ Mk1 c; // = Mk1.a();

        @DexIgnore
        public Bi(MessageDigest messageDigest) {
            this.b = messageDigest;
        }

        @DexIgnore
        @Override // com.fossil.Kk1.Fi
        public Mk1 f() {
            return this.c;
        }
    }

    @DexIgnore
    public final String a(Mb1 mb1) {
        Bi b2 = this.b.b();
        Ik1.d(b2);
        Bi bi = b2;
        try {
            mb1.a(bi.b);
            return Jk1.t(bi.b.digest());
        } finally {
            this.b.a(bi);
        }
    }

    @DexIgnore
    public String b(Mb1 mb1) {
        String g;
        synchronized (this.a) {
            g = this.a.g(mb1);
        }
        if (g == null) {
            g = a(mb1);
        }
        synchronized (this.a) {
            this.a.k(mb1, g);
        }
        return g;
    }
}
