package com.fossil;

import com.fossil.N18;
import com.fossil.P18;
import com.fossil.Q18;
import com.fossil.S18;
import com.fossil.V18;
import java.io.IOException;
import java.util.regex.Pattern;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O88 {
    @DexIgnore
    public static /* final */ char[] l; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Q18 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public Q18.Ai d;
    @DexIgnore
    public /* final */ V18.Ai e; // = new V18.Ai();
    @DexIgnore
    public /* final */ P18.Ai f;
    @DexIgnore
    public R18 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public S18.Ai i;
    @DexIgnore
    public N18.Ai j;
    @DexIgnore
    public RequestBody k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends RequestBody {
        @DexIgnore
        public /* final */ RequestBody a;
        @DexIgnore
        public /* final */ R18 b;

        @DexIgnore
        public Ai(RequestBody requestBody, R18 r18) {
            this.a = requestBody;
            this.b = r18;
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public long a() throws IOException {
            return this.a.a();
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public R18 b() {
            return this.b;
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public void h(J48 j48) throws IOException {
            this.a.h(j48);
        }
    }

    @DexIgnore
    public O88(String str, Q18 q18, String str2, P18 p18, R18 r18, boolean z, boolean z2, boolean z3) {
        this.a = str;
        this.b = q18;
        this.c = str2;
        this.g = r18;
        this.h = z;
        if (p18 != null) {
            this.f = p18.f();
        } else {
            this.f = new P18.Ai();
        }
        if (z2) {
            this.j = new N18.Ai();
        } else if (z3) {
            S18.Ai ai = new S18.Ai();
            this.i = ai;
            ai.f(S18.f);
        }
    }

    @DexIgnore
    public static String g(String str, boolean z) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                I48 i48 = new I48();
                i48.E0(str, 0, i2);
                h(i48, str, i2, length, z);
                return i48.b0();
            }
            i2 += Character.charCount(codePointAt);
        }
        return str;
    }

    @DexIgnore
    public static void h(I48 i48, String str, int i2, int i3, boolean z) {
        I48 i482 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (i482 == null) {
                        i482 = new I48();
                    }
                    i482.F0(codePointAt);
                    while (!i482.u()) {
                        int readByte = i482.readByte() & 255;
                        i48.w0(37);
                        i48.w0(l[(readByte >> 4) & 15]);
                        i48.w0(l[readByte & 15]);
                    }
                } else {
                    i48.F0(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        if (z) {
            this.j.b(str, str2);
        } else {
            this.j.a(str, str2);
        }
    }

    @DexIgnore
    public void b(String str, String str2) {
        if ("Content-Type".equalsIgnoreCase(str)) {
            try {
                this.g = R18.c(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Malformed content type: " + str2, e2);
            }
        } else {
            this.f.a(str, str2);
        }
    }

    @DexIgnore
    public void c(P18 p18, RequestBody requestBody) {
        this.i.c(p18, requestBody);
    }

    @DexIgnore
    public void d(S18.Bi bi) {
        this.i.d(bi);
    }

    @DexIgnore
    public void e(String str, String str2, boolean z) {
        if (this.c != null) {
            String g2 = g(str2, z);
            String str3 = this.c;
            String replace = str3.replace("{" + str + "}", g2);
            if (!m.matcher(replace).matches()) {
                this.c = replace;
                return;
            }
            throw new IllegalArgumentException("@Path parameters shouldn't perform path traversal ('.' or '..'): " + str2);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void f(String str, String str2, boolean z) {
        String str3 = this.c;
        if (str3 != null) {
            Q18.Ai q = this.b.q(str3);
            this.d = q;
            if (q != null) {
                this.c = null;
            } else {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        if (z) {
            this.d.a(str, str2);
        } else {
            this.d.b(str, str2);
        }
    }

    @DexIgnore
    public V18.Ai i() {
        Q18 D;
        Q18.Ai ai = this.d;
        if (ai != null) {
            D = ai.c();
        } else {
            D = this.b.D(this.c);
            if (D == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        Ai ai2 = this.k;
        if (ai2 == null) {
            N18.Ai ai3 = this.j;
            if (ai3 != null) {
                ai2 = ai3.c();
            } else {
                S18.Ai ai4 = this.i;
                if (ai4 != null) {
                    ai2 = ai4.e();
                } else if (this.h) {
                    ai2 = RequestBody.f(null, new byte[0]);
                }
            }
        }
        R18 r18 = this.g;
        if (r18 != null) {
            if (ai2 != null) {
                ai2 = new Ai(ai2, r18);
            } else {
                this.f.a("Content-Type", r18.toString());
            }
        }
        V18.Ai ai5 = this.e;
        ai5.l(D);
        ai5.f(this.f.e());
        ai5.g(this.a, ai2);
        return ai5;
    }

    @DexIgnore
    public void j(RequestBody requestBody) {
        this.k = requestBody;
    }

    @DexIgnore
    public void k(Object obj) {
        this.c = obj.toString();
    }
}
