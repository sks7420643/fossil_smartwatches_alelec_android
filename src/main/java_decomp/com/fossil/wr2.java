package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wr2 implements Parcelable.Creator<Vr2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Vr2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 1;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        Tr2 tr2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i = Ad2.v(parcel, t);
            } else if (l == 2) {
                tr2 = (Tr2) Ad2.e(parcel, t, Tr2.CREATOR);
            } else if (l == 3) {
                iBinder2 = Ad2.u(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                iBinder = Ad2.u(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Vr2(i, tr2, iBinder2, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Vr2[] newArray(int i) {
        return new Vr2[i];
    }
}
