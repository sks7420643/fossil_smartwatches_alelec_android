package com.fossil;

import android.content.Context;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vg2<T> {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public T b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Exception {
        @DexIgnore
        public Ai(String str) {
            super(str);
        }

        @DexIgnore
        public Ai(String str, Throwable th) {
            super(str, th);
        }
    }

    @DexIgnore
    public Vg2(String str) {
        this.a = str;
    }

    @DexIgnore
    public abstract T a(IBinder iBinder);

    @DexIgnore
    public final T b(Context context) throws Ai {
        if (this.b == null) {
            Rc2.k(context);
            Context d = H62.d(context);
            if (d != null) {
                try {
                    this.b = a((IBinder) d.getClassLoader().loadClass(this.a).newInstance());
                } catch (ClassNotFoundException e) {
                    throw new Ai("Could not load creator class.", e);
                } catch (InstantiationException e2) {
                    throw new Ai("Could not instantiate creator.", e2);
                } catch (IllegalAccessException e3) {
                    throw new Ai("Could not access creator.", e3);
                }
            } else {
                throw new Ai("Could not get remote context.");
            }
        }
        return this.b;
    }
}
