package com.fossil;

import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E34<K, V> extends U24<V> {
    @DexIgnore
    @Weak
    public /* final */ A34<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends H54<V> {
        @DexIgnore
        public /* final */ H54<Map.Entry<K, V>> b; // = E34.this.map.entrySet().iterator();

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return this.b.next().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends S24<V> {
        @DexIgnore
        public /* final */ /* synthetic */ Y24 val$entryList;

        @DexIgnore
        public Bi(Y24 y24) {
            this.val$entryList = y24;
        }

        @DexIgnore
        @Override // com.fossil.S24
        public U24<V> delegateCollection() {
            return E34.this;
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            return (V) ((Map.Entry) this.val$entryList.get(i)).getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ A34<?, V> map;

        @DexIgnore
        public Ci(A34<?, V> a34) {
            this.map = a34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.values();
        }
    }

    @DexIgnore
    public E34(A34<K, V> a34) {
        this.map = a34;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public Y24<V> asList() {
        return new Bi(this.map.entrySet().asList());
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean contains(Object obj) {
        return obj != null && P34.f(iterator(), obj);
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.U24, com.fossil.U24, java.lang.Iterable
    public H54<V> iterator() {
        return new Ai();
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public Object writeReplace() {
        return new Ci(this.map);
    }
}
