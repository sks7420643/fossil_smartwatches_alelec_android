package com.fossil;

import com.mapped.Wg6;
import com.mapped.Zf;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sw5 extends Zf.Di<ActivitySummary> {
    @DexIgnore
    public boolean a(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        Wg6.c(activitySummary, "oldItem");
        Wg6.c(activitySummary2, "newItem");
        return Wg6.a(activitySummary, activitySummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        return a(activitySummary, activitySummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        return b(activitySummary, activitySummary2);
    }

    @DexIgnore
    public boolean b(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        Wg6.c(activitySummary, "oldItem");
        Wg6.c(activitySummary2, "newItem");
        return activitySummary.getDay() == activitySummary2.getDay() && activitySummary.getMonth() == activitySummary2.getMonth() && activitySummary.getYear() == activitySummary2.getYear();
    }
}
