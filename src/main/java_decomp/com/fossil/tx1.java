package com.fossil;

import com.fossil.Ix1;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tx1<T> extends Qx1<T> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Tx1(Ry1 ry1) {
        super(ry1);
        Wg6.c(ry1, "version");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Tx1(Ry1 ry1, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new Ry1(1, 0) : ry1);
    }

    @DexIgnore
    @Override // com.fossil.Qx1
    public byte[] a(short s, T t) {
        Wg6.c(t, "entries");
        byte[] b = b(t);
        byte[] array = ByteBuffer.allocate(b.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put((byte) c().getMajor()).put((byte) c().getMinor()).putInt(0).putInt(b.length).put(b).putInt((int) Ix1.a.b(b, Ix1.Ai.CRC32)).array();
        Wg6.b(array, "result.array()");
        return array;
    }
}
