package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ua8 extends Va8<Bitmap> {
    @DexIgnore
    public Bitmap e;

    @DexIgnore
    public Ua8(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void c(Bitmap bitmap, Tj1<? super Bitmap> tj1) {
        Wg6.c(bitmap, "resource");
        this.e = bitmap;
    }

    @DexIgnore
    @Override // com.fossil.Va8, com.fossil.Ei1
    public void onDestroy() {
        Bitmap bitmap;
        super.onDestroy();
        Bitmap bitmap2 = this.e;
        if (bitmap2 != null && !bitmap2.isRecycled() && (bitmap = this.e) != null) {
            bitmap.recycle();
        }
    }
}
