package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qx1<T> {
    @DexIgnore
    public /* final */ Ry1 a;

    @DexIgnore
    public Qx1(Ry1 ry1) {
        Wg6.c(ry1, "signedVersion");
        this.a = ry1;
    }

    @DexIgnore
    public abstract byte[] a(short s, T t);

    @DexIgnore
    public abstract byte[] b(T t);

    @DexIgnore
    public final Ry1 c() {
        return this.a;
    }
}
