package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fp4 implements Factory<Un5> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Fp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Fp4 a(Uo4 uo4) {
        return new Fp4(uo4);
    }

    @DexIgnore
    public static Un5 c(Uo4 uo4) {
        Un5 m = uo4.m();
        Lk7.c(m, "Cannot return null from a non-@Nullable @Provides method");
        return m;
    }

    @DexIgnore
    public Un5 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
