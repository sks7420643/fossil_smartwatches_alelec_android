package com.fossil;

import android.content.Context;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dz4 implements RecyclerView.p {
    @DexIgnore
    public long a;
    @DexIgnore
    public GestureDetector b;
    @DexIgnore
    public /* final */ Bi c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            Wg6.c(motionEvent, "e");
            return true;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(View view, int i);
    }

    @DexIgnore
    public Dz4(Context context, Bi bi) {
        Wg6.c(context, "context");
        this.c = bi;
        this.b = new GestureDetector(context, new Ai());
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
        Wg6.c(recyclerView, "view");
        Wg6.c(motionEvent, "motionEvent");
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
        Wg6.c(recyclerView, "view");
        Wg6.c(motionEvent, "e");
        View findChildViewUnder = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        if (findChildViewUnder == null || this.c == null || !this.b.onTouchEvent(motionEvent) || SystemClock.elapsedRealtime() - this.a < 1000) {
            return false;
        }
        this.a = SystemClock.elapsedRealtime();
        Bi bi = this.c;
        View rootView = findChildViewUnder.getRootView();
        Wg6.b(rootView, "childView.rootView");
        bi.a(rootView, recyclerView.getChildAdapterPosition(findChildViewUnder));
        return false;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public void e(boolean z) {
    }
}
