package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ep1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ep1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ep1 createFromParcel(Parcel parcel) {
            return new Ep1(parcel.readLong(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ep1[] newArray(int i) {
            return new Ep1[i];
        }
    }

    @DexIgnore
    public Ep1(long j, int i) {
        this.b = j;
        this.c = i;
        if (!(i >= 0 && 100 >= i)) {
            throw new IllegalArgumentException(E.c(E.e("chanceOfRain("), this.c, ") is out of range ", "[0, 100]."));
        }
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.b);
            jSONObject.put("rain", this.c);
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ep1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Ep1 ep1 = (Ep1) obj;
            if (this.b != ep1.b) {
                return false;
            }
            return this.c == ep1.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.ChanceOfRainInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.c;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (((int) this.b) * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.M2, Long.valueOf(this.b)), Jd0.r, Integer.valueOf(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
