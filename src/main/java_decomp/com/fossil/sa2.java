package com.fossil;

import android.app.Dialog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sa2 extends Q92 {
    @DexIgnore
    public /* final */ /* synthetic */ Dialog a;
    @DexIgnore
    public /* final */ /* synthetic */ Ta2 b;

    @DexIgnore
    public Sa2(Ta2 ta2, Dialog dialog) {
        this.b = ta2;
        this.a = dialog;
    }

    @DexIgnore
    @Override // com.fossil.Q92
    public final void a() {
        this.b.c.p();
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
    }
}
