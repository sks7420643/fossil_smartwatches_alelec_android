package com.fossil;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.Tj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nj1<Z> extends Rj1<ImageView, Z> implements Tj1.Ai {
    @DexIgnore
    public Animatable h;

    @DexIgnore
    public Nj1(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void b(Z z, Tj1<? super Z> tj1) {
        if (tj1 == null || !tj1.a(z, this)) {
            p(z);
        } else {
            m(z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Jj1, com.fossil.Qj1
    public void f(Drawable drawable) {
        super.f(drawable);
        p(null);
        n(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Jj1, com.fossil.Qj1, com.fossil.Rj1
    public void h(Drawable drawable) {
        super.h(drawable);
        p(null);
        n(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Jj1, com.fossil.Qj1, com.fossil.Rj1
    public void j(Drawable drawable) {
        super.j(drawable);
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.stop();
        }
        p(null);
        n(drawable);
    }

    @DexIgnore
    public final void m(Z z) {
        if (z instanceof Animatable) {
            Z z2 = z;
            this.h = z2;
            z2.start();
            return;
        }
        this.h = null;
    }

    @DexIgnore
    public void n(Drawable drawable) {
        ((ImageView) this.b).setImageDrawable(drawable);
    }

    @DexIgnore
    public abstract void o(Z z);

    @DexIgnore
    @Override // com.fossil.Ei1, com.fossil.Jj1
    public void onStart() {
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.start();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ei1, com.fossil.Jj1
    public void onStop() {
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @DexIgnore
    public final void p(Z z) {
        o(z);
        m(z);
    }
}
