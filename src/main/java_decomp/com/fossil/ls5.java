package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.gu5;
import com.fossil.iq4;
import com.fossil.t47;
import com.fossil.v78;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.MFDeviceService;
import java.lang.reflect.Method;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class ls5 extends AppCompatActivity implements t47.g, v78.a {
    @DexIgnore
    public static IButtonConnectivity y;
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public long e;
    @DexIgnore
    public /* final */ Handler f; // = new Handler();
    @DexIgnore
    public View g;
    @DexIgnore
    public TextView h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public UserRepository j;
    @DexIgnore
    public on5 k;
    @DexIgnore
    public DeviceRepository l;
    @DexIgnore
    public oq4 m;
    @DexIgnore
    public gu5 s;
    @DexIgnore
    public String t;
    @DexIgnore
    public d57 u;
    @DexIgnore
    public /* final */ d v; // = new d(this);
    @DexIgnore
    public /* final */ e w; // = new e(this);
    @DexIgnore
    public /* final */ Runnable x; // = new b(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final IButtonConnectivity a() {
            return ls5.y;
        }

        @DexIgnore
        public final void b(IButtonConnectivity iButtonConnectivity) {
            ls5.y = iButtonConnectivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ls5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements iq4.e<gu5.e, gu5.b> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f2238a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(b bVar) {
                this.f2238a = bVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(gu5.b bVar) {
                pq7.c(bVar, "errorValue");
                this.f2238a.b.K("Disconnected");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(gu5.e eVar) {
                pq7.c(eVar, "responseValue");
                this.f2238a.b.K(String.valueOf(eVar.a()));
            }
        }

        @DexIgnore
        public b(ls5 ls5) {
            this.b = ls5;
        }

        @DexIgnore
        public final void run() {
            if (!TextUtils.isEmpty(this.b.t)) {
                gu5 o = this.b.o();
                String str = this.b.t;
                if (str != null) {
                    o.e(new gu5.d(str), new a(this));
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ls5 b;

        @DexIgnore
        public c(ls5 ls5) {
            this.b = ls5;
        }

        @DexIgnore
        public final void run() {
            Fragment Z;
            try {
                if (this.b.u == null && (Z = this.b.getSupportFragmentManager().Z("ProgressDialogFragment")) != null) {
                    this.b.u = (d57) Z;
                }
                if (this.b.u != null) {
                    FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog dismissAllowingStateLoss");
                    d57 d57 = this.b.u;
                    if (d57 != null) {
                        d57.dismissAllowingStateLoss();
                        this.b.u = null;
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = this.b.r();
                local.d(r, "Exception when dismiss progress dialog=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ServiceConnection {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ls5 f2239a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1", f = "BaseActivity.kt", l = {141, 143}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 0
                    r2 = 2
                    r4 = 1
                    java.lang.Object r1 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0060
                    if (r0 == r4) goto L_0x0044
                    if (r0 != r2) goto L_0x003c
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)     // Catch:{ Exception -> 0x007f }
                    r0 = r7
                L_0x0017:
                    com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0     // Catch:{ Exception -> 0x007f }
                    if (r0 == 0) goto L_0x0032
                    com.fossil.ls5$a r1 = com.fossil.ls5.z     // Catch:{ Exception -> 0x007f }
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = r1.a()     // Catch:{ Exception -> 0x007f }
                    if (r1 == 0) goto L_0x0032
                    com.fossil.ls5$a r1 = com.fossil.ls5.z     // Catch:{ Exception -> 0x007f }
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = r1.a()     // Catch:{ Exception -> 0x007f }
                    if (r1 == 0) goto L_0x007b
                    java.lang.String r0 = r0.getUserId()     // Catch:{ Exception -> 0x007f }
                    r1.updateUserId(r0)     // Catch:{ Exception -> 0x007f }
                L_0x0032:
                    com.fossil.ls5$d r0 = r6.this$0     // Catch:{ Exception -> 0x007f }
                    com.fossil.ls5 r0 = r0.f2239a     // Catch:{ Exception -> 0x007f }
                    r0.z()     // Catch:{ Exception -> 0x007f }
                L_0x0039:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x003b:
                    return r0
                L_0x003c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0044:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x004b:
                    com.fossil.ls5$d r2 = r6.this$0
                    com.fossil.ls5 r2 = r2.f2239a
                    com.portfolio.platform.data.source.UserRepository r2 = r2.q()
                    r6.L$0 = r0
                    r0 = 2
                    r6.label = r0
                    java.lang.Object r0 = r2.getCurrentUser(r6)
                    if (r0 != r1) goto L_0x0017
                    r0 = r1
                    goto L_0x003b
                L_0x0060:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r0 = r6.p$
                    com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
                    com.fossil.ls5$a r3 = com.fossil.ls5.z
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r3 = r3.a()
                    if (r3 == 0) goto L_0x00a6
                    r6.L$0 = r0
                    r6.label = r4
                    java.lang.Object r2 = r2.m(r3, r6)
                    if (r2 != r1) goto L_0x004b
                    r0 = r1
                    goto L_0x003b
                L_0x007b:
                    com.fossil.pq7.i()
                    throw r5
                L_0x007f:
                    r0 = move-exception
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.fossil.ls5$d r2 = r6.this$0
                    com.fossil.ls5 r2 = r2.f2239a
                    java.lang.String r2 = r2.r()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = ".onServiceConnected(), ex="
                    r3.append(r4)
                    r3.append(r0)
                    java.lang.String r3 = r3.toString()
                    r1.e(r2, r3)
                    r0.printStackTrace()
                    goto L_0x0039
                L_0x00a6:
                    com.fossil.pq7.i()
                    throw r5
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ls5.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(ls5 ls5) {
            this.f2239a = ls5;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            pq7.c(componentName, "name");
            pq7.c(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.f2239a.r(), "Button service connected");
            ls5.z.b(IButtonConnectivity.Stub.asInterface(iBinder));
            this.f2239a.B(true);
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            pq7.c(componentName, "name");
            FLogger.INSTANCE.getLocal().d(this.f2239a.r(), "Button service disconnected");
            this.f2239a.B(false);
            ls5.z.b(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ServiceConnection {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ls5 f2240a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(ls5 ls5) {
            this.f2240a = ls5;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            pq7.c(componentName, "name");
            pq7.c(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.f2240a.r(), "Misfit service connected");
            MFDeviceService.b bVar = (MFDeviceService.b) iBinder;
            this.f2240a.E(bVar.a());
            PortfolioApp.h0.n(bVar);
            this.f2240a.C(true);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            pq7.c(componentName, "name");
            this.f2240a.C(false);
            this.f2240a.E(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ls5 b;

        @DexIgnore
        public f(ls5 ls5) {
            this.b = ls5;
        }

        @DexIgnore
        public final void run() {
            FragmentManager supportFragmentManager = this.b.getSupportFragmentManager();
            pq7.b(supportFragmentManager, "supportFragmentManager");
            if (supportFragmentManager.d0() > 1) {
                this.b.getSupportFragmentManager().H0();
            } else {
                this.b.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ls5 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public g(ls5 ls5, String str, boolean z) {
            this.b = ls5;
            this.c = str;
            this.d = z;
        }

        @DexIgnore
        public final void run() {
            try {
                if (this.b.isDestroyed() || this.b.isFinishing()) {
                    FLogger.INSTANCE.getLocal().d(this.b.r(), "Activity is destroy or finishing, no need to show dialog");
                    return;
                }
                this.b.u = d57.d.a(this.c);
                d57 d57 = this.b.u;
                if (d57 != null) {
                    d57.setCancelable(this.d);
                    xq0 j = this.b.getSupportFragmentManager().j();
                    pq7.b(j, "supportFragmentManager.beginTransaction()");
                    d57 d572 = this.b.u;
                    if (d572 != null) {
                        j.d(d572, "ProgressDialogFragment");
                        j.i();
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = this.b.r();
                local.d(r, "Exception when showing progress dialog=" + e);
            }
        }
    }

    @DexIgnore
    public ls5() {
        String simpleName = getClass().getSimpleName();
        pq7.b(simpleName, "this.javaClass.simpleName");
        this.b = simpleName;
    }

    @DexIgnore
    public static /* synthetic */ void I(ls5 ls5, boolean z2, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                str = "";
            }
            ls5.H(z2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showLoadingDialog");
    }

    @DexIgnore
    public final void A() {
        View view = this.g;
        if (view != null && this.i) {
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(this.g);
                    this.i = false;
                    this.f.removeCallbacks(this.x);
                } else {
                    throw new il7("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        try {
            gu5 gu5 = this.s;
            if (gu5 != null) {
                gu5.s();
            } else {
                pq7.n("mGetRssi");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void B(boolean z2) {
        this.d = z2;
    }

    @DexIgnore
    public final void C(boolean z2) {
        this.c = z2;
    }

    @DexIgnore
    @Override // com.fossil.v78.a
    public void C4(int i2, List<String> list) {
        pq7.c(list, "perms");
    }

    @DexIgnore
    public final void D(long j2) {
        this.e = j2;
    }

    @DexIgnore
    public final void E(MFDeviceService mFDeviceService) {
    }

    @DexIgnore
    public final void F() {
        I(this, false, null, 2, null);
    }

    @DexIgnore
    public final void G(String str) {
        pq7.c(str, "title");
        H(false, str);
    }

    @DexIgnore
    public final void H(boolean z2, String str) {
        pq7.c(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "showLoadingDialog: cancelable = " + z2);
        t();
        this.f.post(new g(this, str, z2));
    }

    @DexIgnore
    public final <T extends Service> void J(Class<? extends T>... clsArr) {
        pq7.c(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.b, "unbindServices()");
        for (Class<? extends T> cls : clsArr) {
            if (pq7.a(cls, MFDeviceService.class)) {
                if (this.c) {
                    FLogger.INSTANCE.getLocal().d(this.b, "Unbinding from mIsMisfitServiceBound");
                    i47.f1583a.g(this, this.w);
                    this.c = false;
                }
            } else if (pq7.a(cls, ButtonService.class) && this.d) {
                FLogger.INSTANCE.getLocal().d(this.b, "Unbinding from mButtonServiceBound");
                i47.f1583a.g(this, this.v);
                this.d = false;
            }
        }
    }

    @DexIgnore
    public final void K(String str) {
        pq7.c(str, "rssiInfo");
        TextView textView = this.h;
        if (textView != null) {
            textView.setText(str);
            this.f.postDelayed(this.x, 1000);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.b;
        local.d(str2, "Inside .onDialogFragmentResult tag=" + str);
        if (str.hashCode() == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131362442) {
            w();
        }
    }

    @DexIgnore
    public void finish() {
        super.finish();
        if (u()) {
            overridePendingTransition(2130772013, 2130772016);
        }
    }

    @DexIgnore
    public final void i(Fragment fragment, int i2) {
        pq7.c(fragment, "fragment");
        k(fragment, null, i2);
    }

    @DexIgnore
    public final void j(Fragment fragment, String str) {
        pq7.c(fragment, "fragment");
        pq7.c(str, "tag");
        k(fragment, str, 2131362158);
    }

    @DexIgnore
    public final void k(Fragment fragment, String str, int i2) {
        pq7.c(fragment, "fragment");
        xq0 j2 = getSupportFragmentManager().j();
        j2.s(i2, fragment, str);
        j2.f(str);
        j2.i();
    }

    @DexIgnore
    @Override // com.fossil.v78.a
    public void k1(int i2, List<String> list) {
        pq7.c(list, "perms");
    }

    @DexIgnore
    @TargetApi(23)
    public final void l(boolean z2) {
        if (Build.VERSION.SDK_INT >= 23) {
            Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(RecyclerView.UNDEFINED_DURATION);
            pq7.b(window, "window");
            View decorView = window.getDecorView();
            pq7.b(decorView, "window.decorView");
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (z2) {
                decorView.setSystemUiVisibility(systemUiVisibility & -8193);
            } else {
                decorView.setSystemUiVisibility(systemUiVisibility | 8192);
            }
        }
    }

    @DexIgnore
    public <T extends Service> void m(Class<? extends T>... clsArr) {
        pq7.c(clsArr, "services");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.b;
        local.d(str, "Service Tracking - startAndBindService " + clsArr);
        int length = clsArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            Class<? extends T> cls = clsArr[i2];
            if (pq7.a(cls, MFDeviceService.class)) {
                i47.f1583a.c(this, MFDeviceService.class, this.w, 0);
            } else if (pq7.a(cls, ButtonService.class)) {
                i47.f1583a.c(this, ButtonService.class, this.v, 1);
            }
        }
    }

    @DexIgnore
    public final DeviceRepository n() {
        DeviceRepository deviceRepository = this.l;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        pq7.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final gu5 o() {
        gu5 gu5 = this.s;
        if (gu5 != null) {
            return gu5;
        }
        pq7.n("mGetRssi");
        throw null;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity
    public void onBackPressed() {
        runOnUiThread(new f(this));
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        PortfolioApp.h0.c().M().u1(this);
        super.onCreate(bundle);
        if (u()) {
            overridePendingTransition(2130772014, 2130772015);
        }
        pn5.o.a().p();
        Window window = getWindow();
        pq7.b(window, "window");
        View decorView = window.getDecorView();
        pq7.b(decorView, "window.decorView");
        decorView.setSystemUiVisibility(3328);
        ck5.f.g();
    }

    @DexIgnore
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        pq7.c(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            v();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onPause() {
        super.onPause();
        try {
            PortfolioApp.h0.l(this);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.b;
            local.e(str, "Inside " + this.b + ".onPause - exception=" + e2);
        }
        if (!PortfolioApp.h0.c().z0()) {
            on5 on5 = this.k;
            if (on5 == null) {
                pq7.n("mSharePrefs");
                throw null;
            } else if (on5.i0()) {
                A();
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onResume() {
        super.onResume();
        PortfolioApp.h0.h(this);
        l(false);
        if (PortfolioApp.h0.f()) {
            pn5.o.a().p();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().d(this.b, "onStart()");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d(this.b, "onStop()");
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final long p() {
        return this.e;
    }

    @DexIgnore
    public final UserRepository q() {
        UserRepository userRepository = this.j;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final String r() {
        return this.b;
    }

    @DexIgnore
    public final int s() {
        try {
            Method method = Context.class.getMethod("getThemeResId", new Class[0]);
            pq7.b(method, "currentClass.getMethod(\"getThemeResId\")");
            method.setAccessible(true);
            Object invoke = method.invoke(this, new Object[0]);
            if (invoke != null) {
                return ((Integer) invoke).intValue();
            }
            throw new il7("null cannot be cast to non-null type kotlin.Int");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(this.b, "Failed to get theme resource ID");
            return 0;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void startActivityForResult(Intent intent, int i2) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, i2);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog");
        this.f.post(new c(this));
    }

    @DexIgnore
    public final boolean u() {
        return s() == 2131951631;
    }

    @DexIgnore
    public final void v() {
        try {
            xk0.e(this);
        } catch (IllegalArgumentException e2) {
            finish();
        }
    }

    @DexIgnore
    public final void w() {
        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    @DexIgnore
    public final void x() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, getPackageName(), null));
        startActivity(intent);
    }

    @DexIgnore
    public final void y() {
        if (Build.VERSION.SDK_INT >= 29) {
            startActivity(new Intent("android.settings.panel.action.INTERNET_CONNECTIVITY"));
        } else {
            startActivity(new Intent("android.settings.SETTINGS"));
        }
    }

    @DexIgnore
    public final void z() {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.b;
            StringBuilder sb = new StringBuilder();
            sb.append("needToUpdateBLEWhenUpgradeLegacy - isNeedToUpdateBLE=");
            on5 on5 = this.k;
            if (on5 != null) {
                sb.append(on5.o0());
                local.d(str, sb.toString());
                on5 on52 = this.k;
                if (on52 == null) {
                    pq7.n("mSharePrefs");
                    throw null;
                } else if (on52.o0()) {
                    try {
                        oq4 oq4 = this.m;
                        if (oq4 != null) {
                            oq4.w();
                        } else {
                            pq7.n("mMigrationManager");
                            throw null;
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = this.b;
                        local2.e(str2, "needToUpdateBLEWhenUpgradeLegacy - e=" + e2);
                    }
                }
            } else {
                pq7.n("mSharePrefs");
                throw null;
            }
        }
    }
}
