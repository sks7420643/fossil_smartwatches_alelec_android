package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Iu1 {
    DEFAULT((byte) 0),
    PRE_SHARED_KEY((byte) 1),
    SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Iu1 a(byte b) {
            Iu1[] values = Iu1.values();
            for (Iu1 iu1 : values) {
                if (iu1.a() == b) {
                    return iu1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Iu1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
