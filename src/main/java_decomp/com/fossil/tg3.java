package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tg3 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> b; // = this.c.b.keySet().iterator();
    @DexIgnore
    public /* final */ /* synthetic */ Ug3 c;

    @DexIgnore
    public Tg3(Ug3 ug3) {
        this.c = ug3;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.b.hasNext();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.b.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
