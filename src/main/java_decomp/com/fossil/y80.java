package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y80 extends Z80 {
    @DexIgnore
    public static long h; // = 100;
    @DexIgnore
    public static /* final */ Y80 i; // = new Y80();

    @DexIgnore
    public Y80() {
        super("raw_minute_data", 102400, 20971520, "raw_minute_data", "raw_minute_data", new Zw1("", "", ""), 1800, new B90(), Ld0.d, false);
    }

    @DexIgnore
    @Override // com.fossil.Z80
    public long e() {
        return h;
    }
}
