package com.fossil;

import com.mapped.Rc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kz7 {
    @DexIgnore
    public static /* final */ Object a; // = new Vz7("CONDITION_FALSE");

    @DexIgnore
    public static final Object a() {
        return a;
    }

    @DexIgnore
    public static final Lz7 b(Object obj) {
        Lz7 lz7;
        Sz7 sz7 = (Sz7) (!(obj instanceof Sz7) ? null : obj);
        if (sz7 != null && (lz7 = sz7.a) != null) {
            return lz7;
        }
        if (obj != null) {
            return (Lz7) obj;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
}
