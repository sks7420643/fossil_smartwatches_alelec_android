package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum R4 {
    b(10),
    c(11),
    d(12);
    
    @DexIgnore
    public static /* final */ P4 f; // = new P4(null);

    @DexIgnore
    public R4(int i) {
    }
}
