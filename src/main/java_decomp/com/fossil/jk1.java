package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jk1 {
    @DexIgnore
    public static /* final */ char[] a; // = "0123456789abcdef".toCharArray();
    @DexIgnore
    public static /* final */ char[] b; // = new char[64];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Bitmap.Config.values().length];
            a = iArr;
            try {
                iArr[Bitmap.Config.ALPHA_8.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Bitmap.Config.RGBA_F16.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Bitmap.Config.ARGB_8888.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
        */
    }

    @DexIgnore
    public static void a() {
        if (!p()) {
            throw new IllegalArgumentException("You must call this method on a background thread");
        }
    }

    @DexIgnore
    public static void b() {
        if (!q()) {
            throw new IllegalArgumentException("You must call this method on the main thread");
        }
    }

    @DexIgnore
    public static boolean c(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj instanceof Ye1 ? ((Ye1) obj).a(obj2) : obj.equals(obj2);
    }

    @DexIgnore
    public static boolean d(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    @DexIgnore
    public static String e(byte[] bArr, char[] cArr) {
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i] & 255;
            int i3 = i * 2;
            char[] cArr2 = a;
            cArr[i3] = (char) cArr2[i2 >>> 4];
            cArr[i3 + 1] = (char) cArr2[i2 & 15];
        }
        return new String(cArr);
    }

    @DexIgnore
    public static <T> Queue<T> f(int i) {
        return new ArrayDeque(i);
    }

    @DexIgnore
    public static int g(int i, int i2, Bitmap.Config config) {
        return i * i2 * i(config);
    }

    @DexIgnore
    @TargetApi(19)
    public static int h(Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            if (Build.VERSION.SDK_INT >= 19) {
                try {
                    return bitmap.getAllocationByteCount();
                } catch (NullPointerException e) {
                }
            }
            return bitmap.getHeight() * bitmap.getRowBytes();
        }
        throw new IllegalStateException("Cannot obtain size for recycled Bitmap: " + bitmap + "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig());
    }

    @DexIgnore
    public static int i(Bitmap.Config config) {
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        int i = Ai.a[config.ordinal()];
        if (i == 1) {
            return 1;
        }
        if (i == 2 || i == 3) {
            return 2;
        }
        return i != 4 ? 4 : 8;
    }

    @DexIgnore
    public static <T> List<T> j(Collection<T> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (T t : collection) {
            if (t != null) {
                arrayList.add(t);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static int k(float f) {
        return l(f, 17);
    }

    @DexIgnore
    public static int l(float f, int i) {
        return m(Float.floatToIntBits(f), i);
    }

    @DexIgnore
    public static int m(int i, int i2) {
        return (i2 * 31) + i;
    }

    @DexIgnore
    public static int n(Object obj, int i) {
        return m(obj == null ? 0 : obj.hashCode(), i);
    }

    @DexIgnore
    public static int o(boolean z, int i) {
        return m(z ? 1 : 0, i);
    }

    @DexIgnore
    public static boolean p() {
        return !q();
    }

    @DexIgnore
    public static boolean q() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    @DexIgnore
    public static boolean r(int i) {
        return i > 0 || i == Integer.MIN_VALUE;
    }

    @DexIgnore
    public static boolean s(int i, int i2) {
        return r(i) && r(i2);
    }

    @DexIgnore
    public static String t(byte[] bArr) {
        String e;
        synchronized (b) {
            e = e(bArr, b);
        }
        return e;
    }
}
