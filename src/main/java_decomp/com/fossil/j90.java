package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J90 {
    @DexIgnore
    public final long a(Date date) {
        return date.getTime();
    }

    @DexIgnore
    public final Date b(long j) {
        return new Date(j);
    }
}
