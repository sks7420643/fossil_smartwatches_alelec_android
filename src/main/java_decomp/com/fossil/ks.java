package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ks extends Ps {
    @DexIgnore
    public /* final */ N6 G;
    @DexIgnore
    public /* final */ boolean H;
    @DexIgnore
    public long I;
    @DexIgnore
    public /* final */ ArrayList<Gs> J;
    @DexIgnore
    public /* final */ byte[] K;
    @DexIgnore
    public /* final */ N6 L;
    @DexIgnore
    public /* final */ int M;

    @DexIgnore
    @Override // com.fossil.Ps
    public Mt E(byte b) {
        return new Is();
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public void I(O7 o7) {
        super.I(o7);
        this.J.add(new Gs(o7.a, System.currentTimeMillis(), o7.b));
        if (this.J.size() >= this.M) {
            this.E = true;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public N6 K() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public N6 N() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.I = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.I;
    }
}
