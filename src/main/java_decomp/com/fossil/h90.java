package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H90 implements Parcelable.Creator<I90> {
    @DexIgnore
    public /* synthetic */ H90(Qg6 qg6) {
    }

    @DexIgnore
    public I90 a(Parcel parcel) {
        return new I90(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public I90 createFromParcel(Parcel parcel) {
        return new I90(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public I90[] newArray(int i) {
        return new I90[i];
    }
}
