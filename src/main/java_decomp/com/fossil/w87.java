package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W87 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ float e;

    @DexIgnore
    public W87(float f, float f2, float f3, float f4, float f5) {
        this.a = f;
        this.b = f2;
        this.c = f3;
        this.d = f4;
        this.e = f5;
    }

    @DexIgnore
    public final float a() {
        return this.d;
    }

    @DexIgnore
    public final float b() {
        return this.e;
    }

    @DexIgnore
    public final float c() {
        return this.c;
    }

    @DexIgnore
    public final float d() {
        return this.a;
    }

    @DexIgnore
    public final float e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof W87) {
                W87 w87 = (W87) obj;
                if (!(Float.compare(this.a, w87.a) == 0 && Float.compare(this.b, w87.b) == 0 && Float.compare(this.c, w87.c) == 0 && Float.compare(this.d, w87.d) == 0 && Float.compare(this.e, w87.e) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Jb7 f() {
        return new Jb7(this.a, this.b, this.c, this.d);
    }

    @DexIgnore
    public int hashCode() {
        return (((((((Float.floatToIntBits(this.a) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d)) * 31) + Float.floatToIntBits(this.e);
    }

    @DexIgnore
    public String toString() {
        return "Metric(xPos=" + this.a + ", yPos=" + this.b + ", width=" + this.c + ", height=" + this.d + ", scaleFactor=" + this.e + ")";
    }
}
