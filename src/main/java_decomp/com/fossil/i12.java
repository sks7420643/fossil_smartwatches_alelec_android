package com.fossil;

import java.util.concurrent.Executor;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I12 implements K12 {
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(M02.class.getName());
    @DexIgnore
    public /* final */ H22 a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ T02 c;
    @DexIgnore
    public /* final */ K22 d;
    @DexIgnore
    public /* final */ S32 e;

    @DexIgnore
    public I12(Executor executor, T02 t02, H22 h22, K22 k22, S32 s32) {
        this.b = executor;
        this.c = t02;
        this.a = h22;
        this.d = k22;
        this.e = s32;
    }

    @DexIgnore
    public static /* synthetic */ Object b(I12 i12, H02 h02, C02 c02) {
        i12.d.Y(h02, c02);
        i12.a.a(h02, 1);
        return null;
    }

    @DexIgnore
    public static /* synthetic */ void c(I12 i12, H02 h02, Zy1 zy1, C02 c02) {
        try {
            B12 b2 = i12.c.b(h02.b());
            if (b2 == null) {
                String format = String.format("Transport backend '%s' is not registered", h02.b());
                f.warning(format);
                zy1.a(new IllegalArgumentException(format));
                return;
            }
            i12.e.a(H12.b(i12, h02, b2.b(c02)));
            zy1.a(null);
        } catch (Exception e2) {
            Logger logger = f;
            logger.warning("Error scheduling event " + e2.getMessage());
            zy1.a(e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.K12
    public void a(H02 h02, C02 c02, Zy1 zy1) {
        this.b.execute(G12.a(this, h02, zy1, c02));
    }
}
