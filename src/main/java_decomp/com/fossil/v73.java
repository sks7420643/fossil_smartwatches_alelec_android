package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V73 implements Xw2<Y73> {
    @DexIgnore
    public static V73 c; // = new V73();
    @DexIgnore
    public /* final */ Xw2<Y73> b;

    @DexIgnore
    public V73() {
        this(Ww2.b(new X73()));
    }

    @DexIgnore
    public V73(Xw2<Y73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((Y73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((Y73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((Y73) c.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ Y73 zza() {
        return this.b.zza();
    }
}
