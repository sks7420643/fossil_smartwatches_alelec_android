package com.fossil;

import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j97 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f1731a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote$deleteBackgroundPhotos$2", f = "WFBackgroundPhotoRemote.kt", l = {20}, m = "invokeSuspend")
    public static final class a extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $deletedPhotoIds;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ j97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(j97 j97, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = j97;
            this.$deletedPhotoIds = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new a(this.this$0, this.$deletedPhotoIds, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            throw null;
            //return ((a) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f1731a;
                gj4 gj4 = this.$deletedPhotoIds;
                this.label = 1;
                Object deleteBackgroundPhotos = apiServiceV2.deleteBackgroundPhotos(gj4, this);
                return deleteBackgroundPhotos == d ? d : deleteBackgroundPhotos;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote$fetchBackgroundPhotos$2", f = "WFBackgroundPhotoRemote.kt", l = {12}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<ApiResponse<g97>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ j97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(j97 j97, qn7 qn7) {
            super(1, qn7);
            this.this$0 = j97;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<g97>>> qn7) {
            throw null;
            //return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f1731a;
                this.label = 1;
                Object fetchBackgroundPhotos = apiServiceV2.fetchBackgroundPhotos(this);
                return fetchBackgroundPhotos == d ? d : fetchBackgroundPhotos;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote$upsertBackgroundPhoto$2", f = "WFBackgroundPhotoRemote.kt", l = {16}, m = "invokeSuspend")
    public static final class c extends ko7 implements rp7<qn7<? super q88<ApiResponse<g97>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $photoData;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ j97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(j97 j97, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = j97;
            this.$photoData = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new c(this.this$0, this.$photoData, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<g97>>> qn7) {
            throw null;
            //return ((c) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f1731a;
                gj4 gj4 = this.$photoData;
                this.label = 1;
                Object upsertBackgroundPhotos = apiServiceV2.upsertBackgroundPhotos(gj4, this);
                return upsertBackgroundPhotos == d ? d : upsertBackgroundPhotos;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public j97(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f1731a = apiServiceV2;
    }

    @DexIgnore
    public final Object b(gj4 gj4, qn7<? super iq5<gj4>> qn7) {
        return jq5.d(new a(this, gj4, null), qn7);
    }

    @DexIgnore
    public final Object c(qn7<? super iq5<ApiResponse<g97>>> qn7) {
        return jq5.d(new b(this, null), qn7);
    }

    @DexIgnore
    public final Object d(gj4 gj4, qn7<? super iq5<ApiResponse<g97>>> qn7) {
        return jq5.d(new c(this, gj4, null), qn7);
    }
}
