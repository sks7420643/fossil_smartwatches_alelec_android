package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tc extends Qq7 implements Hg6<Cd6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ E60 b;
    @DexIgnore
    public /* final */ /* synthetic */ C2 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Tc(E60 e60, C2 c2) {
        super(1);
        this.b = e60;
        this.c = c2;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Cd6 cd6) {
        E60 e60 = this.b;
        C2 c2 = this.c;
        byte b2 = c2.c;
        I2 i2 = (I2) c2;
        Sp1 sp1 = new Sp1(b2, i2.e, i2.f);
        Q40.Bi bi = e60.w;
        if (bi != null) {
            bi.onEventReceived(e60, sp1);
        }
        return Cd6.a;
    }
}
