package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zg1<T extends Drawable> implements Id1<T>, Ed1 {
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public Zg1(T t) {
        Ik1.d(t);
        this.b = t;
    }

    @DexIgnore
    @Override // com.fossil.Ed1
    public void a() {
        T t = this.b;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof Hh1) {
            ((Hh1) t).e().prepareToDraw();
        }
    }

    @DexIgnore
    public final T e() {
        Drawable.ConstantState constantState = this.b.getConstantState();
        return constantState == null ? this.b : (T) constantState.newDrawable();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public /* bridge */ /* synthetic */ Object get() {
        return e();
    }
}
