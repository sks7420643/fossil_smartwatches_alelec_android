package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class S68 implements T68 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public S68(String str) {
        if (str != null) {
            this.a = str;
            int indexOf = str.indexOf(47);
            if (indexOf != -1) {
                str.substring(0, indexOf);
                str.substring(indexOf + 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("MIME type may not be null");
    }

    @DexIgnore
    @Override // com.fossil.U68
    public String c() {
        return this.a;
    }
}
