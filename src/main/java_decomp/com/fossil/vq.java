package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.logic.phase.InstallThemePackagePhase$onStart$1", f = "InstallThemePackagePhase.kt", l = {33}, m = "invokeSuspend")
public final class vq extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ bg e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vq(bg bgVar, qn7 qn7) {
        super(2, qn7);
        this.e = bgVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        vq vqVar = new vq(this.e, qn7);
        vqVar.b = (iv7) obj;
        throw null;
        //return vqVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        vq vqVar = new vq(this.e, qn7);
        vqVar.b = iv7;
        return vqVar.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object a2;
        kw1 edit;
        lw1 a3;
        ry1 packageVersion;
        Integer e2;
        Object d2 = yn7.d();
        int i = this.d;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.b;
            nc0 nc0 = nc0.f2498a;
            ec0 themeClassifier = this.e.C.getThemeClassifier();
            ry1 uiPackageOSVersion = this.e.x.a().getUiPackageOSVersion();
            this.c = iv7;
            this.d = 1;
            a2 = nc0.a(themeClassifier, uiPackageOSVersion, false, this);
            if (a2 == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.c;
            el7.b(obj);
            a2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        V v = ((o) a2).f2599a;
        if (v != null) {
            V v2 = !(v instanceof lw1) ? null : v;
            if (!(((v2 == null || (packageVersion = v2.getPackageVersion()) == null || (e2 = ao7.e(packageVersion.getMajor())) == null) ? 0 : e2.intValue()) <= this.e.C.getPackageVersion().getMajor() || (edit = this.e.C.edit()) == null || (a3 = edit.a(v)) == null)) {
                this.e.C = a3;
            }
        }
        bg.G(this.e);
        return tl7.f3441a;
    }
}
