package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wu5 implements Factory<GetUser> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public Wu5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Wu5 a(Provider<UserRepository> provider) {
        return new Wu5(provider);
    }

    @DexIgnore
    public static GetUser c(UserRepository userRepository) {
        return new GetUser(userRepository);
    }

    @DexIgnore
    public GetUser b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
