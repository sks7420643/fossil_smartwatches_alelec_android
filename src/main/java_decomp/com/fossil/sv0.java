package com.fossil;

import androidx.collection.SimpleArrayMap;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sv0 {
    @DexIgnore
    public /* final */ SimpleArrayMap<RecyclerView.ViewHolder, Ai> a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ Dj0<RecyclerView.ViewHolder> b; // = new Dj0<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public static Mn0<Ai> d; // = new Nn0(20);
        @DexIgnore
        public int a;
        @DexIgnore
        public RecyclerView.j.c b;
        @DexIgnore
        public RecyclerView.j.c c;

        @DexIgnore
        public static void a() {
            do {
            } while (d.b() != null);
        }

        @DexIgnore
        public static Ai b() {
            Ai b2 = d.b();
            return b2 == null ? new Ai() : b2;
        }

        @DexIgnore
        public static void c(Ai ai) {
            ai.a = 0;
            ai.b = null;
            ai.c = null;
            d.a(ai);
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        void b(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);

        @DexIgnore
        void c(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);

        @DexIgnore
        void d(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);
    }

    @DexIgnore
    public void a(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        Ai ai = this.a.get(viewHolder);
        if (ai == null) {
            ai = Ai.b();
            this.a.put(viewHolder, ai);
        }
        ai.a |= 2;
        ai.b = cVar;
    }

    @DexIgnore
    public void b(RecyclerView.ViewHolder viewHolder) {
        Ai ai = this.a.get(viewHolder);
        if (ai == null) {
            ai = Ai.b();
            this.a.put(viewHolder, ai);
        }
        ai.a |= 1;
    }

    @DexIgnore
    public void c(long j, RecyclerView.ViewHolder viewHolder) {
        this.b.r(j, viewHolder);
    }

    @DexIgnore
    public void d(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        Ai ai = this.a.get(viewHolder);
        if (ai == null) {
            ai = Ai.b();
            this.a.put(viewHolder, ai);
        }
        ai.c = cVar;
        ai.a |= 8;
    }

    @DexIgnore
    public void e(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        Ai ai = this.a.get(viewHolder);
        if (ai == null) {
            ai = Ai.b();
            this.a.put(viewHolder, ai);
        }
        ai.b = cVar;
        ai.a |= 4;
    }

    @DexIgnore
    public void f() {
        this.a.clear();
        this.b.e();
    }

    @DexIgnore
    public RecyclerView.ViewHolder g(long j) {
        return this.b.l(j);
    }

    @DexIgnore
    public boolean h(RecyclerView.ViewHolder viewHolder) {
        Ai ai = this.a.get(viewHolder);
        return (ai == null || (ai.a & 1) == 0) ? false : true;
    }

    @DexIgnore
    public boolean i(RecyclerView.ViewHolder viewHolder) {
        Ai ai = this.a.get(viewHolder);
        return (ai == null || (ai.a & 4) == 0) ? false : true;
    }

    @DexIgnore
    public void j() {
        Ai.a();
    }

    @DexIgnore
    public void k(RecyclerView.ViewHolder viewHolder) {
        p(viewHolder);
    }

    @DexIgnore
    public final RecyclerView.j.c l(RecyclerView.ViewHolder viewHolder, int i) {
        Ai n;
        RecyclerView.j.c cVar = null;
        int g = this.a.g(viewHolder);
        if (g >= 0 && (n = this.a.n(g)) != null) {
            int i2 = n.a;
            if ((i2 & i) != 0) {
                n.a = i & i2;
                if (i == 4) {
                    cVar = n.b;
                } else if (i == 8) {
                    cVar = n.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((n.a & 12) == 0) {
                    this.a.l(g);
                    Ai.c(n);
                }
            }
        }
        return cVar;
    }

    @DexIgnore
    public RecyclerView.j.c m(RecyclerView.ViewHolder viewHolder) {
        return l(viewHolder, 8);
    }

    @DexIgnore
    public RecyclerView.j.c n(RecyclerView.ViewHolder viewHolder) {
        return l(viewHolder, 4);
    }

    @DexIgnore
    public void o(Bi bi) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            RecyclerView.ViewHolder j = this.a.j(size);
            Ai l = this.a.l(size);
            int i = l.a;
            if ((i & 3) == 3) {
                bi.a(j);
            } else if ((i & 1) != 0) {
                RecyclerView.j.c cVar = l.b;
                if (cVar == null) {
                    bi.a(j);
                } else {
                    bi.c(j, cVar, l.c);
                }
            } else if ((i & 14) == 14) {
                bi.b(j, l.b, l.c);
            } else if ((i & 12) == 12) {
                bi.d(j, l.b, l.c);
            } else if ((i & 4) != 0) {
                bi.c(j, l.b, null);
            } else if ((i & 8) != 0) {
                bi.b(j, l.b, l.c);
            }
            Ai.c(l);
        }
    }

    @DexIgnore
    public void p(RecyclerView.ViewHolder viewHolder) {
        Ai ai = this.a.get(viewHolder);
        if (ai != null) {
            ai.a &= -2;
        }
    }

    @DexIgnore
    public void q(RecyclerView.ViewHolder viewHolder) {
        int u = this.b.u() - 1;
        while (true) {
            if (u < 0) {
                break;
            } else if (viewHolder == this.b.v(u)) {
                this.b.t(u);
                break;
            } else {
                u--;
            }
        }
        Ai remove = this.a.remove(viewHolder);
        if (remove != null) {
            Ai.c(remove);
        }
    }
}
