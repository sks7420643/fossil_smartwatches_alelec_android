package com.fossil;

import com.fossil.Xb1;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wg1 implements Xb1<ByteBuffer> {
    @DexIgnore
    public /* final */ ByteBuffer a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Xb1.Ai<ByteBuffer> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.Xb1' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Xb1.Ai
        public /* bridge */ /* synthetic */ Xb1<ByteBuffer> a(ByteBuffer byteBuffer) {
            return b(byteBuffer);
        }

        @DexIgnore
        public Xb1<ByteBuffer> b(ByteBuffer byteBuffer) {
            return new Wg1(byteBuffer);
        }

        @DexIgnore
        @Override // com.fossil.Xb1.Ai
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexIgnore
    public Wg1(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    @DexIgnore
    @Override // com.fossil.Xb1
    public void a() {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xb1
    public /* bridge */ /* synthetic */ ByteBuffer b() throws IOException {
        return c();
    }

    @DexIgnore
    public ByteBuffer c() {
        this.a.position(0);
        return this.a;
    }
}
