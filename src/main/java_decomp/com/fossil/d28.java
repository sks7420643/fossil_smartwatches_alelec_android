package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.stetho.server.http.HttpHeaders;
import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.F28;
import com.fossil.P18;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D28 implements Interceptor {
    @DexIgnore
    public /* final */ I28 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements C58 {
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ K48 c;
        @DexIgnore
        public /* final */ /* synthetic */ E28 d;
        @DexIgnore
        public /* final */ /* synthetic */ J48 e;

        @DexIgnore
        public Ai(D28 d28, K48 k48, E28 e28, J48 j48) {
            this.c = k48;
            this.d = e28;
            this.e = j48;
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.b && !B28.p(this, 100, TimeUnit.MILLISECONDS)) {
                this.b = true;
                this.d.abort();
            }
            this.c.close();
        }

        @DexIgnore
        @Override // com.fossil.C58
        public long d0(I48 i48, long j) throws IOException {
            try {
                long d0 = this.c.d0(i48, j);
                if (d0 == -1) {
                    if (!this.b) {
                        this.b = true;
                        this.e.close();
                    }
                    return -1;
                }
                i48.C(this.e.d(), i48.p0() - d0, d0);
                this.e.x();
                return d0;
            } catch (IOException e2) {
                if (!this.b) {
                    this.b = true;
                    this.d.abort();
                }
                throw e2;
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public D58 e() {
            return this.c.e();
        }
    }

    @DexIgnore
    public D28(I28 i28) {
        this.a = i28;
    }

    @DexIgnore
    public static P18 b(P18 p18, P18 p182) {
        P18.Ai ai = new P18.Ai();
        int h = p18.h();
        for (int i = 0; i < h; i++) {
            String e = p18.e(i);
            String i2 = p18.i(i);
            if ((!"Warning".equalsIgnoreCase(e) || !i2.startsWith("1")) && (c(e) || !d(e) || p182.c(e) == null)) {
                Z18.a.b(ai, e, i2);
            }
        }
        int h2 = p182.h();
        for (int i3 = 0; i3 < h2; i3++) {
            String e2 = p182.e(i3);
            if (!c(e2) && d(e2)) {
                Z18.a.b(ai, e2, p182.i(i3));
            }
        }
        return ai.e();
    }

    @DexIgnore
    public static boolean c(String str) {
        return HttpHeaders.CONTENT_LENGTH.equalsIgnoreCase(str) || GraphRequest.CONTENT_ENCODING_HEADER.equalsIgnoreCase(str) || "Content-Type".equalsIgnoreCase(str);
    }

    @DexIgnore
    public static boolean d(String str) {
        return !WebSocketHandler.HEADER_CONNECTION.equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public static Response e(Response response) {
        if (response == null || response.a() == null) {
            return response;
        }
        Response.a B = response.B();
        B.b(null);
        return B.c();
    }

    @DexIgnore
    public final Response a(E28 e28, Response response) throws IOException {
        A58 body;
        if (e28 == null || (body = e28.body()) == null) {
            return response;
        }
        Ai ai = new Ai(this, response.a().source(), e28, S48.c(body));
        String j = response.j("Content-Type");
        long contentLength = response.a().contentLength();
        Response.a B = response.B();
        B.b(new X28(j, contentLength, S48.d(ai)));
        return B.c();
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        I28 i28 = this.a;
        Response e = i28 != null ? i28.e(chain.c()) : null;
        F28 c = new F28.Ai(System.currentTimeMillis(), chain.c(), e).c();
        V18 v18 = c.a;
        Response response = c.b;
        I28 i282 = this.a;
        if (i282 != null) {
            i282.b(c);
        }
        if (e != null && response == null) {
            B28.g(e.a());
        }
        if (v18 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.p(chain.c());
            aVar.n(T18.HTTP_1_1);
            aVar.g(504);
            aVar.k("Unsatisfiable Request (only-if-cached)");
            aVar.b(B28.c);
            aVar.q(-1);
            aVar.o(System.currentTimeMillis());
            return aVar.c();
        } else if (v18 == null) {
            Response.a B = response.B();
            B.d(e(response));
            return B.c();
        } else {
            try {
                Response d = chain.d(v18);
                if (d == null && e != null) {
                }
                if (response != null) {
                    if (d.f() == 304) {
                        Response.a B2 = response.B();
                        B2.j(b(response.l(), d.l()));
                        B2.q(d.L());
                        B2.o(d.F());
                        B2.d(e(response));
                        B2.l(e(d));
                        Response c2 = B2.c();
                        d.a().close();
                        this.a.a();
                        this.a.f(response, c2);
                        return c2;
                    }
                    B28.g(response.a());
                }
                Response.a B3 = d.B();
                B3.d(e(response));
                B3.l(e(d));
                Response c3 = B3.c();
                if (this.a == null) {
                    return c3;
                }
                if (U28.c(c3) && F28.a(c3, v18)) {
                    return a(this.a.d(c3), c3);
                }
                if (!V28.a(v18.g())) {
                    return c3;
                }
                try {
                    this.a.c(v18);
                    return c3;
                } catch (IOException e2) {
                    return c3;
                }
            } finally {
                if (e != null) {
                    B28.g(e.a());
                }
            }
        }
    }
}
