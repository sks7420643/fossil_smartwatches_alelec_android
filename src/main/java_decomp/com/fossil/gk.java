package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gk extends Ro {
    @DexIgnore
    public /* final */ Zn1 S;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Gk(K5 k5, I60 i60, Zn1 zn1, short s, String str, int i) {
        super(k5, i60, Yp.z, true, (i & 8) != 0 ? Ke.b.b(k5.x, Ob.m) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 160);
        this.S = zn1;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.d, this.S.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public byte[] M() {
        try {
            Ea ea = Ea.d;
            short s = this.D;
            Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.m.b));
            if (ry1 == null) {
                ry1 = Hd0.y.d();
            }
            return ea.a(s, ry1, this.S);
        } catch (Sx1 e) {
            return new byte[0];
        }
    }
}
