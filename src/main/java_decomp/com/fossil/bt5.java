package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bt5 implements Factory<DianaSyncUseCase> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<V36> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> d;
    @DexIgnore
    public /* final */ Provider<FileRepository> e;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> f;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> g;
    @DexIgnore
    public /* final */ Provider<D26> h;
    @DexIgnore
    public /* final */ Provider<VerifySecretKeyUseCase> i;
    @DexIgnore
    public /* final */ Provider<UpdateFirmwareUsecase> j;
    @DexIgnore
    public /* final */ Provider<AnalyticsHelper> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> m;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> n;

    @DexIgnore
    public Bt5(Provider<An4> provider, Provider<V36> provider2, Provider<PortfolioApp> provider3, Provider<DeviceRepository> provider4, Provider<FileRepository> provider5, Provider<DianaPresetRepository> provider6, Provider<NotificationSettingsDatabase> provider7, Provider<D26> provider8, Provider<VerifySecretKeyUseCase> provider9, Provider<UpdateFirmwareUsecase> provider10, Provider<AnalyticsHelper> provider11, Provider<AlarmsRepository> provider12, Provider<DianaAppSettingRepository> provider13, Provider<WorkoutSettingRepository> provider14) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
    }

    @DexIgnore
    public static Bt5 a(Provider<An4> provider, Provider<V36> provider2, Provider<PortfolioApp> provider3, Provider<DeviceRepository> provider4, Provider<FileRepository> provider5, Provider<DianaPresetRepository> provider6, Provider<NotificationSettingsDatabase> provider7, Provider<D26> provider8, Provider<VerifySecretKeyUseCase> provider9, Provider<UpdateFirmwareUsecase> provider10, Provider<AnalyticsHelper> provider11, Provider<AlarmsRepository> provider12, Provider<DianaAppSettingRepository> provider13, Provider<WorkoutSettingRepository> provider14) {
        return new Bt5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14);
    }

    @DexIgnore
    public static DianaSyncUseCase c(An4 an4, V36 v36, PortfolioApp portfolioApp, DeviceRepository deviceRepository, FileRepository fileRepository, DianaPresetRepository dianaPresetRepository, NotificationSettingsDatabase notificationSettingsDatabase, D26 d26, VerifySecretKeyUseCase verifySecretKeyUseCase, UpdateFirmwareUsecase updateFirmwareUsecase, AnalyticsHelper analyticsHelper, AlarmsRepository alarmsRepository, DianaAppSettingRepository dianaAppSettingRepository, WorkoutSettingRepository workoutSettingRepository) {
        return new DianaSyncUseCase(an4, v36, portfolioApp, deviceRepository, fileRepository, dianaPresetRepository, notificationSettingsDatabase, d26, verifySecretKeyUseCase, updateFirmwareUsecase, analyticsHelper, alarmsRepository, dianaAppSettingRepository, workoutSettingRepository);
    }

    @DexIgnore
    public DianaSyncUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
