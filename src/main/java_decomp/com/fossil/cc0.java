package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ix1;
import com.mapped.Wg6;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cc0 extends Ox1 implements Parcelable, Nx1 {
    @DexIgnore
    public static /* final */ Bc0 CREATOR; // = new Bc0(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore
    public Cc0(String str, byte[] bArr) {
        this.b = str;
        this.c = bArr;
    }

    @DexIgnore
    @Override // java.lang.Object
    public Nx1 clone() {
        String str = this.b;
        byte[] bArr = this.c;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Cc0(str, copyOf);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return Gy1.d(Gy1.d(new JSONObject(), Jd0.P4, this.b), Jd0.U0, Long.valueOf(Ix1.a.b(this.c, Ix1.Ai.CRC32)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeByteArray(this.c);
    }
}
