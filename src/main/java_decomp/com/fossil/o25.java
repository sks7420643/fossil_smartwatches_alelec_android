package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O25 extends N25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P;
    @DexIgnore
    public long N;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        P = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        P.put(2131362690, 2);
        P.put(2131362309, 3);
        P.put(2131362547, 4);
        P.put(2131362300, 5);
        P.put(2131362483, 6);
        P.put(2131362743, 7);
        P.put(2131362514, 8);
        P.put(2131363166, 9);
        P.put(2131362059, 10);
        P.put(2131362193, 11);
        P.put(2131362191, 12);
        P.put(2131362195, 13);
        P.put(2131362196, 14);
        P.put(2131362194, 15);
        P.put(2131362190, 16);
        P.put(2131362192, 17);
        P.put(2131363457, 18);
        P.put(2131362901, 19);
        P.put(2131362903, 20);
        P.put(2131362902, 21);
        P.put(2131362285, 22);
    }
    */

    @DexIgnore
    public O25(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 23, O, P));
    }

    @DexIgnore
    public O25(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[10], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[14], (FlexibleButton) objArr[22], (FlexibleEditText) objArr[5], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[4], (ImageView) objArr[1], (RTLImageView) objArr[2], (ImageView) objArr[7], (NumberPicker) objArr[19], (NumberPicker) objArr[21], (NumberPicker) objArr[20], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[9], (View) objArr[18]);
        this.N = -1;
        this.K.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.N != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.N = 1;
        }
        w();
    }
}
