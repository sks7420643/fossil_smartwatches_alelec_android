package com.fossil;

import android.os.RemoteException;
import com.fossil.Ha3;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Or2 extends Ha3.Ai<Ka3> {
    @DexIgnore
    public /* final */ /* synthetic */ Ia3 s;
    @DexIgnore
    public /* final */ /* synthetic */ String t; // = null;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Or2(Nr2 nr2, R62 r62, Ia3 ia3, String str) {
        super(r62);
        this.s = ia3;
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ Z62 f(Status status) {
        return new Ka3(status);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi] */
    @Override // com.fossil.I72
    public final /* synthetic */ void u(Fr2 fr2) throws RemoteException {
        fr2.y0(this.s, this, this.t);
    }
}
