package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N57 {
    @DexIgnore
    public static /* final */ ExecutorService f; // = Executors.newCachedThreadPool();
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ WeakReference<Context> b;
    @DexIgnore
    public /* final */ M57 c;
    @DexIgnore
    public /* final */ Bitmap d;
    @DexIgnore
    public /* final */ Bi e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ BitmapDrawable b;

            @DexIgnore
            public Aii(BitmapDrawable bitmapDrawable) {
                this.b = bitmapDrawable;
            }

            @DexIgnore
            public void run() {
                N57.this.e.a(this.b);
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            N57 n57 = N57.this;
            BitmapDrawable bitmapDrawable = new BitmapDrawable(n57.a, I57.a(N57.this.b.get(), n57.d, n57.c));
            if (N57.this.e != null) {
                new Handler(Looper.getMainLooper()).post(new Aii(bitmapDrawable));
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(BitmapDrawable bitmapDrawable);
    }

    @DexIgnore
    public N57(Context context, Bitmap bitmap, M57 m57, Bi bi) {
        this.a = context.getResources();
        this.c = m57;
        this.e = bi;
        this.b = new WeakReference<>(context);
        this.d = bitmap;
    }

    @DexIgnore
    public void a() {
        f.execute(new Ai());
    }
}
