package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lx7 implements Dw7, Qu7 {
    @DexIgnore
    public static /* final */ Lx7 b; // = new Lx7();

    @DexIgnore
    @Override // com.fossil.Qu7
    public boolean c(Throwable th) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Dw7
    public void dispose() {
    }

    @DexIgnore
    public String toString() {
        return "NonDisposableHandle";
    }
}
