package com.fossil;

import com.mapped.Wg6;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F48 {
    @DexIgnore
    public static final boolean a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        Wg6.c(bArr, "a");
        Wg6.c(bArr2, "b");
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static final void b(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException("size=" + j + " offset=" + j2 + " byteCount=" + j3);
        }
    }

    @DexIgnore
    public static final int c(int i) {
        return ((i & 255) << 24) | ((-16777216 & i) >>> 24) | ((16711680 & i) >>> 8) | ((65280 & i) << 8);
    }

    @DexIgnore
    public static final short d(short s) {
        int i = 65535 & s;
        return (short) (((i & 65280) >>> 8) | ((i & 255) << 8));
    }

    @DexIgnore
    public static final String e(byte b) {
        return new String(new char[]{F58.f()[(b >> 4) & 15], F58.f()[b & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]});
    }

    @DexIgnore
    public static final String f(int i) {
        if (i == 0) {
            return "0";
        }
        char[] cArr = {(char) F58.f()[(i >> 28) & 15], (char) F58.f()[(i >> 24) & 15], (char) F58.f()[(i >> 20) & 15], (char) F58.f()[(i >> 16) & 15], (char) F58.f()[(i >> 12) & 15], (char) F58.f()[(i >> 8) & 15], (char) F58.f()[(i >> 4) & 15], (char) F58.f()[i & 15]};
        int i2 = 0;
        while (i2 < 8 && cArr[i2] == '0') {
            i2++;
        }
        return new String(cArr, i2, 8 - i2);
    }
}
