package com.fossil;

import com.misfit.frameworks.buttonservice.communite.CommunicateMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Yq5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[CommunicateMode.values().length];
        a = iArr;
        iArr[CommunicateMode.SENDING_ENCRYPTED_DATA_SESSION.ordinal()] = 1;
        a[CommunicateMode.HW_LOG_SYNC.ordinal()] = 2;
        a[CommunicateMode.SYNC.ordinal()] = 3;
        a[CommunicateMode.GET_BATTERY_LEVEL.ordinal()] = 4;
        a[CommunicateMode.OTA.ordinal()] = 5;
        a[CommunicateMode.MOVE_HAND.ordinal()] = 6;
        a[CommunicateMode.SET_ALARM.ordinal()] = 7;
        a[CommunicateMode.RECONNECT.ordinal()] = 8;
        a[CommunicateMode.EXCHANGE_SECRET_KEY.ordinal()] = 9;
        a[CommunicateMode.SET_WATCH_PARAMS.ordinal()] = 10;
        a[CommunicateMode.READ_CURRENT_WORKOUT_SESSION.ordinal()] = 11;
        a[CommunicateMode.SET_WATCH_APP_FILE_SESSION.ordinal()] = 12;
    }
    */
}
