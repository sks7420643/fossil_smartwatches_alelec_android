package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.B88;
import com.fossil.E88;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N88 {
    @DexIgnore
    public static /* final */ N88 a; // = e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends N88 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii implements Executor {
            @DexIgnore
            public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

            @DexIgnore
            public void execute(Runnable runnable) {
                this.b.post(runnable);
            }
        }

        @DexIgnore
        @Override // com.fossil.N88
        public List<? extends B88.Ai> a(Executor executor) {
            if (executor != null) {
                F88 f88 = new F88(executor);
                if (Build.VERSION.SDK_INT < 24) {
                    return Collections.singletonList(f88);
                }
                return Arrays.asList(D88.a, f88);
            }
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.N88
        public Executor b() {
            return new Aii();
        }

        @DexIgnore
        @Override // com.fossil.N88
        public List<? extends E88.Ai> c() {
            return Build.VERSION.SDK_INT >= 24 ? Collections.singletonList(L88.a) : Collections.emptyList();
        }

        @DexIgnore
        @Override // com.fossil.N88
        public int d() {
            return Build.VERSION.SDK_INT >= 24 ? 1 : 0;
        }

        @DexIgnore
        @Override // com.fossil.N88
        @IgnoreJRERequirement
        public boolean h(Method method) {
            if (Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return method.isDefault();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static class Bi extends N88 {
        @DexIgnore
        @Override // com.fossil.N88
        public List<? extends B88.Ai> a(Executor executor) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(D88.a);
            arrayList.add(new F88(executor));
            return Collections.unmodifiableList(arrayList);
        }

        @DexIgnore
        @Override // com.fossil.N88
        public List<? extends E88.Ai> c() {
            return Collections.singletonList(L88.a);
        }

        @DexIgnore
        @Override // com.fossil.N88
        public int d() {
            return 1;
        }

        @DexIgnore
        @Override // com.fossil.N88
        public Object g(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            Constructor declaredConstructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, Integer.TYPE);
            declaredConstructor.setAccessible(true);
            return ((MethodHandles.Lookup) declaredConstructor.newInstance(cls, -1)).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
        }

        @DexIgnore
        @Override // com.fossil.N88
        public boolean h(Method method) {
            return method.isDefault();
        }
    }

    @DexIgnore
    public static N88 e() {
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                return new Ai();
            }
        } catch (ClassNotFoundException e) {
        }
        try {
            Class.forName("java.util.Optional");
            return new Bi();
        } catch (ClassNotFoundException e2) {
            return new N88();
        }
    }

    @DexIgnore
    public static N88 f() {
        return a;
    }

    @DexIgnore
    public List<? extends B88.Ai> a(Executor executor) {
        return Collections.singletonList(new F88(executor));
    }

    @DexIgnore
    public Executor b() {
        return null;
    }

    @DexIgnore
    public List<? extends E88.Ai> c() {
        return Collections.emptyList();
    }

    @DexIgnore
    public int d() {
        return 0;
    }

    @DexIgnore
    public Object g(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean h(Method method) {
        return false;
    }
}
