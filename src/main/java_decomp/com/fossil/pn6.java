package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.WorkoutSessionRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pn6 extends Hq4 {
    @DexIgnore
    public String h;
    @DexIgnore
    public MutableLiveData<Ai> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutSessionRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public Ai() {
            this(0, null, false, 7, null);
        }

        @DexIgnore
        public Ai(int i, String str, boolean z) {
            Wg6.c(str, "networkErrorMessage");
            this.a = i;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(int i, String str, boolean z, int i2, Qg6 qg6) {
            this((i2 & 1) != 0 ? -1 : i, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? false : z);
        }

        @DexIgnore
        public final boolean a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Pn6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Object>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<Object>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WorkoutSessionRepository workoutSessionRepository = this.this$0.this$0.j;
                    String str = this.this$0.this$0.h;
                    if (str != null) {
                        this.L$0 = il6;
                        this.label = 1;
                        Object deleteWorkoutSession = workoutSessionRepository.deleteWorkoutSession(str, this);
                        return deleteWorkoutSession == d ? d : deleteWorkoutSession;
                    }
                    Wg6.i();
                    throw null;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Xe6 xe6, Pn6 pn6) {
            super(2, xe6);
            this.this$0 = pn6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(xe6, this.this$0);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Hq4.d(this.this$0, true, false, null, 6, null);
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ap4 ap4 = (Ap4) g;
            if (ap4 instanceof Kq5) {
                Hq4.d(this.this$0, false, true, null, 5, null);
                this.this$0.i.l(new Ai(0, null, true, 3, null));
            } else if (ap4 instanceof Hq5) {
                Hq4.d(this.this$0, false, true, null, 5, null);
                Hq5 hq5 = (Hq5) ap4;
                hq5.c();
                MutableLiveData mutableLiveData = this.this$0.i;
                ServerError c = hq5.c();
                if (c == null || (str = c.getMessage()) == null) {
                    str = "";
                }
                mutableLiveData.l(new Ai(hq5.a(), str, false));
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public Pn6(WorkoutSessionRepository workoutSessionRepository) {
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        this.j = workoutSessionRepository;
    }

    @DexIgnore
    public final void q() {
        if (this.h != null) {
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(null, this), 3, null);
        }
    }

    @DexIgnore
    public final MutableLiveData<Ai> r() {
        return this.i;
    }

    @DexIgnore
    public final void s(String str) {
        Wg6.c(str, "id");
        this.h = str;
    }
}
