package com.fossil;

import com.facebook.internal.FileLruCache;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class L03 extends Uz2 {
    @DexIgnore
    public static /* final */ Logger b; // = Logger.getLogger(L03.class.getName());
    @DexIgnore
    public static /* final */ boolean c; // = E43.m();
    @DexIgnore
    public N03 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends L03 {
        @DexIgnore
        public /* final */ byte[] d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public Ai(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i2 + 0;
                if (((bArr.length - i3) | i2 | 0) >= 0) {
                    this.d = bArr;
                    this.f = 0;
                    this.e = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)));
            }
            throw new NullPointerException(FileLruCache.BufferFile.FILE_NAME_PREFIX);
        }

        @DexIgnore
        public final void G0(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.d, this.f, i2);
                this.f += i2;
            } catch (IndexOutOfBoundsException e2) {
                throw new Bi(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), Integer.valueOf(i2)), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void O(int i) throws IOException {
            if (!L03.c || Qz2.b() || b() < 5) {
                while ((i & -128) != 0) {
                    try {
                        byte[] bArr = this.d;
                        int i2 = this.f;
                        this.f = i2 + 1;
                        bArr[i2] = (byte) ((byte) ((i & 127) | 128));
                        i >>>= 7;
                    } catch (IndexOutOfBoundsException e2) {
                        throw new Bi(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
                    }
                }
                byte[] bArr2 = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr2[i3] = (byte) ((byte) i);
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                E43.l(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.d;
                int i5 = this.f;
                this.f = i5 + 1;
                E43.l(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.d;
                    int i7 = this.f;
                    this.f = i7 + 1;
                    E43.l(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.d;
                int i8 = this.f;
                this.f = i8 + 1;
                E43.l(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.d;
                    int i10 = this.f;
                    this.f = i10 + 1;
                    E43.l(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.d;
                int i11 = this.f;
                this.f = i11 + 1;
                E43.l(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.d;
                    int i13 = this.f;
                    this.f = i13 + 1;
                    E43.l(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.d;
                int i14 = this.f;
                this.f = i14 + 1;
                E43.l(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.d;
                int i15 = this.f;
                this.f = i15 + 1;
                E43.l(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void P(int i, int i2) throws IOException {
            m(i, 0);
            j(i2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void R(int i, Xz2 xz2) throws IOException {
            m(1, 3);
            Y(2, i);
            o(3, xz2);
            m(1, 4);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void T(byte[] bArr, int i, int i2) throws IOException {
            O(i2);
            G0(bArr, 0, i2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void Y(int i, int i2) throws IOException {
            m(i, 0);
            O(i2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void Z(int i, long j) throws IOException {
            m(i, 1);
            a0(j);
        }

        @DexIgnore
        @Override // com.fossil.Uz2
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            G0(bArr, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void a0(long j) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                int i2 = i + 1;
                this.f = i2;
                bArr[i] = (byte) ((byte) ((int) j));
                byte[] bArr2 = this.d;
                int i3 = i2 + 1;
                this.f = i3;
                bArr2[i2] = (byte) ((byte) ((int) (j >> 8)));
                byte[] bArr3 = this.d;
                int i4 = i3 + 1;
                this.f = i4;
                bArr3[i3] = (byte) ((byte) ((int) (j >> 16)));
                byte[] bArr4 = this.d;
                int i5 = i4 + 1;
                this.f = i5;
                bArr4[i4] = (byte) ((byte) ((int) (j >> 24)));
                byte[] bArr5 = this.d;
                int i6 = i5 + 1;
                this.f = i6;
                bArr5[i5] = (byte) ((byte) ((int) (j >> 32)));
                byte[] bArr6 = this.d;
                int i7 = i6 + 1;
                this.f = i7;
                bArr6[i6] = (byte) ((byte) ((int) (j >> 40)));
                byte[] bArr7 = this.d;
                int i8 = i7 + 1;
                this.f = i8;
                bArr7[i7] = (byte) ((byte) ((int) (j >> 48)));
                byte[] bArr8 = this.d;
                this.f = i8 + 1;
                bArr8[i8] = (byte) ((byte) ((int) (j >> 56)));
            } catch (IndexOutOfBoundsException e2) {
                throw new Bi(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final int b() {
            return this.e - this.f;
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void f0(int i) throws IOException {
            try {
                byte[] bArr = this.d;
                int i2 = this.f;
                int i3 = i2 + 1;
                this.f = i3;
                bArr[i2] = (byte) ((byte) i);
                byte[] bArr2 = this.d;
                int i4 = i3 + 1;
                this.f = i4;
                bArr2[i3] = (byte) ((byte) (i >> 8));
                byte[] bArr3 = this.d;
                int i5 = i4 + 1;
                this.f = i5;
                bArr3[i4] = (byte) ((byte) (i >> 16));
                byte[] bArr4 = this.d;
                this.f = i5 + 1;
                bArr4[i5] = (byte) ((byte) (i >>> 24));
            } catch (IndexOutOfBoundsException e2) {
                throw new Bi(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void g(byte b) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                this.f = i + 1;
                bArr[i] = (byte) b;
            } catch (IndexOutOfBoundsException e2) {
                throw new Bi(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void j(int i) throws IOException {
            if (i >= 0) {
                O(i);
            } else {
                t((long) i);
            }
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void k0(int i, int i2) throws IOException {
            m(i, 5);
            f0(i2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void m(int i, int i2) throws IOException {
            O((i << 3) | i2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void n(int i, long j) throws IOException {
            m(i, 0);
            t(j);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void o(int i, Xz2 xz2) throws IOException {
            m(i, 2);
            u(xz2);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void p(int i, M23 m23) throws IOException {
            m(1, 3);
            Y(2, i);
            m(3, 2);
            v(m23);
            m(1, 4);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void q(int i, M23 m23, F33 f33) throws IOException {
            m(i, 2);
            Nz2 nz2 = (Nz2) m23;
            int f2 = nz2.f();
            if (f2 == -1) {
                f2 = f33.zzb(nz2);
                nz2.m(f2);
            }
            O(f2);
            f33.b(m23, this.a);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void r(int i, String str) throws IOException {
            m(i, 2);
            w(str);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void s(int i, boolean z) throws IOException {
            m(i, 0);
            g(z ? (byte) 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void t(long j) throws IOException {
            if (!L03.c || b() < 10) {
                while ((j & -128) != 0) {
                    try {
                        byte[] bArr = this.d;
                        int i = this.f;
                        this.f = i + 1;
                        bArr[i] = (byte) ((byte) ((((int) j) & 127) | 128));
                        j >>>= 7;
                    } catch (IndexOutOfBoundsException e2) {
                        throw new Bi(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
                    }
                }
                byte[] bArr2 = this.d;
                int i2 = this.f;
                this.f = i2 + 1;
                bArr2[i2] = (byte) ((byte) ((int) j));
                return;
            }
            while ((j & -128) != 0) {
                byte[] bArr3 = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                E43.l(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                j >>>= 7;
            }
            byte[] bArr4 = this.d;
            int i4 = this.f;
            this.f = i4 + 1;
            E43.l(bArr4, (long) i4, (byte) ((int) j));
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void u(Xz2 xz2) throws IOException {
            O(xz2.zza());
            xz2.zza(this);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void v(M23 m23) throws IOException {
            O(m23.l());
            m23.n(this);
        }

        @DexIgnore
        @Override // com.fossil.L03
        public final void w(String str) throws IOException {
            int i = this.f;
            try {
                int p0 = L03.p0(str.length() * 3);
                int p02 = L03.p0(str.length());
                if (p02 == p0) {
                    int i2 = i + p02;
                    this.f = i2;
                    int e2 = G43.e(str, this.d, i2, b());
                    this.f = i;
                    O((e2 - i) - p02);
                    this.f = e2;
                    return;
                }
                O(G43.d(str));
                this.f = G43.e(str, this.d, this.f, b());
            } catch (K43 e3) {
                this.f = i;
                x(str, e3);
            } catch (IndexOutOfBoundsException e4) {
                throw new Bi(e4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends IOException {
        @DexIgnore
        public Bi() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Bi(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r0 = java.lang.String.valueOf(r3)
                int r1 = r0.length()
                if (r1 == 0) goto L_0x0014
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                java.lang.String r0 = r1.concat(r0)
            L_0x0010:
                r2.<init>(r0, r4)
                return
            L_0x0014:
                java.lang.String r0 = new java.lang.String
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                r0.<init>(r1)
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.L03.Bi.<init>(java.lang.String, java.lang.Throwable):void");
        }

        @DexIgnore
        public Bi(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }
    }

    @DexIgnore
    public L03() {
    }

    @DexIgnore
    public static int A(float f) {
        return 4;
    }

    @DexIgnore
    public static int A0(int i) {
        return 4;
    }

    @DexIgnore
    public static int B(int i, double d) {
        return h0(i) + 8;
    }

    @DexIgnore
    public static int B0(int i, int i2) {
        return h0(i) + 4;
    }

    @DexIgnore
    public static int C(int i, float f) {
        return h0(i) + 4;
    }

    @DexIgnore
    public static int C0(int i) {
        return l0(i);
    }

    @DexIgnore
    public static int D(int i, U13 u13) {
        return (h0(1) << 1) + q0(2, i) + c(3, u13);
    }

    @DexIgnore
    public static int D0(int i, int i2) {
        return h0(i) + l0(i2);
    }

    @DexIgnore
    public static int E(int i, M23 m23) {
        return (h0(1) << 1) + q0(2, i) + h0(3) + J(m23);
    }

    @DexIgnore
    @Deprecated
    public static int E0(int i) {
        return p0(i);
    }

    @DexIgnore
    public static int F(int i, M23 m23, F33 f33) {
        return h0(i) + e(m23, f33);
    }

    @DexIgnore
    public static int F0(int i) {
        return (i >> 31) ^ (i << 1);
    }

    @DexIgnore
    public static int G(int i, String str) {
        return h0(i) + K(str);
    }

    @DexIgnore
    public static int H(int i, boolean z) {
        return h0(i) + 1;
    }

    @DexIgnore
    public static int I(Xz2 xz2) {
        int zza = xz2.zza();
        return zza + p0(zza);
    }

    @DexIgnore
    public static int J(M23 m23) {
        int l = m23.l();
        return l + p0(l);
    }

    @DexIgnore
    public static int K(String str) {
        int length;
        try {
            length = G43.d(str);
        } catch (K43 e) {
            length = str.getBytes(H13.a).length;
        }
        return length + p0(length);
    }

    @DexIgnore
    public static int L(boolean z) {
        return 1;
    }

    @DexIgnore
    public static int M(byte[] bArr) {
        int length = bArr.length;
        return length + p0(length);
    }

    @DexIgnore
    public static int U(int i, Xz2 xz2) {
        int h0 = h0(i);
        int zza = xz2.zza();
        return h0 + zza + p0(zza);
    }

    @DexIgnore
    @Deprecated
    public static int V(int i, M23 m23, F33 f33) {
        int h0 = h0(i);
        Nz2 nz2 = (Nz2) m23;
        int f = nz2.f();
        if (f == -1) {
            f = f33.zzb(nz2);
            nz2.m(f);
        }
        return f + (h0 << 1);
    }

    @DexIgnore
    @Deprecated
    public static int W(M23 m23) {
        return m23.l();
    }

    @DexIgnore
    public static int c(int i, U13 u13) {
        int h0 = h0(i);
        int b2 = u13.b();
        return h0 + b2 + p0(b2);
    }

    @DexIgnore
    public static int c0(int i, long j) {
        return h0(i) + j0(j);
    }

    @DexIgnore
    public static int d(U13 u13) {
        int b2 = u13.b();
        return b2 + p0(b2);
    }

    @DexIgnore
    public static int d0(int i, Xz2 xz2) {
        return (h0(1) << 1) + q0(2, i) + U(3, xz2);
    }

    @DexIgnore
    public static int e(M23 m23, F33 f33) {
        Nz2 nz2 = (Nz2) m23;
        int f = nz2.f();
        if (f == -1) {
            f = f33.zzb(nz2);
            nz2.m(f);
        }
        return f + p0(f);
    }

    @DexIgnore
    public static int e0(long j) {
        return j0(j);
    }

    @DexIgnore
    public static L03 f(byte[] bArr) {
        return new Ai(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static int h0(int i) {
        return p0(i << 3);
    }

    @DexIgnore
    public static int i0(int i, long j) {
        return h0(i) + j0(j);
    }

    @DexIgnore
    public static int j0(long j) {
        int i;
        long j2;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j2 = j >>> 28;
        } else {
            i = 2;
            j2 = j;
        }
        if ((-2097152 & j2) != 0) {
            i += 2;
            j2 >>>= 14;
        }
        return (j2 & -16384) != 0 ? i + 1 : i;
    }

    @DexIgnore
    public static int l0(int i) {
        if (i >= 0) {
            return p0(i);
        }
        return 10;
    }

    @DexIgnore
    public static int m0(int i, int i2) {
        return h0(i) + l0(i2);
    }

    @DexIgnore
    public static int n0(int i, long j) {
        return h0(i) + j0(z0(j));
    }

    @DexIgnore
    public static int o0(long j) {
        return j0(z0(j));
    }

    @DexIgnore
    public static int p0(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    @DexIgnore
    public static int q0(int i, int i2) {
        return h0(i) + p0(i2);
    }

    @DexIgnore
    public static int r0(int i, long j) {
        return h0(i) + 8;
    }

    @DexIgnore
    public static int s0(long j) {
        return 8;
    }

    @DexIgnore
    public static int t0(int i) {
        return p0(F0(i));
    }

    @DexIgnore
    public static int u0(int i, int i2) {
        return h0(i) + p0(F0(i2));
    }

    @DexIgnore
    public static int v0(int i, long j) {
        return h0(i) + 8;
    }

    @DexIgnore
    public static int w0(long j) {
        return 8;
    }

    @DexIgnore
    public static int x0(int i) {
        return 4;
    }

    @DexIgnore
    public static int y0(int i, int i2) {
        return h0(i) + 4;
    }

    @DexIgnore
    public static int z(double d) {
        return 8;
    }

    @DexIgnore
    public static long z0(long j) {
        return (j >> 63) ^ (j << 1);
    }

    @DexIgnore
    public final void N() {
        if (b() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    @DexIgnore
    public abstract void O(int i) throws IOException;

    @DexIgnore
    public abstract void P(int i, int i2) throws IOException;

    @DexIgnore
    public final void Q(int i, long j) throws IOException {
        n(i, z0(j));
    }

    @DexIgnore
    public abstract void R(int i, Xz2 xz2) throws IOException;

    @DexIgnore
    public final void S(long j) throws IOException {
        t(z0(j));
    }

    @DexIgnore
    public abstract void T(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    public final void X(int i) throws IOException {
        O(F0(i));
    }

    @DexIgnore
    public abstract void Y(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void Z(int i, long j) throws IOException;

    @DexIgnore
    public abstract void a0(long j) throws IOException;

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract void f0(int i) throws IOException;

    @DexIgnore
    public abstract void g(byte b2) throws IOException;

    @DexIgnore
    public final void g0(int i, int i2) throws IOException {
        Y(i, F0(i2));
    }

    @DexIgnore
    public final void h(double d) throws IOException {
        a0(Double.doubleToRawLongBits(d));
    }

    @DexIgnore
    public final void i(float f) throws IOException {
        f0(Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public abstract void j(int i) throws IOException;

    @DexIgnore
    public final void k(int i, double d) throws IOException {
        Z(i, Double.doubleToRawLongBits(d));
    }

    @DexIgnore
    public abstract void k0(int i, int i2) throws IOException;

    @DexIgnore
    public final void l(int i, float f) throws IOException {
        k0(i, Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public abstract void m(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void n(int i, long j) throws IOException;

    @DexIgnore
    public abstract void o(int i, Xz2 xz2) throws IOException;

    @DexIgnore
    public abstract void p(int i, M23 m23) throws IOException;

    @DexIgnore
    public abstract void q(int i, M23 m23, F33 f33) throws IOException;

    @DexIgnore
    public abstract void r(int i, String str) throws IOException;

    @DexIgnore
    public abstract void s(int i, boolean z) throws IOException;

    @DexIgnore
    public abstract void t(long j) throws IOException;

    @DexIgnore
    public abstract void u(Xz2 xz2) throws IOException;

    @DexIgnore
    public abstract void v(M23 m23) throws IOException;

    @DexIgnore
    public abstract void w(String str) throws IOException;

    @DexIgnore
    public final void x(String str, K43 k43) throws IOException {
        b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) k43);
        byte[] bytes = str.getBytes(H13.a);
        try {
            O(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new Bi(e);
        } catch (Bi e2) {
            throw e2;
        }
    }

    @DexIgnore
    public final void y(boolean z) throws IOException {
        g(z ? (byte) 1 : 0);
    }
}
