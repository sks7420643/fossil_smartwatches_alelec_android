package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F04 extends Zz3 {
    @DexIgnore
    public float a; // = -1.0f;

    @DexIgnore
    @Override // com.fossil.Zz3
    public void a(I04 i04, float f, float f2, float f3) {
        i04.n(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3 * f2, 180.0f, 180.0f - f);
        float f4 = 2.0f * f3 * f2;
        i04.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4, f4, 180.0f, f);
    }
}
