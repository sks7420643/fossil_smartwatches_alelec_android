package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T96 implements Factory<S96> {
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRepository> a;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> b;

    @DexIgnore
    public T96(Provider<WFBackgroundPhotoRepository> provider, Provider<RingStyleRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static T96 a(Provider<WFBackgroundPhotoRepository> provider, Provider<RingStyleRepository> provider2) {
        return new T96(provider, provider2);
    }

    @DexIgnore
    public static S96 c(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, RingStyleRepository ringStyleRepository) {
        return new S96(wFBackgroundPhotoRepository, ringStyleRepository);
    }

    @DexIgnore
    public S96 b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
