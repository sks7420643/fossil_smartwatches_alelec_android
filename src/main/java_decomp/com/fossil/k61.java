package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K61 extends D61 {
    @DexIgnore
    public /* final */ K48 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Q51 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public K61(K48 k48, String str, Q51 q51) {
        super(null);
        Wg6.c(k48, "source");
        Wg6.c(q51, "dataSource");
        this.a = k48;
        this.b = str;
        this.c = q51;
    }

    @DexIgnore
    public final Q51 a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final K48 c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof K61) {
                K61 k61 = (K61) obj;
                if (!Wg6.a(this.a, k61.a) || !Wg6.a(this.b, k61.b) || !Wg6.a(this.c, k61.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        K48 k48 = this.a;
        int hashCode = k48 != null ? k48.hashCode() : 0;
        String str = this.b;
        int hashCode2 = str != null ? str.hashCode() : 0;
        Q51 q51 = this.c;
        if (q51 != null) {
            i = q51.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "SourceResult(source=" + this.a + ", mimeType=" + this.b + ", dataSource=" + this.c + ")";
    }
}
