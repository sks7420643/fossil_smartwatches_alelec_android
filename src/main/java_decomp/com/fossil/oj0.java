package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oj0 {
    @DexIgnore
    public static int k; // = 1;
    @DexIgnore
    public String a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public float e;
    @DexIgnore
    public float[] f; // = new float[7];
    @DexIgnore
    public Ai g;
    @DexIgnore
    public Hj0[] h; // = new Hj0[8];
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public int j; // = 0;

    @DexIgnore
    public enum Ai {
        UNRESTRICTED,
        CONSTANT,
        SLACK,
        ERROR,
        UNKNOWN
    }

    @DexIgnore
    public Oj0(Ai ai, String str) {
        this.g = ai;
    }

    @DexIgnore
    public static void b() {
        k++;
    }

    @DexIgnore
    public final void a(Hj0 hj0) {
        int i2 = 0;
        while (true) {
            int i3 = this.i;
            if (i2 >= i3) {
                Hj0[] hj0Arr = this.h;
                if (i3 >= hj0Arr.length) {
                    this.h = (Hj0[]) Arrays.copyOf(hj0Arr, hj0Arr.length * 2);
                }
                Hj0[] hj0Arr2 = this.h;
                int i4 = this.i;
                hj0Arr2[i4] = hj0;
                this.i = i4 + 1;
                return;
            } else if (this.h[i2] != hj0) {
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void c(Hj0 hj0) {
        int i2 = this.i;
        for (int i3 = 0; i3 < i2; i3++) {
            if (this.h[i3] == hj0) {
                for (int i4 = 0; i4 < (i2 - i3) - 1; i4++) {
                    Hj0[] hj0Arr = this.h;
                    int i5 = i3 + i4;
                    hj0Arr[i5] = hj0Arr[i5 + 1];
                }
                this.i--;
                return;
            }
        }
    }

    @DexIgnore
    public void d() {
        this.a = null;
        this.g = Ai.UNKNOWN;
        this.d = 0;
        this.b = -1;
        this.c = -1;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = 0;
        this.j = 0;
    }

    @DexIgnore
    public void e(Ai ai, String str) {
        this.g = ai;
    }

    @DexIgnore
    public final void f(Hj0 hj0) {
        int i2 = this.i;
        for (int i3 = 0; i3 < i2; i3++) {
            Hj0[] hj0Arr = this.h;
            hj0Arr[i3].d.n(hj0Arr[i3], hj0, false);
        }
        this.i = 0;
    }

    @DexIgnore
    public String toString() {
        return "" + this.a;
    }
}
