package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Cd4 extends K84 {
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public Cd4(String str, String str2, Kb4 kb4, Ib4 ib4, String str3) {
        super(str, str2, kb4, ib4);
        this.f = str3;
    }

    @DexIgnore
    public final Jb4 g(Jb4 jb4, Vc4 vc4) {
        jb4.d("X-CRASHLYTICS-ORG-ID", vc4.a);
        jb4.d("X-CRASHLYTICS-GOOGLE-APP-ID", vc4.b);
        jb4.d("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        jb4.d("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
        return jb4;
    }

    @DexIgnore
    public final Jb4 h(Jb4 jb4, Vc4 vc4) {
        jb4.g("org_id", vc4.a);
        jb4.g("app[identifier]", vc4.c);
        jb4.g("app[name]", vc4.g);
        jb4.g("app[display_version]", vc4.d);
        jb4.g("app[build_version]", vc4.e);
        jb4.g("app[source]", Integer.toString(vc4.h));
        jb4.g("app[minimum_sdk_version]", vc4.i);
        jb4.g("app[built_sdk_version]", vc4.j);
        if (!R84.D(vc4.f)) {
            jb4.g("app[instance_identifier]", vc4.f);
        }
        return jb4;
    }

    @DexIgnore
    public boolean i(Vc4 vc4, boolean z) {
        if (z) {
            Jb4 c = c();
            g(c, vc4);
            h(c, vc4);
            X74 f2 = X74.f();
            f2.b("Sending app info to " + e());
            try {
                Lb4 b = c.b();
                int b2 = b.b();
                String str = "POST".equalsIgnoreCase(c.f()) ? "Create" : "Update";
                X74 f3 = X74.f();
                f3.b(str + " app request ID: " + b.d("X-REQUEST-ID"));
                X74 f4 = X74.f();
                f4.b("Result was " + b2);
                return N94.a(b2) == 0;
            } catch (IOException e) {
                X74.f().e("HTTP request failed.", e);
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }
}
