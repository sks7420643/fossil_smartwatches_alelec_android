package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G44<T> extends I44<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ I44<? super T> ordering;

    @DexIgnore
    public G44(I44<? super T> i44) {
        this.ordering = i44;
    }

    @DexIgnore
    @Override // com.fossil.I44, java.util.Comparator
    public int compare(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return 1;
        }
        if (t2 == null) {
            return -1;
        }
        return this.ordering.compare(t, t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof G44) {
            return this.ordering.equals(((G44) obj).ordering);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.ordering.hashCode() ^ -921210296;
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends T> I44<S> nullsFirst() {
        return this.ordering.nullsFirst();
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends T> I44<S> nullsLast() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends T> I44<S> reverse() {
        return this.ordering.reverse().nullsFirst();
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".nullsLast()";
    }
}
