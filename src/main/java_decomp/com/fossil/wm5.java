package com.fossil;

import com.mapped.Ku3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wm5 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public void d(Ku3 ku3) {
        if (ku3.s("platform")) {
            ku3.p("platform").f();
        }
        if (ku3.s("data")) {
            this.a = ku3.p("data").d().p("url").f();
        }
        if (ku3.s("metadata")) {
            this.b = ku3.p("metadata").d().p("checksum").f();
        }
        if (ku3.s("metadata")) {
            this.c = ku3.p("metadata").d().p("appVersion").f();
        }
        if (ku3.s("createdAt")) {
            ku3.p("createdAt").f();
        }
        if (ku3.s("updatedAt")) {
            this.d = ku3.p("updatedAt").f();
        }
        if (ku3.s("objectId")) {
            ku3.p("objectId").f();
        }
    }

    @DexIgnore
    public String toString() {
        return "[LocalizationResponse:, \ndownloadUrl=" + this.a + ", \nchecksum=" + this.b + ", \nappVersion=" + this.c + ", \nupdatedAt=" + this.d + "]";
    }
}
