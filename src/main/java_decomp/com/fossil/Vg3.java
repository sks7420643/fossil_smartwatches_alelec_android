package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vg3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Vg3> CREATOR; // = new Yg3();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Ug3 c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public Vg3(Vg3 vg3, long j) {
        Rc2.k(vg3);
        this.b = vg3.b;
        this.c = vg3.c;
        this.d = vg3.d;
        this.e = j;
    }

    @DexIgnore
    public Vg3(String str, Ug3 ug3, String str2, long j) {
        this.b = str;
        this.c = ug3;
        this.d = str2;
        this.e = j;
    }

    @DexIgnore
    public final String toString() {
        String str = this.d;
        String str2 = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 2, this.b, false);
        Bd2.t(parcel, 3, this.c, i, false);
        Bd2.u(parcel, 4, this.d, false);
        Bd2.r(parcel, 5, this.e);
        Bd2.b(parcel, a2);
    }
}
