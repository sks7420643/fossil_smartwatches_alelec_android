package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z86 implements MembersInjector<SearchSecondTimezonePresenter> {
    @DexIgnore
    public static void a(SearchSecondTimezonePresenter searchSecondTimezonePresenter) {
        searchSecondTimezonePresenter.r();
    }
}
