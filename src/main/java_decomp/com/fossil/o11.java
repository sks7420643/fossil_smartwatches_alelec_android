package com.fossil;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O11 {
    @DexIgnore
    public static /* final */ String a; // = X01.f("Schedulers");

    @DexIgnore
    public static N11 a(Context context, S11 s11) {
        if (Build.VERSION.SDK_INT >= 23) {
            D21 d21 = new D21(context, s11);
            Y31.a(context, SystemJobService.class, true);
            X01.c().a(a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return d21;
        }
        N11 c = c(context);
        if (c != null) {
            return c;
        }
        B21 b21 = new B21(context);
        Y31.a(context, SystemAlarmService.class, true);
        X01.c().a(a, "Created SystemAlarmScheduler", new Throwable[0]);
        return b21;
    }

    @DexIgnore
    public static void b(O01 o01, WorkDatabase workDatabase, List<N11> list) {
        if (list != null && list.size() != 0) {
            P31 j = workDatabase.j();
            workDatabase.beginTransaction();
            try {
                List<O31> g = j.g(o01.e());
                List<O31> c = j.c();
                if (g != null && g.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (O31 o31 : g) {
                        j.d(o31.a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (g != null && g.size() > 0) {
                    O31[] o31Arr = (O31[]) g.toArray(new O31[g.size()]);
                    for (N11 n11 : list) {
                        if (n11.c()) {
                            n11.a(o31Arr);
                        }
                    }
                }
                if (c != null && c.size() > 0) {
                    O31[] o31Arr2 = (O31[]) c.toArray(new O31[c.size()]);
                    for (N11 n112 : list) {
                        if (!n112.c()) {
                            n112.a(o31Arr2);
                        }
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    public static N11 c(Context context) {
        try {
            N11 n11 = (N11) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            X01.c().a(a, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
            return n11;
        } catch (Throwable th) {
            X01.c().a(a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
