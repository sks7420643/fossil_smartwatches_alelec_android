package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Wg6;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ml7 implements Collection<Ll7>, Jr7 {
    @DexIgnore
    public /* final */ int[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Hn7 {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ int[] c;

        @DexIgnore
        public Ai(int[] iArr) {
            Wg6.c(iArr, "array");
            this.c = iArr;
        }

        @DexIgnore
        @Override // com.fossil.Hn7
        public int b() {
            int i = this.b;
            int[] iArr = this.c;
            if (i < iArr.length) {
                this.b = i + 1;
                int i2 = iArr[i];
                Ll7.e(i2);
                return i2;
            }
            throw new NoSuchElementException(String.valueOf(this.b));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.length;
        }
    }

    @DexIgnore
    public static boolean b(int[] iArr, int i) {
        return Em7.z(iArr, i);
    }

    @DexIgnore
    public static boolean c(int[] iArr, Collection<Ll7> collection) {
        boolean z;
        Wg6.c(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof Ll7) || !Em7.z(iArr, t.j())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean e(int[] iArr, Object obj) {
        return (obj instanceof Ml7) && Wg6.a(iArr, ((Ml7) obj).m());
    }

    @DexIgnore
    public static int g(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public static int h(int[] iArr) {
        if (iArr != null) {
            return Arrays.hashCode(iArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean i(int[] iArr) {
        return iArr.length == 0;
    }

    @DexIgnore
    public static Hn7 k(int[] iArr) {
        return new Ai(iArr);
    }

    @DexIgnore
    public static String l(int[] iArr) {
        return "UIntArray(storage=" + Arrays.toString(iArr) + ")";
    }

    @DexIgnore
    public boolean a(int i) {
        return b(this.b, i);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(Ll7 ll7) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends Ll7> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof Ll7) {
            return a(((Ll7) obj).j());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return c(this.b, collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return e(this.b, obj);
    }

    @DexIgnore
    public int f() {
        return g(this.b);
    }

    @DexIgnore
    public int hashCode() {
        return h(this.b);
    }

    @DexIgnore
    public boolean isEmpty() {
        return i(this.b);
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator<Ll7> iterator() {
        return j();
    }

    @DexIgnore
    public Hn7 j() {
        return k(this.b);
    }

    @DexIgnore
    public final /* synthetic */ int[] m() {
        return this.b;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return f();
    }

    @DexIgnore
    public Object[] toArray() {
        return Jq7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) Jq7.b(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return l(this.b);
    }
}
