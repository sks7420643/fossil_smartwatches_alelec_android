package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Te {
    @DexIgnore
    public static /* final */ Te a; // = new Te();

    /*
    static {
        new Ve(24, 40, 0, 600);
        new Ve(9, 12, 0, 72);
        new Ve(80, 108, 4, 600);
    }
    */

    @DexIgnore
    public final boolean a(Se se, Ve ve) {
        int i = ve.b;
        int i2 = ve.c;
        int i3 = se.b;
        return i <= i3 && i2 >= i3 && se.c == ve.d && se.d >= ve.e;
    }
}
