package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zb0 implements Parcelable.Creator<Ac0> {
    @DexIgnore
    public /* synthetic */ Zb0(Qg6 qg6) {
    }

    @DexIgnore
    public final Ac0 a(byte[] bArr) {
        try {
            return new Ac0(bArr);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ac0 createFromParcel(Parcel parcel) {
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            Wg6.b(createByteArray, "parcel.createByteArray()!!");
            return new Ac0(createByteArray);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ac0[] newArray(int i) {
        return new Ac0[i];
    }
}
