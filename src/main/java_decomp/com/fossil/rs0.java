package com.fossil;

import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rs0 {
    @DexIgnore
    public /* final */ LifecycleRegistry a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public Ai c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Runnable {
        @DexIgnore
        public /* final */ LifecycleRegistry b;
        @DexIgnore
        public /* final */ Lifecycle.a c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public Ai(LifecycleRegistry lifecycleRegistry, Lifecycle.a aVar) {
            this.b = lifecycleRegistry;
            this.c = aVar;
        }

        @DexIgnore
        public void run() {
            if (!this.d) {
                this.b.i(this.c);
                this.d = true;
            }
        }
    }

    @DexIgnore
    public Rs0(LifecycleOwner lifecycleOwner) {
        this.a = new LifecycleRegistry(lifecycleOwner);
    }

    @DexIgnore
    public Lifecycle a() {
        return this.a;
    }

    @DexIgnore
    public void b() {
        f(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public void c() {
        f(Lifecycle.a.ON_CREATE);
    }

    @DexIgnore
    public void d() {
        f(Lifecycle.a.ON_STOP);
        f(Lifecycle.a.ON_DESTROY);
    }

    @DexIgnore
    public void e() {
        f(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public final void f(Lifecycle.a aVar) {
        Ai ai = this.c;
        if (ai != null) {
            ai.run();
        }
        Ai ai2 = new Ai(this.a, aVar);
        this.c = ai2;
        this.b.postAtFrontOfQueue(ai2);
    }
}
