package com.fossil;

import com.fossil.Dl7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D08 {
    @DexIgnore
    public static final void a(Xe6<? super Cd6> xe6, Xe6<?> xe62) {
        try {
            Xe6 c = Xn7.c(xe6);
            Dl7.Ai ai = Dl7.Companion;
            Wv7.b(c, Dl7.constructor-impl(Cd6.a));
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            xe62.resumeWith(Dl7.constructor-impl(El7.a(th)));
        }
    }

    @DexIgnore
    public static final <T> void b(Hg6<? super Xe6<? super T>, ? extends Object> hg6, Xe6<? super T> xe6) {
        try {
            Xe6 c = Xn7.c(Xn7.a(hg6, xe6));
            Dl7.Ai ai = Dl7.Companion;
            Wv7.b(c, Dl7.constructor-impl(Cd6.a));
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            xe6.resumeWith(Dl7.constructor-impl(El7.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void c(Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine, R r, Xe6<? super T> xe6) {
        try {
            Xe6 c = Xn7.c(Xn7.b(coroutine, r, xe6));
            Dl7.Ai ai = Dl7.Companion;
            Wv7.b(c, Dl7.constructor-impl(Cd6.a));
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            xe6.resumeWith(Dl7.constructor-impl(El7.a(th)));
        }
    }
}
