package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.Ac2;
import com.fossil.M62;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Db2 implements S92 {
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, Bb2<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, Bb2<?>> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<M62<?>, Boolean> d;
    @DexIgnore
    public /* final */ L72 e;
    @DexIgnore
    public /* final */ T82 f;
    @DexIgnore
    public /* final */ Lock g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ D62 i;
    @DexIgnore
    public /* final */ Condition j;
    @DexIgnore
    public /* final */ Ac2 k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ boolean m;
    @DexIgnore
    public /* final */ Queue<I72<?, ?>> s; // = new LinkedList();
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Map<G72<?>, Z52> u;
    @DexIgnore
    public Map<G72<?>, Z52> v;
    @DexIgnore
    public Eb2 w;
    @DexIgnore
    public Z52 x;

    @DexIgnore
    public Db2(Context context, Lock lock, Looper looper, D62 d62, Map<M62.Ci<?>, M62.Fi> map, Ac2 ac2, Map<M62<?>, Boolean> map2, M62.Ai<? extends Ys3, Gs3> ai, ArrayList<Wa2> arrayList, T82 t82, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.g = lock;
        this.h = looper;
        this.j = lock.newCondition();
        this.i = d62;
        this.f = t82;
        this.d = map2;
        this.k = ac2;
        this.l = z;
        HashMap hashMap = new HashMap();
        for (M62<?> m62 : map2.keySet()) {
            hashMap.put(m62.a(), m62);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Wa2 wa2 = arrayList.get(i2);
            i2++;
            Wa2 wa22 = wa2;
            hashMap2.put(wa22.b, wa22);
        }
        boolean z5 = false;
        boolean z6 = true;
        boolean z7 = false;
        for (Map.Entry<M62.Ci<?>, M62.Fi> entry : map.entrySet()) {
            M62 m622 = (M62) hashMap.get(entry.getKey());
            M62.Fi value = entry.getValue();
            if (!value.r()) {
                z2 = false;
                z3 = z7;
                z4 = z5;
            } else if (!this.d.get(m622).booleanValue()) {
                z2 = z6;
                z3 = true;
                z4 = true;
            } else {
                z2 = z6;
                z3 = z7;
                z4 = true;
            }
            Bb2<?> bb2 = new Bb2<>(context, m622, looper, value, (Wa2) hashMap2.get(m622), ac2, ai);
            this.b.put(entry.getKey(), bb2);
            if (value.v()) {
                this.c.put(entry.getKey(), bb2);
            }
            z5 = z4;
            z7 = z3;
            z6 = z2;
        }
        this.m = z5 && !z6 && !z7;
        this.e = L72.m();
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void a() {
        this.g.lock();
        try {
            this.t = false;
            this.u = null;
            this.v = null;
            if (this.w != null) {
                this.w.a();
                this.w = null;
            }
            this.x = null;
            while (!this.s.isEmpty()) {
                I72<?, ?> remove = this.s.remove();
                remove.n(null);
                remove.e();
            }
            this.j.signalAll();
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void b() {
        this.g.lock();
        try {
            if (!this.t) {
                this.t = true;
                this.u = null;
                this.v = null;
                this.w = null;
                this.x = null;
                this.e.D();
                this.e.g(this.b.values()).c(new Rf2(this.h), new Fb2(this));
                this.g.unlock();
            }
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final boolean c() {
        this.g.lock();
        try {
            return this.u != null && this.x == null;
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    public final Z52 d(M62<?> m62) {
        return i(m62.a());
    }

    @DexIgnore
    public final boolean e() {
        this.g.lock();
        try {
            return this.u == null && this.t;
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.S92
    public final boolean g(T72 t72) {
        this.g.lock();
        try {
            if (!this.t || t()) {
                this.g.unlock();
                return false;
            }
            this.e.D();
            this.w = new Eb2(this, t72);
            this.e.g(this.c.values()).c(new Rf2(this.h), this.w);
            this.g.unlock();
            return true;
        } catch (Throwable th) {
            this.g.unlock();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void h() {
        this.g.lock();
        try {
            this.e.a();
            if (this.w != null) {
                this.w.a();
                this.w = null;
            }
            if (this.v == null) {
                this.v = new Zi0(this.c.size());
            }
            Z52 z52 = new Z52(4);
            for (Bb2<?> bb2 : this.c.values()) {
                this.v.put(bb2.a(), z52);
            }
            if (this.u != null) {
                this.u.putAll(this.v);
            }
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    public final Z52 i(M62.Ci<?> ci) {
        this.g.lock();
        try {
            Bb2<?> bb2 = this.b.get(ci);
            if (this.u != null && bb2 != null) {
                return this.u.get(bb2.a());
            }
            this.g.unlock();
            return null;
        } finally {
            this.g.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t2) {
        M62.Ci<A> w2 = t2.w();
        if (!this.l || !y(t2)) {
            this.f.y.b(t2);
            this.b.get(w2).h(t2);
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t2) {
        if (!this.l || !y(t2)) {
            if (!c()) {
                this.s.add(t2);
            } else {
                this.f.y.b(t2);
                this.b.get(t2.w()).d(t2);
            }
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void l() {
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final Z52 m() {
        b();
        while (e()) {
            try {
                this.j.await();
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                return new Z52(15, null);
            }
        }
        if (c()) {
            return Z52.f;
        }
        Z52 z52 = this.x;
        return z52 == null ? new Z52(13, null) : z52;
    }

    @DexIgnore
    public final boolean q(Bb2<?> bb2, Z52 z52) {
        return !z52.A() && !z52.k() && this.d.get(bb2.i()).booleanValue() && bb2.s().r() && this.i.m(z52.c());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean t() {
        /*
            r3 = this;
            r1 = 0
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.lock()
            boolean r0 = r3.t     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x000e
            boolean r0 = r3.l     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0015
        L_0x000e:
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.unlock()
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            java.util.Map<com.fossil.M62$Ci<?>, com.fossil.Bb2<?>> r0 = r3.c
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x001f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r2.next()
            com.fossil.M62$Ci r0 = (com.fossil.M62.Ci) r0
            com.fossil.Z52 r0 = r3.i(r0)
            if (r0 == 0) goto L_0x0037
            boolean r0 = r0.A()
            if (r0 != 0) goto L_0x001f
        L_0x0037:
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.unlock()
            r0 = r1
            goto L_0x0014
        L_0x003e:
            java.util.concurrent.locks.Lock r0 = r3.g
            r0.unlock()
            r0 = 1
            goto L_0x0014
        L_0x0045:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r3.g
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Db2.t():boolean");
    }

    @DexIgnore
    public final void u() {
        if (this.k == null) {
            this.f.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(this.k.j());
        Map<M62<?>, Ac2.Bi> g2 = this.k.g();
        for (M62<?> m62 : g2.keySet()) {
            Z52 d2 = d(m62);
            if (d2 != null && d2.A()) {
                hashSet.addAll(g2.get(m62).a);
            }
        }
        this.f.q = hashSet;
    }

    @DexIgnore
    public final void v() {
        while (!this.s.isEmpty()) {
            j(this.s.remove());
        }
        this.f.b(null);
    }

    @DexIgnore
    public final Z52 w() {
        int i2 = 0;
        Z52 z52 = null;
        Z52 z522 = null;
        int i3 = 0;
        for (Bb2<?> bb2 : this.b.values()) {
            M62<?> i4 = bb2.i();
            Z52 z523 = this.u.get(bb2.a());
            if (!z523.A() && (!this.d.get(i4).booleanValue() || z523.k() || this.i.m(z523.c()))) {
                if (z523.c() != 4 || !this.l) {
                    int b2 = i4.c().b();
                    if (z522 == null || i3 > b2) {
                        z522 = z523;
                        i3 = b2;
                    }
                } else {
                    int b3 = i4.c().b();
                    if (z52 == null || i2 > b3) {
                        i2 = b3;
                        z52 = z523;
                    }
                }
            }
        }
        return (z522 == null || z52 == null || i3 <= i2) ? z522 : z52;
    }

    @DexIgnore
    public final <T extends I72<? extends Z62, ? extends M62.Bi>> boolean y(T t2) {
        M62.Ci<?> w2 = t2.w();
        Z52 i2 = i(w2);
        if (i2 == null || i2.c() != 4) {
            return false;
        }
        t2.A(new Status(4, null, this.e.c(this.b.get(w2).a(), System.identityHashCode(this.f))));
        return true;
    }
}
