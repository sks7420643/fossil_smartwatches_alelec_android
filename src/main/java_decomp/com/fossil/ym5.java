package com.fossil;

import android.text.TextUtils;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ym5 {
    @DexIgnore
    public static Xm5 a; // = new Xm5();
    @DexIgnore
    public static String b; // = ", ";

    @DexIgnore
    public static void a() {
        a.a();
        Tm5.d().b();
    }

    @DexIgnore
    public static String b(String str) {
        return TextUtils.isEmpty(str) ? "" : str.substring(str.lastIndexOf(File.separator) + 1);
    }

    @DexIgnore
    public static void c() {
        Xm5 xm5 = a;
        if (!(xm5 == null || xm5.b() == null)) {
            Tm5 d = Tm5.d();
            for (Map.Entry<String, String> entry : a.b().entrySet()) {
                if (d.a(entry.getKey())) {
                    d.e(entry.getKey());
                }
                d.g(entry.getKey(), entry.getValue());
            }
        }
    }

    @DexIgnore
    public static void d(String str, boolean z) {
        a.d(str, z);
    }

    @DexIgnore
    public static String e(int i) {
        return F68.c(i, "0123456789abcdef");
    }

    @DexIgnore
    public static String f(String str) {
        return a.c(str);
    }
}
