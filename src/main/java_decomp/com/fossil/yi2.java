package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yi2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Yi2> CREATOR; // = new Xi2();
    @DexIgnore
    public /* final */ Mo2 b;

    @DexIgnore
    public Yi2(IBinder iBinder) {
        this.b = Oo2.e(iBinder);
    }

    @DexIgnore
    public Yi2(Mo2 mo2) {
        this.b = mo2;
    }

    @DexIgnore
    public final String toString() {
        return String.format("DisableFitRequest", new Object[0]);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.m(parcel, 1, this.b.asBinder(), false);
        Bd2.b(parcel, a2);
    }
}
