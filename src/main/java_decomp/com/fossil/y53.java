package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y53 implements Xw2<X53> {
    @DexIgnore
    public static Y53 c; // = new Y53();
    @DexIgnore
    public /* final */ Xw2<X53> b;

    @DexIgnore
    public Y53() {
        this(Ww2.b(new A63()));
    }

    @DexIgnore
    public Y53(Xw2<X53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((X53) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ X53 zza() {
        return this.b.zza();
    }
}
