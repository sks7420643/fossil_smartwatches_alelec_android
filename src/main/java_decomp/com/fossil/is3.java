package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Is3 implements Parcelable.Creator<Js3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Js3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Intent intent = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i2 = Ad2.v(parcel, t);
            } else if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                intent = (Intent) Ad2.e(parcel, t, Intent.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Js3(i2, i, intent);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Js3[] newArray(int i) {
        return new Js3[i];
    }
}
