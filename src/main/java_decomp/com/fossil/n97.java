package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.Jn5;
import com.fossil.K96;
import com.mapped.AlertDialogFragment;
import com.mapped.Lc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N97 extends J77 implements K96.Bi, AlertDialogFragment.Gi {
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public Wg5 i;
    @DexIgnore
    public P97 j;
    @DexIgnore
    public M97 k;
    @DexIgnore
    public K96 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements D77 {
        @DexIgnore
        public /* final */ /* synthetic */ N97 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(N97 n97) {
            this.a = n97;
        }

        @DexIgnore
        @Override // com.fossil.D77
        public void a() {
            N97.L6(this.a).i(true);
        }

        @DexIgnore
        @Override // com.fossil.D77
        public void b(Fb7 fb7, int i) {
            Wg6.c(fb7, "watchFaceWrapper");
            N97.L6(this.a).g(i);
            N97.M6(this.a).B(fb7, i);
            N97.K6(this.a).q.smoothScrollToPosition(i);
        }

        @DexIgnore
        @Override // com.fossil.D77
        public void c() {
            this.a.S6();
        }

        @DexIgnore
        @Override // com.fossil.D77
        public void d(Fb7 fb7) {
            Wg6.c(fb7, AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_PHOTO);
            if (Wg6.a(N97.M6(this.a).y(), fb7.b())) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.R(childFragmentManager);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("background_photo_id_extra", fb7.b());
            S37 s372 = S37.c;
            FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager2, "childFragmentManager");
            s372.e(childFragmentManager2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.D77
        public void onCancel() {
            N97.L6(this.a).i(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<Lc6<? extends List<Object>, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ N97 a;

        @DexIgnore
        public Bi(N97 n97) {
            this.a = n97;
        }

        @DexIgnore
        public final void a(Lc6<? extends List<Object>, Integer> lc6) {
            N97.L6(this.a).h((List) lc6.getFirst(), lc6.getSecond().intValue());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends List<Object>, ? extends Integer> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Fb7> {
        @DexIgnore
        public /* final */ /* synthetic */ N97 a;

        @DexIgnore
        public Ci(N97 n97) {
            this.a = n97;
        }

        @DexIgnore
        public final void a(Fb7 fb7) {
            Gc7 e = Hc7.c.e(this.a);
            if (e != null) {
                e.z(fb7);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Fb7 fb7) {
            a(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ N97 a;

        @DexIgnore
        public Di(N97 n97) {
            this.a = n97;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(Object obj) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.R(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Fb7> {
        @DexIgnore
        public /* final */ /* synthetic */ N97 a;

        @DexIgnore
        public Ei(N97 n97) {
            this.a = n97;
        }

        @DexIgnore
        public final void a(Fb7 fb7) {
            P97.F(N97.M6(this.a), fb7.b(), false, 2, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Fb7 fb7) {
            a(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ N97 a;

        @DexIgnore
        public Fi(N97 n97) {
            this.a = n97;
        }

        @DexIgnore
        public final void a(String str) {
            P97 M6 = N97.M6(this.a);
            if (str == null) {
                str = "";
            }
            M6.E(str, true);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Wg5 K6(N97 n97) {
        Wg5 wg5 = n97.i;
        if (wg5 != null) {
            return wg5;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ M97 L6(N97 n97) {
        M97 m97 = n97.k;
        if (m97 != null) {
            return m97;
        }
        Wg6.n("photoAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ P97 M6(N97 n97) {
        P97 p97 = n97.j;
        if (p97 != null) {
            return p97;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFacePhotoBackgroundFragment";
    }

    @DexIgnore
    public final void O6() {
        this.k = new M97(new Ai(this));
        Wg5 wg5 = this.i;
        if (wg5 != null) {
            RecyclerView recyclerView = wg5.q;
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(null);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
            M97 m97 = this.k;
            if (m97 != null) {
                recyclerView.setAdapter(m97);
            } else {
                Wg6.n("photoAdapter");
                throw null;
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6() {
        MutableLiveData<String> r;
        LiveData<Fb7> e;
        P97 p97 = this.j;
        if (p97 != null) {
            p97.w().h(getViewLifecycleOwner(), new Bi(this));
            P97 p972 = this.j;
            if (p972 != null) {
                p972.x().h(getViewLifecycleOwner(), new Ci(this));
                P97 p973 = this.j;
                if (p973 != null) {
                    p973.u().h(getViewLifecycleOwner(), new Di(this));
                    Gc7 e2 = Hc7.c.e(this);
                    if (!(e2 == null || (e = e2.e()) == null)) {
                        e.h(getViewLifecycleOwner(), new Ei(this));
                    }
                    Gc7 e3 = Hc7.c.e(this);
                    if (e3 != null && (r = e3.r()) != null) {
                        r.h(getViewLifecycleOwner(), new Fi(this));
                        return;
                    }
                    return;
                }
                Wg6.n("viewModel");
                throw null;
            }
            Wg6.n("viewModel");
            throw null;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void Q6(Uri uri) {
        Intent intent = new Intent(getContext(), EditPhotoActivity.class);
        intent.putExtra("IMAGE_URI_EXTRA", uri);
        startActivityForResult(intent, 3);
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (Wg6.a(str, "DELETE_PHOTO_BACKGROUND") && i2 == 2131363373) {
            String stringExtra = intent != null ? intent.getStringExtra("background_photo_id_extra") : null;
            P97 p97 = this.j;
            if (p97 != null) {
                if (stringExtra == null) {
                    stringExtra = "";
                }
                p97.t(stringExtra);
                return;
            }
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void R6(String str) {
        Uri parse = Uri.parse(str);
        Wg6.b(parse, "Uri.parse(uri)");
        Q6(parse);
    }

    @DexIgnore
    public final void S6() {
        K96 a2 = K96.t.a();
        this.l = a2;
        if (a2 != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            a2.show(childFragmentManager, "AddPhotoMenuFragment");
        }
        K96 k96 = this.l;
        if (k96 != null) {
            k96.B6(this);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().w().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(P97.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026undViewModel::class.java)");
            P97 p97 = (P97) a2;
            this.j = p97;
            if (p97 != null) {
                p97.z();
                String d = ThemeManager.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    Wg5 wg5 = this.i;
                    if (wg5 != null) {
                        wg5.r.setBackgroundColor(Color.parseColor(d));
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                }
                O6();
                P6();
                return;
            }
            Wg6.n("viewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        Bundle extras;
        boolean z = true;
        Uri uri = null;
        String str = null;
        if (i3 == -1) {
            if (i2 == 1) {
                if (intent != null) {
                    uri = intent.getData();
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("WatchFacePhotoBackgroundFragment", "From Gallery, uri == " + uri);
                if (uri != null) {
                    String uri2 = uri.toString();
                    Wg6.b(uri2, "uri.toString()");
                    R6(uri2);
                }
            } else if (i2 == 2) {
                Uri g = Vk5.g(intent, PortfolioApp.get.instance());
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("WatchFacePhotoBackgroundFragment", "From Camera, uri = " + g);
                if (g != null) {
                    str = g.getPath();
                }
                if (!(str == null || str.length() == 0)) {
                    z = false;
                }
                if (!z) {
                    R6(str);
                }
            } else if (i2 == 3) {
                String string = (intent == null || (extras = intent.getExtras()) == null) ? null : extras.getString("WATCH_FACE_ID");
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.e("WatchFacePhotoBackgroundFragment", "Image is cropped and saved with watchFaceId = " + string);
                P97 p97 = this.j;
                if (p97 != null) {
                    if (string == null) {
                        string = "";
                    }
                    p97.A(string);
                    return;
                }
                Wg6.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Wg5 z = Wg5.z(layoutInflater);
        Wg6.b(z, "WatchFacePhotoBackground\u2026Binding.inflate(inflater)");
        this.i = z;
        if (z != null) {
            View n = z.n();
            Wg6.b(n, "binding.root");
            return n;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment, com.fossil.J77
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.K96.Bi
    public void p5() {
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @DexIgnore
    @Override // com.fossil.K96.Bi
    public void u1() {
        FragmentActivity activity;
        Intent d;
        if (Jn5.c(Jn5.b, getActivity(), Jn5.Ai.CAPTURE_IMAGE, false, false, false, null, 60, null) && (activity = getActivity()) != null && (d = Vk5.d(activity)) != null) {
            startActivityForResult(d, 2);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.J77
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
