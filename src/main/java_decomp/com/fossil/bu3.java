package com.fossil;

import com.mapped.Lc3;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bu3<TResult> implements Hu3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public Lc3 c;

    @DexIgnore
    public Bu3(Executor executor, Lc3 lc3) {
        this.a = executor;
        this.c = lc3;
    }

    @DexIgnore
    @Override // com.fossil.Hu3
    public final void a(Nt3<TResult> nt3) {
        if (!nt3.q() && !nt3.o()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new Cu3(this, nt3));
                }
            }
        }
    }
}
