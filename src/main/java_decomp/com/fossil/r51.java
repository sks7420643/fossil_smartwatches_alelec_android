package com.fossil;

import android.graphics.drawable.Drawable;
import com.mapped.Wg6;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R51 {
    @DexIgnore
    public /* final */ Drawable a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public R51(Drawable drawable, boolean z) {
        Wg6.c(drawable, ResourceManager.DRAWABLE);
        this.a = drawable;
        this.b = z;
    }

    @DexIgnore
    public final Drawable a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof R51) {
                R51 r51 = (R51) obj;
                if (!Wg6.a(this.a, r51.a) || this.b != r51.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Drawable drawable = this.a;
        int hashCode = drawable != null ? drawable.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "DecodeResult(drawable=" + this.a + ", isSampled=" + this.b + ")";
    }
}
