package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N22 implements Factory<String> {
    @DexIgnore
    public static /* final */ N22 a; // = new N22();

    @DexIgnore
    public static N22 a() {
        return a;
    }

    @DexIgnore
    public static String b() {
        String a2 = M22.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public String c() {
        return b();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return c();
    }
}
