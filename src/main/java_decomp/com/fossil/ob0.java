package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ob0 extends Mb0 {
    @DexIgnore
    public static /* final */ Nb0 CREATOR; // = new Nb0(null);

    @DexIgnore
    public /* synthetic */ Ob0(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Ob0(Bv1 bv1, Ry1 ry1) {
        super(bv1, ry1);
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public List<Va0> b() {
        return Gm7.b(new Ka0());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }
}
