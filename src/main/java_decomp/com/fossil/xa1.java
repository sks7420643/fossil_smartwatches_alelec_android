package com.fossil;

import com.fossil.Xa1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xa1<CHILD extends Xa1<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    @DexIgnore
    public Uj1<? super TranscodeType> b; // = Sj1.b();

    @DexIgnore
    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return d();
    }

    @DexIgnore
    public final CHILD d() {
        try {
            return (CHILD) ((Xa1) super.clone());
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @DexIgnore
    public final Uj1<? super TranscodeType> e() {
        return this.b;
    }
}
