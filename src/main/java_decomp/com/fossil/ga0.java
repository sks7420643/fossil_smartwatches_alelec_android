package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ga0 extends Va0 {
    @DexIgnore
    public static /* final */ Gb0 CREATOR; // = new Gb0(null);
    @DexIgnore
    public /* final */ Fa0 c;

    @DexIgnore
    public /* synthetic */ Ga0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.c = Fa0.valueOf(readString);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public Ga0(Fa0 fa0) {
        super(Aa0.l);
        this.c = fa0;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c.b);
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ga0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Ga0) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.VibeInstr");
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Va0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.C2, Ey1.a(this.c));
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
