package com.fossil;

import android.app.Activity;
import android.app.ActivityOptions;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sk0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Sk0 {
        @DexIgnore
        public /* final */ ActivityOptions a;

        @DexIgnore
        public Ai(ActivityOptions activityOptions) {
            this.a = activityOptions;
        }

        @DexIgnore
        @Override // com.fossil.Sk0
        public Bundle b() {
            return this.a.toBundle();
        }
    }

    @DexIgnore
    public static Sk0 a(Activity activity, Ln0<View, String>... ln0Arr) {
        if (Build.VERSION.SDK_INT < 21) {
            return new Sk0();
        }
        Pair[] pairArr = null;
        if (ln0Arr != null) {
            Pair[] pairArr2 = new Pair[ln0Arr.length];
            for (int i = 0; i < ln0Arr.length; i++) {
                pairArr2[i] = Pair.create(ln0Arr[i].a, ln0Arr[i].b);
            }
            pairArr = pairArr2;
        }
        return new Ai(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    @DexIgnore
    public Bundle b() {
        return null;
    }
}
