package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ck2<T> implements Fk2<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public Ck2(T t) {
        this.a = t;
    }

    @DexIgnore
    @Override // com.fossil.Fk2
    public final T get() {
        return this.a;
    }
}
