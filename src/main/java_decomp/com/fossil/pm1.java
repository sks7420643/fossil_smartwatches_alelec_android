package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pm1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ int g; // = Hy1.a(Gr7.a);
    @DexIgnore
    public static /* final */ int h; // = Hy1.a(Gr7.a);
    @DexIgnore
    public static /* final */ int i; // = Hy1.a(Gr7.a);
    @DexIgnore
    public static /* final */ int j; // = Hy1.a(Gr7.a);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Pm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Pm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 12) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new Pm1(Hy1.n(order.getShort(1)), Hy1.n(order.getShort(3)), Hy1.n(order.getShort(5)), Hy1.n(order.getShort(7)));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 12"));
        }

        @DexIgnore
        public Pm1 b(Parcel parcel) {
            return new Pm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Pm1 createFromParcel(Parcel parcel) {
            return new Pm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Pm1[] newArray(int i) {
            return new Pm1[i];
        }
    }

    @DexIgnore
    public Pm1(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        super(Zm1.CYCLING_CADENCE);
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        d();
    }

    @DexIgnore
    public /* synthetic */ Pm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).put((byte) 2).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).putShort((short) this.f).putShort(0).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.B5, Integer.valueOf(this.c));
            G80.k(jSONObject, Jd0.C5, Integer.valueOf(this.d));
            G80.k(jSONObject, Jd0.D5, Integer.valueOf(this.e));
            G80.k(jSONObject, Jd0.E5, Integer.valueOf(this.f));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        int i2 = g;
        int i3 = this.c;
        if (i3 >= 0 && i2 >= i3) {
            int i4 = h;
            int i5 = this.d;
            if (i5 >= 0 && i4 >= i5) {
                int i6 = i;
                int i7 = this.e;
                if (i7 >= 0 && i6 >= i7) {
                    int i8 = j;
                    int i9 = this.f;
                    if (i9 < 0 || i8 < i9) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder e2 = E.e("cog(");
                        e2.append(this.f);
                        e2.append(") is out of range ");
                        e2.append("[0, ");
                        throw new IllegalArgumentException(E.b(e2, j, "]."));
                    }
                    return;
                }
                StringBuilder e3 = E.e("chainring(");
                e3.append(this.e);
                e3.append(") is out of range ");
                e3.append("[0, ");
                throw new IllegalArgumentException(E.b(e3, i, "]."));
            }
            StringBuilder e4 = E.e("tire size(");
            e4.append(this.d);
            e4.append(") is out of range ");
            e4.append("[0, ");
            throw new IllegalArgumentException(E.b(e4, h, "]."));
        }
        StringBuilder e5 = E.e("diameter(");
        e5.append(this.c);
        e5.append(") is out of range ");
        e5.append("[0, ");
        throw new IllegalArgumentException(E.b(e5, g, "]."));
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Pm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Pm1 pm1 = (Pm1) obj;
            if (this.c != pm1.c) {
                return false;
            }
            if (this.d != pm1.d) {
                return false;
            }
            if (this.e != pm1.e) {
                return false;
            }
            return this.f == pm1.f;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CyclingCadenceConfig");
    }

    @DexIgnore
    public final int getChainring() {
        return this.e;
    }

    @DexIgnore
    public final int getCog() {
        return this.f;
    }

    @DexIgnore
    public final int getDiameterInMillimeter() {
        return this.c;
    }

    @DexIgnore
    public final int getTireSizeInMillimeter() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (((((this.c * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
