package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.source.FileRepository;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ y87 f4259a; // = new y87();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2", f = "StorageHelper.kt", l = {51, 52, 53}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FileRepository $fileRepository;
        @DexIgnore
        public /* final */ /* synthetic */ s77 $wfAssetRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.y87$a$a")
        @eo7(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2$initFontTask$1", f = "StorageHelper.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.y87$a$a  reason: collision with other inner class name */
        public static final class C0293a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public C0293a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0293a aVar = new C0293a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0293a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    if (!z87.d.e()) {
                        z87.d.g();
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2$initRingTask$1", f = "StorageHelper.kt", l = {40}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object f;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    if (!a97.b.d()) {
                        s77 s77 = this.this$0.$wfAssetRepository;
                        this.L$0 = iv7;
                        this.label = 1;
                        f = s77.f(this);
                        if (f == d) {
                            return d;
                        }
                    }
                    return tl7.f3441a;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    f = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                for (p77 p77 : (List) f) {
                    File fileByRemoteUrl = this.this$0.$fileRepository.getFileByRemoteUrl(p77.c().a());
                    Bitmap decodeFile = BitmapFactory.decodeFile(fileByRemoteUrl != null ? fileByRemoteUrl.getAbsolutePath() : null);
                    if (decodeFile != null) {
                        a97.b.a(p77.d(), decodeFile);
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2$initStickerTask$1", f = "StorageHelper.kt", l = {24}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    if (!b97.c.d()) {
                        s77 s77 = this.this$0.$wfAssetRepository;
                        this.L$0 = iv7;
                        this.label = 1;
                        g = s77.g(this);
                        if (g == d) {
                            return d;
                        }
                    }
                    return tl7.f3441a;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<p77> list = (List) g;
                for (p77 p77 : list) {
                    String b = ym5.b(p77.c().a());
                    FileRepository fileRepository = this.this$0.$fileRepository;
                    pq7.b(b, "fileName");
                    File fileByName = fileRepository.getFileByName(b, FileType.WATCH_FACE);
                    Bitmap decodeFile = BitmapFactory.decodeFile(fileByName != null ? fileByName.getAbsolutePath() : null);
                    if (decodeFile != null) {
                        b97.c.a(p77.f(), decodeFile);
                    }
                }
                b97.c.g(list);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(s77 s77, FileRepository fileRepository, qn7 qn7) {
            super(2, qn7);
            this.$wfAssetRepository = s77;
            this.$fileRepository = fileRepository;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$wfAssetRepository, this.$fileRepository, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00bd  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 3
                r9 = 1
                r4 = 2
                r2 = 0
                java.lang.Object r8 = com.fossil.yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x007b
                if (r0 == r9) goto L_0x0055
                if (r0 == r4) goto L_0x0030
                if (r0 != r10) goto L_0x0028
                java.lang.Object r0 = r11.L$3
                com.fossil.rv7 r0 = (com.fossil.rv7) r0
                java.lang.Object r0 = r11.L$2
                com.fossil.rv7 r0 = (com.fossil.rv7) r0
                java.lang.Object r0 = r11.L$1
                com.fossil.rv7 r0 = (com.fossil.rv7) r0
                java.lang.Object r0 = r11.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r12)
            L_0x0025:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0027:
                return r0
            L_0x0028:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0030:
                java.lang.Object r0 = r11.L$3
                com.fossil.rv7 r0 = (com.fossil.rv7) r0
                java.lang.Object r1 = r11.L$2
                com.fossil.rv7 r1 = (com.fossil.rv7) r1
                java.lang.Object r2 = r11.L$1
                com.fossil.rv7 r2 = (com.fossil.rv7) r2
                java.lang.Object r3 = r11.L$0
                com.fossil.iv7 r3 = (com.fossil.iv7) r3
                com.fossil.el7.b(r12)
            L_0x0043:
                r11.L$0 = r3
                r11.L$1 = r2
                r11.L$2 = r1
                r11.L$3 = r0
                r11.label = r10
                java.lang.Object r0 = r0.l(r11)
                if (r0 != r8) goto L_0x0025
                r0 = r8
                goto L_0x0027
            L_0x0055:
                java.lang.Object r0 = r11.L$3
                com.fossil.rv7 r0 = (com.fossil.rv7) r0
                java.lang.Object r1 = r11.L$2
                com.fossil.rv7 r1 = (com.fossil.rv7) r1
                java.lang.Object r2 = r11.L$1
                com.fossil.rv7 r2 = (com.fossil.rv7) r2
                java.lang.Object r3 = r11.L$0
                com.fossil.iv7 r3 = (com.fossil.iv7) r3
                com.fossil.el7.b(r12)
                r5 = r0
            L_0x0069:
                r11.L$0 = r3
                r11.L$1 = r2
                r11.L$2 = r1
                r11.L$3 = r5
                r11.label = r4
                java.lang.Object r0 = r1.l(r11)
                if (r0 != r8) goto L_0x00bd
                r0 = r8
                goto L_0x0027
            L_0x007b:
                com.fossil.el7.b(r12)
                com.fossil.iv7 r0 = r11.p$
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.y87$a$a r3 = new com.fossil.y87$a$a
                r3.<init>(r2)
                r5 = r2
                com.fossil.rv7 r6 = com.fossil.eu7.b(r0, r1, r2, r3, r4, r5)
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.y87$a$c r3 = new com.fossil.y87$a$c
                r3.<init>(r11, r2)
                r5 = r2
                com.fossil.rv7 r7 = com.fossil.eu7.b(r0, r1, r2, r3, r4, r5)
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.y87$a$b r3 = new com.fossil.y87$a$b
                r3.<init>(r11, r2)
                r5 = r2
                com.fossil.rv7 r5 = com.fossil.eu7.b(r0, r1, r2, r3, r4, r5)
                r11.L$0 = r0
                r11.L$1 = r6
                r11.L$2 = r7
                r11.L$3 = r5
                r11.label = r9
                java.lang.Object r1 = r6.l(r11)
                if (r1 != r8) goto L_0x00bf
                r0 = r8
                goto L_0x0027
            L_0x00bd:
                r0 = r5
                goto L_0x0043
            L_0x00bf:
                r2 = r6
                r1 = r7
                r3 = r0
                goto L_0x0069
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.y87.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public final void a() {
        z87.d.b();
        b97.c.b();
        a97.b.b();
    }

    @DexIgnore
    public final Object b(s77 s77, FileRepository fileRepository, qn7<? super tl7> qn7) {
        Object e = jv7.e(new a(s77, fileRepository, null), qn7);
        return e == yn7.d() ? e : tl7.f3441a;
    }
}
