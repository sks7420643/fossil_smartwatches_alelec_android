package com.fossil;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dh1 implements Qb1<File, File> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(File file, Ob1 ob1) throws IOException {
        return d(file, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<File> b(File file, int i, int i2, Ob1 ob1) throws IOException {
        return c(file, i, i2, ob1);
    }

    @DexIgnore
    public Id1<File> c(File file, int i, int i2, Ob1 ob1) {
        return new Eh1(file);
    }

    @DexIgnore
    public boolean d(File file, Ob1 ob1) {
        return true;
    }
}
