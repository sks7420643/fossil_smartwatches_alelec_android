package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class On4 extends Mn4 {
    @DexIgnore
    @Override // com.fossil.Jn4, com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (kl4 == Kl4.UPC_E) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode UPC_E, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.Jn4
    public boolean[] c(String str) {
        if (str.length() == 8) {
            int i = Nn4.f[Integer.parseInt(str.substring(7, 8))];
            boolean[] zArr = new boolean[51];
            int b = Jn4.b(zArr, 0, Ln4.a, true) + 0;
            int i2 = 1;
            while (i2 <= 6) {
                int i3 = i2 + 1;
                int parseInt = Integer.parseInt(str.substring(i2, i3));
                if (((i >> (6 - i2)) & 1) == 1) {
                    parseInt += 10;
                }
                b = Jn4.b(zArr, b, Ln4.e[parseInt], false) + b;
                i2 = i3;
            }
            Jn4.b(zArr, b, Ln4.c, false);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
