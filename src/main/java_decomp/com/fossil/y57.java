package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.SectionIndexer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y57 extends RecyclerView.AdapterDataObserver {
    @DexIgnore
    @SuppressLint({"HandlerLeak"})
    public Handler A; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float f4246a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public RecyclerView j; // = null;
    @DexIgnore
    public SectionIndexer k; // = null;
    @DexIgnore
    public String[] l; // = null;
    @DexIgnore
    public RectF m;
    @DexIgnore
    public int n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public int s;
    @DexIgnore
    public Typeface t; // = null;
    @DexIgnore
    public Boolean u; // = Boolean.TRUE;
    @DexIgnore
    public Boolean v; // = Boolean.FALSE;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Handler {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1) {
                y57.this.j.invalidate();
            }
        }
    }

    @DexIgnore
    public y57(Context context, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        this.n = alphabetFastScrollRecyclerView.e;
        this.o = alphabetFastScrollRecyclerView.f;
        this.p = alphabetFastScrollRecyclerView.g;
        this.q = alphabetFastScrollRecyclerView.h;
        this.s = alphabetFastScrollRecyclerView.i;
        this.w = alphabetFastScrollRecyclerView.k;
        this.x = alphabetFastScrollRecyclerView.l;
        this.y = alphabetFastScrollRecyclerView.m;
        this.z = i(alphabetFastScrollRecyclerView.j);
        this.d = context.getResources().getDisplayMetrics().density;
        this.e = context.getResources().getDisplayMetrics().scaledDensity;
        this.j = alphabetFastScrollRecyclerView;
        p(alphabetFastScrollRecyclerView.getAdapter());
        float f2 = this.o;
        float f3 = this.d;
        this.f4246a = f2 * f3;
        this.b = this.p * f3;
        this.c = ((float) this.q) * f3;
    }

    @DexIgnore
    public void A(int i2) {
        this.q = i2;
    }

    @DexIgnore
    public void B(boolean z2) {
        this.r = z2;
    }

    @DexIgnore
    public void C(Typeface typeface) {
        this.t = typeface;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
    public void a() {
        super.a();
        this.l = (String[]) this.k.getSections();
    }

    @DexIgnore
    public boolean h(float f2, float f3) {
        RectF rectF = this.m;
        if (f2 >= rectF.left) {
            float f4 = rectF.top;
            return f3 >= f4 && f3 <= rectF.height() + f4;
        }
    }

    @DexIgnore
    public final int i(float f2) {
        return (int) (255.0f * f2);
    }

    @DexIgnore
    public void j(Canvas canvas) {
        int i2;
        if (this.u.booleanValue()) {
            Paint paint = new Paint();
            paint.setColor(this.w);
            paint.setAlpha(this.z);
            paint.setAntiAlias(true);
            RectF rectF = this.m;
            int i3 = this.s;
            float f2 = this.d;
            canvas.drawRoundRect(rectF, ((float) i3) * f2, ((float) i3) * f2, paint);
            String[] strArr = this.l;
            if (strArr != null && strArr.length > 0) {
                if (this.r && (i2 = this.h) >= 0 && !TextUtils.isEmpty(strArr[i2])) {
                    Paint paint2 = new Paint();
                    paint2.setColor(-16777216);
                    paint2.setAlpha(0);
                    paint2.setAntiAlias(true);
                    paint2.setShadowLayer(3.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Color.argb(64, 0, 0, 0));
                    Paint paint3 = new Paint();
                    paint3.setColor(-1);
                    paint3.setAntiAlias(true);
                    paint3.setTextSize(this.e * 50.0f);
                    paint3.setTypeface(this.t);
                    float descent = ((this.c * 2.0f) + paint3.descent()) - paint3.ascent();
                    int i4 = this.f;
                    int i5 = this.g;
                    RectF rectF2 = new RectF((((float) i4) - descent) / 1.15f, (((float) i5) - descent) / 5.0f, ((((float) i4) - descent) / 1.0f) + (descent / 2.0f), descent + ((((float) i5) - descent) / 5.0f));
                    float f3 = this.d;
                    canvas.drawRoundRect(rectF2, f3 * 5.0f, f3 * 5.0f, paint2);
                    float f4 = rectF2.right;
                    float f5 = rectF2.left;
                    canvas.drawText(this.l[this.h], (((f4 - f5) / 2.0f) + f5) - (paint3.measureText(this.l[this.h]) / 2.0f), ((rectF2.top + this.c) - paint3.ascent()) + 1.0f, paint3);
                    k();
                }
                Paint paint4 = new Paint();
                paint4.setColor(this.x);
                paint4.setAntiAlias(true);
                paint4.setTextSize(((float) this.n) * this.e);
                paint4.setTypeface(this.t);
                float height = (this.m.height() - (this.b * 2.0f)) / ((float) this.l.length);
                float descent2 = (height - (paint4.descent() - paint4.ascent())) / 2.0f;
                for (int i6 = 0; i6 < this.l.length; i6++) {
                    if (!this.v.booleanValue()) {
                        String str = this.l[i6];
                        RectF rectF3 = this.m;
                        canvas.drawText(str, ((this.f4246a - paint4.measureText(this.l[i6])) / 2.0f) + rectF3.left, (((rectF3.top + this.b) + (((float) i6) * height)) + descent2) - paint4.ascent(), paint4);
                    } else {
                        int i7 = this.h;
                        if (i7 <= -1 || i6 != i7) {
                            paint4.setTypeface(this.t);
                            paint4.setTextSize(((float) this.n) * this.e);
                            paint4.setColor(this.x);
                        } else {
                            paint4.setTypeface(Typeface.create(this.t, 1));
                            paint4.setTextSize(((float) (this.n + 3)) * this.e);
                            paint4.setColor(this.y);
                        }
                        String str2 = this.l[i6];
                        RectF rectF4 = this.m;
                        canvas.drawText(str2, ((this.f4246a - paint4.measureText(this.l[i6])) / 2.0f) + rectF4.left, (((rectF4.top + this.b) + (((float) i6) * height)) + descent2) - paint4.ascent(), paint4);
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void k() {
        this.A.removeMessages(0);
        this.A.sendEmptyMessageAtTime(1, SystemClock.uptimeMillis() + 300);
    }

    @DexIgnore
    public final int l(float f2) {
        String[] strArr = this.l;
        if (strArr == null || strArr.length == 0) {
            return 0;
        }
        RectF rectF = this.m;
        float f3 = rectF.top;
        if (f2 < this.b + f3) {
            return 0;
        }
        float height = rectF.height();
        float f4 = this.b;
        if (f2 >= (height + f3) - f4) {
            return this.l.length - 1;
        }
        RectF rectF2 = this.m;
        return (int) (((f2 - rectF2.top) - f4) / ((rectF2.height() - (this.b * 2.0f)) / ((float) this.l.length)));
    }

    @DexIgnore
    public void m(int i2, int i3, int i4, int i5) {
        this.f = i2;
        this.g = i3;
        float f2 = (float) i2;
        float f3 = this.b;
        this.m = new RectF((f2 - f3) - this.f4246a, f3, f2 - f3, ((float) i3) - f3);
    }

    @DexIgnore
    public boolean n(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2 && this.i) {
                    if (!h(motionEvent.getX(), motionEvent.getY())) {
                        return true;
                    }
                    this.h = l(motionEvent.getY());
                    o();
                    return true;
                }
            } else if (this.i) {
                this.i = false;
                this.h = -1;
            }
        } else if (h(motionEvent.getX(), motionEvent.getY())) {
            this.i = true;
            this.h = l(motionEvent.getY());
            o();
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void o() {
        try {
            int positionForSection = this.k.getPositionForSection(this.h);
            RecyclerView.m layoutManager = this.j.getLayoutManager();
            if (layoutManager != null) {
                if (layoutManager instanceof LinearLayoutManager) {
                    ((LinearLayoutManager) layoutManager).D2(positionForSection, 0);
                } else {
                    layoutManager.y1(positionForSection);
                }
            }
        } catch (Exception e2) {
            Log.d("INDEX_BAR", "Data size returns null");
        }
    }

    @DexIgnore
    public void p(RecyclerView.g gVar) {
        if (gVar instanceof SectionIndexer) {
            gVar.registerAdapterDataObserver(this);
            SectionIndexer sectionIndexer = (SectionIndexer) gVar;
            this.k = sectionIndexer;
            this.l = (String[]) sectionIndexer.getSections();
        }
    }

    @DexIgnore
    public void q(int i2) {
        this.w = i2;
    }

    @DexIgnore
    public void r(int i2) {
        this.s = i2;
    }

    @DexIgnore
    public void s(int i2) {
        this.y = i2;
    }

    @DexIgnore
    public void t(boolean z2) {
        this.v = Boolean.valueOf(z2);
    }

    @DexIgnore
    public void u(int i2) {
        this.x = i2;
    }

    @DexIgnore
    public void v(float f2) {
        this.z = i(f2);
    }

    @DexIgnore
    public void w(boolean z2) {
        this.u = Boolean.valueOf(z2);
    }

    @DexIgnore
    public void x(int i2) {
        this.n = i2;
    }

    @DexIgnore
    public void y(float f2) {
        this.b = f2;
    }

    @DexIgnore
    public void z(float f2) {
        this.f4246a = f2;
    }
}
