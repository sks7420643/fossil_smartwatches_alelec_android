package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Md3 extends IInterface {
    @DexIgnore
    Ac3 F2(Rg2 rg2) throws RemoteException;

    @DexIgnore
    void p2(Rg2 rg2, int i) throws RemoteException;

    @DexIgnore
    Bc3 q0(Rg2 rg2, GoogleMapOptions googleMapOptions) throws RemoteException;

    @DexIgnore
    Ec3 t1(Rg2 rg2, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException;

    @DexIgnore
    Yb3 zze() throws RemoteException;

    @DexIgnore
    Fs2 zzf() throws RemoteException;
}
