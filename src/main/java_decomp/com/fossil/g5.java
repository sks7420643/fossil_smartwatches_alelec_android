package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G5 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ N6 d;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] e;

    @DexIgnore
    public G5(K5 k5, G7 g7, N6 n6, byte[] bArr) {
        this.b = k5;
        this.c = g7;
        this.d = n6;
        this.e = bArr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.a.f();
        this.b.z.a.c(new L7(this.c, this.d, this.e));
    }
}
