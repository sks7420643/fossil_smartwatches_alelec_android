package com.fossil;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B24 {
    @DexIgnore
    public static /* final */ D14 a; // = D14.i(", ").k("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements B14<Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Collection b;

        @DexIgnore
        public Ai(Collection collection) {
            this.b = collection;
        }

        @DexIgnore
        @Override // com.fossil.B14
        public Object apply(Object obj) {
            return obj == this.b ? "(this Collection)" : obj;
        }
    }

    @DexIgnore
    public static <T> Collection<T> a(Iterable<T> iterable) {
        return (Collection) iterable;
    }

    @DexIgnore
    public static boolean b(Collection<?> collection, Collection<?> collection2) {
        return O34.b(collection2, K14.b(collection));
    }

    @DexIgnore
    public static StringBuilder c(int i) {
        A24.b(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824L));
    }

    @DexIgnore
    public static boolean d(Collection<?> collection, Object obj) {
        I14.l(collection);
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException e) {
            return false;
        }
    }

    @DexIgnore
    public static String e(Collection<?> collection) {
        StringBuilder c = c(collection.size());
        c.append('[');
        a.c(c, O34.k(collection, new Ai(collection)));
        c.append(']');
        return c.toString();
    }
}
