package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rf2 implements Executor {
    @DexIgnore
    public /* final */ Handler b;

    @DexIgnore
    public Rf2(Looper looper) {
        this.b = new Xl2(looper);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.post(runnable);
    }
}
