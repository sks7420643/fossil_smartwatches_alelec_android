package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cb7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Cb7(String str, String str2, String str3) {
        Wg6.c(str, "ringId");
        Wg6.c(str2, "name");
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Cb7) {
                Cb7 cb7 = (Cb7) obj;
                if (!Wg6.a(this.a, cb7.a) || !Wg6.a(this.b, cb7.b) || !Wg6.a(this.c, cb7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "RingWrapper(ringId=" + this.a + ", name=" + this.b + ", url=" + this.c + ")";
    }
}
