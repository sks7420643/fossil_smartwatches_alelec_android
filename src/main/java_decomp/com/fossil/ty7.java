package com.fossil;

import com.mapped.Cd6;
import com.mapped.Wg6;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ty7<E> extends Jy7<E> {
    @DexIgnore
    public static /* final */ Vz7 e; // = new Vz7("EMPTY");
    @DexIgnore
    public /* final */ ReentrantLock c; // = new ReentrantLock();
    @DexIgnore
    public Object d; // = e;

    @DexIgnore
    @Override // com.fossil.Ly7
    public String e() {
        return "(value=" + this.d + ')';
    }

    @DexIgnore
    @Override // com.fossil.Jy7
    public boolean p(Uy7<? super E> uy7) {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return super.p(uy7);
        } finally {
            reentrantLock.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.Jy7
    public final boolean q() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Jy7
    public final boolean r() {
        return this.d == e;
    }

    @DexIgnore
    @Override // com.fossil.Jy7
    public Object u() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            if (this.d == e) {
                Object f = f();
                if (f == null) {
                    f = Ky7.b;
                }
                return f;
            }
            Object obj = this.d;
            this.d = e;
            Cd6 cd6 = Cd6.a;
            reentrantLock.unlock();
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }

    @DexIgnore
    public Object x(E e2) {
        Wy7<E> l;
        Vz7 e3;
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            Py7<?> f = f();
            if (f != null) {
                return f;
            }
            if (this.d == e) {
                do {
                    l = l();
                    if (l != null) {
                        if (l instanceof Py7) {
                            if (l != null) {
                                reentrantLock.unlock();
                                return l;
                            }
                            Wg6.i();
                            throw null;
                        } else if (l != null) {
                            e3 = l.e(e2, null);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                } while (e3 == null);
                if (Nv7.a()) {
                    if (!(e3 == Mu7.a)) {
                        throw new AssertionError();
                    }
                }
                Cd6 cd6 = Cd6.a;
                reentrantLock.unlock();
                if (l != null) {
                    l.d(e2);
                    if (l != null) {
                        return l.a();
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            this.d = e2;
            Object obj = Ky7.a;
            reentrantLock.unlock();
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }
}
