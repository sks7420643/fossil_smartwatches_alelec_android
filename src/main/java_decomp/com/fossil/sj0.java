package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Uj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sj0 {
    @DexIgnore
    public Uj0 a;
    @DexIgnore
    public Uj0 b;
    @DexIgnore
    public Uj0 c;
    @DexIgnore
    public Uj0 d;
    @DexIgnore
    public Uj0 e;
    @DexIgnore
    public Uj0 f;
    @DexIgnore
    public Uj0 g;
    @DexIgnore
    public ArrayList<Uj0> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;

    @DexIgnore
    public Sj0(Uj0 uj0, int i2, boolean z) {
        this.a = uj0;
        this.l = i2;
        this.m = z;
    }

    @DexIgnore
    public static boolean c(Uj0 uj0, int i2) {
        if (uj0.C() != 8 && uj0.C[i2] == Uj0.Bi.MATCH_CONSTRAINT) {
            int[] iArr = uj0.g;
            if (iArr[i2] == 0 || iArr[i2] == 3) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a() {
        if (!this.q) {
            b();
        }
        this.q = true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00b7 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
        // Method dump skipped, instructions count: 225
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sj0.b():void");
    }
}
