package com.fossil;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings;
import android.text.TextUtils;
import com.facebook.LegacyTokenHelper;
import com.facebook.internal.Utility;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class R84 {
    @DexIgnore
    public static /* final */ char[] a; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    @DexIgnore
    public static long b; // = -1;
    @DexIgnore
    public static /* final */ Comparator<File> c; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Comparator<File> {
        @DexIgnore
        public int a(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(File file, File file2) {
            return a(file, file2);
        }
    }

    @DexIgnore
    public enum Bi {
        X86_32,
        X86_64,
        ARM_UNKNOWN,
        PPC,
        PPC64,
        ARMV6,
        ARMV7,
        UNKNOWN,
        ARMV7S,
        ARM64;
        
        @DexIgnore
        public static /* final */ Map<String, Bi> b;

        /*
        static {
            HashMap hashMap = new HashMap(4);
            b = hashMap;
            hashMap.put("armeabi-v7a", ARMV7);
            b.put("armeabi", ARMV6);
            b.put("arm64-v8a", ARM64);
            b.put("x86", X86_32);
        }
        */

        @DexIgnore
        public static Bi getValue() {
            String str = Build.CPU_ABI;
            if (TextUtils.isEmpty(str)) {
                X74.f().b("Architecture#getValue()::Build.CPU_ABI returned null or empty");
                return UNKNOWN;
            }
            Bi bi = b.get(str.toLowerCase(Locale.US));
            return bi == null ? UNKNOWN : bi;
        }
    }

    @DexIgnore
    public static boolean A(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    @DexIgnore
    public static boolean B() {
        return Debug.isDebuggerConnected() || Debug.waitingForDebugger();
    }

    @DexIgnore
    public static boolean C(Context context) {
        return "sdk".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT) || Settings.Secure.getString(context.getContentResolver(), "android_id") == null;
    }

    @DexIgnore
    public static boolean D(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static boolean E(Context context) {
        boolean C = C(context);
        String str = Build.TAGS;
        if ((!C && str != null && str.contains("test-keys")) || new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        return !C && new File("/system/xbin/su").exists();
    }

    @DexIgnore
    public static String F(Context context) {
        int s = s(context, "com.google.firebase.crashlytics.unity_version", LegacyTokenHelper.TYPE_STRING);
        if (s == 0) {
            return null;
        }
        String string = context.getResources().getString(s);
        X74 f = X74.f();
        f.b("Unity Editor version is: " + string);
        return string;
    }

    @DexIgnore
    public static String G(InputStream inputStream) {
        return w(inputStream, Utility.HASH_ALGORITHM_SHA1);
    }

    @DexIgnore
    public static String H(String str) {
        return x(str, Utility.HASH_ALGORITHM_SHA1);
    }

    @DexIgnore
    public static String I(InputStream inputStream) throws IOException {
        Scanner useDelimiter = new Scanner(inputStream).useDelimiter("\\A");
        return useDelimiter.hasNext() ? useDelimiter.next() : "";
    }

    @DexIgnore
    public static long a(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    @DexIgnore
    public static long b(String str) {
        StatFs statFs = new StatFs(str);
        long blockSize = (long) statFs.getBlockSize();
        return (((long) statFs.getBlockCount()) * blockSize) - (((long) statFs.getAvailableBlocks()) * blockSize);
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public static boolean c(Context context) {
        if (!d(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @DexIgnore
    public static boolean d(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    @DexIgnore
    public static void e(Closeable closeable, String str) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                X74.f().e(str, e);
            }
        }
    }

    @DexIgnore
    public static void f(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    @DexIgnore
    public static long g(String str, String str2, int i) {
        return Long.parseLong(str.split(str2)[0].trim()) * ((long) i);
    }

    @DexIgnore
    public static String h(String... strArr) {
        if (strArr != null) {
            if (strArr.length == 0) {
                return null;
            }
            ArrayList<String> arrayList = new ArrayList();
            for (String str : strArr) {
                if (str != null) {
                    arrayList.add(str.replace(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "").toLowerCase(Locale.US));
                }
            }
            Collections.sort(arrayList);
            StringBuilder sb = new StringBuilder();
            for (String str2 : arrayList) {
                sb.append(str2);
            }
            String sb2 = sb.toString();
            if (sb2.length() > 0) {
                return H(sb2);
            }
        }
        return null;
    }

    @DexIgnore
    public static String i(File file, String str) {
        BufferedReader bufferedReader;
        Exception e;
        String str2 = null;
        str2 = null;
        str2 = null;
        BufferedReader bufferedReader2 = null;
        if (file.exists()) {
            try {
                bufferedReader = new BufferedReader(new FileReader(file), 1024);
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        String[] split = Pattern.compile("\\s*:\\s*").split(readLine, 2);
                        if (split.length > 1 && split[0].equals(str)) {
                            str2 = split[1];
                            break;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            X74.f().e("Error parsing " + file, e);
                            e(bufferedReader, "Failed to close system file reader.");
                            return str2;
                        } catch (Throwable th) {
                            th = th;
                            bufferedReader2 = bufferedReader;
                        }
                    }
                }
            } catch (Exception e3) {
                e = e3;
                bufferedReader = null;
                X74.f().e("Error parsing " + file, e);
                e(bufferedReader, "Failed to close system file reader.");
                return str2;
            } catch (Throwable th2) {
                th = th2;
                e(bufferedReader2, "Failed to close system file reader.");
                throw th;
            }
            e(bufferedReader, "Failed to close system file reader.");
        }
        return str2;
    }

    @DexIgnore
    public static void j(Flushable flushable, String str) {
        if (flushable != null) {
            try {
                flushable.flush();
            } catch (IOException e) {
                X74.f().e(str, e);
            }
        }
    }

    @DexIgnore
    public static ActivityManager.RunningAppProcessInfo k(String str, Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.processName.equals(str)) {
                    return runningAppProcessInfo;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public static boolean l(Context context, String str, boolean z) {
        Resources resources;
        if (context == null || (resources = context.getResources()) == null) {
            return z;
        }
        int s = s(context, str, LegacyTokenHelper.TYPE_BOOLEAN);
        if (s > 0) {
            return resources.getBoolean(s);
        }
        int s2 = s(context, str, LegacyTokenHelper.TYPE_STRING);
        return s2 > 0 ? Boolean.parseBoolean(context.getString(s2)) : z;
    }

    @DexIgnore
    public static int m() {
        return Bi.getValue().ordinal();
    }

    @DexIgnore
    public static int n(Context context) {
        int i = C(context) ? 1 : 0;
        if (E(context)) {
            i |= 2;
        }
        return B() ? i | 4 : i;
    }

    @DexIgnore
    public static SharedPreferences o(Context context) {
        return context.getSharedPreferences("com.crashlytics.prefs", 0);
    }

    @DexIgnore
    public static String p(Context context) {
        int s = s(context, "com.google.firebase.crashlytics.mapping_file_id", LegacyTokenHelper.TYPE_STRING);
        if (s == 0) {
            s = s(context, "com.crashlytics.android.build_id", LegacyTokenHelper.TYPE_STRING);
        }
        if (s != 0) {
            return context.getResources().getString(s);
        }
        return null;
    }

    @DexIgnore
    public static boolean q(Context context) {
        if (C(context)) {
            return false;
        }
        return ((SensorManager) context.getSystemService("sensor")).getDefaultSensor(8) != null;
    }

    @DexIgnore
    public static String r(Context context) {
        int i = context.getApplicationContext().getApplicationInfo().icon;
        if (i <= 0) {
            return context.getPackageName();
        }
        try {
            return context.getResources().getResourcePackageName(i);
        } catch (Resources.NotFoundException e) {
            return context.getPackageName();
        }
    }

    @DexIgnore
    public static int s(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str, str2, r(context));
    }

    @DexIgnore
    public static SharedPreferences t(Context context) {
        return context.getSharedPreferences("com.google.firebase.crashlytics", 0);
    }

    @DexIgnore
    public static String u(Context context, String str) {
        int s = s(context, str, LegacyTokenHelper.TYPE_STRING);
        return s > 0 ? context.getString(s) : "";
    }

    @DexIgnore
    public static long v() {
        long j;
        long j2;
        synchronized (R84.class) {
            try {
                if (b == -1) {
                    String i = i(new File("/proc/meminfo"), "MemTotal");
                    if (!TextUtils.isEmpty(i)) {
                        String upperCase = i.toUpperCase(Locale.US);
                        try {
                            if (upperCase.endsWith("KB")) {
                                j2 = g(upperCase, "KB", 1024);
                            } else if (upperCase.endsWith("MB")) {
                                j2 = g(upperCase, "MB", 1048576);
                            } else if (upperCase.endsWith("GB")) {
                                j2 = g(upperCase, "GB", 1073741824);
                            } else {
                                X74.f().b("Unexpected meminfo format while computing RAM: " + upperCase);
                                j2 = 0;
                            }
                        } catch (NumberFormatException e) {
                            X74.f().e("Unexpected meminfo format while computing RAM: " + upperCase, e);
                            j2 = 0;
                        }
                    } else {
                        j2 = 0;
                    }
                    b = j2;
                }
                j = b;
            } catch (Throwable th) {
                throw th;
            }
        }
        return j;
    }

    @DexIgnore
    public static String w(InputStream inputStream, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return z(instance.digest());
                }
                instance.update(bArr, 0, read);
            }
        } catch (Exception e) {
            X74.f().e("Could not calculate hash for app icon.", e);
            return "";
        }
    }

    @DexIgnore
    public static String x(String str, String str2) {
        return y(str.getBytes(), str2);
    }

    @DexIgnore
    public static String y(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return z(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            X74 f = X74.f();
            f.e("Could not create hashing algorithm: " + str + ", returning empty string.", e);
            return "";
        }
    }

    @DexIgnore
    public static String z(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i] & 255;
            int i3 = i * 2;
            char[] cArr2 = a;
            cArr[i3] = (char) cArr2[i2 >>> 4];
            cArr[i3 + 1] = (char) cArr2[i2 & 15];
        }
        return new String(cArr);
    }
}
