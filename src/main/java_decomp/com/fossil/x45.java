package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ TodayHeartRateChart s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    public X45(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, TodayHeartRateChart todayHeartRateChart, FlexibleButton flexibleButton, ImageView imageView, ImageView imageView2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view2, View view3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = todayHeartRateChart;
        this.t = flexibleButton;
        this.u = imageView;
        this.v = imageView2;
        this.w = flexibleTextView;
        this.x = flexibleTextView2;
        this.y = view2;
        this.z = view3;
    }
}
