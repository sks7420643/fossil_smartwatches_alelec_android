package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Rv2 implements Pv2 {
    @DexIgnore
    public /* final */ Sv2 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Rv2(Sv2 sv2, String str) {
        this.a = sv2;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.Pv2
    public final Object zza() {
        return this.a.c(this.b);
    }
}
