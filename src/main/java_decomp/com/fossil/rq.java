package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.FitnessData;
import com.mapped.H60;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rq extends Lp {
    @DexIgnore
    public /* final */ boolean C; // = true;
    @DexIgnore
    public FitnessData[] D; // = new FitnessData[0];
    @DexIgnore
    public byte[][] E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public float I; // = 0.5f;
    @DexIgnore
    public float J; // = 0.5f;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ int N;
    @DexIgnore
    public /* final */ ArrayList<Ow> O;
    @DexIgnore
    public /* final */ H60 P;
    @DexIgnore
    public /* final */ HashMap<Hu1, Object> Q;

    @DexIgnore
    public Rq(K5 k5, I60 i60, H60 h60, HashMap<Hu1, Object> hashMap) {
        super(k5, i60, Yp.o0, null, false, 24);
        this.P = h60;
        this.Q = hashMap;
        Boolean bool = (Boolean) hashMap.get(Hu1.SKIP_LIST);
        this.K = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) this.Q.get(Hu1.SKIP_ERASE);
        this.L = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) this.Q.get(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.M = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) this.Q.get(Hu1.NUMBER_OF_FILE_REQUIRED);
        this.N = num != null ? num.intValue() : 0;
        this.O = By1.a(this.i, Hm7.c(Ow.d, Ow.e));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        if (Q3.f.f(this.x.a())) {
            this.I = 1.0f;
            this.J = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            J();
            return;
        }
        Lp.i(this, new Iv(Ke.b.a(this.w.x, Ob.e), this.w, 0, 4), new Jl(this), new Vl(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        JSONObject put = super.C().put(Zm1.BIOMETRIC_PROFILE.b(), this.P.c()).put(Ey1.a(Hu1.SKIP_LIST), this.K).put(Ey1.a(Hu1.SKIP_ERASE), this.L).put(Ey1.a(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.M).put(Ey1.a(Hu1.NUMBER_OF_FILE_REQUIRED), this.N);
        Wg6.b(put, "super.optionDescription(\u2026me, numberOfFileRequired)");
        return put;
    }

    @DexIgnore
    public final void I() {
        Lp.i(this, new Iv(Ke.b.a(this.w.x, Ob.f), this.w, 0, 4), new Im(this), new Um(this), null, new Gn(this), null, 40, null);
    }

    @DexIgnore
    public final void J() {
        Lp.h(this, new Mi(this.w, this.x, this.P, this.Q, this.z, null, 32), new Dp(this), new Qp(this), new Dq(this), null, null, 48, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean t() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.O;
    }
}
