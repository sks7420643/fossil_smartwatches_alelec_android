package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Rt {
    c((byte) 0),
    d((byte) 1),
    e((byte) 2);
    
    @DexIgnore
    public static /* final */ Qt g; // = new Qt(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Rt(byte b2) {
        this.b = (byte) b2;
    }
}
