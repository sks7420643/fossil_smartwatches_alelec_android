package com.fossil;

import android.content.Context;
import com.fossil.N02;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yz1 extends N02 {
    @DexIgnore
    public Provider<Executor> b;
    @DexIgnore
    public Provider<Context> c;
    @DexIgnore
    public Provider d;
    @DexIgnore
    public Provider e;
    @DexIgnore
    public Provider f;
    @DexIgnore
    public Provider<J32> g;
    @DexIgnore
    public Provider<V12> h;
    @DexIgnore
    public Provider<H22> i;
    @DexIgnore
    public Provider<I12> j;
    @DexIgnore
    public Provider<B22> k;
    @DexIgnore
    public Provider<F22> l;
    @DexIgnore
    public Provider<M02> m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements N02.Ai {
        @DexIgnore
        public Context a;

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.N02.Ai
        public /* bridge */ /* synthetic */ N02.Ai a(Context context) {
            b(context);
            return this;
        }

        @DexIgnore
        public Bi b(Context context) {
            Lk7.b(context);
            this.a = context;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.N02.Ai
        public N02 build() {
            Lk7.a(this.a, Context.class);
            return new Yz1(this.a);
        }
    }

    @DexIgnore
    public Yz1(Context context) {
        f(context);
    }

    @DexIgnore
    public static N02.Ai c() {
        return new Bi();
    }

    @DexIgnore
    @Override // com.fossil.N02
    public K22 a() {
        return this.g.get();
    }

    @DexIgnore
    @Override // com.fossil.N02
    public M02 b() {
        return this.m.get();
    }

    @DexIgnore
    public final void f(Context context) {
        this.b = Ik7.a(E02.a());
        Factory a2 = Jk7.a(context);
        this.c = a2;
        Y02 a3 = Y02.a(a2, V32.a(), W32.a());
        this.d = a3;
        this.e = Ik7.a(A12.a(this.c, a3));
        this.f = Q32.a(this.c, N22.a(), O22.a());
        this.g = Ik7.a(K32.a(V32.a(), W32.a(), P22.a(), this.f));
        M12 b2 = M12.b(V32.a());
        this.h = b2;
        O12 a4 = O12.a(this.c, this.g, b2, W32.a());
        this.i = a4;
        Provider<Executor> provider = this.b;
        Provider provider2 = this.e;
        Provider<J32> provider3 = this.g;
        this.j = J12.a(provider, provider2, a4, provider3, provider3);
        Provider<Context> provider4 = this.c;
        Provider provider5 = this.e;
        Provider<J32> provider6 = this.g;
        this.k = C22.a(provider4, provider5, provider6, this.i, this.b, provider6, V32.a());
        Provider<Executor> provider7 = this.b;
        Provider<J32> provider8 = this.g;
        this.l = G22.a(provider7, provider8, this.i, provider8);
        this.m = Ik7.a(O02.a(V32.a(), W32.a(), this.j, this.k, this.l));
    }
}
