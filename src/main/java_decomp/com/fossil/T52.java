package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.M62;
import com.fossil.R62;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T52 extends M62.Ai<U42, GoogleSignInOptions> {
    @DexIgnore
    @Override // com.fossil.M62.Ei
    public final /* synthetic */ List a(Object obj) {
        GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions) obj;
        return googleSignInOptions == null ? Collections.emptyList() : googleSignInOptions.h();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.M62$Fi' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.content.Context, android.os.Looper, com.fossil.Ac2, java.lang.Object, com.fossil.R62$Bi, com.fossil.R62$Ci] */
    @Override // com.fossil.M62.Ai
    public final /* synthetic */ U42 c(Context context, Looper looper, Ac2 ac2, GoogleSignInOptions googleSignInOptions, R62.Bi bi, R62.Ci ci) {
        return new U42(context, looper, ac2, googleSignInOptions, bi, ci);
    }
}
