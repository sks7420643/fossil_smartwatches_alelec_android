package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.FrameLayout;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class Bs3 extends FrameLayout {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Di {
        @DexIgnore
        public Context a;

        @DexIgnore
        public Ai(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.Bs3.Di
        public final Drawable a(int i) {
            return this.a.getResources().getDrawable(17301508);
        }

        @DexIgnore
        @Override // com.fossil.Bs3.Di
        public final boolean isValid() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Di {
        @DexIgnore
        public Context a;

        @DexIgnore
        public Bi(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.Bs3.Di
        public final Drawable a(int i) {
            try {
                Resources resources = this.a.createPackageContext("com.google.android.gms", 4).getResources();
                return resources.getDrawable(resources.getIdentifier(i != 0 ? i != 1 ? i != 2 ? "ic_plusone_standard" : "ic_plusone_tall" : "ic_plusone_medium" : "ic_plusone_small", ResourceManager.DRAWABLE, "com.google.android.gms"));
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        }

        @DexIgnore
        @Override // com.fossil.Bs3.Di
        public final boolean isValid() {
            try {
                this.a.createPackageContext("com.google.android.gms", 4).getResources();
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Di {
        @DexIgnore
        public Context a;

        @DexIgnore
        public Ci(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.Bs3.Di
        public final Drawable a(int i) {
            return this.a.getResources().getDrawable(this.a.getResources().getIdentifier(i != 0 ? i != 1 ? i != 2 ? "ic_plusone_standard_off_client" : "ic_plusone_tall_off_client" : "ic_plusone_medium_off_client" : "ic_plusone_small_off_client", ResourceManager.DRAWABLE, this.a.getPackageName()));
        }

        @DexIgnore
        @Override // com.fossil.Bs3.Di
        public final boolean isValid() {
            return (this.a.getResources().getIdentifier("ic_plusone_small_off_client", ResourceManager.DRAWABLE, this.a.getPackageName()) == 0 || this.a.getResources().getIdentifier("ic_plusone_medium_off_client", ResourceManager.DRAWABLE, this.a.getPackageName()) == 0 || this.a.getResources().getIdentifier("ic_plusone_tall_off_client", ResourceManager.DRAWABLE, this.a.getPackageName()) == 0 || this.a.getResources().getIdentifier("ic_plusone_standard_off_client", ResourceManager.DRAWABLE, this.a.getPackageName()) == 0) ? false : true;
        }
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        Drawable a(int i);

        @DexIgnore
        boolean isValid();
    }

    @DexIgnore
    @Deprecated
    public Bs3(Context context, int i) {
        super(context);
        int i2;
        Button button = new Button(context);
        button.setEnabled(false);
        Di bi = new Bi(getContext());
        bi = !bi.isValid() ? new Ci(getContext()) : bi;
        button.setBackgroundDrawable((!bi.isValid() ? new Ai(getContext()) : bi).a(i));
        Point point = new Point();
        int i3 = 20;
        if (i == 0) {
            i3 = 14;
            i2 = 24;
        } else if (i == 1) {
            i2 = 32;
        } else if (i != 2) {
            i2 = 38;
            i3 = 24;
        } else {
            i2 = 50;
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float applyDimension = TypedValue.applyDimension(1, (float) i2, displayMetrics);
        float applyDimension2 = TypedValue.applyDimension(1, (float) i3, displayMetrics);
        point.x = (int) (((double) applyDimension) + 0.5d);
        point.y = (int) (((double) applyDimension2) + 0.5d);
        addView(button, new FrameLayout.LayoutParams(point.x, point.y, 17));
    }
}
