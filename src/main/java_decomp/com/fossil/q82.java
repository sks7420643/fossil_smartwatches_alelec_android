package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q82 extends Ls3 {
    @DexIgnore
    public /* final */ WeakReference<H82> b;

    @DexIgnore
    public Q82(H82 h82) {
        this.b = new WeakReference<>(h82);
    }

    @DexIgnore
    @Override // com.fossil.Ks3
    public final void Q2(Us3 us3) {
        H82 h82 = this.b.get();
        if (h82 != null) {
            h82.a.p(new P82(this, h82, h82, us3));
        }
    }
}
