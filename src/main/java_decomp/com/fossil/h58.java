package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H58 extends T58 {
    @DexIgnore
    public Q58 group;
    @DexIgnore
    public P58 option;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public H58(com.fossil.Q58 r3, com.fossil.P58 r4) {
        /*
            r2 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "The option '"
            r0.append(r1)
            java.lang.String r1 = r4.getKey()
            r0.append(r1)
            java.lang.String r1 = "' was specified but an option from this group "
            r0.append(r1)
            java.lang.String r1 = "has already been selected: '"
            r0.append(r1)
            java.lang.String r1 = r3.getSelected()
            r0.append(r1)
            java.lang.String r1 = "'"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.group = r3
            r2.option = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H58.<init>(com.fossil.Q58, com.fossil.P58):void");
    }

    @DexIgnore
    public H58(String str) {
        super(str);
    }

    @DexIgnore
    public P58 getOption() {
        return this.option;
    }

    @DexIgnore
    public Q58 getOptionGroup() {
        return this.group;
    }
}
