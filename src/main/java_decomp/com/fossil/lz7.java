package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lz7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(Lz7.class, Object.class, "_next");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater c; // = AtomicReferenceFieldUpdater.newUpdater(Lz7.class, Object.class, "_prev");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater d; // = AtomicReferenceFieldUpdater.newUpdater(Lz7.class, Object.class, "_removedRef");
    @DexIgnore
    public volatile Object _next; // = this;
    @DexIgnore
    public volatile Object _prev; // = this;
    @DexIgnore
    public volatile Object _removedRef; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai extends Az7 {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi extends Cz7<Lz7> {
        @DexIgnore
        public Lz7 b;
        @DexIgnore
        public /* final */ Lz7 c;

        @DexIgnore
        public Bi(Lz7 lz7) {
            this.c = lz7;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Cz7
        public /* bridge */ /* synthetic */ void d(Lz7 lz7, Object obj) {
            h(lz7, obj);
        }

        @DexIgnore
        public void h(Lz7 lz7, Object obj) {
            boolean z = obj == null;
            Lz7 lz72 = z ? this.c : this.b;
            if (lz72 != null && Lz7.b.compareAndSet(lz7, this, lz72) && z) {
                Lz7 lz73 = this.c;
                Lz7 lz74 = this.b;
                if (lz74 != null) {
                    lz73.k(lz74);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Rz7 {
        @DexIgnore
        public /* final */ Ai a;

        @DexIgnore
        public final void d() {
            throw null;
        }
    }

    @DexIgnore
    public final boolean g(Lz7 lz7, Lz7 lz72) {
        c.lazySet(lz7, this);
        b.lazySet(lz7, lz72);
        if (!b.compareAndSet(this, lz72, lz7)) {
            return false;
        }
        lz7.k(lz72);
        return true;
    }

    @DexIgnore
    public final boolean h(Lz7 lz7) {
        c.lazySet(lz7, this);
        b.lazySet(lz7, this);
        while (l() == this) {
            if (b.compareAndSet(this, this, lz7)) {
                lz7.k(this);
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
        if (com.fossil.Lz7.b.compareAndSet(r2, r1, ((com.fossil.Sz7) r1).a) == false) goto L_0x0001;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Lz7 i(com.fossil.Rz7 r7) {
        /*
            r6 = this;
            r3 = 0
        L_0x0001:
            java.lang.Object r0 = r6._prev
            com.fossil.Lz7 r0 = (com.fossil.Lz7) r0
            r1 = r0
        L_0x0006:
            r2 = r3
            r4 = r1
        L_0x0008:
            java.lang.Object r1 = r4._next
            if (r1 != r6) goto L_0x0018
            if (r0 != r4) goto L_0x000f
        L_0x000e:
            return r4
        L_0x000f:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = com.fossil.Lz7.c
            boolean r0 = r1.compareAndSet(r6, r0, r4)
            if (r0 != 0) goto L_0x000e
            goto L_0x0001
        L_0x0018:
            boolean r5 = r6.q()
            if (r5 == 0) goto L_0x0020
            r4 = r3
            goto L_0x000e
        L_0x0020:
            if (r1 == r7) goto L_0x000e
            boolean r5 = r1 instanceof com.fossil.Rz7
            if (r5 == 0) goto L_0x0039
            if (r7 == 0) goto L_0x0033
            r0 = r1
            com.fossil.Rz7 r0 = (com.fossil.Rz7) r0
            boolean r0 = r7.b(r0)
            if (r0 == 0) goto L_0x0033
            r4 = r3
            goto L_0x000e
        L_0x0033:
            com.fossil.Rz7 r1 = (com.fossil.Rz7) r1
            r1.c(r4)
            goto L_0x0001
        L_0x0039:
            boolean r5 = r1 instanceof com.fossil.Sz7
            if (r5 == 0) goto L_0x0053
            if (r2 == 0) goto L_0x004d
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r5 = com.fossil.Lz7.b
            com.fossil.Sz7 r1 = (com.fossil.Sz7) r1
            com.fossil.Lz7 r1 = r1.a
            boolean r1 = r5.compareAndSet(r2, r4, r1)
            if (r1 == 0) goto L_0x0001
            r1 = r2
            goto L_0x0006
        L_0x004d:
            java.lang.Object r1 = r4._prev
            com.fossil.Lz7 r1 = (com.fossil.Lz7) r1
            r4 = r1
            goto L_0x0008
        L_0x0053:
            if (r1 == 0) goto L_0x005a
            com.fossil.Lz7 r1 = (com.fossil.Lz7) r1
            r2 = r4
            r4 = r1
            goto L_0x0008
        L_0x005a:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
        */
        //  java.lang.String r1 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
        /*
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lz7.i(com.fossil.Rz7):com.fossil.Lz7");
    }

    @DexIgnore
    public final Lz7 j(Lz7 lz7) {
        while (lz7.q()) {
            lz7 = (Lz7) lz7._prev;
        }
        return lz7;
    }

    @DexIgnore
    public final void k(Lz7 lz7) {
        Lz7 lz72;
        do {
            lz72 = (Lz7) lz7._prev;
            if (l() != lz7) {
                return;
            }
        } while (!c.compareAndSet(lz7, lz72, this));
        if (q()) {
            lz7.i(null);
        }
    }

    @DexIgnore
    public final Object l() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof Rz7)) {
                return obj;
            }
            ((Rz7) obj).c(this);
        }
    }

    @DexIgnore
    public final Lz7 m() {
        return Kz7.b(l());
    }

    @DexIgnore
    public final Lz7 n() {
        Lz7 i = i(null);
        return i != null ? i : j((Lz7) this._prev);
    }

    @DexIgnore
    public final void o() {
        Object l = l();
        if (l != null) {
            ((Sz7) l).a.i(null);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
    }

    @DexIgnore
    public final void p() {
        while (true) {
            Object l = this.l();
            if (!(l instanceof Sz7)) {
                this.i(null);
                return;
            }
            this = ((Sz7) l).a;
        }
    }

    @DexIgnore
    public boolean q() {
        return l() instanceof Sz7;
    }

    @DexIgnore
    public boolean r() {
        return t() == null;
    }

    @DexIgnore
    public final Lz7 s() {
        while (true) {
            Object l = l();
            if (l != null) {
                Lz7 lz7 = (Lz7) l;
                if (lz7 == this) {
                    return null;
                }
                if (lz7.r()) {
                    return lz7;
                }
                lz7.o();
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final Lz7 t() {
        Object l;
        Lz7 lz7;
        do {
            l = l();
            if (l instanceof Sz7) {
                return ((Sz7) l).a;
            }
            if (l == this) {
                return (Lz7) l;
            }
            if (l != null) {
                lz7 = (Lz7) l;
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!b.compareAndSet(this, l, lz7.u()));
        lz7.i(null);
        return null;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(System.identityHashCode(this));
    }

    @DexIgnore
    public final Sz7 u() {
        Sz7 sz7 = (Sz7) this._removedRef;
        if (sz7 != null) {
            return sz7;
        }
        Sz7 sz72 = new Sz7(this);
        d.lazySet(this, sz72);
        return sz72;
    }

    @DexIgnore
    public final int v(Lz7 lz7, Lz7 lz72, Bi bi) {
        c.lazySet(lz7, this);
        b.lazySet(lz7, lz72);
        bi.b = lz72;
        if (!b.compareAndSet(this, lz72, bi)) {
            return 0;
        }
        return bi.c(this) == null ? 1 : 2;
    }
}
