package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.Lifecycle;
import com.fossil.X71;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T71 extends X71 {
    @DexIgnore
    public /* final */ Drawable A;
    @DexIgnore
    public /* final */ Drawable B;
    @DexIgnore
    public /* final */ Drawable C;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ J81 c;
    @DexIgnore
    public /* final */ Lifecycle d;
    @DexIgnore
    public /* final */ N81 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ List<String> g;
    @DexIgnore
    public /* final */ X71.Ai h;
    @DexIgnore
    public /* final */ G81 i;
    @DexIgnore
    public /* final */ E81 j;
    @DexIgnore
    public /* final */ D81 k;
    @DexIgnore
    public /* final */ U51 l;
    @DexIgnore
    public /* final */ Dv7 m;
    @DexIgnore
    public /* final */ List<M81> n;
    @DexIgnore
    public /* final */ Bitmap.Config o;
    @DexIgnore
    public /* final */ ColorSpace p;
    @DexIgnore
    public /* final */ P18 q;
    @DexIgnore
    public /* final */ W71 r;
    @DexIgnore
    public /* final */ S71 s;
    @DexIgnore
    public /* final */ S71 t;
    @DexIgnore
    public /* final */ S71 u;
    @DexIgnore
    public /* final */ boolean v;
    @DexIgnore
    public /* final */ boolean w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r16v0, resolved type: java.util.List<? extends com.fossil.M81> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public T71(Context context, Object obj, J81 j81, Lifecycle lifecycle, N81 n81, String str, List<String> list, X71.Ai ai, G81 g81, E81 e81, D81 d81, U51 u51, Dv7 dv7, List<? extends M81> list2, Bitmap.Config config, ColorSpace colorSpace, P18 p18, W71 w71, S71 s71, S71 s712, S71 s713, boolean z2, boolean z3, int i2, int i3, int i4, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        super(null);
        Wg6.c(context, "context");
        Wg6.c(list, "aliasKeys");
        Wg6.c(d81, "precision");
        Wg6.c(dv7, "dispatcher");
        Wg6.c(list2, "transformations");
        Wg6.c(config, "bitmapConfig");
        Wg6.c(p18, "headers");
        Wg6.c(w71, "parameters");
        Wg6.c(s71, "networkCachePolicy");
        Wg6.c(s712, "diskCachePolicy");
        Wg6.c(s713, "memoryCachePolicy");
        this.a = context;
        this.b = obj;
        this.c = j81;
        this.d = lifecycle;
        this.e = n81;
        this.f = str;
        this.g = list;
        this.h = ai;
        this.i = g81;
        this.j = e81;
        this.k = d81;
        this.l = u51;
        this.m = dv7;
        this.n = list2;
        this.o = config;
        this.p = colorSpace;
        this.q = p18;
        this.r = w71;
        this.s = s71;
        this.t = s712;
        this.u = s713;
        this.v = z2;
        this.w = z3;
        this.x = i2;
        this.y = i3;
        this.z = i4;
        this.A = drawable;
        this.B = drawable2;
        this.C = drawable3;
    }

    @DexIgnore
    public Lifecycle A() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public List<String> a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public boolean b() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public boolean c() {
        return this.w;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public Bitmap.Config d() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public ColorSpace e() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public U51 f() {
        return this.l;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public S71 g() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public Dv7 h() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public Drawable i() {
        return z(this.a, this.B, this.y);
    }

    @DexIgnore
    @Override // com.fossil.X71
    public Drawable j() {
        return z(this.a, this.C, this.z);
    }

    @DexIgnore
    @Override // com.fossil.X71
    public P18 k() {
        return this.q;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public String l() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public X71.Ai m() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public S71 n() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public S71 o() {
        return this.s;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public W71 p() {
        return this.r;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public Drawable q() {
        return z(this.a, this.A, this.x);
    }

    @DexIgnore
    @Override // com.fossil.X71
    public D81 r() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public E81 s() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public G81 t() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public J81 u() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public List<M81> v() {
        return this.n;
    }

    @DexIgnore
    @Override // com.fossil.X71
    public N81 w() {
        return this.e;
    }

    @DexIgnore
    public final Context x() {
        return this.a;
    }

    @DexIgnore
    public Object y() {
        return this.b;
    }

    @DexIgnore
    public final Drawable z(Context context, Drawable drawable, int i2) {
        if (drawable != null) {
            return drawable;
        }
        if (i2 != 0) {
            return T81.a(context, i2);
        }
        return null;
    }
}
