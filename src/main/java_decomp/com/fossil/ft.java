package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ft extends Bt {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ Rt L;
    @DexIgnore
    public /* final */ byte[] M;

    @DexIgnore
    public Ft(K5 k5, Rt rt, byte[] bArr) {
        super(k5, Ut.f, Hs.H, 0, 8);
        this.L = rt;
        this.M = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.q2, Dy1.e(this.K, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 17) {
            if (this.L.b == bArr[0]) {
                byte[] k = Dm7.k(bArr, 1, 17);
                this.K = k;
                G80.k(jSONObject, Jd0.q2, Dy1.e(k, null, 1, null));
                this.v = Mw.a(this.v, null, null, Lw.b, null, null, 27);
            } else {
                this.v = Mw.a(this.v, null, null, Lw.p, null, null, 27);
            }
        } else {
            this.v = Mw.a(this.v, null, null, Lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(this.M.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.L.b).put(this.M).array();
        Wg6.b(array, "ByteBuffer.allocate(1 + \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.u2, Ey1.a(this.L)), Jd0.o2, Dy1.e(this.M, null, 1, null));
    }
}
