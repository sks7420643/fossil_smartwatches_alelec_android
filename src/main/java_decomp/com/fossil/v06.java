package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V06 implements MembersInjector<NotificationCallsAndMessagesActivity> {
    @DexIgnore
    public static void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity, NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
        notificationCallsAndMessagesActivity.A = notificationCallsAndMessagesPresenter;
    }
}
