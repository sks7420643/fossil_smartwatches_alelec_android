package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ej7 extends AsyncTask<Uri, Void, List<BelvedereResult>> {
    @DexIgnore
    public /* final */ BelvedereCallback<List<BelvedereResult>> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Dj7 c;
    @DexIgnore
    public /* final */ Gj7 d;

    @DexIgnore
    public Ej7(Context context, Dj7 dj7, Gj7 gj7, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        this.b = context;
        this.c = dj7;
        this.d = gj7;
        this.a = belvedereCallback;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0096 A[SYNTHETIC, Splitter:B:29:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009b A[SYNTHETIC, Splitter:B:32:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e6 A[SYNTHETIC, Splitter:B:51:0x00e6] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00eb A[SYNTHETIC, Splitter:B:54:0x00eb] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0109 A[SYNTHETIC, Splitter:B:62:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x004e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x004e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.zendesk.belvedere.BelvedereResult> a(android.net.Uri... r14) {
        /*
        // Method dump skipped, instructions count: 338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ej7.a(android.net.Uri[]):java.util.List");
    }

    @DexIgnore
    public void b(List<BelvedereResult> list) {
        super.onPostExecute(list);
        BelvedereCallback<List<BelvedereResult>> belvedereCallback = this.a;
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(list);
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ List<BelvedereResult> doInBackground(Uri[] uriArr) {
        return a(uriArr);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(List<BelvedereResult> list) {
        b(list);
    }
}
