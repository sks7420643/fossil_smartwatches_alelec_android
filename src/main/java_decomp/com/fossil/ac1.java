package com.fossil;

import android.content.res.AssetManager;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ac1 extends Ub1<ParcelFileDescriptor> {
    @DexIgnore
    public Ac1(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Ub1
    public /* bridge */ /* synthetic */ void b(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        f(parcelFileDescriptor);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Ub1
    public /* bridge */ /* synthetic */ ParcelFileDescriptor e(AssetManager assetManager, String str) throws IOException {
        return g(assetManager, str);
    }

    @DexIgnore
    public void f(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        parcelFileDescriptor.close();
    }

    @DexIgnore
    public ParcelFileDescriptor g(AssetManager assetManager, String str) throws IOException {
        return assetManager.openFd(str).getParcelFileDescriptor();
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public Class<ParcelFileDescriptor> getDataClass() {
        return ParcelFileDescriptor.class;
    }
}
