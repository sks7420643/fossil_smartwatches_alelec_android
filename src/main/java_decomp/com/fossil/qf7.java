package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qf7 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public Bi e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public static Qf7 a(Bundle bundle) {
            Qf7 qf7 = new Qf7();
            qf7.a = bundle.getInt("_wxobject_sdkVer");
            qf7.b = bundle.getString("_wxobject_title");
            qf7.c = bundle.getString("_wxobject_description");
            qf7.d = bundle.getByteArray("_wxobject_thumbdata");
            qf7.f = bundle.getString("_wxobject_mediatagname");
            qf7.g = bundle.getString("_wxobject_message_action");
            qf7.h = bundle.getString("_wxobject_message_ext");
            String c = c(bundle.getString("_wxobject_identifier_"));
            if (c != null && c.length() > 0) {
                try {
                    Bi bi = (Bi) Class.forName(c).newInstance();
                    qf7.e = bi;
                    bi.b(bundle);
                    return qf7;
                } catch (Exception e) {
                    e.printStackTrace();
                    Ye7.b("MicroMsg.SDK.WXMediaMessage", "get media object from bundle failed: unknown ident " + c + ", ex = " + e.getMessage());
                }
            }
            return qf7;
        }

        @DexIgnore
        public static String b(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.modelmsg", "com.tencent.mm.sdk.openapi");
            }
            Ye7.b("MicroMsg.SDK.WXMediaMessage", "pathNewToOld fail, newPath is null");
            return str;
        }

        @DexIgnore
        public static String c(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.openapi", "com.tencent.mm.sdk.modelmsg");
            }
            Ye7.b("MicroMsg.SDK.WXMediaMessage", "pathOldToNew fail, oldPath is null");
            return str;
        }

        @DexIgnore
        public static Bundle d(Qf7 qf7) {
            Bundle bundle = new Bundle();
            bundle.putInt("_wxobject_sdkVer", qf7.a);
            bundle.putString("_wxobject_title", qf7.b);
            bundle.putString("_wxobject_description", qf7.c);
            bundle.putByteArray("_wxobject_thumbdata", qf7.d);
            Bi bi = qf7.e;
            if (bi != null) {
                bundle.putString("_wxobject_identifier_", b(bi.getClass().getName()));
                qf7.e.d(bundle);
            }
            bundle.putString("_wxobject_mediatagname", qf7.f);
            bundle.putString("_wxobject_message_action", qf7.g);
            bundle.putString("_wxobject_message_ext", qf7.h);
            return bundle;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        int a();

        @DexIgnore
        void b(Bundle bundle);

        @DexIgnore
        boolean c();

        @DexIgnore
        void d(Bundle bundle);
    }

    @DexIgnore
    public Qf7() {
        this(null);
    }

    @DexIgnore
    public Qf7(Bi bi) {
        this.e = bi;
    }

    @DexIgnore
    public final boolean a() {
        String str;
        byte[] bArr;
        if (b() == 8 && ((bArr = this.d) == null || bArr.length == 0)) {
            str = "checkArgs fail, thumbData should not be null when send emoji";
        } else {
            byte[] bArr2 = this.d;
            if (bArr2 == null || bArr2.length <= 32768) {
                String str2 = this.b;
                if (str2 == null || str2.length() <= 512) {
                    String str3 = this.c;
                    if (str3 != null && str3.length() > 1024) {
                        str = "checkArgs fail, description is invalid";
                    } else if (this.e == null) {
                        str = "checkArgs fail, mediaObject is null";
                    } else {
                        String str4 = this.f;
                        if (str4 == null || str4.length() <= 64) {
                            String str5 = this.g;
                            if (str5 == null || str5.length() <= 2048) {
                                String str6 = this.h;
                                if (str6 == null || str6.length() <= 2048) {
                                    return this.e.c();
                                }
                                str = "checkArgs fail, messageExt is too long";
                            } else {
                                str = "checkArgs fail, messageAction is too long";
                            }
                        } else {
                            str = "checkArgs fail, mediaTagName is too long";
                        }
                    }
                } else {
                    str = "checkArgs fail, title is invalid";
                }
            } else {
                str = "checkArgs fail, thumbData is invalid";
            }
        }
        Ye7.b("MicroMsg.SDK.WXMediaMessage", str);
        return false;
    }

    @DexIgnore
    public final int b() {
        Bi bi = this.e;
        if (bi == null) {
            return 0;
        }
        return bi.a();
    }
}
