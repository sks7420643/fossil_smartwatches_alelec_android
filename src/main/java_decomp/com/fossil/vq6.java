package com.fossil;

import com.fossil.Nu5;
import com.fossil.Tq4;
import com.mapped.Hx5;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vq6 extends Qq6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public /* final */ Rq6 e;
    @DexIgnore
    public /* final */ Nu5 f;
    @DexIgnore
    public /* final */ Uq4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Vq6.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Di<Nu5.Di, Nu5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ Vq6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(Vq6 vq6) {
            this.a = vq6;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Nu5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Nu5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Vq6.i.a();
            local.e(a2, "updateUserPassword onFailure: errorValue = " + ci);
            if (this.a.e.isActive()) {
                this.a.e.k();
                int a3 = ci.a();
                if (a3 == 400004) {
                    this.a.e.P2();
                } else if (a3 != 403005) {
                    this.a.e.I0(ci.a(), ci.b());
                } else {
                    this.a.e.f4();
                }
            }
        }

        @DexIgnore
        public void c(Nu5.Di di) {
            Wg6.c(di, "successResponse");
            if (this.a.e.isActive()) {
                this.a.e.k();
                this.a.e.l6();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(Nu5.Di di) {
            c(di);
        }
    }

    /*
    static {
        String simpleName = Vq6.class.getSimpleName();
        Wg6.b(simpleName, "ProfileChangePasswordPre\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public Vq6(Rq6 rq6, Nu5 nu5, Uq4 uq4) {
        Wg6.c(rq6, "mView");
        Wg6.c(nu5, "mChangePasswordUseCase");
        Wg6.c(uq4, "mUseCaseHandler");
        this.e = rq6;
        this.f = nu5;
        this.g = uq4;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(h, "presenter starts");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(h, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.Qq6
    public void n(String str, String str2) {
        Wg6.c(str, "oldPass");
        Wg6.c(str2, "newPass");
        if (!Hx5.b(PortfolioApp.get.instance())) {
            this.e.I0(601, null);
            return;
        }
        this.e.m();
        this.g.a(this.f, new Nu5.Bi(str, str2), new Bi(this));
    }

    @DexIgnore
    public void q() {
        this.e.M5(this);
    }
}
