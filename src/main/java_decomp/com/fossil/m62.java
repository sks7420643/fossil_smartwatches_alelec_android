package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.M62.Di;
import com.fossil.R62;
import com.fossil.Yb2;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M62<O extends Di> {
    @DexIgnore
    public /* final */ Ai<?, O> a;
    @DexIgnore
    public /* final */ Gi<?> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<T extends Fi, O> extends Ei<T, O> {
        @DexIgnore
        @Deprecated
        public abstract T c(Context context, Looper looper, Ac2 ac2, O o, R62.Bi bi, R62.Ci ci);
    }

    @DexIgnore
    public interface Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<C extends Bi> {
    }

    @DexIgnore
    public interface Di {

        @DexIgnore
        public interface Aii extends Cii, Di {
            @DexIgnore
            Account m();
        }

        @DexIgnore
        public interface Bii extends Cii {
            @DexIgnore
            GoogleSignInAccount b();
        }

        @DexIgnore
        public interface Cii extends Di {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii implements Di {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei<T extends Bi, O> {
        @DexIgnore
        public List<Scope> a(O o) {
            return Collections.emptyList();
        }

        @DexIgnore
        public int b() {
            return Integer.MAX_VALUE;
        }
    }

    @DexIgnore
    public interface Fi extends Bi {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        boolean c();

        @DexIgnore
        void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        @DexIgnore
        boolean g();

        @DexIgnore
        Set<Scope> h();

        @DexIgnore
        void i(Jc2 jc2, Set<Scope> set);

        @DexIgnore
        boolean j();

        @DexIgnore
        String k();

        @DexIgnore
        void l(Yb2.Ci ci);

        @DexIgnore
        void n(Yb2.Ei ei);

        @DexIgnore
        boolean r();

        @DexIgnore
        int s();

        @DexIgnore
        B62[] t();

        @DexIgnore
        Intent u();

        @DexIgnore
        boolean v();

        @DexIgnore
        IBinder w();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<C extends Fi> extends Ci<C> {
    }

    @DexIgnore
    public interface Hi<T extends IInterface> extends Bi {
        @DexIgnore
        void m(int i, T t);

        @DexIgnore
        String p();

        @DexIgnore
        T q(IBinder iBinder);

        @DexIgnore
        String x();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.M62$Ai<C extends com.fossil.M62$Fi, O extends com.fossil.M62$Di> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.M62$Gi<C extends com.fossil.M62$Fi> */
    /* JADX WARN: Multi-variable type inference failed */
    public <C extends Fi> M62(String str, Ai<C, O> ai, Gi<C> gi) {
        Rc2.l(ai, "Cannot construct an Api with a null ClientBuilder");
        Rc2.l(gi, "Cannot construct an Api with a null ClientKey");
        this.c = str;
        this.a = ai;
        this.b = gi;
    }

    @DexIgnore
    public final Ci<?> a() {
        Gi<?> gi = this.b;
        if (gi != null) {
            return gi;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final Ei<?, O> c() {
        return this.a;
    }

    @DexIgnore
    public final Ai<?, O> d() {
        Rc2.o(this.a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.a;
    }
}
