package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wz2 extends Yz2 {
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public /* final */ int c; // = this.d.zza();
    @DexIgnore
    public /* final */ /* synthetic */ Xz2 d;

    @DexIgnore
    public Wz2(Xz2 xz2) {
        this.d = xz2;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.b < this.c;
    }

    @DexIgnore
    @Override // com.fossil.B03
    public final byte zza() {
        int i = this.b;
        if (i < this.c) {
            this.b = i + 1;
            return this.d.zzb(i);
        }
        throw new NoSuchElementException();
    }
}
