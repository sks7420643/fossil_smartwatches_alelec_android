package com.fossil;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E43 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(E43.class.getName());
    @DexIgnore
    public static /* final */ Unsafe b; // = t();
    @DexIgnore
    public static /* final */ Class<?> c; // = Qz2.c();
    @DexIgnore
    public static /* final */ boolean d; // = B(Long.TYPE);
    @DexIgnore
    public static /* final */ boolean e; // = B(Integer.TYPE);
    @DexIgnore
    public static /* final */ Ci f;
    @DexIgnore
    public static /* final */ boolean g; // = E();
    @DexIgnore
    public static /* final */ boolean h; // = A();
    @DexIgnore
    public static /* final */ long i; // = ((long) n(byte[].class));
    @DexIgnore
    public static /* final */ boolean j; // = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ci {
        @DexIgnore
        public Ai(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final byte a(Object obj, long j) {
            return E43.j ? E43.L(obj, j) : E43.M(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void b(Object obj, long j, byte b) {
            if (E43.j) {
                E43.u(obj, j, b);
            } else {
                E43.y(obj, j, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void c(Object obj, long j, double d) {
            f(obj, j, Double.doubleToLongBits(d));
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void d(Object obj, long j, float f) {
            e(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void g(Object obj, long j, boolean z) {
            if (E43.j) {
                E43.z(obj, j, z);
            } else {
                E43.D(obj, j, z);
            }
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final boolean h(Object obj, long j) {
            return E43.j ? E43.N(obj, j) : E43.O(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final float i(Object obj, long j) {
            return Float.intBitsToFloat(k(obj, j));
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final double j(Object obj, long j) {
            return Double.longBitsToDouble(l(obj, j));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ci {
        @DexIgnore
        public Bi(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final byte a(Object obj, long j) {
            return E43.j ? E43.L(obj, j) : E43.M(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void b(Object obj, long j, byte b) {
            if (E43.j) {
                E43.u(obj, j, b);
            } else {
                E43.y(obj, j, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void c(Object obj, long j, double d) {
            f(obj, j, Double.doubleToLongBits(d));
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void d(Object obj, long j, float f) {
            e(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void g(Object obj, long j, boolean z) {
            if (E43.j) {
                E43.z(obj, j, z);
            } else {
                E43.D(obj, j, z);
            }
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final boolean h(Object obj, long j) {
            return E43.j ? E43.N(obj, j) : E43.O(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final float i(Object obj, long j) {
            return Float.intBitsToFloat(k(obj, j));
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final double j(Object obj, long j) {
            return Double.longBitsToDouble(l(obj, j));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci {
        @DexIgnore
        public Unsafe a;

        @DexIgnore
        public Ci(Unsafe unsafe) {
            this.a = unsafe;
        }

        @DexIgnore
        public abstract byte a(Object obj, long j);

        @DexIgnore
        public abstract void b(Object obj, long j, byte b);

        @DexIgnore
        public abstract void c(Object obj, long j, double d);

        @DexIgnore
        public abstract void d(Object obj, long j, float f);

        @DexIgnore
        public final void e(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        @DexIgnore
        public final void f(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }

        @DexIgnore
        public abstract void g(Object obj, long j, boolean z);

        @DexIgnore
        public abstract boolean h(Object obj, long j);

        @DexIgnore
        public abstract float i(Object obj, long j);

        @DexIgnore
        public abstract double j(Object obj, long j);

        @DexIgnore
        public final int k(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        @DexIgnore
        public final long l(Object obj, long j) {
            return this.a.getLong(obj, j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Ci {
        @DexIgnore
        public Di(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final byte a(Object obj, long j) {
            return this.a.getByte(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void b(Object obj, long j, byte b) {
            this.a.putByte(obj, j, b);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void c(Object obj, long j, double d) {
            this.a.putDouble(obj, j, d);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void d(Object obj, long j, float f) {
            this.a.putFloat(obj, j, f);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final void g(Object obj, long j, boolean z) {
            this.a.putBoolean(obj, j, z);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final boolean h(Object obj, long j) {
            return this.a.getBoolean(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final float i(Object obj, long j) {
            return this.a.getFloat(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.E43.Ci
        public final double j(Object obj, long j) {
            return this.a.getDouble(obj, j);
        }
    }

    /*
    static {
        Ci ci;
        Ci ci2 = null;
        if (b != null) {
            if (!Qz2.b()) {
                ci2 = new Di(b);
            } else if (d) {
                ci2 = new Ai(b);
            } else if (e) {
                ci2 = new Bi(b);
            }
        }
        f = ci2;
        n(boolean[].class);
        s(boolean[].class);
        n(int[].class);
        s(int[].class);
        n(long[].class);
        s(long[].class);
        n(float[].class);
        s(float[].class);
        n(double[].class);
        s(double[].class);
        n(Object[].class);
        s(Object[].class);
        Field G = G();
        if (!(G == null || (ci = f) == null)) {
            ci.a.objectFieldOffset(G);
        }
    }
    */

    @DexIgnore
    public static boolean A() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (Qz2.b()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static boolean B(Class<?> cls) {
        if (!Qz2.b()) {
            return false;
        }
        try {
            Class<?> cls2 = c;
            cls2.getMethod("peekLong", cls, Boolean.TYPE);
            cls2.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls2.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls2.getMethod("peekInt", cls, Boolean.TYPE);
            cls2.getMethod("pokeByte", cls, Byte.TYPE);
            cls2.getMethod("peekByte", cls);
            cls2.getMethod("pokeByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            cls2.getMethod("peekByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    @DexIgnore
    public static double C(Object obj, long j2) {
        return f.j(obj, j2);
    }

    @DexIgnore
    public static void D(Object obj, long j2, boolean z) {
        y(obj, j2, z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static boolean E() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (G() == null) {
                return false;
            }
            if (Qz2.b()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static Object F(Object obj, long j2) {
        return f.a.getObject(obj, j2);
    }

    @DexIgnore
    public static Field G() {
        Field d2;
        if (Qz2.b() && (d2 = d(Buffer.class, "effectiveDirectAddress")) != null) {
            return d2;
        }
        Field d3 = d(Buffer.class, "address");
        if (d3 == null || d3.getType() != Long.TYPE) {
            return null;
        }
        return d3;
    }

    @DexIgnore
    public static byte L(Object obj, long j2) {
        return (byte) (b(obj, -4 & j2) >>> ((int) ((3 & j2) << 3)));
    }

    @DexIgnore
    public static byte M(Object obj, long j2) {
        return (byte) (b(obj, -4 & j2) >>> ((int) ((3 & j2) << 3)));
    }

    @DexIgnore
    public static boolean N(Object obj, long j2) {
        return L(obj, j2) != 0;
    }

    @DexIgnore
    public static boolean O(Object obj, long j2) {
        return M(obj, j2) != 0;
    }

    @DexIgnore
    public static byte a(byte[] bArr, long j2) {
        return f.a(bArr, i + j2);
    }

    @DexIgnore
    public static int b(Object obj, long j2) {
        return f.k(obj, j2);
    }

    @DexIgnore
    public static <T> T c(Class<T> cls) {
        try {
            return (T) b.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public static Field d(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static void f(Object obj, long j2, double d2) {
        f.c(obj, j2, d2);
    }

    @DexIgnore
    public static void g(Object obj, long j2, float f2) {
        f.d(obj, j2, f2);
    }

    @DexIgnore
    public static void h(Object obj, long j2, int i2) {
        f.e(obj, j2, i2);
    }

    @DexIgnore
    public static void i(Object obj, long j2, long j3) {
        f.f(obj, j2, j3);
    }

    @DexIgnore
    public static void j(Object obj, long j2, Object obj2) {
        f.a.putObject(obj, j2, obj2);
    }

    @DexIgnore
    public static void k(Object obj, long j2, boolean z) {
        f.g(obj, j2, z);
    }

    @DexIgnore
    public static void l(byte[] bArr, long j2, byte b2) {
        f.b(bArr, i + j2, b2);
    }

    @DexIgnore
    public static boolean m() {
        return h;
    }

    @DexIgnore
    public static int n(Class<?> cls) {
        if (h) {
            return f.a.arrayBaseOffset(cls);
        }
        return -1;
    }

    @DexIgnore
    public static long o(Object obj, long j2) {
        return f.l(obj, j2);
    }

    @DexIgnore
    public static boolean r() {
        return g;
    }

    @DexIgnore
    public static int s(Class<?> cls) {
        if (h) {
            return f.a.arrayIndexScale(cls);
        }
        return -1;
    }

    @DexIgnore
    public static Unsafe t() {
        try {
            return (Unsafe) AccessController.doPrivileged(new C43());
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static void u(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = (((int) j2) & 3) << 3;
        h(obj, j3, (b(obj, j3) & (255 << i2)) | ((b2 & 255) << i2));
    }

    @DexIgnore
    public static boolean w(Object obj, long j2) {
        return f.h(obj, j2);
    }

    @DexIgnore
    public static float x(Object obj, long j2) {
        return f.i(obj, j2);
    }

    @DexIgnore
    public static void y(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = (((int) j2) & 3) << 3;
        h(obj, j3, (b(obj, j3) & (255 << i2)) | ((b2 & 255) << i2));
    }

    @DexIgnore
    public static void z(Object obj, long j2, boolean z) {
        u(obj, j2, z ? (byte) 1 : 0);
    }
}
