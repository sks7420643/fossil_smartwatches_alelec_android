package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hg5 extends Gg5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131362727, 1);
        D.put(2131362487, 2);
        D.put(2131363161, 3);
        D.put(2131362473, 4);
        D.put(2131362553, 5);
        D.put(2131361997, 6);
        D.put(2131363456, 7);
        D.put(2131362575, 8);
        D.put(2131362576, 9);
        D.put(2131362577, 10);
    }
    */

    @DexIgnore
    public Hg5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 11, C, D));
    }

    @DexIgnore
    public Hg5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleCheckBox) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5], (Guideline) objArr[8], (Guideline) objArr[9], (Guideline) objArr[10], (RTLImageView) objArr[1], (FlexibleSwitchCompat) objArr[3], (View) objArr[7]);
        this.B = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.A = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.B != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.B = 1;
        }
        w();
    }
}
