package com.fossil;

import com.mapped.InAppNotificationManager;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lp4 implements Factory<InAppNotificationManager> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<InAppNotificationRepository> b;

    @DexIgnore
    public Lp4(Uo4 uo4, Provider<InAppNotificationRepository> provider) {
        this.a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static Lp4 a(Uo4 uo4, Provider<InAppNotificationRepository> provider) {
        return new Lp4(uo4, provider);
    }

    @DexIgnore
    public static InAppNotificationManager c(Uo4 uo4, InAppNotificationRepository inAppNotificationRepository) {
        InAppNotificationManager s = uo4.s(inAppNotificationRepository);
        Lk7.c(s, "Cannot return null from a non-@Nullable @Provides method");
        return s;
    }

    @DexIgnore
    public InAppNotificationManager b() {
        return c(this.a, this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
