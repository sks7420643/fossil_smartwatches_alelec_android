package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum N13 {
    zza(Void.class, Void.class, null),
    zzb(Integer.TYPE, Integer.class, 0),
    zzc(Long.TYPE, Long.class, 0L),
    zzd(Float.TYPE, Float.class, Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)),
    zze(Double.TYPE, Double.class, Double.valueOf(0.0d)),
    zzf(Boolean.TYPE, Boolean.class, Boolean.FALSE),
    zzg(String.class, String.class, ""),
    zzh(Xz2.class, Xz2.class, Xz2.zza),
    zzi(Integer.TYPE, Integer.class, null),
    zzj(Object.class, Object.class, null);
    
    @DexIgnore
    public /* final */ Class<?> zzk;
    @DexIgnore
    public /* final */ Class<?> zzl;
    @DexIgnore
    public /* final */ Object zzm;

    @DexIgnore
    public N13(Class cls, Class cls2, Object obj) {
        this.zzk = cls;
        this.zzl = cls2;
        this.zzm = obj;
    }

    @DexIgnore
    public final Class<?> zza() {
        return this.zzl;
    }
}
