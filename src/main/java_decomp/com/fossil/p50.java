package com.fossil;

import com.mapped.Q40;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P50 implements D5 {
    @DexIgnore
    public /* final */ /* synthetic */ E60 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public P50(E60 e60) {
        this.a = e60;
    }

    @DexIgnore
    @Override // com.fossil.D5
    public void a(K5 k5, B5 b5, B5 b52) {
        if (Wg6.a(k5, this.a.d)) {
            this.a.r0(Q40.Di.g.a(b5), Q40.Di.g.a(b52));
        }
    }

    @DexIgnore
    @Override // com.fossil.D5
    public void b(K5 k5, F5 f5) {
        Lp lp;
        if (L10.a[f5.ordinal()] == 1) {
            this.a.v0(false);
        }
        if (E60.w0(this.a)) {
            this.a.E0();
        }
        Lp[] f = this.a.g.f();
        int length = f.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                lp = null;
                break;
            }
            lp = f[i];
            Yp yp = lp.y;
            if (yp == Yp.h || yp == Yp.c || yp == Yp.i) {
                break;
            }
            i++;
        }
        if (lp == null) {
            int i2 = L10.b[f5.ordinal()];
            if (i2 == 1) {
                this.a.q0(Q40.Ci.DISCONNECTED);
            } else if (i2 == 2) {
                this.a.q0(Q40.Ci.CONNECTING);
            } else if (i2 == 4) {
                this.a.q0(Q40.Ci.DISCONNECTING);
            }
        }
    }
}
