package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ D5 b;
    @DexIgnore
    public /* final */ /* synthetic */ K5 c;
    @DexIgnore
    public /* final */ /* synthetic */ F5 d;

    @DexIgnore
    public W4(D5 d5, K5 k5, F5 f5) {
        this.b = d5;
        this.c = k5;
        this.d = f5;
    }

    @DexIgnore
    public final void run() {
        this.b.b(this.c, this.d);
    }
}
