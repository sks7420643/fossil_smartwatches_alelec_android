package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Di4 implements Parcelable.Creator<Ci4> {
    @DexIgnore
    public static void c(Ci4 ci4, Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.e(parcel, 2, ci4.b, false);
        Bd2.b(parcel, a2);
    }

    @DexIgnore
    public Ci4 a(Parcel parcel) {
        int C = Ad2.C(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 2) {
                Ad2.B(parcel, t);
            } else {
                bundle = Ad2.a(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ci4(bundle);
    }

    @DexIgnore
    public Ci4[] b(int i) {
        return new Ci4[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public /* bridge */ /* synthetic */ Ci4 createFromParcel(Parcel parcel) {
        return a(parcel);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public /* bridge */ /* synthetic */ Ci4[] newArray(int i) {
        return b(i);
    }
}
