package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xb extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Wb CREATOR; // = new Wb(null);
    @DexIgnore
    public /* final */ double b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public Xb(double d, int i) {
        this.b = d;
        this.c = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Xb.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Xb xb = (Xb) obj;
            if (this.b != xb.b) {
                return false;
            }
            return this.c == xb.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutGPSDistance");
    }

    @DexIgnore
    public int hashCode() {
        return (Double.valueOf(this.b).hashCode() * 31) + Integer.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.V1, Double.valueOf(this.b)), Jd0.L5, Integer.valueOf(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
