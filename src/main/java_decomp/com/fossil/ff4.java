package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ff4 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Ff4 {
        @DexIgnore
        public /* final */ IBinder b;

        @DexIgnore
        public Ai(IBinder iBinder) {
            this.b = iBinder;
        }

        @DexIgnore
        @Override // com.fossil.Ff4
        public void P(Message message) throws RemoteException {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
            obtain.writeInt(1);
            message.writeToParcel(obtain, 0);
            try {
                this.b.transact(1, obtain, null, 1);
            } finally {
                obtain.recycle();
            }
        }

        @DexIgnore
        public IBinder asBinder() {
            return this.b;
        }
    }

    @DexIgnore
    void P(Message message) throws RemoteException;
}
