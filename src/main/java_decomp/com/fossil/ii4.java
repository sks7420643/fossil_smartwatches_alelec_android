package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ii4 {
    @DexIgnore
    public static WeakReference<Ii4> d;
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public Gi4 b;
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public Ii4(SharedPreferences sharedPreferences, Executor executor) {
        this.c = executor;
        this.a = sharedPreferences;
    }

    @DexIgnore
    public static Ii4 a(Context context, Executor executor) {
        Ii4 ii4;
        synchronized (Ii4.class) {
            ii4 = null;
            try {
                if (d != null) {
                    ii4 = d.get();
                }
                if (ii4 == null) {
                    ii4 = new Ii4(context.getSharedPreferences("com.google.android.gms.appid", 0), executor);
                    ii4.c();
                    d = new WeakReference<>(ii4);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return ii4;
    }

    @DexIgnore
    public final Hi4 b() {
        Hi4 a2;
        synchronized (this) {
            a2 = Hi4.a(this.b.e());
        }
        return a2;
    }

    @DexIgnore
    public final void c() {
        synchronized (this) {
            this.b = Gi4.c(this.a, "topic_operation_queue", ",", this.c);
        }
    }

    @DexIgnore
    public final boolean d(Hi4 hi4) {
        boolean f;
        synchronized (this) {
            f = this.b.f(hi4.e());
        }
        return f;
    }
}
