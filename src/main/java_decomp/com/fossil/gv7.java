package com.fossil;

import com.fossil.Vx7;
import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Ve6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gv7 extends Ve6 implements Vx7<String> {
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Af6.Ci<Gv7> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public Gv7(long j) {
        super(c);
        this.b = j;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Af6, java.lang.Object] */
    @Override // com.fossil.Vx7
    public /* bridge */ /* synthetic */ void B(Af6 af6, String str) {
        P(af6, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Vx7
    public /* bridge */ /* synthetic */ String F(Af6 af6) {
        return Q(af6);
    }

    @DexIgnore
    public final long M() {
        return this.b;
    }

    @DexIgnore
    public void P(Af6 af6, String str) {
        Thread.currentThread().setName(str);
    }

    @DexIgnore
    public String Q(Af6 af6) {
        String M;
        Hv7 hv7 = (Hv7) af6.get(Hv7.c);
        String str = (hv7 == null || (M = hv7.M()) == null) ? "coroutine" : M;
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        int L = Wt7.L(name, " @", 0, false, 6, null);
        if (L < 0) {
            L = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + L + 10);
        if (name != null) {
            String substring = name.substring(0, L);
            Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(" @");
            sb.append(str);
            sb.append('#');
            sb.append(this.b);
            String sb2 = sb.toString();
            Wg6.b(sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
            currentThread.setName(sb2);
            return name;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof Gv7) && this.b == ((Gv7) obj).b);
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Ve6
    public <R> R fold(R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
        return (R) Vx7.Ai.a(this, r, coroutine);
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Ve6, com.mapped.Af6.Bi
    public <E extends Af6.Bi> E get(Af6.Ci<E> ci) {
        return (E) Vx7.Ai.b(this, ci);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Ve6
    public Af6 minusKey(Af6.Ci<?> ci) {
        return Vx7.Ai.c(this, ci);
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Ve6
    public Af6 plus(Af6 af6) {
        return Vx7.Ai.d(this, af6);
    }

    @DexIgnore
    public String toString() {
        return "CoroutineId(" + this.b + ')';
    }
}
