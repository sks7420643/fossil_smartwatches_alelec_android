package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import com.fossil.Kl0;
import com.fossil.Nl0;
import com.fossil.Zm0;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"NewApi"})
public class Sl0 {
    @DexIgnore
    public static /* final */ Yl0 a;
    @DexIgnore
    public static /* final */ Ej0<String, Typeface> b; // = new Ej0<>(16);

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            a = new Xl0();
        } else if (i >= 28) {
            a = new Wl0();
        } else if (i >= 26) {
            a = new Vl0();
        } else if (i >= 24 && Ul0.m()) {
            a = new Ul0();
        } else if (Build.VERSION.SDK_INT >= 21) {
            a = new Tl0();
        } else {
            a = new Yl0();
        }
    }
    */

    @DexIgnore
    public static Typeface a(Context context, Typeface typeface, int i) {
        Typeface g;
        if (context != null) {
            return (Build.VERSION.SDK_INT >= 21 || (g = g(context, typeface, i)) == null) ? Typeface.create(typeface, i) : g;
        }
        throw new IllegalArgumentException("Context cannot be null");
    }

    @DexIgnore
    public static Typeface b(Context context, CancellationSignal cancellationSignal, Zm0.Fi[] fiArr, int i) {
        return a.c(context, cancellationSignal, fiArr, i);
    }

    @DexIgnore
    public static Typeface c(Context context, Kl0.Ai ai, Resources resources, int i, int i2, Nl0.Ai ai2, Handler handler, boolean z) {
        Typeface b2;
        if (ai instanceof Kl0.Di) {
            Kl0.Di di = (Kl0.Di) ai;
            boolean z2 = false;
            if (!z ? ai2 == null : di.a() == 0) {
                z2 = true;
            }
            b2 = Zm0.g(context, di.b(), ai2, handler, z2, z ? di.c() : -1, i2);
        } else {
            b2 = a.b(context, (Kl0.Bi) ai, resources, i2);
            if (ai2 != null) {
                if (b2 != null) {
                    ai2.b(b2, handler);
                } else {
                    ai2.a(-3, handler);
                }
            }
        }
        if (b2 != null) {
            b.f(e(resources, i, i2), b2);
        }
        return b2;
    }

    @DexIgnore
    public static Typeface d(Context context, Resources resources, int i, String str, int i2) {
        Typeface e = a.e(context, resources, i, str, i2);
        if (e != null) {
            b.f(e(resources, i, i2), e);
        }
        return e;
    }

    @DexIgnore
    public static String e(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
    }

    @DexIgnore
    public static Typeface f(Resources resources, int i, int i2) {
        return b.d(e(resources, i, i2));
    }

    @DexIgnore
    public static Typeface g(Context context, Typeface typeface, int i) {
        Kl0.Bi i2 = a.i(typeface);
        if (i2 == null) {
            return null;
        }
        return a.b(context, i2, context.getResources(), i);
    }
}
