package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Fw5;
import com.mapped.AlertDialogFragment;
import com.mapped.PermissionUtils;
import com.mapped.Rc6;
import com.mapped.WatchAppsAdapter;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X96 extends BaseFragment implements W96, AlertDialogFragment.Gi {
    @DexIgnore
    public G37<Cc5> g;
    @DexIgnore
    public V96 h;
    @DexIgnore
    public Fw5 i;
    @DexIgnore
    public WatchAppsAdapter j;
    @DexIgnore
    public Po4 k;
    @DexIgnore
    public WatchAppEditViewModel l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements WatchAppsAdapter.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ X96 a;

        @DexIgnore
        public Ai(X96 x96) {
            this.a = x96;
        }

        @DexIgnore
        @Override // com.mapped.WatchAppsAdapter.Ai
        public void a(WatchApp watchApp) {
            Wg6.c(watchApp, "watchApp");
            X96.K6(this.a).r(watchApp.getWatchappId());
        }

        @DexIgnore
        @Override // com.mapped.WatchAppsAdapter.Ai
        public void b(WatchApp watchApp) {
            Wg6.c(watchApp, "watchApp");
            Jn5.c(Jn5.b, this.a.getContext(), Hl5.a.e(watchApp.getWatchappId()), false, false, false, null, 60, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Fw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ X96 a;

        @DexIgnore
        public Bi(X96 x96) {
            this.a = x96;
        }

        @DexIgnore
        @Override // com.fossil.Fw5.Bi
        public void a(Category category) {
            Wg6.c(category, "category");
            X96.K6(this.a).q(category);
        }

        @DexIgnore
        @Override // com.fossil.Fw5.Bi
        public void b() {
            X96.K6(this.a).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ X96 b;

        @DexIgnore
        public Ci(X96 x96) {
            this.b = x96;
        }

        @DexIgnore
        public final void onClick(View view) {
            X96.K6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ X96 b;

        @DexIgnore
        public Di(X96 x96) {
            this.b = x96;
        }

        @DexIgnore
        public final void onClick(View view) {
            X96.K6(this.b).p();
        }
    }

    @DexIgnore
    public static final /* synthetic */ V96 K6(X96 x96) {
        V96 v96 = x96.h;
        if (v96 != null) {
            return v96;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchAppsFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final void L6() {
        if (isActive()) {
            WatchAppsAdapter watchAppsAdapter = this.j;
            if (watchAppsAdapter != null) {
                watchAppsAdapter.j();
            } else {
                Wg6.n("mWatchAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(V96 v96) {
        M6(v96);
    }

    @DexIgnore
    public void M6(V96 v96) {
        Wg6.c(v96, "presenter");
        this.h = v96;
    }

    @DexIgnore
    public final void N6(WatchApp watchApp) {
        G37<Cc5> g37 = this.g;
        if (g37 != null) {
            Cc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                Wg6.b(flexibleTextView, "binding.tvSelectedWatchapps");
                flexibleTextView.setText(Um5.d(PortfolioApp.get.instance(), watchApp.getNameKey(), watchApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.w;
                Wg6.b(flexibleTextView2, "binding.tvWatchappsDetail");
                flexibleTextView2.setText(Um5.d(PortfolioApp.get.instance(), watchApp.getDescriptionKey(), watchApp.getDescription()));
                WatchAppsAdapter watchAppsAdapter = this.j;
                if (watchAppsAdapter != null) {
                    int k2 = watchAppsAdapter.k(watchApp.getWatchappId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "updateDetailComplication watchAppId=" + watchApp.getWatchappId() + " scrollTo " + k2);
                    if (k2 >= 0) {
                        a2.t.scrollToPosition(k2);
                        return;
                    }
                    return;
                }
                Wg6.n("mWatchAppAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        Wg6.c(str, "tag");
        if (Wg6.a(str, "REQUEST_NOTIFICATION_ACCESS")) {
            if (i2 == 2131363291) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        } else if (Wg6.a(str, S37.c.f())) {
            if (i2 == 2131363373 && (activity3 = getActivity()) != null) {
                if (!V78.d(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    PermissionUtils.a.t(this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity3 != null) {
                    ((BaseActivity) activity3).x();
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (Wg6.a(str, "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363373 && (activity2 = getActivity()) != null) {
                if (activity2 != null) {
                    ((BaseActivity) activity2).x();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (Wg6.a(str, "LOCATION_PERMISSION_TAG")) {
            if (i2 == 2131363373 && (activity = getActivity()) != null) {
                if (activity != null) {
                    ((BaseActivity) activity).x();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (Wg6.a(str, InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363373) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void S(List<Category> list) {
        Wg6.c(list, "categories");
        Fw5 fw5 = this.i;
        if (fw5 != null) {
            fw5.n(list);
        } else {
            Wg6.n("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void S5(List<WatchApp> list) {
        Wg6.c(list, "watchApps");
        WatchAppsAdapter watchAppsAdapter = this.j;
        if (watchAppsAdapter != null) {
            watchAppsAdapter.o(list);
        } else {
            Wg6.n("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void U4(String str) {
        Wg6.c(str, "content");
        G37<Cc5> g37 = this.g;
        if (g37 != null) {
            Cc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "it.tvWatchappsDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void a0(String str, String str2, String str3) {
        Wg6.c(str, "topWatchApp");
        Wg6.c(str2, "middleWatchApp");
        Wg6.c(str3, "bottomWatchApp");
        WatchAppSearchActivity.B.a(this, str, str2, str3);
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void c1(boolean z) {
        if (isActive()) {
            G37<Cc5> g37 = this.g;
            if (g37 != null) {
                Cc5 a2 = g37.a();
                if (a2 != null) {
                    a2.v.setCompoundDrawablesWithIntrinsicBounds(0, 0, z ? 2131231087 : 0, 0);
                    Drawable[] compoundDrawables = a2.v.getCompoundDrawables();
                    Wg6.b(compoundDrawables, "it.tvSelectedWatchapps.getCompoundDrawables()");
                    if (compoundDrawables[2] != null) {
                        Drawable drawable = compoundDrawables[2];
                        if (drawable != null) {
                            drawable.setColorFilter(getResources().getColor(2131099968), PorterDuff.Mode.MULTIPLY);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void d6(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        WeatherSettingActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void f0(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        CommuteTimeWatchAppSettingsActivity.A.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void g0(boolean z, String str, String str2, String str3) {
        Wg6.c(str, "watchAppId");
        Wg6.c(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "updateSetting of watchAppId " + str + " requestContent " + str2 + " setting " + str3);
        G37<Cc5> g37 = this.g;
        if (g37 != null) {
            Cc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.x;
                Wg6.b(flexibleTextView2, "it.tvWatchappsPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.y;
                    Wg6.b(flexibleTextView3, "it.tvWatchappsSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.y;
                        Wg6.b(flexibleTextView4, "it.tvWatchappsSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.y;
                    Wg6.b(flexibleTextView5, "it.tvWatchappsSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.y;
                Wg6.b(flexibleTextView6, "it.tvWatchappsSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void i0(String str) {
        Wg6.c(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.A;
            Wg6.b(activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void o5(WatchApp watchApp) {
        if (watchApp != null) {
            WatchAppsAdapter watchAppsAdapter = this.j;
            if (watchAppsAdapter != null) {
                watchAppsAdapter.q(watchApp.getWatchappId());
                N6(watchApp);
                return;
            }
            Wg6.n("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchAppEditActivity watchAppEditActivity = (WatchAppEditActivity) activity;
            Po4 po4 = this.k;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(watchAppEditActivity, po4).a(WatchAppEditViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026ditViewModel::class.java)");
                WatchAppEditViewModel watchAppEditViewModel = (WatchAppEditViewModel) a2;
                this.l = watchAppEditViewModel;
                V96 v96 = this.h;
                if (v96 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (watchAppEditViewModel != null) {
                    v96.s(watchAppEditViewModel);
                    V96 v962 = this.h;
                    if (v962 != null) {
                        v962.l();
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else {
                    Wg6.n("mShareViewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 101) {
            if (i2 != 105) {
                if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) intent.getParcelableExtra("COMMUTE_TIME_WATCH_APP_SETTING")) != null) {
                    V96 v96 = this.h;
                    if (v96 != null) {
                        v96.t("commute-time", commuteTimeWatchAppSetting);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
            } else if (i3 == -1 && intent != null && (weatherWatchAppSetting = (WeatherWatchAppSetting) intent.getParcelableExtra("WEATHER_WATCH_APP_SETTING")) != null) {
                V96 v962 = this.h;
                if (v962 != null) {
                    v962.t("weather", weatherWatchAppSetting);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("SEARCH_WATCH_APP_RESULT_ID");
            if (!TextUtils.isEmpty(stringExtra)) {
                V96 v963 = this.h;
                if (v963 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (stringExtra != null) {
                    v963.r(stringExtra);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Cc5 cc5 = (Cc5) Aq0.f(layoutInflater, 2131558637, viewGroup, false, A6());
        PortfolioApp.get.instance().getIface().T1(new Z96(this)).a(this);
        this.g = new G37<>(this, cc5);
        Wg6.b(cc5, "binding");
        return cc5.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        V96 v96 = this.h;
        if (v96 != null) {
            v96.m();
            super.onDestroy();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        WatchAppsAdapter watchAppsAdapter = new WatchAppsAdapter(null, null, 3, null);
        watchAppsAdapter.p(new Ai(this));
        this.j = watchAppsAdapter;
        Fw5 fw5 = new Fw5(null, null, 3, null);
        fw5.o(new Bi(this));
        this.i = fw5;
        G37<Cc5> g37 = this.g;
        if (g37 != null) {
            Cc5 a2 = g37.a();
            if (a2 != null) {
                String d = ThemeManager.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    a2.r.setBackgroundColor(Color.parseColor(d));
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                Fw5 fw52 = this.i;
                if (fw52 != null) {
                    recyclerView.setAdapter(fw52);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    WatchAppsAdapter watchAppsAdapter2 = this.j;
                    if (watchAppsAdapter2 != null) {
                        recyclerView2.setAdapter(watchAppsAdapter2);
                        a2.y.setOnClickListener(new Ci(this));
                        a2.v.setOnClickListener(new Di(this));
                        return;
                    }
                    Wg6.n("mWatchAppAdapter");
                    throw null;
                }
                Wg6.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W96
    public void u0(String str) {
        Wg6.c(str, "category");
        G37<Cc5> g37 = this.g;
        if (g37 != null) {
            Cc5 a2 = g37.a();
            if (a2 != null) {
                Fw5 fw5 = this.i;
                if (fw5 != null) {
                    int j2 = fw5.j(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "scrollToCategory category=" + str + " scrollTo " + j2);
                    if (j2 >= 0) {
                        Fw5 fw52 = this.i;
                        if (fw52 != null) {
                            fw52.p(j2);
                            a2.s.smoothScrollToPosition(j2);
                            return;
                        }
                        Wg6.n("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                Wg6.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
