package com.fossil;

import com.fossil.Af1;
import com.fossil.Wb1;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oe1<Data> implements Af1<byte[], Data> {
    @DexIgnore
    public /* final */ Bi<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Bf1<byte[], ByteBuffer> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Bi<ByteBuffer> {
            @DexIgnore
            public Aii(Ai ai) {
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Oe1.Bi
            public /* bridge */ /* synthetic */ ByteBuffer a(byte[] bArr) {
                return b(bArr);
            }

            @DexIgnore
            public ByteBuffer b(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }

            @DexIgnore
            @Override // com.fossil.Oe1.Bi
            public Class<ByteBuffer> getDataClass() {
                return ByteBuffer.class;
            }
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<byte[], ByteBuffer> b(Ef1 ef1) {
            return new Oe1(new Aii(this));
        }
    }

    @DexIgnore
    public interface Bi<Data> {
        @DexIgnore
        Data a(byte[] bArr);

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<Data> implements Wb1<Data> {
        @DexIgnore
        public /* final */ byte[] b;
        @DexIgnore
        public /* final */ Bi<Data> c;

        @DexIgnore
        public Ci(byte[] bArr, Bi<Data> bi) {
            this.b = bArr;
            this.c = bi;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super Data> ai) {
            ai.e(this.c.a(this.b));
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<Data> getDataClass() {
            return this.c.getDataClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements Bf1<byte[], InputStream> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Bi<InputStream> {
            @DexIgnore
            public Aii(Di di) {
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Oe1.Bi
            public /* bridge */ /* synthetic */ InputStream a(byte[] bArr) {
                return b(bArr);
            }

            @DexIgnore
            public InputStream b(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }

            @DexIgnore
            @Override // com.fossil.Oe1.Bi
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<byte[], InputStream> b(Ef1 ef1) {
            return new Oe1(new Aii(this));
        }
    }

    @DexIgnore
    public Oe1(Bi<Data> bi) {
        this.a = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(byte[] bArr) {
        return d(bArr);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(byte[] bArr, int i, int i2, Ob1 ob1) {
        return c(bArr, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<Data> c(byte[] bArr, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(bArr), new Ci(bArr, this.a));
    }

    @DexIgnore
    public boolean d(byte[] bArr) {
        return true;
    }
}
