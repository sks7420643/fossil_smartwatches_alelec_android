package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Kc4 implements Zy1 {
    @DexIgnore
    public /* final */ Ot3 a;
    @DexIgnore
    public /* final */ Z84 b;

    @DexIgnore
    public Kc4(Ot3 ot3, Z84 z84) {
        this.a = ot3;
        this.b = z84;
    }

    @DexIgnore
    public static Zy1 b(Ot3 ot3, Z84 z84) {
        return new Kc4(ot3, z84);
    }

    @DexIgnore
    @Override // com.fossil.Zy1
    public void a(Exception exc) {
        Mc4.b(this.a, this.b, exc);
    }
}
