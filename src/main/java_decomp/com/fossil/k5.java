package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K5 implements Parcelable {
    @DexIgnore
    public static /* final */ T4 CREATOR; // = new T4(null);
    @DexIgnore
    public /* final */ BluetoothDevice A;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public BluetoothGatt c;
    @DexIgnore
    public long d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public V4 f;
    @DexIgnore
    public /* final */ BluetoothGattCallback g;
    @DexIgnore
    public L4 h;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<Bs> i;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<Ds> j;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<D5> k;
    @DexIgnore
    public /* final */ HashMap<N6, P6> l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public /* final */ BroadcastReceiver t;
    @DexIgnore
    public /* final */ BroadcastReceiver u;
    @DexIgnore
    public /* final */ BroadcastReceiver v;
    @DexIgnore
    public F5 w;
    @DexIgnore
    public /* final */ String x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public /* final */ N4 z;

    @DexIgnore
    public /* synthetic */ K5(BluetoothDevice bluetoothDevice, Qg6 qg6) {
        this.A = bluetoothDevice;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.b = new Handler(myLooper);
            this.g = new K6(this);
            this.h = new L4(this);
            this.i = new CopyOnWriteArraySet<>();
            this.j = new CopyOnWriteArraySet<>();
            this.k = new CopyOnWriteArraySet<>();
            this.l = new HashMap<>();
            this.m = 20;
            this.s = 20;
            this.t = new M7(this);
            this.u = new Q7(this);
            this.v = new U7(this);
            this.w = F5.b;
            String address = this.A.getAddress();
            Wg6.b(address, "bluetoothDevice.address");
            this.x = address;
            this.z = new N4();
            BroadcastReceiver broadcastReceiver = this.t;
            Context a2 = Id0.i.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (int i2 = 0; i2 < 1; i2++) {
                    intentFilter.addAction(new String[]{"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"}[i2]);
                }
                Ct0.b(a2).c(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = this.u;
            Context a3 = Id0.i.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (int i3 = 0; i3 < 1; i3++) {
                    intentFilter2.addAction(new String[]{"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED"}[i3]);
                }
                Ct0.b(a3).c(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = this.v;
            Context a4 = Id0.i.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (int i4 = 0; i4 < 1; i4++) {
                    intentFilter3.addAction(new String[]{"com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED"}[i4]);
                }
                Ct0.b(a4).c(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void A() {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new S4(this, new G7(F7.h, 0, 2), new UUID[0], new N6[0]));
            return;
        }
        BluetoothGatt bluetoothGatt = this.c;
        if (bluetoothGatt == null) {
            this.b.post(new S4(this, new G7(F7.c, 0, 2), new UUID[0], new N6[0]));
        } else if (true != bluetoothGatt.discoverServices()) {
            this.e = true;
            this.b.post(new S4(this, new G7(F7.f, 0, 2), new UUID[0], new N6[0]));
        }
    }

    @DexIgnore
    public final String C() {
        String name = this.A.getName();
        return name != null ? name : "";
    }

    @DexIgnore
    public final B5 D() {
        return B5.g.a(L80.d.h(this.A));
    }

    @DexIgnore
    public final boolean E() {
        if (!Q3.f.a()) {
            return false;
        }
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            return false;
        }
        try {
            Ky1 ky12 = Ky1.DEBUG;
            Method method = this.A.getClass().getMethod("removeBond", new Class[0]);
            Wg6.b(method, "bluetoothDevice.javaClass.getMethod(\"removeBond\")");
            Object invoke = method.invoke(this.A, new Object[0]);
            if (invoke != null) {
                return ((Boolean) invoke).booleanValue();
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Boolean");
        } catch (Exception e2) {
            D90.i.i(e2);
            return false;
        }
    }

    @DexIgnore
    public final void F() {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new A5(this, new G7(F7.h, 0, 2), 0));
            return;
        }
        G7 g7 = new G7(F7.b, 0, 2);
        BluetoothGatt bluetoothGatt = this.c;
        if (bluetoothGatt == null) {
            g7 = new G7(F7.c, 0, 2);
        } else if (true != bluetoothGatt.readRemoteRssi()) {
            g7 = new G7(F7.f, 0, 2);
        }
        if (g7.b != F7.b) {
            this.b.post(new A5(this, g7, 0));
        }
    }

    @DexIgnore
    public final R4 H() {
        return R4.f.a(this.A.getBondState());
    }

    @DexIgnore
    public final void I() {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new C5(this, new G7(F7.h, 0, 2), H(), H()));
        } else if (R4.b == H()) {
            this.b.post(new C5(this, new G7(F7.b, 0, 2), H(), H()));
        } else if (!E()) {
            this.b.post(new C5(this, new G7(F7.f, 0, 2), H(), H()));
        }
    }

    @DexIgnore
    public final void a() {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            G7 g7 = new G7(F7.h, 0, 2);
            B5 b5 = B5.b;
            this.b.post(new I4(this, g7, b5, b5));
            return;
        }
        int i2 = H5.c[D().ordinal()];
        if (i2 == 1) {
            G7 a2 = G7.d.a(L80.d.a(this.A));
            if (a2.b != F7.b) {
                B5 b52 = B5.b;
                this.b.post(new I4(this, a2, b52, b52));
            }
        } else if (i2 == 3) {
            G7 g72 = new G7(F7.b, 0, 2);
            B5 b53 = B5.d;
            this.b.post(new I4(this, g72, b53, b53));
        }
    }

    @DexIgnore
    public final void b(int i2) {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new E5(this, new G7(F7.h, 0, 2), 0));
        } else if (i2 == this.m) {
            this.b.post(new E5(this, new G7(F7.b, 0, 2), this.m));
        } else {
            BluetoothGatt bluetoothGatt = this.c;
            if (bluetoothGatt == null) {
                this.b.post(new E5(this, new G7(F7.c, 0, 2), this.m));
            } else if (true != bluetoothGatt.requestMtu(i2)) {
                this.e = true;
                this.b.post(new E5(this, new G7(F7.f, 0, 2), this.m));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0016, code lost:
        if (r7 != 3) goto L_0x0018;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(int r7, int r8) {
        /*
        // Method dump skipped, instructions count: 235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.K5.c(int, int):void");
    }

    @DexIgnore
    public final void d(int i2, F5 f5) {
        Iterator<Ds> it = this.j.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new K4(it.next(), f5, i2));
            } catch (Exception e2) {
                D90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void e(V4 v4) {
        A90 a90 = new A90("connection_state_changed", V80.h, this.x, "", "", v4.a == 0, null, null, null, G80.k(G80.k(new JSONObject(), Jd0.w0, Integer.valueOf(v4.a)), Jd0.B0, Ey1.a(w(v4.b))), 448);
        Long l2 = (Long) Pm7.G(v4.c);
        if (l2 != null) {
            long longValue = l2.longValue();
            a90.b = longValue;
            G80.k(a90.n, Jd0.X, Double.valueOf(Hy1.f(longValue)));
            JSONObject jSONObject = a90.n;
            Jd0 jd0 = Jd0.Y;
            Long l3 = (Long) Pm7.Q(v4.c);
            if (l3 != null) {
                longValue = l3.longValue();
            }
            G80.k(jSONObject, jd0, Double.valueOf(Hy1.f(longValue)));
            JSONArray jSONArray = new JSONArray();
            Iterator<T> it = v4.c.iterator();
            while (it.hasNext()) {
                jSONArray.put(Hy1.f(it.next().longValue()));
            }
            G80.k(a90.n, Jd0.i5, jSONArray);
            D90.i.d(a90);
        }
    }

    @DexIgnore
    public final void f(B5 b5, B5 b52) {
        this.b.post(new Q4(this, new G7(F7.b, 0, 2), b5, b52));
        this.b.post(new I4(this, new G7(F7.b, 0, 2), b5, b52));
        Iterator<T> it = this.k.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new U4(it.next(), this, b5, b52));
            } catch (Exception e2) {
                D90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public final void g(F5 f5) {
        Iterator<T> it = this.k.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new W4(it.next(), this, f5));
            } catch (Exception e2) {
                D90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public final void m(U5 u5) {
        u5.f = new S7(this, u5);
        u5.g = new T7(this, u5);
        this.h.b(u5);
    }

    @DexIgnore
    public final void n(U5 u5, R5 r5) {
        if (u5 != null) {
            L4 l4 = this.h;
            if (!Wg6.a(u5, l4.b)) {
                u5.a(J4.b);
                l4.a.remove(u5);
            }
            u5.e(r5);
        }
    }

    @DexIgnore
    public final void p(N6 n6) {
        G7 g7;
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new Y4(this, new G7(F7.h, 0, 2), n6, new byte[0]));
            return;
        }
        G7 g72 = new G7(F7.b, 0, 2);
        if (this.c == null) {
            g7 = new G7(F7.c, 0, 2);
        } else {
            P6 p6 = this.l.get(n6);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = p6 != null ? p6.a : null;
            if (bluetoothGattCharacteristic == null) {
                g7 = new G7(F7.d, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.c;
                if (bluetoothGatt == null || true != bluetoothGatt.readCharacteristic(bluetoothGattCharacteristic)) {
                    this.e = true;
                    g7 = new G7(F7.f, 0, 2);
                } else {
                    g7 = g72;
                }
            }
        }
        if (g7.b != F7.b) {
            this.b.post(new Y4(this, g7, n6, new byte[0]));
        }
    }

    @DexIgnore
    public final void q(N6 n6, byte[] bArr) {
        Ky1 ky1 = Ky1.DEBUG;
        n6.name();
        Dy1.e(bArr, null, 1, null);
        Iterator<Ds> it = this.j.iterator();
        while (it.hasNext()) {
            try {
                this.b.post(new V7(it.next(), n6, bArr));
            } catch (Exception e2) {
                D90.i.i(e2);
            }
        }
    }

    @DexIgnore
    public final void r(BluetoothLeAdapter.Ci ci) {
        if (ci != BluetoothLeAdapter.Ci.ENABLED) {
            this.e = true;
            c(0, X6.k.b);
        }
    }

    @DexIgnore
    public final void s(List<? extends BluetoothGattService> list) {
        for (BluetoothGattService bluetoothGattService : list) {
            for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
                O6 o6 = P6.b;
                UUID uuid = bluetoothGattCharacteristic.getUuid();
                Wg6.b(uuid, "characteristic.uuid");
                N6 a2 = o6.a(uuid);
                if (a2 != N6.d) {
                    this.l.put(a2, new P6(bluetoothGattCharacteristic));
                }
            }
        }
    }

    @DexIgnore
    public final void t(boolean z2) {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new G4(this, new G7(F7.h, 0, 2), F5.b));
            return;
        }
        int i2 = H5.a[this.w.ordinal()];
        if (i2 == 1) {
            c(1, 0);
            BluetoothGatt bluetoothGatt = this.c;
            if (bluetoothGatt == null) {
                Ky1 ky12 = Ky1.DEBUG;
                BluetoothGatt connectGatt = Build.VERSION.SDK_INT >= 23 ? this.A.connectGatt(Id0.i.a(), z2, this.g, 2) : this.A.connectGatt(Id0.i.a(), z2, this.g);
                this.c = connectGatt;
                if (connectGatt == null) {
                    Ky1 ky13 = Ky1.DEBUG;
                    c(0, X6.f.b);
                    return;
                }
                this.d = System.currentTimeMillis();
                Ky1 ky14 = Ky1.DEBUG;
            } else if (this.e || true != bluetoothGatt.connect()) {
                Ky1 ky15 = Ky1.DEBUG;
                m(new X5(this.z));
                c(0, X6.f.b);
            } else {
                this.d = System.currentTimeMillis();
                Ky1 ky16 = Ky1.DEBUG;
            }
        } else if (i2 != 3) {
        } else {
            if (this.c != null) {
                this.b.post(new G4(this, new G7(F7.b, 0, 2), F5.d));
            } else {
                c(0, X6.c.b);
            }
        }
    }

    @DexIgnore
    public final boolean u(N6 n6, boolean z2) {
        BluetoothGatt bluetoothGatt;
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
        } else {
            P6 p6 = this.l.get(n6);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = p6 != null ? p6.a : null;
            if (!(bluetoothGattCharacteristic == null || (bluetoothGatt = this.c) == null || true != bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, z2))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean v(BluetoothGatt bluetoothGatt) {
        Object obj;
        boolean z2 = false;
        if (!Q3.f.a()) {
            obj = null;
        } else if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            obj = "Peripheral.refreshDeviceCache: Bluetooth Off.";
        } else {
            try {
                Ky1 ky12 = Ky1.DEBUG;
                Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
                Wg6.b(method, "gatt.javaClass.getMethod(\"refresh\")");
                Object invoke = method.invoke(bluetoothGatt, new Object[0]);
                if (invoke != null) {
                    z2 = ((Boolean) invoke).booleanValue();
                    obj = null;
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.Boolean");
                }
            } catch (Exception e2) {
                obj = Fy1.a(e2);
                D90.i.i(e2);
            }
        }
        D90.i.d(new A90("refresh_device_cache", V80.e, this.x, "", E.a("UUID.randomUUID().toString()"), z2, null, null, null, G80.k(new JSONObject(), Jd0.P0, obj != null ? obj : JSONObject.NULL), 448));
        return z2;
    }

    @DexIgnore
    public final F5 w(int i2) {
        return i2 != 0 ? i2 != 1 ? i2 != 2 ? i2 != 3 ? F5.b : F5.e : F5.d : F5.c : F5.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.A, i2);
        }
    }

    @DexIgnore
    public final void x() {
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new M4(this, new G7(F7.h, 0, 2), H(), H()));
        } else if (R4.d == H()) {
            this.b.post(new M4(this, new G7(F7.b, 0, 2), H(), H()));
        } else if (!this.A.createBond()) {
            this.b.post(new M4(this, new G7(F7.f, 0, 2), H(), H()));
        }
    }

    @DexIgnore
    public final void y(N6 n6, byte[] bArr) {
        G7 g7;
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            Ky1 ky1 = Ky1.DEBUG;
            this.b.post(new G5(this, new G7(F7.h, 0, 2), n6, new byte[0]));
            return;
        }
        G7 g72 = new G7(F7.b, 0, 2);
        if (this.c == null) {
            g7 = new G7(F7.c, 0, 2);
        } else {
            P6 p6 = this.l.get(n6);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = p6 != null ? p6.a : null;
            if (bluetoothGattCharacteristic != null) {
                bluetoothGattCharacteristic.setValue(bArr);
            }
            if (bluetoothGattCharacteristic == null) {
                g7 = new G7(F7.d, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.c;
                if (bluetoothGatt == null || true != bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)) {
                    this.e = true;
                    g7 = new G7(F7.f, 0, 2);
                } else {
                    g7 = g72;
                }
            }
        }
        Ky1 ky12 = Ky1.DEBUG;
        int length = bArr.length;
        Dy1.e(bArr, null, 1, null);
        if (g7.b != F7.b) {
            this.b.post(new G5(this, g7, n6, new byte[0]));
        }
    }

    @DexIgnore
    public final void z(boolean z2) {
        this.y = z2;
    }
}
