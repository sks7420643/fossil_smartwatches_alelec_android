package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ki6 implements Factory<DashboardHeartRatePresenter> {
    @DexIgnore
    public static DashboardHeartRatePresenter a(Hi6 hi6, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        return new DashboardHeartRatePresenter(hi6, heartRateSummaryRepository, fitnessDataRepository, userRepository, u04);
    }
}
