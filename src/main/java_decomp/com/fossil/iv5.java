package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv5 extends iq4<a, c, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ AuthApiGuestService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1675a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, Constants.EMAIL);
            this.f1675a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f1675a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1676a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            this.f1676a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f1676a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase", f = "ResetPasswordUseCase.kt", l = {27}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iv5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(iv5 iv5, qn7 qn7) {
            super(qn7);
            this.this$0 = iv5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$run$response$1", f = "ResetPasswordUseCase.kt", l = {27}, m = "invokeSuspend")
    public static final class e extends ko7 implements rp7<qn7<? super q88<Void>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ iv5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(iv5 iv5, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = iv5;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new e(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<Void>> qn7) {
            throw null;
            //return ((e) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                AuthApiGuestService authApiGuestService = this.this$0.d;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object passwordRequestReset = authApiGuestService.passwordRequestReset(gj4, this);
                return passwordRequestReset == d ? d : passwordRequestReset;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = iv5.class.getSimpleName();
        pq7.b(simpleName, "ResetPasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public iv5(AuthApiGuestService authApiGuestService) {
        pq7.c(authApiGuestService, "mApiGuestService");
        this.d = authApiGuestService;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: n */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.iv5.a r10, com.fossil.qn7<java.lang.Object> r11) {
        /*
        // Method dump skipped, instructions count: 257
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv5.k(com.fossil.iv5$a, com.fossil.qn7):java.lang.Object");
    }
}
