package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Pc2;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cj2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Cj2> CREATOR; // = new Bj2();
    @DexIgnore
    public /* final */ DataSet b;
    @DexIgnore
    public /* final */ Mo2 c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public Cj2(DataSet dataSet, IBinder iBinder, boolean z) {
        this.b = dataSet;
        this.c = Oo2.e(iBinder);
        this.d = z;
    }

    @DexIgnore
    public Cj2(DataSet dataSet, Mo2 mo2, boolean z) {
        this.b = dataSet;
        this.c = mo2;
        this.d = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return obj == this || ((obj instanceof Cj2) && Pc2.a(this.b, ((Cj2) obj).b));
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(this.b);
    }

    @DexIgnore
    public final String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a("dataSet", this.b);
        return c2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, this.b, i, false);
        Mo2 mo2 = this.c;
        Bd2.m(parcel, 2, mo2 == null ? null : mo2.asBinder(), false);
        Bd2.c(parcel, 4, this.d);
        Bd2.b(parcel, a2);
    }
}
