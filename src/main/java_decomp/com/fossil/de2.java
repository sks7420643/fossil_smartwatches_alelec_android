package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class De2 implements Parcelable.Creator<Ee2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ee2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        B62[] b62Arr = null;
        Bundle bundle = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                bundle = Ad2.a(parcel, t);
            } else if (l == 2) {
                b62Arr = (B62[]) Ad2.i(parcel, t, B62.CREATOR);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                i = Ad2.v(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ee2(bundle, b62Arr, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ee2[] newArray(int i) {
        return new Ee2[i];
    }
}
