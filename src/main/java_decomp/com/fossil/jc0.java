package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.model.uiframework.packages.theme.ThemeTemplateFactory", f = "ThemeTemplateFactory.kt", l = {27, 31, 36}, m = "getThemeTemplate$blesdk_productionRelease")
public final class Jc0 extends Jf6 {
    @DexIgnore
    public /* synthetic */ Object b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ Nc0 d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public Object g;
    @DexIgnore
    public Object h;
    @DexIgnore
    public Object i;
    @DexIgnore
    public boolean j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Jc0(Nc0 nc0, Xe6 xe6) {
        super(xe6);
        this.d = nc0;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.b = obj;
        this.c |= RecyclerView.UNDEFINED_DURATION;
        return this.d.a(null, null, false, this);
    }
}
