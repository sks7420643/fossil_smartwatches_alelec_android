package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Id6 implements Factory<Hi6> {
    @DexIgnore
    public static Hi6 a(Dd6 dd6) {
        Hi6 e = dd6.e();
        Lk7.c(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
