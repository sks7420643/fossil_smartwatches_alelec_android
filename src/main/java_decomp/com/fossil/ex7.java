package com.fossil;

import com.mapped.Rc6;
import com.mapped.Rm6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ex7<J extends Rm6> extends Zu7 implements Dw7, Sw7 {
    @DexIgnore
    public /* final */ J e;

    @DexIgnore
    public Ex7(J j) {
        this.e = j;
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public Kx7 b() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Dw7
    public void dispose() {
        J j = this.e;
        if (j != null) {
            ((Fx7) j).i0(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public boolean isActive() {
        return true;
    }
}
