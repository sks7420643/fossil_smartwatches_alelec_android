package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Az0 implements Bz0 {
    @DexIgnore
    public /* final */ ViewGroupOverlay a;

    @DexIgnore
    public Az0(ViewGroup viewGroup) {
        this.a = viewGroup.getOverlay();
    }

    @DexIgnore
    @Override // com.fossil.Gz0
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Gz0
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Bz0
    public void c(View view) {
        this.a.add(view);
    }

    @DexIgnore
    @Override // com.fossil.Bz0
    public void d(View view) {
        this.a.remove(view);
    }
}
