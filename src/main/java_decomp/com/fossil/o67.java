package com.fossil;

import android.animation.Animator;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Gg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O67 implements View.OnTouchListener {
    @DexIgnore
    public static /* final */ DisplayMetrics j;
    @DexIgnore
    public static /* final */ int k; // = s.b(10.0f);
    @DexIgnore
    public static /* final */ int l; // = s.b(1.0f);
    @DexIgnore
    public static /* final */ Yk7 m; // = Zk7.a(Ai.INSTANCE);
    @DexIgnore
    public static /* final */ Bi s; // = new Bi(null);
    @DexIgnore
    public /* final */ Ci b; // = new Ci(this);
    @DexIgnore
    public /* final */ Vn0 c; // = new Vn0(this.i.getContext(), this.b);
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public /* final */ W67 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Gg6<Float> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(0);
        }

        @DexIgnore
        /* Return type fixed from 'float' to match base method */
        @Override // com.mapped.Gg6
        public final Float invoke() {
            throw null;
            //return TypedValue.applyDimension(1, 2.0f, O67.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int b(float f) {
            return (int) TypedValue.applyDimension(1, f, O67.j);
        }

        @DexIgnore
        public final float c() {
            Yk7 yk7 = O67.m;
            Bi bi = O67.s;
            return ((Number) yk7.getValue()).floatValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public /* final */ /* synthetic */ O67 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(O67 o67) {
            this.b = o67;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            this.b.c().o();
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends M67 {
        @DexIgnore
        public /* final */ /* synthetic */ O67 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(O67 o67) {
            this.a = o67;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.c().setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.a.c().e();
            this.a.c().setAnimating$app_fossilRelease(false);
        }
    }

    /*
    static {
        Resources system = Resources.getSystem();
        Wg6.b(system, "Resources.getSystem()");
        j = system.getDisplayMetrics();
    }
    */

    @DexIgnore
    public O67(W67 w67) {
        Wg6.c(w67, "element");
        this.i = w67;
    }

    @DexIgnore
    public final W67 c() {
        return this.i;
    }

    @DexIgnore
    public void d(View view, MotionEvent motionEvent) {
        boolean z = false;
        int i2 = 1;
        Wg6.c(view, "_view");
        Wg6.c(motionEvent, Constants.EVENT);
        if (!this.i.h()) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                this.h = motionEvent.getPointerId(0);
                this.d = motionEvent.getX();
                this.e = motionEvent.getY();
                this.i.setEditing$app_fossilRelease(true);
                this.i.setTranslating$app_fossilRelease(false);
                this.i.setElevation(s.c());
                W67 w67 = this.i;
                w67.setTouchCount$app_fossilRelease(w67.getTouchCount$app_fossilRelease() + 1);
                this.i.bringToFront();
                this.i.q();
            } else if (actionMasked == 1) {
                this.h = -1;
                this.i.setEditing$app_fossilRelease(false);
                if (!this.i.a()) {
                    this.i.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.i.e();
                    float f2 = this.f;
                    int i3 = l;
                    if (f2 > ((float) i3) || this.g > ((float) i3)) {
                        z = true;
                    }
                    if (z) {
                        this.i.m();
                    } else if (this.i.getTouchCount$app_fossilRelease() == 1) {
                        this.i.m();
                    }
                } else if (this.i.k(motionEvent)) {
                    this.i.b();
                    this.i.e();
                } else {
                    Di di = new Di(this);
                    this.i.setAnimating$app_fossilRelease(true);
                    this.i.animate().translationX(this.i.getMPrevTranslateX()).translationY(this.i.getMPrevTranslateY()).setListener(di);
                }
            } else if (actionMasked != 2) {
                if (actionMasked == 3) {
                    this.h = -1;
                } else if (actionMasked == 6) {
                    int actionIndex = motionEvent.getActionIndex();
                    if (motionEvent.getPointerId(actionIndex) == this.h) {
                        if (actionIndex != 0) {
                            i2 = 0;
                        }
                        this.d = motionEvent.getX(i2);
                        this.e = motionEvent.getY(i2);
                        this.h = motionEvent.getPointerId(i2);
                    }
                }
            } else if (motionEvent.getPointerId(motionEvent.getActionIndex()) == this.h) {
                float[] fArr = {motionEvent.getX() - this.d, motionEvent.getY() - this.e};
                this.i.getMatrix().mapVectors(fArr);
                W67 w672 = this.i;
                w672.setTranslationX(w672.getTranslationX() + fArr[0]);
                W67 w673 = this.i;
                w673.setTranslationY(w673.getTranslationY() + fArr[1]);
                this.f = Math.abs(this.i.getTranslationX() - this.i.getMPrevTranslateX());
                this.g = Math.abs(this.i.getTranslationY() - this.i.getMPrevTranslateY());
                if (!this.i.l()) {
                    W67 w674 = this.i;
                    float f3 = this.f;
                    int i4 = k;
                    if (f3 > ((float) i4) || this.g > ((float) i4)) {
                        z = true;
                    }
                    w674.setTranslating$app_fossilRelease(z);
                }
                if (this.i.l()) {
                    this.i.f();
                }
            }
            if (!this.i.l()) {
                this.c.a(motionEvent);
            }
        }
    }

    @DexIgnore
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Wg6.c(view, "view");
        Wg6.c(motionEvent, Constants.EVENT);
        boolean z = this.i.i() || this.i.p();
        if (z) {
            d(view, motionEvent);
        }
        return z;
    }
}
