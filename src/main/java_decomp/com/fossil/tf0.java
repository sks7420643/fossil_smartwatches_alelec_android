package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tf0 extends MenuInflater {
    @DexIgnore
    public static /* final */ Class<?>[] e;
    @DexIgnore
    public static /* final */ Class<?>[] f;
    @DexIgnore
    public /* final */ Object[] a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public Object d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements MenuItem.OnMenuItemClickListener {
        @DexIgnore
        public static /* final */ Class<?>[] c; // = {MenuItem.class};
        @DexIgnore
        public Object a;
        @DexIgnore
        public Method b;

        @DexIgnore
        public Ai(Object obj, String str) {
            this.a = obj;
            Class<?> cls = obj.getClass();
            try {
                this.b = cls.getMethod(str, c);
            } catch (Exception e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.b.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.b.invoke(this.a, menuItem)).booleanValue();
                }
                this.b.invoke(this.a, menuItem);
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi {
        @DexIgnore
        public Sn0 A;
        @DexIgnore
        public CharSequence B;
        @DexIgnore
        public CharSequence C;
        @DexIgnore
        public ColorStateList D; // = null;
        @DexIgnore
        public PorterDuff.Mode E; // = null;
        @DexIgnore
        public Menu a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public int i;
        @DexIgnore
        public int j;
        @DexIgnore
        public CharSequence k;
        @DexIgnore
        public CharSequence l;
        @DexIgnore
        public int m;
        @DexIgnore
        public char n;
        @DexIgnore
        public int o;
        @DexIgnore
        public char p;
        @DexIgnore
        public int q;
        @DexIgnore
        public int r;
        @DexIgnore
        public boolean s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public boolean u;
        @DexIgnore
        public int v;
        @DexIgnore
        public int w;
        @DexIgnore
        public String x;
        @DexIgnore
        public String y;
        @DexIgnore
        public String z;

        @DexIgnore
        public Bi(Menu menu) {
            this.a = menu;
            h();
        }

        @DexIgnore
        public void a() {
            this.h = true;
            i(this.a.add(this.b, this.i, this.j, this.k));
        }

        @DexIgnore
        public SubMenu b() {
            this.h = true;
            SubMenu addSubMenu = this.a.addSubMenu(this.b, this.i, this.j, this.k);
            i(addSubMenu.getItem());
            return addSubMenu;
        }

        @DexIgnore
        public final char c(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        @DexIgnore
        public boolean d() {
            return this.h;
        }

        @DexIgnore
        public final <T> T e(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = Class.forName(str, false, Tf0.this.c.getClassLoader()).getConstructor(clsArr);
                constructor.setAccessible(true);
                return (T) constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }

        @DexIgnore
        public void f(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = Tf0.this.c.obtainStyledAttributes(attributeSet, Ue0.MenuGroup);
            this.b = obtainStyledAttributes.getResourceId(Ue0.MenuGroup_android_id, 0);
            this.c = obtainStyledAttributes.getInt(Ue0.MenuGroup_android_menuCategory, 0);
            this.d = obtainStyledAttributes.getInt(Ue0.MenuGroup_android_orderInCategory, 0);
            this.e = obtainStyledAttributes.getInt(Ue0.MenuGroup_android_checkableBehavior, 0);
            this.f = obtainStyledAttributes.getBoolean(Ue0.MenuGroup_android_visible, true);
            this.g = obtainStyledAttributes.getBoolean(Ue0.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        public void g(AttributeSet attributeSet) {
            Th0 u2 = Th0.u(Tf0.this.c, attributeSet, Ue0.MenuItem);
            this.i = u2.n(Ue0.MenuItem_android_id, 0);
            this.j = (u2.k(Ue0.MenuItem_android_menuCategory, this.c) & -65536) | (u2.k(Ue0.MenuItem_android_orderInCategory, this.d) & 65535);
            this.k = u2.p(Ue0.MenuItem_android_title);
            this.l = u2.p(Ue0.MenuItem_android_titleCondensed);
            this.m = u2.n(Ue0.MenuItem_android_icon, 0);
            this.n = c(u2.o(Ue0.MenuItem_android_alphabeticShortcut));
            this.o = u2.k(Ue0.MenuItem_alphabeticModifiers, 4096);
            this.p = c(u2.o(Ue0.MenuItem_android_numericShortcut));
            this.q = u2.k(Ue0.MenuItem_numericModifiers, 4096);
            if (u2.s(Ue0.MenuItem_android_checkable)) {
                this.r = u2.a(Ue0.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.r = this.e;
            }
            this.s = u2.a(Ue0.MenuItem_android_checked, false);
            this.t = u2.a(Ue0.MenuItem_android_visible, this.f);
            this.u = u2.a(Ue0.MenuItem_android_enabled, this.g);
            this.v = u2.k(Ue0.MenuItem_showAsAction, -1);
            this.z = u2.o(Ue0.MenuItem_android_onClick);
            this.w = u2.n(Ue0.MenuItem_actionLayout, 0);
            this.x = u2.o(Ue0.MenuItem_actionViewClass);
            String o2 = u2.o(Ue0.MenuItem_actionProviderClass);
            this.y = o2;
            boolean z2 = o2 != null;
            if (z2 && this.w == 0 && this.x == null) {
                this.A = (Sn0) e(this.y, Tf0.f, Tf0.this.b);
            } else {
                if (z2) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.A = null;
            }
            this.B = u2.p(Ue0.MenuItem_contentDescription);
            this.C = u2.p(Ue0.MenuItem_tooltipText);
            if (u2.s(Ue0.MenuItem_iconTintMode)) {
                this.E = Dh0.e(u2.k(Ue0.MenuItem_iconTintMode, -1), this.E);
            } else {
                this.E = null;
            }
            if (u2.s(Ue0.MenuItem_iconTint)) {
                this.D = u2.c(Ue0.MenuItem_iconTint);
            } else {
                this.D = null;
            }
            u2.w();
            this.h = false;
        }

        @DexIgnore
        public void h() {
            this.b = 0;
            this.c = 0;
            this.d = 0;
            this.e = 0;
            this.f = true;
            this.g = true;
        }

        @DexIgnore
        public final void i(MenuItem menuItem) {
            boolean z2 = true;
            menuItem.setChecked(this.s).setVisible(this.t).setEnabled(this.u).setCheckable(this.r >= 1).setTitleCondensed(this.l).setIcon(this.m);
            int i2 = this.v;
            if (i2 >= 0) {
                menuItem.setShowAsAction(i2);
            }
            if (this.z != null) {
                if (!Tf0.this.c.isRestricted()) {
                    menuItem.setOnMenuItemClickListener(new Ai(Tf0.this.b(), this.z));
                } else {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
            }
            if (this.r >= 2) {
                if (menuItem instanceof Eg0) {
                    ((Eg0) menuItem).t(true);
                } else if (menuItem instanceof Fg0) {
                    ((Fg0) menuItem).h(true);
                }
            }
            String str = this.x;
            if (str != null) {
                menuItem.setActionView((View) e(str, Tf0.e, Tf0.this.a));
            } else {
                z2 = false;
            }
            int i3 = this.w;
            if (i3 > 0) {
                if (!z2) {
                    menuItem.setActionView(i3);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            Sn0 sn0 = this.A;
            if (sn0 != null) {
                Ao0.a(menuItem, sn0);
            }
            Ao0.c(menuItem, this.B);
            Ao0.g(menuItem, this.C);
            Ao0.b(menuItem, this.n, this.o);
            Ao0.f(menuItem, this.p, this.q);
            PorterDuff.Mode mode = this.E;
            if (mode != null) {
                Ao0.e(menuItem, mode);
            }
            ColorStateList colorStateList = this.D;
            if (colorStateList != null) {
                Ao0.d(menuItem, colorStateList);
            }
        }
    }

    /*
    static {
        Class<?>[] clsArr = {Context.class};
        e = clsArr;
        f = clsArr;
    }
    */

    @DexIgnore
    public Tf0(Context context) {
        super(context);
        this.c = context;
        Object[] objArr = {context};
        this.a = objArr;
        this.b = objArr;
    }

    @DexIgnore
    public final Object a(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? a(((ContextWrapper) obj).getBaseContext()) : obj;
    }

    @DexIgnore
    public Object b() {
        if (this.d == null) {
            this.d = a(this.c);
        }
        return this.d;
    }

    @DexIgnore
    public final void c(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        boolean z;
        Bi bi = new Bi(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        boolean z2 = false;
        String str = null;
        int i = eventType;
        boolean z3 = false;
        while (!z3) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        z = z3;
                    } else {
                        String name2 = xmlPullParser.getName();
                        if (z2 && name2.equals(str)) {
                            z = z3;
                            z2 = false;
                            str = null;
                        } else if (name2.equals("group")) {
                            bi.h();
                            z = z3;
                        } else {
                            if (name2.equals("item")) {
                                if (!bi.d()) {
                                    Sn0 sn0 = bi.A;
                                    if (sn0 == null || !sn0.hasSubMenu()) {
                                        bi.a();
                                        z = z3;
                                    } else {
                                        bi.b();
                                        z = z3;
                                    }
                                }
                            } else if (name2.equals("menu")) {
                                z = true;
                            }
                            z = z3;
                        }
                    }
                } else if (z2) {
                    z = z3;
                } else {
                    String name3 = xmlPullParser.getName();
                    if (name3.equals("group")) {
                        bi.f(attributeSet);
                        z = z3;
                    } else if (name3.equals("item")) {
                        bi.g(attributeSet);
                        z = z3;
                    } else if (name3.equals("menu")) {
                        c(xmlPullParser, attributeSet, bi.b());
                        z = z3;
                    } else {
                        z = z3;
                        z2 = true;
                        str = name3;
                    }
                }
                i = xmlPullParser.next();
                z3 = z;
            } else {
                throw new RuntimeException("Unexpected end of document");
            }
        }
    }

    @DexIgnore
    public void inflate(int i, Menu menu) {
        XmlResourceParser xmlResourceParser = null;
        if (!(menu instanceof Gm0)) {
            super.inflate(i, menu);
            return;
        }
        try {
            xmlResourceParser = this.c.getResources().getLayout(i);
            c(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }
}
