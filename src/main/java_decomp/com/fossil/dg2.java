package com.fossil;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dg2 {
    @DexIgnore
    public static volatile Ne2 a;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Context c;

    @DexIgnore
    public static Mg2 a(String str, Eg2 eg2, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return d(str, eg2, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public static final /* synthetic */ String b(boolean z, String str, Eg2 eg2) throws Exception {
        boolean z2 = true;
        if (z || !d(str, eg2, true, false).a) {
            z2 = false;
        }
        return Mg2.e(str, eg2, z, z2);
    }

    @DexIgnore
    public static void c(Context context) {
        synchronized (Dg2.class) {
            try {
                if (c != null) {
                    Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
                } else if (context != null) {
                    c = context.getApplicationContext();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static Mg2 d(String str, Eg2 eg2, boolean z, boolean z2) {
        try {
            if (a == null) {
                Rc2.k(c);
                synchronized (b) {
                    if (a == null) {
                        a = Pe2.e(DynamiteModule.e(c, DynamiteModule.k, "com.google.android.gms.googlecertificates").d("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            Rc2.k(c);
            try {
                return a.y2(new Kg2(str, eg2, z, z2), Tg2.n(c.getPackageManager())) ? Mg2.f() : Mg2.c(new Fg2(z, str, eg2));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return Mg2.b("module call", e);
            }
        } catch (DynamiteModule.a e2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            String valueOf = String.valueOf(e2.getMessage());
            return Mg2.b(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }
}
