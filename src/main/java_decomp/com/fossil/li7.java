package com.fossil;

import android.content.Context;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Li7 {
    @DexIgnore
    public static volatile Li7 c;
    @DexIgnore
    public Timer a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public Li7(Context context) {
        this.b = context.getApplicationContext();
        this.a = new Timer(false);
    }

    @DexIgnore
    public static Li7 b(Context context) {
        if (c == null) {
            synchronized (Li7.class) {
                try {
                    if (c == null) {
                        c = new Li7(context);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return c;
    }

    @DexIgnore
    public void c() {
        if (Fg7.I() == Gg7.PERIOD) {
            long F = (long) (Fg7.F() * 60 * 1000);
            if (Fg7.K()) {
                Th7 p = Ei7.p();
                p.h("setupPeriodTimer delay:" + F);
            }
            d(new Mi7(this), F);
        }
    }

    @DexIgnore
    public void d(TimerTask timerTask, long j) {
        if (this.a != null) {
            if (Fg7.K()) {
                Th7 p = Ei7.p();
                p.h("setupPeriodTimer schedule delay:" + j);
            }
            this.a.schedule(timerTask, j);
        } else if (Fg7.K()) {
            Ei7.p().l("setupPeriodTimer schedule timer == null");
        }
    }
}
