package com.fossil;

import com.mapped.Lc3;
import com.mapped.Mc3;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nt3<TResult> {
    @DexIgnore
    public Nt3<TResult> a(Executor executor, Gt3 gt3) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @DexIgnore
    public Nt3<TResult> b(Ht3<TResult> ht3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public Nt3<TResult> c(Executor executor, Ht3<TResult> ht3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public abstract Nt3<TResult> d(Lc3 lc3);

    @DexIgnore
    public abstract Nt3<TResult> e(Executor executor, Lc3 lc3);

    @DexIgnore
    public abstract Nt3<TResult> f(Mc3<? super TResult> mc3);

    @DexIgnore
    public abstract Nt3<TResult> g(Executor executor, Mc3<? super TResult> mc3);

    @DexIgnore
    public <TContinuationResult> Nt3<TContinuationResult> h(Ft3<TResult, TContinuationResult> ft3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> Nt3<TContinuationResult> i(Executor executor, Ft3<TResult, TContinuationResult> ft3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> Nt3<TContinuationResult> j(Ft3<TResult, Nt3<TContinuationResult>> ft3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> Nt3<TContinuationResult> k(Executor executor, Ft3<TResult, Nt3<TContinuationResult>> ft3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public abstract Exception l();

    @DexIgnore
    public abstract TResult m();

    @DexIgnore
    public abstract <X extends Throwable> TResult n(Class<X> cls) throws Throwable;

    @DexIgnore
    public abstract boolean o();

    @DexIgnore
    public abstract boolean p();

    @DexIgnore
    public abstract boolean q();

    @DexIgnore
    public <TContinuationResult> Nt3<TContinuationResult> r(Mt3<TResult, TContinuationResult> mt3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> Nt3<TContinuationResult> s(Executor executor, Mt3<TResult, TContinuationResult> mt3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
