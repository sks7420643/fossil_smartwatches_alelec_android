package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class So2 extends Gn2 {
    @DexIgnore
    public /* final */ /* synthetic */ DataSet s;
    @DexIgnore
    public /* final */ /* synthetic */ boolean t; // = false;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public So2(To2 to2, R62 r62, DataSet dataSet, boolean z) {
        super(r62);
        this.s = dataSet;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi] */
    @Override // com.fossil.I72
    public final /* synthetic */ void u(Zm2 zm2) throws RemoteException {
        ((Fo2) zm2.I()).E(new Cj2(this.s, (Mo2) new Wo2(this), this.t));
    }
}
