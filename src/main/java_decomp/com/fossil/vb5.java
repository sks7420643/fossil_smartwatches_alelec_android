package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vb5 extends Ub5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I;
    @DexIgnore
    public long G;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        I = sparseIntArray;
        sparseIntArray.put(2131362968, 1);
        I.put(2131362122, 2);
        I.put(2131362551, 3);
        I.put(2131362973, 4);
        I.put(2131362393, 5);
        I.put(2131362552, 6);
        I.put(2131363066, 7);
        I.put(2131362632, 8);
        I.put(2131362267, 9);
        I.put(2131362121, 10);
        I.put(2131362548, 11);
        I.put(2131362715, 12);
        I.put(2131362433, 13);
        I.put(2131362451, 14);
        I.put(2131362290, 15);
    }
    */

    @DexIgnore
    public Vb5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 16, H, I));
    }

    @DexIgnore
    public Vb5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[10], (ConstraintLayout) objArr[2], (FlexibleButton) objArr[9], (FlexibleButton) objArr[15], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[6], (TabLayout) objArr[8], (ImageView) objArr[12], (DashBar) objArr[1], (FlexibleProgressBar) objArr[4], (ConstraintLayout) objArr[0], (ViewPager2) objArr[7]);
        this.G = -1;
        this.E.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.G != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.G = 1;
        }
        w();
    }
}
