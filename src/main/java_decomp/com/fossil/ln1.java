package com.fossil;

import com.fossil.En1;
import com.mapped.A70;
import com.mapped.H60;
import com.mapped.H70;
import com.mapped.Hg6;
import com.mapped.I70;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.V60;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ln1 {
    @DexIgnore
    public /* final */ ArrayList<R60> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<R60, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ R60 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(R60 r60) {
            super(1);
            this.b = r60;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public Boolean invoke(R60 r60) {
            return Boolean.valueOf(r60.getKey() == this.b.getKey());
        }
    }

    @DexIgnore
    public final void a(R60 r60) {
        Mm7.w(this.a, new Ai(r60));
        this.a.add(r60);
    }

    @DexIgnore
    public final R60[] b() {
        Object[] array = this.a.toArray(new R60[0]);
        if (array != null) {
            return (R60[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Ln1 c(Kn1[] kn1Arr) throws IllegalArgumentException {
        a(new Jn1(kn1Arr));
        return this;
    }

    @DexIgnore
    public final Ln1 d(byte b, H60.Ai ai, short s, short s2, H60.Bi bi) throws IllegalArgumentException {
        a(new H60(b, ai, s, s2, bi));
        return this;
    }

    @DexIgnore
    public final Ln1 e(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a(new Pm1(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final Ln1 f(int i) throws IllegalArgumentException {
        a(new Qm1(i));
        return this;
    }

    @DexIgnore
    public final Ln1 g(long j) throws IllegalArgumentException {
        a(new Rm1(j));
        return this;
    }

    @DexIgnore
    public final Ln1 h(long j) throws IllegalArgumentException {
        a(new Sm1(j));
        return this;
    }

    @DexIgnore
    public final Ln1 i(long j) throws IllegalArgumentException {
        a(new Tm1(j));
        return this;
    }

    @DexIgnore
    public final Ln1 j(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a(new Um1(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final Ln1 k(long j) throws IllegalArgumentException {
        a(new Vm1(j));
        return this;
    }

    @DexIgnore
    public final Ln1 l(long j) throws IllegalArgumentException {
        a(new Wm1(j));
        return this;
    }

    @DexIgnore
    public final Ln1 m(int i) throws IllegalArgumentException {
        a(new Xm1(i));
        return this;
    }

    @DexIgnore
    public final Ln1 n(H70 h70, Mn1 mn1, Pn1 pn1, I70 i70, On1 on1) {
        a(new An1(h70, mn1, pn1, i70, on1));
        return this;
    }

    @DexIgnore
    public final Ln1 o(V60.Ai ai) {
        a(new V60(ai));
        return this;
    }

    @DexIgnore
    public final Ln1 p(byte b, byte b2, byte b3, byte b4, short s, En1.Ai ai) throws IllegalArgumentException {
        a(new En1(b, b2, b3, b4, s, ai));
        return this;
    }

    @DexIgnore
    public final Ln1 q(short s) throws IllegalArgumentException {
        a(new Fn1(s));
        return this;
    }

    @DexIgnore
    public final Ln1 r(long j, short s, short s2) throws IllegalArgumentException {
        a(new Gn1(j, s, s2));
        return this;
    }

    @DexIgnore
    public final Ln1 s(A70.Ai ai) {
        a(new A70(ai));
        return this;
    }
}
