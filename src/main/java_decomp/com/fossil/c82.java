package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C82 implements Ht3<TResult> {
    @DexIgnore
    public /* final */ /* synthetic */ Ot3 a;
    @DexIgnore
    public /* final */ /* synthetic */ A82 b;

    @DexIgnore
    public C82(A82 a82, Ot3 ot3) {
        this.b = a82;
        this.a = ot3;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3<TResult> nt3) {
        A82.h(this.b).remove(this.a);
    }
}
