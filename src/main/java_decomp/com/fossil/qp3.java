package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 d;

    @DexIgnore
    public Qp3(Fp3 fp3, Bundle bundle, Or3 or3) {
        this.d = fp3;
        this.b = bundle;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        Cl3 cl3 = this.d.d;
        if (cl3 == null) {
            this.d.d().F().a("Failed to send default event parameters to service");
            return;
        }
        try {
            cl3.n2(this.b, this.c);
        } catch (RemoteException e) {
            this.d.d().F().b("Failed to send default event parameters to service", e);
        }
    }
}
