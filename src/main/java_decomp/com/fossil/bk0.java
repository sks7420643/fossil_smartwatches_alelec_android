package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bk0 extends Ck0 {
    @DexIgnore
    public float c; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

    @DexIgnore
    @Override // com.fossil.Ck0
    public void e() {
        super.e();
        this.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void g() {
        this.b = 2;
    }

    @DexIgnore
    public void h(int i) {
        if (this.b == 0 || this.c != ((float) i)) {
            this.c = (float) i;
            if (this.b == 1) {
                c();
            }
            b();
        }
    }
}
