package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hk1 {
    @DexIgnore
    public Class<?> a;
    @DexIgnore
    public Class<?> b;
    @DexIgnore
    public Class<?> c;

    @DexIgnore
    public Hk1() {
    }

    @DexIgnore
    public Hk1(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Hk1.class != obj.getClass()) {
            return false;
        }
        Hk1 hk1 = (Hk1) obj;
        if (!this.a.equals(hk1.a)) {
            return false;
        }
        if (!this.b.equals(hk1.b)) {
            return false;
        }
        return Jk1.d(this.c, hk1.c);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        int hashCode2 = this.b.hashCode();
        Class<?> cls = this.c;
        return (cls != null ? cls.hashCode() : 0) + (((hashCode * 31) + hashCode2) * 31);
    }

    @DexIgnore
    public String toString() {
        return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
    }
}
