package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O75 extends N75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d V;
    @DexIgnore
    public static /* final */ SparseIntArray W;
    @DexIgnore
    public long U;

    /*
    static {
        ViewDataBinding.d dVar = new ViewDataBinding.d(30);
        V = dVar;
        dVar.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{2131558843});
        SparseIntArray sparseIntArray = new SparseIntArray();
        W = sparseIntArray;
        sparseIntArray.put(2131362361, 3);
        W.put(2131362360, 4);
        W.put(2131362779, 5);
        W.put(2131361966, 6);
        W.put(2131363026, 7);
        W.put(2131362499, 8);
        W.put(2131362803, 9);
        W.put(2131363289, 10);
        W.put(2131362767, 11);
        W.put(2131363459, 12);
        W.put(2131362799, 13);
        W.put(2131362368, 14);
        W.put(2131363460, 15);
        W.put(2131362835, 16);
        W.put(2131362555, 17);
        W.put(2131363461, 18);
        W.put(2131362416, 19);
        W.put(2131362519, 20);
        W.put(2131363167, 21);
        W.put(2131362104, 22);
        W.put(2131362522, 23);
        W.put(2131362824, 24);
        W.put(2131362523, 25);
        W.put(2131362520, 26);
        W.put(2131362823, 27);
        W.put(2131362521, 28);
        W.put(2131362490, 29);
    }
    */

    @DexIgnore
    public O75(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 30, V, W));
    }

    @DexIgnore
    public O75(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 1, (FlexibleButton) objArr[6], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[22], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[29], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[20], (FlexibleTextView) objArr[26], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[17], (Lg5) objArr[2], (RTLImageView) objArr[11], (ConstraintLayout) objArr[5], (LinearLayout) objArr[13], (ConstraintLayout) objArr[9], (LinearLayout) objArr[27], (LinearLayout) objArr[24], (LinearLayout) objArr[16], (ConstraintLayout) objArr[0], (RecyclerView) objArr[7], (SwitchCompat) objArr[21], (FlexibleTextView) objArr[10], (View) objArr[12], (View) objArr[15], (View) objArr[18]);
        this.U = -1;
        this.r.setTag(null);
        this.N.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.U = 0;
        }
        ViewDataBinding.i(this.F);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.F.o() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return false;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            long r2 = r6.U     // Catch:{ all -> 0x0017 }
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0017 }
        L_0x000b:
            return r0
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0017 }
            com.fossil.Lg5 r1 = r6.F
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            r0 = 0
            goto L_0x000b
        L_0x0017:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.O75.o():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.U = 2;
        }
        this.F.q();
        w();
    }
}
