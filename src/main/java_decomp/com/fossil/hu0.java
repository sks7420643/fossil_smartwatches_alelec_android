package com.fossil;

import com.mapped.Cf;
import com.mapped.Xe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hu0<T> extends Cf<T> {
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ Object v;
    @DexIgnore
    public /* final */ Xe<?, T> w;

    @DexIgnore
    public Hu0(Cf<T> cf) {
        super(cf.f.B(), cf.b, cf.c, null, cf.e);
        this.w = cf.p();
        this.u = cf.s();
        this.g = cf.g;
        this.v = cf.q();
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public void o(Cf<T> cf, Cf.Ei ei) {
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public Xe<?, T> p() {
        return this.w;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public Object q() {
        return this.v;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public boolean s() {
        return this.u;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public boolean t() {
        return true;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public boolean u() {
        return true;
    }

    @DexIgnore
    @Override // com.mapped.Cf
    public void w(int i) {
    }
}
