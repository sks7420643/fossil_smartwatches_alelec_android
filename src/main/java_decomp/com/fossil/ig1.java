package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ig1 implements Sb1<Drawable> {
    @DexIgnore
    public /* final */ Sb1<Bitmap> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public Ig1(Sb1<Bitmap> sb1, boolean z) {
        this.b = sb1;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.Sb1
    public Id1<Drawable> b(Context context, Id1<Drawable> id1, int i, int i2) {
        Rd1 f = Oa1.c(context).f();
        Drawable drawable = id1.get();
        Id1<Bitmap> a2 = Hg1.a(f, drawable, i, i2);
        if (a2 != null) {
            Id1<Bitmap> b2 = this.b.b(context, a2, i, i2);
            if (!b2.equals(a2)) {
                return d(context, b2);
            }
            b2.b();
            return id1;
        } else if (!this.c) {
            return id1;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    public Sb1<BitmapDrawable> c() {
        return this;
    }

    @DexIgnore
    public final Id1<Drawable> d(Context context, Id1<Bitmap> id1) {
        return Og1.f(context.getResources(), id1);
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (obj instanceof Ig1) {
            return this.b.equals(((Ig1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        return this.b.hashCode();
    }
}
