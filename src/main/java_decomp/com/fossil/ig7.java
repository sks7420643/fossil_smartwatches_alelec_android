package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ig7 {
    @DexIgnore
    public static Yh7 a;
    @DexIgnore
    public static volatile Map<String, Properties> b; // = new ConcurrentHashMap();
    @DexIgnore
    public static volatile Map<Integer, Integer> c; // = new ConcurrentHashMap(10);
    @DexIgnore
    public static volatile long d; // = 0;
    @DexIgnore
    public static volatile long e; // = 0;
    @DexIgnore
    public static volatile long f; // = 0;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static volatile int h; // = 0;
    @DexIgnore
    public static volatile String i; // = "";
    @DexIgnore
    public static volatile String j; // = "";
    @DexIgnore
    public static Map<String, Long> k; // = new ConcurrentHashMap();
    @DexIgnore
    public static Map<String, Long> l; // = new ConcurrentHashMap();
    @DexIgnore
    public static Th7 m; // = Ei7.p();
    @DexIgnore
    public static Thread.UncaughtExceptionHandler n; // = null;
    @DexIgnore
    public static volatile boolean o; // = true;
    @DexIgnore
    public static volatile int p; // = 0;
    @DexIgnore
    public static volatile long q; // = 0;
    @DexIgnore
    public static Context r; // = null;
    @DexIgnore
    public static volatile long s; // = 0;

    /*
    static {
        new ConcurrentHashMap();
    }
    */

    @DexIgnore
    public static void D(Context context, Jg7 jg7) {
        if (Fg7.M() && l(context) != null) {
            a.a(new Ui7(context, jg7));
        }
    }

    @DexIgnore
    public static void E(Context context, Jg7 jg7) {
        if (Fg7.M() && l(context) != null) {
            a.a(new Yg7(context, jg7));
        }
    }

    @DexIgnore
    public static boolean F(Context context, String str, String str2, Jg7 jg7) {
        try {
            if (!Fg7.M()) {
                m.f("MTA StatService is disable.");
                return false;
            }
            if (Fg7.K()) {
                m.b("MTA SDK version, current: 2.0.3 ,required: " + str2);
            }
            if (context == null || str2 == null) {
                m.f("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
                Fg7.P(false);
                return false;
            } else if (Ei7.o("2.0.3") < Ei7.o(str2)) {
                m.f(("MTA SDK version conflicted, current: 2.0.3,required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                Fg7.P(false);
                return false;
            } else {
                String v = Fg7.v(context);
                if (v == null || v.length() == 0) {
                    Fg7.R(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                }
                if (str != null) {
                    Fg7.N(context, str);
                }
                if (l(context) != null) {
                    a.a(new Zg7(context, jg7));
                }
                return true;
            }
        } catch (Throwable th) {
            m.e(th);
            return false;
        }
    }

    @DexIgnore
    public static void G(Context context) {
        if (Fg7.M()) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.testSpeed() can not be null!");
            } else if (l(x) != null) {
                a.a(new Vg7(x));
            }
        }
    }

    @DexIgnore
    public static void H(Context context, String str, Jg7 jg7) {
        if (Fg7.M()) {
            Context x = x(context);
            if (x == null || str == null || str.length() == 0) {
                m.f("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (l(x) != null) {
                a.a(new Yi7(str2, x, jg7));
            }
        }
    }

    @DexIgnore
    public static void I(Context context, String str, Properties properties, Jg7 jg7) {
        Th7 th7;
        String str2;
        if (Fg7.M()) {
            Context x = x(context);
            if (x == null) {
                th7 = m;
                str2 = "The Context of StatService.trackCustomEvent() can not be null!";
            } else if (h(str)) {
                th7 = m;
                str2 = "The event_id of StatService.trackCustomEvent() can not be null or empty.";
            } else {
                Lg7 lg7 = new Lg7(str, null, properties);
                if (l(x) != null) {
                    a.a(new Xi7(x, jg7, lg7));
                    return;
                }
                return;
            }
            th7.f(str2);
        }
    }

    @DexIgnore
    public static void J(Context context, String str, Jg7 jg7) {
        if (Fg7.M()) {
            Context x = x(context);
            if (x == null || str == null || str.length() == 0) {
                m.f("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (l(x) != null) {
                a.a(new Xg7(x, str2, jg7));
            }
        }
    }

    @DexIgnore
    public static int a(Context context, boolean z, Jg7 jg7) {
        boolean z2 = true;
        long currentTimeMillis = System.currentTimeMillis();
        boolean z3 = z && currentTimeMillis - e >= ((long) Fg7.G());
        e = currentTimeMillis;
        if (f == 0) {
            f = Ei7.r();
        }
        if (currentTimeMillis >= f) {
            f = Ei7.r();
            if (Gh7.b(context).v(context).e() != 1) {
                Gh7.b(context).v(context).b(1);
            }
            Fg7.o(0);
            p = 0;
            g = Ei7.g(0);
            z3 = true;
        }
        String str = g;
        if (Ei7.m(jg7)) {
            str = jg7.a() + g;
        }
        if (l.containsKey(str)) {
            z2 = z3;
        }
        if (z2) {
            if (Ei7.m(jg7)) {
                e(context, jg7);
            } else if (Fg7.r() < Fg7.y()) {
                Ei7.T(context);
                e(context, null);
            } else {
                m.d("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            l.put(str, 1L);
        }
        if (o) {
            G(context);
            o = false;
        }
        return h;
    }

    @DexIgnore
    public static void d(Context context) {
        synchronized (Ig7.class) {
            if (context != null) {
                try {
                    if (a == null) {
                        if (k(context)) {
                            Context applicationContext = context.getApplicationContext();
                            r = applicationContext;
                            a = new Yh7();
                            g = Ei7.g(0);
                            d = System.currentTimeMillis() + Fg7.x;
                            a.a(new Ti7(applicationContext));
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    @DexIgnore
    public static void e(Context context, Jg7 jg7) {
        if (l(context) != null) {
            if (Fg7.K()) {
                m.b("start new session.");
            }
            if (jg7 == null || h == 0) {
                h = Ei7.d();
            }
            Fg7.d(0);
            Fg7.n();
            new Ch7(new Sg7(context, h, j(), jg7)).b();
        }
    }

    @DexIgnore
    public static void f(Context context, Throwable th) {
        if (Fg7.M()) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (l(x) != null) {
                a.a(new Vi7(x, th));
            }
        }
    }

    @DexIgnore
    public static boolean g() {
        if (p < 2) {
            return false;
        }
        q = System.currentTimeMillis();
        return true;
    }

    @DexIgnore
    public static boolean h(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static JSONObject j() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (Fg7.c.d != 0) {
                jSONObject2.put("v", Fg7.c.d);
            }
            jSONObject.put(Integer.toString(Fg7.c.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (Fg7.b.d != 0) {
                jSONObject3.put("v", Fg7.b.d);
            }
            jSONObject.put(Integer.toString(Fg7.b.a), jSONObject3);
        } catch (JSONException e2) {
            m.e(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public static boolean k(Context context) {
        boolean z;
        boolean z2 = false;
        long b2 = Ii7.b(context, Fg7.n, 0);
        long o2 = Ei7.o("2.0.3");
        if (o2 <= b2) {
            Th7 th7 = m;
            th7.f("MTA is disable for current version:" + o2 + ",wakeup version:" + b2);
            z = false;
        } else {
            z = true;
        }
        long b3 = Ii7.b(context, Fg7.o, 0);
        if (b3 > System.currentTimeMillis()) {
            Th7 th72 = m;
            th72.f("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + b3);
        } else {
            z2 = z;
        }
        Fg7.P(z2);
        return z2;
    }

    @DexIgnore
    public static Yh7 l(Context context) {
        if (a == null) {
            synchronized (Ig7.class) {
                try {
                    if (a == null) {
                        try {
                            d(context);
                        } catch (Throwable th) {
                            m.g(th);
                            Fg7.P(false);
                        }
                    }
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        }
        return a;
    }

    @DexIgnore
    public static void n() {
        p = 0;
        q = 0;
    }

    @DexIgnore
    public static void o(Context context, int i2) {
        Th7 th7;
        String str;
        if (Fg7.M()) {
            if (Fg7.K()) {
                Th7 th72 = m;
                th72.h("commitEvents, maxNumber=" + i2);
            }
            Context x = x(context);
            if (x == null) {
                th7 = m;
                str = "The Context of StatService.commitEvents() can not be null!";
            } else if (i2 < -1 || i2 == 0) {
                th7 = m;
                str = "The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.";
            } else if (Tg7.a(r).j() && l(x) != null) {
                a.a(new Ug7(x, i2));
                return;
            } else {
                return;
            }
            th7.f(str);
        }
    }

    @DexIgnore
    public static void p() {
        p++;
        q = System.currentTimeMillis();
        u(r);
    }

    @DexIgnore
    public static void q(Context context) {
        if (Fg7.M()) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                Qi7.f(x).c(new Pg7(x), new Wi7());
            } catch (Throwable th) {
                m.e(th);
            }
        }
    }

    @DexIgnore
    public static void s(Context context) {
        s = System.currentTimeMillis() + ((long) (Fg7.F() * 60000));
        Ii7.f(context, "last_period_ts", s);
        o(context, -1);
    }

    @DexIgnore
    public static void u(Context context) {
        if (Fg7.M() && Fg7.J > 0) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.testSpeed() can not be null!");
            } else {
                Gh7.b(x).B();
            }
        }
    }

    @DexIgnore
    public static Properties w(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static Context x(Context context) {
        return context != null ? context : r;
    }
}
