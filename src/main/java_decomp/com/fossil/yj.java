package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yj extends Qq7 implements Coroutine<byte[], N6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Kk b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Yj(Kk kk) {
        super(2);
        this.b = kk;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(byte[] bArr, N6 n6) {
        byte[] bArr2 = bArr;
        N6 n62 = n6;
        Coroutine<? super byte[], ? super N6, Cd6> coroutine = this.b.D;
        if (coroutine != null) {
            coroutine.invoke(bArr2, n62);
        }
        return Cd6.a;
    }
}
