package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zc3 extends Ds2 implements Yc3 {
    @DexIgnore
    public Zc3() {
        super("com.google.android.gms.maps.internal.IOnPolylineClickListener");
    }

    @DexIgnore
    @Override // com.fossil.Ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        c0(Bs2.e(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
