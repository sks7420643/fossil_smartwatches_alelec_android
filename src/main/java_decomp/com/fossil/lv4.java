package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lv4 {
    @DexIgnore
    public /* final */ F57.Bi a;
    @DexIgnore
    public /* final */ Uy4 b; // = Uy4.d.b();
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ String a; // = ThemeManager.l.a().d("nonBrandSurface");
        @DexIgnore
        public /* final */ String b; // = ThemeManager.l.a().d("nonBrandPlaceholderBackground");
        @DexIgnore
        public /* final */ Jf5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Jf5 jf5) {
            super(jf5.n());
            Wg6.c(jf5, "binding");
            this.c = jf5;
        }

        @DexIgnore
        public final void a(Ms4 ms4, long j, int i, F57.Bi bi, Uy4 uy4) {
            String str;
            Date f;
            String str2;
            Wg6.c(ms4, "player");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Jf5 jf5 = this.c;
            FlexibleTextView flexibleTextView = jf5.t;
            Wg6.b(flexibleTextView, "tvLastSync");
            Jl5 jl5 = Jl5.b;
            Date f2 = ms4.f();
            flexibleTextView.setText(jl5.n(f2 != null ? f2.getTime() : 0));
            Integer n = ms4.n();
            int intValue = n != null ? n.intValue() : 0;
            Integer m = ms4.m();
            int intValue2 = intValue - (m != null ? m.intValue() : 0);
            Hr7 hr7 = Hr7.a;
            FlexibleTextView flexibleTextView2 = jf5.v;
            Wg6.b(flexibleTextView2, "tvSteps");
            String c2 = Um5.c(flexibleTextView2.getContext(), 2131886310);
            Wg6.b(c2, "LanguageHelper.getString\u2026rd_FullView_Label__Steps)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue2)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            String d = ThemeManager.l.a().d("primaryText");
            if (d == null) {
                d = "#242424";
            }
            int parseColor = Color.parseColor(d);
            String d2 = ThemeManager.l.a().d("nonBrandNonReachGoal");
            if (d2 == null) {
                d2 = "#a7a7a7";
            }
            int parseColor2 = Color.parseColor(d2);
            if (Wg6.a(ms4.p(), Boolean.TRUE)) {
                FlexibleTextView flexibleTextView3 = jf5.s;
                Wg6.b(flexibleTextView3, "tvFullName");
                View n2 = jf5.n();
                Wg6.b(n2, "root");
                flexibleTextView3.setText(Um5.c(n2.getContext(), 2131887315));
                ImageView imageView = jf5.r;
                Wg6.b(imageView, "ivAvatar");
                Ty4.b(imageView, ms4.g(), "", bi, uy4);
                FlexibleTextView flexibleTextView4 = jf5.u;
                Wg6.b(flexibleTextView4, "tvRank");
                Integer h = ms4.h();
                if (h == null || (str2 = String.valueOf(h.intValue())) == null) {
                    str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                flexibleTextView4.setText(str2);
                jf5.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                jf5.s.setTextColor(parseColor);
                FlexibleTextView flexibleTextView5 = jf5.v;
                Wg6.b(flexibleTextView5, "tvSteps");
                flexibleTextView5.setText(Jl5.b(Jl5.b, String.valueOf(intValue2), format, 0, 4, null));
            } else {
                if (Wg6.a(ms4.d(), PortfolioApp.get.instance().l0())) {
                    FlexibleTextView flexibleTextView6 = jf5.s;
                    Wg6.b(flexibleTextView6, "tvFullName");
                    View n3 = jf5.n();
                    Wg6.b(n3, "root");
                    flexibleTextView6.setText(Um5.c(n3.getContext(), 2131886250));
                    if (this.b != null) {
                        jf5.n().setBackgroundColor(Color.parseColor(this.b));
                    }
                } else {
                    FlexibleTextView flexibleTextView7 = jf5.s;
                    Wg6.b(flexibleTextView7, "tvFullName");
                    flexibleTextView7.setText(Hz4.a.b(ms4.c(), ms4.e(), ms4.i()));
                    if (this.a != null) {
                        jf5.n().setBackgroundColor(Color.parseColor(this.a));
                    }
                }
                ImageView imageView2 = jf5.r;
                Wg6.b(imageView2, "ivAvatar");
                Ty4.b(imageView2, ms4.g(), ms4.e(), bi, uy4);
                if (Wg6.a("left_after_start", ms4.k())) {
                    FlexibleTextView flexibleTextView8 = jf5.u;
                    Wg6.b(flexibleTextView8, "tvRank");
                    flexibleTextView8.setText("");
                    jf5.u.setCompoundDrawablesWithIntrinsicBounds(2131231026, 0, 0, 0);
                    jf5.s.setTextColor(parseColor2);
                    FlexibleTextView flexibleTextView9 = jf5.v;
                    Wg6.b(flexibleTextView9, "tvSteps");
                    flexibleTextView9.setText(Jl5.b.a(String.valueOf(intValue2), format, 0));
                } else {
                    FlexibleTextView flexibleTextView10 = jf5.u;
                    Wg6.b(flexibleTextView10, "tvRank");
                    Integer h2 = ms4.h();
                    if (h2 == null || (str = String.valueOf(h2.intValue())) == null) {
                        str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                    }
                    flexibleTextView10.setText(str);
                    jf5.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    jf5.s.setTextColor(parseColor);
                    FlexibleTextView flexibleTextView11 = jf5.v;
                    Wg6.b(flexibleTextView11, "tvSteps");
                    flexibleTextView11.setText(Jl5.b(Jl5.b, String.valueOf(intValue2), format, 0, 4, null));
                }
            }
            String k = ms4.k();
            if (k != null && k.hashCode() == -1402931637 && k.equals("completed") && i != -1 && (f = ms4.f()) != null) {
                long time = (f.getTime() - j) / ((long) 1000);
                FlexibleTextView flexibleTextView12 = jf5.t;
                Wg6.b(flexibleTextView12, "tvLastSync");
                flexibleTextView12.setText(Jl5.b.s((int) time));
            }
        }
    }

    @DexIgnore
    public Lv4(int i, long j, int i2) {
        this.c = j;
        this.d = i2;
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.a = f;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        return list.get(i) instanceof Ms4;
    }

    @DexIgnore
    public void b(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Ai ai = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof Ms4)) {
            obj = null;
        }
        Ms4 ms4 = (Ms4) obj;
        if (viewHolder instanceof Ai) {
            ai = viewHolder;
        }
        Ai ai2 = ai;
        if (ms4 != null && ai2 != null) {
            ai2.a(ms4, this.c, this.d, this.a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder c(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Jf5 z = Jf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemPlayerListBinding.in\u2026(inflater, parent, false)");
        return new Ai(z);
    }
}
