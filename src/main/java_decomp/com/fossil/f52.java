package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class F52 extends Wk2 implements E52 {
    @DexIgnore
    public F52() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.Wk2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 101:
                y0((GoogleSignInAccount) Xk2.a(parcel, GoogleSignInAccount.CREATOR), (Status) Xk2.a(parcel, Status.CREATOR));
                throw null;
            case 102:
                A((Status) Xk2.a(parcel, Status.CREATOR));
                break;
            case 103:
                W((Status) Xk2.a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
