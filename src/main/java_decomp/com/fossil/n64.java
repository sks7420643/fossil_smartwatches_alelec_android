package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.M64;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N64 implements M64 {
    @DexIgnore
    public static volatile M64 c;
    @DexIgnore
    public /* final */ Fg3 a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new ConcurrentHashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements M64.Ai {
        @DexIgnore
        public Ai(N64 n64, String str) {
        }
    }

    @DexIgnore
    public N64(Fg3 fg3) {
        Rc2.k(fg3);
        this.a = fg3;
    }

    @DexIgnore
    public static M64 d(J64 j64, Context context, Ge4 ge4) {
        Rc2.k(j64);
        Rc2.k(context);
        Rc2.k(ge4);
        Rc2.k(context.getApplicationContext());
        if (c == null) {
            synchronized (N64.class) {
                try {
                    if (c == null) {
                        Bundle bundle = new Bundle(1);
                        if (j64.q()) {
                            ge4.b(H64.class, U64.b, V64.a);
                            bundle.putBoolean("dataCollectionDefaultEnabled", j64.p());
                        }
                        c = new N64(Zs2.b(context, null, null, null, bundle).e());
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return c;
    }

    @DexIgnore
    public static final /* synthetic */ void e(De4 de4) {
        boolean z = ((H64) de4.a()).a;
        synchronized (N64.class) {
            try {
                ((N64) c).a.d(z);
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.M64
    public void a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (O64.a(str) && O64.b(str2, bundle) && O64.d(str, str2, bundle)) {
            O64.e(str, str2, bundle);
            this.a.a(str, str2, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.M64
    public void b(String str, String str2, Object obj) {
        if (O64.a(str) && O64.c(str, str2)) {
            this.a.c(str, str2, obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.M64
    public M64.Ai c(String str, M64.Bi bi) {
        Rc2.k(bi);
        if (!O64.a(str) || f(str)) {
            return null;
        }
        Fg3 fg3 = this.a;
        Object r64 = "fiam".equals(str) ? new R64(fg3, bi) : (CrashDumperPlugin.NAME.equals(str) || "clx".equals(str)) ? new T64(fg3, bi) : null;
        if (r64 == null) {
            return null;
        }
        this.b.put(str, r64);
        return new Ai(this, str);
    }

    @DexIgnore
    public final boolean f(String str) {
        return !str.isEmpty() && this.b.containsKey(str) && this.b.get(str) != null;
    }
}
