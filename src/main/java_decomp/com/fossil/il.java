package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Il extends Lp {
    @DexIgnore
    public ArrayList<Ru1> C; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Ac0> D; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Ru1> E; // = new ArrayList<>(Em7.d0(this.H));
    @DexIgnore
    public int F; // = -1;
    @DexIgnore
    public float G;
    @DexIgnore
    public /* final */ Ru1[] H;

    @DexIgnore
    public Il(K5 k5, I60 i60, Ru1[] ru1Arr) {
        super(k5, i60, Yp.H0, null, false, 24);
        this.H = ru1Arr;
    }

    @DexIgnore
    public static final /* synthetic */ void I(Il il) {
        if (!il.D.isEmpty()) {
            Ac0 remove = il.D.remove(0);
            Wg6.b(remove, "needToUninstallWatchApps.removeAt(0)");
            Lp.h(il, new Ur(il.w, il.x, remove), Xj.b, Jk.b, null, new Vk(il), null, 40, null);
        } else if (il.E.isEmpty()) {
            il.d(1.0f);
            il.l(Nr.a(il.v, null, Zq.b, null, null, 13));
        } else {
            il.J();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.h(this, new Oj(this.w, this.x), new Ki(this), new Yi(this), new Kj(this), null, null, 48, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.I5, Px1.a(this.H));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        Jd0 jd0 = Jd0.J5;
        Object[] array = this.C.toArray(new Ru1[0]);
        if (array != null) {
            return G80.k(E2, jd0, Px1.a((Ox1[]) array));
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void J() {
        int i = this.F + 1;
        this.F = i;
        if (i < this.E.size()) {
            Ru1 ru1 = this.E.get(this.F);
            Wg6.b(ru1, "needToInstallWatchApps[watchAppToInstallIndex]");
            Ru1 ru12 = ru1;
            Lp.h(this, new Pg(this.w, this.x, ru12), new Wg(this, ru12), new Jh(this), new Wh(this), null, null, 48, null);
            return;
        }
        l(Nr.a(this.v, null, Zq.b, null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        T t;
        boolean z;
        Ru1[] ru1Arr = this.H;
        ArrayList arrayList = new ArrayList();
        for (Ru1 ru1 : ru1Arr) {
            Iterator<T> it = this.C.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                T t2 = t;
                if (!Wg6.a(t2.getBundleId(), ru1.getBundleId()) || t2.getPackageCrc() != ru1.getPackageCrc()) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t != null) {
                arrayList.add(ru1);
            }
        }
        Object[] array = arrayList.toArray(new Ru1[0]);
        if (array != null) {
            return (Ru1[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
