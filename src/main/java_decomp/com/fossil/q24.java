package com.fossil;

import java.util.Collection;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Q24<E> extends L24<E> implements Set<E> {
    @DexIgnore
    @Override // com.fossil.L24, com.fossil.L24, com.fossil.O24
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.L24, com.fossil.L24, com.fossil.O24
    public abstract /* bridge */ /* synthetic */ Collection delegate();

    @DexIgnore
    @Override // com.fossil.L24, com.fossil.L24, com.fossil.O24
    public abstract Set<E> delegate();

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || delegate().equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return delegate().hashCode();
    }

    @DexIgnore
    public boolean standardEquals(Object obj) {
        return X44.a(this, obj);
    }

    @DexIgnore
    public int standardHashCode() {
        return X44.b(this);
    }

    @DexIgnore
    @Override // com.fossil.L24
    public boolean standardRemoveAll(Collection<?> collection) {
        I14.l(collection);
        return X44.f(this, collection);
    }
}
