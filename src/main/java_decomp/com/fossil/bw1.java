package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bw1 extends Ox1 implements Parcelable, Serializable, Nx1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Bw1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bw1 createFromParcel(Parcel parcel) {
            return new Bw1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bw1[] newArray(int i) {
            return new Bw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Bw1(Parcel parcel, Qg6 qg6) {
        String readString = parcel.readString();
        if (readString != null) {
            this.b = readString;
            this.c = parcel.readInt();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public Bw1(String str, int i) {
        this.b = str;
        this.c = i;
    }

    @DexIgnore
    public Bw1(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("loc");
            Wg6.b(string, "jsonObject.getString(UIScriptConstant.LOC)");
            this.b = string;
            this.c = jSONObject.getInt("utc");
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("loc", this.b).put("utc", this.c);
        Wg6.b(put, "JSONObject()\n           \u2026ant.UTC, offsetInMinutes)");
        return put;
    }

    @DexIgnore
    @Override // java.lang.Object
    public Bw1 clone() {
        return new Bw1(this.b, this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getLocation() {
        return this.b;
    }

    @DexIgnore
    public final int getOffsetInMinutes() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
    }
}
