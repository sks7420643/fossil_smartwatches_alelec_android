package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M18 {
    @DexIgnore
    public static /* final */ M18 a; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends M18 {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Ci {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.M18.Ci
        public M18 a(A18 a18) {
            return M18.this;
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        M18 a(A18 a18);
    }

    @DexIgnore
    public static Ci k(M18 m18) {
        return new Bi();
    }

    @DexIgnore
    public void a(A18 a18) {
    }

    @DexIgnore
    public void b(A18 a18, IOException iOException) {
    }

    @DexIgnore
    public void c(A18 a18) {
    }

    @DexIgnore
    public void d(A18 a18, InetSocketAddress inetSocketAddress, Proxy proxy, T18 t18) {
    }

    @DexIgnore
    public void e(A18 a18, InetSocketAddress inetSocketAddress, Proxy proxy, T18 t18, IOException iOException) {
    }

    @DexIgnore
    public void f(A18 a18, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    @DexIgnore
    public void g(A18 a18, E18 e18) {
    }

    @DexIgnore
    public void h(A18 a18, E18 e18) {
    }

    @DexIgnore
    public void i(A18 a18, String str, List<InetAddress> list) {
    }

    @DexIgnore
    public void j(A18 a18, String str) {
    }

    @DexIgnore
    public void l(A18 a18, long j) {
    }

    @DexIgnore
    public void m(A18 a18) {
    }

    @DexIgnore
    public void n(A18 a18, V18 v18) {
    }

    @DexIgnore
    public void o(A18 a18) {
    }

    @DexIgnore
    public void p(A18 a18, long j) {
    }

    @DexIgnore
    public void q(A18 a18) {
    }

    @DexIgnore
    public void r(A18 a18, Response response) {
    }

    @DexIgnore
    public void s(A18 a18) {
    }

    @DexIgnore
    public void t(A18 a18, O18 o18) {
    }

    @DexIgnore
    public void u(A18 a18) {
    }
}
