package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y44<K, V> extends T24<K, V> {
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient T24<V, K> inverse;
    @DexIgnore
    public /* final */ transient K singleKey;
    @DexIgnore
    public /* final */ transient V singleValue;

    @DexIgnore
    public Y44(K k, V v) {
        A24.a(k, v);
        this.singleKey = k;
        this.singleValue = v;
    }

    @DexIgnore
    public Y44(K k, V v, T24<V, K> t24) {
        this.singleKey = k;
        this.singleValue = v;
        this.inverse = t24;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean containsKey(Object obj) {
        return this.singleKey.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean containsValue(Object obj) {
        return this.singleValue.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H34<Map.Entry<K, V>> createEntrySet() {
        return H34.of(X34.e(this.singleKey, this.singleValue));
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H34<K> createKeySet() {
        return H34.of(this.singleKey);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34
    public V get(Object obj) {
        if (this.singleKey.equals(obj)) {
            return this.singleValue;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.T24, com.fossil.T24
    public T24<V, K> inverse() {
        T24<V, K> t24 = this.inverse;
        if (t24 != null) {
            return t24;
        }
        Y44 y44 = new Y44(this.singleValue, this.singleKey, this);
        this.inverse = y44;
        return y44;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }
}
