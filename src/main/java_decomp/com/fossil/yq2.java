package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.RemoteException;
import com.fossil.P72;
import com.google.android.gms.location.LocationRequest;
import com.mapped.Yv2;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yq2 {
    @DexIgnore
    public /* final */ Mr2<Uq2> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ Map<P72.Ai<Ga3>, Dr2> d; // = new HashMap();
    @DexIgnore
    public /* final */ Map<P72.Ai<Object>, Cr2> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<P72.Ai<Yv2>, Zq2> f; // = new HashMap();

    @DexIgnore
    public Yq2(Context context, Mr2<Uq2> mr2) {
        this.b = context;
        this.a = mr2;
    }

    @DexIgnore
    public final Location a() throws RemoteException {
        this.a.a();
        return this.a.getService().zza(this.b.getPackageName());
    }

    @DexIgnore
    public final void b() throws RemoteException {
        synchronized (this.d) {
            for (Dr2 dr2 : this.d.values()) {
                if (dr2 != null) {
                    this.a.getService().g2(Kr2.f(dr2, null));
                }
            }
            this.d.clear();
        }
        synchronized (this.f) {
            for (Zq2 zq2 : this.f.values()) {
                if (zq2 != null) {
                    this.a.getService().g2(Kr2.c(zq2, null));
                }
            }
            this.f.clear();
        }
        synchronized (this.e) {
            for (Cr2 cr2 : this.e.values()) {
                if (cr2 != null) {
                    this.a.getService().c1(new Vr2(2, null, cr2.asBinder(), null));
                }
            }
            this.e.clear();
        }
    }

    @DexIgnore
    public final Dr2 c(P72<Ga3> p72) {
        Dr2 dr2;
        synchronized (this.d) {
            dr2 = this.d.get(p72.b());
            if (dr2 == null) {
                dr2 = new Dr2(p72);
            }
            this.d.put(p72.b(), dr2);
        }
        return dr2;
    }

    @DexIgnore
    public final void d(P72.Ai<Ga3> ai, Rq2 rq2) throws RemoteException {
        this.a.a();
        Rc2.l(ai, "Invalid null listener key");
        synchronized (this.d) {
            Dr2 remove = this.d.remove(ai);
            if (remove != null) {
                remove.i();
                this.a.getService().g2(Kr2.f(remove, rq2));
            }
        }
    }

    @DexIgnore
    public final void e(Ir2 ir2, P72<Yv2> p72, Rq2 rq2) throws RemoteException {
        this.a.a();
        this.a.getService().g2(new Kr2(1, ir2, null, null, h(p72).asBinder(), rq2 != null ? rq2.asBinder() : null));
    }

    @DexIgnore
    public final void f(LocationRequest locationRequest, P72<Ga3> p72, Rq2 rq2) throws RemoteException {
        this.a.a();
        this.a.getService().g2(new Kr2(1, Ir2.c(locationRequest), c(p72).asBinder(), null, null, rq2 != null ? rq2.asBinder() : null));
    }

    @DexIgnore
    public final void g(boolean z) throws RemoteException {
        this.a.a();
        this.a.getService().X1(z);
        this.c = z;
    }

    @DexIgnore
    public final Zq2 h(P72<Yv2> p72) {
        Zq2 zq2;
        synchronized (this.f) {
            zq2 = this.f.get(p72.b());
            if (zq2 == null) {
                zq2 = new Zq2(p72);
            }
            this.f.put(p72.b(), zq2);
        }
        return zq2;
    }

    @DexIgnore
    public final void i() throws RemoteException {
        if (this.c) {
            g(false);
        }
    }

    @DexIgnore
    public final void j(P72.Ai<Yv2> ai, Rq2 rq2) throws RemoteException {
        this.a.a();
        Rc2.l(ai, "Invalid null listener key");
        synchronized (this.f) {
            Zq2 remove = this.f.remove(ai);
            if (remove != null) {
                remove.i();
                this.a.getService().g2(Kr2.c(remove, rq2));
            }
        }
    }
}
