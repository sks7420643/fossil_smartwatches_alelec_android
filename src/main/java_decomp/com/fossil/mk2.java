package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mk2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Jk2 b;
    @DexIgnore
    public /* final */ /* synthetic */ Lk2 c;

    @DexIgnore
    public Mk2(Lk2 lk2, Jk2 jk2) {
        this.c = lk2;
        this.b = jk2;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.c.b.handleIntent(this.b.a);
        this.b.a();
    }
}
