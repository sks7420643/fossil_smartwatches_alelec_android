package com.fossil;

import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.V18;
import com.misfit.frameworks.common.enums.Action;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import net.sqlcipher.database.SQLiteDatabase;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z28 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public volatile P28 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public volatile boolean d;

    @DexIgnore
    public Z28(OkHttpClient okHttpClient, boolean z) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public void a() {
        this.d = true;
        P28 p28 = this.b;
        if (p28 != null) {
            p28.b();
        }
    }

    @DexIgnore
    public final X08 b(Q18 q18) {
        C18 c18;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (q18.n()) {
            sSLSocketFactory = this.a.M();
            hostnameVerifier = this.a.u();
            c18 = this.a.i();
        } else {
            c18 = null;
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new X08(q18.m(), q18.z(), this.a.q(), this.a.K(), sSLSocketFactory, hostnameVerifier, c18, this.a.G(), this.a.E(), this.a.C(), this.a.n(), this.a.H());
    }

    @DexIgnore
    public final V18 c(Response response, X18 x18) throws IOException {
        String j;
        Q18 D;
        RequestBody requestBody = null;
        if (response != null) {
            int f = response.f();
            String g = response.G().g();
            if (f == 307 || f == 308) {
                if (!g.equals("GET") && !g.equals("HEAD")) {
                    return null;
                }
            } else if (f == 401) {
                return this.a.e().authenticate(x18, response);
            } else {
                if (f != 503) {
                    if (f != 407) {
                        if (f != 408) {
                            switch (f) {
                                case SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS /* 300 */:
                                case Action.Presenter.NEXT /* 301 */:
                                case Action.Presenter.PREVIOUS /* 302 */:
                                case Action.Presenter.BLACKOUT /* 303 */:
                                    break;
                                default:
                                    return null;
                            }
                        } else if (!this.a.J()) {
                            return null;
                        } else {
                            response.G().a();
                            if ((response.C() == null || response.C().f() != 408) && h(response, 0) <= 0) {
                                return response.G();
                            }
                            return null;
                        }
                    } else if (x18.b().type() == Proxy.Type.HTTP) {
                        return this.a.G().authenticate(x18, response);
                    } else {
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    }
                } else if ((response.C() == null || response.C().f() != 503) && h(response, Integer.MAX_VALUE) == 0) {
                    return response.G();
                } else {
                    return null;
                }
            }
            if (!this.a.s() || (j = response.j("Location")) == null || (D = response.G().j().D(j)) == null) {
                return null;
            }
            if (!D.E().equals(response.G().j().E()) && !this.a.t()) {
                return null;
            }
            V18.Ai h = response.G().h();
            if (V28.b(g)) {
                boolean d2 = V28.d(g);
                if (V28.c(g)) {
                    h.g("GET", null);
                } else {
                    if (d2) {
                        requestBody = response.G().a();
                    }
                    h.g(g, requestBody);
                }
                if (!d2) {
                    h.i("Transfer-Encoding");
                    h.i(HttpHeaders.CONTENT_LENGTH);
                    h.i("Content-Type");
                }
            }
            if (!i(response, D)) {
                h.i("Authorization");
            }
            h.l(D);
            return h.b();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public boolean d() {
        return this.d;
    }

    @DexIgnore
    public final boolean e(IOException iOException, boolean z) {
        boolean z2 = true;
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (!(iOException instanceof InterruptedIOException)) {
            return (!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException);
        }
        if (!(iOException instanceof SocketTimeoutException) || z) {
            z2 = false;
        }
        return z2;
    }

    @DexIgnore
    public final boolean f(IOException iOException, P28 p28, boolean z, V18 v18) {
        p28.q(iOException);
        if (!this.a.J()) {
            return false;
        }
        return (!z || !g(iOException, v18)) && e(iOException, z) && p28.h();
    }

    @DexIgnore
    public final boolean g(IOException iOException, V18 v18) {
        v18.a();
        return iOException instanceof FileNotFoundException;
    }

    @DexIgnore
    public final int h(Response response, int i) {
        String j = response.j("Retry-After");
        if (j == null) {
            return i;
        }
        if (j.matches("\\d+")) {
            return Integer.valueOf(j).intValue();
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public final boolean i(Response response, Q18 q18) {
        Q18 j = response.G().j();
        return j.m().equals(q18.m()) && j.z() == q18.z() && j.E().equals(q18.E());
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        V18 c2 = chain.c();
        W28 w28 = (W28) chain;
        A18 g = w28.g();
        M18 h = w28.h();
        P28 p28 = new P28(this.a.l(), b(c2.j()), g, h, this.c);
        this.b = p28;
        int i = 0;
        Response response = null;
        V18 v18 = c2;
        while (!this.d) {
            try {
                Response j = w28.j(v18, p28, null, null);
                if (response != null) {
                    Response.a B = j.B();
                    Response.a B2 = response.B();
                    B2.b(null);
                    B.m(B2.c());
                    j = B.c();
                }
                try {
                    V18 c3 = c(j, p28.o());
                    if (c3 == null) {
                        p28.k();
                        return j;
                    }
                    B28.g(j.a());
                    int i2 = i + 1;
                    if (i2 <= 20) {
                        c3.a();
                        if (!i(j, c3.j())) {
                            p28.k();
                            p28 = new P28(this.a.l(), b(c3.j()), g, h, this.c);
                            this.b = p28;
                        } else if (p28.c() != null) {
                            throw new IllegalStateException("Closing the body of " + j + " didn't close its backing stream. Bad interceptor?");
                        }
                        i = i2;
                        response = j;
                        v18 = c3;
                    } else {
                        p28.k();
                        throw new ProtocolException("Too many follow-up requests: " + i2);
                    }
                } catch (IOException e) {
                    p28.k();
                    throw e;
                }
            } catch (N28 e2) {
                if (!f(e2.getLastConnectException(), p28, false, v18)) {
                    throw e2.getFirstConnectException();
                }
            } catch (IOException e3) {
                if (!f(e3, p28, !(e3 instanceof C38), v18)) {
                    throw e3;
                }
            } catch (Throwable th) {
                p28.q(null);
                p28.k();
                throw th;
            }
        }
        p28.k();
        throw new IOException("Canceled");
    }

    @DexIgnore
    public void j(Object obj) {
        this.c = obj;
    }
}
