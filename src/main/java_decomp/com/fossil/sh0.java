package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sh0 extends Kh0 {
    @DexIgnore
    public /* final */ WeakReference<Context> b;

    @DexIgnore
    public Sh0(Context context, Resources resources) {
        super(resources);
        this.b = new WeakReference<>(context);
    }

    @DexIgnore
    @Override // com.fossil.Kh0, android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Drawable drawable = super.getDrawable(i);
        Context context = this.b.get();
        if (!(drawable == null || context == null)) {
            Jh0.h().x(context, i, drawable);
        }
        return drawable;
    }
}
