package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ts5;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ss5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public ArrayList<Ts5> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Ys5> b;
    @DexIgnore
    public Bi c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public ImageView b;
        @DexIgnore
        public RecyclerView c;
        @DexIgnore
        public /* final */ /* synthetic */ Ss5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.e();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Bii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.e();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ss5 ss5, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.d = ss5;
            View findViewById = view.findViewById(2131363307);
            if (findViewById != null) {
                TextView textView = (TextView) findViewById;
                textView.setOnClickListener(new Aii(this));
                this.a = textView;
                View findViewById2 = view.findViewById(2131362663);
                if (findViewById2 != null) {
                    ImageView imageView = (ImageView) findViewById2;
                    imageView.setOnClickListener(new Bii(this));
                    this.b = imageView;
                    View findViewById3 = view.findViewById(2131363033);
                    if (findViewById3 != null) {
                        this.c = (RecyclerView) findViewById3;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final ImageView b() {
            return this.b;
        }

        @DexIgnore
        public final RecyclerView c() {
            return this.c;
        }

        @DexIgnore
        public final TextView d() {
            return this.a;
        }

        @DexIgnore
        public final void e() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                ArrayList<Ys5> h = this.d.h();
                if (h != null) {
                    boolean c2 = h.get(adapterPosition).c();
                    ArrayList<Ys5> h2 = this.d.h();
                    if (h2 != null) {
                        h2.get(adapterPosition).d(!c2);
                        this.d.notifyItemChanged(adapterPosition);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Ts5.Ei {
        @DexIgnore
        public /* final */ /* synthetic */ Ss5 a;

        @DexIgnore
        public Ci(Ys5 ys5, Ss5 ss5) {
            this.a = ss5;
        }

        @DexIgnore
        @Override // com.fossil.Ts5.Ei
        public void a(String str, int i, int i2, Object obj, Bundle bundle) {
            Wg6.c(str, "tagName");
            Bi g = this.a.g();
            if (g != null) {
                g.a(str, i, i2, obj, bundle);
            }
        }

        @DexIgnore
        @Override // com.fossil.Ts5.Ei
        public void b(String str, int i, int i2, Object obj, Bundle bundle) {
            Wg6.c(str, "tagName");
            Bi g = this.a.g();
            if (g != null) {
                g.b(str, i, i2, obj, bundle);
            }
        }
    }

    @DexIgnore
    public final Bi g() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        ArrayList<Ys5> arrayList = this.b;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    @DexIgnore
    public final ArrayList<Ys5> h() {
        return this.b;
    }

    @DexIgnore
    public void i(Ai ai, int i) {
        Wg6.c(ai, "holder");
        TextView d = ai.d();
        ArrayList<Ys5> arrayList = this.b;
        if (arrayList != null) {
            d.setText(arrayList.get(i).b());
            RecyclerView c2 = ai.c();
            c2.setLayoutManager(new LinearLayoutManager(c2.getContext()));
            Ts5 ts5 = this.a.get(i);
            Wg6.b(ts5, "listDebugChildAdapter[position]");
            Ts5 ts52 = ts5;
            ts52.l(i);
            c2.setAdapter(ts52);
            ArrayList<Ys5> arrayList2 = this.b;
            if (arrayList2 == null) {
                Wg6.i();
                throw null;
            } else if (arrayList2.get(i).c()) {
                ai.b().setImageResource(2131230996);
                ai.c().setVisibility(0);
            } else {
                ai.b().setImageResource(2131230995);
                ai.c().setVisibility(8);
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ai j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558679, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Ai(this, inflate);
    }

    @DexIgnore
    public final void k(ArrayList<Ys5> arrayList) {
        this.a.clear();
        if (arrayList != null) {
            for (T t : arrayList) {
                ArrayList<Ts5> arrayList2 = this.a;
                Ts5 ts5 = new Ts5();
                ts5.k(new Ci(t, this));
                ts5.j(t.a());
                arrayList2.add(ts5);
            }
        }
        this.b = arrayList;
    }

    @DexIgnore
    public final void l(Bi bi) {
        Wg6.c(bi, "itemClickListener");
        this.c = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        i(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
