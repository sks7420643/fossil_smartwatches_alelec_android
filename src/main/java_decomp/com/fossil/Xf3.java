package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.fossil.Qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xf3 extends Ed3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Ni b;

    @DexIgnore
    public Xf3(Qb3 qb3, Qb3.Ni ni) {
        this.b = ni;
    }

    @DexIgnore
    @Override // com.fossil.Dd3
    public final void onSnapshotReady(Bitmap bitmap) throws RemoteException {
        this.b.onSnapshotReady(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Dd3
    public final void q1(Rg2 rg2) throws RemoteException {
        this.b.onSnapshotReady((Bitmap) Tg2.i(rg2));
    }
}
