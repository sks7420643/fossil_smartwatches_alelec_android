package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J33 extends G33<FieldDescriptorType, Object> {
    @DexIgnore
    public J33(int i) {
        super(i, null);
    }

    @DexIgnore
    @Override // com.fossil.G33
    public final void f() {
        if (!j()) {
            for (int i = 0; i < k(); i++) {
                Map.Entry i2 = i(i);
                if (((V03) i2.getKey()).zzd()) {
                    i2.setValue(Collections.unmodifiableList((List) i2.getValue()));
                }
            }
            for (Map.Entry entry : n()) {
                if (((V03) entry.getKey()).zzd()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.f();
    }
}
