package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ln7 extends Kn7 {
    @DexIgnore
    public static final boolean a(int[] iArr, int[] iArr2) {
        Wg6.c(iArr, "$this$contentEquals");
        Wg6.c(iArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(iArr, iArr2);
    }

    @DexIgnore
    public static final boolean b(byte[] bArr, byte[] bArr2) {
        Wg6.c(bArr, "$this$contentEquals");
        Wg6.c(bArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(bArr, bArr2);
    }

    @DexIgnore
    public static final boolean c(short[] sArr, short[] sArr2) {
        Wg6.c(sArr, "$this$contentEquals");
        Wg6.c(sArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(sArr, sArr2);
    }

    @DexIgnore
    public static final boolean d(long[] jArr, long[] jArr2) {
        Wg6.c(jArr, "$this$contentEquals");
        Wg6.c(jArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(jArr, jArr2);
    }
}
