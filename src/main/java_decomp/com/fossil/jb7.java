package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jb7 {
    @DexIgnore
    public float a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;

    @DexIgnore
    public Jb7(float f, float f2, float f3, float f4) {
        this.a = f;
        this.b = f2;
        this.c = f3;
        this.d = f4;
    }

    @DexIgnore
    public final float a() {
        return this.d;
    }

    @DexIgnore
    public final float b() {
        return this.c;
    }

    @DexIgnore
    public final float c() {
        return this.a;
    }

    @DexIgnore
    public final float d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Jb7) {
                Jb7 jb7 = (Jb7) obj;
                if (!(Float.compare(this.a, jb7.a) == 0 && Float.compare(this.b, jb7.b) == 0 && Float.compare(this.c, jb7.c) == 0 && Float.compare(this.d, jb7.d) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((((Float.floatToIntBits(this.a) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d);
    }

    @DexIgnore
    public String toString() {
        return "WFMetric(scaledX=" + this.a + ", scaledY=" + this.b + ", scaledWidth=" + this.c + ", scaledHeight=" + this.d + ")";
    }
}
