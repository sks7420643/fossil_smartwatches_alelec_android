package com.fossil;

import com.fossil.Ec4;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fc4 implements Ec4 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ File[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;

    @DexIgnore
    public Fc4(File file) {
        this(file, Collections.emptyMap());
    }

    @DexIgnore
    public Fc4(File file, Map<String, String> map) {
        this.a = file;
        this.b = new File[]{file};
        this.c = new HashMap(map);
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.c);
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public String b() {
        String e = e();
        return e.substring(0, e.lastIndexOf(46));
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public File c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public File[] d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public String e() {
        return c().getName();
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public Ec4.Ai getType() {
        return Ec4.Ai.JAVA;
    }

    @DexIgnore
    @Override // com.fossil.Ec4
    public void remove() {
        X74 f = X74.f();
        f.b("Removing report at " + this.a.getPath());
        this.a.delete();
    }
}
