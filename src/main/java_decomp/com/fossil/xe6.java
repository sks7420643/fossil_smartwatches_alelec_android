package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xe6 implements Factory<DashboardActivityPresenter> {
    @DexIgnore
    public static DashboardActivityPresenter a(Ue6 ue6, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        return new DashboardActivityPresenter(ue6, summariesRepository, fitnessDataRepository, userRepository, u04);
    }
}
