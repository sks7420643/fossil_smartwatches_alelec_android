package com.fossil;

import com.facebook.internal.Utility;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I53 implements F53 {
    @DexIgnore
    public static /* final */ Xv2<Long> A;
    @DexIgnore
    public static /* final */ Xv2<Long> B;
    @DexIgnore
    public static /* final */ Xv2<Long> C;
    @DexIgnore
    public static /* final */ Xv2<Long> D;
    @DexIgnore
    public static /* final */ Xv2<Long> E;
    @DexIgnore
    public static /* final */ Xv2<String> F;
    @DexIgnore
    public static /* final */ Xv2<Long> G;
    @DexIgnore
    public static /* final */ Xv2<Long> a;
    @DexIgnore
    public static /* final */ Xv2<Long> b;
    @DexIgnore
    public static /* final */ Xv2<String> c;
    @DexIgnore
    public static /* final */ Xv2<String> d;
    @DexIgnore
    public static /* final */ Xv2<Long> e;
    @DexIgnore
    public static /* final */ Xv2<Long> f;
    @DexIgnore
    public static /* final */ Xv2<Long> g;
    @DexIgnore
    public static /* final */ Xv2<Long> h;
    @DexIgnore
    public static /* final */ Xv2<Long> i;
    @DexIgnore
    public static /* final */ Xv2<Long> j;
    @DexIgnore
    public static /* final */ Xv2<Long> k;
    @DexIgnore
    public static /* final */ Xv2<Long> l;
    @DexIgnore
    public static /* final */ Xv2<Long> m;
    @DexIgnore
    public static /* final */ Xv2<Long> n;
    @DexIgnore
    public static /* final */ Xv2<Long> o;
    @DexIgnore
    public static /* final */ Xv2<Long> p;
    @DexIgnore
    public static /* final */ Xv2<Long> q;
    @DexIgnore
    public static /* final */ Xv2<Long> r;
    @DexIgnore
    public static /* final */ Xv2<Long> s;
    @DexIgnore
    public static /* final */ Xv2<Long> t;
    @DexIgnore
    public static /* final */ Xv2<Long> u;
    @DexIgnore
    public static /* final */ Xv2<Long> v;
    @DexIgnore
    public static /* final */ Xv2<Long> w;
    @DexIgnore
    public static /* final */ Xv2<Long> x;
    @DexIgnore
    public static /* final */ Xv2<Long> y;
    @DexIgnore
    public static /* final */ Xv2<Long> z;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.b("measurement.ad_id_cache_time", ButtonService.CONNECT_TIMEOUT);
        b = hw2.b("measurement.config.cache_time", LogBuilder.MAX_INTERVAL);
        hw2.c("measurement.log_tag", "FA");
        c = hw2.c("measurement.config.url_authority", "app-measurement.com");
        d = hw2.c("measurement.config.url_scheme", Utility.URL_SCHEME);
        e = hw2.b("measurement.upload.debug_upload_interval", 1000);
        f = hw2.b("measurement.lifetimevalue.max_currency_tracked", 4);
        g = hw2.b("measurement.store.max_stored_events_per_app", 100000);
        h = hw2.b("measurement.experiment.max_ids", 50);
        i = hw2.b("measurement.audience.filter_result_max_count", 200);
        j = hw2.b("measurement.alarm_manager.minimum_interval", 60000);
        k = hw2.b("measurement.upload.minimum_delay", 500);
        l = hw2.b("measurement.monitoring.sample_period_millis", LogBuilder.MAX_INTERVAL);
        m = hw2.b("measurement.upload.realtime_upload_interval", ButtonService.CONNECT_TIMEOUT);
        n = hw2.b("measurement.upload.refresh_blacklisted_config_interval", 604800000);
        hw2.b("measurement.config.cache_time.service", 3600000);
        o = hw2.b("measurement.service_client.idle_disconnect_millis", 5000);
        hw2.c("measurement.log_tag.service", "FA-SVC");
        p = hw2.b("measurement.upload.stale_data_deletion_interval", LogBuilder.MAX_INTERVAL);
        q = hw2.b("measurement.upload.backoff_period", 43200000);
        r = hw2.b("measurement.upload.initial_upload_delay_time", 15000);
        s = hw2.b("measurement.upload.interval", 3600000);
        t = hw2.b("measurement.upload.max_bundle_size", 65536);
        u = hw2.b("measurement.upload.max_bundles", 100);
        v = hw2.b("measurement.upload.max_conversions_per_day", 500);
        w = hw2.b("measurement.upload.max_error_events_per_day", 1000);
        x = hw2.b("measurement.upload.max_events_per_bundle", 1000);
        y = hw2.b("measurement.upload.max_events_per_day", 100000);
        z = hw2.b("measurement.upload.max_public_events_per_day", 50000);
        A = hw2.b("measurement.upload.max_queue_time", 2419200000L);
        B = hw2.b("measurement.upload.max_realtime_events_per_day", 10);
        C = hw2.b("measurement.upload.max_batch_size", 65536);
        D = hw2.b("measurement.upload.retry_count", 6);
        E = hw2.b("measurement.upload.retry_time", 1800000);
        F = hw2.c("measurement.upload.url", "https://app-measurement.com/a");
        G = hw2.b("measurement.upload.window_interval", 3600000);
    }
    */

    @DexIgnore
    @Override // com.fossil.F53
    public final long a() {
        return j.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long b() {
        return u.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long c() {
        return q.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long d() {
        return r.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long e() {
        return n.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long f() {
        return p.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long g() {
        return i.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long h() {
        return v.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long i() {
        return y.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long j() {
        return G.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long k() {
        return z.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long l() {
        return w.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long m() {
        return E.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long n() {
        return o.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long o() {
        return x.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long p() {
        return k.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final String q() {
        return F.o();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long r() {
        return C.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long s() {
        return D.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long t() {
        return A.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long u() {
        return s.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long v() {
        return B.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long w() {
        return t.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zza() {
        return a.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zzb() {
        return b.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final String zzc() {
        return c.o();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final String zzd() {
        return d.o();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zze() {
        return e.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zzf() {
        return f.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zzg() {
        return g.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zzh() {
        return h.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zzl() {
        return l.o().longValue();
    }

    @DexIgnore
    @Override // com.fossil.F53
    public final long zzm() {
        return m.o().longValue();
    }
}
