package com.fossil;

import android.location.Location;
import com.fossil.iq4;
import com.fossil.lo4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu5 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ lo4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ DeviceLocation f1209a;

        @DexIgnore
        public c(DeviceLocation deviceLocation) {
            pq7.c(deviceLocation, "deviceLocation");
            this.f1209a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.f1209a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements lo4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fu5 f1210a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(fu5 fu5) {
            this.f1210a = fu5;
        }

        @DexIgnore
        @Override // com.fossil.lo4.b
        public void a(Location location, int i) {
            if (i >= 0) {
                FLogger.INSTANCE.getLocal().d("LoadLocation", "onLocationUpdated OK");
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    if (accuracy <= 500.0f) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("LoadLocation", "onLocationUpdated - accuracy: " + accuracy);
                        try {
                            DeviceLocation deviceLocation = new DeviceLocation(PortfolioApp.h0.c().J(), location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                            mn5.p.a().i().saveDeviceLocation(deviceLocation);
                            this.f1210a.j(new c(deviceLocation));
                        } catch (Exception e) {
                            this.f1210a.i(new b());
                        }
                        this.f1210a.m().p(this);
                        return;
                    }
                    return;
                }
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("LoadLocation", "onLocationUpdated error - error: " + i);
            this.f1210a.m().p(this);
            this.f1210a.i(new b());
        }
    }

    @DexIgnore
    public fu5(lo4 lo4) {
        pq7.c(lo4, "mfLocationService");
        i14.o(lo4, "mfLocationService cannot be null!", new Object[0]);
        pq7.b(lo4, "checkNotNull(mfLocationS\u2026Service cannot be null!\")");
        this.d = lo4;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "LoadLocation";
    }

    @DexIgnore
    public final lo4 m() {
        return this.d;
    }

    @DexIgnore
    /* renamed from: n */
    public Object k(a aVar, qn7<Object> qn7) {
        FLogger.INSTANCE.getLocal().d("LoadLocation", "running UseCase");
        this.d.k(new d(this));
        return new Object();
    }
}
