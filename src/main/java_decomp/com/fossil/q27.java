package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.mapped.An4;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.security.KeyPair;
import java.security.KeyStore;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q27 extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public /* final */ An4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Wh5 b;

        @DexIgnore
        public Ai(String str, Wh5 wh5) {
            Wg6.c(str, "aliasName");
            Wg6.c(wh5, "scope");
            this.a = str;
            this.b = wh5;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final Wh5 b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Bi(int i, String str) {
            Wg6.c(str, "errorMessage");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ci(String str) {
            Wg6.c(str, "decryptedValue");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore
    public Q27(An4 an4) {
        Wg6.c(an4, "mSharedPreferencesManager");
        this.d = an4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "DecryptValueKeyStoreUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    public Object m(Ai ai, Xe6<Object> xe6) {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                An4 an4 = this.d;
                if (ai != null) {
                    String n = an4.n(ai.a(), ai.b());
                    String o = this.d.o(ai.a(), ai.b());
                    KeyStore.SecretKeyEntry f = Z37.b.f(ai.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(2, f.getSecretKey(), new GCMParameterSpec(128, Base64.decode(n, 0)));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DecryptValueKeyStoreUseCase", "Start decrypt with iv " + n + " encryptedValue " + o);
                    byte[] doFinal = instance.doFinal(Base64.decode(o, 0));
                    Wg6.b(doFinal, "decryptedByteArray");
                    String str = new String(doFinal, Et7.a);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str);
                    j(new Ci(str));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                An4 an42 = this.d;
                if (ai != null) {
                    String o2 = an42.o(ai.a(), ai.b());
                    KeyPair d2 = Z37.b.d(ai.a());
                    if (d2 == null) {
                        d2 = Z37.b.b(ai.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(2, d2.getPrivate());
                    byte[] doFinal2 = instance2.doFinal(Base64.decode(o2, 0));
                    Wg6.b(doFinal2, "decryptedByte");
                    String str2 = new String(doFinal2, Et7.a);
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str2);
                    j(new Ci(str2));
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("DecryptValueKeyStoreUseCase", "Exception when decrypt value " + e);
            i(new Bi(600, ""));
        }
        return new Object();
    }
}
