package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.f17;
import com.fossil.nk5;
import com.fossil.t47;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b17 extends qv5 implements v17, t47.g {
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public u17 h;
    @DexIgnore
    public g37<j65> i;
    @DexIgnore
    public qb3 j;
    @DexIgnore
    public View k;
    @DexIgnore
    public Bitmap l;
    @DexIgnore
    public wa1 m;
    @DexIgnore
    public /* final */ String s; // = qn5.l.a().d("primaryText");
    @DexIgnore
    public /* final */ String t; // = qn5.l.a().d("success");
    @DexIgnore
    public /* final */ String u; // = qn5.l.a().d("secondaryText");
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final b17 a() {
            return new b17();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b17 b;

        @DexIgnore
        public b(b17 b17) {
            this.b = b17;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.requireActivity().finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sb3 {
        @DexIgnore
        public /* final */ /* synthetic */ b17 b;

        @DexIgnore
        public c(b17 b17) {
            this.b = b17;
        }

        @DexIgnore
        @Override // com.fossil.sb3
        public final void onMapReady(qb3 qb3) {
            this.b.j = qb3;
            pq7.b(qb3, "googleMap");
            xb3 l = qb3.l();
            pq7.b(l, "googleMap.uiSettings");
            l.j(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ b17 f381a;
        @DexIgnore
        public /* final */ /* synthetic */ j65 b;

        @DexIgnore
        public d(b17 b17, j65 j65) {
            this.f381a = b17;
            this.b = j65;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            p47.l(this.b.C);
            ConstraintLayout constraintLayout = this.b.r;
            pq7.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(z ? 0 : 4);
            b17.K6(this.f381a).n(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ j65 f382a;
        @DexIgnore
        public /* final */ /* synthetic */ b17 b;

        @DexIgnore
        public e(j65 j65, b17 b17, f17.d dVar) {
            this.f382a = j65;
            this.b = b17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            b17.L6(this.b).t(str2).F0(this.f382a.A);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ View f383a;
        @DexIgnore
        public /* final */ /* synthetic */ b17 b;

        @DexIgnore
        public f(View view, String str, b17 b17, f17.d dVar) {
            this.f383a = view;
            this.b = b17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            if (this.b.isActive()) {
                View findViewById = this.f383a.findViewById(2131362616);
                if (findViewById != null) {
                    ((ImageView) findViewById).setImageBitmap(vk5.c(str2));
                    int dimensionPixelSize = PortfolioApp.h0.c().getResources().getDimensionPixelSize(2131165442);
                    this.f383a.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
                    View view = this.f383a;
                    view.layout(0, 0, view.getMeasuredWidth(), this.f383a.getMeasuredHeight());
                    this.b.l = vk5.h(this.f383a);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.ImageView");
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ u17 K6(b17 b17) {
        u17 u17 = b17.h;
        if (u17 != null) {
            return u17;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ wa1 L6(b17 b17) {
        wa1 wa1 = b17.m;
        if (wa1 != null) {
            return wa1;
        }
        pq7.n("mRequestManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void E1(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        j65 O6 = O6();
        if (O6 != null) {
            FlexibleTextView flexibleTextView = O6.t;
            pq7.b(flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(8);
            if (d2 != null && d3 != null) {
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                qb3 qb3 = this.j;
                if (qb3 != null) {
                    qb3.o(pb3.d(latLng, 13.0f));
                    qb3.e(pb3.j(18.0f));
                    qb3.g();
                    Bitmap bitmap = this.l;
                    if (bitmap != null) {
                        le3 le3 = new le3();
                        le3.B0(um5.c(PortfolioApp.h0.c(), 2131887151));
                        le3.u0(ae3.d(bitmap));
                        le3.z0(latLng);
                        qb3.b(le3);
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void I2(long j2) {
        FlexibleTextView flexibleTextView;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showTime: time = " + j2);
        g37<j65> g37 = this.i;
        if (g37 != null) {
            j65 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.x) != null) {
                flexibleTextView.setText(DateUtils.getRelativeTimeSpanString(j2));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void L2(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "address");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showAddress: address = " + str);
        g37<j65> g37 = this.i;
        if (g37 != null) {
            j65 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.u) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final j65 O6() {
        g37<j65> g37 = this.i;
        if (g37 != null) {
            return g37.a();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(u17 u17) {
        pq7.c(u17, "presenter");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "setPresenter: presenter = " + u17);
        this.h = u17;
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void W0(int i2) {
        g37<j65> g37 = this.i;
        if (g37 != null) {
            j65 a2 = g37.a();
            if (a2 != null) {
                float g = nk5.o.g(i2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDeviceFragment", "showProximity - rssi: " + i2 + ", distanceInFt: " + g);
                if (g >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && g <= 5.0f) {
                    FlexibleTextView flexibleTextView = a2.E;
                    pq7.b(flexibleTextView, "binding.tvLocationStatus");
                    flexibleTextView.setText(um5.c(getContext(), 2131887155));
                    FlexibleTextView flexibleTextView2 = a2.E;
                    pq7.b(flexibleTextView2, "binding.tvLocationStatus");
                    flexibleTextView2.setEnabled(true);
                    String str = this.t;
                    if (str != null) {
                        a2.E.setTextColor(Color.parseColor(str));
                        a2.E.setBackgroundColor(Color.parseColor(str));
                        FlexibleTextView flexibleTextView3 = a2.E;
                        pq7.b(flexibleTextView3, "binding.tvLocationStatus");
                        Drawable background = flexibleTextView3.getBackground();
                        pq7.b(background, "binding.tvLocationStatus.background");
                        background.setAlpha(128);
                    }
                } else if (g >= 5.0f && g <= 10.0f) {
                    FlexibleTextView flexibleTextView4 = a2.E;
                    pq7.b(flexibleTextView4, "binding.tvLocationStatus");
                    flexibleTextView4.setText(um5.c(getContext(), 2131887153));
                    FlexibleTextView flexibleTextView5 = a2.E;
                    pq7.b(flexibleTextView5, "binding.tvLocationStatus");
                    flexibleTextView5.setEnabled(true);
                    String str2 = this.t;
                    if (str2 != null) {
                        a2.E.setTextColor(Color.parseColor(str2));
                        a2.E.setBackgroundColor(Color.parseColor(str2));
                        FlexibleTextView flexibleTextView6 = a2.E;
                        pq7.b(flexibleTextView6, "binding.tvLocationStatus");
                        Drawable background2 = flexibleTextView6.getBackground();
                        pq7.b(background2, "binding.tvLocationStatus.background");
                        background2.setAlpha(128);
                    }
                } else if (g < 10.0f || g > 30.0f) {
                    FlexibleTextView flexibleTextView7 = a2.E;
                    pq7.b(flexibleTextView7, "binding.tvLocationStatus");
                    flexibleTextView7.setText(um5.c(getContext(), 2131887152));
                    FlexibleTextView flexibleTextView8 = a2.E;
                    pq7.b(flexibleTextView8, "binding.tvLocationStatus");
                    flexibleTextView8.setEnabled(false);
                    String str3 = this.u;
                    if (str3 != null) {
                        a2.E.setTextColor(Color.parseColor(str3));
                        a2.E.setBackgroundColor(Color.parseColor(str3));
                        FlexibleTextView flexibleTextView9 = a2.E;
                        pq7.b(flexibleTextView9, "binding.tvLocationStatus");
                        Drawable background3 = flexibleTextView9.getBackground();
                        pq7.b(background3, "binding.tvLocationStatus.background");
                        background3.setAlpha(128);
                    }
                } else {
                    FlexibleTextView flexibleTextView10 = a2.E;
                    pq7.b(flexibleTextView10, "binding.tvLocationStatus");
                    flexibleTextView10.setText(um5.c(getContext(), 2131887154));
                    FlexibleTextView flexibleTextView11 = a2.E;
                    pq7.b(flexibleTextView11, "binding.tvLocationStatus");
                    flexibleTextView11.setEnabled(true);
                    String str4 = this.t;
                    if (str4 != null) {
                        a2.E.setTextColor(Color.parseColor(str4));
                        a2.E.setBackgroundColor(Color.parseColor(str4));
                        FlexibleTextView flexibleTextView12 = a2.E;
                        pq7.b(flexibleTextView12, "binding.tvLocationStatus");
                        Drawable background4 = flexibleTextView12.getBackground();
                        pq7.b(background4, "binding.tvLocationStatus.background");
                        background4.setAlpha(128);
                    }
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void f3(boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocationEnable: enable = " + z + ", needWarning = " + z2);
        g37<j65> g37 = this.i;
        if (g37 != null) {
            j65 a2 = g37.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.C;
                pq7.b(flexibleSwitchCompat, "binding.swLocate");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void j1() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationPermissionError");
        j65 O6 = O6();
        if (O6 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = O6.C;
            pq7.b(flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = O6.r;
            pq7.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            u17 u17 = this.h;
            if (u17 != null) {
                u17.o(false);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void o(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showErrorDialog");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
        j65 O6 = O6();
        if (O6 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = O6.C;
            pq7.b(flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = O6.r;
            pq7.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            u17 u17 = this.h;
            if (u17 != null) {
                u17.o(false);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        wa1 v2 = oa1.v(this);
        pq7.b(v2, "Glide.with(this)");
        this.m = v2;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String d2;
        pq7.c(layoutInflater, "inflater");
        j65 j65 = (j65) aq0.f(layoutInflater, 2131558555, viewGroup, false, A6());
        View inflate = layoutInflater.inflate(2131558736, (ViewGroup) null);
        this.k = inflate;
        if (inflate != null) {
            int dimensionPixelSize = PortfolioApp.h0.c().getResources().getDimensionPixelSize(2131165442);
            inflate.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
            inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
            this.l = vk5.h(inflate);
        }
        j65.z.setOnClickListener(new b(this));
        Fragment Y = getChildFragmentManager().Y(2131362251);
        if (Y != null) {
            ((SupportMapFragment) Y).v6(new c(this));
            j65.C.setOnCheckedChangeListener(new d(this, j65));
            ConstraintLayout constraintLayout = j65.q;
            if (!(constraintLayout == null || (d2 = qn5.l.a().d("nonBrandSurface")) == null)) {
                constraintLayout.setBackgroundColor(Color.parseColor(d2));
            }
            this.i = new g37<>(this, j65);
            pq7.b(j65, "binding");
            return j65.n();
        }
        throw new il7("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onPause");
        super.onPause();
        u17 u17 = this.h;
        if (u17 != null) {
            u17.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onResume");
        super.onResume();
        u17 u17 = this.h;
        if (u17 != null) {
            u17.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void r6() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationNotFound");
        j65 O6 = O6();
        if (O6 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = O6.C;
            pq7.b(flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setEnabled(false);
            FlexibleSwitchCompat flexibleSwitchCompat2 = O6.C;
            pq7.b(flexibleSwitchCompat2, "binding.swLocate");
            flexibleSwitchCompat2.setChecked(false);
            String str = this.u;
            if (str != null) {
                O6.y.setTextColor(Color.parseColor(str));
            }
            ConstraintLayout constraintLayout = O6.r;
            pq7.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            FlexibleTextView flexibleTextView = O6.t;
            pq7.b(flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(0);
            u17 u17 = this.h;
            if (u17 != null) {
                u17.o(false);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.v17
    public void w3(f17.d dVar) {
        pq7.c(dVar, "watchSetting");
        if (isActive()) {
            g37<j65> g37 = this.i;
            if (g37 != null) {
                j65 a2 = g37.a();
                if (a2 != null) {
                    String deviceId = dVar.a().getDeviceId();
                    FlexibleTextView flexibleTextView = a2.D;
                    pq7.b(flexibleTextView, "it.tvDeviceName");
                    flexibleTextView.setText(dVar.b());
                    a2.D.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    FlexibleTextView flexibleTextView2 = a2.t;
                    pq7.b(flexibleTextView2, "it.ftvError");
                    hr7 hr7 = hr7.f1520a;
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131887158);
                    pq7.b(c2, "LanguageHelper.getString\u2026chNameHasntBeenConnected)");
                    String format = String.format(c2, Arrays.copyOf(new Object[]{dVar.b()}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView2.setText(format);
                    boolean d2 = dVar.d();
                    if (d2) {
                        if (dVar.e()) {
                            String str = this.t;
                            if (str != null) {
                                a2.D.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView3 = a2.D;
                            pq7.b(flexibleTextView3, "it.tvDeviceName");
                            flexibleTextView3.setAlpha(1.0f);
                            a2.D.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, gl0.f(PortfolioApp.h0.c(), 2131231075), (Drawable) null);
                        } else {
                            String str2 = this.s;
                            if (str2 != null) {
                                a2.D.setTextColor(Color.parseColor(str2));
                            }
                            FlexibleTextView flexibleTextView4 = a2.D;
                            pq7.b(flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView5 = a2.E;
                            pq7.b(flexibleTextView5, "it.tvLocationStatus");
                            flexibleTextView5.setText(um5.c(getContext(), 2131887152));
                            FlexibleTextView flexibleTextView6 = a2.E;
                            pq7.b(flexibleTextView6, "it.tvLocationStatus");
                            flexibleTextView6.setEnabled(false);
                            String str3 = this.u;
                            if (str3 != null) {
                                a2.E.setTextColor(Color.parseColor(str3));
                                a2.E.setBackgroundColor(Color.parseColor(str3));
                                FlexibleTextView flexibleTextView7 = a2.E;
                                pq7.b(flexibleTextView7, "it.tvLocationStatus");
                                Drawable background = flexibleTextView7.getBackground();
                                pq7.b(background, "it.tvLocationStatus.background");
                                background.setAlpha(128);
                            }
                        }
                    } else if (!d2) {
                        String str4 = this.s;
                        if (str4 != null) {
                            a2.D.setTextColor(Color.parseColor(str4));
                        }
                        FlexibleTextView flexibleTextView8 = a2.D;
                        pq7.b(flexibleTextView8, "it.tvDeviceName");
                        flexibleTextView8.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView9 = a2.E;
                        pq7.b(flexibleTextView9, "it.tvLocationStatus");
                        flexibleTextView9.setText(um5.c(getContext(), 2131887152));
                        FlexibleTextView flexibleTextView10 = a2.E;
                        pq7.b(flexibleTextView10, "it.tvLocationStatus");
                        flexibleTextView10.setEnabled(false);
                        String str5 = this.u;
                        if (str5 != null) {
                            a2.E.setTextColor(Color.parseColor(str5));
                            a2.E.setBackgroundColor(Color.parseColor(str5));
                            FlexibleTextView flexibleTextView11 = a2.E;
                            pq7.b(flexibleTextView11, "it.tvLocationStatus");
                            Drawable background2 = flexibleTextView11.getBackground();
                            pq7.b(background2, "it.tvLocationStatus.background");
                            background2.setAlpha(128);
                        }
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(deviceId).setSerialPrefix(nk5.o.m(deviceId)).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.A;
                    pq7.b(imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, nk5.o.i(deviceId, nk5.b.SMALL)).setImageCallback(new e(a2, this, dVar)).download();
                    View view = this.k;
                    if (view != null) {
                        new CloudImageHelper().with().setSerialNumber(deviceId).setSerialPrefix(nk5.o.m(deviceId)).setType(Constants.DeviceType.TYPE_LARGE).setImageCallback(new f(view, deviceId, this, dVar)).download();
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }
}
