package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mt6 implements Factory<CustomizeTextViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Mt6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Mt6 a(Provider<ThemeRepository> provider) {
        return new Mt6(provider);
    }

    @DexIgnore
    public static CustomizeTextViewModel c(ThemeRepository themeRepository) {
        return new CustomizeTextViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeTextViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
