package com.fossil;

import com.fossil.T41;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V41 {
    @DexIgnore
    public T41<?> a;

    @DexIgnore
    public V41(T41<?> t41) {
        this.a = t41;
    }

    @DexIgnore
    public void a() {
        this.a = null;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        T41.Gi o;
        try {
            T41<?> t41 = this.a;
            if (!(t41 == null || (o = T41.o()) == null)) {
                o.a(t41, new W41(t41.m()));
            }
        } finally {
            super.finalize();
        }
    }
}
