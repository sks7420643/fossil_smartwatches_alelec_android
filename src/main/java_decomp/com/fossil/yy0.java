package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.view.View;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yy0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends AnimatorListenerAdapter implements Transition.f {
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public int[] e;
        @DexIgnore
        public float f;
        @DexIgnore
        public float g;
        @DexIgnore
        public /* final */ float h;
        @DexIgnore
        public /* final */ float i;

        @DexIgnore
        public Ai(View view, View view2, int i2, int i3, float f2, float f3) {
            this.b = view;
            this.a = view2;
            this.c = i2 - Math.round(view.getTranslationX());
            this.d = i3 - Math.round(this.b.getTranslationY());
            this.h = f2;
            this.i = f3;
            int[] iArr = (int[]) this.a.getTag(Ny0.transition_position);
            this.e = iArr;
            if (iArr != null) {
                this.a.setTag(Ny0.transition_position, null);
            }
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void a(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void b(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void c(Transition transition) {
            this.b.setTranslationX(this.h);
            this.b.setTranslationY(this.i);
            transition.d0(this);
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void d(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void e(Transition transition) {
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            if (this.e == null) {
                this.e = new int[2];
            }
            this.e[0] = Math.round(((float) this.c) + this.b.getTranslationX());
            this.e[1] = Math.round(((float) this.d) + this.b.getTranslationY());
            this.a.setTag(Ny0.transition_position, this.e);
        }

        @DexIgnore
        public void onAnimationPause(Animator animator) {
            this.f = this.b.getTranslationX();
            this.g = this.b.getTranslationY();
            this.b.setTranslationX(this.h);
            this.b.setTranslationY(this.i);
        }

        @DexIgnore
        public void onAnimationResume(Animator animator) {
            this.b.setTranslationX(this.f);
            this.b.setTranslationY(this.g);
        }
    }

    @DexIgnore
    public static Animator a(View view, Wy0 wy0, int i, int i2, float f, float f2, float f3, float f4, TimeInterpolator timeInterpolator, Transition transition) {
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        int[] iArr = (int[]) wy0.b.getTag(Ny0.transition_position);
        if (iArr != null) {
            f = ((float) (iArr[0] - i)) + translationX;
            f2 = ((float) (iArr[1] - i2)) + translationY;
        }
        int round = Math.round(f - translationX);
        int round2 = Math.round(f2 - translationY);
        view.setTranslationX(f);
        view.setTranslationY(f2);
        if (f == f3 && f2 == f4) {
            return null;
        }
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(view, PropertyValuesHolder.ofFloat(View.TRANSLATION_X, f, f3), PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, f2, f4));
        Ai ai = new Ai(view, wy0.b, i + round, round2 + i2, translationX, translationY);
        transition.d(ai);
        ofPropertyValuesHolder.addListener(ai);
        Yx0.a(ofPropertyValuesHolder, ai);
        ofPropertyValuesHolder.setInterpolator(timeInterpolator);
        return ofPropertyValuesHolder;
    }
}
