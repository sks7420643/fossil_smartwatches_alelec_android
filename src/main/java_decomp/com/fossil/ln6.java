package com.fossil;

import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ln6 implements Factory<WorkoutEditViewModel> {
    @DexIgnore
    public /* final */ Provider<WorkoutSessionRepository> a;

    @DexIgnore
    public Ln6(Provider<WorkoutSessionRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Ln6 a(Provider<WorkoutSessionRepository> provider) {
        return new Ln6(provider);
    }

    @DexIgnore
    public static WorkoutEditViewModel c(WorkoutSessionRepository workoutSessionRepository) {
        return new WorkoutEditViewModel(workoutSessionRepository);
    }

    @DexIgnore
    public WorkoutEditViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
