package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zp4 implements Factory<Rl5> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;

    @DexIgnore
    public Zp4(Uo4 uo4, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static Zp4 a(Uo4 uo4, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return new Zp4(uo4, provider, provider2);
    }

    @DexIgnore
    public static Rl5 c(Uo4 uo4, DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        Rl5 G = uo4.G(deviceRepository, portfolioApp);
        Lk7.c(G, "Cannot return null from a non-@Nullable @Provides method");
        return G;
    }

    @DexIgnore
    public Rl5 b() {
        return c(this.a, this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
