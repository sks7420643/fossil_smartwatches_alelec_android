package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.MetaData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends TypeToken<MetaData> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends TypeToken<MetaData> {
    }

    @DexIgnore
    public final String a(Data data) {
        Wg6.c(data, "data");
        String u = new Gson().u(data, new Ai().getType());
        Wg6.b(u, "Gson().toJson(data, type)");
        return u;
    }

    @DexIgnore
    public final Data b(String str) {
        Wg6.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Data) new Gson().l(str, new Bi().getType());
    }

    @DexIgnore
    public final MetaData c(String str) {
        Wg6.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (MetaData) new Gson().l(str, new Ci().getType());
    }

    @DexIgnore
    public final String d(MetaData metaData) {
        Wg6.c(metaData, "metaData");
        String u = new Gson().u(metaData, new Di().getType());
        Wg6.b(u, "Gson().toJson(metaData, type)");
        return u;
    }
}
