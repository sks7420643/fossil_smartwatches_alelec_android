package com.fossil;

import android.os.Build;
import androidx.work.ListenableWorker;
import androidx.work.OverwritingInputMerger;
import com.fossil.H11;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z01 extends H11 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends H11.Ai<Ai, Z01> {
        @DexIgnore
        public Ai(Class<? extends ListenableWorker> cls) {
            super(cls);
            this.c.d = OverwritingInputMerger.class.getName();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.H11' to match base method */
        @Override // com.fossil.H11.Ai
        public /* bridge */ /* synthetic */ Z01 c() {
            return f();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.H11$Ai' to match base method */
        @Override // com.fossil.H11.Ai
        public /* bridge */ /* synthetic */ Ai d() {
            g();
            return this;
        }

        @DexIgnore
        public Z01 f() {
            if (!this.a || Build.VERSION.SDK_INT < 23 || !this.c.j.h()) {
                O31 o31 = this.c;
                if (!o31.q || Build.VERSION.SDK_INT < 23 || !o31.j.h()) {
                    return new Z01(this);
                }
                throw new IllegalArgumentException("Cannot run in foreground with an idle mode constraint");
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
        }

        @DexIgnore
        public Ai g() {
            return this;
        }
    }

    @DexIgnore
    public Z01(Ai ai) {
        super(ai.b, ai.c, ai.d);
    }

    @DexIgnore
    public static Z01 e(Class<? extends ListenableWorker> cls) {
        return (Z01) new Ai(cls).b();
    }
}
