package com.fossil;

import android.os.Bundle;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R93 extends X93 {
    @DexIgnore
    public /* final */ AtomicReference<Bundle> b; // = new AtomicReference<>();
    @DexIgnore
    public boolean c;

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r0 = r7.get("r");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> T e(android.os.Bundle r7, java.lang.Class<T> r8) {
        /*
            if (r7 == 0) goto L_0x0037
            java.lang.String r0 = "r"
            java.lang.Object r0 = r7.get(r0)
            if (r0 == 0) goto L_0x0037
            java.lang.Object r0 = r8.cast(r0)     // Catch:{ ClassCastException -> 0x000f }
        L_0x000e:
            return r0
        L_0x000f:
            r1 = move-exception
            java.lang.String r2 = r8.getCanonicalName()
            java.lang.Class r0 = r0.getClass()
            java.lang.String r0 = r0.getCanonicalName()
            java.lang.String r3 = "AM"
            java.lang.String r4 = "Unexpected object type. Expected, Received"
            java.lang.String r5 = ": %s, %s"
            java.lang.String r4 = r4.concat(r5)
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            r5[r6] = r2
            r2 = 1
            r5[r2] = r0
            java.lang.String r0 = java.lang.String.format(r4, r5)
            android.util.Log.w(r3, r0, r1)
            throw r1
        L_0x0037:
            r0 = 0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.R93.e(android.os.Bundle, java.lang.Class):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.U93
    public final void c(Bundle bundle) {
        synchronized (this.b) {
            try {
                this.b.set(bundle);
                this.c = true;
                this.b.notify();
            } catch (Throwable th) {
                this.b.notify();
                throw th;
            }
        }
    }

    @DexIgnore
    public final String i(long j) {
        return (String) e(n(j), String.class);
    }

    @DexIgnore
    public final Bundle n(long j) {
        Bundle bundle;
        synchronized (this.b) {
            if (!this.c) {
                try {
                    this.b.wait(j);
                } catch (InterruptedException e) {
                    return null;
                }
            }
            bundle = this.b.get();
        }
        return bundle;
    }
}
