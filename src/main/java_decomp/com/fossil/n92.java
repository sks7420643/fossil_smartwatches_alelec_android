package com.fossil;

import android.util.Log;
import com.fossil.L72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N92 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Z52 b;
    @DexIgnore
    public /* final */ /* synthetic */ L72.Ci c;

    @DexIgnore
    public N92(L72.Ci ci, Z52 z52) {
        this.c = ci;
        this.b = z52;
    }

    @DexIgnore
    public final void run() {
        L72.Ai ai = (L72.Ai) L72.this.i.get(this.c.b);
        if (ai != null) {
            if (this.b.A()) {
                this.c.e = true;
                if (this.c.a.v()) {
                    this.c.g();
                    return;
                }
                try {
                    this.c.a.i(null, this.c.a.h());
                } catch (SecurityException e) {
                    Log.e("GoogleApiManager", "Failed to get service from broker. ", e);
                    ai.n(new Z52(10));
                }
            } else {
                ai.n(this.b);
            }
        }
    }
}
