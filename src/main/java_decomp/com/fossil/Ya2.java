package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.M62;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya2 implements S92 {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ T82 c;
    @DexIgnore
    public /* final */ Looper d;
    @DexIgnore
    public /* final */ C92 e;
    @DexIgnore
    public /* final */ C92 f;
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, C92> g;
    @DexIgnore
    public /* final */ Set<T72> h; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ M62.Fi i;
    @DexIgnore
    public Bundle j;
    @DexIgnore
    public Z52 k; // = null;
    @DexIgnore
    public Z52 l; // = null;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public /* final */ Lock s;
    @DexIgnore
    public int t; // = 0;

    @DexIgnore
    public Ya2(Context context, T82 t82, Lock lock, Looper looper, D62 d62, Map<M62.Ci<?>, M62.Fi> map, Map<M62.Ci<?>, M62.Fi> map2, Ac2 ac2, M62.Ai<? extends Ys3, Gs3> ai, M62.Fi fi, ArrayList<Wa2> arrayList, ArrayList<Wa2> arrayList2, Map<M62<?>, Boolean> map3, Map<M62<?>, Boolean> map4) {
        this.b = context;
        this.c = t82;
        this.s = lock;
        this.d = looper;
        this.i = fi;
        this.e = new C92(context, t82, lock, looper, d62, map2, null, map4, null, arrayList2, new Ab2(this, null));
        this.f = new C92(context, this.c, lock, looper, d62, map, ac2, map3, ai, arrayList, new Za2(this, null));
        Zi0 zi0 = new Zi0();
        for (M62.Ci<?> ci : map2.keySet()) {
            zi0.put(ci, this.e);
        }
        for (M62.Ci<?> ci2 : map.keySet()) {
            zi0.put(ci2, this.f);
        }
        this.g = Collections.unmodifiableMap(zi0);
    }

    @DexIgnore
    public static Ya2 i(Context context, T82 t82, Lock lock, Looper looper, D62 d62, Map<M62.Ci<?>, M62.Fi> map, Ac2 ac2, Map<M62<?>, Boolean> map2, M62.Ai<? extends Ys3, Gs3> ai, ArrayList<Wa2> arrayList) {
        Zi0 zi0 = new Zi0();
        Zi0 zi02 = new Zi0();
        M62.Fi fi = null;
        for (Map.Entry<M62.Ci<?>, M62.Fi> entry : map.entrySet()) {
            M62.Fi value = entry.getValue();
            if (value.g()) {
                fi = value;
            }
            if (value.v()) {
                zi0.put(entry.getKey(), value);
            } else {
                zi02.put(entry.getKey(), value);
            }
        }
        Rc2.o(!zi0.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        Zi0 zi03 = new Zi0();
        Zi0 zi04 = new Zi0();
        for (M62<?> m62 : map2.keySet()) {
            M62.Ci<?> a2 = m62.a();
            if (zi0.containsKey(a2)) {
                zi03.put(m62, map2.get(m62));
            } else if (zi02.containsKey(a2)) {
                zi04.put(m62, map2.get(m62));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Wa2 wa2 = arrayList.get(i2);
            i2++;
            Wa2 wa22 = wa2;
            if (zi03.containsKey(wa22.b)) {
                arrayList2.add(wa22);
            } else if (zi04.containsKey(wa22.b)) {
                arrayList3.add(wa22);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new Ya2(context, t82, lock, looper, d62, zi0, zi02, ac2, ai, fi, arrayList2, arrayList3, zi03, zi04);
    }

    @DexIgnore
    public static boolean x(Z52 z52) {
        return z52 != null && z52.A();
    }

    @DexIgnore
    public final void C() {
        Z52 z52;
        if (x(this.k)) {
            if (x(this.l) || E()) {
                int i2 = this.t;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.t = 0;
                        return;
                    }
                    this.c.b(this.j);
                }
                D();
                this.t = 0;
                return;
            }
            Z52 z522 = this.l;
            if (z522 == null) {
                return;
            }
            if (this.t == 1) {
                D();
                return;
            }
            q(z522);
            this.e.a();
        } else if (this.k == null || !x(this.l)) {
            Z52 z523 = this.k;
            if (z523 != null && (z52 = this.l) != null) {
                if (this.f.s >= this.e.s) {
                    z52 = z523;
                }
                q(z52);
            }
        } else {
            this.f.a();
            q(this.k);
        }
    }

    @DexIgnore
    public final void D() {
        for (T72 t72 : this.h) {
            t72.onComplete();
        }
        this.h.clear();
    }

    @DexIgnore
    public final boolean E() {
        Z52 z52 = this.l;
        return z52 != null && z52.c() == 4;
    }

    @DexIgnore
    public final PendingIntent F() {
        if (this.i == null) {
            return null;
        }
        return PendingIntent.getActivity(this.b, System.identityHashCode(this.c), this.i.u(), 134217728);
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void a() {
        this.l = null;
        this.k = null;
        this.t = 0;
        this.e.a();
        this.f.a();
        D();
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void b() {
        this.t = 2;
        this.m = false;
        this.l = null;
        this.k = null;
        this.e.b();
        this.f.b();
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final boolean c() {
        boolean z = true;
        this.s.lock();
        try {
            if (!this.e.c() || (!this.f.c() && !E() && this.t != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.s.unlock();
        }
    }

    @DexIgnore
    public final boolean d() {
        this.s.lock();
        try {
            return this.t == 2;
        } finally {
            this.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("authClient").println(":");
        this.f.f(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append("anonClient").println(":");
        this.e.f(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final boolean g(T72 t72) {
        this.s.lock();
        try {
            if ((d() || c()) && !this.f.c()) {
                this.h.add(t72);
                if (this.t == 0) {
                    this.t = 1;
                }
                this.l = null;
                this.f.b();
                return true;
            }
            this.s.unlock();
            return false;
        } finally {
            this.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void h() {
        this.s.lock();
        try {
            boolean d2 = d();
            this.f.a();
            this.l = new Z52(4);
            if (d2) {
                new Ol2(this.d).post(new Xa2(this));
            } else {
                D();
            }
        } finally {
            this.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t2) {
        if (!t(t2)) {
            return (T) this.e.j(t2);
        }
        if (!E()) {
            return (T) this.f.j(t2);
        }
        t2.A(new Status(4, null, F()));
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t2) {
        if (!t(t2)) {
            return (T) this.e.k(t2);
        }
        if (!E()) {
            return (T) this.f.k(t2);
        }
        t2.A(new Status(4, null, F()));
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void l() {
        this.e.l();
        this.f.l();
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final Z52 m() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void o(int i2, boolean z) {
        this.c.c(i2, z);
        this.l = null;
        this.k = null;
    }

    @DexIgnore
    public final void p(Bundle bundle) {
        Bundle bundle2 = this.j;
        if (bundle2 == null) {
            this.j = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    @DexIgnore
    public final void q(Z52 z52) {
        int i2 = this.t;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.t = 0;
            }
            this.c.a(z52);
        }
        D();
        this.t = 0;
    }

    @DexIgnore
    public final boolean t(I72<? extends Z62, ? extends M62.Bi> i72) {
        M62.Ci<? extends M62.Bi> w = i72.w();
        Rc2.b(this.g.containsKey(w), "GoogleApiClient is not configured to use the API required for this call.");
        return this.g.get(w).equals(this.f);
    }
}
