package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xy4 {
    @DexIgnore
    public static /* final */ Xy4 a; // = new Xy4();

    @DexIgnore
    public final Date a() {
        return new Date();
    }

    @DexIgnore
    public final long b() {
        return new Date().getTime();
    }
}
