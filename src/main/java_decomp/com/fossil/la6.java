package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class La6 implements Factory<CommuteTimeSettingsDetailViewModel> {
    @DexIgnore
    public /* final */ Provider<An4> a;

    @DexIgnore
    public La6(Provider<An4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static La6 a(Provider<An4> provider) {
        return new La6(provider);
    }

    @DexIgnore
    public static CommuteTimeSettingsDetailViewModel c(An4 an4) {
        return new CommuteTimeSettingsDetailViewModel(an4);
    }

    @DexIgnore
    public CommuteTimeSettingsDetailViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
