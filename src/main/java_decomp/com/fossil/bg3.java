package com.fossil;

import com.fossil.Qb3;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bg3 extends Kc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Gi b;

    @DexIgnore
    public Bg3(Qb3 qb3, Qb3.Gi gi) {
        this.b = gi;
    }

    @DexIgnore
    @Override // com.fossil.Jc3
    public final void onMapClick(LatLng latLng) {
        this.b.onMapClick(latLng);
    }
}
