package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class St5 implements CoroutineUseCase.Ai {
    @DexIgnore
    public /* final */ Zh5 a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public St5(Zh5 zh5, ArrayList<Integer> arrayList) {
        Wg6.c(zh5, "lastErrorCode");
        this.a = zh5;
        this.b = arrayList;
    }

    @DexIgnore
    public final Zh5 a() {
        return this.a;
    }

    @DexIgnore
    public final ArrayList<Integer> b() {
        return this.b;
    }
}
