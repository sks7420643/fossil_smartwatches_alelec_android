package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Hw7 extends Dv7 {
    @DexIgnore
    public long c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Zy7<Yv7<?>> e;

    @DexIgnore
    public static /* synthetic */ void T(Hw7 hw7, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            hw7.S(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: decrementUseCount");
    }

    @DexIgnore
    public static /* synthetic */ void i0(Hw7 hw7, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            hw7.g0(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: incrementUseCount");
    }

    @DexIgnore
    public final void S(boolean z) {
        long V = this.c - V(z);
        this.c = V;
        if (V <= 0) {
            if (Nv7.a()) {
                if (!(this.c == 0)) {
                    throw new AssertionError();
                }
            }
            if (this.d) {
                shutdown();
            }
        }
    }

    @DexIgnore
    public final long V(boolean z) {
        return z ? 4294967296L : 1;
    }

    @DexIgnore
    public final void X(Yv7<?> yv7) {
        Zy7<Yv7<?>> zy7 = this.e;
        if (zy7 == null) {
            zy7 = new Zy7<>();
            this.e = zy7;
        }
        zy7.a(yv7);
    }

    @DexIgnore
    public long b0() {
        Zy7<Yv7<?>> zy7 = this.e;
        return (zy7 == null || zy7.c()) ? Long.MAX_VALUE : 0;
    }

    @DexIgnore
    public final void g0(boolean z) {
        this.c += V(z);
        if (!z) {
            this.d = true;
        }
    }

    @DexIgnore
    public final boolean o0() {
        return this.c >= V(true);
    }

    @DexIgnore
    public final boolean p0() {
        Zy7<Yv7<?>> zy7 = this.e;
        if (zy7 != null) {
            return zy7.c();
        }
        return true;
    }

    @DexIgnore
    public long q0() {
        if (!r0()) {
            return Long.MAX_VALUE;
        }
        return b0();
    }

    @DexIgnore
    public final boolean r0() {
        Yv7<?> d2;
        Zy7<Yv7<?>> zy7 = this.e;
        if (zy7 == null || (d2 = zy7.d()) == null) {
            return false;
        }
        d2.run();
        return true;
    }

    @DexIgnore
    public boolean s0() {
        return false;
    }

    @DexIgnore
    public void shutdown() {
    }
}
