package com.fossil;

import com.fossil.A74;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oi4 implements Ti4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Pi4 b;

    @DexIgnore
    public Oi4(Set<Ri4> set, Pi4 pi4) {
        this.a = d(set);
        this.b = pi4;
    }

    @DexIgnore
    public static A74<Ti4> b() {
        A74.Bi a2 = A74.a(Ti4.class);
        a2.b(K74.h(Ri4.class));
        a2.f(Ni4.b());
        return a2.d();
    }

    @DexIgnore
    public static /* synthetic */ Ti4 c(B74 b74) {
        return new Oi4(b74.c(Ri4.class), Pi4.a());
    }

    @DexIgnore
    public static String d(Set<Ri4> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<Ri4> it = set.iterator();
        while (it.hasNext()) {
            Ri4 next = it.next();
            sb.append(next.b());
            sb.append('/');
            sb.append(next.c());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.Ti4
    public String a() {
        if (this.b.b().isEmpty()) {
            return this.a;
        }
        return this.a + ' ' + d(this.b.b());
    }
}
