package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kk1 {
    @DexIgnore
    public static /* final */ Gi<Object> a; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Gi<Object> {
        @DexIgnore
        @Override // com.fossil.Kk1.Gi
        public void a(Object obj) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Di<List<T>> {
        @DexIgnore
        public List<T> a() {
            return new ArrayList();
        }

        @DexIgnore
        @Override // com.fossil.Kk1.Di
        public /* bridge */ /* synthetic */ Object create() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Gi<List<T>> {
        @DexIgnore
        @Override // com.fossil.Kk1.Gi
        public /* bridge */ /* synthetic */ void a(Object obj) {
            b((List) obj);
        }

        @DexIgnore
        public void b(List<T> list) {
            list.clear();
        }
    }

    @DexIgnore
    public interface Di<T> {
        @DexIgnore
        T create();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Mn0<T> {
        @DexIgnore
        public /* final */ Di<T> a;
        @DexIgnore
        public /* final */ Gi<T> b;
        @DexIgnore
        public /* final */ Mn0<T> c;

        @DexIgnore
        public Ei(Mn0<T> mn0, Di<T> di, Gi<T> gi) {
            this.c = mn0;
            this.a = di;
            this.b = gi;
        }

        @DexIgnore
        @Override // com.fossil.Mn0
        public boolean a(T t) {
            if (t instanceof Fi) {
                t.f().b(true);
            }
            this.b.a(t);
            return this.c.a(t);
        }

        @DexIgnore
        @Override // com.fossil.Mn0
        public T b() {
            T b2 = this.c.b();
            if (b2 == null) {
                b2 = this.a.create();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + b2.getClass());
                }
            }
            if (b2 instanceof Fi) {
                b2.f().b(false);
            }
            return b2;
        }
    }

    @DexIgnore
    public interface Fi {
        @DexIgnore
        Mk1 f();
    }

    @DexIgnore
    public interface Gi<T> {
        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    public static <T extends Fi> Mn0<T> a(Mn0<T> mn0, Di<T> di) {
        return b(mn0, di, c());
    }

    @DexIgnore
    public static <T> Mn0<T> b(Mn0<T> mn0, Di<T> di, Gi<T> gi) {
        return new Ei(mn0, di, gi);
    }

    @DexIgnore
    public static <T> Gi<T> c() {
        return (Gi<T>) a;
    }

    @DexIgnore
    public static <T extends Fi> Mn0<T> d(int i, Di<T> di) {
        return a(new On0(i), di);
    }

    @DexIgnore
    public static <T> Mn0<List<T>> e() {
        return f(20);
    }

    @DexIgnore
    public static <T> Mn0<List<T>> f(int i) {
        return b(new On0(i), new Bi(), new Ci());
    }
}
