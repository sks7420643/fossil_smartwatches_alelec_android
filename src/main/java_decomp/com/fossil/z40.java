package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z40 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ B70 b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;

    @DexIgnore
    public Z40(B70 b70, Object obj) {
        this.b = b70;
        this.c = obj;
    }

    @DexIgnore
    public final void run() {
        this.b.b.o(this.c);
    }
}
