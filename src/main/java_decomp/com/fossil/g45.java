package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G45 extends F45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v;
    @DexIgnore
    public long t;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        v = sparseIntArray;
        sparseIntArray.put(2131363300, 1);
        v.put(2131361968, 2);
    }
    */

    @DexIgnore
    public G45(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 3, u, v));
    }

    @DexIgnore
    public G45(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[2], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[1]);
        this.t = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.t != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.t = 1;
        }
        w();
    }
}
