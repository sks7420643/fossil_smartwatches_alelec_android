package com.fossil;

import android.webkit.MimeTypeMap;
import com.fossil.E61;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F61 implements E61<File> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ boolean a(File file) {
        return e(file);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(File file) {
        return f(file);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ Object c(G51 g51, File file, F81 f81, X51 x51, Xe6 xe6) {
        return d(g51, file, f81, x51, xe6);
    }

    @DexIgnore
    public Object d(G51 g51, File file, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        return new K61(S48.d(S48.k(file)), MimeTypeMap.getSingleton().getMimeTypeFromExtension(Cp7.f(file)), Q51.DISK);
    }

    @DexIgnore
    public boolean e(File file) {
        Wg6.c(file, "data");
        return E61.Ai.a(this, file);
    }

    @DexIgnore
    public String f(File file) {
        Wg6.c(file, "data");
        return file.getPath() + ':' + file.lastModified();
    }
}
