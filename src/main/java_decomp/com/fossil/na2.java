package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.L72;
import com.fossil.M62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Na2<ResultT> extends Ja2 {
    @DexIgnore
    public /* final */ W72<M62.Bi, ResultT> b;
    @DexIgnore
    public /* final */ Ot3<ResultT> c;
    @DexIgnore
    public /* final */ U72 d;

    @DexIgnore
    public Na2(int i, W72<M62.Bi, ResultT> w72, Ot3<ResultT> ot3, U72 u72) {
        super(i);
        this.c = ot3;
        this.b = w72;
        this.d = u72;
        if (i == 2 && w72.b()) {
            throw new IllegalArgumentException("Best-effort write calls cannot pass methods that should auto-resolve missing features.");
        }
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void b(Status status) {
        this.c.d(this.d.a(status));
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void c(L72.Ai<?> ai) throws DeadObjectException {
        try {
            this.b.a(ai.R(), this.c);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            b(Z82.a(e2));
        } catch (RuntimeException e3) {
            e(e3);
        }
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void d(A82 a82, boolean z) {
        a82.c(this.c, z);
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void e(Exception exc) {
        this.c.d(exc);
    }

    @DexIgnore
    @Override // com.fossil.Ja2
    public final B62[] g(L72.Ai<?> ai) {
        return this.b.c();
    }

    @DexIgnore
    @Override // com.fossil.Ja2
    public final boolean h(L72.Ai<?> ai) {
        return this.b.b();
    }
}
