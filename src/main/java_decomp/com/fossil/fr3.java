package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fr3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Fr3> CREATOR; // = new Ir3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ Long e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Double h;

    @DexIgnore
    public Fr3(int i, String str, long j, Long l, Float f2, String str2, String str3, Double d2) {
        this.b = i;
        this.c = str;
        this.d = j;
        this.e = l;
        if (i == 1) {
            this.h = f2 != null ? Double.valueOf(f2.doubleValue()) : null;
        } else {
            this.h = d2;
        }
        this.f = str2;
        this.g = str3;
    }

    @DexIgnore
    public Fr3(Hr3 hr3) {
        this(hr3.c, hr3.d, hr3.e, hr3.b);
    }

    @DexIgnore
    public Fr3(String str, long j, Object obj, String str2) {
        Rc2.g(str);
        this.b = 2;
        this.c = str;
        this.d = j;
        this.g = str2;
        if (obj == null) {
            this.e = null;
            this.h = null;
            this.f = null;
        } else if (obj instanceof Long) {
            this.e = (Long) obj;
            this.h = null;
            this.f = null;
        } else if (obj instanceof String) {
            this.e = null;
            this.h = null;
            this.f = (String) obj;
        } else if (obj instanceof Double) {
            this.e = null;
            this.h = (Double) obj;
            this.f = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    @DexIgnore
    public final Object c() {
        Long l = this.e;
        if (l != null) {
            return l;
        }
        Double d2 = this.h;
        if (d2 != null) {
            return d2;
        }
        String str = this.f;
        if (str == null) {
            return null;
        }
        return str;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.u(parcel, 2, this.c, false);
        Bd2.r(parcel, 3, this.d);
        Bd2.s(parcel, 4, this.e, false);
        Bd2.l(parcel, 5, null, false);
        Bd2.u(parcel, 6, this.f, false);
        Bd2.u(parcel, 7, this.g, false);
        Bd2.i(parcel, 8, this.h, false);
        Bd2.b(parcel, a2);
    }
}
