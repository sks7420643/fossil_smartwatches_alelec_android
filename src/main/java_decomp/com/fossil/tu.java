package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.mapped.C90;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tu extends Mu {
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public C90 M; // = new C90(0, WorkoutType.UNKNOWN, WorkoutState.END, 0, 0, 0, 0, 0, 0, 0, 0, false);

    @DexIgnore
    public Tu(K5 k5) {
        super(Fu.p, Hs.D, k5, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.f0, this.M.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        WorkoutState workoutState;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 29) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            WorkoutType d = G80.d(Hy1.p(order.get(4)));
            short p = Hy1.p(order.get(5));
            WorkoutState[] values = WorkoutState.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    workoutState = null;
                    break;
                }
                workoutState = values[i];
                if (workoutState.getValue() == p) {
                    break;
                }
                i++;
            }
            if (workoutState == null) {
                this.v = Mw.a(this.v, null, null, Lw.l, null, null, 27);
            } else {
                C90 c90 = new C90(Hy1.o(order.getInt(0)), d, workoutState, Hy1.o(order.getInt(6)), Hy1.o(order.getInt(10)), Hy1.o(order.getInt(14)), Hy1.o(order.getInt(18)), Hy1.o(order.getInt(22)), Hy1.p(order.get(26)), Hy1.p(order.get(27)), Hy1.p(order.get(28)), bArr.length != 29 && ((Hy1.p(order.get(29)) >> 0) & 1) == 1);
                this.M = c90;
                G80.k(jSONObject, Jd0.f0, c90.toJSONObject());
                this.v = Mw.a(this.v, null, null, Lw.b, null, null, 27);
            }
        } else {
            this.v = Mw.a(this.v, null, null, Lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.L;
    }

    @DexIgnore
    public final C90 Q() {
        return this.M;
    }
}
