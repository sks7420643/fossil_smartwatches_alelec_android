package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fm4 {
    @DexIgnore
    public /* final */ Dm4 a;
    @DexIgnore
    public /* final */ List<Em4> b;

    @DexIgnore
    public Fm4(Dm4 dm4) {
        this.a = dm4;
        ArrayList arrayList = new ArrayList();
        this.b = arrayList;
        arrayList.add(new Em4(dm4, new int[]{1}));
    }

    @DexIgnore
    public final Em4 a(int i) {
        if (i >= this.b.size()) {
            List<Em4> list = this.b;
            Em4 em4 = list.get(list.size() - 1);
            for (int size = this.b.size(); size <= i; size++) {
                Dm4 dm4 = this.a;
                em4 = em4.g(new Em4(dm4, new int[]{1, dm4.c((size - 1) + dm4.d())}));
                this.b.add(em4);
            }
        }
        return this.b.get(i);
    }

    @DexIgnore
    public void b(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                Em4 a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] d = new Em4(this.a, iArr2).h(i, 1).b(a2)[1].d();
                int length2 = i - d.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(d, 0, iArr, length + length2, d.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
