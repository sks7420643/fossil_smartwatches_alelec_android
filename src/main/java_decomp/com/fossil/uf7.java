package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.j256.ormlite.field.FieldType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uf7 implements SharedPreferences {
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ String[] b; // = {FieldType.FOREIGN_ID_FIELD_SUFFIX, "key", "type", "value"};
    @DexIgnore
    public /* final */ HashMap<String, Object> c; // = new HashMap<>();
    @DexIgnore
    public Ai d; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements SharedPreferences.Editor {
        @DexIgnore
        public Map<String, Object> a; // = new HashMap();
        @DexIgnore
        public Set<String> b; // = new HashSet();
        @DexIgnore
        public boolean c; // = false;
        @DexIgnore
        public ContentResolver d;

        @DexIgnore
        public Ai(ContentResolver contentResolver) {
            this.d = contentResolver;
        }

        @DexIgnore
        public void apply() {
        }

        @DexIgnore
        public SharedPreferences.Editor clear() {
            this.c = true;
            return this;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x003f A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean commit() {
            /*
                r9 = this;
                r5 = 0
                r3 = 1
                r2 = 0
                android.content.ContentValues r4 = new android.content.ContentValues
                r4.<init>()
                boolean r0 = r9.c
                if (r0 == 0) goto L_0x0015
                android.content.ContentResolver r0 = r9.d
                android.net.Uri r1 = com.fossil.Cf7.a
                r0.delete(r1, r5, r5)
                r9.c = r2
            L_0x0015:
                java.util.Set<java.lang.String> r0 = r9.b
                java.util.Iterator r1 = r0.iterator()
            L_0x001b:
                boolean r0 = r1.hasNext()
                if (r0 == 0) goto L_0x0035
                java.lang.Object r0 = r1.next()
                java.lang.String r0 = (java.lang.String) r0
                android.content.ContentResolver r5 = r9.d
                android.net.Uri r6 = com.fossil.Cf7.a
                java.lang.String r7 = "key = ?"
                java.lang.String[] r8 = new java.lang.String[r3]
                r8[r2] = r0
                r5.delete(r6, r7, r8)
                goto L_0x001b
            L_0x0035:
                java.util.Map<java.lang.String, java.lang.Object> r0 = r9.a
                java.util.Set r0 = r0.entrySet()
                java.util.Iterator r5 = r0.iterator()
            L_0x003f:
                boolean r0 = r5.hasNext()
                if (r0 == 0) goto L_0x00c1
                java.lang.Object r0 = r5.next()
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                java.lang.Object r6 = r0.getValue()
                if (r6 != 0) goto L_0x0072
                java.lang.String r1 = "unresolve failed, null value"
            L_0x0053:
                java.lang.String r7 = "MicroMsg.SDK.PluginProvider.Resolver"
                com.fossil.Ye7.b(r7, r1)
                r1 = r2
            L_0x0059:
                if (r1 != 0) goto L_0x00ad
                r1 = r2
            L_0x005c:
                if (r1 == 0) goto L_0x003f
                android.content.ContentResolver r1 = r9.d
                android.net.Uri r6 = com.fossil.Cf7.a
                java.lang.String r7 = "key = ?"
                java.lang.String[] r8 = new java.lang.String[r3]
                java.lang.Object r0 = r0.getKey()
                java.lang.String r0 = (java.lang.String) r0
                r8[r2] = r0
                r1.update(r6, r4, r7, r8)
                goto L_0x003f
            L_0x0072:
                boolean r1 = r6 instanceof java.lang.Integer
                if (r1 == 0) goto L_0x0078
                r1 = r3
                goto L_0x0059
            L_0x0078:
                boolean r1 = r6 instanceof java.lang.Long
                if (r1 == 0) goto L_0x007e
                r1 = 2
                goto L_0x0059
            L_0x007e:
                boolean r1 = r6 instanceof java.lang.String
                if (r1 == 0) goto L_0x0084
                r1 = 3
                goto L_0x0059
            L_0x0084:
                boolean r1 = r6 instanceof java.lang.Boolean
                if (r1 == 0) goto L_0x008a
                r1 = 4
                goto L_0x0059
            L_0x008a:
                boolean r1 = r6 instanceof java.lang.Float
                if (r1 == 0) goto L_0x0090
                r1 = 5
                goto L_0x0059
            L_0x0090:
                boolean r1 = r6 instanceof java.lang.Double
                if (r1 == 0) goto L_0x0096
                r1 = 6
                goto L_0x0059
            L_0x0096:
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                java.lang.String r7 = "unresolve failed, unknown type="
                r1.<init>(r7)
                java.lang.Class r7 = r6.getClass()
                java.lang.String r7 = r7.toString()
                r1.append(r7)
                java.lang.String r1 = r1.toString()
                goto L_0x0053
            L_0x00ad:
                java.lang.String r7 = "type"
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
                r4.put(r7, r1)
                java.lang.String r1 = "value"
                java.lang.String r6 = r6.toString()
                r4.put(r1, r6)
                r1 = r3
                goto L_0x005c
            L_0x00c1:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Uf7.Ai.commit():boolean");
        }

        @DexIgnore
        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            this.a.put(str, Boolean.valueOf(z));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putFloat(String str, float f) {
            this.a.put(str, Float.valueOf(f));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putInt(String str, int i) {
            this.a.put(str, Integer.valueOf(i));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putLong(String str, long j) {
            this.a.put(str, Long.valueOf(j));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putString(String str, String str2) {
            this.a.put(str, str2);
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            return null;
        }

        @DexIgnore
        public SharedPreferences.Editor remove(String str) {
            this.b.add(str);
            return this;
        }
    }

    @DexIgnore
    public Uf7(Context context) {
        this.a = context.getContentResolver();
    }

    @DexIgnore
    public final Object a(String str) {
        try {
            Cursor query = this.a.query(Cf7.a, this.b, "key = ?", new String[]{str}, null);
            if (query == null) {
                return null;
            }
            Object a2 = query.moveToFirst() ? Bf7.a(query.getInt(query.getColumnIndex("type")), query.getString(query.getColumnIndex("value"))) : null;
            query.close();
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public boolean contains(String str) {
        return a(str) != null;
    }

    @DexIgnore
    public SharedPreferences.Editor edit() {
        if (this.d == null) {
            this.d = new Ai(this.a);
        }
        return this.d;
    }

    @DexIgnore
    @Override // android.content.SharedPreferences
    public Map<String, ?> getAll() {
        try {
            Cursor query = this.a.query(Cf7.a, this.b, null, null, null);
            if (query == null) {
                return null;
            }
            int columnIndex = query.getColumnIndex("key");
            int columnIndex2 = query.getColumnIndex("type");
            int columnIndex3 = query.getColumnIndex("value");
            while (query.moveToNext()) {
                this.c.put(query.getString(columnIndex), Bf7.a(query.getInt(columnIndex2), query.getString(columnIndex3)));
            }
            query.close();
            return this.c;
        } catch (Exception e) {
            e.printStackTrace();
            return this.c;
        }
    }

    @DexIgnore
    public boolean getBoolean(String str, boolean z) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Boolean)) ? z : ((Boolean) a2).booleanValue();
    }

    @DexIgnore
    public float getFloat(String str, float f) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Float)) ? f : ((Float) a2).floatValue();
    }

    @DexIgnore
    public int getInt(String str, int i) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Integer)) ? i : ((Integer) a2).intValue();
    }

    @DexIgnore
    public long getLong(String str, long j) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Long)) ? j : ((Long) a2).longValue();
    }

    @DexIgnore
    public String getString(String str, String str2) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof String)) ? str2 : (String) a2;
    }

    @DexIgnore
    @Override // android.content.SharedPreferences
    public Set<String> getStringSet(String str, Set<String> set) {
        return null;
    }

    @DexIgnore
    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    @DexIgnore
    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }
}
