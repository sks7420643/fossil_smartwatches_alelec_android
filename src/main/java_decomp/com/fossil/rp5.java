package com.fossil;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rp5 extends BaseProvider {
    @DexIgnore
    boolean a();

    @DexIgnore
    boolean addOrUpdateServerSetting(ServerSetting serverSetting);

    @DexIgnore
    ServerSetting getServerSettingByKey(String str);

    @DexIgnore
    void k(List<ServerSetting> list);
}
