package com.fossil;

import android.app.Activity;
import android.util.Log;
import androidx.fragment.app.FragmentActivity;
import com.fossil.uk5;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy5 extends by5 implements uk5.d {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public /* final */ cy5 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ uk5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return fy5.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends x62<Status> {
        @DexIgnore
        public /* final */ /* synthetic */ fy5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fy5 fy5, Activity activity, int i) {
            super(activity, i);
            this.c = fy5;
        }

        @DexIgnore
        @Override // com.fossil.x62
        public void d(Status status) {
            pq7.c(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fy5.k.a();
            local.d(a2, "onGFLogout.onUnresolvableFailure(): isSuccess = " + status.D() + ", statusMessage = " + status.h() + ", statusCode = " + status.f());
            if (status.f() == 17 || status.f() == 5010) {
                this.c.x();
                return;
            }
            this.c.g.N2(true);
            this.c.g.I5(status.f());
        }

        @DexIgnore
        /* renamed from: e */
        public void c(Status status) {
            pq7.c(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fy5.k.a();
            local.d(a2, "onGFLogout.onSuccess(): isSuccess = " + status.D() + ", statusMessage = " + status.h());
            if (status.D()) {
                this.c.x();
                return;
            }
            this.c.g.N2(true);
            this.c.g.I5(status.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1", f = "ConnectedAppsPresenter.kt", l = {43}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fy5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1", f = "ConnectedAppsPresenter.kt", l = {44}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object currentUser;
                fy5 fy5;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    fy5 fy52 = this.this$0.this$0;
                    UserRepository userRepository = fy52.h;
                    this.L$0 = iv7;
                    this.L$1 = fy52;
                    this.label = 1;
                    currentUser = userRepository.getCurrentUser(this);
                    if (currentUser == d) {
                        return d;
                    }
                    fy5 = fy52;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    fy5 = (fy5) this.L$1;
                    currentUser = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                fy5.e = (MFUser) currentUser;
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fy5 fy5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = fy5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            boolean e;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                e = this.this$0.i.e();
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.Z$0 = e;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                boolean z = this.Z$0;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                e = z;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fy5.k.a();
            local.d(a2, "starts isGFConnected=" + this.this$0.i.e());
            this.this$0.i.i(this.this$0);
            this.this$0.g.N2(e);
            if (!e && this.this$0.i.f() && !this.this$0.f) {
                this.this$0.i.k();
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fy5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.h;
                    MFUser mFUser = this.this$0.$it;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object updateUser = userRepository.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(MFUser mFUser, qn7 qn7, fy5 fy5) {
            super(2, qn7);
            this.$it = mFUser;
            this.this$0 = fy5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.$it, qn7, this.this$0);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = fy5.class.getSimpleName();
        pq7.b(simpleName, "ConnectedAppsPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public fy5(cy5 cy5, UserRepository userRepository, PortfolioApp portfolioApp, uk5 uk5) {
        pq7.c(cy5, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(portfolioApp, "mApp");
        pq7.c(uk5, "mGoogleFitHelper");
        this.g = cy5;
        this.h = userRepository;
        this.i = uk5;
    }

    @DexIgnore
    @Override // com.fossil.uk5.d
    public void b() {
        FLogger.INSTANCE.getLocal().d(j, "onGFConnectionSuccess");
        String name = sh5.GoogleFit.getName();
        pq7.b(name, "Integration.GoogleFit.getName()");
        w(name);
        this.g.N2(true);
    }

    @DexIgnore
    @Override // com.fossil.uk5.d
    public void c(z52 z52) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onGFConnectionFailed - Cause: " + z52);
        if (z52 == null) {
            return;
        }
        if (z52.k()) {
            this.f = false;
            this.g.f1(z52);
            return;
        }
        this.g.I5(z52.c());
    }

    @DexIgnore
    @Override // com.fossil.uk5.d
    public void f(t62<Status> t62) {
        if (t62 != null) {
            cy5 cy5 = this.g;
            if (cy5 != null) {
                FragmentActivity activity = ((xn6) cy5).getActivity();
                if (activity != null) {
                    t62.d(new b(this, activity, 3));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        Log.d(j, "stop");
        MFUser mFUser = this.e;
        if (mFUser != null) {
            xw7 unused = gu7.d(k(), null, null, new d(mFUser, null, this), 3, null);
        }
        this.i.i(null);
    }

    @DexIgnore
    @Override // com.fossil.by5
    public void n() {
        if (!PortfolioApp.h0.c().p0()) {
            this.g.I5(601);
            this.g.N2(this.i.e());
        } else if (this.i.e()) {
            this.i.g();
        } else if (this.i.d()) {
            this.i.a();
        } else {
            uk5 uk5 = this.i;
            cy5 cy5 = this.g;
            if (cy5 != null) {
                uk5.h((xn6) cy5);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.by5
    public void o() {
        this.i.a();
    }

    @DexIgnore
    public final void w(String str) {
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrationsList = mFUser.getIntegrationsList();
            if (integrationsList != null) {
                if (!integrationsList.contains(str)) {
                    integrationsList.add(str);
                }
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    mFUser2.setIntegrationsList(integrationsList);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void x() {
        this.i.b();
        String name = sh5.GoogleFit.getName();
        pq7.b(name, "Integration.GoogleFit.getName()");
        y(name);
        this.g.N2(false);
    }

    @DexIgnore
    public final void y(String str) {
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrationsList = mFUser.getIntegrationsList();
            if (integrationsList != null) {
                integrationsList.remove(str);
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    mFUser2.setIntegrationsList(integrationsList);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public void z() {
        this.g.M5(this);
    }
}
