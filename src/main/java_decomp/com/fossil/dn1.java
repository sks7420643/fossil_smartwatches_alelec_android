package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dn1 extends R60 {
    @DexIgnore
    public static /* final */ Ci CREATOR; // = new Ci(null);
    @DexIgnore
    public /* final */ Ai c;
    @DexIgnore
    public /* final */ Bi d;
    @DexIgnore
    public /* final */ Bi e;
    @DexIgnore
    public /* final */ Bi f;
    @DexIgnore
    public /* final */ Bi g;
    @DexIgnore
    public /* final */ Bi h;
    @DexIgnore
    public /* final */ Bi i;
    @DexIgnore
    public /* final */ Bi j;
    @DexIgnore
    public /* final */ Bi k;
    @DexIgnore
    public /* final */ Bi l;
    @DexIgnore
    public /* final */ short m;
    @DexIgnore
    public /* final */ short n;

    @DexIgnore
    public enum Ai {
        DAILY((byte) 0),
        EXTENDED((byte) 1),
        TIME_ONLY((byte) 2),
        CUSTOM((byte) 3);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(byte b) throws IllegalArgumentException {
                Ai ai;
                Ai[] values = Ai.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        ai = null;
                        break;
                    }
                    ai = values[i];
                    if (ai.a() == b) {
                        break;
                    }
                    i++;
                }
                if (ai != null) {
                    return ai;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public Ai(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore
    public enum Bi {
        OFF((byte) 0),
        ON((byte) 1);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Bi a(byte b) throws IllegalArgumentException {
                Bi bi;
                Bi[] values = Bi.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bi = null;
                        break;
                    }
                    bi = values[i];
                    if (bi.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bi != null) {
                    return bi;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public Bi(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Parcelable.Creator<Dn1> {
        @DexIgnore
        public /* synthetic */ Ci(Qg6 qg6) {
        }

        @DexIgnore
        public final Dn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 14) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new Dn1(Ai.d.a(order.get(0)), Bi.d.a(order.get(1)), Bi.d.a(order.get(2)), Bi.d.a(order.get(3)), Bi.d.a(order.get(4)), Bi.d.a(order.get(5)), Bi.d.a(order.get(6)), Bi.d.a(order.get(7)), Bi.d.a(order.get(8)), Bi.d.a(order.get(9)), order.getShort(10), order.getShort(12));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 14"));
        }

        @DexIgnore
        public Dn1 b(Parcel parcel) {
            return new Dn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dn1 createFromParcel(Parcel parcel) {
            return new Dn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dn1[] newArray(int i) {
            return new Dn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Dn1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = Ai.d.a(parcel.readByte());
        this.d = Bi.d.a(parcel.readByte());
        this.e = Bi.d.a(parcel.readByte());
        this.f = Bi.d.a(parcel.readByte());
        this.g = Bi.d.a(parcel.readByte());
        this.h = Bi.d.a(parcel.readByte());
        this.i = Bi.d.a(parcel.readByte());
        this.j = Bi.d.a(parcel.readByte());
        this.k = Bi.d.a(parcel.readByte());
        this.l = Bi.d.a(parcel.readByte());
        this.m = (short) ((short) parcel.readInt());
        this.n = (short) ((short) parcel.readInt());
    }

    @DexIgnore
    public Dn1(Ai ai, Bi bi, Bi bi2, Bi bi3, Bi bi4, Bi bi5, Bi bi6, Bi bi7, Bi bi8, Bi bi9, short s, short s2) {
        super(Zm1.HELLAS_BATTERY);
        this.c = ai;
        this.d = bi;
        this.e = bi2;
        this.f = bi3;
        this.g = bi4;
        this.h = bi5;
        this.i = bi6;
        this.j = bi7;
        this.k = bi8;
        this.l = bi9;
        this.m = (short) s;
        this.n = (short) s2;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(14).order(ByteOrder.LITTLE_ENDIAN).put(this.c.a()).put(this.d.a()).put(this.e.a()).put(this.f.a()).put(this.g.a()).put(this.h.a()).put(this.i.a()).put(this.j.a()).put(this.k.a()).put(this.l.a()).putShort(this.m).putShort(this.n).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.m5, Ey1.a(this.c));
            G80.k(jSONObject, Jd0.n5, Ey1.a(this.d));
            G80.k(jSONObject, Jd0.o5, Ey1.a(this.e));
            G80.k(jSONObject, Jd0.p5, Ey1.a(this.f));
            G80.k(jSONObject, Jd0.q5, Ey1.a(this.g));
            G80.k(jSONObject, Jd0.r5, Ey1.a(this.h));
            G80.k(jSONObject, Jd0.s5, Ey1.a(this.i));
            G80.k(jSONObject, Jd0.t5, Ey1.a(this.j));
            G80.k(jSONObject, Jd0.u5, Ey1.a(this.k));
            G80.k(jSONObject, Jd0.v5, Ey1.a(this.l));
            G80.k(jSONObject, Jd0.w5, Short.valueOf(this.m));
            G80.k(jSONObject, Jd0.x5, Short.valueOf(this.n));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Dn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Dn1 dn1 = (Dn1) obj;
            if (this.c != dn1.c) {
                return false;
            }
            if (this.d != dn1.d) {
                return false;
            }
            if (this.e != dn1.e) {
                return false;
            }
            if (this.f != dn1.f) {
                return false;
            }
            if (this.g != dn1.g) {
                return false;
            }
            if (this.h != dn1.h) {
                return false;
            }
            if (this.i != dn1.i) {
                return false;
            }
            if (this.j != dn1.j) {
                return false;
            }
            if (this.k != dn1.k) {
                return false;
            }
            if (this.l != dn1.l) {
                return false;
            }
            if (this.m != dn1.m) {
                return false;
            }
            return this.n == dn1.n;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HellasBatteryConfig");
    }

    @DexIgnore
    public final Bi getAlwaysOnScreenState() {
        return this.e;
    }

    @DexIgnore
    public final Ai getBatteryMode() {
        return this.c;
    }

    @DexIgnore
    public final short getBleFromMinuteOffset() {
        return this.m;
    }

    @DexIgnore
    public final short getBleToMinuteOffset() {
        return this.n;
    }

    @DexIgnore
    public final Bi getGoogleDetectState() {
        return this.l;
    }

    @DexIgnore
    public final Bi getLocationState() {
        return this.h;
    }

    @DexIgnore
    public final Bi getNfcState() {
        return this.d;
    }

    @DexIgnore
    public final Bi getSpeakerState() {
        return this.j;
    }

    @DexIgnore
    public final Bi getTiltToWakeState() {
        return this.g;
    }

    @DexIgnore
    public final Bi getTouchToWakeState() {
        return this.f;
    }

    @DexIgnore
    public final Bi getVibrationState() {
        return this.i;
    }

    @DexIgnore
    public final Bi getWifiState() {
        return this.k;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = this.l.hashCode();
        return (((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + Short.valueOf(this.m).hashCode()) * 31) + Short.valueOf(this.n).hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.g.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.h.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.i.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.j.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.k.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.l.a());
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.m));
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.n));
        }
    }
}
