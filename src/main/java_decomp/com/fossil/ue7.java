package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ue7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public Bundle e;

        @DexIgnore
        public final String toString() {
            return "targetPkgName:" + this.a + ", targetClassName:" + this.b + ", content:" + this.c + ", flags:" + this.d + ", bundle:" + this.e;
        }
    }

    @DexIgnore
    public static boolean a(Context context, Ai ai) {
        if (context == null) {
            Ye7.b("MicroMsg.SDK.MMessageAct", "send fail, invalid argument");
            return false;
        } else if (Af7.a(ai.a)) {
            Ye7.b("MicroMsg.SDK.MMessageAct", "send fail, invalid targetPkgName, targetPkgName = " + ai.a);
            return false;
        } else {
            if (Af7.a(ai.b)) {
                ai.b = ai.a + ".wxapi.WXEntryActivity";
            }
            Ye7.e("MicroMsg.SDK.MMessageAct", "send, targetPkgName = " + ai.a + ", targetClassName = " + ai.b);
            Intent intent = new Intent();
            intent.setClassName(ai.a, ai.b);
            Bundle bundle = ai.e;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 587268097);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", ai.c);
            intent.putExtra("_mmessage_checksum", We7.a(ai.c, 587268097, packageName));
            int i = ai.d;
            if (i == -1) {
                intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY).addFlags(134217728);
            } else {
                intent.setFlags(i);
            }
            try {
                context.startActivity(intent);
                Ye7.e("MicroMsg.SDK.MMessageAct", "send mm message, intent=" + intent);
                return true;
            } catch (Exception e) {
                Ye7.a("MicroMsg.SDK.MMessageAct", "send fail, ex = %s", e.getMessage());
                return false;
            }
        }
    }
}
