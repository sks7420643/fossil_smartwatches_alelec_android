package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O48 extends D58 {
    @DexIgnore
    public D58 e;

    @DexIgnore
    public O48(D58 d58) {
        Wg6.c(d58, "delegate");
        this.e = d58;
    }

    @DexIgnore
    @Override // com.fossil.D58
    public D58 a() {
        return this.e.a();
    }

    @DexIgnore
    @Override // com.fossil.D58
    public D58 b() {
        return this.e.b();
    }

    @DexIgnore
    @Override // com.fossil.D58
    public long c() {
        return this.e.c();
    }

    @DexIgnore
    @Override // com.fossil.D58
    public D58 d(long j) {
        return this.e.d(j);
    }

    @DexIgnore
    @Override // com.fossil.D58
    public boolean e() {
        return this.e.e();
    }

    @DexIgnore
    @Override // com.fossil.D58
    public void f() throws IOException {
        this.e.f();
    }

    @DexIgnore
    @Override // com.fossil.D58
    public D58 g(long j, TimeUnit timeUnit) {
        Wg6.c(timeUnit, Constants.PROFILE_KEY_UNIT);
        return this.e.g(j, timeUnit);
    }

    @DexIgnore
    @Override // com.fossil.D58
    public long h() {
        return this.e.h();
    }

    @DexIgnore
    public final D58 i() {
        return this.e;
    }

    @DexIgnore
    public final O48 j(D58 d58) {
        Wg6.c(d58, "delegate");
        this.e = d58;
        return this;
    }
}
