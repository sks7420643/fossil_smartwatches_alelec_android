package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface B74 {
    @DexIgnore
    <T> Mg4<T> a(Class<T> cls);

    @DexIgnore
    <T> Mg4<Set<T>> b(Class<T> cls);

    @DexIgnore
    <T> Set<T> c(Class<T> cls);

    @DexIgnore
    <T> T get(Class<T> cls);
}
