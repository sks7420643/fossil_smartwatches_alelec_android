package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.K70;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Hl1 extends Gl1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ Set<K70> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Hl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Hl1 a(byte[] bArr) throws IllegalArgumentException {
            boolean z = false;
            if (bArr.length == 3) {
                byte b = bArr[0];
                Set<K70> a2 = K70.e.a(bArr[0]);
                if (((byte) (((byte) 128) & bArr[1])) != 0) {
                    z = true;
                }
                byte b2 = bArr[1];
                byte b3 = (byte) (bArr[1] & 63);
                byte b4 = (byte) (bArr[2] & 31);
                return z ? new Ol1(b4, b3, a2) : (a2.isEmpty() || a2.size() == K70.values().length) ? new Kl1(b4, b3, null) : new Kl1(b4, b3, (K70) Pm7.E(a2));
            }
            throw new IllegalArgumentException(E.b(E.e("Size("), bArr.length, " not equal to 3."));
        }

        @DexIgnore
        public Hl1 b(Parcel parcel) {
            boolean z = true;
            parcel.readInt();
            byte readByte = parcel.readByte();
            byte readByte2 = parcel.readByte();
            K70.Ai ai = K70.e;
            String[] createStringArray = parcel.createStringArray();
            if (createStringArray != null) {
                Wg6.b(createStringArray, "parcel.createStringArray()!!");
                LinkedHashSet linkedHashSet = new LinkedHashSet(Em7.d0(ai.b(createStringArray)));
                boolean z2 = parcel.readInt() != 0;
                boolean z3 = parcel.readInt() != 0;
                if (parcel.readInt() == 0) {
                    z = false;
                }
                if (z2) {
                    return new Ol1(readByte, readByte2, linkedHashSet, z3, z);
                }
                return new Kl1(readByte, readByte2, !linkedHashSet.isEmpty() ? linkedHashSet.size() == K70.values().length ? null : (K70) Pm7.E(linkedHashSet) : null);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Hl1 createFromParcel(Parcel parcel) {
            return b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hl1[] newArray(int i) {
            return new Hl1[i];
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.util.Set<? extends com.mapped.K70> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Hl1(byte b, byte b2, Set<? extends K70> set, boolean z, boolean z2, boolean z3) throws IllegalArgumentException {
        super(R8.c);
        boolean z4 = true;
        this.c = (byte) b;
        this.d = (byte) b2;
        this.e = set;
        this.f = z;
        this.g = z2;
        this.h = z3;
        if (b >= 0 && 23 >= b) {
            byte b3 = this.d;
            if (!((b3 < 0 || 59 < b3) ? false : z4)) {
                throw new IllegalArgumentException(E.c(E.e("minute("), this.d, ") is out of range ", "[0, 59]."));
            }
            return;
        }
        throw new IllegalArgumentException(E.c(E.e("hour("), this.c, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public byte[] b() {
        return new byte[]{(byte) ((this.h ? (byte) 128 : (byte) 0) | G80.a(this.e)), (byte) (((byte) ((this.f ? (byte) 128 : (byte) 0) | (this.g ? (byte) 64 : (byte) 0))) | this.d), this.c};
    }

    @DexIgnore
    public final Set<K70> c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Hl1 hl1 = (Hl1) obj;
            return this.c == hl1.c && this.d == hl1.d && G80.a(this.e) == G80.a(hl1.e) && this.f == hl1.f && this.g == hl1.g && this.h == hl1.h;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
    }

    @DexIgnore
    public final byte getHour() {
        return this.c;
    }

    @DexIgnore
    public final byte getMinute() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public int hashCode() {
        int hashCode = super.hashCode();
        byte b = this.c;
        byte b2 = this.d;
        int hashCode2 = this.e.hashCode();
        int hashCode3 = Boolean.valueOf(this.f).hashCode();
        return (((((((((((hashCode * 31) + b) * 31) + b2) * 31) + hashCode2) * 31) + hashCode3) * 31) + Boolean.valueOf(this.g).hashCode()) * 31) + Boolean.valueOf(this.h).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.Gl1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(G80.k(super.toJSONObject(), Jd0.A, Byte.valueOf(this.c)), Jd0.B, Byte.valueOf(this.d));
        Jd0 jd0 = Jd0.C;
        Set<K70> set = this.e;
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = set.iterator();
        while (it.hasNext()) {
            jSONArray.put(Ey1.a(it.next()));
        }
        return G80.k(G80.k(G80.k(G80.k(k, jd0, jSONArray), Jd0.D, Boolean.valueOf(this.f)), Jd0.F, Boolean.valueOf(this.h)), Jd0.E, Boolean.valueOf(this.g));
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            Object[] array = this.e.toArray(new K70[0]);
            if (array != null) {
                K70[] k70Arr = (K70[]) array;
                ArrayList arrayList = new ArrayList(k70Arr.length);
                for (K70 k70 : k70Arr) {
                    arrayList.add(k70.name());
                }
                Object[] array2 = arrayList.toArray(new String[0]);
                if (array2 != null) {
                    parcel.writeStringArray((String[]) array2);
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        if (parcel != null) {
            parcel.writeInt(this.f ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.g ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.h ? 1 : 0);
        }
    }
}
