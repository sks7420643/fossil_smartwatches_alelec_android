package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rx5 implements Factory<ArrayList<Alarm>> {
    @DexIgnore
    public static ArrayList<Alarm> a(Qx5 qx5) {
        ArrayList<Alarm> b = qx5.b();
        Lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
