package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S53 implements Xw2<R53> {
    @DexIgnore
    public static S53 c; // = new S53();
    @DexIgnore
    public /* final */ Xw2<R53> b;

    @DexIgnore
    public S53() {
        this(Ww2.b(new U53()));
    }

    @DexIgnore
    public S53(Xw2<R53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((R53) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((R53) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((R53) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((R53) c.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((R53) c.zza()).zze();
    }

    @DexIgnore
    public static boolean f() {
        return ((R53) c.zza()).zzf();
    }

    @DexIgnore
    public static boolean g() {
        return ((R53) c.zza()).zzg();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ R53 zza() {
        return this.b.zza();
    }
}
