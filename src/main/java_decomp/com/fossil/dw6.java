package com.fossil;

import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dw6 implements Factory<Cw6> {
    @DexIgnore
    public static Cw6 a(Zv6 zv6, ResetPasswordUseCase resetPasswordUseCase) {
        return new Cw6(zv6, resetPasswordUseCase);
    }
}
