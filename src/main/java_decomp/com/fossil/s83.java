package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S83 implements Xw2<R83> {
    @DexIgnore
    public static S83 c; // = new S83();
    @DexIgnore
    public /* final */ Xw2<R83> b;

    @DexIgnore
    public S83() {
        this(Ww2.b(new U83()));
    }

    @DexIgnore
    public S83(Xw2<R83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((R83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ R83 zza() {
        return this.b.zza();
    }
}
