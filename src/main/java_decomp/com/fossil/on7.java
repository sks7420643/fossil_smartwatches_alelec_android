package com.fossil;

import com.mapped.Af6;
import com.mapped.Af6.Bi;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class On7<B extends Af6.Bi, E extends B> implements Af6.Ci<E> {
    @DexIgnore
    public /* final */ Af6.Ci<?> a;
    @DexIgnore
    public /* final */ Hg6<Af6.Bi, E> b;

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r2v1. Raw type applied. Possible types: com.mapped.Af6$Ci<?> */
    /* JADX WARN: Type inference failed for: r3v0, types: [com.mapped.Hg6<? super com.mapped.Af6$Bi, ? extends E extends B>, java.lang.Object, com.mapped.Hg6<com.mapped.Af6$Bi, E extends B>] */
    public On7(Af6.Ci<B> ci, Hg6<? super Af6.Bi, ? extends E> hg6) {
        Wg6.c(ci, "baseKey");
        Wg6.c(hg6, "safeCast");
        this.b = hg6;
        this.a = ci instanceof On7 ? (Af6.Ci<B>) ((On7) ci).a : ci;
    }

    @DexIgnore
    public final boolean a(Af6.Ci<?> ci) {
        Wg6.c(ci, "key");
        return ci == this || this.a == ci;
    }

    @DexIgnore
    public final E b(Af6.Bi bi) {
        Wg6.c(bi, "element");
        return this.b.invoke(bi);
    }
}
