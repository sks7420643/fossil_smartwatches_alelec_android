package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.M62;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C92 implements S92, Va2 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Condition c;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ D62 e;
    @DexIgnore
    public /* final */ E92 f;
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, M62.Fi> g;
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, Z52> h; // = new HashMap();
    @DexIgnore
    public /* final */ Ac2 i;
    @DexIgnore
    public /* final */ Map<M62<?>, Boolean> j;
    @DexIgnore
    public /* final */ M62.Ai<? extends Ys3, Gs3> k;
    @DexIgnore
    public volatile D92 l;
    @DexIgnore
    public Z52 m; // = null;
    @DexIgnore
    public int s;
    @DexIgnore
    public /* final */ T82 t;
    @DexIgnore
    public /* final */ R92 u;

    @DexIgnore
    public C92(Context context, T82 t82, Lock lock, Looper looper, D62 d62, Map<M62.Ci<?>, M62.Fi> map, Ac2 ac2, Map<M62<?>, Boolean> map2, M62.Ai<? extends Ys3, Gs3> ai, ArrayList<Wa2> arrayList, R92 r92) {
        this.d = context;
        this.b = lock;
        this.e = d62;
        this.g = map;
        this.i = ac2;
        this.j = map2;
        this.k = ai;
        this.t = t82;
        this.u = r92;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Wa2 wa2 = arrayList.get(i2);
            i2++;
            wa2.a(this);
        }
        this.f = new E92(this, looper);
        this.c = lock.newCondition();
        this.l = new U82(this);
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void a() {
        if (this.l.a()) {
            this.h.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void b() {
        this.l.b();
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final boolean c() {
        return this.l instanceof G82;
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void d(int i2) {
        this.b.lock();
        try {
            this.l.d(i2);
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void e(Bundle bundle) {
        this.b.lock();
        try {
            this.l.e(bundle);
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append("mState=").println(this.l);
        for (M62<?> m62 : this.j.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) m62.b()).println(":");
            this.g.get(m62.a()).f(concat, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final boolean g(T72 t72) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void h() {
    }

    @DexIgnore
    @Override // com.fossil.Va2
    public final void i(Z52 z52, M62<?> m62, boolean z) {
        this.b.lock();
        try {
            this.l.i(z52, m62, z);
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t2) {
        t2.t();
        return (T) this.l.j(t2);
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t2) {
        t2.t();
        return (T) this.l.k(t2);
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final void l() {
        if (c()) {
            ((G82) this.l).g();
        }
    }

    @DexIgnore
    @Override // com.fossil.S92
    public final Z52 m() {
        b();
        while (n()) {
            try {
                this.c.await();
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                return new Z52(15, null);
            }
        }
        if (c()) {
            return Z52.f;
        }
        Z52 z52 = this.m;
        return z52 == null ? new Z52(13, null) : z52;
    }

    @DexIgnore
    public final boolean n() {
        return this.l instanceof H82;
    }

    @DexIgnore
    public final void p(F92 f92) {
        this.f.sendMessage(this.f.obtainMessage(1, f92));
    }

    @DexIgnore
    public final void q(RuntimeException runtimeException) {
        this.f.sendMessage(this.f.obtainMessage(2, runtimeException));
    }

    @DexIgnore
    public final void r() {
        this.b.lock();
        try {
            this.l = new H82(this, this.i, this.j, this.e, this.k, this.b, this.d);
            this.l.f();
            this.c.signalAll();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void s() {
        this.b.lock();
        try {
            this.t.D();
            this.l = new G82(this);
            this.l.f();
            this.c.signalAll();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void u(Z52 z52) {
        this.b.lock();
        try {
            this.m = z52;
            this.l = new U82(this);
            this.l.f();
            this.c.signalAll();
        } finally {
            this.b.unlock();
        }
    }
}
