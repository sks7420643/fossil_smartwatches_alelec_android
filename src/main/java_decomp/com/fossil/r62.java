package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.fossil.Ac2;
import com.fossil.M62;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class R62 {
    @DexIgnore
    public static /* final */ Set<R62> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static final class Ai {
        @DexIgnore
        public Account a;
        @DexIgnore
        public /* final */ Set<Scope> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Scope> c; // = new HashSet();
        @DexIgnore
        public int d;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public /* final */ Map<M62<?>, Ac2.Bi> h; // = new Zi0();
        @DexIgnore
        public /* final */ Context i;
        @DexIgnore
        public /* final */ Map<M62<?>, M62.Di> j; // = new Zi0();
        @DexIgnore
        public N72 k;
        @DexIgnore
        public int l; // = -1;
        @DexIgnore
        public Ci m;
        @DexIgnore
        public Looper n;
        @DexIgnore
        public C62 o; // = C62.q();
        @DexIgnore
        public M62.Ai<? extends Ys3, Gs3> p; // = Vs3.c;
        @DexIgnore
        public /* final */ ArrayList<Bi> q; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Ci> r; // = new ArrayList<>();

        @DexIgnore
        public Ai(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @DexIgnore
        public final Ai a(M62<? extends Object> m62) {
            Rc2.l(m62, "Api must not be null");
            this.j.put(m62, null);
            List<Scope> a2 = m62.c().a(null);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final <O extends M62.Di.Cii> Ai b(M62<O> m62, O o2) {
            Rc2.l(m62, "Api must not be null");
            Rc2.l(o2, "Null options are not permitted for this Api");
            this.j.put(m62, o2);
            List<Scope> a2 = m62.c().a(o2);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final Ai c(M62<? extends Object> m62, Scope... scopeArr) {
            Rc2.l(m62, "Api must not be null");
            this.j.put(m62, null);
            j(m62, null, scopeArr);
            return this;
        }

        @DexIgnore
        public final Ai d(Bi bi) {
            Rc2.l(bi, "Listener must not be null");
            this.q.add(bi);
            return this;
        }

        @DexIgnore
        public final Ai e(Ci ci) {
            Rc2.l(ci, "Listener must not be null");
            this.r.add(ci);
            return this;
        }

        @DexIgnore
        public final Ai f(String[] strArr) {
            for (String str : strArr) {
                this.b.add(new Scope(str));
            }
            return this;
        }

        @DexIgnore
        public final R62 g() {
            Rc2.b(!this.j.isEmpty(), "must call addApi() to add at least one API");
            Ac2 h2 = h();
            Map<M62<?>, Ac2.Bi> g2 = h2.g();
            Zi0 zi0 = new Zi0();
            Zi0 zi02 = new Zi0();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            M62<?> m62 = null;
            for (M62<?> m622 : this.j.keySet()) {
                M62.Di di = this.j.get(m622);
                boolean z2 = g2.get(m622) != null;
                zi0.put(m622, Boolean.valueOf(z2));
                Wa2 wa2 = new Wa2(m622, z2);
                arrayList.add(wa2);
                M62.Ai<?, ?> d2 = m622.d();
                M62.Fi c2 = d2.c(this.i, this.n, h2, di, wa2, wa2);
                zi02.put(m622.a(), c2);
                boolean z3 = d2.b() == 1 ? di != null : z;
                if (!c2.g()) {
                    z = z3;
                } else if (m62 == null) {
                    z = z3;
                    m62 = m622;
                } else {
                    String b2 = m622.b();
                    String b3 = m62.b();
                    StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 21 + String.valueOf(b3).length());
                    sb.append(b2);
                    sb.append(" cannot be used with ");
                    sb.append(b3);
                    throw new IllegalStateException(sb.toString());
                }
            }
            if (m62 != null) {
                if (!z) {
                    Rc2.p(this.a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", m62.b());
                    Rc2.p(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", m62.b());
                } else {
                    String b4 = m62.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b4).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b4);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            T82 t82 = new T82(this.i, new ReentrantLock(), this.n, h2, this.o, this.p, zi0, this.q, this.r, zi02, this.l, T82.x(zi02.values(), true), arrayList, false);
            synchronized (R62.a) {
                R62.a.add(t82);
            }
            if (this.l >= 0) {
                Oa2.q(this.k).s(this.l, t82, this.m);
            }
            return t82;
        }

        @DexIgnore
        public final Ac2 h() {
            Gs3 gs3 = Gs3.k;
            if (this.j.containsKey(Vs3.e)) {
                gs3 = (Gs3) this.j.get(Vs3.e);
            }
            return new Ac2(this.a, this.b, this.h, this.d, this.e, this.f, this.g, gs3, false);
        }

        @DexIgnore
        public final Ai i(Handler handler) {
            Rc2.l(handler, "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }

        @DexIgnore
        public final <O extends M62.Di> void j(M62<O> m62, O o2, Scope... scopeArr) {
            HashSet hashSet = new HashSet(m62.c().a(o2));
            for (Scope scope : scopeArr) {
                hashSet.add(scope);
            }
            this.h.put(m62, new Ac2.Bi(hashSet));
        }
    }

    @DexIgnore
    @Deprecated
    public interface Bi extends K72 {
    }

    @DexIgnore
    @Deprecated
    public interface Ci extends R72 {
    }

    @DexIgnore
    public static Set<R62> k() {
        Set<R62> set;
        synchronized (a) {
            set = a;
        }
        return set;
    }

    @DexIgnore
    public abstract Z52 d();

    @DexIgnore
    public abstract T62<Status> e();

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public abstract void g();

    @DexIgnore
    public abstract void h(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    public <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T i(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Context l() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Looper m() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean n();

    @DexIgnore
    public boolean o(T72 t72) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public void p() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void q(Ci ci);

    @DexIgnore
    public abstract void r(Ci ci);

    @DexIgnore
    public void s(Da2 da2) {
        throw new UnsupportedOperationException();
    }
}
