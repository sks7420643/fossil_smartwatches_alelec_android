package com.fossil;

import android.content.Context;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hw2 {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Hw2(Uri uri) {
        this(null, uri, "", "", false, false, false, false, null);
    }

    @DexIgnore
    public Hw2(String str, Uri uri, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, Rw2<Context, Boolean> rw2) {
        this.a = uri;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final Xv2<Double> a(String str, double d) {
        return Xv2.a(this, str, -3.0d, true);
    }

    @DexIgnore
    public final Xv2<Long> b(String str, long j) {
        return Xv2.b(this, str, j, true);
    }

    @DexIgnore
    public final Xv2<String> c(String str, String str2) {
        return Xv2.c(this, str, str2, true);
    }

    @DexIgnore
    public final Xv2<Boolean> d(String str, boolean z) {
        return Xv2.d(this, str, z, true);
    }
}
