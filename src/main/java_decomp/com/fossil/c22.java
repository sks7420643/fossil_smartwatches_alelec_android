package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C22 implements Factory<B22> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<T02> b;
    @DexIgnore
    public /* final */ Provider<K22> c;
    @DexIgnore
    public /* final */ Provider<H22> d;
    @DexIgnore
    public /* final */ Provider<Executor> e;
    @DexIgnore
    public /* final */ Provider<S32> f;
    @DexIgnore
    public /* final */ Provider<T32> g;

    @DexIgnore
    public C22(Provider<Context> provider, Provider<T02> provider2, Provider<K22> provider3, Provider<H22> provider4, Provider<Executor> provider5, Provider<S32> provider6, Provider<T32> provider7) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static C22 a(Provider<Context> provider, Provider<T02> provider2, Provider<K22> provider3, Provider<H22> provider4, Provider<Executor> provider5, Provider<S32> provider6, Provider<T32> provider7) {
        return new C22(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    public B22 b() {
        return new B22(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
