package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ki extends Qq7 implements Hg6<Lp, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Il b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ki(Il il) {
        super(1);
        this.b = il;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Lp lp) {
        T t;
        Ac0[] a0 = ((Oj) lp).a0();
        for (Ac0 ac0 : a0) {
            if (ac0.b == Jw1.WATCH_APP) {
                Iterator<T> it = this.b.E.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.getBundleId(), ac0.a())) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 == null) {
                    this.b.D.add(ac0);
                } else if (t2.getPackageCrc() == ac0.b()) {
                    this.b.E.remove(t2);
                    this.b.C.add(t2);
                }
            }
        }
        Il il = this.b;
        il.G = 0.9f / ((float) il.E.size());
        Il.I(this.b);
        return Cd6.a;
    }
}
