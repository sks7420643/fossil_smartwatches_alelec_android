package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jh2 extends Rl2 implements Kh2 {
    @DexIgnore
    public Jh2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @DexIgnore
    @Override // com.fossil.Kh2
    public final int L1(Rg2 rg2, String str, boolean z) throws RemoteException {
        Parcel d = d();
        Sl2.c(d, rg2);
        d.writeString(str);
        Sl2.a(d, z);
        Parcel e = e(3, d);
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.Kh2
    public final Rg2 V1(Rg2 rg2, String str, int i) throws RemoteException {
        Parcel d = d();
        Sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        Parcel e = e(4, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Kh2
    public final int f2() throws RemoteException {
        Parcel e = e(6, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.Kh2
    public final int m1(Rg2 rg2, String str, boolean z) throws RemoteException {
        Parcel d = d();
        Sl2.c(d, rg2);
        d.writeString(str);
        Sl2.a(d, z);
        Parcel e = e(5, d);
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.Kh2
    public final Rg2 w0(Rg2 rg2, String str, int i) throws RemoteException {
        Parcel d = d();
        Sl2.c(d, rg2);
        d.writeString(str);
        d.writeInt(i);
        Parcel e = e(2, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
