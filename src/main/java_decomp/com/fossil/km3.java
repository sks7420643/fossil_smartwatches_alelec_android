package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Km3 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ Im3 b;

    @DexIgnore
    public Km3(Im3 im3, String str) {
        this.b = im3;
        Rc2.k(str);
        this.a = str;
    }

    @DexIgnore
    public final void uncaughtException(Thread thread, Throwable th) {
        synchronized (this) {
            this.b.d().F().b(this.a, th);
        }
    }
}
