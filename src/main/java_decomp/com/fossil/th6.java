package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Th6 implements Factory<Bi6> {
    @DexIgnore
    public static Bi6 a(Qh6 qh6) {
        Bi6 c = qh6.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
