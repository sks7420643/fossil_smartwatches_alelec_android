package com.fossil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wj0 {
    @DexIgnore
    public List<Uj0> a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public /* final */ int[] e; // = {-1, -1};
    @DexIgnore
    public List<Uj0> f; // = new ArrayList();
    @DexIgnore
    public List<Uj0> g; // = new ArrayList();
    @DexIgnore
    public HashSet<Uj0> h; // = new HashSet<>();
    @DexIgnore
    public HashSet<Uj0> i; // = new HashSet<>();
    @DexIgnore
    public List<Uj0> j; // = new ArrayList();
    @DexIgnore
    public List<Uj0> k; // = new ArrayList();

    @DexIgnore
    public Wj0(List<Uj0> list) {
        this.a = list;
    }

    @DexIgnore
    public Wj0(List<Uj0> list, boolean z) {
        this.a = list;
        this.d = z;
    }

    @DexIgnore
    public void a(Uj0 uj0, int i2) {
        if (i2 == 0) {
            this.h.add(uj0);
        } else if (i2 == 1) {
            this.i.add(uj0);
        }
    }

    @DexIgnore
    public List<Uj0> b(int i2) {
        if (i2 == 0) {
            return this.f;
        }
        if (i2 == 1) {
            return this.g;
        }
        return null;
    }

    @DexIgnore
    public Set<Uj0> c(int i2) {
        if (i2 == 0) {
            return this.h;
        }
        if (i2 == 1) {
            return this.i;
        }
        return null;
    }

    @DexIgnore
    public List<Uj0> d() {
        if (!this.j.isEmpty()) {
            return this.j;
        }
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            Uj0 uj0 = this.a.get(i2);
            if (!uj0.b0) {
                e((ArrayList) this.j, uj0);
            }
        }
        this.k.clear();
        this.k.addAll(this.a);
        this.k.removeAll(this.j);
        return this.j;
    }

    @DexIgnore
    public final void e(ArrayList<Uj0> arrayList, Uj0 uj0) {
        if (!uj0.d0) {
            arrayList.add(uj0);
            uj0.d0 = true;
            if (!uj0.L()) {
                if (uj0 instanceof Yj0) {
                    Yj0 yj0 = (Yj0) uj0;
                    int i2 = yj0.l0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        e(arrayList, yj0.k0[i3]);
                    }
                }
                int length = uj0.A.length;
                for (int i4 = 0; i4 < length; i4++) {
                    Tj0 tj0 = uj0.A[i4].d;
                    if (tj0 != null) {
                        Uj0 uj02 = tj0.b;
                        if (!(tj0 == null || uj02 == uj0.u())) {
                            e(arrayList, uj02);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void f(com.fossil.Uj0 r7) {
        /*
        // Method dump skipped, instructions count: 225
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Wj0.f(com.fossil.Uj0):void");
    }

    @DexIgnore
    public void g() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            f(this.k.get(i2));
        }
    }
}
