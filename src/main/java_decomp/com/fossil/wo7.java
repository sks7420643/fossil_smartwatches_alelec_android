package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wo7 implements Ts7<File> {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ Yo7 b;
    @DexIgnore
    public /* final */ Hg6<File, Boolean> c;
    @DexIgnore
    public /* final */ Hg6<File, Cd6> d;
    @DexIgnore
    public /* final */ Coroutine<File, IOException, Cd6> e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai extends Ci {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(File file) {
            super(file);
            Wg6.c(file, "rootDir");
            if (Vl7.a) {
                boolean isDirectory = file.isDirectory();
                if (Vl7.a && !isDirectory) {
                    throw new AssertionError("rootDir must be verified to be directory beforehand.");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends Yl7<File> {
        @DexIgnore
        public /* final */ ArrayDeque<Ci> d; // = new ArrayDeque<>();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Aii extends Ai {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public File[] c;
            @DexIgnore
            public int d;
            @DexIgnore
            public boolean e;
            @DexIgnore
            public /* final */ /* synthetic */ Bi f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, File file) {
                super(file);
                Wg6.c(file, "rootDir");
                this.f = bi;
            }

            @DexIgnore
            @Override // com.fossil.Wo7.Ci
            public File b() {
                if (!this.e && this.c == null) {
                    Hg6 hg6 = Wo7.this.c;
                    if (hg6 != null && !((Boolean) hg6.invoke(a())).booleanValue()) {
                        return null;
                    }
                    File[] listFiles = a().listFiles();
                    this.c = listFiles;
                    if (listFiles == null) {
                        Coroutine coroutine = Wo7.this.e;
                        if (coroutine != null) {
                            Cd6 cd6 = (Cd6) coroutine.invoke(a(), new Qo7(a(), null, "Cannot list files in a directory", 2, null));
                        }
                        this.e = true;
                    }
                }
                File[] fileArr = this.c;
                if (fileArr != null) {
                    int i = this.d;
                    if (fileArr == null) {
                        Wg6.i();
                        throw null;
                    } else if (i < fileArr.length) {
                        if (fileArr != null) {
                            this.d = i + 1;
                            return fileArr[i];
                        }
                        Wg6.i();
                        throw null;
                    }
                }
                if (!this.b) {
                    this.b = true;
                    return a();
                }
                Hg6 hg62 = Wo7.this.d;
                if (hg62 == null) {
                    return null;
                }
                Cd6 cd62 = (Cd6) hg62.invoke(a());
                return null;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Bii extends Ci {
            @DexIgnore
            public boolean b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, File file) {
                super(file);
                Wg6.c(file, "rootFile");
                if (Vl7.a) {
                    boolean isFile = file.isFile();
                    if (Vl7.a && !isFile) {
                        throw new AssertionError("rootFile must be verified to be file beforehand.");
                    }
                }
            }

            @DexIgnore
            @Override // com.fossil.Wo7.Ci
            public File b() {
                if (this.b) {
                    return null;
                }
                this.b = true;
                return a();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Cii extends Ai {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public File[] c;
            @DexIgnore
            public int d;
            @DexIgnore
            public /* final */ /* synthetic */ Bi e;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Bi bi, File file) {
                super(file);
                Wg6.c(file, "rootDir");
                this.e = bi;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
                if (r0.length == 0) goto L_0x006d;
             */
            @DexIgnore
            @Override // com.fossil.Wo7.Ci
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.io.File b() {
                /*
                    r8 = this;
                    r2 = 0
                    boolean r0 = r8.b
                    if (r0 != 0) goto L_0x0028
                    com.fossil.Wo7$Bi r0 = r8.e
                    com.fossil.Wo7 r0 = com.fossil.Wo7.this
                    com.mapped.Hg6 r0 = com.fossil.Wo7.c(r0)
                    if (r0 == 0) goto L_0x0020
                    java.io.File r1 = r8.a()
                    java.lang.Object r0 = r0.invoke(r1)
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x0020
                L_0x001f:
                    return r2
                L_0x0020:
                    r0 = 1
                    r8.b = r0
                    java.io.File r2 = r8.a()
                    goto L_0x001f
                L_0x0028:
                    java.io.File[] r0 = r8.c
                    if (r0 == 0) goto L_0x0033
                    int r1 = r8.d
                    if (r0 == 0) goto L_0x0097
                    int r0 = r0.length
                    if (r1 >= r0) goto L_0x0082
                L_0x0033:
                    java.io.File[] r0 = r8.c
                    if (r0 != 0) goto L_0x009f
                    java.io.File r0 = r8.a()
                    java.io.File[] r0 = r0.listFiles()
                    r8.c = r0
                    if (r0 != 0) goto L_0x0064
                    com.fossil.Wo7$Bi r0 = r8.e
                    com.fossil.Wo7 r0 = com.fossil.Wo7.this
                    com.mapped.Coroutine r6 = com.fossil.Wo7.d(r0)
                    if (r6 == 0) goto L_0x0064
                    java.io.File r7 = r8.a()
                    com.fossil.Qo7 r0 = new com.fossil.Qo7
                    java.io.File r1 = r8.a()
                    java.lang.String r3 = "Cannot list files in a directory"
                    r4 = 2
                    r5 = r2
                    r0.<init>(r1, r2, r3, r4, r5)
                    java.lang.Object r0 = r6.invoke(r7, r0)
                    com.mapped.Cd6 r0 = (com.mapped.Cd6) r0
                L_0x0064:
                    java.io.File[] r0 = r8.c
                    if (r0 == 0) goto L_0x006d
                    if (r0 == 0) goto L_0x009b
                    int r0 = r0.length
                    if (r0 != 0) goto L_0x009f
                L_0x006d:
                    com.fossil.Wo7$Bi r0 = r8.e
                    com.fossil.Wo7 r0 = com.fossil.Wo7.this
                    com.mapped.Hg6 r0 = com.fossil.Wo7.e(r0)
                    if (r0 == 0) goto L_0x001f
                    java.io.File r1 = r8.a()
                    java.lang.Object r0 = r0.invoke(r1)
                    com.mapped.Cd6 r0 = (com.mapped.Cd6) r0
                    goto L_0x001f
                L_0x0082:
                    com.fossil.Wo7$Bi r0 = r8.e
                    com.fossil.Wo7 r0 = com.fossil.Wo7.this
                    com.mapped.Hg6 r0 = com.fossil.Wo7.e(r0)
                    if (r0 == 0) goto L_0x001f
                    java.io.File r1 = r8.a()
                    java.lang.Object r0 = r0.invoke(r1)
                    com.mapped.Cd6 r0 = (com.mapped.Cd6) r0
                    goto L_0x001f
                L_0x0097:
                    com.mapped.Wg6.i()
                    throw r2
                L_0x009b:
                    com.mapped.Wg6.i()
                    throw r2
                L_0x009f:
                    java.io.File[] r0 = r8.c
                    if (r0 == 0) goto L_0x00ad
                    int r1 = r8.d
                    int r2 = r1 + 1
                    r8.d = r2
                    r2 = r0[r1]
                    goto L_0x001f
                L_0x00ad:
                    com.mapped.Wg6.i()
                    throw r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.Wo7.Bi.Cii.b():java.io.File");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
            if (Wo7.this.a.isDirectory()) {
                this.d.push(f(Wo7.this.a));
            } else if (Wo7.this.a.isFile()) {
                this.d.push(new Bii(this, Wo7.this.a));
            } else {
                b();
            }
        }

        @DexIgnore
        @Override // com.fossil.Yl7
        public void a() {
            File g = g();
            if (g != null) {
                c(g);
            } else {
                b();
            }
        }

        @DexIgnore
        public final Ai f(File file) {
            int i = Xo7.a[Wo7.this.b.ordinal()];
            if (i == 1) {
                return new Cii(this, file);
            }
            if (i == 2) {
                return new Aii(this, file);
            }
            throw new Kc6();
        }

        @DexIgnore
        public final File g() {
            File b;
            while (true) {
                Ci peek = this.d.peek();
                if (peek == null) {
                    return null;
                }
                b = peek.b();
                if (b == null) {
                    this.d.pop();
                } else if (!Wg6.a(b, peek.a()) && b.isDirectory() && this.d.size() < Wo7.this.f) {
                    this.d.push(f(b));
                }
            }
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public Ci(File file) {
            Wg6.c(file, "root");
            this.a = file;
        }

        @DexIgnore
        public final File a() {
            return this.a;
        }

        @DexIgnore
        public abstract File b();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Wo7(File file, Yo7 yo7) {
        this(file, yo7, null, null, null, 0, 32, null);
        Wg6.c(file, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Wg6.c(yo7, "direction");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Hg6<? super java.io.File, java.lang.Boolean> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.mapped.Hg6<? super java.io.File, com.mapped.Cd6> */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.mapped.Coroutine<? super java.io.File, ? super java.io.IOException, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Wo7(File file, Yo7 yo7, Hg6<? super File, Boolean> hg6, Hg6<? super File, Cd6> hg62, Coroutine<? super File, ? super IOException, Cd6> coroutine, int i) {
        this.a = file;
        this.b = yo7;
        this.c = hg6;
        this.d = hg62;
        this.e = coroutine;
        this.f = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Wo7(File file, Yo7 yo7, Hg6 hg6, Hg6 hg62, Coroutine coroutine, int i, int i2, Qg6 qg6) {
        this(file, (i2 & 2) != 0 ? Yo7.TOP_DOWN : yo7, hg6, hg62, coroutine, (i2 & 32) != 0 ? Integer.MAX_VALUE : i);
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<File> iterator() {
        return new Bi();
    }
}
