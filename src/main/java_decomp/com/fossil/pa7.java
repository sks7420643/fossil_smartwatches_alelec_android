package com.fossil;

import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.watchface.faces.WatchFaceListViewModel;
import com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pa7 implements Factory<WatchFaceListViewModel> {
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<PreviewWatchFaceUseCase> d;

    @DexIgnore
    public Pa7(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PreviewWatchFaceUseCase> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static Pa7 a(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PreviewWatchFaceUseCase> provider4) {
        return new Pa7(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static WatchFaceListViewModel c(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, DianaPresetRepository dianaPresetRepository, PreviewWatchFaceUseCase previewWatchFaceUseCase) {
        return new WatchFaceListViewModel(dianaWatchFaceRepository, fileRepository, dianaPresetRepository, previewWatchFaceUseCase);
    }

    @DexIgnore
    public WatchFaceListViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
