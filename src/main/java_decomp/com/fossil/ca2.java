package com.fossil;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ca2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Z62 b;
    @DexIgnore
    public /* final */ /* synthetic */ Da2 c;

    @DexIgnore
    public Ca2(Da2 da2, Z62 z62) {
        this.c = da2;
        this.b = z62;
    }

    @DexIgnore
    public final void run() {
        try {
            BasePendingResult.p.set(Boolean.TRUE);
            this.c.g.sendMessage(this.c.g.obtainMessage(0, this.c.a.b(this.b)));
            BasePendingResult.p.set(Boolean.FALSE);
            Da2 da2 = this.c;
            Da2.c(this.b);
            R62 r62 = (R62) this.c.f.get();
            if (r62 != null) {
                r62.s(this.c);
            }
        } catch (RuntimeException e) {
            this.c.g.sendMessage(this.c.g.obtainMessage(1, e));
            BasePendingResult.p.set(Boolean.FALSE);
            Da2 da22 = this.c;
            Da2.c(this.b);
            R62 r622 = (R62) this.c.f.get();
            if (r622 != null) {
                r622.s(this.c);
            }
        } catch (Throwable th) {
            BasePendingResult.p.set(Boolean.FALSE);
            Da2 da23 = this.c;
            Da2.c(this.b);
            R62 r623 = (R62) this.c.f.get();
            if (r623 != null) {
                r623.s(this.c);
            }
            throw th;
        }
    }
}
