package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mh6 extends jh6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ LiveData<h47<GoalTrackingSummary>> i;
    @DexIgnore
    public /* final */ LiveData<h47<List<GoalTrackingData>>> j;
    @DexIgnore
    public /* final */ kh6 k;
    @DexIgnore
    public /* final */ on5 l;
    @DexIgnore
    public /* final */ GoalTrackingRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mh6 f2384a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mh6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$mGoalTrackingData$1$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {41, 41}, m = "invokeSuspend")
        /* renamed from: com.fossil.mh6$a$a  reason: collision with other inner class name */
        public static final class C0156a extends ko7 implements vp7<hs0<h47<? extends List<GoalTrackingData>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0156a(a aVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0156a aVar = new C0156a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<GoalTrackingData>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0156a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.mh6$a r1 = r8.this$0
                    com.fossil.mh6 r1 = r1.f2384a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.fossil.mh6.q(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getGoalTrackingDataList(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.mh6.a.C0156a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public a(mh6 mh6) {
            this.f2384a = mh6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<GoalTrackingData>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "mGoalTrackingData onDateChange " + date);
            return or0.c(null, 0, new C0156a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mh6 f2385a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$mGoalTrackingSummary$1$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {35, 35}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends GoalTrackingSummary>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends GoalTrackingSummary>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.mh6$b r1 = r7.this$0
                    com.fossil.mh6 r1 = r1.f2385a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.fossil.mh6.q(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.mh6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(mh6 mh6) {
            this.f2385a = mh6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<GoalTrackingSummary>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "mGoalTrackingSummary onDateChange " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {101, 104}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$maxValue$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(ArrayList arrayList, qn7 qn7) {
                super(2, qn7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$data, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> d;
                ArrayList<BarChart.b> arrayList;
                int i = 0;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    Iterator it = this.$data.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        Object next = it.next();
                        if (!it.hasNext()) {
                            obj2 = next;
                        } else {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).d().get(0);
                            pq7.b(arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 = ao7.e(it2.next().e()).intValue() + i2;
                            }
                            Integer e = ao7.e(i2);
                            while (true) {
                                next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).d().get(0);
                                pq7.b(arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 = ao7.e(it3.next().e()).intValue() + i3;
                                }
                                e = ao7.e(i3);
                                if (e.compareTo(e) >= 0) {
                                    e = e;
                                    next = next;
                                }
                                if (!it.hasNext()) {
                                    break;
                                }
                            }
                            obj2 = next;
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (d = aVar.d()) == null || (arrayList = d.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += ao7.e(it4.next().e()).intValue();
                    }
                    return ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$pair$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    cn6 cn6 = cn6.f632a;
                    Date date = this.this$0.this$0.e;
                    if (date != null) {
                        h47 h47 = (h47) this.this$0.this$0.j.e();
                        return cn6.c(date, h47 != null ? (List) h47.c() : null);
                    }
                    pq7.i();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mh6 mh6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mh6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00bc  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00da  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 231
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mh6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<h47<? extends GoalTrackingSummary>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mh6 f2386a;

        @DexIgnore
        public d(mh6 mh6) {
            this.f2386a = mh6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<GoalTrackingSummary> h47) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "start - mGoalTrackingSummary -- summary=" + h47);
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                this.f2386a.g = true;
                if (this.f2386a.g && this.f2386a.h) {
                    this.f2386a.A();
                }
                this.f2386a.k.x(!this.f2386a.l.g0());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<h47<? extends List<GoalTrackingData>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mh6 f2387a;

        @DexIgnore
        public e(mh6 mh6) {
            this.f2387a = mh6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<GoalTrackingData>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingData -- goalTrackingSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewDayPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                this.f2387a.h = true;
                if (this.f2387a.g && this.f2387a.h) {
                    this.f2387a.A();
                }
                this.f2387a.k.x(!this.f2387a.l.g0());
            }
        }
    }

    @DexIgnore
    public mh6(kh6 kh6, on5 on5, GoalTrackingRepository goalTrackingRepository) {
        pq7.c(kh6, "mView");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        this.k = kh6;
        this.l = on5;
        this.m = goalTrackingRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<h47<GoalTrackingSummary>> c2 = ss0.c(mutableLiveData, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.i = c2;
        LiveData<h47<List<GoalTrackingData>>> c3 = ss0.c(this.f, new a(this));
        pq7.b(c3, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.j = c3;
    }

    @DexIgnore
    public final xw7 A() {
        return gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !lk5.p0(date).booleanValue()) {
            this.g = false;
            this.h = false;
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayPresenter", "loadData - mDate=" + this.e);
        LiveData<h47<GoalTrackingSummary>> liveData = this.i;
        kh6 kh6 = this.k;
        if (kh6 != null) {
            liveData.h((lh6) kh6, new d(this));
            this.j.h((LifecycleOwner) this.k, new e(this));
            this.k.x(!this.l.g0());
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", "stop");
        try {
            LiveData<h47<List<GoalTrackingData>>> liveData = this.j;
            kh6 kh6 = this.k;
            if (kh6 != null) {
                liveData.n((lh6) kh6);
                this.i.n((LifecycleOwner) this.k);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public void z() {
        this.k.M5(this);
    }
}
