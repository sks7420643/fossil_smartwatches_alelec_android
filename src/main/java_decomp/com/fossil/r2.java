package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R2 implements Parcelable.Creator<S2> {
    @DexIgnore
    public /* synthetic */ R2(Qg6 qg6) {
    }

    @DexIgnore
    public final S2 a(byte b, byte[] bArr) {
        return new S2(b, new Bv1(bArr));
    }

    @DexIgnore
    public S2 b(Parcel parcel) {
        return new S2(parcel, (Qg6) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public S2 createFromParcel(Parcel parcel) {
        return new S2(parcel, (Qg6) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public S2[] newArray(int i) {
        return new S2[i];
    }
}
