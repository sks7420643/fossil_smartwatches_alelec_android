package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Tb extends Enum<Tb> {
    @DexIgnore
    public static /* final */ Tb c;
    @DexIgnore
    public static /* final */ Tb d;
    @DexIgnore
    public static /* final */ Tb e;
    @DexIgnore
    public static /* final */ Tb f;
    @DexIgnore
    public static /* final */ Tb g;
    @DexIgnore
    public static /* final */ Tb h;
    @DexIgnore
    public static /* final */ Tb i;
    @DexIgnore
    public static /* final */ /* synthetic */ Tb[] j;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Tb tb = new Tb("APPLICATION_NAME", 0, (byte) 1);
        Tb tb2 = new Tb("SENDER_NAME", 1, (byte) 2);
        c = tb2;
        Tb tb3 = new Tb("APP_BUNDLE_CRC32", 2, (byte) 4);
        d = tb3;
        Tb tb4 = new Tb("GROUP_ID", 3, (byte) 128);
        e = tb4;
        Tb tb5 = new Tb("APP_DISPLAY_NAME", 4, (byte) 129);
        Tb tb6 = new Tb("ICON_IMAGE", 5, (byte) 130);
        f = tb6;
        Tb tb7 = new Tb("PRIORITY", 6, (byte) 193);
        g = tb7;
        Tb tb8 = new Tb("HAND_MOVING", 7, (byte) 194);
        h = tb8;
        Tb tb9 = new Tb("VIBE", 8, (byte) 195);
        i = tb9;
        j = new Tb[]{tb, tb2, tb3, tb4, tb5, tb6, tb7, tb8, tb9};
    }
    */

    @DexIgnore
    public Tb(String str, int i2, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Tb valueOf(String str) {
        return (Tb) Enum.valueOf(Tb.class, str);
    }

    @DexIgnore
    public static Tb[] values() {
        return (Tb[]) j.clone();
    }
}
