package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wr extends Vr {
    @DexIgnore
    public /* final */ int g; // = 1;

    @DexIgnore
    public Wr(byte[] bArr, int i, N6 n6) {
        super(bArr, i, n6);
    }

    @DexIgnore
    @Override // com.fossil.Vr
    public int b() {
        return this.g;
    }
}
