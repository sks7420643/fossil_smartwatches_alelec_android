package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M90 {
    @DexIgnore
    public /* synthetic */ M90(Qg6 qg6) {
    }

    @DexIgnore
    public final N90 a(String str) {
        N90[] values = N90.values();
        for (N90 n90 : values) {
            if (Wg6.a(n90.b, str)) {
                return n90;
            }
        }
        return null;
    }
}
