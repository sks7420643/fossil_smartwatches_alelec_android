package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H08 extends I08 {
    @DexIgnore
    public static /* final */ Dv7 h;
    @DexIgnore
    public static /* final */ H08 i;

    /*
    static {
        H08 h08 = new H08();
        i = h08;
        h = h08.T(Wz7.f("kotlinx.coroutines.io.parallelism", Bs7.d(64, Wz7.a()), 0, 0, 12, null));
    }
    */

    @DexIgnore
    public H08() {
        super(0, 0, null, 7, null);
    }

    @DexIgnore
    public final Dv7 b0() {
        return h;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new UnsupportedOperationException("DefaultDispatcher cannot be closed");
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        return "DefaultDispatcher";
    }
}
