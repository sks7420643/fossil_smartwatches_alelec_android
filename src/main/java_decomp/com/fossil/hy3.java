package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hy3 implements Comparable<Hy3>, Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Hy3> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ Calendar b;
    @DexIgnore
    public /* final */ String c; // = Ny3.n().format(this.b.getTime());
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e; // = this.b.get(1);
    @DexIgnore
    public /* final */ int f; // = this.b.getMaximum(7);
    @DexIgnore
    public /* final */ int g; // = this.b.getActualMaximum(5);
    @DexIgnore
    public /* final */ long h; // = this.b.getTimeInMillis();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Hy3> {
        @DexIgnore
        public Hy3 a(Parcel parcel) {
            return Hy3.b(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        public Hy3[] b(int i) {
            return new Hy3[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Hy3 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Hy3[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public Hy3(Calendar calendar) {
        calendar.set(5, 1);
        Calendar d2 = Ny3.d(calendar);
        this.b = d2;
        this.d = d2.get(2);
    }

    @DexIgnore
    public static Hy3 b(int i, int i2) {
        Calendar k = Ny3.k();
        k.set(1, i);
        k.set(2, i2);
        return new Hy3(k);
    }

    @DexIgnore
    public static Hy3 c(long j) {
        Calendar k = Ny3.k();
        k.setTimeInMillis(j);
        return new Hy3(k);
    }

    @DexIgnore
    public static Hy3 n() {
        return new Hy3(Ny3.i());
    }

    @DexIgnore
    public int a(Hy3 hy3) {
        return this.b.compareTo(hy3.b);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Hy3 hy3) {
        return a(hy3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int e() {
        int firstDayOfWeek = this.b.get(7) - this.b.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.f : firstDayOfWeek;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Hy3)) {
            return false;
        }
        Hy3 hy3 = (Hy3) obj;
        return this.d == hy3.d && this.e == hy3.e;
    }

    @DexIgnore
    public long f(int i) {
        Calendar d2 = Ny3.d(this.b);
        d2.set(5, i);
        return d2.getTimeInMillis();
    }

    @DexIgnore
    public String h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.d), Integer.valueOf(this.e)});
    }

    @DexIgnore
    public long i() {
        return this.b.getTimeInMillis();
    }

    @DexIgnore
    public Hy3 k(int i) {
        Calendar d2 = Ny3.d(this.b);
        d2.add(2, i);
        return new Hy3(d2);
    }

    @DexIgnore
    public int m(Hy3 hy3) {
        if (this.b instanceof GregorianCalendar) {
            return ((hy3.e - this.e) * 12) + (hy3.d - this.d);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.e);
        parcel.writeInt(this.d);
    }
}
