package com.fossil;

import android.util.Log;
import com.fossil.Af1;
import com.fossil.Wb1;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qe1 implements Af1<File, ByteBuffer> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Wb1<ByteBuffer> {
        @DexIgnore
        public /* final */ File b;

        @DexIgnore
        public Ai(File file) {
            this.b = file;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super ByteBuffer> ai) {
            try {
                ai.e(Zj1.a(this.b));
            } catch (IOException e) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                ai.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Bf1<File, ByteBuffer> {
        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<File, ByteBuffer> b(Ef1 ef1) {
            return new Qe1();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(File file) {
        return d(file);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<ByteBuffer> b(File file, int i, int i2, Ob1 ob1) {
        return c(file, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<ByteBuffer> c(File file, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(file), new Ai(file));
    }

    @DexIgnore
    public boolean d(File file) {
        return true;
    }
}
