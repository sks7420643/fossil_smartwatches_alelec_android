package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cy3 extends BaseAdapter {
    @DexIgnore
    public static /* final */ int e; // = (Build.VERSION.SDK_INT >= 26 ? 4 : 1);
    @DexIgnore
    public /* final */ Calendar b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d; // = this.b.getFirstDayOfWeek();

    @DexIgnore
    public Cy3() {
        Calendar k = Ny3.k();
        this.b = k;
        this.c = k.getMaximum(7);
    }

    @DexIgnore
    public Integer a(int i) {
        if (i >= this.c) {
            return null;
        }
        return Integer.valueOf(b(i));
    }

    @DexIgnore
    public final int b(int i) {
        int i2 = this.d + i;
        int i3 = this.c;
        return i2 > i3 ? i2 - i3 : i2;
    }

    @DexIgnore
    public int getCount() {
        return this.c;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return a(i);
    }

    @DexIgnore
    public long getItemId(int i) {
        return 0;
    }

    @DexIgnore
    @SuppressLint({"WrongConstant"})
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) view;
        if (view == null) {
            textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(Pw3.mtrl_calendar_day_of_week, viewGroup, false);
        }
        this.b.set(7, b(i));
        textView.setText(this.b.getDisplayName(7, e, Locale.getDefault()));
        textView.setContentDescription(String.format(viewGroup.getContext().getString(Rw3.mtrl_picker_day_of_week_column_header), this.b.getDisplayName(7, 2, Locale.getDefault())));
        return textView;
    }
}
