package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gn0 {
    @DexIgnore
    public static /* final */ Fn0 a; // = new Ei(null, false);
    @DexIgnore
    public static /* final */ Fn0 b; // = new Ei(null, true);
    @DexIgnore
    public static /* final */ Fn0 c; // = new Ei(Bi.a, false);
    @DexIgnore
    public static /* final */ Fn0 d; // = new Ei(Bi.a, true);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Ci {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai(true);
        @DexIgnore
        public /* final */ boolean a;

        @DexIgnore
        public Ai(boolean z) {
            this.a = z;
        }

        @DexIgnore
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:20:0x0018 */
        @Override // com.fossil.Gn0.Ci
        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i;
            boolean z = false;
            while (i3 < i2 + i) {
                int a2 = Gn0.a(Character.getDirectionality(charSequence.charAt(i3)));
                if (a2 == 0) {
                    if (this.a) {
                        return 0;
                    }
                    z = true;
                } else if (a2 == 1) {
                    if (!this.a) {
                        return 1;
                    }
                    z = true;
                } else {
                    continue;
                }
                i3++;
                z = z;
            }
            if (z) {
                return this.a ? 1 : 0;
            }
            return 2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Ci {
        @DexIgnore
        public static /* final */ Bi a; // = new Bi();

        @DexIgnore
        @Override // com.fossil.Gn0.Ci
        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = 2;
            for (int i4 = i; i4 < i2 + i && i3 == 2; i4++) {
                i3 = Gn0.b(Character.getDirectionality(charSequence.charAt(i4)));
            }
            return i3;
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        int a(CharSequence charSequence, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Di implements Fn0 {
        @DexIgnore
        public /* final */ Ci a;

        @DexIgnore
        public Di(Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        @Override // com.fossil.Fn0
        public boolean a(CharSequence charSequence, int i, int i2) {
            if (charSequence != null && i >= 0 && i2 >= 0 && charSequence.length() - i2 >= i) {
                return this.a == null ? b() : c(charSequence, i, i2);
            }
            throw new IllegalArgumentException();
        }

        @DexIgnore
        public abstract boolean b();

        @DexIgnore
        public final boolean c(CharSequence charSequence, int i, int i2) {
            int a2 = this.a.a(charSequence, i, i2);
            if (a2 == 0) {
                return true;
            }
            if (a2 != 1) {
                return b();
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends Di {
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ei(Ci ci, boolean z) {
            super(ci);
            this.b = z;
        }

        @DexIgnore
        @Override // com.fossil.Gn0.Di
        public boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi extends Di {
        @DexIgnore
        public static /* final */ Fi b; // = new Fi();

        @DexIgnore
        public Fi() {
            super(null);
        }

        @DexIgnore
        @Override // com.fossil.Gn0.Di
        public boolean b() {
            return Hn0.b(Locale.getDefault()) == 1;
        }
    }

    /*
    static {
        Ai ai = Ai.b;
        Fi fi = Fi.b;
    }
    */

    @DexIgnore
    public static int a(int i) {
        if (i != 0) {
            return (i == 1 || i == 2) ? 0 : 2;
        }
        return 1;
    }

    @DexIgnore
    public static int b(int i) {
        if (i != 0) {
            if (!(i == 1 || i == 2)) {
                switch (i) {
                    case 14:
                    case 15:
                        break;
                    case 16:
                    case 17:
                        break;
                    default:
                        return 2;
                }
            }
            return 0;
        }
        return 1;
    }
}
