package com.fossil;

import com.facebook.stetho.dumpapp.Framer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Di7 extends Bi7 {
    @DexIgnore
    public static /* final */ byte[] j; // = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, Framer.EXIT_FRAME_PREFIX, 121, 122, 48, Framer.STDOUT_FRAME_PREFIX, Framer.STDERR_FRAME_PREFIX, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    @DexIgnore
    public static /* final */ byte[] k; // = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, Framer.EXIT_FRAME_PREFIX, 121, 122, 48, Framer.STDOUT_FRAME_PREFIX, Framer.STDERR_FRAME_PREFIX, 51, 52, 53, 54, 55, 56, 57, Framer.STDIN_FRAME_PREFIX, Framer.STDIN_REQUEST_FRAME_PREFIX};
    @DexIgnore
    public static /* final */ /* synthetic */ boolean l; // = (!Ai7.class.desiredAssertionStatus());
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ byte[] i;

    @DexIgnore
    public Di7(int i2, byte[] bArr) {
        boolean z = true;
        this.a = bArr;
        this.f = (i2 & 1) == 0;
        this.g = (i2 & 2) == 0;
        this.h = (i2 & 4) == 0 ? false : z;
        this.i = (i2 & 8) == 0 ? j : k;
        this.c = new byte[2];
        this.d = 0;
        this.e = this.g ? 19 : -1;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0108 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0251  */
    public boolean a(byte[] r12, int r13, int r14, boolean r15) {
        /*
        // Method dump skipped, instructions count: 604
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Di7.a(byte[], int, int, boolean):boolean");
    }
}
