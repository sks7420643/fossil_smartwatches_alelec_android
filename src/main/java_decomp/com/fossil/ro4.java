package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.workers.TimeChangeReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ro4 {
    @DexIgnore
    void A(DebugActivity debugActivity);

    @DexIgnore
    void A0(NetworkChangedReceiver networkChangedReceiver);

    @DexIgnore
    void A1(FossilNotificationListenerService fossilNotificationListenerService);

    @DexIgnore
    w06 B(b16 b16);

    @DexIgnore
    ou4 B0();

    @DexIgnore
    hx4 B1();

    @DexIgnore
    wr6 C(zr6 zr6);

    @DexIgnore
    fv4 C0();

    @DexIgnore
    fx6 C1(ix6 ix6);

    @DexIgnore
    qr6 D(tr6 tr6);

    @DexIgnore
    bt6 D0(et6 et6);

    @DexIgnore
    rv4 D1();

    @DexIgnore
    vd6 E(de6 de6);

    @DexIgnore
    ew4 E0();

    @DexIgnore
    wx6 E1(zx6 zx6);

    @DexIgnore
    nx4 F();

    @DexIgnore
    xw6 F0(ax6 ax6);

    @DexIgnore
    dp6 F1();

    @DexIgnore
    pc6 G(tc6 tc6);

    @DexIgnore
    e87 G0();

    @DexIgnore
    wp6 G1(zp6 zp6);

    @DexIgnore
    r07 H(v07 v07);

    @DexIgnore
    void H0(PortfolioApp portfolioApp);

    @DexIgnore
    e07 H1(f07 f07);

    @DexIgnore
    void I(n47 n47);

    @DexIgnore
    yz5 I0(d06 d06);

    @DexIgnore
    void I1(sn5 sn5);

    @DexIgnore
    s86 J(v86 v86);

    @DexIgnore
    void J0(TimeChangeReceiver timeChangeReceiver);

    @DexIgnore
    ma6 J1();

    @DexIgnore
    rm6 K(vm6 vm6);

    @DexIgnore
    c17 K0();

    @DexIgnore
    void K1(dk5 dk5);

    @DexIgnore
    o66 L(p66 p66);

    @DexIgnore
    ow6 L0(rw6 rw6);

    @DexIgnore
    mx5 L1(qx5 qx5);

    @DexIgnore
    xb6 M(cc6 cc6);

    @DexIgnore
    qk6 M0(uk6 uk6);

    @DexIgnore
    ja6 M1();

    @DexIgnore
    mp6 N(qp6 qp6);

    @DexIgnore
    tu6 N0(xu6 xu6);

    @DexIgnore
    jy4 N1();

    @DexIgnore
    void O(NotificationReceiver notificationReceiver);

    @DexIgnore
    pb6 O0(qb6 qb6);

    @DexIgnore
    void O1(qn5 qn5);

    @DexIgnore
    b96 P(f96 f96);

    @DexIgnore
    ih6 P0(qh6 qh6);

    @DexIgnore
    q46 P1(u46 u46);

    @DexIgnore
    void Q(p37 p37);

    @DexIgnore
    gy6 Q0(jy6 jy6);

    @DexIgnore
    gy4 Q1();

    @DexIgnore
    void R(en5 en5);

    @DexIgnore
    po6 R0(to6 to6);

    @DexIgnore
    nz6 R1(rz6 rz6);

    @DexIgnore
    t17 S(w17 w17);

    @DexIgnore
    void S0(av6 av6);

    @DexIgnore
    gh5 S1();

    @DexIgnore
    rw4 T();

    @DexIgnore
    q76 T0(c86 c86);

    @DexIgnore
    u96 T1(z96 z96);

    @DexIgnore
    iu6 U();

    @DexIgnore
    yc6 U0();

    @DexIgnore
    t16 U1(y16 y16);

    @DexIgnore
    mv4 V();

    @DexIgnore
    hm6 V0(lm6 lm6);

    @DexIgnore
    void V1(WorkoutDetailActivity workoutDetailActivity);

    @DexIgnore
    en6 W();

    @DexIgnore
    yv4 W0();

    @DexIgnore
    void W1(CloudImageHelper cloudImageHelper);

    @DexIgnore
    qa6 X(ta6 ta6);

    @DexIgnore
    y97 X0();

    @DexIgnore
    qj6 X1(yj6 yj6);

    @DexIgnore
    er6 Y(hr6 hr6);

    @DexIgnore
    kw4 Y0();

    @DexIgnore
    void Z(MFDeviceService mFDeviceService);

    @DexIgnore
    cs6 Z0(fs6 fs6);

    @DexIgnore
    js6 a(ms6 ms6);

    @DexIgnore
    mn6 a0();

    @DexIgnore
    da7 a1();

    @DexIgnore
    nl6 b(rl6 rl6);

    @DexIgnore
    kr6 b0(nr6 nr6);

    @DexIgnore
    tx4 b1();

    @DexIgnore
    xl6 c(bm6 bm6);

    @DexIgnore
    void c0(cx6 cx6);

    @DexIgnore
    void c1(cs5 cs5);

    @DexIgnore
    ot6 d(rt6 rt6);

    @DexIgnore
    ju4 d0();

    @DexIgnore
    r97 d1();

    @DexIgnore
    vs6 e(ys6 ys6);

    @DexIgnore
    j87 e0();

    @DexIgnore
    j17 e1(m17 m17);

    @DexIgnore
    ps6 f(ss6 ss6);

    @DexIgnore
    void f0(ComplicationWeatherService complicationWeatherService);

    @DexIgnore
    za6 f1(db6 db6);

    @DexIgnore
    fq6 g(jq6 jq6);

    @DexIgnore
    void g0(pl5 pl5);

    @DexIgnore
    hc6 g1(kc6 kc6);

    @DexIgnore
    ay5 h(dy5 dy5);

    @DexIgnore
    ze6 h0(if6 if6);

    @DexIgnore
    fo6 h1(jo6 jo6);

    @DexIgnore
    void i(AlarmReceiver alarmReceiver);

    @DexIgnore
    gw6 i0(jw6 jw6);

    @DexIgnore
    void i1(cn5 cn5);

    @DexIgnore
    b56 j(f56 f56);

    @DexIgnore
    uu4 j0();

    @DexIgnore
    m36 j1(n36 n36);

    @DexIgnore
    al6 k(el6 el6);

    @DexIgnore
    i86 k0(m86 m86);

    @DexIgnore
    cx4 k1();

    @DexIgnore
    void l(up5 up5);

    @DexIgnore
    void l0(FossilFirebaseMessagingService fossilFirebaseMessagingService);

    @DexIgnore
    void l1(x27 x27);

    @DexIgnore
    f46 m(j46 j46);

    @DexIgnore
    u76 m0(x76 x76);

    @DexIgnore
    h16 m1(l16 l16);

    @DexIgnore
    void n(nk5 nk5);

    @DexIgnore
    n56 n0(r56 r56);

    @DexIgnore
    ov6 n1(rv6 rv6);

    @DexIgnore
    u66 o(v66 v66);

    @DexIgnore
    ev4 o0();

    @DexIgnore
    void o1(LocaleChangedReceiver localeChangedReceiver);

    @DexIgnore
    void p(URLRequestTaskHelper uRLRequestTaskHelper);

    @DexIgnore
    w26 p0(b36 b36);

    @DexIgnore
    nz5 p1(oz5 oz5);

    @DexIgnore
    a66 q(e66 e66);

    @DexIgnore
    la7 q0();

    @DexIgnore
    w77 q1();

    @DexIgnore
    h07 r(k07 k07);

    @DexIgnore
    xv6 r0(aw6 aw6);

    @DexIgnore
    void r1(BCNotificationActionReceiver bCNotificationActionReceiver);

    @DexIgnore
    void s(AppPackageRemoveReceiver appPackageRemoveReceiver);

    @DexIgnore
    wa7 s0();

    @DexIgnore
    eg6 s1(mg6 mg6);

    @DexIgnore
    mi6 t(ui6 ui6);

    @DexIgnore
    cd6 t0(dd6 dd6);

    @DexIgnore
    void t1(CommuteTimeService commuteTimeService);

    @DexIgnore
    bz6 u(gz6 gz6);

    @DexIgnore
    ht6 u0(kt6 kt6);

    @DexIgnore
    void u1(ls5 ls5);

    @DexIgnore
    yq6 v(br6 br6);

    @DexIgnore
    l96 v0();

    @DexIgnore
    iv6 v1();

    @DexIgnore
    e97 w();

    @DexIgnore
    void w0(go5 go5);

    @DexIgnore
    e27 w1(j27 j27);

    @DexIgnore
    dv6 x();

    @DexIgnore
    void x0(mn5 mn5);

    @DexIgnore
    vt6 x1(yt6 yt6);

    @DexIgnore
    pq6 y(tq6 tq6);

    @DexIgnore
    l06 y0(p06 p06);

    @DexIgnore
    i26 y1();

    @DexIgnore
    void z(BootReceiver bootReceiver);

    @DexIgnore
    void z0(uz6 uz6);

    @DexIgnore
    xy5 z1(hz5 hz5);
}
