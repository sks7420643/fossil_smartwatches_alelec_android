package com.fossil;

import com.fossil.Xb1;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dc1 implements Xb1<InputStream> {
    @DexIgnore
    public /* final */ Qg1 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Xb1.Ai<InputStream> {
        @DexIgnore
        public /* final */ Od1 a;

        @DexIgnore
        public Ai(Od1 od1) {
            this.a = od1;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Xb1' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Xb1.Ai
        public /* bridge */ /* synthetic */ Xb1<InputStream> a(InputStream inputStream) {
            return b(inputStream);
        }

        @DexIgnore
        public Xb1<InputStream> b(InputStream inputStream) {
            return new Dc1(inputStream, this.a);
        }

        @DexIgnore
        @Override // com.fossil.Xb1.Ai
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore
    public Dc1(InputStream inputStream, Od1 od1) {
        Qg1 qg1 = new Qg1(inputStream, od1);
        this.a = qg1;
        qg1.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    }

    @DexIgnore
    @Override // com.fossil.Xb1
    public void a() {
        this.a.c();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xb1
    public /* bridge */ /* synthetic */ InputStream b() throws IOException {
        return d();
    }

    @DexIgnore
    public void c() {
        this.a.b();
    }

    @DexIgnore
    public InputStream d() throws IOException {
        this.a.reset();
        return this.a;
    }
}
