package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ll1 extends Ml1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ll1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ll1 createFromParcel(Parcel parcel) {
            Ml1 b = Ml1.CREATOR.b(parcel);
            if (b != null) {
                return (Ll1) b;
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotReminder");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ll1[] newArray(int i) {
            return new Ll1[i];
        }
    }

    @DexIgnore
    public Ll1(Kl1 kl1, Ql1 ql1, Il1 il1) throws IllegalArgumentException {
        super(kl1, ql1, il1);
    }

    @DexIgnore
    @Override // com.fossil.El1
    public Kl1 getFireTime() {
        Gl1[] b = b();
        for (Gl1 gl1 : b) {
            if (gl1 instanceof Kl1) {
                if (gl1 != null) {
                    return (Kl1) gl1;
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotFireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
