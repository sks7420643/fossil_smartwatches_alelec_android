package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface R92 {
    @DexIgnore
    void a(Z52 z52);

    @DexIgnore
    void b(Bundle bundle);

    @DexIgnore
    void c(int i, boolean z);
}
