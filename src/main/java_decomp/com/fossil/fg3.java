package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fg3 {
    @DexIgnore
    public /* final */ Zs2 a;

    @DexIgnore
    public interface Ai extends Sn3 {
    }

    @DexIgnore
    public Fg3(Zs2 zs2) {
        this.a = zs2;
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        this.a.s(str, str2, bundle);
    }

    @DexIgnore
    public void b(Ai ai) {
        this.a.n(ai);
    }

    @DexIgnore
    public void c(String str, String str2, Object obj) {
        this.a.u(str, str2, obj);
    }

    @DexIgnore
    public final void d(boolean z) {
        this.a.F(z);
    }
}
