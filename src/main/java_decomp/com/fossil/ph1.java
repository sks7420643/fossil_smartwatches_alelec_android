package com.fossil;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ph1 implements Th1<Bitmap, byte[]> {
    @DexIgnore
    public /* final */ Bitmap.CompressFormat a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public Ph1() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    public Ph1(Bitmap.CompressFormat compressFormat, int i) {
        this.a = compressFormat;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.Th1
    public Id1<byte[]> a(Id1<Bitmap> id1, Ob1 ob1) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        id1.get().compress(this.a, this.b, byteArrayOutputStream);
        id1.b();
        return new Xg1(byteArrayOutputStream.toByteArray());
    }
}
