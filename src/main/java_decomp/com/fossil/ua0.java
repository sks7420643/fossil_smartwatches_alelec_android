package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ua0 implements Parcelable.Creator<Va0> {
    @DexIgnore
    public /* synthetic */ Ua0(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Va0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            Aa0 valueOf = Aa0.valueOf(readString);
            parcel.setDataPosition(0);
            switch (Ta0.a[valueOf.ordinal()]) {
                case 1:
                    return Ia0.CREATOR.a(parcel);
                case 2:
                    return Ka0.CREATOR.a(parcel);
                case 3:
                    return Fb0.CREATOR.a(parcel);
                case 4:
                    return Za0.CREATOR.a(parcel);
                case 5:
                    return Oa0.CREATOR.a(parcel);
                case 6:
                    return Xa0.CREATOR.a(parcel);
                case 7:
                    return Bb0.CREATOR.a(parcel);
                case 8:
                    return Qa0.CREATOR.a(parcel);
                case 9:
                    return Ma0.CREATOR.a(parcel);
                case 10:
                    return Ga0.CREATOR.a(parcel);
                case 11:
                    return Db0.CREATOR.a(parcel);
                case 12:
                    return Sa0.CREATOR.a(parcel);
                default:
                    throw new Kc6();
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Va0[] newArray(int i) {
        return new Va0[i];
    }
}
