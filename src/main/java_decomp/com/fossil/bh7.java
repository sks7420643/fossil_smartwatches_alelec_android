package com.fossil;

import android.content.Context;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bh7 implements Runnable {
    @DexIgnore
    public Context b; // = null;
    @DexIgnore
    public Map<String, Integer> c; // = null;
    @DexIgnore
    public Jg7 d; // = null;

    @DexIgnore
    public Bh7(Context context, Map<String, Integer> map, Jg7 jg7) {
        this.b = context;
        this.d = jg7;
        if (map != null) {
            this.c = map;
        }
    }

    @DexIgnore
    public final Eg7 a(String str, int i) {
        int i2;
        Eg7 eg7 = new Eg7();
        Socket socket = new Socket();
        try {
            eg7.a(str);
            eg7.c(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            eg7.b(System.currentTimeMillis() - currentTimeMillis);
            eg7.d(inetSocketAddress.getAddress().getHostAddress());
            socket.close();
            try {
                socket.close();
            } catch (Throwable th) {
                Ig7.m.e(th);
            }
            i2 = 0;
        } catch (IOException e) {
            Ig7.m.e(e);
            socket.close();
        } catch (Throwable th2) {
            Ig7.m.e(th2);
        }
        eg7.e(i2);
        return eg7;
        i2 = -1;
        eg7.e(i2);
        return eg7;
    }

    @DexIgnore
    public final Map<String, Integer> b() {
        String str;
        HashMap hashMap = new HashMap();
        String c2 = Fg7.c("__MTA_TEST_SPEED__", null);
        if (!(c2 == null || c2.trim().length() == 0)) {
            for (String str2 : c2.split(";")) {
                String[] split = str2.split(",");
                if (!(split == null || split.length != 2 || (str = split[0]) == null || str.trim().length() == 0)) {
                    try {
                        hashMap.put(str, Integer.valueOf(Integer.valueOf(split[1]).intValue()));
                    } catch (NumberFormatException e) {
                        Ig7.m.e(e);
                    }
                }
            }
        }
        return hashMap;
    }

    @DexIgnore
    public void run() {
        Th7 th7;
        String str;
        try {
            if (this.c == null) {
                this.c = b();
            }
            if (this.c == null || this.c.size() == 0) {
                Ig7.m.h("empty domain list.");
                return;
            }
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry<String, Integer> entry : this.c.entrySet()) {
                String key = entry.getKey();
                if (key == null || key.length() == 0) {
                    th7 = Ig7.m;
                    str = "empty domain name.";
                } else if (entry.getValue() == null) {
                    str = "port is null for " + key;
                    th7 = Ig7.m;
                } else {
                    jSONArray.put(a(entry.getKey(), entry.getValue().intValue()).f());
                }
                th7.l(str);
            }
            if (jSONArray.length() != 0) {
                Qg7 qg7 = new Qg7(this.b, Ig7.a(this.b, false, this.d), this.d);
                qg7.i(jSONArray.toString());
                new Ch7(qg7).b();
            }
        } catch (Throwable th) {
            Ig7.m.e(th);
        }
    }
}
