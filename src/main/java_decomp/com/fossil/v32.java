package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V32 implements Factory<T32> {
    @DexIgnore
    public static /* final */ V32 a; // = new V32();

    @DexIgnore
    public static V32 a() {
        return a;
    }

    @DexIgnore
    public static T32 b() {
        T32 a2 = U32.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public T32 c() {
        return b();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return c();
    }
}
