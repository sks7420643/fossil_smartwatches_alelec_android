package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Rc6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zv7 {
    @DexIgnore
    public static final <T> void a(Yv7<? super T> yv7, int i) {
        Xe6<? super T> c = yv7.c();
        if (!c(i) || !(c instanceof Vv7) || b(i) != b(yv7.d)) {
            d(yv7, c, i);
            return;
        }
        Dv7 dv7 = ((Vv7) c).h;
        Af6 context = c.getContext();
        if (dv7.Q(context)) {
            dv7.M(context, yv7);
        } else {
            e(yv7);
        }
    }

    @DexIgnore
    public static final boolean b(int i) {
        return i == 1;
    }

    @DexIgnore
    public static final boolean c(int i) {
        return i == 0 || i == 1;
    }

    @DexIgnore
    public static final <T> void d(Yv7<? super T> yv7, Xe6<? super T> xe6, int i) {
        Object r0;
        Object j = yv7.j();
        Throwable d = yv7.d(j);
        Throwable th = d != null ? Nv7.d() ? !(xe6 instanceof Do7) ? d : Uz7.j(d, (Do7) xe6) : d : null;
        if (th != null) {
            Dl7.Ai ai = Dl7.Companion;
            r0 = Dl7.constructor-impl(El7.a(th));
        } else {
            Dl7.Ai ai2 = Dl7.Companion;
            r0 = Dl7.constructor-impl(j);
        }
        if (i == 0) {
            xe6.resumeWith(r0);
        } else if (i == 1) {
            Wv7.b(xe6, r0);
        } else if (i != 2) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        } else if (xe6 != null) {
            Vv7 vv7 = (Vv7) xe6;
            Af6 context = vv7.getContext();
            Object c = Zz7.c(context, vv7.g);
            try {
                vv7.i.resumeWith(r0);
                Cd6 cd6 = Cd6.a;
            } finally {
                Zz7.a(context, c);
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        }
    }

    @DexIgnore
    public static final void e(Yv7<?> yv7) {
        Hw7 b = Wx7.b.b();
        if (b.o0()) {
            b.X(yv7);
            return;
        }
        b.g0(true);
        try {
            d(yv7, yv7.c(), 2);
            do {
            } while (b.r0());
        } catch (Throwable th) {
            b.S(true);
            throw th;
        }
        b.S(true);
    }
}
