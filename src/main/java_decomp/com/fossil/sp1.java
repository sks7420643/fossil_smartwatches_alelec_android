package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sp1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Cu1 d;
    @DexIgnore
    public /* final */ byte e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Sp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Sp1 createFromParcel(Parcel parcel) {
            return new Sp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Sp1[] newArray(int i) {
            return new Sp1[i];
        }
    }

    @DexIgnore
    public Sp1(byte b, Cu1 cu1, byte b2) {
        super(E90.BATTERY, b);
        this.d = cu1;
        this.e = (byte) b2;
    }

    @DexIgnore
    public /* synthetic */ Sp1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Cu1 a2 = Cu1.d.a(parcel.readByte());
        this.d = a2 == null ? Cu1.LOW : a2;
        this.e = parcel.readByte();
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Sp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Sp1 sp1 = (Sp1) obj;
            if (this.d != sp1.d) {
                return false;
            }
            return this.e == sp1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.BatteryNotification");
    }

    @DexIgnore
    public final byte getBatteryPercentage() {
        return this.e;
    }

    @DexIgnore
    public final Cu1 getBatteryState() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.d.hashCode()) * 31) + Byte.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.n1, Ey1.a(this.d)), Jd0.b5, Byte.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
    }
}
