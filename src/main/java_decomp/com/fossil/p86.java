package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P86 implements Factory<SearchRingPhonePresenter> {
    @DexIgnore
    public static SearchRingPhonePresenter a(K86 k86) {
        return new SearchRingPhonePresenter(k86);
    }
}
