package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zc6 extends Ts0 {
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ai(Integer num, boolean z) {
            this.a = num;
            this.b = z;
        }

        @DexIgnore
        public final Integer a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || this.b != ai.b) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.a;
            int hashCode = num != null ? num.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(tutorialResource=" + this.a + ", showLoading=" + this.b + ")";
        }
    }

    /*
    static {
        Wg6.b(Zc6.class.getSimpleName(), "CustomizeTutorialViewModel::class.java.simpleName");
    }
    */

    @DexIgnore
    public static /* synthetic */ void b(Zc6 zc6, Integer num, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        if ((i & 2) != 0) {
            z = false;
        }
        zc6.a(num, z);
    }

    @DexIgnore
    public final void a(Integer num, boolean z) {
        this.a.l(new Ai(num, z));
    }

    @DexIgnore
    public final MutableLiveData<Ai> c() {
        return this.a;
    }

    @DexIgnore
    public final void d(String str) {
        Wg6.c(str, "watchAppId");
        b(this, Ol5.c.d(str), false, 2, null);
    }
}
