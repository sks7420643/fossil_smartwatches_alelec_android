package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Wg6;
import java.io.Serializable;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yb0 extends Ox1 implements Parcelable, Serializable, Nx1 {
    @DexIgnore
    public static /* final */ Xb0 CREATOR; // = new Xb0(null);
    @DexIgnore
    public /* final */ Jw1 b;
    @DexIgnore
    public /* final */ Ry1 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore
    public Yb0(Jw1 jw1, Ry1 ry1, boolean z, byte[] bArr) {
        this.b = jw1;
        this.c = ry1;
        this.d = z;
        this.e = bArr;
    }

    @DexIgnore
    public Yb0(byte[] bArr) {
        boolean z = true;
        if (bArr.length >= 12) {
            Jw1 a2 = Jw1.d.a(bArr[0]);
            if (a2 != null) {
                this.b = a2;
                this.c = new Ry1(bArr[1], bArr[2]);
                this.d = bArr[3] != ((byte) 1) ? false : z;
                this.e = Dm7.k(bArr, 4, 12);
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        throw new IllegalArgumentException("Invalid data.".toString());
    }

    @DexIgnore
    public byte[] a() {
        byte a2 = this.b.a();
        byte b2 = this.d;
        byte[] bArr = this.e;
        return Dm7.q(new byte[]{a2, (byte) this.c.getMajor(), (byte) this.c.getMinor(), b2}, bArr);
    }

    @DexIgnore
    @Override // java.lang.Object
    public Nx1 clone() {
        Jw1 jw1 = this.b;
        Ry1 clone = this.c.clone();
        boolean z = this.d;
        byte[] bArr = this.e;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Yb0(jw1, clone, z, copyOf);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return Gy1.d(Gy1.d(Gy1.d(Gy1.d(new JSONObject(), Jd0.e, Ey1.a(this.b)), Jd0.K2, this.c.toJSONObject()), Jd0.N4, Boolean.valueOf(this.d)), Jd0.O4, Dy1.e(this.e, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeParcelable(this.c, i);
        parcel.writeByte(this.d ? (byte) 1 : 0);
        parcel.writeByteArray(this.e);
    }
}
