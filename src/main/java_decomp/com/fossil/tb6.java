package com.fossil;

import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tb6 implements Factory<HybridCustomizeViewModel> {
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> b;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> c;

    @DexIgnore
    public Tb6(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Tb6 a(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        return new Tb6(provider, provider2, provider3);
    }

    @DexIgnore
    public static HybridCustomizeViewModel c(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        return new HybridCustomizeViewModel(hybridPresetRepository, microAppLastSettingRepository, microAppRepository);
    }

    @DexIgnore
    public HybridCustomizeViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
