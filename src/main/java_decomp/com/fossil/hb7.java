package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hb7 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public String b;

    @DexIgnore
    public Hb7(byte[] bArr, String str) {
        Wg6.c(str, "idOfBackground");
        this.a = bArr;
        this.b = str;
    }

    @DexIgnore
    public final byte[] a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Hb7.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.b, ((Hb7) obj).b) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFBackground");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "WFBackground(byteData=" + Arrays.toString(this.a) + ", idOfBackground=" + this.b + ")";
    }
}
