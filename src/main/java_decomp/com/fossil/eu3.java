package com.fossil;

import com.mapped.Mc3;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eu3<TResult> implements Hu3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public Mc3<? super TResult> c;

    @DexIgnore
    public Eu3(Executor executor, Mc3<? super TResult> mc3) {
        this.a = executor;
        this.c = mc3;
    }

    @DexIgnore
    @Override // com.fossil.Hu3
    public final void a(Nt3<TResult> nt3) {
        if (nt3.q()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new Du3(this, nt3));
                }
            }
        }
    }
}
