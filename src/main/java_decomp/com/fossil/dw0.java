package com.fossil;

import android.os.Build;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dw0 extends Cw0 {
    @DexIgnore
    public Tv0 e;

    @DexIgnore
    public Dw0(long j, RenderScript renderScript) {
        super(j, renderScript);
    }

    @DexIgnore
    public static Dw0 k(RenderScript renderScript, Vv0 vv0) {
        if (vv0.q(Vv0.k(renderScript)) || vv0.q(Vv0.j(renderScript))) {
            boolean z = renderScript.i() && Build.VERSION.SDK_INT < 19;
            Dw0 dw0 = new Dw0(renderScript.C(5, vv0.c(renderScript), z), renderScript);
            dw0.h(z);
            dw0.n(5.0f);
            return dw0;
        }
        throw new Yv0("Unsupported element type.");
    }

    @DexIgnore
    public void l(Tv0 tv0) {
        if (tv0.l().k() != 0) {
            f(0, null, tv0, null);
            return;
        }
        throw new Yv0("Output is a 1D Allocation");
    }

    @DexIgnore
    public void m(Tv0 tv0) {
        if (tv0.l().k() != 0) {
            this.e = tv0;
            j(1, tv0);
            return;
        }
        throw new Yv0("Input set to a 1D Allocation");
    }

    @DexIgnore
    public void n(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 25.0f) {
            throw new Yv0("Radius out of range (0 < r <= 25).");
        }
        i(0, f);
    }
}
