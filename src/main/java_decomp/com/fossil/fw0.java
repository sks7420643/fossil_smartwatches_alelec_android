package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fw0 {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "androidx.room.CoroutinesRoom$Companion$execute$2", f = "CoroutinesRoom.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super R>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Callable $callable;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Callable callable, Xe6 xe6) {
                super(2, xe6);
                this.$callable = callable;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$callable, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Object obj) {
                throw null;
                //return ((Aii) create(il6, (Xe6) obj)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.$callable.call();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final <R> Object a(Oh oh, boolean z, Callable<R> callable, Xe6<? super R> xe6) {
            Rn7 b;
            if (oh.isOpen() && oh.inTransaction()) {
                return callable.call();
            }
            Yw0 yw0 = (Yw0) xe6.getContext().get(Yw0.c);
            if (yw0 == null || (b = yw0.b()) == null) {
                b = z ? Gw0.b(oh) : Gw0.a(oh);
            }
            return Eu7.g(b, new Aii(callable, null), xe6);
        }
    }

    @DexIgnore
    public static final <R> Object a(Oh oh, boolean z, Callable<R> callable, Xe6<? super R> xe6) {
        return a.a(oh, z, callable, xe6);
    }
}
