package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseIntArray;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S61 {
    @DexIgnore
    public /* final */ SparseIntArray a; // = new SparseIntArray();
    @DexIgnore
    public /* final */ O51 b; // = new O51(0, 1, null);
    @DexIgnore
    public /* final */ G51 c;

    @DexIgnore
    public S61(G51 g51) {
        Wg6.c(g51, "bitmapPool");
        this.c = g51;
    }

    @DexIgnore
    public final void a(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int identityHashCode = System.identityHashCode(bitmap);
        int i = this.a.get(identityHashCode) - 1;
        this.a.put(identityHashCode, i);
        if (Q81.c.a() && Q81.c.b() <= 2) {
            Log.println(2, "BitmapReferenceCounter", "DECREMENT: [" + identityHashCode + ", " + i + ']');
        }
        if (i <= 0) {
            this.a.delete(identityHashCode);
            if (!this.b.b(identityHashCode)) {
                this.c.b(bitmap);
            }
        }
    }

    @DexIgnore
    public final void b(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int identityHashCode = System.identityHashCode(bitmap);
        int i = this.a.get(identityHashCode) + 1;
        this.a.put(identityHashCode, i);
        if (Q81.c.a() && Q81.c.b() <= 2) {
            Log.println(2, "BitmapReferenceCounter", "INCREMENT: [" + identityHashCode + ", " + i + ']');
        }
    }

    @DexIgnore
    public final void c(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        this.b.a(System.identityHashCode(bitmap));
    }
}
