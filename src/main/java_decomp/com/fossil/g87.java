package com.fossil;

import com.mapped.WatchFaceComplicationFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G87 implements MembersInjector<WatchFaceComplicationFragment> {
    @DexIgnore
    public static void a(WatchFaceComplicationFragment watchFaceComplicationFragment, Po4 po4) {
        watchFaceComplicationFragment.g = po4;
    }
}
