package com.fossil;

import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class C41 implements Runnable {
    @DexIgnore
    public static /* final */ String e; // = X01.f("StopWorkRunnable");
    @DexIgnore
    public /* final */ S11 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public C41(S11 s11, String str, boolean z) {
        this.b = s11;
        this.c = str;
        this.d = z;
    }

    @DexIgnore
    public void run() {
        boolean n;
        WorkDatabase p = this.b.p();
        M11 n2 = this.b.n();
        P31 j = p.j();
        p.beginTransaction();
        try {
            boolean g = n2.g(this.c);
            if (this.d) {
                n = this.b.n().m(this.c);
            } else {
                if (!g && j.n(this.c) == F11.RUNNING) {
                    j.a(F11.ENQUEUED, this.c);
                }
                n = this.b.n().n(this.c);
            }
            X01.c().a(e, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", this.c, Boolean.valueOf(n)), new Throwable[0]);
            p.setTransactionSuccessful();
        } finally {
            p.endTransaction();
        }
    }
}
