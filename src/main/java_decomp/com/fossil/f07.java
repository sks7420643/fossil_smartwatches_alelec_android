package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F07 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ Zz6 b;

    @DexIgnore
    public F07(BaseActivity baseActivity, Zz6 zz6) {
        Wg6.c(baseActivity, "mContext");
        Wg6.c(zz6, "mView");
        this.a = baseActivity;
        this.b = zz6;
    }

    @DexIgnore
    public final Zz6 a() {
        return this.b;
    }
}
