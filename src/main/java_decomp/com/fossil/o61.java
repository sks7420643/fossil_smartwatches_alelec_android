package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface O61<T, V> {
    @DexIgnore
    boolean a(T t);

    @DexIgnore
    V b(T t, F81 f81);
}
