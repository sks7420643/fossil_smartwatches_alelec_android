package com.fossil;

import android.app.Activity;
import android.content.IntentSender;
import android.util.Log;
import com.fossil.Z62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X62<R extends Z62> extends B72<R> {
    @DexIgnore
    public /* final */ Activity a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public X62(Activity activity, int i) {
        Rc2.l(activity, "Activity must not be null");
        this.a = activity;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.B72
    public final void b(Status status) {
        if (status.k()) {
            try {
                status.F(this.a, this.b);
            } catch (IntentSender.SendIntentException e) {
                Log.e("ResolvingResultCallback", "Failed to start resolution", e);
                d(new Status(8));
            }
        } else {
            d(status);
        }
    }

    @DexIgnore
    public abstract void d(Status status);
}
