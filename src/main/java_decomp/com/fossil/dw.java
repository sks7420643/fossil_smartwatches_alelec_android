package com.fossil;

import com.mapped.Cd6;
import com.mapped.Gg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dw extends Fs {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ long C; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ Gg6<Cd6> E;
    @DexIgnore
    public Bw F;
    @DexIgnore
    public /* final */ short G;
    @DexIgnore
    public /* final */ Vr H;

    @DexIgnore
    public Dw(short s, Vr vr, K5 k5) {
        super(Hs.P, k5, 0, 4);
        this.G = (short) s;
        this.H = vr;
        this.E = new Cw(this, k5);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.o3, Long.valueOf(this.A));
    }

    @DexIgnore
    public final void D() {
        Bw bw = this.F;
        if (bw != null) {
            this.j.removeCallbacks(bw);
        }
        Bw bw2 = this.F;
        if (bw2 != null) {
            bw2.b = true;
        }
        this.F = null;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.B = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void s(O7 o7) {
        if (o7.a == N6.j) {
            byte[] bArr = o7.b;
            if (bArr.length >= 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                if (this.G != order.getShort(1)) {
                    return;
                }
                if (Sv.f.b == b) {
                    D();
                    Mw b2 = Mw.g.b(Kt.k.a(order.get(3)));
                    this.v = Mw.a(this.v, null, null, b2.d, null, b2.f, 11);
                    this.A = Hy1.o(order.getInt(5));
                    this.g.add(new Hw(0, o7.a, o7.b, G80.k(new JSONObject(), Jd0.I0, Long.valueOf(this.A)), 1));
                    m(this.v);
                    return;
                }
                this.g.add(new Hw(0, o7.a, o7.b, null, 9));
                m(Mw.a(this.v, null, null, Lw.e, null, null, 27));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void v(U5 u5) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.v = Mw.a(this.v, null, null, Mw.g.a(u5.e).d, u5.e, null, 19);
        A90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        A90 a902 = this.f;
        if (!(a902 == null || (jSONObject2 = a902.n) == null)) {
            G80.k(jSONObject2, Jd0.k, Ey1.a(Lw.b));
        }
        Lw lw = this.v.d;
        Lw lw2 = Lw.b;
        n(this.p);
        A90 a903 = this.f;
        if (!(a903 == null || (jSONObject = a903.n) == null)) {
            G80.k(jSONObject, Jd0.q3, Integer.valueOf(this.H.c()));
        }
        float min = Math.min((((float) this.H.c()) * 1.0f) / ((float) this.H.c), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.H.c() >= this.H.c) {
            this.D = min;
            e(min);
        }
        q();
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public U5 w() {
        if (!(this.H.b.remaining() > 0)) {
            return null;
        }
        byte[] a2 = this.H.a();
        if (this.s) {
            a2 = Jx.b.c(this.y.x, this.H.f, a2);
        }
        return new J6(this.H.f, a2, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void y() {
        B();
        C();
        D();
        Bw bw = new Bw(this, this.E);
        this.F = bw;
        if (bw != null) {
            t().postDelayed(bw, this.C);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.q3, Integer.valueOf(this.H.c)), Jd0.p3, Integer.valueOf(((Wr) this.H).g));
    }
}
