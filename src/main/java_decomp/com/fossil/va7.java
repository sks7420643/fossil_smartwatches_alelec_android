package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import com.mapped.MyFaceFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Va7 extends FragmentStateAdapter {
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ String k;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Va7(Fragment fragment, String str, String str2) {
        super(fragment);
        Wg6.c(fragment, "fragment");
        Wg6.c(str, "watchFaceId");
        Wg6.c(str2, "watchFaceIdType");
        this.j = str;
        this.k = str2;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return 2;
    }

    @DexIgnore
    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment i(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("Brown", "createFragment position=" + i);
        if (i == 0) {
            return MyFaceFragment.y.a(this.j, this.k);
        }
        if (i == 1) {
            return Ua7.j.a();
        }
        throw new IllegalArgumentException("Unknown fragment");
    }
}
