package com.fossil;

import com.portfolio.platform.data.source.remote.ApiService2Dot1;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xo4 implements Factory<ApiService2Dot1> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<AuthenticationInterceptor> b;
    @DexIgnore
    public /* final */ Provider<Uq5> c;

    @DexIgnore
    public Xo4(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static Xo4 a(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        return new Xo4(uo4, provider, provider2);
    }

    @DexIgnore
    public static ApiService2Dot1 c(Uo4 uo4, AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        ApiService2Dot1 f = uo4.f(authenticationInterceptor, uq5);
        Lk7.c(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }

    @DexIgnore
    public ApiService2Dot1 b() {
        return c(this.a, this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
