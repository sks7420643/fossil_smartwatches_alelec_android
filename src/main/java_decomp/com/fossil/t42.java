package com.fossil;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T42 implements I42 {
    @DexIgnore
    @Override // com.fossil.I42
    public final L42 a(Intent intent) {
        return V42.a(intent);
    }

    @DexIgnore
    @Override // com.fossil.I42
    public final T62<Status> b(R62 r62) {
        return V42.f(r62, r62.l(), false);
    }
}
