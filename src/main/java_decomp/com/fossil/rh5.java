package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Rh5 {
    ACTIVE_TIME(1),
    TOTAL_STEPS(2),
    CALORIES(3),
    GOAL_TRACKING(3),
    TOTAL_SLEEP(11),
    RESTFUL(12),
    LIGHT(13),
    AWAKE(14);
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public int mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public Rh5(int i) {
        this.mValue = i;
    }

    @DexIgnore
    public final int getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.mValue = i;
    }
}
