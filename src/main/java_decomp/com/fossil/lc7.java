package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.workers.TimeChangeReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lc7 implements MembersInjector<TimeChangeReceiver> {
    @DexIgnore
    public static void a(TimeChangeReceiver timeChangeReceiver, AlarmHelper alarmHelper) {
        timeChangeReceiver.a = alarmHelper;
    }

    @DexIgnore
    public static void b(TimeChangeReceiver timeChangeReceiver, An4 an4) {
        timeChangeReceiver.b = an4;
    }
}
