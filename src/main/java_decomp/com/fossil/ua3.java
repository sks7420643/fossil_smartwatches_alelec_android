package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ua3 implements Parcelable.Creator<Ia3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ia3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        Sa3 sa3 = null;
        ArrayList arrayList = null;
        boolean z2 = false;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                arrayList = Ad2.j(parcel, t, LocationRequest.CREATOR);
            } else if (l == 2) {
                z2 = Ad2.m(parcel, t);
            } else if (l == 3) {
                z = Ad2.m(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                sa3 = (Sa3) Ad2.e(parcel, t, Sa3.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Ia3(arrayList, z2, z, sa3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ia3[] newArray(int i) {
        return new Ia3[i];
    }
}
