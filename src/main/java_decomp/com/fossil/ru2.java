package com.fossil;

import com.fossil.E13;
import com.fossil.Qu2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ru2 extends E13<Ru2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Ru2 zzl;
    @DexIgnore
    public static volatile Z23<Ru2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public int zzf;
    @DexIgnore
    public M13<Su2> zzg; // = E13.B();
    @DexIgnore
    public M13<Qu2> zzh; // = E13.B();
    @DexIgnore
    public M13<Gu2> zzi; // = E13.B();
    @DexIgnore
    public String zzj; // = "";
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Ru2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Ru2.zzl);
        }

        @DexIgnore
        public /* synthetic */ Ai(Ou2 ou2) {
            this();
        }

        @DexIgnore
        public final List<Gu2> B() {
            return Collections.unmodifiableList(((Ru2) this.c).N());
        }

        @DexIgnore
        public final Ai C() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Ru2) this.c).S();
            return this;
        }

        @DexIgnore
        public final int x() {
            return ((Ru2) this.c).M();
        }

        @DexIgnore
        public final Qu2 y(int i) {
            return ((Ru2) this.c).C(i);
        }

        @DexIgnore
        public final Ai z(int i, Qu2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Ru2) this.c).D(i, (Qu2) ((E13) ai.h()));
            return this;
        }
    }

    /*
    static {
        Ru2 ru2 = new Ru2();
        zzl = ru2;
        E13.u(Ru2.class, ru2);
    }
    */

    @DexIgnore
    public static Ai P() {
        return (Ai) zzl.w();
    }

    @DexIgnore
    public static Ru2 Q() {
        return zzl;
    }

    @DexIgnore
    public final Qu2 C(int i) {
        return this.zzh.get(i);
    }

    @DexIgnore
    public final void D(int i, Qu2 qu2) {
        qu2.getClass();
        M13<Qu2> m13 = this.zzh;
        if (!m13.zza()) {
            this.zzh = E13.q(m13);
        }
        this.zzh.set(i, qu2);
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final long I() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String K() {
        return this.zze;
    }

    @DexIgnore
    public final List<Su2> L() {
        return this.zzg;
    }

    @DexIgnore
    public final int M() {
        return this.zzh.size();
    }

    @DexIgnore
    public final List<Gu2> N() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean O() {
        return this.zzk;
    }

    @DexIgnore
    public final void S() {
        this.zzi = E13.B();
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Ou2.a[i - 1]) {
            case 1:
                return new Ru2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001\u1002\u0000\u0002\u1008\u0001\u0003\u1004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\u1008\u0003\b\u1007\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", Su2.class, "zzh", Qu2.class, "zzi", Gu2.class, "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                Z23<Ru2> z232 = zzm;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Ru2.class) {
                    try {
                        z23 = zzm;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzl);
                            zzm = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
