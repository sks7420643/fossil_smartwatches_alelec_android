package com.fossil;

import com.fossil.Dl7;
import com.fossil.Lz7;
import com.mapped.Cd6;
import com.mapped.Lk6;
import com.mapped.Rc6;
import com.mapped.Xe6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V08 implements U08 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(V08.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Bi {
        @DexIgnore
        public /* final */ Lk6<Cd6> f;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Lk6<? super com.mapped.Cd6> */
        /* JADX WARN: Multi-variable type inference failed */
        public Ai(Object obj, Lk6<? super Cd6> lk6) {
            super(obj);
            this.f = lk6;
        }

        @DexIgnore
        @Override // com.fossil.Lz7
        public String toString() {
            return "LockCont[" + this.e + ", " + this.f + ']';
        }

        @DexIgnore
        @Override // com.fossil.V08.Bi
        public void w(Object obj) {
            this.f.g(obj);
        }

        @DexIgnore
        @Override // com.fossil.V08.Bi
        public Object x() {
            return Lk6.Ai.a(this.f, Cd6.a, null, 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi extends Lz7 implements Dw7 {
        @DexIgnore
        public /* final */ Object e;

        @DexIgnore
        public Bi(Object obj) {
            this.e = obj;
        }

        @DexIgnore
        @Override // com.fossil.Dw7
        public final void dispose() {
            r();
        }

        @DexIgnore
        public abstract void w(Object obj);

        @DexIgnore
        public abstract Object x();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Jz7 {
        @DexIgnore
        public Object e;

        @DexIgnore
        public Ci(Object obj) {
            this.e = obj;
        }

        @DexIgnore
        @Override // com.fossil.Lz7
        public String toString() {
            return "LockedQueue[" + this.e + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Rz7 {
        @DexIgnore
        public /* final */ Ci a;

        @DexIgnore
        public Di(Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        @Override // com.fossil.Rz7
        public Cz7<?> a() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.Rz7
        public Object c(Object obj) {
            Object obj2 = this.a.w() ? W08.e : this.a;
            if (obj != null) {
                V08 v08 = (V08) obj;
                V08.a.compareAndSet(v08, this, obj2);
                if (v08._state == this.a) {
                    return W08.a;
                }
                return null;
            }
            throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Lz7.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ V08 e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Lz7 lz7, Lz7 lz72, Object obj, Lk6 lk6, Ai ai, V08 v08, Object obj2) {
            super(lz72);
            this.d = obj;
            this.e = v08;
            this.f = obj2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Cz7
        public /* bridge */ /* synthetic */ Object g(Lz7 lz7) {
            return i(lz7);
        }

        @DexIgnore
        public Object i(Lz7 lz7) {
            if (this.e._state == this.d) {
                return null;
            }
            return Kz7.a();
        }
    }

    @DexIgnore
    public V08(boolean z) {
        this._state = z ? W08.d : W08.e;
    }

    @DexIgnore
    @Override // com.fossil.U08
    public Object a(Object obj, Xe6<? super Cd6> xe6) {
        if (d(obj)) {
            return Cd6.a;
        }
        Object c = c(obj, xe6);
        return c != Yn7.d() ? Cd6.a : c;
    }

    @DexIgnore
    @Override // com.fossil.U08
    public void b(Object obj) {
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof T08) {
                if (obj == null) {
                    if (!(((T08) obj2).a != W08.c)) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    T08 t08 = (T08) obj2;
                    if (!(t08.a == obj)) {
                        throw new IllegalStateException(("Mutex is locked by " + t08.a + " but expected " + obj).toString());
                    }
                }
                if (a.compareAndSet(this, obj2, W08.e)) {
                    return;
                }
            } else if (obj2 instanceof Rz7) {
                ((Rz7) obj2).c(this);
            } else if (obj2 instanceof Ci) {
                if (obj != null) {
                    Ci ci = (Ci) obj2;
                    if (!(ci.e == obj)) {
                        throw new IllegalStateException(("Mutex is locked by " + ci.e + " but expected " + obj).toString());
                    }
                }
                Ci ci2 = (Ci) obj2;
                Lz7 s = ci2.s();
                if (s == null) {
                    Di di = new Di(ci2);
                    if (a.compareAndSet(this, obj2, di) && di.c(this) == null) {
                        return;
                    }
                } else {
                    Bi bi = (Bi) s;
                    Object x = bi.x();
                    if (x != null) {
                        Object obj3 = bi.e;
                        if (obj3 == null) {
                            obj3 = W08.b;
                        }
                        ci2.e = obj3;
                        bi.w(x);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, Xe6<? super Cd6> xe6) {
        boolean z;
        Lu7 b = Nu7.b(Xn7.c(xe6));
        Ai ai = new Ai(obj, b);
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof T08) {
                T08 t08 = (T08) obj2;
                if (t08.a != W08.c) {
                    a.compareAndSet(this, obj2, new Ci(t08.a));
                } else {
                    if (a.compareAndSet(this, obj2, obj == null ? W08.d : new T08(obj))) {
                        Cd6 cd6 = Cd6.a;
                        Dl7.Ai ai2 = Dl7.Companion;
                        b.resumeWith(Dl7.constructor-impl(cd6));
                        break;
                    }
                }
            } else if (obj2 instanceof Ci) {
                Ci ci = (Ci) obj2;
                if (ci.e != obj) {
                    Ei ei = new Ei(ai, ai, obj2, b, ai, this, obj);
                    while (true) {
                        int v = ci.n().v(ai, ci, ei);
                        if (v != 1) {
                            if (v == 2) {
                                z = false;
                                break;
                            }
                        } else {
                            z = true;
                            break;
                        }
                    }
                    if (z) {
                        Nu7.c(b, ai);
                        break;
                    }
                } else {
                    throw new IllegalStateException(("Already locked by " + obj).toString());
                }
            } else if (obj2 instanceof Rz7) {
                ((Rz7) obj2).c(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
        Object t = b.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public boolean d(Object obj) {
        boolean z = true;
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof T08) {
                if (((T08) obj2).a != W08.c) {
                    return false;
                }
                if (a.compareAndSet(this, obj2, obj == null ? W08.d : new T08(obj))) {
                    return true;
                }
            } else if (obj2 instanceof Ci) {
                if (((Ci) obj2).e == obj) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                throw new IllegalStateException(("Already locked by " + obj).toString());
            } else if (obj2 instanceof Rz7) {
                ((Rz7) obj2).c(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof T08) {
                return "Mutex[" + ((T08) obj).a + ']';
            } else if (obj instanceof Rz7) {
                ((Rz7) obj).c(this);
            } else if (obj instanceof Ci) {
                return "Mutex[" + ((Ci) obj).e + ']';
            } else {
                throw new IllegalStateException(("Illegal state " + obj).toString());
            }
        }
    }
}
