package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iu2 extends E13<Iu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Iu2 zzh;
    @DexIgnore
    public static volatile Z23<Iu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public Lu2 zzd;
    @DexIgnore
    public Ju2 zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public String zzg; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Iu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Iu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ Ai(Fu2 fu2) {
            this();
        }

        @DexIgnore
        public final Ai x(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Iu2) this.c).D(str);
            return this;
        }
    }

    /*
    static {
        Iu2 iu2 = new Iu2();
        zzh = iu2;
        E13.u(Iu2.class, iu2);
    }
    */

    @DexIgnore
    public static Iu2 N() {
        return zzh;
    }

    @DexIgnore
    public final void D(String str) {
        str.getClass();
        this.zzc |= 8;
        this.zzg = str;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final Lu2 G() {
        Lu2 lu2 = this.zzd;
        return lu2 == null ? Lu2.L() : lu2;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final Ju2 I() {
        Ju2 ju2 = this.zze;
        return ju2 == null ? Ju2.N() : ju2;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean K() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String M() {
        return this.zzg;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Fu2.a[i - 1]) {
            case 1:
                return new Iu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1009\u0000\u0002\u1009\u0001\u0003\u1007\u0002\u0004\u1008\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                Z23<Iu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Iu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
