package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class v05 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon13 b;
    @DexIgnore
    public /* final */ /* synthetic */ List c;

    @DexIgnore
    public /* synthetic */ v05(PresetRepository.Anon13 anon13, List list) {
        this.b = anon13;
        this.c = list;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
