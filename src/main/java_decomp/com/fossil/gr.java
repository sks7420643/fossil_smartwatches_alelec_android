package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gr extends Lp {
    @DexIgnore
    public int C;
    @DexIgnore
    public Se D;
    @DexIgnore
    public /* final */ ArrayList<Ow> E; // = By1.a(this.i, Hm7.c(Ow.c));
    @DexIgnore
    public /* final */ Ve[] F;

    @DexIgnore
    public Gr(K5 k5, I60 i60, String str, Ve[] veArr) {
        super(k5, i60, Yp.T, str, false, 16);
        this.F = veArr;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        I();
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        Jd0 jd0 = Jd0.n4;
        Se se = this.D;
        return G80.k(E2, jd0, se != null ? se.toJSONObject() : null);
    }

    @DexIgnore
    public final void I() {
        int i = this.C;
        Ve[] veArr = this.F;
        if (i < veArr.length) {
            Lp.i(this, new Xu(veArr[i], this.w), new Eq(this), new Sq(this), null, null, null, 56, null);
        } else {
            l(Nr.a(this.v, null, Zq.j, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        Se se = this.D;
        return se != null ? se : new Se(0, 0, 0);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.E;
    }
}
