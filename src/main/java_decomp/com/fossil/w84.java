package com.fossil;

import android.content.Context;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W84 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ J64 b;
    @DexIgnore
    public /* final */ C94 c;
    @DexIgnore
    public /* final */ long d; // = System.currentTimeMillis();
    @DexIgnore
    public X84 e;
    @DexIgnore
    public X84 f;
    @DexIgnore
    public U84 g;
    @DexIgnore
    public /* final */ H94 h;
    @DexIgnore
    public /* final */ I84 i;
    @DexIgnore
    public /* final */ B84 j;
    @DexIgnore
    public ExecutorService k;
    @DexIgnore
    public S84 l;
    @DexIgnore
    public W74 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Callable<Nt3<Void>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rc4 a;

        @DexIgnore
        public Ai(Rc4 rc4) {
            this.a = rc4;
        }

        @DexIgnore
        public Nt3<Void> a() throws Exception {
            return W84.this.f(this.a);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Nt3<Void> call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Rc4 b;

        @DexIgnore
        public Bi(Rc4 rc4) {
            this.b = rc4;
        }

        @DexIgnore
        public void run() {
            W84.this.f(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Callable<Boolean> {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public Boolean a() throws Exception {
            try {
                boolean d = W84.this.e.d();
                X74 f = X74.f();
                f.b("Initialization marker file removed: " + d);
                return Boolean.valueOf(d);
            } catch (Exception e) {
                X74.f().e("Problem encountered deleting Crashlytics initialization marker.", e);
                return Boolean.FALSE;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Boolean call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Callable<Boolean> {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public Boolean a() throws Exception {
            return Boolean.valueOf(W84.this.g.H());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Boolean call() throws Exception {
            return a();
        }
    }

    @DexIgnore
    public W84(J64 j64, H94 h94, W74 w74, C94 c94, I84 i84, B84 b84, ExecutorService executorService) {
        this.b = j64;
        this.c = c94;
        this.a = j64.g();
        this.h = h94;
        this.m = w74;
        this.i = i84;
        this.j = b84;
        this.k = executorService;
        this.l = new S84(executorService);
    }

    @DexIgnore
    public static String i() {
        return "17.1.1";
    }

    @DexIgnore
    public static boolean j(String str, boolean z) {
        if (!z) {
            X74.f().b("Configured not to require a build ID.");
            return true;
        } else if (!R84.D(str)) {
            return true;
        } else {
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", ".     |  | ");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".   \\ |  | /");
            Log.e("FirebaseCrashlytics", ".    \\    /");
            Log.e("FirebaseCrashlytics", ".     \\  /");
            Log.e("FirebaseCrashlytics", ".      \\/");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", ".      /\\");
            Log.e("FirebaseCrashlytics", ".     /  \\");
            Log.e("FirebaseCrashlytics", ".    /    \\");
            Log.e("FirebaseCrashlytics", ".   / |  | \\");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            return false;
        }
    }

    @DexIgnore
    public final void d() {
        try {
            Boolean.TRUE.equals((Boolean) T94.a(this.l.h(new Di())));
        } catch (Exception e2) {
        }
    }

    @DexIgnore
    public boolean e() {
        return this.e.c();
    }

    @DexIgnore
    public final Nt3<Void> f(Rc4 rc4) {
        m();
        this.g.B();
        try {
            this.i.a(V84.b(this));
            Zc4 b2 = rc4.b();
            if (!b2.a().a) {
                X74.f().b("Collection of crash reports disabled in Crashlytics settings.");
                return Qt3.e(new RuntimeException("Collection of crash reports disabled in Crashlytics settings."));
            }
            if (!this.g.P(b2.b().a)) {
                X74.f().b("Could not finalize previous sessions.");
            }
            Nt3<Void> x0 = this.g.x0(1.0f, rc4.a());
            l();
            return x0;
        } catch (Exception e2) {
            X74.f().e("Crashlytics encountered a problem during asynchronous initialization.", e2);
            return Qt3.e(e2);
        } finally {
            l();
        }
    }

    @DexIgnore
    public Nt3<Void> g(Rc4 rc4) {
        return T94.b(this.k, new Ai(rc4));
    }

    @DexIgnore
    public final void h(Rc4 rc4) {
        Future<?> submit = this.k.submit(new Bi(rc4));
        X74.f().b("Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            X74.f().e("Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            X74.f().e("Problem encountered during Crashlytics initialization.", e3);
        } catch (TimeoutException e4) {
            X74.f().e("Crashlytics timed out during initialization.", e4);
        }
    }

    @DexIgnore
    public void k(String str) {
        this.g.Q0(System.currentTimeMillis() - this.d, str);
    }

    @DexIgnore
    public void l() {
        this.l.h(new Ci());
    }

    @DexIgnore
    public void m() {
        this.l.b();
        this.e.a();
        X74.f().b("Initialization marker file created.");
    }

    @DexIgnore
    public boolean n(Rc4 rc4) {
        String p = R84.p(this.a);
        X74 f2 = X74.f();
        f2.b("Mapping file ID is: " + p);
        if (j(p, R84.l(this.a, "com.crashlytics.RequireBuildId", true))) {
            String c2 = this.b.j().c();
            try {
                X74 f3 = X74.f();
                f3.g("Initializing Crashlytics " + i());
                Ub4 ub4 = new Ub4(this.a);
                this.f = new X84("crash_marker", ub4);
                this.e = new X84("initialization_marker", ub4);
                Kb4 kb4 = new Kb4();
                L84 a2 = L84.a(this.a, this.h, c2, p);
                Md4 md4 = new Md4(this.a);
                X74 f4 = X74.f();
                f4.b("Installer package name is: " + a2.c);
                this.g = new U84(this.a, this.l, kb4, this.h, this.c, ub4, this.f, a2, null, null, this.m, md4, this.j, rc4);
                boolean e2 = e();
                d();
                this.g.M(Thread.getDefaultUncaughtExceptionHandler(), rc4);
                if (!e2 || !R84.c(this.a)) {
                    X74.f().b("Exception handling initialization successful");
                    return true;
                }
                X74.f().b("Crashlytics did not finish previous background initialization. Initializing synchronously.");
                h(rc4);
                return false;
            } catch (Exception e3) {
                X74.f().e("Crashlytics was not started due to an exception during initialization", e3);
                this.g = null;
                return false;
            }
        } else {
            throw new IllegalStateException("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    @DexIgnore
    public void o(String str, String str2) {
        this.g.v0(str, str2);
    }

    @DexIgnore
    public void p(String str) {
        this.g.w0(str);
    }
}
