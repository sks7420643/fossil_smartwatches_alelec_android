package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wq5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3984a;
    @DexIgnore
    public static c<CommunicateMode, a> b; // = new c<>();
    @DexIgnore
    public static /* final */ HashMap<CommunicateMode, d<b>> c; // = new HashMap<>();
    @DexIgnore
    public static /* final */ wq5 d; // = new wq5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Intent f3985a;

        @DexIgnore
        public a(CommunicateMode communicateMode, String str, Intent intent) {
            pq7.c(communicateMode, "mode");
            pq7.c(intent, "intent");
            this.f3985a = intent;
        }

        @DexIgnore
        public final Intent a() {
            return this.f3985a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CommunicateMode communicateMode, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T, V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ HashMap<T, V> f3986a; // = new HashMap<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final V a(T t) {
            return this.f3986a.get(t);
        }

        @DexIgnore
        public final boolean b(T t, V v) {
            boolean z;
            synchronized (this.b) {
                if (pq7.a(this.f3986a.get(t), v)) {
                    this.f3986a.remove(t);
                    z = true;
                } else {
                    z = false;
                }
            }
            return z;
        }

        @DexIgnore
        public final void c(T t, V v) {
            synchronized (this.b) {
                this.f3986a.put(t, v);
                tl7 tl7 = tl7.f3441a;
            }
        }

        @DexIgnore
        public String toString() {
            String hashMap = this.f3986a.toString();
            pq7.b(hashMap, "mHashMap.toString()");
            return hashMap;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<T> f3987a; // = new ArrayList();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.f3987a.add(t);
            }
        }

        @DexIgnore
        public final List<T> b() {
            ArrayList arrayList;
            synchronized (this.b) {
                arrayList = new ArrayList();
                arrayList.addAll(this.f3987a);
            }
            return arrayList;
        }

        @DexIgnore
        public final void c(T t) {
            synchronized (this.b) {
                this.f3987a.remove(t);
            }
        }

        @DexIgnore
        public final int d() {
            return this.f3987a.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1", f = "BleCommandResultManager.kt", l = {69}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $communicateModes;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1", f = "BleCommandResultManager.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ a $bleResult;
            @DexIgnore
            public /* final */ /* synthetic */ CommunicateMode $communicateMode;
            @DexIgnore
            public /* final */ /* synthetic */ d $observerList;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, CommunicateMode communicateMode, a aVar, qn7 qn7) {
                super(2, qn7);
                this.$observerList = dVar;
                this.$communicateMode = communicateMode;
                this.$bleResult = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$observerList, this.$communicateMode, this.$bleResult, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    for (b bVar : this.$observerList.b()) {
                        bVar.a(this.$communicateMode, this.$bleResult.a());
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(List list, qn7 qn7) {
            super(2, qn7);
            this.$communicateModes = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$communicateModes, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0114 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0038  */
        @Override // com.fossil.zn7
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 284
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wq5.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = wq5.class.getSimpleName();
        pq7.b(simpleName, "BleCommandResultManager::class.java.simpleName");
        f3984a = simpleName;
    }
    */

    @DexIgnore
    public final void d(b bVar, List<? extends CommunicateMode> list) {
        synchronized (this) {
            pq7.c(bVar, "observerReceiver");
            pq7.c(list, "communicateModes");
            for (T t : list) {
                d<b> dVar = c.get(t);
                if (dVar != null) {
                    dVar.a(bVar);
                } else {
                    HashMap<CommunicateMode, d<b>> hashMap = c;
                    d<b> dVar2 = new d<>();
                    dVar2.a(bVar);
                    hashMap.put(t, dVar2);
                }
            }
        }
    }

    @DexIgnore
    public final void e(b bVar, CommunicateMode... communicateModeArr) {
        pq7.c(bVar, "observerReceiver");
        pq7.c(communicateModeArr, "communicateModes");
        d(bVar, hm7.i((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void f(List<? extends CommunicateMode> list) {
        synchronized (this) {
            pq7.c(list, "communicateModes");
            xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new e(list, null), 3, null);
        }
    }

    @DexIgnore
    public final void g(CommunicateMode... communicateModeArr) {
        pq7.c(communicateModeArr, "communicateModes");
        f(hm7.i((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void h(CommunicateMode communicateMode, a aVar) {
        synchronized (this) {
            pq7.c(communicateMode, "mode");
            pq7.c(aVar, "bleResult");
            b.c(communicateMode, aVar);
            g(communicateMode);
        }
    }

    @DexIgnore
    public final void i(b bVar, List<? extends CommunicateMode> list) {
        synchronized (this) {
            pq7.c(bVar, "observerReceiver");
            pq7.c(list, "communicateModes");
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                d<b> dVar = c.get(it.next());
                if (dVar != null) {
                    dVar.c(bVar);
                }
            }
        }
    }

    @DexIgnore
    public final void j(b bVar, CommunicateMode... communicateModeArr) {
        pq7.c(bVar, "observerReceiver");
        pq7.c(communicateModeArr, "communicateModes");
        i(bVar, hm7.i((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }
}
