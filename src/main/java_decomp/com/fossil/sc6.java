package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ow5;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sc6 extends BaseFragment implements Rc6 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<R85> g;
    @DexIgnore
    public Ow5 h;
    @DexIgnore
    public Qc6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Sc6.k;
        }

        @DexIgnore
        public final Sc6 b() {
            return new Sc6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sc6 b;

        @DexIgnore
        public Bi(Sc6 sc6) {
            this.b = sc6;
        }

        @DexIgnore
        public final void onClick(View view) {
            R85 a2 = this.b.M6().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Sc6 b;

        @DexIgnore
        public Ci(Sc6 sc6) {
            this.b = sc6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            FlexibleTextView flexibleTextView;
            ImageView imageView2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Sc6.l.a();
            local.d(a2, "onTextChanged " + charSequence);
            if (TextUtils.isEmpty(charSequence)) {
                R85 a3 = this.b.M6().a();
                if (!(a3 == null || (imageView2 = a3.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.b.z("");
                this.b.N6().n();
                return;
            }
            R85 a4 = this.b.M6().a();
            if (!(a4 == null || (flexibleTextView = a4.w) == null)) {
                flexibleTextView.setVisibility(8);
            }
            R85 a5 = this.b.M6().a();
            if (!(a5 == null || (imageView = a5.r) == null)) {
                imageView.setVisibility(0);
            }
            this.b.z(String.valueOf(charSequence));
            this.b.N6().p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sc6 b;

        @DexIgnore
        public Di(Sc6 sc6) {
            this.b = sc6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Ow5.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ Sc6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(Sc6 sc6) {
            this.a = sc6;
        }

        @DexIgnore
        @Override // com.fossil.Ow5.Ci
        public void a(String str) {
            Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            R85 a2 = this.a.M6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                Wg6.b(flexibleTextView2, "it.tvNotFound");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886813);
                Wg6.b(c, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Ow5.Di {
        @DexIgnore
        public /* final */ /* synthetic */ Sc6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(Sc6 sc6) {
            this.a = sc6;
        }

        @DexIgnore
        @Override // com.fossil.Ow5.Di
        public void a(MicroApp microApp) {
            Wg6.c(microApp, "item");
            this.a.N6().o(microApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ R85 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public Gi(R85 r85, long j) {
            this.a = r85;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleButton flexibleButton = this.a.q;
            Wg6.b(flexibleButton, "binding.btnCancel");
            if (flexibleButton.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.a.q.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = Sc6.class.getSimpleName();
        Wg6.b(simpleName, "SearchMicroAppFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.Rc6
    public void B(List<Lc6<MicroApp, String>> list) {
        Wg6.c(list, "results");
        Ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.m(list);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rc6
    public void D() {
        Ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.m(null);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Rc6
    public void K(List<Lc6<MicroApp, String>> list) {
        Wg6.c(list, "recentSearchResult");
        Ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.l(list);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void K6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Xk5 xk5 = Xk5.a;
            G37<R85> g37 = this.g;
            if (g37 != null) {
                R85 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    Wg6.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.View");
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Qc6 qc6) {
        Q6(qc6);
    }

    @DexIgnore
    public final G37<R85> M6() {
        G37<R85> g37 = this.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final Qc6 N6() {
        Qc6 qc6 = this.i;
        if (qc6 != null) {
            return qc6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void O6(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = Is5.a.a(j2);
        Window window = fragmentActivity.getWindow();
        Wg6.b(window, "context.window");
        window.setEnterTransition(a2);
        G37<R85> g37 = this.g;
        if (g37 != null) {
            R85 a3 = g37.a();
            if (a3 != null) {
                Wg6.b(a3, "binding");
                P6(a2, j2, a3);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet P6(TransitionSet transitionSet, long j2, R85 r85) {
        FlexibleButton flexibleButton = r85.q;
        Wg6.b(flexibleButton, "binding.btnCancel");
        flexibleButton.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new Gi(r85, j2));
    }

    @DexIgnore
    public void Q6(Qc6 qc6) {
        Wg6.c(qc6, "presenter");
        this.i = qc6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<R85> g37 = new G37<>(this, (R85) Aq0.f(layoutInflater, 2131558586, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            R85 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Qc6 qc6 = this.i;
        if (qc6 != null) {
            qc6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        Qc6 qc6 = this.i;
        if (qc6 != null) {
            qc6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Wg6.b(activity, "it");
            O6(activity, 550);
        }
        this.h = new Ow5();
        G37<R85> g37 = this.g;
        if (g37 != null) {
            R85 a2 = g37.a();
            if (a2 != null) {
                R85 r85 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = r85.v;
                Wg6.b(recyclerViewEmptySupport, "this.rvResults");
                Ow5 ow5 = this.h;
                if (ow5 != null) {
                    recyclerViewEmptySupport.setAdapter(ow5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = r85.v;
                    Wg6.b(recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = r85.v;
                    FlexibleTextView flexibleTextView = r85.w;
                    Wg6.b(flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = r85.r;
                    Wg6.b(imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    r85.r.setOnClickListener(new Bi(this));
                    r85.t.addTextChangedListener(new Ci(this));
                    r85.q.setOnClickListener(new Di(this));
                    Ow5 ow52 = this.h;
                    if (ow52 != null) {
                        ow52.o(new Ei(this));
                        Ow5 ow53 = this.h;
                        if (ow53 != null) {
                            ow53.p(new Fi(this));
                        } else {
                            Wg6.n("mAdapter");
                            throw null;
                        }
                    } else {
                        Wg6.n("mAdapter");
                        throw null;
                    }
                } else {
                    Wg6.n("mAdapter");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rc6
    public void v1(MicroApp microApp) {
        Wg6.c(microApp, "selectedMicroApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Xk5 xk5 = Xk5.a;
            G37<R85> g37 = this.g;
            if (g37 != null) {
                R85 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    Wg6.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_MICRO_APP_RESULT_ID", microApp.getId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.View");
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Rc6
    public void z(String str) {
        Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.n(str);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }
}
