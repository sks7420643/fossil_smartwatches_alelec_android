package com.fossil;

import com.fossil.Bv7;
import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Bv7<T extends Throwable & Bv7<T>> {
    @DexIgnore
    T createCopy();
}
