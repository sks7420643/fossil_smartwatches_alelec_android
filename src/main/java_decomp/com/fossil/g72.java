package com.fossil;

import com.fossil.M62;
import com.fossil.M62.Di;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G72<O extends M62.Di> {
    @DexIgnore
    public /* final */ boolean a; // = true;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ M62<O> c;
    @DexIgnore
    public /* final */ O d;

    @DexIgnore
    public G72(M62<O> m62) {
        this.c = m62;
        this.d = null;
        this.b = System.identityHashCode(this);
    }

    @DexIgnore
    public G72(M62<O> m62, O o) {
        this.c = m62;
        this.d = o;
        this.b = Pc2.b(m62, o);
    }

    @DexIgnore
    public static <O extends M62.Di> G72<O> b(M62<O> m62, O o) {
        return new G72<>(m62, o);
    }

    @DexIgnore
    public static <O extends M62.Di> G72<O> c(M62<O> m62) {
        return new G72<>(m62);
    }

    @DexIgnore
    public final String a() {
        return this.c.b();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof G72)) {
            return false;
        }
        G72 g72 = (G72) obj;
        return !this.a && !g72.a && Pc2.a(this.c, g72.c) && Pc2.a(this.d, g72.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b;
    }
}
