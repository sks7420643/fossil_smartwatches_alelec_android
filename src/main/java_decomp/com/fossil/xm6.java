package com.fossil;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.an6;
import com.fossil.bn6;
import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm6 extends sm6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<cl7<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE k; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.h0.c().J());
    @DexIgnore
    public List<MFSleepDay> l; // = new ArrayList();
    @DexIgnore
    public List<MFSleepSession> m; // = new ArrayList();
    @DexIgnore
    public MFSleepDay n;
    @DexIgnore
    public List<MFSleepSession> o;
    @DexIgnore
    public LiveData<h47<List<MFSleepDay>>> p;
    @DexIgnore
    public LiveData<h47<List<MFSleepSession>>> q;
    @DexIgnore
    public /* final */ tm6 r;
    @DexIgnore
    public /* final */ SleepSummariesRepository s;
    @DexIgnore
    public /* final */ SleepSessionsRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<MFSleepSession, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(MFSleepSession mFSleepSession) {
            return Boolean.valueOf(invoke(mFSleepSession));
        }

        @DexIgnore
        public final boolean invoke(MFSleepSession mFSleepSession) {
            pq7.c(mFSleepSession, "it");
            return lk5.m0(mFSleepSession.getDay(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xm6 f4141a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$sessionTransformations$1$1", f = "SleepDetailPresenter.kt", l = {65, 65}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<MFSleepSession>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<MFSleepSession>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.xm6$b r1 = r7.this$0
                    com.fossil.xm6 r1 = r1.f4141a
                    com.portfolio.platform.data.source.SleepSessionsRepository r1 = com.fossil.xm6.A(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSleepSessionList(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.xm6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(xm6 xm6) {
            this.f4141a = xm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<MFSleepSession>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(null, 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1", f = "SleepDetailPresenter.kt", l = {150, 174, 175}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$1", f = "SleepDetailPresenter.kt", l = {150}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Date> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object n0 = c.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<MFSleepSession>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<MFSleepSession>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    xm6 xm6 = this.this$0.this$0;
                    return xm6.W(xm6.f, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xm6$c$c")
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$summary$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.xm6$c$c  reason: collision with other inner class name */
        public static final class C0284c extends ko7 implements vp7<iv7, qn7<? super MFSleepDay>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0284c(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0284c cVar = new C0284c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFSleepDay> qn7) {
                throw null;
                //return ((C0284c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    xm6 xm6 = this.this$0.this$0;
                    return xm6.X(xm6.f, this.this$0.this$0.l);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xm6 xm6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xm6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$date, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00bc  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0214  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 538
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xm6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showDetailChart$1", f = "SleepDetailPresenter.kt", l = {328}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showDetailChart$1$data$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends bn6.b>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends bn6.b>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    xm6 xm6 = this.this$0.this$0;
                    return xm6.b0(xm6.o);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xm6 xm6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xm6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<bn6.b> list = (List) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailPresenter", "showDetailChart - data=" + list);
            if (!list.isEmpty()) {
                this.this$0.r.o2(list);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$1", f = "SleepDetailPresenter.kt", l = {125}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$1$data$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ArrayList<an6.a>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ArrayList<an6.a>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    xm6 xm6 = this.this$0.this$0;
                    return xm6.V(xm6.o);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(xm6 xm6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = xm6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList<an6.a> arrayList = (ArrayList) g;
            nk5.a aVar2 = nk5.o;
            String str = this.this$0.h;
            if (str != null) {
                if (aVar2.x(str)) {
                    if (arrayList.isEmpty()) {
                        arrayList.add(new an6.a(null, 0, 0, 0, 15, null));
                    }
                    this.this$0.r.u5(arrayList);
                } else if (arrayList.isEmpty()) {
                    this.this$0.r.H0();
                } else {
                    this.this$0.r.u5(arrayList);
                }
                return tl7.f3441a;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<h47<? extends List<MFSleepDay>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xm6 f4142a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1", f = "SleepDetailPresenter.kt", l = {86}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xm6$f$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1$summary$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.xm6$f$a$a  reason: collision with other inner class name */
            public static final class C0285a extends ko7 implements vp7<iv7, qn7<? super MFSleepDay>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0285a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0285a aVar = new C0285a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super MFSleepDay> qn7) {
                    throw null;
                    //return ((C0285a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        xm6 xm6 = this.this$0.this$0.f4142a;
                        return xm6.X(xm6.f, this.this$0.this$0.f4142a.l);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 h = this.this$0.f4142a.h();
                    C0285a aVar = new C0285a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFSleepDay mFSleepDay = (MFSleepDay) g;
                if (this.this$0.f4142a.n == null || (!pq7.a(this.this$0.f4142a.n, mFSleepDay))) {
                    this.this$0.f4142a.n = mFSleepDay;
                    this.this$0.f4142a.r.P5(mFSleepDay);
                    if (this.this$0.f4142a.i && this.this$0.f4142a.j) {
                        this.this$0.f4142a.Z();
                        this.this$0.f4142a.a0();
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public f(xm6 xm6) {
            this.f4142a = xm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<MFSleepDay>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepDetailPresenter", sb.toString());
            if (a2 == xh5.NETWORK_LOADING || a2 == xh5.SUCCESS) {
                this.f4142a.l = list;
                this.f4142a.i = true;
                xw7 unused = gu7.d(this.f4142a.k(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<h47<? extends List<MFSleepSession>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xm6 f4143a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$2$1", f = "SleepDetailPresenter.kt", l = {110}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xm6$g$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$2$1$sessions$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.xm6$g$a$a  reason: collision with other inner class name */
            public static final class C0286a extends ko7 implements vp7<iv7, qn7<? super List<MFSleepSession>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0286a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0286a aVar = new C0286a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<MFSleepSession>> qn7) {
                    throw null;
                    //return ((C0286a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        xm6 xm6 = this.this$0.this$0.f4143a;
                        return xm6.W(xm6.f, this.this$0.this$0.f4143a.m);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 h = this.this$0.f4143a.h();
                    C0286a aVar = new C0286a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (this.this$0.f4143a.o == null || (!pq7.a(this.this$0.f4143a.o, list))) {
                    this.this$0.f4143a.o = list;
                    if (this.this$0.f4143a.i && this.this$0.f4143a.j) {
                        this.this$0.f4143a.Z();
                        this.this$0.f4143a.a0();
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public g(xm6 xm6) {
            this.f4143a = xm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<MFSleepSession>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sessionTransformations -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepDetailPresenter", sb.toString());
            if (a2 == xh5.NETWORK_LOADING || a2 == xh5.SUCCESS) {
                this.f4143a.m = list;
                this.f4143a.j = true;
                xw7 unused = gu7.d(this.f4143a.k(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xm6 f4144a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$summaryTransformations$1$1", f = "SleepDetailPresenter.kt", l = {61, 61}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<MFSleepDay>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = hVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<MFSleepDay>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.xm6$h r1 = r7.this$0
                    com.fossil.xm6 r1 = r1.f4144a
                    com.portfolio.platform.data.source.SleepSummariesRepository r1 = com.fossil.xm6.F(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSleepSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.xm6.h.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public h(xm6 xm6) {
            this.f4144a = xm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<MFSleepDay>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(null, 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 3, null);
        }
    }

    @DexIgnore
    public xm6(tm6 tm6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        pq7.c(tm6, "mView");
        pq7.c(sleepSummariesRepository, "mSummariesRepository");
        pq7.c(sleepSessionsRepository, "mSessionsRepository");
        this.r = tm6;
        this.s = sleepSummariesRepository;
        this.t = sleepSessionsRepository;
        LiveData<h47<List<MFSleepDay>>> c2 = ss0.c(this.g, new h(this));
        pq7.b(c2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.p = c2;
        LiveData<h47<List<MFSleepSession>>> c3 = ss0.c(this.g, new b(this));
        pq7.b(c3, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.q = c3;
    }

    @DexIgnore
    public final ArrayList<an6.a> V(List<MFSleepSession> list) {
        short s2;
        short s3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("extractHeartRateDataFromSleepSession - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        ArrayList<an6.a> arrayList = new ArrayList<>();
        if (list != null) {
            for (T t2 : list) {
                List<WrapperSleepStateChange> sleepStateChange = t2.getSleepStateChange();
                try {
                    SleepSessionHeartRate heartRate = t2.getHeartRate();
                    if (heartRate != null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("extractHeartRateDataFromSleepSession - sleepStates.size=");
                        sb2.append(sleepStateChange != null ? Integer.valueOf(sleepStateChange.size()) : null);
                        sb2.append(", heartRateData.size=");
                        sb2.append(heartRate.getValues().size());
                        local2.d("SleepDetailPresenter", sb2.toString());
                        ArrayList arrayList2 = new ArrayList();
                        int resolutionInSecond = heartRate.getResolutionInSecond();
                        short s4 = Short.MAX_VALUE;
                        short s5 = Short.MIN_VALUE;
                        int i2 = 0;
                        for (T t3 : heartRate.getValues()) {
                            if (i2 >= 0) {
                                short shortValue = t3.shortValue();
                                short s6 = (s4 <= shortValue || shortValue == ((short) 0)) ? s4 : shortValue;
                                short s7 = s5 < shortValue ? shortValue : s5;
                                v57 v57 = new v57(0, 0, 0, 7, null);
                                v57.g((i2 * resolutionInSecond) / 60);
                                v57.h(shortValue);
                                if (sleepStateChange != null) {
                                    int size = sleepStateChange.size();
                                    int i3 = 0;
                                    while (true) {
                                        if (i3 >= size) {
                                            break;
                                        }
                                        if (i3 < hm7.g(sleepStateChange)) {
                                            if (sleepStateChange.get(i3).index <= ((long) v57.e()) && ((long) v57.e()) < sleepStateChange.get(i3 + 1).index) {
                                                v57.f(sleepStateChange.get(i3).state);
                                                s2 = s6;
                                                s3 = s7;
                                                break;
                                            }
                                        } else {
                                            v57.f(sleepStateChange.get(i3).state);
                                        }
                                        i3++;
                                    }
                                }
                                s2 = s6;
                                s3 = s7;
                                arrayList2.add(v57);
                                i2++;
                                s5 = s3;
                                s4 = s2;
                            } else {
                                hm7.l();
                                throw null;
                            }
                        }
                        if (s4 == Short.MAX_VALUE) {
                            s4 = 0;
                        }
                        try {
                            arrayList.add(new an6.a(arrayList2, t2.getRealSleepStateDistInMinute().getTotalMinuteBySleepDistribution(), s4, s5 == Short.MIN_VALUE ? 100 : s5));
                        } catch (Exception e2) {
                            e = e2;
                        }
                    } else {
                        continue;
                    }
                } catch (Exception e3) {
                    e = e3;
                    FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "extractHeartRateDataFromSleepSession - e=" + e);
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<MFSleepSession> W(Date date, List<MFSleepSession> list) {
        ts7 z;
        ts7 h2;
        if (list == null || (z = pm7.z(list)) == null || (h2 = at7.h(z, new a(date))) == null) {
            return null;
        }
        return at7.u(h2);
    }

    @DexIgnore
    public final MFSleepDay X(Date date, List<MFSleepDay> list) {
        T t2;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(date);
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "findSleepSummary - date=" + date);
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (lk5.m0(instance.getTime(), next.getDate())) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    public void Y() {
        this.r.M5(this);
    }

    @DexIgnore
    public final xw7 Z() {
        return gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public final xw7 a0() {
        return gu7.d(k(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final List<bn6.b> b0(List<MFSleepSession> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSleepSessionsToDetailChart - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        int e2 = sk5.c.e(this.n);
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (T t2 : list) {
                BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
                ArrayList arrayList2 = new ArrayList();
                List<WrapperSleepStateChange> sleepStateChange = t2.getSleepStateChange();
                ArrayList arrayList3 = new ArrayList();
                SleepDistribution sleepState = t2.getSleepState();
                int realStartTime = t2.getRealStartTime();
                int totalMinuteBySleepDistribution = sleepState.getTotalMinuteBySleepDistribution();
                if (sleepStateChange != null) {
                    for (T t3 : sleepStateChange) {
                        BarChart.b bVar = new BarChart.b(0, null, 0, 0, null, 31, null);
                        bVar.g((int) t3.index);
                        bVar.j(realStartTime);
                        bVar.i(totalMinuteBySleepDistribution);
                        int i2 = t3.state;
                        if (i2 == 0) {
                            bVar.h(BarChart.e.LOWEST);
                        } else if (i2 == 1) {
                            bVar.h(BarChart.e.DEFAULT);
                        } else if (i2 == 2) {
                            bVar.h(BarChart.e.HIGHEST);
                        }
                        arrayList3.add(bVar);
                    }
                }
                arrayList2.add(arrayList3);
                cVar.b().add(new BarChart.a(e2, arrayList2.size() != 0 ? arrayList2 : hm7.c(hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), 0, false, 12, null));
                cVar.f(e2);
                cVar.e(e2);
                int awake = sleepState.getAwake();
                int light = sleepState.getLight();
                int deep = sleepState.getDeep();
                if (totalMinuteBySleepDistribution > 0) {
                    float f2 = (float) totalMinuteBySleepDistribution;
                    float f3 = ((float) awake) / f2;
                    float f4 = ((float) light) / f2;
                    arrayList.add(new bn6.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, t2.getTimezoneOffset()));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.h = PortfolioApp.h0.c().J();
        LiveData<h47<List<MFSleepDay>>> liveData = this.p;
        tm6 tm6 = this.r;
        if (tm6 != null) {
            liveData.h((um6) tm6, new f(this));
            this.q.h((LifecycleOwner) this.r, new g(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "stop");
        LiveData<h47<List<MFSleepDay>>> liveData = this.p;
        tm6 tm6 = this.r;
        if (tm6 != null) {
            liveData.n((um6) tm6);
            this.q.n((LifecycleOwner) this.r);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.sm6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.k;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.sm6
    public void o(Bundle bundle) {
        pq7.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    @Override // com.fossil.sm6
    public void p(Date date) {
        pq7.c(date, "date");
        xw7 unused = gu7.d(k(), null, null, new c(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.sm6
    public void q() {
        Date O = lk5.O(this.f);
        pq7.b(O, "DateHelper.getNextDate(mDate)");
        p(O);
    }

    @DexIgnore
    @Override // com.fossil.sm6
    public void r() {
        Date P = lk5.P(this.f);
        pq7.b(P, "DateHelper.getPrevDate(mDate)");
        p(P);
    }
}
