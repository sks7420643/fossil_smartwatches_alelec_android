package com.fossil;

import android.view.ViewGroup;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Uy0 {
    @DexIgnore
    public abstract void a(Wy0 wy0);

    @DexIgnore
    public abstract String[] b();

    @DexIgnore
    public abstract long c(ViewGroup viewGroup, Transition transition, Wy0 wy0, Wy0 wy02);
}
