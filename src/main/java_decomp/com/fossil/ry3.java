package com.fossil;

import android.content.Context;
import android.graphics.Color;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ry3 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public Ry3(Context context) {
        this.a = Nz3.b(context, Jw3.elevationOverlayEnabled, false);
        this.b = Vx3.a(context, Jw3.elevationOverlayColor, 0);
        this.c = Vx3.a(context, Jw3.colorSurface, 0);
        this.d = context.getResources().getDisplayMetrics().density;
    }

    @DexIgnore
    public float a(float f) {
        float f2 = this.d;
        return (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : Math.min(((((float) Math.log1p((double) (f / f2))) * 4.5f) + 2.0f) / 100.0f, 1.0f);
    }

    @DexIgnore
    public int b(int i, float f) {
        float a2 = a(f);
        return Pl0.h(Vx3.f(Pl0.h(i, 255), this.b, a2), Color.alpha(i));
    }

    @DexIgnore
    public int c(int i, float f) {
        return (!this.a || !e(i)) ? i : b(i, f);
    }

    @DexIgnore
    public boolean d() {
        return this.a;
    }

    @DexIgnore
    public final boolean e(int i) {
        return Pl0.h(i, 255) == this.c;
    }
}
