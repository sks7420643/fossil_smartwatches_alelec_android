package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CompoundButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tg0 {
    @DexIgnore
    public /* final */ CompoundButton a;
    @DexIgnore
    public ColorStateList b; // = null;
    @DexIgnore
    public PorterDuff.Mode c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public Tg0(CompoundButton compoundButton) {
        this.a = compoundButton;
    }

    @DexIgnore
    public void a() {
        Drawable a2 = Ep0.a(this.a);
        if (a2 == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = Am0.r(a2).mutate();
            if (this.d) {
                Am0.o(mutate, this.b);
            }
            if (this.e) {
                Am0.p(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.a.getDrawableState());
            }
            this.a.setButtonDrawable(mutate);
        }
    }

    @DexIgnore
    public int b(int i) {
        Drawable a2;
        return (Build.VERSION.SDK_INT >= 17 || (a2 = Ep0.a(this.a)) == null) ? i : i + a2.getIntrinsicWidth();
    }

    @DexIgnore
    public ColorStateList c() {
        return this.b;
    }

    @DexIgnore
    public PorterDuff.Mode d() {
        return this.c;
    }

    @DexIgnore
    public void e(AttributeSet attributeSet, int i) {
        int n;
        int n2;
        boolean z = false;
        Th0 v = Th0.v(this.a.getContext(), attributeSet, Ue0.CompoundButton, i, 0);
        CompoundButton compoundButton = this.a;
        Mo0.j0(compoundButton, compoundButton.getContext(), Ue0.CompoundButton, attributeSet, v.r(), i, 0);
        try {
            if (v.s(Ue0.CompoundButton_buttonCompat) && (n2 = v.n(Ue0.CompoundButton_buttonCompat, 0)) != 0) {
                try {
                    this.a.setButtonDrawable(Gf0.d(this.a.getContext(), n2));
                    z = true;
                } catch (Resources.NotFoundException e2) {
                }
            }
            if (!z && v.s(Ue0.CompoundButton_android_button) && (n = v.n(Ue0.CompoundButton_android_button, 0)) != 0) {
                this.a.setButtonDrawable(Gf0.d(this.a.getContext(), n));
            }
            if (v.s(Ue0.CompoundButton_buttonTint)) {
                Ep0.c(this.a, v.c(Ue0.CompoundButton_buttonTint));
            }
            if (v.s(Ue0.CompoundButton_buttonTintMode)) {
                Ep0.d(this.a, Dh0.e(v.k(Ue0.CompoundButton_buttonTintMode, -1), null));
            }
        } finally {
            v.w();
        }
    }

    @DexIgnore
    public void f() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        a();
    }

    @DexIgnore
    public void g(ColorStateList colorStateList) {
        this.b = colorStateList;
        this.d = true;
        a();
    }

    @DexIgnore
    public void h(PorterDuff.Mode mode) {
        this.c = mode;
        this.e = true;
        a();
    }
}
