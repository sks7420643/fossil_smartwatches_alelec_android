package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.AlertDialogFragment;
import com.mapped.Iface;
import com.mapped.NotificationFavoriteContactAdapter;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z06 extends Qv5 implements Y06, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ Ai w; // = new Ai(null);
    @DexIgnore
    public X06 h;
    @DexIgnore
    public G37<X85> i;
    @DexIgnore
    public NotificationFavoriteContactAdapter j;
    @DexIgnore
    public W16 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public A26 m;
    @DexIgnore
    public R16 s;
    @DexIgnore
    public Po4 t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Z06.v;
        }

        @DexIgnore
        public final Z06 b() {
            return new Z06();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements NotificationFavoriteContactAdapter.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 a;

        @DexIgnore
        public Bi(Z06 z06) {
            this.a = z06;
        }

        @DexIgnore
        @Override // com.mapped.NotificationFavoriteContactAdapter.Ai
        public void a(ContactGroup contactGroup) {
            Wg6.c(contactGroup, "contactGroup");
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.c0(childFragmentManager, contactGroup);
        }

        @DexIgnore
        @Override // com.mapped.NotificationFavoriteContactAdapter.Ai
        public void b() {
            NotificationContactsActivity.B.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Ci(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.A;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 a;

        @DexIgnore
        public Di(Z06 z06) {
            this.a = z06;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (this.a.h == null) {
                return;
            }
            if (!z) {
                Z06.L6(this.a).r(false);
            } else if (!this.a.O6()) {
                Z06.L6(this.a).u(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Ei(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(Z06.w.a(), "press on button back");
            Z06.L6(this.b).v(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Fi(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z06.L6(this.b).w(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Gi(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z06.L6(this.b).w(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Hi(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Ii(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.A;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z06 b;

        @DexIgnore
        public Ji(Z06 z06) {
            this.b = z06;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z06.L6(this.b).n();
        }
    }

    /*
    static {
        String simpleName = Z06.class.getSimpleName();
        Wg6.b(simpleName, "NotificationCallsAndMess\u2026nt::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ X06 L6(Z06 z06) {
        X06 x06 = z06.h;
        if (x06 != null) {
            return x06;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void C2(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        X06 x06 = this.h;
        if (x06 != null) {
            x06.r(z);
            G37<X85> g37 = this.i;
            if (g37 != null) {
                X85 a2 = g37.a();
                if (a2 != null && (flexibleSwitchCompat = a2.F) != null) {
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void D4(List<ContactGroup> list) {
        Wg6.c(list, "mListContactGroup");
        NotificationFavoriteContactAdapter notificationFavoriteContactAdapter = this.j;
        if (notificationFavoriteContactAdapter != null) {
            notificationFavoriteContactAdapter.n(list);
        } else {
            Wg6.n("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void E2() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.t0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void F(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            G37<X85> g37 = this.i;
            if (g37 != null) {
                X85 a2 = g37.a();
                if (a2 != null && (rTLImageView2 = a2.y) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<X85> g372 = this.i;
        if (g372 != null) {
            X85 a3 = g372.a();
            if (a3 != null && (rTLImageView = a3.y) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d(v, "onActivityBackPressed -");
        X06 x06 = this.h;
        if (x06 != null) {
            x06.v(true);
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public int L1() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        CharSequence charSequence = null;
        G37<X85> g37 = this.i;
        if (g37 != null) {
            X85 a2 = g37.a();
            if (Wg6.a((a2 == null || (flexibleTextView2 = a2.r) == null) ? null : flexibleTextView2.getText(), Um5.c(PortfolioApp.get.instance(), 2131886089))) {
                return 0;
            }
            G37<X85> g372 = this.i;
            if (g372 != null) {
                X85 a3 = g372.a();
                if (!(a3 == null || (flexibleTextView = a3.r) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return Wg6.a(charSequence, Um5.c(PortfolioApp.get.instance(), 2131886090)) ? 1 : 2;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        P6((X06) obj);
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public int O2() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        CharSequence charSequence = null;
        G37<X85> g37 = this.i;
        if (g37 != null) {
            X85 a2 = g37.a();
            if (Wg6.a((a2 == null || (flexibleTextView2 = a2.s) == null) ? null : flexibleTextView2.getText(), Um5.c(PortfolioApp.get.instance(), 2131886089))) {
                return 0;
            }
            G37<X85> g372 = this.i;
            if (g372 != null) {
                X85 a3 = g372.a();
                if (!(a3 == null || (flexibleTextView = a3.s) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return Wg6.a(charSequence, Um5.c(PortfolioApp.get.instance(), 2131886090)) ? 1 : 2;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public boolean O5() {
        return this.l;
    }

    @DexIgnore
    public final boolean O6() {
        X06 x06 = this.h;
        if (x06 != null) {
            return x06.o();
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public void P6(X06 x06) {
        Wg6.c(x06, "presenter");
        this.h = x06;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        ContactGroup contactGroup;
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -638196028) {
            if (hashCode != -4398250) {
                if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363373) {
                    X06 x06 = this.h;
                    if (x06 != null) {
                        x06.p();
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("SET TO WATCH FAIL")) {
            } else {
                if (i2 == 2131363291) {
                    C2(false);
                } else if (i2 == 2131363373) {
                    X06 x062 = this.h;
                    if (x062 != null) {
                        x062.u(true);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("CONFIRM_REMOVE_CONTACT") && i2 == 2131363373) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (contactGroup = (ContactGroup) extras.getSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE")) != null) {
                X06 x063 = this.h;
                if (x063 != null) {
                    x063.q(contactGroup);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void V4() {
        W16 w16;
        FLogger.INSTANCE.getLocal().d(v, "showNotificationSettingDialog");
        if (isActive() && (w16 = this.k) != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            w16.show(childFragmentManager, W16.w.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void d4() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.clearFlags(16);
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public List<ContactGroup> f5() {
        NotificationFavoriteContactAdapter notificationFavoriteContactAdapter = this.j;
        if (notificationFavoriteContactAdapter != null) {
            return notificationFavoriteContactAdapter.j();
        }
        Wg6.n("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void h3(ContactGroup contactGroup) {
        Wg6.c(contactGroup, "contactGroup");
        NotificationFavoriteContactAdapter notificationFavoriteContactAdapter = this.j;
        if (notificationFavoriteContactAdapter != null) {
            notificationFavoriteContactAdapter.m(contactGroup);
        } else {
            Wg6.n("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void j6() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.setFlags(16, 16);
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void m4(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "settingsTypeName");
        G37<X85> g37 = this.i;
        if (g37 != null) {
            X85 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        Iface iface = PortfolioApp.get.instance().getIface();
        W16 w16 = this.k;
        if (w16 != null) {
            iface.U1(new Y16(w16)).b(this);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
                Po4 po4 = this.t;
                if (po4 != null) {
                    Ts0 a2 = Vs0.f(notificationCallsAndMessagesActivity, po4).a(R16.class);
                    Wg6.b(a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                    R16 r16 = (R16) a2;
                    this.s = r16;
                    X06 x06 = this.h;
                    if (x06 == null) {
                        Wg6.n("mPresenter");
                        throw null;
                    } else if (r16 != null) {
                        x06.t(r16);
                    } else {
                        Wg6.n("mNotificationSettingViewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModelFactory");
                    throw null;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == 1) {
            this.l = true;
            if (i2 == 112) {
                X06 x06 = this.h;
                if (x06 != null) {
                    x06.u(false);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else {
                X06 x062 = this.h;
                if (x062 != null) {
                    x062.v(false);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == 2 && i2 == 111) {
            X06 x063 = this.h;
            if (x063 != null) {
                x063.s();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        X85 x85 = (X85) Aq0.f(layoutInflater, 2131558589, viewGroup, false, A6());
        String d = ThemeManager.l.a().d("onPrimaryButton");
        if (!TextUtils.isEmpty(d)) {
            int parseColor = Color.parseColor(d);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131230985);
            if (drawable != null) {
                drawable.setTint(parseColor);
                x85.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        W16 w16 = (W16) getChildFragmentManager().Z(W16.w.a());
        this.k = w16;
        if (w16 == null) {
            this.k = W16.w.b();
        }
        x85.C.setOnClickListener(new Ci(this));
        x85.F.setOnCheckedChangeListener(new Di(this));
        x85.x.setOnClickListener(new Ei(this));
        x85.A.setOnClickListener(new Fi(this));
        x85.B.setOnClickListener(new Gi(this));
        x85.q.setOnClickListener(new Hi(this));
        FlexibleSwitchCompat flexibleSwitchCompat = x85.F;
        Wg6.b(flexibleSwitchCompat, "binding.swQrEnabled");
        flexibleSwitchCompat.setChecked(O6());
        x85.C.setOnClickListener(new Ii(this));
        x85.y.setOnClickListener(new Ji(this));
        NotificationFavoriteContactAdapter notificationFavoriteContactAdapter = new NotificationFavoriteContactAdapter();
        notificationFavoriteContactAdapter.o(new Bi(this));
        this.j = notificationFavoriteContactAdapter;
        RecyclerView recyclerView = x85.E;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        NotificationFavoriteContactAdapter notificationFavoriteContactAdapter2 = this.j;
        if (notificationFavoriteContactAdapter2 != null) {
            recyclerView.setAdapter(notificationFavoriteContactAdapter2);
            recyclerView.setHasFixedSize(true);
            Iface iface = PortfolioApp.get.instance().getIface();
            W16 w162 = this.k;
            if (w162 != null) {
                iface.U1(new Y16(w162)).b(this);
                this.i = new G37<>(this, x85);
                E6("call_message_view");
                Wg6.b(x85, "binding");
                return x85.n();
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
        Wg6.n("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        X06 x06 = this.h;
        if (x06 != null) {
            x06.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FlexibleSwitchCompat flexibleSwitchCompat;
        super.onResume();
        X06 x06 = this.h;
        if (x06 != null) {
            if (x06 != null) {
                x06.l();
                G37<X85> g37 = this.i;
                if (g37 != null) {
                    X85 a2 = g37.a();
                    if (!(a2 == null || (flexibleSwitchCompat = a2.F) == null)) {
                        flexibleSwitchCompat.setChecked(O6());
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Y06
    public void w1(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "settingsTypeName");
        G37<X85> g37 = this.i;
        if (g37 != null) {
            X85 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.s) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }
}
