package com.fossil;

import android.os.Bundle;
import com.fossil.Yb2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ud2 implements Yb2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ K72 a;

    @DexIgnore
    public Ud2(K72 k72) {
        this.a = k72;
    }

    @DexIgnore
    @Override // com.fossil.Yb2.Ai
    public final void d(int i) {
        this.a.d(i);
    }

    @DexIgnore
    @Override // com.fossil.Yb2.Ai
    public final void e(Bundle bundle) {
        this.a.e(bundle);
    }
}
