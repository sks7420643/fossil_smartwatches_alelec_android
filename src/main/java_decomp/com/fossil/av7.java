package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Av7 extends RuntimeException {
    @DexIgnore
    public Av7(String str, Throwable th) {
        super(str, th);
    }
}
