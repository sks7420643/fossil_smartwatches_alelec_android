package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ut1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ut1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ut1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Pq1.class.getClassLoader());
            if (readParcelable != null) {
                Wg6.b(readParcelable, "parcel.readParcelable<St\u2026class.java.classLoader)!!");
                Pq1 pq1 = (Pq1) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(Nt1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new Ut1(pq1, (Nt1) readParcelable2);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ut1[] newArray(int i) {
            return new Ut1[i];
        }
    }

    @DexIgnore
    public Ut1(Pq1 pq1, Nt1 nt1) {
        super(pq1, nt1);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            Nt1 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.toJSONObject());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    D90.i.i(e);
                }
                String jSONObject4 = jSONObject2.toString();
                Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = Hd0.y.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
            Wg6.i();
            throw null;
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
    }
}
