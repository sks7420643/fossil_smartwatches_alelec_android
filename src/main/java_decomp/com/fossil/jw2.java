package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jw2 implements Nv2 {
    @DexIgnore
    public static /* final */ Map<String, Jw2> f; // = new Zi0();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences.OnSharedPreferenceChangeListener b; // = new Iw2(this);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, ?> d;
    @DexIgnore
    public /* final */ List<Ov2> e; // = new ArrayList();

    @DexIgnore
    public Jw2(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
        sharedPreferences.registerOnSharedPreferenceChangeListener(this.b);
    }

    @DexIgnore
    public static Jw2 a(Context context, String str) {
        Jw2 jw2;
        if (!((!Hv2.a() || str.startsWith("direct_boot:")) ? true : Hv2.b(context))) {
            return null;
        }
        synchronized (Jw2.class) {
            try {
                jw2 = f.get(str);
                if (jw2 == null) {
                    jw2 = new Jw2(d(context, str));
                    f.put(str, jw2);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return jw2;
    }

    @DexIgnore
    public static void b() {
        synchronized (Jw2.class) {
            try {
                for (Jw2 jw2 : f.values()) {
                    jw2.a.unregisterOnSharedPreferenceChangeListener(jw2.b);
                }
                f.clear();
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static SharedPreferences d(Context context, String str) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (str.startsWith("direct_boot:")) {
                if (Hv2.a()) {
                    context = context.createDeviceProtectedStorageContext();
                }
                return context.getSharedPreferences(str.substring(12), 0);
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences(str, 0);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return sharedPreferences;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public final /* synthetic */ void c(SharedPreferences sharedPreferences, String str) {
        synchronized (this.c) {
            this.d = null;
            Xv2.g();
        }
        synchronized (this) {
            for (Ov2 ov2 : this.e) {
                ov2.zza();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Nv2
    public final Object zza(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        map = this.a.getAll();
                        this.d = map;
                    } finally {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                    }
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }
}
