package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F63 implements Xw2<I63> {
    @DexIgnore
    public static F63 c; // = new F63();
    @DexIgnore
    public /* final */ Xw2<I63> b;

    @DexIgnore
    public F63() {
        this(Ww2.b(new H63()));
    }

    @DexIgnore
    public F63(Xw2<I63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((I63) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ I63 zza() {
        return this.b.zza();
    }
}
