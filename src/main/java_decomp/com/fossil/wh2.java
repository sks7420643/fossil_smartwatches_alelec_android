package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wh2 extends Zc2 {
    @DexIgnore
    public static /* final */ Wh2 A; // = D("circumference");
    @DexIgnore
    public static /* final */ Wh2 B; // = D("percentage");
    @DexIgnore
    public static /* final */ Wh2 C; // = D(PlaceManager.PARAM_SPEED);
    @DexIgnore
    public static /* final */ Parcelable.Creator<Wh2> CREATOR; // = new Qi2();
    @DexIgnore
    public static /* final */ Wh2 D; // = D("rpm");
    @DexIgnore
    public static /* final */ Wh2 E; // = o0("google.android.fitness.GoalV2");
    @DexIgnore
    public static /* final */ Wh2 F; // = o0("symptom");
    @DexIgnore
    public static /* final */ Wh2 G; // = o0("google.android.fitness.StrideModel");
    @DexIgnore
    public static /* final */ Wh2 H; // = o0("google.android.fitness.Device");
    @DexIgnore
    public static /* final */ Wh2 I; // = k("revolutions");
    @DexIgnore
    public static /* final */ Wh2 J; // = D("calories");
    @DexIgnore
    public static /* final */ Wh2 K; // = D("watts");
    @DexIgnore
    public static /* final */ Wh2 L; // = D("volume");
    @DexIgnore
    public static /* final */ Wh2 M; // = A("meal_type");
    @DexIgnore
    public static /* final */ Wh2 N; // = new Wh2("food_item", 3, Boolean.TRUE);
    @DexIgnore
    public static /* final */ Wh2 O; // = L("nutrients");
    @DexIgnore
    public static /* final */ Wh2 P; // = D("elevation.change");
    @DexIgnore
    public static /* final */ Wh2 Q; // = L("elevation.gain");
    @DexIgnore
    public static /* final */ Wh2 R; // = L("elevation.loss");
    @DexIgnore
    public static /* final */ Wh2 S; // = D("floors");
    @DexIgnore
    public static /* final */ Wh2 T; // = L("floor.gain");
    @DexIgnore
    public static /* final */ Wh2 U; // = L("floor.loss");
    @DexIgnore
    public static /* final */ Wh2 V; // = new Wh2("exercise", 3);
    @DexIgnore
    public static /* final */ Wh2 W; // = A("repetitions");
    @DexIgnore
    public static /* final */ Wh2 X; // = F("resistance");
    @DexIgnore
    public static /* final */ Wh2 Y; // = A("resistance_type");
    @DexIgnore
    public static /* final */ Wh2 Z; // = k("num_segments");
    @DexIgnore
    public static /* final */ Wh2 a0; // = D(GoalTrackingSummary.COLUMN_AVERAGE);
    @DexIgnore
    public static /* final */ Wh2 b0; // = D("max");
    @DexIgnore
    public static /* final */ Wh2 c0; // = D("min");
    @DexIgnore
    public static /* final */ Wh2 d0; // = D("low_latitude");
    @DexIgnore
    public static /* final */ Wh2 e; // = k(Constants.ACTIVITY);
    @DexIgnore
    public static /* final */ Wh2 e0; // = D("low_longitude");
    @DexIgnore
    public static /* final */ Wh2 f; // = D("confidence");
    @DexIgnore
    public static /* final */ Wh2 f0; // = D("high_latitude");
    @DexIgnore
    @Deprecated
    public static /* final */ Wh2 g; // = L("activity_confidence");
    @DexIgnore
    public static /* final */ Wh2 g0; // = D("high_longitude");
    @DexIgnore
    public static /* final */ Wh2 h; // = k("steps");
    @DexIgnore
    public static /* final */ Wh2 h0; // = k("occurrences");
    @DexIgnore
    public static /* final */ Wh2 i; // = D("step_length");
    @DexIgnore
    public static /* final */ Wh2 i0; // = k("sensor_type");
    @DexIgnore
    public static /* final */ Wh2 j; // = k("duration");
    @DexIgnore
    public static /* final */ Wh2 j0; // = k("sensor_types");
    @DexIgnore
    public static /* final */ Wh2 k; // = A("duration");
    @DexIgnore
    public static /* final */ Wh2 k0; // = new Wh2("timestamps", 5);
    @DexIgnore
    public static /* final */ Wh2 l; // = L("activity_duration.ascending");
    @DexIgnore
    public static /* final */ Wh2 l0; // = k("sample_period");
    @DexIgnore
    public static /* final */ Wh2 m; // = L("activity_duration.descending");
    @DexIgnore
    public static /* final */ Wh2 m0; // = k("num_samples");
    @DexIgnore
    public static /* final */ Wh2 n0; // = k("num_dimensions");
    @DexIgnore
    public static /* final */ Wh2 o0; // = new Wh2("sensor_values", 6);
    @DexIgnore
    public static /* final */ Wh2 p0; // = D("intensity");
    @DexIgnore
    public static /* final */ Wh2 q0; // = D("probability");
    @DexIgnore
    public static /* final */ Wh2 s; // = D("bpm");
    @DexIgnore
    public static /* final */ Wh2 t; // = D("latitude");
    @DexIgnore
    public static /* final */ Wh2 u; // = D("longitude");
    @DexIgnore
    public static /* final */ Wh2 v; // = D(PlaceManager.PARAM_ACCURACY);
    @DexIgnore
    public static /* final */ Wh2 w; // = F(PlaceManager.PARAM_ALTITUDE);
    @DexIgnore
    public static /* final */ Wh2 x; // = D("distance");
    @DexIgnore
    public static /* final */ Wh2 y; // = D("height");
    @DexIgnore
    public static /* final */ Wh2 z; // = D(Constants.PROFILE_KEY_UNITS_WEIGHT);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Wh2 a; // = Wh2.D("x");
        @DexIgnore
        public static /* final */ Wh2 b; // = Wh2.D("y");
        @DexIgnore
        public static /* final */ Wh2 c; // = Wh2.D("z");
        @DexIgnore
        public static /* final */ Wh2 d; // = Wh2.p0("debug_session");
        @DexIgnore
        public static /* final */ Wh2 e; // = Wh2.p0("google.android.fitness.SessionV2");
        @DexIgnore
        public static /* final */ Wh2 f; // = Wh2.o0("google.android.fitness.DataPointSession");
    }

    @DexIgnore
    public Wh2(String str, int i2) {
        this(str, i2, null);
    }

    @DexIgnore
    public Wh2(String str, int i2, Boolean bool) {
        Rc2.k(str);
        this.b = str;
        this.c = i2;
        this.d = bool;
    }

    @DexIgnore
    public static Wh2 A(String str) {
        return new Wh2(str, 1, Boolean.TRUE);
    }

    @DexIgnore
    public static Wh2 D(String str) {
        return new Wh2(str, 2);
    }

    @DexIgnore
    public static Wh2 F(String str) {
        return new Wh2(str, 2, Boolean.TRUE);
    }

    @DexIgnore
    public static Wh2 L(String str) {
        return new Wh2(str, 4);
    }

    @DexIgnore
    public static Wh2 k(String str) {
        return new Wh2(str, 1);
    }

    @DexIgnore
    public static Wh2 o0(String str) {
        return new Wh2(str, 7);
    }

    @DexIgnore
    public static Wh2 p0(String str) {
        return new Wh2(str, 7, Boolean.TRUE);
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Wh2)) {
            return false;
        }
        Wh2 wh2 = (Wh2) obj;
        return this.b.equals(wh2.b) && this.c == wh2.c;
    }

    @DexIgnore
    public final String f() {
        return this.b;
    }

    @DexIgnore
    public final Boolean h() {
        return this.d;
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        return String.format("%s(%s)", this.b, this.c == 1 ? "i" : "f");
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 1, f(), false);
        Bd2.n(parcel, 2, c());
        Bd2.d(parcel, 3, h(), false);
        Bd2.b(parcel, a2);
    }
}
