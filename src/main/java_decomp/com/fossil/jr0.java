package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jr0<T> {
    @DexIgnore
    public Rm6 a;
    @DexIgnore
    public Rm6 b;
    @DexIgnore
    public /* final */ Nr0<T> c;
    @DexIgnore
    public /* final */ Coroutine<Hs0<T>, Xe6<? super Cd6>, Object> d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ Il6 f;
    @DexIgnore
    public /* final */ Gg6<Cd6> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "androidx.lifecycle.BlockRunner$cancel$1", f = "CoroutineLiveData.kt", l = {187}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Jr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Jr0 jr0, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = jr0;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                long j = this.this$0.e;
                this.L$0 = il6;
                this.label = 1;
                if (Uv7.a(j, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!this.this$0.c.g()) {
                Rm6 rm6 = this.this$0.a;
                if (rm6 != null) {
                    Rm6.Ai.a(rm6, null, 1, null);
                }
                this.this$0.a = null;
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "androidx.lifecycle.BlockRunner$maybeRun$1", f = "CoroutineLiveData.kt", l = {176}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Jr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Jr0 jr0, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = jr0;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Is0 is0 = new Is0(this.this$0.c, il6.h());
                Coroutine coroutine = this.this$0.d;
                this.L$0 = il6;
                this.L$1 = is0;
                this.label = 1;
                if (coroutine.invoke(is0, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Is0 is02 = (Is0) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.g.invoke();
            return Cd6.a;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Coroutine<? super com.fossil.Hs0<T>, ? super com.mapped.Xe6<? super com.mapped.Cd6>, ? extends java.lang.Object> */
    /* JADX WARN: Multi-variable type inference failed */
    public Jr0(Nr0<T> nr0, Coroutine<? super Hs0<T>, ? super Xe6<? super Cd6>, ? extends Object> coroutine, long j, Il6 il6, Gg6<Cd6> gg6) {
        Wg6.c(nr0, "liveData");
        Wg6.c(coroutine, "block");
        Wg6.c(il6, "scope");
        Wg6.c(gg6, "onDone");
        this.c = nr0;
        this.d = coroutine;
        this.e = j;
        this.f = il6;
        this.g = gg6;
    }

    @DexIgnore
    public final void g() {
        if (this.b == null) {
            this.b = Gu7.d(this.f, Bw7.c().S(), null, new Ai(this, null), 2, null);
            return;
        }
        throw new IllegalStateException("Cancel call cannot happen without a maybeRun".toString());
    }

    @DexIgnore
    public final void h() {
        Rm6 rm6 = this.b;
        if (rm6 != null) {
            Rm6.Ai.a(rm6, null, 1, null);
        }
        this.b = null;
        if (this.a == null) {
            this.a = Gu7.d(this.f, null, null, new Bi(this, null), 3, null);
        }
    }
}
