package com.fossil;

import com.fossil.Ec4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gc4 implements Hc4 {
    @DexIgnore
    public /* final */ Ic4 a;
    @DexIgnore
    public /* final */ Jc4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Ec4.Ai.values().length];
            a = iArr;
            try {
                iArr[Ec4.Ai.JAVA.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Ec4.Ai.NATIVE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public Gc4(Ic4 ic4, Jc4 jc4) {
        this.a = ic4;
        this.b = jc4;
    }

    @DexIgnore
    @Override // com.fossil.Hc4
    public boolean b(Cc4 cc4, boolean z) {
        int i = Ai.a[cc4.c.getType().ordinal()];
        if (i == 1) {
            this.a.b(cc4, z);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.b.b(cc4, z);
            return true;
        }
    }
}
