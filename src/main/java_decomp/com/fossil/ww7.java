package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Rm6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ww7 extends Ex7<Rm6> {
    @DexIgnore
    public /* final */ Hg6<Throwable, Cd6> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Hg6<? super java.lang.Throwable, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Ww7(Rm6 rm6, Hg6<? super Throwable, Cd6> hg6) {
        super(rm6);
        this.f = hg6;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        w(th);
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return "InvokeOnCompletion[" + Ov7.a(this) + '@' + Ov7.b(this) + ']';
    }

    @DexIgnore
    @Override // com.fossil.Zu7
    public void w(Throwable th) {
        this.f.invoke(th);
    }
}
