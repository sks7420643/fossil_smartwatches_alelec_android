package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.portfolio.platform.view.ColorPanelView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V47 extends BaseAdapter {
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public /* final */ int[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi {
        @DexIgnore
        public View a;
        @DexIgnore
        public ColorPanelView b;
        @DexIgnore
        public ImageView c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ int b;

            @DexIgnore
            public Aii(int i) {
                this.b = i;
            }

            @DexIgnore
            public void onClick(View view) {
                V47 v47 = V47.this;
                int i = v47.d;
                int i2 = this.b;
                if (i != i2) {
                    v47.d = i2;
                    v47.notifyDataSetChanged();
                }
                V47 v472 = V47.this;
                v472.b.a(v472.c[this.b]);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements View.OnLongClickListener {
            @DexIgnore
            public Bii() {
            }

            @DexIgnore
            public boolean onLongClick(View view) {
                Bi.this.b.d();
                return true;
            }
        }

        @DexIgnore
        public Bi(Context context) {
            View inflate = View.inflate(context, V47.this.e == 0 ? 2131558456 : 2131558455, null);
            this.a = inflate;
            this.b = (ColorPanelView) inflate.findViewById(2131362166);
            this.c = (ImageView) this.a.findViewById(2131362163);
            this.d = this.b.getBorderColor();
            this.a.setTag(this);
        }

        @DexIgnore
        public final void a(int i) {
            V47 v47 = V47.this;
            if (i != v47.d || Pl0.b(v47.c[i]) < 0.65d) {
                this.c.setColorFilter((ColorFilter) null);
            } else {
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            }
        }

        @DexIgnore
        public final void b(int i) {
            this.b.setOnClickListener(new Aii(i));
            this.b.setOnLongClickListener(new Bii());
        }

        @DexIgnore
        public void c(int i) {
            int i2 = V47.this.c[i];
            int alpha = Color.alpha(i2);
            this.b.setColor(i2);
            this.c.setImageResource(V47.this.d == i ? 2131230942 : 0);
            if (alpha == 255) {
                a(i);
            } else if (alpha <= 165) {
                this.b.setBorderColor(i2 | -16777216);
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            } else {
                this.b.setBorderColor(this.d);
                this.c.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
            }
            b(i);
        }
    }

    @DexIgnore
    public V47(Ai ai, int[] iArr, int i, int i2) {
        this.b = ai;
        this.c = iArr;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    public void a() {
        this.d = -1;
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getCount() {
        return this.c.length;
    }

    @DexIgnore
    public Object getItem(int i) {
        return Integer.valueOf(this.c[i]);
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        Bi bi;
        if (view == null) {
            bi = new Bi(viewGroup.getContext());
            view = bi.a;
        } else {
            bi = (Bi) view.getTag();
        }
        bi.c(i);
        return view;
    }
}
