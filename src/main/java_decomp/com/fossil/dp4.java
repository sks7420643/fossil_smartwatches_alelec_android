package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dp4 implements Factory<AuthApiUserService> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<AuthenticationInterceptor> b;
    @DexIgnore
    public /* final */ Provider<Uq5> c;

    @DexIgnore
    public Dp4(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static Dp4 a(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        return new Dp4(uo4, provider, provider2);
    }

    @DexIgnore
    public static AuthApiUserService c(Uo4 uo4, AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        AuthApiUserService k = uo4.k(authenticationInterceptor, uq5);
        Lk7.c(k, "Cannot return null from a non-@Nullable @Provides method");
        return k;
    }

    @DexIgnore
    public AuthApiUserService b() {
        return c(this.a, this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
