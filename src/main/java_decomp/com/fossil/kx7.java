package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kx7 extends Jz7 implements Sw7 {
    @DexIgnore
    @Override // com.fossil.Sw7
    public Kx7 b() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public boolean isActive() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return Nv7.c() ? x("Active") : super.toString();
    }

    @DexIgnore
    public final String x(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        Object l = l();
        if (l != null) {
            boolean z = true;
            Lz7 lz7 = (Lz7) l;
            while (!Wg6.a(lz7, this)) {
                if (lz7 instanceof Ex7) {
                    Ex7 ex7 = (Ex7) lz7;
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(ex7);
                }
                lz7 = lz7.m();
                z = z;
            }
            sb.append("]");
            String sb2 = sb.toString();
            Wg6.b(sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
}
