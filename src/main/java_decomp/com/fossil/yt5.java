package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ on5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final FirmwareData a(on5 on5, String str) {
            pq7.c(on5, "sharedPreferencesManager");
            pq7.c(str, "deviceSKU");
            if (vt7.j("release", "release", true) || !on5.Y()) {
                Firmware e = mn5.p.a().g().e(str);
                if (e != null) {
                    return FirmwareFactory.getInstance().createFirmwareData(e.getVersionNumber(), e.getDeviceModel(), e.getChecksum());
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = b();
                local.e(b, "Error when update firmware, can't find latest fw of model=" + str);
                return null;
            }
            Firmware k = on5.k(str);
            if (k != null) {
                return FirmwareFactory.getInstance().createFirmwareData(k.getVersionNumber(), k.getDeviceModel(), k.getChecksum());
            }
            Firmware e2 = mn5.p.a().g().e(str);
            if (e2 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(e2.getVersionNumber(), e2.getDeviceModel(), e2.getChecksum());
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String b2 = b();
            local2.e(b2, "Error when update firmware, can't find latest fw of model=" + str);
            return null;
        }

        @DexIgnore
        public final String b() {
            return yt5.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f4369a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(String str, boolean z) {
            pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.f4369a = str;
            this.b = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(String str, boolean z, int i, kq7 kq7) {
            this(str, (i & 2) != 0 ? false : z);
        }

        @DexIgnore
        public final String a() {
            return this.f4369a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase", f = "UpdateFirmwareUsecase.kt", l = {45, 54}, m = "run")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(yt5 yt5, qn7 qn7) {
            super(qn7);
            this.this$0 = yt5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = yt5.class.getSimpleName();
        pq7.b(simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public yt5(DeviceRepository deviceRepository, on5 on5) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f A[Catch:{ Exception -> 0x0168 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01f4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: n */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.yt5.b r11, com.fossil.qn7<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 514
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt5.k(com.fossil.yt5$b, com.fossil.qn7):java.lang.Object");
    }
}
