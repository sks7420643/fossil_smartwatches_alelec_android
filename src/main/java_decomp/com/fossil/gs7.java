package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Gs7<R> extends Ds7<R>, Uk7<R> {
    @DexIgnore
    boolean isExternal();

    @DexIgnore
    boolean isInfix();

    @DexIgnore
    boolean isInline();

    @DexIgnore
    boolean isOperator();

    @DexIgnore
    @Override // com.fossil.Ds7
    boolean isSuspend();
}
