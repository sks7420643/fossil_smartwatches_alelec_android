package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rz7 {
    @DexIgnore
    public abstract Cz7<?> a();

    @DexIgnore
    public final boolean b(Rz7 rz7) {
        Cz7<?> a2;
        Cz7<?> a3 = a();
        return (a3 == null || (a2 = rz7.a()) == null || a3.f() >= a2.f()) ? false : true;
    }

    @DexIgnore
    public abstract Object c(Object obj);

    @DexIgnore
    public String toString() {
        return Ov7.a(this) + '@' + Ov7.b(this);
    }
}
