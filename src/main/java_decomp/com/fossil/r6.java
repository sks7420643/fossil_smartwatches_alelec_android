package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class R6 extends Enum<R6> {
    @DexIgnore
    public static /* final */ R6 c;
    @DexIgnore
    public static /* final */ R6 d;
    @DexIgnore
    public static /* final */ /* synthetic */ R6[] e;
    @DexIgnore
    public static /* final */ Q6 f; // = new Q6(null);
    @DexIgnore
    public /* final */ int b;

    /*
    static {
        R6 r6 = new R6("PROPERTY_BROADCAST", 0, 1);
        R6 r62 = new R6("PROPERTY_READ", 1, 2);
        R6 r63 = new R6("PROPERTY_WRITE_NO_RESPONSE", 2, 4);
        R6 r64 = new R6("PROPERTY_WRITE", 3, 8);
        R6 r65 = new R6("PROPERTY_NOTIFY", 4, 16);
        c = r65;
        R6 r66 = new R6("PROPERTY_INDICATE", 5, 32);
        d = r66;
        e = new R6[]{r6, r62, r63, r64, r65, r66, new R6("PROPERTY_SIGNED_WRITE", 6, 64), new R6("PROPERTY_EXTENDED_PROPS", 7, 128)};
    }
    */

    @DexIgnore
    public R6(String str, int i, int i2) {
        this.b = i2;
    }

    @DexIgnore
    public static R6 valueOf(String str) {
        return (R6) Enum.valueOf(R6.class, str);
    }

    @DexIgnore
    public static R6[] values() {
        return (R6[]) e.clone();
    }
}
