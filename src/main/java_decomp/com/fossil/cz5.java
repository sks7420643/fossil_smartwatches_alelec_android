package com.fossil;

import com.mapped.HomeDashboardFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cz5 implements MembersInjector<HomeDashboardFragment> {
    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardActiveTimePresenter dashboardActiveTimePresenter) {
        homeDashboardFragment.H = dashboardActiveTimePresenter;
    }

    @DexIgnore
    public static void b(HomeDashboardFragment homeDashboardFragment, DashboardActivityPresenter dashboardActivityPresenter) {
        homeDashboardFragment.G = dashboardActivityPresenter;
    }

    @DexIgnore
    public static void c(HomeDashboardFragment homeDashboardFragment, DashboardCaloriesPresenter dashboardCaloriesPresenter) {
        homeDashboardFragment.I = dashboardCaloriesPresenter;
    }

    @DexIgnore
    public static void d(HomeDashboardFragment homeDashboardFragment, DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
        homeDashboardFragment.L = dashboardGoalTrackingPresenter;
    }

    @DexIgnore
    public static void e(HomeDashboardFragment homeDashboardFragment, DashboardHeartRatePresenter dashboardHeartRatePresenter) {
        homeDashboardFragment.J = dashboardHeartRatePresenter;
    }

    @DexIgnore
    public static void f(HomeDashboardFragment homeDashboardFragment, DashboardSleepPresenter dashboardSleepPresenter) {
        homeDashboardFragment.K = dashboardSleepPresenter;
    }
}
