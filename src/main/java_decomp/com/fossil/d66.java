package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fsl.contact.Contact;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D66 extends BaseFragment implements C66, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public B66 g;
    @DexIgnore
    public G37<J95> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return D66.j;
        }

        @DexIgnore
        public final D66 b() {
            return new D66();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D66 b;

        @DexIgnore
        public Bi(D66 d66) {
            this.b = d66;
        }

        @DexIgnore
        public final void onClick(View view) {
            D66.K6(this.b).q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D66 b;

        @DexIgnore
        public Ci(D66 d66) {
            this.b = d66;
        }

        @DexIgnore
        public final void onClick(View view) {
            D66.K6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D66 b;

        @DexIgnore
        public Di(D66 d66) {
            this.b = d66;
        }

        @DexIgnore
        public final void onClick(View view) {
            D66.K6(this.b).p();
        }
    }

    /*
    static {
        String simpleName = D66.class.getSimpleName();
        Wg6.b(simpleName, "NotificationHybridEveryo\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ B66 K6(D66 d66) {
        B66 b66 = d66.g;
        if (b66 != null) {
            return b66;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.V78.Ai
    public void C4(int i2, List<String> list) {
        Wg6.c(list, "perms");
        FLogger.INSTANCE.getLocal().d(j, ".Inside onPermissionsGranted");
        B66 b66 = this.g;
        if (b66 != null) {
            b66.s();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        B66 b66 = this.g;
        if (b66 != null) {
            b66.q();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.C66
    public void I(ArrayList<J06> arrayList) {
        Wg6.c(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(B66 b66) {
        M6(b66);
    }

    @DexIgnore
    public void M6(B66 b66) {
        Wg6.c(b66, "presenter");
        this.g = b66;
    }

    @DexIgnore
    @Override // com.fossil.C66
    public void N0(J06 j06) {
        Wg6.c(j06, "contactWrapper");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            int currentHandGroup = j06.getCurrentHandGroup();
            B66 b66 = this.g;
            if (b66 != null) {
                s37.b0(childFragmentManager, j06, currentHandGroup, b66.n());
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        J06 j06;
        Wg6.c(str, "tag");
        if (str.hashCode() == 1018078562 && str.equals("CONFIRM_REASSIGN_CONTACT") && i2 == 2131363373) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (j06 = (J06) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                B66 b66 = this.g;
                if (b66 != null) {
                    b66.r(j06);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.C66
    public void V3(List<J06> list, int i2) {
        FlexibleCheckBox flexibleCheckBox;
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        FlexibleCheckBox flexibleCheckBox2;
        FlexibleTextView flexibleTextView3;
        FlexibleTextView flexibleTextView4;
        FlexibleCheckBox flexibleCheckBox3;
        FlexibleTextView flexibleTextView5;
        FlexibleTextView flexibleTextView6;
        FlexibleCheckBox flexibleCheckBox4;
        FlexibleTextView flexibleTextView7;
        FlexibleTextView flexibleTextView8;
        FlexibleCheckBox flexibleCheckBox5;
        FlexibleTextView flexibleTextView9;
        FlexibleTextView flexibleTextView10;
        FlexibleCheckBox flexibleCheckBox6;
        FlexibleTextView flexibleTextView11;
        FlexibleTextView flexibleTextView12;
        Wg6.c(list, "listContactWrapper");
        boolean z = false;
        boolean z2 = false;
        for (T t : list) {
            Contact contact = t.getContact();
            if (contact == null || contact.getContactId() != -100) {
                Contact contact2 = t.getContact();
                if (contact2 != null && contact2.getContactId() == -200) {
                    if (t.getCurrentHandGroup() != i2) {
                        G37<J95> g37 = this.h;
                        if (g37 != null) {
                            J95 a2 = g37.a();
                            if (!(a2 == null || (flexibleTextView8 = a2.v) == null)) {
                                Hr7 hr7 = Hr7.a;
                                String c = Um5.c(PortfolioApp.get.instance(), 2131886157);
                                Wg6.b(c, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(t.getCurrentHandGroup())}, 1));
                                Wg6.b(format, "java.lang.String.format(format, *args)");
                                flexibleTextView8.setText(format);
                            }
                            G37<J95> g372 = this.h;
                            if (g372 != null) {
                                J95 a3 = g372.a();
                                if (!(a3 == null || (flexibleTextView7 = a3.v) == null)) {
                                    flexibleTextView7.setVisibility(0);
                                }
                                G37<J95> g373 = this.h;
                                if (g373 != null) {
                                    J95 a4 = g373.a();
                                    if (!(a4 == null || (flexibleCheckBox4 = a4.r) == null)) {
                                        flexibleCheckBox4.setChecked(false);
                                    }
                                } else {
                                    Wg6.n("mBinding");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mBinding");
                                throw null;
                            }
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    } else {
                        G37<J95> g374 = this.h;
                        if (g374 != null) {
                            J95 a5 = g374.a();
                            if (!(a5 == null || (flexibleTextView6 = a5.v) == null)) {
                                flexibleTextView6.setText("");
                            }
                            G37<J95> g375 = this.h;
                            if (g375 != null) {
                                J95 a6 = g375.a();
                                if (!(a6 == null || (flexibleTextView5 = a6.v) == null)) {
                                    flexibleTextView5.setVisibility(8);
                                }
                                G37<J95> g376 = this.h;
                                if (g376 != null) {
                                    J95 a7 = g376.a();
                                    if (!(a7 == null || (flexibleCheckBox3 = a7.r) == null)) {
                                        flexibleCheckBox3.setChecked(true);
                                    }
                                } else {
                                    Wg6.n("mBinding");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mBinding");
                                throw null;
                            }
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    }
                    z = true;
                }
            } else {
                if (t.getCurrentHandGroup() != i2) {
                    G37<J95> g377 = this.h;
                    if (g377 != null) {
                        J95 a8 = g377.a();
                        if (!(a8 == null || (flexibleTextView12 = a8.u) == null)) {
                            Hr7 hr72 = Hr7.a;
                            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886157);
                            Wg6.b(c2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                            String format2 = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(t.getCurrentHandGroup())}, 1));
                            Wg6.b(format2, "java.lang.String.format(format, *args)");
                            flexibleTextView12.setText(format2);
                        }
                        G37<J95> g378 = this.h;
                        if (g378 != null) {
                            J95 a9 = g378.a();
                            if (!(a9 == null || (flexibleTextView11 = a9.u) == null)) {
                                flexibleTextView11.setVisibility(0);
                            }
                            G37<J95> g379 = this.h;
                            if (g379 != null) {
                                J95 a10 = g379.a();
                                if (!(a10 == null || (flexibleCheckBox6 = a10.q) == null)) {
                                    flexibleCheckBox6.setChecked(false);
                                }
                            } else {
                                Wg6.n("mBinding");
                                throw null;
                            }
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                } else {
                    G37<J95> g3710 = this.h;
                    if (g3710 != null) {
                        J95 a11 = g3710.a();
                        if (!(a11 == null || (flexibleTextView10 = a11.u) == null)) {
                            flexibleTextView10.setText("");
                        }
                        G37<J95> g3711 = this.h;
                        if (g3711 != null) {
                            J95 a12 = g3711.a();
                            if (!(a12 == null || (flexibleTextView9 = a12.u) == null)) {
                                flexibleTextView9.setVisibility(8);
                            }
                            G37<J95> g3712 = this.h;
                            if (g3712 != null) {
                                J95 a13 = g3712.a();
                                if (!(a13 == null || (flexibleCheckBox5 = a13.q) == null)) {
                                    flexibleCheckBox5.setChecked(true);
                                }
                            } else {
                                Wg6.n("mBinding");
                                throw null;
                            }
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                }
                z2 = true;
            }
        }
        if (!z2) {
            G37<J95> g3713 = this.h;
            if (g3713 != null) {
                J95 a14 = g3713.a();
                if (!(a14 == null || (flexibleTextView4 = a14.u) == null)) {
                    flexibleTextView4.setText("");
                }
                G37<J95> g3714 = this.h;
                if (g3714 != null) {
                    J95 a15 = g3714.a();
                    if (!(a15 == null || (flexibleTextView3 = a15.u) == null)) {
                        flexibleTextView3.setVisibility(8);
                    }
                    G37<J95> g3715 = this.h;
                    if (g3715 != null) {
                        J95 a16 = g3715.a();
                        if (!(a16 == null || (flexibleCheckBox2 = a16.q) == null)) {
                            flexibleCheckBox2.setChecked(false);
                        }
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        if (!z) {
            G37<J95> g3716 = this.h;
            if (g3716 != null) {
                J95 a17 = g3716.a();
                if (!(a17 == null || (flexibleTextView2 = a17.v) == null)) {
                    flexibleTextView2.setText("");
                }
                G37<J95> g3717 = this.h;
                if (g3717 != null) {
                    J95 a18 = g3717.a();
                    if (!(a18 == null || (flexibleTextView = a18.v) == null)) {
                        flexibleTextView.setVisibility(8);
                    }
                    G37<J95> g3718 = this.h;
                    if (g3718 != null) {
                        J95 a19 = g3718.a();
                        if (!(a19 == null || (flexibleCheckBox = a19.r) == null)) {
                            flexibleCheckBox.setChecked(false);
                            return;
                        }
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.V78.Ai
    public void k1(int i2, List<String> list) {
        Wg6.c(list, "perms");
        FLogger.INSTANCE.getLocal().d(j, ".Inside onPermissionsDenied");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "Permission Denied : " + it.next());
        }
        if (V78.f(this, list) && isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.h0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        J95 j95 = (J95) Aq0.f(layoutInflater, 2131558595, viewGroup, false, A6());
        j95.x.setOnClickListener(new Bi(this));
        j95.q.setOnClickListener(new Ci(this));
        j95.r.setOnClickListener(new Di(this));
        String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d)) {
            j95.y.setBackgroundColor(Color.parseColor(d));
        }
        this.h = new G37<>(this, j95);
        Wg6.b(j95, "binding");
        return j95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        B66 b66 = this.g;
        if (b66 != null) {
            b66.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        B66 b66 = this.g;
        if (b66 != null) {
            b66.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
