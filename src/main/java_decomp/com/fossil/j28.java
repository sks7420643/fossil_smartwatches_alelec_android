package com.fossil;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J28 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;

    @DexIgnore
    public J28(OkHttpClient okHttpClient) {
        this.a = okHttpClient;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        W28 w28 = (W28) chain;
        V18 c = w28.c();
        P28 k = w28.k();
        return w28.j(c, k, k.i(this.a, chain, !c.g().equals("GET")), k.d());
    }
}
