package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jh1 extends Zg1<Hh1> implements Ed1 {
    @DexIgnore
    public Jh1(Hh1 hh1) {
        super(hh1);
    }

    @DexIgnore
    @Override // com.fossil.Ed1, com.fossil.Zg1
    public void a() {
        ((Hh1) this.b).e().prepareToDraw();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
        ((Hh1) this.b).stop();
        ((Hh1) this.b).k();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public int c() {
        return ((Hh1) this.b).i();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Class<Hh1> d() {
        return Hh1.class;
    }
}
