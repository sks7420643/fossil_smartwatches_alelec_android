package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface H97 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    void b(String str);

    @DexIgnore
    long c(G97 g97);

    @DexIgnore
    List<G97> d();

    @DexIgnore
    List<G97> e();

    @DexIgnore
    List<G97> f();

    @DexIgnore
    G97 g(String str);

    @DexIgnore
    void h(String str);

    @DexIgnore
    Long[] insert(List<G97> list);
}
