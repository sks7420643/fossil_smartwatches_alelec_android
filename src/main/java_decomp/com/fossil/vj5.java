package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vj5<TranscodeType> extends Va1<TranscodeType> implements Cloneable {
    @DexIgnore
    public Vj5(Oa1 oa1, Wa1 wa1, Class<TranscodeType> cls, Context context) {
        super(oa1, wa1, cls, context);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 I0(Bitmap bitmap) {
        return c1(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 J0(Uri uri) {
        return d1(uri);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 K0(File file) {
        return e1(file);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 L0(Integer num) {
        return f1(num);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 M0(Object obj) {
        return g1(obj);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 N0(String str) {
        return h1(str);
    }

    @DexIgnore
    public Vj5<TranscodeType> S0(Ej1<TranscodeType> ej1) {
        super.t0(ej1);
        return this;
    }

    @DexIgnore
    public Vj5<TranscodeType> T0(Yi1<?> yi1) {
        return (Vj5) super.u0(yi1);
    }

    @DexIgnore
    public Vj5<TranscodeType> U0() {
        return (Vj5) super.g();
    }

    @DexIgnore
    public Vj5<TranscodeType> V0() {
        return (Vj5) super.y0();
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 W() {
        return i1();
    }

    @DexIgnore
    public Vj5<TranscodeType> W0(Class<?> cls) {
        return (Vj5) super.j(cls);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 X() {
        return j1();
    }

    @DexIgnore
    public Vj5<TranscodeType> X0(Wc1 wc1) {
        return (Vj5) super.l(wc1);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 Y() {
        return k1();
    }

    @DexIgnore
    public Vj5<TranscodeType> Y0() {
        return (Vj5) super.n();
    }

    @DexIgnore
    public Vj5<TranscodeType> Z0(Fg1 fg1) {
        return (Vj5) super.o(fg1);
    }

    @DexIgnore
    public Vj5<TranscodeType> a1(Va1<TranscodeType> va1) {
        super.z0(va1);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 b0(int i, int i2) {
        return l1(i, i2);
    }

    @DexIgnore
    public Vj5<TranscodeType> b1(Ej1<TranscodeType> ej1) {
        return (Vj5) super.H0(ej1);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 c0(int i) {
        return m1(i);
    }

    @DexIgnore
    public Vj5<TranscodeType> c1(Bitmap bitmap) {
        return (Vj5) super.I0(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Yi1, com.fossil.Va1, java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return V0();
    }

    @DexIgnore
    @Override // com.fossil.Yi1, com.fossil.Va1
    public /* bridge */ /* synthetic */ Yi1 d(Yi1 yi1) {
        return T0(yi1);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 d0(Drawable drawable) {
        return n1(drawable);
    }

    @DexIgnore
    public Vj5<TranscodeType> d1(Uri uri) {
        super.J0(uri);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 e0(Sa1 sa1) {
        return o1(sa1);
    }

    @DexIgnore
    public Vj5<TranscodeType> e1(File file) {
        super.K0(file);
        return this;
    }

    @DexIgnore
    public Vj5<TranscodeType> f1(Integer num) {
        return (Vj5) super.L0(num);
    }

    @DexIgnore
    public Vj5<TranscodeType> g1(Object obj) {
        super.M0(obj);
        return this;
    }

    @DexIgnore
    public Vj5<TranscodeType> h1(String str) {
        super.N0(str);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Yi1, com.fossil.Va1
    public /* bridge */ /* synthetic */ Yi1 i() {
        return V0();
    }

    @DexIgnore
    public Vj5<TranscodeType> i1() {
        return (Vj5) super.W();
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 j(Class cls) {
        return W0(cls);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 j0(Nb1 nb1, Object obj) {
        return p1(nb1, obj);
    }

    @DexIgnore
    public Vj5<TranscodeType> j1() {
        return (Vj5) super.X();
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 k0(Mb1 mb1) {
        return q1(mb1);
    }

    @DexIgnore
    public Vj5<TranscodeType> k1() {
        return (Vj5) super.Y();
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 l(Wc1 wc1) {
        return X0(wc1);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 l0(float f) {
        return r1(f);
    }

    @DexIgnore
    public Vj5<TranscodeType> l1(int i, int i2) {
        return (Vj5) super.b0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 m0(boolean z) {
        return s1(z);
    }

    @DexIgnore
    public Vj5<TranscodeType> m1(int i) {
        return (Vj5) super.c0(i);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 n() {
        return Y0();
    }

    @DexIgnore
    public Vj5<TranscodeType> n1(Drawable drawable) {
        return (Vj5) super.d0(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 o(Fg1 fg1) {
        return Z0(fg1);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 o0(Sb1 sb1) {
        return u1(sb1);
    }

    @DexIgnore
    public Vj5<TranscodeType> o1(Sa1 sa1) {
        return (Vj5) super.e0(sa1);
    }

    @DexIgnore
    public <Y> Vj5<TranscodeType> p1(Nb1<Y> nb1, Y y) {
        return (Vj5) super.j0(nb1, y);
    }

    @DexIgnore
    public Vj5<TranscodeType> q1(Mb1 mb1) {
        return (Vj5) super.k0(mb1);
    }

    @DexIgnore
    public Vj5<TranscodeType> r1(float f) {
        return (Vj5) super.l0(f);
    }

    @DexIgnore
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Yi1 s0(boolean z) {
        return v1(z);
    }

    @DexIgnore
    public Vj5<TranscodeType> s1(boolean z) {
        return (Vj5) super.m0(z);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 t0(Ej1 ej1) {
        return S0(ej1);
    }

    @DexIgnore
    public Vj5<TranscodeType> t1(int i) {
        return (Vj5) super.n0(i);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 u0(Yi1 yi1) {
        return T0(yi1);
    }

    @DexIgnore
    public Vj5<TranscodeType> u1(Sb1<Bitmap> sb1) {
        return (Vj5) super.o0(sb1);
    }

    @DexIgnore
    public Vj5<TranscodeType> v1(boolean z) {
        return (Vj5) super.s0(z);
    }

    @DexIgnore
    @Override // com.fossil.Va1
    public /* bridge */ /* synthetic */ Va1 y0() {
        return V0();
    }
}
