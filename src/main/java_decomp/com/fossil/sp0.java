package com.fossil;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import androidx.collection.SparseArrayCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Tp0;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sp0 extends Rn0 {
    @DexIgnore
    public static /* final */ Rect n; // = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    @DexIgnore
    public static /* final */ Tp0.Ai<Yo0> o; // = new Ai();
    @DexIgnore
    public static /* final */ Tp0.Bi<SparseArrayCompat<Yo0>, Yo0> p; // = new Bi();
    @DexIgnore
    public /* final */ Rect d; // = new Rect();
    @DexIgnore
    public /* final */ Rect e; // = new Rect();
    @DexIgnore
    public /* final */ Rect f; // = new Rect();
    @DexIgnore
    public /* final */ int[] g; // = new int[2];
    @DexIgnore
    public /* final */ AccessibilityManager h;
    @DexIgnore
    public /* final */ View i;
    @DexIgnore
    public Ci j;
    @DexIgnore
    public int k; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public int l; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public int m; // = RecyclerView.UNDEFINED_DURATION;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Tp0.Ai<Yo0> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, android.graphics.Rect] */
        @Override // com.fossil.Tp0.Ai
        public /* bridge */ /* synthetic */ void a(Yo0 yo0, Rect rect) {
            b(yo0, rect);
        }

        @DexIgnore
        public void b(Yo0 yo0, Rect rect) {
            yo0.m(rect);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tp0.Bi<SparseArrayCompat<Yo0>, Yo0> {
        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int] */
        @Override // com.fossil.Tp0.Bi
        public /* bridge */ /* synthetic */ Yo0 a(SparseArrayCompat<Yo0> sparseArrayCompat, int i) {
            return c(sparseArrayCompat, i);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tp0.Bi
        public /* bridge */ /* synthetic */ int b(SparseArrayCompat<Yo0> sparseArrayCompat) {
            return d(sparseArrayCompat);
        }

        @DexIgnore
        public Yo0 c(SparseArrayCompat<Yo0> sparseArrayCompat, int i) {
            return sparseArrayCompat.t(i);
        }

        @DexIgnore
        public int d(SparseArrayCompat<Yo0> sparseArrayCompat) {
            return sparseArrayCompat.s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Zo0 {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.Zo0
        public Yo0 a(int i) {
            return Yo0.Q(Sp0.this.H(i));
        }

        @DexIgnore
        @Override // com.fossil.Zo0
        public Yo0 c(int i) {
            int i2 = i == 2 ? Sp0.this.k : Sp0.this.l;
            if (i2 == Integer.MIN_VALUE) {
                return null;
            }
            return a(i2);
        }

        @DexIgnore
        @Override // com.fossil.Zo0
        public boolean e(int i, int i2, Bundle bundle) {
            return Sp0.this.P(i, i2, bundle);
        }
    }

    @DexIgnore
    public Sp0(View view) {
        if (view != null) {
            this.i = view;
            this.h = (AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (Mo0.x(view) == 0) {
                Mo0.v0(view, 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }

    @DexIgnore
    public static Rect D(View view, int i2, Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        if (i2 == 17) {
            rect.set(width, 0, width, height);
        } else if (i2 == 33) {
            rect.set(0, height, width, height);
        } else if (i2 == 66) {
            rect.set(-1, 0, -1, height);
        } else if (i2 == 130) {
            rect.set(0, -1, width, -1);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    @DexIgnore
    public static int F(int i2) {
        if (i2 == 19) {
            return 33;
        }
        if (i2 != 21) {
            return i2 != 22 ? 130 : 66;
        }
        return 17;
    }

    @DexIgnore
    public final int A() {
        return this.l;
    }

    @DexIgnore
    public abstract int B(float f2, float f3);

    @DexIgnore
    public abstract void C(List<Integer> list);

    @DexIgnore
    public final boolean E(Rect rect) {
        boolean z;
        if (rect != null) {
            if (rect.isEmpty()) {
                z = false;
            } else if (this.i.getWindowVisibility() != 0) {
                return false;
            } else {
                ViewParent parent = this.i.getParent();
                while (parent instanceof View) {
                    View view = (View) parent;
                    if (view.getAlpha() <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || view.getVisibility() != 0) {
                        return false;
                    }
                    parent = view.getParent();
                }
                if (parent != null) {
                    z = true;
                }
            }
            return z;
        }
        z = false;
        return z;
    }

    @DexIgnore
    public final boolean G(int i2, Rect rect) {
        Yo0 yo0;
        boolean z = true;
        SparseArrayCompat<Yo0> y = y();
        int i3 = this.l;
        Yo0 j2 = i3 == Integer.MIN_VALUE ? null : y.j(i3);
        if (i2 == 1 || i2 == 2) {
            if (Mo0.z(this.i) != 1) {
                z = false;
            }
            yo0 = (Yo0) Tp0.d(y, p, o, j2, i2, z, false);
        } else if (i2 == 17 || i2 == 33 || i2 == 66 || i2 == 130) {
            Rect rect2 = new Rect();
            int i4 = this.l;
            if (i4 != Integer.MIN_VALUE) {
                z(i4, rect2);
            } else if (rect != null) {
                rect2.set(rect);
            } else {
                D(this.i, i2, rect2);
            }
            yo0 = (Yo0) Tp0.c(y, p, o, j2, rect2, i2);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return T(yo0 == null ? Integer.MIN_VALUE : y.p(y.o(yo0)));
    }

    @DexIgnore
    public Yo0 H(int i2) {
        return i2 == -1 ? u() : t(i2);
    }

    @DexIgnore
    public final void I(boolean z, int i2, Rect rect) {
        int i3 = this.l;
        if (i3 != Integer.MIN_VALUE) {
            o(i3);
        }
        if (z) {
            G(i2, rect);
        }
    }

    @DexIgnore
    public abstract boolean J(int i2, int i3, Bundle bundle);

    @DexIgnore
    public void K(AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    public void L(int i2, AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    public void M(Yo0 yo0) {
    }

    @DexIgnore
    public abstract void N(int i2, Yo0 yo0);

    @DexIgnore
    public void O(int i2, boolean z) {
    }

    @DexIgnore
    public boolean P(int i2, int i3, Bundle bundle) {
        return i2 != -1 ? Q(i2, i3, bundle) : R(i3, bundle);
    }

    @DexIgnore
    public final boolean Q(int i2, int i3, Bundle bundle) {
        return i3 != 1 ? i3 != 2 ? i3 != 64 ? i3 != 128 ? J(i2, i3, bundle) : n(i2) : S(i2) : o(i2) : T(i2);
    }

    @DexIgnore
    public final boolean R(int i2, Bundle bundle) {
        return Mo0.a0(this.i, i2, bundle);
    }

    @DexIgnore
    public final boolean S(int i2) {
        int i3;
        if (!this.h.isEnabled() || !this.h.isTouchExplorationEnabled() || (i3 = this.k) == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            n(i3);
        }
        this.k = i2;
        this.i.invalidate();
        U(i2, 32768);
        return true;
    }

    @DexIgnore
    public final boolean T(int i2) {
        int i3;
        if ((!this.i.isFocused() && !this.i.requestFocus()) || (i3 = this.l) == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            o(i3);
        }
        this.l = i2;
        O(i2, true);
        U(i2, 8);
        return true;
    }

    @DexIgnore
    public final boolean U(int i2, int i3) {
        ViewParent parent;
        if (i2 == Integer.MIN_VALUE || !this.h.isEnabled() || (parent = this.i.getParent()) == null) {
            return false;
        }
        return Qo0.h(parent, this.i, q(i2, i3));
    }

    @DexIgnore
    public final void V(int i2) {
        int i3 = this.m;
        if (i3 != i2) {
            this.m = i2;
            U(i2, 128);
            U(i3, 256);
        }
    }

    @DexIgnore
    @Override // com.fossil.Rn0
    public Zo0 b(View view) {
        if (this.j == null) {
            this.j = new Ci();
        }
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.Rn0
    public void f(View view, AccessibilityEvent accessibilityEvent) {
        super.f(view, accessibilityEvent);
        K(accessibilityEvent);
    }

    @DexIgnore
    @Override // com.fossil.Rn0
    public void g(View view, Yo0 yo0) {
        super.g(view, yo0);
        M(yo0);
    }

    @DexIgnore
    public final boolean n(int i2) {
        if (this.k != i2) {
            return false;
        }
        this.k = RecyclerView.UNDEFINED_DURATION;
        this.i.invalidate();
        U(i2, 65536);
        return true;
    }

    @DexIgnore
    public final boolean o(int i2) {
        if (this.l != i2) {
            return false;
        }
        this.l = RecyclerView.UNDEFINED_DURATION;
        O(i2, false);
        U(i2, 8);
        return true;
    }

    @DexIgnore
    public final boolean p() {
        int i2 = this.l;
        return i2 != Integer.MIN_VALUE && J(i2, 16, null);
    }

    @DexIgnore
    public final AccessibilityEvent q(int i2, int i3) {
        return i2 != -1 ? r(i2, i3) : s(i3);
    }

    @DexIgnore
    public final AccessibilityEvent r(int i2, int i3) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i3);
        Yo0 H = H(i2);
        obtain.getText().add(H.x());
        obtain.setContentDescription(H.r());
        obtain.setScrollable(H.K());
        obtain.setPassword(H.J());
        obtain.setEnabled(H.F());
        obtain.setChecked(H.D());
        L(i2, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            obtain.setClassName(H.p());
            Ap0.c(obtain, this.i, i2);
            obtain.setPackageName(this.i.getContext().getPackageName());
            return obtain;
        }
        throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    @DexIgnore
    public final AccessibilityEvent s(int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        this.i.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }

    @DexIgnore
    public final Yo0 t(int i2) {
        Yo0 O = Yo0.O();
        O.j0(true);
        O.l0(true);
        O.c0("android.view.View");
        O.X(n);
        O.Y(n);
        O.t0(this.i);
        N(i2, O);
        if (O.x() == null && O.r() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        O.m(this.e);
        if (!this.e.equals(n)) {
            int k2 = O.k();
            if ((k2 & 64) != 0) {
                throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            } else if ((k2 & 128) == 0) {
                O.r0(this.i.getContext().getPackageName());
                O.A0(this.i, i2);
                if (this.k == i2) {
                    O.V(true);
                    O.a(128);
                } else {
                    O.V(false);
                    O.a(64);
                }
                boolean z = this.l == i2;
                if (z) {
                    O.a(2);
                } else if (O.G()) {
                    O.a(1);
                }
                O.m0(z);
                this.i.getLocationOnScreen(this.g);
                O.n(this.d);
                if (this.d.equals(n)) {
                    O.m(this.d);
                    if (O.b != -1) {
                        Yo0 O2 = Yo0.O();
                        for (int i3 = O.b; i3 != -1; i3 = O2.b) {
                            O2.u0(this.i, -1);
                            O2.X(n);
                            N(i3, O2);
                            O2.m(this.e);
                            Rect rect = this.d;
                            Rect rect2 = this.e;
                            rect.offset(rect2.left, rect2.top);
                        }
                        O2.S();
                    }
                    this.d.offset(this.g[0] - this.i.getScrollX(), this.g[1] - this.i.getScrollY());
                }
                if (this.i.getLocalVisibleRect(this.f)) {
                    this.f.offset(this.g[0] - this.i.getScrollX(), this.g[1] - this.i.getScrollY());
                    if (this.d.intersect(this.f)) {
                        O.Y(this.d);
                        if (E(this.d)) {
                            O.C0(true);
                        }
                    }
                }
                return O;
            } else {
                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
        } else {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
    }

    @DexIgnore
    public final Yo0 u() {
        Yo0 P = Yo0.P(this.i);
        Mo0.Y(this.i, P);
        ArrayList arrayList = new ArrayList();
        C(arrayList);
        if (P.o() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                P.d(this.i, ((Integer) arrayList.get(i2)).intValue());
            }
            return P;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    @DexIgnore
    public final boolean v(MotionEvent motionEvent) {
        if (!this.h.isEnabled() || !this.h.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 7 || action == 9) {
            int B = B(motionEvent.getX(), motionEvent.getY());
            V(B);
            return B != Integer.MIN_VALUE;
        } else if (action != 10 || this.m == Integer.MIN_VALUE) {
            return false;
        } else {
            V(RecyclerView.UNDEFINED_DURATION);
            return true;
        }
    }

    @DexIgnore
    public final boolean w(KeyEvent keyEvent) {
        int i2 = 0;
        if (keyEvent.getAction() == 1) {
            return false;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 61) {
            if (keyCode != 66) {
                switch (keyCode) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        if (!keyEvent.hasNoModifiers()) {
                            return false;
                        }
                        int F = F(keyCode);
                        int repeatCount = keyEvent.getRepeatCount();
                        boolean z = false;
                        while (i2 < repeatCount + 1 && G(F, null)) {
                            i2++;
                            z = true;
                        }
                        return z;
                    case 23:
                        break;
                    default:
                        return false;
                }
            }
            if (!keyEvent.hasNoModifiers() || keyEvent.getRepeatCount() != 0) {
                return false;
            }
            p();
            return true;
        } else if (keyEvent.hasNoModifiers()) {
            return G(2, null);
        } else {
            if (keyEvent.hasModifiers(1)) {
                return G(1, null);
            }
            return false;
        }
    }

    @DexIgnore
    public final int x() {
        return this.k;
    }

    @DexIgnore
    public final SparseArrayCompat<Yo0> y() {
        ArrayList arrayList = new ArrayList();
        C(arrayList);
        SparseArrayCompat<Yo0> sparseArrayCompat = new SparseArrayCompat<>();
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            sparseArrayCompat.q(i2, t(i2));
        }
        return sparseArrayCompat;
    }

    @DexIgnore
    public final void z(int i2, Rect rect) {
        H(i2).m(rect);
    }
}
