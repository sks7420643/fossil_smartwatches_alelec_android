package com.fossil;

import com.fossil.Ks7;
import com.mapped.Gg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ls7<R> extends Ks7<R>, Gg6<R> {

    @DexIgnore
    public interface Ai<R> extends Ks7.Ai<R>, Gg6<R> {
    }

    @DexIgnore
    Object getDelegate();

    @DexIgnore
    Ai<R> getGetter();
}
