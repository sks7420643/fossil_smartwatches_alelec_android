package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M51 {
    @DexIgnore
    public static /* final */ M51 a; // = new M51();

    @DexIgnore
    public final int a(int i) {
        if (i <= 4) {
            return 8;
        }
        return i * 2;
    }

    @DexIgnore
    public final int[] b(int[] iArr, int i, int i2, int i3) {
        Wg6.c(iArr, "array");
        if (i + 1 <= iArr.length) {
            System.arraycopy(iArr, i2, iArr, i2 + 1, i - i2);
            iArr[i2] = i3;
            return iArr;
        }
        int[] iArr2 = new int[a(i)];
        System.arraycopy(iArr, 0, iArr2, 0, i2);
        iArr2[i2] = i3;
        System.arraycopy(iArr, i2, iArr2, i2 + 1, iArr.length - i2);
        return iArr2;
    }
}
