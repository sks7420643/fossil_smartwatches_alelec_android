package com.fossil;

import com.mapped.Wg6;
import java.util.Arrays;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hx {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public byte[] b; // = new byte[8];
    @DexIgnore
    public byte[] c; // = new byte[8];
    @DexIgnore
    public /* final */ Hashtable<N6, Kx> d; // = new Hashtable<>();

    @DexIgnore
    public final Kx a(N6 n6) {
        Kx kx = this.d.get(n6);
        if (kx == null) {
            kx = new Kx(n6, this.b, this.c, 0, 0);
        }
        this.d.put(n6, kx);
        return kx;
    }

    @DexIgnore
    public final void b(byte[] bArr, byte[] bArr2) {
        if (bArr.length == 8 && bArr2.length == 8) {
            this.b = bArr;
            this.c = bArr2;
        }
    }

    @DexIgnore
    public final byte[] c() {
        byte[] bArr = this.a;
        if (bArr == null) {
            return null;
        }
        if (bArr.length <= 16) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, 16);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public final void d(N6 n6) {
        Kx a2 = a(n6);
        byte[] bArr = this.b;
        byte[] bArr2 = this.c;
        if (!Arrays.equals(a2.b, bArr) || !Arrays.equals(a2.c, bArr2)) {
            a2.d = 0;
        }
        a2.b = bArr;
        a2.c = bArr2;
        a2.d++;
        a2.e = 0;
    }
}
