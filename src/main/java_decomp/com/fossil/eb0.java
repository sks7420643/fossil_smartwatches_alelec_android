package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eb0 implements Parcelable.Creator<Fb0> {
    @DexIgnore
    public /* synthetic */ Eb0(Qg6 qg6) {
    }

    @DexIgnore
    public Fb0 a(Parcel parcel) {
        return new Fb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Fb0 createFromParcel(Parcel parcel) {
        return new Fb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Fb0[] newArray(int i) {
        return new Fb0[i];
    }
}
