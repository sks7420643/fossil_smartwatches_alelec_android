package com.fossil;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z13 extends Rz2<Long> implements J13, Y23, RandomAccess {
    @DexIgnore
    public static /* final */ Z13 e;
    @DexIgnore
    public long[] c;
    @DexIgnore
    public int d;

    /*
    static {
        Z13 z13 = new Z13(new long[0], 0);
        e = z13;
        z13.zzb();
    }
    */

    @DexIgnore
    public Z13() {
        this(new long[10], 0);
    }

    @DexIgnore
    public Z13(long[] jArr, int i) {
        this.c = jArr;
        this.d = i;
    }

    @DexIgnore
    public static Z13 c() {
        return e;
    }

    @DexIgnore
    @Override // com.fossil.J13
    public final J13 C(int i) {
        if (i >= this.d) {
            return new Z13(Arrays.copyOf(this.c, i), this.d);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        long longValue = ((Long) obj).longValue();
        a();
        if (i < 0 || i > (i2 = this.d)) {
            throw new IndexOutOfBoundsException(e(i));
        }
        long[] jArr = this.c;
        if (i2 < jArr.length) {
            System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
        } else {
            long[] jArr2 = new long[(((i2 * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.c, i, jArr2, i + 1, this.d - i);
            this.c = jArr2;
        }
        this.c[i] = longValue;
        this.d++;
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, com.fossil.Rz2
    public final /* synthetic */ boolean add(Long l) {
        b(l.longValue());
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.Rz2
    public final boolean addAll(Collection<? extends Long> collection) {
        a();
        H13.d(collection);
        if (!(collection instanceof Z13)) {
            return super.addAll(collection);
        }
        Z13 z13 = (Z13) collection;
        int i = z13.d;
        if (i == 0) {
            return false;
        }
        int i2 = this.d;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i + i2;
            long[] jArr = this.c;
            if (i3 > jArr.length) {
                this.c = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(z13.c, 0, this.c, this.d, z13.d);
            this.d = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final void b(long j) {
        a();
        int i = this.d;
        long[] jArr = this.c;
        if (i == jArr.length) {
            long[] jArr2 = new long[(((i * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.c = jArr2;
        }
        long[] jArr3 = this.c;
        int i2 = this.d;
        this.d = i2 + 1;
        jArr3[i2] = j;
    }

    @DexIgnore
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @DexIgnore
    public final void d(int i) {
        if (i < 0 || i >= this.d) {
            throw new IndexOutOfBoundsException(e(i));
        }
    }

    @DexIgnore
    public final String e(int i) {
        int i2 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.Rz2
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Z13)) {
            return super.equals(obj);
        }
        Z13 z13 = (Z13) obj;
        if (this.d != z13.d) {
            return false;
        }
        long[] jArr = z13.c;
        for (int i = 0; i < this.d; i++) {
            if (this.c[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(zzb(i));
    }

    @DexIgnore
    @Override // com.fossil.Rz2
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.d; i2++) {
            i = (i * 31) + H13.b(this.c[i2]);
        }
        return i;
    }

    @DexIgnore
    public final int indexOf(Object obj) {
        if (!(obj instanceof Long)) {
            return -1;
        }
        long longValue = ((Long) obj).longValue();
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.c[i] == longValue) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object remove(int i) {
        a();
        d(i);
        long[] jArr = this.c;
        long j = jArr[i];
        int i2 = this.d;
        if (i < i2 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.d--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.Rz2
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.d; i++) {
            if (obj.equals(Long.valueOf(this.c[i]))) {
                long[] jArr = this.c;
                System.arraycopy(jArr, i + 1, jArr, i, (this.d - i) - 1);
                this.d--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            long[] jArr = this.c;
            System.arraycopy(jArr, i2, jArr, i, this.d - i2);
            this.d -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        a();
        d(i);
        long[] jArr = this.c;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    @DexIgnore
    public final int size() {
        return this.d;
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.M13' to match base method */
    @Override // com.fossil.M13
    public final /* synthetic */ M13<Long> zza(int i) {
        return C(i);
    }

    @DexIgnore
    @Override // com.fossil.J13
    public final long zzb(int i) {
        d(i);
        return this.c[i];
    }
}
