package com.fossil;

import android.content.Intent;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt5 extends iq4<a, c, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ d e; // = new d();
    @DexIgnore
    public /* final */ List<QuickResponseMessage> f; // = new ArrayList();
    @DexIgnore
    public /* final */ QuickResponseRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<QuickResponseMessage> f2415a;

        @DexIgnore
        public a(List<QuickResponseMessage> list) {
            pq7.c(list, "quickResponseMessageList");
            this.f2415a = list;
        }

        @DexIgnore
        public final List<QuickResponseMessage> a() {
            return this.f2415a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
        @DexIgnore
        public b(int i, int i2, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "errorCodes");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase$SetReplyMessageReceiver$receive$1", f = "SetReplyMessageMappingUseCase.kt", l = {39}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mt5$d$a$a")
            @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase$SetReplyMessageReceiver$receive$1$1", f = "SetReplyMessageMappingUseCase.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.mt5$d$a$a  reason: collision with other inner class name */
            public static final class C0158a extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0158a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0158a aVar = new C0158a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
                    throw null;
                    //return ((C0158a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return mt5.this.j(new c());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    mt5.this.g.updateQR(mt5.this.f);
                    jx7 c = bw7.c();
                    C0158a aVar = new C0158a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(c, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetReplyMessageMappingUseCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_REPLY_MESSAGE_MAPPING && mt5.this.d) {
                mt5.this.d = false;
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                mt5.this.t();
                if (z) {
                    xw7 unused = gu7.d(mt5.this.g(), null, null, new a(this, null), 3, null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetReplyMessageMappingUseCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                mt5.this.i(new b(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore
    public mt5(QuickResponseRepository quickResponseRepository) {
        pq7.c(quickResponseRepository, "mQuickResponseRepository");
        this.g = quickResponseRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetReplyMessageMappingUseCase";
    }

    @DexIgnore
    public final void r() {
        wq5.d.e(this.e, CommunicateMode.SET_REPLY_MESSAGE_MAPPING);
    }

    @DexIgnore
    /* renamed from: s */
    public Object k(a aVar, qn7<Object> qn7) {
        this.d = true;
        r();
        if (aVar != null) {
            List<QuickResponseMessage> a2 = aVar.a();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList();
            for (T t : a2) {
                if (!ao7.a(t.getResponse().length() == 0).booleanValue()) {
                    arrayList.add(t);
                }
            }
            ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
            for (QuickResponseMessage quickResponseMessage : arrayList) {
                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
            }
            ao7.f(PortfolioApp.h0.c().G1(new ReplyMessageMappingGroup(pm7.j0(arrayList2), "icMessage.icon"), PortfolioApp.h0.c().J()));
        }
        return new Object();
    }

    @DexIgnore
    public final void t() {
        wq5.d.j(this.e, CommunicateMode.SET_REPLY_MESSAGE_MAPPING);
    }
}
