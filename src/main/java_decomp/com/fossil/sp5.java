package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.DatabaseHelper;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sp5 extends BaseDbProvider implements Rp5 {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Sp5(Context context, String str) {
        super(context, str);
        Wg6.c(context, "context");
        Wg6.c(str, "dbPath");
    }

    @DexIgnore
    @Override // com.fossil.Rp5
    public boolean a() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            Wg6.b(str, "TAG");
            local.d(str, "Inside .clearAllServerSettings");
            o().deleteBuilder().delete();
            return true;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            Wg6.b(str2, "TAG");
            local2.e(str2, "Error Inside e = " + e);
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rp5
    public boolean addOrUpdateServerSetting(ServerSetting serverSetting) {
        if (serverSetting == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            Wg6.b(str, "TAG");
            local.d(str, "serverSetting is null");
            return false;
        }
        try {
            Dao.CreateOrUpdateStatus createOrUpdate = o().createOrUpdate(serverSetting);
            Wg6.b(createOrUpdate, "status");
            return createOrUpdate.isCreated() || createOrUpdate.isUpdated();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            Wg6.b(str2, "TAG");
            local2.e(str2, "addOrUpdateServerSetting + e = " + e);
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{ServerSetting.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        DatabaseHelper databaseHelper = this.databaseHelper;
        Wg6.b(databaseHelper, "databaseHelper");
        String dbPath = databaseHelper.getDbPath();
        Wg6.b(dbPath, "databaseHelper.dbPath");
        return dbPath;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.Rp5
    public ServerSetting getServerSettingByKey(String str) {
        if (str == null) {
            return null;
        }
        try {
            return o().queryBuilder().where().eq("key", str).queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            Wg6.b(str2, "TAG");
            local.e(str2, "getServerSettingByKey + e = " + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rp5
    public void k(List<ServerSetting> list) {
        Boolean valueOf = list != null ? Boolean.valueOf(list.isEmpty()) : null;
        if (valueOf == null) {
            Wg6.i();
            throw null;
        } else if (valueOf.booleanValue()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            Wg6.b(str, "TAG");
            local.d(str, "serverSettingList is null or empty");
        } else {
            try {
                for (ServerSetting serverSetting : list) {
                    o().createOrUpdate(serverSetting);
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                Wg6.b(str2, "TAG");
                local2.e(str2, "addOrUpdateServerSettingList + e = " + e);
            }
        }
    }

    @DexIgnore
    public final Dao<ServerSetting, String> o() {
        Dao<ServerSetting, String> dao = this.databaseHelper.getDao(ServerSetting.class);
        Wg6.b(dao, "databaseHelper.getDao(ServerSetting::class.java)");
        return dao;
    }
}
