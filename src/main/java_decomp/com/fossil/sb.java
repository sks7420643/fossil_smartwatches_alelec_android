package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Sb extends Enum<Sb> {
    @DexIgnore
    public static /* final */ Sb c;
    @DexIgnore
    public static /* final */ /* synthetic */ Sb[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Sb sb = new Sb("FOSSIL", 0, (byte) 1);
        c = sb;
        d = new Sb[]{sb, new Sb("GOOGLE", 1, (byte) 2)};
    }
    */

    @DexIgnore
    public Sb(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Sb valueOf(String str) {
        return (Sb) Enum.valueOf(Sb.class, str);
    }

    @DexIgnore
    public static Sb[] values() {
        return (Sb[]) d.clone();
    }
}
