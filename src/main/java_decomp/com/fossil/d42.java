package com.fossil;

import android.os.Bundle;
import com.fossil.M62;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D42 {
    @DexIgnore
    public static /* final */ M62.Gi<Yk2> a; // = new M62.Gi<>();
    @DexIgnore
    public static /* final */ M62.Gi<U42> b; // = new M62.Gi<>();
    @DexIgnore
    public static /* final */ M62.Ai<Yk2, Ai> c; // = new S52();
    @DexIgnore
    public static /* final */ M62.Ai<U42, GoogleSignInOptions> d; // = new T52();
    @DexIgnore
    public static /* final */ M62<GoogleSignInOptions> e; // = new M62<>("Auth.GOOGLE_SIGN_IN_API", d, b);
    @DexIgnore
    public static /* final */ I42 f; // = new T42();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class Ai implements M62.Di.Cii, M62.Di {
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Deprecated
        public static class Aii {
            @DexIgnore
            public Boolean a; // = Boolean.FALSE;

            @DexIgnore
            public Ai a() {
                return new Ai(this);
            }
        }

        /*
        static {
            new Aii().a();
        }
        */

        @DexIgnore
        public Ai(Aii aii) {
            this.b = aii.a.booleanValue();
        }

        @DexIgnore
        public final Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", null);
            bundle.putBoolean("force_save_dialog", this.b);
            return bundle;
        }
    }

    /*
    static {
        M62<F42> m62 = E42.c;
        new M62("Auth.CREDENTIALS_API", c, a);
        G42 g42 = E42.d;
    }
    */
}
