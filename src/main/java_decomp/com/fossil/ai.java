package com.fossil;

import com.fossil.Ix1;
import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ai extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Oi b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ai(Oi oi) {
        super(1);
        this.b = oi;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        Oi oi = this.b;
        Lv lv = (Lv) fs;
        long j = lv.M;
        long j2 = lv.N;
        long j3 = lv.O;
        if (0 == j2) {
            oi.D = oi.E;
            oi.l(Nr.a(oi.v, null, Zq.b, null, null, 13));
        } else if (j != oi.E) {
            oi.l(Nr.a(oi.v, null, Zq.i, null, null, 13));
        } else {
            oi.F = j + j2;
            long a2 = Ix1.a.a(oi.H.f, (int) j, (int) j2, Ix1.Ai.CRC32);
            Ky1 ky1 = Ky1.DEBUG;
            if (j3 == a2) {
                long j4 = oi.F;
                long j5 = oi.G;
                if (j4 == j5) {
                    oi.D = j4;
                    oi.l(Nr.a(oi.v, null, Zq.b, null, null, 13));
                } else {
                    oi.F = Math.min(((j4 - oi.E) / ((long) 2)) + j4, j5);
                    oi.E = j4;
                    Lp.j(oi, Hs.p, null, 2, null);
                }
            } else {
                oi.F = (oi.F + oi.E) / ((long) 2);
                Lp.j(oi, Hs.p, null, 2, null);
            }
        }
        return Cd6.a;
    }
}
