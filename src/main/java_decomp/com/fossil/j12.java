package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J12 implements Factory<I12> {
    @DexIgnore
    public /* final */ Provider<Executor> a;
    @DexIgnore
    public /* final */ Provider<T02> b;
    @DexIgnore
    public /* final */ Provider<H22> c;
    @DexIgnore
    public /* final */ Provider<K22> d;
    @DexIgnore
    public /* final */ Provider<S32> e;

    @DexIgnore
    public J12(Provider<Executor> provider, Provider<T02> provider2, Provider<H22> provider3, Provider<K22> provider4, Provider<S32> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static J12 a(Provider<Executor> provider, Provider<T02> provider2, Provider<H22> provider3, Provider<K22> provider4, Provider<S32> provider5) {
        return new J12(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public I12 b() {
        return new I12(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
