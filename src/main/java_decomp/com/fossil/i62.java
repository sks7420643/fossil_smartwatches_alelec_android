package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I62 {
    @DexIgnore
    public static I62 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public I62(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static I62 a(Context context) {
        Rc2.k(context);
        synchronized (I62.class) {
            try {
                if (b == null) {
                    Dg2.c(context);
                    b = new I62(context);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return b;
    }

    @DexIgnore
    public static Eg2 d(PackageInfo packageInfo, Eg2... eg2Arr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        Hg2 hg2 = new Hg2(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < eg2Arr.length; i++) {
            if (eg2Arr[i].equals(hg2)) {
                return eg2Arr[i];
            }
        }
        return null;
    }

    @DexIgnore
    public static boolean f(PackageInfo packageInfo, boolean z) {
        Eg2 d;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                d = d(packageInfo, Jg2.a);
            } else {
                d = d(packageInfo, Jg2.a[0]);
            }
            if (d != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean b(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (f(packageInfo, false)) {
            return true;
        }
        if (!f(packageInfo, true)) {
            return false;
        }
        if (H62.f(this.a)) {
            return true;
        }
        Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        return false;
    }

    @DexIgnore
    public boolean c(int i) {
        Mg2 mg2;
        String[] f = Ag2.a(this.a).f(i);
        if (f == null || f.length == 0) {
            mg2 = Mg2.d("no pkgs");
        } else {
            mg2 = null;
            for (String str : f) {
                mg2 = e(str, i);
                if (mg2.a) {
                    break;
                }
            }
        }
        mg2.g();
        return mg2.a;
    }

    @DexIgnore
    public final Mg2 e(String str, int i) {
        try {
            PackageInfo h = Ag2.a(this.a).h(str, 64, i);
            boolean f = H62.f(this.a);
            if (h == null) {
                return Mg2.d("null pkg");
            }
            if (h.signatures == null || h.signatures.length != 1) {
                return Mg2.d("single cert required");
            }
            Hg2 hg2 = new Hg2(h.signatures[0].toByteArray());
            String str2 = h.packageName;
            Mg2 a2 = Dg2.a(str2, hg2, f, false);
            return (!a2.a || h.applicationInfo == null || (h.applicationInfo.flags & 2) == 0 || !Dg2.a(str2, hg2, false, true).a) ? a2 : Mg2.d("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(str);
            return Mg2.d(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }
}
