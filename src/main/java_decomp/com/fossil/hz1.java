package com.fossil;

import com.fossil.Nz1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hz1 extends Nz1 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Lz1 c;
    @DexIgnore
    public /* final */ Integer d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ List<Mz1> f;
    @DexIgnore
    public /* final */ Qz1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Nz1.Ai {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Lz1 c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public String e;
        @DexIgnore
        public List<Mz1> f;
        @DexIgnore
        public Qz1 g;

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai c(Lz1 lz1) {
            this.c = lz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai d(Qz1 qz1) {
            this.g = qz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai e(Integer num) {
            this.d = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai f(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai g(List<Mz1> list) {
            this.f = list;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1 h() {
            String str = "";
            if (this.a == null) {
                str = " requestTimeMs";
            }
            if (this.b == null) {
                str = str + " requestUptimeMs";
            }
            if (str.isEmpty()) {
                return new Hz1(this.a.longValue(), this.b.longValue(), this.c, this.d, this.e, this.f, this.g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Nz1.Ai
        public Nz1.Ai i(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public /* synthetic */ Hz1(long j, long j2, Lz1 lz1, Integer num, String str, List list, Qz1 qz1, Ai ai) {
        this.a = j;
        this.b = j2;
        this.c = lz1;
        this.d = num;
        this.e = str;
        this.f = list;
        this.g = qz1;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public Lz1 b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public List<Mz1> c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public Integer d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public String e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        Lz1 lz1;
        Integer num;
        String str;
        List<Mz1> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Nz1)) {
            return false;
        }
        Nz1 nz1 = (Nz1) obj;
        if (this.a == nz1.g() && this.b == nz1.h() && ((lz1 = this.c) != null ? lz1.equals(((Hz1) nz1).c) : ((Hz1) nz1).c == null) && ((num = this.d) != null ? num.equals(((Hz1) nz1).d) : ((Hz1) nz1).d == null) && ((str = this.e) != null ? str.equals(((Hz1) nz1).e) : ((Hz1) nz1).e == null) && ((list = this.f) != null ? list.equals(((Hz1) nz1).f) : ((Hz1) nz1).f == null)) {
            Qz1 qz1 = this.g;
            if (qz1 == null) {
                if (((Hz1) nz1).g == null) {
                    z = true;
                    return z;
                }
            } else if (qz1.equals(((Hz1) nz1).g)) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public Qz1 f() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public long g() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Nz1
    public long h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        long j = this.a;
        int i2 = (int) (j ^ (j >>> 32));
        long j2 = this.b;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        Lz1 lz1 = this.c;
        int hashCode = lz1 == null ? 0 : lz1.hashCode();
        Integer num = this.d;
        int hashCode2 = num == null ? 0 : num.hashCode();
        String str = this.e;
        int hashCode3 = str == null ? 0 : str.hashCode();
        List<Mz1> list = this.f;
        int hashCode4 = list == null ? 0 : list.hashCode();
        Qz1 qz1 = this.g;
        if (qz1 != null) {
            i = qz1.hashCode();
        }
        return ((((((((hashCode ^ ((((i2 ^ 1000003) * 1000003) ^ i3) * 1000003)) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "LogRequest{requestTimeMs=" + this.a + ", requestUptimeMs=" + this.b + ", clientInfo=" + this.c + ", logSource=" + this.d + ", logSourceName=" + this.e + ", logEvents=" + this.f + ", qosTier=" + this.g + "}";
    }
}
