package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qd5 extends Pd5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362665, 1);
        x.put(2131363299, 2);
        x.put(2131363260, 3);
        x.put(2131362783, 4);
    }
    */

    @DexIgnore
    public Qd5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 5, w, x));
    }

    @DexIgnore
    public Qd5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[0], (ImageView) objArr[1], (View) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[2]);
        this.v = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.v != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.v = 1;
        }
        w();
    }
}
