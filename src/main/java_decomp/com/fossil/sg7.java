package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sg7 extends Ng7 {
    @DexIgnore
    public Vh7 m;
    @DexIgnore
    public JSONObject n; // = null;

    @DexIgnore
    public Sg7(Context context, int i, JSONObject jSONObject, Jg7 jg7) {
        super(context, i, jg7);
        this.m = new Vh7(context);
        this.n = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public Og7 a() {
        return Og7.b;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public boolean b(JSONObject jSONObject) {
        Uh7 uh7 = this.d;
        if (uh7 != null) {
            jSONObject.put("ut", uh7.e());
        }
        JSONObject jSONObject2 = this.n;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (Ei7.U(this.j)) {
            jSONObject.put("ncts", 1);
        }
        this.m.b(jSONObject, null);
        return true;
    }
}
