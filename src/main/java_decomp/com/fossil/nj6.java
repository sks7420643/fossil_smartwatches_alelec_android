package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj6 extends kj6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<SleepSummary> f;
    @DexIgnore
    public /* final */ lj6 g;
    @DexIgnore
    public /* final */ SleepSummariesRepository h;
    @DexIgnore
    public /* final */ SleepSessionsRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ no4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1", f = "DashboardSleepPresenter.kt", l = {65, 73}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nj6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nj6$a$a")
        /* renamed from: com.fossil.nj6$a$a  reason: collision with other inner class name */
        public static final class C0169a implements fl5.a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f2534a;

            @DexIgnore
            public C0169a(a aVar) {
                this.f2534a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.fl5.a
            public final void e(fl5.g gVar) {
                pq7.c(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardSleepPresenter", "onStatusChange status=" + gVar);
                if (gVar.b()) {
                    this.f2534a.this$0.y().d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<cu0<SleepSummary>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f2535a;

            @DexIgnore
            public b(a aVar) {
                this.f2535a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<SleepSummary> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
                local.d("DashboardSleepPresenter", sb.toString());
                if (cu0 != null) {
                    this.f2535a.this$0.y().q(cu0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1", f = "DashboardSleepPresenter.kt", l = {65}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.k;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(nj6 nj6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = nj6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 2
                r4 = 1
                java.lang.Object r9 = com.fossil.yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x00a5
                if (r0 == r4) goto L_0x0058
                if (r0 != r10) goto L_0x0050
                java.lang.Object r0 = r11.L$4
                com.fossil.nj6 r0 = (com.fossil.nj6) r0
                java.lang.Object r1 = r11.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r11.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r11.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r11.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r12)
                r1 = r12
                r2 = r0
            L_0x0027:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.fossil.nj6.x(r2, r0)
                com.fossil.nj6 r0 = r11.this$0
                com.fossil.lj6 r0 = r0.y()
                com.fossil.nj6 r1 = r11.this$0
                com.portfolio.platform.data.Listing r1 = com.fossil.nj6.u(r1)
                if (r1 == 0) goto L_0x004d
                androidx.lifecycle.LiveData r1 = r1.getPagedList()
                if (r1 == 0) goto L_0x004d
                if (r0 == 0) goto L_0x00c5
                com.fossil.mj6 r0 = (com.fossil.mj6) r0
                com.fossil.nj6$a$b r2 = new com.fossil.nj6$a$b
                r2.<init>(r11)
                r1.h(r0, r2)
            L_0x004d:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x004f:
                return r0
            L_0x0050:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0058:
                java.lang.Object r0 = r11.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r12)
                r7 = r0
                r1 = r12
            L_0x0061:
                r6 = r1
                com.portfolio.platform.data.model.MFUser r6 = (com.portfolio.platform.data.model.MFUser) r6
                if (r6 == 0) goto L_0x004d
                java.lang.String r0 = r6.getCreatedAt()
                java.util.Date r3 = com.fossil.lk5.q0(r0)
                com.fossil.nj6 r8 = r11.this$0
                com.portfolio.platform.data.source.SleepSummariesRepository r0 = com.fossil.nj6.v(r8)
                com.fossil.nj6 r1 = r11.this$0
                com.portfolio.platform.data.source.SleepSessionsRepository r1 = com.fossil.nj6.t(r1)
                com.fossil.nj6 r2 = r11.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r2 = com.fossil.nj6.s(r2)
                java.lang.String r4 = "createdDate"
                com.fossil.pq7.b(r3, r4)
                com.fossil.nj6 r4 = r11.this$0
                com.fossil.no4 r4 = com.fossil.nj6.r(r4)
                com.fossil.nj6$a$a r5 = new com.fossil.nj6$a$a
                r5.<init>(r11)
                r11.L$0 = r7
                r11.L$1 = r6
                r11.L$2 = r6
                r11.L$3 = r3
                r11.L$4 = r8
                r11.label = r10
                r6 = r11
                java.lang.Object r1 = r0.getSummariesPaging(r1, r2, r3, r4, r5, r6)
                if (r1 != r9) goto L_0x00c2
                r0 = r9
                goto L_0x004f
            L_0x00a5:
                com.fossil.el7.b(r12)
                com.fossil.iv7 r0 = r11.p$
                com.fossil.nj6 r1 = r11.this$0
                com.fossil.dv7 r1 = com.fossil.nj6.q(r1)
                com.fossil.nj6$a$c r2 = new com.fossil.nj6$a$c
                r3 = 0
                r2.<init>(r11, r3)
                r11.L$0 = r0
                r11.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r11)
                if (r1 != r9) goto L_0x00cd
                r0 = r9
                goto L_0x004f
            L_0x00c2:
                r2 = r8
                goto L_0x0027
            L_0x00c5:
                com.fossil.il7 r0 = new com.fossil.il7
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment"
                r0.<init>(r1)
                throw r0
            L_0x00cd:
                r7 = r0
                goto L_0x0061
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.nj6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public nj6(lj6 lj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, no4 no4) {
        pq7.c(lj6, "mView");
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(sleepSessionsRepository, "mSleepSessionsRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(no4, "mAppExecutors");
        this.g = lj6;
        this.h = sleepSummariesRepository;
        this.i = sleepSessionsRepository;
        this.j = fitnessDataRepository;
        this.k = userRepository;
        this.l = no4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.h0.c().J());
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        Boolean p0 = lk5.p0(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepPresenter", "start isDateTodayDate " + p0 + " listingPage " + this.f);
        if (!p0.booleanValue()) {
            this.e = new Date();
            Listing<SleepSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void o() {
        LiveData<cu0<SleepSummary>> pagedList;
        try {
            lj6 lj6 = this.g;
            Listing<SleepSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (lj6 != null) {
                    pagedList.n((mj6) lj6);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("DashboardSleepPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void p() {
        gp7<tl7> retry;
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "retry all failed request");
        Listing<SleepSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public final lj6 y() {
        return this.g;
    }

    @DexIgnore
    public void z() {
        this.g.M5(this);
    }
}
