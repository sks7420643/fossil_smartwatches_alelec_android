package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Lk6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qz7 extends Jx7 implements Tv7 {
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public Qz7(Throwable th, String str) {
        this.c = th;
        this.d = str;
    }

    @DexIgnore
    @Override // com.fossil.Tv7
    public Dw7 G(long j, Runnable runnable) {
        V();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public /* bridge */ /* synthetic */ void M(Af6 af6, Runnable runnable) {
        T(af6, runnable);
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public boolean Q(Af6 af6) {
        V();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jx7
    public Jx7 S() {
        return this;
    }

    @DexIgnore
    public Void T(Af6 af6, Runnable runnable) {
        V();
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r0 != null) goto L_0x0025;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Void V() {
        /*
            r4 = this;
            java.lang.Throwable r0 = r4.c
            if (r0 == 0) goto L_0x0037
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = "Module with the Main dispatcher had failed to initialize"
            r1.append(r0)
            java.lang.String r0 = r4.d
            if (r0 == 0) goto L_0x0034
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ". "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            if (r0 == 0) goto L_0x0034
        L_0x0025:
            r1.append(r0)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            java.lang.Throwable r2 = r4.c
            r0.<init>(r1, r2)
            throw r0
        L_0x0034:
            java.lang.String r0 = ""
            goto L_0x0025
        L_0x0037:
            com.fossil.Pz7.c()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Qz7.V():java.lang.Void");
    }

    @DexIgnore
    public Void X(long j, Lk6<? super Cd6> lk6) {
        V();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Tv7
    public /* bridge */ /* synthetic */ void f(long j, Lk6 lk6) {
        X(j, lk6);
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Main[missing");
        if (this.c != null) {
            str = ", cause=" + this.c;
        } else {
            str = "";
        }
        sb.append(str);
        sb.append(']');
        return sb.toString();
    }
}
