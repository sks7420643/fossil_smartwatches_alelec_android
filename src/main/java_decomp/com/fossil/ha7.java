package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ha7 implements Factory<Ga7> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Ha7 a; // = new Ha7();
    }

    @DexIgnore
    public static Ha7 a() {
        return Ai.a;
    }

    @DexIgnore
    public static Ga7 c() {
        return new Ga7();
    }

    @DexIgnore
    public Ga7 b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
