package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Go4 {
    @DexIgnore
    public Zn4 a;
    @DexIgnore
    public Yn4 b;
    @DexIgnore
    public Ao4 c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public Co4 e;

    @DexIgnore
    public static boolean b(int i) {
        return i >= 0 && i < 8;
    }

    @DexIgnore
    public Co4 a() {
        return this.e;
    }

    @DexIgnore
    public void c(Yn4 yn4) {
        this.b = yn4;
    }

    @DexIgnore
    public void d(int i) {
        this.d = i;
    }

    @DexIgnore
    public void e(Co4 co4) {
        this.e = co4;
    }

    @DexIgnore
    public void f(Zn4 zn4) {
        this.a = zn4;
    }

    @DexIgnore
    public void g(Ao4 ao4) {
        this.c = ao4;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.a);
        sb.append("\n ecLevel: ");
        sb.append(this.b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }
}
