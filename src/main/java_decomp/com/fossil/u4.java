package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ D5 b;
    @DexIgnore
    public /* final */ /* synthetic */ K5 c;
    @DexIgnore
    public /* final */ /* synthetic */ B5 d;
    @DexIgnore
    public /* final */ /* synthetic */ B5 e;

    @DexIgnore
    public U4(D5 d5, K5 k5, B5 b5, B5 b52) {
        this.b = d5;
        this.c = k5;
        this.d = b5;
        this.e = b52;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d, this.e);
    }
}
