package com.fossil;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iz2 extends WeakReference<Throwable> {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public Iz2(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == Iz2.class) {
            if (this == obj) {
                return true;
            }
            Iz2 iz2 = (Iz2) obj;
            return this.a == iz2.a && get() == iz2.get();
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.a;
    }
}
