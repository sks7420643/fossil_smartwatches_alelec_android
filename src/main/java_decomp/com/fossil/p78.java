package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class P78 implements E78 {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public volatile E78 c;
    @DexIgnore
    public Boolean d;
    @DexIgnore
    public Method e;
    @DexIgnore
    public H78 f;
    @DexIgnore
    public Queue<K78> g;
    @DexIgnore
    public /* final */ boolean h;

    @DexIgnore
    public P78(String str, Queue<K78> queue, boolean z) {
        this.b = str;
        this.g = queue;
        this.h = z;
    }

    @DexIgnore
    public E78 a() {
        return this.c != null ? this.c : this.h ? M78.NOP_LOGGER : b();
    }

    @DexIgnore
    public final E78 b() {
        if (this.f == null) {
            this.f = new H78(this, this.g);
        }
        return this.f;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean d() {
        Boolean bool = this.d;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            this.e = this.c.getClass().getMethod("log", J78.class);
            this.d = Boolean.TRUE;
        } catch (NoSuchMethodException e2) {
            this.d = Boolean.FALSE;
        }
        return this.d.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void debug(String str) {
        a().debug(str);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void debug(String str, Throwable th) {
        a().debug(str, th);
    }

    @DexIgnore
    public boolean e() {
        return this.c instanceof M78;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || P78.class != obj.getClass()) {
            return false;
        }
        return this.b.equals(((P78) obj).b);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void error(String str) {
        a().error(str);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void error(String str, Throwable th) {
        a().error(str, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void error(String str, Object... objArr) {
        a().error(str, objArr);
    }

    @DexIgnore
    public boolean f() {
        return this.c == null;
    }

    @DexIgnore
    public void g(J78 j78) {
        if (d()) {
            try {
                this.e.invoke(this.c, j78);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
            }
        }
    }

    @DexIgnore
    public void h(E78 e78) {
        this.c = e78;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void info(String str) {
        a().info(str);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void info(String str, Object obj) {
        a().info(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void info(String str, Throwable th) {
        a().info(str, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isDebugEnabled() {
        return a().isDebugEnabled();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isErrorEnabled() {
        return a().isErrorEnabled();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isInfoEnabled() {
        return a().isInfoEnabled();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isTraceEnabled() {
        return a().isTraceEnabled();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isWarnEnabled() {
        return a().isWarnEnabled();
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void trace(String str) {
        a().trace(str);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void trace(String str, Throwable th) {
        a().trace(str, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void warn(String str) {
        a().warn(str);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void warn(String str, Object obj, Object obj2) {
        a().warn(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void warn(String str, Throwable th) {
        a().warn(str, th);
    }
}
