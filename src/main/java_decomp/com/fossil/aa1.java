package com.fossil;

import com.facebook.share.internal.VideoUploader;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class Aa1 implements Da1 {
    @DexIgnore
    public /* final */ HttpClient a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends HttpEntityEnclosingRequestBase {
        @DexIgnore
        public Ai(String str) {
            setURI(URI.create(str));
        }

        @DexIgnore
        public String getMethod() {
            return "PATCH";
        }
    }

    @DexIgnore
    public Aa1(HttpClient httpClient) {
        this.a = httpClient;
    }

    @DexIgnore
    public static HttpUriRequest b(M91<?> m91, Map<String, String> map) throws Z81 {
        switch (m91.getMethod()) {
            case -1:
                byte[] postBody = m91.getPostBody();
                if (postBody == null) {
                    return new HttpGet(m91.getUrl());
                }
                HttpPost httpPost = new HttpPost(m91.getUrl());
                httpPost.addHeader("Content-Type", m91.getPostBodyContentType());
                httpPost.setEntity(new ByteArrayEntity(postBody));
                return httpPost;
            case 0:
                return new HttpGet(m91.getUrl());
            case 1:
                HttpPost httpPost2 = new HttpPost(m91.getUrl());
                httpPost2.addHeader("Content-Type", m91.getBodyContentType());
                d(httpPost2, m91);
                return httpPost2;
            case 2:
                HttpPut httpPut = new HttpPut(m91.getUrl());
                httpPut.addHeader("Content-Type", m91.getBodyContentType());
                d(httpPut, m91);
                return httpPut;
            case 3:
                return new HttpDelete(m91.getUrl());
            case 4:
                return new HttpHead(m91.getUrl());
            case 5:
                return new HttpOptions(m91.getUrl());
            case 6:
                return new HttpTrace(m91.getUrl());
            case 7:
                Ai ai = new Ai(m91.getUrl());
                ai.addHeader("Content-Type", m91.getBodyContentType());
                d(ai, m91);
                return ai;
            default:
                throw new IllegalStateException("Unknown request method.");
        }
    }

    @DexIgnore
    public static void d(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, M91<?> m91) throws Z81 {
        byte[] body = m91.getBody();
        if (body != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(body));
        }
    }

    @DexIgnore
    public static void e(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String str : map.keySet()) {
            httpUriRequest.setHeader(str, map.get(str));
        }
    }

    @DexIgnore
    @Override // com.fossil.Da1
    public HttpResponse a(M91<?> m91, Map<String, String> map) throws IOException, Z81 {
        HttpUriRequest b = b(m91, map);
        e(b, map);
        e(b, m91.getHeaders());
        c(b);
        HttpParams params = b.getParams();
        int timeoutMs = m91.getTimeoutMs();
        HttpConnectionParams.setConnectionTimeout(params, VideoUploader.RETRY_DELAY_UNIT_MS);
        HttpConnectionParams.setSoTimeout(params, timeoutMs);
        return this.a.execute(b);
    }

    @DexIgnore
    public void c(HttpUriRequest httpUriRequest) throws IOException {
    }
}
