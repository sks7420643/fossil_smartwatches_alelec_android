package com.fossil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ph7 extends SQLiteOpenHelper {
    @DexIgnore
    public String b; // = "";

    @DexIgnore
    public Ph7(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 3);
        this.b = str;
        context.getApplicationContext();
        if (Fg7.K()) {
            Th7 th7 = Gh7.l;
            th7.h("SQLiteOpenHelper " + this.b);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r9 = this;
            r8 = 0
            java.lang.String r1 = "user"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r10
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0048 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x005e }
            r0.<init>()     // Catch:{ all -> 0x005e }
            boolean r2 = r1.moveToNext()     // Catch:{ all -> 0x005e }
            if (r2 == 0) goto L_0x0033
            r2 = 0
            java.lang.String r8 = r1.getString(r2)     // Catch:{ all -> 0x005e }
            r2 = 1
            r1.getInt(r2)     // Catch:{ all -> 0x005e }
            r2 = 2
            r1.getString(r2)     // Catch:{ all -> 0x005e }
            r2 = 3
            r1.getLong(r2)     // Catch:{ all -> 0x005e }
            java.lang.String r2 = "uid"
            java.lang.String r3 = com.fossil.Ji7.g(r8)     // Catch:{ all -> 0x005e }
            r0.put(r2, r3)     // Catch:{ all -> 0x005e }
        L_0x0033:
            if (r8 == 0) goto L_0x0042
            java.lang.String r2 = "user"
            java.lang.String r3 = "uid=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x005e }
            r5 = 0
            r4[r5] = r8     // Catch:{ all -> 0x005e }
            r10.update(r2, r0, r3, r4)     // Catch:{ all -> 0x005e }
        L_0x0042:
            if (r1 == 0) goto L_0x0047
            r1.close()
        L_0x0047:
            return
        L_0x0048:
            r0 = move-exception
            r1 = r8
        L_0x004a:
            com.fossil.Th7 r2 = com.fossil.Gh7.E()     // Catch:{ all -> 0x0057 }
            r2.e(r0)     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0047
            r1.close()
            goto L_0x0047
        L_0x0057:
            r0 = move-exception
            if (r1 == 0) goto L_0x005d
            r1.close()
        L_0x005d:
            throw r0
        L_0x005e:
            r0 = move-exception
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ph7.a(android.database.sqlite.SQLiteDatabase):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(android.database.sqlite.SQLiteDatabase r11) {
        /*
            r10 = this;
            r8 = 0
            java.lang.String r1 = "events"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r11
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0086 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0036 }
            r0.<init>()     // Catch:{ all -> 0x0036 }
        L_0x0013:
            boolean r1 = r7.moveToNext()     // Catch:{ all -> 0x0036 }
            if (r1 == 0) goto L_0x0045
            r1 = 0
            long r2 = r7.getLong(r1)     // Catch:{ all -> 0x0036 }
            r1 = 1
            java.lang.String r4 = r7.getString(r1)     // Catch:{ all -> 0x0036 }
            r1 = 2
            int r5 = r7.getInt(r1)     // Catch:{ all -> 0x0036 }
            r1 = 3
            int r6 = r7.getInt(r1)     // Catch:{ all -> 0x0036 }
            com.fossil.Qh7 r1 = new com.fossil.Qh7     // Catch:{ all -> 0x0036 }
            r1.<init>(r2, r4, r5, r6)     // Catch:{ all -> 0x0036 }
            r0.add(r1)     // Catch:{ all -> 0x0036 }
            goto L_0x0013
        L_0x0036:
            r0 = move-exception
            r1 = r7
        L_0x0038:
            com.fossil.Th7 r2 = com.fossil.Gh7.E()     // Catch:{ all -> 0x007f }
            r2.e(r0)     // Catch:{ all -> 0x007f }
            if (r1 == 0) goto L_0x0044
            r1.close()
        L_0x0044:
            return
        L_0x0045:
            android.content.ContentValues r1 = new android.content.ContentValues
            r1.<init>()
            java.util.Iterator r2 = r0.iterator()
        L_0x004e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r0 = r2.next()
            com.fossil.Qh7 r0 = (com.fossil.Qh7) r0
            java.lang.String r3 = "content"
            java.lang.String r4 = r0.b
            java.lang.String r4 = com.fossil.Ji7.g(r4)
            r1.put(r3, r4)
            java.lang.String r3 = "events"
            java.lang.String r4 = "event_id=?"
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]
            r6 = 0
            long r8 = r0.a
            java.lang.String r0 = java.lang.Long.toString(r8)
            r5[r6] = r0
            r11.update(r3, r1, r4, r5)
            goto L_0x004e
        L_0x0079:
            if (r7 == 0) goto L_0x0044
            r7.close()
            goto L_0x0044
        L_0x007f:
            r0 = move-exception
            if (r1 == 0) goto L_0x0085
            r1.close()
        L_0x0085:
            throw r0
        L_0x0086:
            r0 = move-exception
            r1 = r8
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ph7.b(android.database.sqlite.SQLiteDatabase):void");
    }

    @DexIgnore
    @Override // java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            super.close();
        }
    }

    @DexIgnore
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table if not exists events(event_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, content TEXT, status INTEGER, send_count INTEGER, timestamp LONG)");
        sQLiteDatabase.execSQL("create table if not exists user(uid TEXT PRIMARY KEY, user_type INTEGER, app_ver TEXT, ts INTEGER)");
        sQLiteDatabase.execSQL("create table if not exists config(type INTEGER PRIMARY KEY NOT NULL, content TEXT, md5sum TEXT, version INTEGER)");
        sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
        sQLiteDatabase.execSQL("CREATE INDEX if not exists status_idx ON events(status)");
    }

    @DexIgnore
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Th7 th7 = Gh7.l;
        th7.c("upgrade DB from oldVersion " + i + " to newVersion " + i2);
        if (i == 1) {
            sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
        if (i == 2) {
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
    }
}
