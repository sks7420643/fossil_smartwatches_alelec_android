package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class F74 implements Mg4 {
    @DexIgnore
    public /* final */ I74 a;
    @DexIgnore
    public /* final */ A74 b;

    @DexIgnore
    public F74(I74 i74, A74 a74) {
        this.a = i74;
        this.b = a74;
    }

    @DexIgnore
    public static Mg4 a(I74 i74, A74 a74) {
        return new F74(i74, a74);
    }

    @DexIgnore
    @Override // com.fossil.Mg4
    public Object get() {
        A74 a74;
        return a74.d().a(new S74(this.b, this.a));
    }
}
