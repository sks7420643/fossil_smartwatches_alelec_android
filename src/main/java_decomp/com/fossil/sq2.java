package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sq2 extends Gr2 implements Rq2 {
    @DexIgnore
    public Sq2() {
        super("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    @Override // com.fossil.Gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        M((Pq2) Qr2.a(parcel, Pq2.CREATOR));
        return true;
    }
}
