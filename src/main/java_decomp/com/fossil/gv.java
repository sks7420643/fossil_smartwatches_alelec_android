package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gv extends Fv {
    @DexIgnore
    public byte[] X;
    @DexIgnore
    public long Y;
    @DexIgnore
    public long Z;
    @DexIgnore
    public /* final */ long a0;
    @DexIgnore
    public /* final */ long b0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Gv(short s, long j, long j2, K5 k5, int i, int i2) {
        super(Iu.c, s, Hs.w, k5, (i2 & 16) != 0 ? 3 : i);
        this.a0 = j;
        this.b0 = j2;
        this.X = new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a0).putInt((int) this.b0).array();
        Wg6.b(array, "ByteBuffer.allocate(8).o\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public void Q(long j) {
        this.Y = j;
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public void R(byte[] bArr) {
        this.X = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public void S(long j) {
        this.Z = j;
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public long T() {
        return this.Y;
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public long U() {
        return this.Z;
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public byte[] V() {
        return this.X;
    }

    @DexIgnore
    @Override // com.fossil.Dv, com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.c1, Long.valueOf(this.a0)), Jd0.d1, Long.valueOf(this.b0));
    }
}
