package com.fossil;

import com.mapped.Af6;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K08 extends Mw7 implements O08, Executor {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater g; // = AtomicIntegerFieldUpdater.newUpdater(K08.class, "inFlightTasks");
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<Runnable> c; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ I08 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public volatile int inFlightTasks; // = 0;

    @DexIgnore
    public K08(I08 i08, int i, int i2) {
        this.d = i08;
        this.e = i;
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public void M(Af6 af6, Runnable runnable) {
        T(runnable, false);
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public void P(Af6 af6, Runnable runnable) {
        T(runnable, true);
    }

    @DexIgnore
    @Override // com.fossil.Mw7
    public Executor S() {
        return this;
    }

    @DexIgnore
    public final void T(Runnable runnable, boolean z) {
        Runnable poll;
        while (g.incrementAndGet(this) > this.e) {
            this.c.add(runnable);
            if (g.decrementAndGet(this) < this.e && (poll = this.c.poll()) != null) {
                runnable = poll;
            } else {
                return;
            }
        }
        this.d.X(runnable, this, z);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        T(runnable, false);
    }

    @DexIgnore
    @Override // com.fossil.O08
    public void h() {
        Runnable poll = this.c.poll();
        if (poll != null) {
            this.d.X(poll, this, true);
            return;
        }
        g.decrementAndGet(this);
        Runnable poll2 = this.c.poll();
        if (poll2 != null) {
            T(poll2, true);
        }
    }

    @DexIgnore
    @Override // com.fossil.O08
    public int o() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        return super.toString() + "[dispatcher = " + this.d + ']';
    }
}
