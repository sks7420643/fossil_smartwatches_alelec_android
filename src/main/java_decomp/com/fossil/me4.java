package com.fossil;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Me4 implements Callable {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public Me4(Context context, Intent intent) {
        this.a = context;
        this.b = intent;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Integer.valueOf(Cg4.b().g(this.a, this.b));
    }
}
