package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mr4 implements Lr4 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<Rr4> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<Rr4> {
        @DexIgnore
        public Ai(Mr4 mr4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, Rr4 rr4) {
            if (rr4.a() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, rr4.a());
            }
            if (rr4.b() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, rr4.b());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Rr4 rr4) {
            a(mi, rr4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `flag` (`id`,`variantKey`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(Mr4 mr4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM flag";
        }
    }

    @DexIgnore
    public Mr4(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
        new Bi(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.Lr4
    public Rr4 a(String str) {
        Rr4 rr4 = null;
        Rh f = Rh.f("SELECT * FROM flag WHERE id = ? LIMIT 1", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c = Dx0.c(b2, "id");
            int c2 = Dx0.c(b2, "variantKey");
            if (b2.moveToFirst()) {
                rr4 = new Rr4(b2.getString(c), b2.getString(c2));
            }
            return rr4;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lr4
    public Long[] insert(List<Rr4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }
}
