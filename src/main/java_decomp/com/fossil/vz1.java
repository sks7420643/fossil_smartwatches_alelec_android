package com.fossil;

import com.fossil.C02;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vz1 extends C02 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Integer b;
    @DexIgnore
    public /* final */ B02 c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ Map<String, String> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends C02.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public B02 c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Map<String, String> f;

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02 d() {
            String str = "";
            if (this.a == null) {
                str = " transportName";
            }
            if (this.c == null) {
                str = str + " encodedPayload";
            }
            if (this.d == null) {
                str = str + " eventMillis";
            }
            if (this.e == null) {
                str = str + " uptimeMillis";
            }
            if (this.f == null) {
                str = str + " autoMetadata";
            }
            if (str.isEmpty()) {
                return new Vz1(this.a, this.b, this.c, this.d.longValue(), this.e.longValue(), this.f);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public Map<String, String> e() {
            Map<String, String> map = this.f;
            if (map != null) {
                return map;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02.Ai f(Map<String, String> map) {
            if (map != null) {
                this.f = map;
                return this;
            }
            throw new NullPointerException("Null autoMetadata");
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02.Ai g(Integer num) {
            this.b = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02.Ai h(B02 b02) {
            if (b02 != null) {
                this.c = b02;
                return this;
            }
            throw new NullPointerException("Null encodedPayload");
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02.Ai i(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02.Ai j(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }

        @DexIgnore
        @Override // com.fossil.C02.Ai
        public C02.Ai k(long j) {
            this.e = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public Vz1(String str, Integer num, B02 b02, long j, long j2, Map<String, String> map) {
        this.a = str;
        this.b = num;
        this.c = b02;
        this.d = j;
        this.e = j2;
        this.f = map;
    }

    @DexIgnore
    @Override // com.fossil.C02
    public Map<String, String> c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.C02
    public Integer d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.C02
    public B02 e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C02)) {
            return false;
        }
        C02 c02 = (C02) obj;
        return this.a.equals(c02.j()) && ((num = this.b) != null ? num.equals(c02.d()) : c02.d() == null) && this.c.equals(c02.e()) && this.d == c02.f() && this.e == c02.k() && this.f.equals(c02.c());
    }

    @DexIgnore
    @Override // com.fossil.C02
    public long f() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        Integer num = this.b;
        int hashCode2 = num == null ? 0 : num.hashCode();
        int hashCode3 = this.c.hashCode();
        long j = this.d;
        int i = (int) (j ^ (j >>> 32));
        long j2 = this.e;
        return ((((((((hashCode2 ^ ((hashCode ^ 1000003) * 1000003)) * 1000003) ^ hashCode3) * 1000003) ^ i) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.f.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.C02
    public String j() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.C02
    public long k() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        return "EventInternal{transportName=" + this.a + ", code=" + this.b + ", encodedPayload=" + this.c + ", eventMillis=" + this.d + ", uptimeMillis=" + this.e + ", autoMetadata=" + this.f + "}";
    }
}
