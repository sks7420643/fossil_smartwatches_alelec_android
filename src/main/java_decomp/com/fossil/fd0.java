package com.fossil;

import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fd0<T> {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Vector<Gd0<T>> b; // = new Vector<>();

    @DexIgnore
    public final void a() {
        synchronized (this) {
            this.a = false;
        }
    }

    @DexIgnore
    public final void b(Gd0<T> gd0) {
        synchronized (this) {
            if (!this.b.contains(gd0)) {
                this.b.addElement(gd0);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        r0 = ((com.fossil.Gd0[]) r2.element).length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        r1 = r0 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r1 < 0) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        ((com.fossil.U5) ((com.fossil.Gd0[]) r2.element)[r1]).g(r4);
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(T r4) {
        /*
            r3 = this;
            com.mapped.Jh6 r2 = new com.mapped.Jh6
            r2.<init>()
            monitor-enter(r3)
            boolean r0 = r3.e()     // Catch:{ all -> 0x0043 }
            if (r0 != 0) goto L_0x000e
            monitor-exit(r3)
        L_0x000d:
            return
        L_0x000e:
            java.util.Vector<com.fossil.Gd0<T>> r0 = r3.b
            r1 = 0
            com.fossil.Gd0[] r1 = new com.fossil.Gd0[r1]
            java.lang.Object[] r0 = r0.toArray(r1)
            if (r0 == 0) goto L_0x003b
            com.fossil.Gd0[] r0 = (com.fossil.Gd0[]) r0
            r2.element = r0
            r3.a()
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            monitor-exit(r3)
            if (r4 == 0) goto L_0x000d
            T r0 = r2.element
            com.fossil.Gd0[] r0 = (com.fossil.Gd0[]) r0
            int r0 = r0.length
        L_0x002a:
            int r1 = r0 + -1
            if (r1 < 0) goto L_0x000d
            T r0 = r2.element
            com.fossil.Gd0[] r0 = (com.fossil.Gd0[]) r0
            r0 = r0[r1]
            com.fossil.U5 r0 = (com.fossil.U5) r0
            r0.g(r4)
            r0 = r1
            goto L_0x002a
        L_0x003b:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        L_0x0043:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fd0.c(java.lang.Object):void");
    }

    @DexIgnore
    public final void d(Gd0<T> gd0) {
        synchronized (this) {
            this.b.removeElement(gd0);
        }
    }

    @DexIgnore
    public final boolean e() {
        boolean z;
        synchronized (this) {
            z = this.a;
        }
        return z;
    }

    @DexIgnore
    public final void f() {
        synchronized (this) {
            this.a = true;
        }
    }
}
