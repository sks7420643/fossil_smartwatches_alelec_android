package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class V80 extends Enum<V80> {
    @DexIgnore
    public static /* final */ V80 b;
    @DexIgnore
    public static /* final */ V80 c;
    @DexIgnore
    public static /* final */ V80 d;
    @DexIgnore
    public static /* final */ V80 e;
    @DexIgnore
    public static /* final */ V80 f;
    @DexIgnore
    public static /* final */ V80 g;
    @DexIgnore
    public static /* final */ V80 h;
    @DexIgnore
    public static /* final */ V80 i;
    @DexIgnore
    public static /* final */ V80 j;
    @DexIgnore
    public static /* final */ /* synthetic */ V80[] k;

    /*
    static {
        V80 v80 = new V80("PHASE_INIT", 0);
        b = v80;
        V80 v802 = new V80("PHASE_START", 1);
        c = v802;
        V80 v803 = new V80("PHASE_END", 2);
        d = v803;
        V80 v804 = new V80("REQUEST", 3);
        e = v804;
        V80 v805 = new V80("RESPONSE", 4);
        f = v805;
        V80 v806 = new V80("SYSTEM_EVENT", 5);
        g = v806;
        V80 v807 = new V80("CENTRAL_EVENT", 6);
        h = v807;
        V80 v808 = new V80("DEVICE_EVENT", 7);
        i = v808;
        V80 v809 = new V80("GATT_SERVER_EVENT", 8);
        V80 v8010 = new V80("EXCEPTION", 9);
        V80 v8011 = new V80("DATABASE", 10);
        V80 v8012 = new V80("MSL", 11);
        j = v8012;
        k = new V80[]{v80, v802, v803, v804, v805, v806, v807, v808, v809, v8010, v8011, v8012};
    }
    */

    @DexIgnore
    public V80(String str, int i2) {
    }

    @DexIgnore
    public static V80 valueOf(String str) {
        return (V80) Enum.valueOf(V80.class, str);
    }

    @DexIgnore
    public static V80[] values() {
        return (V80[]) k.clone();
    }
}
