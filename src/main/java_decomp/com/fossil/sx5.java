package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sx5 implements Factory<String> {
    @DexIgnore
    public static String a(Qx5 qx5) {
        String c = qx5.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
