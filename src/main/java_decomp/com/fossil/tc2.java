package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Jc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tc2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Tc2> CREATOR; // = new Be2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public IBinder c;
    @DexIgnore
    public Z52 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public Tc2(int i, IBinder iBinder, Z52 z52, boolean z, boolean z2) {
        this.b = i;
        this.c = iBinder;
        this.d = z52;
        this.e = z;
        this.f = z2;
    }

    @DexIgnore
    public Jc2 c() {
        return Jc2.Ai.e(this.c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tc2)) {
            return false;
        }
        Tc2 tc2 = (Tc2) obj;
        return this.d.equals(tc2.d) && c().equals(tc2.c());
    }

    @DexIgnore
    public Z52 f() {
        return this.d;
    }

    @DexIgnore
    public boolean h() {
        return this.e;
    }

    @DexIgnore
    public boolean k() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.m(parcel, 2, this.c, false);
        Bd2.t(parcel, 3, f(), i, false);
        Bd2.c(parcel, 4, h());
        Bd2.c(parcel, 5, k());
        Bd2.b(parcel, a2);
    }
}
