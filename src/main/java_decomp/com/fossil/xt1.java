package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Fp1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Qq1.class.getClassLoader());
            if (readParcelable != null) {
                Qq1 qq1 = (Qq1) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(Nt1.class.getClassLoader());
                if (readParcelable2 != null) {
                    Nt1 nt1 = (Nt1) readParcelable2;
                    Parcelable readParcelable3 = parcel.readParcelable(Fp1.class.getClassLoader());
                    if (readParcelable3 != null) {
                        return new Xt1(qq1, nt1, (Fp1) readParcelable3);
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xt1[] newArray(int i) {
            return new Xt1[i];
        }
    }

    @DexIgnore
    public Xt1(Fp1 fp1) {
        this(null, null, fp1);
    }

    @DexIgnore
    public Xt1(Qq1 qq1, Fp1 fp1) {
        super(qq1, null);
        this.d = fp1;
    }

    @DexIgnore
    public Xt1(Qq1 qq1, Nt1 nt1, Fp1 fp1) {
        super(qq1, nt1);
        this.d = fp1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return Gy1.c(super.a(), this.d.toJSONObject());
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("weatherInfo", this.d.a());
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Xt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.d, ((Xt1) obj).d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherComplicationData");
    }

    @DexIgnore
    public final Fp1 getWeatherInfo() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
