package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ti1 {
    @DexIgnore
    public /* final */ List<ImageHeaderParser> a; // = new ArrayList();

    @DexIgnore
    public void a(ImageHeaderParser imageHeaderParser) {
        synchronized (this) {
            this.a.add(imageHeaderParser);
        }
    }

    @DexIgnore
    public List<ImageHeaderParser> b() {
        List<ImageHeaderParser> list;
        synchronized (this) {
            list = this.a;
        }
        return list;
    }
}
