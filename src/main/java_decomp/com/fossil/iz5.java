package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iz5 implements Factory<Zy5> {
    @DexIgnore
    public static Zy5 a(Hz5 hz5) {
        Zy5 a2 = hz5.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
