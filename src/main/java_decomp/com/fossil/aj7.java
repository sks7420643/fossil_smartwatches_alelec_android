package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.mapped.W6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Aj7 extends Ze0 {
    @DexIgnore
    public ListView b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Gi {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;

        @DexIgnore
        public Ai(Aj7 aj7, Fragment fragment) {
            this.a = fragment;
        }

        @DexIgnore
        @Override // com.fossil.Aj7.Gi
        public void a(Cj7 cj7) {
            cj7.c(this.a);
        }

        @DexIgnore
        @Override // com.fossil.Aj7.Gi
        public Context getContext() {
            return this.a.getContext();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Gi {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity a;

        @DexIgnore
        public Bi(Aj7 aj7, FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @DexIgnore
        @Override // com.fossil.Aj7.Gi
        public void a(Cj7 cj7) {
            cj7.b(this.a);
        }

        @DexIgnore
        @Override // com.fossil.Aj7.Gi
        public Context getContext() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Gi b;

        @DexIgnore
        public Ci(Gi gi) {
            this.b = gi;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (view.getTag() instanceof Cj7) {
                this.b.a((Cj7) view.getTag());
                Aj7.this.dismiss();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Di {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Fj7.values().length];
            a = iArr;
            try {
                iArr[Fj7.Camera.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Fj7.Gallery.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends ArrayAdapter<Cj7> {
        @DexIgnore
        public Context b;

        @DexIgnore
        public Ei(Aj7 aj7, Context context, int i, List<Cj7> list) {
            super(context, i, list);
            this.b = context;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(this.b).inflate(Kj7.belvedere_dialog_row, viewGroup, false);
            }
            Cj7 cj7 = (Cj7) getItem(i);
            Fi a2 = Fi.a(cj7, this.b);
            ((ImageView) view.findViewById(Jj7.belvedere_dialog_row_image)).setImageDrawable(W6.f(this.b, a2.b()));
            ((TextView) view.findViewById(Jj7.belvedere_dialog_row_text)).setText(a2.c());
            view.setTag(cj7);
            return view;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Fi(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public static Fi a(Cj7 cj7, Context context) {
            int i = Di.a[cj7.a().ordinal()];
            return i != 1 ? i != 2 ? new Fi(-1, context.getString(Lj7.belvedere_dialog_unknown)) : new Fi(Ij7.ic_image, context.getString(Lj7.belvedere_dialog_gallery)) : new Fi(Ij7.ic_camera, context.getString(Lj7.belvedere_dialog_camera));
        }

        @DexIgnore
        public int b() {
            return this.a;
        }

        @DexIgnore
        public String c() {
            return this.b;
        }
    }

    @DexIgnore
    public interface Gi {
        @DexIgnore
        void a(Cj7 cj7);

        @DexIgnore
        Context getContext();
    }

    @DexIgnore
    public static void w6(FragmentManager fragmentManager, List<Cj7> list) {
        if (list != null && list.size() != 0) {
            Aj7 aj7 = new Aj7();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("extra_intent", new ArrayList<>(list));
            aj7.setArguments(bundle);
            aj7.show(fragmentManager.j(), "BelvedereDialog");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("extra_intent");
        if (getParentFragment() != null) {
            v6(new Ai(this, getParentFragment()), parcelableArrayList);
        } else if (getActivity() != null) {
            v6(new Bi(this, getActivity()), parcelableArrayList);
        } else {
            Log.w("BelvedereDialog", "Not able to find a valid context for starting an BelvedereIntent");
            dismiss();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, getTheme());
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(Kj7.belvedere_dialog, viewGroup, false);
        this.b = (ListView) inflate.findViewById(Jj7.belvedere_dialog_listview);
        return inflate;
    }

    @DexIgnore
    public final void v6(Gi gi, List<Cj7> list) {
        this.b.setAdapter((ListAdapter) new Ei(this, gi.getContext(), Kj7.belvedere_dialog_row, list));
        this.b.setOnItemClickListener(new Ci(gi));
    }
}
