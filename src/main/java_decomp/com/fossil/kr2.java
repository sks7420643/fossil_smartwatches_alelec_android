package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kr2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Kr2> CREATOR; // = new Lr2();
    @DexIgnore
    public int b;
    @DexIgnore
    public Ir2 c;
    @DexIgnore
    public Lb3 d;
    @DexIgnore
    public PendingIntent e;
    @DexIgnore
    public Ib3 f;
    @DexIgnore
    public Rq2 g;

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [com.fossil.Rq2] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Kr2(int r3, com.fossil.Ir2 r4, android.os.IBinder r5, android.app.PendingIntent r6, android.os.IBinder r7, android.os.IBinder r8) {
        /*
            r2 = this;
            r1 = 0
            r2.<init>()
            r2.b = r3
            r2.c = r4
            if (r5 != 0) goto L_0x0019
            r0 = r1
        L_0x000b:
            r2.d = r0
            r2.e = r6
            if (r7 != 0) goto L_0x001e
            r0 = r1
        L_0x0012:
            r2.f = r0
            if (r8 != 0) goto L_0x0023
        L_0x0016:
            r2.g = r1
            return
        L_0x0019:
            com.fossil.Lb3 r0 = com.fossil.Mb3.e(r5)
            goto L_0x000b
        L_0x001e:
            com.fossil.Ib3 r0 = com.fossil.Jb3.e(r7)
            goto L_0x0012
        L_0x0023:
            if (r8 == 0) goto L_0x0016
            java.lang.String r0 = "com.google.android.gms.location.internal.IFusedLocationProviderCallback"
            android.os.IInterface r0 = r8.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.fossil.Rq2
            if (r1 == 0) goto L_0x0033
            com.fossil.Rq2 r0 = (com.fossil.Rq2) r0
            r1 = r0
            goto L_0x0016
        L_0x0033:
            com.fossil.Tq2 r1 = new com.fossil.Tq2
            r1.<init>(r8)
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kr2.<init>(int, com.fossil.Ir2, android.os.IBinder, android.app.PendingIntent, android.os.IBinder, android.os.IBinder):void");
    }

    @DexIgnore
    public static Kr2 c(Ib3 ib3, Rq2 rq2) {
        return new Kr2(2, null, null, null, ib3.asBinder(), rq2 != null ? rq2.asBinder() : null);
    }

    @DexIgnore
    public static Kr2 f(Lb3 lb3, Rq2 rq2) {
        return new Kr2(2, null, lb3.asBinder(), null, null, rq2 != null ? rq2.asBinder() : null);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        IBinder iBinder = null;
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.t(parcel, 2, this.c, i, false);
        Lb3 lb3 = this.d;
        Bd2.m(parcel, 3, lb3 == null ? null : lb3.asBinder(), false);
        Bd2.t(parcel, 4, this.e, i, false);
        Ib3 ib3 = this.f;
        Bd2.m(parcel, 5, ib3 == null ? null : ib3.asBinder(), false);
        Rq2 rq2 = this.g;
        if (rq2 != null) {
            iBinder = rq2.asBinder();
        }
        Bd2.m(parcel, 6, iBinder, false);
        Bd2.b(parcel, a2);
    }
}
