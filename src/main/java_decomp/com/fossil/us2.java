package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Us2 extends IInterface {
    @DexIgnore
    void C1(String str, String str2, Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    int zza() throws RemoteException;
}
