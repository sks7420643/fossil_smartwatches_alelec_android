package com.fossil;

import com.mapped.Af6;
import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tu7 extends Mw7 {
    @DexIgnore
    public static /* final */ int c;
    @DexIgnore
    public static boolean d;
    @DexIgnore
    public static /* final */ Tu7 e; // = new Tu7();
    @DexIgnore
    public static volatile Executor pool;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicInteger a;

        @DexIgnore
        public Ai(AtomicInteger atomicInteger) {
            this.a = atomicInteger;
        }

        @DexIgnore
        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "CommonPool-worker-" + this.a.incrementAndGet());
            thread.setDaemon(true);
            return thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public static /* final */ Bi b; // = new Bi();

        @DexIgnore
        public final void run() {
        }
    }

    /*
    static {
        String str;
        int i;
        try {
            str = System.getProperty("kotlinx.coroutines.default.parallelism");
        } catch (Throwable th) {
            str = null;
        }
        if (str != null) {
            Integer c2 = Ut7.c(str);
            if (c2 == null || c2.intValue() < 1) {
                throw new IllegalStateException(("Expected positive number in kotlinx.coroutines.default.parallelism, but has " + str).toString());
            }
            i = c2.intValue();
        } else {
            i = -1;
        }
        c = i;
    }
    */

    @DexIgnore
    @Override // com.fossil.Dv7
    public void M(Af6 af6, Runnable runnable) {
        Runnable runnable2;
        try {
            Executor executor = pool;
            Executor X = executor != null ? executor : X();
            Xx7 a2 = Yx7.a();
            if (a2 == null || (runnable2 = a2.b(runnable)) == null) {
                runnable2 = runnable;
            }
            X.execute(runnable2);
        } catch (RejectedExecutionException e2) {
            Xx7 a3 = Yx7.a();
            if (a3 != null) {
                a3.d();
            }
            Pv7.i.z0(runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.Mw7
    public Executor S() {
        Executor executor = pool;
        return executor != null ? executor : X();
    }

    @DexIgnore
    public final ExecutorService T() {
        return Executors.newFixedThreadPool(b0(), new Ai(new AtomicInteger()));
    }

    @DexIgnore
    public final ExecutorService V() {
        Class<?> cls;
        ExecutorService executorService;
        ExecutorService executorService2;
        if (System.getSecurityManager() != null) {
            return T();
        }
        try {
            cls = Class.forName("java.util.concurrent.ForkJoinPool");
        } catch (Throwable th) {
            cls = null;
        }
        if (cls == null) {
            return T();
        }
        if (!d && c < 0) {
            try {
                Method method = cls.getMethod("commonPool", new Class[0]);
                Object invoke = method != null ? method.invoke(null, new Object[0]) : null;
                if (!(invoke instanceof ExecutorService)) {
                    invoke = null;
                }
                executorService2 = (ExecutorService) invoke;
            } catch (Throwable th2) {
                executorService2 = null;
            }
            if (executorService2 != null) {
                if (!e.g0(cls, executorService2)) {
                    executorService2 = null;
                }
                if (executorService2 != null) {
                    return executorService2;
                }
            }
        }
        try {
            Object newInstance = cls.getConstructor(Integer.TYPE).newInstance(Integer.valueOf(e.b0()));
            if (!(newInstance instanceof ExecutorService)) {
                newInstance = null;
            }
            executorService = (ExecutorService) newInstance;
        } catch (Throwable th3) {
            executorService = null;
        }
        return executorService == null ? T() : executorService;
    }

    @DexIgnore
    public final Executor X() {
        Executor executor;
        synchronized (this) {
            executor = pool;
            if (executor == null) {
                executor = V();
                pool = executor;
            }
        }
        return executor;
    }

    @DexIgnore
    public final int b0() {
        Integer valueOf = Integer.valueOf(c);
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        return valueOf != null ? valueOf.intValue() : Bs7.d(Runtime.getRuntime().availableProcessors() - 1, 1);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }

    @DexIgnore
    public final boolean g0(Class<?> cls, ExecutorService executorService) {
        Integer num;
        executorService.submit(Bi.b);
        try {
            Object invoke = cls.getMethod("getPoolSize", new Class[0]).invoke(executorService, new Object[0]);
            if (!(invoke instanceof Integer)) {
                invoke = null;
            }
            num = (Integer) invoke;
        } catch (Throwable th) {
            num = null;
        }
        return num != null && num.intValue() >= 1;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        return "CommonPool";
    }
}
