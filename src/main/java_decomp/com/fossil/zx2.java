package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zx2<K, V> {
    @DexIgnore
    public Object[] a;
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public Zx2(int i) {
        this.a = new Object[(i * 2)];
    }
}
