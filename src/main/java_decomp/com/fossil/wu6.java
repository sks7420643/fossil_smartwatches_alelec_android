package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wu6 extends BaseFragment implements Vu6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public Uu6 g;
    @DexIgnore
    public G37<Bb5> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Wu6.j;
        }

        @DexIgnore
        public final Wu6 b() {
            return new Wu6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Bb5 b;

            @DexIgnore
            public Aii(Bb5 bb5) {
                this.b = bb5;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.b.K;
                Wg6.b(flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = this.b.L;
                Wg6.b(flexibleButton, "it.tvSignup");
                flexibleButton.setVisibility(0);
            }
        }

        @DexIgnore
        public Bi(View view, Wu6 wu6) {
            this.b = view;
            this.c = wu6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                Bb5 bb5 = (Bb5) Wu6.K6(this.c).a();
                if (bb5 != null) {
                    try {
                        FlexibleTextView flexibleTextView = bb5.K;
                        Wg6.b(flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleButton flexibleButton = bb5.L;
                        Wg6.b(flexibleButton, "it.tvSignup");
                        flexibleButton.setVisibility(8);
                        Cd6 cd6 = Cd6.a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = Wu6.k.a();
                        local.d(a2, "onCreateView - e=" + e);
                        Cd6 cd62 = Cd6.a;
                    }
                }
            } else {
                Bb5 bb52 = (Bb5) Wu6.K6(this.c).a();
                if (bb52 != null) {
                    try {
                        Wg6.b(bb52, "it");
                        bb52.n().postDelayed(new Aii(bb52), 100);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = Wu6.k.a();
                        local2.d(a3, "onCreateView - e=" + e2);
                        Cd6 cd63 = Cd6.a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Ci(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().u();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Di(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                SignUpActivity.a aVar = SignUpActivity.F;
                Wg6.b(activity, "it");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Ei(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 a;

        @DexIgnore
        public Fi(Wu6 wu6) {
            this.a = wu6;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(Wu6.k.a(), "Password DONE key, trigger login flow");
            this.a.O6();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Bb5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 c;

        @DexIgnore
        public Gi(Bb5 bb5, Wu6 wu6) {
            this.b = bb5;
            this.c = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.b.w;
            Wg6.b(flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.b.v;
            Wg6.b(flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.c.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Hi(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.b.N6().w(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Ii(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.b.N6().v(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Ji(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.b.N6().x(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Ki(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Li(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Mi(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Ni(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Wu6 b;

        @DexIgnore
        public Oi(Wu6 wu6) {
            this.b = wu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.N6().t();
        }
    }

    /*
    static {
        String simpleName = Wu6.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "LoginFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Wu6 wu6) {
        G37<Bb5> g37 = wu6.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void C(int i2, String str) {
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void H5(String str) {
        Wg6.c(str, Constants.EMAIL);
        Context context = getContext();
        if (context != null) {
            ForgotPasswordActivity.a aVar = ForgotPasswordActivity.B;
            Wg6.b(context, "it");
            aVar.a(context, str);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Uu6 uu6) {
        P6(uu6);
    }

    @DexIgnore
    public final Uu6 N6() {
        Uu6 uu6 = this.g;
        if (uu6 != null) {
            return uu6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void O0() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        G37<Bb5> g37 = this.h;
        if (g37 != null) {
            Bb5 a2 = g37.a();
            if (!(a2 == null || (flexibleButton2 = a2.s) == null)) {
                flexibleButton2.setEnabled(false);
            }
            G37<Bb5> g372 = this.h;
            if (g372 != null) {
                Bb5 a3 = g372.a();
                if (a3 != null && (flexibleButton = a3.s) != null) {
                    flexibleButton.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void O4(boolean z) {
        G37<Bb5> g37 = this.h;
        if (g37 != null) {
            Bb5 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ConstraintLayout constraintLayout = a2.D;
                Wg6.b(constraintLayout, "it.ivWeibo");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.C;
                Wg6.b(constraintLayout2, "it.ivWechat");
                constraintLayout2.setVisibility(0);
                ImageView imageView = a2.B;
                Wg6.b(imageView, "it.ivGoogle");
                imageView.setVisibility(8);
                ImageView imageView2 = a2.A;
                Wg6.b(imageView2, "it.ivFacebook");
                imageView2.setVisibility(8);
                return;
            }
            ConstraintLayout constraintLayout3 = a2.D;
            Wg6.b(constraintLayout3, "it.ivWeibo");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.C;
            Wg6.b(constraintLayout4, "it.ivWechat");
            constraintLayout4.setVisibility(8);
            ImageView imageView3 = a2.B;
            Wg6.b(imageView3, "it.ivGoogle");
            imageView3.setVisibility(0);
            ImageView imageView4 = a2.A;
            Wg6.b(imageView4, "it.ivFacebook");
            imageView4.setVisibility(0);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        G37<Bb5> g37 = this.h;
        if (g37 != null) {
            Bb5 a2 = g37.a();
            if (a2 != null) {
                Uu6 uu6 = this.g;
                if (uu6 != null) {
                    FlexibleTextInputEditText flexibleTextInputEditText = a2.t;
                    Wg6.b(flexibleTextInputEditText, "etEmail");
                    String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                    FlexibleTextInputEditText flexibleTextInputEditText2 = a2.u;
                    Wg6.b(flexibleTextInputEditText2, "etPassword");
                    uu6.q(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void P1(boolean z, String str) {
        FlexibleTextInputLayout flexibleTextInputLayout;
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            G37<Bb5> g37 = this.h;
            if (g37 != null) {
                Bb5 a2 = g37.a();
                if (a2 != null && (flexibleTextInputLayout = a2.v) != null) {
                    Wg6.b(flexibleTextInputLayout, "it");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    flexibleTextInputLayout.setError(str);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void P6(Uu6 uu6) {
        Wg6.c(uu6, "presenter");
        this.g = uu6;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void R() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = H37.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.E;
            Wg6.b(activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void W1() {
        if (isActive()) {
            G37<Bb5> g37 = this.h;
            if (g37 != null) {
                Bb5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                    Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setError(" ");
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.w;
                    Wg6.b(flexibleTextInputLayout2, "it.inputPassword");
                    flexibleTextInputLayout2.setError(Um5.c(PortfolioApp.get.instance(), 2131886912));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void c3() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        G37<Bb5> g37 = this.h;
        if (g37 != null) {
            Bb5 a2 = g37.a();
            if (!(a2 == null || (flexibleButton2 = a2.s) == null)) {
                flexibleButton2.setEnabled(true);
            }
            G37<Bb5> g372 = this.h;
            if (g372 != null) {
                Bb5 a3 = g372.a();
                if (a3 != null && (flexibleButton = a3.s) != null) {
                    flexibleButton.d("flexible_button_primary");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void e5(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.B;
            Wg6.b(activity, "it");
            aVar.b(activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void i() {
        String string = getString(2131886911);
        Wg6.b(string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        H6(string);
    }

    @DexIgnore
    @Override // com.fossil.Vu6
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            Wg6.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e(j, "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "Apple Auth Info = " + signUpSocialAuth);
            Uu6 uu6 = this.g;
            if (uu6 != null) {
                Wg6.b(signUpSocialAuth, "authCode");
                uu6.p(signUpSocialAuth);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Bb5 bb5 = (Bb5) Aq0.f(layoutInflater, 2131558621, viewGroup, false, A6());
        Wg6.b(bb5, "binding");
        View n = bb5.n();
        Wg6.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new Bi(n, this));
        G37<Bb5> g37 = new G37<>(this, bb5);
        this.h = g37;
        if (g37 != null) {
            Bb5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Uu6 uu6 = this.g;
        if (uu6 != null) {
            uu6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Uu6 uu6 = this.g;
        if (uu6 != null) {
            uu6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Bb5> g37 = this.h;
        if (g37 != null) {
            Bb5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new Gi(a2, this));
                a2.t.addTextChangedListener(new Hi(this));
                a2.t.setOnFocusChangeListener(new Ii(this));
                a2.u.addTextChangedListener(new Ji(this));
                a2.z.setOnClickListener(new Ki(this));
                a2.A.setOnClickListener(new Li(this));
                a2.B.setOnClickListener(new Mi(this));
                a2.x.setOnClickListener(new Ni(this));
                a2.C.setOnClickListener(new Oi(this));
                a2.D.setOnClickListener(new Ci(this));
                a2.L.setOnClickListener(new Di(this));
                a2.J.setOnClickListener(new Ei(this));
                a2.u.setOnEditorActionListener(new Fi(this));
            }
            E6("login_view");
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
