package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pw7 {
    @DexIgnore
    public static final Dv7 a(Executor executor) {
        Dv7 dv7;
        Aw7 aw7 = (Aw7) (!(executor instanceof Aw7) ? null : executor);
        return (aw7 == null || (dv7 = aw7.b) == null) ? new Ow7(executor) : dv7;
    }

    @DexIgnore
    public static final Mw7 b(ExecutorService executorService) {
        return new Ow7(executorService);
    }
}
