package com.fossil;

import com.mapped.Qg6;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nz7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e; // = AtomicReferenceFieldUpdater.newUpdater(Nz7.class, Object.class, "_next");
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater f; // = AtomicLongFieldUpdater.newUpdater(Nz7.class, "_state");
    @DexIgnore
    public static /* final */ Vz7 g; // = new Vz7("REMOVE_FROZEN");
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public volatile Object _next; // = null;
    @DexIgnore
    public volatile long _state; // = 0;
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public AtomicReferenceArray b; // = new AtomicReferenceArray(this.c);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int a(long j) {
            return (2305843009213693952L & j) != 0 ? 2 : 1;
        }

        @DexIgnore
        public final long b(long j, int i) {
            return d(j, 1073741823) | (((long) i) << 0);
        }

        @DexIgnore
        public final long c(long j, int i) {
            return d(j, 1152921503533105152L) | (((long) i) << 30);
        }

        @DexIgnore
        public final long d(long j, long j2) {
            return j & j2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Bi(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    public Nz7(int i, boolean z) {
        boolean z2 = true;
        this.c = i;
        this.d = z;
        this.a = i - 1;
        if (this.a <= 1073741823) {
            if (!((this.c & this.a) != 0 ? false : z2)) {
                throw new IllegalStateException("Check failed.".toString());
            }
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(E r15) {
        /*
            r14 = this;
            r12 = 0
            r10 = 1073741823(0x3fffffff, float:1.9999999)
            r6 = 1
            r7 = 0
        L_0x0007:
            long r2 = r14._state
            r0 = 3458764513820540928(0x3000000000000000, double:1.727233711018889E-77)
            long r0 = r0 & r2
            int r0 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r0 == 0) goto L_0x0017
            com.fossil.Nz7$Ai r0 = com.fossil.Nz7.h
            int r0 = r0.a(r2)
        L_0x0016:
            return r0
        L_0x0017:
            r0 = 1073741823(0x3fffffff, double:5.304989472E-315)
            long r0 = r0 & r2
            long r0 = r0 >> r7
            int r0 = (int) r0
            r4 = 1152921503533105152(0xfffffffc0000000, double:1.2882296003504729E-231)
            long r4 = r4 & r2
            r1 = 30
            long r4 = r4 >> r1
            int r8 = (int) r4
            int r9 = r14.a
            int r1 = r8 + 2
            r1 = r1 & r9
            r4 = r0 & r9
            if (r1 != r4) goto L_0x0032
            r0 = r6
            goto L_0x0016
        L_0x0032:
            boolean r1 = r14.d
            if (r1 != 0) goto L_0x004f
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r14.b
            r4 = r8 & r9
            java.lang.Object r1 = r1.get(r4)
            if (r1 == 0) goto L_0x004f
            int r1 = r14.c
            r2 = 1024(0x400, float:1.435E-42)
            if (r1 < r2) goto L_0x004d
            int r0 = r8 - r0
            r0 = r0 & r10
            int r1 = r1 >> 1
            if (r0 <= r1) goto L_0x0007
        L_0x004d:
            r0 = r6
            goto L_0x0016
        L_0x004f:
            java.util.concurrent.atomic.AtomicLongFieldUpdater r0 = com.fossil.Nz7.f
            com.fossil.Nz7$Ai r1 = com.fossil.Nz7.h
            int r4 = r8 + 1
            r4 = r4 & r10
            long r4 = r1.c(r2, r4)
            r1 = r14
            boolean r0 = r0.compareAndSet(r1, r2, r4)
            if (r0 == 0) goto L_0x0007
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r14.b
            r1 = r8 & r9
            r0.set(r1, r15)
        L_0x0068:
            long r0 = r14._state
            r2 = 1152921504606846976(0x1000000000000000, double:1.2882297539194267E-231)
            long r0 = r0 & r2
            int r0 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x0073
        L_0x0071:
            r0 = r7
            goto L_0x0016
        L_0x0073:
            com.fossil.Nz7 r0 = r14.i()
            com.fossil.Nz7 r14 = r0.e(r8, r15)
            if (r14 == 0) goto L_0x0071
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Nz7.a(java.lang.Object):int");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v6, resolved type: java.util.concurrent.atomic.AtomicReferenceArray */
    /* JADX WARN: Multi-variable type inference failed */
    public final Nz7<E> b(long j) {
        Nz7<E> nz7 = new Nz7<>(this.c * 2, this.d);
        int i = (int) ((1073741823 & j) >> 0);
        int i2 = (int) ((1152921503533105152L & j) >> 30);
        while (true) {
            int i3 = this.a;
            if ((i & i3) != (i2 & i3)) {
                Object obj = this.b.get(i3 & i);
                if (obj == null) {
                    obj = new Bi(i);
                }
                nz7.b.set(nz7.a & i, obj);
                i++;
            } else {
                nz7._state = h.d(j, 1152921504606846976L);
                return nz7;
            }
        }
    }

    @DexIgnore
    public final Nz7<E> c(long j) {
        while (true) {
            Nz7<E> nz7 = (Nz7) this._next;
            if (nz7 != null) {
                return nz7;
            }
            e.compareAndSet(this, null, b(j));
        }
    }

    @DexIgnore
    public final boolean d() {
        long j;
        do {
            j = this._state;
            if ((j & 2305843009213693952L) != 0) {
                return true;
            }
            if ((1152921504606846976L & j) != 0) {
                return false;
            }
        } while (!f.compareAndSet(this, j, j | 2305843009213693952L));
        return true;
    }

    @DexIgnore
    public final Nz7<E> e(int i, E e2) {
        Object obj = this.b.get(this.a & i);
        if (!(obj instanceof Bi) || ((Bi) obj).a != i) {
            return null;
        }
        this.b.set(this.a & i, e2);
        return this;
    }

    @DexIgnore
    public final int f() {
        long j = this._state;
        return (((int) ((j & 1152921503533105152L) >> 30)) - ((int) ((1073741823 & j) >> 0))) & 1073741823;
    }

    @DexIgnore
    public final boolean g() {
        long j = this._state;
        return ((int) ((1073741823 & j) >> 0)) == ((int) ((j & 1152921503533105152L) >> 30));
    }

    @DexIgnore
    public final long h() {
        long j;
        long j2;
        do {
            j = this._state;
            if ((j & 1152921504606846976L) != 0) {
                return j;
            }
            j2 = j | 1152921504606846976L;
        } while (!f.compareAndSet(this, j, j2));
        return j2;
    }

    @DexIgnore
    public final Nz7<E> i() {
        return c(h());
    }

    @DexIgnore
    public final Object j() {
        while (true) {
            long j = this._state;
            if ((1152921504606846976L & j) != 0) {
                return g;
            }
            int i = (int) ((1073741823 & j) >> 0);
            int i2 = this.a;
            if ((((int) ((1152921503533105152L & j) >> 30)) & i2) == (i & i2)) {
                return null;
            }
            Object obj = this.b.get(i2 & i);
            if (obj == null) {
                if (this.d) {
                    return null;
                }
            } else if (obj instanceof Bi) {
                return null;
            } else {
                int i3 = (i + 1) & 1073741823;
                if (f.compareAndSet(this, j, h.b(j, i3))) {
                    this.b.set(this.a & i, null);
                    return obj;
                } else if (this.d) {
                    do {
                        this = this.k(i, i3);
                    } while (this != null);
                    return obj;
                }
            }
        }
    }

    @DexIgnore
    public final Nz7<E> k(int i, int i2) {
        long j;
        int i3;
        do {
            j = this._state;
            i3 = (int) ((1073741823 & j) >> 0);
            if (Nv7.a()) {
                if (!(i3 == i)) {
                    throw new AssertionError();
                }
            }
            if ((1152921504606846976L & j) != 0) {
                return i();
            }
        } while (!f.compareAndSet(this, j, h.b(j, i2)));
        this.b.set(this.a & i3, null);
        return null;
    }
}
