package com.fossil;

import com.fossil.M34;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class N34<E> extends H34<E> {
    @DexIgnore
    @Deprecated
    public static <E> M34.Ai<E> builder() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> copyOf(E[] eArr) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> of(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> of(E e, E e2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> of(E e, E e2, E e3) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> of(E e, E e2, E e3, E e4) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> of(E e, E e2, E e3, E e4, E e5) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> M34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        throw new UnsupportedOperationException();
    }
}
