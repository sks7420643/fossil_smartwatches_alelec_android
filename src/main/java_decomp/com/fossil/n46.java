package com.fossil;

import androidx.loader.app.LoaderManager;
import com.mapped.SetNotificationFiltersUserCase;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N46 implements Factory<NotificationContactsAndAppsAssignedPresenter> {
    @DexIgnore
    public static NotificationContactsAndAppsAssignedPresenter a(LoaderManager loaderManager, H46 h46, int i, Uq4 uq4, Y56 y56, L56 l56, K66 k66, SetNotificationFiltersUserCase setNotificationFiltersUserCase) {
        return new NotificationContactsAndAppsAssignedPresenter(loaderManager, h46, i, uq4, y56, l56, k66, setNotificationFiltersUserCase);
    }
}
