package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ss4 {
    @DexIgnore
    @Vu3("challengeId")
    public String a;
    @DexIgnore
    @Vu3("name")
    public String b;
    @DexIgnore
    @Vu3("challengeType")
    public String c;
    @DexIgnore
    @Vu3("target")
    public Integer d;
    @DexIgnore
    @Vu3("duration")
    public Integer e;

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final Integer c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ss4) {
                Ss4 ss4 = (Ss4) obj;
                if (!Wg6.a(this.a, ss4.a) || !Wg6.a(this.b, ss4.b) || !Wg6.a(this.c, ss4.c) || !Wg6.a(this.d, ss4.d) || !Wg6.a(this.e, ss4.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Integer num = this.d;
        int hashCode4 = num != null ? num.hashCode() : 0;
        Integer num2 = this.e;
        if (num2 != null) {
            i = num2.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeData(id=" + this.a + ", name=" + this.b + ", type=" + this.c + ", target=" + this.d + ", duration=" + this.e + ")";
    }
}
