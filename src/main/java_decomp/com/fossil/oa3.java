package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oa3 implements Parcelable.Creator<LocationAvailability> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationAvailability createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        long j = 0;
        Xa3[] xa3Arr = null;
        int i = 1000;
        int i2 = 1;
        int i3 = 1;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i3 = Ad2.v(parcel, t);
            } else if (l == 2) {
                i2 = Ad2.v(parcel, t);
            } else if (l == 3) {
                j = Ad2.y(parcel, t);
            } else if (l == 4) {
                i = Ad2.v(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                xa3Arr = (Xa3[]) Ad2.i(parcel, t, Xa3.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new LocationAvailability(i, i3, i2, j, xa3Arr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationAvailability[] newArray(int i) {
        return new LocationAvailability[i];
    }
}
