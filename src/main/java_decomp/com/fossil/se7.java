package com.fossil;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import com.facebook.places.PlaceManager;
import com.facebook.places.model.PlaceFields;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Se7 {
    @DexIgnore
    public static String a(Context context) {
        try {
            if (d(context, "android.permission.READ_PHONE_STATE")) {
                String deviceId = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getDeviceId();
                if (deviceId != null) {
                    return deviceId;
                }
            } else {
                Log.i("MID", "Could not get permission of android.permission.READ_PHONE_STATE");
            }
        } catch (Throwable th) {
            Log.w("MID", th);
        }
        return "";
    }

    @DexIgnore
    public static void b(String str, Throwable th) {
        Log.e("MID", str, th);
    }

    @DexIgnore
    public static void c(JSONObject jSONObject, String str, String str2) {
        if (f(str2)) {
            jSONObject.put(str, str2);
        }
    }

    @DexIgnore
    public static boolean d(Context context, String str) {
        try {
            return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
        } catch (Throwable th) {
            b("checkPermission error", th);
            return false;
        }
    }

    @DexIgnore
    public static String e(Context context) {
        String str;
        if (d(context, "android.permission.ACCESS_WIFI_STATE")) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService(PlaceManager.PARAM_WIFI);
                return wifiManager == null ? "" : wifiManager.getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                str = "get wifi address error" + e;
            }
        } else {
            str = "Could not get permission of android.permission.ACCESS_WIFI_STATE";
            Log.i("MID", str);
            return "";
        }
    }

    @DexIgnore
    public static boolean f(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }

    @DexIgnore
    public static boolean g(String str) {
        return str != null && str.trim().length() >= 40;
    }

    @DexIgnore
    public static String h(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(Zh7.d(Base64.decode(str.getBytes("UTF-8"), 0)), "UTF-8").trim().replace("\t", "").replace("\n", "").replace("\r", "");
        } catch (Throwable th) {
            b("decode error", th);
            return str;
        }
    }

    @DexIgnore
    public static String i(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(Base64.encode(Zh7.b(str.getBytes("UTF-8")), 0), "UTF-8").trim().replace("\t", "").replace("\n", "").replace("\r", "");
        } catch (Throwable th) {
            b("decode error", th);
            return str;
        }
    }
}
