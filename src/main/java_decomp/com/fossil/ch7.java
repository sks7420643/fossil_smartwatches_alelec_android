package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ch7 {
    @DexIgnore
    public static volatile long f;
    @DexIgnore
    public Ng7 a;
    @DexIgnore
    public Gg7 b; // = null;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public Context d; // = null;
    @DexIgnore
    public long e; // = System.currentTimeMillis();

    @DexIgnore
    public Ch7(Ng7 ng7) {
        this.a = ng7;
        this.b = Fg7.I();
        this.c = ng7.g();
        this.d = ng7.f();
    }

    @DexIgnore
    public void b() {
        if (!h()) {
            if (Fg7.J > 0 && this.e >= f) {
                Ig7.u(this.d);
                f = this.e + Fg7.K;
                if (Fg7.K()) {
                    Th7 th7 = Ig7.m;
                    th7.h("nextFlushTime=" + f);
                }
            }
            if (Tg7.a(this.d).j()) {
                if (Fg7.K()) {
                    Th7 th72 = Ig7.m;
                    th72.h("sendFailedCount=" + Ig7.p);
                }
                if (!Ig7.g()) {
                    e();
                    return;
                }
                Gh7.b(this.d).g(this.a, null, this.c, false);
                if (this.e - Ig7.q > 1800000) {
                    Ig7.q(this.d);
                    return;
                }
                return;
            }
            Gh7.b(this.d).g(this.a, null, this.c, false);
        }
    }

    @DexIgnore
    public final void c(Pi7 pi7) {
        Qi7.f(Ig7.r).c(this.a, pi7);
    }

    @DexIgnore
    public final void e() {
        if (this.a.e() != null && this.a.e().e()) {
            this.b = Gg7.INSTANT;
        }
        if (Fg7.z && Tg7.a(Ig7.r).i()) {
            this.b = Gg7.INSTANT;
        }
        if (Fg7.K()) {
            Th7 th7 = Ig7.m;
            th7.h("strategy=" + this.b.name());
        }
        switch (Wg7.a[this.b.ordinal()]) {
            case 1:
                f();
                return;
            case 2:
                Gh7.b(this.d).g(this.a, null, this.c, false);
                if (Fg7.K()) {
                    Th7 th72 = Ig7.m;
                    th72.h("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + Ig7.s + ",difftime=" + (Ig7.s - this.e));
                }
                if (Ig7.s == 0) {
                    Ig7.s = Ii7.b(this.d, "last_period_ts", 0);
                    if (this.e > Ig7.s) {
                        Ig7.s(this.d);
                    }
                    long F = this.e + ((long) (Fg7.F() * 60 * 1000));
                    if (Ig7.s > F) {
                        Ig7.s = F;
                    }
                    Li7.b(this.d).c();
                }
                if (Fg7.K()) {
                    Th7 th73 = Ig7.m;
                    th73.h("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + Ig7.s + ",difftime=" + (Ig7.s - this.e));
                }
                if (this.e > Ig7.s) {
                    Ig7.s(this.d);
                    return;
                }
                return;
            case 3:
            case 4:
                Gh7.b(this.d).g(this.a, null, this.c, false);
                return;
            case 5:
                Gh7.b(this.d).g(this.a, new Dh7(this), this.c, true);
                return;
            case 6:
                if (Tg7.a(Ig7.r).g() == 1) {
                    f();
                    return;
                } else {
                    Gh7.b(this.d).g(this.a, null, this.c, false);
                    return;
                }
            case 7:
                if (Ei7.x(this.d)) {
                    c(new Eh7(this));
                    return;
                }
                return;
            default:
                Th7 th74 = Ig7.m;
                th74.f("Invalid stat strategy:" + Fg7.I());
                return;
        }
    }

    @DexIgnore
    public final void f() {
        if (Gh7.u().f <= 0 || !Fg7.I) {
            c(new Fh7(this));
            return;
        }
        Gh7.u().g(this.a, null, this.c, true);
        Gh7.u().d(-1);
    }

    @DexIgnore
    public final boolean h() {
        if (Fg7.w > 0) {
            if (this.e > Ig7.d) {
                Ig7.c.clear();
                long unused = Ig7.d = this.e + Fg7.x;
                if (Fg7.K()) {
                    Th7 th7 = Ig7.m;
                    th7.h("clear methodsCalledLimitMap, nextLimitCallClearTime=" + Ig7.d);
                }
            }
            Integer valueOf = Integer.valueOf(this.a.a().a());
            Integer num = (Integer) Ig7.c.get(valueOf);
            if (num != null) {
                Ig7.c.put(valueOf, Integer.valueOf(num.intValue() + 1));
                if (num.intValue() > Fg7.w) {
                    if (Fg7.K()) {
                        Th7 th72 = Ig7.m;
                        th72.d("event " + this.a.h() + " was discard, cause of called limit, current:" + num + ", limit:" + Fg7.w + ", period:" + Fg7.x + " ms");
                    }
                    return true;
                }
            } else {
                Ig7.c.put(valueOf, 1);
            }
        }
        return false;
    }
}
