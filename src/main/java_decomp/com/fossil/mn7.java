package com.fossil;

import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mn7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6[] b;

        @DexIgnore
        public Ai(Hg6[] hg6Arr) {
            this.b = hg6Arr;
        }

        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.d(t, t2, this.b);
        }
    }

    @DexIgnore
    public static final <T> Comparator<T> b(Hg6<? super T, ? extends Comparable<?>>... hg6Arr) {
        Wg6.c(hg6Arr, "selectors");
        if (hg6Arr.length > 0) {
            return new Ai(hg6Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    public static final <T extends Comparable<?>> int c(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    @DexIgnore
    public static final <T> int d(T t, T t2, Hg6<? super T, ? extends Comparable<?>>[] hg6Arr) {
        for (Hg6<? super T, ? extends Comparable<?>> hg6 : hg6Arr) {
            int c = c((Comparable) hg6.invoke(t), (Comparable) hg6.invoke(t2));
            if (c != 0) {
                return c;
            }
        }
        return 0;
    }
}
