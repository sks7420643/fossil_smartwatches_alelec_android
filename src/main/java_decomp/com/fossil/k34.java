package com.fossil;

import com.fossil.A34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K34<K, V> extends L34<K, V> implements NavigableMap<K, V> {
    @DexIgnore
    public static /* final */ Comparator<Comparable> i; // = I44.natural();
    @DexIgnore
    public static /* final */ K34<Comparable, Object> j; // = new K34<>(M34.emptySet(I44.natural()), Y24.of());
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient S44<K> f;
    @DexIgnore
    public /* final */ transient Y24<V> g;
    @DexIgnore
    public transient K34<K, V> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends C34<K, V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends S24<Map.Entry<K, V>> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.fossil.S24
            public U24<Map.Entry<K, V>> delegateCollection() {
                return Ai.this;
            }

            @DexIgnore
            @Override // java.util.List
            public Map.Entry<K, V> get(int i) {
                return X34.e(K34.this.f.asList().get(i), K34.this.g.get(i));
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.H34
        public Y24<Map.Entry<K, V>> createAsList() {
            return new Aii();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
        public H54<Map.Entry<K, V>> iterator() {
            return asList().iterator();
        }

        @DexIgnore
        @Override // com.fossil.C34
        public A34<K, V> map() {
            return K34.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<K, V> extends A34.Bi<K, V> {
        @DexIgnore
        public /* final */ Comparator<? super K> e;

        @DexIgnore
        public Bi(Comparator<? super K> comparator) {
            I14.l(comparator);
            this.e = comparator;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34 a() {
            return g();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi c(Object obj, Object obj2) {
            h(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi d(Map.Entry entry) {
            i(entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi e(Iterable iterable) {
            j(iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi f(Map map) {
            k(map);
            return this;
        }

        @DexIgnore
        public K34<K, V> g() {
            int i = this.c;
            return i != 0 ? i != 1 ? K34.e(this.e, false, this.b, i) : K34.g(this.e, this.b[0].getKey(), this.b[0].getValue()) : K34.emptyMap(this.e);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> h(K k, V v) {
            super.c(k, v);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> i(Map.Entry<? extends K, ? extends V> entry) {
            super.d(entry);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> j(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.e(iterable);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<K, V> k(Map<? extends K, ? extends V> map) {
            super.f(map);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends A34.Ei {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<Object> comparator;

        @DexIgnore
        public Ci(K34<?, ?> k34) {
            super(k34);
            this.comparator = k34.comparator();
        }

        @DexIgnore
        @Override // com.fossil.A34.Ei
        public Object readResolve() {
            return createMap(new Bi(this.comparator));
        }
    }

    @DexIgnore
    public K34(S44<K> s44, Y24<V> y24) {
        this(s44, y24, null);
    }

    @DexIgnore
    public K34(S44<K> s44, Y24<V> y24, K34<K, V> k34) {
        this.f = s44;
        this.g = y24;
        this.h = k34;
    }

    @DexIgnore
    public static <K, V> K34<K, V> b(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        boolean z = false;
        if (map instanceof SortedMap) {
            Comparator<? super K> comparator2 = ((SortedMap) map).comparator();
            if (comparator2 != null) {
                z = comparator.equals(comparator2);
            } else if (comparator == i) {
                z = true;
            }
        }
        if (z && (map instanceof K34)) {
            K34<K, V> k34 = (K34) map;
            if (!k34.isPartialView()) {
                return k34;
            }
        }
        return c(comparator, z, map.entrySet());
    }

    @DexIgnore
    public static <K, V> K34<K, V> c(Comparator<? super K> comparator, boolean z, Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) O34.i(iterable, A34.EMPTY_ENTRY_ARRAY);
        return e(comparator, z, entryArr, entryArr.length);
    }

    @DexIgnore
    public static <K, V> K34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return copyOf(iterable, (I44) i);
    }

    @DexIgnore
    public static <K, V> K34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable, Comparator<? super K> comparator) {
        I14.l(comparator);
        return c(comparator, false, iterable);
    }

    @DexIgnore
    public static <K, V> K34<K, V> copyOf(Map<? extends K, ? extends V> map) {
        return b(map, (I44) i);
    }

    @DexIgnore
    public static <K, V> K34<K, V> copyOf(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        I14.l(comparator);
        return b(map, comparator);
    }

    @DexIgnore
    public static <K, V> K34<K, V> copyOfSorted(SortedMap<K, ? extends V> sortedMap) {
        Comparator<? super K> comparator = sortedMap.comparator();
        Comparator<Comparable> comparator2 = comparator == null ? i : comparator;
        if (sortedMap instanceof K34) {
            K34<K, V> k34 = (K34) sortedMap;
            if (!k34.isPartialView()) {
                return k34;
            }
        }
        return c(comparator2, true, sortedMap.entrySet());
    }

    @DexIgnore
    public static <K, V> K34<K, V> e(Comparator<? super K> comparator, boolean z, Map.Entry<K, V>[] entryArr, int i2) {
        if (i2 == 0) {
            return emptyMap(comparator);
        }
        if (i2 == 1) {
            return g(comparator, entryArr[0].getKey(), entryArr[0].getValue());
        }
        Object[] objArr = new Object[i2];
        Object[] objArr2 = new Object[i2];
        if (z) {
            for (int i3 = 0; i3 < i2; i3++) {
                K key = entryArr[i3].getKey();
                V value = entryArr[i3].getValue();
                A24.a(key, value);
                objArr[i3] = key;
                objArr2[i3] = value;
            }
        } else {
            Arrays.sort(entryArr, 0, i2, I44.from(comparator).onKeys());
            K key2 = entryArr[0].getKey();
            objArr[0] = key2;
            objArr2[0] = entryArr[0].getValue();
            int i4 = 1;
            while (i4 < i2) {
                K key3 = entryArr[i4].getKey();
                V value2 = entryArr[i4].getValue();
                A24.a(key3, value2);
                objArr[i4] = key3;
                objArr2[i4] = value2;
                A34.checkNoConflict(comparator.compare(key2, key3) != 0, "key", entryArr[i4 - 1], entryArr[i4]);
                i4++;
                key2 = key3;
            }
        }
        return new K34<>(new S44(new O44(objArr), comparator), new O44(objArr2));
    }

    @DexIgnore
    public static <K, V> K34<K, V> emptyMap(Comparator<? super K> comparator) {
        return I44.natural().equals(comparator) ? of() : new K34<>(M34.emptySet(comparator), Y24.of());
    }

    @DexIgnore
    public static <K, V> K34<K, V> g(Comparator<? super K> comparator, K k, V v) {
        Y24 of = Y24.of(k);
        I14.l(comparator);
        return new K34<>(new S44(of, comparator), Y24.of(v));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> K34<K, V> h(B34<K, V>... b34Arr) {
        return e(I44.natural(), false, b34Arr, b34Arr.length);
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> Bi<K, V> naturalOrder() {
        return new Bi<>(I44.natural());
    }

    @DexIgnore
    public static <K, V> K34<K, V> of() {
        return (K34<K, V>) j;
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> K34<K, V> of(K k, V v) {
        return g(I44.natural(), k, v);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> K34<K, V> of(K k, V v, K k2, V v2) {
        return h(A34.entryOf(k, v), A34.entryOf(k2, v2));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> K34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return h(A34.entryOf(k, v), A34.entryOf(k2, v2), A34.entryOf(k3, v3));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> K34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return h(A34.entryOf(k, v), A34.entryOf(k2, v2), A34.entryOf(k3, v3), A34.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> K34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return h(A34.entryOf(k, v), A34.entryOf(k2, v2), A34.entryOf(k3, v3), A34.entryOf(k4, v4), A34.entryOf(k5, v5));
    }

    @DexIgnore
    public static <K, V> Bi<K, V> orderedBy(Comparator<K> comparator) {
        return new Bi<>(comparator);
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> Bi<K, V> reverseOrder() {
        return new Bi<>(I44.natural().reverse());
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> ceilingEntry(K k) {
        return tailMap((K34<K, V>) k, true).firstEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K ceilingKey(K k) {
        return (K) X34.i(ceilingEntry(k));
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public Comparator<? super K> comparator() {
        return keySet().comparator();
    }

    @DexIgnore
    @Override // com.fossil.A34
    public H34<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? H34.of() : new Ai();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public M34<K> descendingKeySet() {
        return this.f.descendingSet();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K34<K, V> descendingMap() {
        K34<K, V> k34 = this.h;
        return k34 == null ? isEmpty() ? emptyMap(I44.from(comparator()).reverse()) : new K34<>((S44) this.f.descendingSet(), this.g.reverse(), this) : k34;
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34, com.fossil.A34, java.util.SortedMap
    public H34<Map.Entry<K, V>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    public final K34<K, V> f(int i2, int i3) {
        return (i2 == 0 && i3 == size()) ? this : i2 == i3 ? emptyMap(comparator()) : new K34<>(this.f.getSubSet(i2, i3), this.g.subList(i2, i3));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> firstEntry() {
        if (isEmpty()) {
            return null;
        }
        return entrySet().asList().get(0);
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public K firstKey() {
        return keySet().first();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> floorEntry(K k) {
        return headMap((K34<K, V>) k, true).lastEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K floorKey(K k) {
        return (K) X34.i(floorEntry(k));
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34
    public V get(Object obj) {
        int indexOf = this.f.indexOf(obj);
        if (indexOf == -1) {
            return null;
        }
        return this.g.get(indexOf);
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public K34<K, V> headMap(K k) {
        return headMap((K34<K, V>) k, false);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K34<K, V> headMap(K k, boolean z) {
        S44<K> s44 = this.f;
        I14.l(k);
        return f(0, s44.headIndex(k, z));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> higherEntry(K k) {
        return tailMap((K34<K, V>) k, false).firstEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K higherKey(K k) {
        return (K) X34.i(higherEntry(k));
    }

    @DexIgnore
    @Override // com.fossil.A34
    public boolean isPartialView() {
        return this.f.isPartialView() || this.g.isPartialView();
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34, com.fossil.A34, java.util.SortedMap
    public M34<K> keySet() {
        return this.f;
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> lastEntry() {
        if (isEmpty()) {
            return null;
        }
        return entrySet().asList().get(size() - 1);
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public K lastKey() {
        return keySet().last();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> lowerEntry(K k) {
        return headMap((K34<K, V>) k, false).lastEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K lowerKey(K k) {
        return (K) X34.i(lowerEntry(k));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public M34<K> navigableKeySet() {
        return this.f;
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    @CanIgnoreReturnValue
    @Deprecated
    public final Map.Entry<K, V> pollFirstEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    @CanIgnoreReturnValue
    @Deprecated
    public final Map.Entry<K, V> pollLastEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int size() {
        return this.g.size();
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public K34<K, V> subMap(K k, K k2) {
        return subMap((boolean) k, true, (boolean) k2, false);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K34<K, V> subMap(K k, boolean z, K k2, boolean z2) {
        I14.l(k);
        I14.l(k2);
        I14.i(comparator().compare(k, k2) <= 0, "expected fromKey <= toKey but %s > %s", k, k2);
        return headMap((K34<K, V>) k2, z2).tailMap((K34<K, V>) k, z);
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public K34<K, V> tailMap(K k) {
        return tailMap((K34<K, V>) k, true);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K34<K, V> tailMap(K k, boolean z) {
        S44<K> s44 = this.f;
        I14.l(k);
        return f(s44.tailIndex(k, z), size());
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34, com.fossil.A34, java.util.SortedMap
    public U24<V> values() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.A34
    public Object writeReplace() {
        return new Ci(this);
    }
}
