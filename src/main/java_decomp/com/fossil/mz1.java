package com.fossil;

import com.fossil.Gz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Ai a(long j);

        @DexIgnore
        public abstract Ai b(Pz1 pz1);

        @DexIgnore
        public abstract Ai c(Integer num);

        @DexIgnore
        public abstract Mz1 d();

        @DexIgnore
        public abstract Ai e(long j);

        @DexIgnore
        public abstract Ai f(long j);
    }

    @DexIgnore
    public static Ai a(String str) {
        Gz1.Bi bi = new Gz1.Bi();
        bi.g(str);
        return bi;
    }

    @DexIgnore
    public static Ai b(byte[] bArr) {
        Gz1.Bi bi = new Gz1.Bi();
        bi.h(bArr);
        return bi;
    }

    @DexIgnore
    public abstract Integer c();

    @DexIgnore
    public abstract long d();

    @DexIgnore
    public abstract long e();

    @DexIgnore
    public abstract Pz1 f();

    @DexIgnore
    public abstract byte[] g();

    @DexIgnore
    public abstract String h();

    @DexIgnore
    public abstract long i();
}
