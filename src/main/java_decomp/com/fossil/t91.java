package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class T91 extends Exception {
    @DexIgnore
    public /* final */ J91 networkResponse;
    @DexIgnore
    public long networkTimeMs;

    @DexIgnore
    public T91() {
        this.networkResponse = null;
    }

    @DexIgnore
    public T91(J91 j91) {
        this.networkResponse = j91;
    }

    @DexIgnore
    public T91(String str) {
        super(str);
        this.networkResponse = null;
    }

    @DexIgnore
    public T91(String str, Throwable th) {
        super(str, th);
        this.networkResponse = null;
    }

    @DexIgnore
    public T91(Throwable th) {
        super(th);
        this.networkResponse = null;
    }

    @DexIgnore
    public long getNetworkTimeMs() {
        return this.networkTimeMs;
    }

    @DexIgnore
    public void setNetworkTimeMs(long j) {
        this.networkTimeMs = j;
    }
}
