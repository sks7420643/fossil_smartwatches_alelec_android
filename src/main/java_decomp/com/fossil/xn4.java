package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xn4 implements Ql4 {
    @DexIgnore
    public static Bm4 b(Go4 go4, int i, int i2, int i3) {
        Co4 a2 = go4.a();
        if (a2 != null) {
            int e = a2.e();
            int d = a2.d();
            int i4 = i3 << 1;
            int i5 = e + i4;
            int i6 = i4 + d;
            int max = Math.max(i, i5);
            int max2 = Math.max(i2, i6);
            int min = Math.min(max / i5, max2 / i6);
            int i7 = (max - (e * min)) / 2;
            Bm4 bm4 = new Bm4(max, max2);
            int i8 = 0;
            int i9 = (max2 - (d * min)) / 2;
            while (i8 < d) {
                int i10 = 0;
                int i11 = i7;
                while (i10 < e) {
                    if (a2.b(i10, i8) == 1) {
                        bm4.o(i11, i9, min, min);
                    }
                    i10++;
                    i11 += min;
                }
                i8++;
                i9 += min;
            }
            return bm4;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    @Override // com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (kl4 != Kl4.QR_CODE) {
            throw new IllegalArgumentException("Can only encode QR_CODE, but got " + kl4);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            Yn4 yn4 = Yn4.L;
            int i3 = 4;
            if (map != null) {
                if (map.containsKey(Ml4.ERROR_CORRECTION)) {
                    yn4 = Yn4.valueOf(map.get(Ml4.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(Ml4.MARGIN)) {
                    i3 = Integer.parseInt(map.get(Ml4.MARGIN).toString());
                }
            }
            return b(Do4.n(str, yn4, map), i, i2, i3);
        }
    }
}
