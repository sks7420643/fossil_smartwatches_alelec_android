package com.fossil;

import com.fossil.Jn5;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hl5 {
    @DexIgnore
    public static /* final */ Hl5 a; // = new Hl5();

    @DexIgnore
    public final List<Jn5.Ai> a(List<Oo5> list) {
        Wg6.c(list, "buttons");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Jn5.Ai e = a.e(it.next().a());
            if (e != null) {
                arrayList2.add(e);
            }
        }
        arrayList.addAll(arrayList2);
        return arrayList;
    }

    @DexIgnore
    public final Jn5.Ai b(String str) {
        Wg6.c(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode != -48173007) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                return Jn5.Ai.SET_COMPLICATION_WATCH_APP_WEATHER;
            }
        } else if (str.equals("chance-of-rain")) {
            return Jn5.Ai.SET_COMPLICATION_CHANCE_OF_RAIN;
        }
        return null;
    }

    @DexIgnore
    public final List<Jn5.Ai> c(HybridPreset hybridPreset) {
        Wg6.c(hybridPreset, "hybridPreset");
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = buttons.iterator();
        while (it.hasNext()) {
            Jn5.Ai d = a.d(it.next().getAppId());
            if (d != null) {
                arrayList.add(d);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Jn5.Ai d(String str) {
        Wg6.c(str, "microAppid");
        if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return Jn5.Ai.SET_MICRO_APP_MUSIC;
        }
        if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            return Jn5.Ai.SET_MICRO_APP_COMMUTE_TIME;
        }
        return null;
    }

    @DexIgnore
    public final Jn5.Ai e(String str) {
        Wg6.c(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode != 104263205) {
                if (hashCode == 1223440372 && str.equals("weather")) {
                    return Jn5.Ai.SET_COMPLICATION_WATCH_APP_WEATHER;
                }
            } else if (str.equals(Constants.MUSIC)) {
                return Jn5.Ai.SET_WATCH_APP_MUSIC;
            }
        } else if (str.equals("commute-time")) {
            return Jn5.Ai.SET_WATCH_APP_COMMUTE_TIME;
        }
        return null;
    }
}
