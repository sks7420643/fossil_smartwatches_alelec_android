package com.fossil;

import com.facebook.AccessToken;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uc4 implements Tc4 {
    @DexIgnore
    public static Wc4 b(JSONObject jSONObject, JSONObject jSONObject2) throws JSONException {
        String format;
        String string = jSONObject2.getString("status");
        boolean equals = "new".equals(string);
        String string2 = jSONObject.getString("bundle_id");
        String string3 = jSONObject.getString("org_id");
        if (equals) {
            format = "https://update.crashlytics.com/spi/v1/platforms/android/apps";
        } else {
            format = String.format(Locale.US, "https://update.crashlytics.com/spi/v1/platforms/android/apps/%s", string2);
        }
        return new Wc4(string, format, String.format(Locale.US, "https://reports.crashlytics.com/spi/v1/platforms/android/apps/%s/reports", string2), String.format(Locale.US, "https://reports.crashlytics.com/sdk-api/v1/platforms/android/apps/%s/minidumps", string2), string2, string3, jSONObject2.optBoolean("update_required", false), jSONObject2.optInt("report_upload_variant", 0), jSONObject2.optInt("native_report_upload_variant", 0));
    }

    @DexIgnore
    public static Xc4 c(JSONObject jSONObject) {
        return new Xc4(jSONObject.optBoolean("collect_reports", true));
    }

    @DexIgnore
    public static Yc4 d() {
        return new Yc4(8, 4);
    }

    @DexIgnore
    public static long e(B94 b94, long j, JSONObject jSONObject) {
        return jSONObject.has(AccessToken.EXPIRES_AT_KEY) ? jSONObject.optLong(AccessToken.EXPIRES_AT_KEY) : b94.a() + (1000 * j);
    }

    @DexIgnore
    @Override // com.fossil.Tc4
    public Ad4 a(B94 b94, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new Ad4(e(b94, (long) optInt2, jSONObject), b(jSONObject.getJSONObject("fabric"), jSONObject.getJSONObject("app")), d(), c(jSONObject.getJSONObject("features")), optInt, optInt2);
    }
}
