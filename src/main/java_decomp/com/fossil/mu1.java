package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Mu1 {
    OPEN("open"),
    CLOSE("close");
    
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Mu1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
