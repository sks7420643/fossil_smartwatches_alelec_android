package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U43 implements Xw2<T43> {
    @DexIgnore
    public static U43 c; // = new U43();
    @DexIgnore
    public /* final */ Xw2<T43> b;

    @DexIgnore
    public U43() {
        this(Ww2.b(new W43()));
    }

    @DexIgnore
    public U43(Xw2<T43> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((T43) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ T43 zza() {
        return this.b.zza();
    }
}
