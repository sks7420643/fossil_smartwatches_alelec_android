package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E58 {
    @DexIgnore
    public static /* final */ byte[] a; // = E48.a("0123456789abcdef");

    @DexIgnore
    public static final byte[] a() {
        return a;
    }

    @DexIgnore
    public static final String b(I48 i48, long j) {
        Wg6.c(i48, "$this$readUtf8Line");
        if (j > 0) {
            long j2 = j - 1;
            if (i48.M(j2) == ((byte) 13)) {
                String g0 = i48.g0(j2);
                i48.skip(2);
                return g0;
            }
        }
        String g02 = i48.g0(j);
        i48.skip(1);
        return g02;
    }
}
