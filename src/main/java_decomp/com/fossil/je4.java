package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Je4 {

    @DexIgnore
    public enum Ai {
        NONE(0),
        SDK(1),
        GLOBAL(2),
        COMBINED(3);
        
        @DexIgnore
        public /* final */ int code;

        @DexIgnore
        public Ai(int i) {
            this.code = i;
        }

        @DexIgnore
        public int getCode() {
            return this.code;
        }
    }

    @DexIgnore
    Ai a(String str);
}
