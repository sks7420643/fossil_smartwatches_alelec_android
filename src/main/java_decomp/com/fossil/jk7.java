package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jk7<T> implements Factory<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public Jk7(T t) {
        this.a = t;
    }

    @DexIgnore
    public static <T> Factory<T> a(T t) {
        Lk7.c(t, "instance cannot be null");
        return new Jk7(t);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public T get() {
        return this.a;
    }
}
