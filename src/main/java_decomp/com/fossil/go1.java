package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Go1 extends Bo1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Go1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Go1 createFromParcel(Parcel parcel) {
            return new Go1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Go1[] newArray(int i) {
            return new Go1[i];
        }
    }

    @DexIgnore
    public Go1(byte b, int i) {
        super(Do1.REPLY_MESSAGE);
        this.c = (byte) b;
        this.d = i;
    }

    @DexIgnore
    public /* synthetic */ Go1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readInt();
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.c;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Bo1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.k, Byte.valueOf(this.c)), Jd0.j, Integer.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.Bo1
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeByte(this.c);
        parcel.writeInt(this.d);
    }
}
