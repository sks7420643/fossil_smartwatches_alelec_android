package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B46 implements Factory<HomeAlertsHybridPresenter> {
    @DexIgnore
    public static HomeAlertsHybridPresenter a(Y36 y36, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, An4 an4) {
        return new HomeAlertsHybridPresenter(y36, alarmHelper, setAlarms, alarmsRepository, an4);
    }
}
