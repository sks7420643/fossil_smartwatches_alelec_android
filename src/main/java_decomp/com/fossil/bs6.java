package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bs6 implements Factory<CustomizeBackgroundViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Bs6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Bs6 a(Provider<ThemeRepository> provider) {
        return new Bs6(provider);
    }

    @DexIgnore
    public static CustomizeBackgroundViewModel c(ThemeRepository themeRepository) {
        return new CustomizeBackgroundViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeBackgroundViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
