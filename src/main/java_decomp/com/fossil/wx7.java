package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wx7 {
    @DexIgnore
    public static /* final */ ThreadLocal<Hw7> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ Wx7 b; // = new Wx7();

    @DexIgnore
    public final Hw7 a() {
        return a.get();
    }

    @DexIgnore
    public final Hw7 b() {
        Hw7 hw7 = a.get();
        if (hw7 != null) {
            return hw7;
        }
        Hw7 a2 = Kw7.a();
        a.set(a2);
        return a2;
    }

    @DexIgnore
    public final void c() {
        a.set(null);
    }

    @DexIgnore
    public final void d(Hw7 hw7) {
        a.set(hw7);
    }
}
