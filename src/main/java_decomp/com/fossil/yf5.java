package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.portfolio.platform.view.FlexibleProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yf5 {
    @DexIgnore
    public /* final */ RelativeLayout a;
    @DexIgnore
    public /* final */ FlexibleProgressBar b;
    @DexIgnore
    public /* final */ ImageView c;

    @DexIgnore
    public Yf5(RelativeLayout relativeLayout, FlexibleProgressBar flexibleProgressBar, ImageView imageView) {
        this.a = relativeLayout;
        this.b = flexibleProgressBar;
        this.c = imageView;
    }

    @DexIgnore
    public static Yf5 a(View view) {
        int i;
        FlexibleProgressBar flexibleProgressBar = (FlexibleProgressBar) view.findViewById(2131362970);
        if (flexibleProgressBar != null) {
            ImageView imageView = (ImageView) view.findViewById(2131363150);
            if (imageView != null) {
                return new Yf5((RelativeLayout) view, flexibleProgressBar, imageView);
            }
            i = 2131363150;
        } else {
            i = 2131362970;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static Yf5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558719, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public RelativeLayout b() {
        return this.a;
    }
}
