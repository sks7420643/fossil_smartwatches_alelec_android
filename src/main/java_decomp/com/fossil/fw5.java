package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public /* final */ Category a;
    @DexIgnore
    public int b;
    @DexIgnore
    public ArrayList<Category> c;
    @DexIgnore
    public Bi d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public /* final */ /* synthetic */ Fw5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.b.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CategoriesAdapter", "onItemClick pos=" + this.b.getAdapterPosition() + " currentPos=" + this.b.b.b);
                    Ai ai = this.b;
                    ai.b.b = ai.getAdapterPosition();
                    if (this.b.b.b != 0) {
                        Bi k = this.b.b.k();
                        if (k != null) {
                            Object obj = this.b.b.c.get(this.b.b.b);
                            Wg6.b(obj, "mData[mSelectedIndex]");
                            k.a((Category) obj);
                            return;
                        }
                        return;
                    }
                    Bi k2 = this.b.b.k();
                    if (k2 != null) {
                        k2.b();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Fw5 fw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.b = fw5;
            View findViewById = view.findViewById(2131362379);
            Wg6.b(findViewById, "view.findViewById(R.id.ftv_category)");
            FlexibleButton flexibleButton = (FlexibleButton) findViewById;
            this.a = flexibleButton;
            flexibleButton.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(Category category, int i) {
            Wg6.c(category, "category");
            Context context = this.a.getContext();
            FlexibleButton flexibleButton = this.a;
            flexibleButton.setCompoundDrawablePadding((int) P47.a(6, flexibleButton.getContext()));
            if (TextUtils.isEmpty(category.getId())) {
                FlexibleButton flexibleButton2 = this.a;
                Wg6.b(context, "context");
                flexibleButton2.setText(context.getResources().getString(2131886996));
            } else {
                String d = Um5.d(PortfolioApp.get.instance(), category.getName(), category.getEnglishName());
                if (i == 0) {
                    this.a.setText(d);
                } else {
                    Wg6.b(d, "name");
                    if (d != null) {
                        String upperCase = d.toUpperCase();
                        Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                        this.a.setText(upperCase);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
            if (i == 0) {
                this.a.d("flexible_button_search");
                Drawable f = W6.f(context, 2131231050);
                if (f != null) {
                    f.setTintList(ColorStateList.valueOf(W6.d(context, 2131099967)));
                }
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds(f, (Drawable) null, (Drawable) null, (Drawable) null);
            } else if (i == this.b.b) {
                this.a.setSelected(true);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.d("flexible_button_primary");
            } else {
                this.a.setSelected(false);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.d("flexible_button_secondary");
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(Category category);

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore
    public Fw5(ArrayList<Category> arrayList, Bi bi) {
        Wg6.c(arrayList, "mData");
        this.c = arrayList;
        this.d = bi;
        this.a = new Category("Search", "Search", "Customization_Complications_Elements_Input__Search", "", "", -1);
        this.b = -1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Fw5(ArrayList arrayList, Bi bi, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bi);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final int j(String str) {
        T t;
        Wg6.c(str, "category");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final Bi k() {
        return this.d;
    }

    @DexIgnore
    public void l(Ai ai, int i) {
        Wg6.c(ai, "holder");
        if (getItemCount() > i && i != -1) {
            Category category = this.c.get(i);
            Wg6.b(category, "mData[position]");
            ai.a(category, i);
        }
    }

    @DexIgnore
    public Ai m(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558667, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026_category, parent, false)");
        return new Ai(this, inflate);
    }

    @DexIgnore
    public final void n(List<Category> list) {
        Wg6.c(list, "data");
        this.c.clear();
        this.c.addAll(list);
        this.c.add(0, this.a);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(Bi bi) {
        Wg6.c(bi, "listener");
        this.d = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        l(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return m(viewGroup, i);
    }

    @DexIgnore
    public final void p(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CategoriesAdapter", "setSelectedCategory pos=" + i);
        if (i < getItemCount()) {
            this.b = i;
            notifyDataSetChanged();
        }
    }
}
