package com.fossil;

import com.fossil.iq4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu5 extends iq4<a, c, b> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f659a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, "deviceId");
            this.f659a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f659a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ DeviceLocation f660a;

        @DexIgnore
        public c(DeviceLocation deviceLocation) {
            pq7.c(deviceLocation, "deviceLocation");
            this.f660a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.f660a;
        }
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GetLocation";
    }

    @DexIgnore
    /* renamed from: m */
    public Object k(a aVar, qn7<Object> qn7) {
        FLogger.INSTANCE.getLocal().d("GetLocation", "running UseCase");
        DeviceLocation deviceLocation = mn5.p.a().i().getDeviceLocation(aVar != null ? aVar.a() : null);
        return deviceLocation != null ? new c(deviceLocation) : new b();
    }
}
