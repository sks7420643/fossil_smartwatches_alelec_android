package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J42 extends Q62<GoogleSignInOptions> {
    @DexIgnore
    public static int j; // = Ai.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Ai {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] e; // = {1, 2, 3, 4};

        @DexIgnore
        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    @DexIgnore
    public J42(Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, D42.e, googleSignInOptions, (U72) new F72());
    }

    @DexIgnore
    public J42(Context context, GoogleSignInOptions googleSignInOptions) {
        super(context, D42.e, googleSignInOptions, new F72());
    }

    @DexIgnore
    public Intent s() {
        Context k = k();
        int i = P52.a[u() - 1];
        return i != 1 ? i != 2 ? V42.g(k, (GoogleSignInOptions) j()) : V42.b(k, (GoogleSignInOptions) j()) : V42.e(k, (GoogleSignInOptions) j());
    }

    @DexIgnore
    public Nt3<Void> t() {
        return Qc2.c(V42.c(b(), k(), u() == Ai.c));
    }

    @DexIgnore
    public final int u() {
        int i;
        synchronized (this) {
            if (j == Ai.a) {
                Context k = k();
                C62 q = C62.q();
                int j2 = q.j(k, H62.a);
                if (j2 == 0) {
                    j = Ai.d;
                } else if (q.d(k, j2, null) != null || DynamiteModule.a(k, "com.google.android.gms.auth.api.fallback") == 0) {
                    j = Ai.b;
                } else {
                    j = Ai.c;
                }
            }
            i = j;
        }
        return i;
    }
}
