package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.helper.AnalyticsHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vl5 extends Ul5 {
    @DexIgnore
    public String i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Vl5(AnalyticsHelper analyticsHelper, String str, String str2) {
        super(analyticsHelper, str, str2);
        Wg6.c(analyticsHelper, "analyticsHelper");
        Wg6.c(str, "traceName");
        J68.b(Wg6.a(str, "view_appearance"), "traceName should be view_appearance", new Object[0]);
    }

    @DexIgnore
    public final String j() {
        return this.i;
    }

    @DexIgnore
    public final boolean k(Vl5 vl5) {
        if (vl5 == null) {
            return false;
        }
        return Wg6.a(vl5.i, this.i);
    }

    @DexIgnore
    public final Vl5 l(String str) {
        Wg6.c(str, "viewName");
        this.i = str;
        b("view_name", str);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Ul5
    public String toString() {
        return "View name: " + this.i + ", running: " + f();
    }
}
