package com.fossil;

import com.fossil.E13;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R03 extends S03<E13.Ei> {
    @DexIgnore
    @Override // com.fossil.S03
    public final int a(Map.Entry<?, ?> entry) {
        E13.Ei ei = (E13.Ei) entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.S03
    public final T03<E13.Ei> b(Object obj) {
        return ((E13.Bi) obj).zzc;
    }

    @DexIgnore
    @Override // com.fossil.S03
    public final Object c(Q03 q03, M23 m23, int i) {
        return q03.b(m23, i);
    }

    @DexIgnore
    @Override // com.fossil.S03
    public final void d(R43 r43, Map.Entry<?, ?> entry) throws IOException {
        E13.Ei ei = (E13.Ei) entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.S03
    public final boolean e(M23 m23) {
        return m23 instanceof E13.Bi;
    }

    @DexIgnore
    @Override // com.fossil.S03
    public final T03<E13.Ei> f(Object obj) {
        return ((E13.Bi) obj).C();
    }

    @DexIgnore
    @Override // com.fossil.S03
    public final void g(Object obj) {
        b(obj).k();
    }
}
