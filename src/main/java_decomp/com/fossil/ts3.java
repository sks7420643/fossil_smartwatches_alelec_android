package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ts3 implements Parcelable.Creator<Us3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Us3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        Tc2 tc2 = null;
        Z52 z52 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i = Ad2.v(parcel, t);
            } else if (l == 2) {
                z52 = (Z52) Ad2.e(parcel, t, Z52.CREATOR);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                tc2 = (Tc2) Ad2.e(parcel, t, Tc2.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Us3(i, z52, tc2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Us3[] newArray(int i) {
        return new Us3[i];
    }
}
