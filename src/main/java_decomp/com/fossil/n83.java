package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N83 implements Xw2<Q83> {
    @DexIgnore
    public static N83 c; // = new N83();
    @DexIgnore
    public /* final */ Xw2<Q83> b;

    @DexIgnore
    public N83() {
        this(Ww2.b(new P83()));
    }

    @DexIgnore
    public N83(Xw2<Q83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((Q83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ Q83 zza() {
        return this.b.zza();
    }
}
