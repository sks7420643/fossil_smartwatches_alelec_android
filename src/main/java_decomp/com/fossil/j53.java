package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J53 implements K53 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.service.configurable_service_limits", true);
        b = hw2.d("measurement.client.configurable_service_limits", true);
        hw2.b("measurement.id.service.configurable_service_limits", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.K53
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.K53
    public final boolean zzb() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.K53
    public final boolean zzc() {
        return b.o().booleanValue();
    }
}
