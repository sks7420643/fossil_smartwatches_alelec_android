package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sz3 extends Drawable implements J04, Bm0 {
    @DexIgnore
    public Bi b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Drawable.ConstantState {
        @DexIgnore
        public C04 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Bi(C04 c04) {
            this.a = c04;
            this.b = false;
        }

        @DexIgnore
        public Bi(Bi bi) {
            this.a = (C04) bi.a.getConstantState().newDrawable();
            this.b = bi.b;
        }

        @DexIgnore
        public Sz3 a() {
            return new Sz3(new Bi(this));
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Drawable newDrawable() {
            return a();
        }
    }

    @DexIgnore
    public Sz3(G04 g04) {
        this(new Bi(new C04(g04)));
    }

    @DexIgnore
    public Sz3(Bi bi) {
        this.b = bi;
    }

    @DexIgnore
    public Sz3 a() {
        this.b = new Bi(this.b);
        return this;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Bi bi = this.b;
        if (bi.b) {
            bi.a.draw(canvas);
        }
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    @DexIgnore
    public int getOpacity() {
        return this.b.a.getOpacity();
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Drawable mutate() {
        a();
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.b.a.setBounds(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        if (this.b.a.setState(iArr)) {
            onStateChange = true;
        }
        boolean e = Tz3.e(iArr);
        Bi bi = this.b;
        if (bi.b == e) {
            return onStateChange;
        }
        bi.b = e;
        return true;
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.b.a.setAlpha(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.a.setColorFilter(colorFilter);
    }

    @DexIgnore
    @Override // com.fossil.J04
    public void setShapeAppearanceModel(G04 g04) {
        this.b.a.setShapeAppearanceModel(g04);
    }

    @DexIgnore
    @Override // com.fossil.Bm0
    public void setTint(int i) {
        this.b.a.setTint(i);
    }

    @DexIgnore
    @Override // com.fossil.Bm0
    public void setTintList(ColorStateList colorStateList) {
        this.b.a.setTintList(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.Bm0
    public void setTintMode(PorterDuff.Mode mode) {
        this.b.a.setTintMode(mode);
    }
}
