package com.fossil;

import android.webkit.MimeTypeMap;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.A18;
import com.fossil.E61;
import com.fossil.Z08;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class G61<T> implements E61<T> {
    @DexIgnore
    public static /* final */ Z08 b;
    @DexIgnore
    public static /* final */ Z08 c;
    @DexIgnore
    public /* final */ A18.Ai a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "coil.fetch.HttpFetcher", f = "HttpFetcher.kt", l = {106}, m = "fetch$suspendImpl")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ G61 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(G61 g61, Xe6 xe6) {
            super(xe6);
            this.this$0 = g61;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return G61.d(this.this$0, null, null, null, null, this);
        }
    }

    /*
    static {
        Z08.Ai ai = new Z08.Ai();
        ai.c();
        ai.d();
        b = ai.a();
        Z08.Ai ai2 = new Z08.Ai();
        ai2.c();
        ai2.e();
        c = ai2.a();
    }
    */

    @DexIgnore
    public G61(A18.Ai ai) {
        Wg6.c(ai, "callFactory");
        this.a = ai;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object d(com.fossil.G61 r9, com.fossil.G51 r10, java.lang.Object r11, com.fossil.F81 r12, com.fossil.X51 r13, com.mapped.Xe6 r14) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.G61.d(com.fossil.G61, com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.E61
    public boolean a(T t) {
        Wg6.c(t, "data");
        return E61.Ai.a(this, t);
    }

    @DexIgnore
    @Override // com.fossil.E61
    public Object c(G51 g51, T t, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        return d(this, g51, t, f81, x51, xe6);
    }

    @DexIgnore
    public final String e(Q18 q18, W18 w18) {
        R18 contentType = w18.contentType();
        String r18 = contentType != null ? contentType.toString() : null;
        if (r18 != null && !Vt7.s(r18, "text/plain", false, 2, null)) {
            return r18;
        }
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        Wg6.b(singleton, "MimeTypeMap.getSingleton()");
        String g = W81.g(singleton, q18.toString());
        return g != null ? g : r18;
    }

    @DexIgnore
    public abstract Q18 f(T t);
}
