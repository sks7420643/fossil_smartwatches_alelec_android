package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleButton C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ LinearLayout E;
    @DexIgnore
    public /* final */ LinearLayout F;
    @DexIgnore
    public /* final */ LinearLayout G;
    @DexIgnore
    public /* final */ LinearLayout H;
    @DexIgnore
    public /* final */ LinearLayout I;
    @DexIgnore
    public /* final */ ProgressBar J;
    @DexIgnore
    public /* final */ ConstraintLayout K;
    @DexIgnore
    public /* final */ ViewPager2 L;
    @DexIgnore
    public /* final */ TabLayout M;
    @DexIgnore
    public /* final */ FlexibleTextView N;
    @DexIgnore
    public /* final */ FlexibleTextView O;
    @DexIgnore
    public /* final */ FlexibleTextView P;
    @DexIgnore
    public /* final */ FlexibleTextView Q;
    @DexIgnore
    public /* final */ FlexibleTextView R;
    @DexIgnore
    public /* final */ FlexibleTextView S;
    @DexIgnore
    public /* final */ FlexibleTextView T;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ CustomizeWidget v;
    @DexIgnore
    public /* final */ CustomizeWidget w;
    @DexIgnore
    public /* final */ CustomizeWidget x;
    @DexIgnore
    public /* final */ CustomizeWidget y;
    @DexIgnore
    public /* final */ CustomizeWidget z;

    @DexIgnore
    public X75(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4, CustomizeWidget customizeWidget5, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, LinearLayout linearLayout4, LinearLayout linearLayout5, ProgressBar progressBar, ConstraintLayout constraintLayout5, ViewPager2 viewPager2, TabLayout tabLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9) {
        super(obj, view, i);
        this.q = view2;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = constraintLayout4;
        this.v = customizeWidget;
        this.w = customizeWidget2;
        this.x = customizeWidget3;
        this.y = customizeWidget4;
        this.z = customizeWidget5;
        this.A = flexibleTextView;
        this.B = flexibleTextView2;
        this.C = flexibleButton;
        this.D = rTLImageView;
        this.E = linearLayout;
        this.F = linearLayout2;
        this.G = linearLayout3;
        this.H = linearLayout4;
        this.I = linearLayout5;
        this.J = progressBar;
        this.K = constraintLayout5;
        this.L = viewPager2;
        this.M = tabLayout;
        this.N = flexibleTextView3;
        this.O = flexibleTextView4;
        this.P = flexibleTextView5;
        this.Q = flexibleTextView6;
        this.R = flexibleTextView7;
        this.S = flexibleTextView8;
        this.T = flexibleTextView9;
    }
}
