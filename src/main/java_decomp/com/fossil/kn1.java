package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kn1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ WorkoutType b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ In1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Kn1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Kn1 a(byte[] bArr) throws IllegalArgumentException {
            boolean z = true;
            if (bArr.length >= 6) {
                WorkoutType d = G80.d(bArr[0]);
                boolean z2 = ((bArr[1] >> 0) & 1) == 1;
                if (((bArr[1] >> 1) & 1) != 1) {
                    z = false;
                }
                return new Kn1(d, z2, z, new In1(Hy1.p(bArr[2]), Hy1.p(bArr[3]), Hy1.p(bArr[4]), Hy1.p(bArr[5])));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 6"));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kn1 createFromParcel(Parcel parcel) {
            return new Kn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kn1[] newArray(int i) {
            return new Kn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Kn1(Parcel parcel, Qg6 qg6) {
        boolean z = true;
        this.b = G80.d(parcel.readByte());
        this.c = parcel.readInt() == 1;
        this.d = parcel.readInt() != 1 ? false : z;
        Parcelable readParcelable = parcel.readParcelable(In1.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (In1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Kn1(WorkoutType workoutType, boolean z, boolean z2, In1 in1) {
        this.b = workoutType;
        this.c = z;
        this.d = z2;
        this.e = in1;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put((byte) this.b.getValue()).put((byte) (((this.c ? 1 : 0) << 0) | 0 | ((this.d ? 1 : 0) << 1))).put((byte) this.e.getStartLatencyInMinute()).put((byte) this.e.getPauseLatencyInMinute()).put((byte) this.e.getResumeLatencyInMinute()).put((byte) this.e.getStopLatencyInMinute()).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Kn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Kn1 kn1 = (Kn1) obj;
            if (this.b != kn1.b) {
                return false;
            }
            if (this.c != kn1.c) {
                return false;
            }
            if (this.d != kn1.d) {
                return false;
            }
            return !(Wg6.a(this.e, kn1.e) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.AutoWorkoutDetectionItemConfig");
    }

    @DexIgnore
    public final In1 getActivityDetectionLatency() {
        return this.e;
    }

    @DexIgnore
    public final boolean getEnableAutoDetection() {
        return this.c;
    }

    @DexIgnore
    public final boolean getEnablePrompt() {
        return this.d;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = Boolean.valueOf(this.c).hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Boolean.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("workout_type", this.b.name());
            jSONObject.put("enable_detection", this.c);
            jSONObject.put("enable_prompt", this.d);
            jSONObject.put("activity_detection_latency", this.e);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte((byte) this.b.getValue());
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
