package com.fossil;

import com.fossil.E13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dv2 extends E13<Dv2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Dv2 zzf;
    @DexIgnore
    public static volatile Z23<Dv2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public J13 zze; // = E13.z();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Dv2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Dv2.zzf);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Dv2) this.c).J(i);
            return this;
        }

        @DexIgnore
        public final Ai y(Iterable<? extends Long> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Dv2) this.c).G(iterable);
            return this;
        }
    }

    /*
    static {
        Dv2 dv2 = new Dv2();
        zzf = dv2;
        E13.u(Dv2.class, dv2);
    }
    */

    @DexIgnore
    public static Ai M() {
        return (Ai) zzf.w();
    }

    @DexIgnore
    public final long C(int i) {
        return this.zze.zzb(i);
    }

    @DexIgnore
    public final void G(Iterable<? extends Long> iterable) {
        J13 j13 = this.zze;
        if (!j13.zza()) {
            this.zze = E13.p(j13);
        }
        Nz2.a(iterable, this.zze);
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int I() {
        return this.zzd;
    }

    @DexIgnore
    public final void J(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final List<Long> K() {
        return this.zze;
    }

    @DexIgnore
    public final int L() {
        return this.zze.size();
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Dv2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u1004\u0000\u0002\u0014", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                Z23<Dv2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Dv2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
