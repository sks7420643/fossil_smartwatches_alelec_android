package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A21 implements K11 {
    @DexIgnore
    public static /* final */ String l; // = X01.f("SystemAlarmDispatcher");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ K41 c;
    @DexIgnore
    public /* final */ G41 d;
    @DexIgnore
    public /* final */ M11 e;
    @DexIgnore
    public /* final */ S11 f;
    @DexIgnore
    public /* final */ X11 g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ List<Intent> i;
    @DexIgnore
    public Intent j;
    @DexIgnore
    public Ci k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            A21 a21;
            Di di;
            synchronized (A21.this.i) {
                A21.this.j = A21.this.i.get(0);
            }
            Intent intent = A21.this.j;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = A21.this.j.getIntExtra("KEY_START_ID", 0);
                X01.c().a(A21.l, String.format("Processing command %s, %s", A21.this.j, Integer.valueOf(intExtra)), new Throwable[0]);
                PowerManager.WakeLock b2 = D41.b(A21.this.b, String.format("%s (%s)", action, Integer.valueOf(intExtra)));
                try {
                    X01.c().a(A21.l, String.format("Acquiring operation wake lock (%s) %s", action, b2), new Throwable[0]);
                    b2.acquire();
                    A21.this.g.p(A21.this.j, intExtra, A21.this);
                    X01.c().a(A21.l, String.format("Releasing operation wake lock (%s) %s", action, b2), new Throwable[0]);
                    b2.release();
                    a21 = A21.this;
                    di = new Di(a21);
                } catch (Throwable th) {
                    X01.c().a(A21.l, String.format("Releasing operation wake lock (%s) %s", action, b2), new Throwable[0]);
                    b2.release();
                    A21 a212 = A21.this;
                    a212.k(new Di(a212));
                    throw th;
                }
                a21.k(di);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Runnable {
        @DexIgnore
        public /* final */ A21 b;
        @DexIgnore
        public /* final */ Intent c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public Bi(A21 a21, Intent intent, int i) {
            this.b = a21;
            this.c = intent;
            this.d = i;
        }

        @DexIgnore
        public void run() {
            this.b.a(this.c, this.d);
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements Runnable {
        @DexIgnore
        public /* final */ A21 b;

        @DexIgnore
        public Di(A21 a21) {
            this.b = a21;
        }

        @DexIgnore
        public void run() {
            this.b.c();
        }
    }

    @DexIgnore
    public A21(Context context) {
        this(context, null, null);
    }

    @DexIgnore
    public A21(Context context, M11 m11, S11 s11) {
        this.b = context.getApplicationContext();
        this.g = new X11(this.b);
        this.d = new G41();
        s11 = s11 == null ? S11.l(context) : s11;
        this.f = s11;
        this.e = m11 == null ? s11.n() : m11;
        this.c = this.f.q();
        this.e.b(this);
        this.i = new ArrayList();
        this.j = null;
        this.h = new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public boolean a(Intent intent, int i2) {
        boolean z = false;
        X01.c().a(l, String.format("Adding command %s (%s)", intent, Integer.valueOf(i2)), new Throwable[0]);
        b();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            X01.c().h(l, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && i("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i2);
            synchronized (this.i) {
                if (!this.i.isEmpty()) {
                    z = true;
                }
                this.i.add(intent);
                if (!z) {
                    l();
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final void b() {
        if (this.h.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }

    @DexIgnore
    public void c() {
        X01.c().a(l, "Checking if commands are complete.", new Throwable[0]);
        b();
        synchronized (this.i) {
            if (this.j != null) {
                X01.c().a(l, String.format("Removing command %s", this.j), new Throwable[0]);
                if (this.i.remove(0).equals(this.j)) {
                    this.j = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            A41 c2 = this.c.c();
            if (!this.g.o() && this.i.isEmpty() && !c2.a()) {
                X01.c().a(l, "No more commands & intents.", new Throwable[0]);
                if (this.k != null) {
                    this.k.a();
                }
            } else if (!this.i.isEmpty()) {
                l();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.K11
    public void d(String str, boolean z) {
        k(new Bi(this, X11.c(this.b, str, z), 0));
    }

    @DexIgnore
    public M11 e() {
        return this.e;
    }

    @DexIgnore
    public K41 f() {
        return this.c;
    }

    @DexIgnore
    public S11 g() {
        return this.f;
    }

    @DexIgnore
    public G41 h() {
        return this.d;
    }

    @DexIgnore
    public final boolean i(String str) {
        b();
        synchronized (this.i) {
            for (Intent intent : this.i) {
                if (str.equals(intent.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void j() {
        X01.c().a(l, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.e.h(this);
        this.d.a();
        this.k = null;
    }

    @DexIgnore
    public void k(Runnable runnable) {
        this.h.post(runnable);
    }

    @DexIgnore
    public final void l() {
        b();
        PowerManager.WakeLock b2 = D41.b(this.b, "ProcessCommand");
        try {
            b2.acquire();
            this.f.q().b(new Ai());
        } finally {
            b2.release();
        }
    }

    @DexIgnore
    public void m(Ci ci) {
        if (this.k != null) {
            X01.c().b(l, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.k = ci;
        }
    }
}
