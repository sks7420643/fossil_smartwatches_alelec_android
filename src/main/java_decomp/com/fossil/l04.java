package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.internal.FacebookWebFallbackDialog;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L04 {
    @DexIgnore
    public static L04 e;
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new Ai());
    @DexIgnore
    public Ci c;
    @DexIgnore
    public Ci d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Handler.Callback {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            L04.this.d((Ci) message.obj);
            return true;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(int i);

        @DexIgnore
        Object show();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ WeakReference<Bi> a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public Ci(int i, Bi bi) {
            this.a = new WeakReference<>(bi);
            this.b = i;
        }

        @DexIgnore
        public boolean a(Bi bi) {
            return bi != null && this.a.get() == bi;
        }
    }

    @DexIgnore
    public static L04 c() {
        if (e == null) {
            e = new L04();
        }
        return e;
    }

    @DexIgnore
    public final boolean a(Ci ci, int i) {
        Bi bi = ci.a.get();
        if (bi == null) {
            return false;
        }
        this.b.removeCallbacksAndMessages(ci);
        bi.a(i);
        return true;
    }

    @DexIgnore
    public void b(Bi bi, int i) {
        synchronized (this.a) {
            if (f(bi)) {
                a(this.c, i);
            } else if (g(bi)) {
                a(this.d, i);
            }
        }
    }

    @DexIgnore
    public void d(Ci ci) {
        synchronized (this.a) {
            if (this.c == ci || this.d == ci) {
                a(ci, 2);
            }
        }
    }

    @DexIgnore
    public boolean e(Bi bi) {
        boolean z;
        synchronized (this.a) {
            z = f(bi) || g(bi);
        }
        return z;
    }

    @DexIgnore
    public final boolean f(Bi bi) {
        Ci ci = this.c;
        return ci != null && ci.a(bi);
    }

    @DexIgnore
    public final boolean g(Bi bi) {
        Ci ci = this.d;
        return ci != null && ci.a(bi);
    }

    @DexIgnore
    public void h(Bi bi) {
        synchronized (this.a) {
            if (f(bi)) {
                this.c = null;
                if (this.d != null) {
                    n();
                }
            }
        }
    }

    @DexIgnore
    public void i(Bi bi) {
        synchronized (this.a) {
            if (f(bi)) {
                l(this.c);
            }
        }
    }

    @DexIgnore
    public void j(Bi bi) {
        synchronized (this.a) {
            if (f(bi) && !this.c.c) {
                this.c.c = true;
                this.b.removeCallbacksAndMessages(this.c);
            }
        }
    }

    @DexIgnore
    public void k(Bi bi) {
        synchronized (this.a) {
            if (f(bi) && this.c.c) {
                this.c.c = false;
                l(this.c);
            }
        }
    }

    @DexIgnore
    public final void l(Ci ci) {
        int i = ci.b;
        if (i != -2) {
            if (i <= 0) {
                i = i == -1 ? FacebookWebFallbackDialog.OS_BACK_BUTTON_RESPONSE_TIMEOUT_MILLISECONDS : 2750;
            }
            this.b.removeCallbacksAndMessages(ci);
            Handler handler = this.b;
            handler.sendMessageDelayed(Message.obtain(handler, 0, ci), (long) i);
        }
    }

    @DexIgnore
    public void m(int i, Bi bi) {
        synchronized (this.a) {
            if (f(bi)) {
                this.c.b = i;
                this.b.removeCallbacksAndMessages(this.c);
                l(this.c);
                return;
            }
            if (g(bi)) {
                this.d.b = i;
            } else {
                this.d = new Ci(i, bi);
            }
            if (this.c == null || !a(this.c, 4)) {
                this.c = null;
                n();
            }
        }
    }

    @DexIgnore
    public final void n() {
        Ci ci = this.d;
        if (ci != null) {
            this.c = ci;
            this.d = null;
            Bi bi = ci.a.get();
            if (bi != null) {
                bi.show();
            } else {
                this.c = null;
            }
        }
    }
}
