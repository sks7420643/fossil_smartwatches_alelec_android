package com.fossil;

import android.animation.TimeInterpolator;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Mz3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Az3 {
    @DexIgnore
    public static /* final */ boolean V; // = (Build.VERSION.SDK_INT < 18);
    @DexIgnore
    public static /* final */ Paint W; // = null;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public Bitmap B;
    @DexIgnore
    public Paint C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public int[] H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public /* final */ TextPaint J;
    @DexIgnore
    public /* final */ TextPaint K;
    @DexIgnore
    public TimeInterpolator L;
    @DexIgnore
    public TimeInterpolator M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public ColorStateList Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public float T;
    @DexIgnore
    public ColorStateList U;
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public float c;
    @DexIgnore
    public /* final */ Rect d;
    @DexIgnore
    public /* final */ Rect e;
    @DexIgnore
    public /* final */ RectF f;
    @DexIgnore
    public int g; // = 16;
    @DexIgnore
    public int h; // = 16;
    @DexIgnore
    public float i; // = 15.0f;
    @DexIgnore
    public float j; // = 15.0f;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public Typeface s;
    @DexIgnore
    public Typeface t;
    @DexIgnore
    public Typeface u;
    @DexIgnore
    public Mz3 v;
    @DexIgnore
    public Mz3 w;
    @DexIgnore
    public CharSequence x;
    @DexIgnore
    public CharSequence y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Mz3.Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Mz3.Ai
        public void a(Typeface typeface) {
            Az3.this.L(typeface);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Mz3.Ai {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.Mz3.Ai
        public void a(Typeface typeface) {
            Az3.this.T(typeface);
        }
    }

    @DexIgnore
    public Az3(View view) {
        this.a = view;
        this.J = new TextPaint(129);
        this.K = new TextPaint(this.J);
        this.e = new Rect();
        this.d = new Rect();
        this.f = new RectF();
    }

    @DexIgnore
    public static boolean A(float f2, float f3) {
        return Math.abs(f2 - f3) < 0.001f;
    }

    @DexIgnore
    public static float C(float f2, float f3, float f4, TimeInterpolator timeInterpolator) {
        if (timeInterpolator != null) {
            f4 = timeInterpolator.getInterpolation(f4);
        }
        return Uw3.a(f2, f3, f4);
    }

    @DexIgnore
    public static boolean F(Rect rect, int i2, int i3, int i4, int i5) {
        return rect.left == i2 && rect.top == i3 && rect.right == i4 && rect.bottom == i5;
    }

    @DexIgnore
    public static int a(int i2, int i3, float f2) {
        float f3 = 1.0f - f2;
        return Color.argb((int) ((((float) Color.alpha(i2)) * f3) + (((float) Color.alpha(i3)) * f2)), (int) ((((float) Color.red(i2)) * f3) + (((float) Color.red(i3)) * f2)), (int) ((((float) Color.green(i2)) * f3) + (((float) Color.green(i3)) * f2)), (int) ((f3 * ((float) Color.blue(i2))) + (((float) Color.blue(i3)) * f2)));
    }

    @DexIgnore
    public final boolean B() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2 = this.l;
        return (colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList = this.k) != null && colorStateList.isStateful());
    }

    @DexIgnore
    public void D() {
        this.b = this.e.width() > 0 && this.e.height() > 0 && this.d.width() > 0 && this.d.height() > 0;
    }

    @DexIgnore
    public void E() {
        if (this.a.getHeight() > 0 && this.a.getWidth() > 0) {
            b();
            d();
        }
    }

    @DexIgnore
    public void G(int i2, int i3, int i4, int i5) {
        if (!F(this.e, i2, i3, i4, i5)) {
            this.e.set(i2, i3, i4, i5);
            this.I = true;
            D();
        }
    }

    @DexIgnore
    public void H(Rect rect) {
        G(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public void I(int i2) {
        Pz3 pz3 = new Pz3(this.a.getContext(), i2);
        ColorStateList colorStateList = pz3.b;
        if (colorStateList != null) {
            this.l = colorStateList;
        }
        float f2 = pz3.a;
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.j = f2;
        }
        ColorStateList colorStateList2 = pz3.h;
        if (colorStateList2 != null) {
            this.Q = colorStateList2;
        }
        this.O = pz3.i;
        this.P = pz3.j;
        this.N = pz3.k;
        Mz3 mz3 = this.w;
        if (mz3 != null) {
            mz3.c();
        }
        this.w = new Mz3(new Ai(), pz3.e());
        pz3.h(this.a.getContext(), this.w);
        E();
    }

    @DexIgnore
    public void J(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            E();
        }
    }

    @DexIgnore
    public void K(int i2) {
        if (this.h != i2) {
            this.h = i2;
            E();
        }
    }

    @DexIgnore
    public void L(Typeface typeface) {
        if (M(typeface)) {
            E();
        }
    }

    @DexIgnore
    public final boolean M(Typeface typeface) {
        Mz3 mz3 = this.w;
        if (mz3 != null) {
            mz3.c();
        }
        if (this.s == typeface) {
            return false;
        }
        this.s = typeface;
        return true;
    }

    @DexIgnore
    public void N(int i2, int i3, int i4, int i5) {
        if (!F(this.d, i2, i3, i4, i5)) {
            this.d.set(i2, i3, i4, i5);
            this.I = true;
            D();
        }
    }

    @DexIgnore
    public void O(Rect rect) {
        N(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public void P(int i2) {
        Pz3 pz3 = new Pz3(this.a.getContext(), i2);
        ColorStateList colorStateList = pz3.b;
        if (colorStateList != null) {
            this.k = colorStateList;
        }
        float f2 = pz3.a;
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.i = f2;
        }
        ColorStateList colorStateList2 = pz3.h;
        if (colorStateList2 != null) {
            this.U = colorStateList2;
        }
        this.S = pz3.i;
        this.T = pz3.j;
        this.R = pz3.k;
        Mz3 mz3 = this.v;
        if (mz3 != null) {
            mz3.c();
        }
        this.v = new Mz3(new Bi(), pz3.e());
        pz3.h(this.a.getContext(), this.v);
        E();
    }

    @DexIgnore
    public void Q(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            E();
        }
    }

    @DexIgnore
    public void R(int i2) {
        if (this.g != i2) {
            this.g = i2;
            E();
        }
    }

    @DexIgnore
    public void S(float f2) {
        if (this.i != f2) {
            this.i = f2;
            E();
        }
    }

    @DexIgnore
    public void T(Typeface typeface) {
        if (U(typeface)) {
            E();
        }
    }

    @DexIgnore
    public final boolean U(Typeface typeface) {
        Mz3 mz3 = this.v;
        if (mz3 != null) {
            mz3.c();
        }
        if (this.t == typeface) {
            return false;
        }
        this.t = typeface;
        return true;
    }

    @DexIgnore
    public void V(float f2) {
        float a2 = Jm0.a(f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        if (a2 != this.c) {
            this.c = a2;
            d();
        }
    }

    @DexIgnore
    public final void W(float f2) {
        g(f2);
        boolean z2 = V && this.F != 1.0f;
        this.A = z2;
        if (z2) {
            j();
        }
        Mo0.b0(this.a);
    }

    @DexIgnore
    public void X(TimeInterpolator timeInterpolator) {
        this.L = timeInterpolator;
        E();
    }

    @DexIgnore
    public final boolean Y(int[] iArr) {
        this.H = iArr;
        if (!B()) {
            return false;
        }
        E();
        return true;
    }

    @DexIgnore
    public void Z(CharSequence charSequence) {
        if (charSequence == null || !TextUtils.equals(this.x, charSequence)) {
            this.x = charSequence;
            this.y = null;
            h();
            E();
        }
    }

    @DexIgnore
    public void a0(TimeInterpolator timeInterpolator) {
        this.M = timeInterpolator;
        E();
    }

    @DexIgnore
    public final void b() {
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f3 = this.G;
        g(this.j);
        CharSequence charSequence = this.y;
        float measureText = charSequence != null ? this.J.measureText(charSequence, 0, charSequence.length()) : 0.0f;
        int b2 = Wn0.b(this.h, this.z ? 1 : 0);
        int i2 = b2 & 112;
        if (i2 == 48) {
            this.n = ((float) this.e.top) - this.J.ascent();
        } else if (i2 != 80) {
            this.n = (((this.J.descent() - this.J.ascent()) / 2.0f) - this.J.descent()) + ((float) this.e.centerY());
        } else {
            this.n = (float) this.e.bottom;
        }
        int i3 = b2 & 8388615;
        if (i3 == 1) {
            this.p = ((float) this.e.centerX()) - (measureText / 2.0f);
        } else if (i3 != 5) {
            this.p = (float) this.e.left;
        } else {
            this.p = ((float) this.e.right) - measureText;
        }
        g(this.i);
        CharSequence charSequence2 = this.y;
        if (charSequence2 != null) {
            f2 = this.J.measureText(charSequence2, 0, charSequence2.length());
        }
        int b3 = Wn0.b(this.g, this.z ? 1 : 0);
        int i4 = b3 & 112;
        if (i4 == 48) {
            this.m = ((float) this.d.top) - this.J.ascent();
        } else if (i4 != 80) {
            this.m = (((this.J.descent() - this.J.ascent()) / 2.0f) - this.J.descent()) + ((float) this.d.centerY());
        } else {
            this.m = (float) this.d.bottom;
        }
        int i5 = b3 & 8388615;
        if (i5 == 1) {
            this.o = ((float) this.d.centerX()) - (f2 / 2.0f);
        } else if (i5 != 5) {
            this.o = (float) this.d.left;
        } else {
            this.o = ((float) this.d.right) - f2;
        }
        h();
        W(f3);
    }

    @DexIgnore
    public void b0(Typeface typeface) {
        boolean M2 = M(typeface);
        boolean U2 = U(typeface);
        if (M2 || U2) {
            E();
        }
    }

    @DexIgnore
    public float c() {
        if (this.x == null) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        x(this.K);
        TextPaint textPaint = this.K;
        CharSequence charSequence = this.x;
        return textPaint.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public final void d() {
        f(this.c);
    }

    @DexIgnore
    public final boolean e(CharSequence charSequence) {
        boolean z2 = true;
        if (Mo0.z(this.a) != 1) {
            z2 = false;
        }
        return (z2 ? Gn0.d : Gn0.c).a(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public final void f(float f2) {
        z(f2);
        this.q = C(this.o, this.p, f2, this.L);
        this.r = C(this.m, this.n, f2, this.L);
        W(C(this.i, this.j, f2, this.M));
        if (this.l != this.k) {
            this.J.setColor(a(r(), p(), f2));
        } else {
            this.J.setColor(p());
        }
        this.J.setShadowLayer(C(this.R, this.N, f2, null), C(this.S, this.O, f2, null), C(this.T, this.P, f2, null), a(q(this.U), q(this.Q), f2));
        Mo0.b0(this.a);
    }

    @DexIgnore
    public final void g(float f2) {
        float f3;
        boolean z2;
        boolean z3 = true;
        if (this.x != null) {
            float width = (float) this.e.width();
            float width2 = (float) this.d.width();
            if (A(f2, this.j)) {
                f3 = this.j;
                this.F = 1.0f;
                Typeface typeface = this.u;
                Typeface typeface2 = this.s;
                if (typeface != typeface2) {
                    this.u = typeface2;
                    z2 = true;
                } else {
                    z2 = false;
                }
            } else {
                f3 = this.i;
                Typeface typeface3 = this.u;
                Typeface typeface4 = this.t;
                if (typeface3 != typeface4) {
                    this.u = typeface4;
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (A(f2, this.i)) {
                    this.F = 1.0f;
                } else {
                    this.F = f2 / this.i;
                }
                float f4 = this.j / this.i;
                width = width2 * f4 > width ? Math.min(width / f4, width2) : width2;
            }
            if (width > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z2 = this.G != f3 || this.I || z2;
                this.G = f3;
                this.I = false;
            }
            if (this.y == null || z2) {
                this.J.setTextSize(this.G);
                this.J.setTypeface(this.u);
                TextPaint textPaint = this.J;
                if (this.F == 1.0f) {
                    z3 = false;
                }
                textPaint.setLinearText(z3);
                CharSequence ellipsize = TextUtils.ellipsize(this.x, this.J, width, TextUtils.TruncateAt.END);
                if (!TextUtils.equals(ellipsize, this.y)) {
                    this.y = ellipsize;
                    this.z = e(ellipsize);
                }
            }
        }
    }

    @DexIgnore
    public final void h() {
        Bitmap bitmap = this.B;
        if (bitmap != null) {
            bitmap.recycle();
            this.B = null;
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        float ascent;
        int save = canvas.save();
        if (this.y != null && this.b) {
            float f2 = this.q;
            float f3 = this.r;
            boolean z2 = this.A && this.B != null;
            if (z2) {
                ascent = this.D * this.F;
            } else {
                ascent = this.J.ascent() * this.F;
                this.J.descent();
            }
            if (z2) {
                f3 += ascent;
            }
            float f4 = this.F;
            if (f4 != 1.0f) {
                canvas.scale(f4, f4, f2, f3);
            }
            if (z2) {
                canvas.drawBitmap(this.B, f2, f3, this.C);
            } else {
                CharSequence charSequence = this.y;
                canvas.drawText(charSequence, 0, charSequence.length(), f2, f3, this.J);
            }
        }
        canvas.restoreToCount(save);
    }

    @DexIgnore
    public final void j() {
        if (this.B == null && !this.d.isEmpty() && !TextUtils.isEmpty(this.y)) {
            f(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.D = this.J.ascent();
            this.E = this.J.descent();
            TextPaint textPaint = this.J;
            CharSequence charSequence = this.y;
            int round = Math.round(textPaint.measureText(charSequence, 0, charSequence.length()));
            int round2 = Math.round(this.E - this.D);
            if (round > 0 && round2 > 0) {
                this.B = Bitmap.createBitmap(round, round2, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(this.B);
                CharSequence charSequence2 = this.y;
                canvas.drawText(charSequence2, 0, charSequence2.length(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) round2) - this.J.descent(), this.J);
                if (this.C == null) {
                    this.C = new Paint(3);
                }
            }
        }
    }

    @DexIgnore
    public void k(RectF rectF) {
        boolean e2 = e(this.x);
        Rect rect = this.e;
        float c2 = !e2 ? (float) rect.left : ((float) rect.right) - c();
        rectF.left = c2;
        Rect rect2 = this.e;
        rectF.top = (float) rect2.top;
        rectF.right = !e2 ? c2 + c() : (float) rect2.right;
        rectF.bottom = ((float) this.e.top) + n();
    }

    @DexIgnore
    public ColorStateList l() {
        return this.l;
    }

    @DexIgnore
    public int m() {
        return this.h;
    }

    @DexIgnore
    public float n() {
        x(this.K);
        return -this.K.ascent();
    }

    @DexIgnore
    public Typeface o() {
        Typeface typeface = this.s;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    @DexIgnore
    public int p() {
        return q(this.l);
    }

    @DexIgnore
    public final int q(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return 0;
        }
        int[] iArr = this.H;
        return iArr != null ? colorStateList.getColorForState(iArr, 0) : colorStateList.getDefaultColor();
    }

    @DexIgnore
    public final int r() {
        return q(this.k);
    }

    @DexIgnore
    public int s() {
        return this.g;
    }

    @DexIgnore
    public float t() {
        y(this.K);
        return -this.K.ascent();
    }

    @DexIgnore
    public Typeface u() {
        Typeface typeface = this.t;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    @DexIgnore
    public float v() {
        return this.c;
    }

    @DexIgnore
    public CharSequence w() {
        return this.x;
    }

    @DexIgnore
    public final void x(TextPaint textPaint) {
        textPaint.setTextSize(this.j);
        textPaint.setTypeface(this.s);
    }

    @DexIgnore
    public final void y(TextPaint textPaint) {
        textPaint.setTextSize(this.i);
        textPaint.setTypeface(this.t);
    }

    @DexIgnore
    public final void z(float f2) {
        this.f.left = C((float) this.d.left, (float) this.e.left, f2, this.L);
        this.f.top = C(this.m, this.n, f2, this.L);
        this.f.right = C((float) this.d.right, (float) this.e.right, f2, this.L);
        this.f.bottom = C((float) this.d.bottom, (float) this.e.bottom, f2, this.L);
    }
}
