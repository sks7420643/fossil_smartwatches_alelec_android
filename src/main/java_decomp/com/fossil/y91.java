package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y91 {
    @DexIgnore
    public static /* final */ Comparator<byte[]> e; // = new Ai();
    @DexIgnore
    public /* final */ List<byte[]> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> b; // = new ArrayList(64);
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Comparator<byte[]> {
        @DexIgnore
        public int a(byte[] bArr, byte[] bArr2) {
            return bArr.length - bArr2.length;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(byte[] bArr, byte[] bArr2) {
            return a(bArr, bArr2);
        }
    }

    @DexIgnore
    public Y91(int i) {
        this.d = i;
    }

    @DexIgnore
    public byte[] a(int i) {
        byte[] bArr;
        synchronized (this) {
            int i2 = 0;
            while (true) {
                if (i2 >= this.b.size()) {
                    bArr = new byte[i];
                    break;
                }
                bArr = this.b.get(i2);
                if (bArr.length >= i) {
                    this.c -= bArr.length;
                    this.b.remove(i2);
                    this.a.remove(bArr);
                    break;
                }
                i2++;
            }
        }
        return bArr;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        synchronized (this) {
            if (bArr != null) {
                if (bArr.length <= this.d) {
                    this.a.add(bArr);
                    int binarySearch = Collections.binarySearch(this.b, bArr, e);
                    if (binarySearch < 0) {
                        binarySearch = (-binarySearch) - 1;
                    }
                    this.b.add(binarySearch, bArr);
                    this.c += bArr.length;
                    c();
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        synchronized (this) {
            while (this.c > this.d) {
                byte[] remove = this.a.remove(0);
                this.b.remove(remove);
                this.c -= remove.length;
            }
        }
    }
}
