package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nc7 {
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<Pc7>> a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Qc7> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Uc7 d;
    @DexIgnore
    public /* final */ Rc7 e;
    @DexIgnore
    public /* final */ ThreadLocal<ConcurrentLinkedQueue<Ci>> f;
    @DexIgnore
    public /* final */ ThreadLocal<Boolean> g;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<Class<?>>> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends ThreadLocal<ConcurrentLinkedQueue<Ci>> {
        @DexIgnore
        public Ai(Nc7 nc7) {
        }

        @DexIgnore
        public ConcurrentLinkedQueue<Ci> a() {
            return new ConcurrentLinkedQueue<>();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ ConcurrentLinkedQueue<Ci> initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends ThreadLocal<Boolean> {
        @DexIgnore
        public Bi(Nc7 nc7) {
        }

        @DexIgnore
        public Boolean a() {
            return Boolean.FALSE;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ Boolean initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ Pc7 b;

        @DexIgnore
        public Ci(Object obj, Pc7 pc7) {
            this.a = obj;
            this.b = pc7;
        }
    }

    @DexIgnore
    public Nc7(Uc7 uc7) {
        this(uc7, Endpoints.DEFAULT_NAME);
    }

    @DexIgnore
    public Nc7(Uc7 uc7, String str) {
        this(uc7, str, Rc7.a);
    }

    @DexIgnore
    public Nc7(Uc7 uc7, String str, Rc7 rc7) {
        this.a = new ConcurrentHashMap();
        this.b = new ConcurrentHashMap();
        this.f = new Ai(this);
        this.g = new Bi(this);
        this.h = new ConcurrentHashMap();
        this.d = uc7;
        this.c = str;
        this.e = rc7;
    }

    @DexIgnore
    public static void k(String str, InvocationTargetException invocationTargetException) {
        Throwable cause = invocationTargetException.getCause();
        if (cause != null) {
            throw new RuntimeException(str + ": " + cause.getMessage(), cause);
        }
        throw new RuntimeException(str + ": " + invocationTargetException.getMessage(), invocationTargetException);
    }

    @DexIgnore
    public void a(Object obj, Pc7 pc7) {
        try {
            pc7.a(obj);
        } catch (InvocationTargetException e2) {
            k("Could not dispatch event: " + obj.getClass() + " to handler " + pc7, e2);
            throw null;
        }
    }

    @DexIgnore
    public final void b(Pc7 pc7, Qc7 qc7) {
        try {
            Object c2 = qc7.c();
            if (c2 != null) {
                a(c2, pc7);
            }
        } catch (InvocationTargetException e2) {
            k("Producer " + qc7 + " threw an exception.", e2);
            throw null;
        }
    }

    @DexIgnore
    public void c() {
        if (!this.g.get().booleanValue()) {
            this.g.set(Boolean.TRUE);
            while (true) {
                try {
                    Ci poll = this.f.get().poll();
                    if (poll != null) {
                        if (poll.b.c()) {
                            a(poll.a, poll.b);
                        }
                    } else {
                        return;
                    }
                } finally {
                    this.g.set(Boolean.FALSE);
                }
            }
        }
    }

    @DexIgnore
    public void d(Object obj, Pc7 pc7) {
        this.f.get().offer(new Ci(obj, pc7));
    }

    @DexIgnore
    public Set<Class<?>> e(Class<?> cls) {
        Set<Class<?>> set = this.h.get(cls);
        if (set != null) {
            return set;
        }
        Set<Class<?>> f2 = f(cls);
        Set<Class<?>> putIfAbsent = this.h.putIfAbsent(cls, f2);
        return putIfAbsent == null ? f2 : putIfAbsent;
    }

    @DexIgnore
    public final Set<Class<?>> f(Class<?> cls) {
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        linkedList.add(cls);
        while (!linkedList.isEmpty()) {
            Class cls2 = (Class) linkedList.remove(0);
            hashSet.add(cls2);
            Class superclass = cls2.getSuperclass();
            if (superclass != null) {
                linkedList.add(superclass);
            }
        }
        return hashSet;
    }

    @DexIgnore
    public Set<Pc7> g(Class<?> cls) {
        return this.a.get(cls);
    }

    @DexIgnore
    public Qc7 h(Class<?> cls) {
        return this.b.get(cls);
    }

    @DexIgnore
    public void i(Object obj) {
        if (obj != null) {
            this.d.a(this);
            boolean z = false;
            for (Class<?> cls : e(obj.getClass())) {
                Set<Pc7> g2 = g(cls);
                if (g2 != null && !g2.isEmpty()) {
                    z = true;
                    for (Pc7 pc7 : g2) {
                        d(obj, pc7);
                    }
                }
            }
            if (!z && !(obj instanceof Oc7)) {
                i(new Oc7(this, obj));
            }
            c();
            return;
        }
        throw new NullPointerException("Event to post must not be null.");
    }

    @DexIgnore
    public void j(Object obj) {
        Set<Pc7> set;
        if (obj != null) {
            this.d.a(this);
            Map<Class<?>, Qc7> b2 = this.e.b(obj);
            for (Class<?> cls : b2.keySet()) {
                Qc7 qc7 = b2.get(cls);
                Qc7 putIfAbsent = this.b.putIfAbsent(cls, qc7);
                if (putIfAbsent == null) {
                    Set<Pc7> set2 = this.a.get(cls);
                    if (set2 != null && !set2.isEmpty()) {
                        for (Pc7 pc7 : set2) {
                            b(pc7, qc7);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Producer method for type " + cls + " found on type " + qc7.a.getClass() + ", but already registered by type " + putIfAbsent.a.getClass() + CodelessMatcher.CURRENT_CLASS_NAME);
                }
            }
            Map<Class<?>, Set<Pc7>> a2 = this.e.a(obj);
            for (Class<?> cls2 : a2.keySet()) {
                Set<Pc7> set3 = this.a.get(cls2);
                if (set3 == null) {
                    set = new CopyOnWriteArraySet<>();
                    Set<Pc7> putIfAbsent2 = this.a.putIfAbsent(cls2, set);
                    if (putIfAbsent2 != null) {
                        set = putIfAbsent2;
                    }
                } else {
                    set = set3;
                }
                if (!set.addAll(a2.get(cls2))) {
                    throw new IllegalArgumentException("Object already registered.");
                }
            }
            for (Map.Entry<Class<?>, Set<Pc7>> entry : a2.entrySet()) {
                Qc7 qc72 = this.b.get(entry.getKey());
                if (qc72 != null && qc72.b()) {
                    for (Pc7 pc72 : entry.getValue()) {
                        if (!qc72.b()) {
                            break;
                        } else if (pc72.c()) {
                            b(pc72, qc72);
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("Object to register must not be null.");
    }

    @DexIgnore
    public void l(Object obj) {
        if (obj != null) {
            this.d.a(this);
            for (Map.Entry<Class<?>, Qc7> entry : this.e.b(obj).entrySet()) {
                Class<?> key = entry.getKey();
                Qc7 h2 = h(key);
                Qc7 value = entry.getValue();
                if (value == null || !value.equals(h2)) {
                    throw new IllegalArgumentException("Missing event producer for an annotated method. Is " + obj.getClass() + " registered?");
                }
                this.b.remove(key).a();
            }
            for (Map.Entry<Class<?>, Set<Pc7>> entry2 : this.e.a(obj).entrySet()) {
                Set<Pc7> g2 = g(entry2.getKey());
                Set<Pc7> value2 = entry2.getValue();
                if (g2 == null || !g2.containsAll(value2)) {
                    throw new IllegalArgumentException("Missing event handler for an annotated method. Is " + obj.getClass() + " registered?");
                }
                for (Pc7 pc7 : g2) {
                    if (value2.contains(pc7)) {
                        pc7.b();
                    }
                }
                g2.removeAll(value2);
            }
            return;
        }
        throw new NullPointerException("Object to unregister must not be null.");
    }

    @DexIgnore
    public String toString() {
        return "[Bus \"" + this.c + "\"]";
    }
}
