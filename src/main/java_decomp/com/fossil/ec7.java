package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.source.FileRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ec7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f919a;
    @DexIgnore
    public static /* final */ ec7 b; // = new ec7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.utils.WFCommon", f = "WFCommon.kt", l = {103, 130}, m = "deletingBackgroundPhotos")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ec7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ec7 ec7, qn7 qn7) {
            super(qn7);
            this.this$0 = ec7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.utils.WFCommon$downloadWFAsset$2", f = "WFCommon.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FileRepository $fileRepository;
        @DexIgnore
        public /* final */ /* synthetic */ s77 $wfAssetRepository;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.utils.WFCommon$downloadWFAsset$2$1", f = "WFCommon.kt", l = {31}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object c;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    s77 s77 = this.this$0.$wfAssetRepository;
                    this.L$0 = iv7;
                    this.label = 1;
                    c = s77.c(this);
                    if (c == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    c = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                kz4 kz4 = (kz4) c;
                if (kz4.c() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    ec7 ec7 = ec7.b;
                    String str = ec7.f919a;
                    local.e(str, "templates: " + ((List) kz4.c()));
                    for (p77 p77 : (Iterable) kz4.c()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        ec7 ec72 = ec7.b;
                        String str2 = ec7.f919a;
                        local2.e(str2, "downloading template: " + p77.c().a());
                        FileRepository.asyncDownloadFromUrl$default(this.this$0.$fileRepository, p77.c().a(), FileType.WATCH_FACE, null, 4, null);
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ec7$b$b")
        @eo7(c = "com.portfolio.platform.watchface.utils.WFCommon$downloadWFAsset$2$2", f = "WFCommon.kt", l = {43}, m = "invokeSuspend")
        /* renamed from: com.fossil.ec7$b$b  reason: collision with other inner class name */
        public static final class C0064b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0064b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0064b bVar = new C0064b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0064b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object e;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    s77 s77 = this.this$0.$wfAssetRepository;
                    this.L$0 = iv7;
                    this.label = 1;
                    e = s77.e(this);
                    if (e == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    e = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                kz4 kz4 = (kz4) e;
                if (kz4.c() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    ec7 ec7 = ec7.b;
                    String str = ec7.f919a;
                    local.e(str, "tickers: " + ((List) kz4.c()));
                    for (p77 p77 : (Iterable) kz4.c()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        ec7 ec72 = ec7.b;
                        String str2 = ec7.f919a;
                        local2.e(str2, "downloading ticker: " + p77.c().a());
                        FileRepository.asyncDownloadFromUrl$default(this.this$0.$fileRepository, p77.c().a(), FileType.WATCH_FACE, null, 4, null);
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.utils.WFCommon$downloadWFAsset$2$3", f = "WFCommon.kt", l = {55}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d;
                Object d2 = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    s77 s77 = this.this$0.$wfAssetRepository;
                    this.L$0 = iv7;
                    this.label = 1;
                    d = s77.d(this);
                    if (d == d2) {
                        return d2;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    d = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                kz4 kz4 = (kz4) d;
                if (kz4.c() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    ec7 ec7 = ec7.b;
                    String str = ec7.f919a;
                    local.e(str, "rings: " + ((List) kz4.c()));
                    for (p77 p77 : (Iterable) kz4.c()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        ec7 ec72 = ec7.b;
                        String str2 = ec7.f919a;
                        local2.e(str2, "downloading ring: " + p77.c().a());
                        FileRepository.asyncDownloadFromUrl$default(this.this$0.$fileRepository, p77.c().a(), FileType.WATCH_FACE, null, 4, null);
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(s77 s77, FileRepository fileRepository, qn7 qn7) {
            super(2, qn7);
            this.$wfAssetRepository = s77;
            this.$fileRepository = fileRepository;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.$wfAssetRepository, this.$fileRepository, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                ec7 ec7 = ec7.b;
                local.e(ec7.f919a, "start fetching assets");
                xw7 unused = gu7.d(iv7, null, null, new a(this, null), 3, null);
                xw7 unused2 = gu7.d(iv7, null, null, new C0064b(this, null), 3, null);
                return gu7.d(iv7, null, null, new c(this, null), 3, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.utils.WFCommon", f = "WFCommon.kt", l = {71, 74, 86, 91, 92}, m = "upsertPendingAndDownloadPhoto")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ec7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ec7 ec7, qn7 qn7) {
            super(qn7);
            this.this$0 = ec7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, null, null, this);
        }
    }

    /*
    static {
        String simpleName = ec7.class.getSimpleName();
        pq7.b(simpleName, "WFCommon::class.java.simpleName");
        f919a = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.ec7 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Object f(ec7 ec7, k97 k97, FileRepository fileRepository, List list, qn7 qn7, int i, Object obj) {
        if ((i & 4) != 0) {
            list = null;
        }
        return ec7.e(k97, fileRepository, list, qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(com.fossil.k97 r11, com.portfolio.platform.data.source.FileRepository r12, com.fossil.qn7<? super java.lang.Boolean> r13) {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ec7.b(com.fossil.k97, com.portfolio.platform.data.source.FileRepository, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object c(s77 s77, FileRepository fileRepository, qn7<? super xw7> qn7) {
        return jv7.e(new b(s77, fileRepository, null), qn7);
    }

    @DexIgnore
    public final fb7 d() {
        return new fb7("-metadata.priority", "", "", "", l77.BACKGROUND_TEMPLATE, "", null, null, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0259  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.k97 r14, com.portfolio.platform.data.source.FileRepository r15, java.util.List<com.fossil.g97> r16, com.fossil.qn7<? super com.fossil.tl7> r17) {
        /*
        // Method dump skipped, instructions count: 619
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ec7.e(com.fossil.k97, com.portfolio.platform.data.source.FileRepository, java.util.List, com.fossil.qn7):java.lang.Object");
    }
}
