package com.fossil;

import com.fossil.Qc2;
import com.fossil.T62;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wd2 implements T62.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ T62 a;
    @DexIgnore
    public /* final */ /* synthetic */ Ot3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Qc2.Ai c;
    @DexIgnore
    public /* final */ /* synthetic */ Qc2.Bi d;

    @DexIgnore
    public Wd2(T62 t62, Ot3 ot3, Qc2.Ai ai, Qc2.Bi bi) {
        this.a = t62;
        this.b = ot3;
        this.c = ai;
        this.d = bi;
    }

    @DexIgnore
    @Override // com.fossil.T62.Ai
    public final void a(Status status) {
        if (status.D()) {
            this.b.c(this.c.a(this.a.c(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.b.b(this.d.a(status));
    }
}
