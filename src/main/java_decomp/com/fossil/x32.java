package com.fossil;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X32 implements T32 {
    @DexIgnore
    @Override // com.fossil.T32
    public long a() {
        return SystemClock.elapsedRealtime();
    }
}
