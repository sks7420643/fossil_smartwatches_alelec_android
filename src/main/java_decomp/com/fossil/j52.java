package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J52 implements Parcelable.Creator<SignInConfiguration> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SignInConfiguration createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        GoogleSignInOptions googleSignInOptions = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                str = Ad2.f(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                googleSignInOptions = (GoogleSignInOptions) Ad2.e(parcel, t, GoogleSignInOptions.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new SignInConfiguration(str, googleSignInOptions);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SignInConfiguration[] newArray(int i) {
        return new SignInConfiguration[i];
    }
}
