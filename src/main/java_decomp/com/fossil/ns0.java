package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import com.fossil.Os0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ns0 implements LifecycleOwner {
    @DexIgnore
    public static /* final */ Ns0 j; // = new Ns0();
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public Handler f;
    @DexIgnore
    public /* final */ LifecycleRegistry g; // = new LifecycleRegistry(this);
    @DexIgnore
    public Runnable h; // = new Ai();
    @DexIgnore
    public Os0.Ai i; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            Ns0.this.f();
            Ns0.this.g();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Os0.Ai {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.Os0.Ai
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Os0.Ai
        public void onResume() {
            Ns0.this.b();
        }

        @DexIgnore
        @Override // com.fossil.Os0.Ai
        public void onStart() {
            Ns0.this.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Tr0 {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            Os0.f(activity).h(Ns0.this.i);
        }

        @DexIgnore
        @Override // com.fossil.Tr0
        public void onActivityPaused(Activity activity) {
            Ns0.this.a();
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            Ns0.this.d();
        }
    }

    @DexIgnore
    public static LifecycleOwner h() {
        return j;
    }

    @DexIgnore
    public static void i(Context context) {
        j.e(context);
    }

    @DexIgnore
    public void a() {
        int i2 = this.c - 1;
        this.c = i2;
        if (i2 == 0) {
            this.f.postDelayed(this.h, 700);
        }
    }

    @DexIgnore
    public void b() {
        int i2 = this.c + 1;
        this.c = i2;
        if (i2 != 1) {
            return;
        }
        if (this.d) {
            this.g.i(Lifecycle.a.ON_RESUME);
            this.d = false;
            return;
        }
        this.f.removeCallbacks(this.h);
    }

    @DexIgnore
    public void c() {
        int i2 = this.b + 1;
        this.b = i2;
        if (i2 == 1 && this.e) {
            this.g.i(Lifecycle.a.ON_START);
            this.e = false;
        }
    }

    @DexIgnore
    public void d() {
        this.b--;
        g();
    }

    @DexIgnore
    public void e(Context context) {
        this.f = new Handler();
        this.g.i(Lifecycle.a.ON_CREATE);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new Ci());
    }

    @DexIgnore
    public void f() {
        if (this.c == 0) {
            this.d = true;
            this.g.i(Lifecycle.a.ON_PAUSE);
        }
    }

    @DexIgnore
    public void g() {
        if (this.b == 0 && this.d) {
            this.g.i(Lifecycle.a.ON_STOP);
            this.e = true;
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LifecycleOwner
    public Lifecycle getLifecycle() {
        return this.g;
    }
}
