package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import androidx.work.ListenableWorker;
import com.fossil.Kk7;
import com.mapped.An4;
import com.mapped.Brown;
import com.mapped.Cj4;
import com.mapped.DianaCustomizeEditFragment;
import com.mapped.EditPhotoFragment;
import com.mapped.GetLocation;
import com.mapped.HomeAlertsFragment;
import com.mapped.HomeDashboardFragment;
import com.mapped.HybridCustomizeEditPresenter;
import com.mapped.HybridMessageNotificationComponent;
import com.mapped.Iface;
import com.mapped.InAppNotificationManager;
import com.mapped.Kk4;
import com.mapped.LoadLocation;
import com.mapped.MapPickerFragment;
import com.mapped.MapPickerViewModel;
import com.mapped.MyFaceFragment;
import com.mapped.SetNotificationFiltersUserCase;
import com.mapped.ThemesFragment;
import com.mapped.TimeTickReceiver;
import com.mapped.U04;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.WatchAppEditFragment;
import com.mapped.WatchFaceComplicationFragment;
import com.mapped.WorkoutSettingFragment;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.analytic.BCAnalytic;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.NotificationRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel;
import com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroViewModel;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel;
import com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel;
import com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel;
import com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel;
import com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel;
import com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel;
import com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel;
import com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.CloudImageHelper_MembersInjector;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper_MembersInjector;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.LocationSource_Factory;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.RingStyleRepository_Factory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository_Factory;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.ActivitiesRepository_Factory;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.AlarmsRepository_Factory;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.CategoryRepository_Factory;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.ComplicationRepository_Factory;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceDatabase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DeviceRepository_Factory;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository_Factory;
import com.portfolio.platform.data.source.DianaPresetRepository_Factory;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository_Factory;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FileRepository_Factory;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.FitnessDataRepository_Factory;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository_Factory;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository_Factory;
import com.portfolio.platform.data.source.LabelRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.MicroAppRepository_Factory;
import com.portfolio.platform.data.source.NotificationsDataSource;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.NotificationsRepositoryModule;
import com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory;
import com.portfolio.platform.data.source.NotificationsRepository_Factory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessHelperFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidePresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSkuDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideUserSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesBuddyChallengeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.QuickResponseRepository_Factory;
import com.portfolio.platform.data.source.RepositoriesModule;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideComplicationRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.SkuDao;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository_Factory;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository_Factory;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository_Factory;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.ThemeRepository_Factory;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository_Factory;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserRepository_Factory;
import com.portfolio.platform.data.source.UserSettingDao;
import com.portfolio.platform.data.source.UserSettingDatabase;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchAppRepository_Factory;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository_Factory;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository_Factory;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository_Factory;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.data.source.loader.NotificationsLoader_Factory;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository_Factory;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ApiService2Dot1;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.SecureApiService;
import com.portfolio.platform.data.source.remote.SecureApiService2Dot1;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource_Factory;
import com.portfolio.platform.deeplink.DeepLinkActivity;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.manager.FileDownloadManager;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.manager.validation.DataValidationManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.preset.data.source.DianaPresetRemote;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRemote;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.profile.usecase.GetZendeskInformation;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.alarm.AlarmPresenter;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.HomeFragment;
import com.portfolio.platform.uirenew.home.HomePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveMinutesChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActivityChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeButtonViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeGoalTrackingChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter;
import com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase;
import com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.uirenew.migration.MigrationViewModel;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingFragment;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.uirenew.welcome.WelcomePresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.DSTChangeUseCase;
import com.portfolio.platform.usecase.GeneratePassphraseUseCase;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.GetWeather;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.watchface.data.source.WFAssetRemote;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.WatchFaceEditFragment;
import com.portfolio.platform.watchface.edit.WatchFaceEditViewModel;
import com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel;
import com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel;
import com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel;
import com.portfolio.platform.watchface.faces.WatchFaceListFragment;
import com.portfolio.platform.watchface.faces.WatchFaceListViewModel;
import com.portfolio.platform.watchface.faces.page.MyFaceViewModel;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel;
import com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase;
import com.portfolio.platform.watchface.usecase.SetPresetUseCase;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.portfolio.platform.workers.TimeChangeReceiver;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kq4 implements Iface {
    @DexIgnore
    public Provider<AlarmsRepository> A;
    @DexIgnore
    public Provider<Tt4> A0;
    @DexIgnore
    public Provider<ShakeFeedbackService> A1;
    @DexIgnore
    public Provider<GetUserLocation> A2;
    @DexIgnore
    public Provider<Po4> A3;
    @DexIgnore
    public Provider<AlarmHelper> B;
    @DexIgnore
    public Provider<Ft4> B0;
    @DexIgnore
    public Provider<FitnessDataRepository> B1;
    @DexIgnore
    public Provider<GetAddress> B2;
    @DexIgnore
    public Provider<NotificationSettingsDao> B3;
    @DexIgnore
    public Provider<FitnessHelper> C;
    @DexIgnore
    public Provider<Bu4> C0;
    @DexIgnore
    public Provider<Kk4> C1;
    @DexIgnore
    public Provider<MapPickerViewModel> C2;
    @DexIgnore
    public Provider<ServerSettingDataSource> C3;
    @DexIgnore
    public Provider<SummariesRepository> D;
    @DexIgnore
    public Provider<NotificationRemoteDataSource> D0;
    @DexIgnore
    public Provider<ThirdPartyRepository> D1;
    @DexIgnore
    public Provider<Ax4> D2;
    @DexIgnore
    public Provider<ServerSettingDataSource> D3;
    @DexIgnore
    public Provider<SleepSummariesRepository> E;
    @DexIgnore
    public Provider<NotificationRepository> E0;
    @DexIgnore
    public Provider<PushPendingDataWorker.b> E1;
    @DexIgnore
    public Provider<BCMainViewModel> E2;
    @DexIgnore
    public Provider<N47> E3;
    @DexIgnore
    public Provider<GuestApiService> F;
    @DexIgnore
    public Provider<FileDatabase> F0;
    @DexIgnore
    public Provider<WatchFaceRemoteDataSource> F1;
    @DexIgnore
    public Provider<BCCreateSocialProfileViewModel> F2;
    @DexIgnore
    public Provider<U04> G;
    @DexIgnore
    public Provider<FileDao> G0;
    @DexIgnore
    public Provider<WatchFaceRepository> G1;
    @DexIgnore
    public Provider<BCFriendTabViewModel> G2;
    @DexIgnore
    public Provider<NotificationsDataSource> H;
    @DexIgnore
    public Provider<FileDownloadManager> H0;
    @DexIgnore
    public Provider<DataValidationManager> H1;
    @DexIgnore
    public Provider<BCNotificationViewModel> H2;
    @DexIgnore
    public Provider<NotificationsRepository> I;
    @DexIgnore
    public Provider<FileRepository> I0;
    @DexIgnore
    public Provider<AddressDatabase> I1;
    @DexIgnore
    public Provider<BCFindFriendsViewModel> I2;
    @DexIgnore
    public Provider<DeviceDatabase> J;
    @DexIgnore
    public Provider<FCMRepository> J0;
    @DexIgnore
    public Provider<AddressDao> J1;
    @DexIgnore
    public Provider<BCCreateChallengeInputViewModel> J2;
    @DexIgnore
    public Provider<DeviceDao> K;
    @DexIgnore
    public Provider<SecureApiService> K0;
    @DexIgnore
    public Provider<LocationSource> K1;
    @DexIgnore
    public Provider<BCInviteFriendViewModel> K2;
    @DexIgnore
    public Provider<SkuDao> L;
    @DexIgnore
    public Provider<DianaPresetRemote> L0;
    @DexIgnore
    public Provider<WorkoutTetherGpsManager> L1;
    @DexIgnore
    public Provider<BCCreateSubTabViewModel> L2;
    @DexIgnore
    public Provider<ApiService2Dot1> M;
    @DexIgnore
    public Provider<DianaPresetRepository> M0;
    @DexIgnore
    public Provider<Aq5> M1;
    @DexIgnore
    public Provider<BCRecommendationViewModel> M2;
    @DexIgnore
    public Provider<SecureApiService2Dot1> N;
    @DexIgnore
    public Provider<WFBackgroundPhotoRemote> N0;
    @DexIgnore
    public Provider<SmsMmsReceiver> N1;
    @DexIgnore
    public Provider<BCCurrentChallengeSubTabViewModel> N2;
    @DexIgnore
    public Provider<DeviceRemoteDataSource> O;
    @DexIgnore
    public Provider<WFBackgroundPhotoRepository> O0;
    @DexIgnore
    public Provider<BCAnalytic> O1;
    @DexIgnore
    public Provider<BCWaitingChallengeDetailViewModel> O2;
    @DexIgnore
    public Provider<DeviceRepository> P;
    @DexIgnore
    public Provider<AnalyticsHelper> P0;
    @DexIgnore
    public Provider<SetNotificationUseCase> P1;
    @DexIgnore
    public Provider<BCMemberInChallengeViewModel> P2;
    @DexIgnore
    public Provider<ContentResolver> Q;
    @DexIgnore
    public Provider<CategoryDatabase> Q0;
    @DexIgnore
    public Provider<MusicControlComponent> Q1;
    @DexIgnore
    public Provider<BCOverviewLeaderBoardViewModel> Q2;
    @DexIgnore
    public Provider<Uq4> R;
    @DexIgnore
    public Provider<CategoryDao> R0;
    @DexIgnore
    public Provider<LinkStreamingManager> R1;
    @DexIgnore
    public Provider<BCLeaderBoardViewModel> R2;
    @DexIgnore
    public Provider<HybridCustomizeDatabase> S;
    @DexIgnore
    public Provider<CategoryRemoteDataSource> S0;
    @DexIgnore
    public Provider<Rl5> S1;
    @DexIgnore
    public Provider<BCHistoryViewModel> S2;
    @DexIgnore
    public Provider<HybridPresetDao> T;
    @DexIgnore
    public Provider<CategoryRepository> T0;
    @DexIgnore
    public Provider<HybridMessageNotificationComponent> T1;
    @DexIgnore
    public Provider<SetReplyMessageMappingUseCase> T2;
    @DexIgnore
    public Provider<HybridPresetRemoteDataSource> U;
    @DexIgnore
    public Provider<WatchAppRepository> U0;
    @DexIgnore
    public Provider<Un5> U1;
    @DexIgnore
    public Provider<QuickResponseViewModel> U2;
    @DexIgnore
    public Provider<HybridPresetRepository> V;
    @DexIgnore
    public Provider<ComplicationRepository> V0;
    @DexIgnore
    public Provider<Xn5> V1;
    @DexIgnore
    public Provider<CustomizeTextViewModel> V2;
    @DexIgnore
    public Provider<ActivitiesRepository> W;
    @DexIgnore
    public Provider<DianaAppSettingRepository> W0;
    @DexIgnore
    public Provider<MFLoginWechatManager> W1;
    @DexIgnore
    public Provider<CustomizeButtonViewModel> W2;
    @DexIgnore
    public Provider<ShortcutApiService> X;
    @DexIgnore
    public Provider<DianaWatchFaceRemoteDataSource> X0;
    @DexIgnore
    public Provider<MicroAppLastSettingRepository> X1;
    @DexIgnore
    public Provider<CustomizeBackgroundViewModel> X2;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> Y;
    @DexIgnore
    public Provider<DianaWatchFaceRepository> Y0;
    @DexIgnore
    public Provider<MigrationManager> Y1;
    @DexIgnore
    public Provider<CustomizeRingChartViewModel> Y2;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> Z;
    @DexIgnore
    public Provider<RingStyleRemoteDataSource> Z0;
    @DexIgnore
    public Provider<FirmwareFileRepository> Z1;
    @DexIgnore
    public Provider<CustomizeActivityChartViewModel> Z2;
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public Provider<MicroAppSettingRepository> a0;
    @DexIgnore
    public Provider<RingStyleRepository> a1;
    @DexIgnore
    public Provider<Pu5> a2;
    @DexIgnore
    public Provider<CustomizeActiveMinutesChartViewModel> a3;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider<SleepSessionsRepository> b0;
    @DexIgnore
    public Provider<WatchLocalizationRepository> b1;
    @DexIgnore
    public Provider<InAppNotificationDatabase> b2;
    @DexIgnore
    public Provider<CustomizeActiveCaloriesChartViewModel> b3;
    @DexIgnore
    public Provider<An4> c;
    @DexIgnore
    public Provider<GoalTrackingRepository> c0;
    @DexIgnore
    public Provider<WFAssetRemote> c1;
    @DexIgnore
    public Provider<InAppNotificationDao> c2;
    @DexIgnore
    public Provider<CustomizeGoalTrackingChartViewModel> c3;
    @DexIgnore
    public Provider<PortfolioApp> d;
    @DexIgnore
    public Provider<Vn5> d0;
    @DexIgnore
    public Provider<WFAssetRepository> d1;
    @DexIgnore
    public Provider<InAppNotificationRepository> d2;
    @DexIgnore
    public Provider<CustomizeHeartRateChartViewModel> d3;
    @DexIgnore
    public Provider<DNDSettingsDatabase> e;
    @DexIgnore
    public Provider<WatchAppRemoteDataSource> e0;
    @DexIgnore
    public Provider<DianaRecommendedPresetRemote> e1;
    @DexIgnore
    public Provider<InAppNotificationManager> e2;
    @DexIgnore
    public Provider<CustomizeSleepChartViewModel> e3;
    @DexIgnore
    public Provider<QuickResponseDatabase> f;
    @DexIgnore
    public Provider<ComplicationRemoteDataSource> f0;
    @DexIgnore
    public Provider<DianaRecommendedPresetRepository> f1;
    @DexIgnore
    public Provider<GoogleApiService> f2;
    @DexIgnore
    public Provider<UserCustomizeThemeViewModel> f3;
    @DexIgnore
    public Provider<QuickResponseMessageDao> g;
    @DexIgnore
    public Provider<MicroAppDao> g0;
    @DexIgnore
    public Provider<GetDianaDeviceSettingUseCase> g1;
    @DexIgnore
    public Provider<DownloadServiceApi> g2;
    @DexIgnore
    public Provider<J26> g3;
    @DexIgnore
    public Provider<QuickResponseSenderDao> h;
    @DexIgnore
    public Provider<MicroAppRemoteDataSource> h0;
    @DexIgnore
    public Provider<GetHybridDeviceSettingUseCase> h1;
    @DexIgnore
    public Provider<Ct0> h2;
    @DexIgnore
    public Provider<WorkoutEditViewModel> h3;
    @DexIgnore
    public Provider<QuickResponseRepository> i;
    @DexIgnore
    public Provider<MicroAppRepository> i0;
    @DexIgnore
    public Provider<HybridSyncUseCase> i1;
    @DexIgnore
    public Provider<Pl5> i2;
    @DexIgnore
    public Provider<Pn6> i3;
    @DexIgnore
    public Provider<NotificationSettingsDatabase> j;
    @DexIgnore
    public Provider<MicroAppLastSettingDao> j0;
    @DexIgnore
    public Provider<Q27> j1;
    @DexIgnore
    public Provider<Lo4> j2;
    @DexIgnore
    public Provider<SetActivityGoalUserCase> j3;
    @DexIgnore
    public Provider<DianaNotificationComponent> k;
    @DexIgnore
    public Provider<HeartRateSampleRepository> k0;
    @DexIgnore
    public Provider<VerifySecretKeyUseCase> k1;
    @DexIgnore
    public Provider<WatchAppEditViewModel> k2;
    @DexIgnore
    public Provider<ProfileGoalEditViewModel> k3;
    @DexIgnore
    public Provider<AuthApiGuestService> l;
    @DexIgnore
    public Provider<HeartRateSummaryRepository> l0;
    @DexIgnore
    public Provider<UpdateFirmwareUsecase> l1;
    @DexIgnore
    public Provider<HybridCustomizeViewModel> l2;
    @DexIgnore
    public Provider<PreviewWatchFaceUseCase> l3;
    @DexIgnore
    public Provider<AuthenticationInterceptor> m;
    @DexIgnore
    public Provider<WorkoutSessionRepository> m0;
    @DexIgnore
    public Provider<WorkoutSettingRemoteDataSource> m1;
    @DexIgnore
    public Provider<UpdateUser> m2;
    @DexIgnore
    public Provider<WatchFaceGalleryViewModel> m3;
    @DexIgnore
    public Provider<Uq5> n;
    @DexIgnore
    public Provider<RemindersSettingsDatabase> n0;
    @DexIgnore
    public Provider<WorkoutSettingRepository> n1;
    @DexIgnore
    public Provider<GetUser> n2;
    @DexIgnore
    public Provider<CustomizeRealDataManager> n3;
    @DexIgnore
    public Provider<ApiServiceV2> o;
    @DexIgnore
    public Provider<BuddyChallengeDatabase> o0;
    @DexIgnore
    public Provider<DianaSyncUseCase> o1;
    @DexIgnore
    public Provider<ProfileEditViewModel> o2;
    @DexIgnore
    public Provider<WatchFaceEditViewModel> o3;
    @DexIgnore
    public Provider<DianaPresetRemoteDataSource> p;
    @DexIgnore
    public Provider<Jt4> p0;
    @DexIgnore
    public Provider<Cj4> p1;
    @DexIgnore
    public Provider<EditPhotoViewModel> p2;
    @DexIgnore
    public Provider<WatchFaceComplicationViewModel> p3;
    @DexIgnore
    public Provider<com.portfolio.platform.data.source.DianaPresetRepository> q;
    @DexIgnore
    public Provider<Fu4> q0;
    @DexIgnore
    public Provider<AppSettingsDatabase> q1;
    @DexIgnore
    public Provider<S96> q2;
    @DexIgnore
    public Provider<WatchFaceRingViewModel> q3;
    @DexIgnore
    public Provider<CustomizeRealDataDatabase> r;
    @DexIgnore
    public Provider<ProfileRemoteDataSource> r0;
    @DexIgnore
    public Provider<Lr4> r1;
    @DexIgnore
    public Provider<SetVibrationStrengthUseCase> r2;
    @DexIgnore
    public Provider<P97> r3;
    @DexIgnore
    public Provider<CustomizeRealDataDao> s;
    @DexIgnore
    public Provider<ProfileRepository> s0;
    @DexIgnore
    public Provider<Nr4> s1;
    @DexIgnore
    public Provider<UnlinkDeviceUseCase> s2;
    @DexIgnore
    public Provider<WatchFaceStickerViewModel> s3;
    @DexIgnore
    public Provider<CustomizeRealDataRepository> t;
    @DexIgnore
    public Provider<Ys4> t0;
    @DexIgnore
    public Provider<FlagRemoteDataSource> t1;
    @DexIgnore
    public Provider<SwitchActiveDeviceUseCase> t2;
    @DexIgnore
    public Provider<WatchFaceTemplateViewModel> t3;
    @DexIgnore
    public Provider<AuthApiUserService> u;
    @DexIgnore
    public Provider<Xt4> u0;
    @DexIgnore
    public Provider<FlagRepository> u1;
    @DexIgnore
    public Provider<WatchSettingViewModel> u2;
    @DexIgnore
    public Provider<WatchFaceListViewModel> u3;
    @DexIgnore
    public Provider<UserRemoteDataSource> v;
    @DexIgnore
    public Provider<FriendRemoteDataSource> v0;
    @DexIgnore
    public Provider<BuddyChallengeManager> v1;
    @DexIgnore
    public Provider<CommuteTimeWatchAppSettingsViewModel> v2;
    @DexIgnore
    public Provider<MyFaceViewModel> v3;
    @DexIgnore
    public Provider<UserSettingDatabase> w;
    @DexIgnore
    public Provider<FriendRepository> w0;
    @DexIgnore
    public Provider<ThemeDatabase> w1;
    @DexIgnore
    public Provider<CommuteTimeSettingsDetailViewModel> w2;
    @DexIgnore
    public Provider<Hh5> w3;
    @DexIgnore
    public Provider<UserSettingDao> x;
    @DexIgnore
    public Provider<Qs4> x0;
    @DexIgnore
    public Provider<ThemeDao> x1;
    @DexIgnore
    public Provider<ThemesViewModel> x2;
    @DexIgnore
    public Provider<MigrationHelper> x3;
    @DexIgnore
    public Provider<UserRepository> y;
    @DexIgnore
    public Provider<Rt4> y0;
    @DexIgnore
    public Provider<ThemeRepository> y1;
    @DexIgnore
    public Provider<SetWorkoutSettingUseCase> y2;
    @DexIgnore
    public Provider<MigrationViewModel> y3;
    @DexIgnore
    public Provider<AlarmsRemoteDataSource> z;
    @DexIgnore
    public Provider<ChallengeRemoteDataSource> z0;
    @DexIgnore
    public Provider<ApplicationEventListener> z1;
    @DexIgnore
    public Provider<WorkoutSettingViewModel> z2;
    @DexIgnore
    public Provider<Map<Class<? extends Ts0>, Provider<Ts0>>> z3;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements Fo6 {
        @DexIgnore
        public /* final */ Jo6 a;

        @DexIgnore
        public Bi(Jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        @Override // com.fossil.Fo6
        public void a(AboutActivity aboutActivity) {
            c(aboutActivity);
        }

        @DexIgnore
        public final Lo6 b() {
            Lo6 a2 = Mo6.a(Ko6.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final AboutActivity c(AboutActivity aboutActivity) {
            Ms5.e(aboutActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(aboutActivity, (An4) Kq4.this.c.get());
            Ms5.a(aboutActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(aboutActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(aboutActivity, new Gu5());
            Eo6.a(aboutActivity, b());
            return aboutActivity;
        }

        @DexIgnore
        public final Lo6 d(Lo6 lo6) {
            No6.a(lo6);
            return lo6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements Qk6 {
        @DexIgnore
        public /* final */ Uk6 a;

        @DexIgnore
        public Ci(Uk6 uk6) {
            this.a = uk6;
        }

        @DexIgnore
        @Override // com.fossil.Qk6
        public void a(ActiveTimeDetailActivity activeTimeDetailActivity) {
            c(activeTimeDetailActivity);
        }

        @DexIgnore
        public final ActiveTimeDetailPresenter b() {
            ActiveTimeDetailPresenter a2 = Xk6.a(Vk6.a(this.a), (SummariesRepository) Kq4.this.D.get(), (ActivitiesRepository) Kq4.this.W.get(), (UserRepository) Kq4.this.y.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (FileRepository) Kq4.this.I0.get(), (U04) Kq4.this.G.get(), (PortfolioApp) Kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeDetailActivity c(ActiveTimeDetailActivity activeTimeDetailActivity) {
            Ms5.e(activeTimeDetailActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(activeTimeDetailActivity, (An4) Kq4.this.c.get());
            Ms5.a(activeTimeDetailActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(activeTimeDetailActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(activeTimeDetailActivity, new Gu5());
            Pk6.a(activeTimeDetailActivity, b());
            return activeTimeDetailActivity;
        }

        @DexIgnore
        public final ActiveTimeDetailPresenter d(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            Yk6.a(activeTimeDetailPresenter);
            return activeTimeDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements Vd6 {
        @DexIgnore
        public /* final */ De6 a;

        @DexIgnore
        public Di(De6 de6) {
            this.a = de6;
        }

        @DexIgnore
        @Override // com.fossil.Vd6
        public void a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            f(activeTimeOverviewFragment);
        }

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter b() {
            ActiveTimeOverviewDayPresenter a2 = Ae6.a(Ee6.a(this.a), (SummariesRepository) Kq4.this.D.get(), (ActivitiesRepository) Kq4.this.W.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (PortfolioApp) Kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter c() {
            ActiveTimeOverviewMonthPresenter a2 = Le6.a(Fe6.a(this.a), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (PortfolioApp) Kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter d() {
            ActiveTimeOverviewWeekPresenter a2 = Re6.a(Ge6.a(this.a), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (PortfolioApp) Kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter e(ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
            Be6.a(activeTimeOverviewDayPresenter);
            return activeTimeOverviewDayPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewFragment f(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            Ce6.a(activeTimeOverviewFragment, b());
            Ce6.c(activeTimeOverviewFragment, d());
            Ce6.b(activeTimeOverviewFragment, c());
            return activeTimeOverviewFragment;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter g(ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
            Me6.a(activeTimeOverviewMonthPresenter);
            return activeTimeOverviewMonthPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter h(ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter) {
            Se6.a(activeTimeOverviewWeekPresenter);
            return activeTimeOverviewWeekPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei implements Al6 {
        @DexIgnore
        public /* final */ El6 a;

        @DexIgnore
        public Ei(El6 el6) {
            this.a = el6;
        }

        @DexIgnore
        @Override // com.fossil.Al6
        public void a(ActivityDetailActivity activityDetailActivity) {
            c(activityDetailActivity);
        }

        @DexIgnore
        public final ActivityDetailPresenter b() {
            ActivityDetailPresenter a2 = Hl6.a(Fl6.a(this.a), (SummariesRepository) Kq4.this.D.get(), (ActivitiesRepository) Kq4.this.W.get(), (UserRepository) Kq4.this.y.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (FileRepository) Kq4.this.I0.get(), (U04) Kq4.this.G.get(), (PortfolioApp) Kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityDetailActivity c(ActivityDetailActivity activityDetailActivity) {
            Ms5.e(activityDetailActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(activityDetailActivity, (An4) Kq4.this.c.get());
            Ms5.a(activityDetailActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(activityDetailActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(activityDetailActivity, new Gu5());
            Zk6.a(activityDetailActivity, b());
            return activityDetailActivity;
        }

        @DexIgnore
        public final ActivityDetailPresenter d(ActivityDetailPresenter activityDetailPresenter) {
            Il6.a(activityDetailPresenter);
            return activityDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Fi implements Ze6 {
        @DexIgnore
        public /* final */ If6 a;

        @DexIgnore
        public Fi(If6 if6) {
            this.a = if6;
        }

        @DexIgnore
        @Override // com.fossil.Ze6
        public void a(ActivityOverviewFragment activityOverviewFragment) {
            f(activityOverviewFragment);
        }

        @DexIgnore
        public final ActivityOverviewDayPresenter b() {
            ActivityOverviewDayPresenter a2 = Ff6.a(Jf6.a(this.a), (SummariesRepository) Kq4.this.D.get(), (ActivitiesRepository) Kq4.this.W.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (PortfolioApp) Kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter c() {
            ActivityOverviewMonthPresenter a2 = Qf6.a(Kf6.a(this.a), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (PortfolioApp) Kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter d() {
            ActivityOverviewWeekPresenter a2 = Wf6.a(Lf6.a(this.a), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (PortfolioApp) Kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewDayPresenter e(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            Gf6.a(activityOverviewDayPresenter);
            return activityOverviewDayPresenter;
        }

        @DexIgnore
        public final ActivityOverviewFragment f(ActivityOverviewFragment activityOverviewFragment) {
            Hf6.a(activityOverviewFragment, b());
            Hf6.c(activityOverviewFragment, d());
            Hf6.b(activityOverviewFragment, c());
            return activityOverviewFragment;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter g(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            Rf6.a(activityOverviewMonthPresenter);
            return activityOverviewMonthPresenter;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter h(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
            Xf6.a(activityOverviewWeekPresenter);
            return activityOverviewWeekPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Gi implements Mx5 {
        @DexIgnore
        public /* final */ Qx5 a;

        @DexIgnore
        public Gi(Qx5 qx5) {
            this.a = qx5;
        }

        @DexIgnore
        @Override // com.fossil.Mx5
        public void a(AlarmActivity alarmActivity) {
            e(alarmActivity);
        }

        @DexIgnore
        public final AlarmPresenter b() {
            AlarmPresenter a2 = Vx5.a(Tx5.a(this.a), Sx5.a(this.a), Rx5.a(this.a), this.a.a(), d(), (AlarmHelper) Kq4.this.B.get(), c(), (UserRepository) Kq4.this.y.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final DeleteAlarm c() {
            return new DeleteAlarm((AlarmsRepository) Kq4.this.A.get());
        }

        @DexIgnore
        public final SetAlarms d() {
            return new SetAlarms((PortfolioApp) Kq4.this.d.get(), (AlarmsRepository) Kq4.this.A.get());
        }

        @DexIgnore
        public final AlarmActivity e(AlarmActivity alarmActivity) {
            Ms5.e(alarmActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(alarmActivity, (An4) Kq4.this.c.get());
            Ms5.a(alarmActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(alarmActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(alarmActivity, new Gu5());
            Lx5.a(alarmActivity, b());
            return alarmActivity;
        }

        @DexIgnore
        public final AlarmPresenter f(AlarmPresenter alarmPresenter) {
            Wx5.a(alarmPresenter);
            return alarmPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Hi implements Cx4 {
        @DexIgnore
        public Hi() {
        }

        @DexIgnore
        @Override // com.fossil.Cx4
        public void a(Dx4 dx4) {
            b(dx4);
        }

        @DexIgnore
        public final Dx4 b(Dx4 dx4) {
            Ex4.a(dx4, (Po4) Kq4.this.A3.get());
            return dx4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ii implements Ju4 {
        @DexIgnore
        public Ii() {
        }

        @DexIgnore
        @Override // com.fossil.Ju4
        public void a(Ku4 ku4) {
            b(ku4);
        }

        @DexIgnore
        public final Ku4 b(Ku4 ku4) {
            Lu4.a(ku4, (Po4) Kq4.this.A3.get());
            return ku4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ji implements Ou4 {
        @DexIgnore
        public Ji() {
        }

        @DexIgnore
        @Override // com.fossil.Ou4
        public void a(Pu4 pu4) {
            b(pu4);
        }

        @DexIgnore
        public final Pu4 b(Pu4 pu4) {
            Qu4.a(pu4, (Po4) Kq4.this.A3.get());
            return pu4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ki implements Hx4 {
        @DexIgnore
        public Ki() {
        }

        @DexIgnore
        @Override // com.fossil.Hx4
        public void a(Ix4 ix4) {
            b(ix4);
        }

        @DexIgnore
        public final Ix4 b(Ix4 ix4) {
            Kx4.a(ix4, (Po4) Kq4.this.A3.get());
            return ix4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Li implements Ev4 {
        @DexIgnore
        public Li() {
        }

        @DexIgnore
        @Override // com.fossil.Ev4
        public void a(Av4 av4) {
            b(av4);
        }

        @DexIgnore
        public final Av4 b(Av4 av4) {
            Bv4.a(av4, (Po4) Kq4.this.A3.get());
            return av4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Mi implements Nx4 {
        @DexIgnore
        public Mi() {
        }

        @DexIgnore
        @Override // com.fossil.Nx4
        public void a(Ox4 ox4) {
            b(ox4);
        }

        @DexIgnore
        public final Ox4 b(Ox4 ox4) {
            Px4.a(ox4, (Po4) Kq4.this.A3.get());
            return ox4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ni implements Rw4 {
        @DexIgnore
        public Ni() {
        }

        @DexIgnore
        @Override // com.fossil.Rw4
        public void a(Sw4 sw4) {
            b(sw4);
        }

        @DexIgnore
        public final Sw4 b(Sw4 sw4) {
            Uw4.a(sw4, (Po4) Kq4.this.A3.get());
            return sw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Oi implements Gy4 {
        @DexIgnore
        public Oi() {
        }

        @DexIgnore
        @Override // com.fossil.Gy4
        public void a(By4 by4) {
            b(by4);
        }

        @DexIgnore
        public final By4 b(By4 by4) {
            Dy4.a(by4, (Po4) Kq4.this.A3.get());
            return by4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Pi implements Jy4 {
        @DexIgnore
        public Pi() {
        }

        @DexIgnore
        @Override // com.fossil.Jy4
        public void a(Ky4 ky4) {
            b(ky4);
        }

        @DexIgnore
        public final Ky4 b(Ky4 ky4) {
            Ly4.a(ky4, (Po4) Kq4.this.A3.get());
            return ky4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Qi implements Uu4 {
        @DexIgnore
        public Qi() {
        }

        @DexIgnore
        @Override // com.fossil.Uu4
        public void a(Vu4 vu4) {
            b(vu4);
        }

        @DexIgnore
        public final Vu4 b(Vu4 vu4) {
            Wu4.a(vu4, (Po4) Kq4.this.A3.get());
            return vu4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ri implements Fv4 {
        @DexIgnore
        public Ri() {
        }

        @DexIgnore
        @Override // com.fossil.Fv4
        public void a(Gv4 gv4) {
            b(gv4);
        }

        @DexIgnore
        public final Gv4 b(Gv4 gv4) {
            Hv4.a(gv4, (Po4) Kq4.this.A3.get());
            return gv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Si implements Mv4 {
        @DexIgnore
        public Si() {
        }

        @DexIgnore
        @Override // com.fossil.Mv4
        public void a(Nv4 nv4) {
            b(nv4);
        }

        @DexIgnore
        public final Nv4 b(Nv4 nv4) {
            Ov4.a(nv4, (Po4) Kq4.this.A3.get());
            return nv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ti implements Rv4 {
        @DexIgnore
        public Ti() {
        }

        @DexIgnore
        @Override // com.fossil.Rv4
        public void a(Sv4 sv4) {
            b(sv4);
        }

        @DexIgnore
        public final Sv4 b(Sv4 sv4) {
            Tv4.a(sv4, (Po4) Kq4.this.A3.get());
            return sv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ui implements Yv4 {
        @DexIgnore
        public Ui() {
        }

        @DexIgnore
        @Override // com.fossil.Yv4
        public void a(Zv4 zv4) {
            b(zv4);
        }

        @DexIgnore
        public final Zv4 b(Zv4 zv4) {
            Aw4.a(zv4, (Po4) Kq4.this.A3.get());
            return zv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Vi implements Ew4 {
        @DexIgnore
        public Vi() {
        }

        @DexIgnore
        @Override // com.fossil.Ew4
        public void a(Fw4 fw4) {
            b(fw4);
        }

        @DexIgnore
        public final Fw4 b(Fw4 fw4) {
            Gw4.a(fw4, (Po4) Kq4.this.A3.get());
            return fw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Wi implements Tx4 {
        @DexIgnore
        public Wi() {
        }

        @DexIgnore
        @Override // com.fossil.Tx4
        public void a(Ux4 ux4) {
            b(ux4);
        }

        @DexIgnore
        public final Ux4 b(Ux4 ux4) {
            Vx4.a(ux4, (Po4) Kq4.this.A3.get());
            return ux4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Xi implements Kw4 {
        @DexIgnore
        public Xi() {
        }

        @DexIgnore
        @Override // com.fossil.Kw4
        public void a(Lw4 lw4) {
            b(lw4);
        }

        @DexIgnore
        public final Lw4 b(Lw4 lw4) {
            Nw4.a(lw4, (Po4) Kq4.this.A3.get());
            return lw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Yi implements Fx6 {
        @DexIgnore
        public /* final */ Ix6 a;

        @DexIgnore
        public Yi(Ix6 ix6) {
            this.a = ix6;
        }

        @DexIgnore
        @Override // com.fossil.Fx6
        public void a(Yo6 yo6) {
            e(yo6);
        }

        @DexIgnore
        @Override // com.fossil.Fx6
        public void b(Xv5 xv5) {
            f(xv5);
        }

        @DexIgnore
        public final Kx6 c() {
            Kx6 a2 = Lx6.a(Jx6.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final Kx6 d(Kx6 kx6) {
            Mx6.a(kx6);
            return kx6;
        }

        @DexIgnore
        public final Yo6 e(Yo6 yo6) {
            Ap6.b(yo6, (Po4) Kq4.this.A3.get());
            Ap6.a(yo6, c());
            return yo6;
        }

        @DexIgnore
        public final Xv5 f(Xv5 xv5) {
            Zv5.a(xv5, c());
            return xv5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Zi {
        @DexIgnore
        public Uo4 a;
        @DexIgnore
        public PortfolioDatabaseModule b;
        @DexIgnore
        public MicroAppSettingRepositoryModule c;
        @DexIgnore
        public RepositoriesModule d;
        @DexIgnore
        public NotificationsRepositoryModule e;
        @DexIgnore
        public UAppSystemVersionRepositoryModule f;

        @DexIgnore
        public Zi() {
        }

        @DexIgnore
        public Zi a(Uo4 uo4) {
            Lk7.b(uo4);
            this.a = uo4;
            return this;
        }

        @DexIgnore
        public Iface b() {
            Lk7.a(this.a, Uo4.class);
            if (this.b == null) {
                this.b = new PortfolioDatabaseModule();
            }
            if (this.c == null) {
                this.c = new MicroAppSettingRepositoryModule();
            }
            if (this.d == null) {
                this.d = new RepositoriesModule();
            }
            if (this.e == null) {
                this.e = new NotificationsRepositoryModule();
            }
            if (this.f == null) {
                this.f = new UAppSystemVersionRepositoryModule();
            }
            return new Kq4(this.a, this.b, this.c, this.d, this.e, this.f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a0 implements J17 {
        @DexIgnore
        public /* final */ M17 a;

        @DexIgnore
        public a0(M17 m17) {
            this.a = m17;
        }

        @DexIgnore
        @Override // com.fossil.J17
        public void a(CalibrationActivity calibrationActivity) {
            c(calibrationActivity);
        }

        @DexIgnore
        public final CalibrationPresenter b() {
            CalibrationPresenter a2 = Q17.a((PortfolioApp) Kq4.this.d.get(), N17.a(this.a), (UserRepository) Kq4.this.y.get(), (Ct0) Kq4.this.h2.get(), (Pl5) Kq4.this.i2.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CalibrationActivity c(CalibrationActivity calibrationActivity) {
            Ms5.e(calibrationActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(calibrationActivity, (An4) Kq4.this.c.get());
            Ms5.a(calibrationActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(calibrationActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(calibrationActivity, new Gu5());
            I17.a(calibrationActivity, b());
            return calibrationActivity;
        }

        @DexIgnore
        public final CalibrationPresenter d(CalibrationPresenter calibrationPresenter) {
            R17.a(calibrationPresenter);
            return calibrationPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a1 implements E07 {
        @DexIgnore
        public /* final */ F07 a;

        @DexIgnore
        public a1(F07 f07) {
            this.a = f07;
        }

        @DexIgnore
        @Override // com.fossil.E07
        public void a(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            d(emailOtpVerificationActivity);
        }

        @DexIgnore
        public final B07 b() {
            B07 a2 = C07.a(G07.a(this.a));
            e(a2);
            return a2;
        }

        @DexIgnore
        public final VerifyEmailOtp c() {
            return new VerifyEmailOtp((UserRepository) Kq4.this.y.get());
        }

        @DexIgnore
        public final EmailOtpVerificationActivity d(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            Ms5.e(emailOtpVerificationActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(emailOtpVerificationActivity, (An4) Kq4.this.c.get());
            Ms5.a(emailOtpVerificationActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(emailOtpVerificationActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(emailOtpVerificationActivity, new Gu5());
            Xz6.a(emailOtpVerificationActivity, b());
            return emailOtpVerificationActivity;
        }

        @DexIgnore
        public final B07 e(B07 b07) {
            D07.a(b07, Kq4.this.G4());
            D07.b(b07, c());
            D07.c(b07);
            return b07;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a2 implements T16 {
        @DexIgnore
        public /* final */ Y16 a;

        @DexIgnore
        public a2(Y16 y16) {
            this.a = y16;
        }

        @DexIgnore
        @Override // com.fossil.T16
        public void a(NotificationAppsFragment notificationAppsFragment) {
        }

        @DexIgnore
        @Override // com.fossil.T16
        public void b(Z06 z06) {
            e(z06);
        }

        @DexIgnore
        @Override // com.fossil.T16
        public void c(W16 w16) {
            f(w16);
        }

        @DexIgnore
        public final A26 d() {
            A26 a2 = B26.a(Z16.a(this.a));
            g(a2);
            return a2;
        }

        @DexIgnore
        public final Z06 e(Z06 z06) {
            A16.a(z06, d());
            A16.b(z06, (Po4) Kq4.this.A3.get());
            return z06;
        }

        @DexIgnore
        public final W16 f(W16 w16) {
            X16.a(w16, (Po4) Kq4.this.A3.get());
            return w16;
        }

        @DexIgnore
        public final A26 g(A26 a26) {
            C26.a(a26);
            return a26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a3 implements U96 {
        @DexIgnore
        public a3(Z96 z96) {
        }

        @DexIgnore
        @Override // com.fossil.U96
        public void a(X96 x96) {
            b(x96);
        }

        @DexIgnore
        public final X96 b(X96 x96) {
            Y96.a(x96, (Po4) Kq4.this.A3.get());
            return x96;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements Nl6 {
        @DexIgnore
        public /* final */ Rl6 a;

        @DexIgnore
        public b0(Rl6 rl6) {
            this.a = rl6;
        }

        @DexIgnore
        @Override // com.fossil.Nl6
        public void a(CaloriesDetailActivity caloriesDetailActivity) {
            c(caloriesDetailActivity);
        }

        @DexIgnore
        public final CaloriesDetailPresenter b() {
            CaloriesDetailPresenter a2 = Ul6.a(Sl6.a(this.a), (SummariesRepository) Kq4.this.D.get(), (ActivitiesRepository) Kq4.this.W.get(), (UserRepository) Kq4.this.y.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (FileRepository) Kq4.this.I0.get(), (U04) Kq4.this.G.get(), (PortfolioApp) Kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesDetailActivity c(CaloriesDetailActivity caloriesDetailActivity) {
            Ms5.e(caloriesDetailActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(caloriesDetailActivity, (An4) Kq4.this.c.get());
            Ms5.a(caloriesDetailActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(caloriesDetailActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(caloriesDetailActivity, new Gu5());
            Ml6.a(caloriesDetailActivity, b());
            return caloriesDetailActivity;
        }

        @DexIgnore
        public final CaloriesDetailPresenter d(CaloriesDetailPresenter caloriesDetailPresenter) {
            Vl6.a(caloriesDetailPresenter);
            return caloriesDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b1 implements Ov6 {
        @DexIgnore
        public /* final */ Rv6 a;

        @DexIgnore
        public b1(Rv6 rv6) {
            this.a = rv6;
        }

        @DexIgnore
        @Override // com.fossil.Ov6
        public void a(ExploreWatchActivity exploreWatchActivity) {
            c(exploreWatchActivity);
        }

        @DexIgnore
        public final ExploreWatchPresenter b() {
            ExploreWatchPresenter a2 = Uv6.a(Sv6.a(this.a), Kq4.this.a4(), (DeviceRepository) Kq4.this.P.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ExploreWatchActivity c(ExploreWatchActivity exploreWatchActivity) {
            Ms5.e(exploreWatchActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(exploreWatchActivity, (An4) Kq4.this.c.get());
            Ms5.a(exploreWatchActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(exploreWatchActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(exploreWatchActivity, new Gu5());
            Nv6.a(exploreWatchActivity, b());
            return exploreWatchActivity;
        }

        @DexIgnore
        public final ExploreWatchPresenter d(ExploreWatchPresenter exploreWatchPresenter) {
            Vv6.a(exploreWatchPresenter);
            return exploreWatchPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b2 implements W26 {
        @DexIgnore
        public /* final */ B36 a;

        @DexIgnore
        public b2(B36 b36) {
            this.a = b36;
        }

        @DexIgnore
        @Override // com.fossil.W26
        public void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            c(notificationWatchRemindersActivity);
        }

        @DexIgnore
        public final NotificationWatchRemindersPresenter b() {
            NotificationWatchRemindersPresenter a2 = E36.a(C36.a(this.a), (An4) Kq4.this.c.get(), (RemindersSettingsDatabase) Kq4.this.n0.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationWatchRemindersActivity c(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            Ms5.e(notificationWatchRemindersActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationWatchRemindersActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationWatchRemindersActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationWatchRemindersActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationWatchRemindersActivity, new Gu5());
            V26.a(notificationWatchRemindersActivity, b());
            return notificationWatchRemindersActivity;
        }

        @DexIgnore
        public final NotificationWatchRemindersPresenter d(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            F36.a(notificationWatchRemindersPresenter);
            return notificationWatchRemindersPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b3 implements E87 {
        @DexIgnore
        public b3() {
        }

        @DexIgnore
        @Override // com.fossil.E87
        public void a(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            b(watchFaceComplicationFragment);
        }

        @DexIgnore
        public final WatchFaceComplicationFragment b(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            G87.a(watchFaceComplicationFragment, (Po4) Kq4.this.A3.get());
            return watchFaceComplicationFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements Eg6 {
        @DexIgnore
        public /* final */ Mg6 a;

        @DexIgnore
        public c0(Mg6 mg6) {
            this.a = mg6;
        }

        @DexIgnore
        @Override // com.fossil.Eg6
        public void a(CaloriesOverviewFragment caloriesOverviewFragment) {
            f(caloriesOverviewFragment);
        }

        @DexIgnore
        public final CaloriesOverviewDayPresenter b() {
            CaloriesOverviewDayPresenter a2 = Jg6.a(Ng6.a(this.a), (SummariesRepository) Kq4.this.D.get(), (ActivitiesRepository) Kq4.this.W.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (PortfolioApp) Kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter c() {
            CaloriesOverviewMonthPresenter a2 = Ug6.a(Og6.a(this.a), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (PortfolioApp) Kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter d() {
            CaloriesOverviewWeekPresenter a2 = Ah6.a(Pg6.a(this.a), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (PortfolioApp) Kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewDayPresenter e(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            Kg6.a(caloriesOverviewDayPresenter);
            return caloriesOverviewDayPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewFragment f(CaloriesOverviewFragment caloriesOverviewFragment) {
            Lg6.a(caloriesOverviewFragment, b());
            Lg6.c(caloriesOverviewFragment, d());
            Lg6.b(caloriesOverviewFragment, c());
            return caloriesOverviewFragment;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter g(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
            Vg6.a(caloriesOverviewMonthPresenter);
            return caloriesOverviewMonthPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter h(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
            Bh6.a(caloriesOverviewWeekPresenter);
            return caloriesOverviewWeekPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c1 implements T17 {
        @DexIgnore
        public /* final */ W17 a;

        @DexIgnore
        public c1(W17 w17) {
            this.a = w17;
        }

        @DexIgnore
        @Override // com.fossil.T17
        public void a(FindDeviceActivity findDeviceActivity) {
            e(findDeviceActivity);
        }

        @DexIgnore
        public final FindDevicePresenter b() {
            FindDevicePresenter a2 = Z17.a((Ct0) Kq4.this.h2.get(), (DeviceRepository) Kq4.this.P.get(), (An4) Kq4.this.c.get(), X17.a(this.a), new GetLocation(), d(), c(), new Gu5(), (PortfolioApp) Kq4.this.d.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final GetAddress c() {
            return new GetAddress((GoogleApiService) Kq4.this.f2.get());
        }

        @DexIgnore
        public final LoadLocation d() {
            return new LoadLocation((Lo4) Kq4.this.j2.get());
        }

        @DexIgnore
        public final FindDeviceActivity e(FindDeviceActivity findDeviceActivity) {
            Ms5.e(findDeviceActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(findDeviceActivity, (An4) Kq4.this.c.get());
            Ms5.a(findDeviceActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(findDeviceActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(findDeviceActivity, new Gu5());
            S17.a(findDeviceActivity, b());
            return findDeviceActivity;
        }

        @DexIgnore
        public final FindDevicePresenter f(FindDevicePresenter findDevicePresenter) {
            A27.a(findDevicePresenter);
            return findDevicePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c2 implements Gw6 {
        @DexIgnore
        public /* final */ Jw6 a;

        @DexIgnore
        public c2(Jw6 jw6) {
            this.a = jw6;
        }

        @DexIgnore
        @Override // com.fossil.Gw6
        public void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            d(onboardingHeightWeightActivity);
        }

        @DexIgnore
        public final GetRecommendedGoalUseCase b() {
            return new GetRecommendedGoalUseCase((SummariesRepository) Kq4.this.D.get(), (SleepSummariesRepository) Kq4.this.E.get(), (SleepSessionsRepository) Kq4.this.b0.get());
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter c() {
            OnboardingHeightWeightPresenter a2 = Mw6.a(Kw6.a(this.a), (UserRepository) Kq4.this.y.get(), b());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final OnboardingHeightWeightActivity d(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            Ms5.e(onboardingHeightWeightActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(onboardingHeightWeightActivity, (An4) Kq4.this.c.get());
            Ms5.a(onboardingHeightWeightActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(onboardingHeightWeightActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(onboardingHeightWeightActivity, new Gu5());
            Fw6.a(onboardingHeightWeightActivity, c());
            return onboardingHeightWeightActivity;
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter e(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
            Nw6.a(onboardingHeightWeightPresenter);
            return onboardingHeightWeightPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c3 implements W77 {
        @DexIgnore
        public c3() {
        }

        @DexIgnore
        @Override // com.fossil.W77
        public void a(WatchFaceEditFragment watchFaceEditFragment) {
            b(watchFaceEditFragment);
        }

        @DexIgnore
        public final WatchFaceEditFragment b(WatchFaceEditFragment watchFaceEditFragment) {
            Z77.a(watchFaceEditFragment, (Po4) Kq4.this.A3.get());
            return watchFaceEditFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 implements Q76 {
        @DexIgnore
        public /* final */ C86 a;

        @DexIgnore
        public d0(C86 c86) {
            this.a = c86;
        }

        @DexIgnore
        @Override // com.fossil.Q76
        public void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            c(commuteTimeSettingsActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsPresenter b() {
            CommuteTimeSettingsPresenter a2 = F86.a(D86.a(this.a), (An4) Kq4.this.c.get(), (UserRepository) Kq4.this.y.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsActivity c(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            Ms5.e(commuteTimeSettingsActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(commuteTimeSettingsActivity, (An4) Kq4.this.c.get());
            Ms5.a(commuteTimeSettingsActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(commuteTimeSettingsActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(commuteTimeSettingsActivity, new Gu5());
            P76.a(commuteTimeSettingsActivity, b());
            return commuteTimeSettingsActivity;
        }

        @DexIgnore
        public final CommuteTimeSettingsPresenter d(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter) {
            G86.a(commuteTimeSettingsPresenter);
            return commuteTimeSettingsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d1 implements Xv6 {
        @DexIgnore
        public /* final */ Aw6 a;

        @DexIgnore
        public d1(Aw6 aw6) {
            this.a = aw6;
        }

        @DexIgnore
        @Override // com.fossil.Xv6
        public void a(ForgotPasswordActivity forgotPasswordActivity) {
            d(forgotPasswordActivity);
        }

        @DexIgnore
        public final Cw6 b() {
            Cw6 a2 = Dw6.a(Bw6.a(this.a), c());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ResetPasswordUseCase c() {
            return new ResetPasswordUseCase((AuthApiGuestService) Kq4.this.l.get());
        }

        @DexIgnore
        public final ForgotPasswordActivity d(ForgotPasswordActivity forgotPasswordActivity) {
            Ms5.e(forgotPasswordActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(forgotPasswordActivity, (An4) Kq4.this.c.get());
            Ms5.a(forgotPasswordActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(forgotPasswordActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(forgotPasswordActivity, new Gu5());
            Wv6.a(forgotPasswordActivity, b());
            return forgotPasswordActivity;
        }

        @DexIgnore
        public final Cw6 e(Cw6 cw6) {
            Ew6.a(cw6);
            return cw6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d2 implements Gy6 {
        @DexIgnore
        public /* final */ Jy6 a;

        @DexIgnore
        public d2(Jy6 jy6) {
            this.a = jy6;
        }

        @DexIgnore
        @Override // com.fossil.Gy6
        public void a(PairingActivity pairingActivity) {
            g(pairingActivity);
        }

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase b() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) Kq4.this.d.get(), (GuestApiService) Kq4.this.F.get());
        }

        @DexIgnore
        public final LabelRemoteDataSource c() {
            return new LabelRemoteDataSource((ApiServiceV2) Kq4.this.o.get());
        }

        @DexIgnore
        public final LabelRepository d() {
            return new LabelRepository((Context) Kq4.this.b.get(), c());
        }

        @DexIgnore
        public final LinkDeviceUseCase e() {
            return new LinkDeviceUseCase((DeviceRepository) Kq4.this.P.get(), b(), (An4) Kq4.this.c.get(), (UserRepository) Kq4.this.y.get(), Kq4.this.Y3(), Kq4.this.a4(), d(), (FileRepository) Kq4.this.I0.get());
        }

        @DexIgnore
        public final PairingPresenter f() {
            PairingPresenter a2 = Ny6.a(Ky6.a(this.a), e(), (DeviceRepository) Kq4.this.P.get(), Kq4.this.a4(), (MusicControlComponent) Kq4.this.Q1.get(), Kq4.this.J4(), (An4) Kq4.this.c.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final PairingActivity g(PairingActivity pairingActivity) {
            Ms5.e(pairingActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(pairingActivity, (An4) Kq4.this.c.get());
            Ms5.a(pairingActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(pairingActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(pairingActivity, new Gu5());
            Fy6.a(pairingActivity, f());
            return pairingActivity;
        }

        @DexIgnore
        public final PairingPresenter h(PairingPresenter pairingPresenter) {
            Oy6.a(pairingPresenter);
            return pairingPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d3 implements Wa7 {
        @DexIgnore
        public d3() {
        }

        @DexIgnore
        @Override // com.fossil.Wa7
        public void a(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            b(watchFaceGalleryFragment);
        }

        @DexIgnore
        public final WatchFaceGalleryFragment b(WatchFaceGalleryFragment watchFaceGalleryFragment) {
            Xa7.a(watchFaceGalleryFragment, (Po4) Kq4.this.A3.get());
            return watchFaceGalleryFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 implements U76 {
        @DexIgnore
        public /* final */ X76 a;

        @DexIgnore
        public e0(X76 x76) {
            this.a = x76;
        }

        @DexIgnore
        @Override // com.fossil.U76
        public void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            c(commuteTimeSettingsDefaultAddressActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter b() {
            CommuteTimeSettingsDefaultAddressPresenter a2 = A86.a(Y76.a(this.a), (An4) Kq4.this.c.get(), (UserRepository) Kq4.this.y.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressActivity c(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            Ms5.e(commuteTimeSettingsDefaultAddressActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(commuteTimeSettingsDefaultAddressActivity, (An4) Kq4.this.c.get());
            Ms5.a(commuteTimeSettingsDefaultAddressActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(commuteTimeSettingsDefaultAddressActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(commuteTimeSettingsDefaultAddressActivity, new Gu5());
            T76.a(commuteTimeSettingsDefaultAddressActivity, b());
            return commuteTimeSettingsDefaultAddressActivity;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter d(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
            B86.a(commuteTimeSettingsDefaultAddressPresenter);
            return commuteTimeSettingsDefaultAddressPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e1 implements Xl6 {
        @DexIgnore
        public /* final */ Bm6 a;

        @DexIgnore
        public e1(Bm6 bm6) {
            this.a = bm6;
        }

        @DexIgnore
        @Override // com.fossil.Xl6
        public void a(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            c(goalTrackingDetailActivity);
        }

        @DexIgnore
        public final GoalTrackingDetailPresenter b() {
            GoalTrackingDetailPresenter a2 = Em6.a(Cm6.a(this.a), (GoalTrackingRepository) Kq4.this.c0.get(), (U04) Kq4.this.G.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingDetailActivity c(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            Ms5.e(goalTrackingDetailActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(goalTrackingDetailActivity, (An4) Kq4.this.c.get());
            Ms5.a(goalTrackingDetailActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(goalTrackingDetailActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(goalTrackingDetailActivity, new Gu5());
            Wl6.a(goalTrackingDetailActivity, b());
            return goalTrackingDetailActivity;
        }

        @DexIgnore
        public final GoalTrackingDetailPresenter d(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            Fm6.a(goalTrackingDetailPresenter);
            return goalTrackingDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e2 implements Wx6 {
        @DexIgnore
        public /* final */ Zx6 a;

        @DexIgnore
        public e2(Zx6 zx6) {
            this.a = zx6;
        }

        @DexIgnore
        @Override // com.fossil.Wx6
        public void a(PairingInstructionsActivity pairingInstructionsActivity) {
            c(pairingInstructionsActivity);
        }

        @DexIgnore
        public final By6 b() {
            By6 a2 = Cy6.a(Ay6.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final PairingInstructionsActivity c(PairingInstructionsActivity pairingInstructionsActivity) {
            Ms5.e(pairingInstructionsActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(pairingInstructionsActivity, (An4) Kq4.this.c.get());
            Ms5.a(pairingInstructionsActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(pairingInstructionsActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(pairingInstructionsActivity, new Gu5());
            Vx6.a(pairingInstructionsActivity, b());
            return pairingInstructionsActivity;
        }

        @DexIgnore
        public final By6 d(By6 by6) {
            Dy6.a(by6);
            return by6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e3 implements La7 {
        @DexIgnore
        public e3() {
        }

        @DexIgnore
        @Override // com.fossil.La7
        public void a(MyFaceFragment myFaceFragment) {
            c(myFaceFragment);
        }

        @DexIgnore
        @Override // com.fossil.La7
        public void b(WatchFaceListFragment watchFaceListFragment) {
            d(watchFaceListFragment);
        }

        @DexIgnore
        public final MyFaceFragment c(MyFaceFragment myFaceFragment) {
            Ra7.a(myFaceFragment, (Po4) Kq4.this.A3.get());
            return myFaceFragment;
        }

        @DexIgnore
        public final WatchFaceListFragment d(WatchFaceListFragment watchFaceListFragment) {
            Na7.a(watchFaceListFragment, (Po4) Kq4.this.A3.get());
            return watchFaceListFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f0 implements Ja6 {
        @DexIgnore
        public f0() {
        }

        @DexIgnore
        @Override // com.fossil.Ja6
        public void a(Da6 da6) {
            b(da6);
        }

        @DexIgnore
        public final Da6 b(Da6 da6) {
            Fa6.a(da6, (Po4) Kq4.this.A3.get());
            return da6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f1 implements Ih6 {
        @DexIgnore
        public /* final */ Qh6 a;

        @DexIgnore
        public f1(Qh6 qh6) {
            this.a = qh6;
        }

        @DexIgnore
        @Override // com.fossil.Ih6
        public void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            f(goalTrackingOverviewFragment);
        }

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter b() {
            GoalTrackingOverviewDayPresenter a2 = Nh6.a(Rh6.a(this.a), (An4) Kq4.this.c.get(), (GoalTrackingRepository) Kq4.this.c0.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter c() {
            GoalTrackingOverviewMonthPresenter a2 = Yh6.a(Sh6.a(this.a), (UserRepository) Kq4.this.y.get(), (An4) Kq4.this.c.get(), (GoalTrackingRepository) Kq4.this.c0.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter d() {
            GoalTrackingOverviewWeekPresenter a2 = Ei6.a(Th6.a(this.a), (UserRepository) Kq4.this.y.get(), (An4) Kq4.this.c.get(), (GoalTrackingRepository) Kq4.this.c0.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter e(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            Oh6.a(goalTrackingOverviewDayPresenter);
            return goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewFragment f(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            Ph6.a(goalTrackingOverviewFragment, b());
            Ph6.c(goalTrackingOverviewFragment, d());
            Ph6.b(goalTrackingOverviewFragment, c());
            return goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter g(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            Zh6.a(goalTrackingOverviewMonthPresenter);
            return goalTrackingOverviewMonthPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter h(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
            Fi6.a(goalTrackingOverviewWeekPresenter);
            return goalTrackingOverviewWeekPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f2 implements Bz6 {
        @DexIgnore
        public /* final */ Gz6 a;

        @DexIgnore
        public f2(Gz6 gz6) {
            this.a = gz6;
        }

        @DexIgnore
        @Override // com.fossil.Bz6
        public void a(PermissionActivity permissionActivity) {
            c(permissionActivity);
        }

        @DexIgnore
        public final Jz6 b() {
            Jz6 a2 = Kz6.a(Iz6.a(this.a), Hz6.a(this.a), (An4) Kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final PermissionActivity c(PermissionActivity permissionActivity) {
            Ms5.e(permissionActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(permissionActivity, (An4) Kq4.this.c.get());
            Ms5.a(permissionActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(permissionActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(permissionActivity, new Gu5());
            Az6.a(permissionActivity, b());
            return permissionActivity;
        }

        @DexIgnore
        public final Jz6 d(Jz6 jz6) {
            Lz6.a(jz6);
            return jz6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f3 implements E97 {
        @DexIgnore
        public f3() {
        }

        @DexIgnore
        @Override // com.fossil.E97
        public void a(N97 n97) {
            b(n97);
        }

        @DexIgnore
        public final N97 b(N97 n97) {
            O97.a(n97, (Po4) Kq4.this.A3.get());
            return n97;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements Ma6 {
        @DexIgnore
        public g0() {
        }

        @DexIgnore
        @Override // com.fossil.Ma6
        public void a(Ga6 ga6) {
            b(ga6);
        }

        @DexIgnore
        public final Ga6 b(Ga6 ga6) {
            Ha6.a(ga6, (Po4) Kq4.this.A3.get());
            return ga6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g1 implements Hm6 {
        @DexIgnore
        public /* final */ Lm6 a;

        @DexIgnore
        public g1(Lm6 lm6) {
            this.a = lm6;
        }

        @DexIgnore
        @Override // com.fossil.Hm6
        public void a(HeartRateDetailActivity heartRateDetailActivity) {
            c(heartRateDetailActivity);
        }

        @DexIgnore
        public final HeartRateDetailPresenter b() {
            HeartRateDetailPresenter a2 = Om6.a(Mm6.a(this.a), (HeartRateSummaryRepository) Kq4.this.l0.get(), (HeartRateSampleRepository) Kq4.this.k0.get(), (UserRepository) Kq4.this.y.get(), (WorkoutSessionRepository) Kq4.this.m0.get(), (FileRepository) Kq4.this.I0.get(), (U04) Kq4.this.G.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateDetailActivity c(HeartRateDetailActivity heartRateDetailActivity) {
            Ms5.e(heartRateDetailActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(heartRateDetailActivity, (An4) Kq4.this.c.get());
            Ms5.a(heartRateDetailActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(heartRateDetailActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(heartRateDetailActivity, new Gu5());
            Gm6.a(heartRateDetailActivity, b());
            return heartRateDetailActivity;
        }

        @DexIgnore
        public final HeartRateDetailPresenter d(HeartRateDetailPresenter heartRateDetailPresenter) {
            Pm6.a(heartRateDetailPresenter);
            return heartRateDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g2 implements Vt6 {
        @DexIgnore
        public /* final */ Yt6 a;

        @DexIgnore
        public g2(Yt6 yt6) {
            this.a = yt6;
        }

        @DexIgnore
        @Override // com.fossil.Vt6
        public void a(PreferredUnitActivity preferredUnitActivity) {
            c(preferredUnitActivity);
        }

        @DexIgnore
        public final PreferredUnitPresenter b() {
            PreferredUnitPresenter a2 = Bu6.a(Zt6.a(this.a), (UserRepository) Kq4.this.y.get(), Kq4.this.Q4());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final PreferredUnitActivity c(PreferredUnitActivity preferredUnitActivity) {
            Ms5.e(preferredUnitActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(preferredUnitActivity, (An4) Kq4.this.c.get());
            Ms5.a(preferredUnitActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(preferredUnitActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(preferredUnitActivity, new Gu5());
            Ut6.a(preferredUnitActivity, b());
            return preferredUnitActivity;
        }

        @DexIgnore
        public final PreferredUnitPresenter d(PreferredUnitPresenter preferredUnitPresenter) {
            Cu6.a(preferredUnitPresenter);
            return preferredUnitPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g3 implements J87 {
        @DexIgnore
        public g3() {
        }

        @DexIgnore
        @Override // com.fossil.J87
        public void a(K87 k87) {
            b(k87);
        }

        @DexIgnore
        public final K87 b(K87 k87) {
            L87.a(k87, (Po4) Kq4.this.A3.get());
            return k87;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements B96 {
        @DexIgnore
        public /* final */ F96 a;

        @DexIgnore
        public h0(F96 f96) {
            this.a = f96;
        }

        @DexIgnore
        @Override // com.fossil.B96
        public void a(ComplicationSearchActivity complicationSearchActivity) {
            c(complicationSearchActivity);
        }

        @DexIgnore
        public final ComplicationSearchPresenter b() {
            ComplicationSearchPresenter a2 = I96.a(G96.a(this.a), Kq4.this.T3(), (An4) Kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ComplicationSearchActivity c(ComplicationSearchActivity complicationSearchActivity) {
            Ms5.e(complicationSearchActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(complicationSearchActivity, (An4) Kq4.this.c.get());
            Ms5.a(complicationSearchActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(complicationSearchActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(complicationSearchActivity, new Gu5());
            A96.a(complicationSearchActivity, b());
            return complicationSearchActivity;
        }

        @DexIgnore
        public final ComplicationSearchPresenter d(ComplicationSearchPresenter complicationSearchPresenter) {
            J96.a(complicationSearchPresenter);
            return complicationSearchPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h1 implements Mi6 {
        @DexIgnore
        public /* final */ Ui6 a;

        @DexIgnore
        public h1(Ui6 ui6) {
            this.a = ui6;
        }

        @DexIgnore
        @Override // com.fossil.Mi6
        public void a(HeartRateOverviewFragment heartRateOverviewFragment) {
            f(heartRateOverviewFragment);
        }

        @DexIgnore
        public final HeartRateOverviewDayPresenter b() {
            HeartRateOverviewDayPresenter a2 = Ri6.a(Vi6.a(this.a), (HeartRateSampleRepository) Kq4.this.k0.get(), (WorkoutSessionRepository) Kq4.this.m0.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter c() {
            HeartRateOverviewMonthPresenter a2 = Cj6.a(Wi6.a(this.a), (UserRepository) Kq4.this.y.get(), (HeartRateSummaryRepository) Kq4.this.l0.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter d() {
            HeartRateOverviewWeekPresenter a2 = Ij6.a(Xi6.a(this.a), (UserRepository) Kq4.this.y.get(), (HeartRateSummaryRepository) Kq4.this.l0.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewDayPresenter e(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            Si6.a(heartRateOverviewDayPresenter);
            return heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewFragment f(HeartRateOverviewFragment heartRateOverviewFragment) {
            Ti6.a(heartRateOverviewFragment, b());
            Ti6.c(heartRateOverviewFragment, d());
            Ti6.b(heartRateOverviewFragment, c());
            return heartRateOverviewFragment;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter g(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            Dj6.a(heartRateOverviewMonthPresenter);
            return heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter h(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
            Jj6.a(heartRateOverviewWeekPresenter);
            return heartRateOverviewWeekPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h2 implements Pq6 {
        @DexIgnore
        public /* final */ Tq6 a;

        @DexIgnore
        public h2(Tq6 tq6) {
            this.a = tq6;
        }

        @DexIgnore
        @Override // com.fossil.Pq6
        public void a(ProfileChangePasswordActivity profileChangePasswordActivity) {
            d(profileChangePasswordActivity);
        }

        @DexIgnore
        public final Nu5 b() {
            return new Nu5((AuthApiUserService) Kq4.this.u.get());
        }

        @DexIgnore
        public final Vq6 c() {
            Vq6 a2 = Wq6.a(Uq6.a(this.a), b(), (Uq4) Kq4.this.R.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileChangePasswordActivity d(ProfileChangePasswordActivity profileChangePasswordActivity) {
            Ms5.e(profileChangePasswordActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(profileChangePasswordActivity, (An4) Kq4.this.c.get());
            Ms5.a(profileChangePasswordActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(profileChangePasswordActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(profileChangePasswordActivity, new Gu5());
            Oq6.a(profileChangePasswordActivity, c());
            return profileChangePasswordActivity;
        }

        @DexIgnore
        public final Vq6 e(Vq6 vq6) {
            Xq6.a(vq6);
            return vq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h3 implements R97 {
        @DexIgnore
        public h3() {
        }

        @DexIgnore
        @Override // com.fossil.R97
        public void a(S97 s97) {
            b(s97);
        }

        @DexIgnore
        public final S97 b(S97 s97) {
            T97.a(s97, (Po4) Kq4.this.A3.get());
            return s97;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i0 implements O66 {
        @DexIgnore
        public /* final */ P66 a;

        @DexIgnore
        public i0(P66 p66) {
            this.a = p66;
        }

        @DexIgnore
        @Override // com.fossil.O66
        public void a(WatchAppEditFragment watchAppEditFragment) {
            c(watchAppEditFragment);
        }

        @DexIgnore
        public final WatchAppsPresenter b() {
            WatchAppsPresenter a2 = Ba6.a(Q66.a(this.a), Kq4.this.Q3(), (An4) Kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppEditFragment c(WatchAppEditFragment watchAppEditFragment) {
            G76.a(watchAppEditFragment, b());
            G76.b(watchAppEditFragment, (Po4) Kq4.this.A3.get());
            return watchAppEditFragment;
        }

        @DexIgnore
        public final WatchAppsPresenter d(WatchAppsPresenter watchAppsPresenter) {
            Ca6.a(watchAppsPresenter);
            return watchAppsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i1 implements Mp6 {
        @DexIgnore
        public /* final */ Qp6 a;

        @DexIgnore
        public i1(Qp6 qp6) {
            this.a = qp6;
        }

        @DexIgnore
        @Override // com.fossil.Mp6
        public void a(HelpActivity helpActivity) {
            d(helpActivity);
        }

        @DexIgnore
        public final GetZendeskInformation b() {
            return new GetZendeskInformation((UserRepository) Kq4.this.y.get(), (DeviceRepository) Kq4.this.P.get(), (An4) Kq4.this.c.get());
        }

        @DexIgnore
        public final HelpPresenter c() {
            HelpPresenter a2 = Tp6.a(Rp6.a(this.a), (DeviceRepository) Kq4.this.P.get(), b(), (AnalyticsHelper) Kq4.this.P0.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final HelpActivity d(HelpActivity helpActivity) {
            Ms5.e(helpActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(helpActivity, (An4) Kq4.this.c.get());
            Ms5.a(helpActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(helpActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(helpActivity, new Gu5());
            Lp6.a(helpActivity, c());
            return helpActivity;
        }

        @DexIgnore
        public final HelpPresenter e(HelpPresenter helpPresenter) {
            Up6.a(helpPresenter);
            return helpPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i2 implements Dp6 {
        @DexIgnore
        public i2() {
        }

        @DexIgnore
        @Override // com.fossil.Dp6
        public void a(Brown brown) {
            b(brown);
        }

        @DexIgnore
        public final Brown b(Brown brown) {
            Gp6.a(brown, (Po4) Kq4.this.A3.get());
            return brown;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i3 implements Y97 {
        @DexIgnore
        public i3() {
        }

        @DexIgnore
        @Override // com.fossil.Y97
        public void a(Z97 z97) {
            b(z97);
        }

        @DexIgnore
        public final Z97 b(Z97 z97) {
            Aa7.a(z97, (Po4) Kq4.this.A3.get());
            return z97;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j0 implements Ay5 {
        @DexIgnore
        public /* final */ Dy5 a;

        @DexIgnore
        public j0(Dy5 dy5) {
            this.a = dy5;
        }

        @DexIgnore
        @Override // com.fossil.Ay5
        public void a(ConnectedAppsActivity connectedAppsActivity) {
            c(connectedAppsActivity);
        }

        @DexIgnore
        public final ConnectedAppsPresenter b() {
            ConnectedAppsPresenter a2 = Gy5.a(Ey5.a(this.a), (UserRepository) Kq4.this.y.get(), (PortfolioApp) Kq4.this.d.get(), Kq4.this.w4());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ConnectedAppsActivity c(ConnectedAppsActivity connectedAppsActivity) {
            Ms5.e(connectedAppsActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(connectedAppsActivity, (An4) Kq4.this.c.get());
            Ms5.a(connectedAppsActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(connectedAppsActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(connectedAppsActivity, new Gu5());
            Zx5.a(connectedAppsActivity, b());
            return connectedAppsActivity;
        }

        @DexIgnore
        public final ConnectedAppsPresenter d(ConnectedAppsPresenter connectedAppsPresenter) {
            Hy5.a(connectedAppsPresenter);
            return connectedAppsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j1 implements Yz5 {
        @DexIgnore
        public /* final */ D06 a;

        @DexIgnore
        public j1(D06 d06) {
            this.a = d06;
        }

        @DexIgnore
        @Override // com.fossil.Yz5
        public void a(HomeAlertsFragment homeAlertsFragment) {
            d(homeAlertsFragment);
        }

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter b() {
            DoNotDisturbScheduledTimePresenter a2 = T36.a(E06.a(this.a), (DNDSettingsDatabase) Kq4.this.e.get());
            c(a2);
            return a2;
        }

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter c(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter) {
            U36.a(doNotDisturbScheduledTimePresenter);
            return doNotDisturbScheduledTimePresenter;
        }

        @DexIgnore
        public final HomeAlertsFragment d(HomeAlertsFragment homeAlertsFragment) {
            C06.a(homeAlertsFragment, b());
            return homeAlertsFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j2 implements Fq6 {
        @DexIgnore
        public /* final */ Jq6 a;

        @DexIgnore
        public j2(Jq6 jq6) {
            this.a = jq6;
        }

        @DexIgnore
        @Override // com.fossil.Fq6
        public void a(ProfileOptInActivity profileOptInActivity) {
            c(profileOptInActivity);
        }

        @DexIgnore
        public final ProfileOptInPresenter b() {
            ProfileOptInPresenter a2 = Mq6.a(Kq6.a(this.a), Kq4.this.Q4(), (UserRepository) Kq4.this.y.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileOptInActivity c(ProfileOptInActivity profileOptInActivity) {
            Ms5.e(profileOptInActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(profileOptInActivity, (An4) Kq4.this.c.get());
            Ms5.a(profileOptInActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(profileOptInActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(profileOptInActivity, new Gu5());
            Eq6.a(profileOptInActivity, b());
            return profileOptInActivity;
        }

        @DexIgnore
        public final ProfileOptInPresenter d(ProfileOptInPresenter profileOptInPresenter) {
            Nq6.a(profileOptInPresenter);
            return profileOptInPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j3 implements Da7 {
        @DexIgnore
        public j3() {
        }

        @DexIgnore
        @Override // com.fossil.Da7
        public void a(Ea7 ea7) {
            b(ea7);
        }

        @DexIgnore
        public final Ea7 b(Ea7 ea7) {
            Fa7.a(ea7, (Po4) Kq4.this.A3.get());
            return ea7;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k0 implements Er6 {
        @DexIgnore
        public k0(Hr6 hr6) {
        }

        @DexIgnore
        @Override // com.fossil.Er6
        public void a(Fr6 fr6) {
            b(fr6);
        }

        @DexIgnore
        public final Fr6 b(Fr6 fr6) {
            Gr6.a(fr6, (Po4) Kq4.this.A3.get());
            return fr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k1 implements Xy5 {
        @DexIgnore
        public /* final */ Hz5 a;

        @DexIgnore
        public k1(Hz5 hz5) {
            this.a = hz5;
        }

        @DexIgnore
        @Override // com.fossil.Xy5
        public void a(HomeActivity homeActivity) {
            f(homeActivity);
        }

        @DexIgnore
        public final Bv5 b() {
            return new Bv5(e());
        }

        @DexIgnore
        public final GetZendeskInformation c() {
            return new GetZendeskInformation((UserRepository) Kq4.this.y.get(), (DeviceRepository) Kq4.this.P.get(), (An4) Kq4.this.c.get());
        }

        @DexIgnore
        public final HomePresenter d() {
            HomePresenter a2 = Kz5.a(Iz5.a(this.a), (An4) Kq4.this.c.get(), (DeviceRepository) Kq4.this.P.get(), (PortfolioApp) Kq4.this.d.get(), Kq4.this.P4(), (Uq4) Kq4.this.R.get(), Kq4.this.a4(), b(), (N47) Kq4.this.E3.get(), e(), c(), (FlagRepository) Kq4.this.u1.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository e() {
            return new ServerSettingRepository((ServerSettingDataSource) Kq4.this.C3.get(), (ServerSettingDataSource) Kq4.this.D3.get());
        }

        @DexIgnore
        public final HomeActivity f(HomeActivity homeActivity) {
            Ms5.e(homeActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(homeActivity, (An4) Kq4.this.c.get());
            Ms5.a(homeActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(homeActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(homeActivity, new Gu5());
            Wy5.a(homeActivity, d());
            return homeActivity;
        }

        @DexIgnore
        public final HomePresenter g(HomePresenter homePresenter) {
            Lz5.a(homePresenter);
            return homePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k2 implements Xw6 {
        @DexIgnore
        public /* final */ Ax6 a;

        @DexIgnore
        public k2(Ax6 ax6) {
            this.a = ax6;
        }

        @DexIgnore
        @Override // com.fossil.Xw6
        public void a(ProfileSetupActivity profileSetupActivity) {
            e(profileSetupActivity);
        }

        @DexIgnore
        public final GetRecommendedGoalUseCase b() {
            return new GetRecommendedGoalUseCase((SummariesRepository) Kq4.this.D.get(), (SleepSummariesRepository) Kq4.this.E.get(), (SleepSessionsRepository) Kq4.this.b0.get());
        }

        @DexIgnore
        public final ProfileSetupPresenter c() {
            ProfileSetupPresenter a2 = Dx6.a(Bx6.a(this.a), b(), (UserRepository) Kq4.this.y.get(), (DeviceRepository) Kq4.this.P.get(), d(), (FlagRepository) Kq4.this.u1.get(), (An4) Kq4.this.c.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository d() {
            return new ServerSettingRepository((ServerSettingDataSource) Kq4.this.C3.get(), (ServerSettingDataSource) Kq4.this.D3.get());
        }

        @DexIgnore
        public final ProfileSetupActivity e(ProfileSetupActivity profileSetupActivity) {
            Ms5.e(profileSetupActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(profileSetupActivity, (An4) Kq4.this.c.get());
            Ms5.a(profileSetupActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(profileSetupActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(profileSetupActivity, new Gu5());
            Ww6.a(profileSetupActivity, c());
            return profileSetupActivity;
        }

        @DexIgnore
        public final ProfileSetupPresenter f(ProfileSetupPresenter profileSetupPresenter) {
            Ex6.c(profileSetupPresenter, Kq4.this.L4());
            Ex6.d(profileSetupPresenter, Kq4.this.M4());
            Ex6.b(profileSetupPresenter, Kq4.this.u4());
            Ex6.a(profileSetupPresenter, (AnalyticsHelper) Kq4.this.P0.get());
            Ex6.e(profileSetupPresenter);
            return profileSetupPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k3 implements C17 {
        @DexIgnore
        public k3() {
        }

        @DexIgnore
        @Override // com.fossil.C17
        public void a(WatchSettingFragment watchSettingFragment) {
            b(watchSettingFragment);
        }

        @DexIgnore
        public final WatchSettingFragment b(WatchSettingFragment watchSettingFragment) {
            E17.a(watchSettingFragment, (Po4) Kq4.this.A3.get());
            return watchSettingFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l0 implements Kr6 {
        @DexIgnore
        public l0(Nr6 nr6) {
        }

        @DexIgnore
        @Override // com.fossil.Kr6
        public void a(Lr6 lr6) {
            b(lr6);
        }

        @DexIgnore
        public final Lr6 b(Lr6 lr6) {
            Mr6.a(lr6, (Po4) Kq4.this.A3.get());
            return lr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l1 implements Nz5 {
        @DexIgnore
        public /* final */ Oz5 a;

        @DexIgnore
        public l1(Oz5 oz5) {
            this.a = oz5;
        }

        @DexIgnore
        @Override // com.fossil.Nz5
        public void a(HomeFragment homeFragment) {
            r(homeFragment);
        }

        @DexIgnore
        public final HomeAlertsHybridPresenter b() {
            HomeAlertsHybridPresenter a2 = B46.a(Pz5.a(this.a), (AlarmHelper) Kq4.this.B.get(), j(), (AlarmsRepository) Kq4.this.A.get(), (An4) Kq4.this.c.get());
            n(a2);
            return a2;
        }

        @DexIgnore
        public final HomeAlertsPresenter c() {
            HomeAlertsPresenter a2 = G06.a(Qz5.a(this.a), (Uq4) Kq4.this.R.get(), (AlarmHelper) Kq4.this.B.get(), new V36(), new D26(), i(), (NotificationSettingsDatabase) Kq4.this.j.get(), j(), (AlarmsRepository) Kq4.this.A.get(), (An4) Kq4.this.c.get(), (DNDSettingsDatabase) Kq4.this.e.get());
            o(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDashboardPresenter d() {
            HomeDashboardPresenter a2 = Nd6.a(Rz5.a(this.a), (PortfolioApp) Kq4.this.d.get(), (DeviceRepository) Kq4.this.P.get(), Kq4.this.a4(), (SummariesRepository) Kq4.this.D.get(), (GoalTrackingRepository) Kq4.this.c0.get(), (SleepSummariesRepository) Kq4.this.E.get(), (An4) Kq4.this.c.get(), (HeartRateSampleRepository) Kq4.this.k0.get(), (Tt4) Kq4.this.A0.get());
            p(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter e() {
            HomeDianaCustomizePresenter a2 = A76.a(Sz5.a(this.a), Kq4.this.U4(), Kq4.this.T3(), m(), (UserRepository) Kq4.this.y.get(), Kq4.this.V3(), (FileRepository) Kq4.this.I0.get(), (CustomizeRealDataRepository) Kq4.this.t.get(), (UserRepository) Kq4.this.y.get(), Kq4.this.e4(), (PortfolioApp) Kq4.this.d.get(), (DianaPresetRepository) Kq4.this.M0.get(), (WFAssetRepository) Kq4.this.d1.get(), l());
            q(a2);
            return a2;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter f() {
            HomeHybridCustomizePresenter a2 = Nb6.a((PortfolioApp) Kq4.this.d.get(), Tz5.a(this.a), (MicroAppRepository) Kq4.this.i0.get(), (HybridPresetRepository) Kq4.this.V.get(), k(), (An4) Kq4.this.c.get());
            s(a2);
            return a2;
        }

        @DexIgnore
        public final HomeProfilePresenter g() {
            HomeProfilePresenter a2 = Co6.a(Uz5.a(this.a), (PortfolioApp) Kq4.this.d.get(), Kq4.this.u4(), Kq4.this.Q4(), (DeviceRepository) Kq4.this.P.get(), (UserRepository) Kq4.this.y.get(), (SummariesRepository) Kq4.this.D.get(), (SleepSummariesRepository) Kq4.this.E.get(), Kq4.this.Z3(), (ProfileRepository) Kq4.this.s0.get(), (Tt4) Kq4.this.A0.get(), (An4) Kq4.this.c.get());
            t(a2);
            return a2;
        }

        @DexIgnore
        public final Un6 h() {
            Un6 a2 = Vn6.a(Vz5.a(this.a));
            u(a2);
            return a2;
        }

        @DexIgnore
        public final U06 i() {
            return new U06((NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final SetAlarms j() {
            return new SetAlarms((PortfolioApp) Kq4.this.d.get(), (AlarmsRepository) Kq4.this.A.get());
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase k() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) Kq4.this.P.get(), (HybridPresetRepository) Kq4.this.V.get(), (MicroAppRepository) Kq4.this.i0.get(), Kq4.this.F4());
        }

        @DexIgnore
        public final SetPresetUseCase l() {
            return new SetPresetUseCase((DianaPresetRepository) Kq4.this.M0.get(), (FileRepository) Kq4.this.I0.get(), (An4) Kq4.this.c.get(), (WFAssetRepository) Kq4.this.d1.get(), Kq4.this.b4());
        }

        @DexIgnore
        public final SetWatchAppUseCase m() {
            return new SetWatchAppUseCase((DianaPresetRepository) Kq4.this.M0.get(), (FileRepository) Kq4.this.I0.get(), Kq4.this.b4(), Kq4.this.b4());
        }

        @DexIgnore
        public final HomeAlertsHybridPresenter n(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            C46.a(homeAlertsHybridPresenter);
            return homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final HomeAlertsPresenter o(HomeAlertsPresenter homeAlertsPresenter) {
            H06.a(homeAlertsPresenter);
            return homeAlertsPresenter;
        }

        @DexIgnore
        public final HomeDashboardPresenter p(HomeDashboardPresenter homeDashboardPresenter) {
            Od6.a(homeDashboardPresenter);
            return homeDashboardPresenter;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter q(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            B76.a(homeDianaCustomizePresenter);
            return homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final HomeFragment r(HomeFragment homeFragment) {
            Fz5.c(homeFragment, d());
            Fz5.d(homeFragment, e());
            Fz5.e(homeFragment, f());
            Fz5.f(homeFragment, g());
            Fz5.b(homeFragment, c());
            Fz5.a(homeFragment, b());
            Fz5.g(homeFragment, h());
            return homeFragment;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter s(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            Ob6.a(homeHybridCustomizePresenter);
            return homeHybridCustomizePresenter;
        }

        @DexIgnore
        public final HomeProfilePresenter t(HomeProfilePresenter homeProfilePresenter) {
            Do6.a(homeProfilePresenter);
            return homeProfilePresenter;
        }

        @DexIgnore
        public final Un6 u(Un6 un6) {
            Wn6.a(un6);
            return un6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l2 implements I26 {
        @DexIgnore
        public l2() {
        }

        @DexIgnore
        @Override // com.fossil.I26
        public void a(L26 l26) {
            b(l26);
        }

        @DexIgnore
        public final L26 b(L26 l26) {
            M26.a(l26, (Po4) Kq4.this.A3.get());
            return l26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l3 implements Qa6 {
        @DexIgnore
        public /* final */ Ta6 a;

        @DexIgnore
        public l3(Ta6 ta6) {
            this.a = ta6;
        }

        @DexIgnore
        @Override // com.fossil.Qa6
        public void a(WeatherSettingActivity weatherSettingActivity) {
            c(weatherSettingActivity);
        }

        @DexIgnore
        public final WeatherSettingPresenter b() {
            WeatherSettingPresenter a2 = Wa6.a(Ua6.a(this.a), (GoogleApiService) Kq4.this.f2.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WeatherSettingActivity c(WeatherSettingActivity weatherSettingActivity) {
            Ms5.e(weatherSettingActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(weatherSettingActivity, (An4) Kq4.this.c.get());
            Ms5.a(weatherSettingActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(weatherSettingActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(weatherSettingActivity, new Gu5());
            Pa6.a(weatherSettingActivity, b());
            return weatherSettingActivity;
        }

        @DexIgnore
        public final WeatherSettingPresenter d(WeatherSettingPresenter weatherSettingPresenter) {
            Xa6.a(weatherSettingPresenter);
            return weatherSettingPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m0 implements Qr6 {
        @DexIgnore
        public m0(Tr6 tr6) {
        }

        @DexIgnore
        @Override // com.fossil.Qr6
        public void a(Rr6 rr6) {
            b(rr6);
        }

        @DexIgnore
        public final Rr6 b(Rr6 rr6) {
            Sr6.a(rr6, (Po4) Kq4.this.A3.get());
            return rr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m1 implements Xb6 {
        @DexIgnore
        public /* final */ Cc6 a;

        @DexIgnore
        public m1(Cc6 cc6) {
            this.a = cc6;
        }

        @DexIgnore
        @Override // com.fossil.Xb6
        public void a(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            d(hybridCustomizeEditActivity);
        }

        @DexIgnore
        public final HybridCustomizeEditPresenter b() {
            HybridCustomizeEditPresenter a2 = Fc6.a(Dc6.a(this.a), c());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase c() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) Kq4.this.P.get(), (HybridPresetRepository) Kq4.this.V.get(), (MicroAppRepository) Kq4.this.i0.get(), Kq4.this.F4());
        }

        @DexIgnore
        public final HybridCustomizeEditActivity d(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            Ms5.e(hybridCustomizeEditActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(hybridCustomizeEditActivity, (An4) Kq4.this.c.get());
            Ms5.a(hybridCustomizeEditActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(hybridCustomizeEditActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(hybridCustomizeEditActivity, new Gu5());
            Wb6.a(hybridCustomizeEditActivity, b());
            Wb6.b(hybridCustomizeEditActivity, (Po4) Kq4.this.A3.get());
            return hybridCustomizeEditActivity;
        }

        @DexIgnore
        public final HybridCustomizeEditPresenter e(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            Gc6.a(hybridCustomizeEditPresenter);
            return hybridCustomizeEditPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m2 implements M36 {
        @DexIgnore
        public /* final */ N36 a;

        @DexIgnore
        public m2(N36 n36) {
            this.a = n36;
        }

        @DexIgnore
        @Override // com.fossil.M36
        public void a(Z26 z26) {
            e(z26);
        }

        @DexIgnore
        public final InactivityNudgeTimePresenter b() {
            InactivityNudgeTimePresenter a2 = T26.a(O36.a(this.a), (RemindersSettingsDatabase) Kq4.this.n0.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final RemindTimePresenter c() {
            RemindTimePresenter a2 = K36.a(P36.a(this.a), (RemindersSettingsDatabase) Kq4.this.n0.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final InactivityNudgeTimePresenter d(InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
            U26.a(inactivityNudgeTimePresenter);
            return inactivityNudgeTimePresenter;
        }

        @DexIgnore
        public final Z26 e(Z26 z26) {
            A36.a(z26, b());
            A36.b(z26, c());
            return z26;
        }

        @DexIgnore
        public final RemindTimePresenter f(RemindTimePresenter remindTimePresenter) {
            L36.a(remindTimePresenter);
            return remindTimePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m3 implements E27 {
        @DexIgnore
        public /* final */ J27 a;

        @DexIgnore
        public m3(J27 j27) {
            this.a = j27;
        }

        @DexIgnore
        @Override // com.fossil.E27
        public void a(WelcomeActivity welcomeActivity) {
            c(welcomeActivity);
        }

        @DexIgnore
        public final WelcomePresenter b() {
            WelcomePresenter a2 = L27.a(K27.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WelcomeActivity c(WelcomeActivity welcomeActivity) {
            Ms5.e(welcomeActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(welcomeActivity, (An4) Kq4.this.c.get());
            Ms5.a(welcomeActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(welcomeActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(welcomeActivity, new Gu5());
            D27.a(welcomeActivity, b());
            return welcomeActivity;
        }

        @DexIgnore
        public final WelcomePresenter d(WelcomePresenter welcomePresenter) {
            M27.a(welcomePresenter);
            return welcomePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n0 implements Wr6 {
        @DexIgnore
        public n0(Zr6 zr6) {
        }

        @DexIgnore
        @Override // com.fossil.Wr6
        public void a(Xr6 xr6) {
            b(xr6);
        }

        @DexIgnore
        public final Xr6 b(Xr6 xr6) {
            Yr6.a(xr6, (Po4) Kq4.this.A3.get());
            return xr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n1 implements Pb6 {
        @DexIgnore
        public /* final */ Qb6 a;

        @DexIgnore
        public n1(Qb6 qb6) {
            this.a = qb6;
        }

        @DexIgnore
        @Override // com.fossil.Pb6
        public void a(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            c(dianaCustomizeEditFragment);
        }

        @DexIgnore
        public final MicroAppPresenter b() {
            MicroAppPresenter a2 = Mc6.a(Rb6.a(this.a), Kq4.this.Q3());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final DianaCustomizeEditFragment c(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            Bc6.a(dianaCustomizeEditFragment, b());
            Bc6.b(dianaCustomizeEditFragment, (Po4) Kq4.this.A3.get());
            return dianaCustomizeEditFragment;
        }

        @DexIgnore
        public final MicroAppPresenter d(MicroAppPresenter microAppPresenter) {
            Nc6.a(microAppPresenter);
            return microAppPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n2 implements Po6 {
        @DexIgnore
        public /* final */ To6 a;

        @DexIgnore
        public n2(To6 to6) {
            this.a = to6;
        }

        @DexIgnore
        @Override // com.fossil.Po6
        public void a(ReplaceBatteryActivity replaceBatteryActivity) {
            c(replaceBatteryActivity);
        }

        @DexIgnore
        public final Vo6 b() {
            Vo6 a2 = Wo6.a(Uo6.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final ReplaceBatteryActivity c(ReplaceBatteryActivity replaceBatteryActivity) {
            Ms5.e(replaceBatteryActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(replaceBatteryActivity, (An4) Kq4.this.c.get());
            Ms5.a(replaceBatteryActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(replaceBatteryActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(replaceBatteryActivity, new Gu5());
            Oo6.a(replaceBatteryActivity, b());
            return replaceBatteryActivity;
        }

        @DexIgnore
        public final Vo6 d(Vo6 vo6) {
            Xo6.a(vo6);
            return vo6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n3 implements En6 {
        @DexIgnore
        public n3() {
        }

        @DexIgnore
        @Override // com.fossil.En6
        public void a(Fn6 fn6) {
            b(fn6);
        }

        @DexIgnore
        public final Fn6 b(Fn6 fn6) {
            Hn6.a(fn6, (Po4) Kq4.this.A3.get());
            return fn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o0 implements Cs6 {
        @DexIgnore
        public o0(Fs6 fs6) {
        }

        @DexIgnore
        @Override // com.fossil.Cs6
        public void a(Ds6 ds6) {
            b(ds6);
        }

        @DexIgnore
        public final Ds6 b(Ds6 ds6) {
            Es6.a(ds6, (Po4) Kq4.this.A3.get());
            return ds6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o1 implements Tu6 {
        @DexIgnore
        public /* final */ Xu6 a;

        @DexIgnore
        public o1(Xu6 xu6) {
            this.a = xu6;
        }

        @DexIgnore
        @Override // com.fossil.Tu6
        public void a(LoginActivity loginActivity) {
            c(loginActivity);
        }

        @DexIgnore
        public final LoginPresenter b() {
            LoginPresenter a2 = Bv6.a(Zu6.a(this.a), Yu6.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final LoginActivity c(LoginActivity loginActivity) {
            Ms5.e(loginActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(loginActivity, (An4) Kq4.this.c.get());
            Ms5.a(loginActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(loginActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(loginActivity, new Gu5());
            Su6.a(loginActivity, (Un5) Kq4.this.U1.get());
            Su6.b(loginActivity, (Vn5) Kq4.this.d0.get());
            Su6.d(loginActivity, (Xn5) Kq4.this.V1.get());
            Su6.c(loginActivity, (MFLoginWechatManager) Kq4.this.W1.get());
            Su6.e(loginActivity, b());
            return loginActivity;
        }

        @DexIgnore
        public final LoginPresenter d(LoginPresenter loginPresenter) {
            Cv6.s(loginPresenter, Kq4.this.y4());
            Cv6.v(loginPresenter, Kq4.this.B4());
            Cv6.h(loginPresenter, Kq4.this.f4());
            Cv6.y(loginPresenter, new ReconnectDeviceUseCase());
            Cv6.g(loginPresenter, Kq4.this.a4());
            Cv6.D(loginPresenter, (UserRepository) Kq4.this.y.get());
            Cv6.f(loginPresenter, (DeviceRepository) Kq4.this.P.get());
            Cv6.z(loginPresenter, (An4) Kq4.this.c.get());
            Cv6.i(loginPresenter, Kq4.this.h4());
            Cv6.p(loginPresenter, Kq4.this.o4());
            Cv6.C(loginPresenter, (Uq4) Kq4.this.R.get());
            Cv6.n(loginPresenter, Kq4.this.m4());
            Cv6.o(loginPresenter, Kq4.this.n4());
            Cv6.m(loginPresenter, Kq4.this.l4());
            Cv6.k(loginPresenter, Kq4.this.j4());
            Cv6.t(loginPresenter, Kq4.this.z4());
            Cv6.G(loginPresenter, (Xn5) Kq4.this.V1.get());
            Cv6.u(loginPresenter, Kq4.this.A4());
            Cv6.x(loginPresenter, Kq4.this.D4());
            Cv6.w(loginPresenter, Kq4.this.C4());
            Cv6.e(loginPresenter, Kq4.this.S3());
            Cv6.d(loginPresenter, (AnalyticsHelper) Kq4.this.P0.get());
            Cv6.B(loginPresenter, (SummariesRepository) Kq4.this.D.get());
            Cv6.A(loginPresenter, (SleepSummariesRepository) Kq4.this.E.get());
            Cv6.r(loginPresenter, (GoalTrackingRepository) Kq4.this.c0.get());
            Cv6.j(loginPresenter, Kq4.this.i4());
            Cv6.l(loginPresenter, Kq4.this.k4());
            Cv6.q(loginPresenter, Kq4.this.t4());
            Cv6.E(loginPresenter, Kq4.this.X4());
            Cv6.c(loginPresenter, (AlarmsRepository) Kq4.this.A.get());
            Cv6.I(loginPresenter, (ProfileRepository) Kq4.this.s0.get());
            Cv6.a(loginPresenter, (FCMRepository) Kq4.this.J0.get());
            Cv6.b(loginPresenter, (FlagRepository) Kq4.this.u1.get());
            Cv6.F(loginPresenter, Kq4.this.Z4());
            Cv6.H(loginPresenter);
            return loginPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o2 implements Pc6 {
        @DexIgnore
        public /* final */ Tc6 a;

        @DexIgnore
        public o2(Tc6 tc6) {
            this.a = tc6;
        }

        @DexIgnore
        @Override // com.fossil.Pc6
        public void a(SearchMicroAppActivity searchMicroAppActivity) {
            c(searchMicroAppActivity);
        }

        @DexIgnore
        public final SearchMicroAppPresenter b() {
            SearchMicroAppPresenter a2 = Wc6.a(Uc6.a(this.a), (MicroAppRepository) Kq4.this.i0.get(), (An4) Kq4.this.c.get(), (PortfolioApp) Kq4.this.d.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SearchMicroAppActivity c(SearchMicroAppActivity searchMicroAppActivity) {
            Ms5.e(searchMicroAppActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(searchMicroAppActivity, (An4) Kq4.this.c.get());
            Ms5.a(searchMicroAppActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(searchMicroAppActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(searchMicroAppActivity, new Gu5());
            Oc6.a(searchMicroAppActivity, b());
            return searchMicroAppActivity;
        }

        @DexIgnore
        public final SearchMicroAppPresenter d(SearchMicroAppPresenter searchMicroAppPresenter) {
            Xc6.a(searchMicroAppPresenter);
            return searchMicroAppPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o3 implements Mn6 {
        @DexIgnore
        public o3() {
        }

        @DexIgnore
        @Override // com.fossil.Mn6
        public void a(Nn6 nn6) {
            b(nn6);
        }

        @DexIgnore
        public final Nn6 b(Nn6 nn6) {
            On6.a(nn6, (Po4) Kq4.this.A3.get());
            return nn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p0 implements Js6 {
        @DexIgnore
        public p0(Ms6 ms6) {
        }

        @DexIgnore
        @Override // com.fossil.Js6
        public void a(Ks6 ks6) {
            b(ks6);
        }

        @DexIgnore
        public final Ks6 b(Ks6 ks6) {
            Ls6.a(ks6, (Po4) Kq4.this.A3.get());
            return ks6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p1 implements Dv6 {
        @DexIgnore
        public p1() {
        }

        @DexIgnore
        @Override // com.fossil.Dv6
        public void a(MapPickerFragment mapPickerFragment) {
            b(mapPickerFragment);
        }

        @DexIgnore
        public final MapPickerFragment b(MapPickerFragment mapPickerFragment) {
            Fv6.a(mapPickerFragment, (Po4) Kq4.this.A3.get());
            return mapPickerFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p2 implements I86 {
        @DexIgnore
        public /* final */ M86 a;

        @DexIgnore
        public p2(M86 m86) {
            this.a = m86;
        }

        @DexIgnore
        @Override // com.fossil.I86
        public void a(SearchRingPhoneActivity searchRingPhoneActivity) {
            c(searchRingPhoneActivity);
        }

        @DexIgnore
        public final SearchRingPhonePresenter b() {
            SearchRingPhonePresenter a2 = P86.a(N86.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SearchRingPhoneActivity c(SearchRingPhoneActivity searchRingPhoneActivity) {
            Ms5.e(searchRingPhoneActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(searchRingPhoneActivity, (An4) Kq4.this.c.get());
            Ms5.a(searchRingPhoneActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(searchRingPhoneActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(searchRingPhoneActivity, new Gu5());
            H86.a(searchRingPhoneActivity, b());
            return searchRingPhoneActivity;
        }

        @DexIgnore
        public final SearchRingPhonePresenter d(SearchRingPhonePresenter searchRingPhonePresenter) {
            Q86.a(searchRingPhonePresenter);
            return searchRingPhonePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p3 implements Iu6 {
        @DexIgnore
        public p3() {
        }

        @DexIgnore
        @Override // com.fossil.Iu6
        public void a(WorkoutSettingFragment workoutSettingFragment) {
            b(workoutSettingFragment);
        }

        @DexIgnore
        public final WorkoutSettingFragment b(WorkoutSettingFragment workoutSettingFragment) {
            Lu6.a(workoutSettingFragment, (Po4) Kq4.this.A3.get());
            return workoutSettingFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q0 implements Ps6 {
        @DexIgnore
        public q0(Ss6 ss6) {
        }

        @DexIgnore
        @Override // com.fossil.Ps6
        public void a(Qs6 qs6) {
            b(qs6);
        }

        @DexIgnore
        public final Qs6 b(Qs6 qs6) {
            Rs6.a(qs6, (Po4) Kq4.this.A3.get());
            return qs6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q1 implements Hc6 {
        @DexIgnore
        public q1(Kc6 kc6) {
        }

        @DexIgnore
        @Override // com.fossil.Hc6
        public void a(Ub6 ub6) {
            b(ub6);
        }

        @DexIgnore
        public final Ub6 b(Ub6 ub6) {
            Vb6.a(ub6, (Po4) Kq4.this.A3.get());
            return ub6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q2 implements S86 {
        @DexIgnore
        public /* final */ V86 a;

        @DexIgnore
        public q2(V86 v86) {
            this.a = v86;
        }

        @DexIgnore
        @Override // com.fossil.S86
        public void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            c(searchSecondTimezoneActivity);
        }

        @DexIgnore
        public final SearchSecondTimezonePresenter b() {
            SearchSecondTimezonePresenter a2 = Y86.a(W86.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SearchSecondTimezoneActivity c(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            Ms5.e(searchSecondTimezoneActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(searchSecondTimezoneActivity, (An4) Kq4.this.c.get());
            Ms5.a(searchSecondTimezoneActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(searchSecondTimezoneActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(searchSecondTimezoneActivity, new Gu5());
            R86.a(searchSecondTimezoneActivity, b());
            return searchSecondTimezoneActivity;
        }

        @DexIgnore
        public final SearchSecondTimezonePresenter d(SearchSecondTimezonePresenter searchSecondTimezonePresenter) {
            Z86.a(searchSecondTimezonePresenter);
            return searchSecondTimezonePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r0 implements Vs6 {
        @DexIgnore
        public r0(Ys6 ys6) {
        }

        @DexIgnore
        @Override // com.fossil.Vs6
        public void a(Ws6 ws6) {
            b(ws6);
        }

        @DexIgnore
        public final Ws6 b(Ws6 ws6) {
            Xs6.a(ws6, (Po4) Kq4.this.A3.get());
            return ws6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r1 implements Iv6 {
        @DexIgnore
        public r1() {
        }

        @DexIgnore
        @Override // com.fossil.Iv6
        public void a(Jv6 jv6) {
            b(jv6);
        }

        @DexIgnore
        public final Jv6 b(Jv6 jv6) {
            Kv6.a(jv6, (Po4) Kq4.this.A3.get());
            return jv6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r2 implements Nz6 {
        @DexIgnore
        public /* final */ Rz6 a;

        @DexIgnore
        public r2(Rz6 rz6) {
            this.a = rz6;
        }

        @DexIgnore
        @Override // com.fossil.Nz6
        public void a(SignUpActivity signUpActivity) {
            c(signUpActivity);
        }

        @DexIgnore
        public final SignUpPresenter b() {
            SignUpPresenter a2 = Vz6.a(Tz6.a(this.a), Sz6.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SignUpActivity c(SignUpActivity signUpActivity) {
            Ms5.e(signUpActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(signUpActivity, (An4) Kq4.this.c.get());
            Ms5.a(signUpActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(signUpActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(signUpActivity, new Gu5());
            Mz6.e(signUpActivity, b());
            Mz6.a(signUpActivity, (Un5) Kq4.this.U1.get());
            Mz6.b(signUpActivity, (Vn5) Kq4.this.d0.get());
            Mz6.d(signUpActivity, (Xn5) Kq4.this.V1.get());
            Mz6.c(signUpActivity, (MFLoginWechatManager) Kq4.this.W1.get());
            return signUpActivity;
        }

        @DexIgnore
        public final SignUpPresenter d(SignUpPresenter signUpPresenter) {
            Wz6.u(signUpPresenter, Kq4.this.z4());
            Wz6.y(signUpPresenter, (Xn5) Kq4.this.V1.get());
            Wz6.v(signUpPresenter, Kq4.this.A4());
            Wz6.z(signUpPresenter, Kq4.this.D4());
            Wz6.x(signUpPresenter, Kq4.this.C4());
            Wz6.w(signUpPresenter, Kq4.this.B4());
            Wz6.H(signUpPresenter, (UserRepository) Kq4.this.y.get());
            Wz6.g(signUpPresenter, (DeviceRepository) Kq4.this.P.get());
            Wz6.G(signUpPresenter, (Uq4) Kq4.this.R.get());
            Wz6.o(signUpPresenter, Kq4.this.m4());
            Wz6.p(signUpPresenter, Kq4.this.n4());
            Wz6.j(signUpPresenter, Kq4.this.h4());
            Wz6.q(signUpPresenter, Kq4.this.o4());
            Wz6.n(signUpPresenter, Kq4.this.l4());
            Wz6.l(signUpPresenter, Kq4.this.j4());
            Wz6.c(signUpPresenter, (AlarmsRepository) Kq4.this.A.get());
            Wz6.A(signUpPresenter, new ReconnectDeviceUseCase());
            Wz6.h(signUpPresenter, Kq4.this.a4());
            Wz6.i(signUpPresenter, Kq4.this.f4());
            Wz6.D(signUpPresenter, (An4) Kq4.this.c.get());
            Wz6.e(signUpPresenter, Kq4.this.R3());
            Wz6.f(signUpPresenter, Kq4.this.S3());
            Wz6.d(signUpPresenter, (AnalyticsHelper) Kq4.this.P0.get());
            Wz6.s(signUpPresenter, Kq4.this.u4());
            Wz6.F(signUpPresenter, (SummariesRepository) Kq4.this.D.get());
            Wz6.E(signUpPresenter, (SleepSummariesRepository) Kq4.this.E.get());
            Wz6.t(signUpPresenter, (GoalTrackingRepository) Kq4.this.c0.get());
            Wz6.k(signUpPresenter, Kq4.this.i4());
            Wz6.m(signUpPresenter, Kq4.this.k4());
            Wz6.B(signUpPresenter, Kq4.this.G4());
            Wz6.r(signUpPresenter, Kq4.this.t4());
            Wz6.I(signUpPresenter, Kq4.this.X4());
            Wz6.C(signUpPresenter, (An4) Kq4.this.c.get());
            Wz6.a(signUpPresenter, (FCMRepository) Kq4.this.J0.get());
            Wz6.b(signUpPresenter, (FlagRepository) Kq4.this.u1.get());
            Wz6.J(signUpPresenter, Kq4.this.Z4());
            Wz6.K(signUpPresenter);
            return signUpPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s0 implements Bt6 {
        @DexIgnore
        public s0(Et6 et6) {
        }

        @DexIgnore
        @Override // com.fossil.Bt6
        public void a(Ct6 ct6) {
            b(ct6);
        }

        @DexIgnore
        public final Ct6 b(Ct6 ct6) {
            Dt6.a(ct6, (Po4) Kq4.this.A3.get());
            return ct6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s1 implements L06 {
        @DexIgnore
        public /* final */ P06 a;

        @DexIgnore
        public s1(P06 p06) {
            this.a = p06;
        }

        @DexIgnore
        @Override // com.fossil.L06
        public void a(NotificationAppsActivity notificationAppsActivity) {
            d(notificationAppsActivity);
        }

        @DexIgnore
        public final NotificationAppsPresenter b() {
            NotificationAppsPresenter a2 = S06.a(Q06.a(this.a), (Uq4) Kq4.this.R.get(), new V36(), new D26(), c(), (An4) Kq4.this.c.get(), (NotificationSettingsDatabase) Kq4.this.j.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final U06 c() {
            return new U06((NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationAppsActivity d(NotificationAppsActivity notificationAppsActivity) {
            Ms5.e(notificationAppsActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationAppsActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationAppsActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationAppsActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationAppsActivity, new Gu5());
            K06.a(notificationAppsActivity, b());
            return notificationAppsActivity;
        }

        @DexIgnore
        public final NotificationAppsPresenter e(NotificationAppsPresenter notificationAppsPresenter) {
            T06.a(notificationAppsPresenter);
            return notificationAppsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s2 implements Rm6 {
        @DexIgnore
        public /* final */ Vm6 a;

        @DexIgnore
        public s2(Vm6 vm6) {
            this.a = vm6;
        }

        @DexIgnore
        @Override // com.fossil.Rm6
        public void a(SleepDetailActivity sleepDetailActivity) {
            c(sleepDetailActivity);
        }

        @DexIgnore
        public final SleepDetailPresenter b() {
            SleepDetailPresenter a2 = Ym6.a(Wm6.a(this.a), (SleepSummariesRepository) Kq4.this.E.get(), (SleepSessionsRepository) Kq4.this.b0.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final SleepDetailActivity c(SleepDetailActivity sleepDetailActivity) {
            Ms5.e(sleepDetailActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(sleepDetailActivity, (An4) Kq4.this.c.get());
            Ms5.a(sleepDetailActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(sleepDetailActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(sleepDetailActivity, new Gu5());
            Qm6.a(sleepDetailActivity, b());
            return sleepDetailActivity;
        }

        @DexIgnore
        public final SleepDetailPresenter d(SleepDetailPresenter sleepDetailPresenter) {
            Zm6.a(sleepDetailPresenter);
            return sleepDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t0 implements Ht6 {
        @DexIgnore
        public t0(Kt6 kt6) {
        }

        @DexIgnore
        @Override // com.fossil.Ht6
        public void a(It6 it6) {
            b(it6);
        }

        @DexIgnore
        public final It6 b(It6 it6) {
            Jt6.a(it6, (Po4) Kq4.this.A3.get());
            return it6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t1 implements W06 {
        @DexIgnore
        public /* final */ B16 a;

        @DexIgnore
        public t1(B16 b16) {
            this.a = b16;
        }

        @DexIgnore
        @Override // com.fossil.W06
        public void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            e(notificationCallsAndMessagesActivity);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter b() {
            NotificationCallsAndMessagesPresenter a2 = E16.a(C16.a(this.a), (Uq4) Kq4.this.R.get(), new D26(), c(), d(), new V36(), (An4) Kq4.this.c.get(), (NotificationSettingsDao) Kq4.this.B3.get(), Kq4.this.K4(), (QuickResponseRepository) Kq4.this.i.get(), (NotificationSettingsDatabase) Kq4.this.j.get());
            f(a2);
            return a2;
        }

        @DexIgnore
        public final F26 c() {
            return new F26((NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final G26 d() {
            return new G26((NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationCallsAndMessagesActivity e(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            Ms5.e(notificationCallsAndMessagesActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationCallsAndMessagesActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationCallsAndMessagesActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationCallsAndMessagesActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationCallsAndMessagesActivity, new Gu5());
            V06.a(notificationCallsAndMessagesActivity, b());
            return notificationCallsAndMessagesActivity;
        }

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter f(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            F16.a(notificationCallsAndMessagesPresenter);
            return notificationCallsAndMessagesPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t2 implements Qj6 {
        @DexIgnore
        public /* final */ Yj6 a;

        @DexIgnore
        public t2(Yj6 yj6) {
            this.a = yj6;
        }

        @DexIgnore
        @Override // com.fossil.Qj6
        public void a(SleepOverviewFragment sleepOverviewFragment) {
            f(sleepOverviewFragment);
        }

        @DexIgnore
        public final SleepOverviewDayPresenter b() {
            SleepOverviewDayPresenter a2 = Vj6.a(Zj6.a(this.a), (SleepSummariesRepository) Kq4.this.E.get(), (SleepSessionsRepository) Kq4.this.b0.get(), (PortfolioApp) Kq4.this.d.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter c() {
            SleepOverviewMonthPresenter a2 = Gk6.a(Ak6.a(this.a), (UserRepository) Kq4.this.y.get(), (SleepSummariesRepository) Kq4.this.E.get(), (PortfolioApp) Kq4.this.d.get());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter d() {
            SleepOverviewWeekPresenter a2 = Mk6.a(Bk6.a(this.a), (UserRepository) Kq4.this.y.get(), (SleepSummariesRepository) Kq4.this.E.get(), (PortfolioApp) Kq4.this.d.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewDayPresenter e(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            Wj6.a(sleepOverviewDayPresenter);
            return sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final SleepOverviewFragment f(SleepOverviewFragment sleepOverviewFragment) {
            Xj6.a(sleepOverviewFragment, b());
            Xj6.c(sleepOverviewFragment, d());
            Xj6.b(sleepOverviewFragment, c());
            return sleepOverviewFragment;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter g(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            Hk6.a(sleepOverviewMonthPresenter);
            return sleepOverviewMonthPresenter;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter h(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
            Nk6.a(sleepOverviewWeekPresenter);
            return sleepOverviewWeekPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u0 implements Yc6 {
        @DexIgnore
        public u0() {
        }

        @DexIgnore
        @Override // com.fossil.Yc6
        public void a(S66 s66) {
            b(s66);
        }

        @DexIgnore
        public final S66 b(S66 s66) {
            T66.a(s66, (Po4) Kq4.this.A3.get());
            return s66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u1 implements F46 {
        @DexIgnore
        public /* final */ J46 a;

        @DexIgnore
        public u1(J46 j46) {
            this.a = j46;
        }

        @DexIgnore
        @Override // com.fossil.F46
        public void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            f(notificationContactsAndAppsAssignedActivity);
        }

        @DexIgnore
        public final L56 b() {
            return new L56((Context) Kq4.this.b.get());
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter c() {
            NotificationContactsAndAppsAssignedPresenter a2 = N46.a(K46.a(this.a), L46.a(this.a), this.a.a(), (Uq4) Kq4.this.R.get(), new Y56(), b(), d(), e());
            g(a2);
            return a2;
        }

        @DexIgnore
        public final K66 d() {
            return new K66((NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final SetNotificationFiltersUserCase e() {
            return new SetNotificationFiltersUserCase((NotificationsRepository) Kq4.this.I.get(), (DeviceRepository) Kq4.this.P.get());
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedActivity f(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            Ms5.e(notificationContactsAndAppsAssignedActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationContactsAndAppsAssignedActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationContactsAndAppsAssignedActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationContactsAndAppsAssignedActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationContactsAndAppsAssignedActivity, new Gu5());
            E46.a(notificationContactsAndAppsAssignedActivity, c());
            return notificationContactsAndAppsAssignedActivity;
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter g(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            O46.a(notificationContactsAndAppsAssignedPresenter);
            return notificationContactsAndAppsAssignedPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u2 implements H07 {
        @DexIgnore
        public /* final */ K07 a;

        @DexIgnore
        public u2(K07 k07) {
            this.a = k07;
        }

        @DexIgnore
        @Override // com.fossil.H07
        public void a(SplashScreenActivity splashScreenActivity) {
            d(splashScreenActivity);
        }

        @DexIgnore
        public final SplashPresenter b() {
            SplashPresenter a2 = N07.a(L07.a(this.a), (UserRepository) Kq4.this.y.get(), (MigrationHelper) Kq4.this.x3.get(), Kq4.this.a4(), (ThemeRepository) Kq4.this.y1.get());
            c(a2);
            return a2;
        }

        @DexIgnore
        public final SplashPresenter c(SplashPresenter splashPresenter) {
            O07.a(splashPresenter);
            return splashPresenter;
        }

        @DexIgnore
        public final SplashScreenActivity d(SplashScreenActivity splashScreenActivity) {
            Ms5.e(splashScreenActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(splashScreenActivity, (An4) Kq4.this.c.get());
            Ms5.a(splashScreenActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(splashScreenActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(splashScreenActivity, new Gu5());
            P07.a(splashScreenActivity, b());
            return splashScreenActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v0 implements Cd6 {
        @DexIgnore
        public /* final */ Dd6 a;

        @DexIgnore
        public v0(Dd6 dd6) {
            this.a = dd6;
        }

        @DexIgnore
        @Override // com.fossil.Cd6
        public void a(HomeDashboardFragment homeDashboardFragment) {
            n(homeDashboardFragment);
        }

        @DexIgnore
        public final DashboardActiveTimePresenter b() {
            DashboardActiveTimePresenter a2 = Td6.a(Ed6.a(this.a), (SummariesRepository) Kq4.this.D.get(), Kq4.this.p4(), (UserRepository) Kq4.this.y.get(), (U04) Kq4.this.G.get());
            h(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardActivityPresenter c() {
            DashboardActivityPresenter a2 = Xe6.a(Fd6.a(this.a), (SummariesRepository) Kq4.this.D.get(), Kq4.this.p4(), (UserRepository) Kq4.this.y.get(), (U04) Kq4.this.G.get());
            i(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter d() {
            DashboardCaloriesPresenter a2 = Cg6.a(Gd6.a(this.a), (SummariesRepository) Kq4.this.D.get(), Kq4.this.p4(), (UserRepository) Kq4.this.y.get(), (U04) Kq4.this.G.get());
            j(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter e() {
            DashboardGoalTrackingPresenter a2 = Gh6.a(Hd6.a(this.a), (GoalTrackingRepository) Kq4.this.c0.get(), (UserRepository) Kq4.this.y.get(), (U04) Kq4.this.G.get());
            k(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter f() {
            DashboardHeartRatePresenter a2 = Ki6.a(Id6.a(this.a), (HeartRateSummaryRepository) Kq4.this.l0.get(), Kq4.this.p4(), (UserRepository) Kq4.this.y.get(), (U04) Kq4.this.G.get());
            l(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardSleepPresenter g() {
            DashboardSleepPresenter a2 = Oj6.a(Jd6.a(this.a), (SleepSummariesRepository) Kq4.this.E.get(), (SleepSessionsRepository) Kq4.this.b0.get(), Kq4.this.p4(), (UserRepository) Kq4.this.y.get(), (U04) Kq4.this.G.get());
            m(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardActiveTimePresenter h(DashboardActiveTimePresenter dashboardActiveTimePresenter) {
            Ud6.a(dashboardActiveTimePresenter);
            return dashboardActiveTimePresenter;
        }

        @DexIgnore
        public final DashboardActivityPresenter i(DashboardActivityPresenter dashboardActivityPresenter) {
            Ye6.a(dashboardActivityPresenter);
            return dashboardActivityPresenter;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter j(DashboardCaloriesPresenter dashboardCaloriesPresenter) {
            Dg6.a(dashboardCaloriesPresenter);
            return dashboardCaloriesPresenter;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter k(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
            Hh6.a(dashboardGoalTrackingPresenter);
            return dashboardGoalTrackingPresenter;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter l(DashboardHeartRatePresenter dashboardHeartRatePresenter) {
            Li6.a(dashboardHeartRatePresenter);
            return dashboardHeartRatePresenter;
        }

        @DexIgnore
        public final DashboardSleepPresenter m(DashboardSleepPresenter dashboardSleepPresenter) {
            Pj6.a(dashboardSleepPresenter);
            return dashboardSleepPresenter;
        }

        @DexIgnore
        public final HomeDashboardFragment n(HomeDashboardFragment homeDashboardFragment) {
            Cz5.b(homeDashboardFragment, c());
            Cz5.a(homeDashboardFragment, b());
            Cz5.c(homeDashboardFragment, d());
            Cz5.e(homeDashboardFragment, f());
            Cz5.f(homeDashboardFragment, g());
            Cz5.d(homeDashboardFragment, e());
            return homeDashboardFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v1 implements H16 {
        @DexIgnore
        public /* final */ L16 a;

        @DexIgnore
        public v1(L16 l16) {
            this.a = l16;
        }

        @DexIgnore
        @Override // com.fossil.H16
        public void a(NotificationContactsActivity notificationContactsActivity) {
            d(notificationContactsActivity);
        }

        @DexIgnore
        public final NotificationContactsPresenter b() {
            NotificationContactsPresenter a2 = P16.a(N16.a(this.a), (Uq4) Kq4.this.R.get(), new D26(), c(), M16.a(this.a));
            e(a2);
            return a2;
        }

        @DexIgnore
        public final G26 c() {
            return new G26((NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationContactsActivity d(NotificationContactsActivity notificationContactsActivity) {
            Ms5.e(notificationContactsActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationContactsActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationContactsActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationContactsActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationContactsActivity, new Gu5());
            G16.a(notificationContactsActivity, b());
            return notificationContactsActivity;
        }

        @DexIgnore
        public final NotificationContactsPresenter e(NotificationContactsPresenter notificationContactsPresenter) {
            Q16.a(notificationContactsPresenter);
            return notificationContactsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v2 implements Yq6 {
        @DexIgnore
        public v2(Br6 br6) {
        }

        @DexIgnore
        @Override // com.fossil.Yq6
        public void a(ThemesFragment themesFragment) {
            b(themesFragment);
        }

        @DexIgnore
        public final ThemesFragment b(ThemesFragment themesFragment) {
            Ar6.a(themesFragment, (Po4) Kq4.this.A3.get());
            return themesFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w0 implements Gh5 {
        @DexIgnore
        public w0() {
        }

        @DexIgnore
        @Override // com.fossil.Gh5
        public void a(DeepLinkActivity deepLinkActivity) {
            b(deepLinkActivity);
        }

        @DexIgnore
        public final DeepLinkActivity b(DeepLinkActivity deepLinkActivity) {
            Ms5.e(deepLinkActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(deepLinkActivity, (An4) Kq4.this.c.get());
            Ms5.a(deepLinkActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(deepLinkActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(deepLinkActivity, new Gu5());
            Fh5.a(deepLinkActivity, (Po4) Kq4.this.A3.get());
            return deepLinkActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w1 implements Q46 {
        @DexIgnore
        public /* final */ U46 a;

        @DexIgnore
        public w1(U46 u46) {
            this.a = u46;
        }

        @DexIgnore
        @Override // com.fossil.Q46
        public void a(NotificationDialLandingActivity notificationDialLandingActivity) {
            d(notificationDialLandingActivity);
        }

        @DexIgnore
        public final NotificationDialLandingPresenter b() {
            NotificationDialLandingPresenter a2 = Y46.a(W46.a(this.a), c(), V46.a(this.a));
            e(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationsLoader c() {
            return NotificationsLoader_Factory.newInstance((Context) Kq4.this.b.get(), (NotificationsRepository) Kq4.this.I.get());
        }

        @DexIgnore
        public final NotificationDialLandingActivity d(NotificationDialLandingActivity notificationDialLandingActivity) {
            Ms5.e(notificationDialLandingActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationDialLandingActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationDialLandingActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationDialLandingActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationDialLandingActivity, new Gu5());
            P46.a(notificationDialLandingActivity, b());
            return notificationDialLandingActivity;
        }

        @DexIgnore
        public final NotificationDialLandingPresenter e(NotificationDialLandingPresenter notificationDialLandingPresenter) {
            Z46.a(notificationDialLandingPresenter);
            return notificationDialLandingPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w2 implements R07 {
        @DexIgnore
        public /* final */ V07 a;

        @DexIgnore
        public w2(V07 v07) {
            this.a = v07;
        }

        @DexIgnore
        @Override // com.fossil.R07
        public void a(TroubleshootingActivity troubleshootingActivity) {
            c(troubleshootingActivity);
        }

        @DexIgnore
        public final X07 b() {
            X07 a2 = Y07.a(W07.a(this.a));
            d(a2);
            return a2;
        }

        @DexIgnore
        public final TroubleshootingActivity c(TroubleshootingActivity troubleshootingActivity) {
            Ms5.e(troubleshootingActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(troubleshootingActivity, (An4) Kq4.this.c.get());
            Ms5.a(troubleshootingActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(troubleshootingActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(troubleshootingActivity, new Gu5());
            Q07.a(troubleshootingActivity, b());
            return troubleshootingActivity;
        }

        @DexIgnore
        public final X07 d(X07 x07) {
            Z07.a(x07);
            return x07;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x0 implements Wp6 {
        @DexIgnore
        public /* final */ Zp6 a;

        @DexIgnore
        public x0(Zp6 zp6) {
            this.a = zp6;
        }

        @DexIgnore
        @Override // com.fossil.Wp6
        public void a(DeleteAccountActivity deleteAccountActivity) {
            d(deleteAccountActivity);
        }

        @DexIgnore
        public final DeleteAccountPresenter b() {
            DeleteAccountPresenter a2 = Cq6.a(Aq6.a(this.a), (DeviceRepository) Kq4.this.P.get(), (AnalyticsHelper) Kq4.this.P0.get(), (UserRepository) Kq4.this.y.get(), c(), Kq4.this.Z3());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final GetZendeskInformation c() {
            return new GetZendeskInformation((UserRepository) Kq4.this.y.get(), (DeviceRepository) Kq4.this.P.get(), (An4) Kq4.this.c.get());
        }

        @DexIgnore
        public final DeleteAccountActivity d(DeleteAccountActivity deleteAccountActivity) {
            Ms5.e(deleteAccountActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(deleteAccountActivity, (An4) Kq4.this.c.get());
            Ms5.a(deleteAccountActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(deleteAccountActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(deleteAccountActivity, new Gu5());
            Vp6.a(deleteAccountActivity, b());
            return deleteAccountActivity;
        }

        @DexIgnore
        public final DeleteAccountPresenter e(DeleteAccountPresenter deleteAccountPresenter) {
            Dq6.a(deleteAccountPresenter);
            return deleteAccountPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x1 implements B56 {
        @DexIgnore
        public /* final */ F56 a;

        @DexIgnore
        public x1(F56 f56) {
            this.a = f56;
        }

        @DexIgnore
        @Override // com.fossil.B56
        public void a(NotificationHybridAppActivity notificationHybridAppActivity) {
            d(notificationHybridAppActivity);
        }

        @DexIgnore
        public final L56 b() {
            return new L56((Context) Kq4.this.b.get());
        }

        @DexIgnore
        public final I56 c() {
            I56 a2 = J56.a(H56.a(this.a), this.a.a(), G56.a(this.a), (Uq4) Kq4.this.R.get(), b());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridAppActivity d(NotificationHybridAppActivity notificationHybridAppActivity) {
            Ms5.e(notificationHybridAppActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationHybridAppActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationHybridAppActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationHybridAppActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationHybridAppActivity, new Gu5());
            A56.a(notificationHybridAppActivity, c());
            return notificationHybridAppActivity;
        }

        @DexIgnore
        public final I56 e(I56 i56) {
            K56.a(i56);
            return i56;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x2 implements Ow6 {
        @DexIgnore
        public /* final */ Rw6 a;

        @DexIgnore
        public x2(Rw6 rw6) {
            this.a = rw6;
        }

        @DexIgnore
        @Override // com.fossil.Ow6
        public void a(UpdateFirmwareActivity updateFirmwareActivity) {
            d(updateFirmwareActivity);
        }

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase b() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) Kq4.this.d.get(), (GuestApiService) Kq4.this.F.get());
        }

        @DexIgnore
        public final UpdateFirmwarePresenter c() {
            UpdateFirmwarePresenter a2 = Uw6.a(Sw6.a(this.a), (DeviceRepository) Kq4.this.P.get(), (UserRepository) Kq4.this.y.get(), Kq4.this.a4(), (An4) Kq4.this.c.get(), Kq4.this.P4(), b());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final UpdateFirmwareActivity d(UpdateFirmwareActivity updateFirmwareActivity) {
            Ms5.e(updateFirmwareActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(updateFirmwareActivity, (An4) Kq4.this.c.get());
            Ms5.a(updateFirmwareActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(updateFirmwareActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(updateFirmwareActivity, new Gu5());
            Nx6.a(updateFirmwareActivity, c());
            return updateFirmwareActivity;
        }

        @DexIgnore
        public final UpdateFirmwarePresenter e(UpdateFirmwarePresenter updateFirmwarePresenter) {
            Vw6.a(updateFirmwarePresenter);
            return updateFirmwarePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y0 implements U66 {
        @DexIgnore
        public /* final */ V66 a;

        @DexIgnore
        public y0(V66 v66) {
            this.a = v66;
        }

        @DexIgnore
        @Override // com.fossil.U66
        public void a(WatchAppEditActivity watchAppEditActivity) {
            d(watchAppEditActivity);
        }

        @DexIgnore
        public final SetWatchAppUseCase b() {
            return new SetWatchAppUseCase((DianaPresetRepository) Kq4.this.M0.get(), (FileRepository) Kq4.this.I0.get(), Kq4.this.b4(), Kq4.this.b4());
        }

        @DexIgnore
        public final WatchAppEditPresenter c() {
            WatchAppEditPresenter a2 = I76.a(W66.a(this.a), b(), (An4) Kq4.this.c.get());
            e(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppEditActivity d(WatchAppEditActivity watchAppEditActivity) {
            Ms5.e(watchAppEditActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(watchAppEditActivity, (An4) Kq4.this.c.get());
            Ms5.a(watchAppEditActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(watchAppEditActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(watchAppEditActivity, new Gu5());
            C76.a(watchAppEditActivity, c());
            C76.b(watchAppEditActivity, (Po4) Kq4.this.A3.get());
            return watchAppEditActivity;
        }

        @DexIgnore
        public final WatchAppEditPresenter e(WatchAppEditPresenter watchAppEditPresenter) {
            J76.a(watchAppEditPresenter);
            return watchAppEditPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y1 implements N56 {
        @DexIgnore
        public /* final */ R56 a;

        @DexIgnore
        public y1(R56 r56) {
            this.a = r56;
        }

        @DexIgnore
        @Override // com.fossil.N56
        public void a(NotificationHybridContactActivity notificationHybridContactActivity) {
            c(notificationHybridContactActivity);
        }

        @DexIgnore
        public final NotificationHybridContactPresenter b() {
            NotificationHybridContactPresenter a2 = W56.a(U56.a(this.a), this.a.b(), S56.a(this.a), T56.a(this.a), (Uq4) Kq4.this.R.get(), new Y56());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridContactActivity c(NotificationHybridContactActivity notificationHybridContactActivity) {
            Ms5.e(notificationHybridContactActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationHybridContactActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationHybridContactActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationHybridContactActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationHybridContactActivity, new Gu5());
            M56.a(notificationHybridContactActivity, b());
            return notificationHybridContactActivity;
        }

        @DexIgnore
        public final NotificationHybridContactPresenter d(NotificationHybridContactPresenter notificationHybridContactPresenter) {
            X56.a(notificationHybridContactPresenter);
            return notificationHybridContactPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y2 implements Ot6 {
        @DexIgnore
        public y2(Rt6 rt6) {
        }

        @DexIgnore
        @Override // com.fossil.Ot6
        public void a(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            b(userCustomizeThemeFragment);
        }

        @DexIgnore
        public final UserCustomizeThemeFragment b(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            Qt6.a(userCustomizeThemeFragment, (Po4) Kq4.this.A3.get());
            return userCustomizeThemeFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z0 implements L96 {
        @DexIgnore
        public z0() {
        }

        @DexIgnore
        @Override // com.fossil.L96
        public void a(EditPhotoFragment editPhotoFragment) {
            b(editPhotoFragment);
        }

        @DexIgnore
        public final EditPhotoFragment b(EditPhotoFragment editPhotoFragment) {
            N96.a(editPhotoFragment, (Po4) Kq4.this.A3.get());
            return editPhotoFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z1 implements A66 {
        @DexIgnore
        public /* final */ E66 a;

        @DexIgnore
        public z1(E66 e66) {
            this.a = e66;
        }

        @DexIgnore
        @Override // com.fossil.A66
        public void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            c(notificationHybridEveryoneActivity);
        }

        @DexIgnore
        public final NotificationHybridEveryonePresenter b() {
            NotificationHybridEveryonePresenter a2 = I66.a(G66.a(this.a), this.a.b(), F66.a(this.a), (Uq4) Kq4.this.R.get(), new Y56());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridEveryoneActivity c(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            Ms5.e(notificationHybridEveryoneActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(notificationHybridEveryoneActivity, (An4) Kq4.this.c.get());
            Ms5.a(notificationHybridEveryoneActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(notificationHybridEveryoneActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(notificationHybridEveryoneActivity, new Gu5());
            Z56.a(notificationHybridEveryoneActivity, b());
            return notificationHybridEveryoneActivity;
        }

        @DexIgnore
        public final NotificationHybridEveryonePresenter d(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter) {
            J66.a(notificationHybridEveryonePresenter);
            return notificationHybridEveryonePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z2 implements Za6 {
        @DexIgnore
        public /* final */ Db6 a;

        @DexIgnore
        public z2(Db6 db6) {
            this.a = db6;
        }

        @DexIgnore
        @Override // com.fossil.Za6
        public void a(WatchAppSearchActivity watchAppSearchActivity) {
            c(watchAppSearchActivity);
        }

        @DexIgnore
        public final WatchAppSearchPresenter b() {
            WatchAppSearchPresenter a2 = Gb6.a(Eb6.a(this.a), Kq4.this.U4(), (An4) Kq4.this.c.get());
            d(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppSearchActivity c(WatchAppSearchActivity watchAppSearchActivity) {
            Ms5.e(watchAppSearchActivity, (UserRepository) Kq4.this.y.get());
            Ms5.d(watchAppSearchActivity, (An4) Kq4.this.c.get());
            Ms5.a(watchAppSearchActivity, (DeviceRepository) Kq4.this.P.get());
            Ms5.c(watchAppSearchActivity, (MigrationManager) Kq4.this.Y1.get());
            Ms5.b(watchAppSearchActivity, new Gu5());
            Ya6.a(watchAppSearchActivity, b());
            return watchAppSearchActivity;
        }

        @DexIgnore
        public final WatchAppSearchPresenter d(WatchAppSearchPresenter watchAppSearchPresenter) {
            Hb6.a(watchAppSearchPresenter);
            return watchAppSearchPresenter;
        }
    }

    @DexIgnore
    public Kq4(Uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.a = uo4;
        a5(uo4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
        b5(uo4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
        c5(uo4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static Zi O3() {
        return new Zi();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void A(DebugActivity debugActivity) {
        o5(debugActivity);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void A0(NetworkChangedReceiver networkChangedReceiver) {
        y5(networkChangedReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void A1(FossilNotificationListenerService fossilNotificationListenerService) {
        t5(fossilNotificationListenerService);
    }

    @DexIgnore
    public final Ev5 A4() {
        return new Ev5(this.d0.get());
    }

    @DexIgnore
    public final PortfolioApp A5(PortfolioApp portfolioApp) {
        Qq4.D(portfolioApp, this.c.get());
        Qq4.v(portfolioApp, this.D.get());
        Qq4.u(portfolioApp, this.E.get());
        Qq4.b(portfolioApp, this.B.get());
        Qq4.p(portfolioApp, this.F.get());
        Qq4.f(portfolioApp, this.G.get());
        Qq4.d(portfolioApp, this.o.get());
        Qq4.g(portfolioApp, U3());
        Qq4.x(portfolioApp, this.R.get());
        Qq4.k(portfolioApp, Z3());
        Qq4.c(portfolioApp, this.P0.get());
        Qq4.e(portfolioApp, this.z1.get());
        Qq4.l(portfolioApp, this.P.get());
        Qq4.n(portfolioApp, this.C.get());
        Qq4.t(portfolioApp, this.A1.get());
        Qq4.m(portfolioApp, this.q.get());
        Qq4.h(portfolioApp, X3());
        Qq4.A(portfolioApp, X4());
        Qq4.i(portfolioApp, this.H1.get());
        Qq4.y(portfolioApp, this.y.get());
        Qq4.E(portfolioApp, W4());
        Qq4.s(portfolioApp, this.i.get());
        Qq4.C(portfolioApp, Z4());
        Qq4.z(portfolioApp, T4());
        Qq4.B(portfolioApp, this.L1.get());
        Qq4.r(portfolioApp, this.M1.get());
        Qq4.q(portfolioApp, this.N1.get());
        Qq4.w(portfolioApp, this.y1.get());
        Qq4.j(portfolioApp, Y3());
        Qq4.o(portfolioApp, q4());
        Qq4.a(portfolioApp, this.O1.get());
        return portfolioApp;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public W06 B(B16 b16) {
        Lk7.b(b16);
        return new t1(b16);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ou4 B0() {
        return new Ji();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Hx4 B1() {
        return new Ki();
    }

    @DexIgnore
    public final LoginSocialUseCase B4() {
        return new LoginSocialUseCase(this.y.get());
    }

    @DexIgnore
    public final ProfileSetupPresenter B5(ProfileSetupPresenter profileSetupPresenter) {
        Ex6.c(profileSetupPresenter, L4());
        Ex6.d(profileSetupPresenter, M4());
        Ex6.b(profileSetupPresenter, u4());
        Ex6.a(profileSetupPresenter, this.P0.get());
        Ex6.e(profileSetupPresenter);
        return profileSetupPresenter;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Wr6 C(Zr6 zr6) {
        Lk7.b(zr6);
        return new n0(zr6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Fv4 C0() {
        return new Ri();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Fx6 C1(Ix6 ix6) {
        Lk7.b(ix6);
        return new Yi(ix6);
    }

    @DexIgnore
    public final Gv5 C4() {
        return new Gv5(this.W1.get());
    }

    @DexIgnore
    public final SignUpPresenter C5(SignUpPresenter signUpPresenter) {
        Wz6.u(signUpPresenter, z4());
        Wz6.y(signUpPresenter, this.V1.get());
        Wz6.v(signUpPresenter, A4());
        Wz6.z(signUpPresenter, D4());
        Wz6.x(signUpPresenter, C4());
        Wz6.w(signUpPresenter, B4());
        Wz6.H(signUpPresenter, this.y.get());
        Wz6.g(signUpPresenter, this.P.get());
        Wz6.G(signUpPresenter, this.R.get());
        Wz6.o(signUpPresenter, m4());
        Wz6.p(signUpPresenter, n4());
        Wz6.j(signUpPresenter, h4());
        Wz6.q(signUpPresenter, o4());
        Wz6.n(signUpPresenter, l4());
        Wz6.l(signUpPresenter, j4());
        Wz6.c(signUpPresenter, this.A.get());
        Wz6.A(signUpPresenter, new ReconnectDeviceUseCase());
        Wz6.h(signUpPresenter, a4());
        Wz6.i(signUpPresenter, f4());
        Wz6.D(signUpPresenter, this.c.get());
        Wz6.e(signUpPresenter, R3());
        Wz6.f(signUpPresenter, S3());
        Wz6.d(signUpPresenter, this.P0.get());
        Wz6.s(signUpPresenter, u4());
        Wz6.F(signUpPresenter, this.D.get());
        Wz6.E(signUpPresenter, this.E.get());
        Wz6.t(signUpPresenter, this.c0.get());
        Wz6.k(signUpPresenter, i4());
        Wz6.m(signUpPresenter, k4());
        Wz6.B(signUpPresenter, G4());
        Wz6.r(signUpPresenter, t4());
        Wz6.I(signUpPresenter, X4());
        Wz6.C(signUpPresenter, this.c.get());
        Wz6.a(signUpPresenter, this.J0.get());
        Wz6.b(signUpPresenter, this.u1.get());
        Wz6.J(signUpPresenter, Z4());
        Wz6.K(signUpPresenter);
        return signUpPresenter;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Qr6 D(Tr6 tr6) {
        Lk7.b(tr6);
        return new m0(tr6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Bt6 D0(Et6 et6) {
        Lk7.b(et6);
        return new s0(et6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Rv4 D1() {
        return new Ti();
    }

    @DexIgnore
    public final Hv5 D4() {
        return new Hv5(this.V1.get());
    }

    @DexIgnore
    public final ThemeManager D5(ThemeManager themeManager) {
        Rn5.a(themeManager, this.y1.get());
        return themeManager;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Vd6 E(De6 de6) {
        Lk7.b(de6);
        return new Di(de6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ew4 E0() {
        return new Vi();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Wx6 E1(Zx6 zx6) {
        Lk7.b(zx6);
        return new e2(zx6);
    }

    @DexIgnore
    public final Map<Class<? extends ListenableWorker>, Provider<Jc7<? extends ListenableWorker>>> E4() {
        return A34.of(PushPendingDataWorker.class, this.E1);
    }

    @DexIgnore
    public final TimeChangeReceiver E5(TimeChangeReceiver timeChangeReceiver) {
        Lc7.a(timeChangeReceiver, this.B.get());
        Lc7.b(timeChangeReceiver, this.c.get());
        return timeChangeReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Nx4 F() {
        return new Mi();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Xw6 F0(Ax6 ax6) {
        Lk7.b(ax6);
        return new k2(ax6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Dp6 F1() {
        return new i2();
    }

    @DexIgnore
    public final MicroAppLastSettingRepository F4() {
        return new MicroAppLastSettingRepository(this.j0.get());
    }

    @DexIgnore
    public final TimeTickReceiver F5(TimeTickReceiver timeTickReceiver) {
        Ho5.c(timeTickReceiver, this.c.get());
        Ho5.b(timeTickReceiver, W3());
        Ho5.a(timeTickReceiver, this.B.get());
        return timeTickReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Pc6 G(Tc6 tc6) {
        Lk7.b(tc6);
        return new o2(tc6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public E87 G0() {
        return new b3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Wp6 G1(Zp6 zp6) {
        Lk7.b(zp6);
        return new x0(zp6);
    }

    @DexIgnore
    public final RequestEmailOtp G4() {
        return new RequestEmailOtp(this.y.get());
    }

    @DexIgnore
    public final URLRequestTaskHelper G5(URLRequestTaskHelper uRLRequestTaskHelper) {
        URLRequestTaskHelper_MembersInjector.injectMApiService(uRLRequestTaskHelper, this.o.get());
        return uRLRequestTaskHelper;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public R07 H(V07 v07) {
        Lk7.b(v07);
        return new w2(v07);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void H0(PortfolioApp portfolioApp) {
        A5(portfolioApp);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public E07 H1(F07 f07) {
        Lk7.b(f07);
        return new a1(f07);
    }

    @DexIgnore
    public final RingStyleRemoteDataSource H4() {
        return new RingStyleRemoteDataSource(this.o.get());
    }

    @DexIgnore
    public final N47 H5(N47 n47) {
        O47.a(n47, this.c.get());
        return n47;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void I(N47 n47) {
        H5(n47);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Yz5 I0(D06 d06) {
        Lk7.b(d06);
        return new j1(d06);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void I1(WeatherManager weatherManager) {
        K5(weatherManager);
    }

    @DexIgnore
    public final RingStyleRepository I4() {
        return new RingStyleRepository(H4(), this.I0.get());
    }

    @DexIgnore
    public final WatchAppCommuteTimeManager I5(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        Es5.d(watchAppCommuteTimeManager, this.d.get());
        Es5.a(watchAppCommuteTimeManager, this.o.get());
        Es5.c(watchAppCommuteTimeManager, this.K1.get());
        Es5.f(watchAppCommuteTimeManager, this.y.get());
        Es5.b(watchAppCommuteTimeManager, b4());
        Es5.e(watchAppCommuteTimeManager, this.c.get());
        return watchAppCommuteTimeManager;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public S86 J(V86 v86) {
        Lk7.b(v86);
        return new q2(v86);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void J0(TimeChangeReceiver timeChangeReceiver) {
        E5(timeChangeReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ma6 J1() {
        return new g0();
    }

    @DexIgnore
    public final SetNotificationUseCase J4() {
        return new SetNotificationUseCase(new V36(), new D26(), this.j.get(), this.I.get(), this.P.get(), this.c.get());
    }

    @DexIgnore
    public final Pl5 J5(Pl5 pl5) {
        Ql5.a(pl5, this.c.get());
        return pl5;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Rm6 K(Vm6 vm6) {
        Lk7.b(vm6);
        return new s2(vm6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public C17 K0() {
        return new k3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void K1(AppHelper appHelper) {
        e5(appHelper);
    }

    @DexIgnore
    public final SetReplyMessageMappingUseCase K4() {
        return new SetReplyMessageMappingUseCase(this.i.get());
    }

    @DexIgnore
    public final WeatherManager K5(WeatherManager weatherManager) {
        Tn5.f(weatherManager, this.d.get());
        Tn5.a(weatherManager, this.o.get());
        Tn5.e(weatherManager, this.K1.get());
        Tn5.g(weatherManager, this.y.get());
        Tn5.b(weatherManager, this.t.get());
        Tn5.c(weatherManager, b4());
        Tn5.d(weatherManager, this.f2.get());
        return weatherManager;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public O66 L(P66 p66) {
        Lk7.b(p66);
        return new i0(p66);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ow6 L0(Rw6 rw6) {
        Lk7.b(rw6);
        return new x2(rw6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Mx5 L1(Qx5 qx5) {
        Lk7.b(qx5);
        return new Gi(qx5);
    }

    @DexIgnore
    public final SignUpEmailUseCase L4() {
        return new SignUpEmailUseCase(this.y.get(), this.c.get());
    }

    @DexIgnore
    public final WorkoutDetailActivity L5(WorkoutDetailActivity workoutDetailActivity) {
        Dn6.a(workoutDetailActivity, this.m0.get());
        return workoutDetailActivity;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Xb6 M(Cc6 cc6) {
        Lk7.b(cc6);
        return new m1(cc6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Qk6 M0(Uk6 uk6) {
        Lk7.b(uk6);
        return new Ci(uk6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ja6 M1() {
        return new f0();
    }

    @DexIgnore
    public final SignUpSocialUseCase M4() {
        return new SignUpSocialUseCase(this.y.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Mp6 N(Qp6 qp6) {
        Lk7.b(qp6);
        return new i1(qp6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Tu6 N0(Xu6 xu6) {
        Lk7.b(xu6);
        return new o1(xu6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Jy4 N1() {
        return new Pi();
    }

    @DexIgnore
    public final Lu5 N4() {
        return Mu5.a(this.I.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void O(NotificationReceiver notificationReceiver) {
        z5(notificationReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Pb6 O0(Qb6 qb6) {
        Lk7.b(qb6);
        return new n1(qb6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void O1(ThemeManager themeManager) {
        D5(themeManager);
    }

    @DexIgnore
    public final UpdateDeviceToSpecificFirmwareUsecase O4() {
        return new UpdateDeviceToSpecificFirmwareUsecase(this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public B96 P(F96 f96) {
        Lk7.b(f96);
        return new h0(f96);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ih6 P0(Qh6 qh6) {
        Lk7.b(qh6);
        return new f1(qh6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Q46 P1(U46 u46) {
        Lk7.b(u46);
        return new w1(u46);
    }

    @DexIgnore
    public final CategoryRemoteDataSource P3() {
        return new CategoryRemoteDataSource(this.o.get());
    }

    @DexIgnore
    public final UpdateFirmwareUsecase P4() {
        return new UpdateFirmwareUsecase(this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void Q(DeviceUtils deviceUtils) {
        q5(deviceUtils);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Gy6 Q0(Jy6 jy6) {
        Lk7.b(jy6);
        return new d2(jy6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Gy4 Q1() {
        return new Oi();
    }

    @DexIgnore
    public final CategoryRepository Q3() {
        return new CategoryRepository(this.R0.get(), P3());
    }

    @DexIgnore
    public final UpdateUser Q4() {
        return new UpdateUser(this.y.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void R(LightAndHapticsManager lightAndHapticsManager) {
        u5(lightAndHapticsManager);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Po6 R0(To6 to6) {
        Lk7.b(to6);
        return new n2(to6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Nz6 R1(Rz6 rz6) {
        Lk7.b(rz6);
        return new r2(rz6);
    }

    @DexIgnore
    public final CheckAuthenticationEmailExisting R3() {
        return new CheckAuthenticationEmailExisting(this.y.get());
    }

    @DexIgnore
    public final VerifySecretKeyUseCase R4() {
        return new VerifySecretKeyUseCase(this.P.get(), this.y.get(), Y3(), this.d.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public T17 S(W17 w17) {
        Lk7.b(w17);
        return new c1(w17);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void S0(LoginPresenter loginPresenter) {
        v5(loginPresenter);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Gh5 S1() {
        return new w0();
    }

    @DexIgnore
    public final CheckAuthenticationSocialExisting S3() {
        return new CheckAuthenticationSocialExisting(this.y.get());
    }

    @DexIgnore
    public final WatchAppDataRemoteDataSource S4() {
        return new WatchAppDataRemoteDataSource(this.o.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Rw4 T() {
        return new Ni();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Q76 T0(C86 c86) {
        Lk7.b(c86);
        return new d0(c86);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public U96 T1(Z96 z96) {
        Lk7.b(z96);
        return new a3(z96);
    }

    @DexIgnore
    public final ComplicationRepository T3() {
        return new ComplicationRepository(this.f0.get(), this.d.get());
    }

    @DexIgnore
    public final WatchAppDataRepository T4() {
        return new WatchAppDataRepository(S4(), this.I0.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Iu6 U() {
        return new p3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Yc6 U0() {
        return new u0();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public T16 U1(Y16 y16) {
        Lk7.b(y16);
        return new a2(y16);
    }

    @DexIgnore
    public final L37 U3() {
        L37 a4 = M37.a(this.d.get(), this.I.get(), this.P.get(), this.j.get(), this.G.get(), this.Q.get(), N4(), this.c.get());
        n5(a4);
        return a4;
    }

    @DexIgnore
    public final WatchAppRepository U4() {
        return new WatchAppRepository(this.e0.get(), this.d.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Mv4 V() {
        return new Si();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Hm6 V0(Lm6 lm6) {
        Lk7.b(lm6);
        return new g1(lm6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void V1(WorkoutDetailActivity workoutDetailActivity) {
        L5(workoutDetailActivity);
    }

    @DexIgnore
    public final CustomizeRealDataManager V3() {
        return new CustomizeRealDataManager(this.d.get(), this.P.get(), b4());
    }

    @DexIgnore
    public final WatchFaceRemoteDataSource V4() {
        return new WatchFaceRemoteDataSource(this.o.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public En6 W() {
        return new n3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Yv4 W0() {
        return new Ui();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void W1(CloudImageHelper cloudImageHelper) {
        k5(cloudImageHelper);
    }

    @DexIgnore
    public final DSTChangeUseCase W3() {
        return new DSTChangeUseCase(this.q.get(), this.t.get());
    }

    @DexIgnore
    public final WatchFaceRepository W4() {
        return new WatchFaceRepository(this.b.get(), V4(), this.I0.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Qa6 X(Ta6 ta6) {
        Lk7.b(ta6);
        return new l3(ta6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Y97 X0() {
        return new i3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Qj6 X1(Yj6 yj6) {
        Lk7.b(yj6);
        return new t2(yj6);
    }

    @DexIgnore
    public final Ic7 X3() {
        return new Ic7(E4());
    }

    @DexIgnore
    public final WatchLocalizationRepository X4() {
        return new WatchLocalizationRepository(this.o.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Er6 Y(Hr6 hr6) {
        Lk7.b(hr6);
        return new k0(hr6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Kw4 Y0() {
        return new Xi();
    }

    @DexIgnore
    public final Q27 Y3() {
        return new Q27(this.c.get());
    }

    @DexIgnore
    public final WorkoutSettingRemoteDataSource Y4() {
        return new WorkoutSettingRemoteDataSource(this.o.get());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void Z(MFDeviceService mFDeviceService) {
        w5(mFDeviceService);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Cs6 Z0(Fs6 fs6) {
        Lk7.b(fs6);
        return new o0(fs6);
    }

    @DexIgnore
    public final DeleteLogoutUserUseCase Z3() {
        return new DeleteLogoutUserUseCase(this.y.get(), this.A.get(), this.c.get(), this.V.get(), this.W.get(), this.D.get(), this.a0.get(), this.I.get(), this.P.get(), this.b0.get(), this.c0.get(), this.d0.get(), w4(), this.E.get(), this.q.get(), U4(), T3(), this.j.get(), this.e.get(), this.i0.get(), F4(), p4(), this.k0.get(), this.l0.get(), this.m0.get(), this.n0.get(), this.s0.get(), this.w0.get(), this.A0.get(), this.E0.get(), this.I0.get(), this.i.get(), W4(), I4(), this.J0.get(), Z4(), b4(), e4(), T4(), this.M0.get(), this.O0.get());
    }

    @DexIgnore
    public final WorkoutSettingRepository Z4() {
        return new WorkoutSettingRepository(Y4());
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Js6 a(Ms6 ms6) {
        Lk7.b(ms6);
        return new p0(ms6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Mn6 a0() {
        return new o3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Da7 a1() {
        return new j3();
    }

    @DexIgnore
    public final Cj4 a4() {
        return new Cj4(r4(), s4(), x4(), c4());
    }

    @DexIgnore
    public final void a5(Uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        Provider<Context> a4 = Ik7.a(Zo4.a(uo4));
        this.b = a4;
        this.c = Ik7.a(Up4.a(uo4, a4));
        Provider<PortfolioApp> a5 = Ik7.a(Bp4.a(uo4));
        this.d = a5;
        this.e = Ik7.a(PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory.create(portfolioDatabaseModule, a5));
        Provider<QuickResponseDatabase> a6 = Ik7.a(PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.f = a6;
        this.g = Ik7.a(PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory.create(portfolioDatabaseModule, a6));
        Provider<QuickResponseSenderDao> a7 = Ik7.a(PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory.create(portfolioDatabaseModule, this.f));
        this.h = a7;
        this.i = Ik7.a(QuickResponseRepository_Factory.create(this.g, a7));
        Provider<NotificationSettingsDatabase> a8 = Ik7.a(PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.j = a8;
        this.k = Ik7.a(Xr5.a(this.c, this.e, this.i, a8));
        Provider<AuthApiGuestService> a9 = Ik7.a(Cp4.a(uo4, this.c));
        this.l = a9;
        this.m = Ik7.a(Rq5.a(this.d, a9, this.c));
        Provider<Uq5> a10 = Ik7.a(Vq5.a(this.l, this.c));
        this.n = a10;
        Provider<ApiServiceV2> a11 = Ik7.a(Yo4.a(uo4, this.m, a10));
        this.o = a11;
        Provider<DianaPresetRemoteDataSource> a12 = Ik7.a(RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory.create(repositoriesModule, a11));
        this.p = a12;
        this.q = Ik7.a(DianaPresetRepository_Factory.create(a12));
        Provider<CustomizeRealDataDatabase> a13 = Ik7.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.r = a13;
        Provider<CustomizeRealDataDao> a14 = Ik7.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory.create(portfolioDatabaseModule, a13));
        this.s = a14;
        this.t = Ik7.a(CustomizeRealDataRepository_Factory.create(a14));
        Provider<AuthApiUserService> a15 = Ik7.a(Dp4.a(uo4, this.m, this.n));
        this.u = a15;
        this.v = UserRemoteDataSource_Factory.create(this.o, this.l, a15);
        Provider<UserSettingDatabase> a16 = Ik7.a(PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.w = a16;
        Provider<UserSettingDao> a17 = Ik7.a(PortfolioDatabaseModule_ProvideUserSettingDaoFactory.create(portfolioDatabaseModule, a16));
        this.x = a17;
        this.y = Ik7.a(UserRepository_Factory.create(this.v, a17, this.c));
        Provider<AlarmsRemoteDataSource> a18 = Ik7.a(AlarmsRemoteDataSource_Factory.create(this.o));
        this.z = a18;
        Provider<AlarmsRepository> a19 = Ik7.a(AlarmsRepository_Factory.create(a18));
        this.A = a19;
        this.B = Ik7.a(Vo4.a(uo4, this.c, this.y, a19));
        Provider<FitnessHelper> a20 = Ik7.a(PortfolioDatabaseModule_ProvideFitnessHelperFactory.create(portfolioDatabaseModule, this.c));
        this.C = a20;
        this.D = Ik7.a(SummariesRepository_Factory.create(this.o, a20));
        this.E = Ik7.a(SleepSummariesRepository_Factory.create(this.o));
        this.F = Ik7.a(Kp4.a(uo4, this.c));
        this.G = Ik7.a(Oo4.a());
        Provider<NotificationsDataSource> a21 = Ik7.a(NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory.create(notificationsRepositoryModule));
        this.H = a21;
        this.I = Ik7.a(NotificationsRepository_Factory.create(a21));
        Provider<DeviceDatabase> a22 = Ik7.a(PortfolioDatabaseModule_ProvideDeviceDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.J = a22;
        this.K = Ik7.a(PortfolioDatabaseModule_ProvideDeviceDaoFactory.create(portfolioDatabaseModule, a22));
        this.L = Ik7.a(PortfolioDatabaseModule_ProvideSkuDaoFactory.create(portfolioDatabaseModule, this.J));
        this.M = Ik7.a(Xo4.a(uo4, this.m, this.n));
        Provider<SecureApiService2Dot1> a23 = Ik7.a(Tp4.a(uo4, this.c));
        this.N = a23;
        DeviceRemoteDataSource_Factory create = DeviceRemoteDataSource_Factory.create(this.o, this.M, a23);
        this.O = create;
        this.P = Ik7.a(DeviceRepository_Factory.create(this.K, this.L, create));
        this.Q = Ik7.a(Ep4.a(uo4));
        this.R = Ik7.a(Wp4.a(uo4));
        Provider<HybridCustomizeDatabase> a24 = Ik7.a(PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.S = a24;
        this.T = Ik7.a(PortfolioDatabaseModule_ProvidePresetDaoFactory.create(portfolioDatabaseModule, a24));
        HybridPresetRemoteDataSource_Factory create2 = HybridPresetRemoteDataSource_Factory.create(this.o);
        this.U = create2;
        this.V = Ik7.a(HybridPresetRepository_Factory.create(this.T, create2));
        this.W = Ik7.a(ActivitiesRepository_Factory.create(this.o, this.y, this.C));
        Provider<ShortcutApiService> a25 = Ik7.a(Vp4.a(uo4, this.m, this.n));
        this.X = a25;
        this.Y = Ik7.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory.create(microAppSettingRepositoryModule, a25, this.G));
        Provider<MicroAppSettingDataSource> a26 = Ik7.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory.create(microAppSettingRepositoryModule));
        this.Z = a26;
        this.a0 = Ik7.a(MicroAppSettingRepository_Factory.create(this.Y, a26, this.G));
        this.b0 = Ik7.a(SleepSessionsRepository_Factory.create(this.y, this.o));
        this.c0 = Ik7.a(GoalTrackingRepository_Factory.create(this.y, this.c, this.o));
        this.d0 = Ik7.a(Op4.a(uo4));
        this.e0 = Ik7.a(RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory.create(repositoriesModule, this.o));
        this.f0 = Ik7.a(RepositoriesModule_ProvideComplicationRemoteDataSourceFactory.create(repositoriesModule, this.o));
        this.g0 = Ik7.a(PortfolioDatabaseModule_ProvideMicroAppDaoFactory.create(portfolioDatabaseModule, this.S));
        MicroAppRemoteDataSource_Factory create3 = MicroAppRemoteDataSource_Factory.create(this.o);
        this.h0 = create3;
        this.i0 = Ik7.a(MicroAppRepository_Factory.create(this.g0, create3, this.d));
        this.j0 = Ik7.a(PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory.create(portfolioDatabaseModule, this.S));
        this.k0 = Ik7.a(HeartRateSampleRepository_Factory.create(this.o));
        this.l0 = Ik7.a(HeartRateSummaryRepository_Factory.create(this.o));
        this.m0 = Ik7.a(WorkoutSessionRepository_Factory.create(this.o));
        this.n0 = Ik7.a(PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        Provider<BuddyChallengeDatabase> a27 = Ik7.a(PortfolioDatabaseModule_ProvidesBuddyChallengeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.o0 = a27;
        Provider<Jt4> a28 = Ik7.a(PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory.create(portfolioDatabaseModule, a27));
        this.p0 = a28;
        this.q0 = Ik7.a(PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory.create(portfolioDatabaseModule, a28));
        Provider<ProfileRemoteDataSource> a29 = Ik7.a(PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.r0 = a29;
        this.s0 = Ik7.a(Iu4.a(this.q0, a29, this.c));
        Provider<Ys4> a30 = Ik7.a(PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory.create(portfolioDatabaseModule, this.o0));
        this.t0 = a30;
        this.u0 = Ik7.a(PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory.create(portfolioDatabaseModule, a30));
        Provider<FriendRemoteDataSource> a31 = Ik7.a(PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.v0 = a31;
        this.w0 = Ik7.a(Au4.a(this.u0, a31, this.c));
        Provider<Qs4> a32 = Ik7.a(PortfolioDatabaseModule_ProvidesChallengeDaoFactory.create(portfolioDatabaseModule, this.o0));
        this.x0 = a32;
        this.y0 = Ik7.a(PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory.create(portfolioDatabaseModule, a32));
        Provider<ChallengeRemoteDataSource> a33 = Ik7.a(PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory.create(portfolioDatabaseModule, this.o));
        this.z0 = a33;
        this.A0 = Ik7.a(Ut4.a(this.y0, a33, this.c));
        Provider<Ft4> a34 = Ik7.a(PortfolioDatabaseModule_ProvidesNotificationDaoFactory.create(portfolioDatabaseModule, this.o0));
        this.B0 = a34;
        this.C0 = Ik7.a(PortfolioDatabaseModule_ProvidesNotificationLocalFactory.create(portfolioDatabaseModule, a34));
        Provider<NotificationRemoteDataSource> a35 = Ik7.a(PortfolioDatabaseModule_ProvidesNotificationRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.D0 = a35;
        this.E0 = Ik7.a(Eu4.a(this.C0, a35));
        Provider<FileDatabase> a36 = Ik7.a(PortfolioDatabaseModule_ProvideFileDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.F0 = a36;
        this.G0 = Ik7.a(PortfolioDatabaseModule_ProvideFileDaoFactory.create(portfolioDatabaseModule, a36));
        Provider<FileDownloadManager> a37 = Ik7.a(Gp4.a(uo4));
        this.H0 = a37;
        this.I0 = Ik7.a(FileRepository_Factory.create(this.G0, a37, this.d));
        this.J0 = Ik7.a(Wt4.a(this.c, this.o));
        Provider<SecureApiService> a38 = Ik7.a(Sp4.a(uo4, this.c));
        this.K0 = a38;
        To5 a39 = To5.a(this.o, a38);
        this.L0 = a39;
        this.M0 = Ik7.a(Vo5.a(a39));
        Provider<WFBackgroundPhotoRemote> a40 = Ik7.a(PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.N0 = a40;
        this.O0 = Ik7.a(L97.a(a40));
        this.P0 = Ik7.a(Wo4.a(uo4));
        Provider<CategoryDatabase> a41 = Ik7.a(PortfolioDatabaseModule_ProvideCategoryDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.Q0 = a41;
        this.R0 = Ik7.a(PortfolioDatabaseModule_ProvideCategoryDaoFactory.create(portfolioDatabaseModule, a41));
        CategoryRemoteDataSource_Factory create4 = CategoryRemoteDataSource_Factory.create(this.o);
        this.S0 = create4;
        this.T0 = CategoryRepository_Factory.create(this.R0, create4);
        this.U0 = WatchAppRepository_Factory.create(this.e0, this.d);
        this.V0 = ComplicationRepository_Factory.create(this.f0, this.d);
        this.W0 = DianaAppSettingRepository_Factory.create(this.o);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Nl6 b(Rl6 rl6) {
        Lk7.b(rl6);
        return new b0(rl6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Kr6 b0(Nr6 nr6) {
        Lk7.b(nr6);
        return new l0(nr6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Tx4 b1() {
        return new Wi();
    }

    @DexIgnore
    public final DianaAppSettingRepository b4() {
        return new DianaAppSettingRepository(this.o.get());
    }

    @DexIgnore
    public final void b5(Uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        DianaWatchFaceRemoteDataSource_Factory create = DianaWatchFaceRemoteDataSource_Factory.create(this.o, this.K0);
        this.X0 = create;
        this.Y0 = DianaWatchFaceRepository_Factory.create(this.I0, create);
        RingStyleRemoteDataSource_Factory create2 = RingStyleRemoteDataSource_Factory.create(this.o);
        this.Z0 = create2;
        this.a1 = RingStyleRepository_Factory.create(create2, this.I0);
        this.b1 = WatchLocalizationRepository_Factory.create(this.o, this.c);
        this.c1 = Ik7.a(PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.d1 = Ik7.a(T77.a(N77.a(), this.c1));
        Provider<DianaRecommendedPresetRemote> a4 = Ik7.a(PortfolioDatabaseModule_ProvidesDianaRecommendedPresetRemoteFactory.create(portfolioDatabaseModule, this.o));
        this.e1 = a4;
        this.f1 = Ik7.a(Ap5.a(a4));
        this.g1 = Xy6.a(this.U0, this.V0, this.W0, this.T0, this.Y0, this.a1, W36.a(), E26.a(), this.j, this.c, this.b1, this.A, this.d1, this.I0, this.M0, this.f1, this.y, this.O0);
        this.h1 = Zy6.a(this.V, this.i0, this.P, this.I, this.T0, this.A);
        this.i1 = Et5.a(this.i0, this.c, this.P, this.d, this.V, this.I, this.P0, this.A);
        R27 a5 = R27.a(this.c);
        this.j1 = a5;
        this.k1 = E37.a(this.P, this.y, a5, this.d);
        this.l1 = Zt5.a(this.P, this.c);
        WorkoutSettingRemoteDataSource_Factory create3 = WorkoutSettingRemoteDataSource_Factory.create(this.o);
        this.m1 = create3;
        this.n1 = WorkoutSettingRepository_Factory.create(create3);
        Bt5 a6 = Bt5.a(this.c, W36.a(), this.d, this.P, this.I0, this.M0, this.j, E26.a(), this.k1, this.l1, this.P0, this.A, this.W0, this.n1);
        this.o1 = a6;
        this.p1 = Nj5.a(this.g1, this.h1, this.i1, a6);
        Provider<AppSettingsDatabase> a7 = Ik7.a(PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.q1 = a7;
        Provider<Lr4> a8 = Ik7.a(PortfolioDatabaseModule_ProvidesFlagDaoFactory.create(portfolioDatabaseModule, a7));
        this.r1 = a8;
        this.s1 = Ik7.a(PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory.create(portfolioDatabaseModule, a8));
        Provider<FlagRemoteDataSource> a9 = Ik7.a(PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory.create(portfolioDatabaseModule, this.o));
        this.t1 = a9;
        this.u1 = Ik7.a(Qr4.a(this.s1, a9));
        this.v1 = Ik7.a(Er5.a(this.d, this.A0, this.w0, this.c));
        Provider<ThemeDatabase> a10 = Ik7.a(PortfolioDatabaseModule_ProvideThemeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.w1 = a10;
        Provider<ThemeDao> a11 = Ik7.a(PortfolioDatabaseModule_ProvideThemeDaoFactory.create(portfolioDatabaseModule, a10));
        this.x1 = a11;
        Provider<ThemeRepository> a12 = Ik7.a(ThemeRepository_Factory.create(a11, this.d));
        this.y1 = a12;
        this.z1 = Ik7.a(Ap4.a(uo4, this.d, this.c, this.V, this.T0, this.U0, this.V0, this.i0, this.q, this.P, this.y, this.A, this.p1, this.Y0, this.b1, this.a1, this.s0, this.w0, this.A0, this.I0, this.J0, this.n1, this.u1, this.v1, a12, this.d1, this.M0, this.f1, this.O0, this.W0));
        this.A1 = Ik7.a(Cr5.a(this.y));
        this.B1 = FitnessDataRepository_Factory.create(this.o);
        Jp4 a13 = Jp4.a(uo4, this.b, this.G, this.c);
        this.C1 = a13;
        Provider<ThirdPartyRepository> a14 = Ik7.a(ThirdPartyRepository_Factory.create(a13, this.W, this.d));
        this.D1 = a14;
        this.E1 = Kc7.a(this.W, this.D, this.b0, this.E, this.c0, this.k0, this.l0, this.B1, this.A, this.c, this.V, a14, this.A0, this.w0, this.I0, this.y, this.n1, this.J0, this.Y0, this.d);
        WatchFaceRemoteDataSource_Factory create4 = WatchFaceRemoteDataSource_Factory.create(this.o);
        this.F1 = create4;
        WatchFaceRepository_Factory create5 = WatchFaceRepository_Factory.create(this.b, create4, this.I0);
        this.G1 = create5;
        this.H1 = Ik7.a(Co5.a(this.q, this.c, create5));
        Provider<AddressDatabase> a15 = Ik7.a(PortfolioDatabaseModule_ProvideAddressDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.I1 = a15;
        Provider<AddressDao> a16 = Ik7.a(PortfolioDatabaseModule_ProvideAddressDaoFactory.create(portfolioDatabaseModule, a15));
        this.J1 = a16;
        Provider<LocationSource> a17 = Ik7.a(LocationSource_Factory.create(a16));
        this.K1 = a17;
        this.L1 = Ik7.a(Gs5.a(this.d, a17));
        this.M1 = Ik7.a(Bq5.a(this.k, this.c));
        this.N1 = Ik7.a(Eq5.a(this.k, this.c));
        this.O1 = Ik7.a(Zr4.a(this.P0, this.A0));
        this.P1 = B37.a(W36.a(), E26.a(), this.j, this.I, this.P, this.c);
        Provider<MusicControlComponent> a18 = Ik7.a(Pr5.a());
        this.Q1 = a18;
        this.R1 = Ik7.a(Mp4.a(uo4, this.V, this.v1, this.i, this.A, this.c, this.P1, a18, this.L1));
        this.S1 = Ik7.a(Zp4.a(uo4, this.P, this.d));
        this.T1 = Ik7.a(Zr5.a());
        this.U1 = Ik7.a(Fp4.a(uo4));
        this.V1 = Ik7.a(Bq4.a(uo4));
        this.W1 = Ik7.a(Aq4.a(uo4));
        Ik7.a(UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory.create(uAppSystemVersionRepositoryModule));
        MicroAppLastSettingRepository_Factory create6 = MicroAppLastSettingRepository_Factory.create(this.j0);
        this.X1 = create6;
        this.Y1 = Ik7.a(Rp4.a(uo4, this.c, this.y, this.h1, this.I, this.d, this.c0, this.K, this.S, create6, this.p1, this.u1, this.q, this.G1, this.I0, this.O0, this.M0, this.d1, this.P, this.W0, this.Y0));
        this.Z1 = Ik7.a(Hp4.a(uo4));
        this.a2 = Ik7.a(Qu5.a());
        Provider<InAppNotificationDatabase> a19 = Ik7.a(PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.b2 = a19;
        Provider<InAppNotificationDao> a20 = Ik7.a(PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory.create(portfolioDatabaseModule, a19));
        this.c2 = a20;
        Provider<InAppNotificationRepository> a21 = Ik7.a(InAppNotificationRepository_Factory.create(a20));
        this.d2 = a21;
        this.e2 = Ik7.a(Lp4.a(uo4, a21));
        this.f2 = Ik7.a(Ip4.a(uo4, this.m, this.n));
        this.g2 = Ik7.a(Cq4.a(uo4));
        this.h2 = Ik7.a(Np4.a(uo4));
        this.i2 = Ik7.a(Yp4.a(uo4));
        this.j2 = Ik7.a(Pp4.a(uo4));
        this.k2 = L76.a(this.M0, this.W0, this.U0, this.c);
        this.l2 = Tb6.a(this.V, this.X1, this.i0);
        this.m2 = Yu5.a(this.y);
        Wu5 a22 = Wu5.a(this.y);
        this.n2 = a22;
        this.o2 = Cp6.a(this.m2, a22);
        this.p2 = P96.a(this.O0);
        this.q2 = T96.a(this.O0, this.a1);
        this.r2 = Pt5.a(this.P, this.c);
        this.s2 = Wt5.a(this.P, this.V, this.q, this.d, this.G1, this.M0);
        Rt5 a23 = Rt5.a(this.y, this.P, this.p1, this.d, this.c);
        this.t2 = a23;
        this.u2 = H17.a(this.P, this.r2, this.s2, this.p1, this.c, a23, It5.a(), this.A0, this.d);
        this.v2 = Oa6.a(this.c, this.y);
        this.w2 = La6.a(this.c);
        this.x2 = Dr6.a(this.y1);
        Gu6 a24 = Gu6.a(this.n1);
        this.y2 = a24;
        this.z2 = Nu6.a(a24, this.n1);
        this.A2 = Eu5.a(this.K1, this.d);
        Bu5 a25 = Bu5.a(this.f2);
        this.B2 = a25;
        this.C2 = Hv6.a(this.d, this.A2, a25);
        this.D2 = Bx4.a(this.c, this.s0);
        this.E2 = Qv4.a(this.s0);
        this.F2 = Dv4.a(this.s0, this.J0, this.c);
        this.G2 = Fy4.a(this.c, this.w0);
        this.H2 = Cw4.a(this.E0);
        this.I2 = Ww4.a(this.w0, this.A0);
        this.J2 = Nu4.a(this.c, this.A0);
        this.K2 = Yu4.a(this.w0, this.A0, this.c);
        this.L2 = Mx4.a(this.c, this.A0, this.w0);
        this.M2 = Xx4.a(this.A0);
        this.N2 = Rx4.a(this.A0, this.c);
        this.O2 = Qw4.a(this.A0, this.w0, this.c);
        this.P2 = Vv4.a(this.A0, this.w0);
        this.Q2 = Jw4.a(this.A0, this.c);
        this.R2 = Jv4.a(this.A0);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Xl6 c(Bm6 bm6) {
        Lk7.b(bm6);
        return new e1(bm6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void c0(ProfileSetupPresenter profileSetupPresenter) {
        B5(profileSetupPresenter);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void c1(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        I5(watchAppCommuteTimeManager);
    }

    @DexIgnore
    public final DianaSyncUseCase c4() {
        return new DianaSyncUseCase(this.c.get(), new V36(), this.d.get(), this.P.get(), this.I0.get(), this.M0.get(), this.j.get(), new D26(), R4(), P4(), this.P0.get(), this.A.get(), b4(), Z4());
    }

    @DexIgnore
    public final void c5(Uo4 uo4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.S2 = Ny4.a(this.A0);
        Nt5 a4 = Nt5.a(this.i);
        this.T2 = a4;
        this.U2 = O26.a(this.i, a4);
        this.V2 = Mt6.a(this.y1);
        this.W2 = Hs6.a(this.y1);
        this.X2 = Bs6.a(this.y1);
        this.Y2 = At6.a(this.y1);
        this.Z2 = Vr6.a(this.y1);
        this.a3 = Pr6.a(this.y1);
        this.b3 = Jr6.a(this.y1);
        this.c3 = Os6.a(this.y1);
        this.d3 = Us6.a(this.y1);
        this.e3 = Gt6.a(this.y1);
        this.f3 = Tt6.a(this.y1);
        this.g3 = K26.a(this.i);
        this.h3 = Ln6.a(this.m0);
        this.i3 = Qn6.a(this.m0);
        Kt5 a5 = Kt5.a(this.d, this.y, this.x);
        this.j3 = a5;
        this.k3 = Jp6.a(this.D, this.E, this.c0, this.y, a5);
        Xb7 a6 = Xb7.a(this.Y0, this.I0);
        this.l3 = a6;
        this.m3 = Ya7.a(this.y, this.Y0, a6);
        An5 a7 = An5.a(this.d, this.P, this.W0);
        this.n3 = a7;
        this.o3 = C87.a(a7, this.y, this.t, this.M0, this.I0, this.d1, this.W0, this.Y0);
        this.p3 = I87.a(this.V0, this.W0);
        this.q3 = N87.a(this.d1);
        this.r3 = Q97.a(this.O0);
        this.s3 = V97.a(this.d1, this.I0);
        this.t3 = Ca7.a(this.d1, this.I0);
        this.u3 = Pa7.a(this.Y0, this.I0, this.M0, this.l3);
        this.v3 = Ta7.a(this.d, this.Y0, this.I0, this.d1, this.y, this.n3, this.t);
        this.w3 = Ih5.a(this.y);
        Provider<MigrationHelper> a8 = Ik7.a(Qp4.a(uo4, this.c, this.P, this.q, this.Y1));
        this.x3 = a8;
        this.y3 = Mv6.a(a8, this.p1, this.c);
        Kk7.Bi b4 = Kk7.b(59);
        b4.c(WatchAppEditViewModel.class, this.k2);
        b4.c(HybridCustomizeViewModel.class, this.l2);
        b4.c(ProfileEditViewModel.class, this.o2);
        b4.c(EditPhotoViewModel.class, this.p2);
        b4.c(S96.class, this.q2);
        b4.c(WatchSettingViewModel.class, this.u2);
        b4.c(CommuteTimeWatchAppSettingsViewModel.class, this.v2);
        b4.c(CommuteTimeSettingsDetailViewModel.class, this.w2);
        b4.c(Zc6.class, Ad6.a());
        b4.c(R16.class, S16.a());
        b4.c(ThemesViewModel.class, this.x2);
        b4.c(WorkoutSettingViewModel.class, this.z2);
        b4.c(MapPickerViewModel.class, this.C2);
        b4.c(Ax4.class, this.D2);
        b4.c(BCMainViewModel.class, this.E2);
        b4.c(BCCreateSocialProfileViewModel.class, this.F2);
        b4.c(Fx4.class, Gx4.a());
        b4.c(BCFriendTabViewModel.class, this.G2);
        b4.c(BCNotificationViewModel.class, this.H2);
        b4.c(BCFindFriendsViewModel.class, this.I2);
        b4.c(BCCreateChallengeIntroViewModel.class, Su4.a());
        b4.c(BCCreateChallengeInputViewModel.class, this.J2);
        b4.c(BCInviteFriendViewModel.class, this.K2);
        b4.c(BCCreateSubTabViewModel.class, this.L2);
        b4.c(BCRecommendationViewModel.class, this.M2);
        b4.c(BCCurrentChallengeSubTabViewModel.class, this.N2);
        b4.c(BCWaitingChallengeDetailViewModel.class, this.O2);
        b4.c(BCMemberInChallengeViewModel.class, this.P2);
        b4.c(BCOverviewLeaderBoardViewModel.class, this.Q2);
        b4.c(BCLeaderBoardViewModel.class, this.R2);
        b4.c(BCHistoryViewModel.class, this.S2);
        b4.c(QuickResponseViewModel.class, this.U2);
        b4.c(CustomizeTextViewModel.class, this.V2);
        b4.c(CustomizeButtonViewModel.class, this.W2);
        b4.c(CustomizeBackgroundViewModel.class, this.X2);
        b4.c(CustomizeRingChartViewModel.class, this.Y2);
        b4.c(CustomizeActivityChartViewModel.class, this.Z2);
        b4.c(CustomizeActiveMinutesChartViewModel.class, this.a3);
        b4.c(CustomizeActiveCaloriesChartViewModel.class, this.b3);
        b4.c(CustomizeGoalTrackingChartViewModel.class, this.c3);
        b4.c(CustomizeHeartRateChartViewModel.class, this.d3);
        b4.c(CustomizeSleepChartViewModel.class, this.e3);
        b4.c(UserCustomizeThemeViewModel.class, this.f3);
        b4.c(J26.class, this.g3);
        b4.c(WorkoutEditViewModel.class, this.h3);
        b4.c(Pn6.class, this.i3);
        b4.c(ProfileGoalEditViewModel.class, this.k3);
        b4.c(WatchFaceGalleryViewModel.class, this.m3);
        b4.c(WatchFaceEditViewModel.class, this.o3);
        b4.c(WatchFaceComplicationViewModel.class, this.p3);
        b4.c(WatchFaceRingViewModel.class, this.q3);
        b4.c(P97.class, this.r3);
        b4.c(WatchFaceStickerViewModel.class, this.s3);
        b4.c(WatchFaceTemplateViewModel.class, this.t3);
        b4.c(WatchFaceListViewModel.class, this.u3);
        b4.c(MyFaceViewModel.class, this.v3);
        b4.c(Ga7.class, Ha7.a());
        b4.c(Hh5.class, this.w3);
        b4.c(MigrationViewModel.class, this.y3);
        Kk7 b5 = b4.b();
        this.z3 = b5;
        this.A3 = Ik7.a(Qo4.a(b5));
        this.B3 = Ik7.a(PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory.create(portfolioDatabaseModule, this.j));
        this.C3 = Ik7.a(RepositoriesModule_ProvideServerSettingLocalDataSourceFactory.create(repositoriesModule));
        this.D3 = Ik7.a(RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory.create(repositoriesModule, this.o));
        this.E3 = Ik7.a(Xp4.a(uo4));
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ot6 d(Rt6 rt6) {
        Lk7.b(rt6);
        return new y2(rt6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ju4 d0() {
        return new Ii();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public R97 d1() {
        return new h3();
    }

    @DexIgnore
    public final DianaWatchFaceRemoteDataSource d4() {
        return new DianaWatchFaceRemoteDataSource(this.o.get(), this.K0.get());
    }

    @DexIgnore
    public final AlarmReceiver d5(AlarmReceiver alarmReceiver) {
        Tp5.e(alarmReceiver, this.y.get());
        Tp5.d(alarmReceiver, this.c.get());
        Tp5.c(alarmReceiver, this.P.get());
        Tp5.a(alarmReceiver, this.B.get());
        Tp5.b(alarmReceiver, this.A.get());
        return alarmReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Vs6 e(Ys6 ys6) {
        Lk7.b(ys6);
        return new r0(ys6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public J87 e0() {
        return new g3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public J17 e1(M17 m17) {
        Lk7.b(m17);
        return new a0(m17);
    }

    @DexIgnore
    public final DianaWatchFaceRepository e4() {
        return new DianaWatchFaceRepository(this.I0.get(), d4());
    }

    @DexIgnore
    public final AppHelper e5(AppHelper appHelper) {
        Ek5.c(appHelper, this.c.get());
        Ek5.b(appHelper, this.P.get());
        Ek5.d(appHelper, this.y.get());
        Ek5.a(appHelper, Y3());
        return appHelper;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ps6 f(Ss6 ss6) {
        Lk7.b(ss6);
        return new q0(ss6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void f0(ComplicationWeatherService complicationWeatherService) {
        m5(complicationWeatherService);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Za6 f1(Db6 db6) {
        Lk7.b(db6);
        return new z2(db6);
    }

    @DexIgnore
    public final DownloadUserInfoUseCase f4() {
        return new DownloadUserInfoUseCase(this.y.get());
    }

    @DexIgnore
    public final AppPackageInstallReceiver f5(AppPackageInstallReceiver appPackageInstallReceiver) {
        Vp5.e(appPackageInstallReceiver, this.I.get());
        Vp5.f(appPackageInstallReceiver, this.c.get());
        Vp5.a(appPackageInstallReceiver, a4());
        Vp5.c(appPackageInstallReceiver, new V36());
        Vp5.b(appPackageInstallReceiver, new D26());
        Vp5.d(appPackageInstallReceiver, this.j.get());
        return appPackageInstallReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Fq6 g(Jq6 jq6) {
        Lk7.b(jq6);
        return new j2(jq6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void g0(Pl5 pl5) {
        J5(pl5);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Hc6 g1(Kc6 kc6) {
        Lk7.b(kc6);
        return new q1(kc6);
    }

    @DexIgnore
    public final S27 g4() {
        return new S27(this.c.get());
    }

    @DexIgnore
    public final AppPackageRemoveReceiver g5(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        Wp5.a(appPackageRemoveReceiver, this.I.get());
        return appPackageRemoveReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ay5 h(Dy5 dy5) {
        Lk7.b(dy5);
        return new j0(dy5);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ze6 h0(If6 if6) {
        Lk7.b(if6);
        return new Fi(if6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Fo6 h1(Jo6 jo6) {
        Lk7.b(jo6);
        return new Bi(jo6);
    }

    @DexIgnore
    public final FetchActivities h4() {
        return new FetchActivities(this.W.get(), this.y.get(), p4());
    }

    @DexIgnore
    public final BCNotificationActionReceiver h5(BCNotificationActionReceiver bCNotificationActionReceiver) {
        Ry4.b(bCNotificationActionReceiver, this.w0.get());
        Ry4.a(bCNotificationActionReceiver, this.A0.get());
        Ry4.c(bCNotificationActionReceiver, this.c.get());
        return bCNotificationActionReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void i(AlarmReceiver alarmReceiver) {
        d5(alarmReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Gw6 i0(Jw6 jw6) {
        Lk7.b(jw6);
        return new c2(jw6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void i1(FileDownloadManager fileDownloadManager) {
        r5(fileDownloadManager);
    }

    @DexIgnore
    public final FetchDailyGoalTrackingSummaries i4() {
        return new FetchDailyGoalTrackingSummaries(this.c0.get(), this.y.get());
    }

    @DexIgnore
    public final BaseActivity i5(BaseActivity baseActivity) {
        Ms5.e(baseActivity, this.y.get());
        Ms5.d(baseActivity, this.c.get());
        Ms5.a(baseActivity, this.P.get());
        Ms5.c(baseActivity, this.Y1.get());
        Ms5.b(baseActivity, new Gu5());
        return baseActivity;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public B56 j(F56 f56) {
        Lk7.b(f56);
        return new x1(f56);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Uu4 j0() {
        return new Qi();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public M36 j1(N36 n36) {
        Lk7.b(n36);
        return new m2(n36);
    }

    @DexIgnore
    public final FetchDailyHeartRateSummaries j4() {
        return new FetchDailyHeartRateSummaries(this.l0.get(), this.y.get(), p4());
    }

    @DexIgnore
    public final BootReceiver j5(BootReceiver bootReceiver) {
        Yp5.a(bootReceiver, this.B.get());
        return bootReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Al6 k(El6 el6) {
        Lk7.b(el6);
        return new Ei(el6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public I86 k0(M86 m86) {
        Lk7.b(m86);
        return new p2(m86);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Cx4 k1() {
        return new Hi();
    }

    @DexIgnore
    public final FetchGoalTrackingData k4() {
        return new FetchGoalTrackingData(this.c0.get(), this.y.get());
    }

    @DexIgnore
    public final CloudImageHelper k5(CloudImageHelper cloudImageHelper) {
        CloudImageHelper_MembersInjector.injectMAppExecutors(cloudImageHelper, this.G.get());
        CloudImageHelper_MembersInjector.injectMApp(cloudImageHelper, this.d.get());
        return cloudImageHelper;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void l(AppPackageInstallReceiver appPackageInstallReceiver) {
        f5(appPackageInstallReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void l0(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        s5(fossilFirebaseMessagingService);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void l1(X27 x27) {
        x5(x27);
    }

    @DexIgnore
    public final FetchHeartRateSamples l4() {
        return new FetchHeartRateSamples(this.k0.get(), p4(), this.y.get());
    }

    @DexIgnore
    public final CommuteTimeService l5(CommuteTimeService commuteTimeService) {
        Lr5.a(commuteTimeService, this.a2.get());
        return commuteTimeService;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public F46 m(J46 j46) {
        Lk7.b(j46);
        return new u1(j46);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public U76 m0(X76 x76) {
        Lk7.b(x76);
        return new e0(x76);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public H16 m1(L16 l16) {
        Lk7.b(l16);
        return new v1(l16);
    }

    @DexIgnore
    public final FetchSleepSessions m4() {
        return new FetchSleepSessions(this.b0.get(), this.y.get(), p4());
    }

    @DexIgnore
    public final ComplicationWeatherService m5(ComplicationWeatherService complicationWeatherService) {
        Gr5.a(complicationWeatherService, this.K1.get());
        Ir5.b(complicationWeatherService, v4());
        Ir5.e(complicationWeatherService, this.R.get());
        Ir5.d(complicationWeatherService, this.c.get());
        Ir5.c(complicationWeatherService, this.d.get());
        Ir5.a(complicationWeatherService, this.t.get());
        Ir5.f(complicationWeatherService, this.y.get());
        return complicationWeatherService;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void n(DeviceHelper deviceHelper) {
        p5(deviceHelper);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public N56 n0(R56 r56) {
        Lk7.b(r56);
        return new y1(r56);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ov6 n1(Rv6 rv6) {
        Lk7.b(rv6);
        return new b1(rv6);
    }

    @DexIgnore
    public final FetchSleepSummaries n4() {
        return new FetchSleepSummaries(this.E.get(), this.y.get(), this.b0.get(), p4());
    }

    @DexIgnore
    public final L37 n5(L37 l37) {
        N37.a(l37, this.P.get());
        N37.b(l37, this.j.get());
        return l37;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public U66 o(V66 v66) {
        Lk7.b(v66);
        return new y0(v66);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ev4 o0() {
        return new Li();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void o1(LocaleChangedReceiver localeChangedReceiver) {
    }

    @DexIgnore
    public final FetchSummaries o4() {
        return new FetchSummaries(this.D.get(), p4(), this.y.get(), this.W.get());
    }

    @DexIgnore
    public final DebugActivity o5(DebugActivity debugActivity) {
        Ms5.e(debugActivity, this.y.get());
        Ms5.d(debugActivity, this.c.get());
        Ms5.a(debugActivity, this.P.get());
        Ms5.c(debugActivity, this.Y1.get());
        Ms5.b(debugActivity, new Gu5());
        Rs5.g(debugActivity, this.c.get());
        Rs5.e(debugActivity, O4());
        Rs5.c(debugActivity, this.Z1.get());
        Rs5.d(debugActivity, this.F.get());
        Rs5.b(debugActivity, this.q.get());
        Rs5.f(debugActivity, this.A1.get());
        Rs5.a(debugActivity, a4());
        return debugActivity;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void p(URLRequestTaskHelper uRLRequestTaskHelper) {
        G5(uRLRequestTaskHelper);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public W26 p0(B36 b36) {
        Lk7.b(b36);
        return new b2(b36);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Nz5 p1(Oz5 oz5) {
        Lk7.b(oz5);
        return new l1(oz5);
    }

    @DexIgnore
    public final FitnessDataRepository p4() {
        return new FitnessDataRepository(this.o.get());
    }

    @DexIgnore
    public final DeviceHelper p5(DeviceHelper deviceHelper) {
        Ok5.b(deviceHelper, this.c.get());
        Ok5.a(deviceHelper, this.P.get());
        return deviceHelper;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public A66 q(E66 e66) {
        Lk7.b(e66);
        return new z1(e66);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public La7 q0() {
        return new e3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public W77 q1() {
        return new c3();
    }

    @DexIgnore
    public final GeneratePassphraseUseCase q4() {
        return new GeneratePassphraseUseCase(g4());
    }

    @DexIgnore
    public final DeviceUtils q5(DeviceUtils deviceUtils) {
        Q37.a(deviceUtils, this.P.get());
        Q37.d(deviceUtils, this.c.get());
        Q37.e(deviceUtils, this.y.get());
        Q37.c(deviceUtils, this.F.get());
        Q37.b(deviceUtils, this.Z1.get());
        return deviceUtils;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public H07 r(K07 k07) {
        Lk7.b(k07);
        return new u2(k07);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Xv6 r0(Aw6 aw6) {
        Lk7.b(aw6);
        return new d1(aw6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void r1(BCNotificationActionReceiver bCNotificationActionReceiver) {
        h5(bCNotificationActionReceiver);
    }

    @DexIgnore
    public final GetDianaDeviceSettingUseCase r4() {
        return new GetDianaDeviceSettingUseCase(U4(), T3(), b4(), Q3(), e4(), I4(), new V36(), new D26(), this.j.get(), this.c.get(), X4(), this.A.get(), this.d1.get(), this.I0.get(), this.M0.get(), this.f1.get(), this.y.get(), this.O0.get());
    }

    @DexIgnore
    public final FileDownloadManager r5(FileDownloadManager fileDownloadManager) {
        Dn5.a(fileDownloadManager, this.g2.get());
        return fileDownloadManager;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void s(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        g5(appPackageRemoveReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Wa7 s0() {
        return new d3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Eg6 s1(Mg6 mg6) {
        Lk7.b(mg6);
        return new c0(mg6);
    }

    @DexIgnore
    public final GetHybridDeviceSettingUseCase s4() {
        return new GetHybridDeviceSettingUseCase(this.V.get(), this.i0.get(), this.P.get(), this.I.get(), Q3(), this.A.get());
    }

    @DexIgnore
    public final FossilFirebaseMessagingService s5(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        Jr5.d(fossilFirebaseMessagingService, this.e2.get());
        Jr5.c(fossilFirebaseMessagingService, this.v1.get());
        Jr5.e(fossilFirebaseMessagingService, this.c.get());
        Jr5.a(fossilFirebaseMessagingService, this.J0.get());
        Jr5.b(fossilFirebaseMessagingService, this.u1.get());
        return fossilFirebaseMessagingService;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Mi6 t(Ui6 ui6) {
        Lk7.b(ui6);
        return new h1(ui6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Cd6 t0(Dd6 dd6) {
        Lk7.b(dd6);
        return new v0(dd6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void t1(CommuteTimeService commuteTimeService) {
        l5(commuteTimeService);
    }

    @DexIgnore
    public final GetSecretKeyUseCase t4() {
        return new GetSecretKeyUseCase(g4(), this.P.get());
    }

    @DexIgnore
    public final FossilNotificationListenerService t5(FossilNotificationListenerService fossilNotificationListenerService) {
        Xq5.d(fossilNotificationListenerService, this.Q1.get());
        Xq5.b(fossilNotificationListenerService, this.k.get());
        Xq5.c(fossilNotificationListenerService, this.T1.get());
        Xq5.a(fossilNotificationListenerService, this.G.get());
        Xq5.e(fossilNotificationListenerService, this.c.get());
        Xq5.f(fossilNotificationListenerService, this.y.get());
        return fossilNotificationListenerService;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Bz6 u(Gz6 gz6) {
        Lk7.b(gz6);
        return new f2(gz6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Ht6 u0(Kt6 kt6) {
        Lk7.b(kt6);
        return new t0(kt6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void u1(BaseActivity baseActivity) {
        i5(baseActivity);
    }

    @DexIgnore
    public final GetUser u4() {
        return new GetUser(this.y.get());
    }

    @DexIgnore
    public final LightAndHapticsManager u5(LightAndHapticsManager lightAndHapticsManager) {
        Gn5.a(lightAndHapticsManager, this.P.get());
        Gn5.b(lightAndHapticsManager, this.c.get());
        return lightAndHapticsManager;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Yq6 v(Br6 br6) {
        Lk7.b(br6);
        return new v2(br6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public L96 v0() {
        return new z0();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Iv6 v1() {
        return new r1();
    }

    @DexIgnore
    public final GetWeather v4() {
        return new GetWeather(this.o.get());
    }

    @DexIgnore
    public final LoginPresenter v5(LoginPresenter loginPresenter) {
        Cv6.s(loginPresenter, y4());
        Cv6.v(loginPresenter, B4());
        Cv6.h(loginPresenter, f4());
        Cv6.y(loginPresenter, new ReconnectDeviceUseCase());
        Cv6.g(loginPresenter, a4());
        Cv6.D(loginPresenter, this.y.get());
        Cv6.f(loginPresenter, this.P.get());
        Cv6.z(loginPresenter, this.c.get());
        Cv6.i(loginPresenter, h4());
        Cv6.p(loginPresenter, o4());
        Cv6.C(loginPresenter, this.R.get());
        Cv6.n(loginPresenter, m4());
        Cv6.o(loginPresenter, n4());
        Cv6.m(loginPresenter, l4());
        Cv6.k(loginPresenter, j4());
        Cv6.t(loginPresenter, z4());
        Cv6.G(loginPresenter, this.V1.get());
        Cv6.u(loginPresenter, A4());
        Cv6.x(loginPresenter, D4());
        Cv6.w(loginPresenter, C4());
        Cv6.e(loginPresenter, S3());
        Cv6.d(loginPresenter, this.P0.get());
        Cv6.B(loginPresenter, this.D.get());
        Cv6.A(loginPresenter, this.E.get());
        Cv6.r(loginPresenter, this.c0.get());
        Cv6.j(loginPresenter, i4());
        Cv6.l(loginPresenter, k4());
        Cv6.q(loginPresenter, t4());
        Cv6.E(loginPresenter, X4());
        Cv6.c(loginPresenter, this.A.get());
        Cv6.I(loginPresenter, this.s0.get());
        Cv6.a(loginPresenter, this.J0.get());
        Cv6.b(loginPresenter, this.u1.get());
        Cv6.F(loginPresenter, Z4());
        Cv6.H(loginPresenter);
        return loginPresenter;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public E97 w() {
        return new f3();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void w0(TimeTickReceiver timeTickReceiver) {
        F5(timeTickReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public E27 w1(J27 j27) {
        Lk7.b(j27);
        return new m3(j27);
    }

    @DexIgnore
    public final Kk4 w4() {
        return Jp4.c(this.a, this.b.get(), this.G.get(), this.c.get());
    }

    @DexIgnore
    public final MFDeviceService w5(MFDeviceService mFDeviceService) {
        Zq5.q(mFDeviceService, this.c.get());
        Zq5.f(mFDeviceService, this.P.get());
        Zq5.a(mFDeviceService, this.W.get());
        Zq5.t(mFDeviceService, this.D.get());
        Zq5.r(mFDeviceService, this.b0.get());
        Zq5.s(mFDeviceService, this.E.get());
        Zq5.v(mFDeviceService, this.y.get());
        Zq5.p(mFDeviceService, this.V.get());
        Zq5.n(mFDeviceService, this.i0.get());
        Zq5.k(mFDeviceService, this.k0.get());
        Zq5.l(mFDeviceService, this.l0.get());
        Zq5.y(mFDeviceService, this.m0.get());
        Zq5.h(mFDeviceService, p4());
        Zq5.j(mFDeviceService, this.c0.get());
        Zq5.d(mFDeviceService, this.G.get());
        Zq5.b(mFDeviceService, this.P0.get());
        Zq5.c(mFDeviceService, this.d.get());
        Zq5.m(mFDeviceService, this.R1.get());
        Zq5.u(mFDeviceService, this.D1.get());
        Zq5.g(mFDeviceService, g4());
        Zq5.w(mFDeviceService, R4());
        Zq5.i(mFDeviceService, this.C.get());
        Zq5.x(mFDeviceService, this.S1.get());
        Zq5.e(mFDeviceService, this.v1.get());
        Zq5.z(mFDeviceService, this.L1.get());
        Zq5.o(mFDeviceService, this.Q1.get());
        return mFDeviceService;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Dv6 x() {
        return new p1();
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void x0(Mn5 mn5) {
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Vt6 x1(Yt6 yt6) {
        Lk7.b(yt6);
        return new g2(yt6);
    }

    @DexIgnore
    public final HybridSyncUseCase x4() {
        return new HybridSyncUseCase(this.i0.get(), this.c.get(), this.P.get(), this.d.get(), this.V.get(), this.I.get(), this.P0.get(), this.A.get());
    }

    @DexIgnore
    public final X27 x5(X27 x27) {
        Y27.a(x27, this.W.get());
        return x27;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Pq6 y(Tq6 tq6) {
        Lk7.b(tq6);
        return new h2(tq6);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public L06 y0(P06 p06) {
        Lk7.b(p06);
        return new s1(p06);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public I26 y1() {
        return new l2();
    }

    @DexIgnore
    public final LoginEmailUseCase y4() {
        return new LoginEmailUseCase(this.y.get());
    }

    @DexIgnore
    public final NetworkChangedReceiver y5(NetworkChangedReceiver networkChangedReceiver) {
        Zp5.a(networkChangedReceiver, this.y.get());
        return networkChangedReceiver;
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void z(BootReceiver bootReceiver) {
        j5(bootReceiver);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public void z0(SignUpPresenter signUpPresenter) {
        C5(signUpPresenter);
    }

    @DexIgnore
    @Override // com.mapped.Iface
    public Xy5 z1(Hz5 hz5) {
        Lk7.b(hz5);
        return new k1(hz5);
    }

    @DexIgnore
    public final Dv5 z4() {
        return new Dv5(this.U1.get());
    }

    @DexIgnore
    public final NotificationReceiver z5(NotificationReceiver notificationReceiver) {
        Fo5.b(notificationReceiver, this.c.get());
        Fo5.a(notificationReceiver, a4());
        Fo5.c(notificationReceiver, this.y.get());
        return notificationReceiver;
    }
}
