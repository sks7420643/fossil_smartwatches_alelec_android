package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ah7 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        if (Fg7.M() && Ig7.r != null) {
            if (Fg7.J()) {
                Gh7.b(Ig7.r).g(new Mg7(Ig7.r, Ig7.a(Ig7.r, false, null), 2, th, thread, null), null, false, true);
                Ig7.m.c("MTA has caught the following uncaught exception:");
                Ig7.m.g(th);
            }
            Ig7.u(Ig7.r);
            if (Ig7.n != null) {
                Ig7.m.b("Call the original uncaught exception handler.");
                if (!(Ig7.n instanceof Ah7)) {
                    Ig7.n.uncaughtException(thread, th);
                }
            }
        }
    }
}
