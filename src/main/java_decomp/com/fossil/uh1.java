package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uh1 {
    @DexIgnore
    public /* final */ List<Ai<?, ?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<Z, R> {
        @DexIgnore
        public /* final */ Class<Z> a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ Th1<Z, R> c;

        @DexIgnore
        public Ai(Class<Z> cls, Class<R> cls2, Th1<Z, R> th1) {
            this.a = cls;
            this.b = cls2;
            this.c = th1;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public <Z, R> Th1<Z, R> a(Class<Z> cls, Class<R> cls2) {
        Th1<Z, R> th1;
        synchronized (this) {
            if (cls2.isAssignableFrom(cls)) {
                th1 = Vh1.b();
            } else {
                for (Ai<?, ?> ai : this.a) {
                    if (ai.a(cls, cls2)) {
                        th1 = ai.c;
                    }
                }
                throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
            }
        }
        return th1;
    }

    @DexIgnore
    public <Z, R> List<Class<R>> b(Class<Z> cls, Class<R> cls2) {
        synchronized (this) {
            ArrayList arrayList = new ArrayList();
            if (cls2.isAssignableFrom(cls)) {
                arrayList.add(cls2);
                return arrayList;
            }
            for (Ai<?, ?> ai : this.a) {
                if (ai.a(cls, cls2)) {
                    arrayList.add(cls2);
                }
            }
            return arrayList;
        }
    }

    @DexIgnore
    public <Z, R> void c(Class<Z> cls, Class<R> cls2, Th1<Z, R> th1) {
        synchronized (this) {
            this.a.add(new Ai<>(cls, cls2, th1));
        }
    }
}
