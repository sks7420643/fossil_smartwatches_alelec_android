package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import com.mapped.Wg6;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P6 {
    @DexIgnore
    public static /* final */ O6 b; // = new O6(null);
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic a;

    @DexIgnore
    public P6(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        this.a = bluetoothGattCharacteristic;
        O6 o6 = b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        Wg6.b(uuid, "this.bluetoothGattCharacteristic.uuid");
        o6.a(uuid);
    }
}
