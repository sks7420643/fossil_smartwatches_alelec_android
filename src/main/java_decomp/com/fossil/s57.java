package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum s57 {
    Left,
    Right,
    Top,
    Bottom;
    
    @DexIgnore
    public static /* final */ List<s57> FREEDOM; // = Arrays.asList(values());
    @DexIgnore
    public static /* final */ List<s57> FREEDOM_NO_BOTTOM; // = Arrays.asList(Top, Left, Right);
    @DexIgnore
    public static /* final */ List<s57> HORIZONTAL; // = Arrays.asList(Left, Right);
    @DexIgnore
    public static /* final */ List<s57> VERTICAL; // = Arrays.asList(Top, Bottom);

    @DexIgnore
    public static List<s57> from(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? FREEDOM : VERTICAL : HORIZONTAL : FREEDOM_NO_BOTTOM : FREEDOM;
    }
}
