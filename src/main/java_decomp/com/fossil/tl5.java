package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tl5 {
    @DexIgnore
    public /* final */ Map<String, String> a; // = new HashMap();
    @DexIgnore
    public /* final */ AnalyticsHelper b;

    @DexIgnore
    public Tl5(AnalyticsHelper analyticsHelper) {
        Wg6.c(analyticsHelper, "mAnalyticsInstance");
        this.b = analyticsHelper;
    }

    @DexIgnore
    public final Tl5 a(String str, String str2) {
        Wg6.c(str, "propertyName");
        Wg6.c(str2, "propertyValue");
        this.a.put(str, str2);
        return this;
    }

    @DexIgnore
    public final void b() {
        this.b.r(this.a);
    }
}
