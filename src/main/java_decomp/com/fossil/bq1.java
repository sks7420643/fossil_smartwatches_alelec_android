package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bq1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ EnumMap<Mu1, Ap1[]> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Bq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bq1 createFromParcel(Parcel parcel) {
            return new Bq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bq1[] newArray(int i) {
            return new Bq1[i];
        }
    }

    @DexIgnore
    public Bq1(byte b, int i, EnumMap<Mu1, Ap1[]> enumMap) {
        super(E90.WATCH_APP_LIFE_CYCLE, b);
        this.d = i;
        this.e = enumMap;
    }

    @DexIgnore
    public /* synthetic */ Bq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.d = parcel.readInt();
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            this.e = (EnumMap) readSerializable;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.util.EnumMap<com.fossil.blesdk.model.enumerate.WatchAppStatus, kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchAppId>>");
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Bq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Bq1 bq1 = (Bq1) obj;
            if (this.d != bq1.d) {
                return false;
            }
            return !(Wg6.a(this.e, bq1.e) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.WatchAppLifeCycleNotification");
    }

    @DexIgnore
    public final EnumMap<Mu1, Ap1[]> getWatchAppStatus() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + Integer.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(super.toJSONObject(), Jd0.q, Integer.valueOf(this.d));
        for (Map.Entry<K, Ap1[]> entry : this.e.entrySet()) {
            JSONArray jSONArray = new JSONArray();
            Ap1[] value = entry.getValue();
            Wg6.b(value, "entry.value");
            for (Ap1 ap1 : value) {
                jSONArray.put(ap1.a());
            }
            if (jSONArray.length() > 0) {
                k.put(entry.getKey().a(), jSONArray);
            }
        }
        return k;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.e);
        }
    }
}
