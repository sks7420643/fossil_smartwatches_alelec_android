package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jx3 extends B04 implements Cloneable {
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;

    @DexIgnore
    public Jx3(float f2, float f3, float f4) {
        this.c = f2;
        this.b = f3;
        this.e = f4;
        if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            return;
        }
        throw new IllegalArgumentException("cradleVerticalOffset must be positive.");
    }

    @DexIgnore
    @Override // com.fossil.B04
    public void d(float f2, float f3, float f4, I04 i04) {
        float f5 = this.d;
        if (f5 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            i04.l(f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float f6 = (f5 + (this.c * 2.0f)) / 2.0f;
        float f7 = f4 * this.b;
        float f8 = f3 + this.f;
        float f9 = (this.e * f4) + ((1.0f - f4) * f6);
        if (f9 / f6 >= 1.0f) {
            i04.l(f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float f10 = f6 + f7;
        float f11 = f9 + f7;
        float sqrt = (float) Math.sqrt((double) ((f10 * f10) - (f11 * f11)));
        float f12 = f8 - sqrt;
        float f13 = f8 + sqrt;
        float degrees = (float) Math.toDegrees(Math.atan((double) (sqrt / f11)));
        float f14 = 90.0f - degrees;
        i04.l(f12, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float f15 = f7 * 2.0f;
        i04.a(f12 - f7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f12 + f7, f15, 270.0f, degrees);
        i04.a(f8 - f6, (-f6) - f9, f8 + f6, f6 - f9, 180.0f - f14, (2.0f * f14) - 180.0f);
        i04.a(f13 - f7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f13 + f7, f15, 270.0f - degrees, degrees);
        i04.l(f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public float e() {
        return this.e;
    }

    @DexIgnore
    public float g() {
        return this.c;
    }

    @DexIgnore
    public float i() {
        return this.b;
    }

    @DexIgnore
    public float j() {
        return this.d;
    }

    @DexIgnore
    public float l() {
        return this.f;
    }

    @DexIgnore
    public void n(float f2) {
        this.e = f2;
    }

    @DexIgnore
    public void o(float f2) {
        this.c = f2;
    }

    @DexIgnore
    public void p(float f2) {
        this.b = f2;
    }

    @DexIgnore
    public void q(float f2) {
        this.d = f2;
    }

    @DexIgnore
    public void r(float f2) {
        this.f = f2;
    }
}
