package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class S21<T> extends T21<T> {
    @DexIgnore
    public static /* final */ String h; // = X01.f("BrdcstRcvrCnstrntTrckr");
    @DexIgnore
    public /* final */ BroadcastReceiver g; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends BroadcastReceiver {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                S21.this.h(context, intent);
            }
        }
    }

    @DexIgnore
    public S21(Context context, K41 k41) {
        super(context, k41);
    }

    @DexIgnore
    @Override // com.fossil.T21
    public void e() {
        X01.c().a(h, String.format("%s: registering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.b.registerReceiver(this.g, g());
    }

    @DexIgnore
    @Override // com.fossil.T21
    public void f() {
        X01.c().a(h, String.format("%s: unregistering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.b.unregisterReceiver(this.g);
    }

    @DexIgnore
    public abstract IntentFilter g();

    @DexIgnore
    public abstract void h(Context context, Intent intent);
}
