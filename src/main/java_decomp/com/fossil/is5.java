package com.fossil;

import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Is5 {
    @DexIgnore
    public static /* final */ Is5 a; // = new Is5();

    @DexIgnore
    public final TransitionSet a(long j) {
        TransitionSet transitionSet = new TransitionSet();
        Slide slide = new Slide(80);
        slide.setInterpolator(AnimationUtils.loadInterpolator(PortfolioApp.get.instance(), 17563662));
        slide.setDuration(j);
        slide.excludeTarget(16908336, true);
        slide.excludeTarget(16908335, true);
        transitionSet.addTransition(slide);
        return transitionSet;
    }

    @DexIgnore
    public final Transition b() {
        Fade fade = new Fade();
        fade.excludeTarget(16908336, true);
        fade.excludeTarget(16908335, true);
        return fade;
    }

    @DexIgnore
    public final Transition c(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "mApp");
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.setOrdering(0);
        transitionSet.setDuration(500L);
        ChangeImageTransform changeImageTransform = new ChangeImageTransform();
        changeImageTransform.addTarget(portfolioApp.getString(2131887594));
        changeImageTransform.addTarget(portfolioApp.getString(2131887595));
        transitionSet.addTransition(changeImageTransform);
        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.addTarget(portfolioApp.getString(2131887594));
        changeBounds.addTarget(portfolioApp.getString(2131887595));
        changeBounds.addTarget(portfolioApp.getString(2131887600));
        changeBounds.addTarget(portfolioApp.getString(2131887593));
        changeBounds.addTarget(portfolioApp.getString(2131887590));
        changeBounds.addTarget(portfolioApp.getString(2131887592));
        changeBounds.addTarget(portfolioApp.getString(2131887591));
        changeBounds.addTarget(portfolioApp.getString(2131887604));
        changeBounds.addTarget(portfolioApp.getString(2131887603));
        changeBounds.addTarget(portfolioApp.getString(2131887602));
        changeBounds.addTarget(portfolioApp.getString(2131887598));
        changeBounds.addTarget(portfolioApp.getString(2131887597));
        changeBounds.addTarget(portfolioApp.getString(2131887599));
        changeBounds.addTarget(portfolioApp.getString(2131887601));
        transitionSet.addTransition(changeBounds);
        ChangeTransform changeTransform = new ChangeTransform();
        changeTransform.addTarget(portfolioApp.getString(2131887594));
        changeTransform.addTarget(portfolioApp.getString(2131887595));
        changeTransform.addTarget(portfolioApp.getString(2131887600));
        changeTransform.addTarget(portfolioApp.getString(2131887593));
        changeTransform.addTarget(portfolioApp.getString(2131887590));
        changeTransform.addTarget(portfolioApp.getString(2131887592));
        changeTransform.addTarget(portfolioApp.getString(2131887591));
        changeTransform.addTarget(portfolioApp.getString(2131887604));
        changeTransform.addTarget(portfolioApp.getString(2131887603));
        changeTransform.addTarget(portfolioApp.getString(2131887602));
        changeTransform.addTarget(portfolioApp.getString(2131887598));
        changeTransform.addTarget(portfolioApp.getString(2131887599));
        changeTransform.addTarget(portfolioApp.getString(2131887597));
        changeTransform.addTarget(portfolioApp.getString(2131887601));
        transitionSet.addTransition(changeTransform);
        Fade fade = new Fade();
        fade.addTarget(portfolioApp.getString(2131887601));
        fade.addTarget(portfolioApp.getString(2131887596));
        transitionSet.addTransition(fade);
        Ks5 ks5 = new Ks5();
        ks5.addTarget(portfolioApp.getString(2131887593));
        ks5.addTarget(portfolioApp.getString(2131887593));
        ks5.addTarget(portfolioApp.getString(2131887590));
        ks5.addTarget(portfolioApp.getString(2131887592));
        ks5.addTarget(portfolioApp.getString(2131887591));
        ks5.addTarget(portfolioApp.getString(2131887604));
        ks5.addTarget(portfolioApp.getString(2131887603));
        ks5.addTarget(portfolioApp.getString(2131887602));
        transitionSet.addTransition(ks5);
        Js5 js5 = new Js5();
        js5.addTarget(portfolioApp.getString(2131887600));
        transitionSet.addTransition(js5);
        return transitionSet;
    }

    @DexIgnore
    public final void d(View view) {
        Wg6.c(view, "view");
        view.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) view.getHeight(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        view.startAnimation(translateAnimation);
    }
}
