package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ap1 {
    DIAGNOSTICS("diagnosticsApp"),
    WELLNESS("wellnessApp"),
    WORKOUT("workoutApp"),
    MUSIC("musicApp"),
    NOTIFICATIONS_PANEL("notificationsPanelApp"),
    EMPTY("empty"),
    STOP_WATCH("stopwatchApp"),
    ASSISTANT("assistantApp"),
    TIMER("timerApp"),
    WEATHER("weatherApp"),
    COMMUTE("commuteApp"),
    BUDDY_CHALLENGE("buddyChallengeApp");
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Ap1 a(Object obj) {
            Ap1 ap1;
            Ap1[] values = Ap1.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ap1 = null;
                    break;
                }
                ap1 = values[i];
                if (Wg6.a(ap1.a(), obj)) {
                    break;
                }
                i++;
            }
            return ap1 != null ? ap1 : Ap1.EMPTY;
        }
    }

    @DexIgnore
    public Ap1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj = Vb.a[ordinal()] != 1 ? this.b : JSONObject.NULL;
        Wg6.b(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
