package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ScrollView q;
    @DexIgnore
    public /* final */ RecyclerView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;

    @DexIgnore
    public Zg5(Object obj, View view, int i, ScrollView scrollView, RecyclerView recyclerView, FlexibleTextView flexibleTextView, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = scrollView;
        this.r = recyclerView;
        this.s = flexibleTextView;
        this.t = constraintLayout;
    }

    @DexIgnore
    @Deprecated
    public static Zg5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Zg5) ViewDataBinding.p(layoutInflater, 2131558862, viewGroup, z, obj);
    }

    @DexIgnore
    public static Zg5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
