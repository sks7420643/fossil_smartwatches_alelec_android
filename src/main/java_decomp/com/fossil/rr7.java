package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rr7 {
    @DexIgnore
    public static /* final */ Rr7 a; // = Mo7.a.b();
    @DexIgnore
    public static /* final */ Bi b; // = new Bi(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Rr7 {
        @DexIgnore
        public static /* final */ Ai c; // = new Ai();

        @DexIgnore
        @Override // com.fossil.Rr7
        public int b(int i) {
            return Rr7.b.b(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Rr7 {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.Rr7
        public int b(int i) {
            return Rr7.a.b(i);
        }

        @DexIgnore
        @Override // com.fossil.Rr7
        public int c() {
            return Rr7.a.c();
        }
    }

    /*
    static {
        Ai ai = Ai.c;
    }
    */

    @DexIgnore
    public abstract int b(int i);

    @DexIgnore
    public int c() {
        return b(32);
    }
}
