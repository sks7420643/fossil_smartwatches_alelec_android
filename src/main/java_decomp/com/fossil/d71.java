package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface D71 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static void a(D71 d71, Bitmap bitmap) {
            Object tag = d71.b().getView().getTag(F51.coil_bitmap);
            if (!(tag instanceof Bitmap)) {
                tag = null;
            }
            Bitmap bitmap2 = (Bitmap) tag;
            if (bitmap2 != null) {
                d71.a().a(bitmap2);
            }
            d71.b().getView().setTag(F51.coil_bitmap, bitmap);
        }

        @DexIgnore
        public static void b(D71 d71, Bitmap bitmap) {
            if (bitmap != null) {
                d71.a().b(bitmap);
            }
        }
    }

    @DexIgnore
    S61 a();

    @DexIgnore
    I81<?> b();

    @DexIgnore
    void c(Bitmap bitmap);

    @DexIgnore
    void d(Bitmap bitmap);
}
