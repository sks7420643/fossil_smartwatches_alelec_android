package com.fossil;

import android.renderscript.BaseObj;
import androidx.renderscript.RenderScript;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uv0 {
    @DexIgnore
    public long a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public RenderScript c;

    @DexIgnore
    public Uv0(long j, RenderScript renderScript) {
        renderScript.I();
        this.c = renderScript;
        this.a = j;
    }

    @DexIgnore
    public void a() {
        if (this.a == 0 && d() == null) {
            throw new Yv0("Invalid object.");
        }
    }

    @DexIgnore
    public void b() {
        if (!this.b) {
            e();
            return;
        }
        throw new Zv0("Object already destroyed.");
    }

    @DexIgnore
    public long c(RenderScript renderScript) {
        this.c.I();
        if (this.b) {
            throw new Zv0("using a destroyed object.");
        } else if (this.a == 0) {
            throw new Aw0("Internal error: Object id 0.");
        } else if (renderScript == null || renderScript == this.c) {
            return this.a;
        } else {
            throw new Zv0("using object with mismatched context.");
        }
    }

    @DexIgnore
    public BaseObj d() {
        return null;
    }

    @DexIgnore
    public final void e() {
        boolean z = true;
        synchronized (this) {
            if (!this.b) {
                this.b = true;
            } else {
                z = false;
            }
        }
        if (z) {
            ReentrantReadWriteLock.ReadLock readLock = this.c.k.readLock();
            readLock.lock();
            if (this.c.h()) {
                this.c.A(this.a);
            }
            readLock.unlock();
            this.c = null;
            this.a = 0;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.a == ((Uv0) obj).a;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        e();
        super.finalize();
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return (int) ((j & 268435455) ^ (j >> 32));
    }
}
