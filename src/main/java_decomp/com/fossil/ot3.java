package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ot3<TResult> {
    @DexIgnore
    public /* final */ Lu3<TResult> a; // = new Lu3<>();

    @DexIgnore
    public Ot3() {
    }

    @DexIgnore
    public Ot3(Dt3 dt3) {
        dt3.b(new Ju3(this));
    }

    @DexIgnore
    public Nt3<TResult> a() {
        return this.a;
    }

    @DexIgnore
    public void b(Exception exc) {
        this.a.t(exc);
    }

    @DexIgnore
    public void c(TResult tresult) {
        this.a.u(tresult);
    }

    @DexIgnore
    public boolean d(Exception exc) {
        return this.a.x(exc);
    }

    @DexIgnore
    public boolean e(TResult tresult) {
        return this.a.y(tresult);
    }
}
