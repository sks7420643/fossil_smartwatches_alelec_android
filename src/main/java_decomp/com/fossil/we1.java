package com.fossil;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class We1 implements Ue1 {
    @DexIgnore
    public /* final */ Map<String, List<Ve1>> b;
    @DexIgnore
    public volatile Map<String, String> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ String b; // = b();
        @DexIgnore
        public static /* final */ Map<String, List<Ve1>> c;
        @DexIgnore
        public Map<String, List<Ve1>> a; // = c;

        /*
        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(b)) {
                hashMap.put("User-Agent", Collections.singletonList(new Bi(b)));
            }
            c = Collections.unmodifiableMap(hashMap);
        }
        */

        @DexIgnore
        public static String b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == '\t') && charAt < '\u007f') {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        @DexIgnore
        public We1 a() {
            return new We1(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ve1 {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Bi(String str) {
            this.a = str;
        }

        @DexIgnore
        @Override // com.fossil.Ve1
        public String a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof Bi) {
                return this.a.equals(((Bi) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "StringHeaderFactory{value='" + this.a + "'}";
        }
    }

    @DexIgnore
    public We1(Map<String, List<Ve1>> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    @Override // com.fossil.Ue1
    public Map<String, String> a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = Collections.unmodifiableMap(c());
                }
            }
        }
        return this.c;
    }

    @DexIgnore
    public final String b(List<Ve1> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String a2 = list.get(i).a();
            if (!TextUtils.isEmpty(a2)) {
                sb.append(a2);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public final Map<String, String> c() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<String, List<Ve1>> entry : this.b.entrySet()) {
            String b2 = b(entry.getValue());
            if (!TextUtils.isEmpty(b2)) {
                hashMap.put(entry.getKey(), b2);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof We1) {
            return this.b.equals(((We1) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "LazyHeaders{headers=" + this.b + '}';
    }
}
