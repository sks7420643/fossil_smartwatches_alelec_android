package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q21 extends S21<Boolean> {
    @DexIgnore
    public static /* final */ String i; // = X01.f("BatteryChrgTracker");

    @DexIgnore
    public Q21(Context context, K41 k41) {
        super(context, k41);
    }

    @DexIgnore
    @Override // com.fossil.T21
    public /* bridge */ /* synthetic */ Object b() {
        return i();
    }

    @DexIgnore
    @Override // com.fossil.S21
    public IntentFilter g() {
        IntentFilter intentFilter = new IntentFilter();
        if (Build.VERSION.SDK_INT >= 23) {
            intentFilter.addAction("android.os.action.CHARGING");
            intentFilter.addAction("android.os.action.DISCHARGING");
        } else {
            intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
            intentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        }
        return intentFilter;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        if (r4.equals("android.os.action.CHARGING") != false) goto L_0x0028;
     */
    @DexIgnore
    @Override // com.fossil.S21
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h(android.content.Context r10, android.content.Intent r11) {
        /*
            r9 = this;
            r3 = 3
            r1 = 2
            r2 = 1
            r0 = 0
            java.lang.String r4 = r11.getAction()
            if (r4 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            com.fossil.X01 r5 = com.fossil.X01.c()
            java.lang.String r6 = com.fossil.Q21.i
            java.lang.String r7 = "Received %s"
            java.lang.Object[] r8 = new java.lang.Object[r2]
            r8[r0] = r4
            java.lang.String r7 = java.lang.String.format(r7, r8)
            java.lang.Throwable[] r8 = new java.lang.Throwable[r0]
            r5.a(r6, r7, r8)
            int r5 = r4.hashCode()
            switch(r5) {
                case -1886648615: goto L_0x0053;
                case -54942926: goto L_0x0049;
                case 948344062: goto L_0x0040;
                case 1019184907: goto L_0x0036;
                default: goto L_0x0027;
            }
        L_0x0027:
            r0 = -1
        L_0x0028:
            if (r0 == 0) goto L_0x0069
            if (r0 == r2) goto L_0x0063
            if (r0 == r1) goto L_0x005d
            if (r0 != r3) goto L_0x000a
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r9.d(r0)
            goto L_0x000a
        L_0x0036:
            java.lang.String r0 = "android.intent.action.ACTION_POWER_CONNECTED"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0027
            r0 = r1
            goto L_0x0028
        L_0x0040:
            java.lang.String r5 = "android.os.action.CHARGING"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0027
            goto L_0x0028
        L_0x0049:
            java.lang.String r0 = "android.os.action.DISCHARGING"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0027
            r0 = r2
            goto L_0x0028
        L_0x0053:
            java.lang.String r0 = "android.intent.action.ACTION_POWER_DISCONNECTED"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0027
            r0 = r3
            goto L_0x0028
        L_0x005d:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r9.d(r0)
            goto L_0x000a
        L_0x0063:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r9.d(r0)
            goto L_0x000a
        L_0x0069:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r9.d(r0)
            goto L_0x000a
            switch-data {-1886648615->0x0053, -54942926->0x0049, 948344062->0x0040, 1019184907->0x0036, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q21.h(android.content.Context, android.content.Intent):void");
    }

    @DexIgnore
    public Boolean i() {
        Intent registerReceiver = this.b.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            return Boolean.valueOf(j(registerReceiver));
        }
        X01.c().b(i, "getInitialState - null intent received", new Throwable[0]);
        return null;
    }

    @DexIgnore
    public final boolean j(Intent intent) {
        if (Build.VERSION.SDK_INT >= 23) {
            int intExtra = intent.getIntExtra("status", -1);
            if (intExtra == 2 || intExtra == 5) {
                return true;
            }
        } else if (intent.getIntExtra("plugged", 0) != 0) {
            return true;
        }
        return false;
    }
}
