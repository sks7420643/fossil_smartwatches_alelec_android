package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z44<E> extends Y24<E> {
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public Z44(E e) {
        I14.l(e);
        this.element = e;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        I14.j(i, 1);
        return this.element;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.U24, com.fossil.U24, com.fossil.Y24, com.fossil.Y24, java.lang.Iterable
    public H54<E> iterator() {
        return P34.u(this.element);
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.Y24, com.fossil.Y24
    public Y24<E> subList(int i, int i2) {
        I14.r(i, i2, 1);
        return i == i2 ? Y24.of() : this;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }
}
