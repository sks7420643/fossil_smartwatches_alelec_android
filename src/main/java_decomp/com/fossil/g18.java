package com.fossil;

import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G18 {
    @DexIgnore
    public static /* final */ D18[] e; // = {D18.q, D18.r, D18.s, D18.t, D18.u, D18.k, D18.m, D18.l, D18.n, D18.p, D18.o};
    @DexIgnore
    public static /* final */ D18[] f; // = {D18.q, D18.r, D18.s, D18.t, D18.u, D18.k, D18.m, D18.l, D18.n, D18.p, D18.o, D18.i, D18.j, D18.g, D18.h, D18.e, D18.f, D18.d};
    @DexIgnore
    public static /* final */ G18 g;
    @DexIgnore
    public static /* final */ G18 h; // = new Ai(false).a();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String[] c;
    @DexIgnore
    public /* final */ String[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public String[] b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public Ai(G18 g18) {
            this.a = g18.a;
            this.b = g18.c;
            this.c = g18.d;
            this.d = g18.b;
        }

        @DexIgnore
        public Ai(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public G18 a() {
            return new G18(this);
        }

        @DexIgnore
        public Ai b(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length != 0) {
                this.b = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one cipher suite is required");
            }
        }

        @DexIgnore
        public Ai c(D18... d18Arr) {
            if (this.a) {
                String[] strArr = new String[d18Arr.length];
                for (int i = 0; i < d18Arr.length; i++) {
                    strArr[i] = d18Arr[i].a;
                }
                b(strArr);
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public Ai d(boolean z) {
            if (this.a) {
                this.d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        @DexIgnore
        public Ai e(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length != 0) {
                this.c = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one TLS version is required");
            }
        }

        @DexIgnore
        public Ai f(Y18... y18Arr) {
            if (this.a) {
                String[] strArr = new String[y18Arr.length];
                for (int i = 0; i < y18Arr.length; i++) {
                    strArr[i] = y18Arr[i].javaName;
                }
                e(strArr);
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }
    }

    /*
    static {
        Ai ai = new Ai(true);
        ai.c(e);
        ai.f(Y18.TLS_1_3, Y18.TLS_1_2);
        ai.d(true);
        ai.a();
        Ai ai2 = new Ai(true);
        ai2.c(f);
        ai2.f(Y18.TLS_1_3, Y18.TLS_1_2, Y18.TLS_1_1, Y18.TLS_1_0);
        ai2.d(true);
        g = ai2.a();
        Ai ai3 = new Ai(true);
        ai3.c(f);
        ai3.f(Y18.TLS_1_0);
        ai3.d(true);
        ai3.a();
    }
    */

    @DexIgnore
    public G18(Ai ai) {
        this.a = ai.a;
        this.c = ai.b;
        this.d = ai.c;
        this.b = ai.d;
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, boolean z) {
        G18 e2 = e(sSLSocket, z);
        String[] strArr = e2.d;
        if (strArr != null) {
            sSLSocket.setEnabledProtocols(strArr);
        }
        String[] strArr2 = e2.c;
        if (strArr2 != null) {
            sSLSocket.setEnabledCipherSuites(strArr2);
        }
    }

    @DexIgnore
    public List<D18> b() {
        String[] strArr = this.c;
        if (strArr != null) {
            return D18.b(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean c(SSLSocket sSLSocket) {
        if (!this.a) {
            return false;
        }
        String[] strArr = this.d;
        if (strArr != null && !B28.B(B28.p, strArr, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        String[] strArr2 = this.c;
        return strArr2 == null || B28.B(D18.b, strArr2, sSLSocket.getEnabledCipherSuites());
    }

    @DexIgnore
    public boolean d() {
        return this.a;
    }

    @DexIgnore
    public final G18 e(SSLSocket sSLSocket, boolean z) {
        String[] z2 = this.c != null ? B28.z(D18.b, sSLSocket.getEnabledCipherSuites(), this.c) : sSLSocket.getEnabledCipherSuites();
        String[] z3 = this.d != null ? B28.z(B28.p, sSLSocket.getEnabledProtocols(), this.d) : sSLSocket.getEnabledProtocols();
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int w = B28.w(D18.b, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && w != -1) {
            z2 = B28.i(z2, supportedCipherSuites[w]);
        }
        Ai ai = new Ai(this);
        ai.b(z2);
        ai.e(z3);
        return ai.a();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof G18)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        G18 g18 = (G18) obj;
        boolean z = this.a;
        if (z == g18.a) {
            return !z || (Arrays.equals(this.c, g18.c) && Arrays.equals(this.d, g18.d) && this.b == g18.b);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.b;
    }

    @DexIgnore
    public List<Y18> g() {
        String[] strArr = this.d;
        if (strArr != null) {
            return Y18.forJavaNames(strArr);
        }
        return null;
    }

    @DexIgnore
    public int hashCode() {
        if (this.a) {
            return ((((Arrays.hashCode(this.c) + 527) * 31) + Arrays.hashCode(this.d)) * 31) + (!this.b ? 1 : 0);
        }
        return 17;
    }

    @DexIgnore
    public String toString() {
        if (!this.a) {
            return "ConnectionSpec()";
        }
        String str = "[all enabled]";
        String obj = this.c != null ? b().toString() : "[all enabled]";
        if (this.d != null) {
            str = g().toString();
        }
        return "ConnectionSpec(cipherSuites=" + obj + ", tlsVersions=" + str + ", supportsTlsExtensions=" + this.b + ")";
    }
}
