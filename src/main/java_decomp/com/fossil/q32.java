package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q32 implements Factory<P32> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<String> b;
    @DexIgnore
    public /* final */ Provider<Integer> c;

    @DexIgnore
    public Q32(Provider<Context> provider, Provider<String> provider2, Provider<Integer> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Q32 a(Provider<Context> provider, Provider<String> provider2, Provider<Integer> provider3) {
        return new Q32(provider, provider2, provider3);
    }

    @DexIgnore
    public P32 b() {
        return new P32(this.a.get(), this.b.get(), this.c.get().intValue());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
