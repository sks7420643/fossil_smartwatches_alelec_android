package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class At6 implements Factory<CustomizeRingChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public At6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static At6 a(Provider<ThemeRepository> provider) {
        return new At6(provider);
    }

    @DexIgnore
    public static CustomizeRingChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeRingChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeRingChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
