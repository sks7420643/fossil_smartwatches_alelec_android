package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V8 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ U8 CREATOR; // = new U8(null);
    @DexIgnore
    public /* final */ T8 b;
    @DexIgnore
    public /* final */ short c;

    @DexIgnore
    public V8(T8 t8, short s) {
        this.b = t8;
        this.c = (short) s;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(V8.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            V8 v8 = (V8) obj;
            if (this.b != v8.b) {
                return false;
            }
            return this.c == v8.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame");
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.v0, Ey1.a(this.b)), Jd0.A0, Hy1.l(this.c, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
