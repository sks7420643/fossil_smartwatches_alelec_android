package com.fossil;

import com.fossil.La8;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oa8 {
    @DexIgnore
    public static /* final */ Oa8 a; // = new Oa8();

    @DexIgnore
    public final La8 a(Map<?, ?> map) {
        Wg6.c(map, Constants.MAP);
        La8 la8 = new La8();
        Object obj = map.get("title");
        if (obj != null) {
            la8.d(((Boolean) obj).booleanValue());
            La8.Bi bi = new La8.Bi();
            la8.e(bi);
            Object obj2 = map.get("size");
            if (obj2 != null) {
                Map map2 = (Map) obj2;
                Object obj3 = map2.get("minWidth");
                if (obj3 != null) {
                    bi.h(((Integer) obj3).intValue());
                    Object obj4 = map2.get("maxWidth");
                    if (obj4 != null) {
                        bi.f(((Integer) obj4).intValue());
                        Object obj5 = map2.get("minHeight");
                        if (obj5 != null) {
                            bi.g(((Integer) obj5).intValue());
                            Object obj6 = map2.get("maxHeight");
                            if (obj6 != null) {
                                bi.e(((Integer) obj6).intValue());
                                La8.Ai ai = new La8.Ai();
                                la8.c(ai);
                                Object obj7 = map.get("duration");
                                if (obj7 != null) {
                                    Map map3 = (Map) obj7;
                                    Object obj8 = map3.get("min");
                                    if (obj8 != null) {
                                        ai.d((long) ((Integer) obj8).intValue());
                                        Object obj9 = map3.get("max");
                                        if (obj9 != null) {
                                            ai.c((long) ((Integer) obj9).intValue());
                                            return la8;
                                        }
                                        throw new Rc6("null cannot be cast to non-null type kotlin.Int");
                                    }
                                    throw new Rc6("null cannot be cast to non-null type kotlin.Int");
                                }
                                throw new Rc6("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                            }
                            throw new Rc6("null cannot be cast to non-null type kotlin.Int");
                        }
                        throw new Rc6("null cannot be cast to non-null type kotlin.Int");
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlin.Int");
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Int");
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Boolean");
    }

    @DexIgnore
    public final Map<String, Object> b(List<Ka8> list) {
        Wg6.c(list, "list");
        ArrayList arrayList = new ArrayList();
        for (Ka8 ka8 : list) {
            long j = (long) 1000;
            arrayList.add(Zm7.j(Hl7.a("id", ka8.e()), Hl7.a("duration", Long.valueOf(ka8.c() / j)), Hl7.a("type", Integer.valueOf(ka8.j())), Hl7.a("createDt", Long.valueOf(ka8.a() / j)), Hl7.a("width", Integer.valueOf(ka8.k())), Hl7.a("height", Integer.valueOf(ka8.d())), Hl7.a("modifiedDt", Long.valueOf(ka8.h())), Hl7.a(Constants.LAT, ka8.f()), Hl7.a("lng", ka8.g()), Hl7.a("title", ka8.b())));
        }
        return Ym7.c(Hl7.a("data", arrayList));
    }

    @DexIgnore
    public final Map<String, Object> c(Ka8 ka8) {
        Wg6.c(ka8, "entity");
        return Ym7.c(Hl7.a("data", Zm7.j(Hl7.a("id", ka8.e()), Hl7.a("duration", Long.valueOf(ka8.c())), Hl7.a("type", Integer.valueOf(ka8.j())), Hl7.a("createDt", Long.valueOf(ka8.a() / ((long) 1000))), Hl7.a("width", Integer.valueOf(ka8.k())), Hl7.a("height", Integer.valueOf(ka8.d())), Hl7.a("modifiedDt", Long.valueOf(ka8.h())), Hl7.a(Constants.LAT, ka8.f()), Hl7.a("lng", ka8.g()), Hl7.a("title", ka8.b()))));
    }

    @DexIgnore
    public final Map<String, Object> d(List<Ma8> list) {
        Wg6.c(list, "list");
        ArrayList arrayList = new ArrayList();
        for (Ma8 ma8 : list) {
            Map j = Zm7.j(Hl7.a("id", ma8.a()), Hl7.a("name", ma8.c()), Hl7.a("length", Integer.valueOf(ma8.b())), Hl7.a("isAll", Boolean.valueOf(ma8.d())));
            if (ma8.b() > 0) {
                arrayList.add(j);
            }
        }
        return Ym7.c(Hl7.a("data", arrayList));
    }
}
