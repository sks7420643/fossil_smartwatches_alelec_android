package com.fossil;

import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ActiveMinutesComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.BatteryComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.CaloriesComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ChanceOfRainComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.DateComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.HeartRateComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.NoneComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.SecondTimezoneComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.StepsComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.WeatherComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLEGoalTrackingCustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLENonCustomization;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.BuddyChallengeMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.CommuteTimeWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.DiagnosticsWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.MusicWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.NoneWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.NotificationPanelWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.StopWatchWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.TimerWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WeatherWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WellnessWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WorkoutWatchAppMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Gesture;
import com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj5 {
    @DexIgnore
    public static final ArrayList<oo5> a(List<oo5> list) {
        pq7.c(list, "$this$clone");
        ArrayList<oo5> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList(im7.m(list, 10));
        for (T t : list) {
            arrayList2.add(new oo5(t.a(), t.b(), t.c()));
        }
        arrayList.addAll(arrayList2);
        return arrayList;
    }

    @DexIgnore
    public static final ArrayList<HybridPresetAppSetting> b(List<HybridPresetAppSetting> list) {
        pq7.c(list, "$this$deepCopyAppSetting");
        ArrayList<HybridPresetAppSetting> arrayList = new ArrayList<>();
        for (T t : list) {
            arrayList.add(new HybridPresetAppSetting(t.getPosition(), t.getAppId(), t.getLocalUpdateAt(), t.getSettings()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final ArrayList<DianaPresetComplicationSetting> c(List<DianaPresetComplicationSetting> list) {
        pq7.c(list, "$this$deepCopyPresetComplicationSetting");
        ArrayList<DianaPresetComplicationSetting> arrayList = new ArrayList<>();
        for (DianaPresetComplicationSetting dianaPresetComplicationSetting : list) {
            arrayList.add(new DianaPresetComplicationSetting(dianaPresetComplicationSetting.component1(), dianaPresetComplicationSetting.component2(), dianaPresetComplicationSetting.component3(), dianaPresetComplicationSetting.component4()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final ArrayList<DianaPresetWatchAppSetting> d(List<DianaPresetWatchAppSetting> list) {
        pq7.c(list, "$this$deepCopyPresetWatchAppSetting");
        ArrayList<DianaPresetWatchAppSetting> arrayList = new ArrayList<>();
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            arrayList.add(new DianaPresetWatchAppSetting(dianaPresetWatchAppSetting.component1(), dianaPresetWatchAppSetting.component2(), dianaPresetWatchAppSetting.component3(), dianaPresetWatchAppSetting.component4()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final Gesture e(HybridPresetAppSetting hybridPresetAppSetting) {
        pq7.c(hybridPresetAppSetting, "$this$getBleGesture");
        String position = hybridPresetAppSetting.getPosition();
        return pq7.a(position, PusherConfiguration.Pusher.TOP_PUSHER.getValue()) ? Gesture.SAM_BT1_SINGLE_PRESS : pq7.a(position, PusherConfiguration.Pusher.MID_PUSHER.getValue()) ? Gesture.SAM_BT2_SINGLE_PRESS : Gesture.SAM_BT3_SINGLE_PRESS;
    }

    @DexIgnore
    public static final MicroAppVariant f(HybridPresetAppSetting hybridPresetAppSetting, String str, int i, MicroAppRepository microAppRepository, String str2) {
        String str3;
        CommuteTimeSetting commuteTimeSetting;
        pq7.c(hybridPresetAppSetting, "$this$getVariant");
        pq7.c(str, "serialNumber");
        pq7.c(microAppRepository, "mMicroAppRepository");
        MicroAppInstruction.MicroAppID microAppId = MicroAppInstruction.MicroAppID.Companion.getMicroAppId(hybridPresetAppSetting.getAppId());
        if (microAppId != MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME || str2 == null || TextUtils.isEmpty(str2) || (commuteTimeSetting = (CommuteTimeSetting) new Gson().k(str2, CommuteTimeSetting.class)) == null) {
            str3 = "";
        } else {
            str3 = commuteTimeSetting.getFormat();
            if (TextUtils.isEmpty(str3)) {
                str3 = "travel";
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getVariant", "variantName " + str3 + " microAppId " + hybridPresetAppSetting.getAppId() + ' ' + str + " major " + i + " bleAppEnum " + microAppId + " setting " + str2);
        if (str3.length() == 0) {
            return microAppRepository.getMicroAppVariant(str, hybridPresetAppSetting.getAppId(), i);
        }
        String appId = hybridPresetAppSetting.getAppId();
        if (str3 != null) {
            String lowerCase = str3.toLowerCase();
            pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
            return microAppRepository.getMicroAppVariant(str, appId, lowerCase, i);
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final boolean g(HybridPresetAppSetting hybridPresetAppSetting, Gson gson, HybridPresetAppSetting hybridPresetAppSetting2) {
        pq7.c(hybridPresetAppSetting, "$this$isHybridAppSettingEquals");
        pq7.c(gson, "gson");
        pq7.c(hybridPresetAppSetting2, FacebookRequestErrorClassification.KEY_OTHER);
        if (!pq7.a(hybridPresetAppSetting.getPosition(), hybridPresetAppSetting2.getPosition())) {
            return false;
        }
        if (!pq7.a(hybridPresetAppSetting.getAppId(), hybridPresetAppSetting2.getAppId())) {
            return false;
        }
        if (TextUtils.isEmpty(hybridPresetAppSetting.getSettings()) && TextUtils.isEmpty(hybridPresetAppSetting2.getSettings())) {
            return true;
        }
        String appId = hybridPresetAppSetting.getAppId();
        if (pq7.a(appId, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            return fj5.a(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        if (pq7.a(appId, MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            return fj5.c(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        if (pq7.a(appId, MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return fj5.b(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        return true;
    }

    @DexIgnore
    public static final boolean h(List<HybridPresetAppSetting> list, Gson gson, List<HybridPresetAppSetting> list2) {
        T t;
        pq7.c(list, "$this$isHybridAppSettingListEquals");
        pq7.c(gson, "gson");
        pq7.c(list2, FacebookRequestErrorClassification.KEY_OTHER);
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (HybridPresetAppSetting hybridPresetAppSetting : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(hybridPresetAppSetting.getPosition(), next.getPosition())) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            if (t2 != null && !g(hybridPresetAppSetting, gson, t2)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final MicroAppMapping i(HybridPresetAppSetting hybridPresetAppSetting, long j, MicroAppVariant microAppVariant, String str) {
        pq7.c(hybridPresetAppSetting, "$this$toBleMicroAppMapping");
        Gesture e = e(hybridPresetAppSetting);
        MicroAppInstruction.MicroAppID microAppId = MicroAppInstruction.MicroAppID.Companion.getMicroAppId(hybridPresetAppSetting.getAppId());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("bleGesture ");
        sb.append(e);
        sb.append(" bleAppEnum ");
        sb.append(microAppId);
        sb.append(' ');
        sb.append("setting ");
        sb.append(str);
        sb.append(" variant ");
        sb.append(microAppVariant);
        sb.append(" declarationsFile ");
        sb.append(microAppVariant != null ? microAppVariant.getDeclarationFileList() : null);
        local.d("toBleMicroAppMapping", sb.toString());
        if (microAppVariant == null || !(!microAppVariant.getDeclarationFileList().isEmpty())) {
            return null;
        }
        String[] strArr = new String[microAppVariant.getDeclarationFileList().size()];
        int size = microAppVariant.getDeclarationFileList().size();
        for (int i = 0; i < size; i++) {
            strArr[i] = microAppVariant.getDeclarationFileList().get(i).getContent();
        }
        BLECustomization bLENonCustomization = new BLENonCustomization();
        if (microAppId == MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID) {
            bLENonCustomization = new BLEGoalTrackingCustomization(0);
        }
        return new MicroAppMapping(e, microAppId.getValue(), strArr, bLENonCustomization, Long.valueOf(j));
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static final ComplicationAppMappingSettings j(List<DianaPresetComplicationSetting> list, Gson gson) {
        ComplicationAppMapping complicationAppMapping;
        ComplicationAppMapping complicationAppMapping2;
        ComplicationAppMapping complicationAppMapping3;
        ComplicationAppMapping complicationAppMapping4;
        ComplicationAppMapping batteryComplicationAppMapping;
        SecondTimezoneSetting secondTimezoneSetting;
        pq7.c(list, "$this$toComplicationSetting");
        pq7.c(gson, "mGson");
        FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Convert DianaPresetSetting to complication mapping");
        ComplicationAppMapping complicationAppMapping5 = null;
        ComplicationAppMapping complicationAppMapping6 = null;
        ComplicationAppMapping complicationAppMapping7 = null;
        ComplicationAppMapping complicationAppMapping8 = null;
        for (DianaPresetComplicationSetting dianaPresetComplicationSetting : list) {
            String component1 = dianaPresetComplicationSetting.component1();
            String component2 = dianaPresetComplicationSetting.component2();
            String component4 = dianaPresetComplicationSetting.component4();
            switch (component2.hashCode()) {
                case -331239923:
                    if (component2.equals(Constants.BATTERY)) {
                        batteryComplicationAppMapping = new BatteryComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case -168965370:
                    if (component2.equals("calories")) {
                        batteryComplicationAppMapping = new CaloriesComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case -85386984:
                    if (component2.equals("active-minutes")) {
                        batteryComplicationAppMapping = new ActiveMinutesComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case -48173007:
                    if (component2.equals("chance-of-rain")) {
                        batteryComplicationAppMapping = new ChanceOfRainComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 3076014:
                    if (component2.equals("date")) {
                        batteryComplicationAppMapping = new DateComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 96634189:
                    if (component2.equals("empty")) {
                        batteryComplicationAppMapping = new NoneComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 109761319:
                    if (component2.equals("steps")) {
                        batteryComplicationAppMapping = new StepsComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 134170930:
                    if (component2.equals("second-timezone")) {
                        if (component4 != null) {
                            secondTimezoneSetting = (SecondTimezoneSetting) gson.k(component4, SecondTimezoneSetting.class);
                            if (secondTimezoneSetting == null) {
                                secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
                            }
                        } else {
                            secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
                        }
                        secondTimezoneSetting.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId()));
                        batteryComplicationAppMapping = new SecondTimezoneComplicationAppMapping(secondTimezoneSetting.getCityCode(), secondTimezoneSetting.getTimezoneOffset());
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 1223440372:
                    if (component2.equals("weather")) {
                        batteryComplicationAppMapping = new WeatherComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 1884273159:
                    if (component2.equals("heart-rate")) {
                        batteryComplicationAppMapping = new HeartRateComplicationAppMapping();
                        break;
                    }
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
                default:
                    batteryComplicationAppMapping = new NoneComplicationAppMapping();
                    break;
            }
            switch (component1.hashCode()) {
                case -1383228885:
                    if (component1.equals("bottom")) {
                        complicationAppMapping5 = batteryComplicationAppMapping;
                        break;
                    } else {
                        break;
                    }
                case 115029:
                    if (component1.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        complicationAppMapping8 = batteryComplicationAppMapping;
                        break;
                    } else {
                        break;
                    }
                case 3317767:
                    if (component1.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        complicationAppMapping6 = batteryComplicationAppMapping;
                        break;
                    } else {
                        break;
                    }
                case 108511772:
                    if (component1.equals("right")) {
                        complicationAppMapping7 = batteryComplicationAppMapping;
                        break;
                    } else {
                        break;
                    }
            }
        }
        if (complicationAppMapping8 == null) {
            complicationAppMapping = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Top complication empty");
        } else {
            complicationAppMapping = complicationAppMapping8;
        }
        if (complicationAppMapping7 == null) {
            complicationAppMapping2 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Right complication empty");
        } else {
            complicationAppMapping2 = complicationAppMapping7;
        }
        if (complicationAppMapping6 == null) {
            complicationAppMapping3 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Left complication empty");
        } else {
            complicationAppMapping3 = complicationAppMapping6;
        }
        if (complicationAppMapping5 == null) {
            complicationAppMapping4 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Bottom complication empty");
        } else {
            complicationAppMapping4 = complicationAppMapping5;
        }
        return new ComplicationAppMappingSettings(complicationAppMapping, complicationAppMapping4, complicationAppMapping3, complicationAppMapping2, System.currentTimeMillis());
    }

    @DexIgnore
    public static final List<MicroAppMapping> k(HybridPreset hybridPreset, String str, DeviceRepository deviceRepository, MicroAppRepository microAppRepository) {
        pq7.c(hybridPreset, "$this$toMicroAppMappingBLE");
        pq7.c(str, "serialNumber");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(microAppRepository, "mMicroAppRepository");
        Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
        if (deviceBySerial == null) {
            return new ArrayList();
        }
        if (deviceBySerial.getMajor() == 255 || deviceBySerial.getMinor() == 255) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("toMicroAppMappingBLE", "buttons " + hybridPreset.getButtons());
        long currentTimeMillis = System.currentTimeMillis();
        Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("toMicroAppMappingBLE", "setting for " + next + " is " + next.getSettings());
            pq7.b(next, "buttonMapping");
            MicroAppVariant f = f(next, str, deviceBySerial.getMajor(), microAppRepository, next.getSettings());
            if (f == null) {
                return new ArrayList();
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("toMicroAppMappingBLE", "variant for " + next + " is " + f);
            MicroAppMapping i = i(next, currentTimeMillis, f, next.getSettings());
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("toMicroAppMappingBLE", "bleMicroAppMapping for " + next + " is " + i);
            arrayList.add(i);
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static final WatchAppMappingSettings l(List<oo5> list, Gson gson, List<DianaAppSetting> list2) {
        WatchAppMapping watchAppMapping;
        WatchAppMapping watchAppMapping2;
        DianaAppSetting dianaAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        WatchAppMapping commuteTimeWatchAppMapping;
        pq7.c(list, "$this$toWatchAppMappingSettings");
        pq7.c(gson, "mGson");
        pq7.c(list2, "settingList");
        FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Convert DianaPresetSetting to watch app mapping");
        WatchAppMapping watchAppMapping3 = null;
        WatchAppMapping watchAppMapping4 = null;
        WatchAppMapping watchAppMapping5 = null;
        for (T t : list) {
            String a2 = t.a();
            switch (a2.hashCode()) {
                case -829740640:
                    if (a2.equals("commute-time")) {
                        ListIterator<DianaAppSetting> listIterator = list2.listIterator(list2.size());
                        while (true) {
                            if (listIterator.hasPrevious()) {
                                DianaAppSetting previous = listIterator.previous();
                                if (pq7.a(previous.getAppId(), "commute-time")) {
                                    dianaAppSetting = previous;
                                }
                            } else {
                                dianaAppSetting = null;
                            }
                        }
                        DianaAppSetting dianaAppSetting2 = dianaAppSetting;
                        if (dianaAppSetting2 != null) {
                            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) gson.k(dianaAppSetting2.getSetting(), CommuteTimeWatchAppSetting.class);
                            if (commuteTimeWatchAppSetting == null) {
                                commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
                            }
                        } else {
                            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
                        }
                        commuteTimeWatchAppMapping = new CommuteTimeWatchAppMapping(commuteTimeWatchAppSetting.getListAddressNameExceptOf(null));
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case -740386388:
                    if (a2.equals("diagnostics")) {
                        commuteTimeWatchAppMapping = new DiagnosticsWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case -420342747:
                    if (a2.equals("wellness")) {
                        commuteTimeWatchAppMapping = new WellnessWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 96634189:
                    if (a2.equals("empty")) {
                        commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 104263205:
                    if (a2.equals(Constants.MUSIC)) {
                        commuteTimeWatchAppMapping = new MusicWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 110364485:
                    if (a2.equals("timer")) {
                        commuteTimeWatchAppMapping = new TimerWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1223440372:
                    if (a2.equals("weather")) {
                        commuteTimeWatchAppMapping = new WeatherWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1374620322:
                    if (a2.equals("notification-panel")) {
                        commuteTimeWatchAppMapping = new NotificationPanelWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1525170845:
                    if (a2.equals("workout")) {
                        commuteTimeWatchAppMapping = new WorkoutWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1860261700:
                    if (a2.equals("stop-watch")) {
                        commuteTimeWatchAppMapping = new StopWatchWatchAppMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1904923164:
                    if (a2.equals("buddy-challenge")) {
                        commuteTimeWatchAppMapping = new BuddyChallengeMapping();
                        break;
                    }
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
                default:
                    commuteTimeWatchAppMapping = new NoneWatchAppMapping();
                    break;
            }
            String b = t.b();
            int hashCode = b.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && b.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        watchAppMapping5 = commuteTimeWatchAppMapping;
                    }
                } else if (b.equals("middle")) {
                    watchAppMapping4 = commuteTimeWatchAppMapping;
                }
            } else if (b.equals("bottom")) {
                watchAppMapping3 = commuteTimeWatchAppMapping;
            }
        }
        if (watchAppMapping5 == null) {
            watchAppMapping = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "top watch app empty");
        } else {
            watchAppMapping = watchAppMapping5;
        }
        if (watchAppMapping4 == null) {
            watchAppMapping2 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "middle watch app empty");
        } else {
            watchAppMapping2 = watchAppMapping4;
        }
        if (watchAppMapping3 == null) {
            watchAppMapping3 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "bottom watch app empty");
        }
        if (watchAppMapping == null) {
            pq7.i();
            throw null;
        } else if (watchAppMapping2 == null) {
            pq7.i();
            throw null;
        } else if (watchAppMapping3 != null) {
            return new WatchAppMappingSettings(watchAppMapping, watchAppMapping2, watchAppMapping3, System.currentTimeMillis());
        } else {
            pq7.i();
            throw null;
        }
    }
}
