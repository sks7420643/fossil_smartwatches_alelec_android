package com.fossil;

import com.mapped.N70;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fi extends Cq {
    @DexIgnore
    public /* final */ ArrayList<Ow> E; // = By1.a(this.C, Hm7.c(Ow.g));
    @DexIgnore
    public /* final */ N70 F;

    @DexIgnore
    public Fi(K5 k5, I60 i60, N70 n70) {
        super(k5, i60, Yp.B, new Ws(n70, k5));
        this.F = n70;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Cq
    public JSONObject C() {
        return G80.k(super.C(), Jd0.u0, this.F.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Cq
    public ArrayList<Ow> z() {
        return this.E;
    }
}
