package com.fossil;

import android.util.Log;
import com.fossil.Be1;
import com.fossil.Ya1;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fe1 implements Be1 {
    @DexIgnore
    public /* final */ Ke1 a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ De1 d; // = new De1();
    @DexIgnore
    public Ya1 e;

    @DexIgnore
    @Deprecated
    public Fe1(File file, long j) {
        this.b = file;
        this.c = j;
        this.a = new Ke1();
    }

    @DexIgnore
    public static Be1 c(File file, long j) {
        return new Fe1(file, j);
    }

    @DexIgnore
    @Override // com.fossil.Be1
    public void a(Mb1 mb1, Be1.Bi bi) {
        String b2 = this.a.b(mb1);
        this.d.a(b2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + b2 + " for for Key: " + mb1);
            }
            try {
                Ya1 d2 = d();
                if (d2.L(b2) == null) {
                    Ya1.Ci D = d2.D(b2);
                    if (D != null) {
                        try {
                            if (bi.a(D.f(0))) {
                                D.e();
                            }
                            this.d.b(b2);
                        } finally {
                            D.b();
                        }
                    } else {
                        throw new IllegalStateException("Had two simultaneous puts for: " + b2);
                    }
                }
            } catch (IOException e2) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e2);
                }
            }
        } finally {
            this.d.b(b2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Be1
    public File b(Mb1 mb1) {
        String b2 = this.a.b(mb1);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + b2 + " for for Key: " + mb1);
        }
        try {
            Ya1.Ei L = d().L(b2);
            if (L != null) {
                return L.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    @DexIgnore
    public final Ya1 d() throws IOException {
        Ya1 ya1;
        synchronized (this) {
            if (this.e == null) {
                this.e = Ya1.P(this.b, 1, 1, this.c);
            }
            ya1 = this.e;
        }
        return ya1;
    }
}
