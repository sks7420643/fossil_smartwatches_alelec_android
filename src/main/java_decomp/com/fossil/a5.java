package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A5 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;

    @DexIgnore
    public A5(K5 k5, G7 g7, int i) {
        this.b = k5;
        this.c = g7;
        this.d = i;
    }

    @DexIgnore
    public final void run() {
        this.b.z.h.f();
        this.b.z.h.c(new J7(this.c, this.d));
    }
}
