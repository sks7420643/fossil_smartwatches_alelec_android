package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Aj2 implements Parcelable.Creator<Wi2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Wi2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        IBinder iBinder = null;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        Zh2 zh2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                zh2 = (Zh2) Ad2.e(parcel, t, Zh2.CREATOR);
            } else if (l == 2) {
                arrayList2 = Ad2.j(parcel, t, DataSet.CREATOR);
            } else if (l == 3) {
                arrayList = Ad2.j(parcel, t, DataPoint.CREATOR);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                iBinder = Ad2.u(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Wi2(zh2, arrayList2, arrayList, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Wi2[] newArray(int i) {
        return new Wi2[i];
    }
}
