package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.O80;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uo1 extends O80 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Uo1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Uo1 a(Parcel parcel) {
            return new Uo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uo1 createFromParcel(Parcel parcel) {
            return new Uo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uo1[] newArray(int i) {
            return new Uo1[i];
        }
    }

    @DexIgnore
    public Uo1() {
        super(Ap1.MUSIC, null, null, 6);
    }

    @DexIgnore
    public /* synthetic */ Uo1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Uo1(Vw1 vw1) {
        super(Ap1.MUSIC, vw1, null, 4);
    }
}
