package com.fossil;

import android.animation.Animator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uy3 {
    @DexIgnore
    public Animator a;

    @DexIgnore
    public void a() {
        Animator animator = this.a;
        if (animator != null) {
            animator.cancel();
        }
    }

    @DexIgnore
    public void b() {
        this.a = null;
    }

    @DexIgnore
    public void c(Animator animator) {
        a();
        this.a = animator;
    }
}
