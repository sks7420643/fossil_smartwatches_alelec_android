package com.fossil;

import android.util.Log;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ih1 implements Rb1<Hh1> {
    @DexIgnore
    @Override // com.fossil.Jb1
    public /* bridge */ /* synthetic */ boolean a(Object obj, File file, Ob1 ob1) {
        return c((Id1) obj, file, ob1);
    }

    @DexIgnore
    @Override // com.fossil.Rb1
    public Ib1 b(Ob1 ob1) {
        return Ib1.SOURCE;
    }

    @DexIgnore
    public boolean c(Id1<Hh1> id1, File file, Ob1 ob1) {
        try {
            Zj1.e(id1.get().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
