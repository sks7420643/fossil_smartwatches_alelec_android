package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.db.DataLogService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oo5 {
    @DexIgnore
    @Vu3("appId")
    public String a;
    @DexIgnore
    @Vu3("buttonPosition")
    public String b;
    @DexIgnore
    @Vu3("localUpdatedAt")
    public String c;

    @DexIgnore
    public Oo5(String str, String str2, String str3) {
        Wg6.c(str, "appId");
        Wg6.c(str2, "position");
        Wg6.c(str3, DataLogService.COLUMN_UPDATE_AT);
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final void d(String str) {
        Wg6.c(str, "<set-?>");
        this.a = str;
    }

    @DexIgnore
    public final void e(String str) {
        Wg6.c(str, "<set-?>");
        this.b = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Oo5) {
                Oo5 oo5 = (Oo5) obj;
                if (!Wg6.a(this.a, oo5.a) || !Wg6.a(this.b, oo5.b) || !Wg6.a(this.c, oo5.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "PresetButton(appId=" + this.a + ", position=" + this.b + ", updateAt=" + this.c + ")";
    }
}
