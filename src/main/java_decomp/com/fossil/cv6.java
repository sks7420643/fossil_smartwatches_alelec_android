package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cv6 implements MembersInjector<LoginPresenter> {
    @DexIgnore
    public static void A(LoginPresenter loginPresenter, SleepSummariesRepository sleepSummariesRepository) {
        loginPresenter.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void B(LoginPresenter loginPresenter, SummariesRepository summariesRepository) {
        loginPresenter.C = summariesRepository;
    }

    @DexIgnore
    public static void C(LoginPresenter loginPresenter, Uq4 uq4) {
        loginPresenter.q = uq4;
    }

    @DexIgnore
    public static void D(LoginPresenter loginPresenter, UserRepository userRepository) {
        loginPresenter.l = userRepository;
    }

    @DexIgnore
    public static void E(LoginPresenter loginPresenter, WatchLocalizationRepository watchLocalizationRepository) {
        loginPresenter.I = watchLocalizationRepository;
    }

    @DexIgnore
    public static void F(LoginPresenter loginPresenter, WorkoutSettingRepository workoutSettingRepository) {
        loginPresenter.N = workoutSettingRepository;
    }

    @DexIgnore
    public static void G(LoginPresenter loginPresenter, Xn5 xn5) {
        loginPresenter.w = xn5;
    }

    @DexIgnore
    public static void H(LoginPresenter loginPresenter) {
        loginPresenter.c0();
    }

    @DexIgnore
    public static void I(LoginPresenter loginPresenter, ProfileRepository profileRepository) {
        loginPresenter.K = profileRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FCMRepository fCMRepository) {
        loginPresenter.L = fCMRepository;
    }

    @DexIgnore
    public static void b(LoginPresenter loginPresenter, FlagRepository flagRepository) {
        loginPresenter.M = flagRepository;
    }

    @DexIgnore
    public static void c(LoginPresenter loginPresenter, AlarmsRepository alarmsRepository) {
        loginPresenter.J = alarmsRepository;
    }

    @DexIgnore
    public static void d(LoginPresenter loginPresenter, AnalyticsHelper analyticsHelper) {
        loginPresenter.B = analyticsHelper;
    }

    @DexIgnore
    public static void e(LoginPresenter loginPresenter, CheckAuthenticationSocialExisting checkAuthenticationSocialExisting) {
        loginPresenter.A = checkAuthenticationSocialExisting;
    }

    @DexIgnore
    public static void f(LoginPresenter loginPresenter, DeviceRepository deviceRepository) {
        loginPresenter.m = deviceRepository;
    }

    @DexIgnore
    public static void g(LoginPresenter loginPresenter, Cj4 cj4) {
        loginPresenter.k = cj4;
    }

    @DexIgnore
    public static void h(LoginPresenter loginPresenter, DownloadUserInfoUseCase downloadUserInfoUseCase) {
        loginPresenter.i = downloadUserInfoUseCase;
    }

    @DexIgnore
    public static void i(LoginPresenter loginPresenter, FetchActivities fetchActivities) {
        loginPresenter.o = fetchActivities;
    }

    @DexIgnore
    public static void j(LoginPresenter loginPresenter, FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries) {
        loginPresenter.F = fetchDailyGoalTrackingSummaries;
    }

    @DexIgnore
    public static void k(LoginPresenter loginPresenter, FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries) {
        loginPresenter.u = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public static void l(LoginPresenter loginPresenter, FetchGoalTrackingData fetchGoalTrackingData) {
        loginPresenter.G = fetchGoalTrackingData;
    }

    @DexIgnore
    public static void m(LoginPresenter loginPresenter, FetchHeartRateSamples fetchHeartRateSamples) {
        loginPresenter.t = fetchHeartRateSamples;
    }

    @DexIgnore
    public static void n(LoginPresenter loginPresenter, FetchSleepSessions fetchSleepSessions) {
        loginPresenter.r = fetchSleepSessions;
    }

    @DexIgnore
    public static void o(LoginPresenter loginPresenter, FetchSleepSummaries fetchSleepSummaries) {
        loginPresenter.s = fetchSleepSummaries;
    }

    @DexIgnore
    public static void p(LoginPresenter loginPresenter, FetchSummaries fetchSummaries) {
        loginPresenter.p = fetchSummaries;
    }

    @DexIgnore
    public static void q(LoginPresenter loginPresenter, GetSecretKeyUseCase getSecretKeyUseCase) {
        loginPresenter.H = getSecretKeyUseCase;
    }

    @DexIgnore
    public static void r(LoginPresenter loginPresenter, GoalTrackingRepository goalTrackingRepository) {
        loginPresenter.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void s(LoginPresenter loginPresenter, LoginEmailUseCase loginEmailUseCase) {
        loginPresenter.g = loginEmailUseCase;
    }

    @DexIgnore
    public static void t(LoginPresenter loginPresenter, Dv5 dv5) {
        loginPresenter.v = dv5;
    }

    @DexIgnore
    public static void u(LoginPresenter loginPresenter, Ev5 ev5) {
        loginPresenter.x = ev5;
    }

    @DexIgnore
    public static void v(LoginPresenter loginPresenter, LoginSocialUseCase loginSocialUseCase) {
        loginPresenter.h = loginSocialUseCase;
    }

    @DexIgnore
    public static void w(LoginPresenter loginPresenter, Gv5 gv5) {
        loginPresenter.z = gv5;
    }

    @DexIgnore
    public static void x(LoginPresenter loginPresenter, Hv5 hv5) {
        loginPresenter.y = hv5;
    }

    @DexIgnore
    public static void y(LoginPresenter loginPresenter, ReconnectDeviceUseCase reconnectDeviceUseCase) {
        loginPresenter.j = reconnectDeviceUseCase;
    }

    @DexIgnore
    public static void z(LoginPresenter loginPresenter, An4 an4) {
        loginPresenter.n = an4;
    }
}
