package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kz4<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ Range b;
    @DexIgnore
    public /* final */ ServerError c;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Kz4(ServerError serverError) {
        this(null, null, serverError);
        Wg6.c(serverError, "error");
    }

    @DexIgnore
    public Kz4(T t, Range range) {
        this(t, range, null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Kz4(Object obj, Range range, int i, Qg6 qg6) {
        this(obj, (i & 2) != 0 ? null : range);
    }

    @DexIgnore
    public Kz4(T t, Range range, ServerError serverError) {
        this.a = t;
        this.b = range;
        this.c = serverError;
    }

    @DexIgnore
    public final ServerError a() {
        return this.c;
    }

    @DexIgnore
    public final Range b() {
        return this.b;
    }

    @DexIgnore
    public final T c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Kz4) {
                Kz4 kz4 = (Kz4) obj;
                if (!Wg6.a(this.a, kz4.a) || !Wg6.a(this.b, kz4.b) || !Wg6.a(this.c, kz4.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        T t = this.a;
        int hashCode = t != null ? t.hashCode() : 0;
        Range range = this.b;
        int hashCode2 = range != null ? range.hashCode() : 0;
        ServerError serverError = this.c;
        if (serverError != null) {
            i = serverError.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ResultWrapper(value=" + ((Object) this.a) + ", range=" + this.b + ", error=" + this.c + ")";
    }
}
