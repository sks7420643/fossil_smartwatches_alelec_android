package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import com.fossil.N57;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J57 {
    @DexIgnore
    public static /* final */ String a; // = "j57";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ M57 c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ K57 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements N57.Bi {
            @DexIgnore
            public /* final */ /* synthetic */ ImageView a;

            @DexIgnore
            public Aii(ImageView imageView) {
                this.a = imageView;
            }

            @DexIgnore
            @Override // com.fossil.N57.Bi
            public void a(BitmapDrawable bitmapDrawable) {
                K57 k57 = Ai.this.e;
                if (k57 == null) {
                    this.a.setImageDrawable(bitmapDrawable);
                } else {
                    k57.a(bitmapDrawable);
                }
            }
        }

        @DexIgnore
        public Ai(Context context, Bitmap bitmap, M57 m57, boolean z, K57 k57) {
            this.a = context;
            this.b = bitmap;
            this.c = m57;
            this.d = z;
            this.e = k57;
        }

        @DexIgnore
        public void a(ImageView imageView) {
            this.c.a = this.b.getWidth();
            this.c.b = this.b.getHeight();
            if (this.d) {
                new N57(imageView.getContext(), this.b, this.c, new Aii(imageView)).a();
            } else {
                imageView.setImageDrawable(new BitmapDrawable(this.a.getResources(), I57.a(imageView.getContext(), this.b, this.c)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ M57 c; // = new M57();
        @DexIgnore
        public boolean d;
        @DexIgnore
        public K57 e;

        @DexIgnore
        public Bi(Context context) {
            this.b = context;
            View view = new View(context);
            this.a = view;
            view.setTag(J57.a);
        }

        @DexIgnore
        public Ai a(Bitmap bitmap) {
            return new Ai(this.b, bitmap, this.c, this.d, this.e);
        }

        @DexIgnore
        public Bi b(int i) {
            this.c.c = i;
            return this;
        }

        @DexIgnore
        public Bi c(int i) {
            this.c.d = i;
            return this;
        }
    }

    @DexIgnore
    public static Bi a(Context context) {
        return new Bi(context);
    }
}
