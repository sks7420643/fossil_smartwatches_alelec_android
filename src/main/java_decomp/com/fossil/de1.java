package com.fossil;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class De1 {
    @DexIgnore
    public /* final */ Map<String, Ai> a; // = new HashMap();
    @DexIgnore
    public /* final */ Bi b; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Lock a; // = new ReentrantLock();
        @DexIgnore
        public int b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ Queue<Ai> a; // = new ArrayDeque();

        @DexIgnore
        public Ai a() {
            Ai poll;
            synchronized (this.a) {
                poll = this.a.poll();
            }
            return poll == null ? new Ai() : poll;
        }

        @DexIgnore
        public void b(Ai ai) {
            synchronized (this.a) {
                if (this.a.size() < 10) {
                    this.a.offer(ai);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        Ai ai;
        synchronized (this) {
            ai = this.a.get(str);
            if (ai == null) {
                ai = this.b.a();
                this.a.put(str, ai);
            }
            ai.b++;
        }
        ai.a.lock();
    }

    @DexIgnore
    public void b(String str) {
        Ai ai;
        synchronized (this) {
            Ai ai2 = this.a.get(str);
            Ik1.d(ai2);
            ai = ai2;
            if (ai.b >= 1) {
                int i = ai.b - 1;
                ai.b = i;
                if (i == 0) {
                    Ai remove = this.a.remove(str);
                    if (remove.equals(ai)) {
                        this.b.b(remove);
                    } else {
                        throw new IllegalStateException("Removed the wrong lock, expected to remove: " + ai + ", but actually removed: " + remove + ", safeKey: " + str);
                    }
                }
            } else {
                throw new IllegalStateException("Cannot release a lock that is not held, safeKey: " + str + ", interestedThreads: " + ai.b);
            }
        }
        ai.a.unlock();
    }
}
