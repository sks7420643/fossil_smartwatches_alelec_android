package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import com.fossil.C71;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F71 implements C71 {
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public /* final */ S61 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ej0<String, C71.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ F71 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(F71 f71, int i2, int i3) {
            super(i3);
            this.i = f71;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [boolean, java.lang.Object, java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Ej0
        public /* bridge */ /* synthetic */ void b(boolean z, String str, C71.Bi bi, C71.Bi bi2) {
            l(z, str, bi, bi2);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Ej0
        public /* bridge */ /* synthetic */ int j(String str, C71.Bi bi) {
            return m(str, bi);
        }

        @DexIgnore
        public void l(boolean z, String str, C71.Bi bi, C71.Bi bi2) {
            Wg6.c(str, "key");
            Wg6.c(bi, "oldValue");
            this.i.c.a(bi.a());
        }

        @DexIgnore
        public int m(String str, C71.Bi bi) {
            Wg6.c(str, "key");
            Wg6.c(bi, "value");
            return bi.b();
        }
    }

    @DexIgnore
    public F71(S61 s61, int i) {
        Wg6.c(s61, "referenceCounter");
        this.c = s61;
        this.b = new Ai(this, i, i);
    }

    @DexIgnore
    @Override // com.fossil.C71
    public void a(int i) {
        if (Q81.c.a() && Q81.c.b() <= 3) {
            Log.println(3, "RealMemoryCache", "trimMemory, level=" + i);
        }
        if (i >= 40) {
            e();
        } else if (10 <= i && 20 > i) {
            this.b.k(g() / 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.C71
    public C71.Bi b(String str) {
        Wg6.c(str, "key");
        return (C71.Bi) this.b.d(str);
    }

    @DexIgnore
    @Override // com.fossil.C71
    public void c(String str, Bitmap bitmap, boolean z) {
        Wg6.c(str, "key");
        Wg6.c(bitmap, "value");
        int b2 = W81.b(bitmap);
        if (b2 > f()) {
            this.b.g(str);
            return;
        }
        this.c.b(bitmap);
        this.b.f(str, new C71.Bi(bitmap, z, b2));
    }

    @DexIgnore
    public void e() {
        if (Q81.c.a() && Q81.c.b() <= 3) {
            Log.println(3, "RealMemoryCache", "clearMemory");
        }
        this.b.k(-1);
    }

    @DexIgnore
    public int f() {
        return this.b.e();
    }

    @DexIgnore
    public int g() {
        return this.b.i();
    }
}
