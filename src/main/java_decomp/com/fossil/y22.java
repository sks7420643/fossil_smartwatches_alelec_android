package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Y22 implements J32.Di {
    @DexIgnore
    public /* final */ SQLiteDatabase a;

    @DexIgnore
    public Y22(SQLiteDatabase sQLiteDatabase) {
        this.a = sQLiteDatabase;
    }

    @DexIgnore
    public static J32.Di b(SQLiteDatabase sQLiteDatabase) {
        return new Y22(sQLiteDatabase);
    }

    @DexIgnore
    @Override // com.fossil.J32.Di
    public Object a() {
        return this.a.beginTransaction();
    }
}
