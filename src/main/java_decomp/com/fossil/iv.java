package com.fossil;

import com.mapped.Rc6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iv extends Fv {
    @DexIgnore
    public ArrayList<J0> X;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Iv(short s, K5 k5, int i, int i2) {
        super(Iu.d, s, Hs.v, k5, (i2 & 4) != 0 ? 3 : i);
        this.X = new ArrayList<>();
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        JSONObject A = super.A();
        Jd0 jd0 = Jd0.w1;
        Object[] array = this.X.toArray(new J0[0]);
        if (array != null) {
            return G80.k(A, jd0, Px1.a((Ox1[]) array));
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Fv
    public void W() {
        X(this.N);
    }

    @DexIgnore
    public final void X(byte[] bArr) {
        int i = 0;
        while (true) {
            int i2 = i + 10;
            if (i2 <= bArr.length) {
                ByteBuffer order = ByteBuffer.wrap(Dm7.k(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN);
                this.X.add(new J0(this.y.x, order.getShort(0), Hy1.o(order.getInt(2)), Hy1.o(order.getInt(6))));
                i = i2;
            } else {
                return;
            }
        }
    }
}
