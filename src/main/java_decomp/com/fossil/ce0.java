package com.fossil;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.KeyEvent;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ce0 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Object obj);

        @DexIgnore
        void b(int i, int i2, int i3, int i4, int i5);

        @DexIgnore
        void c(Object obj);

        @DexIgnore
        void d(String str, Bundle bundle);

        @DexIgnore
        void f(Bundle bundle);

        @DexIgnore
        void h(List<?> list);

        @DexIgnore
        void j(CharSequence charSequence);

        @DexIgnore
        Object k();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<T extends Ai> extends MediaController.Callback {
        @DexIgnore
        public /* final */ T a;

        @DexIgnore
        public Bi(T t) {
            this.a = t;
        }

        @DexIgnore
        public void onAudioInfoChanged(MediaController.PlaybackInfo playbackInfo) {
            this.a.b(playbackInfo.getPlaybackType(), Ci.b(playbackInfo), playbackInfo.getVolumeControl(), playbackInfo.getMaxVolume(), playbackInfo.getCurrentVolume());
        }

        @DexIgnore
        public void onExtrasChanged(Bundle bundle) {
            MediaSessionCompat.a(bundle);
            this.a.f(bundle);
        }

        @DexIgnore
        public void onMetadataChanged(MediaMetadata mediaMetadata) {
            this.a.a(mediaMetadata);
        }

        @DexIgnore
        public void onPlaybackStateChanged(PlaybackState playbackState) {
            this.a.c(playbackState);
        }

        @DexIgnore
        @Override // android.media.session.MediaController.Callback
        public void onQueueChanged(List<MediaSession.QueueItem> list) {
            this.a.h(list);
        }

        @DexIgnore
        public void onQueueTitleChanged(CharSequence charSequence) {
            this.a.j(charSequence);
        }

        @DexIgnore
        public void onSessionDestroyed() {
            this.a.k();
        }

        @DexIgnore
        public void onSessionEvent(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            this.a.d(str, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public static AudioAttributes a(Object obj) {
            return ((MediaController.PlaybackInfo) obj).getAudioAttributes();
        }

        @DexIgnore
        public static int b(Object obj) {
            return c(a(obj));
        }

        @DexIgnore
        public static int c(AudioAttributes audioAttributes) {
            if ((audioAttributes.getFlags() & 1) == 1) {
                return 7;
            }
            if ((audioAttributes.getFlags() & 4) == 4) {
                return 6;
            }
            int usage = audioAttributes.getUsage();
            if (usage == 13) {
                return 1;
            }
            switch (usage) {
                case 2:
                    return 0;
                case 3:
                    return 8;
                case 4:
                    return 4;
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                    return 5;
                case 6:
                    return 2;
                default:
                    return 3;
            }
        }
    }

    @DexIgnore
    public static Object a(Ai ai) {
        return new Bi(ai);
    }

    @DexIgnore
    public static boolean b(Object obj, KeyEvent keyEvent) {
        return ((MediaController) obj).dispatchMediaButtonEvent(keyEvent);
    }

    @DexIgnore
    public static Object c(Context context, Object obj) {
        return new MediaController(context, (MediaSession.Token) obj);
    }

    @DexIgnore
    public static void d(Object obj, String str, Bundle bundle, ResultReceiver resultReceiver) {
        ((MediaController) obj).sendCommand(str, bundle, resultReceiver);
    }
}
