package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.U40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G8 extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Hc b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public G8(Hc hc) {
        super(1);
        this.b = hc;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Yx1 yx1) {
        Yx1 yx12 = yx1;
        Zw zw = Zw.i;
        Zw.d.remove(this.b.b);
        Bl1 bl1 = (Bl1) yx12;
        if (bl1.getErrorCode() == U40.REQUEST_UNSUPPORTED) {
            Zw.i.h(this.b.b);
            this.b.b.t0(yx12);
        } else if (bl1.getErrorCode() == U40.REQUEST_FAILED && bl1.a().c == Zq.F) {
            Zw zw2 = Zw.i;
            Zw.c.add(this.b.b);
        } else {
            Zw.i.c(this.b.b, Qd0.b.a("HID_EXPONENT_BACK_OFF_TAG"));
        }
        return Cd6.a;
    }
}
