package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.d26;
import com.fossil.f26;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.mt5;
import com.fossil.r16;
import com.fossil.tq4;
import com.fossil.v36;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d16 extends x06 {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ a E; // = new a(null);
    @DexIgnore
    public /* final */ mt5 A;
    @DexIgnore
    public /* final */ QuickResponseRepository B;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase C;
    @DexIgnore
    public List<AppNotificationFilter> e; // = new ArrayList();
    @DexIgnore
    public List<i06> f; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> g; // = new ArrayList();
    @DexIgnore
    public /* final */ b h; // = new b();
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public /* final */ List<j06> o; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> p; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<List<NotificationSettingsModel>> q; // = this.z.getListNotificationSettings();
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public r16 s;
    @DexIgnore
    public /* final */ y06 t;
    @DexIgnore
    public /* final */ uq4 u;
    @DexIgnore
    public /* final */ d26 v;
    @DexIgnore
    public /* final */ f26 w;
    @DexIgnore
    public /* final */ v36 x;
    @DexIgnore
    public /* final */ on5 y;
    @DexIgnore
    public /* final */ NotificationSettingsDao z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return d16.D;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(d16.E.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = d16.E.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode + " isSettingRules " + d16.this.i);
            if (communicateMode == CommunicateMode.SET_NOTIFICATION_FILTERS && d16.this.i) {
                d16.this.i = false;
                d16.this.t.a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(d16.E.a(), "onReceive - success");
                    d16.this.t.close();
                    return;
                }
                FLogger.INSTANCE.getLocal().d(d16.E.a(), "onReceive - failed");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = d16.E.a();
                local2.d(a3, "mAllowMessagesFromFirsLoad=" + d16.this.U() + " mAlowCallsFromFirstLoad=" + d16.this.V() + " mListFavoriteContactFirstLoad=" + d16.this.Y() + " size=" + d16.this.Y().size());
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", d16.this.V(), true);
                NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", d16.this.U(), false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                d16.this.c0(arrayList);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                ArrayList<Integer> arrayList2 = integerArrayListExtra != null ? integerArrayListExtra : new ArrayList<>(intExtra2);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = d16.E.a();
                local3.d(a4, "permissionErrorCodes=" + arrayList2 + " , size=" + arrayList2.size());
                int size = arrayList2.size();
                for (int i = 0; i < size; i++) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a5 = d16.E.a();
                    local4.d(a5, "error code " + i + " =" + arrayList2.get(i));
                }
                if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                    List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(arrayList2);
                    pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                    y06 y06 = d16.this.t;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                    if (array != null) {
                        uh5[] uh5Arr = (uh5[]) array;
                        y06.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
                d16.this.t.c();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.d<f26.b, tq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d16 f711a;
        @DexIgnore
        public /* final */ /* synthetic */ ContactGroup b;

        @DexIgnore
        public c(d16 d16, ContactGroup contactGroup) {
            this.f711a = d16;
            this.b = contactGroup;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(tq4.a aVar) {
            pq7.c(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(d16.E.a(), ".Inside mRemoveContactGroup onError");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(f26.b bVar) {
            pq7.c(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(d16.E.a(), ".Inside mRemoveContactGroup onSuccess");
            this.f711a.t.h3(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {499}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $settings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d16$d$a$a")
            /* renamed from: com.fossil.d16$d$a$a  reason: collision with other inner class name */
            public static final class RunnableC0041a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ a b;

                @DexIgnore
                public RunnableC0041a(a aVar) {
                    this.b = aVar;
                }

                @DexIgnore
                public final void run() {
                    this.b.this$0.this$0.C.getNotificationSettingsDao().insertListNotificationSettings(this.b.this$0.$settings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.C.runInTransaction(new RunnableC0041a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(d16 d16, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = d16;
            this.$settings = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$settings, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {428}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements iq4.e<mt5.c, mt5.b> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ e f712a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(e eVar) {
                this.f712a = eVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(mt5.b bVar) {
                pq7.c(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().e(d16.E.a(), "Set reply message to device fail");
                this.f712a.this$0.t.a();
                this.f712a.this$0.t.C2(false);
                if (!jn5.b.m(PortfolioApp.h0.c().getApplicationContext(), jn5.c.BLUETOOTH_CONNECTION)) {
                    jn5 jn5 = jn5.b;
                    y06 y06 = this.f712a.this$0.t;
                    if (y06 != null) {
                        jn5.c(jn5, ((z06) y06).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null);
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
                }
                this.f712a.this$0.t.E2();
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(mt5.c cVar) {
                pq7.c(cVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(d16.E.a(), "Set reply message to device success");
                this.f712a.this$0.t.a();
                this.f712a.this$0.t.C2(true);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1$messages$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<? extends QuickResponseMessage>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends QuickResponseMessage>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.B.getAllQuickResponse();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(d16 d16, qn7 qn7) {
            super(2, qn7);
            this.this$0 = d16;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.t.b();
                dv7 i2 = this.this$0.i();
                b bVar = new b(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, bVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.A.e(new mt5.a((List) g), new a(this));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {393, 395}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {333}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends AppNotificationFilter>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX DEBUG: Failed to insert an additional move for type inference into block B:42:0x00e3 */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Object] */
            /* JADX WARN: Type inference failed for: r2v1 */
            /* JADX WARN: Type inference failed for: r2v2, types: [java.lang.Object] */
            /* JADX WARN: Type inference failed for: r2v3 */
            /* JADX WARN: Type inference failed for: r2v4, types: [java.util.List, java.util.ArrayList] */
            /* JADX WARN: Type inference failed for: r2v5 */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 722
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.d16.f.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$otherAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends AppNotificationFilter>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return d47.c(this.this$0.this$0.X(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(d16 d16, qn7 qn7) {
            super(2, qn7);
            this.this$0 = d16;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00e0  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 534
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.d16.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<r16.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d16 f713a;

        @DexIgnore
        public g(d16 d16) {
            this.f713a = d16;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r16.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = d16.E.a();
            local.d(a2, "NotificationSettingChanged value = " + aVar);
            String Z = this.f713a.Z(aVar.a());
            if (aVar.b()) {
                this.f713a.t.m4(Z);
            } else {
                this.f713a.t.w1(Z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2", f = "NotificationCallsAndMessagesPresenter.kt", l = {158, 165}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {159}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements iq4.e<d26.c, d26.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ h f714a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(h hVar) {
                this.f714a = hVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(d26.a aVar) {
                pq7.c(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(d16.E.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(d26.c cVar) {
                pq7.c(cVar, "responseValue");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = d16.E.a();
                local.d(a2, "inside start, GetAllContactGroup onSuccess, size = " + cVar.a().size());
                this.f714a.this$0.t.D4(pm7.j0(cVar.a()));
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = d16.E.a();
                local2.d(a3, "mFlagGetListFavoriteFirstLoad=" + this.f714a.this$0.W());
                if (this.f714a.this$0.W()) {
                    this.f714a.this$0.g0(pm7.j0(cVar.a()));
                    for (T t : cVar.a()) {
                        List<Contact> contacts = t.getContacts();
                        pq7.b(contacts, "contactGroup.contacts");
                        if (!contacts.isEmpty()) {
                            Contact contact = t.getContacts().get(0);
                            j06 j06 = new j06(contact, null, 2, null);
                            j06.setAdded(true);
                            Contact contact2 = j06.getContact();
                            if (contact2 != null) {
                                pq7.b(contact, "contact");
                                contact2.setDbRowId(contact.getDbRowId());
                            }
                            Contact contact3 = j06.getContact();
                            if (contact3 != null) {
                                pq7.b(contact, "contact");
                                contact3.setUseSms(contact.isUseSms());
                            }
                            Contact contact4 = j06.getContact();
                            if (contact4 != null) {
                                pq7.b(contact, "contact");
                                contact4.setUseCall(contact.isUseCall());
                            }
                            pq7.b(contact, "contact");
                            List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                            pq7.b(phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                pq7.b(phoneNumber, "contact.phoneNumbers[0]");
                                if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                    j06.setHasPhoneNumber(true);
                                    PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                    pq7.b(phoneNumber2, "contact.phoneNumbers[0]");
                                    j06.setPhoneNumber(phoneNumber2.getNumber());
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String a4 = d16.E.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                    PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                    pq7.b(phoneNumber3, "contact.phoneNumbers[0]");
                                    sb.append(phoneNumber3.getNumber());
                                    local3.d(a4, sb.toString());
                                }
                            }
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String a5 = d16.E.a();
                            local4.d(a5, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.f714a.this$0.Y().add(j06);
                        }
                    }
                    this.f714a.this$0.f0(false);
                }
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String a6 = d16.E.a();
                local5.d(a6, "start, mListFavoriteContactWrapperFirstLoad=" + this.f714a.this$0.Y() + " size=" + this.f714a.this$0.Y().size());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements iq4.e<v36.a, iq4.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ h f715a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public c(h hVar) {
                this.f715a = hVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(iq4.a aVar) {
                pq7.c(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(d16.E.a(), "GetApps onError");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(v36.a aVar) {
                pq7.c(aVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(d16.E.a(), "GetApps onSuccess");
                this.f715a.this$0.X().clear();
                this.f715a.this$0.X().addAll(aVar.a());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$settings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(h hVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, qn7);
                dVar.p$ = (iv7) obj;
                throw null;
                //return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends NotificationSettingsModel>> qn7) {
                throw null;
                //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.z.getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(d16 d16, qn7 qn7) {
            super(2, qn7);
            this.this$0 = d16;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x008c  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 383
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.d16.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = d16.class.getSimpleName();
        pq7.b(simpleName, "NotificationCallsAndMess\u2026er::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    public d16(y06 y06, uq4 uq4, d26 d26, f26 f26, g26 g26, v36 v36, on5 on5, NotificationSettingsDao notificationSettingsDao, mt5 mt5, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        pq7.c(y06, "mView");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(d26, "mGetAllContactGroup");
        pq7.c(f26, "mRemoveContactGroup");
        pq7.c(g26, "mSaveContactGroupsNotification");
        pq7.c(v36, "mGetApps");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(notificationSettingsDao, "mNotificationSettingDao");
        pq7.c(mt5, "mReplyMessageUseCase");
        pq7.c(quickResponseRepository, "mQRRepository");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.t = y06;
        this.u = uq4;
        this.v = d26;
        this.w = f26;
        this.x = v36;
        this.y = on5;
        this.z = notificationSettingsDao;
        this.A = mt5;
        this.B = quickResponseRepository;
        this.C = notificationSettingsDatabase;
    }

    @DexIgnore
    public final int U() {
        return this.l;
    }

    @DexIgnore
    public final int V() {
        return this.j;
    }

    @DexIgnore
    public final boolean W() {
        return this.n;
    }

    @DexIgnore
    public final List<i06> X() {
        return this.f;
    }

    @DexIgnore
    public final List<j06> Y() {
        return this.o;
    }

    @DexIgnore
    public final String Z(int i2) {
        if (i2 == 0) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886089);
            pq7.b(c2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return c2;
        } else if (i2 != 1) {
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886091);
            pq7.b(c3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return c3;
        } else {
            String c4 = um5.c(PortfolioApp.h0.c(), 2131886090);
            pq7.b(c4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return c4;
        }
    }

    @DexIgnore
    public final boolean a0() {
        return (this.t.L1() == this.j && this.t.O2() == this.l && !(pq7.a(this.p, this.t.f5()) ^ true)) ? false : true;
    }

    @DexIgnore
    public final void b0() {
        FLogger.INSTANCE.getLocal().d(D, "registerBroadcastReceiver");
        wq5.d.e(this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void c0(List<NotificationSettingsModel> list) {
        FLogger.INSTANCE.getLocal().d(D, "saveNotificationSettings");
        xw7 unused = gu7.d(k(), null, null, new d(this, list, null), 3, null);
    }

    @DexIgnore
    public final void d0(int i2) {
        this.l = i2;
    }

    @DexIgnore
    public final void e0(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void f0(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    public final void g0(List<ContactGroup> list) {
        pq7.c(list, "<set-?>");
        this.p = list;
    }

    @DexIgnore
    public void h0() {
        this.t.M5(this);
    }

    @DexIgnore
    public final void i0() {
        FLogger.INSTANCE.getLocal().d(D, "unregisterBroadcastReceiver");
        wq5.d.j(this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(D, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        b0();
        wq5.d.g(CommunicateMode.SET_NOTIFICATION_FILTERS);
        y06 y06 = this.t;
        if (y06 != null) {
            FragmentActivity activity = ((z06) y06).getActivity();
            r16 r16 = this.s;
            if (r16 != null) {
                r16.a().h((LifecycleOwner) this.t, new g(this));
                if (!this.t.O5()) {
                    jn5 jn5 = jn5.b;
                    if (activity == null) {
                        pq7.i();
                        throw null;
                    } else if (jn5.c(jn5, activity, jn5.a.NOTIFICATION_GET_CONTACTS, false, true, false, null, 52, null)) {
                        xw7 unused = gu7.d(k(), null, null, new h(this, null), 3, null);
                    }
                }
                y06 y062 = this.t;
                y062.F(!jn5.c(jn5.b, ((z06) y062).getContext(), jn5.a.QUICK_RESPONSE, false, false, false, null, 56, null));
                return;
            }
            pq7.n("mNotificationSettingViewModel");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        i0();
        LiveData<List<NotificationSettingsModel>> liveData = this.q;
        y06 y06 = this.t;
        if (y06 != null) {
            liveData.n((z06) y06);
            r16 r16 = this.s;
            if (r16 != null) {
                r16.a().n((LifecycleOwner) this.t);
                FLogger.INSTANCE.getLocal().d(D, "stop");
                return;
            }
            pq7.n("mNotificationSettingViewModel");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void n() {
        jn5 jn5 = jn5.b;
        y06 y06 = this.t;
        if (y06 != null) {
            jn5.c(jn5, ((z06) y06).getContext(), jn5.a.QUICK_RESPONSE, false, false, false, null, 60, null);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.x06
    public boolean o() {
        Boolean r0 = this.y.r0();
        pq7.b(r0, "mSharedPreferencesManager.isQuickResponseEnabled");
        return r0.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void p() {
        this.t.a();
        this.t.close();
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void q(ContactGroup contactGroup) {
        pq7.c(contactGroup, "contactGroup");
        this.u.a(this.w, new f26.a(contactGroup), new c(this, contactGroup));
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void r(boolean z2) {
        this.y.K1(Boolean.valueOf(z2));
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void s() {
        this.y.Z0(PortfolioApp.h0.c().J(), 0, false);
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void t(r16 r16) {
        pq7.c(r16, "viewModel");
        this.s = r16;
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void u(boolean z2) {
        if (z2) {
            jn5 jn5 = jn5.b;
            y06 y06 = this.t;
            if (y06 == null) {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
            } else if (!jn5.c(jn5, ((z06) y06).getContext(), jn5.a.QUICK_RESPONSE, false, false, true, 112, 12, null)) {
                return;
            }
        }
        xw7 unused = gu7.d(k(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void v(boolean z2) {
        if (!a0()) {
            FLogger.INSTANCE.getLocal().d(D, "setRuleToDevice, nothing changed");
            this.t.close();
            return;
        }
        jn5 jn5 = jn5.b;
        y06 y06 = this.t;
        if (y06 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        } else if (!jn5.c(jn5, ((z06) y06).getContext(), jn5.a.SET_BLE_COMMAND, false, true, false, null, 52, null)) {
        } else {
            if (!z2 || jn5.c(jn5.b, ((z06) this.t).getContext(), jn5.a.NOTIFICATION_DIANA, false, false, true, null, 44, null)) {
                xw7 unused = gu7.d(k(), null, null, new f(this, null), 3, null);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.x06
    public void w(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = D;
        local.d(str, "updateNotificationSetting isCall=" + z2);
        y06 y06 = this.t;
        r16.a aVar = new r16.a(z2 ? y06.L1() : y06.O2(), z2);
        r16 r16 = this.s;
        if (r16 != null) {
            r16.a().l(aVar);
            this.t.V4();
            return;
        }
        pq7.n("mNotificationSettingViewModel");
        throw null;
    }
}
