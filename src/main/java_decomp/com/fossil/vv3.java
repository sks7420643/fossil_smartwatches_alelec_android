package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vv3 implements Parcelable.Creator<Uv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Uv3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        byte b = 0;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    i = Ad2.v(parcel, t);
                    break;
                case 3:
                    str7 = Ad2.f(parcel, t);
                    break;
                case 4:
                    str6 = Ad2.f(parcel, t);
                    break;
                case 5:
                    str5 = Ad2.f(parcel, t);
                    break;
                case 6:
                    str4 = Ad2.f(parcel, t);
                    break;
                case 7:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 8:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 9:
                    b = Ad2.o(parcel, t);
                    break;
                case 10:
                    b2 = Ad2.o(parcel, t);
                    break;
                case 11:
                    b3 = Ad2.o(parcel, t);
                    break;
                case 12:
                    b4 = Ad2.o(parcel, t);
                    break;
                case 13:
                    str = Ad2.f(parcel, t);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Uv3(i, str7, str6, str5, str4, str3, str2, b, b2, b3, b4, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Uv3[] newArray(int i) {
        return new Uv3[i];
    }
}
