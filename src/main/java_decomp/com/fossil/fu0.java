package com.fossil;

import com.mapped.Zf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fu0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Zf.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Eu0 a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ Eu0 c;
        @DexIgnore
        public /* final */ /* synthetic */ Zf.Di d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public Ai(Eu0 eu0, int i, Eu0 eu02, Zf.Di di, int i2, int i3) {
            this.a = eu0;
            this.b = i;
            this.c = eu02;
            this.d = di;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        @Override // com.mapped.Zf.Bi
        public boolean a(int i, int i2) {
            Object obj = this.a.get(this.b + i);
            Eu0 eu0 = this.c;
            Object obj2 = eu0.get(eu0.h() + i2);
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        @Override // com.mapped.Zf.Bi
        public boolean b(int i, int i2) {
            Object obj = this.a.get(this.b + i);
            Eu0 eu0 = this.c;
            Object obj2 = eu0.get(eu0.h() + i2);
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areItemsTheSame(obj, obj2);
        }

        @DexIgnore
        @Override // com.mapped.Zf.Bi
        public Object c(int i, int i2) {
            Object obj = this.a.get(this.b + i);
            Eu0 eu0 = this.c;
            Object obj2 = eu0.get(eu0.h() + i2);
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        @Override // com.mapped.Zf.Bi
        public int d() {
            return this.f;
        }

        @DexIgnore
        @Override // com.mapped.Zf.Bi
        public int e() {
            return this.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Jv0 {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Jv0 b;

        @DexIgnore
        public Bi(int i, Jv0 jv0) {
            this.a = i;
            this.b = jv0;
        }

        @DexIgnore
        @Override // com.fossil.Jv0
        public void a(int i, int i2) {
            Jv0 jv0 = this.b;
            int i3 = this.a;
            jv0.a(i + i3, i3 + i2);
        }

        @DexIgnore
        @Override // com.fossil.Jv0
        public void b(int i, int i2) {
            this.b.b(this.a + i, i2);
        }

        @DexIgnore
        @Override // com.fossil.Jv0
        public void c(int i, int i2) {
            this.b.c(this.a + i, i2);
        }

        @DexIgnore
        @Override // com.fossil.Jv0
        public void d(int i, int i2, Object obj) {
            this.b.d(this.a + i, i2, obj);
        }
    }

    @DexIgnore
    public static <T> Zf.Ci a(Eu0<T> eu0, Eu0<T> eu02, Zf.Di<T> di) {
        int d = eu0.d();
        return Zf.b(new Ai(eu0, d, eu02, di, (eu0.size() - d) - eu0.e(), (eu02.size() - eu02.d()) - eu02.e()), true);
    }

    @DexIgnore
    public static <T> void b(Jv0 jv0, Eu0<T> eu0, Eu0<T> eu02, Zf.Ci ci) {
        int e = eu0.e();
        int e2 = eu02.e();
        int d = eu0.d();
        int d2 = eu02.d();
        if (e == 0 && e2 == 0 && d == 0 && d2 == 0) {
            ci.e(jv0);
            return;
        }
        if (e > e2) {
            int i = e - e2;
            jv0.c(eu0.size() - i, i);
        } else if (e < e2) {
            jv0.b(eu0.size(), e2 - e);
        }
        if (d > d2) {
            jv0.c(0, d - d2);
        } else if (d < d2) {
            jv0.b(0, d2 - d);
        }
        if (d2 != 0) {
            ci.e(new Bi(d2, jv0));
        } else {
            ci.e(jv0);
        }
    }

    @DexIgnore
    public static int c(Zf.Ci ci, Eu0 eu0, Eu0 eu02, int i) {
        int d = eu0.d();
        int i2 = i - d;
        int size = eu0.size();
        int e = eu0.e();
        if (i2 >= 0 && i2 < (size - d) - e) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 % 2 == 1 ? -1 : 1) * (i3 / 2)) + i2;
                if (i4 >= 0 && i4 < eu0.n()) {
                    try {
                        int b = ci.b(i4);
                        if (b != -1) {
                            return b + eu02.h();
                        }
                    } catch (IndexOutOfBoundsException e2) {
                    }
                }
            }
        }
        return Math.max(0, Math.min(i, eu02.size() - 1));
    }
}
