package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class D22 implements Runnable {
    @DexIgnore
    public /* final */ F22 b;

    @DexIgnore
    public D22(F22 f22) {
        this.b = f22;
    }

    @DexIgnore
    public static Runnable a(F22 f22) {
        return new D22(f22);
    }

    @DexIgnore
    public void run() {
        F22 f22;
        f22.d.a(E22.b(this.b));
    }
}
