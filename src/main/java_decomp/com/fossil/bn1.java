package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bn1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ byte f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Bn1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Bn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new Bn1(bArr[0], bArr[1], bArr[2], bArr[3]);
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public Bn1 b(Parcel parcel) {
            return new Bn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bn1 createFromParcel(Parcel parcel) {
            return new Bn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bn1[] newArray(int i) {
            return new Bn1[i];
        }
    }

    @DexIgnore
    public Bn1(byte b, byte b2, byte b3, byte b4) throws IllegalArgumentException {
        super(Zm1.DO_NOT_DISTURB_SCHEDULE);
        this.c = (byte) b;
        this.d = (byte) b2;
        this.e = (byte) b3;
        this.f = (byte) b4;
        d();
    }

    @DexIgnore
    public /* synthetic */ Bn1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        this.f = parcel.readByte();
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(this.c).put(this.d).put(this.e).put(this.f).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.c));
            jSONObject.put("start_minute", Byte.valueOf(this.d));
            jSONObject.put("stop_hour", Byte.valueOf(this.e));
            jSONObject.put("stop_minute", Byte.valueOf(this.f));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        byte b = this.c;
        if (b >= 0 && 23 >= b) {
            byte b2 = this.d;
            if (b2 >= 0 && 59 >= b2) {
                byte b3 = this.e;
                if (b3 >= 0 && 23 >= b3) {
                    byte b4 = this.f;
                    if (b4 < 0 || 59 < b4) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalArgumentException(E.c(E.e("stopMinute("), this.f, ") is out of range ", "[0, 59]."));
                    }
                    return;
                }
                throw new IllegalArgumentException(E.c(E.e("stopHour("), this.e, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(E.c(E.e("startMinute("), this.d, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(E.c(E.e("startHour("), this.c, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Bn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Bn1 bn1 = (Bn1) obj;
            if (this.c != bn1.c) {
                return false;
            }
            if (this.d != bn1.d) {
                return false;
            }
            if (this.e != bn1.e) {
                return false;
            }
            return this.f == bn1.f;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DoNotDisturbScheduleConfig");
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.c;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.d;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.e;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (((((((super.hashCode() * 31) + this.c) * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
        if (parcel != null) {
            parcel.writeByte(this.f);
        }
    }
}
