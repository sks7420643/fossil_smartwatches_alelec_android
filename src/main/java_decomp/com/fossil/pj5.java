package com.fossil;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.fossil.Af1;
import com.fossil.Wb1;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pj5 implements Af1<Qj5, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai implements Wb1<InputStream> {
        @DexIgnore
        public volatile boolean b;
        @DexIgnore
        public /* final */ Qj5 c;

        @DexIgnore
        public Ai(Pj5 pj5, Qj5 qj5) {
            Wg6.c(qj5, "mAppIconModel");
            this.c = qj5;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
            this.b = true;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super InputStream> ai) {
            Wg6.c(sa1, "priority");
            Wg6.c(ai, Constants.CALLBACK);
            try {
                Drawable applicationIcon = PortfolioApp.get.instance().getPackageManager().getApplicationIcon(this.c.c().getIdentifier());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap i = I37.i(applicationIcon);
                if (i != null) {
                    i.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                }
                ai.e(this.b ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            } catch (PackageManager.NameNotFoundException e) {
                ai.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Bf1<Qj5, InputStream> {
        @DexIgnore
        public Pj5 a(Ef1 ef1) {
            Wg6.c(ef1, "multiFactory");
            return new Pj5();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Af1' to match base method */
        @Override // com.fossil.Bf1
        public /* bridge */ /* synthetic */ Af1<Qj5, InputStream> b(Ef1 ef1) {
            return a(ef1);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Qj5 qj5) {
        return d(qj5);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(Qj5 qj5, int i, int i2, Ob1 ob1) {
        return c(qj5, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> c(Qj5 qj5, int i, int i2, Ob1 ob1) {
        Wg6.c(qj5, "appIconModel");
        Wg6.c(ob1, "options");
        return new Af1.Ai<>(qj5, new Ai(this, qj5));
    }

    @DexIgnore
    public boolean d(Qj5 qj5) {
        Wg6.c(qj5, "appIconModel");
        return true;
    }
}
