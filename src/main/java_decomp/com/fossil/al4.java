package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Al4 extends Hl4 {
    @DexIgnore
    public static volatile Al4[] f;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public Al4() {
        c();
    }

    @DexIgnore
    public static Al4[] d() {
        if (f == null) {
            synchronized (Fl4.a) {
                if (f == null) {
                    f = new Al4[0];
                }
            }
        }
        return f;
    }

    @DexIgnore
    @Override // com.fossil.Hl4
    public /* bridge */ /* synthetic */ Hl4 b(El4 el4) throws IOException {
        e(el4);
        return this;
    }

    @DexIgnore
    public Al4 c() {
        this.a = "";
        this.b = "";
        this.c = Jl4.a;
        this.d = "";
        this.e = "";
        return this;
    }

    @DexIgnore
    public Al4 e(El4 el4) throws IOException {
        while (true) {
            int q = el4.q();
            if (q == 0) {
                break;
            } else if (q == 10) {
                this.a = el4.p();
            } else if (q == 18) {
                this.b = el4.p();
            } else if (q == 26) {
                int a2 = Jl4.a(el4, 26);
                String[] strArr = this.c;
                int length = strArr == null ? 0 : strArr.length;
                int i = a2 + length;
                String[] strArr2 = new String[i];
                if (length != 0) {
                    System.arraycopy(this.c, 0, strArr2, 0, length);
                }
                while (length < i - 1) {
                    strArr2[length] = el4.p();
                    el4.q();
                    length++;
                }
                strArr2[length] = el4.p();
                this.c = strArr2;
            } else if (q == 34) {
                this.d = el4.p();
            } else if (q == 42) {
                this.e = el4.p();
            } else if (q == 48) {
                el4.h();
            } else if (!Jl4.e(el4, q)) {
                break;
            }
        }
        return this;
    }
}
