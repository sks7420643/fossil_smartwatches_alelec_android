package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum K77 {
    WHITE(1),
    GREY(2);
    
    @DexIgnore
    public /* final */ int id;

    @DexIgnore
    public K77(int i) {
        this.id = i;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }
}
