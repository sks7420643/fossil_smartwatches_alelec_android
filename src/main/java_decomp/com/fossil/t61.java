package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.lifecycle.Lifecycle;
import coil.memory.BaseRequestDelegate;
import coil.memory.RequestDelegate;
import coil.memory.ViewTargetRequestDelegate;
import com.mapped.Kc6;
import com.mapped.Rl6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T61 {
    @DexIgnore
    public /* final */ A51 a;
    @DexIgnore
    public /* final */ S61 b;

    @DexIgnore
    public T61(A51 a51, S61 s61) {
        Wg6.c(a51, "imageLoader");
        Wg6.c(s61, "referenceCounter");
        this.a = a51;
        this.b = s61;
    }

    @DexIgnore
    public final RequestDelegate a(X71 x71, I71 i71, Lifecycle lifecycle, Dv7 dv7, Rl6<? extends Drawable> rl6) {
        Wg6.c(x71, "request");
        Wg6.c(i71, "targetDelegate");
        Wg6.c(lifecycle, "lifecycle");
        Wg6.c(dv7, "mainDispatcher");
        Wg6.c(rl6, "deferred");
        if (x71 instanceof T71) {
            J81 u = x71.u();
            if (u instanceof K81) {
                ViewTargetRequestDelegate viewTargetRequestDelegate = new ViewTargetRequestDelegate(this.a, (T71) x71, i71, lifecycle, dv7, rl6);
                lifecycle.a(viewTargetRequestDelegate);
                W81.i(((K81) u).getView()).c(viewTargetRequestDelegate);
                return viewTargetRequestDelegate;
            }
            BaseRequestDelegate baseRequestDelegate = new BaseRequestDelegate(lifecycle, dv7, rl6);
            lifecycle.a(baseRequestDelegate);
            return baseRequestDelegate;
        }
        throw new Kc6();
    }

    @DexIgnore
    public final I71 b(X71 x71) {
        Wg6.c(x71, "request");
        if (x71 instanceof T71) {
            J81 u = x71.u();
            return u == null ? V61.a : u instanceof I81 ? new E71((I81) u, this.b) : new A71(u, this.b);
        }
        throw new Kc6();
    }
}
