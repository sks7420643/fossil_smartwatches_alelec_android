package com.fossil;

import com.mapped.HybridCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fc6 implements Factory<HybridCustomizeEditPresenter> {
    @DexIgnore
    public static HybridCustomizeEditPresenter a(Zb6 zb6, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase) {
        return new HybridCustomizeEditPresenter(zb6, setHybridPresetToWatchUseCase);
    }
}
