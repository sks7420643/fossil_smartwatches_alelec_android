package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ng2 implements Parcelable.Creator<Kg2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Kg2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        IBinder iBinder = null;
        String str = null;
        boolean z2 = false;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                str = Ad2.f(parcel, t);
            } else if (l == 2) {
                iBinder = Ad2.u(parcel, t);
            } else if (l == 3) {
                z2 = Ad2.m(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                z = Ad2.m(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Kg2(str, iBinder, z2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Kg2[] newArray(int i) {
        return new Kg2[i];
    }
}
