package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X71 {

    @DexIgnore
    public interface Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public static void a(Ai ai, Object obj) {
            }

            @DexIgnore
            public static void b(Ai ai, Object obj, Throwable th) {
                Wg6.c(th, "throwable");
            }

            @DexIgnore
            public static void c(Ai ai, Object obj) {
                Wg6.c(obj, "data");
            }

            @DexIgnore
            public static void d(Ai ai, Object obj, Q51 q51) {
                Wg6.c(obj, "data");
                Wg6.c(q51, "source");
            }
        }

        @DexIgnore
        void a(Object obj);

        @DexIgnore
        void b(Object obj, Throwable th);

        @DexIgnore
        void c(Object obj, Q51 q51);

        @DexIgnore
        void onCancel(Object obj);
    }

    @DexIgnore
    public X71() {
    }

    @DexIgnore
    public /* synthetic */ X71(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public abstract List<String> a();

    @DexIgnore
    public abstract boolean b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract Bitmap.Config d();

    @DexIgnore
    public abstract ColorSpace e();

    @DexIgnore
    public abstract U51 f();

    @DexIgnore
    public abstract S71 g();

    @DexIgnore
    public abstract Dv7 h();

    @DexIgnore
    public abstract Drawable i();

    @DexIgnore
    public abstract Drawable j();

    @DexIgnore
    public abstract P18 k();

    @DexIgnore
    public abstract String l();

    @DexIgnore
    public abstract Ai m();

    @DexIgnore
    public abstract S71 n();

    @DexIgnore
    public abstract S71 o();

    @DexIgnore
    public abstract W71 p();

    @DexIgnore
    public abstract Drawable q();

    @DexIgnore
    public abstract D81 r();

    @DexIgnore
    public abstract E81 s();

    @DexIgnore
    public abstract G81 t();

    @DexIgnore
    public abstract J81 u();

    @DexIgnore
    public abstract List<M81> v();

    @DexIgnore
    public abstract N81 w();
}
