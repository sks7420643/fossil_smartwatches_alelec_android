package com.fossil;

import com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xx4 implements Factory<BCRecommendationViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;

    @DexIgnore
    public Xx4(Provider<Tt4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Xx4 a(Provider<Tt4> provider) {
        return new Xx4(provider);
    }

    @DexIgnore
    public static BCRecommendationViewModel c(Tt4 tt4) {
        return new BCRecommendationViewModel(tt4);
    }

    @DexIgnore
    public BCRecommendationViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
