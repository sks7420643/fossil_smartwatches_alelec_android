package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M63 implements J63 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.sdk.referrer.delayed_install_referrer_api", false);
        hw2.b("measurement.id.sdk.referrer.delayed_install_referrer_api", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.J63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.J63
    public final boolean zzb() {
        return a.o().booleanValue();
    }
}
