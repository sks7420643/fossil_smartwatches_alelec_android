package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oc6 implements MembersInjector<SearchMicroAppActivity> {
    @DexIgnore
    public static void a(SearchMicroAppActivity searchMicroAppActivity, SearchMicroAppPresenter searchMicroAppPresenter) {
        searchMicroAppActivity.A = searchMicroAppPresenter;
    }
}
