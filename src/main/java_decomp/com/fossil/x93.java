package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X93 extends Pu2 implements U93 {
    @DexIgnore
    public X93() {
        super("com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    @Override // com.fossil.Pu2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        c((Bundle) Qt2.a(parcel, Bundle.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
