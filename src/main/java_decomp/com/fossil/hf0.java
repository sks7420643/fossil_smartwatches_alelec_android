package com.fossil;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.StateSet;
import androidx.collection.SparseArrayCompat;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.If0;
import com.fossil.Kf0;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public class Hf0 extends Kf0 implements Bm0 {
    @DexIgnore
    public Ci u;
    @DexIgnore
    public Gi v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Gi {
        @DexIgnore
        public /* final */ Animatable a;

        @DexIgnore
        public Bi(Animatable animatable) {
            super();
            this.a = animatable;
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void c() {
            this.a.start();
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void d() {
            this.a.stop();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Kf0.Ai {
        @DexIgnore
        public Dj0<Long> K;
        @DexIgnore
        public SparseArrayCompat<Integer> L;

        @DexIgnore
        public Ci(Ci ci, Hf0 hf0, Resources resources) {
            super(ci, hf0, resources);
            if (ci != null) {
                this.K = ci.K;
                this.L = ci.L;
                return;
            }
            this.K = new Dj0<>();
            this.L = new SparseArrayCompat<>();
        }

        @DexIgnore
        public static long D(int i, int i2) {
            return (((long) i) << 32) | ((long) i2);
        }

        @DexIgnore
        public int B(int[] iArr, Drawable drawable, int i) {
            int z = super.z(iArr, drawable);
            this.L.q(z, Integer.valueOf(i));
            return z;
        }

        @DexIgnore
        public int C(int i, int i2, Drawable drawable, boolean z) {
            int a2 = super.a(drawable);
            long D = D(i, i2);
            long j = z ? 8589934592L : 0;
            long j2 = (long) a2;
            this.K.d(D, Long.valueOf(j2 | j));
            if (z) {
                this.K.d(D(i2, i), Long.valueOf(j | j2 | 4294967296L));
            }
            return a2;
        }

        @DexIgnore
        public int E(int i) {
            if (i < 0) {
                return 0;
            }
            return this.L.l(i, 0).intValue();
        }

        @DexIgnore
        public int F(int[] iArr) {
            int A = super.A(iArr);
            return A >= 0 ? A : super.A(StateSet.WILD_CARD);
        }

        @DexIgnore
        public int G(int i, int i2) {
            return (int) this.K.n(D(i, i2), -1L).longValue();
        }

        @DexIgnore
        public boolean H(int i, int i2) {
            return (this.K.n(D(i, i2), -1L).longValue() & 4294967296L) != 0;
        }

        @DexIgnore
        public boolean I(int i, int i2) {
            return (this.K.n(D(i, i2), -1L).longValue() & 8589934592L) != 0;
        }

        @DexIgnore
        @Override // com.fossil.Kf0.Ai
        public Drawable newDrawable() {
            return new Hf0(this, null);
        }

        @DexIgnore
        @Override // com.fossil.Kf0.Ai
        public Drawable newDrawable(Resources resources) {
            return new Hf0(this, resources);
        }

        @DexIgnore
        @Override // com.fossil.Kf0.Ai, com.fossil.If0.Ci
        public void r() {
            this.K = this.K.g();
            this.L = this.L.g();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di extends Gi {
        @DexIgnore
        public /* final */ Uz0 a;

        @DexIgnore
        public Di(Uz0 uz0) {
            super();
            this.a = uz0;
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void c() {
            this.a.start();
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void d() {
            this.a.stop();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends Gi {
        @DexIgnore
        public /* final */ ObjectAnimator a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ei(AnimationDrawable animationDrawable, boolean z, boolean z2) {
            super();
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            int i = z ? numberOfFrames - 1 : 0;
            int i2 = z ? 0 : numberOfFrames - 1;
            Fi fi = new Fi(animationDrawable, z);
            ObjectAnimator ofInt = ObjectAnimator.ofInt(animationDrawable, "currentIndex", i, i2);
            if (Build.VERSION.SDK_INT >= 18) {
                ofInt.setAutoCancel(true);
            }
            ofInt.setDuration((long) fi.a());
            ofInt.setInterpolator(fi);
            this.b = z2;
            this.a = ofInt;
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public boolean a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void b() {
            this.a.reverse();
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void c() {
            this.a.start();
        }

        @DexIgnore
        @Override // com.fossil.Hf0.Gi
        public void d() {
            this.a.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi implements TimeInterpolator {
        @DexIgnore
        public int[] a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public Fi(AnimationDrawable animationDrawable, boolean z) {
            b(animationDrawable, z);
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        public int b(AnimationDrawable animationDrawable, boolean z) {
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            this.b = numberOfFrames;
            int[] iArr = this.a;
            if (iArr == null || iArr.length < numberOfFrames) {
                this.a = new int[numberOfFrames];
            }
            int[] iArr2 = this.a;
            int i = 0;
            int i2 = 0;
            while (i2 < numberOfFrames) {
                int duration = animationDrawable.getDuration(z ? (numberOfFrames - i2) - 1 : i2);
                iArr2[i2] = duration;
                i2++;
                i = duration + i;
            }
            this.c = i;
            return i;
        }

        @DexIgnore
        public float getInterpolation(float f) {
            int i = (int) ((((float) this.c) * f) + 0.5f);
            int i2 = this.b;
            int[] iArr = this.a;
            int i3 = 0;
            while (i3 < i2 && i >= iArr[i3]) {
                i -= iArr[i3];
                i3++;
            }
            return (i3 < i2 ? ((float) i) / ((float) this.c) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + (((float) i3) / ((float) i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Gi {
        @DexIgnore
        public Gi() {
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public abstract void c();

        @DexIgnore
        public abstract void d();
    }

    @DexIgnore
    public Hf0() {
        this(null, null);
    }

    @DexIgnore
    public Hf0(Ci ci, Resources resources) {
        super(null);
        this.w = -1;
        this.x = -1;
        h(new Ci(ci, this, resources));
        onStateChange(getState());
        jumpToCurrentState();
    }

    @DexIgnore
    public static Hf0 m(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("animated-selector")) {
            Hf0 hf0 = new Hf0();
            hf0.n(context, resources, xmlPullParser, attributeSet, theme);
            return hf0;
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid animated-selector tag " + name);
    }

    @DexIgnore
    @Override // com.fossil.If0, com.fossil.Kf0
    public /* bridge */ /* synthetic */ If0.Ci b() {
        return l();
    }

    @DexIgnore
    @Override // com.fossil.If0, com.fossil.Kf0
    public void h(If0.Ci ci) {
        super.h(ci);
        if (ci instanceof Ci) {
            this.u = (Ci) ci;
        }
    }

    @DexIgnore
    @Override // com.fossil.Kf0
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Kf0
    public /* bridge */ /* synthetic */ Kf0.Ai j() {
        return l();
    }

    @DexIgnore
    @Override // com.fossil.If0
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        Gi gi = this.v;
        if (gi != null) {
            gi.d();
            this.v = null;
            g(this.w);
            this.w = -1;
            this.x = -1;
        }
    }

    @DexIgnore
    public Ci l() {
        return new Ci(this.u, this, null);
    }

    @DexIgnore
    @Override // com.fossil.If0, com.fossil.Kf0
    public Drawable mutate() {
        if (!this.y) {
            super.mutate();
            this.u.r();
            this.y = true;
        }
        return this;
    }

    @DexIgnore
    public void n(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        TypedArray k = Ol0.k(resources, theme, attributeSet, Mf0.AnimatedStateListDrawableCompat);
        setVisible(k.getBoolean(Mf0.AnimatedStateListDrawableCompat_android_visible, true), true);
        t(k);
        i(resources);
        k.recycle();
        o(context, resources, xmlPullParser, attributeSet, theme);
        p();
    }

    @DexIgnore
    public final void o(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth = xmlPullParser.getDepth() + 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1) {
                int depth2 = xmlPullParser.getDepth();
                if (depth2 < depth && next == 3) {
                    return;
                }
                if (next == 2 && depth2 <= depth) {
                    if (xmlPullParser.getName().equals("item")) {
                        q(context, resources, xmlPullParser, attributeSet, theme);
                    } else if (xmlPullParser.getName().equals("transition")) {
                        r(context, resources, xmlPullParser, attributeSet, theme);
                    }
                }
            } else {
                return;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.If0, com.fossil.Kf0
    public boolean onStateChange(int[] iArr) {
        int F = this.u.F(iArr);
        boolean z = F != c() && (s(F) || g(F));
        Drawable current = getCurrent();
        return current != null ? z | current.setState(iArr) : z;
    }

    @DexIgnore
    public final void p() {
        onStateChange(getState());
    }

    @DexIgnore
    public final int q(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray k = Ol0.k(resources, theme, attributeSet, Mf0.AnimatedStateListDrawableItem);
        int resourceId = k.getResourceId(Mf0.AnimatedStateListDrawableItem_android_id, 0);
        int resourceId2 = k.getResourceId(Mf0.AnimatedStateListDrawableItem_android_drawable, -1);
        Drawable j = resourceId2 > 0 ? Jh0.h().j(context, resourceId2) : null;
        k.recycle();
        int[] k2 = k(attributeSet);
        if (j == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next == 2) {
                j = xmlPullParser.getName().equals("vector") ? A01.c(resources, xmlPullParser, attributeSet, theme) : Build.VERSION.SDK_INT >= 21 ? Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme) : Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            } else {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
            }
        }
        if (j != null) {
            return this.u.B(k2, j, resourceId);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
    }

    @DexIgnore
    public final int r(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray k = Ol0.k(resources, theme, attributeSet, Mf0.AnimatedStateListDrawableTransition);
        int resourceId = k.getResourceId(Mf0.AnimatedStateListDrawableTransition_android_fromId, -1);
        int resourceId2 = k.getResourceId(Mf0.AnimatedStateListDrawableTransition_android_toId, -1);
        int resourceId3 = k.getResourceId(Mf0.AnimatedStateListDrawableTransition_android_drawable, -1);
        Drawable j = resourceId3 > 0 ? Jh0.h().j(context, resourceId3) : null;
        boolean z = k.getBoolean(Mf0.AnimatedStateListDrawableTransition_android_reversible, false);
        k.recycle();
        if (j == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next == 2) {
                j = xmlPullParser.getName().equals("animated-vector") ? Uz0.a(context, resources, xmlPullParser, attributeSet, theme) : Build.VERSION.SDK_INT >= 21 ? Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme) : Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            } else {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
            }
        }
        if (j == null) {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
        } else if (resourceId != -1 && resourceId2 != -1) {
            return this.u.C(resourceId, resourceId2, j, z);
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires 'fromId' & 'toId' attributes");
        }
    }

    @DexIgnore
    public final boolean s(int i) {
        int c;
        Gi bi;
        Gi gi = this.v;
        if (gi == null) {
            c = c();
        } else if (i == this.w) {
            return true;
        } else {
            if (i != this.x || !gi.a()) {
                int i2 = this.w;
                gi.d();
                c = i2;
            } else {
                gi.b();
                this.w = this.x;
                this.x = i;
                return true;
            }
        }
        this.v = null;
        this.x = -1;
        this.w = -1;
        Ci ci = this.u;
        int E = ci.E(c);
        int E2 = ci.E(i);
        if (!(E2 == 0 || E == 0)) {
            int G = ci.G(E, E2);
            if (G < 0) {
                return false;
            }
            boolean I = ci.I(E, E2);
            g(G);
            Drawable current = getCurrent();
            if (current instanceof AnimationDrawable) {
                bi = new Ei((AnimationDrawable) current, ci.H(E, E2), I);
            } else if (current instanceof Uz0) {
                bi = new Di((Uz0) current);
            } else if (current instanceof Animatable) {
                bi = new Bi((Animatable) current);
            }
            bi.c();
            this.v = bi;
            this.x = c;
            this.w = i;
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.If0
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (this.v != null && (visible || z2)) {
            if (z) {
                this.v.c();
            } else {
                jumpToCurrentState();
            }
        }
        return visible;
    }

    @DexIgnore
    public final void t(TypedArray typedArray) {
        Ci ci = this.u;
        if (Build.VERSION.SDK_INT >= 21) {
            ci.d |= typedArray.getChangingConfigurations();
        }
        ci.x(typedArray.getBoolean(Mf0.AnimatedStateListDrawableCompat_android_variablePadding, ci.i));
        ci.t(typedArray.getBoolean(Mf0.AnimatedStateListDrawableCompat_android_constantSize, ci.l));
        ci.u(typedArray.getInt(Mf0.AnimatedStateListDrawableCompat_android_enterFadeDuration, ci.A));
        ci.v(typedArray.getInt(Mf0.AnimatedStateListDrawableCompat_android_exitFadeDuration, ci.B));
        setDither(typedArray.getBoolean(Mf0.AnimatedStateListDrawableCompat_android_dither, ci.x));
    }
}
