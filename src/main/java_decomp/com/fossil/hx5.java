package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hx5 extends Jx5<Bi> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public /* final */ ArrayList<String> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> j; // = new ArrayList<>();
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public Ai m;
    @DexIgnore
    public AlphabetIndexer s;
    @DexIgnore
    public /* final */ List<J06> t;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ Ae5 h;
        @DexIgnore
        public /* final */ /* synthetic */ Hx5 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.c();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Hx5 hx5, Ae5 ae5) {
            super(ae5.n());
            Wg6.c(ae5, "binding");
            this.i = hx5;
            this.h = ae5;
            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            String d3 = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d2)) {
                this.h.s.setBackgroundColor(Color.parseColor(d2));
            }
            if (!TextUtils.isEmpty(d3)) {
                this.h.x.setBackgroundColor(Color.parseColor(d3));
            }
            this.h.r.setOnClickListener(new Aii(this));
            this.h.q.setOnClickListener(new Bii(this));
        }

        @DexIgnore
        public final J06 b() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.a);
            contact.setPhotoThumbUri(this.b);
            J06 j06 = new J06(contact, null, 2, null);
            if (this.f == 1) {
                j06.setHasPhoneNumber(true);
                j06.setPhoneNumber(this.c);
            } else {
                j06.setHasPhoneNumber(false);
            }
            Contact contact2 = j06.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    contact3.setUseCall(true);
                    j06.setFavorites(this.g);
                    j06.setAdded(true);
                    return j06;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final void c() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.t.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((J06) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 != -1) {
                    this.i.t.remove(i2);
                } else {
                    this.i.t.add(b());
                }
                Ai ai = this.i.m;
                if (ai != null) {
                    ai.a();
                }
                this.i.notifyItemChanged(adapterPosition);
            }
        }

        @DexIgnore
        public final void d(Cursor cursor, int i2) {
            Object obj;
            boolean z;
            boolean z2 = true;
            Wg6.c(cursor, "cursor");
            cursor.moveToPosition(i2);
            FLogger.INSTANCE.getLocal().d(Hx5.u, ".Inside renderData, cursor move position=" + i2);
            this.a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            if (this.i.l < i2) {
                this.i.l = i2;
            }
            String d2 = P47.d(this.c);
            if (this.i.j.contains(Integer.valueOf(i2)) || (this.i.l < i2 && this.i.k == this.e && this.i.i.contains(d2))) {
                if (!this.i.j.contains(Integer.valueOf(i2))) {
                    this.i.j.add(Integer.valueOf(i2));
                }
                e(8);
            } else {
                if (i2 > this.i.l) {
                    this.i.l = i2;
                }
                if (this.i.k != this.e) {
                    this.i.i.clear();
                    this.i.k = this.e;
                }
                this.i.i.add(d2);
                e(0);
                if (cursor.moveToPrevious() && cursor.getInt(cursor.getColumnIndex("contact_id")) == this.e) {
                    FlexibleCheckBox flexibleCheckBox = this.h.q;
                    Wg6.b(flexibleCheckBox, "binding.accbSelect");
                    flexibleCheckBox.setVisibility(4);
                    FlexibleTextView flexibleTextView = this.h.w;
                    Wg6.b(flexibleTextView, "binding.pickContactTitle");
                    flexibleTextView.setVisibility(8);
                }
                cursor.moveToNext();
            }
            FlexibleTextView flexibleTextView2 = this.h.w;
            Wg6.b(flexibleTextView2, "binding.pickContactTitle");
            flexibleTextView2.setText(this.a);
            if (this.f == 1) {
                FlexibleTextView flexibleTextView3 = this.h.v;
                Wg6.b(flexibleTextView3, "binding.pickContactPhone");
                flexibleTextView3.setText(this.c);
                FlexibleTextView flexibleTextView4 = this.h.v;
                Wg6.b(flexibleTextView4, "binding.pickContactPhone");
                flexibleTextView4.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView5 = this.h.v;
                Wg6.b(flexibleTextView5, "binding.pickContactPhone");
                flexibleTextView5.setVisibility(8);
            }
            Iterator it = this.i.t.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                Object next = it.next();
                Contact contact = ((J06) next).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    obj = next;
                    break;
                }
            }
            J06 j06 = (J06) obj;
            FlexibleCheckBox flexibleCheckBox2 = this.h.q;
            Wg6.b(flexibleCheckBox2, "binding.accbSelect");
            if (j06 == null) {
                z2 = false;
            }
            flexibleCheckBox2.setChecked(z2);
            FLogger.INSTANCE.getLocal().d(Hx5.u, "Inside renderData, contactId = " + this.e + ", displayName = " + this.a + ", hasPhoneNumber = " + this.f + ", phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                FlexibleTextView flexibleTextView6 = this.h.t;
                Wg6.b(flexibleTextView6, "binding.ftvAlphabet");
                flexibleTextView6.setVisibility(0);
                FlexibleTextView flexibleTextView7 = this.h.t;
                Wg6.b(flexibleTextView7, "binding.ftvAlphabet");
                flexibleTextView7.setText(Character.toString(Jl5.b.j(this.d)));
                return;
            }
            FlexibleTextView flexibleTextView8 = this.h.t;
            Wg6.b(flexibleTextView8, "binding.ftvAlphabet");
            flexibleTextView8.setVisibility(8);
        }

        @DexIgnore
        public final void e(int i2) {
            FlexibleCheckBox flexibleCheckBox = this.h.q;
            Wg6.b(flexibleCheckBox, "binding.accbSelect");
            flexibleCheckBox.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            Wg6.b(constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView = this.h.t;
            Wg6.b(flexibleTextView, "binding.ftvAlphabet");
            flexibleTextView.setVisibility(i2);
            LinearLayout linearLayout = this.h.u;
            Wg6.b(linearLayout, "binding.llTextContainer");
            linearLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView2 = this.h.v;
            Wg6.b(flexibleTextView2, "binding.pickContactPhone");
            flexibleTextView2.setVisibility(i2);
            FlexibleTextView flexibleTextView3 = this.h.w;
            Wg6.b(flexibleTextView3, "binding.pickContactTitle");
            flexibleTextView3.setVisibility(i2);
            View view = this.h.x;
            Wg6.b(view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.s;
            Wg6.b(constraintLayout2, "binding.clRoot");
            constraintLayout2.setVisibility(i2);
        }
    }

    /*
    static {
        String simpleName = Hx5.class.getSimpleName();
        Wg6.b(simpleName, "CursorContactSearchAdapter::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Hx5(Cursor cursor, List<J06> list) {
        super(cursor);
        Wg6.c(list, "mContactWrapperList");
        this.t = list;
    }

    @DexIgnore
    public int getPositionForSection(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.s;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i2);
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.s;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i2);
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.s;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, android.database.Cursor, int] */
    @Override // com.fossil.Jx5
    public /* bridge */ /* synthetic */ void i(Bi bi, Cursor cursor, int i2) {
        w(bi, cursor, i2);
    }

    @DexIgnore
    @Override // com.fossil.Jx5
    public Cursor l(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        AlphabetIndexer alphabetIndexer = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRTSUVWXYZ");
        this.s = alphabetIndexer;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.i.clear();
            this.j.clear();
            this.k = -1;
            this.l = -1;
            return super.l(cursor);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        return x(viewGroup, i2);
    }

    @DexIgnore
    public final void v(String str) {
        Wg6.c(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    public void w(Bi bi, Cursor cursor, int i2) {
        Wg6.c(bi, "holder");
        if (cursor != null) {
            bi.d(cursor, i2);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Bi x(ViewGroup viewGroup, int i2) {
        Wg6.c(viewGroup, "parent");
        Ae5 z = Ae5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemContactBinding.infla\u2026.context), parent, false)");
        return new Bi(this, z);
    }

    @DexIgnore
    public final void y(Ai ai) {
        Wg6.c(ai, "itemClickListener");
        this.m = ai;
    }
}
