package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P73 implements Xw2<S73> {
    @DexIgnore
    public static P73 c; // = new P73();
    @DexIgnore
    public /* final */ Xw2<S73> b;

    @DexIgnore
    public P73() {
        this(Ww2.b(new R73()));
    }

    @DexIgnore
    public P73(Xw2<S73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((S73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((S73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((S73) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((S73) c.zza()).zzd();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ S73 zza() {
        return this.b.zza();
    }
}
