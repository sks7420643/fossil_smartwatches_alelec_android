package com.fossil;

import com.fossil.Ar1;
import com.mapped.Cd6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gd extends Qq7 implements Coroutine<byte[], N6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ E60 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Gd(E60 e60) {
        super(2);
        this.b = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(byte[] bArr, N6 n6) {
        Nn1 nn1;
        E60 e60;
        Ar1.Ai ai;
        byte[] bArr2 = bArr;
        N6 n62 = n6;
        Nn1[] values = Nn1.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                nn1 = null;
                break;
            }
            nn1 = values[i];
            if (nn1.a() == n62) {
                break;
            }
            i++;
        }
        if (!(nn1 == null || (ai = (e60 = this.b).s) == null)) {
            ai.a(e60, bArr2, nn1);
        }
        return Cd6.a;
    }
}
