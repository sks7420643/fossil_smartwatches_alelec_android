package com.fossil;

import com.fossil.Jh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gh4 extends Jh4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Kh4 d;
    @DexIgnore
    public /* final */ Jh4.Bi e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Jh4.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Kh4 d;
        @DexIgnore
        public Jh4.Bi e;

        @DexIgnore
        @Override // com.fossil.Jh4.Ai
        public Jh4 a() {
            return new Gh4(this.a, this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        @Override // com.fossil.Jh4.Ai
        public Jh4.Ai b(Kh4 kh4) {
            this.d = kh4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Jh4.Ai
        public Jh4.Ai c(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Jh4.Ai
        public Jh4.Ai d(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Jh4.Ai
        public Jh4.Ai e(Jh4.Bi bi) {
            this.e = bi;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Jh4.Ai
        public Jh4.Ai f(String str) {
            this.a = str;
            return this;
        }
    }

    @DexIgnore
    public Gh4(String str, String str2, String str3, Kh4 kh4, Jh4.Bi bi) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = kh4;
        this.e = bi;
    }

    @DexIgnore
    @Override // com.fossil.Jh4
    public Kh4 b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Jh4
    public String c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Jh4
    public String d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Jh4
    public Jh4.Bi e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Jh4)) {
            return false;
        }
        Jh4 jh4 = (Jh4) obj;
        String str = this.a;
        if (str != null ? str.equals(jh4.f()) : jh4.f() == null) {
            String str2 = this.b;
            if (str2 != null ? str2.equals(jh4.c()) : jh4.c() == null) {
                String str3 = this.c;
                if (str3 != null ? str3.equals(jh4.d()) : jh4.d() == null) {
                    Kh4 kh4 = this.d;
                    if (kh4 != null ? kh4.equals(jh4.b()) : jh4.b() == null) {
                        Jh4.Bi bi = this.e;
                        if (bi == null) {
                            if (jh4.e() == null) {
                                return true;
                            }
                        } else if (bi.equals(jh4.e())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Jh4
    public String f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str == null ? 0 : str.hashCode();
        String str2 = this.b;
        int hashCode2 = str2 == null ? 0 : str2.hashCode();
        String str3 = this.c;
        int hashCode3 = str3 == null ? 0 : str3.hashCode();
        Kh4 kh4 = this.d;
        int hashCode4 = kh4 == null ? 0 : kh4.hashCode();
        Jh4.Bi bi = this.e;
        if (bi != null) {
            i = bi.hashCode();
        }
        return ((((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "InstallationResponse{uri=" + this.a + ", fid=" + this.b + ", refreshToken=" + this.c + ", authToken=" + this.d + ", responseCode=" + this.e + "}";
    }
}
