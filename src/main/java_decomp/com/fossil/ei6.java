package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ei6 implements Factory<GoalTrackingOverviewWeekPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewWeekPresenter a(Bi6 bi6, UserRepository userRepository, An4 an4, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewWeekPresenter(bi6, userRepository, an4, goalTrackingRepository);
    }
}
