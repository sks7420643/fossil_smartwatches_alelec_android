package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class W14<K, V> extends R14<K, V> implements W44<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 7431625294878419160L;

    @DexIgnore
    public W14(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    @Override // com.fossil.R14
    public abstract Set<V> createCollection();

    @DexIgnore
    @Override // com.fossil.R14
    public Set<V> createUnmodifiableEmptyCollection() {
        return H34.of();
    }

    @DexIgnore
    @Override // com.fossil.R14, com.fossil.U14, com.fossil.Y34
    public Set<Map.Entry<K, V>> entries() {
        return (Set) super.entries();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.R14, com.fossil.Y34
    public Set<V> get(K k) {
        return (Set) super.get((W14<K, V>) k);
    }

    @DexIgnore
    @Override // com.fossil.R14, com.fossil.U14, com.fossil.Y34
    @CanIgnoreReturnValue
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    @Override // com.fossil.R14
    @CanIgnoreReturnValue
    public Set<V> removeAll(Object obj) {
        return (Set) super.removeAll(obj);
    }

    @DexIgnore
    @Override // com.fossil.R14, com.fossil.U14
    @CanIgnoreReturnValue
    public Set<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (Set) super.replaceValues((W14<K, V>) k, (Iterable) iterable);
    }
}
