package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Je2 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public Je2(String str, String str2, boolean z, int i, boolean z2) {
        this.b = str;
        this.a = str2;
        this.c = i;
        this.d = z2;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final boolean b() {
        return this.d;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }
}
