package com.fossil;

import android.content.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mg7 extends Ng7 {
    @DexIgnore
    public String m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Thread o; // = null;

    @DexIgnore
    public Mg7(Context context, int i, int i2, Throwable th, Jg7 jg7) {
        super(context, i, jg7);
        i(i2, th);
    }

    @DexIgnore
    public Mg7(Context context, int i, int i2, Throwable th, Thread thread, Jg7 jg7) {
        super(context, i, jg7);
        i(i2, th);
        this.o = thread;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public Og7 a() {
        return Og7.c;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public boolean b(JSONObject jSONObject) {
        Ji7.d(jSONObject, "er", this.m);
        jSONObject.put("ea", this.n);
        int i = this.n;
        if (i != 2 && i != 3) {
            return true;
        }
        new Vh7(this.j).b(jSONObject, this.o);
        return true;
    }

    @DexIgnore
    public final void i(int i, Throwable th) {
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            this.m = stringWriter.toString();
            this.n = i;
            printWriter.close();
        }
    }
}
