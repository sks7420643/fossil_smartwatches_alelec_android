package com.fossil;

import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qo4 implements Factory<Po4> {
    @DexIgnore
    public /* final */ Provider<Map<Class<? extends Ts0>, Provider<Ts0>>> a;

    @DexIgnore
    public Qo4(Provider<Map<Class<? extends Ts0>, Provider<Ts0>>> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Qo4 a(Provider<Map<Class<? extends Ts0>, Provider<Ts0>>> provider) {
        return new Qo4(provider);
    }

    @DexIgnore
    public static Po4 c(Map<Class<? extends Ts0>, Provider<Ts0>> map) {
        return new Po4(map);
    }

    @DexIgnore
    public Po4 b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
