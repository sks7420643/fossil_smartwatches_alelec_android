package com.fossil;

import com.mapped.Qg6;
import com.mapped.Rc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rm {
    @DexIgnore
    public /* synthetic */ Rm(Qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(Rl1[] rl1Arr, short s, Ry1 ry1) {
        Object[] array = Em7.D(rl1Arr).toArray(new Rl1[0]);
        if (array != null) {
            Rl1[] rl1Arr2 = (Rl1[]) array;
            if (rl1Arr2.length == 0) {
                return new byte[0];
            }
            try {
                return Xa.d.a(s, ry1, rl1Arr2);
            } catch (Sx1 e) {
                return new byte[0];
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
