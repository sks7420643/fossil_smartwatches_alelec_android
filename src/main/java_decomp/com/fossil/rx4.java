package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rx4 implements Factory<BCCurrentChallengeSubTabViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Rx4(Provider<Tt4> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Rx4 a(Provider<Tt4> provider, Provider<An4> provider2) {
        return new Rx4(provider, provider2);
    }

    @DexIgnore
    public static BCCurrentChallengeSubTabViewModel c(Tt4 tt4, An4 an4) {
        return new BCCurrentChallengeSubTabViewModel(tt4, an4);
    }

    @DexIgnore
    public BCCurrentChallengeSubTabViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
