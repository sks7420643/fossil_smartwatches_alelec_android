package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Aw extends Tv {
    @DexIgnore
    public N6 L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Aw(long j, long j2, long j3, short s, K5 k5, int i, int i2) {
        super(Sv.d, s, Hs.O, k5, (i2 & 32) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.L = N6.d;
        this.M = true;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.G0, this.L.b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        N6 n6 = N6.k;
        this.L = n6;
        return G80.k(jSONObject, Jd0.G0, n6.b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        Wg6.b(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.Tv, com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(G80.k(super.z(), Jd0.c1, Long.valueOf(this.N)), Jd0.d1, Long.valueOf(this.O)), Jd0.e1, Long.valueOf(this.P));
    }
}
