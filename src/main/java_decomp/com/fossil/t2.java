package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T2 implements Parcelable.Creator<U2> {
    @DexIgnore
    public /* synthetic */ T2(Qg6 qg6) {
    }

    @DexIgnore
    public U2 a(Parcel parcel) {
        return new U2(parcel, (Qg6) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public U2 createFromParcel(Parcel parcel) {
        return new U2(parcel, (Qg6) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public U2[] newArray(int i) {
        return new U2[i];
    }
}
