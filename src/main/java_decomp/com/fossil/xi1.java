package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xi1 {
    @DexIgnore
    public /* final */ List<Ai<?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ Rb1<T> b;

        @DexIgnore
        public Ai(Class<T> cls, Rb1<T> rb1) {
            this.a = cls;
            this.b = rb1;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public <Z> void a(Class<Z> cls, Rb1<Z> rb1) {
        synchronized (this) {
            this.a.add(new Ai<>(cls, rb1));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v6. Raw type applied. Possible types: com.fossil.Rb1<T>, com.fossil.Rb1<Z> */
    public <Z> Rb1<Z> b(Class<Z> cls) {
        synchronized (this) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                Ai<?> ai = this.a.get(i);
                if (ai.a(cls)) {
                    return (Rb1<T>) ai.b;
                }
            }
            return null;
        }
    }
}
