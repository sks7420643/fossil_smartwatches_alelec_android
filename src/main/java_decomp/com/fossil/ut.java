package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ut {
    f(St.d, Tt.c, null, null, 12),
    g(St.d, Tt.d, null, null, 12),
    h(St.d, Tt.e, null, null, 12),
    i(St.c, Tt.f, null, null, 12),
    j(St.d, Tt.g, null, null, 12);
    
    @DexIgnore
    public /* final */ St b;
    @DexIgnore
    public /* final */ Tt c;
    @DexIgnore
    public /* final */ St d;
    @DexIgnore
    public /* final */ Tt e;

    @DexIgnore
    public /* synthetic */ Ut(St st, Tt tt, St st2, Tt tt2, int i2) {
        st2 = (i2 & 4) != 0 ? St.e : st2;
        tt2 = (i2 & 8) != 0 ? tt : tt2;
        this.b = st;
        this.c = tt;
        this.d = st2;
        this.e = tt2;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(2).put(this.b.b).put(this.c.b).array();
        Wg6.b(array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        return array;
    }
}
