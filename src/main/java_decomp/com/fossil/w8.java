package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class W8 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Fm1.values().length];
        a = iArr;
        iArr[Fm1.WEATHER.ordinal()] = 1;
        a[Fm1.HEART_RATE.ordinal()] = 2;
        a[Fm1.STEPS.ordinal()] = 3;
        a[Fm1.DATE.ordinal()] = 4;
        a[Fm1.CHANCE_OF_RAIN.ordinal()] = 5;
        a[Fm1.SECOND_TIMEZONE.ordinal()] = 6;
        a[Fm1.ACTIVE_MINUTES.ordinal()] = 7;
        a[Fm1.CALORIES.ordinal()] = 8;
        a[Fm1.BATTERY.ordinal()] = 9;
        a[Fm1.EMPTY.ordinal()] = 10;
    }
    */
}
