package com.fossil;

import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F56 {
    @DexIgnore
    public /* final */ D56 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<String> c;

    @DexIgnore
    public F56(D56 d56, int i, ArrayList<String> arrayList) {
        Wg6.c(d56, "mView");
        this.a = d56;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        ArrayList<String> arrayList = this.c;
        return arrayList == null ? new ArrayList<>() : arrayList;
    }

    @DexIgnore
    public final D56 c() {
        return this.a;
    }
}
