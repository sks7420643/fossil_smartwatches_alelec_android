package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Xt {
    c(new byte[]{1}),
    d(new byte[]{35, 0, 1, 0, 0, 0});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Xt(byte[] bArr) {
        this.b = bArr;
    }
}
