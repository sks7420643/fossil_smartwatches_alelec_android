package com.fossil;

import com.fossil.Su0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kv0 {
    @DexIgnore
    public /* final */ Ai a;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Su0.Bi bi);

        @DexIgnore
        Su0.Bi b(int i, int i2, int i3, Object obj);
    }

    @DexIgnore
    public Kv0(Ai ai) {
        this.a = ai;
    }

    @DexIgnore
    public final int a(List<Su0.Bi> list) {
        boolean z;
        int size = list.size() - 1;
        boolean z2 = false;
        while (size >= 0) {
            if (list.get(size).a != 8) {
                z = true;
            } else if (z2) {
                return size;
            } else {
                z = z2;
            }
            size--;
            z2 = z;
        }
        return -1;
    }

    @DexIgnore
    public void b(List<Su0.Bi> list) {
        while (true) {
            int a2 = a(list);
            if (a2 != -1) {
                d(list, a2, a2 + 1);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void c(List<Su0.Bi> list, int i, Su0.Bi bi, int i2, Su0.Bi bi2) {
        int i3 = bi.d < bi2.b ? -1 : 0;
        if (bi.b < bi2.b) {
            i3++;
        }
        int i4 = bi2.b;
        int i5 = bi.b;
        if (i4 <= i5) {
            bi.b = bi2.d + i5;
        }
        int i6 = bi2.b;
        int i7 = bi.d;
        if (i6 <= i7) {
            bi.d = bi2.d + i7;
        }
        bi2.b = i3 + bi2.b;
        list.set(i, bi2);
        list.set(i2, bi);
    }

    @DexIgnore
    public final void d(List<Su0.Bi> list, int i, int i2) {
        Su0.Bi bi = list.get(i);
        Su0.Bi bi2 = list.get(i2);
        int i3 = bi2.a;
        if (i3 == 1) {
            c(list, i, bi, i2, bi2);
        } else if (i3 == 2) {
            e(list, i, bi, i2, bi2);
        } else if (i3 == 4) {
            f(list, i, bi, i2, bi2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e(java.util.List<com.fossil.Su0.Bi> r10, int r11, com.fossil.Su0.Bi r12, int r13, com.fossil.Su0.Bi r14) {
        /*
        // Method dump skipped, instructions count: 248
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kv0.e(java.util.List, int, com.fossil.Su0$Bi, int, com.fossil.Su0$Bi):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0013  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f(java.util.List<com.fossil.Su0.Bi> r8, int r9, com.fossil.Su0.Bi r10, int r11, com.fossil.Su0.Bi r12) {
        /*
            r7 = this;
            r1 = 0
            r6 = 4
            int r0 = r10.d
            int r2 = r12.b
            if (r0 >= r2) goto L_0x002c
            int r0 = r2 + -1
            r12.b = r0
        L_0x000c:
            r0 = r1
        L_0x000d:
            int r2 = r10.b
            int r3 = r12.b
            if (r2 > r3) goto L_0x0041
            int r2 = r3 + 1
            r12.b = r2
        L_0x0017:
            r8.set(r11, r10)
            int r2 = r12.d
            if (r2 <= 0) goto L_0x005b
            r8.set(r9, r12)
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r8.add(r9, r0)
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r8.add(r9, r1)
        L_0x002b:
            return
        L_0x002c:
            int r3 = r12.d
            int r2 = r2 + r3
            if (r0 >= r2) goto L_0x000c
            int r0 = r3 + -1
            r12.d = r0
            com.fossil.Kv0$Ai r0 = r7.a
            int r2 = r10.b
            r3 = 1
            java.lang.Object r4 = r12.c
            com.fossil.Su0$Bi r0 = r0.b(r6, r2, r3, r4)
            goto L_0x000d
        L_0x0041:
            int r4 = r12.d
            int r5 = r3 + r4
            if (r2 >= r5) goto L_0x0017
            int r1 = r3 + r4
            int r3 = r1 - r2
            com.fossil.Kv0$Ai r1 = r7.a
            int r2 = r2 + 1
            java.lang.Object r4 = r12.c
            com.fossil.Su0$Bi r1 = r1.b(r6, r2, r3, r4)
            int r2 = r12.d
            int r2 = r2 - r3
            r12.d = r2
            goto L_0x0017
        L_0x005b:
            r8.remove(r9)
            com.fossil.Kv0$Ai r2 = r7.a
            r2.a(r12)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kv0.f(java.util.List, int, com.fossil.Su0$Bi, int, com.fossil.Su0$Bi):void");
    }
}
