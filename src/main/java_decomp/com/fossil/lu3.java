package com.fossil;

import com.mapped.Lc3;
import com.mapped.Mc3;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lu3<TResult> extends Nt3<TResult> {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Iu3<TResult> b; // = new Iu3<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public TResult e;
    @DexIgnore
    public Exception f;

    @DexIgnore
    public final void A() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    @DexIgnore
    public final void B() {
        synchronized (this.a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> a(Executor executor, Gt3 gt3) {
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Wt3(executor, gt3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> b(Ht3<TResult> ht3) {
        c(Pt3.a, ht3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> c(Executor executor, Ht3<TResult> ht3) {
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Au3(executor, ht3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> d(Lc3 lc3) {
        e(Pt3.a, lc3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> e(Executor executor, Lc3 lc3) {
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Bu3(executor, lc3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> f(Mc3<? super TResult> mc3) {
        g(Pt3.a, mc3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Nt3<TResult> g(Executor executor, Mc3<? super TResult> mc3) {
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Eu3(executor, mc3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <TContinuationResult> Nt3<TContinuationResult> h(Ft3<TResult, TContinuationResult> ft3) {
        return i(Pt3.a, ft3);
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <TContinuationResult> Nt3<TContinuationResult> i(Executor executor, Ft3<TResult, TContinuationResult> ft3) {
        Lu3 lu3 = new Lu3();
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Tt3(executor, ft3, lu3));
        B();
        return lu3;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <TContinuationResult> Nt3<TContinuationResult> j(Ft3<TResult, Nt3<TContinuationResult>> ft3) {
        return k(Pt3.a, ft3);
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <TContinuationResult> Nt3<TContinuationResult> k(Executor executor, Ft3<TResult, Nt3<TContinuationResult>> ft3) {
        Lu3 lu3 = new Lu3();
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Ut3(executor, ft3, lu3));
        B();
        return lu3;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final Exception l() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final TResult m() {
        TResult tresult;
        synchronized (this.a) {
            w();
            A();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new Lt3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <X extends Throwable> TResult n(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            w();
            A();
            if (cls.isInstance(this.f)) {
                throw cls.cast(this.f);
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new Lt3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final boolean o() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final boolean p() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final boolean q() {
        boolean z;
        synchronized (this.a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <TContinuationResult> Nt3<TContinuationResult> r(Mt3<TResult, TContinuationResult> mt3) {
        return s(Pt3.a, mt3);
    }

    @DexIgnore
    @Override // com.fossil.Nt3
    public final <TContinuationResult> Nt3<TContinuationResult> s(Executor executor, Mt3<TResult, TContinuationResult> mt3) {
        Lu3 lu3 = new Lu3();
        Iu3<TResult> iu3 = this.b;
        Nu3.a(executor);
        iu3.b(new Fu3(executor, mt3, lu3));
        B();
        return lu3;
    }

    @DexIgnore
    public final void t(Exception exc) {
        Rc2.l(exc, "Exception must not be null");
        synchronized (this.a) {
            z();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final void u(TResult tresult) {
        synchronized (this.a) {
            z();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final boolean v() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void w() {
        Rc2.o(this.c, "Task is not yet complete");
    }

    @DexIgnore
    public final boolean x(Exception exc) {
        Rc2.l(exc, "Exception must not be null");
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final boolean y(TResult tresult) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void z() {
        Rc2.o(!this.c, "Task is already complete");
    }
}
