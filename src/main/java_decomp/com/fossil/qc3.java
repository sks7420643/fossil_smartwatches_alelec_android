package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qc3 extends Ds2 implements Pc3 {
    @DexIgnore
    public Qc3() {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.Ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        Zb3 od3;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            od3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IGoogleMapDelegate");
            od3 = queryLocalInterface instanceof Zb3 ? (Zb3) queryLocalInterface : new Od3(readStrongBinder);
        }
        l2(od3);
        parcel2.writeNoException();
        return true;
    }
}
