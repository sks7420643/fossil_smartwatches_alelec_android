package com.fossil;

import com.mapped.Cd6;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L4 {
    @DexIgnore
    public /* final */ LinkedList<U5> a; // = new LinkedList<>();
    @DexIgnore
    public U5 b;
    @DexIgnore
    public /* final */ K5 c;

    @DexIgnore
    public L4(K5 k5) {
        this.c = k5;
    }

    @DexIgnore
    public final void a(R5 r5) {
        LinkedList linkedList = new LinkedList(this.a);
        this.a.clear();
        U5 u5 = this.b;
        if (u5 != null) {
            u5.e(r5);
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            ((U5) it.next()).e(r5);
        }
    }

    @DexIgnore
    public final void b(U5 u5) {
        U5 poll;
        Ky1 ky1 = Ky1.DEBUG;
        Ey1.a(u5.i);
        try {
            synchronized (this.a) {
                this.a.add(d(u5), u5);
                Cd6 cd6 = Cd6.a;
            }
        } catch (IndexOutOfBoundsException e) {
            this.a.add(u5);
        }
        if (this.b == null && (poll = this.a.poll()) != null) {
            c(poll);
        }
    }

    @DexIgnore
    public final void c(U5 u5) {
        this.b = u5;
        if (u5 != null) {
            u5.h = new H4(this);
            K5 k5 = this.c;
            if (!u5.d) {
                Ky1 ky1 = Ky1.DEBUG;
                Fd0<H7> j = u5.j();
                if (j != null) {
                    j.b(u5);
                }
                u5.d(k5);
            }
        }
    }

    @DexIgnore
    public final int d(U5 u5) {
        synchronized (this.a) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                if (u5.h().compareTo(this.a.get(i).h()) > 0) {
                    return i;
                }
            }
            return this.a.size();
        }
    }
}
