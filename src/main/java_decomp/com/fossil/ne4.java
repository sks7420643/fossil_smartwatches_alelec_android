package com.fossil;

import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Ne4 implements Ft3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public Ne4(Context context, Intent intent) {
        this.a = context;
        this.b = intent;
    }

    @DexIgnore
    @Override // com.fossil.Ft3
    public final Object then(Nt3 nt3) {
        return Qe4.g(this.a, this.b, nt3);
    }
}
