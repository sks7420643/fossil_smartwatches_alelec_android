package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.wearables.fsl.utils.TimeUtils;
import com.mapped.An4;
import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Io5 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        MFLogger.d("TimeTickReceiver", "onReceive - action=" + action);
        if (!TextUtils.isEmpty(action) && Wg6.a(action, "android.intent.action.TIME_TICK")) {
            Calendar instance = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
            Wg6.b(instance, "calendar");
            String format = simpleDateFormat.format(instance.getTime());
            An4 an4 = new An4(context);
            String U = an4.U();
            MFLogger.d("TimeTickReceiver", "onReceive - day= " + format + ", widgetsDateChanged= " + U);
            if (!Vt7.k(U, format, false, 2, null) && context != null) {
                MFLogger.d("TimeTickReceiver", "onReceive - need to resetAllContentWidgetsUI");
                an4.X1(format);
            }
        }
    }
}
