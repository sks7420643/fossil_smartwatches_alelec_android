package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;

    @DexIgnore
    public T45(Object obj, View view, int i, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleTextView;
    }
}
