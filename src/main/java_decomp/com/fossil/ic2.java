package com.fossil;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.M62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ic2 {
    @DexIgnore
    public /* final */ SparseIntArray a; // = new SparseIntArray();
    @DexIgnore
    public D62 b;

    @DexIgnore
    public Ic2(D62 d62) {
        Rc2.k(d62);
        this.b = d62;
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }

    @DexIgnore
    public int b(Context context, M62.Fi fi) {
        int i;
        Rc2.k(context);
        Rc2.k(fi);
        if (!fi.r()) {
            return 0;
        }
        int s = fi.s();
        int i2 = this.a.get(s, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.a.size()) {
                i = i2;
                break;
            }
            int keyAt = this.a.keyAt(i3);
            if (keyAt > s && this.a.get(keyAt) == 0) {
                i = 0;
                break;
            }
            i3++;
        }
        if (i == -1) {
            i = this.b.j(context, s);
        }
        this.a.put(s, i);
        return i;
    }
}
