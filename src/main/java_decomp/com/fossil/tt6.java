package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tt6 implements Factory<UserCustomizeThemeViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Tt6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Tt6 a(Provider<ThemeRepository> provider) {
        return new Tt6(provider);
    }

    @DexIgnore
    public static UserCustomizeThemeViewModel c(ThemeRepository themeRepository) {
        return new UserCustomizeThemeViewModel(themeRepository);
    }

    @DexIgnore
    public UserCustomizeThemeViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
