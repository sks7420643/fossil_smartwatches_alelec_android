package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import com.facebook.appevents.internal.InAppPurchaseEventManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.Jn5;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.lang.reflect.Method;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ln5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static Ln5 b;
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ln5 a() {
            Ln5 b;
            synchronized (this) {
                if (Ln5.c.b() == null) {
                    Ln5.c.c(new Ln5(null));
                }
                b = Ln5.c.b();
                if (b == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return b;
        }

        @DexIgnore
        public final Ln5 b() {
            return Ln5.b;
        }

        @DexIgnore
        public final void c(Ln5 ln5) {
            Ln5.b = ln5;
        }
    }

    /*
    static {
        String simpleName = Ln5.class.getSimpleName();
        Wg6.b(simpleName, "PhoneCallManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public Ln5() {
    }

    @DexIgnore
    public /* synthetic */ Ln5(Qg6 qg6) {
        this();
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    @TargetApi(26)
    public final void a() {
        try {
            Object systemService = PortfolioApp.get.instance().getSystemService("telecom");
            if (systemService != null) {
                ((TelecomManager) systemService).acceptRingingCall();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.telecom.TelecomManager");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when accept call call " + e);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission", "PrivateApi"})
    public final void d() {
        if (Build.VERSION.SDK_INT >= 28) {
            try {
                Object systemService = PortfolioApp.get.instance().getSystemService("telecom");
                if (systemService != null) {
                    ((TelecomManager) systemService).endCall();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.telecom.TelecomManager");
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e("reject >= P", e.getStackTrace().toString());
            }
        }
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony");
            Wg6.b(cls, "Class.forName(\"com.andro\u2026al.telephony.ITelephony\")");
            Class<?> cls2 = cls.getClasses()[0];
            Class<?> cls3 = Class.forName("android.os.ServiceManager");
            Wg6.b(cls3, "Class.forName(\"android.os.ServiceManager\")");
            Class<?> cls4 = Class.forName("android.os.ServiceManagerNative");
            Wg6.b(cls4, "Class.forName(\"android.os.ServiceManagerNative\")");
            Method method = cls3.getMethod("getService", String.class);
            Wg6.b(method, "serviceManagerClass.getM\u2026ice\", String::class.java)");
            Method method2 = cls4.getMethod(InAppPurchaseEventManager.AS_INTERFACE, IBinder.class);
            Wg6.b(method2, "serviceManagerNativeClas\u2026ce\", IBinder::class.java)");
            Binder binder = new Binder();
            binder.attachInterface(null, "fake");
            Object invoke = method.invoke(method2.invoke(null, binder), PlaceFields.PHONE);
            if (invoke != null) {
                Method method3 = cls2.getMethod(InAppPurchaseEventManager.AS_INTERFACE, IBinder.class);
                Wg6.b(method3, "telephonyStubClass.getMe\u2026ce\", IBinder::class.java)");
                Object invoke2 = method3.invoke(null, (IBinder) invoke);
                Method method4 = cls.getMethod("endCall", new Class[0]);
                Wg6.b(method4, "telephonyClass.getMethod(\"endCall\")");
                method4.invoke(invoke2, new Object[0]);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.os.IBinder");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when decline call " + e2);
        }
    }

    @DexIgnore
    public final void e(String str, String str2, BroadcastReceiver broadcastReceiver) {
        Wg6.c(str, "number");
        Wg6.c(str2, "message");
        Wg6.c(broadcastReceiver, "receiver");
        Hr7 hr7 = Hr7.a;
        String format = String.format("DELIVERY_%s", Arrays.copyOf(new Object[]{PortfolioApp.get.instance().Q()}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a;
        local.d(str3, "sendSMSTo with " + format);
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.get.instance(), 0, new Intent(format), 0);
        PortfolioApp.get.instance().registerReceiver(broadcastReceiver, new IntentFilter(format));
        if (Jn5.b.m(PortfolioApp.get.instance().getBaseContext(), Jn5.Ci.SEND_SMS)) {
            SmsManager smsManager = SmsManager.getDefault();
            if (smsManager != null) {
                smsManager.sendTextMessage(str, null, str2, broadcast, null);
                return;
            }
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("SEND_SMS_PERMISSION_REQUIRED", true);
        broadcastReceiver.onReceive(PortfolioApp.get.instance().getBaseContext(), intent);
    }
}
