package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class He3 extends Me3 {
    @DexIgnore
    public He3() {
        super(1, null);
    }

    @DexIgnore
    @Override // com.fossil.Me3
    public final String toString() {
        return "[Dot]";
    }
}
