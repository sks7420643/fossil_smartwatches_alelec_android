package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Hv1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Hv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hv1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            parcel.setDataPosition(0);
            if (Wg6.a(readString, Em1.class.getCanonicalName())) {
                return Em1.CREATOR.a(parcel);
            }
            if (Wg6.a(readString, Zo1.class.getCanonicalName())) {
                return Zo1.CREATOR.a(parcel);
            }
            if (Wg6.a(readString, Sl1.class.getCanonicalName())) {
                return Sl1.CREATOR.b(parcel);
            }
            throw new IllegalArgumentException("Invalid parcel!");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hv1[] newArray(int i) {
            return new Hv1[i];
        }
    }

    @DexIgnore
    public Hv1() {
        Hd0.y.d();
    }

    @DexIgnore
    public Hv1(Parcel parcel) {
        Hd0.y.d();
        parcel.readString();
    }

    @DexIgnore
    public abstract JSONObject a();

    @DexIgnore
    public final void a(Ry1 ry1) {
    }

    @DexIgnore
    public final JSONObject b() {
        try {
            JSONObject put = new JSONObject().put("push", new JSONObject().put("set", a()));
            Wg6.b(put, "JSONObject().put(UIScrip\u2026ET, getAssignmentJSON()))");
            return put;
        } catch (JSONException e) {
            D90.i.i(e);
            return new JSONObject();
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getCanonicalName());
        }
    }
}
