package com.fossil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dx4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<R25> g;
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public /* final */ List<Fragment> i; // = new ArrayList();
    @DexIgnore
    public int j;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Dx4.m;
        }

        @DexIgnore
        public final Dx4 b() {
            return new Dx4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ R25 a;
        @DexIgnore
        public /* final */ /* synthetic */ Dx4 b;

        @DexIgnore
        public Bi(R25 r25, Dx4 dx4, Yw4 yw4) {
            this.a = r25;
            this.b = dx4;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            super.c(i);
            this.b.j = i;
            this.a.q.setCurrentPage(i);
            this.b.k = i != 0 ? i != 1 ? i != 2 ? "" : "bc_challenge_create" : "bc_challenge_list" : "bc_challenge_tab";
            AnalyticsHelper g = AnalyticsHelper.f.g();
            String N6 = this.b.N6();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                g.m(N6, activity);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
        }
    }

    /*
    static {
        String simpleName = Dx4.class.getSimpleName();
        Wg6.b(simpleName, "BCChallengeTabFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public final String N6() {
        return this.k;
    }

    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public final void O6(CustomPageIndicator customPageIndicator, int i2) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        hashMap.put(0, 2131230943);
        hashMap.put(Integer.valueOf(this.i.size() - 1), 2131230817);
        customPageIndicator.i(hashMap);
        customPageIndicator.setNumberOfPage(i2);
    }

    @DexIgnore
    public final void P6() {
        FLogger.INSTANCE.getLocal().e(m, "Inside .initTabs");
        Fragment Z = getChildFragmentManager().Z(Ox4.s.a());
        Fragment Z2 = getChildFragmentManager().Z(Ux4.s.a());
        Fragment Z3 = getChildFragmentManager().Z(Ix4.l.a());
        if (Z == null) {
            Z = Ox4.s.b();
        }
        if (Z2 == null) {
            Z2 = Ux4.s.b();
        }
        if (Z3 == null) {
            Z3 = Ix4.l.b();
        }
        this.i.clear();
        this.i.add(Z);
        this.i.add(Z2);
        this.i.add(Z3);
        Yw4 yw4 = new Yw4(this.i, this);
        G37<R25> g37 = this.g;
        if (g37 != null) {
            R25 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.s;
                Wg6.b(viewPager2, "this");
                viewPager2.setAdapter(yw4);
                viewPager2.setOffscreenPageLimit(this.i.size());
                a2.s.g(new Bi(a2, this, yw4));
                CustomPageIndicator customPageIndicator = a2.q;
                Wg6.b(customPageIndicator, "indicator");
                O6(customPageIndicator, this.i.size());
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "scroll to position=" + i2 + " fragment " + this.i.get(i2));
        G37<R25> g37 = this.g;
        if (g37 != null) {
            R25 a2 = g37.a();
            if (!(a2 == null || (viewPager2 = a2.s) == null)) {
                viewPager2.j(i2, true);
            }
            this.k = "bc_challenge_tab";
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        T t;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 11) {
            if (i2 == 13 && i3 == -1) {
                Q6(0);
                int intExtra = intent != null ? intent.getIntExtra("index_extra", -1) : -1;
                Iterator<T> it = this.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (next instanceof Ux4) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 instanceof Ux4)) {
                    t2 = null;
                }
                Ux4 ux4 = (Ux4) t2;
                if (ux4 != null) {
                    ux4.U6(intExtra);
                }
            }
        } else if (i3 == -1) {
            Q6(0);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().k1().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(Fx4.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            Fx4 fx4 = (Fx4) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        R25 r25 = (R25) Aq0.f(layoutInflater, 2131558505, viewGroup, false, A6());
        this.g = new G37<>(this, r25);
        Wg6.b(r25, "binding");
        return r25.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        String str = this.k;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m(str, activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        P6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
