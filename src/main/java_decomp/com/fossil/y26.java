package com.fossil;

import android.text.SpannableString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Y26 extends Gq4<X26> {
    @DexIgnore
    void S1(boolean z);

    @DexIgnore
    void Y(boolean z);

    @DexIgnore
    void Y5(SpannableString spannableString);

    @DexIgnore
    void b6(String str);

    @DexIgnore
    Object close();  // void declaration

    @DexIgnore
    void n4(boolean z);

    @DexIgnore
    void s1(boolean z);

    @DexIgnore
    void t5(SpannableString spannableString);

    @DexIgnore
    void w4(boolean z);
}
