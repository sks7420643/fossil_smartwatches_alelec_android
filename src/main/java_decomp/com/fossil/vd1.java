package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vd1 implements Nd1<int[]> {
    @DexIgnore
    @Override // com.fossil.Nd1
    public int a() {
        return 4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Nd1
    public /* bridge */ /* synthetic */ int b(int[] iArr) {
        return c(iArr);
    }

    @DexIgnore
    public int c(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public int[] d(int i) {
        return new int[i];
    }

    @DexIgnore
    @Override // com.fossil.Nd1
    public String getTag() {
        return "IntegerArrayPool";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Nd1
    public /* bridge */ /* synthetic */ int[] newArray(int i) {
        return d(i);
    }
}
