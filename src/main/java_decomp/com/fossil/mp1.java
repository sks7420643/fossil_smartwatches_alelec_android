package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mp1 extends Ox1 implements Parcelable {
    @DexIgnore
    public /* final */ E90 b;
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Mp1(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0017
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            com.mapped.E90 r0 = com.mapped.E90.valueOf(r0)
            byte r1 = r3.readByte()
            r2.<init>(r0, r1)
            return
        L_0x0017:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Mp1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Mp1(E90 e90, byte b2) {
        this.b = e90;
        this.c = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Mp1 mp1 = (Mp1) obj;
            if (this.b != mp1.b) {
                return false;
            }
            return this.c == mp1.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.DeviceEvent");
    }

    @DexIgnore
    public final E90 getDeviceEventId() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.O2, Ey1.a(this.b)), Jd0.z1, Short.valueOf(Hy1.p(this.c)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
    }
}
