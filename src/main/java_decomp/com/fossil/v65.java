package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerViewCalendar q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public V65(Object obj, View view, int i, RecyclerViewCalendar recyclerViewCalendar, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = recyclerViewCalendar;
        this.r = constraintLayout;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = flexibleTextView3;
        this.v = flexibleTextView4;
    }
}
