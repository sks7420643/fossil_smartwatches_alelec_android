package com.fossil;

import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I87 implements Factory<WatchFaceComplicationViewModel> {
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> a;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> b;

    @DexIgnore
    public I87(Provider<ComplicationRepository> provider, Provider<DianaAppSettingRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static I87 a(Provider<ComplicationRepository> provider, Provider<DianaAppSettingRepository> provider2) {
        return new I87(provider, provider2);
    }

    @DexIgnore
    public static WatchFaceComplicationViewModel c(ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        return new WatchFaceComplicationViewModel(complicationRepository, dianaAppSettingRepository);
    }

    @DexIgnore
    public WatchFaceComplicationViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
