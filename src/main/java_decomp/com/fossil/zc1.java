package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zc1 {
    @DexIgnore
    void b(Yc1<?> yc1, Mb1 mb1, Cd1<?> cd1);

    @DexIgnore
    void c(Yc1<?> yc1, Mb1 mb1);
}
