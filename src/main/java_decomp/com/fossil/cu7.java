package com.fossil;

import com.mapped.Af6;
import com.mapped.Wg6;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cu7<T> extends Au7<T> {
    @DexIgnore
    public /* final */ Thread e;
    @DexIgnore
    public /* final */ Hw7 f;

    @DexIgnore
    public Cu7(Af6 af6, Thread thread, Hw7 hw7) {
        super(af6, true);
        this.e = thread;
        this.f = hw7;
    }

    @DexIgnore
    public final T A0() {
        Vu7 vu7 = null;
        Xx7 a2 = Yx7.a();
        if (a2 != null) {
            a2.c();
        }
        try {
            Hw7 hw7 = this.f;
            if (hw7 != null) {
                Hw7.i0(hw7, false, 1, null);
            }
            while (!Thread.interrupted()) {
                try {
                    Hw7 hw72 = this.f;
                    long q0 = hw72 != null ? hw72.q0() : Long.MAX_VALUE;
                    if (U()) {
                        T t = (T) Gx7.h(Q());
                        if (t instanceof Vu7) {
                            vu7 = t;
                        }
                        Vu7 vu72 = vu7;
                        if (vu72 == null) {
                            return t;
                        }
                        throw vu72.a;
                    }
                    Xx7 a3 = Yx7.a();
                    if (a3 != null) {
                        a3.f(this, q0);
                    } else {
                        LockSupport.parkNanos(this, q0);
                    }
                } finally {
                    Hw7 hw73 = this.f;
                    if (hw73 != null) {
                        Hw7.T(hw73, false, 1, null);
                    }
                }
            }
            InterruptedException interruptedException = new InterruptedException();
            s(interruptedException);
            throw interruptedException;
        } finally {
            Xx7 a4 = Yx7.a();
            if (a4 != null) {
                a4.g();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public boolean V() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public void p(Object obj) {
        if (!Wg6.a(Thread.currentThread(), this.e)) {
            LockSupport.unpark(this.e);
        }
    }
}
