package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;

public final class Dl7<T> implements Serializable {
    public static final Ai Companion = new Ai(null);
    public final Object value;

    public static final class Ai {
        public Ai() {
        }

        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    public static final class Bi implements Serializable {
        public final Throwable exception;

        public Bi(Throwable th) {
            Wg6.c(th, "exception");
            this.exception = th;
        }

        public boolean equals(Object obj) {
            return (obj instanceof Bi) && Wg6.a(this.exception, ((Bi) obj).exception);
        }

        public int hashCode() {
            return this.exception.hashCode();
        }

        public String toString() {
            return "Failure(" + this.exception + ')';
        }
    }

    public /* synthetic */ Dl7(Object obj) {
        this.value = obj;
    }

    public static final /* synthetic */ Dl7 box-impl(Object obj) {
        return new Dl7(obj);
    }

    public static Object constructor-impl(Object obj) {
        return obj;
    }

    public static boolean equals-impl(Object obj, Object obj2) {
        return (obj2 instanceof Dl7) && Wg6.a(obj, ((Dl7) obj2).unbox-impl());
    }

    public static final boolean equals-impl0(Object obj, Object obj2) {
        return Wg6.a(obj, obj2);
    }

    public static final Throwable exceptionOrNull-impl(Object obj) {
        if (obj instanceof Bi) {
            return ((Bi) obj).exception;
        }
        return null;
    }

    public static int hashCode-impl(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public static final boolean isFailure-impl(Object obj) {
        return obj instanceof Bi;
    }

    public static final boolean isSuccess-impl(Object obj) {
        return !(obj instanceof Bi);
    }

    public static String toString-impl(Object obj) {
        if (obj instanceof Bi) {
            return obj.toString();
        }
        return "Success(" + obj + ')';
    }

    public static /* synthetic */ void value$annotations() {
    }

    public boolean equals(Object obj) {
        return equals-impl(this.value, obj);
    }

    public int hashCode() {
        return hashCode-impl(this.value);
    }

    public String toString() {
        return toString-impl(this.value);
    }

    public final /* synthetic */ Object unbox-impl() {
        return this.value;
    }
}
