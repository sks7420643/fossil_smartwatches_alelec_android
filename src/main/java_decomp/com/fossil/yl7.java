package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yl7<T> implements Iterator<T>, Jr7 {
    @DexIgnore
    public Fn7 b; // = Fn7.NotReady;
    @DexIgnore
    public T c;

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b() {
        this.b = Fn7.Done;
    }

    @DexIgnore
    public final void c(T t) {
        this.c = t;
        this.b = Fn7.Ready;
    }

    @DexIgnore
    public final boolean e() {
        this.b = Fn7.Failed;
        a();
        return this.b == Fn7.Ready;
    }

    @DexIgnore
    public boolean hasNext() {
        if (this.b != Fn7.Failed) {
            int i = Xl7.a[this.b.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i != 2) {
                return e();
            }
            return true;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            this.b = Fn7.NotReady;
            return this.c;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
