package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R02 extends W02 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ T32 b;
    @DexIgnore
    public /* final */ T32 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public R02(Context context, T32 t32, T32 t322, String str) {
        if (context != null) {
            this.a = context;
            if (t32 != null) {
                this.b = t32;
                if (t322 != null) {
                    this.c = t322;
                    if (str != null) {
                        this.d = str;
                        return;
                    }
                    throw new NullPointerException("Null backendName");
                }
                throw new NullPointerException("Null monotonicClock");
            }
            throw new NullPointerException("Null wallClock");
        }
        throw new NullPointerException("Null applicationContext");
    }

    @DexIgnore
    @Override // com.fossil.W02
    public Context b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.W02
    public String c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.W02
    public T32 d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.W02
    public T32 e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof W02)) {
            return false;
        }
        W02 w02 = (W02) obj;
        return this.a.equals(w02.b()) && this.b.equals(w02.e()) && this.c.equals(w02.d()) && this.d.equals(w02.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CreationContext{applicationContext=" + this.a + ", wallClock=" + this.b + ", monotonicClock=" + this.c + ", backendName=" + this.d + "}";
    }
}
