package com.fossil;

import com.mapped.Bv6;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class K68 {
    @DexIgnore
    public static /* final */ Object a; // = "y";
    @DexIgnore
    public static /* final */ Object b; // = "M";
    @DexIgnore
    public static /* final */ Object c; // = "d";
    @DexIgnore
    public static /* final */ Object d; // = "H";
    @DexIgnore
    public static /* final */ Object e; // = "m";
    @DexIgnore
    public static /* final */ Object f; // = "s";
    @DexIgnore
    public static /* final */ Object g; // = DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public int b; // = 1;

        @DexIgnore
        public Ai(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static boolean a(Ai[] aiArr, Object obj) {
            for (Ai ai : aiArr) {
                if (ai.c() == obj) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }

        @DexIgnore
        public Object c() {
            return this.a;
        }

        @DexIgnore
        public void d() {
            this.b++;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            if (this.a.getClass() != ai.a.getClass() || this.b != ai.b) {
                return false;
            }
            Object obj2 = this.a;
            return obj2 instanceof StringBuilder ? obj2.toString().equals(ai.a.toString()) : obj2 instanceof Number ? obj2.equals(ai.a) : obj2 == ai.a;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return Bv6.f(this.a.toString(), this.b);
        }
    }

    @DexIgnore
    public static String a(Ai[] aiArr, long j, long j2, long j3, long j4, long j5, long j6, long j7, boolean z) {
        StringBuilder sb = new StringBuilder();
        boolean z2 = false;
        for (Ai ai : aiArr) {
            Object c2 = ai.c();
            int b2 = ai.b();
            if (c2 instanceof StringBuilder) {
                sb.append(c2.toString());
            } else {
                if (c2.equals(a)) {
                    sb.append(f(j, z, b2));
                } else if (c2.equals(b)) {
                    sb.append(f(j2, z, b2));
                } else if (c2.equals(c)) {
                    sb.append(f(j3, z, b2));
                } else if (c2.equals(d)) {
                    sb.append(f(j4, z, b2));
                } else if (c2.equals(e)) {
                    sb.append(f(j5, z, b2));
                } else if (c2.equals(f)) {
                    sb.append(f(j6, z, b2));
                    z2 = true;
                } else if (c2.equals(g)) {
                    if (z2) {
                        int i = 3;
                        if (z) {
                            i = Math.max(3, b2);
                        }
                        sb.append(f(j7, true, i));
                    } else {
                        sb.append(f(j7, z, b2));
                    }
                    z2 = false;
                }
                z2 = false;
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static String b(long j, String str) {
        return c(j, str, true);
    }

    @DexIgnore
    public static String c(long j, String str, boolean z) {
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        J68.a(0, Long.MAX_VALUE, j, "durationMillis must not be negative");
        Ai[] e2 = e(str);
        if (Ai.a(e2, c)) {
            j2 = j / LogBuilder.MAX_INTERVAL;
            j -= LogBuilder.MAX_INTERVAL * j2;
        } else {
            j2 = 0;
        }
        if (Ai.a(e2, d)) {
            j3 = j / 3600000;
            j -= 3600000 * j3;
        } else {
            j3 = 0;
        }
        if (Ai.a(e2, e)) {
            j4 = j / 60000;
            j -= 60000 * j4;
        } else {
            j4 = 0;
        }
        if (Ai.a(e2, f)) {
            j5 = j / 1000;
            j6 = j - (1000 * j5);
        } else {
            j5 = 0;
            j6 = j;
        }
        return a(e2, 0, 0, j2, j3, j4, j5, j6, z);
    }

    @DexIgnore
    public static String d(long j) {
        return b(j, "HH:mm:ss.SSS");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0022 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.K68.Ai[] e(java.lang.String r10) {
        /*
            r9 = 39
            r4 = 0
            r2 = 0
            java.util.ArrayList r7 = new java.util.ArrayList
            int r0 = r10.length()
            r7.<init>(r0)
            r3 = r4
            r5 = r4
            r0 = r2
            r1 = r2
        L_0x0011:
            int r6 = r10.length()
            if (r5 >= r6) goto L_0x009c
            char r6 = r10.charAt(r5)
            if (r3 == 0) goto L_0x0025
            if (r6 == r9) goto L_0x0025
            r1.append(r6)
        L_0x0022:
            int r5 = r5 + 1
            goto L_0x0011
        L_0x0025:
            if (r6 == r9) goto L_0x007e
            r8 = 72
            if (r6 == r8) goto L_0x007b
            r8 = 77
            if (r6 == r8) goto L_0x0078
            r8 = 83
            if (r6 == r8) goto L_0x0075
            r8 = 100
            if (r6 == r8) goto L_0x0072
            r8 = 109(0x6d, float:1.53E-43)
            if (r6 == r8) goto L_0x006f
            r8 = 115(0x73, float:1.61E-43)
            if (r6 == r8) goto L_0x006c
            r8 = 121(0x79, float:1.7E-43)
            if (r6 == r8) goto L_0x0069
            if (r1 != 0) goto L_0x0052
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.fossil.K68$Ai r8 = new com.fossil.K68$Ai
            r8.<init>(r1)
            r7.add(r8)
        L_0x0052:
            r1.append(r6)
        L_0x0055:
            r6 = r2
        L_0x0056:
            if (r6 == 0) goto L_0x0022
            if (r0 == 0) goto L_0x0093
            java.lang.Object r1 = r0.c()
            boolean r1 = r1.equals(r6)
            if (r1 == 0) goto L_0x0093
            r0.d()
        L_0x0067:
            r1 = r2
            goto L_0x0022
        L_0x0069:
            java.lang.Object r6 = com.fossil.K68.a
            goto L_0x0056
        L_0x006c:
            java.lang.Object r6 = com.fossil.K68.f
            goto L_0x0056
        L_0x006f:
            java.lang.Object r6 = com.fossil.K68.e
            goto L_0x0056
        L_0x0072:
            java.lang.Object r6 = com.fossil.K68.c
            goto L_0x0056
        L_0x0075:
            java.lang.Object r6 = com.fossil.K68.g
            goto L_0x0056
        L_0x0078:
            java.lang.Object r6 = com.fossil.K68.b
            goto L_0x0056
        L_0x007b:
            java.lang.Object r6 = com.fossil.K68.d
            goto L_0x0056
        L_0x007e:
            if (r3 == 0) goto L_0x0084
            r1 = r2
            r3 = r4
            r6 = r2
            goto L_0x0056
        L_0x0084:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.fossil.K68$Ai r3 = new com.fossil.K68$Ai
            r3.<init>(r1)
            r7.add(r3)
            r3 = 1
            goto L_0x0055
        L_0x0093:
            com.fossil.K68$Ai r0 = new com.fossil.K68$Ai
            r0.<init>(r6)
            r7.add(r0)
            goto L_0x0067
        L_0x009c:
            if (r3 != 0) goto L_0x00ab
            int r0 = r7.size()
            com.fossil.K68$Ai[] r0 = new com.fossil.K68.Ai[r0]
            java.lang.Object[] r0 = r7.toArray(r0)
            com.fossil.K68$Ai[] r0 = (com.fossil.K68.Ai[]) r0
            return r0
        L_0x00ab:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unmatched quote in format: "
            r0.append(r1)
            r0.append(r10)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.K68.e(java.lang.String):com.fossil.K68$Ai[]");
    }

    @DexIgnore
    public static String f(long j, boolean z, int i) {
        String l = Long.toString(j);
        return z ? Bv6.c(l, i, '0') : l;
    }
}
