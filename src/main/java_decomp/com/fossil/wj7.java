package com.fossil;

import android.app.Activity;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Wj7 extends Activity implements Dk7 {
    @DexIgnore
    public Ck7<Object> b;

    @DexIgnore
    @Override // com.fossil.Dk7
    public Vj7<Object> c() {
        return this.b;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        Uj7.a(this);
        super.onCreate(bundle);
    }
}
