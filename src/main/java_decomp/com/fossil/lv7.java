package com.fossil;

import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Kc6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Lv7 {
    DEFAULT,
    LAZY,
    ATOMIC,
    UNDISPATCHED;

    @DexIgnore
    public static /* synthetic */ void isLazy$annotations() {
    }

    @DexIgnore
    public final <R, T> void invoke(Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine, R r, Xe6<? super T> xe6) {
        int i = Kv7.b[ordinal()];
        if (i == 1) {
            D08.c(coroutine, r, xe6);
        } else if (i == 2) {
            Sn7.b(coroutine, r, xe6);
        } else if (i == 3) {
            E08.b(coroutine, r, xe6);
        } else if (i != 4) {
            throw new Kc6();
        }
    }

    @DexIgnore
    public final <T> void invoke(Hg6<? super Xe6<? super T>, ? extends Object> hg6, Xe6<? super T> xe6) {
        int i = Kv7.a[ordinal()];
        if (i == 1) {
            D08.b(hg6, xe6);
        } else if (i == 2) {
            Sn7.a(hg6, xe6);
        } else if (i == 3) {
            E08.a(hg6, xe6);
        } else if (i != 4) {
            throw new Kc6();
        }
    }

    @DexIgnore
    public final boolean isLazy() {
        return this == LAZY;
    }
}
