package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H52 extends Vk2 implements G52 {
    @DexIgnore
    public H52(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    @DexIgnore
    @Override // com.fossil.G52
    public final void M0(E52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel d = d();
        Xk2.b(d, e52);
        Xk2.c(d, googleSignInOptions);
        e(102, d);
    }

    @DexIgnore
    @Override // com.fossil.G52
    public final void d1(E52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel d = d();
        Xk2.b(d, e52);
        Xk2.c(d, googleSignInOptions);
        e(103, d);
    }
}
