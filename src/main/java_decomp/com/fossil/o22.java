package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O22 implements Factory<Integer> {
    @DexIgnore
    public static /* final */ O22 a; // = new O22();

    @DexIgnore
    public static O22 a() {
        return a;
    }

    @DexIgnore
    public static int c() {
        return M22.b();
    }

    @DexIgnore
    public Integer b() {
        return Integer.valueOf(c());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
