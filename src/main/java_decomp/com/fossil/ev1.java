package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ev1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Io1[] b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ev1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ev1 createFromParcel(Parcel parcel) {
            Object[] createTypedArray = parcel.createTypedArray(Io1.CREATOR);
            if (createTypedArray != null) {
                Wg6.b(createTypedArray, "parcel.createTypedArray(\u2026tificationReplyMessage)!!");
                return new Ev1((Io1[]) createTypedArray);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ev1[] newArray(int i) {
            return new Ev1[i];
        }
    }

    @DexIgnore
    public Ev1(Io1[] io1Arr) {
        Object[] copyOf = Arrays.copyOf(io1Arr, io1Arr.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : copyOf) {
            if (hashSet.add(Byte.valueOf(((Io1) obj).getMessageId()))) {
                arrayList.add(obj);
            }
        }
        Object[] array = Pm7.b0(arrayList.subList(0, Math.min(10, arrayList.size())), new Sb0()).toArray(new Io1[0]);
        if (array != null) {
            this.b = (Io1[]) array;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (Io1 io1 : this.b) {
                byteArrayOutputStream.write(io1.b());
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            Wg6.b(byteArray, "entriesData.toByteArray()");
            this.c = byteArray;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final byte[] a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ev1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((Ev1) obj).b);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageGroup");
    }

    @DexIgnore
    public final Io1[] getReplyMessages() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return Bm7.a(this.b);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(new JSONObject(), Jd0.n, Px1.a(this.b));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.b, i);
    }
}
