package com.fossil;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ja7;
import com.fossil.S87;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.WatchFaceFontStorage;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ea7 extends BaseFragment {
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public Dh5 h;
    @DexIgnore
    public Ia7 i;
    @DexIgnore
    public /* final */ Map<O87, Integer> j; // = Zm7.j(Hl7.a(O87.BLACK, 2131362976), Hl7.a(O87.WHITE, 2131362979), Hl7.a(O87.LIGHT_GRAY, 2131362978), Hl7.a(O87.DARK_GRAY, 2131362977));
    @DexIgnore
    public Typeface k; // = WatchFaceFontStorage.d.d();
    @DexIgnore
    public O87 l; // = O87.Companion.a();
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<Typeface, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Ea7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ea7 ea7) {
            super(1);
            this.this$0 = ea7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Typeface typeface) {
            invoke(typeface);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(Typeface typeface) {
            Wg6.c(typeface, "it");
            this.this$0.k = typeface;
            Gc7 e = Hc7.c.e(this.this$0);
            if (e != null) {
                e.D(typeface);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Ea7 a;

        @DexIgnore
        public Bi(Ea7 ea7) {
            this.a = ea7;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            T t2 = t;
            T t3 = !(t2 instanceof S87.Ci) ? null : t2;
            if (t3 != null) {
                this.a.k = t3.j();
                this.a.l = t3.g();
                Ea7 ea7 = this.a;
                ea7.S6(ea7.k, this.a.l);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements RadioGroup.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ea7 a;

        @DexIgnore
        public Ci(Ea7 ea7) {
            this.a = ea7;
        }

        @DexIgnore
        public final void onCheckedChanged(RadioGroup radioGroup, int i) {
            O87 o87;
            Ea7 ea7 = this.a;
            switch (i) {
                case 2131362976:
                    o87 = O87.BLACK;
                    break;
                case 2131362977:
                    o87 = O87.DARK_GRAY;
                    break;
                case 2131362978:
                    o87 = O87.LIGHT_GRAY;
                    break;
                case 2131362979:
                    o87 = O87.WHITE;
                    break;
                default:
                    return;
            }
            ea7.l = o87;
            Gc7 e = Hc7.c.e(this.a);
            if (e != null) {
                e.C(this.a.l);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ea7 b;

        @DexIgnore
        public Di(Ea7 ea7) {
            this.b = ea7;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.U6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Ja7.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ea7 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(Ea7 ea7) {
            this.a = ea7;
        }

        @DexIgnore
        @Override // com.fossil.Ja7.Bi
        public void a(String str) {
            Wg6.c(str, "text");
            S87.Ci ci = new S87.Ci(str, this.a.k, 60.0f, this.a.l, null, 0, 0, 112, null);
            Gc7 e = Hc7.c.e(this.a);
            if (e != null) {
                e.b(ci);
            }
        }

        @DexIgnore
        @Override // com.fossil.Ja7.Bi
        public void onCancel() {
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceTextFragment";
    }

    @DexIgnore
    public final void Q6() {
        Ia7 ia7 = new Ia7(new Ai(this));
        this.i = ia7;
        if (ia7 != null) {
            ia7.m(WatchFaceFontStorage.d.f());
            Dh5 dh5 = this.h;
            if (dh5 != null) {
                RecyclerView recyclerView = dh5.x;
                Wg6.b(recyclerView, "binding.rvTypefacePreview");
                Ia7 ia72 = this.i;
                if (ia72 != null) {
                    recyclerView.setAdapter(ia72);
                } else {
                    Wg6.n("adapter");
                    throw null;
                }
            } else {
                Wg6.n("binding");
                throw null;
            }
        } else {
            Wg6.n("adapter");
            throw null;
        }
    }

    @DexIgnore
    public final void R6() {
        LiveData<S87> i2;
        Gc7 e = Hc7.c.e(this);
        if (e != null && (i2 = e.i()) != null) {
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Wg6.b(viewLifecycleOwner, "viewLifecycleOwner");
            i2.h(viewLifecycleOwner, new Bi(this));
        }
    }

    @DexIgnore
    public final void S6(Typeface typeface, O87 o87) {
        Integer num = this.j.get(o87);
        if (num != null) {
            int intValue = num.intValue();
            Dh5 dh5 = this.h;
            if (dh5 != null) {
                dh5.v.check(intValue);
            } else {
                Wg6.n("binding");
                throw null;
            }
        }
        if (typeface != null) {
            Ia7 ia7 = this.i;
            if (ia7 != null) {
                int k2 = ia7.k(typeface);
                if (k2 != -1) {
                    Dh5 dh52 = this.h;
                    if (dh52 != null) {
                        dh52.x.smoothScrollToPosition(k2);
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                }
            } else {
                Wg6.n("adapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void T6() {
        Dh5 dh5 = this.h;
        if (dh5 != null) {
            dh5.v.setOnCheckedChangeListener(new Ci(this));
            Dh5 dh52 = this.h;
            if (dh52 != null) {
                dh52.q.setOnClickListener(new Di(this));
            } else {
                Wg6.n("binding");
                throw null;
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void U6() {
        if (getChildFragmentManager().Z(Ja7.h.a()) == null) {
            Ja7 b = Ja7.h.b(null, new Ei(this));
            if (isActive()) {
                b.show(getChildFragmentManager(), Ja7.h.a());
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().a1().a(this);
        Ts0 a2 = Vs0.c(this).a(Ga7.class);
        Wg6.b(a2, "ViewModelProviders.of(th\u2026extViewModel::class.java)");
        Ga7 ga7 = (Ga7) a2;
        String d = ThemeManager.l.a().d("nonBrandSurface");
        if (!TextUtils.isEmpty(d)) {
            Dh5 dh5 = this.h;
            if (dh5 != null) {
                dh5.w.setBackgroundColor(Color.parseColor(d));
            } else {
                Wg6.n("binding");
                throw null;
            }
        }
        Q6();
        R6();
        T6();
        S6(this.k, this.l);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Dh5 z = Dh5.z(layoutInflater, viewGroup, false);
        Wg6.b(z, "WatchFaceTextFragmentBin\u2026flater, container, false)");
        this.h = z;
        if (z != null) {
            View n = z.n();
            Wg6.b(n, "binding.root");
            return n;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
