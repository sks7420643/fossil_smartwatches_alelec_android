package com.fossil;

import com.mapped.Q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ox implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ E60 b;
    @DexIgnore
    public /* final */ /* synthetic */ Q40.Ci c;
    @DexIgnore
    public /* final */ /* synthetic */ Q40.Ci d;

    @DexIgnore
    public Ox(E60 e60, Q40.Ci ci, Q40.Ci ci2) {
        this.b = e60;
        this.c = ci;
        this.d = ci2;
    }

    @DexIgnore
    public final void run() {
        E60 e60 = this.b;
        Q40.Bi bi = e60.w;
        if (bi != null) {
            bi.onDeviceStateChanged(e60, this.c, this.d);
        }
    }
}
