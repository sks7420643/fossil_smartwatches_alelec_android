package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.mapped.Wg6;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya8 {
    @DexIgnore
    public static /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MethodChannel.Result a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result b;

        @DexIgnore
        public Ai(MethodChannel.Result result) {
            this.b = result;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.b;
            if (result != null) {
                result.notImplemented();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public Bi(MethodChannel.Result result, Object obj) {
            this.b = result;
            this.c = obj;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.b;
            if (result != null) {
                result.success(this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        public Ci(MethodChannel.Result result, String str, String str2, Object obj) {
            this.b = result;
            this.c = str;
            this.d = str2;
            this.e = obj;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.b;
            if (result != null) {
                result.error(this.c, this.d, this.e);
            }
        }
    }

    @DexIgnore
    public Ya8(MethodChannel.Result result) {
        this.a = result;
    }

    @DexIgnore
    public static /* synthetic */ void e(Ya8 ya8, String str, String str2, Object obj, int i, Object obj2) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            obj = null;
        }
        ya8.d(str, str2, obj);
    }

    @DexIgnore
    public final MethodChannel.Result a() {
        return this.a;
    }

    @DexIgnore
    public final void b() {
        MethodChannel.Result result = this.a;
        this.a = null;
        b.post(new Ai(result));
    }

    @DexIgnore
    public final void c(Object obj) {
        MethodChannel.Result result = this.a;
        this.a = null;
        b.post(new Bi(result, obj));
    }

    @DexIgnore
    public final void d(String str, String str2, Object obj) {
        Wg6.c(str, "code");
        MethodChannel.Result result = this.a;
        this.a = null;
        b.post(new Ci(result, str, str2, obj));
    }
}
