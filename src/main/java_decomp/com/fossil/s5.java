package com.fossil;

import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S5 extends Ox1 {
    @DexIgnore
    public static /* final */ O5 e; // = new O5(null);
    @DexIgnore
    public /* final */ V5 b;
    @DexIgnore
    public /* final */ R5 c;
    @DexIgnore
    public /* final */ G7 d;

    @DexIgnore
    public S5(V5 v5, R5 r5, G7 g7) {
        this.b = v5;
        this.c = r5;
        this.d = g7;
    }

    @DexIgnore
    public /* synthetic */ S5(V5 v5, R5 r5, G7 g7, int i) {
        v5 = (i & 1) != 0 ? V5.o : v5;
        g7 = (i & 4) != 0 ? new G7(F7.b, 0, 2) : g7;
        this.b = v5;
        this.c = r5;
        this.d = g7;
    }

    @DexIgnore
    public static /* synthetic */ S5 a(S5 s5, V5 v5, R5 r5, G7 g7, int i) {
        if ((i & 1) != 0) {
            v5 = s5.b;
        }
        if ((i & 2) != 0) {
            r5 = s5.c;
        }
        if ((i & 4) != 0) {
            g7 = s5.d;
        }
        return s5.a(v5, r5, g7);
    }

    @DexIgnore
    public final S5 a(V5 v5, R5 r5, G7 g7) {
        return new S5(v5, r5, g7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof S5) {
                S5 s5 = (S5) obj;
                if (!Wg6.a(this.b, s5.b) || !Wg6.a(this.c, s5.c) || !Wg6.a(this.d, s5.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        V5 v5 = this.b;
        int hashCode = v5 != null ? v5.hashCode() : 0;
        R5 r5 = this.c;
        int hashCode2 = r5 != null ? r5.hashCode() : 0;
        G7 g7 = this.d;
        if (g7 != null) {
            i = g7.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(G80.k(jSONObject, Jd0.E2, Ey1.a(this.b)), Jd0.O0, Ey1.a(this.c));
            if (this.d.b != F7.b) {
                G80.k(jSONObject, Jd0.F2, this.d.toJSONObject());
            }
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e2 = E.e("Result(commandId=");
        e2.append(this.b);
        e2.append(", resultCode=");
        e2.append(this.c);
        e2.append(", gattResult=");
        e2.append(this.d);
        e2.append(")");
        return e2.toString();
    }
}
