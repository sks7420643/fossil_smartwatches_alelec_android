package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.M62;
import com.fossil.M62.Bi;
import com.fossil.Z62;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class I72<R extends Z62, A extends M62.Bi> extends BasePendingResult<R> implements J72<R> {
    @DexIgnore
    public /* final */ M62.Ci<A> q;
    @DexIgnore
    public /* final */ M62<?> r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public I72(M62<?> m62, R62 r62) {
        super(r62);
        Rc2.l(r62, "GoogleApiClient must not be null");
        Rc2.l(m62, "Api must not be null");
        this.q = (M62.Ci<A>) m62.a();
        this.r = m62;
    }

    @DexIgnore
    public final void A(Status status) {
        Rc2.b(!status.D(), "Failed result must not be success");
        R f = f(status);
        j(f);
        x(f);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.I72<R extends com.fossil.Z62, A extends com.fossil.M62$Bi> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.J72
    public /* bridge */ /* synthetic */ void a(Object obj) {
        super.j((Z62) obj);
    }

    @DexIgnore
    public abstract void u(A a2) throws RemoteException;

    @DexIgnore
    public final M62<?> v() {
        return this.r;
    }

    @DexIgnore
    public final M62.Ci<A> w() {
        return this.q;
    }

    @DexIgnore
    public void x(R r2) {
    }

    @DexIgnore
    public final void y(A a2) throws DeadObjectException {
        if (a2 instanceof Wc2) {
            a2 = ((Wc2) a2).t0();
        }
        try {
            u(a2);
        } catch (DeadObjectException e) {
            z(e);
            throw e;
        } catch (RemoteException e2) {
            z(e2);
        }
    }

    @DexIgnore
    public final void z(RemoteException remoteException) {
        A(new Status(8, remoteException.getLocalizedMessage(), null));
    }
}
