package com.fossil;

import android.net.NetworkInfo;
import com.fossil.Xd7;
import com.squareup.picasso.Picasso;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Od7 extends ThreadPoolExecutor {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends FutureTask<Xc7> implements Comparable<Ai> {
        @DexIgnore
        public /* final */ Xc7 b;

        @DexIgnore
        public Ai(Xc7 xc7) {
            super(xc7, null);
            this.b = xc7;
        }

        @DexIgnore
        public int a(Ai ai) {
            Picasso.e p = this.b.p();
            Picasso.e p2 = ai.b.p();
            return p == p2 ? this.b.b - ai.b.b : p2.ordinal() - p.ordinal();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // java.lang.Comparable
        public /* bridge */ /* synthetic */ int compareTo(Ai ai) {
            return a(ai);
        }
    }

    @DexIgnore
    public Od7() {
        super(3, 3, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new Xd7.Fi());
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void a(NetworkInfo networkInfo) {
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            b(3);
            return;
        }
        int type = networkInfo.getType();
        if (type == 0) {
            int subtype = networkInfo.getSubtype();
            switch (subtype) {
                case 1:
                case 2:
                    b(1);
                    return;
                case 3:
                case 4:
                case 5:
                case 6:
                    break;
                default:
                    switch (subtype) {
                        case 12:
                            break;
                        case 13:
                        case 14:
                        case 15:
                            b(3);
                            return;
                        default:
                            b(3);
                            return;
                    }
            }
            b(2);
        } else if (type == 1 || type == 6 || type == 9) {
            b(4);
        } else {
            b(3);
        }
    }

    @DexIgnore
    public final void b(int i) {
        setCorePoolSize(i);
        setMaximumPoolSize(i);
    }

    @DexIgnore
    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    public Future<?> submit(Runnable runnable) {
        Ai ai = new Ai((Xc7) runnable);
        execute(ai);
        return ai;
    }
}
