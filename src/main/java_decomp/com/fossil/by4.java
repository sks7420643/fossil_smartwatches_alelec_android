package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.fossil.Gz4;
import com.mapped.AlertDialogFragment;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class By4 extends BaseFragment implements Oy5, Gz4.Ai, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<H35> h;
    @DexIgnore
    public BCFriendTabViewModel i;
    @DexIgnore
    public Ay4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return By4.l;
        }

        @DexIgnore
        public final By4 b() {
            return new By4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Bi(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            H35 h35 = (H35) By4.K6(this.a).a();
            if (!(h35 == null || (flexibleTextView = h35.r) == null)) {
                flexibleTextView.setVisibility(8);
            }
            By4.N6(this.a).w();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Gz4 {
        @DexIgnore
        public /* final */ /* synthetic */ H35 p;
        @DexIgnore
        public /* final */ /* synthetic */ By4 q;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(H35 h35, RecyclerView recyclerView, By4 by4) {
            super(recyclerView);
            this.p = h35;
            this.q = by4;
        }

        @DexIgnore
        @Override // com.fossil.Gz4
        public void N(RecyclerView.ViewHolder viewHolder, List<Gz4.Ci> list) {
            Integer num = null;
            Wg6.c(viewHolder, "viewHolder");
            Wg6.c(list, "revealedButtons");
            int adapterPosition = viewHolder.getAdapterPosition();
            if (adapterPosition != -1) {
                Object h = By4.L6(this.q).h(adapterPosition);
                if (!(h instanceof Xs4)) {
                    h = null;
                }
                Xs4 xs4 = (Xs4) h;
                FLogger.INSTANCE.getLocal().e(By4.m.a(), "friend: " + xs4);
                if (xs4 != null) {
                    num = Integer.valueOf(xs4.c());
                }
                if (num != null && num.intValue() == 2) {
                    RecyclerView recyclerView = this.p.t;
                    Wg6.b(recyclerView, "rvFriends");
                    list.add(new Gz4.Ci("", 2131231018, W6.d(recyclerView.getContext(), 2131099706), Gz4.Bi.BLOCK, this.q));
                } else if (num != null && num.intValue() == 0) {
                    if (xs4.f() == 0) {
                        RecyclerView recyclerView2 = this.p.t;
                        Wg6.b(recyclerView2, "rvFriends");
                        list.add(new Gz4.Ci("", 2131231042, W6.d(recyclerView2.getContext(), 2131100357), Gz4.Bi.UNFRIEND, this.q));
                        RecyclerView recyclerView3 = this.p.t;
                        Wg6.b(recyclerView3, "rvFriends");
                        list.add(new Gz4.Ci("", 2131231018, W6.d(recyclerView3.getContext(), 2131099706), Gz4.Bi.BLOCK, this.q));
                    }
                } else if (num != null && num.intValue() == -1) {
                    RecyclerView recyclerView4 = this.p.t;
                    Wg6.b(recyclerView4, "rvFriends");
                    list.add(new Gz4.Ci("", 2131231041, W6.d(recyclerView4.getContext(), 2131099706), Gz4.Bi.UNBLOCK, this.q));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Lc6<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Di(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, Boolean> lc6) {
            H35 h35;
            SwipeRefreshLayout swipeRefreshLayout;
            Boolean first = lc6.getFirst();
            Boolean second = lc6.getSecond();
            if (!(first == null || (h35 = (H35) By4.K6(this.a).a()) == null || (swipeRefreshLayout = h35.u) == null)) {
                swipeRefreshLayout.setRefreshing(first.booleanValue());
            }
            if (second == null) {
                return;
            }
            if (second.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends Boolean> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Ei(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        public final void a(List<? extends Object> list) {
            H35 h35;
            FlexibleTextView flexibleTextView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = By4.m.a();
            local.e(a2, "allFriends: " + list);
            Wg6.b(list, "it");
            if (!(!(!list.isEmpty()) || (h35 = (H35) By4.K6(this.a).a()) == null || (flexibleTextView = h35.r) == null)) {
                flexibleTextView.setVisibility(8);
            }
            By4.L6(this.a).i(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Object> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Fi(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            H35 h35 = (H35) By4.K6(this.a).a();
            if (h35 != null) {
                Wg6.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = h35.q;
                    Wg6.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = h35.t;
                    Wg6.b(recyclerView, "rvFriends");
                    recyclerView.setVisibility(8);
                    By4.L6(this.a).g();
                    return;
                }
                FlexibleTextView flexibleTextView2 = h35.q;
                Wg6.b(flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = h35.t;
                Wg6.b(recyclerView2, "rvFriends");
                recyclerView2.setVisibility(0);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Gi(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            H35 h35 = (H35) By4.K6(this.a).a();
            if (h35 != null) {
                Wg6.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = h35.r;
                    Wg6.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = h35.r;
                Wg6.b(flexibleTextView2, "ftvError");
                flexibleTextView2.setVisibility(8);
                if (this.a.isResumed()) {
                    FlexibleTextView flexibleTextView3 = h35.r;
                    Wg6.b(flexibleTextView3, "ftvError");
                    String c = Um5.c(flexibleTextView3.getContext(), 2131886231);
                    FragmentActivity activity = this.a.getActivity();
                    if (activity != null) {
                        Toast.makeText(activity, c, 0).show();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<String> {
        @DexIgnore
        public static /* final */ Hi a; // = new Hi();

        @DexIgnore
        public final void a(String str) {
            Cl5.c.d(PortfolioApp.get.instance(), str.hashCode());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Ii(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886795);
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886794);
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            Wg6.b(c, "title");
            Wg6.b(c2, "des");
            s37.E(childFragmentManager, c, c2);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<Lc6<? extends String, ? extends String>> {
        @DexIgnore
        public static /* final */ Ji a; // = new Ji();

        @DexIgnore
        public final void a(Lc6<String, String> lc6) {
            Xr4.a.b(lc6.getFirst(), PortfolioApp.get.instance().l0(), lc6.getSecond(), PortfolioApp.get.instance());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends String, ? extends String> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<ServerError> {
        @DexIgnore
        public /* final */ /* synthetic */ By4 a;

        @DexIgnore
        public Ki(By4 by4) {
            this.a = by4;
        }

        @DexIgnore
        public final void a(ServerError serverError) {
            Jl5 jl5 = Jl5.b;
            Wg6.b(serverError, "it");
            Integer code = serverError.getCode();
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            String e = jl5.e(code, message);
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.A0(childFragmentManager, e);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(ServerError serverError) {
            a(serverError);
        }
    }

    /*
    static {
        String simpleName = By4.class.getSimpleName();
        Wg6.b(simpleName, "BCFriendTabFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(By4 by4) {
        G37<H35> g37 = by4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Ay4 L6(By4 by4) {
        Ay4 ay4 = by4.j;
        if (ay4 != null) {
            return ay4;
        }
        Wg6.n("friendAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCFriendTabViewModel N6(By4 by4) {
        BCFriendTabViewModel bCFriendTabViewModel = by4.i;
        if (bCFriendTabViewModel != null) {
            return bCFriendTabViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        Ay4 ay4 = new Ay4(this);
        this.j = ay4;
        if (ay4 != null) {
            ay4.setHasStableIds(true);
            G37<H35> g37 = this.h;
            if (g37 != null) {
                H35 a2 = g37.a();
                if (a2 != null) {
                    a2.u.setOnRefreshListener(new Bi(this));
                    RecyclerView recyclerView = a2.t;
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    Ay4 ay42 = this.j;
                    if (ay42 != null) {
                        recyclerView.setAdapter(ay42);
                        RecyclerView recyclerView2 = a2.t;
                        Wg6.b(recyclerView2, "rvFriends");
                        new Ci(a2, recyclerView2, this);
                        return;
                    }
                    Wg6.n("friendAdapter");
                    throw null;
                }
                return;
            }
            Wg6.n("binding");
            throw null;
        }
        Wg6.n("friendAdapter");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        BCFriendTabViewModel bCFriendTabViewModel = this.i;
        if (bCFriendTabViewModel != null) {
            bCFriendTabViewModel.q().h(getViewLifecycleOwner(), new Di(this));
            BCFriendTabViewModel bCFriendTabViewModel2 = this.i;
            if (bCFriendTabViewModel2 != null) {
                bCFriendTabViewModel2.m().h(getViewLifecycleOwner(), new Ei(this));
                BCFriendTabViewModel bCFriendTabViewModel3 = this.i;
                if (bCFriendTabViewModel3 != null) {
                    bCFriendTabViewModel3.o().h(getViewLifecycleOwner(), new Fi(this));
                    BCFriendTabViewModel bCFriendTabViewModel4 = this.i;
                    if (bCFriendTabViewModel4 != null) {
                        bCFriendTabViewModel4.p().h(getViewLifecycleOwner(), new Gi(this));
                        BCFriendTabViewModel bCFriendTabViewModel5 = this.i;
                        if (bCFriendTabViewModel5 != null) {
                            bCFriendTabViewModel5.n().h(getViewLifecycleOwner(), Hi.a);
                            BCFriendTabViewModel bCFriendTabViewModel6 = this.i;
                            if (bCFriendTabViewModel6 != null) {
                                bCFriendTabViewModel6.r().h(getViewLifecycleOwner(), new Ii(this));
                                BCFriendTabViewModel bCFriendTabViewModel7 = this.i;
                                if (bCFriendTabViewModel7 != null) {
                                    bCFriendTabViewModel7.s().h(getViewLifecycleOwner(), Ji.a);
                                    BCFriendTabViewModel bCFriendTabViewModel8 = this.i;
                                    if (bCFriendTabViewModel8 != null) {
                                        bCFriendTabViewModel8.t().h(getViewLifecycleOwner(), new Ki(this));
                                    } else {
                                        Wg6.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Oy5
    public void R3(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        BCFriendTabViewModel bCFriendTabViewModel = this.i;
        if (bCFriendTabViewModel != null) {
            bCFriendTabViewModel.x(xs4, false);
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (i2 == 2131363373) {
            int hashCode = str.hashCode();
            if (hashCode != -1742668797) {
                if (hashCode != -322581840) {
                    if (hashCode == -19037436 && str.equals("UN_FRIEND")) {
                        Xs4 xs4 = intent != null ? (Xs4) intent.getParcelableExtra("friend_extra") : null;
                        if (xs4 != null) {
                            BCFriendTabViewModel bCFriendTabViewModel = this.i;
                            if (bCFriendTabViewModel != null) {
                                bCFriendTabViewModel.z(xs4);
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        }
                    }
                } else if (str.equals("BLOCK_FRIEND")) {
                    Xs4 xs42 = intent != null ? (Xs4) intent.getParcelableExtra("friend_extra") : null;
                    if (xs42 != null) {
                        BCFriendTabViewModel bCFriendTabViewModel2 = this.i;
                        if (bCFriendTabViewModel2 != null) {
                            bCFriendTabViewModel2.j(xs42);
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    }
                }
            } else if (str.equals("CANCEL_FRIEND")) {
                Xs4 xs43 = intent != null ? (Xs4) intent.getParcelableExtra("friend_extra") : null;
                if (xs43 != null) {
                    BCFriendTabViewModel bCFriendTabViewModel3 = this.i;
                    if (bCFriendTabViewModel3 != null) {
                        bCFriendTabViewModel3.k(xs43);
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Gz4.Ai
    public void T3(int i2, Gz4.Bi bi) {
        Wg6.c(bi, "type");
        Ay4 ay4 = this.j;
        if (ay4 != null) {
            Object h2 = ay4.h(i2);
            if (!(h2 instanceof Xs4)) {
                h2 = null;
            }
            Xs4 xs4 = (Xs4) h2;
            FLogger.INSTANCE.getLocal().e(l, "position: " + i2 + " - type: " + bi + " - friend: " + xs4);
            if (xs4 != null) {
                int i3 = Cy4.a[bi.ordinal()];
                if (i3 == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("friend_extra", xs4);
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886301);
                    Wg6.b(c, "LanguageHelper.getString\u2026friendThisUserTheyWillBe)");
                    s37.d(childFragmentManager, bundle, "UN_FRIEND", c);
                } else if (i3 == 2) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putParcelable("friend_extra", xs4);
                    S37 s372 = S37.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    Wg6.b(childFragmentManager2, "childFragmentManager");
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886291);
                    Wg6.b(c2, "LanguageHelper.getString\u2026ext__AreYouSureYouWantTo)");
                    s372.d(childFragmentManager2, bundle2, "BLOCK_FRIEND", c2);
                } else if (i3 == 3) {
                    BCFriendTabViewModel bCFriendTabViewModel = this.i;
                    if (bCFriendTabViewModel != null) {
                        bCFriendTabViewModel.y(xs4);
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else if (i3 == 4) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putParcelable("friend_extra", xs4);
                    S37 s373 = S37.c;
                    FragmentManager childFragmentManager3 = getChildFragmentManager();
                    Wg6.b(childFragmentManager3, "childFragmentManager");
                    String c3 = Um5.c(PortfolioApp.get.instance(), 2131886303);
                    Wg6.b(c3, "LanguageHelper.getString\u2026ext__AreYouSureYouWantTo)");
                    s373.d(childFragmentManager3, bundle3, "CANCEL_FRIEND", c3);
                }
            }
        } else {
            Wg6.n("friendAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Oy5
    public void e4(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        BCFriendTabViewModel bCFriendTabViewModel = this.i;
        if (bCFriendTabViewModel != null) {
            bCFriendTabViewModel.x(xs4, true);
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().Q1().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCFriendTabViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.i = (BCFriendTabViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        H35 h35 = (H35) Aq0.f(layoutInflater, 2131558513, viewGroup, false, A6());
        this.h = new G37<>(this, h35);
        Wg6.b(h35, "binding");
        return h35.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        O6();
        P6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
