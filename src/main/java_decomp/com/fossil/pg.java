package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pg extends Zj {
    @DexIgnore
    public /* final */ Iw1 T;

    @DexIgnore
    public Pg(K5 k5, I60 i60, Iw1 iw1) {
        super(k5, i60, Yp.y0, true, Ke.b.b(k5.x, Ob.x), iw1.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = iw1;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.J4, this.T.toJSONObject());
    }
}
