package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Js0<T> extends MutableLiveData<T> {
    @DexIgnore
    public Fi0<LiveData<?>, Ai<?>> k; // = new Fi0<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<V> implements Ls0<V> {
        @DexIgnore
        public /* final */ LiveData<V> a;
        @DexIgnore
        public /* final */ Ls0<? super V> b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public Ai(LiveData<V> liveData, Ls0<? super V> ls0) {
            this.a = liveData;
            this.b = ls0;
        }

        @DexIgnore
        public void a() {
            this.a.i(this);
        }

        @DexIgnore
        public void b() {
            this.a.m(this);
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public void onChanged(V v) {
            if (this.c != this.a.f()) {
                this.c = this.a.f();
                this.b.onChanged(v);
            }
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void j() {
        Iterator<Map.Entry<LiveData<?>, Ai<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().getValue().a();
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void k() {
        Iterator<Map.Entry<LiveData<?>, Ai<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().getValue().b();
        }
    }

    @DexIgnore
    public <S> void p(LiveData<S> liveData, Ls0<? super S> ls0) {
        Ai<?> ai = new Ai<>(liveData, ls0);
        Ai<?> f = this.k.f(liveData, ai);
        if (f != null && f.b != ls0) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        } else if (f == null && g()) {
            ai.a();
        }
    }

    @DexIgnore
    public <S> void q(LiveData<S> liveData) {
        Ai<?> g = this.k.g(liveData);
        if (g != null) {
            g.b();
        }
    }
}
