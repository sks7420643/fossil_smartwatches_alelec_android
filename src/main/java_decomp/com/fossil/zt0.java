package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.mapped.Cf;
import com.mapped.Xe;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zt0<Key, Value> {
    @DexIgnore
    public Key a;
    @DexIgnore
    public Cf.Fi b;
    @DexIgnore
    public Xe.Bi<Key, Value> c;
    @DexIgnore
    public Cf.Ci d;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public Executor e; // = Bi0.e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Mr0<Cf<Value>> {
        @DexIgnore
        public Cf<Value> g;
        @DexIgnore
        public Xe<Key, Value> h;
        @DexIgnore
        public /* final */ Xe.Ci i; // = new Aii();
        @DexIgnore
        public /* final */ /* synthetic */ Object j;
        @DexIgnore
        public /* final */ /* synthetic */ Xe.Bi k;
        @DexIgnore
        public /* final */ /* synthetic */ Cf.Fi l;
        @DexIgnore
        public /* final */ /* synthetic */ Executor m;
        @DexIgnore
        public /* final */ /* synthetic */ Executor n;
        @DexIgnore
        public /* final */ /* synthetic */ Cf.Ci o;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Xe.Ci {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.mapped.Xe.Ci
            public void a() {
                Ai.this.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Executor executor, Object obj, Xe.Bi bi, Cf.Fi fi, Executor executor2, Executor executor3, Cf.Ci ci) {
            super(executor);
            this.j = obj;
            this.k = bi;
            this.l = fi;
            this.m = executor2;
            this.n = executor3;
            this.o = ci;
        }

        @DexIgnore
        @Override // com.fossil.Mr0
        public /* bridge */ /* synthetic */ Object a() {
            return d();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.mapped.Cf$Di */
        /* JADX WARN: Multi-variable type inference failed */
        public Cf<Value> d() {
            Cf<Value> a2;
            Object obj = this.j;
            Cf<Value> cf = this.g;
            if (cf != null) {
                obj = cf.q();
            }
            do {
                Xe<Key, Value> xe = this.h;
                if (xe != null) {
                    xe.removeInvalidatedCallback(this.i);
                }
                Xe<Key, Value> create = this.k.create();
                this.h = create;
                create.addInvalidatedCallback(this.i);
                Cf.Di di = new Cf.Di(this.h, this.l);
                di.e(this.m);
                di.c(this.n);
                di.b(this.o);
                di.d(obj);
                a2 = di.a();
                this.g = a2;
            } while (a2.t());
            return this.g;
        }
    }

    @DexIgnore
    public Zt0(Xe.Bi<Key, Value> bi, Cf.Fi fi) {
        if (fi == null) {
            throw new IllegalArgumentException("PagedList.Config must be provided");
        } else if (bi != null) {
            this.c = bi;
            this.b = fi;
        } else {
            throw new IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public static <Key, Value> LiveData<Cf<Value>> b(Key key, Cf.Fi fi, Cf.Ci ci, Xe.Bi<Key, Value> bi, Executor executor, Executor executor2) {
        return new Ai(executor2, key, bi, fi, executor, executor2, ci).b();
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public LiveData<Cf<Value>> a() {
        return b(this.a, this.b, this.d, this.c, Bi0.g(), this.e);
    }
}
