package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yu extends Mu {
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    public Yu(long j, K5 k5) {
        super(Fu.r, Hs.F, k5, 0, 8);
        this.L = j;
        this.E = true;
    }

    @DexIgnore
    @Override // com.fossil.Mu, com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        Wg6.b(array, "ByteBuffer.allocate(4).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.A1, Long.valueOf(this.L));
    }
}
