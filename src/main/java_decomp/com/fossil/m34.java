package com.fossil;

import com.fossil.H34;
import com.fossil.U24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M34<E> extends N34<E> implements NavigableSet<E>, B54<E> {
    @DexIgnore
    public /* final */ transient Comparator<? super E> comparator;
    @DexIgnore
    @LazyInit
    public transient M34<E> descendingSet;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<E> extends H34.Ai<E> {
        @DexIgnore
        public /* final */ Comparator<? super E> c;

        @DexIgnore
        public Ai(Comparator<? super E> comparator) {
            I14.l(comparator);
            this.c = comparator;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.H34.Ai, com.fossil.U24.Bi
        public /* bridge */ /* synthetic */ U24.Bi a(Object obj) {
            k(obj);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.H34.Ai
        public /* bridge */ /* synthetic */ H34.Ai g(Object obj) {
            k(obj);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.H34.Ai
        public /* bridge */ /* synthetic */ H34.Ai i(Iterator it) {
            m(it);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.H34.Ai
        public /* bridge */ /* synthetic */ H34 j() {
            return n();
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> k(E e) {
            super.g(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> l(E... eArr) {
            super.h(eArr);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> m(Iterator<? extends E> it) {
            super.i(it);
            return this;
        }

        @DexIgnore
        public M34<E> n() {
            M34<E> construct = M34.construct(this.c, this.b, this.a);
            this.b = construct.size();
            return construct;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<? super E> comparator;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public Bi(Comparator<? super E> comparator2, Object[] objArr) {
            this.comparator = comparator2;
            this.elements = objArr;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M34$Ai */
        /* JADX WARN: Multi-variable type inference failed */
        public Object readResolve() {
            Ai ai = new Ai(this.comparator);
            ai.l(this.elements);
            return ai.n();
        }
    }

    @DexIgnore
    public M34(Comparator<? super E> comparator2) {
        this.comparator = comparator2;
    }

    @DexIgnore
    public static <E> M34<E> construct(Comparator<? super E> comparator2, int i, E... eArr) {
        if (i == 0) {
            return emptySet(comparator2);
        }
        H44.d(eArr, i);
        Arrays.sort(eArr, 0, i, comparator2);
        int i2 = 1;
        for (int i3 = 1; i3 < i; i3++) {
            E e = eArr[i3];
            if (comparator2.compare(e, eArr[i2 - 1]) != 0) {
                eArr[i2] = e;
                i2++;
            }
        }
        Arrays.fill(eArr, i2, i, (Object) null);
        return new S44(Y24.asImmutableList(eArr, i2), comparator2);
    }

    @DexIgnore
    public static <E> M34<E> copyOf(Iterable<? extends E> iterable) {
        return copyOf(I44.natural(), iterable);
    }

    @DexIgnore
    public static <E> M34<E> copyOf(Collection<? extends E> collection) {
        return copyOf((Comparator) I44.natural(), (Collection) collection);
    }

    @DexIgnore
    public static <E> M34<E> copyOf(Comparator<? super E> comparator2, Iterable<? extends E> iterable) {
        I14.l(comparator2);
        if (C54.b(comparator2, iterable) && (iterable instanceof M34)) {
            M34<E> m34 = (M34) iterable;
            if (!m34.isPartialView()) {
                return m34;
            }
        }
        Object[] h = O34.h(iterable);
        return construct(comparator2, h.length, h);
    }

    @DexIgnore
    public static <E> M34<E> copyOf(Comparator<? super E> comparator2, Collection<? extends E> collection) {
        return copyOf((Comparator) comparator2, (Iterable) collection);
    }

    @DexIgnore
    public static <E> M34<E> copyOf(Comparator<? super E> comparator2, Iterator<? extends E> it) {
        Ai ai = new Ai(comparator2);
        ai.m(it);
        return ai.n();
    }

    @DexIgnore
    public static <E> M34<E> copyOf(Iterator<? extends E> it) {
        return copyOf(I44.natural(), it);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> copyOf(E[] eArr) {
        return construct(I44.natural(), eArr.length, (Object[]) eArr.clone());
    }

    @DexIgnore
    public static <E> M34<E> copyOfSorted(SortedSet<E> sortedSet) {
        Comparator a2 = C54.a(sortedSet);
        Y24 copyOf = Y24.copyOf((Collection) sortedSet);
        return copyOf.isEmpty() ? emptySet(a2) : new S44(copyOf, a2);
    }

    @DexIgnore
    public static <E> S44<E> emptySet(Comparator<? super E> comparator2) {
        return I44.natural().equals(comparator2) ? (S44<E>) S44.NATURAL_EMPTY_SET : new S44<>(Y24.of(), comparator2);
    }

    @DexIgnore
    public static <E extends Comparable<?>> Ai<E> naturalOrder() {
        return new Ai<>(I44.natural());
    }

    @DexIgnore
    public static <E> M34<E> of() {
        return S44.NATURAL_EMPTY_SET;
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> of(E e) {
        return new S44(Y24.of((Object) e), I44.natural());
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> of(E e, E e2) {
        return construct(I44.natural(), 2, e, e2);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> of(E e, E e2, E e3) {
        return construct(I44.natural(), 3, e, e2, e3);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> of(E e, E e2, E e3, E e4) {
        return construct(I44.natural(), 4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> of(E e, E e2, E e3, E e4, E e5) {
        return construct(I44.natural(), 5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> M34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        int length = eArr.length + 6;
        Comparable[] comparableArr = new Comparable[length];
        comparableArr[0] = e;
        comparableArr[1] = e2;
        comparableArr[2] = e3;
        comparableArr[3] = e4;
        comparableArr[4] = e5;
        comparableArr[5] = e6;
        System.arraycopy(eArr, 0, comparableArr, 6, eArr.length);
        return construct(I44.natural(), length, comparableArr);
    }

    @DexIgnore
    public static <E> Ai<E> orderedBy(Comparator<E> comparator2) {
        return new Ai<>(comparator2);
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    public static <E extends Comparable<?>> Ai<E> reverseOrder() {
        return new Ai<>(I44.natural().reverse());
    }

    @DexIgnore
    public static int unsafeCompare(Comparator<?> comparator2, Object obj, Object obj2) {
        return comparator2.compare(obj, obj2);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E ceiling(E e) {
        return (E) O34.e(tailSet((M34<E>) e, true), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.B54
    public Comparator<? super E> comparator() {
        return this.comparator;
    }

    @DexIgnore
    public M34<E> createDescendingSet() {
        return new F24(this);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public abstract H54<E> descendingIterator();

    @DexIgnore
    @Override // java.util.NavigableSet
    public M34<E> descendingSet() {
        M34<E> m34 = this.descendingSet;
        if (m34 != null) {
            return m34;
        }
        M34<E> createDescendingSet = createDescendingSet();
        this.descendingSet = createDescendingSet;
        createDescendingSet.descendingSet = this;
        return createDescendingSet;
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E first() {
        return iterator().next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E floor(E e) {
        return (E) P34.n(headSet((M34<E>) e, true).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public M34<E> headSet(E e) {
        return headSet((M34<E>) e, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public M34<E> headSet(E e, boolean z) {
        I14.l(e);
        return headSetImpl(e, z);
    }

    @DexIgnore
    public abstract M34<E> headSetImpl(E e, boolean z);

    @DexIgnore
    @Override // java.util.NavigableSet
    public E higher(E e) {
        return (E) O34.e(tailSet((M34<E>) e, false), null);
    }

    @DexIgnore
    public abstract int indexOf(Object obj);

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, java.util.NavigableSet, com.fossil.H34, com.fossil.H34, java.lang.Iterable
    public abstract H54<E> iterator();

    @DexIgnore
    @Override // java.util.SortedSet
    public E last() {
        return descendingIterator().next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E lower(E e) {
        return (E) P34.n(headSet((M34<E>) e, false).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @CanIgnoreReturnValue
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @CanIgnoreReturnValue
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public M34<E> subSet(E e, E e2) {
        return subSet((boolean) e, true, (boolean) e2, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public M34<E> subSet(E e, boolean z, E e2, boolean z2) {
        I14.l(e);
        I14.l(e2);
        I14.d(this.comparator.compare(e, e2) <= 0);
        return subSetImpl(e, z, e2, z2);
    }

    @DexIgnore
    public abstract M34<E> subSetImpl(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public M34<E> tailSet(E e) {
        return tailSet((M34<E>) e, true);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public M34<E> tailSet(E e, boolean z) {
        I14.l(e);
        return tailSetImpl(e, z);
    }

    @DexIgnore
    public abstract M34<E> tailSetImpl(E e, boolean z);

    @DexIgnore
    public int unsafeCompare(Object obj, Object obj2) {
        return unsafeCompare(this.comparator, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.H34
    public Object writeReplace() {
        return new Bi(this.comparator, toArray());
    }
}
