package com.fossil;

import com.portfolio.platform.uirenew.home.HomeFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fz5 implements MembersInjector<HomeFragment> {
    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
        homeFragment.m = homeAlertsHybridPresenter;
    }

    @DexIgnore
    public static void b(HomeFragment homeFragment, HomeAlertsPresenter homeAlertsPresenter) {
        homeFragment.l = homeAlertsPresenter;
    }

    @DexIgnore
    public static void c(HomeFragment homeFragment, HomeDashboardPresenter homeDashboardPresenter) {
        homeFragment.h = homeDashboardPresenter;
    }

    @DexIgnore
    public static void d(HomeFragment homeFragment, HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
        homeFragment.i = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public static void e(HomeFragment homeFragment, HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
        homeFragment.j = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public static void f(HomeFragment homeFragment, HomeProfilePresenter homeProfilePresenter) {
        homeFragment.k = homeProfilePresenter;
    }

    @DexIgnore
    public static void g(HomeFragment homeFragment, Un6 un6) {
        homeFragment.s = un6;
    }
}
