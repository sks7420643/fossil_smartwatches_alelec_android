package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z14<F, T> extends I44<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ B14<F, ? extends T> function;
    @DexIgnore
    public /* final */ I44<T> ordering;

    @DexIgnore
    public Z14(B14<F, ? extends T> b14, I44<T> i44) {
        I14.l(b14);
        this.function = b14;
        I14.l(i44);
        this.ordering = i44;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.I44<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.I44, java.util.Comparator
    public int compare(F f, F f2) {
        return this.ordering.compare(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Z14)) {
            return false;
        }
        Z14 z14 = (Z14) obj;
        return this.function.equals(z14.function) && this.ordering.equals(z14.ordering);
    }

    @DexIgnore
    public int hashCode() {
        return F14.b(this.function, this.ordering);
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".onResultOf(" + this.function + ")";
    }
}
