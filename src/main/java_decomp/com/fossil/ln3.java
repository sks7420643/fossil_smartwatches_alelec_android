package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ln3 {
    @DexIgnore
    Yr3 b();

    @DexIgnore
    Im3 c();

    @DexIgnore
    Kl3 d();

    @DexIgnore
    Context e();

    @DexIgnore
    Ef2 zzm();
}
