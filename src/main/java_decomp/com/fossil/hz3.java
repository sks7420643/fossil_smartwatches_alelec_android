package com.fossil;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hz3 {
    @DexIgnore
    public /* final */ TextPaint a; // = new TextPaint(1);
    @DexIgnore
    public /* final */ Rz3 b; // = new Ai();
    @DexIgnore
    public float c;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public WeakReference<Bi> e; // = new WeakReference<>(null);
    @DexIgnore
    public Pz3 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Rz3 {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Rz3
        public void a(int i) {
            Hz3.this.d = true;
            Bi bi = (Bi) Hz3.this.e.get();
            if (bi != null) {
                bi.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.Rz3
        public void b(Typeface typeface, boolean z) {
            if (!z) {
                Hz3.this.d = true;
                Bi bi = (Bi) Hz3.this.e.get();
                if (bi != null) {
                    bi.a();
                }
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        int[] getState();

        @DexIgnore
        boolean onStateChange(int[] iArr);
    }

    @DexIgnore
    public Hz3(Bi bi) {
        g(bi);
    }

    @DexIgnore
    public final float c(CharSequence charSequence) {
        return charSequence == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.a.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public Pz3 d() {
        return this.f;
    }

    @DexIgnore
    public TextPaint e() {
        return this.a;
    }

    @DexIgnore
    public float f(String str) {
        if (!this.d) {
            return this.c;
        }
        float c2 = c(str);
        this.c = c2;
        this.d = false;
        return c2;
    }

    @DexIgnore
    public void g(Bi bi) {
        this.e = new WeakReference<>(bi);
    }

    @DexIgnore
    public void h(Pz3 pz3, Context context) {
        if (this.f != pz3) {
            this.f = pz3;
            if (pz3 != null) {
                pz3.j(context, this.a, this.b);
                Bi bi = this.e.get();
                if (bi != null) {
                    this.a.drawableState = bi.getState();
                }
                pz3.i(context, this.a, this.b);
                this.d = true;
            }
            Bi bi2 = this.e.get();
            if (bi2 != null) {
                bi2.a();
                bi2.onStateChange(bi2.getState());
            }
        }
    }

    @DexIgnore
    public void i(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void j(Context context) {
        this.f.i(context, this.a, this.b);
    }
}
