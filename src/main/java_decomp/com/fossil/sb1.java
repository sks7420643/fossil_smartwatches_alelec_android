package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Sb1<T> extends Mb1 {
    @DexIgnore
    Id1<T> b(Context context, Id1<T> id1, int i, int i2);
}
