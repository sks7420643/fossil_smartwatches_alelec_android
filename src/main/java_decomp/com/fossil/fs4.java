package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.ShareConstants;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fs4 extends RecyclerView.g<Ai> {
    @DexIgnore
    public /* final */ List<Gs4> a;
    @DexIgnore
    public /* final */ View.OnClickListener b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ M15 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(M15 m15, View view, View.OnClickListener onClickListener) {
            super(view);
            Wg6.c(m15, "binding");
            Wg6.c(view, "root");
            Wg6.c(onClickListener, "listener");
            this.a = m15;
            view.setTag(this);
            view.setOnClickListener(onClickListener);
        }

        @DexIgnore
        public final void a(Gs4 gs4) {
            Wg6.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            M15 m15 = this.a;
            FlexibleTextView flexibleTextView = m15.q;
            Wg6.b(flexibleTextView, "ftvName");
            flexibleTextView.setText(gs4.b());
            RTLImageView rTLImageView = m15.r;
            Wg6.b(rTLImageView, "ivCheck");
            rTLImageView.setVisibility(gs4.c() ? 0 : 8);
        }
    }

    @DexIgnore
    public Fs4(List<Gs4> list, View.OnClickListener onClickListener) {
        Wg6.c(list, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        Wg6.c(onClickListener, "listener");
        this.a = list;
        this.b = onClickListener;
    }

    @DexIgnore
    public void g(Ai ai, int i) {
        Wg6.c(ai, "holder");
        ai.a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public Ai h(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        M15 z = M15.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "DialogItemPrivacyLayoutB\u2026tInflater, parent, false)");
        View n = z.n();
        Wg6.b(n, "binding.root");
        return new Ai(z, n, this.b);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        g(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return h(viewGroup, i);
    }
}
