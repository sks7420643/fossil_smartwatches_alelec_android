package com.fossil;

import com.fossil.E88;
import com.mapped.Cd6;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A88 extends E88.Ai {
    @DexIgnore
    public boolean a; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements E88<W18, W18> {
        @DexIgnore
        public static /* final */ Ai a; // = new Ai();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ W18 a(W18 w18) throws IOException {
            return b(w18);
        }

        @DexIgnore
        public W18 b(W18 w18) throws IOException {
            try {
                return U88.a(w18);
            } finally {
                w18.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements E88<RequestBody, RequestBody> {
        @DexIgnore
        public static /* final */ Bi a; // = new Bi();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ RequestBody a(RequestBody requestBody) throws IOException {
            RequestBody requestBody2 = requestBody;
            b(requestBody2);
            return requestBody2;
        }

        @DexIgnore
        public RequestBody b(RequestBody requestBody) {
            return requestBody;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements E88<W18, W18> {
        @DexIgnore
        public static /* final */ Ci a; // = new Ci();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ W18 a(W18 w18) throws IOException {
            W18 w182 = w18;
            b(w182);
            return w182;
        }

        @DexIgnore
        public W18 b(W18 w18) {
            return w18;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements E88<Object, String> {
        @DexIgnore
        public static /* final */ Di a; // = new Di();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ String a(Object obj) throws IOException {
            return b(obj);
        }

        @DexIgnore
        public String b(Object obj) {
            return obj.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements E88<W18, Cd6> {
        @DexIgnore
        public static /* final */ Ei a; // = new Ei();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ Cd6 a(W18 w18) throws IOException {
            return b(w18);
        }

        @DexIgnore
        public Cd6 b(W18 w18) {
            w18.close();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements E88<W18, Void> {
        @DexIgnore
        public static /* final */ Fi a; // = new Fi();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ Void a(W18 w18) throws IOException {
            return b(w18);
        }

        @DexIgnore
        public Void b(W18 w18) {
            w18.close();
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.E88.Ai
    public E88<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (RequestBody.class.isAssignableFrom(U88.i(type))) {
            return Bi.a;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.E88.Ai
    public E88<W18, ?> d(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == W18.class) {
            return U88.m(annotationArr, Ba8.class) ? Ci.a : Ai.a;
        }
        if (type == Void.class) {
            return Fi.a;
        }
        if (this.a && type == Cd6.class) {
            try {
                return Ei.a;
            } catch (NoClassDefFoundError e) {
                this.a = false;
            }
        }
        return null;
    }
}
