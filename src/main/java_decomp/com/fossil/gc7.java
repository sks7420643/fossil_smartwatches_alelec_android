package com.fossil;

import android.graphics.Rect;
import android.graphics.Typeface;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.S87;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gc7 {
    @DexIgnore
    public /* final */ MutableLiveData<Fb7> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Rect> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Fb7> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<U37<S87.Ci>> d;
    @DexIgnore
    public /* final */ LiveData<U37<S87.Ci>> e;
    @DexIgnore
    public /* final */ MutableLiveData<U37<S87.Bi>> f;
    @DexIgnore
    public /* final */ MutableLiveData<U37<S87.Bi>> g;
    @DexIgnore
    public /* final */ MutableLiveData<S87> h;
    @DexIgnore
    public /* final */ LiveData<S87> i;
    @DexIgnore
    public /* final */ MutableLiveData<Typeface> j;
    @DexIgnore
    public /* final */ LiveData<Typeface> k;
    @DexIgnore
    public /* final */ MutableLiveData<O87> l;
    @DexIgnore
    public /* final */ LiveData<O87> m;
    @DexIgnore
    public /* final */ MutableLiveData<Object> n;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> o;
    @DexIgnore
    public /* final */ MutableLiveData<String> p;
    @DexIgnore
    public /* final */ MutableLiveData<Eb7> q;
    @DexIgnore
    public /* final */ MutableLiveData<List<DianaAppSetting>> r;
    @DexIgnore
    public /* final */ MutableLiveData<Cb7> s;
    @DexIgnore
    public /* final */ MutableLiveData<U37<Boolean>> t;
    @DexIgnore
    public /* final */ LiveData<U37<Boolean>> u;
    @DexIgnore
    public MutableLiveData<String> v;

    @DexIgnore
    public Gc7() {
        MutableLiveData<U37<S87.Ci>> mutableLiveData = new MutableLiveData<>();
        this.d = mutableLiveData;
        this.e = mutableLiveData;
        MutableLiveData<U37<S87.Bi>> mutableLiveData2 = new MutableLiveData<>();
        this.f = mutableLiveData2;
        this.g = mutableLiveData2;
        MutableLiveData<S87> mutableLiveData3 = new MutableLiveData<>();
        this.h = mutableLiveData3;
        this.i = mutableLiveData3;
        MutableLiveData<Typeface> mutableLiveData4 = new MutableLiveData<>();
        this.j = mutableLiveData4;
        this.k = mutableLiveData4;
        MutableLiveData<O87> mutableLiveData5 = new MutableLiveData<>();
        this.l = mutableLiveData5;
        this.m = mutableLiveData5;
        this.n = new MutableLiveData<>();
        this.o = new MutableLiveData<>();
        this.p = new MutableLiveData<>();
        this.q = new MutableLiveData<>();
        this.r = new MutableLiveData<>();
        this.s = new MutableLiveData<>();
        MutableLiveData<U37<Boolean>> mutableLiveData6 = new MutableLiveData<>();
        this.t = mutableLiveData6;
        this.u = mutableLiveData6;
        this.v = new MutableLiveData<>();
    }

    @DexIgnore
    public final void A(Cb7 cb7) {
        this.s.l(cb7);
    }

    @DexIgnore
    public final void B(Fb7 fb7) {
        Wg6.c(fb7, "wfWrapper");
        this.a.l(fb7);
    }

    @DexIgnore
    public final void C(O87 o87) {
        Wg6.c(o87, "colorSpace");
        this.l.l(o87);
    }

    @DexIgnore
    public final void D(Typeface typeface) {
        Wg6.c(typeface, "typeface");
        this.j.l(typeface);
    }

    @DexIgnore
    public final void E(Eb7 eb7) {
        Wg6.c(eb7, Constants.EVENT);
        this.q.l(eb7);
    }

    @DexIgnore
    public final void F() {
        this.t.l(new U37<>(Boolean.TRUE));
    }

    @DexIgnore
    public final void a(S87.Bi bi) {
        Wg6.c(bi, "stickerConfig");
        this.f.l(new U37<>(bi));
    }

    @DexIgnore
    public final void b(S87.Ci ci) {
        Wg6.c(ci, "textConfig");
        this.d.l(new U37<>(ci));
    }

    @DexIgnore
    public final MutableLiveData<U37<S87.Bi>> c() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<U37<S87.Ci>> d() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Fb7> e() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Object> f() {
        return this.n;
    }

    @DexIgnore
    public final LiveData<List<DianaAppSetting>> g() {
        return this.r;
    }

    @DexIgnore
    public final LiveData<Rect> h() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<S87> i() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Boolean> j() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<String> k() {
        return this.p;
    }

    @DexIgnore
    public final LiveData<Fb7> l() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<U37<Boolean>> m() {
        return this.u;
    }

    @DexIgnore
    public final LiveData<Cb7> n() {
        return this.s;
    }

    @DexIgnore
    public final LiveData<O87> o() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<Typeface> p() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Eb7> q() {
        return this.q;
    }

    @DexIgnore
    public final MutableLiveData<String> r() {
        return this.v;
    }

    @DexIgnore
    public final void s() {
        this.n.l(new Object());
    }

    @DexIgnore
    public final void t(List<DianaAppSetting> list) {
        Wg6.c(list, Constants.USER_SETTING);
        this.r.l(list);
    }

    @DexIgnore
    public final void u(Rect rect) {
        Wg6.c(rect, "rect");
        this.b.l(rect);
    }

    @DexIgnore
    public final void v(S87 s87) {
        this.h.l(s87);
    }

    @DexIgnore
    public final void w(boolean z) {
        this.o.l(Boolean.valueOf(z));
    }

    @DexIgnore
    public final void x(String str) {
        this.v.l(str);
    }

    @DexIgnore
    public final void y(String str) {
        Wg6.c(str, "complicationId");
        this.p.l(str);
    }

    @DexIgnore
    public final void z(Fb7 fb7) {
        this.c.l(fb7);
    }
}
