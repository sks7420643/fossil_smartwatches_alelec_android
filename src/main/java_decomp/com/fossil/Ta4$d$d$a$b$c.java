package com.fossil;

import com.fossil.La4;
import com.fossil.Ta4$d$d$a$b$e;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ta4$d$d$a$b$c {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract Ta4$d$d$a$b$c a();

        @DexIgnore
        public abstract a b(Ta4$d$d$a$b$c ta4$d$d$a$b$c);

        @DexIgnore
        public abstract a c(Ua4<Ta4$d$d$a$b$e.b> ua4);

        @DexIgnore
        public abstract a d(int i);

        @DexIgnore
        public abstract a e(String str);

        @DexIgnore
        public abstract a f(String str);
    }

    @DexIgnore
    public static a a() {
        return new La4.Bi();
    }

    @DexIgnore
    public abstract Ta4$d$d$a$b$c b();

    @DexIgnore
    public abstract Ua4<Ta4$d$d$a$b$e.b> c();

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract String f();
}
