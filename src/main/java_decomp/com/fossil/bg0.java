package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.ListMenuItemView;
import com.fossil.Jg0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bg0 extends BaseAdapter {
    @DexIgnore
    public Cg0 b;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ LayoutInflater f;
    @DexIgnore
    public /* final */ int g;

    @DexIgnore
    public Bg0(Cg0 cg0, LayoutInflater layoutInflater, boolean z, int i) {
        this.e = z;
        this.f = layoutInflater;
        this.b = cg0;
        this.g = i;
        a();
    }

    @DexIgnore
    public void a() {
        Eg0 x = this.b.x();
        if (x != null) {
            ArrayList<Eg0> B = this.b.B();
            int size = B.size();
            for (int i = 0; i < size; i++) {
                if (B.get(i) == x) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    @DexIgnore
    public Cg0 b() {
        return this.b;
    }

    @DexIgnore
    public Eg0 c(int i) {
        ArrayList<Eg0> B = this.e ? this.b.B() : this.b.G();
        int i2 = this.c;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return B.get(i);
    }

    @DexIgnore
    public void d(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public int getCount() {
        ArrayList<Eg0> B = this.e ? this.b.B() : this.b.G();
        return this.c < 0 ? B.size() : B.size() - 1;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return c(i);
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f.inflate(this.g, viewGroup, false) : view;
        int groupId = c(i).getGroupId();
        int i2 = i - 1;
        int groupId2 = i2 >= 0 ? c(i2).getGroupId() : groupId;
        ListMenuItemView listMenuItemView = (ListMenuItemView) inflate;
        listMenuItemView.setGroupDividerEnabled(this.b.H() && groupId != groupId2);
        Jg0.Ai ai = (Jg0.Ai) inflate;
        if (this.d) {
            listMenuItemView.setForceShowIcon(true);
        }
        ai.f(c(i), 0);
        return inflate;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
