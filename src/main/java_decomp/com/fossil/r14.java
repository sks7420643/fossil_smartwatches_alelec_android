package com.fossil;

import com.fossil.X34;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R14<K, V> extends U14<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 2447537837011683357L;
    @DexIgnore
    public transient Map<K, Collection<V>> g;
    @DexIgnore
    public transient int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends R14<K, V>.d {
        @DexIgnore
        public Ai(R14 r14) {
            super();
        }

        @DexIgnore
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends R14<K, V>.d {
        @DexIgnore
        public Bi(R14 r14) {
            super();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) {
            return b(obj, obj2);
        }

        @DexIgnore
        public Map.Entry<K, V> b(K k, V v) {
            return X34.e(k, v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends X34.Gi<K, Collection<V>> {
        @DexIgnore
        public /* final */ transient Map<K, Collection<V>> d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends X34.Di<K, Collection<V>> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.fossil.X34.Di
            public Map<K, Collection<V>> a() {
                return Ci.this;
            }

            @DexIgnore
            @Override // com.fossil.X34.Di
            public boolean contains(Object obj) {
                return B24.d(Ci.this.d.entrySet(), obj);
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new Bii();
            }

            @DexIgnore
            public boolean remove(Object obj) {
                if (!contains(obj)) {
                    return false;
                }
                R14.this.c(((Map.Entry) obj).getKey());
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements Iterator<Map.Entry<K, Collection<V>>> {
            @DexIgnore
            public /* final */ Iterator<Map.Entry<K, Collection<V>>> b; // = Ci.this.d.entrySet().iterator();
            @DexIgnore
            public Collection<V> c;

            @DexIgnore
            public Bii() {
            }

            @DexIgnore
            public Map.Entry<K, Collection<V>> a() {
                Map.Entry<K, Collection<V>> next = this.b.next();
                this.c = next.getValue();
                return Ci.this.f(next);
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public /* bridge */ /* synthetic */ Object next() {
                return a();
            }

            @DexIgnore
            public void remove() {
                this.b.remove();
                R14.access$220(R14.this, this.c.size());
                this.c.clear();
            }
        }

        @DexIgnore
        public Ci(Map<K, Collection<V>> map) {
            this.d = map;
        }

        @DexIgnore
        @Override // com.fossil.X34.Gi
        public Set<Map.Entry<K, Collection<V>>> a() {
            return new Aii();
        }

        @DexIgnore
        public Collection<V> c(Object obj) {
            Collection<V> collection = (Collection) X34.n(this.d, obj);
            if (collection == null) {
                return null;
            }
            return R14.this.wrapCollection(obj, collection);
        }

        @DexIgnore
        public void clear() {
            if (this.d == R14.this.g) {
                R14.this.clear();
            } else {
                P34.e(new Bii());
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj) {
            return X34.m(this.d, obj);
        }

        @DexIgnore
        public Collection<V> e(Object obj) {
            Collection<V> remove = this.d.remove(obj);
            if (remove == null) {
                return null;
            }
            Collection<V> createCollection = R14.this.createCollection();
            createCollection.addAll(remove);
            R14.access$220(R14.this, remove.size());
            remove.clear();
            return createCollection;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || this.d.equals(obj);
        }

        @DexIgnore
        public Map.Entry<K, Collection<V>> f(Map.Entry<K, Collection<V>> entry) {
            K key = entry.getKey();
            return X34.e(key, R14.this.wrapCollection(key, entry.getValue()));
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public /* bridge */ /* synthetic */ Object get(Object obj) {
            return c(obj);
        }

        @DexIgnore
        public int hashCode() {
            return this.d.hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Set<K> keySet() {
            return R14.this.keySet();
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public /* bridge */ /* synthetic */ Object remove(Object obj) {
            return e(obj);
        }

        @DexIgnore
        public int size() {
            return this.d.size();
        }

        @DexIgnore
        public String toString() {
            return this.d.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class Di<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> b;
        @DexIgnore
        public K c; // = null;
        @DexIgnore
        public Collection<V> d; // = null;
        @DexIgnore
        public Iterator<V> e; // = P34.j();

        @DexIgnore
        public Di() {
            this.b = R14.this.g.entrySet().iterator();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext() || this.e.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.e.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.b.next();
                this.c = next.getKey();
                Collection<V> value = next.getValue();
                this.d = value;
                this.e = value.iterator();
            }
            return a(this.c, this.e.next());
        }

        @DexIgnore
        public void remove() {
            this.e.remove();
            if (this.d.isEmpty()) {
                this.b.remove();
            }
            R14.access$210(R14.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends X34.Ei<K, Collection<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Iterator<K> {
            @DexIgnore
            public Map.Entry<K, Collection<V>> b;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator c;

            @DexIgnore
            public Aii(Iterator it) {
                this.c = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.c.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public K next() {
                Map.Entry<K, Collection<V>> entry = (Map.Entry) this.c.next();
                this.b = entry;
                return entry.getKey();
            }

            @DexIgnore
            public void remove() {
                A24.c(this.b != null);
                Collection<V> value = this.b.getValue();
                this.c.remove();
                R14.access$220(R14.this, value.size());
                value.clear();
            }
        }

        @DexIgnore
        public Ei(Map<K, Collection<V>> map) {
            super(map);
        }

        @DexIgnore
        @Override // com.fossil.X34.Ei
        public void clear() {
            P34.e(iterator());
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return a().keySet().containsAll(collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || a().keySet().equals(obj);
        }

        @DexIgnore
        public int hashCode() {
            return a().keySet().hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.X34.Ei, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new Aii(a().entrySet().iterator());
        }

        @DexIgnore
        @Override // com.fossil.X34.Ei
        public boolean remove(Object obj) {
            int i;
            V remove = a().remove(obj);
            if (remove != null) {
                int size = remove.size();
                remove.clear();
                R14.access$220(R14.this, size);
                i = size;
            } else {
                i = 0;
            }
            return i > 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends R14<K, V>.j implements RandomAccess {
        @DexIgnore
        public Fi(R14 r14, K k, List<V> list, R14<K, V>.i iVar) {
            super(k, list, iVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends R14<K, V>.c implements SortedMap<K, Collection<V>> {
        @DexIgnore
        public SortedSet<K> f;

        @DexIgnore
        public Gi(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public Comparator<? super K> comparator() {
            return i().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public K firstKey() {
            return i().firstKey();
        }

        @DexIgnore
        public SortedSet<K> g() {
            return new Hi(i());
        }

        @DexIgnore
        public SortedSet<K> h() {
            SortedSet<K> sortedSet = this.f;
            if (sortedSet != null) {
                return sortedSet;
            }
            SortedSet<K> g2 = g();
            this.f = g2;
            return g2;
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> headMap(K k) {
            return new Gi(i().headMap(k));
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> i() {
            return (SortedMap) this.d;
        }

        @DexIgnore
        @Override // java.util.Map, java.util.SortedMap
        public /* bridge */ /* synthetic */ Set keySet() {
            return h();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public K lastKey() {
            return i().lastKey();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> subMap(K k, K k2) {
            return new Gi(i().subMap(k, k2));
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> tailMap(K k) {
            return new Gi(i().tailMap(k));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi extends R14<K, V>.e implements SortedSet<K> {
        @DexIgnore
        public Hi(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> b() {
            return (SortedMap) super.a();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public Comparator<? super K> comparator() {
            return b().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public K first() {
            return b().firstKey();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> headSet(K k) {
            return new Hi(b().headMap(k));
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public K last() {
            return b().lastKey();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> subSet(K k, K k2) {
            return new Hi(b().subMap(k, k2));
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> tailSet(K k) {
            return new Hi(b().tailMap(k));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii extends AbstractCollection<V> {
        @DexIgnore
        public /* final */ K b;
        @DexIgnore
        public Collection<V> c;
        @DexIgnore
        public /* final */ R14<K, V>.i d;
        @DexIgnore
        public /* final */ Collection<V> e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Iterator<V> {
            @DexIgnore
            public /* final */ Iterator<V> b;
            @DexIgnore
            public /* final */ Collection<V> c; // = Ii.this.c;

            @DexIgnore
            public Aii() {
                this.b = R14.this.b(Ii.this.c);
            }

            @DexIgnore
            public Aii(Iterator<V> it) {
                this.b = it;
            }

            @DexIgnore
            public Iterator<V> a() {
                b();
                return this.b;
            }

            @DexIgnore
            public void b() {
                Ii.this.e();
                if (Ii.this.c != this.c) {
                    throw new ConcurrentModificationException();
                }
            }

            @DexIgnore
            public boolean hasNext() {
                b();
                return this.b.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public V next() {
                b();
                return this.b.next();
            }

            @DexIgnore
            public void remove() {
                this.b.remove();
                R14.access$210(R14.this);
                Ii.this.f();
            }
        }

        @DexIgnore
        public Ii(K k, Collection<V> collection, R14<K, V>.i iVar) {
            this.b = k;
            this.c = collection;
            this.d = iVar;
            this.e = iVar == null ? null : iVar.c();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.util.Map */
        /* JADX WARN: Multi-variable type inference failed */
        public void a() {
            R14<K, V>.i iVar = this.d;
            if (iVar != null) {
                iVar.a();
            } else {
                R14.this.g.put(this.b, this.c);
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean add(V v) {
            e();
            boolean isEmpty = this.c.isEmpty();
            boolean add = this.c.add(v);
            if (add) {
                R14.access$208(R14.this);
                if (isEmpty) {
                    a();
                }
            }
            return add;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.c.addAll(collection);
            if (!addAll) {
                return addAll;
            }
            R14.access$212(R14.this, this.c.size() - size);
            if (size != 0) {
                return addAll;
            }
            a();
            return addAll;
        }

        @DexIgnore
        public R14<K, V>.i b() {
            return this.d;
        }

        @DexIgnore
        public Collection<V> c() {
            return this.c;
        }

        @DexIgnore
        public void clear() {
            int size = size();
            if (size != 0) {
                this.c.clear();
                R14.access$220(R14.this, size);
                f();
            }
        }

        @DexIgnore
        public boolean contains(Object obj) {
            e();
            return this.c.contains(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            e();
            return this.c.containsAll(collection);
        }

        @DexIgnore
        public K d() {
            return this.b;
        }

        @DexIgnore
        public void e() {
            Collection<V> collection;
            R14<K, V>.i iVar = this.d;
            if (iVar != null) {
                iVar.e();
                if (this.d.c() != this.e) {
                    throw new ConcurrentModificationException();
                }
            } else if (this.c.isEmpty() && (collection = (Collection) R14.this.g.get(this.b)) != null) {
                this.c = collection;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            e();
            return this.c.equals(obj);
        }

        @DexIgnore
        public void f() {
            R14<K, V>.i iVar = this.d;
            if (iVar != null) {
                iVar.f();
            } else if (this.c.isEmpty()) {
                R14.this.g.remove(this.b);
            }
        }

        @DexIgnore
        public int hashCode() {
            e();
            return this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            e();
            return new Aii();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            e();
            boolean remove = this.c.remove(obj);
            if (remove) {
                R14.access$210(R14.this);
                f();
            }
            return remove;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean removeAll = this.c.removeAll(collection);
            if (!removeAll) {
                return removeAll;
            }
            R14.access$212(R14.this, this.c.size() - size);
            f();
            return removeAll;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            I14.l(collection);
            int size = size();
            boolean retainAll = this.c.retainAll(collection);
            if (retainAll) {
                R14.access$212(R14.this, this.c.size() - size);
                f();
            }
            return retainAll;
        }

        @DexIgnore
        public int size() {
            e();
            return this.c.size();
        }

        @DexIgnore
        public String toString() {
            e();
            return this.c.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji extends R14<K, V>.i implements List<V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends R14<K, V>.i.a implements ListIterator<V> {
            @DexIgnore
            public Aii() {
                super();
            }

            @DexIgnore
            public Aii(int i) {
                super(Ji.this.g().listIterator(i));
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public void add(V v) {
                boolean isEmpty = Ji.this.isEmpty();
                c().add(v);
                R14.access$208(R14.this);
                if (isEmpty) {
                    Ji.this.a();
                }
            }

            @DexIgnore
            public final ListIterator<V> c() {
                return (ListIterator) a();
            }

            @DexIgnore
            public boolean hasPrevious() {
                return c().hasPrevious();
            }

            @DexIgnore
            public int nextIndex() {
                return c().nextIndex();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public V previous() {
                return c().previous();
            }

            @DexIgnore
            public int previousIndex() {
                return c().previousIndex();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public void set(V v) {
                c().set(v);
            }
        }

        @DexIgnore
        public Ji(K k, List<V> list, R14<K, V>.i iVar) {
            super(k, list, iVar);
        }

        @DexIgnore
        @Override // java.util.List
        public void add(int i, V v) {
            e();
            boolean isEmpty = c().isEmpty();
            g().add(i, v);
            R14.access$208(R14.this);
            if (isEmpty) {
                a();
            }
        }

        @DexIgnore
        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = g().addAll(i, collection);
            if (!addAll) {
                return addAll;
            }
            R14.access$212(R14.this, c().size() - size);
            if (size != 0) {
                return addAll;
            }
            a();
            return addAll;
        }

        @DexIgnore
        public List<V> g() {
            return (List) c();
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            e();
            return g().get(i);
        }

        @DexIgnore
        public int indexOf(Object obj) {
            e();
            return g().indexOf(obj);
        }

        @DexIgnore
        public int lastIndexOf(Object obj) {
            e();
            return g().lastIndexOf(obj);
        }

        @DexIgnore
        @Override // java.util.List
        public ListIterator<V> listIterator() {
            e();
            return new Aii();
        }

        @DexIgnore
        @Override // java.util.List
        public ListIterator<V> listIterator(int i) {
            e();
            return new Aii(i);
        }

        @DexIgnore
        @Override // java.util.List
        public V remove(int i) {
            e();
            V remove = g().remove(i);
            R14.access$210(R14.this);
            f();
            return remove;
        }

        @DexIgnore
        @Override // java.util.List
        public V set(int i, V v) {
            e();
            return g().set(i, v);
        }

        @DexIgnore
        @Override // java.util.List
        public List<V> subList(int i, int i2) {
            e();
            R14 r14 = R14.this;
            Object d = d();
            List<V> subList = g().subList(i, i2);
            if (b() != null) {
                this = b();
            }
            return r14.d(d, subList, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ki extends R14<K, V>.i implements Set<V> {
        @DexIgnore
        public Ki(K k, Set<V> set) {
            super(k, set, null);
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean f = X44.f((Set) this.c, collection);
            if (!f) {
                return f;
            }
            R14.access$212(R14.this, this.c.size() - size);
            f();
            return f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Li extends R14<K, V>.i implements SortedSet<V> {
        @DexIgnore
        public Li(K k, SortedSet<V> sortedSet, R14<K, V>.i iVar) {
            super(k, sortedSet, iVar);
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public Comparator<? super V> comparator() {
            return g().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public V first() {
            e();
            return g().first();
        }

        @DexIgnore
        public SortedSet<V> g() {
            return (SortedSet) c();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<V> headSet(V v) {
            e();
            R14 r14 = R14.this;
            Object d = d();
            SortedSet<V> headSet = g().headSet(v);
            if (b() != null) {
                this = b();
            }
            return new Li(d, headSet, this);
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public V last() {
            e();
            return g().last();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<V> subSet(V v, V v2) {
            e();
            R14 r14 = R14.this;
            Object d = d();
            SortedSet<V> subSet = g().subSet(v, v2);
            if (b() != null) {
                this = b();
            }
            return new Li(d, subSet, this);
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<V> tailSet(V v) {
            e();
            R14 r14 = R14.this;
            Object d = d();
            SortedSet<V> tailSet = g().tailSet(v);
            if (b() != null) {
                this = b();
            }
            return new Li(d, tailSet, this);
        }
    }

    @DexIgnore
    public R14(Map<K, Collection<V>> map) {
        I14.d(map.isEmpty());
        this.g = map;
    }

    @DexIgnore
    public static /* synthetic */ int access$208(R14 r14) {
        int i = r14.h;
        r14.h = i + 1;
        return i;
    }

    @DexIgnore
    public static /* synthetic */ int access$210(R14 r14) {
        int i = r14.h;
        r14.h = i - 1;
        return i;
    }

    @DexIgnore
    public static /* synthetic */ int access$212(R14 r14, int i) {
        int i2 = r14.h + i;
        r14.h = i2;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$220(R14 r14, int i) {
        int i2 = r14.h - i;
        r14.h = i2;
        return i2;
    }

    @DexIgnore
    public final Collection<V> a(K k) {
        Collection<V> collection = this.g.get(k);
        if (collection != null) {
            return collection;
        }
        Collection<V> createCollection = createCollection(k);
        this.g.put(k, createCollection);
        return createCollection;
    }

    @DexIgnore
    public final Iterator<V> b(Collection<V> collection) {
        return collection instanceof List ? ((List) collection).listIterator() : collection.iterator();
    }

    @DexIgnore
    public Map<K, Collection<V>> backingMap() {
        return this.g;
    }

    @DexIgnore
    public final void c(Object obj) {
        Collection collection = (Collection) X34.o(this.g, obj);
        if (collection != null) {
            int size = collection.size();
            collection.clear();
            this.h -= size;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public void clear() {
        for (Collection<V> collection : this.g.values()) {
            collection.clear();
        }
        this.g.clear();
        this.h = 0;
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public boolean containsKey(Object obj) {
        return this.g.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Map<K, Collection<V>> createAsMap() {
        return this.g instanceof SortedMap ? new Gi((SortedMap) this.g) : new Ci(this.g);
    }

    @DexIgnore
    public abstract Collection<V> createCollection();

    @DexIgnore
    public Collection<V> createCollection(K k) {
        return createCollection();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Set<K> createKeySet() {
        return this.g instanceof SortedMap ? new Hi((SortedMap) this.g) : new Ei(this.g);
    }

    @DexIgnore
    public Collection<V> createUnmodifiableEmptyCollection() {
        return unmodifiableCollectionSubclass(createCollection());
    }

    @DexIgnore
    public final List<V> d(K k, List<V> list, R14<K, V>.i iVar) {
        return list instanceof RandomAccess ? new Fi(this, k, list, iVar) : new Ji(k, list, iVar);
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Iterator<Map.Entry<K, V>> entryIterator() {
        return new Bi(this);
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public Collection<V> get(K k) {
        Collection<V> collection = this.g.get(k);
        if (collection == null) {
            collection = createCollection(k);
        }
        return wrapCollection(k, collection);
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public boolean put(K k, V v) {
        Collection<V> collection = this.g.get(k);
        if (collection == null) {
            Collection<V> createCollection = createCollection(k);
            if (createCollection.add(v)) {
                this.h++;
                this.g.put(k, createCollection);
                return true;
            }
            throw new AssertionError("New Collection violated the Collection spec");
        } else if (!collection.add(v)) {
            return false;
        } else {
            this.h++;
            return true;
        }
    }

    @DexIgnore
    public Collection<V> removeAll(Object obj) {
        Collection<V> remove = this.g.remove(obj);
        if (remove == null) {
            return createUnmodifiableEmptyCollection();
        }
        Collection<V> createCollection = createCollection();
        createCollection.addAll(remove);
        this.h -= remove.size();
        remove.clear();
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Collection<V> replaceValues(K k, Iterable<? extends V> iterable) {
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext()) {
            return removeAll(k);
        }
        Collection<? extends V> a2 = a(k);
        Collection<V> createCollection = createCollection();
        createCollection.addAll(a2);
        this.h -= a2.size();
        a2.clear();
        while (it.hasNext()) {
            if (a2.add((Object) it.next())) {
                this.h++;
            }
        }
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    public final void setMap(Map<K, Collection<V>> map) {
        this.g = map;
        this.h = 0;
        for (Collection<V> collection : map.values()) {
            I14.d(!collection.isEmpty());
            this.h = collection.size() + this.h;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public int size() {
        return this.h;
    }

    @DexIgnore
    public Collection<V> unmodifiableCollectionSubclass(Collection<V> collection) {
        return collection instanceof SortedSet ? Collections.unmodifiableSortedSet((SortedSet) collection) : collection instanceof Set ? Collections.unmodifiableSet((Set) collection) : collection instanceof List ? Collections.unmodifiableList((List) collection) : Collections.unmodifiableCollection(collection);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Iterator<V> valueIterator() {
        return new Ai(this);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Collection<V> values() {
        return super.values();
    }

    @DexIgnore
    public Collection<V> wrapCollection(K k, Collection<V> collection) {
        return collection instanceof SortedSet ? new Li(k, (SortedSet) collection, null) : collection instanceof Set ? new Ki(k, (Set) collection) : collection instanceof List ? d(k, (List) collection, null) : new Ii(k, collection, null);
    }
}
