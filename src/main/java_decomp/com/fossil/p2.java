package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P2 implements Parcelable.Creator<Q2> {
    @DexIgnore
    public /* synthetic */ P2(Qg6 qg6) {
    }

    @DexIgnore
    public Q2 a(Parcel parcel) {
        return new Q2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Q2 createFromParcel(Parcel parcel) {
        return new Q2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Q2[] newArray(int i) {
        return new Q2[i];
    }
}
