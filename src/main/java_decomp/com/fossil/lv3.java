package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Lv3 extends IInterface {
    @DexIgnore
    void L0(Sv3 sv3) throws RemoteException;

    @DexIgnore
    void O(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void T1(Pv3 pv3) throws RemoteException;

    @DexIgnore
    void U2(List<Pv3> list) throws RemoteException;

    @DexIgnore
    void a1(Nv3 nv3) throws RemoteException;

    @DexIgnore
    void i0(Pv3 pv3) throws RemoteException;

    @DexIgnore
    void k2(Uv3 uv3) throws RemoteException;

    @DexIgnore
    void r(Ev3 ev3) throws RemoteException;

    @DexIgnore
    void t0(Av3 av3) throws RemoteException;
}
