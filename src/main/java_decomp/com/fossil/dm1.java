package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.F60;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Dm1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Fm1 b;
    @DexIgnore
    public /* final */ At1 c;
    @DexIgnore
    public Dt1 d;
    @DexIgnore
    public Et1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Dm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dm1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                Fm1 valueOf = Fm1.valueOf(readString);
                parcel.setDataPosition(0);
                switch (W8.a[valueOf.ordinal()]) {
                    case 1:
                        return F60.CREATOR.a(parcel);
                    case 2:
                        return Im1.CREATOR.a(parcel);
                    case 3:
                        return Jm1.CREATOR.a(parcel);
                    case 4:
                        return Gm1.CREATOR.a(parcel);
                    case 5:
                        return Cm1.CREATOR.a(parcel);
                    case 6:
                        return Km1.CREATOR.a(parcel);
                    case 7:
                        return Zl1.CREATOR.a(parcel);
                    case 8:
                        return Bm1.CREATOR.a(parcel);
                    case 9:
                        return Am1.CREATOR.a(parcel);
                    case 10:
                        return Hm1.CREATOR.a(parcel);
                    default:
                        throw new Kc6();
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dm1[] newArray(int i) {
            return new Dm1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Dm1(android.os.Parcel r6) {
        /*
            r5 = this;
            r4 = 0
            java.lang.String r0 = r6.readString()
            if (r0 == 0) goto L_0x004a
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            com.fossil.Fm1 r3 = com.fossil.Fm1.valueOf(r0)
            java.lang.Class<com.fossil.At1> r0 = com.fossil.At1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r6.readParcelable(r0)
            if (r0 == 0) goto L_0x0046
            com.fossil.At1 r0 = (com.fossil.At1) r0
            java.lang.Class<com.fossil.Dt1> r1 = com.fossil.Dt1.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r6.readParcelable(r1)
            if (r1 == 0) goto L_0x0042
            com.fossil.Dt1 r1 = (com.fossil.Dt1) r1
            java.lang.Class<com.fossil.Et1> r2 = com.fossil.Et1.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            android.os.Parcelable r2 = r6.readParcelable(r2)
            if (r2 == 0) goto L_0x003e
            com.fossil.Et1 r2 = (com.fossil.Et1) r2
            r5.<init>(r3, r0, r1, r2)
            return
        L_0x003e:
            com.mapped.Wg6.i()
            throw r4
        L_0x0042:
            com.mapped.Wg6.i()
            throw r4
        L_0x0046:
            com.mapped.Wg6.i()
            throw r4
        L_0x004a:
            com.mapped.Wg6.i()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Dm1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Dm1(Fm1 fm1, At1 at1, Dt1 dt1, Et1 et1) {
        this.b = fm1;
        this.c = at1;
        this.d = dt1;
        this.e = et1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Dm1(Fm1 fm1, At1 at1, Dt1 dt1, Et1 et1, int i) {
        this(fm1, (i & 2) != 0 ? new I90() : at1, (i & 4) != 0 ? new Dt1(0, 62) : dt1, (i & 8) != 0 ? new Et1(Et1.CREATOR.a()) : et1);
    }

    @DexIgnore
    public final At1 a() {
        return this.c;
    }

    @DexIgnore
    public final void a(Dt1 dt1) {
        this.d = dt1;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Dm1 dm1 = (Dm1) obj;
            return this.b == dm1.b && !(Wg6.a(this.c, dm1.c) ^ true) && !(Wg6.a(this.d, dm1.d) ^ true) && !(Wg6.a(this.e, dm1.e) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.Complication");
    }

    @DexIgnore
    public final Fm1 getId() {
        return this.b;
    }

    @DexIgnore
    public final Dt1 getPositionConfig() {
        return this.d;
    }

    @DexIgnore
    public final Et1 getThemeConfig() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public final JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.b.a()).put("pos", this.d.toJSONObject()).put("data", this.c.toJSONObject()).put("theme", this.e.toJSONObject());
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
