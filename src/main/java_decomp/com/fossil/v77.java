package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V77 implements U77 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<P77> b;
    @DexIgnore
    public /* final */ O77 c; // = new O77();
    @DexIgnore
    public /* final */ Vh d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<P77> {
        @DexIgnore
        public Ai(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, P77 p77) {
            if (p77.d() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, p77.d());
            }
            if (p77.b() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, p77.b());
            }
            if (p77.f() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, p77.f());
            }
            String b = V77.this.c.b(p77.c());
            if (b == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, b);
            }
            mi.bindLong(5, p77.g() ? 1 : 0);
            String f = V77.this.c.f(p77.e());
            if (f == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, f);
            }
            mi.bindLong(7, (long) V77.this.c.a(p77.a()));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, P77 p77) {
            a(mi, p77);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `wf_asset` (`id`,`category`,`name`,`data`,`isNew`,`meta`,`assetType`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(V77 v77, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE  FROM wf_asset WHERE assetType = ?";
        }
    }

    @DexIgnore
    public V77(Oh oh) {
        this.a = oh;
        this.b = new Ai(oh);
        this.d = new Bi(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.U77
    public void a(L77 l77) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.d.acquire();
        acquire.bindLong(1, (long) this.c.a(l77));
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.U77
    public List<P77> b(L77 l77) {
        Rh f = Rh.f("SELECT * FROM wf_asset WHERE assetType = ?", 1);
        f.bindLong(1, (long) this.c.a(l77));
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "category");
            int c4 = Dx0.c(b2, "name");
            int c5 = Dx0.c(b2, "data");
            int c6 = Dx0.c(b2, "isNew");
            int c7 = Dx0.c(b2, Constants.META);
            int c8 = Dx0.c(b2, "assetType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new P77(b2.getString(c2), b2.getString(c3), b2.getString(c4), this.c.h(b2.getString(c5)), b2.getInt(c6) != 0, this.c.e(b2.getString(c7)), this.c.g(b2.getInt(c8))));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.U77
    public P77 c(L77 l77, String str) {
        P77 p77 = null;
        boolean z = true;
        Rh f = Rh.f("SELECT * FROM wf_asset WHERE assetType = ? and name =?", 2);
        f.bindLong(1, (long) this.c.a(l77));
        if (str == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "category");
            int c4 = Dx0.c(b2, "name");
            int c5 = Dx0.c(b2, "data");
            int c6 = Dx0.c(b2, "isNew");
            int c7 = Dx0.c(b2, Constants.META);
            int c8 = Dx0.c(b2, "assetType");
            if (b2.moveToFirst()) {
                String string = b2.getString(c2);
                String string2 = b2.getString(c3);
                String string3 = b2.getString(c4);
                Q77 h = this.c.h(b2.getString(c5));
                if (b2.getInt(c6) == 0) {
                    z = false;
                }
                p77 = new P77(string, string2, string3, h, z, this.c.e(b2.getString(c7)), this.c.g(b2.getInt(c8)));
            }
            return p77;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.U77
    public Long[] insert(List<P77> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }
}
