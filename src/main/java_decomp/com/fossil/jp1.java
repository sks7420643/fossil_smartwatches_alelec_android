package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ rn1 d;
    @DexIgnore
    public /* final */ gp1 e;
    @DexIgnore
    public /* final */ ip1[] f;
    @DexIgnore
    public /* final */ hp1[] g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jp1 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    rn1 valueOf = rn1.valueOf(readString2);
                    Parcelable readParcelable = parcel.readParcelable(gp1.class.getClassLoader());
                    if (readParcelable != null) {
                        gp1 gp1 = (gp1) readParcelable;
                        Object[] createTypedArray = parcel.createTypedArray(ip1.CREATOR);
                        if (createTypedArray != null) {
                            pq7.b(createTypedArray, "parcel.createTypedArray(\u2026erHourForecast.CREATOR)!!");
                            ip1[] ip1Arr = (ip1[]) createTypedArray;
                            Object[] createTypedArray2 = parcel.createTypedArray(hp1.CREATOR);
                            if (createTypedArray2 != null) {
                                pq7.b(createTypedArray2, "parcel.createTypedArray(\u2026herDayForecast.CREATOR)!!");
                                return new jp1(readLong, readString, valueOf, gp1, ip1Arr, (hp1[]) createTypedArray2);
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jp1[] newArray(int i) {
            return new jp1[i];
        }
    }

    @DexIgnore
    public jp1(long j, String str, rn1 rn1, gp1 gp1, ip1[] ip1Arr, hp1[] hp1Arr) throws IllegalArgumentException {
        this.b = j;
        this.c = str;
        this.d = rn1;
        this.e = gp1;
        this.f = ip1Arr;
        this.g = hp1Arr;
        if (ip1Arr.length < 3) {
            throw new IllegalArgumentException("hourForecast must have at least 3 elements.");
        } else if (hp1Arr.length < 3) {
            throw new IllegalArgumentException("dayForecast must have at least 3 elements.");
        }
    }

    @DexIgnore
    public final JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < 3; i++) {
            jSONArray.put(this.f[i].a());
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i2 = 0; i2 < 3; i2++) {
            jSONArray2.put(this.g[i2].a());
        }
        JSONObject put = new JSONObject().put("alive", this.b).put("city", this.c).put(Constants.PROFILE_KEY_UNIT, ey1.a(this.d));
        pq7.b(put, "JSONObject().put(UIScrip\u2026ratureUnit.lowerCaseName)");
        JSONObject put2 = gy1.c(put, this.e.a()).put("forecast_day", jSONArray).put("forecast_week", jSONArray2);
        pq7.b(put2, "JSONObject().put(UIScrip\u2026ST_WEEK, dayForecastJSON)");
        return put2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(jp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            jp1 jp1 = (jp1) obj;
            if (this.b != jp1.b) {
                return false;
            }
            if (this.d != jp1.d) {
                return false;
            }
            if (!pq7.a(this.c, jp1.c)) {
                return false;
            }
            if (!pq7.a(this.e, jp1.e)) {
                return false;
            }
            if (!Arrays.equals(this.f, jp1.f)) {
                return false;
            }
            return Arrays.equals(this.g, jp1.g);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherInfo");
    }

    @DexIgnore
    public final gp1 getCurrentWeatherInfo() {
        return this.e;
    }

    @DexIgnore
    public final hp1[] getDayForecast() {
        return this.g;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.b;
    }

    @DexIgnore
    public final ip1[] getHourForecast() {
        return this.f;
    }

    @DexIgnore
    public final String getLocation() {
        return this.c;
    }

    @DexIgnore
    public final rn1 getTemperatureUnit() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Long.valueOf(this.b).hashCode();
        int hashCode2 = this.d.hashCode();
        return (((((((((hashCode * 31) + hashCode2) * 31) + this.c.hashCode()) * 31) + this.e.hashCode()) * 31) + Arrays.hashCode(this.f)) * 31) + Arrays.hashCode(this.g);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.M2, Long.valueOf(this.b)), jd0.K, this.c), jd0.u, ey1.a(this.d)), jd0.c2, this.e.toJSONObject()), jd0.d2, px1.a(this.f)), jd0.e2, px1.a(this.g));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.f, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.g, i);
        }
    }
}
