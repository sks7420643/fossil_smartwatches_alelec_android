package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gl1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ R8 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Gl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Gl1[] a(byte[] bArr) {
            Parcelable a2;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < (bArr.length - 1) - 2) {
                R8 a3 = R8.g.a(bArr[i]);
                int i2 = i + 1;
                int i3 = i2 + 2;
                ByteBuffer order = ByteBuffer.wrap(Dm7.k(bArr, i2, i3)).order(ByteOrder.LITTLE_ENDIAN);
                Wg6.b(order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
                int n = Hy1.n(order.getShort());
                byte[] k = Dm7.k(bArr, i3, i3 + n);
                if (a3 == null) {
                    a2 = null;
                } else {
                    int i4 = P8.b[a3.ordinal()];
                    if (i4 == 1) {
                        a2 = Hl1.CREATOR.a(k);
                    } else if (i4 == 2) {
                        a2 = Ql1.CREATOR.a(k);
                    } else if (i4 == 3) {
                        a2 = Il1.CREATOR.a(k);
                    } else {
                        throw new Kc6();
                    }
                }
                if (a2 != null) {
                    arrayList.add(a2);
                }
                i += n + 3;
            }
            Object[] array = arrayList.toArray(new Gl1[0]);
            if (array != null) {
                return (Gl1[]) array;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gl1 createFromParcel(Parcel parcel) {
            R8 r8 = R8.values()[parcel.readInt()];
            parcel.setDataPosition(parcel.dataPosition() - 4);
            int i = P8.a[r8.ordinal()];
            if (i == 1) {
                return Hl1.CREATOR.b(parcel);
            }
            if (i == 2) {
                return Ql1.CREATOR.b(parcel);
            }
            if (i == 3) {
                return Il1.CREATOR.b(parcel);
            }
            throw new Kc6();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gl1[] newArray(int i) {
            return new Gl1[i];
        }
    }

    @DexIgnore
    public Gl1(R8 r8) {
        this.b = r8;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] b2 = b();
        byte[] array = ByteBuffer.allocate(b2.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.b.b).putShort((short) b2.length).put(b2).array();
        Wg6.b(array, "ByteBuffer.allocate(SUB_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((Gl1) obj).b;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.AlarmSubEntry");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(new JSONObject(), Jd0.e, Ey1.a(this.b));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
    }
}
