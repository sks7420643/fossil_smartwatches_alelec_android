package com.fossil;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mj1 extends Nj1<Drawable> {
    @DexIgnore
    public Mj1(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Nj1
    public /* bridge */ /* synthetic */ void o(Drawable drawable) {
        q(drawable);
    }

    @DexIgnore
    public void q(Drawable drawable) {
        ((ImageView) this.b).setImageDrawable(drawable);
    }
}
