package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zq1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ox1 implements Parcelable {
        @DexIgnore
        public static /* final */ Aii CREATOR; // = new Aii(null);
        @DexIgnore
        public /* final */ Nn1 b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ byte[] d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Parcelable.Creator<Ai> {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public Ai createFromParcel(Parcel parcel) {
                Nn1 nn1 = Nn1.values()[parcel.readInt()];
                long readLong = parcel.readLong();
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    Wg6.b(createByteArray, "parcel.createByteArray()!!");
                    return new Ai(nn1, readLong, createByteArray);
                }
                Wg6.i();
                throw null;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public Ai[] newArray(int i) {
                return new Ai[i];
            }
        }

        @DexIgnore
        public Ai(Nn1 nn1, long j, byte[] bArr) {
            this.b = nn1;
            this.c = j;
            this.d = bArr;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public final Nn1 getChannel() {
            return this.b;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.d;
        }

        @DexIgnore
        public final long getTimestampInMs() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.Ox1
        public JSONObject toJSONObject() {
            return G80.k(G80.k(G80.k(new JSONObject(), Jd0.D2, Ey1.a(this.b)), Jd0.Q0, Double.valueOf(Hy1.f(this.c))), Jd0.Y2, Dy1.e(this.d, null, 1, null));
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.b.ordinal());
            parcel.writeLong(this.c);
            parcel.writeByteArray(this.d);
        }
    }
}
