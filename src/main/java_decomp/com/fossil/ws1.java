package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ws1 extends Ox1 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Zs1[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ws1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ws1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    Object[] createTypedArray = parcel.createTypedArray(Zs1.CREATOR);
                    if (createTypedArray != null) {
                        Wg6.b(createTypedArray, "parcel.createTypedArray(Player.CREATOR)!!");
                        return new Ws1(readString, readString2, (Zs1[]) createTypedArray);
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ws1[] newArray(int i) {
            return new Ws1[i];
        }
    }

    @DexIgnore
    public Ws1(String str, String str2, Zs1[] zs1Arr) {
        this.b = str;
        this.c = str2;
        this.d = zs1Arr;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject k = G80.k(G80.k(G80.k(new JSONObject(), Jd0.D4, this.b), Jd0.H, this.c), Jd0.H4, Integer.valueOf(this.d.length));
        G80.k(k, Jd0.F4, Px1.a(this.d));
        return k;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.b;
    }

    @DexIgnore
    public final String getName() {
        return this.c;
    }

    @DexIgnore
    public final Zs1[] getPlayers() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(G80.k(G80.k(new JSONObject(), Jd0.D4, this.b), Jd0.H, this.c), Jd0.H4, Integer.valueOf(this.d.length));
        G80.k(k, Jd0.F4, Px1.a(this.d));
        return k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeTypedArray(this.d, i);
    }
}
