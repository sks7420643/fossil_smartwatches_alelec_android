package com.fossil;

import android.content.Context;
import com.mapped.Qg6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mv5 {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ArrayList<String> a(Context context, BarChart.c cVar) {
            Wg6.c(context, "context");
            Wg6.c(cVar, "chartModel");
            ArrayList<String> arrayList = new ArrayList<>();
            Calendar instance = Calendar.getInstance();
            for (T t : cVar.b()) {
                if (t.c() > 0) {
                    Wg6.b(instance, "calendar");
                    instance.setTimeInMillis(t.c());
                    Boolean p0 = TimeUtils.p0(instance.getTime());
                    Wg6.b(p0, "DateHelper.isToday(calendar.time)");
                    if (p0.booleanValue()) {
                        arrayList.add(Um5.c(context, 2131886648));
                    } else {
                        switch (instance.get(7)) {
                            case 1:
                                arrayList.add(Um5.c(context, 2131886770));
                                continue;
                            case 2:
                                arrayList.add(Um5.c(context, 2131886769));
                                continue;
                            case 3:
                                arrayList.add(Um5.c(context, 2131886772));
                                continue;
                            case 4:
                                arrayList.add(Um5.c(context, 2131886774));
                                continue;
                            case 5:
                                arrayList.add(Um5.c(context, 2131886773));
                                continue;
                            case 6:
                                arrayList.add(Um5.c(context, 2131886768));
                                continue;
                            case 7:
                                arrayList.add(Um5.c(context, 2131886771));
                                continue;
                        }
                    }
                } else {
                    arrayList.add("");
                }
            }
            return arrayList;
        }

        @DexIgnore
        public final int b(int i) {
            int i2 = (int) (((double) i) * 1.15d);
            return (i2 >= 0 && 9 >= i2) ? i2 % 2 != 0 ? ((i2 / 2) * 2) + 2 : i2 : (10 <= i2 && 99 >= i2) ? i2 % 10 != 0 ? ((i2 / 10) * 10) + 10 : i2 : (100 <= i2 && 999 >= i2) ? i2 % 20 != 0 ? ((i2 / 20) * 20) + 20 : i2 : i2 % 200 != 0 ? ((i2 / 200) * 200) + 200 : i2;
        }
    }
}
