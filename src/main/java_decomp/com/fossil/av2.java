package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.E13;
import com.fossil.Ev2;
import com.fossil.Wu2;
import com.sina.weibo.sdk.api.ImageObject;
import java.util.Collections;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Av2 extends E13<Av2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Av2 zzaw;
    @DexIgnore
    public static volatile Z23<Av2> zzax;
    @DexIgnore
    public int zzaa;
    @DexIgnore
    public String zzab; // = "";
    @DexIgnore
    public String zzac; // = "";
    @DexIgnore
    public boolean zzad;
    @DexIgnore
    public M13<Uu2> zzae; // = E13.B();
    @DexIgnore
    public String zzaf; // = "";
    @DexIgnore
    public int zzag;
    @DexIgnore
    public int zzah;
    @DexIgnore
    public int zzai;
    @DexIgnore
    public String zzaj; // = "";
    @DexIgnore
    public long zzak;
    @DexIgnore
    public long zzal;
    @DexIgnore
    public String zzam; // = "";
    @DexIgnore
    public String zzan; // = "";
    @DexIgnore
    public int zzao;
    @DexIgnore
    public String zzap; // = "";
    @DexIgnore
    public Bv2 zzaq;
    @DexIgnore
    public K13 zzar; // = E13.y();
    @DexIgnore
    public long zzas;
    @DexIgnore
    public long zzat;
    @DexIgnore
    public String zzau; // = "";
    @DexIgnore
    public String zzav; // = "";
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public int zze;
    @DexIgnore
    public M13<Wu2> zzf; // = E13.B();
    @DexIgnore
    public M13<Ev2> zzg; // = E13.B();
    @DexIgnore
    public long zzh;
    @DexIgnore
    public long zzi;
    @DexIgnore
    public long zzj;
    @DexIgnore
    public long zzk;
    @DexIgnore
    public long zzl;
    @DexIgnore
    public String zzm; // = "";
    @DexIgnore
    public String zzn; // = "";
    @DexIgnore
    public String zzo; // = "";
    @DexIgnore
    public String zzp; // = "";
    @DexIgnore
    public int zzq;
    @DexIgnore
    public String zzr; // = "";
    @DexIgnore
    public String zzs; // = "";
    @DexIgnore
    public String zzt; // = "";
    @DexIgnore
    public long zzu;
    @DexIgnore
    public long zzv;
    @DexIgnore
    public String zzw; // = "";
    @DexIgnore
    public boolean zzx;
    @DexIgnore
    public String zzy; // = "";
    @DexIgnore
    public long zzz;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Av2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Av2.zzaw);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai A0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).h2(str);
            return this;
        }

        @DexIgnore
        public final Ai B(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).G(j);
            return this;
        }

        @DexIgnore
        public final Ai B0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).k2(j);
            return this;
        }

        @DexIgnore
        public final Ai C(Wu2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).H((Wu2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai C0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).n2(str);
            return this;
        }

        @DexIgnore
        public final String D0() {
            return ((Av2) this.c).j0();
        }

        @DexIgnore
        public final Ai E(Ev2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).S((Ev2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai E0() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).U0();
            return this;
        }

        @DexIgnore
        public final Ai F0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).p2(str);
            return this;
        }

        @DexIgnore
        public final Ai G(Ev2 ev2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).S(ev2);
            return this;
        }

        @DexIgnore
        public final Ai G0() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).V0();
            return this;
        }

        @DexIgnore
        public final Ai H(Iterable<? extends Wu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).T(iterable);
            return this;
        }

        @DexIgnore
        public final Ai H0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).t2(null);
            return this;
        }

        @DexIgnore
        public final Ai I(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).U(str);
            return this;
        }

        @DexIgnore
        public final Ai I0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).v2(str);
            return this;
        }

        @DexIgnore
        public final Ai J(boolean z) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).V(z);
            return this;
        }

        @DexIgnore
        public final String J0() {
            return ((Av2) this.c).L0();
        }

        @DexIgnore
        public final List<Wu2> K() {
            return Collections.unmodifiableList(((Av2) this.c).W0());
        }

        @DexIgnore
        public final Ai K0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).z2(str);
            return this;
        }

        @DexIgnore
        public final int M() {
            return ((Av2) this.c).f1();
        }

        @DexIgnore
        public final Wu2 N(int i) {
            return ((Av2) this.c).C(i);
        }

        @DexIgnore
        public final Ai O(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).z0(j);
            return this;
        }

        @DexIgnore
        public final Ai P(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).F0(str);
            return this;
        }

        @DexIgnore
        public final Ai Q(boolean z) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).G0(z);
            return this;
        }

        @DexIgnore
        public final Ai R() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).P0();
            return this;
        }

        @DexIgnore
        public final Ai S(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).q1(i);
            return this;
        }

        @DexIgnore
        public final Ai T(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).X0(j);
            return this;
        }

        @DexIgnore
        public final Ai U(Iterable<? extends Uu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).d1(iterable);
            return this;
        }

        @DexIgnore
        public final Ai V(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).e1(str);
            return this;
        }

        @DexIgnore
        public final Ai W(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).h1(j);
            return this;
        }

        @DexIgnore
        public final Ai X(Iterable<? extends Integer> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).n1(iterable);
            return this;
        }

        @DexIgnore
        public final Ai Y(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).o1(str);
            return this;
        }

        @DexIgnore
        public final Ev2 Z(int i) {
            return ((Av2) this.c).y0(i);
        }

        @DexIgnore
        public final List<Ev2> a0() {
            return Collections.unmodifiableList(((Av2) this.c).p1());
        }

        @DexIgnore
        public final int b0() {
            return ((Av2) this.c).x1();
        }

        @DexIgnore
        public final Ai c0(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).y1(i);
            return this;
        }

        @DexIgnore
        public final Ai d0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).r1(j);
            return this;
        }

        @DexIgnore
        public final Ai e0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).w1(str);
            return this;
        }

        @DexIgnore
        public final long f0() {
            return ((Av2) this.c).Y1();
        }

        @DexIgnore
        public final Ai g0(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).F1(i);
            return this;
        }

        @DexIgnore
        public final Ai h0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).z1(j);
            return this;
        }

        @DexIgnore
        public final Ai i0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).E1(str);
            return this;
        }

        @DexIgnore
        public final long j0() {
            return ((Av2) this.c).j2();
        }

        @DexIgnore
        public final Ai k0(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).N1(i);
            return this;
        }

        @DexIgnore
        public final Ai l0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).G1(j);
            return this;
        }

        @DexIgnore
        public final Ai m0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).K1(str);
            return this;
        }

        @DexIgnore
        public final Ai n0() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).R0();
            return this;
        }

        @DexIgnore
        public final Ai o0(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).S1(i);
            return this;
        }

        @DexIgnore
        public final Ai p0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).O1(j);
            return this;
        }

        @DexIgnore
        public final Ai q0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).R1(str);
            return this;
        }

        @DexIgnore
        public final Ai r0() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).S0();
            return this;
        }

        @DexIgnore
        public final Ai s0(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).Z1(i);
            return this;
        }

        @DexIgnore
        public final Ai t0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).T1(j);
            return this;
        }

        @DexIgnore
        public final Ai u0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).W1(str);
            return this;
        }

        @DexIgnore
        public final Ai v0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).a2(j);
            return this;
        }

        @DexIgnore
        public final Ai w0(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).d2(str);
            return this;
        }

        @DexIgnore
        public final Ai x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).g1(1);
            return this;
        }

        @DexIgnore
        public final String x0() {
            return ((Av2) this.c).H2();
        }

        @DexIgnore
        public final Ai y(int i, Wu2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).D(i, (Wu2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai y0() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).T0();
            return this;
        }

        @DexIgnore
        public final Ai z(int i, Ev2 ev2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).E(i, ev2);
            return this;
        }

        @DexIgnore
        public final Ai z0(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Av2) this.c).e2(j);
            return this;
        }
    }

    /*
    static {
        Av2 av2 = new Av2();
        zzaw = av2;
        E13.u(Av2.class, av2);
    }
    */

    @DexIgnore
    public static Ai M0() {
        return (Ai) zzaw.w();
    }

    @DexIgnore
    public final String A2() {
        return this.zzm;
    }

    @DexIgnore
    public final String B2() {
        return this.zzn;
    }

    @DexIgnore
    public final Wu2 C(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final String C2() {
        return this.zzo;
    }

    @DexIgnore
    public final void D(int i, Wu2 wu2) {
        wu2.getClass();
        O0();
        this.zzf.set(i, wu2);
    }

    @DexIgnore
    public final String D2() {
        return this.zzp;
    }

    @DexIgnore
    public final void E(int i, Ev2 ev2) {
        ev2.getClass();
        Q0();
        this.zzg.set(i, ev2);
    }

    @DexIgnore
    public final void E1(String str) {
        str.getClass();
        this.zzc |= 4096;
        this.zzs = str;
    }

    @DexIgnore
    public final boolean E2() {
        return (this.zzc & 1024) != 0;
    }

    @DexIgnore
    public final void F0(String str) {
        str.getClass();
        this.zzc |= 128;
        this.zzn = str;
    }

    @DexIgnore
    public final void F1(int i) {
        this.zzc |= 1024;
        this.zzq = i;
    }

    @DexIgnore
    public final int F2() {
        return this.zzq;
    }

    @DexIgnore
    public final void G(long j) {
        this.zzc |= 2;
        this.zzh = j;
    }

    @DexIgnore
    public final void G0(boolean z) {
        this.zzc |= 8388608;
        this.zzad = z;
    }

    @DexIgnore
    public final void G1(long j) {
        this.zzc |= 32768;
        this.zzv = j;
    }

    @DexIgnore
    public final String G2() {
        return this.zzr;
    }

    @DexIgnore
    public final void H(Wu2 wu2) {
        wu2.getClass();
        O0();
        this.zzf.add(wu2);
    }

    @DexIgnore
    public final int H0() {
        return this.zzao;
    }

    @DexIgnore
    public final String H2() {
        return this.zzs;
    }

    @DexIgnore
    public final String I0() {
        return this.zzap;
    }

    @DexIgnore
    public final String I2() {
        return this.zzt;
    }

    @DexIgnore
    public final boolean J0() {
        return (this.zzd & 16) != 0;
    }

    @DexIgnore
    public final boolean J2() {
        return (this.zzc & 16384) != 0;
    }

    @DexIgnore
    public final long K0() {
        return this.zzas;
    }

    @DexIgnore
    public final void K1(String str) {
        str.getClass();
        this.zzc |= 8192;
        this.zzt = str;
    }

    @DexIgnore
    public final String L0() {
        return this.zzau;
    }

    @DexIgnore
    public final boolean L1() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long M1() {
        return this.zzh;
    }

    @DexIgnore
    public final void N1(int i) {
        this.zzc |= 1048576;
        this.zzaa = i;
    }

    @DexIgnore
    public final void O0() {
        M13<Wu2> m13 = this.zzf;
        if (!m13.zza()) {
            this.zzf = E13.q(m13);
        }
    }

    @DexIgnore
    public final void O1(long j) {
        this.zzc |= 524288;
        this.zzz = j;
    }

    @DexIgnore
    public final void P0() {
        this.zzf = E13.B();
    }

    @DexIgnore
    public final void Q0() {
        M13<Ev2> m13 = this.zzg;
        if (!m13.zza()) {
            this.zzg = E13.q(m13);
        }
    }

    @DexIgnore
    public final void R0() {
        this.zzc &= -17;
        this.zzk = 0;
    }

    @DexIgnore
    public final void R1(String str) {
        str.getClass();
        this.zzc |= 65536;
        this.zzw = str;
    }

    @DexIgnore
    public final void S(Ev2 ev2) {
        ev2.getClass();
        Q0();
        this.zzg.add(ev2);
    }

    @DexIgnore
    public final void S0() {
        this.zzc &= -33;
        this.zzl = 0;
    }

    @DexIgnore
    public final void S1(int i) {
        this.zzc |= 33554432;
        this.zzag = i;
    }

    @DexIgnore
    public final void T(Iterable<? extends Wu2> iterable) {
        O0();
        Nz2.a(iterable, this.zzf);
    }

    @DexIgnore
    public final void T0() {
        this.zzc &= -2097153;
        this.zzab = zzaw.zzab;
    }

    @DexIgnore
    public final void T1(long j) {
        this.zzc |= 536870912;
        this.zzak = j;
    }

    @DexIgnore
    public final void U(String str) {
        str.getClass();
        this.zzc |= 64;
        this.zzm = str;
    }

    @DexIgnore
    public final void U0() {
        this.zzae = E13.B();
    }

    @DexIgnore
    public final void V(boolean z) {
        this.zzc |= 131072;
        this.zzx = z;
    }

    @DexIgnore
    public final void V0() {
        this.zzc &= Integer.MAX_VALUE;
        this.zzam = zzaw.zzam;
    }

    @DexIgnore
    public final boolean W() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final List<Wu2> W0() {
        return this.zzf;
    }

    @DexIgnore
    public final void W1(String str) {
        str.getClass();
        this.zzc |= 262144;
        this.zzy = str;
    }

    @DexIgnore
    public final long X() {
        return this.zzu;
    }

    @DexIgnore
    public final void X0(long j) {
        this.zzc |= 8;
        this.zzj = j;
    }

    @DexIgnore
    public final boolean X1() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean Y() {
        return (this.zzc & 32768) != 0;
    }

    @DexIgnore
    public final long Y1() {
        return this.zzi;
    }

    @DexIgnore
    public final long Z() {
        return this.zzv;
    }

    @DexIgnore
    public final void Z1(int i) {
        this.zzd |= 2;
        this.zzao = i;
    }

    @DexIgnore
    public final String a0() {
        return this.zzw;
    }

    @DexIgnore
    public final void a2(long j) {
        this.zzc |= 1073741824;
        this.zzal = j;
    }

    @DexIgnore
    public final boolean b0() {
        return (this.zzc & 131072) != 0;
    }

    @DexIgnore
    public final boolean c0() {
        return this.zzx;
    }

    @DexIgnore
    public final String d0() {
        return this.zzy;
    }

    @DexIgnore
    public final void d1(Iterable<? extends Uu2> iterable) {
        M13<Uu2> m13 = this.zzae;
        if (!m13.zza()) {
            this.zzae = E13.q(m13);
        }
        Nz2.a(iterable, this.zzae);
    }

    @DexIgnore
    public final void d2(String str) {
        str.getClass();
        this.zzc |= ImageObject.DATA_SIZE;
        this.zzab = str;
    }

    @DexIgnore
    public final boolean e0() {
        return (this.zzc & 524288) != 0;
    }

    @DexIgnore
    public final void e1(String str) {
        str.getClass();
        this.zzc |= 256;
        this.zzo = str;
    }

    @DexIgnore
    public final void e2(long j) {
        this.zzd |= 16;
        this.zzas = j;
    }

    @DexIgnore
    public final long f0() {
        return this.zzz;
    }

    @DexIgnore
    public final int f1() {
        return this.zzf.size();
    }

    @DexIgnore
    public final boolean g0() {
        return (this.zzc & 1048576) != 0;
    }

    @DexIgnore
    public final void g1(int i) {
        this.zzc |= 1;
        this.zze = i;
    }

    @DexIgnore
    public final int h0() {
        return this.zzaa;
    }

    @DexIgnore
    public final void h1(long j) {
        this.zzc |= 16;
        this.zzk = j;
    }

    @DexIgnore
    public final void h2(String str) {
        str.getClass();
        this.zzc |= 4194304;
        this.zzac = str;
    }

    @DexIgnore
    public final String i0() {
        return this.zzab;
    }

    @DexIgnore
    public final boolean i2() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String j0() {
        return this.zzac;
    }

    @DexIgnore
    public final long j2() {
        return this.zzj;
    }

    @DexIgnore
    public final boolean k0() {
        return (this.zzc & 8388608) != 0;
    }

    @DexIgnore
    public final void k2(long j) {
        this.zzd |= 32;
        this.zzat = j;
    }

    @DexIgnore
    public final boolean l0() {
        return this.zzad;
    }

    @DexIgnore
    public final List<Uu2> m0() {
        return this.zzae;
    }

    @DexIgnore
    public final String n0() {
        return this.zzaf;
    }

    @DexIgnore
    public final void n1(Iterable<? extends Integer> iterable) {
        K13 k13 = this.zzar;
        if (!k13.zza()) {
            int size = k13.size();
            this.zzar = k13.zzb(size == 0 ? 10 : size << 1);
        }
        Nz2.a(iterable, this.zzar);
    }

    @DexIgnore
    public final void n2(String str) {
        str.getClass();
        this.zzc |= 16777216;
        this.zzaf = str;
    }

    @DexIgnore
    public final boolean o0() {
        return (this.zzc & 33554432) != 0;
    }

    @DexIgnore
    public final void o1(String str) {
        str.getClass();
        this.zzc |= 512;
        this.zzp = str;
    }

    @DexIgnore
    public final int p0() {
        return this.zzag;
    }

    @DexIgnore
    public final List<Ev2> p1() {
        return this.zzg;
    }

    @DexIgnore
    public final void p2(String str) {
        str.getClass();
        this.zzc |= SQLiteDatabase.CREATE_IF_NECESSARY;
        this.zzaj = str;
    }

    @DexIgnore
    public final String q0() {
        return this.zzaj;
    }

    @DexIgnore
    public final void q1(int i) {
        O0();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final boolean q2() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Av2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzaw, "\u0001,\u0000\u0002\u00014,\u0000\u0004\u0000\u0001\u1004\u0000\u0002\u001b\u0003\u001b\u0004\u1002\u0001\u0005\u1002\u0002\u0006\u1002\u0003\u0007\u1002\u0005\b\u1008\u0006\t\u1008\u0007\n\u1008\b\u000b\u1008\t\f\u1004\n\r\u1008\u000b\u000e\u1008\f\u0010\u1008\r\u0011\u1002\u000e\u0012\u1002\u000f\u0013\u1008\u0010\u0014\u1007\u0011\u0015\u1008\u0012\u0016\u1002\u0013\u0017\u1004\u0014\u0018\u1008\u0015\u0019\u1008\u0016\u001a\u1002\u0004\u001c\u1007\u0017\u001d\u001b\u001e\u1008\u0018\u001f\u1004\u0019 \u1004\u001a!\u1004\u001b\"\u1008\u001c#\u1002\u001d$\u1002\u001e%\u1008\u001f&\u1008 '\u1004!)\u1008\",\u1009#-\u001d.\u1002$/\u1002%2\u1008&4\u1008'", new Object[]{"zzc", "zzd", "zze", "zzf", Wu2.class, "zzg", Ev2.class, "zzh", "zzi", "zzj", "zzl", "zzm", "zzn", "zzo", "zzp", "zzq", "zzr", "zzs", "zzt", "zzu", "zzv", "zzw", "zzx", "zzy", "zzz", "zzaa", "zzab", "zzac", "zzk", "zzad", "zzae", Uu2.class, "zzaf", "zzag", "zzah", "zzai", "zzaj", "zzak", "zzal", "zzam", "zzan", "zzao", "zzap", "zzaq", "zzar", "zzas", "zzat", "zzau", "zzav"});
            case 4:
                return zzaw;
            case 5:
                Z23<Av2> z232 = zzax;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Av2.class) {
                    try {
                        z23 = zzax;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzaw);
                            zzax = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final boolean r0() {
        return (this.zzc & 536870912) != 0;
    }

    @DexIgnore
    public final void r1(long j) {
        this.zzc |= 32;
        this.zzl = j;
    }

    @DexIgnore
    public final long r2() {
        return this.zzk;
    }

    @DexIgnore
    public final long s0() {
        return this.zzak;
    }

    @DexIgnore
    public final boolean t0() {
        return (this.zzc & 1073741824) != 0;
    }

    @DexIgnore
    public final void t2(String str) {
        str.getClass();
        this.zzc |= RecyclerView.UNDEFINED_DURATION;
        this.zzam = str;
    }

    @DexIgnore
    public final long u0() {
        return this.zzal;
    }

    @DexIgnore
    public final String v0() {
        return this.zzam;
    }

    @DexIgnore
    public final void v2(String str) {
        str.getClass();
        this.zzd |= 4;
        this.zzap = str;
    }

    @DexIgnore
    public final boolean w0() {
        return (this.zzd & 2) != 0;
    }

    @DexIgnore
    public final void w1(String str) {
        str.getClass();
        this.zzc |= 2048;
        this.zzr = str;
    }

    @DexIgnore
    public final boolean w2() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final int x0() {
        return this.zze;
    }

    @DexIgnore
    public final int x1() {
        return this.zzg.size();
    }

    @DexIgnore
    public final long x2() {
        return this.zzl;
    }

    @DexIgnore
    public final Ev2 y0(int i) {
        return this.zzg.get(i);
    }

    @DexIgnore
    public final void y1(int i) {
        Q0();
        this.zzg.remove(i);
    }

    @DexIgnore
    public final void z0(long j) {
        this.zzc |= 4;
        this.zzi = j;
    }

    @DexIgnore
    public final void z1(long j) {
        this.zzc |= 16384;
        this.zzu = j;
    }

    @DexIgnore
    public final void z2(String str) {
        str.getClass();
        this.zzd |= 64;
        this.zzau = str;
    }
}
