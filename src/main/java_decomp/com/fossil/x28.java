package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X28 extends W18 {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ K48 d;

    @DexIgnore
    public X28(String str, long j, K48 k48) {
        this.b = str;
        this.c = j;
        this.d = k48;
    }

    @DexIgnore
    @Override // com.fossil.W18
    public long contentLength() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.W18
    public R18 contentType() {
        String str = this.b;
        if (str != null) {
            return R18.d(str);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.W18
    public K48 source() {
        return this.d;
    }
}
