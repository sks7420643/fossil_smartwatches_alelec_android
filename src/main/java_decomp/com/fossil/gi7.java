package com.fossil;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gi7 implements FileFilter {
    @DexIgnore
    public boolean accept(File file) {
        return Pattern.matches("cpu[0-9]", file.getName());
    }
}
