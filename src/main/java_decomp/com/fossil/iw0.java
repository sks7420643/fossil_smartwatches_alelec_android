package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iw0<T> extends xw0 {
    @DexIgnore
    public iw0(qw0 qw0) {
        super(qw0);
    }

    @DexIgnore
    public abstract void bind(px0 px0, T t);

    @DexIgnore
    @Override // com.fossil.xw0
    public abstract String createQuery();

    @DexIgnore
    public final int handle(T t) {
        px0 acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.executeUpdateDelete();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.iw0<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final int handleMultiple(Iterable<? extends T> iterable) {
        px0 acquire = acquire();
        int i = 0;
        try {
            Iterator<? extends T> it = iterable.iterator();
            while (it.hasNext()) {
                bind(acquire, it.next());
                i += acquire.executeUpdateDelete();
            }
            return i;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(T[] tArr) {
        px0 acquire = acquire();
        try {
            int i = 0;
            for (T t : tArr) {
                bind(acquire, t);
                i += acquire.executeUpdateDelete();
            }
            return i;
        } finally {
            release(acquire);
        }
    }
}
