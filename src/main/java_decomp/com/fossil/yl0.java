package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.util.Log;
import com.fossil.Kl0;
import com.fossil.Zm0;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yl0 {
    @DexIgnore
    public ConcurrentHashMap<Long, Kl0.Bi> a; // = new ConcurrentHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Ci<Zm0.Fi> {
        @DexIgnore
        public Ai(Yl0 yl0) {
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Yl0.Ci
        public /* bridge */ /* synthetic */ int a(Zm0.Fi fi) {
            return c(fi);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Yl0.Ci
        public /* bridge */ /* synthetic */ boolean b(Zm0.Fi fi) {
            return d(fi);
        }

        @DexIgnore
        public int c(Zm0.Fi fi) {
            return fi.d();
        }

        @DexIgnore
        public boolean d(Zm0.Fi fi) {
            return fi.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Ci<Kl0.Ci> {
        @DexIgnore
        public Bi(Yl0 yl0) {
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Yl0.Ci
        public /* bridge */ /* synthetic */ int a(Kl0.Ci ci) {
            return c(ci);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Yl0.Ci
        public /* bridge */ /* synthetic */ boolean b(Kl0.Ci ci) {
            return d(ci);
        }

        @DexIgnore
        public int c(Kl0.Ci ci) {
            return ci.e();
        }

        @DexIgnore
        public boolean d(Kl0.Ci ci) {
            return ci.f();
        }
    }

    @DexIgnore
    public interface Ci<T> {
        @DexIgnore
        int a(T t);

        @DexIgnore
        boolean b(T t);
    }

    @DexIgnore
    public static <T> T g(T[] tArr, int i, Ci<T> ci) {
        int i2 = (i & 1) == 0 ? 400 : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (ci.b(t2) == z ? 0 : 1) + (Math.abs(ci.a(t2) - i2) * 2);
            if (t == null || i3 > abs) {
                i3 = abs;
                t = t2;
            }
        }
        return t;
    }

    @DexIgnore
    public static long j(Typeface typeface) {
        if (typeface == null) {
            return 0;
        }
        try {
            Field declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
            return ((Number) declaredField.get(typeface)).longValue();
        } catch (NoSuchFieldException e) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e);
            return 0;
        } catch (IllegalAccessException e2) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e2);
            return 0;
        }
    }

    @DexIgnore
    public final void a(Typeface typeface, Kl0.Bi bi) {
        long j = j(typeface);
        if (j != 0) {
            this.a.put(Long.valueOf(j), bi);
        }
    }

    @DexIgnore
    public Typeface b(Context context, Kl0.Bi bi, Resources resources, int i) {
        Kl0.Ci f = f(bi, i);
        if (f == null) {
            return null;
        }
        Typeface d = Sl0.d(context, resources, f.b(), f.a(), i);
        a(d, bi);
        return d;
    }

    @DexIgnore
    public Typeface c(Context context, CancellationSignal cancellationSignal, Zm0.Fi[] fiArr, int i) {
        InputStream inputStream;
        Typeface typeface = null;
        if (fiArr.length >= 1) {
            try {
                inputStream = context.getContentResolver().openInputStream(h(fiArr, i).c());
                try {
                    typeface = d(context, inputStream);
                    Zl0.a(inputStream);
                } catch (IOException e) {
                    Zl0.a(inputStream);
                    return typeface;
                } catch (Throwable th) {
                    th = th;
                    Zl0.a(inputStream);
                    throw th;
                }
            } catch (IOException e2) {
                inputStream = null;
                Zl0.a(inputStream);
                return typeface;
            } catch (Throwable th2) {
                th = th2;
                inputStream = null;
                Zl0.a(inputStream);
                throw th;
            }
        }
        return typeface;
    }

    @DexIgnore
    public Typeface d(Context context, InputStream inputStream) {
        File e = Zl0.e(context);
        if (e == null) {
            return null;
        }
        try {
            if (!Zl0.d(e, inputStream)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(e.getPath());
            e.delete();
            return createFromFile;
        } catch (RuntimeException e2) {
            return null;
        } finally {
            e.delete();
        }
    }

    @DexIgnore
    public Typeface e(Context context, Resources resources, int i, String str, int i2) {
        File e = Zl0.e(context);
        if (e == null) {
            return null;
        }
        try {
            if (!Zl0.c(e, resources, i)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(e.getPath());
            e.delete();
            return createFromFile;
        } catch (RuntimeException e2) {
            return null;
        } finally {
            e.delete();
        }
    }

    @DexIgnore
    public final Kl0.Ci f(Kl0.Bi bi, int i) {
        return (Kl0.Ci) g(bi.a(), i, new Bi(this));
    }

    @DexIgnore
    public Zm0.Fi h(Zm0.Fi[] fiArr, int i) {
        return (Zm0.Fi) g(fiArr, i, new Ai(this));
    }

    @DexIgnore
    public Kl0.Bi i(Typeface typeface) {
        long j = j(typeface);
        if (j == 0) {
            return null;
        }
        return this.a.get(Long.valueOf(j));
    }
}
