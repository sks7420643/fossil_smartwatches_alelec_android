package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Y06 extends Rv5<X06> {
    @DexIgnore
    void C2(boolean z);

    @DexIgnore
    void D4(List<ContactGroup> list);

    @DexIgnore
    Object E2();  // void declaration

    @DexIgnore
    void F(boolean z);

    @DexIgnore
    int L1();

    @DexIgnore
    int O2();

    @DexIgnore
    boolean O5();

    @DexIgnore
    Object V4();  // void declaration

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    Object close();  // void declaration

    @DexIgnore
    Object d4();  // void declaration

    @DexIgnore
    List<ContactGroup> f5();

    @DexIgnore
    void h3(ContactGroup contactGroup);

    @DexIgnore
    Object j6();  // void declaration

    @DexIgnore
    void m4(String str);

    @DexIgnore
    void w1(String str);
}
