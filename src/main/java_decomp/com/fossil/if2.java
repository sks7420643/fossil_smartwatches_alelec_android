package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class If2 {
    @DexIgnore
    public static Boolean a;
    @DexIgnore
    public static Boolean b;
    @DexIgnore
    public static Boolean c;

    @DexIgnore
    public static boolean a() {
        return "user".equals(Build.TYPE);
    }

    @DexIgnore
    @TargetApi(20)
    public static boolean b(Context context) {
        return c(context.getPackageManager());
    }

    @DexIgnore
    @TargetApi(20)
    public static boolean c(PackageManager packageManager) {
        if (a == null) {
            a = Boolean.valueOf(Mf2.g() && packageManager.hasSystemFeature("android.hardware.type.watch"));
        }
        return a.booleanValue();
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean d(Context context) {
        return b(context) && (!Mf2.i() || (e(context) && !Mf2.j()));
    }

    @DexIgnore
    @TargetApi(21)
    public static boolean e(Context context) {
        if (b == null) {
            b = Boolean.valueOf(Mf2.h() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }

    @DexIgnore
    public static boolean f(Context context) {
        if (c == null) {
            c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return c.booleanValue();
    }
}
