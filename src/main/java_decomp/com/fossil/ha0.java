package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ha0 implements Parcelable.Creator<Ia0> {
    @DexIgnore
    public /* synthetic */ Ha0(Qg6 qg6) {
    }

    @DexIgnore
    public Ia0 a(Parcel parcel) {
        return new Ia0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ia0 createFromParcel(Parcel parcel) {
        return new Ia0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ia0[] newArray(int i) {
        return new Ia0[i];
    }
}
