package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Le<T> implements Comparator<Lp> {
    @DexIgnore
    public static /* final */ Le b; // = new Le();

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public int compare(Lp lp, Lp lp2) {
        return lp.y().ordinal() - lp2.y().ordinal();
    }
}
