package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.mapped.Wg6;
import com.portfolio.platform.data.ActivityIntensities;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lz4 {
    @DexIgnore
    public final String a(ActivityIntensities activityIntensities) {
        Wg6.c(activityIntensities, "activityIntensities");
        try {
            return new Gson().t(activityIntensities);
        } catch (Exception e) {
            return "";
        }
    }

    @DexIgnore
    public final ActivityIntensities b(String str) {
        Wg6.c(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ActivityIntensities();
        }
        try {
            Object k = new Gson().k(str, ActivityIntensities.class);
            Wg6.b(k, "Gson().fromJson(data, Ac\u2026yIntensities::class.java)");
            return (ActivityIntensities) k;
        } catch (Exception e) {
            return new ActivityIntensities();
        }
    }
}
