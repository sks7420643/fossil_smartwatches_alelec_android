package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kb0 extends Mb0 {
    @DexIgnore
    public static /* final */ Jb0 CREATOR; // = new Jb0(null);
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public /* synthetic */ Kb0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.d = parcel.readInt() != 0;
    }

    @DexIgnore
    public Kb0(Bv1 bv1, Ry1 ry1, boolean z) {
        super(bv1, ry1);
        this.d = z;
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public List<Va0> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Za0());
        if (this.d) {
            arrayList.add(new Ia0(new R90[]{new R90(Ca0.c, W90.d, Da0.d, Ea0.c, 0), new R90(Ca0.c, W90.d, Da0.d, Ea0.c, 0)}));
        }
        arrayList.add(new Ga0(Fa0.c));
        arrayList.add(new Ka0());
        return arrayList;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Kb0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((Kb0) obj).d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppErrorResponse");
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public int hashCode() {
        return Boolean.valueOf(this.d).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.Mb0
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.w3, Integer.valueOf(this.d ? 1 : 0));
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
