package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vj2 extends Vp2 implements Uj2 {
    @DexIgnore
    public Vj2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gcm.INetworkTaskCallback");
    }

    @DexIgnore
    @Override // com.fossil.Uj2
    public final void d0(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        e(2, d);
    }
}
