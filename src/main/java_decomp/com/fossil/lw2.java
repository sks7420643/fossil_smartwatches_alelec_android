package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lw2 {
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    public Lw2(Kw2 kw2) {
        Sw2.c(kw2, "BuildInfo must be non-null");
        this.a = !kw2.zza();
    }

    @DexIgnore
    public final boolean a(String str) {
        Sw2.c(str, "flagName must not be null");
        if (!this.a) {
            return true;
        }
        return Nw2.a.zza().zza(str);
    }
}
