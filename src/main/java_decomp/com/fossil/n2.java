package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N2 implements Parcelable.Creator<O2> {
    @DexIgnore
    public /* synthetic */ N2(Qg6 qg6) {
    }

    @DexIgnore
    public O2 a(Parcel parcel) {
        return new O2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public O2 createFromParcel(Parcel parcel) {
        return new O2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public O2[] newArray(int i) {
        return new O2[i];
    }
}
