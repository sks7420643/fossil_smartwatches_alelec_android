package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vk4 extends Exception {
    @DexIgnore
    public Ai errorType;
    @DexIgnore
    public String message;

    @DexIgnore
    public enum Ai {
        INVALID_COUNTRY_CODE,
        NOT_A_NUMBER,
        TOO_SHORT_AFTER_IDD,
        TOO_SHORT_NSN,
        TOO_LONG
    }

    @DexIgnore
    public Vk4(Ai ai, String str) {
        super(str);
        this.message = str;
        this.errorType = ai;
    }

    @DexIgnore
    public Ai getErrorType() {
        return this.errorType;
    }

    @DexIgnore
    public String toString() {
        String valueOf = String.valueOf(String.valueOf(this.errorType));
        String valueOf2 = String.valueOf(String.valueOf(this.message));
        StringBuilder sb = new StringBuilder(valueOf.length() + 14 + valueOf2.length());
        sb.append("Error type: ");
        sb.append(valueOf);
        sb.append(". ");
        sb.append(valueOf2);
        return sb.toString();
    }
}
