package com.fossil;

import com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ny4 implements Factory<BCHistoryViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;

    @DexIgnore
    public Ny4(Provider<Tt4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Ny4 a(Provider<Tt4> provider) {
        return new Ny4(provider);
    }

    @DexIgnore
    public static BCHistoryViewModel c(Tt4 tt4) {
        return new BCHistoryViewModel(tt4);
    }

    @DexIgnore
    public BCHistoryViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
