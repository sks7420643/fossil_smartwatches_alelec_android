package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Cd6;
import com.mapped.Rc6;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oj extends Bi {
    @DexIgnore
    public /* final */ ArrayList<Ac0> V; // = new ArrayList<>();

    @DexIgnore
    public Oj(K5 k5, I60 i60) {
        super(k5, i60, Yp.x0, Ke.b.a(k5.x, Ob.x), false, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.TRUE), Hl7.a(Hu1.SKIP_LIST, Boolean.FALSE), Hl7.a(Hu1.ERASE_CACHE_FILE_BEFORE_GET, Boolean.TRUE)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public JSONObject E() {
        JSONObject E = super.E();
        Jd0 jd0 = Jd0.L4;
        Object[] array = this.V.toArray(new Ac0[0]);
        if (array != null) {
            return G80.k(E, jd0, Px1.a((Ox1[]) array));
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Bi
    public void M(ArrayList<J0> arrayList) {
        byte[] bArr;
        J0 j0 = (J0) Pm7.H(arrayList);
        if (!(j0 == null || (bArr = j0.f) == null)) {
            try {
                Mm7.t(this.V, (Object[]) Pa.d.f(bArr));
            } catch (Sx1 e) {
                Cd6 cd6 = Cd6.a;
            }
        }
        l(Nr.a(this.v, null, Zq.b, null, null, 13));
    }

    @DexIgnore
    public Ac0[] a0() {
        Object[] array = this.V.toArray(new Ac0[0]);
        if (array != null) {
            return (Ac0[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public /* bridge */ /* synthetic */ Object x() {
        return a0();
    }
}
