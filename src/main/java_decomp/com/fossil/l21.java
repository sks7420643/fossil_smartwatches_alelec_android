package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L21 extends K21<F21> {
    @DexIgnore
    public L21(Context context, K41 k41) {
        super(W21.c(context, k41).d());
    }

    @DexIgnore
    @Override // com.fossil.K21
    public boolean b(O31 o31) {
        return o31.j.b() == Y01.CONNECTED;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.K21
    public /* bridge */ /* synthetic */ boolean c(F21 f21) {
        return i(f21);
    }

    @DexIgnore
    public boolean i(F21 f21) {
        return Build.VERSION.SDK_INT >= 26 ? !f21.a() || !f21.d() : !f21.a();
    }
}
