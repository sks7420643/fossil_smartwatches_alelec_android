package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R82 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ H82 b;

    @DexIgnore
    public R82(H82 h82) {
        this.b = h82;
    }

    @DexIgnore
    public /* synthetic */ R82(H82 h82, K82 k82) {
        this(h82);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void run() {
        this.b.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.b.b.unlock();
            }
        } catch (RuntimeException e) {
            this.b.a.q(e);
        } finally {
            this.b.b.unlock();
        }
    }
}
