package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class K84 {
    @DexIgnore
    public static /* final */ Pattern e; // = Pattern.compile("http(s?)://[^\\/]+", 2);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Kb4 b;
    @DexIgnore
    public /* final */ Ib4 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public K84(String str, String str2, Kb4 kb4, Ib4 ib4) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (kb4 != null) {
            this.d = str;
            this.a = f(str2);
            this.b = kb4;
            this.c = ib4;
        } else {
            throw new IllegalArgumentException("requestFactory must not be null.");
        }
    }

    @DexIgnore
    public Jb4 c() {
        return d(Collections.emptyMap());
    }

    @DexIgnore
    public Jb4 d(Map<String, String> map) {
        Jb4 a2 = this.b.a(this.c, e(), map);
        a2.d("User-Agent", "Crashlytics Android SDK/" + W84.i());
        a2.d("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
        return a2;
    }

    @DexIgnore
    public String e() {
        return this.a;
    }

    @DexIgnore
    public final String f(String str) {
        return !R84.D(this.d) ? e.matcher(str).replaceFirst(this.d) : str;
    }
}
