package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ic6 extends Fq4 {
    @DexIgnore
    public abstract void n();

    @DexIgnore
    public abstract void o();

    @DexIgnore
    public abstract void p(Category category);

    @DexIgnore
    public abstract void q(String str);

    @DexIgnore
    public abstract void r(HybridCustomizeViewModel hybridCustomizeViewModel);

    @DexIgnore
    public abstract void s(String str, String str2);
}
