package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.Ac2;
import com.fossil.M62;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H82 implements D92 {
    @DexIgnore
    public /* final */ C92 a;
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ D62 d;
    @DexIgnore
    public Z52 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Bundle i; // = new Bundle();
    @DexIgnore
    public /* final */ Set<M62.Ci> j; // = new HashSet();
    @DexIgnore
    public Ys3 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public Jc2 o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ Ac2 r;
    @DexIgnore
    public /* final */ Map<M62<?>, Boolean> s;
    @DexIgnore
    public /* final */ M62.Ai<? extends Ys3, Gs3> t;
    @DexIgnore
    public ArrayList<Future<?>> u; // = new ArrayList<>();

    @DexIgnore
    public H82(C92 c92, Ac2 ac2, Map<M62<?>, Boolean> map, D62 d62, M62.Ai<? extends Ys3, Gs3> ai, Lock lock, Context context) {
        this.a = c92;
        this.r = ac2;
        this.s = map;
        this.d = d62;
        this.t = ai;
        this.b = lock;
        this.c = context;
    }

    @DexIgnore
    public static String B(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    @DexIgnore
    public final boolean C(Z52 z52) {
        return this.l && !z52.k();
    }

    @DexIgnore
    public final void D(Z52 z52) {
        s();
        w(!z52.k());
        this.a.u(z52);
        this.a.u.a(z52);
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final boolean a() {
        s();
        w(true);
        this.a.u(null);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void b() {
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void d(int i2) {
        D(new Z52(8, null));
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void e(Bundle bundle) {
        if (z(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (o()) {
                q();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void f() {
        this.a.h.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (M62<?> m62 : this.s.keySet()) {
            M62.Fi fi = this.a.g.get(m62.a());
            boolean z2 = (m62.c().b() == 1) | z;
            boolean booleanValue = this.s.get(m62).booleanValue();
            if (fi.v()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(m62.a());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(fi, new J82(this, m62, booleanValue));
            z = z2;
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            this.r.m(Integer.valueOf(System.identityHashCode(this.a.t)));
            S82 s82 = new S82(this, null);
            M62.Ai<? extends Ys3, Gs3> ai = this.t;
            Context context = this.c;
            Looper m2 = this.a.t.m();
            Ac2 ac2 = this.r;
            this.k = (Ys3) ai.c(context, m2, ac2, ac2.k(), s82, s82);
        }
        this.h = this.a.g.size();
        this.u.add(G92.a().submit(new M82(this, hashMap)));
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void i(Z52 z52, M62<?> m62, boolean z) {
        if (z(1)) {
            v(z52, m62, z);
            if (o()) {
                q();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t2) {
        this.a.t.i.add(t2);
        return t2;
    }

    @DexIgnore
    public final void m(Us3 us3) {
        if (z(0)) {
            Z52 c2 = us3.c();
            if (c2.A()) {
                Tc2 f2 = us3.f();
                Z52 f3 = f2.f();
                if (!f3.A()) {
                    String valueOf = String.valueOf(f3);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GACConnecting", sb.toString(), new Exception());
                    D(f3);
                    return;
                }
                this.n = true;
                this.o = f2.c();
                this.p = f2.h();
                this.q = f2.k();
                p();
            } else if (C(c2)) {
                r();
                p();
            } else {
                D(c2);
            }
        }
    }

    @DexIgnore
    public final boolean o() {
        int i2 = this.h - 1;
        this.h = i2;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            Log.w("GACConnecting", this.a.t.F());
            Log.wtf("GACConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            D(new Z52(8, null));
            return false;
        }
        Z52 z52 = this.e;
        if (z52 == null) {
            return true;
        }
        this.a.s = this.f;
        D(z52);
        return false;
    }

    @DexIgnore
    public final void p() {
        if (this.h == 0) {
            if (!this.m || this.n) {
                ArrayList arrayList = new ArrayList();
                this.g = 1;
                this.h = this.a.g.size();
                for (M62.Ci<?> ci : this.a.g.keySet()) {
                    if (!this.a.h.containsKey(ci)) {
                        arrayList.add(this.a.g.get(ci));
                    } else if (o()) {
                        q();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(G92.a().submit(new N82(this, arrayList)));
                }
            }
        }
    }

    @DexIgnore
    public final void q() {
        this.a.s();
        G92.a().execute(new K82(this));
        Ys3 ys3 = this.k;
        if (ys3 != null) {
            if (this.p) {
                ys3.d(this.o, this.q);
            }
            w(false);
        }
        for (M62.Ci<?> ci : this.a.h.keySet()) {
            this.a.g.get(ci).a();
        }
        this.a.u.b(this.i.isEmpty() ? null : this.i);
    }

    @DexIgnore
    public final void r() {
        this.m = false;
        this.a.t.q = Collections.emptySet();
        for (M62.Ci<?> ci : this.j) {
            if (!this.a.h.containsKey(ci)) {
                this.a.h.put(ci, new Z52(17, null));
            }
        }
    }

    @DexIgnore
    public final void s() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Future<?> future = arrayList.get(i2);
            i2++;
            future.cancel(true);
        }
        this.u.clear();
    }

    @DexIgnore
    public final Set<Scope> t() {
        if (this.r == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.r.j());
        Map<M62<?>, Ac2.Bi> g2 = this.r.g();
        for (M62<?> m62 : g2.keySet()) {
            if (!this.a.h.containsKey(m62.a())) {
                hashSet.addAll(g2.get(m62).a);
            }
        }
        return hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if ((r6.k() || r5.d.c(r6.c()) != null) != false) goto L_0x0015;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void v(com.fossil.Z52 r6, com.fossil.M62<?> r7, boolean r8) {
        /*
            r5 = this;
            r1 = 1
            r0 = 0
            com.fossil.M62$Ei r2 = r7.c()
            int r3 = r2.b()
            if (r8 == 0) goto L_0x0015
            boolean r2 = r6.k()
            if (r2 == 0) goto L_0x0030
        L_0x0012:
            r2 = r1
        L_0x0013:
            if (r2 == 0) goto L_0x001e
        L_0x0015:
            com.fossil.Z52 r2 = r5.e
            if (r2 == 0) goto L_0x001d
            int r2 = r5.f
            if (r3 >= r2) goto L_0x001e
        L_0x001d:
            r0 = r1
        L_0x001e:
            if (r0 == 0) goto L_0x0024
            r5.e = r6
            r5.f = r3
        L_0x0024:
            com.fossil.C92 r0 = r5.a
            java.util.Map<com.fossil.M62$Ci<?>, com.fossil.Z52> r0 = r0.h
            com.fossil.M62$Ci r1 = r7.a()
            r0.put(r1, r6)
            return
        L_0x0030:
            com.fossil.D62 r2 = r5.d
            int r4 = r6.c()
            android.content.Intent r2 = r2.c(r4)
            if (r2 != 0) goto L_0x0012
            r2 = r0
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H82.v(com.fossil.Z52, com.fossil.M62, boolean):void");
    }

    @DexIgnore
    public final void w(boolean z) {
        Ys3 ys3 = this.k;
        if (ys3 != null) {
            if (ys3.c() && z) {
                this.k.o();
            }
            this.k.a();
            if (this.r.l()) {
                this.k = null;
            }
            this.o = null;
        }
    }

    @DexIgnore
    public final boolean z(int i2) {
        if (this.g == i2) {
            return true;
        }
        Log.w("GACConnecting", this.a.t.F());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GACConnecting", sb.toString());
        int i3 = this.h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        Log.w("GACConnecting", sb2.toString());
        String B = B(this.g);
        String B2 = B(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(B).length() + 70 + String.valueOf(B2).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(B);
        sb3.append(" but received callback for step ");
        sb3.append(B2);
        Log.e("GACConnecting", sb3.toString(), new Exception());
        D(new Z52(8, null));
        return false;
    }
}
