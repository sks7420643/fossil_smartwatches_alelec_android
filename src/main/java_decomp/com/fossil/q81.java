package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q81 {
    @DexIgnore
    public static boolean a; // = false;
    @DexIgnore
    public static int b; // = 3;
    @DexIgnore
    public static /* final */ Q81 c; // = new Q81();

    @DexIgnore
    public final boolean a() {
        return a;
    }

    @DexIgnore
    public final int b() {
        return b;
    }
}
