package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st6 extends ts0 {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f3304a; // = new MutableLiveData<>();
    @DexIgnore
    public a b; // = new a(null, 1, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3305a;

        @DexIgnore
        public a() {
            this(null, 1, null);
        }

        @DexIgnore
        public a(String str) {
            this.f3305a = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(String str, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : str);
        }

        @DexIgnore
        public final String a() {
            return this.f3305a;
        }

        @DexIgnore
        public final void b(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = st6.d;
            local.d(str2, "update themeName=" + str);
            this.f3305a = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateCurrentThemeName$1", f = "UserCustomizeThemeViewModel.kt", l = {30}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ st6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateCurrentThemeName$1$name$1", f = "UserCustomizeThemeViewModel.kt", l = {31}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object themeById;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    this.L$0 = this.p$;
                    this.label = 1;
                    themeById = this.this$0.this$0.c.getThemeById(this.this$0.$id.element, this);
                    if (themeById == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv7 = (iv7) this.L$0;
                    el7.b(obj);
                    themeById = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (themeById != null) {
                    return ((Theme) themeById).getName();
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(st6 st6, dr7 dr7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = st6;
            this.$id = dr7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$id, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.b((String) g);
            this.this$0.e();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateNewName$1", f = "UserCustomizeThemeViewModel.kt", l = {42}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ st6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateNewName$1$1", f = "UserCustomizeThemeViewModel.kt", l = {43, 45}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:10:0x002f  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0070  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r2 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x004f
                    if (r0 == r4) goto L_0x0024
                    if (r0 != r5) goto L_0x001c
                    java.lang.Object r0 = r6.L$1
                    com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0019:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x001b:
                    return r0
                L_0x001c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0024:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                    r3 = r0
                    r1 = r7
                L_0x002d:
                    if (r1 == 0) goto L_0x0070
                    r0 = r1
                    com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                    com.fossil.st6$c r1 = r6.this$0
                    java.lang.String r1 = r1.$name
                    r0.setName(r1)
                    com.fossil.st6$c r1 = r6.this$0
                    com.fossil.st6 r1 = r1.this$0
                    com.portfolio.platform.data.source.ThemeRepository r1 = com.fossil.st6.b(r1)
                    r6.L$0 = r3
                    r6.L$1 = r0
                    r6.label = r5
                    java.lang.Object r0 = r1.upsertUserTheme(r0, r6)
                    if (r0 != r2) goto L_0x0019
                    r0 = r2
                    goto L_0x001b
                L_0x004f:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r3 = r6.p$
                    com.fossil.st6$c r0 = r6.this$0
                    com.fossil.st6 r0 = r0.this$0
                    com.portfolio.platform.data.source.ThemeRepository r1 = com.fossil.st6.b(r0)
                    com.fossil.st6$c r0 = r6.this$0
                    com.fossil.dr7 r0 = r0.$id
                    T r0 = r0.element
                    java.lang.String r0 = (java.lang.String) r0
                    r6.L$0 = r3
                    r6.label = r4
                    java.lang.Object r1 = r1.getThemeById(r0, r6)
                    if (r1 != r2) goto L_0x002d
                    r0 = r2
                    goto L_0x001b
                L_0x0070:
                    com.fossil.pq7.i()
                    r0 = 0
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.st6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(st6 st6, dr7 dr7, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = st6;
            this.$id = dr7;
            this.$name = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$id, this.$name, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.b(this.$name);
            this.this$0.e();
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = st6.class.getSimpleName();
        pq7.b(simpleName, "UserCustomizeThemeViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public st6(ThemeRepository themeRepository) {
        pq7.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void e() {
        this.f3304a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<a> f() {
        return this.f3304a;
    }

    @DexIgnore
    public final void g() {
        h();
    }

    @DexIgnore
    public final void h() {
        dr7 dr7 = new dr7();
        dr7.element = (T) pt6.m.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "updateCurrentThemeName id=" + ((String) dr7.element));
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, dr7, null), 3, null);
    }

    @DexIgnore
    public final void i(String str) {
        pq7.c(str, "name");
        dr7 dr7 = new dr7();
        dr7.element = (T) pt6.m.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = d;
        local.d(str2, "updateNewName id=" + ((String) dr7.element) + " name=" + str);
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, dr7, str, null), 3, null);
    }
}
