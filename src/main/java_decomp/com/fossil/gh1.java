package com.fossil;

import android.graphics.Bitmap;
import com.fossil.Bb1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gh1 implements Bb1.Ai {
    @DexIgnore
    public /* final */ Rd1 a;
    @DexIgnore
    public /* final */ Od1 b;

    @DexIgnore
    public Gh1(Rd1 rd1, Od1 od1) {
        this.a = rd1;
        this.b = od1;
    }

    @DexIgnore
    @Override // com.fossil.Bb1.Ai
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.a.e(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.Bb1.Ai
    public int[] b(int i) {
        Od1 od1 = this.b;
        return od1 == null ? new int[i] : (int[]) od1.g(i, int[].class);
    }

    @DexIgnore
    @Override // com.fossil.Bb1.Ai
    public void c(Bitmap bitmap) {
        this.a.b(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Bb1.Ai
    public void d(byte[] bArr) {
        Od1 od1 = this.b;
        if (od1 != null) {
            od1.f(bArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.Bb1.Ai
    public byte[] e(int i) {
        Od1 od1 = this.b;
        return od1 == null ? new byte[i] : (byte[]) od1.g(i, byte[].class);
    }

    @DexIgnore
    @Override // com.fossil.Bb1.Ai
    public void f(int[] iArr) {
        Od1 od1 = this.b;
        if (od1 != null) {
            od1.f(iArr);
        }
    }
}
