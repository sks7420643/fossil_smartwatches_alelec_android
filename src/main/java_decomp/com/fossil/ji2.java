package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ji2 implements Parcelable.Creator<DataPoint> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataPoint createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        Uh2 uh2 = null;
        Ai2[] ai2Arr = null;
        Uh2 uh22 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    uh22 = (Uh2) Ad2.e(parcel, t, Uh2.CREATOR);
                    break;
                case 2:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 3:
                    j4 = Ad2.y(parcel, t);
                    break;
                case 4:
                    j3 = Ad2.y(parcel, t);
                    break;
                case 5:
                    ai2Arr = (Ai2[]) Ad2.i(parcel, t, Ai2.CREATOR);
                    break;
                case 6:
                    uh2 = (Uh2) Ad2.e(parcel, t, Uh2.CREATOR);
                    break;
                case 7:
                    j2 = Ad2.y(parcel, t);
                    break;
                case 8:
                    j = Ad2.y(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new DataPoint(uh22, j4, j3, ai2Arr, uh2, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataPoint[] newArray(int i) {
        return new DataPoint[i];
    }
}
