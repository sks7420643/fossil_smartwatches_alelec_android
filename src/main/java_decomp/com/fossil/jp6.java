package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jp6 implements Factory<ProfileGoalEditViewModel> {
    @DexIgnore
    public /* final */ Provider<SummariesRepository> a;
    @DexIgnore
    public /* final */ Provider<SleepSummariesRepository> b;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> c;
    @DexIgnore
    public /* final */ Provider<UserRepository> d;
    @DexIgnore
    public /* final */ Provider<SetActivityGoalUserCase> e;

    @DexIgnore
    public Jp6(Provider<SummariesRepository> provider, Provider<SleepSummariesRepository> provider2, Provider<GoalTrackingRepository> provider3, Provider<UserRepository> provider4, Provider<SetActivityGoalUserCase> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static Jp6 a(Provider<SummariesRepository> provider, Provider<SleepSummariesRepository> provider2, Provider<GoalTrackingRepository> provider3, Provider<UserRepository> provider4, Provider<SetActivityGoalUserCase> provider5) {
        return new Jp6(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static ProfileGoalEditViewModel c(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, SetActivityGoalUserCase setActivityGoalUserCase) {
        return new ProfileGoalEditViewModel(summariesRepository, sleepSummariesRepository, goalTrackingRepository, userRepository, setActivityGoalUserCase);
    }

    @DexIgnore
    public ProfileGoalEditViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
