package com.fossil;

import android.net.Uri;
import com.fossil.N61;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R61 implements N61<String, Uri> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ boolean a(String str) {
        return c(str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ Uri b(String str) {
        return d(str);
    }

    @DexIgnore
    public boolean c(String str) {
        Wg6.c(str, "data");
        return N61.Ai.a(this, str);
    }

    @DexIgnore
    public Uri d(String str) {
        Wg6.c(str, "data");
        Uri parse = Uri.parse(str);
        Wg6.b(parse, "Uri.parse(this)");
        return parse;
    }
}
