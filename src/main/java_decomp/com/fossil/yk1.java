package com.fossil;

import android.os.Parcelable;
import com.fossil.fitness.FitnessData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yk1 extends Parcelable {

    @DexIgnore
    public enum a {
        BOND_NONE,
        BONDING,
        BONDED;
        
        @DexIgnore
        public static /* final */ C0295a c; // = new C0295a(null);

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yk1$a$a")
        /* renamed from: com.fossil.yk1$a$a  reason: collision with other inner class name */
        public static final class C0295a {
            @DexIgnore
            public /* synthetic */ C0295a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(int i) {
                return i != 11 ? i != 12 ? a.BOND_NONE : a.BONDED : a.BONDING;
            }

            @DexIgnore
            public final a b(r4 r4Var) {
                int i = vx.f3850a[r4Var.ordinal()];
                if (i == 1) {
                    return a.BOND_NONE;
                }
                if (i == 2) {
                    return a.BONDING;
                }
                if (i == 3) {
                    return a.BONDED;
                }
                throw new al7();
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onAutoSyncFitnessDataReceived(FitnessData[] fitnessDataArr);

        @DexIgnore
        void onDeviceStateChanged(yk1 yk1, c cVar, c cVar2);

        @DexIgnore
        void onEventReceived(yk1 yk1, mp1 mp1);
    }

    @DexIgnore
    public enum c {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        UPGRADING_FIRMWARE,
        DISCONNECTING
    }

    @DexIgnore
    public enum d {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING;
        
        @DexIgnore
        public static /* final */ ez g; // = new ez(null);
    }

    @DexIgnore
    void B(b bVar);

    @DexIgnore
    zk1 M();

    @DexIgnore
    a P();

    @DexIgnore
    <T> T g0(Class<T> cls);

    @DexIgnore
    c getState();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    qy1<tl7> l();
}
