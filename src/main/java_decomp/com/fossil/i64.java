package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class I64 implements Mg4 {
    @DexIgnore
    public /* final */ J64 a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public I64(J64 j64, Context context) {
        this.a = j64;
        this.b = context;
    }

    @DexIgnore
    public static Mg4 a(J64 j64, Context context) {
        return new I64(j64, context);
    }

    @DexIgnore
    @Override // com.fossil.Mg4
    public Object get() {
        return J64.r(this.a, this.b);
    }
}
