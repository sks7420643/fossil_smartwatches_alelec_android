package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D6 extends U5 {
    @DexIgnore
    public UUID[] k; // = new UUID[0];
    @DexIgnore
    public N6[] l; // = new N6[0];

    @DexIgnore
    public D6(N4 n4) {
        super(V5.e, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.A();
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return h7 instanceof C7;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.g;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        C7 c7 = (C7) h7;
        this.k = c7.b;
        this.l = c7.c;
    }
}
