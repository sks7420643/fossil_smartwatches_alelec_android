package com.fossil;

import com.fossil.Qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qf3 extends Sc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Ji b;

    @DexIgnore
    public Qf3(Qb3 qb3, Qb3.Ji ji) {
        this.b = ji;
    }

    @DexIgnore
    @Override // com.fossil.Rc3
    public final boolean v1(Ls2 ls2) {
        return this.b.onMarkerClick(new Ke3(ls2));
    }
}
