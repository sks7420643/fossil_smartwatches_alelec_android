package com.fossil;

import coil.util.CoilContentProvider;
import com.fossil.A51;
import com.mapped.Gg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X41 {
    @DexIgnore
    public static A51 a;
    @DexIgnore
    public static Gg6<? extends A51> b;
    @DexIgnore
    public static /* final */ X41 c; // = new X41();

    @DexIgnore
    public static final A51 b() {
        A51 a51 = a;
        return a51 != null ? a51 : c.a();
    }

    @DexIgnore
    public static final void c(A51 a51) {
        Wg6.c(a51, "loader");
        A51 a512 = a;
        if (a512 != null) {
            a512.shutdown();
        }
        a = a51;
        b = null;
    }

    @DexIgnore
    public final A51 a() {
        A51 a51;
        synchronized (this) {
            a51 = a;
            if (a51 == null) {
                Gg6<? extends A51> gg6 = b;
                if (gg6 == null || (a51 = (A51) gg6.invoke()) == null) {
                    A51.Ai ai = A51.o;
                    a51 = new B51(CoilContentProvider.c.a()).b();
                }
                b = null;
                c(a51);
            }
        }
        return a51;
    }
}
