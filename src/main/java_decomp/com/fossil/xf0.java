package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.Ig0;
import com.fossil.Jg0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xf0 implements Ig0 {
    @DexIgnore
    public Context b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public Cg0 d;
    @DexIgnore
    public LayoutInflater e;
    @DexIgnore
    public Ig0.Ai f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Jg0 i;
    @DexIgnore
    public int j;

    @DexIgnore
    public Xf0(Context context, int i2, int i3) {
        this.b = context;
        this.e = LayoutInflater.from(context);
        this.g = i2;
        this.h = i3;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void b(Cg0 cg0, boolean z) {
        Ig0.Ai ai = this.f;
        if (ai != null) {
            ai.b(cg0, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void c(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.i;
        if (viewGroup != null) {
            Cg0 cg0 = this.d;
            if (cg0 != null) {
                cg0.t();
                ArrayList<Eg0> G = this.d.G();
                int size = G.size();
                int i4 = 0;
                i2 = 0;
                while (i4 < size) {
                    Eg0 eg0 = G.get(i4);
                    if (t(i2, eg0)) {
                        View childAt = viewGroup.getChildAt(i2);
                        Eg0 itemData = childAt instanceof Jg0.Ai ? ((Jg0.Ai) childAt).getItemData() : null;
                        View q = q(eg0, childAt, viewGroup);
                        if (eg0 != itemData) {
                            q.setPressed(false);
                            q.jumpDrawablesToCurrentState();
                        }
                        if (q != childAt) {
                            j(q, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i4++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!o(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean e(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean f(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void g(Ig0.Ai ai) {
        this.f = ai;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public int getId() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void h(Context context, Cg0 cg0) {
        this.c = context;
        LayoutInflater.from(context);
        this.d = cg0;
    }

    @DexIgnore
    public void j(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.i).addView(view, i2);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.Cg0] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.Ig0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean k(com.fossil.Ng0 r2) {
        /*
            r1 = this;
            com.fossil.Ig0$Ai r0 = r1.f
            if (r0 == 0) goto L_0x000e
            if (r2 == 0) goto L_0x000b
        L_0x0006:
            boolean r0 = r0.c(r2)
        L_0x000a:
            return r0
        L_0x000b:
            com.fossil.Cg0 r2 = r1.d
            goto L_0x0006
        L_0x000e:
            r0 = 0
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Xf0.k(com.fossil.Ng0):boolean");
    }

    @DexIgnore
    public abstract void m(Eg0 eg0, Jg0.Ai ai);

    @DexIgnore
    public Jg0.Ai n(ViewGroup viewGroup) {
        return (Jg0.Ai) this.e.inflate(this.h, viewGroup, false);
    }

    @DexIgnore
    public boolean o(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    @DexIgnore
    public Ig0.Ai p() {
        return this.f;
    }

    @DexIgnore
    public View q(Eg0 eg0, View view, ViewGroup viewGroup) {
        Jg0.Ai n = view instanceof Jg0.Ai ? (Jg0.Ai) view : n(viewGroup);
        m(eg0, n);
        return (View) n;
    }

    @DexIgnore
    public Jg0 r(ViewGroup viewGroup) {
        if (this.i == null) {
            Jg0 jg0 = (Jg0) this.e.inflate(this.g, viewGroup, false);
            this.i = jg0;
            jg0.b(this.d);
            c(true);
        }
        return this.i;
    }

    @DexIgnore
    public void s(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public abstract boolean t(int i2, Eg0 eg0);
}
