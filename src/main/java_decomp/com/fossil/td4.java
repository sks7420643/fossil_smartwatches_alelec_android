package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Td4 {
    @DexIgnore
    Td4 a(String str, boolean z) throws IOException;

    @DexIgnore
    Td4 b(String str, long j) throws IOException;

    @DexIgnore
    Td4 c(String str, int i) throws IOException;

    @DexIgnore
    Td4 f(String str, Object obj) throws IOException;
}
