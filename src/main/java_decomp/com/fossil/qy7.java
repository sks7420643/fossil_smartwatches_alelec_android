package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qy7 extends NoSuchElementException {
    @DexIgnore
    public Qy7(String str) {
        super(str);
    }
}
