package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Cl3 extends IInterface {
    @DexIgnore
    void D1(Xr3 xr3) throws RemoteException;

    @DexIgnore
    void E1(Vg3 vg3, Or3 or3) throws RemoteException;

    @DexIgnore
    void K1(Vg3 vg3, String str, String str2) throws RemoteException;

    @DexIgnore
    void N0(long j, String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    void P1(Or3 or3) throws RemoteException;

    @DexIgnore
    List<Fr3> S(String str, String str2, String str3, boolean z) throws RemoteException;

    @DexIgnore
    void S0(Or3 or3) throws RemoteException;

    @DexIgnore
    List<Xr3> T0(String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    List<Xr3> V0(String str, String str2, Or3 or3) throws RemoteException;

    @DexIgnore
    String n0(Or3 or3) throws RemoteException;

    @DexIgnore
    void n2(Bundle bundle, Or3 or3) throws RemoteException;

    @DexIgnore
    List<Fr3> o1(String str, String str2, boolean z, Or3 or3) throws RemoteException;

    @DexIgnore
    List<Fr3> p1(Or3 or3, boolean z) throws RemoteException;

    @DexIgnore
    void r1(Or3 or3) throws RemoteException;

    @DexIgnore
    void s(Xr3 xr3, Or3 or3) throws RemoteException;

    @DexIgnore
    void u2(Fr3 fr3, Or3 or3) throws RemoteException;

    @DexIgnore
    byte[] w2(Vg3 vg3, String str) throws RemoteException;
}
