package com.fossil;

import com.portfolio.platform.watchface.data.WFAssetsLocal;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N77 implements Factory<WFAssetsLocal> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ N77 a; // = new N77();
    }

    @DexIgnore
    public static N77 a() {
        return Ai.a;
    }

    @DexIgnore
    public static WFAssetsLocal c() {
        return new WFAssetsLocal();
    }

    @DexIgnore
    public WFAssetsLocal b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
