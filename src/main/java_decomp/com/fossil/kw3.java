package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kw3 {
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_dark; // = 2131099652;
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_light; // = 2131099653;
    @DexIgnore
    public static /* final */ int abc_btn_colored_borderless_text_material; // = 2131099654;
    @DexIgnore
    public static /* final */ int abc_btn_colored_text_material; // = 2131099655;
    @DexIgnore
    public static /* final */ int abc_color_highlight_material; // = 2131099656;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_dark; // = 2131099659;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_light; // = 2131099660;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_dark; // = 2131099661;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_light; // = 2131099662;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_dark; // = 2131099663;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_light; // = 2131099664;
    @DexIgnore
    public static /* final */ int abc_search_url_text; // = 2131099665;
    @DexIgnore
    public static /* final */ int abc_search_url_text_normal; // = 2131099666;
    @DexIgnore
    public static /* final */ int abc_search_url_text_pressed; // = 2131099667;
    @DexIgnore
    public static /* final */ int abc_search_url_text_selected; // = 2131099668;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_dark; // = 2131099669;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_light; // = 2131099670;
    @DexIgnore
    public static /* final */ int abc_tint_btn_checkable; // = 2131099671;
    @DexIgnore
    public static /* final */ int abc_tint_default; // = 2131099672;
    @DexIgnore
    public static /* final */ int abc_tint_edittext; // = 2131099673;
    @DexIgnore
    public static /* final */ int abc_tint_seek_thumb; // = 2131099674;
    @DexIgnore
    public static /* final */ int abc_tint_spinner; // = 2131099675;
    @DexIgnore
    public static /* final */ int abc_tint_switch_track; // = 2131099676;
    @DexIgnore
    public static /* final */ int accent_material_dark; // = 2131099679;
    @DexIgnore
    public static /* final */ int accent_material_light; // = 2131099680;
    @DexIgnore
    public static /* final */ int background_floating_material_dark; // = 2131099697;
    @DexIgnore
    public static /* final */ int background_floating_material_light; // = 2131099698;
    @DexIgnore
    public static /* final */ int background_material_dark; // = 2131099699;
    @DexIgnore
    public static /* final */ int background_material_light; // = 2131099700;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_dark; // = 2131099708;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_light; // = 2131099709;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_dark; // = 2131099710;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_light; // = 2131099711;
    @DexIgnore
    public static /* final */ int bright_foreground_material_dark; // = 2131099712;
    @DexIgnore
    public static /* final */ int bright_foreground_material_light; // = 2131099713;
    @DexIgnore
    public static /* final */ int button_material_dark; // = 2131099719;
    @DexIgnore
    public static /* final */ int button_material_light; // = 2131099720;
    @DexIgnore
    public static /* final */ int cardview_dark_background; // = 2131099721;
    @DexIgnore
    public static /* final */ int cardview_light_background; // = 2131099722;
    @DexIgnore
    public static /* final */ int cardview_shadow_end_color; // = 2131099723;
    @DexIgnore
    public static /* final */ int cardview_shadow_start_color; // = 2131099724;
    @DexIgnore
    public static /* final */ int checkbox_themeable_attribute_color; // = 2131099725;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_shadow_color; // = 2131099767;
    @DexIgnore
    public static /* final */ int design_box_stroke_color; // = 2131099768;
    @DexIgnore
    public static /* final */ int design_dark_default_color_background; // = 2131099769;
    @DexIgnore
    public static /* final */ int design_dark_default_color_error; // = 2131099770;
    @DexIgnore
    public static /* final */ int design_dark_default_color_on_background; // = 2131099771;
    @DexIgnore
    public static /* final */ int design_dark_default_color_on_error; // = 2131099772;
    @DexIgnore
    public static /* final */ int design_dark_default_color_on_primary; // = 2131099773;
    @DexIgnore
    public static /* final */ int design_dark_default_color_on_secondary; // = 2131099774;
    @DexIgnore
    public static /* final */ int design_dark_default_color_on_surface; // = 2131099775;
    @DexIgnore
    public static /* final */ int design_dark_default_color_primary; // = 2131099776;
    @DexIgnore
    public static /* final */ int design_dark_default_color_primary_dark; // = 2131099777;
    @DexIgnore
    public static /* final */ int design_dark_default_color_primary_variant; // = 2131099778;
    @DexIgnore
    public static /* final */ int design_dark_default_color_secondary; // = 2131099779;
    @DexIgnore
    public static /* final */ int design_dark_default_color_secondary_variant; // = 2131099780;
    @DexIgnore
    public static /* final */ int design_dark_default_color_surface; // = 2131099781;
    @DexIgnore
    public static /* final */ int design_default_color_background; // = 2131099782;
    @DexIgnore
    public static /* final */ int design_default_color_error; // = 2131099783;
    @DexIgnore
    public static /* final */ int design_default_color_on_background; // = 2131099784;
    @DexIgnore
    public static /* final */ int design_default_color_on_error; // = 2131099785;
    @DexIgnore
    public static /* final */ int design_default_color_on_primary; // = 2131099786;
    @DexIgnore
    public static /* final */ int design_default_color_on_secondary; // = 2131099787;
    @DexIgnore
    public static /* final */ int design_default_color_on_surface; // = 2131099788;
    @DexIgnore
    public static /* final */ int design_default_color_primary; // = 2131099789;
    @DexIgnore
    public static /* final */ int design_default_color_primary_dark; // = 2131099790;
    @DexIgnore
    public static /* final */ int design_default_color_primary_variant; // = 2131099791;
    @DexIgnore
    public static /* final */ int design_default_color_secondary; // = 2131099792;
    @DexIgnore
    public static /* final */ int design_default_color_secondary_variant; // = 2131099793;
    @DexIgnore
    public static /* final */ int design_default_color_surface; // = 2131099794;
    @DexIgnore
    public static /* final */ int design_error; // = 2131099795;
    @DexIgnore
    public static /* final */ int design_fab_shadow_end_color; // = 2131099796;
    @DexIgnore
    public static /* final */ int design_fab_shadow_mid_color; // = 2131099797;
    @DexIgnore
    public static /* final */ int design_fab_shadow_start_color; // = 2131099798;
    @DexIgnore
    public static /* final */ int design_fab_stroke_end_inner_color; // = 2131099799;
    @DexIgnore
    public static /* final */ int design_fab_stroke_end_outer_color; // = 2131099800;
    @DexIgnore
    public static /* final */ int design_fab_stroke_top_inner_color; // = 2131099801;
    @DexIgnore
    public static /* final */ int design_fab_stroke_top_outer_color; // = 2131099802;
    @DexIgnore
    public static /* final */ int design_icon_tint; // = 2131099803;
    @DexIgnore
    public static /* final */ int design_snackbar_background_color; // = 2131099804;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_dark; // = 2131099814;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_light; // = 2131099815;
    @DexIgnore
    public static /* final */ int dim_foreground_material_dark; // = 2131099816;
    @DexIgnore
    public static /* final */ int dim_foreground_material_light; // = 2131099817;
    @DexIgnore
    public static /* final */ int error_color_material_dark; // = 2131099822;
    @DexIgnore
    public static /* final */ int error_color_material_light; // = 2131099823;
    @DexIgnore
    public static /* final */ int foreground_material_dark; // = 2131099825;
    @DexIgnore
    public static /* final */ int foreground_material_light; // = 2131099826;
    @DexIgnore
    public static /* final */ int highlighted_text_material_dark; // = 2131099851;
    @DexIgnore
    public static /* final */ int highlighted_text_material_light; // = 2131099852;
    @DexIgnore
    public static /* final */ int material_blue_grey_800; // = 2131099859;
    @DexIgnore
    public static /* final */ int material_blue_grey_900; // = 2131099860;
    @DexIgnore
    public static /* final */ int material_blue_grey_950; // = 2131099861;
    @DexIgnore
    public static /* final */ int material_deep_teal_200; // = 2131099862;
    @DexIgnore
    public static /* final */ int material_deep_teal_500; // = 2131099863;
    @DexIgnore
    public static /* final */ int material_grey_100; // = 2131099864;
    @DexIgnore
    public static /* final */ int material_grey_300; // = 2131099865;
    @DexIgnore
    public static /* final */ int material_grey_50; // = 2131099866;
    @DexIgnore
    public static /* final */ int material_grey_600; // = 2131099867;
    @DexIgnore
    public static /* final */ int material_grey_800; // = 2131099868;
    @DexIgnore
    public static /* final */ int material_grey_850; // = 2131099869;
    @DexIgnore
    public static /* final */ int material_grey_900; // = 2131099870;
    @DexIgnore
    public static /* final */ int material_on_background_disabled; // = 2131099871;
    @DexIgnore
    public static /* final */ int material_on_background_emphasis_high_type; // = 2131099872;
    @DexIgnore
    public static /* final */ int material_on_background_emphasis_medium; // = 2131099873;
    @DexIgnore
    public static /* final */ int material_on_primary_disabled; // = 2131099874;
    @DexIgnore
    public static /* final */ int material_on_primary_emphasis_high_type; // = 2131099875;
    @DexIgnore
    public static /* final */ int material_on_primary_emphasis_medium; // = 2131099876;
    @DexIgnore
    public static /* final */ int material_on_surface_disabled; // = 2131099877;
    @DexIgnore
    public static /* final */ int material_on_surface_emphasis_high_type; // = 2131099878;
    @DexIgnore
    public static /* final */ int material_on_surface_emphasis_medium; // = 2131099879;
    @DexIgnore
    public static /* final */ int mtrl_bottom_nav_colored_item_tint; // = 2131099881;
    @DexIgnore
    public static /* final */ int mtrl_bottom_nav_colored_ripple_color; // = 2131099882;
    @DexIgnore
    public static /* final */ int mtrl_bottom_nav_item_tint; // = 2131099883;
    @DexIgnore
    public static /* final */ int mtrl_bottom_nav_ripple_color; // = 2131099884;
    @DexIgnore
    public static /* final */ int mtrl_btn_bg_color_selector; // = 2131099885;
    @DexIgnore
    public static /* final */ int mtrl_btn_ripple_color; // = 2131099886;
    @DexIgnore
    public static /* final */ int mtrl_btn_stroke_color_selector; // = 2131099887;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_btn_bg_color_selector; // = 2131099888;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_btn_ripple_color; // = 2131099889;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_color_disabled; // = 2131099890;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_color_selector; // = 2131099891;
    @DexIgnore
    public static /* final */ int mtrl_btn_transparent_bg_color; // = 2131099892;
    @DexIgnore
    public static /* final */ int mtrl_calendar_item_stroke_color; // = 2131099893;
    @DexIgnore
    public static /* final */ int mtrl_calendar_selected_range; // = 2131099894;
    @DexIgnore
    public static /* final */ int mtrl_card_view_foreground; // = 2131099895;
    @DexIgnore
    public static /* final */ int mtrl_card_view_ripple; // = 2131099896;
    @DexIgnore
    public static /* final */ int mtrl_chip_background_color; // = 2131099897;
    @DexIgnore
    public static /* final */ int mtrl_chip_close_icon_tint; // = 2131099898;
    @DexIgnore
    public static /* final */ int mtrl_chip_ripple_color; // = 2131099899;
    @DexIgnore
    public static /* final */ int mtrl_chip_surface_color; // = 2131099900;
    @DexIgnore
    public static /* final */ int mtrl_chip_text_color; // = 2131099901;
    @DexIgnore
    public static /* final */ int mtrl_choice_chip_background_color; // = 2131099902;
    @DexIgnore
    public static /* final */ int mtrl_choice_chip_ripple_color; // = 2131099903;
    @DexIgnore
    public static /* final */ int mtrl_choice_chip_text_color; // = 2131099904;
    @DexIgnore
    public static /* final */ int mtrl_error; // = 2131099905;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_bg_color_selector; // = 2131099906;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_ripple_color; // = 2131099907;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_text_color_selector; // = 2131099908;
    @DexIgnore
    public static /* final */ int mtrl_fab_ripple_color; // = 2131099909;
    @DexIgnore
    public static /* final */ int mtrl_filled_background_color; // = 2131099910;
    @DexIgnore
    public static /* final */ int mtrl_filled_icon_tint; // = 2131099911;
    @DexIgnore
    public static /* final */ int mtrl_filled_stroke_color; // = 2131099912;
    @DexIgnore
    public static /* final */ int mtrl_indicator_text_color; // = 2131099913;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_background_color; // = 2131099914;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_icon_tint; // = 2131099915;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_text_color; // = 2131099916;
    @DexIgnore
    public static /* final */ int mtrl_on_primary_text_btn_text_color_selector; // = 2131099917;
    @DexIgnore
    public static /* final */ int mtrl_outlined_icon_tint; // = 2131099918;
    @DexIgnore
    public static /* final */ int mtrl_outlined_stroke_color; // = 2131099919;
    @DexIgnore
    public static /* final */ int mtrl_popupmenu_overlay_color; // = 2131099920;
    @DexIgnore
    public static /* final */ int mtrl_scrim_color; // = 2131099921;
    @DexIgnore
    public static /* final */ int mtrl_tabs_colored_ripple_color; // = 2131099922;
    @DexIgnore
    public static /* final */ int mtrl_tabs_icon_color_selector; // = 2131099923;
    @DexIgnore
    public static /* final */ int mtrl_tabs_icon_color_selector_colored; // = 2131099924;
    @DexIgnore
    public static /* final */ int mtrl_tabs_legacy_text_color_selector; // = 2131099925;
    @DexIgnore
    public static /* final */ int mtrl_tabs_ripple_color; // = 2131099926;
    @DexIgnore
    public static /* final */ int mtrl_text_btn_text_color_selector; // = 2131099927;
    @DexIgnore
    public static /* final */ int mtrl_textinput_default_box_stroke_color; // = 2131099928;
    @DexIgnore
    public static /* final */ int mtrl_textinput_disabled_color; // = 2131099929;
    @DexIgnore
    public static /* final */ int mtrl_textinput_filled_box_default_background_color; // = 2131099930;
    @DexIgnore
    public static /* final */ int mtrl_textinput_focused_box_stroke_color; // = 2131099931;
    @DexIgnore
    public static /* final */ int mtrl_textinput_hovered_box_stroke_color; // = 2131099932;
    @DexIgnore
    public static /* final */ int notification_action_color_filter; // = 2131099944;
    @DexIgnore
    public static /* final */ int notification_icon_bg_color; // = 2131099945;
    @DexIgnore
    public static /* final */ int primary_dark_material_dark; // = 2131099970;
    @DexIgnore
    public static /* final */ int primary_dark_material_light; // = 2131099971;
    @DexIgnore
    public static /* final */ int primary_material_dark; // = 2131099972;
    @DexIgnore
    public static /* final */ int primary_material_light; // = 2131099973;
    @DexIgnore
    public static /* final */ int primary_text_default_material_dark; // = 2131099974;
    @DexIgnore
    public static /* final */ int primary_text_default_material_light; // = 2131099975;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_dark; // = 2131099976;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_light; // = 2131099977;
    @DexIgnore
    public static /* final */ int ripple_material_dark; // = 2131100329;
    @DexIgnore
    public static /* final */ int ripple_material_light; // = 2131100330;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_dark; // = 2131100332;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_light; // = 2131100333;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_dark; // = 2131100334;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_light; // = 2131100335;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_dark; // = 2131100343;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_light; // = 2131100344;
    @DexIgnore
    public static /* final */ int switch_thumb_material_dark; // = 2131100345;
    @DexIgnore
    public static /* final */ int switch_thumb_material_light; // = 2131100346;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_dark; // = 2131100347;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_light; // = 2131100348;
    @DexIgnore
    public static /* final */ int test_mtrl_calendar_day; // = 2131100349;
    @DexIgnore
    public static /* final */ int test_mtrl_calendar_day_selected; // = 2131100350;
    @DexIgnore
    public static /* final */ int tooltip_background_dark; // = 2131100351;
    @DexIgnore
    public static /* final */ int tooltip_background_light; // = 2131100352;
}
