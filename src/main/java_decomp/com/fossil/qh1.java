package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qh1 implements Th1<Bitmap, BitmapDrawable> {
    @DexIgnore
    public /* final */ Resources a;

    @DexIgnore
    public Qh1(Resources resources) {
        Ik1.d(resources);
        this.a = resources;
    }

    @DexIgnore
    @Override // com.fossil.Th1
    public Id1<BitmapDrawable> a(Id1<Bitmap> id1, Ob1 ob1) {
        return Og1.f(this.a, id1);
    }
}
