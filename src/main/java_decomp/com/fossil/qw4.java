package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qw4 implements Factory<BCWaitingChallengeDetailViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;
    @DexIgnore
    public /* final */ Provider<FriendRepository> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Qw4(Provider<Tt4> provider, Provider<FriendRepository> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Qw4 a(Provider<Tt4> provider, Provider<FriendRepository> provider2, Provider<An4> provider3) {
        return new Qw4(provider, provider2, provider3);
    }

    @DexIgnore
    public static BCWaitingChallengeDetailViewModel c(Tt4 tt4, FriendRepository friendRepository, An4 an4) {
        return new BCWaitingChallengeDetailViewModel(tt4, friendRepository, an4);
    }

    @DexIgnore
    public BCWaitingChallengeDetailViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
