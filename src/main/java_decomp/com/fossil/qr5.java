package com.fossil;

import android.view.KeyEvent;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qr5 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Qr5(String str, String str2) {
        Wg6.c(str, "appName");
        Wg6.c(str2, "packageName");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public abstract boolean a(KeyEvent keyEvent);

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public abstract Rr5 c();

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Qr5)) {
            return false;
        }
        return Wg6.a(this.b, ((Qr5) obj).b);
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }
}
