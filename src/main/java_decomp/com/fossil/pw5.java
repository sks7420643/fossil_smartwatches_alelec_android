package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pw5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<SecondTimezoneSetting> a; // = new ArrayList();
    @DexIgnore
    public /* final */ ArrayList<Ci> b; // = new ArrayList<>();
    @DexIgnore
    public Bi c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Pw5 pw5, View view) {
            super(view);
            Wg6.c(view, "view");
            View findViewById = view.findViewById(2131362541);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
                String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(d)) {
                    this.a.setBackgroundColor(Color.parseColor(d));
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public SecondTimezoneSetting a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Aii c; // = Aii.TYPE_HEADER;

        @DexIgnore
        public enum Aii {
            TYPE_HEADER,
            TYPE_VALUE
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final Aii b() {
            return this.c;
        }

        @DexIgnore
        public final SecondTimezoneSetting c() {
            return this.a;
        }

        @DexIgnore
        public final void d(String str) {
            this.b = str;
        }

        @DexIgnore
        public final void e(Aii aii) {
            Wg6.c(aii, "<set-?>");
            this.c = aii;
        }

        @DexIgnore
        public final void f(SecondTimezoneSetting secondTimezoneSetting) {
            this.a = secondTimezoneSetting;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ Bi b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;

            @DexIgnore
            public Aii(Di di) {
                this.b = di;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi = this.b.b;
                if (bi != null) {
                    bi.a(this.b.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Pw5 pw5, View view, Bi bi) {
            super(view);
            Wg6.c(view, "view");
            this.b = bi;
            View findViewById = view.findViewById(2131363009);
            View findViewById2 = view.findViewById(2131362783);
            String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            String d2 = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d)) {
                findViewById.setBackgroundColor(Color.parseColor(d));
            }
            if (!TextUtils.isEmpty(d2)) {
                findViewById2.setBackgroundColor(Color.parseColor(d2));
            }
            view.setOnClickListener(new Aii(this));
            View findViewById3 = view.findViewById(2131363388);
            if (findViewById3 != null) {
                this.a = (FlexibleTextView) findViewById3;
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t.getTimeZoneName(), t2.getTimeZoneName());
        }
    }

    @DexIgnore
    public final void g(String str) {
        Wg6.c(str, "filterText");
        if (TextUtils.isEmpty(str)) {
            k(this.a);
        } else {
            this.b.clear();
            for (SecondTimezoneSetting secondTimezoneSetting : this.a) {
                String timeZoneName = secondTimezoneSetting.getTimeZoneName();
                if (timeZoneName != null) {
                    String lowerCase = timeZoneName.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    String lowerCase2 = str.toLowerCase();
                    Wg6.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                    if (Wt7.v(lowerCase, lowerCase2, false, 2, null)) {
                        Ci ci = new Ci();
                        ci.f(secondTimezoneSetting);
                        ci.e(Ci.Aii.TYPE_VALUE);
                        this.b.add(ci);
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return this.b.get(i).b().ordinal();
    }

    @DexIgnore
    public final List<Ci> h() {
        return this.b;
    }

    @DexIgnore
    public final int i(String str) {
        Wg6.c(str, "letter");
        int i = 0;
        for (Ci ci : this.b) {
            if (ci.b() == Ci.Aii.TYPE_HEADER && Wg6.a(ci.a(), str)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public final void j(Bi bi) {
        Wg6.c(bi, "mItemClickListener");
        this.c = bi;
    }

    @DexIgnore
    public final void k(List<SecondTimezoneSetting> list) {
        Wg6.c(list, "secondTimeZoneList");
        this.b.clear();
        this.a = list;
        List b0 = Pm7.b0(list, new Ei());
        int size = b0.size();
        String str = "";
        int i = 0;
        while (i < size) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) b0.get(i);
            String timeZoneName = secondTimezoneSetting.getTimeZoneName();
            if (timeZoneName != null) {
                char[] charArray = timeZoneName.toCharArray();
                Wg6.b(charArray, "(this as java.lang.String).toCharArray()");
                String valueOf = String.valueOf(Character.toUpperCase(charArray[0]));
                if (!TextUtils.equals(str, valueOf)) {
                    Ci ci = new Ci();
                    ci.d(valueOf);
                    ci.e(Ci.Aii.TYPE_HEADER);
                    this.b.add(ci);
                } else {
                    valueOf = str;
                }
                Ci ci2 = new Ci();
                ci2.f(secondTimezoneSetting);
                ci2.e(Ci.Aii.TYPE_VALUE);
                this.b.add(ci2);
                i++;
                str = valueOf;
            } else {
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        if (getItemViewType(i) == Ci.Aii.TYPE_HEADER.ordinal()) {
            FlexibleTextView a2 = ((Ai) viewHolder).a();
            String a3 = this.b.get(i).a();
            if (a3 != null) {
                a2.setText(a3);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            SecondTimezoneSetting c2 = this.b.get(i).c();
            if (c2 != null) {
                FlexibleTextView b2 = ((Di) viewHolder).b();
                b2.setText(c2.getTimeZoneName() + " (" + c2.getCityCode() + ')');
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == Ci.Aii.TYPE_HEADER.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558715, viewGroup, false);
            Wg6.b(inflate, "v");
            return new Ai(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558734, viewGroup, false);
        Wg6.b(inflate2, "v");
        return new Di(this, inflate2, this.c);
    }
}
