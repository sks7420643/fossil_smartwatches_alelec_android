package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qa4 extends Ta4.Di.Dii.Diii {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Dii.Diii.Aiiii {
        @DexIgnore
        public String a;

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Diii.Aiiii
        public Ta4.Di.Dii.Diii a() {
            String str = "";
            if (this.a == null) {
                str = " content";
            }
            if (str.isEmpty()) {
                return new Qa4(this.a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Diii.Aiiii
        public Ta4.Di.Dii.Diii.Aiiii b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null content");
        }
    }

    @DexIgnore
    public Qa4(String str) {
        this.a = str;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Diii
    public String b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Ta4.Di.Dii.Diii) {
            return this.a.equals(((Ta4.Di.Dii.Diii) obj).b());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Log{content=" + this.a + "}";
    }
}
