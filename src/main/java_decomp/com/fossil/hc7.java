package com.fossil;

import com.mapped.Wg6;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hc7 {
    @DexIgnore
    public static /* final */ HashMap<Object, List<Object>> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<Object, Gc7> b; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Hc7 c; // = new Hc7();

    @DexIgnore
    public final void a(Object obj) {
        Wg6.c(obj, "root");
        if (b.get(obj) == null) {
            b.put(obj, new Gc7());
        }
    }

    @DexIgnore
    public final void b(Object obj, Object obj2) {
        Wg6.c(obj, "root");
        Wg6.c(obj2, "branch");
        List<Object> list = a.get(obj);
        if (list == null) {
            a.put(obj, Hm7.i(obj2));
            return;
        }
        list.add(obj2);
        a.put(obj, list);
    }

    @DexIgnore
    public final void c(Object obj) {
        Wg6.c(obj, "root");
        a.remove(obj);
        b.remove(obj);
    }

    @DexIgnore
    public final Gc7 d(Object obj) {
        Wg6.c(obj, "rootOrBranch");
        Gc7 f = f(obj);
        return f != null ? f : e(obj);
    }

    @DexIgnore
    public final Gc7 e(Object obj) {
        Wg6.c(obj, "branch");
        for (Map.Entry<Object, List<Object>> entry : a.entrySet()) {
            List<Object> value = entry.getValue();
            if (value != null && value.contains(obj)) {
                return b.get(entry.getKey());
            }
        }
        return null;
    }

    @DexIgnore
    public final Gc7 f(Object obj) {
        Wg6.c(obj, "root");
        return b.get(obj);
    }
}
