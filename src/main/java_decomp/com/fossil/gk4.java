package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import com.mapped.Fu3;
import com.mapped.Ku3;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gk4 extends JsonReader {
    @DexIgnore
    public static /* final */ Reader A; // = new Ai();
    @DexIgnore
    public static /* final */ Object B; // = new Object();
    @DexIgnore
    public Object[] w; // = new Object[32];
    @DexIgnore
    public int x; // = 0;
    @DexIgnore
    public String[] y; // = new String[32];
    @DexIgnore
    public int[] z; // = new int[32];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Reader {
        @DexIgnore
        @Override // java.io.Closeable, java.io.Reader, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public Gk4(JsonElement jsonElement) {
        super(A);
        z0(jsonElement);
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    private String B() {
        return " at path " + getPath();
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public boolean C() throws IOException {
        v0(Nk4.BOOLEAN);
        boolean a2 = ((Jj4) x0()).a();
        int i = this.x;
        if (i > 0) {
            int[] iArr = this.z;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return a2;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public double D() throws IOException {
        Nk4 V = V();
        if (V == Nk4.NUMBER || V == Nk4.STRING) {
            double l = ((Jj4) w0()).l();
            if (o() || (!Double.isNaN(l) && !Double.isInfinite(l))) {
                x0();
                int i = this.x;
                if (i > 0) {
                    int[] iArr = this.z;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return l;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + l);
        }
        throw new IllegalStateException("Expected " + Nk4.NUMBER + " but was " + V + B());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public int F() throws IOException {
        Nk4 V = V();
        if (V == Nk4.NUMBER || V == Nk4.STRING) {
            int b = ((Jj4) w0()).b();
            x0();
            int i = this.x;
            if (i > 0) {
                int[] iArr = this.z;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return b;
        }
        throw new IllegalStateException("Expected " + Nk4.NUMBER + " but was " + V + B());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public long G() throws IOException {
        Nk4 V = V();
        if (V == Nk4.NUMBER || V == Nk4.STRING) {
            long m = ((Jj4) w0()).m();
            x0();
            int i = this.x;
            if (i > 0) {
                int[] iArr = this.z;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return m;
        }
        throw new IllegalStateException("Expected " + Nk4.NUMBER + " but was " + V + B());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String L() throws IOException {
        v0(Nk4.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) w0()).next();
        String str = (String) entry.getKey();
        this.y[this.x - 1] = str;
        z0(entry.getValue());
        return str;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void P() throws IOException {
        v0(Nk4.NULL);
        x0();
        int i = this.x;
        if (i > 0) {
            int[] iArr = this.z;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String S() throws IOException {
        Nk4 V = V();
        if (V == Nk4.STRING || V == Nk4.NUMBER) {
            String f = ((Jj4) x0()).f();
            int i = this.x;
            if (i > 0) {
                int[] iArr = this.z;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return f;
        }
        throw new IllegalStateException("Expected " + Nk4.STRING + " but was " + V + B());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public Nk4 V() throws IOException {
        if (this.x == 0) {
            return Nk4.END_DOCUMENT;
        }
        Object w0 = w0();
        if (w0 instanceof Iterator) {
            boolean z2 = this.w[this.x - 2] instanceof Ku3;
            Iterator it = (Iterator) w0;
            if (!it.hasNext()) {
                return z2 ? Nk4.END_OBJECT : Nk4.END_ARRAY;
            }
            if (z2) {
                return Nk4.NAME;
            }
            z0(it.next());
            return V();
        } else if (w0 instanceof Ku3) {
            return Nk4.BEGIN_OBJECT;
        } else {
            if (w0 instanceof Fu3) {
                return Nk4.BEGIN_ARRAY;
            }
            if (w0 instanceof Jj4) {
                Jj4 jj4 = (Jj4) w0;
                if (jj4.s()) {
                    return Nk4.STRING;
                }
                if (jj4.o()) {
                    return Nk4.BOOLEAN;
                }
                if (jj4.q()) {
                    return Nk4.NUMBER;
                }
                throw new AssertionError();
            } else if (w0 instanceof Fj4) {
                return Nk4.NULL;
            } else {
                if (w0 == B) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void a() throws IOException {
        v0(Nk4.BEGIN_ARRAY);
        z0(((Fu3) w0()).iterator());
        this.z[this.x - 1] = 0;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void b() throws IOException {
        v0(Nk4.BEGIN_OBJECT);
        z0(((Ku3) w0()).entrySet().iterator());
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable, com.google.gson.stream.JsonReader
    public void close() throws IOException {
        this.w = new Object[]{B};
        this.x = 1;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String getPath() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.x) {
            Object[] objArr = this.w;
            if (objArr[i] instanceof Fu3) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.z[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof Ku3) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('.');
                    String[] strArr = this.y;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void j() throws IOException {
        v0(Nk4.END_ARRAY);
        x0();
        x0();
        int i = this.x;
        if (i > 0) {
            int[] iArr = this.z;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void k() throws IOException {
        v0(Nk4.END_OBJECT);
        x0();
        x0();
        int i = this.x;
        if (i > 0) {
            int[] iArr = this.z;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public boolean m() throws IOException {
        Nk4 V = V();
        return (V == Nk4.END_OBJECT || V == Nk4.END_ARRAY) ? false : true;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void t0() throws IOException {
        if (V() == Nk4.NAME) {
            L();
            this.y[this.x - 2] = "null";
        } else {
            x0();
            int i = this.x;
            if (i > 0) {
                this.y[i - 1] = "null";
            }
        }
        int i2 = this.x;
        if (i2 > 0) {
            int[] iArr = this.z;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String toString() {
        return Gk4.class.getSimpleName();
    }

    @DexIgnore
    public final void v0(Nk4 nk4) throws IOException {
        if (V() != nk4) {
            throw new IllegalStateException("Expected " + nk4 + " but was " + V() + B());
        }
    }

    @DexIgnore
    public final Object w0() {
        return this.w[this.x - 1];
    }

    @DexIgnore
    public final Object x0() {
        Object[] objArr = this.w;
        int i = this.x - 1;
        this.x = i;
        Object obj = objArr[i];
        objArr[i] = null;
        return obj;
    }

    @DexIgnore
    public void y0() throws IOException {
        v0(Nk4.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) w0()).next();
        z0(entry.getValue());
        z0(new Jj4((String) entry.getKey()));
    }

    @DexIgnore
    public final void z0(Object obj) {
        int i = this.x;
        Object[] objArr = this.w;
        if (i == objArr.length) {
            Object[] objArr2 = new Object[(i * 2)];
            int[] iArr = new int[(i * 2)];
            String[] strArr = new String[(i * 2)];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.z, 0, iArr, 0, this.x);
            System.arraycopy(this.y, 0, strArr, 0, this.x);
            this.w = objArr2;
            this.z = iArr;
            this.y = strArr;
        }
        Object[] objArr3 = this.w;
        int i2 = this.x;
        this.x = i2 + 1;
        objArr3[i2] = obj;
    }
}
