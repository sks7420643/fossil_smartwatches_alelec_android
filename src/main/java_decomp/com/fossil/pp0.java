package com.fossil;

import android.database.Cursor;
import android.widget.Filter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pp0 extends Filter {
    @DexIgnore
    public Ai a;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        Cursor b();

        @DexIgnore
        CharSequence d(Cursor cursor);

        @DexIgnore
        Cursor e(CharSequence charSequence);
    }

    @DexIgnore
    public Pp0(Ai ai) {
        this.a = ai;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        return this.a.d((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor e = this.a.e(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (e != null) {
            filterResults.count = e.getCount();
            filterResults.values = e;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Cursor b = this.a.b();
        Object obj = filterResults.values;
        if (obj != null && obj != b) {
            this.a.a((Cursor) obj);
        }
    }
}
