package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J5 extends Qq7 implements Hg6<BluetoothGattCharacteristic, String> {
    @DexIgnore
    public static /* final */ J5 b; // = new J5();

    @DexIgnore
    public J5() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public String invoke(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic2 = bluetoothGattCharacteristic;
        StringBuilder sb = new StringBuilder();
        sb.append("    ");
        Wg6.b(bluetoothGattCharacteristic2, "characteristic");
        sb.append(bluetoothGattCharacteristic2.getUuid().toString());
        return sb.toString();
    }
}
