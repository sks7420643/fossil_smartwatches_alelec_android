package com.fossil;

import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nw6 implements MembersInjector<OnboardingHeightWeightPresenter> {
    @DexIgnore
    public static void a(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
        onboardingHeightWeightPresenter.z();
    }
}
