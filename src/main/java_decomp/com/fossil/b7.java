package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B7 extends H7 {
    @DexIgnore
    public /* final */ N6 b;
    @DexIgnore
    public /* final */ U6 c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public B7(G7 g7, N6 n6, U6 u6, byte[] bArr) {
        super(g7);
        this.b = n6;
        this.c = u6;
        this.d = bArr;
    }
}
