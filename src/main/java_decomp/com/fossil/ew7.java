package com.fossil;

import com.mapped.Cd6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ew7 extends Iu7 {
    @DexIgnore
    public /* final */ Dw7 b;

    @DexIgnore
    public Ew7(Dw7 dw7) {
        this.b = dw7;
    }

    @DexIgnore
    @Override // com.fossil.Ju7
    public void a(Throwable th) {
        this.b.dispose();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        a(th);
        return Cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCancel[" + this.b + ']';
    }
}
