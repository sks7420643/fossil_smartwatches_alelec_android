package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cp4 implements Factory<AuthApiGuestService> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Cp4(Uo4 uo4, Provider<An4> provider) {
        this.a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static Cp4 a(Uo4 uo4, Provider<An4> provider) {
        return new Cp4(uo4, provider);
    }

    @DexIgnore
    public static AuthApiGuestService c(Uo4 uo4, An4 an4) {
        AuthApiGuestService j = uo4.j(an4);
        Lk7.c(j, "Cannot return null from a non-@Nullable @Provides method");
        return j;
    }

    @DexIgnore
    public AuthApiGuestService b() {
        return c(this.a, this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
