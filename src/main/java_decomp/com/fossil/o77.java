package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.watchface.Data;
import com.portfolio.platform.data.model.watchface.MetaData;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O77 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<Data> {
    }

    @DexIgnore
    public O77() {
        Gson d = new Zi4().d();
        Wg6.b(d, "GsonBuilder().create()");
        this.a = d;
    }

    @DexIgnore
    public final int a(L77 l77) {
        Wg6.c(l77, "type");
        return l77.getValue();
    }

    @DexIgnore
    public final String b(Q77 q77) {
        Wg6.c(q77, "owner");
        try {
            return this.a.u(q77, Q77.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final String c(Data data) {
        Wg6.c(data, "data");
        String u = new Gson().u(data, new Ai().getType());
        Wg6.b(u, "Gson().toJson(data, type)");
        return u;
    }

    @DexIgnore
    public final Data d(String str) {
        Wg6.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Data) new Gson().l(str, new Bi().getType());
    }

    @DexIgnore
    public final MetaData e(String str) {
        try {
            return (MetaData) this.a.k(str, MetaData.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final String f(MetaData metaData) {
        try {
            return this.a.u(metaData, MetaData.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final L77 g(int i) {
        L77[] values = L77.values();
        for (L77 l77 : values) {
            if (l77.getValue() == i) {
                return l77;
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public final Q77 h(String str) {
        Wg6.c(str, "value");
        try {
            return (Q77) this.a.k(str, Q77.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
