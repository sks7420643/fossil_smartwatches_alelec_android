package com.fossil;

import android.os.Bundle;
import com.fossil.M62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface D92 {
    @DexIgnore
    boolean a();

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void d(int i);

    @DexIgnore
    void e(Bundle bundle);

    @DexIgnore
    Object f();  // void declaration

    @DexIgnore
    void i(Z52 z52, M62<?> m62, boolean z);

    @DexIgnore
    <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t);

    @DexIgnore
    <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t);
}
