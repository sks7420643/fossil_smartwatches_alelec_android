package com.fossil;

import com.mapped.R60;
import com.mapped.Rc6;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lm extends Bi {
    @DexIgnore
    public HashMap<Zm1, R60> V;

    @DexIgnore
    public Lm(K5 k5, I60 i60, short s) {
        super(k5, i60, Yp.w, s, true, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.TRUE), Hl7.a(Hu1.NUMBER_OF_FILE_REQUIRED, 1), Hl7.a(Hu1.ERASE_CACHE_FILE_BEFORE_GET, Boolean.TRUE)), 1.0f, null, 128);
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public JSONObject E() {
        JSONArray jSONArray;
        Collection<R60> values;
        JSONObject E = super.E();
        Jd0 jd0 = Jd0.R;
        HashMap<Zm1, R60> hashMap = this.V;
        if (hashMap == null || (values = hashMap.values()) == null) {
            jSONArray = null;
        } else {
            Object[] array = values.toArray(new R60[0]);
            if (array != null) {
                jSONArray = Px1.a((R60[]) array);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return G80.k(E, jd0, jSONArray);
    }

    @DexIgnore
    @Override // com.fossil.Bi
    public void M(ArrayList<J0> arrayList) {
        l(this.v);
    }

    @DexIgnore
    @Override // com.fossil.Bi
    public void Q(J0 j0) {
        Zq zq;
        super.Q(j0);
        try {
            this.V = (HashMap) Ya.f.f(j0.f);
            zq = Zq.b;
        } catch (Sx1 e) {
            D90.i.i(e);
            zq = Zq.q;
        }
        this.v = Nr.a(this.v, null, zq, null, null, 13);
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public Object x() {
        HashMap<Zm1, R60> hashMap = this.V;
        return hashMap != null ? hashMap : new HashMap();
    }
}
