package com.fossil;

import com.google.gson.Gson;
import com.mapped.Fu3;
import com.mapped.Ku3;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mq5 {
    @DexIgnore
    public List<SavedPreset> a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public Range a() {
        return this.b;
    }

    @DexIgnore
    public List<SavedPreset> b() {
        return this.a;
    }

    @DexIgnore
    public void c(Ku3 ku3) {
        Fu3 c;
        this.a = new ArrayList();
        if (ku3.s(CloudLogWriter.ITEMS_PARAM)) {
            try {
                Fu3 q = ku3.q(CloudLogWriter.ITEMS_PARAM);
                if (q.size() > 0) {
                    for (int i = 0; i < q.size(); i++) {
                        Ku3 d = q.m(i).d();
                        SavedPreset savedPreset = new SavedPreset();
                        if (d.s("id")) {
                            savedPreset.setId(d.p("id").f());
                        }
                        if (d.s("name")) {
                            savedPreset.setName(d.p("name").f());
                        }
                        if (d.s("buttons") && (c = d.p("buttons").c()) != null && c.size() > 0) {
                            savedPreset.setButtons(c.toString());
                        }
                        savedPreset.setPinType(0);
                        this.a.add(savedPreset);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (ku3.s("_range")) {
            try {
                this.b = (Range) new Gson().k(ku3.r("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
