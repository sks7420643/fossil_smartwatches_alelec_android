package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F9 extends Ux1<byte[]> {
    @DexIgnore
    public F9() {
        super(null, 1, null);
    }

    @DexIgnore
    @Override // com.fossil.Qx1
    public byte[] b(Object obj) {
        byte[] bArr = (byte[]) obj;
        I9.d.h(bArr);
        return bArr;
    }
}
