package com.fossil;

import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bv6 implements Factory<LoginPresenter> {
    @DexIgnore
    public static LoginPresenter a(Vu6 vu6, BaseActivity baseActivity) {
        return new LoginPresenter(vu6, baseActivity);
    }
}
