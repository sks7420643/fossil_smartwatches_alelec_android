package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dy3;
import com.google.android.material.datepicker.MaterialCalendarGridView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jy3 extends RecyclerView.g<Bi> {
    @DexIgnore
    public /* final */ Wx3 a;
    @DexIgnore
    public /* final */ Zx3<?> b;
    @DexIgnore
    public /* final */ Dy3.Li c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MaterialCalendarGridView b;

        @DexIgnore
        public Ai(MaterialCalendarGridView materialCalendarGridView) {
            this.b = materialCalendarGridView;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (this.b.b().j(i)) {
                Jy3.this.c.a(this.b.b().c(i).longValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ MaterialCalendarGridView b;

        @DexIgnore
        public Bi(LinearLayout linearLayout, boolean z) {
            super(linearLayout);
            TextView textView = (TextView) linearLayout.findViewById(Nw3.month_title);
            this.a = textView;
            Mo0.m0(textView, true);
            this.b = (MaterialCalendarGridView) linearLayout.findViewById(Nw3.month_grid);
            if (!z) {
                this.a.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public Jy3(Context context, Zx3<?> zx3, Wx3 wx3, Dy3.Li li) {
        Hy3 i = wx3.i();
        Hy3 f = wx3.f();
        Hy3 h = wx3.h();
        if (i.a(h) > 0) {
            throw new IllegalArgumentException("firstPage cannot be after currentPage");
        } else if (h.a(f) <= 0) {
            this.d = (Ey3.K6(context) ? Dy3.K6(context) : 0) + (Iy3.f * Dy3.K6(context));
            this.a = wx3;
            this.b = zx3;
            this.c = li;
            setHasStableIds(true);
        } else {
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.g();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return this.a.i().k(i).i();
    }

    @DexIgnore
    public Hy3 h(int i) {
        return this.a.i().k(i);
    }

    @DexIgnore
    public CharSequence i(int i) {
        return h(i).h();
    }

    @DexIgnore
    public int j(Hy3 hy3) {
        return this.a.i().m(hy3);
    }

    @DexIgnore
    public void k(Bi bi, int i) {
        Hy3 k = this.a.i().k(i);
        bi.a.setText(k.h());
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) bi.b.findViewById(Nw3.month_grid);
        if (materialCalendarGridView.b() == null || !k.equals(materialCalendarGridView.b().b)) {
            Iy3 iy3 = new Iy3(k, this.b, this.a);
            materialCalendarGridView.setNumColumns(k.f);
            materialCalendarGridView.setAdapter((ListAdapter) iy3);
        } else {
            materialCalendarGridView.b().notifyDataSetChanged();
        }
        materialCalendarGridView.setOnItemClickListener(new Ai(materialCalendarGridView));
    }

    @DexIgnore
    public Bi l(ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(Pw3.mtrl_calendar_month_labeled, viewGroup, false);
        if (!Ey3.K6(viewGroup.getContext())) {
            return new Bi(linearLayout, false);
        }
        linearLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, this.d));
        return new Bi(linearLayout, true);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        k(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return l(viewGroup, i);
    }
}
