package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H28 extends M48 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public H28(A58 a58) {
        super(a58);
    }

    @DexIgnore
    @Override // com.fossil.M48, com.fossil.A58
    public void K(I48 i48, long j) throws IOException {
        if (this.c) {
            i48.skip(j);
            return;
        }
        try {
            super.K(i48, j);
        } catch (IOException e) {
            this.c = true;
            a(e);
        }
    }

    @DexIgnore
    public abstract void a(IOException iOException);

    @DexIgnore
    @Override // com.fossil.M48, java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.c) {
            try {
                super.close();
            } catch (IOException e) {
                this.c = true;
                a(e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.M48, com.fossil.A58, java.io.Flushable
    public void flush() throws IOException {
        if (!this.c) {
            try {
                super.flush();
            } catch (IOException e) {
                this.c = true;
                a(e);
            }
        }
    }
}
