package com.fossil;

import android.net.ConnectivityManager;
import android.net.Network;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zn5 extends ConnectivityManager.NetworkCallback {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public Th5 a; // = Th5.UNKNOWN;

    /*
    static {
        String simpleName = Zn5.class.getSimpleName();
        Wg6.b(simpleName, "NetworkStateChangeCallback::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public abstract void a(Th5 th5);

    @DexIgnore
    public final void onAvailable(Network network) {
        Wg6.c(network, "network");
        super.onAvailable(network);
        Th5 th5 = this.a == Th5.DISCONNECTED ? Th5.RECONNECTED : Th5.CONNECTED;
        this.a = th5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onAvailable new state " + this.a);
        a(th5);
    }

    @DexIgnore
    public final void onLost(Network network) {
        Wg6.c(network, "network");
        super.onLost(network);
        this.a = Th5.DISCONNECTED;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onLost new state " + this.a);
        a(Th5.DISCONNECTED);
    }

    @DexIgnore
    public final void onUnavailable() {
        super.onUnavailable();
        this.a = Th5.DISCONNECTED;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onUnavailable new state " + this.a);
        a(Th5.DISCONNECTED);
    }
}
