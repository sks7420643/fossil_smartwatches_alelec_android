package com.fossil;

import android.os.Bundle;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O64 {
    @DexIgnore
    public static /* final */ Set<String> a; // = new HashSet(Arrays.asList("_in", "_xa", "_xu", "_aq", "_aa", "_ai", "_ac", "campaign_details", "_ug", "_iapx", "_exp_set", "_exp_clear", "_exp_activate", "_exp_timeout", "_exp_expire"));
    @DexIgnore
    public static /* final */ List<String> b; // = Arrays.asList("_e", "_f", "_iap", "_s", "_au", "_ui", "_cd");
    @DexIgnore
    public static /* final */ List<String> c; // = Arrays.asList("auto", "app", "am");
    @DexIgnore
    public static /* final */ List<String> d; // = Arrays.asList("_r", "_dbg");
    @DexIgnore
    public static /* final */ List<String> e; // = Arrays.asList((String[]) Bf2.a(Qn3.a, Qn3.b));
    @DexIgnore
    public static /* final */ List<String> f; // = Arrays.asList("^_ltv_[A-Z]{3}$", "^_cc[1-5]{1}$");

    @DexIgnore
    public static boolean a(String str) {
        return !c.contains(str);
    }

    @DexIgnore
    public static boolean b(String str, Bundle bundle) {
        if (b.contains(str)) {
            return false;
        }
        if (bundle != null) {
            for (String str2 : d) {
                if (bundle.containsKey(str2)) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean c(String str, String str2) {
        if ("_ce1".equals(str2) || "_ce2".equals(str2)) {
            return str.equals("fcm") || str.equals("frc");
        }
        if ("_ln".equals(str2)) {
            return str.equals("fcm") || str.equals("fiam");
        }
        if (e.contains(str2)) {
            return false;
        }
        for (String str3 : f) {
            if (str2.matches(str3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean d(String str, String str2, Bundle bundle) {
        if (!"_cmp".equals(str2)) {
            return true;
        }
        if (!a(str)) {
            return false;
        }
        if (bundle == null) {
            return false;
        }
        for (String str3 : d) {
            if (bundle.containsKey(str3)) {
                return false;
            }
        }
        char c2 = '\uffff';
        int hashCode = str.hashCode();
        if (hashCode != 101200) {
            if (hashCode != 101230) {
                if (hashCode == 3142703 && str.equals("fiam")) {
                    c2 = 2;
                }
            } else if (str.equals("fdl")) {
                c2 = 1;
            }
        } else if (str.equals("fcm")) {
            c2 = 0;
        }
        if (c2 == 0) {
            bundle.putString("_cis", "fcm_integration");
            return true;
        } else if (c2 == 1) {
            bundle.putString("_cis", "fdl_integration");
            return true;
        } else if (c2 != 2) {
            return false;
        } else {
            bundle.putString("_cis", "fiam_integration");
            return true;
        }
    }

    @DexIgnore
    public static void e(String str, String str2, Bundle bundle) {
        if ("clx".equals(str) && "_ae".equals(str2)) {
            bundle.putLong("_r", 1);
        }
    }

    @DexIgnore
    public static boolean f(String str) {
        return !a.contains(str);
    }

    @DexIgnore
    public static String g(String str) {
        String a2 = On3.a(str);
        return a2 != null ? a2 : str;
    }
}
