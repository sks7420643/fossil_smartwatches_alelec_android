package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L05 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore
    public L05() {
        Zi4 zi4 = new Zi4();
        zi4.f(Date.class, new GsonConverterShortDate());
        Gson d = zi4.d();
        Wg6.b(d, "GsonBuilder().registerTy\u2026rterShortDate()).create()");
        this.a = d;
    }

    @DexIgnore
    public final String a(SleepStatistic.SleepDailyBest sleepDailyBest) {
        if (sleepDailyBest == null) {
            return null;
        }
        try {
            return this.a.t(sleepDailyBest);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepStatisticConverter", "fromSleepDailyBest - e=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final SleepStatistic.SleepDailyBest b(String str) {
        SleepStatistic.SleepDailyBest sleepDailyBest;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            sleepDailyBest = (SleepStatistic.SleepDailyBest) this.a.k(str, SleepStatistic.SleepDailyBest.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepStatisticConverter", "toSleepDailyBest - e=" + e);
            e.printStackTrace();
            sleepDailyBest = null;
        }
        return sleepDailyBest;
    }
}
