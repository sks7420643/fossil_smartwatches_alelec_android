package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nq1 extends Lq1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Nq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nq1 createFromParcel(Parcel parcel) {
            return new Nq1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nq1[] newArray(int i) {
            return new Nq1[i];
        }
    }

    @DexIgnore
    public Nq1(byte b, Bv1 bv1) {
        super(E90.RING_MY_PHONE_MICRO_APP, b, bv1);
    }

    @DexIgnore
    public /* synthetic */ Nq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }
}
