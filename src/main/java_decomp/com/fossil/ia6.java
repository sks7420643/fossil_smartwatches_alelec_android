package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Cr4;
import com.fossil.Rw5;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ia6 extends BaseFragment implements Sa6, Cr4.Ai {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public G37<Hc5> g;
    @DexIgnore
    public Ra6 h;
    @DexIgnore
    public Rw5 i;
    @DexIgnore
    public Cr4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ia6 a() {
            return new Ia6();
        }

        @DexIgnore
        public final String b() {
            return Ia6.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Hc5 c;

        @DexIgnore
        public Bi(View view, Hc5 hc5) {
            this.b = view;
            this.c = hc5;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                Hc5 hc5 = this.c;
                Wg6.b(hc5, "binding");
                hc5.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = N76.s.b();
                local.d(b2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                Wg6.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ia6 b;
        @DexIgnore
        public /* final */ /* synthetic */ Hc5 c;

        @DexIgnore
        public Ci(Ia6 ia6, Hc5 hc5) {
            this.b = ia6;
            this.c = hc5;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            Cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.f(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                Wg6.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = Ia6.m.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                if (this.b.i != null && !TextUtils.isEmpty(f.getPlaceId())) {
                    this.c.q.setText("");
                    Ra6 M6 = Ia6.M6(this.b);
                    String spannableString = fullText.toString();
                    Wg6.b(spannableString, "primaryText.toString()");
                    String placeId = f.getPlaceId();
                    Wg6.b(placeId, "item.placeId");
                    Cr4 cr42 = this.b.j;
                    if (cr42 != null) {
                        M6.n(spannableString, placeId, cr42.g());
                        Cr4 cr43 = this.b.j;
                        if (cr43 != null) {
                            cr43.d();
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Hc5 b;

        @DexIgnore
        public Di(Ia6 ia6, Hc5 hc5) {
            this.b = hc5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.b.s;
            Wg6.b(imageView, "binding.clearIv");
            imageView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ia6 b;

        @DexIgnore
        public Ei(Ia6 ia6, Hc5 hc5) {
            this.b = ia6;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            Wg6.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Rw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ia6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(Ia6 ia6) {
            this.a = ia6;
        }

        @DexIgnore
        @Override // com.fossil.Rw5.Bi
        public void a(int i, boolean z) {
            Ia6.M6(this.a).q(i, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ia6 b;

        @DexIgnore
        public Gi(Ia6 ia6) {
            this.b = ia6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Hc5 b;

        @DexIgnore
        public Hi(Hc5 hc5) {
            this.b = hc5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText("");
        }
    }

    /*
    static {
        String simpleName = Ia6.class.getSimpleName();
        Wg6.b(simpleName, "WeatherSettingFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Ra6 M6(Ia6 ia6) {
        Ra6 ra6 = ia6.h;
        if (ra6 != null) {
            return ra6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Cr4.Ai
    public void D1(boolean z) {
        G37<Hc5> g37 = this.g;
        if (g37 != null) {
            Hc5 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        Q6();
                        return;
                    }
                }
                O6();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Sa6
    public void E4() {
        FLogger.INSTANCE.getLocal().d(l, "showLocationError");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.J(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        G37<Hc5> g37 = this.g;
        if (g37 == null) {
            Wg6.n("mBinding");
            throw null;
        } else if (g37.a() == null || this.i == null) {
            return true;
        } else {
            Ra6 ra6 = this.h;
            if (ra6 != null) {
                ra6.p();
                return true;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Sa6
    public void G1() {
        if (isActive()) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886393);
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            Wg6.b(c, "errorDescription");
            s37.K(childFragmentManager, c);
        }
    }

    @DexIgnore
    @Override // com.fossil.Sa6
    public void H(PlacesClient placesClient) {
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView;
        if (isActive()) {
            Context requireContext = requireContext();
            Wg6.b(requireContext, "requireContext()");
            Cr4 cr4 = new Cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            G37<Hc5> g37 = this.g;
            if (g37 != null) {
                Hc5 a2 = g37.a();
                if (a2 != null && (flexibleAutoCompleteTextView = a2.q) != null) {
                    flexibleAutoCompleteTextView.setAdapter(this.j);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ra6 ra6) {
        P6(ra6);
    }

    @DexIgnore
    public final void O6() {
        G37<Hc5> g37 = this.g;
        if (g37 != null) {
            Hc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                Wg6.b(flexibleTextView, "it.ftvLocationError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.y;
                Wg6.b(flexibleTextView2, "it.tvAdded");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = a2.w;
                Wg6.b(recyclerView, "it.recyclerView");
                recyclerView.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void P6(Ra6 ra6) {
        Wg6.c(ra6, "presenter");
        this.h = ra6;
    }

    @DexIgnore
    public final void Q6() {
        G37<Hc5> g37 = this.g;
        if (g37 != null) {
            Hc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                Wg6.b(flexibleTextView, "it.ftvLocationError");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886813);
                Wg6.b(c, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                String format = String.format(c, Arrays.copyOf(new Object[]{flexibleAutoCompleteTextView.getText()}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                FlexibleTextView flexibleTextView2 = a2.t;
                Wg6.b(flexibleTextView2, "it.ftvLocationError");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.y;
                Wg6.b(flexibleTextView3, "it.tvAdded");
                flexibleTextView3.setVisibility(8);
                RecyclerView recyclerView = a2.w;
                Wg6.b(recyclerView, "it.recyclerView");
                recyclerView.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Sa6
    public void h2(List<WeatherLocationWrapper> list) {
        Wg6.c(list, "locations");
        Rw5 rw5 = this.i;
        if (rw5 != null) {
            rw5.k(Ir7.b(list));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Hc5 hc5 = (Hc5) Aq0.f(layoutInflater, 2131558640, viewGroup, false, A6());
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = hc5.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(W6.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new Ci(this, hc5));
        flexibleAutoCompleteTextView.addTextChangedListener(new Di(this, hc5));
        flexibleAutoCompleteTextView.setOnKeyListener(new Ei(this, hc5));
        Rw5 rw5 = new Rw5();
        this.i = rw5;
        rw5.l(new Fi(this));
        RecyclerView recyclerView = hc5.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        Wg6.b(hc5, "binding");
        View n = hc5.n();
        Wg6.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new Bi(n, hc5));
        hc5.r.setOnClickListener(new Gi(this));
        hc5.s.setOnClickListener(new Hi(hc5));
        this.g = new G37<>(this, hc5);
        return hc5.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Ra6 ra6 = this.h;
        if (ra6 != null) {
            ra6.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Ra6 ra6 = this.h;
        if (ra6 != null) {
            ra6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Sa6
    public void x0(boolean z) {
        if (z) {
            Intent intent = new Intent();
            Ra6 ra6 = this.h;
            if (ra6 != null) {
                intent.putExtra("WEATHER_WATCH_APP_SETTING", ra6.o());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }
}
