package com.fossil;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ui1 {
    @DexIgnore
    public static /* final */ Gd1<?, ?, ?> c; // = new Gd1<>(Object.class, Object.class, Object.class, Collections.singletonList(new Vc1(Object.class, Object.class, Object.class, Collections.emptyList(), new Vh1(), null)), null);
    @DexIgnore
    public /* final */ Zi0<Hk1, Gd1<?, ?, ?>> a; // = new Zi0<>();
    @DexIgnore
    public /* final */ AtomicReference<Hk1> b; // = new AtomicReference<>();

    @DexIgnore
    public <Data, TResource, Transcode> Gd1<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        Gd1<Data, TResource, Transcode> gd1;
        Hk1 b2 = b(cls, cls2, cls3);
        synchronized (this.a) {
            gd1 = (Gd1<Data, TResource, Transcode>) this.a.get(b2);
        }
        this.b.set(b2);
        return gd1;
    }

    @DexIgnore
    public final Hk1 b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        Hk1 andSet = this.b.getAndSet(null);
        if (andSet == null) {
            andSet = new Hk1();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    public boolean c(Gd1<?, ?, ?> gd1) {
        return c.equals(gd1);
    }

    @DexIgnore
    public void d(Class<?> cls, Class<?> cls2, Class<?> cls3, Gd1<?, ?, ?> gd1) {
        synchronized (this.a) {
            Zi0<Hk1, Gd1<?, ?, ?>> zi0 = this.a;
            Hk1 hk1 = new Hk1(cls, cls2, cls3);
            if (gd1 == null) {
                gd1 = c;
            }
            zi0.put(hk1, gd1);
        }
    }
}
