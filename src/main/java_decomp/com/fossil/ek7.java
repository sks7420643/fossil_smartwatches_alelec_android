package com.fossil;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ek7 extends AppCompatActivity implements Dk7 {
    @DexIgnore
    public Ck7<Object> b;

    @DexIgnore
    @Override // com.fossil.Dk7
    public Vj7<Object> c() {
        return this.b;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        Uj7.a(this);
        super.onCreate(bundle);
    }
}
