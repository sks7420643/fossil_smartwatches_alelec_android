package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ns4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f2561a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<List<? extends ms4>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends ms4>> {
    }

    @DexIgnore
    public ns4() {
        Gson d = new zi4().d();
        pq7.b(d, "GsonBuilder().create()");
        this.f2561a = d;
    }

    @DexIgnore
    public final String a(List<ms4> list) {
        pq7.c(list, "players");
        try {
            String t = this.f2561a.t(list);
            pq7.b(t, "gson.toJson(players)");
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final List<ms4> b(String str) {
        List<ms4> e;
        try {
            Object l = this.f2561a.l(str, new a().getType());
            pq7.b(l, "gson.fromJson(value, obj\u2026ist<BCPlayer>>() {}.type)");
            return (List) l;
        } catch (Exception e2) {
            if (!(e2 instanceof mj4)) {
                return hm7.e();
            }
            try {
                FLogger.INSTANCE.getLocal().e("BCPlayerConverter", "stringToPlayers - apply date format for special case");
                zi4 zi4 = new zi4();
                zi4.g("MM dd, yyyy HH:mm:ss");
                e = (List) zi4.d().l(str, new b().getType());
            } catch (Exception e3) {
                e3.printStackTrace();
                e = hm7.e();
            }
            pq7.b(e, "try {\n                  \u2026ayer>()\n                }");
            return e;
        }
    }
}
