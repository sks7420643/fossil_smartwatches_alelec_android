package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rm3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Rn3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Pm3 c;

    @DexIgnore
    public Rm3(Pm3 pm3, Rn3 rn3) {
        this.c = pm3;
        this.b = rn3;
    }

    @DexIgnore
    public final void run() {
        this.c.k(this.b);
        this.c.f();
    }
}
