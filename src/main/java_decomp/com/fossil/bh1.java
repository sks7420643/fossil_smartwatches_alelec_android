package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bh1 implements Qb1<Uri, Drawable> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Bh1(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(Uri uri, Ob1 ob1) throws IOException {
        return h(uri, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Drawable> b(Uri uri, int i, int i2, Ob1 ob1) throws IOException {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Drawable> c(Uri uri, int i, int i2, Ob1 ob1) {
        Context d = d(uri, uri.getAuthority());
        return Ah1.f(Yg1.b(this.a, d, g(d, uri)));
    }

    @DexIgnore
    public final Context d(Uri uri, String str) {
        if (str.equals(this.a.getPackageName())) {
            return this.a;
        }
        try {
            return this.a.createPackageContext(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            if (str.contains(this.a.getPackageName())) {
                return this.a;
            }
            throw new IllegalArgumentException("Failed to obtain context or unrecognized Uri format for: " + uri, e);
        }
    }

    @DexIgnore
    public final int e(Uri uri) {
        try {
            return Integer.parseInt(uri.getPathSegments().get(0));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Unrecognized Uri format: " + uri, e);
        }
    }

    @DexIgnore
    public final int f(Context context, Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        String authority = uri.getAuthority();
        String str = pathSegments.get(0);
        String str2 = pathSegments.get(1);
        int identifier = context.getResources().getIdentifier(str2, str, authority);
        int identifier2 = identifier == 0 ? Resources.getSystem().getIdentifier(str2, str, "android") : identifier;
        if (identifier2 != 0) {
            return identifier2;
        }
        throw new IllegalArgumentException("Failed to find resource id for: " + uri);
    }

    @DexIgnore
    public final int g(Context context, Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 2) {
            return f(context, uri);
        }
        if (pathSegments.size() == 1) {
            return e(uri);
        }
        throw new IllegalArgumentException("Unrecognized Uri format: " + uri);
    }

    @DexIgnore
    public boolean h(Uri uri, Ob1 ob1) {
        return uri.getScheme().equals("android.resource");
    }
}
