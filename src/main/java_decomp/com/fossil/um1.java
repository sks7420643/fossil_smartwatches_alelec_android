package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Um1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ int g; // = Hy1.a(Gr7.a);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Um1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Um1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new Um1(Hy1.n(order.getShort(0)), Hy1.n(order.getShort(2)), Hy1.n(order.getShort(4)), Hy1.n(order.getShort(6)));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        public Um1 b(Parcel parcel) {
            return new Um1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Um1 createFromParcel(Parcel parcel) {
            return new Um1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Um1[] newArray(int i) {
            return new Um1[i];
        }
    }

    @DexIgnore
    public Um1(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        super(Zm1.DAILY_SLEEP);
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
        d();
    }

    @DexIgnore
    public /* synthetic */ Um1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).putShort((short) this.f).array();
        Wg6.b(array, "ByteBuffer.allocate(Dail\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(G80.k(G80.k(G80.k(jSONObject, Jd0.P1, Integer.valueOf(this.c)), Jd0.Q1, Integer.valueOf(this.d)), Jd0.R1, Integer.valueOf(this.e)), Jd0.S1, Integer.valueOf(this.f));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        int i = g;
        int i2 = this.c;
        if (i2 >= 0 && i >= i2) {
            int i3 = g;
            int i4 = this.d;
            if (i4 >= 0 && i3 >= i4) {
                int i5 = g;
                int i6 = this.e;
                if (i6 >= 0 && i5 >= i6) {
                    int i7 = g;
                    int i8 = this.f;
                    if (i8 < 0 || i7 < i8) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder e2 = E.e("deepSleepInMinute (");
                        e2.append(this.f);
                        e2.append(") is out of range ");
                        e2.append("[0, ");
                        throw new IllegalArgumentException(E.b(e2, g, "]."));
                    }
                    return;
                }
                StringBuilder e3 = E.e("lightSleepInMinute (");
                e3.append(this.e);
                e3.append(") is out of range ");
                e3.append("[0, ");
                throw new IllegalArgumentException(E.b(e3, g, "]."));
            }
            StringBuilder e4 = E.e("awakeInMinute (");
            e4.append(this.d);
            e4.append(") is out of range ");
            e4.append("[0, ");
            throw new IllegalArgumentException(E.b(e4, g, "]."));
        }
        StringBuilder e5 = E.e("totalSleepInMinute (");
        e5.append(this.c);
        e5.append(") is out of range ");
        e5.append("[0, ");
        throw new IllegalArgumentException(E.b(e5, g, "]."));
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Um1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Um1 um1 = (Um1) obj;
            if (this.c != um1.c) {
                return false;
            }
            if (this.d != um1.d) {
                return false;
            }
            if (this.e != um1.e) {
                return false;
            }
            return this.f == um1.f;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailySleepConfig");
    }

    @DexIgnore
    public final int getAwakeInMinute() {
        return this.d;
    }

    @DexIgnore
    public final int getDeepSleepInMinute() {
        return this.f;
    }

    @DexIgnore
    public final int getLightSleepInMinute() {
        return this.e;
    }

    @DexIgnore
    public final int getTotalSleepInMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (((((this.c * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
