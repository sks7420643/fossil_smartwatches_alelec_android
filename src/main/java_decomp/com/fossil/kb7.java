package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Kb7 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Lb7.values().length];
        a = iArr;
        iArr[Lb7.STEP.ordinal()] = 1;
        a[Lb7.ACTIVE.ordinal()] = 2;
        a[Lb7.BATTERY.ordinal()] = 3;
        a[Lb7.CALORIES.ordinal()] = 4;
        a[Lb7.RAIN.ordinal()] = 5;
        a[Lb7.DATE.ordinal()] = 6;
        a[Lb7.SECOND_TIME.ordinal()] = 7;
        a[Lb7.WEATHER.ordinal()] = 8;
        a[Lb7.HEART.ordinal()] = 9;
    }
    */
}
