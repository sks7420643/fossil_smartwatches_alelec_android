package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qs extends Ss {
    @DexIgnore
    public UUID[] A; // = new UUID[0];
    @DexIgnore
    public N6[] B; // = new N6[0];
    @DexIgnore
    public long C; // = ButtonService.CONNECT_TIMEOUT;

    @DexIgnore
    public Qs(K5 k5) {
        super(Hs.e, k5);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(G80.k(G80.k(G80.k(super.A(), Jd0.o1, Ey1.a(this.y.H())), Jd0.f5, Integer.valueOf(this.y.A.getType())), Jd0.g1, G80.j(this.A)), Jd0.h1, G80.g(this.B));
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new D6(this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void g(U5 u5) {
        D6 d6 = (D6) u5;
        this.A = d6.k;
        this.B = d6.l;
        this.g.add(new Hw(0, null, null, G80.k(G80.k(new JSONObject(), Jd0.g1, G80.j(this.A)), Jd0.h1, G80.g(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.Rs, com.fossil.Fs
    public void v(U5 u5) {
        JSONObject jSONObject;
        this.v = Mw.a(this.v, null, null, Mw.g.a(u5.e).d, u5.e, null, 19);
        A90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        A90 a902 = this.f;
        if (!(a902 == null || (jSONObject = a902.n) == null)) {
            G80.k(jSONObject, Jd0.k, Ey1.a(Lw.b));
        }
        if (this.v.d == Lw.b) {
            g(u5);
        }
        m(Mw.a(this.v, null, null, (this.A.length == 0) ^ true ? Lw.b : Lw.s, null, null, 27));
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.o1, Ey1.a(this.y.H())), Jd0.f5, Integer.valueOf(this.y.A.getType()));
    }
}
