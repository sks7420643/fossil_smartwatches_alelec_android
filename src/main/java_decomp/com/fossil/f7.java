package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum F7 {
    b(0),
    c(1),
    d(2),
    e(3),
    f(4),
    g(5),
    h(6),
    i(256),
    j(257),
    k(258),
    l(511);
    
    @DexIgnore
    public static /* final */ E7 n; // = new E7(null);

    @DexIgnore
    public F7(int i2) {
    }
}
