package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C76 implements MembersInjector<WatchAppEditActivity> {
    @DexIgnore
    public static void a(WatchAppEditActivity watchAppEditActivity, WatchAppEditPresenter watchAppEditPresenter) {
        watchAppEditActivity.A = watchAppEditPresenter;
    }

    @DexIgnore
    public static void b(WatchAppEditActivity watchAppEditActivity, Po4 po4) {
        watchAppEditActivity.B = po4;
    }
}
