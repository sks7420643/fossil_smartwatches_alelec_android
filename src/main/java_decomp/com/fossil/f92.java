package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class F92 {
    @DexIgnore
    public /* final */ D92 a;

    @DexIgnore
    public F92(D92 d92) {
        this.a = d92;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b(C92 c92) {
        c92.b.lock();
        try {
            if (c92.l == this.a) {
                a();
                c92.b.unlock();
            }
        } finally {
            c92.b.unlock();
        }
    }
}
