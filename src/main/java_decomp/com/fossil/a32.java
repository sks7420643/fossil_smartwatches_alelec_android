package com.fossil;

import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class A32 implements J32.Di {
    @DexIgnore
    public /* final */ P32 a;

    @DexIgnore
    public A32(P32 p32) {
        this.a = p32;
    }

    @DexIgnore
    public static J32.Di b(P32 p32) {
        return new A32(p32);
    }

    @DexIgnore
    @Override // com.fossil.J32.Di
    public Object a() {
        return this.a.getWritableDatabase();
    }
}
