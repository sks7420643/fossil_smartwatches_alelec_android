package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ks4 {
    @DexIgnore
    @Vu3("challengeId")
    public String a;
    @DexIgnore
    @Vu3("numberOfPlayers")
    public Integer b;
    @DexIgnore
    @Vu3("topPlayers")
    public List<Ms4> c;
    @DexIgnore
    @Vu3("nearPlayers")
    public List<Ms4> d;

    @DexIgnore
    public Ks4(String str, Integer num, List<Ms4> list, List<Ms4> list2) {
        Wg6.c(str, "challengeId");
        this.a = str;
        this.b = num;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final List<Ms4> b() {
        return this.d;
    }

    @DexIgnore
    public final Integer c() {
        return this.b;
    }

    @DexIgnore
    public final List<Ms4> d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ks4) {
                Ks4 ks4 = (Ks4) obj;
                if (!Wg6.a(this.a, ks4.a) || !Wg6.a(this.b, ks4.b) || !Wg6.a(this.c, ks4.c) || !Wg6.a(this.d, ks4.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        Integer num = this.b;
        int hashCode2 = num != null ? num.hashCode() : 0;
        List<Ms4> list = this.c;
        int hashCode3 = list != null ? list.hashCode() : 0;
        List<Ms4> list2 = this.d;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BCDisplayPlayer(challengeId=" + this.a + ", numberOfPlayers=" + this.b + ", topPlayers=" + this.c + ", nearPlayers=" + this.d + ")";
    }
}
