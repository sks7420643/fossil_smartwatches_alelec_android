package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oh1 implements Qb1<InputStream, Hh1> {
    @DexIgnore
    public /* final */ List<ImageHeaderParser> a;
    @DexIgnore
    public /* final */ Qb1<ByteBuffer, Hh1> b;
    @DexIgnore
    public /* final */ Od1 c;

    @DexIgnore
    public Oh1(List<ImageHeaderParser> list, Qb1<ByteBuffer, Hh1> qb1, Od1 od1) {
        this.a = list;
        this.b = qb1;
        this.c = od1;
    }

    @DexIgnore
    public static byte[] e(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (Log.isLoggable("StreamGifDecoder", 5)) {
                Log.w("StreamGifDecoder", "Error reading data from stream", e);
            }
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(InputStream inputStream, Ob1 ob1) throws IOException {
        return d(inputStream, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Hh1> b(InputStream inputStream, int i, int i2, Ob1 ob1) throws IOException {
        return c(inputStream, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Hh1> c(InputStream inputStream, int i, int i2, Ob1 ob1) throws IOException {
        byte[] e = e(inputStream);
        if (e == null) {
            return null;
        }
        return this.b.b(ByteBuffer.wrap(e), i, i2, ob1);
    }

    @DexIgnore
    public boolean d(InputStream inputStream, Ob1 ob1) throws IOException {
        return !((Boolean) ob1.c(Nh1.b)).booleanValue() && Lb1.e(this.a, inputStream, this.c) == ImageHeaderParser.ImageType.GIF;
    }
}
