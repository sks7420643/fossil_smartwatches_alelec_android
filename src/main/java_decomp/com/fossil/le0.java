package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Le0 {
    @DexIgnore
    public static /* final */ int actionBarDivider; // = 2130968623;
    @DexIgnore
    public static /* final */ int actionBarItemBackground; // = 2130968624;
    @DexIgnore
    public static /* final */ int actionBarPopupTheme; // = 2130968625;
    @DexIgnore
    public static /* final */ int actionBarSize; // = 2130968626;
    @DexIgnore
    public static /* final */ int actionBarSplitStyle; // = 2130968627;
    @DexIgnore
    public static /* final */ int actionBarStyle; // = 2130968628;
    @DexIgnore
    public static /* final */ int actionBarTabBarStyle; // = 2130968629;
    @DexIgnore
    public static /* final */ int actionBarTabStyle; // = 2130968630;
    @DexIgnore
    public static /* final */ int actionBarTabTextStyle; // = 2130968631;
    @DexIgnore
    public static /* final */ int actionBarTheme; // = 2130968632;
    @DexIgnore
    public static /* final */ int actionBarWidgetTheme; // = 2130968633;
    @DexIgnore
    public static /* final */ int actionButtonStyle; // = 2130968634;
    @DexIgnore
    public static /* final */ int actionDropDownStyle; // = 2130968635;
    @DexIgnore
    public static /* final */ int actionLayout; // = 2130968636;
    @DexIgnore
    public static /* final */ int actionMenuTextAppearance; // = 2130968637;
    @DexIgnore
    public static /* final */ int actionMenuTextColor; // = 2130968638;
    @DexIgnore
    public static /* final */ int actionModeBackground; // = 2130968639;
    @DexIgnore
    public static /* final */ int actionModeCloseButtonStyle; // = 2130968640;
    @DexIgnore
    public static /* final */ int actionModeCloseDrawable; // = 2130968641;
    @DexIgnore
    public static /* final */ int actionModeCopyDrawable; // = 2130968642;
    @DexIgnore
    public static /* final */ int actionModeCutDrawable; // = 2130968643;
    @DexIgnore
    public static /* final */ int actionModeFindDrawable; // = 2130968644;
    @DexIgnore
    public static /* final */ int actionModePasteDrawable; // = 2130968645;
    @DexIgnore
    public static /* final */ int actionModePopupWindowStyle; // = 2130968646;
    @DexIgnore
    public static /* final */ int actionModeSelectAllDrawable; // = 2130968647;
    @DexIgnore
    public static /* final */ int actionModeShareDrawable; // = 2130968648;
    @DexIgnore
    public static /* final */ int actionModeSplitBackground; // = 2130968649;
    @DexIgnore
    public static /* final */ int actionModeStyle; // = 2130968650;
    @DexIgnore
    public static /* final */ int actionModeWebSearchDrawable; // = 2130968651;
    @DexIgnore
    public static /* final */ int actionOverflowButtonStyle; // = 2130968652;
    @DexIgnore
    public static /* final */ int actionOverflowMenuStyle; // = 2130968653;
    @DexIgnore
    public static /* final */ int actionProviderClass; // = 2130968654;
    @DexIgnore
    public static /* final */ int actionViewClass; // = 2130968656;
    @DexIgnore
    public static /* final */ int activityChooserViewStyle; // = 2130968660;
    @DexIgnore
    public static /* final */ int alertDialogButtonGroupStyle; // = 2130968700;
    @DexIgnore
    public static /* final */ int alertDialogCenterButtons; // = 2130968701;
    @DexIgnore
    public static /* final */ int alertDialogStyle; // = 2130968702;
    @DexIgnore
    public static /* final */ int alertDialogTheme; // = 2130968703;
    @DexIgnore
    public static /* final */ int allowStacking; // = 2130968706;
    @DexIgnore
    public static /* final */ int alpha; // = 2130968707;
    @DexIgnore
    public static /* final */ int alphabeticModifiers; // = 2130968709;
    @DexIgnore
    public static /* final */ int arrowHeadLength; // = 2130968714;
    @DexIgnore
    public static /* final */ int arrowShaftLength; // = 2130968715;
    @DexIgnore
    public static /* final */ int autoCompleteTextViewStyle; // = 2130968717;
    @DexIgnore
    public static /* final */ int autoSizeMaxTextSize; // = 2130968719;
    @DexIgnore
    public static /* final */ int autoSizeMinTextSize; // = 2130968720;
    @DexIgnore
    public static /* final */ int autoSizePresetSizes; // = 2130968721;
    @DexIgnore
    public static /* final */ int autoSizeStepGranularity; // = 2130968722;
    @DexIgnore
    public static /* final */ int autoSizeTextType; // = 2130968723;
    @DexIgnore
    public static /* final */ int background; // = 2130968728;
    @DexIgnore
    public static /* final */ int backgroundSplit; // = 2130968738;
    @DexIgnore
    public static /* final */ int backgroundStacked; // = 2130968739;
    @DexIgnore
    public static /* final */ int backgroundTint; // = 2130968740;
    @DexIgnore
    public static /* final */ int backgroundTintMode; // = 2130968741;
    @DexIgnore
    public static /* final */ int barLength; // = 2130968747;
    @DexIgnore
    public static /* final */ int borderlessButtonStyle; // = 2130968822;
    @DexIgnore
    public static /* final */ int buttonBarButtonStyle; // = 2130968841;
    @DexIgnore
    public static /* final */ int buttonBarNegativeButtonStyle; // = 2130968842;
    @DexIgnore
    public static /* final */ int buttonBarNeutralButtonStyle; // = 2130968843;
    @DexIgnore
    public static /* final */ int buttonBarPositiveButtonStyle; // = 2130968844;
    @DexIgnore
    public static /* final */ int buttonBarStyle; // = 2130968845;
    @DexIgnore
    public static /* final */ int buttonCompat; // = 2130968846;
    @DexIgnore
    public static /* final */ int buttonGravity; // = 2130968847;
    @DexIgnore
    public static /* final */ int buttonIconDimen; // = 2130968848;
    @DexIgnore
    public static /* final */ int buttonPanelSideLayout; // = 2130968849;
    @DexIgnore
    public static /* final */ int buttonStyle; // = 2130968851;
    @DexIgnore
    public static /* final */ int buttonStyleSmall; // = 2130968852;
    @DexIgnore
    public static /* final */ int buttonTint; // = 2130968853;
    @DexIgnore
    public static /* final */ int buttonTintMode; // = 2130968854;
    @DexIgnore
    public static /* final */ int checkboxStyle; // = 2130968887;
    @DexIgnore
    public static /* final */ int checkedTextViewStyle; // = 2130968894;
    @DexIgnore
    public static /* final */ int closeIcon; // = 2130968917;
    @DexIgnore
    public static /* final */ int closeItemLayout; // = 2130968924;
    @DexIgnore
    public static /* final */ int collapseContentDescription; // = 2130968926;
    @DexIgnore
    public static /* final */ int collapseIcon; // = 2130968927;
    @DexIgnore
    public static /* final */ int color; // = 2130968930;
    @DexIgnore
    public static /* final */ int colorAccent; // = 2130968931;
    @DexIgnore
    public static /* final */ int colorBackgroundFloating; // = 2130968932;
    @DexIgnore
    public static /* final */ int colorButtonNormal; // = 2130968933;
    @DexIgnore
    public static /* final */ int colorControlActivated; // = 2130968934;
    @DexIgnore
    public static /* final */ int colorControlHighlight; // = 2130968935;
    @DexIgnore
    public static /* final */ int colorControlNormal; // = 2130968936;
    @DexIgnore
    public static /* final */ int colorError; // = 2130968937;
    @DexIgnore
    public static /* final */ int colorPrimary; // = 2130968944;
    @DexIgnore
    public static /* final */ int colorPrimaryDark; // = 2130968945;
    @DexIgnore
    public static /* final */ int colorSwitchThumbNormal; // = 2130968952;
    @DexIgnore
    public static /* final */ int commitIcon; // = 2130968965;
    @DexIgnore
    public static /* final */ int contentDescription; // = 2130968978;
    @DexIgnore
    public static /* final */ int contentInsetEnd; // = 2130968979;
    @DexIgnore
    public static /* final */ int contentInsetEndWithActions; // = 2130968980;
    @DexIgnore
    public static /* final */ int contentInsetLeft; // = 2130968981;
    @DexIgnore
    public static /* final */ int contentInsetRight; // = 2130968982;
    @DexIgnore
    public static /* final */ int contentInsetStart; // = 2130968983;
    @DexIgnore
    public static /* final */ int contentInsetStartWithNavigation; // = 2130968984;
    @DexIgnore
    public static /* final */ int controlBackground; // = 2130968991;
    @DexIgnore
    public static /* final */ int customNavigationLayout; // = 2130969066;
    @DexIgnore
    public static /* final */ int defaultQueryHint; // = 2130969097;
    @DexIgnore
    public static /* final */ int dialogCornerRadius; // = 2130969108;
    @DexIgnore
    public static /* final */ int dialogPreferredPadding; // = 2130969109;
    @DexIgnore
    public static /* final */ int dialogTheme; // = 2130969110;
    @DexIgnore
    public static /* final */ int displayOptions; // = 2130969111;
    @DexIgnore
    public static /* final */ int divider; // = 2130969112;
    @DexIgnore
    public static /* final */ int dividerHorizontal; // = 2130969113;
    @DexIgnore
    public static /* final */ int dividerPadding; // = 2130969114;
    @DexIgnore
    public static /* final */ int dividerVertical; // = 2130969115;
    @DexIgnore
    public static /* final */ int drawableBottomCompat; // = 2130969118;
    @DexIgnore
    public static /* final */ int drawableEndCompat; // = 2130969119;
    @DexIgnore
    public static /* final */ int drawableLeftCompat; // = 2130969120;
    @DexIgnore
    public static /* final */ int drawableRightCompat; // = 2130969121;
    @DexIgnore
    public static /* final */ int drawableSize; // = 2130969122;
    @DexIgnore
    public static /* final */ int drawableStartCompat; // = 2130969123;
    @DexIgnore
    public static /* final */ int drawableTint; // = 2130969124;
    @DexIgnore
    public static /* final */ int drawableTintMode; // = 2130969125;
    @DexIgnore
    public static /* final */ int drawableTopCompat; // = 2130969126;
    @DexIgnore
    public static /* final */ int drawerArrowStyle; // = 2130969127;
    @DexIgnore
    public static /* final */ int dropDownListViewStyle; // = 2130969128;
    @DexIgnore
    public static /* final */ int dropdownListPreferredItemHeight; // = 2130969129;
    @DexIgnore
    public static /* final */ int editTextBackground; // = 2130969132;
    @DexIgnore
    public static /* final */ int editTextColor; // = 2130969133;
    @DexIgnore
    public static /* final */ int editTextStyle; // = 2130969134;
    @DexIgnore
    public static /* final */ int elevation; // = 2130969137;
    @DexIgnore
    public static /* final */ int expandActivityOverflowButtonDrawable; // = 2130969160;
    @DexIgnore
    public static /* final */ int firstBaselineToTopHeight; // = 2130969185;
    @DexIgnore
    public static /* final */ int font; // = 2130969196;
    @DexIgnore
    public static /* final */ int fontFamily; // = 2130969197;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969198;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969199;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969200;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969201;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969202;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969203;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969204;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969205;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969206;
    @DexIgnore
    public static /* final */ int gapBetweenBars; // = 2130969213;
    @DexIgnore
    public static /* final */ int goIcon; // = 2130969215;
    @DexIgnore
    public static /* final */ int height; // = 2130969237;
    @DexIgnore
    public static /* final */ int hideOnContentScroll; // = 2130969244;
    @DexIgnore
    public static /* final */ int homeAsUpIndicator; // = 2130969252;
    @DexIgnore
    public static /* final */ int homeLayout; // = 2130969253;
    @DexIgnore
    public static /* final */ int icon; // = 2130969255;
    @DexIgnore
    public static /* final */ int iconTint; // = 2130969261;
    @DexIgnore
    public static /* final */ int iconTintMode; // = 2130969262;
    @DexIgnore
    public static /* final */ int iconifiedByDefault; // = 2130969267;
    @DexIgnore
    public static /* final */ int imageButtonStyle; // = 2130969271;
    @DexIgnore
    public static /* final */ int indeterminateProgressStyle; // = 2130969273;
    @DexIgnore
    public static /* final */ int initialActivityCount; // = 2130969284;
    @DexIgnore
    public static /* final */ int isLightTheme; // = 2130969294;
    @DexIgnore
    public static /* final */ int itemPadding; // = 2130969305;
    @DexIgnore
    public static /* final */ int lastBaselineToBottomHeight; // = 2130969323;
    @DexIgnore
    public static /* final */ int layout; // = 2130969328;
    @DexIgnore
    public static /* final */ int lineHeight; // = 2130969405;
    @DexIgnore
    public static /* final */ int listChoiceBackgroundIndicator; // = 2130969408;
    @DexIgnore
    public static /* final */ int listChoiceIndicatorMultipleAnimated; // = 2130969409;
    @DexIgnore
    public static /* final */ int listChoiceIndicatorSingleAnimated; // = 2130969410;
    @DexIgnore
    public static /* final */ int listDividerAlertDialog; // = 2130969411;
    @DexIgnore
    public static /* final */ int listItemLayout; // = 2130969412;
    @DexIgnore
    public static /* final */ int listLayout; // = 2130969413;
    @DexIgnore
    public static /* final */ int listMenuViewStyle; // = 2130969414;
    @DexIgnore
    public static /* final */ int listPopupWindowStyle; // = 2130969415;
    @DexIgnore
    public static /* final */ int listPreferredItemHeight; // = 2130969416;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightLarge; // = 2130969417;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightSmall; // = 2130969418;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingEnd; // = 2130969419;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingLeft; // = 2130969420;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingRight; // = 2130969421;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingStart; // = 2130969422;
    @DexIgnore
    public static /* final */ int logo; // = 2130969427;
    @DexIgnore
    public static /* final */ int logoDescription; // = 2130969428;
    @DexIgnore
    public static /* final */ int maxButtonHeight; // = 2130969453;
    @DexIgnore
    public static /* final */ int measureWithLargestChild; // = 2130969458;
    @DexIgnore
    public static /* final */ int menu; // = 2130969459;
    @DexIgnore
    public static /* final */ int multiChoiceItemLayout; // = 2130969463;
    @DexIgnore
    public static /* final */ int navigationContentDescription; // = 2130969468;
    @DexIgnore
    public static /* final */ int navigationIcon; // = 2130969469;
    @DexIgnore
    public static /* final */ int navigationMode; // = 2130969470;
    @DexIgnore
    public static /* final */ int numericModifiers; // = 2130969490;
    @DexIgnore
    public static /* final */ int overlapAnchor; // = 2130969491;
    @DexIgnore
    public static /* final */ int paddingBottomNoButtons; // = 2130969493;
    @DexIgnore
    public static /* final */ int paddingEnd; // = 2130969494;
    @DexIgnore
    public static /* final */ int paddingStart; // = 2130969495;
    @DexIgnore
    public static /* final */ int paddingTopNoTitle; // = 2130969496;
    @DexIgnore
    public static /* final */ int panelBackground; // = 2130969498;
    @DexIgnore
    public static /* final */ int panelMenuListTheme; // = 2130969499;
    @DexIgnore
    public static /* final */ int panelMenuListWidth; // = 2130969500;
    @DexIgnore
    public static /* final */ int popupMenuStyle; // = 2130969509;
    @DexIgnore
    public static /* final */ int popupTheme; // = 2130969510;
    @DexIgnore
    public static /* final */ int popupWindowStyle; // = 2130969511;
    @DexIgnore
    public static /* final */ int preserveIconSpacing; // = 2130969512;
    @DexIgnore
    public static /* final */ int progressBarPadding; // = 2130969518;
    @DexIgnore
    public static /* final */ int progressBarStyle; // = 2130969519;
    @DexIgnore
    public static /* final */ int queryBackground; // = 2130969532;
    @DexIgnore
    public static /* final */ int queryHint; // = 2130969533;
    @DexIgnore
    public static /* final */ int radioButtonStyle; // = 2130969534;
    @DexIgnore
    public static /* final */ int ratingBarStyle; // = 2130969538;
    @DexIgnore
    public static /* final */ int ratingBarStyleIndicator; // = 2130969539;
    @DexIgnore
    public static /* final */ int ratingBarStyleSmall; // = 2130969540;
    @DexIgnore
    public static /* final */ int searchHintIcon; // = 2130969582;
    @DexIgnore
    public static /* final */ int searchIcon; // = 2130969583;
    @DexIgnore
    public static /* final */ int searchViewStyle; // = 2130969585;
    @DexIgnore
    public static /* final */ int seekBarStyle; // = 2130969586;
    @DexIgnore
    public static /* final */ int selectableItemBackground; // = 2130969587;
    @DexIgnore
    public static /* final */ int selectableItemBackgroundBorderless; // = 2130969588;
    @DexIgnore
    public static /* final */ int showAsAction; // = 2130969617;
    @DexIgnore
    public static /* final */ int showDividers; // = 2130969618;
    @DexIgnore
    public static /* final */ int showText; // = 2130969620;
    @DexIgnore
    public static /* final */ int showTitle; // = 2130969621;
    @DexIgnore
    public static /* final */ int singleChoiceItemLayout; // = 2130969623;
    @DexIgnore
    public static /* final */ int spinBars; // = 2130969681;
    @DexIgnore
    public static /* final */ int spinnerDropDownItemStyle; // = 2130969682;
    @DexIgnore
    public static /* final */ int spinnerStyle; // = 2130969683;
    @DexIgnore
    public static /* final */ int splitTrack; // = 2130969684;
    @DexIgnore
    public static /* final */ int srcCompat; // = 2130969685;
    @DexIgnore
    public static /* final */ int state_above_anchor; // = 2130969694;
    @DexIgnore
    public static /* final */ int subMenuArrow; // = 2130969708;
    @DexIgnore
    public static /* final */ int submitBackground; // = 2130969709;
    @DexIgnore
    public static /* final */ int subtitle; // = 2130969710;
    @DexIgnore
    public static /* final */ int subtitleTextAppearance; // = 2130969711;
    @DexIgnore
    public static /* final */ int subtitleTextColor; // = 2130969712;
    @DexIgnore
    public static /* final */ int subtitleTextStyle; // = 2130969713;
    @DexIgnore
    public static /* final */ int suggestionRowLayout; // = 2130969714;
    @DexIgnore
    public static /* final */ int switchMinWidth; // = 2130969718;
    @DexIgnore
    public static /* final */ int switchPadding; // = 2130969719;
    @DexIgnore
    public static /* final */ int switchStyle; // = 2130969720;
    @DexIgnore
    public static /* final */ int switchTextAppearance; // = 2130969721;
    @DexIgnore
    public static /* final */ int textAllCaps; // = 2130969758;
    @DexIgnore
    public static /* final */ int textAppearanceLargePopupMenu; // = 2130969769;
    @DexIgnore
    public static /* final */ int textAppearanceListItem; // = 2130969771;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSecondary; // = 2130969772;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSmall; // = 2130969773;
    @DexIgnore
    public static /* final */ int textAppearancePopupMenuHeader; // = 2130969775;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultSubtitle; // = 2130969776;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultTitle; // = 2130969777;
    @DexIgnore
    public static /* final */ int textAppearanceSmallPopupMenu; // = 2130969778;
    @DexIgnore
    public static /* final */ int textColorAlertDialogListItem; // = 2130969786;
    @DexIgnore
    public static /* final */ int textColorSearchUrl; // = 2130969787;
    @DexIgnore
    public static /* final */ int textLocale; // = 2130969796;
    @DexIgnore
    public static /* final */ int theme; // = 2130969807;
    @DexIgnore
    public static /* final */ int thickness; // = 2130969809;
    @DexIgnore
    public static /* final */ int thumbTextPadding; // = 2130969810;
    @DexIgnore
    public static /* final */ int thumbTint; // = 2130969811;
    @DexIgnore
    public static /* final */ int thumbTintMode; // = 2130969812;
    @DexIgnore
    public static /* final */ int tickMark; // = 2130969813;
    @DexIgnore
    public static /* final */ int tickMarkTint; // = 2130969814;
    @DexIgnore
    public static /* final */ int tickMarkTintMode; // = 2130969815;
    @DexIgnore
    public static /* final */ int tint; // = 2130969817;
    @DexIgnore
    public static /* final */ int tintMode; // = 2130969818;
    @DexIgnore
    public static /* final */ int title; // = 2130969821;
    @DexIgnore
    public static /* final */ int titleMargin; // = 2130969823;
    @DexIgnore
    public static /* final */ int titleMarginBottom; // = 2130969824;
    @DexIgnore
    public static /* final */ int titleMarginEnd; // = 2130969825;
    @DexIgnore
    public static /* final */ int titleMarginStart; // = 2130969826;
    @DexIgnore
    public static /* final */ int titleMarginTop; // = 2130969827;
    @DexIgnore
    public static /* final */ int titleMargins; // = 2130969828;
    @DexIgnore
    public static /* final */ int titleTextAppearance; // = 2130969829;
    @DexIgnore
    public static /* final */ int titleTextColor; // = 2130969830;
    @DexIgnore
    public static /* final */ int titleTextStyle; // = 2130969831;
    @DexIgnore
    public static /* final */ int toolbarNavigationButtonStyle; // = 2130969833;
    @DexIgnore
    public static /* final */ int toolbarStyle; // = 2130969834;
    @DexIgnore
    public static /* final */ int tooltipForegroundColor; // = 2130969835;
    @DexIgnore
    public static /* final */ int tooltipFrameBackground; // = 2130969836;
    @DexIgnore
    public static /* final */ int tooltipText; // = 2130969837;
    @DexIgnore
    public static /* final */ int track; // = 2130969839;
    @DexIgnore
    public static /* final */ int trackTint; // = 2130969840;
    @DexIgnore
    public static /* final */ int trackTintMode; // = 2130969841;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969843;
    @DexIgnore
    public static /* final */ int viewInflaterClass; // = 2130969863;
    @DexIgnore
    public static /* final */ int voiceIcon; // = 2130969879;
    @DexIgnore
    public static /* final */ int windowActionBar; // = 2130969946;
    @DexIgnore
    public static /* final */ int windowActionBarOverlay; // = 2130969947;
    @DexIgnore
    public static /* final */ int windowActionModeOverlay; // = 2130969948;
    @DexIgnore
    public static /* final */ int windowFixedHeightMajor; // = 2130969949;
    @DexIgnore
    public static /* final */ int windowFixedHeightMinor; // = 2130969950;
    @DexIgnore
    public static /* final */ int windowFixedWidthMajor; // = 2130969951;
    @DexIgnore
    public static /* final */ int windowFixedWidthMinor; // = 2130969952;
    @DexIgnore
    public static /* final */ int windowMinWidthMajor; // = 2130969953;
    @DexIgnore
    public static /* final */ int windowMinWidthMinor; // = 2130969954;
    @DexIgnore
    public static /* final */ int windowNoTitle; // = 2130969955;
}
