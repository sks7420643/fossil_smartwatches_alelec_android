package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C53 implements Z43 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.sampling.calculate_bundle_timestamp_before_sampling", true);

    @DexIgnore
    @Override // com.fossil.Z43
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
