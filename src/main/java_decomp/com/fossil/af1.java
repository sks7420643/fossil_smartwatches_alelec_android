package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Af1<Model, Data> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<Data> {
        @DexIgnore
        public /* final */ Mb1 a;
        @DexIgnore
        public /* final */ List<Mb1> b;
        @DexIgnore
        public /* final */ Wb1<Data> c;

        @DexIgnore
        public Ai(Mb1 mb1, Wb1<Data> wb1) {
            this(mb1, Collections.emptyList(), wb1);
        }

        @DexIgnore
        public Ai(Mb1 mb1, List<Mb1> list, Wb1<Data> wb1) {
            Ik1.d(mb1);
            this.a = mb1;
            Ik1.d(list);
            this.b = list;
            Ik1.d(wb1);
            this.c = wb1;
        }
    }

    @DexIgnore
    boolean a(Model model);

    @DexIgnore
    Ai<Data> b(Model model, int i, int i2, Ob1 ob1);
}
