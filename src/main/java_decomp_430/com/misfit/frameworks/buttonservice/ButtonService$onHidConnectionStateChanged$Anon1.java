package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonService$onHidConnectionStateChanged$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$0;

    @DexIgnore
    public ButtonService$onHidConnectionStateChanged$Anon1(ButtonService buttonService, String str) {
        this.this$0 = buttonService;
        this.$serialNumber = str;
    }

    @DexIgnore
    public final void run() {
        this.this$0.enqueue(this.$serialNumber);
    }
}
