package com.misfit.frameworks.buttonservice.db;

import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataLogService {
    @DexIgnore
    public static /* final */ String COLUMN_CONTENT; // = "content";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_LOG_STYLE; // = "logStyle";
    @DexIgnore
    public static /* final */ String COLUMN_STATUS; // = "status";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updateAt";
    @DexIgnore
    public static /* final */ int DEF_MF_LOG; // = 0;
    @DexIgnore
    public static /* final */ int DEF_MF_OTA_LOG; // = 3;
    @DexIgnore
    public static /* final */ int DEF_MF_SETUP_LOG; // = 2;
    @DexIgnore
    public static /* final */ int DEF_MF_SYNC_LOG; // = 1;
    @DexIgnore
    public static /* final */ int DEF_STATUS_MERGED; // = 2;
    @DexIgnore
    public static /* final */ int DEF_STATUS_NOT_SENT; // = 0;
    @DexIgnore
    public static /* final */ int DEF_STATUS_SENDING; // = 1;
    @DexIgnore
    @DatabaseField(columnName = "content")
    public String content;
    @DexIgnore
    @DatabaseField(columnName = "createAt")
    public long createAt;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public int id;
    @DexIgnore
    @DatabaseField(columnName = "logStyle")
    public int logStyle;
    @DexIgnore
    @DatabaseField(columnName = "status")
    public int status;
    @DexIgnore
    @DatabaseField(columnName = "updateAt")
    public long updateAt;

    @DexIgnore
    public DataLogService() {
    }

    @DexIgnore
    public String getContent() {
        return this.content;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public int getId() {
        return this.id;
    }

    @DexIgnore
    public int getLogStyle() {
        return this.logStyle;
    }

    @DexIgnore
    public int getStatus() {
        return this.status;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setContent(String str) {
        this.content = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public void setLogStyle(int i) {
        this.logStyle = i;
    }

    @DexIgnore
    public void setStatus(int i) {
        this.status = i;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public DataLogService(int i, int i2, String str, int i3, long j, long j2) {
        this.id = i;
        this.status = i2;
        this.content = str;
        this.logStyle = i3;
        this.createAt = j;
        this.updateAt = j2;
    }
}
