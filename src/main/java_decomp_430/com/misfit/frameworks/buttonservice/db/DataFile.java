package com.misfit.frameworks.buttonservice.db;

import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataFile implements Serializable {
    @DexIgnore
    public static /* final */ String COLUMN_DATA_FILE; // = "dataFile";
    @DexIgnore
    public static /* final */ String COLUMN_KEY; // = "key";
    @DexIgnore
    public static /* final */ String COLUMN_SERIAL; // = "serial";
    @DexIgnore
    public static /* final */ String COLUMN_SYNC_TIME; // = "syncTime";
    @DexIgnore
    @DatabaseField(columnName = "dataFile")
    public String dataFile;
    @DexIgnore
    @DatabaseField(columnName = "key", id = true)
    public String key;
    @DexIgnore
    @DatabaseField(columnName = "serial")
    public String serial;
    @DexIgnore
    @DatabaseField(columnName = "syncTime")
    public long syncTime;

    @DexIgnore
    public DataFile() {
    }

    @DexIgnore
    public String getDataFile() {
        return this.dataFile;
    }

    @DexIgnore
    public String getKey() {
        return this.key;
    }

    @DexIgnore
    public String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public String toString() {
        return new Gson().a(this);
    }

    @DexIgnore
    public DataFile(String str, String str2, String str3, long j) {
        this.key = str;
        this.dataFile = str2;
        this.serial = str3;
        this.syncTime = j;
    }
}
