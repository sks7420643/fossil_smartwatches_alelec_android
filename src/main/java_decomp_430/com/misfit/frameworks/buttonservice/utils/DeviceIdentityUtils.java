package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.fossil.bv6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DeviceIdentityUtils {
    @DexIgnore
    public static /* final */ String FAKE_SAM_SERIAL_NUMBER_PREFIX; // = "SAM-FSL";
    @DexIgnore
    public static /* final */ String FLASH_SERIAL_NUMBER_PREFIX; // = "F";
    @DexIgnore
    public static /* final */ String[] Q_MOTION_PREFIX; // = {"BF", "BM", "BK", "BS"};
    @DexIgnore
    public static /* final */ String RAY_SERIAL_NUMBER_PREFIX; // = "B0";
    @DexIgnore
    public static /* final */ String RMM_SERIAL_NUMBER_PREFIX; // = "C0";
    @DexIgnore
    public static /* final */ String SAM_DIANA_SERIAL_NUMBER_PREFIX; // = "D0";
    @DexIgnore
    public static /* final */ String SAM_MINI_SERIAL_NUMBER_PREFIX; // = "M0";
    @DexIgnore
    public static /* final */ String SAM_SE0_SERIAL_NUMBER_PREFIX; // = "Z0";
    @DexIgnore
    public static /* final */ String SAM_SERIAL_NUMBER_PREFIX; // = "W0";
    @DexIgnore
    public static /* final */ String SAM_SLIM_SERIAL_NUMBER_PREFIX; // = "L0";
    @DexIgnore
    public static /* final */ String SHINE2_SERIAL_NUMBER_PREFIX; // = "S2";
    @DexIgnore
    public static /* final */ String SHINE_SERIAL_NUMBER_PREFIX; // = "S";
    @DexIgnore
    public static /* final */ String SPEEDO_SERIAL_NUMBER_PREFIX; // = "SV0EZ";
    @DexIgnore
    public static /* final */ String SWAROVSKI_SERIAL_NUMBER_PREFIX; // = "SC";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE; // = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.RMM.ordinal()] = 1;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION.ordinal()] = 2;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.FAKE_SAM.ordinal()] = 3;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 4;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 5;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 6;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SE0.ordinal()] = 7;
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public enum QMotionType {
        BLACK("DZ"),
        STAINLESS_SILVER("SZ"),
        GOLD("GZ"),
        ROSE_GOLD("R1"),
        BLUE("BZ"),
        COPPER("PZ");
        
        @DexIgnore
        public String value;

        @DexIgnore
        public QMotionType(String str) {
            this.value = str;
        }

        @DexIgnore
        public static QMotionType fromColorCode(String str) {
            for (QMotionType qMotionType : values()) {
                if (qMotionType.getValue().equalsIgnoreCase(str)) {
                    return qMotionType;
                }
            }
            return ROSE_GOLD;
        }

        @DexIgnore
        public String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public static MFDeviceFamily getDeviceFamily(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return MFDeviceFamily.DEVICE_FAMILY_RMM;
            case 2:
                return MFDeviceFamily.DEVICE_FAMILY_Q_MOTION;
            case 3:
            case 4:
                return MFDeviceFamily.DEVICE_FAMILY_SAM;
            case 5:
                return MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM;
            case 6:
                return MFDeviceFamily.DEVICE_FAMILY_SAM_MINI;
            case 7:
                return MFDeviceFamily.DEVICE_FAMILY_SE0;
            case 8:
                return MFDeviceFamily.DEVICE_FAMILY_DIANA;
            default:
                return MFDeviceFamily.UNKNOWN;
        }
    }

    @DexIgnore
    public static String getNameBySerial(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return "RMM";
            case 2:
                return "Q Motion";
            case 3:
            case 4:
                return "Hybrid Smartwatch";
            case 5:
                return "SAM Slim";
            case 6:
                return "SAM Mini";
            default:
                return "UNKNOWN";
        }
    }

    @DexIgnore
    public static QMotionType getQMotionTypeBySerial(String str) {
        if (TextUtils.isEmpty(str) || str.length() < 4) {
            return QMotionType.ROSE_GOLD;
        }
        return QMotionType.fromColorCode(bv6.a(str, 3, 5));
    }

    @DexIgnore
    public static boolean isDianaDevice(String str) {
        return str != null && str.startsWith(SAM_DIANA_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isFlash(String str) {
        return str != null && str.startsWith(FLASH_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isFlashButton(String str) {
        return str != null && str.equals(Constants.BUTTON_MODEL);
    }

    @DexIgnore
    public static boolean isMisfitDevice(String str) {
        return FossilDeviceSerialPatternUtil.isRmmDevice(str) || isFlash(str) || isFlashButton(str) || isRay(str) || isShine(str) || isShine2(str) || isSpeedoShine(str) || isSwarovskiShine(str) || isQMotion(str) || FossilDeviceSerialPatternUtil.isHybridSmartWatchDevice(str);
    }

    @DexIgnore
    public static boolean isQMotion(String str) {
        if (!TextUtils.isEmpty(str) && str.length() > 2) {
            String substring = str.substring(0, 2);
            for (String equalsIgnoreCase : Q_MOTION_PREFIX) {
                if (substring.equalsIgnoreCase(equalsIgnoreCase)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean isRay(String str) {
        return str != null && str.startsWith(RAY_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isShine(String str) {
        return str != null && str.startsWith(SHINE_SERIAL_NUMBER_PREFIX) && !str.startsWith("S2");
    }

    @DexIgnore
    public static boolean isShine2(String str) {
        return str != null && str.startsWith("S2");
    }

    @DexIgnore
    public static boolean isSpeedoShine(String str) {
        return str != null && str.startsWith(SPEEDO_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isSwarovskiShine(String str) {
        return str != null && str.startsWith(SWAROVSKI_SERIAL_NUMBER_PREFIX);
    }

    @DexIgnore
    public static boolean isWearOSDevice(String str) {
        return str == null || str.isEmpty();
    }
}
