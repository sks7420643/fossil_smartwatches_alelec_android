package com.misfit.frameworks.buttonservice.utils;

import com.fossil.gg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationUtils$Companion$instance$Anon2 extends xg6 implements gg6<NotificationUtils> {
    @DexIgnore
    public static /* final */ NotificationUtils$Companion$instance$Anon2 INSTANCE; // = new NotificationUtils$Companion$instance$Anon2();

    @DexIgnore
    public NotificationUtils$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    public final NotificationUtils invoke() {
        return NotificationUtils.Singleton.INSTANCE.getINSTANCE();
    }
}
