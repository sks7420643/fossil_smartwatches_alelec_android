package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;
import com.fossil.i70;
import com.fossil.nh6;
import com.fossil.wg6;
import com.portfolio.platform.data.model.ServerSetting;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConversionUtils {
    @DexIgnore
    public static /* final */ ConversionUtils INSTANCE; // = new ConversionUtils();

    @DexIgnore
    public final String SHA1(String str) {
        wg6.b(str, ServerSetting.VALUE);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            Charset forName = Charset.forName("UTF-8");
            wg6.a((Object) forName, "Charset.forName(charsetName)");
            byte[] bytes = str.getBytes(forName);
            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            instance.update(bytes);
            byte[] digest = instance.digest();
            wg6.a((Object) digest, "messageDigest.digest()");
            return byteArrayToString(digest);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final String byteArrayToString(byte[] bArr) {
        wg6.b(bArr, "bytes");
        StringBuilder sb = new StringBuilder();
        for (byte valueOf : bArr) {
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            Object[] objArr = {Byte.valueOf(valueOf)};
            String format = String.format(locale, "%02x", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
        }
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final i70 getTimeFormat(Context context) {
        wg6.b(context, "context");
        if (DateFormat.is24HourFormat(context)) {
            return i70.TWENTY_FOUR;
        }
        return i70.TWELVE;
    }

    @DexIgnore
    public final int getTimezoneRawOffsetById(String str) {
        wg6.b(str, "timezoneId");
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        if (timeZone.inDaylightTime(instance.getTime())) {
            wg6.a((Object) timeZone, "timeZone");
            return (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 60000;
        }
        wg6.a((Object) timeZone, "timeZone");
        return timeZone.getRawOffset() / 60000;
    }
}
