package com.misfit.frameworks.buttonservice.enums;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MFDeviceFamily {
    UNKNOWN("Unknown", 0),
    INTEL("Intel", 1),
    DEVICE_FAMILY_Q_MOTION("Q_MOTION", 2),
    DEVICE_FAMILY_RMM("RMM", 3),
    DEVICE_FAMILY_SAM("SAM", 4),
    DEVICE_FAMILY_SAM_SLIM("SAM_SLIM", 5),
    DEVICE_FAMILY_SAM_MINI("MINI", 6),
    DEVICE_FAMILY_SE0("SE0", 7),
    DEVICE_FAMILY_DIANA("HYBRID HR", 8);
    
    @DexIgnore
    public String serverName;
    @DexIgnore
    public int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE; // = null;

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.RMM.ordinal()] = 1;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 2;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.FAKE_SAM.ordinal()] = 3;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SE0.ordinal()] = 4;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 5;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 6;
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION.ordinal()] = 7;
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public MFDeviceFamily(String str, int i) {
        this.value = i;
        this.serverName = str;
    }

    @DexIgnore
    public static MFDeviceFamily fromInt(int i) {
        for (MFDeviceFamily mFDeviceFamily : values()) {
            if (mFDeviceFamily.getValue() == i) {
                return mFDeviceFamily;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public static MFDeviceFamily fromServerName(String str) {
        for (MFDeviceFamily mFDeviceFamily : values()) {
            if (mFDeviceFamily.getServerName().equalsIgnoreCase(str)) {
                return mFDeviceFamily;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public static MFDeviceFamily getDeviceFamily(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return DEVICE_FAMILY_RMM;
            case 2:
            case 3:
                return DEVICE_FAMILY_SAM;
            case 4:
                return DEVICE_FAMILY_SE0;
            case 5:
                return DEVICE_FAMILY_SAM_SLIM;
            case 6:
                return DEVICE_FAMILY_SAM_MINI;
            case 7:
                return DEVICE_FAMILY_Q_MOTION;
            case 8:
                return DEVICE_FAMILY_DIANA;
            default:
                return UNKNOWN;
        }
    }

    @DexIgnore
    public static boolean isHybridSmartWatchFamily(MFDeviceFamily mFDeviceFamily) {
        return mFDeviceFamily == DEVICE_FAMILY_SAM || mFDeviceFamily == DEVICE_FAMILY_SAM_SLIM || mFDeviceFamily == DEVICE_FAMILY_SAM_MINI || mFDeviceFamily == DEVICE_FAMILY_SE0;
    }

    @DexIgnore
    public static boolean isSlimOrMiniFamily(MFDeviceFamily mFDeviceFamily) {
        return mFDeviceFamily == DEVICE_FAMILY_SAM_SLIM || mFDeviceFamily == DEVICE_FAMILY_SAM_MINI;
    }

    @DexIgnore
    public String getServerName() {
        return this.serverName;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
