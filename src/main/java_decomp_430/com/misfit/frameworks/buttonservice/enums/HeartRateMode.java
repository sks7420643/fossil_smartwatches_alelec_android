package com.misfit.frameworks.buttonservice.enums;

import com.fossil.kc6;
import com.fossil.qg6;
import com.fossil.v60;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum HeartRateMode {
    NONE(0),
    CONTINUOUS(1),
    LOW_POWER(2),
    DISABLE(3);
    
    @DexIgnore
    public static /* final */ Companion Companion; // = null;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[v60.a.values().length];
                $EnumSwitchMapping$0[v60.a.CONTINUOUS.ordinal()] = 1;
                $EnumSwitchMapping$0[v60.a.LOW_POWER.ordinal()] = 2;
                $EnumSwitchMapping$0[v60.a.DISABLE.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final HeartRateMode consume(v60.a aVar) {
            wg6.b(aVar, "sdkHeartRateMode");
            int i = WhenMappings.$EnumSwitchMapping$0[aVar.ordinal()];
            if (i == 1) {
                return HeartRateMode.CONTINUOUS;
            }
            if (i == 2) {
                return HeartRateMode.LOW_POWER;
            }
            if (i == 3) {
                return HeartRateMode.DISABLE;
            }
            throw new kc6();
        }

        @DexIgnore
        public final HeartRateMode fromValue(int i) {
            HeartRateMode heartRateMode;
            HeartRateMode[] values = HeartRateMode.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    heartRateMode = null;
                    break;
                }
                heartRateMode = values[i2];
                if (heartRateMode.getValue() == i) {
                    break;
                }
                i2++;
            }
            return heartRateMode != null ? heartRateMode : HeartRateMode.NONE;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

        /*
        static {
            $EnumSwitchMapping$0 = new int[HeartRateMode.values().length];
            $EnumSwitchMapping$0[HeartRateMode.CONTINUOUS.ordinal()] = 1;
            $EnumSwitchMapping$0[HeartRateMode.LOW_POWER.ordinal()] = 2;
            $EnumSwitchMapping$0[HeartRateMode.DISABLE.ordinal()] = 3;
            $EnumSwitchMapping$0[HeartRateMode.NONE.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        Companion = new Companion((qg6) null);
    }
    */

    @DexIgnore
    public HeartRateMode(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final v60.a toSDKHeartRateMode() {
        int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
        if (i == 1) {
            return v60.a.CONTINUOUS;
        }
        if (i == 2) {
            return v60.a.LOW_POWER;
        }
        if (i == 3) {
            return v60.a.DISABLE;
        }
        if (i == 4) {
            return v60.a.DISABLE;
        }
        throw new kc6();
    }
}
