package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.fossil.dc0;
import com.fossil.l50;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.common.log.MFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BackgroundImgData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public String imgData;
    @DexIgnore
    public String imgName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundImgData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public BackgroundImgData createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new BackgroundImgData(parcel, (qg6) null);
        }

        @DexIgnore
        public BackgroundImgData[] newArray(int i) {
            return new BackgroundImgData[i];
        }
    }

    /*
    static {
        String simpleName = BackgroundImgData.class.getSimpleName();
        wg6.a((Object) simpleName, "BackgroundImgData::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public /* synthetic */ BackgroundImgData(Parcel parcel, qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getHash() {
        return this.imgName + ':' + this.imgData;
    }

    @DexIgnore
    public final String getImgData() {
        return this.imgData;
    }

    @DexIgnore
    public final String getImgName() {
        return this.imgName;
    }

    @DexIgnore
    public final void setImgData(String str) {
        wg6.b(str, "<set-?>");
        this.imgData = str;
    }

    @DexIgnore
    public final void setImgName(String str) {
        wg6.b(str, "<set-?>");
        this.imgName = str;
    }

    @DexIgnore
    public final l50 toSDKBackgroundImage() {
        byte[] bArr;
        try {
            bArr = Base64.decode(this.imgData, 0);
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, ".toSDKBackgroundImage(), ex: " + e);
            bArr = new byte[0];
        }
        byte[] bArr2 = bArr;
        String str2 = this.imgName;
        wg6.a((Object) bArr2, "byteData");
        return new l50(str2, bArr2, (dc0) null, 4, (qg6) null);
    }

    @DexIgnore
    public String toString() {
        return "{imgName: " + this.imgName + ", imgData: BASE64_DATA_SIZE_" + this.imgData.length() + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.imgName);
        parcel.writeString(this.imgData);
    }

    @DexIgnore
    public BackgroundImgData(String str, String str2) {
        wg6.b(str, "imgName");
        wg6.b(str2, "imgData");
        this.imgName = str;
        this.imgData = str2;
    }

    @DexIgnore
    public BackgroundImgData(Parcel parcel) {
        String readString = parcel.readString();
        this.imgName = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.imgData = readString2 == null ? "" : readString2;
    }
}
