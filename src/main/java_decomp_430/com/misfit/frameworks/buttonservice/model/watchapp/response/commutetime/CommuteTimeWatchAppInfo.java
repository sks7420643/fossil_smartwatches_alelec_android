package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.fossil.e90;
import com.fossil.s50;
import com.fossil.sc0;
import com.fossil.tc0;
import com.fossil.w40;
import com.fossil.w90;
import com.fossil.wg6;
import com.fossil.x90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public int commuteTimeInMinute;
    @DexIgnore
    public String destination; // = "";
    @DexIgnore
    public String traffic; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(String str, int i, String str2) {
        super(e90.COMMUTE_TIME_WATCH_APP);
        wg6.b(str, "destination");
        wg6.b(str2, "traffic");
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return new sc0(new s50(this.destination, this.commuteTimeInMinute, this.traffic));
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (x90 instanceof w90) {
            return new sc0((w90) x90, new s50(this.destination, this.commuteTimeInMinute, this.traffic));
        }
        return null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.destination);
        parcel.writeInt(this.commuteTimeInMinute);
        parcel.writeString(this.traffic);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        this.destination = readString == null ? "" : readString;
        this.commuteTimeInMinute = parcel.readInt();
        String readString2 = parcel.readString();
        this.traffic = readString2 == null ? "" : readString2;
    }
}
