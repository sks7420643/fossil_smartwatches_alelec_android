package com.misfit.frameworks.buttonservice.model.notification;

import com.fossil.bh6;
import com.fossil.hi6;
import com.fossil.kh6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationBaseObj$toSilentNotification$Anon1 extends bh6 {
    @DexIgnore
    public NotificationBaseObj$toSilentNotification$Anon1(NotificationBaseObj notificationBaseObj) {
        super(notificationBaseObj);
    }

    @DexIgnore
    public Object get() {
        return ((NotificationBaseObj) this.receiver).getNotificationFlags();
    }

    @DexIgnore
    public String getName() {
        return "notificationFlags";
    }

    @DexIgnore
    public hi6 getOwner() {
        return kh6.a(NotificationBaseObj.class);
    }

    @DexIgnore
    public String getSignature() {
        return "getNotificationFlags()Ljava/util/List;";
    }
}
