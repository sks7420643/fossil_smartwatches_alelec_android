package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.w80;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CurrentWeatherInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public /* final */ int chanceOfRain;
    @DexIgnore
    public /* final */ float currentTemperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition currentWeatherCondition;
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CurrentWeatherInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public CurrentWeatherInfo createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new CurrentWeatherInfo(parcel);
        }

        @DexIgnore
        public CurrentWeatherInfo[] newArray(int i) {
            return new CurrentWeatherInfo[i];
        }
    }

    @DexIgnore
    public CurrentWeatherInfo() {
        this.chanceOfRain = 10;
        this.currentTemperature = 20.0f;
        this.currentWeatherCondition = WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY;
        this.highTemperature = 40.0f;
        this.lowTemperature = 15.0f;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final w80 toSDKCurrentWeatherInfo() {
        return new w80(this.currentTemperature, this.highTemperature, this.lowTemperature, this.chanceOfRain, this.currentWeatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeInt(this.chanceOfRain);
        parcel.writeFloat(this.currentTemperature);
        parcel.writeInt(this.currentWeatherCondition.ordinal());
        parcel.writeFloat(this.highTemperature);
        parcel.writeFloat(this.lowTemperature);
    }

    @DexIgnore
    public CurrentWeatherInfo(int i, float f, WeatherComplicationAppInfo.WeatherCondition weatherCondition, float f2, float f3) {
        wg6.b(weatherCondition, "currentWeatherCondition");
        this.chanceOfRain = i;
        this.currentTemperature = f;
        this.currentWeatherCondition = weatherCondition;
        this.highTemperature = f2;
        this.lowTemperature = f3;
    }

    @DexIgnore
    public CurrentWeatherInfo(Parcel parcel) {
        wg6.b(parcel, "parcel");
        this.chanceOfRain = parcel.readInt();
        this.currentTemperature = parcel.readFloat();
        this.currentWeatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
        this.highTemperature = parcel.readFloat();
        this.lowTemperature = parcel.readFloat();
    }
}
