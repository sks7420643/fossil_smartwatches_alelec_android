package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import com.fossil.AppNotification;
import com.fossil.NotificationFlag;
import com.fossil.NotificationType;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridNotificationObj extends NotificationBaseObj {
    @DexIgnore
    public FNotification fNotification;

    @DexIgnore
    public HybridNotificationObj(FNotification fNotification2) {
        wg6.b(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return "UID=" + getUid() + ", fNotification=" + this.fNotification + ", flag=" + getNotificationFlags();
    }

    @DexIgnore
    public AppNotification toSDKNotification() {
        NotificationType sDKNotificationType = getNotificationType().toSDKNotificationType();
        int uid = getUid();
        String packageName = this.fNotification.getPackageName();
        String title = getTitle();
        String sender = getSender();
        int senderId = getSenderId();
        String message = getMessage();
        Object[] array = toSDKNotificationFlags(getNotificationFlags()).toArray(new NotificationFlag[0]);
        if (array != null) {
            return new AppNotification(sDKNotificationType, uid, packageName, title, sender, senderId, message, (NotificationFlag[]) array, System.currentTimeMillis());
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(getUid());
        parcel.writeInt(getNotificationType().ordinal());
        parcel.writeParcelable(this.fNotification, 0);
        parcel.writeString(getTitle());
        parcel.writeString(getSender());
        parcel.writeString(getMessage());
        ArrayList arrayList = new ArrayList();
        for (NotificationBaseObj.ANotificationFlag ordinal : getNotificationFlags()) {
            arrayList.add(Integer.valueOf(ordinal.ordinal()));
        }
        parcel.writeList(arrayList);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public HybridNotificationObj(int i, NotificationBaseObj.ANotificationType aNotificationType, FNotification fNotification2, String str, String str2, int i2, String str3, List<NotificationBaseObj.ANotificationFlag> list) {
        this(fNotification2);
        wg6.b(aNotificationType, "notificationType");
        wg6.b(fNotification2, "fNotification");
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
        wg6.b(str3, "message");
        wg6.b(list, "notificationFlags");
        setUid(i);
        setNotificationType(aNotificationType);
        setTitle(str);
        setSender(str2);
        setMessage(str3);
        setSenderId(i2);
        setNotificationFlags(list);
    }

    @DexIgnore
    public HybridNotificationObj(Parcel parcel) {
        super(parcel);
        setUid(parcel.readInt());
        setNotificationType(NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification() : fNotification2;
        String readString = parcel.readString();
        setTitle(readString == null ? "" : readString);
        String readString2 = parcel.readString();
        setSender(readString2 == null ? "" : readString2);
        String readString3 = parcel.readString();
        setMessage(readString3 == null ? "" : readString3);
        setNotificationFlags(new ArrayList());
        ArrayList<Number> arrayList = new ArrayList<>();
        parcel.readList(arrayList, (ClassLoader) null);
        for (Number intValue : arrayList) {
            getNotificationFlags().add(NotificationBaseObj.ANotificationFlag.values()[intValue.intValue()]);
        }
    }
}
