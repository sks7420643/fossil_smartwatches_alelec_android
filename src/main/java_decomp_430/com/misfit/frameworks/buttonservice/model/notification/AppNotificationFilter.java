package com.misfit.frameworks.buttonservice.model.notification;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.HandMovingConfig;
import com.fossil.NotificationFilter;
import com.fossil.NotificationHandMovingConfig;
import com.fossil.NotificationIcon;
import com.fossil.NotificationIconConfig;
import com.fossil.NotificationType;
import com.fossil.NotificationVibePattern;
import com.fossil.cd6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xf6;
import com.fossil.yf6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotificationFilter implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public static /* final */ int IS_FIELD_EXIST; // = 1;
    @DexIgnore
    public static /* final */ int IS_FIELD_NOT_EXIST; // = 0;
    @DexIgnore
    public FNotification fNotification;
    @DexIgnore
    public NotificationHandMovingConfig handMovingConfig;
    @DexIgnore
    public Short priority;
    @DexIgnore
    public String sender;
    @DexIgnore
    public NotificationVibePattern vibePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilter> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public AppNotificationFilter createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new AppNotificationFilter(parcel, (qg6) null);
        }

        @DexIgnore
        public AppNotificationFilter[] newArray(int i) {
            return new AppNotificationFilter[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationFilter(Parcel parcel, qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AppNotificationFilter)) {
            return false;
        }
        AppNotificationFilter appNotificationFilter = (AppNotificationFilter) obj;
        if (!wg6.a((Object) this.fNotification, (Object) appNotificationFilter.fNotification) || !wg6.a((Object) this.sender, (Object) appNotificationFilter.sender) || !wg6.a((Object) this.priority, (Object) appNotificationFilter.priority)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.handMovingConfig;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.fNotification.getPackageName();
    }

    @DexIgnore
    public final Short getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.vibePattern;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.handMovingConfig = notificationHandMovingConfig;
    }

    @DexIgnore
    public final void setPriority(Short sh) {
        this.priority = sh;
    }

    @DexIgnore
    public final void setSender(String str) {
        this.sender = str;
    }

    @DexIgnore
    public final void setVibePattern(NotificationVibePattern notificationVibePattern) {
        this.vibePattern = notificationVibePattern;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        com.fossil.yf6.a(r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f9, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x017c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        com.fossil.yf6.a(r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0180, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0183, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        com.fossil.yf6.a(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0187, code lost:
        throw r1;
     */
    @DexIgnore
    public final NotificationFilter toSDKNotificationFilter(Context context, boolean z) {
        NotificationFilter notificationFilter;
        wg6.b(context, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("toSDKNotificationFilter", "toSDKNotificationFilter isSetIcon " + z);
        if (this.handMovingConfig == null || this.vibePattern == null) {
            notificationFilter = new NotificationFilter(this.fNotification.getPackageName());
        } else {
            String packageName = this.fNotification.getPackageName();
            NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
            if (notificationHandMovingConfig != null) {
                NotificationVibePattern notificationVibePattern = this.vibePattern;
                if (notificationVibePattern != null) {
                    notificationFilter = new NotificationFilter(packageName, notificationHandMovingConfig, notificationVibePattern);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        String str = this.sender;
        if (str != null) {
            notificationFilter.setSender(str);
        }
        Short sh = this.priority;
        if (sh != null) {
            notificationFilter.setPriority(sh.shortValue());
        }
        if (z) {
            if (wg6.a((Object) this.fNotification.getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || wg6.a((Object) this.fNotification.getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                try {
                    InputStream open = context.getAssets().open(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getIconFwPath());
                    String iconFwPath = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getIconFwPath();
                    wg6.a((Object) open, "it");
                    NotificationIcon notificationIcon = new NotificationIcon(iconFwPath, xf6.a(open));
                    yf6.a(open, (Throwable) null);
                    InputStream open2 = context.getAssets().open(DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getIconFwPath());
                    String iconFwPath2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getIconFwPath();
                    wg6.a((Object) open2, "it");
                    NotificationIcon notificationIcon2 = new NotificationIcon(iconFwPath2, xf6.a(open2));
                    yf6.a(open2, (Throwable) null);
                    notificationFilter.setIconConfig(new NotificationIconConfig(notificationIcon).setIconForType(NotificationType.INCOMING_CALL, notificationIcon).setIconForType(NotificationType.MISSED_CALL, notificationIcon2));
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    e.printStackTrace();
                    local2.e("toSDKNotificationFilter", String.valueOf(cd6.a));
                }
            } else {
                if (this.fNotification.getIconFwPath().length() > 0) {
                    try {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.e("toSDKNotificationFilter", "openIconPath " + this.fNotification.getIconFwPath());
                        InputStream open3 = context.getAssets().open(this.fNotification.getIconFwPath());
                        String iconFwPath3 = this.fNotification.getIconFwPath();
                        wg6.a((Object) open3, "it");
                        notificationFilter.setIconConfig(new NotificationIconConfig(new NotificationIcon(iconFwPath3, xf6.a(open3))));
                        yf6.a(open3, (Throwable) null);
                    } catch (Exception e2) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        e2.printStackTrace();
                        local4.e("toSDKNotificationFilter", String.valueOf(cd6.a));
                    }
                }
            }
        }
        return notificationFilter;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeParcelable(this.fNotification, 0);
        String str = this.sender;
        if (str != null) {
            parcel.writeInt(1);
            parcel.writeString(str);
        } else {
            parcel.writeInt(0);
        }
        Short sh = this.priority;
        if (sh != null) {
            short shortValue = sh.shortValue();
            parcel.writeInt(1);
            parcel.writeInt(shortValue);
        } else {
            parcel.writeInt(0);
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
        if (notificationHandMovingConfig != null) {
            parcel.writeInt(1);
            parcel.writeParcelable(notificationHandMovingConfig, 0);
        } else {
            parcel.writeInt(0);
        }
        NotificationVibePattern notificationVibePattern = this.vibePattern;
        if (notificationVibePattern != null) {
            parcel.writeInt(1);
            parcel.writeInt(notificationVibePattern.ordinal());
            return;
        }
        parcel.writeInt(0);
    }

    @DexIgnore
    public AppNotificationFilter(FNotification fNotification2) {
        wg6.b(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    public AppNotificationFilter(Parcel parcel) {
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification() : fNotification2;
        if (parcel.readInt() == 1) {
            this.sender = parcel.readString();
        }
        if (parcel.readInt() == 1) {
            this.priority = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readInt() == 1) {
            this.handMovingConfig = parcel.readParcelable(HandMovingConfig.class.getClassLoader());
        }
        if (parcel.readInt() == 1) {
            this.vibePattern = NotificationVibePattern.values()[parcel.readInt()];
        }
    }
}
