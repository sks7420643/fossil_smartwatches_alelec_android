package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.f60;
import com.fossil.wg6;
import com.fossil.x50;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public WeatherComplicationAppMapping() {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public x50 toSDKSetting() {
        return new f60();
    }

    @DexIgnore
    public WeatherComplicationAppMapping(Parcel parcel) {
        super(parcel);
    }
}
