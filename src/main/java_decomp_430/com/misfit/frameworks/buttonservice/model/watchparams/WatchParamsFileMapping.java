package com.misfit.frameworks.buttonservice.model.watchparams;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ej6;
import com.fossil.md0;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tv6;
import com.fossil.wg6;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParamsFileMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String dataContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WatchParamsFileMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public WatchParamsFileMapping createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    wg6.a((Object) cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Parcel.class});
                    wg6.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(new Object[]{parcel});
                    if (newInstance != null) {
                        return (WatchParamsFileMapping) newInstance;
                    }
                    throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public WatchParamsFileMapping[] newArray(int i) {
            return new WatchParamsFileMapping[i];
        }
    }

    @DexIgnore
    public WatchParamsFileMapping(String str) {
        wg6.b(str, "dataContent");
        this.dataContent = str;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void initialize() {
        String str = this.dataContent;
        Charset charset = ej6.a;
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            this.data = tv6.a(bytes);
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final md0 toSDKSetting() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new md0(bArr);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(WatchParamsFileMapping.class.getName());
        parcel.writeString(this.dataContent);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WatchParamsFileMapping(Parcel parcel) {
        this(r2 == null ? "" : r2);
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        initialize();
    }
}
