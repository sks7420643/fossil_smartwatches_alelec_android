package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ae0;
import com.fossil.be0;
import com.fossil.p80;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingSettings implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<WatchAppMappingSettings> CREATOR; // = new WatchAppMappingSettings$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public WatchAppMapping bottomAppMapping;
    @DexIgnore
    public WatchAppMapping middleAppMapping;
    @DexIgnore
    public /* final */ long timeStamp;
    @DexIgnore
    public WatchAppMapping topAppMapping;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final long compareTimeStamp(WatchAppMappingSettings watchAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings2) {
            long j = 0;
            long timeStamp = watchAppMappingSettings != null ? watchAppMappingSettings.getTimeStamp() : 0;
            if (watchAppMappingSettings2 != null) {
                j = watchAppMappingSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(WatchAppMappingSettings watchAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings2) {
            if ((watchAppMappingSettings != null || watchAppMappingSettings2 == null) && (watchAppMappingSettings == null || watchAppMappingSettings2 != null)) {
                return wg6.a((Object) watchAppMappingSettings, (Object) watchAppMappingSettings2);
            }
            return false;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ WatchAppMappingSettings(Parcel parcel, qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WatchAppMappingSettings)) {
            return false;
        }
        return wg6.a((Object) getHash(), (Object) ((WatchAppMappingSettings) obj).getHash());
    }

    @DexIgnore
    public final WatchAppMapping getBottomAppMapping() {
        return this.bottomAppMapping;
    }

    @DexIgnore
    public final String getHash() {
        String str = this.topAppMapping.getHash() + ":" + this.middleAppMapping.getHash() + ":" + this.bottomAppMapping.getHash() + ":";
        wg6.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final void setBottomAppMapping(WatchAppMapping watchAppMapping) {
        wg6.b(watchAppMapping, "<set-?>");
        this.bottomAppMapping = watchAppMapping;
    }

    @DexIgnore
    public final ae0 toSDKSetting() {
        return new ae0(new be0[]{new p80(this.topAppMapping.toSDKSetting(), this.middleAppMapping.toSDKSetting(), this.bottomAppMapping.toSDKSetting())});
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeParcelable(this.topAppMapping, i);
        parcel.writeParcelable(this.middleAppMapping, i);
        parcel.writeParcelable(this.bottomAppMapping, i);
    }

    @DexIgnore
    public WatchAppMappingSettings(WatchAppMapping watchAppMapping, WatchAppMapping watchAppMapping2, WatchAppMapping watchAppMapping3, long j) {
        wg6.b(watchAppMapping, "topAppMapping");
        wg6.b(watchAppMapping2, "middleAppMapping");
        wg6.b(watchAppMapping3, "bottomAppMapping");
        this.topAppMapping = watchAppMapping;
        this.middleAppMapping = watchAppMapping2;
        this.bottomAppMapping = watchAppMapping3;
        this.timeStamp = j;
    }

    @DexIgnore
    public WatchAppMappingSettings(Parcel parcel) {
        this.timeStamp = parcel.readLong();
        WatchAppMapping watchAppMapping = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.topAppMapping = watchAppMapping == null ? new MusicWatchAppMapping() : watchAppMapping;
        WatchAppMapping watchAppMapping2 = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.middleAppMapping = watchAppMapping2 == null ? new NoneWatchAppMapping() : watchAppMapping2;
        WatchAppMapping watchAppMapping3 = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.bottomAppMapping = watchAppMapping3 == null ? new DiagnosticsWatchAppMapping() : watchAppMapping3;
    }
}
