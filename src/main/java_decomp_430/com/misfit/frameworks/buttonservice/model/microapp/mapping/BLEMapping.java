package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.enums.Gesture;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BLEMapping implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<BLEMapping> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String FIELD_TYPE; // = "mType";
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public long mTimeStamp;
    @DexIgnore
    public int mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<BLEMapping> {
        @DexIgnore
        public BLEMapping createFromParcel(Parcel parcel) {
            try {
                Constructor<?> declaredConstructor = Class.forName(parcel.readString()).getDeclaredConstructor(new Class[]{Parcel.class});
                declaredConstructor.setAccessible(true);
                return (BLEMapping) declaredConstructor.newInstance(new Object[]{parcel});
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (InstantiationException e4) {
                e4.printStackTrace();
                return null;
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
                return null;
            }
        }

        @DexIgnore
        public BLEMapping[] newArray(int i) {
            return new BLEMapping[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class BLEMappingType {
        @DexIgnore
        public static /* final */ int LINK_TYPE; // = 1;
        @DexIgnore
        public static /* final */ int MICRO_APP_TYPE; // = 2;
    }

    @DexIgnore
    public BLEMapping() {
        this.className = getClass().getName();
        this.mTimeStamp = 0;
    }

    @DexIgnore
    public static <A extends BLEMapping, B extends BLEMapping> Long compareTimeStamp(List<A> list, List<B> list2) {
        Long l = 0L;
        Long l2 = l;
        for (A timeStamp : list) {
            l2 = Long.valueOf(Math.max(l2.longValue(), timeStamp.getTimeStamp()));
        }
        for (B timeStamp2 : list2) {
            l = Long.valueOf(Math.max(l.longValue(), timeStamp2.getTimeStamp()));
        }
        return Long.valueOf(l2.longValue() - l.longValue());
    }

    @DexIgnore
    public static <A extends BLEMapping, B extends BLEMapping> boolean isMappingTheSame(List<A> list, List<B> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        ArrayList arrayList = new ArrayList(list);
        ArrayList arrayList2 = new ArrayList(list2);
        int size = arrayList.size();
        arrayList.retainAll(arrayList2);
        if (arrayList.size() == size) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BLEMapping)) {
            return false;
        }
        return getHash().equalsIgnoreCase(((BLEMapping) obj).getHash());
    }

    @DexIgnore
    public abstract Gesture getGesture();

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public long getTimeStamp() {
        return this.mTimeStamp;
    }

    @DexIgnore
    public int getType() {
        return this.mType;
    }

    @DexIgnore
    public abstract boolean isNeedHID();

    @DexIgnore
    public abstract boolean isNeedStreaming();

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.className);
        parcel.writeInt(this.mType);
        parcel.writeLong(this.mTimeStamp);
    }

    @DexIgnore
    public BLEMapping(int i, long j) {
        this(i);
        this.mTimeStamp = j;
    }

    @DexIgnore
    public BLEMapping(int i) {
        this.className = getClass().getName();
        this.mTimeStamp = 0;
        this.mType = i;
    }

    @DexIgnore
    public BLEMapping(Parcel parcel) {
        this.className = getClass().getName();
        this.mTimeStamp = 0;
        this.mType = parcel.readInt();
        this.mTimeStamp = parcel.readLong();
    }
}
