package com.misfit.frameworks.buttonservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.communite.CommunicateManager;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonService$bluetoothReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$0;

    @DexIgnore
    public ButtonService$bluetoothReceiver$Anon1(ButtonService buttonService) {
        this.this$0 = buttonService;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        if (wg6.a((Object) "android.bluetooth.adapter.action.STATE_CHANGED", (Object) intent.getAction())) {
            this.this$0.state = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
            switch (this.this$0.state) {
                case 10:
                    FLogger.INSTANCE.getLocal().v(ButtonService.TAG, "Bluetooth off");
                    for (String next : DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0)) {
                        FLogger.INSTANCE.getLocal().d(ButtonService.TAG, "Bluetooth is off. Terminate all running sessions!!");
                        ButtonService buttonService = this.this$0;
                        wg6.a((Object) next, "key");
                        buttonService.addLogToActiveLog(next, "Bluetooth is off. Terminate all running sessions!!");
                        ((CommunicateManager) CommunicateManager.Companion.getInstance(context)).clearCommunicatorSessionQueue(next);
                    }
                    ButtonService.access$getConnectQueue$p(this.this$0).clear();
                    return;
                case 11:
                    FLogger.INSTANCE.getLocal().v(ButtonService.TAG, "Turning Bluetooth on...");
                    return;
                case 12:
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String access$getTAG$cp = ButtonService.TAG;
                    local.v(access$getTAG$cp, "Bluetooth on - Wait " + (ButtonService.WAITING_AFTER_BLUETOOTH_ON / 1000) + " seconds before connecting all devices");
                    this.this$0.lastBluetoothOn = System.currentTimeMillis();
                    new Handler(this.this$0.getMainLooper()).postDelayed(new ButtonService$bluetoothReceiver$Anon1$onReceive$Anon1_Level2(this), (long) ButtonService.WAITING_AFTER_BLUETOOTH_ON);
                    return;
                case 13:
                    FLogger.INSTANCE.getLocal().v(ButtonService.TAG, "Turning Bluetooth off...");
                    return;
                default:
                    return;
            }
        }
    }
}
