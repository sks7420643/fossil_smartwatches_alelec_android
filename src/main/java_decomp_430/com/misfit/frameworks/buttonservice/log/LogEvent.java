package com.misfit.frameworks.buttonservice.log;

import com.fossil.wg6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LogEvent {
    @DexIgnore
    public String appVersion;
    @DexIgnore
    public FLogger.Component component;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String fwVersion;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public FLogger.LogLevel logLevel;
    @DexIgnore
    public String logMessage;
    @DexIgnore
    public String phoneId;
    @DexIgnore
    public String phoneModel;
    @DexIgnore
    public String platform;
    @DexIgnore
    public String platformVersion;
    @DexIgnore
    public String sdkVersion;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public FLogger.Session session;
    @DexIgnore
    public transient Object tag;
    @DexIgnore
    public long timestamp;
    @DexIgnore
    public String userId;

    @DexIgnore
    public LogEvent(FLogger.LogLevel logLevel2, long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, FLogger.Component component2, FLogger.Session session2, String str10, String str11) {
        FLogger.LogLevel logLevel3 = logLevel2;
        String str12 = str;
        String str13 = str2;
        String str14 = str3;
        String str15 = str4;
        String str16 = str5;
        String str17 = str6;
        String str18 = str7;
        String str19 = str8;
        String str20 = str9;
        FLogger.Component component3 = component2;
        FLogger.Session session3 = session2;
        String str21 = str10;
        wg6.b(logLevel3, "logLevel");
        wg6.b(str12, ButtonService.USER_ID);
        wg6.b(str13, "phoneId");
        wg6.b(str14, "appVersion");
        wg6.b(str15, "platform");
        wg6.b(str16, "platformVersion");
        wg6.b(str17, "phoneModel");
        wg6.b(str18, "fwVersion");
        wg6.b(str19, "sdkVersion");
        wg6.b(str20, "deviceModel");
        wg6.b(component3, "component");
        wg6.b(session3, Constants.SESSION);
        wg6.b(str21, "serialNumber");
        wg6.b(str11, "logMessage");
        this.logLevel = logLevel3;
        this.timestamp = j;
        this.userId = str12;
        this.phoneId = str13;
        this.appVersion = str14;
        this.platform = str15;
        this.platformVersion = str16;
        this.phoneModel = str17;
        this.fwVersion = str18;
        this.sdkVersion = str19;
        this.deviceModel = str20;
        this.component = component3;
        this.session = session3;
        this.serialNumber = str21;
        this.logMessage = str11;
        String uuid = UUID.randomUUID().toString();
        wg6.a((Object) uuid, "UUID.randomUUID().toString()");
        this.id = uuid;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final FLogger.Component getComponent() {
        return this.component;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getFwVersion() {
        return this.fwVersion;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final FLogger.LogLevel getLogLevel() {
        return this.logLevel;
    }

    @DexIgnore
    public final String getLogMessage() {
        return this.logMessage;
    }

    @DexIgnore
    public final String getPhoneId() {
        return this.phoneId;
    }

    @DexIgnore
    public final String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getPlatformVersion() {
        return this.platformVersion;
    }

    @DexIgnore
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final FLogger.Session getSession() {
        return this.session;
    }

    @DexIgnore
    public final Object getTag() {
        return this.tag;
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public final void setAppVersion(String str) {
        wg6.b(str, "<set-?>");
        this.appVersion = str;
    }

    @DexIgnore
    public final void setComponent(FLogger.Component component2) {
        wg6.b(component2, "<set-?>");
        this.component = component2;
    }

    @DexIgnore
    public final void setDeviceModel(String str) {
        wg6.b(str, "<set-?>");
        this.deviceModel = str;
    }

    @DexIgnore
    public final void setFwVersion(String str) {
        wg6.b(str, "<set-?>");
        this.fwVersion = str;
    }

    @DexIgnore
    public final void setLogLevel(FLogger.LogLevel logLevel2) {
        wg6.b(logLevel2, "<set-?>");
        this.logLevel = logLevel2;
    }

    @DexIgnore
    public final void setLogMessage(String str) {
        wg6.b(str, ServerSetting.VALUE);
        String substring = str.substring(0, Math.min(str.length(), FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION));
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        this.logMessage = substring;
    }

    @DexIgnore
    public final void setPhoneId(String str) {
        wg6.b(str, "<set-?>");
        this.phoneId = str;
    }

    @DexIgnore
    public final void setPhoneModel(String str) {
        wg6.b(str, "<set-?>");
        this.phoneModel = str;
    }

    @DexIgnore
    public final void setPlatform(String str) {
        wg6.b(str, "<set-?>");
        this.platform = str;
    }

    @DexIgnore
    public final void setPlatformVersion(String str) {
        wg6.b(str, "<set-?>");
        this.platformVersion = str;
    }

    @DexIgnore
    public final void setSdkVersion(String str) {
        wg6.b(str, "<set-?>");
        this.sdkVersion = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setSession(FLogger.Session session2) {
        wg6.b(session2, "<set-?>");
        this.session = session2;
    }

    @DexIgnore
    public final void setTag(Object obj) {
        this.tag = obj;
    }

    @DexIgnore
    public final void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public final void setUserId(String str) {
        wg6.b(str, "<set-?>");
        this.userId = str;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }
}
