package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter$pollEventQueue$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2(xe6 xe6, BufferLogWriter$pollEventQueue$Anon1 bufferLogWriter$pollEventQueue$Anon1) {
        super(2, xe6);
        this.this$0 = bufferLogWriter$pollEventQueue$Anon1;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 = new BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2(xe6, this.this$0);
        bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2.p$ = (il6) obj;
        return bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        IFinishCallback callback;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            BufferDebugOption access$getDebugOption$p = this.this$0.this$0.debugOption;
            if (access$getDebugOption$p == null || (callback = access$getDebugOption$p.getCallback()) == null) {
                return null;
            }
            callback.onFinish();
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
