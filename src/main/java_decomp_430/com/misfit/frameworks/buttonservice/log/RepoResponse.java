package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.qg6;
import com.fossil.rx6;
import com.fossil.wg6;
import com.fossil.zq6;
import com.google.gson.Gson;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class RepoResponse<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final <T> Failure<T> create(Throwable th) {
            wg6.b(th, Constants.YO_ERROR_POST);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("create=");
            th.printStackTrace();
            sb.append(cd6.a);
            local.d("RepoResponse", sb.toString());
            if (th instanceof SocketTimeoutException) {
                return new Failure(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, th, (String) null, 8, (qg6) null);
            }
            return new Failure(601, (ServerError) null, th, (String) null, 8, (qg6) null);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final <T> RepoResponse<T> create(rx6<T> rx6) {
            String str;
            String str2;
            String str3;
            wg6.b(rx6, "response");
            if (rx6.d()) {
                return new Success(rx6.a());
            }
            int b = rx6.b();
            if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
                ServerError serverError = new ServerError();
                serverError.setCode(Integer.valueOf(b));
                zq6 c = rx6.c();
                if (c == null || (str = c.string()) == null) {
                    str = rx6.e();
                }
                serverError.setMessage(str);
                return new Failure(b, serverError, (Throwable) null, (String) null, 8, (qg6) null);
            }
            zq6 c2 = rx6.c();
            if (c2 == null || (str2 = c2.string()) == null) {
                str2 = rx6.e();
            }
            try {
                ServerError serverError2 = (ServerError) new Gson().a(str2, ServerError.class);
                if (serverError2 != null) {
                    Integer code = serverError2.getCode();
                    if (code != null) {
                        if (code.intValue() == 0) {
                        }
                    }
                    return new Failure(rx6.b(), serverError2, (Throwable) null, (String) null, 8, (qg6) null);
                }
                return new Failure(rx6.b(), (ServerError) null, (Throwable) null, str2);
            } catch (Exception unused) {
                zq6 c3 = rx6.c();
                if (c3 == null || (str3 = c3.string()) == null) {
                    str3 = rx6.e();
                }
                return new Failure(rx6.b(), new ServerError(b, str3), (Throwable) null, (String) null, 8, (qg6) null);
            }
        }
    }

    @DexIgnore
    public RepoResponse() {
    }

    @DexIgnore
    public /* synthetic */ RepoResponse(qg6 qg6) {
        this();
    }
}
