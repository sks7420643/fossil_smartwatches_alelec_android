package com.misfit.frameworks.buttonservice.log.db;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.ei;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogDao_Impl implements LogDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<Log> __insertionAdapterOfLog;
    @DexIgnore
    public /* final */ LogFlagConverter __logFlagConverter; // = new LogFlagConverter();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<Log> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `log` (`id`,`timeStamp`,`content`,`cloudFlag`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, Log log) {
            miVar.a(1, (long) log.getId());
            miVar.a(2, log.getTimeStamp());
            if (log.getContent() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, log.getContent());
            }
            String logFlagEnumToString = LogDao_Impl.this.__logFlagConverter.logFlagEnumToString(log.getCloudFlag());
            if (logFlagEnumToString == null) {
                miVar.a(4);
            } else {
                miVar.a(4, logFlagEnumToString);
            }
        }
    }

    @DexIgnore
    public LogDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfLog = new Anon1(ohVar);
    }

    @DexIgnore
    public int countExcept(Log.Flag flag) {
        rh b = rh.b("SELECT COUNT(id) FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.a(1);
        } else {
            b.a(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public int delete(List<Integer> list) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder a = ei.a();
        a.append("DELETE FROM log WHERE id IN (");
        ei.a(a, list.size());
        a.append(")");
        mi compileStatement = this.__db.compileStatement(a.toString());
        int i = 1;
        for (Integer next : list) {
            if (next == null) {
                compileStatement.a(i);
            } else {
                compileStatement.a(i, (long) next.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            int s = compileStatement.s();
            this.__db.setTransactionSuccessful();
            return s;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<Log> getAllLogEventsExcept(Log.Flag flag) {
        rh b = rh.b("SELECT * FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.a(1);
        } else {
            b.a(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "timeStamp");
            int b4 = ai.b(a, "content");
            int b5 = ai.b(a, "cloudFlag");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                Log log = new Log(a.getLong(b3), a.getString(b4), this.__logFlagConverter.stringToLogFlag(a.getString(b5)));
                log.setId(a.getInt(b2));
                arrayList.add(log);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertLogEvent(List<Log> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLog.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateCloudFlagByIds(List<Integer> list, Log.Flag flag) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder a = ei.a();
        a.append("UPDATE log SET cloudFlag = ");
        a.append("?");
        a.append(" WHERE id IN (");
        ei.a(a, list.size());
        a.append(")");
        mi compileStatement = this.__db.compileStatement(a.toString());
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            compileStatement.a(1);
        } else {
            compileStatement.a(1, logFlagEnumToString);
        }
        int i = 2;
        for (Integer next : list) {
            if (next == null) {
                compileStatement.a(i);
            } else {
                compileStatement.a(i, (long) next.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            compileStatement.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
