package com.misfit.frameworks.buttonservice.log;

import com.fossil.cf6;
import com.fossil.ef6;
import com.fossil.ff6;
import com.fossil.nf6;
import com.fossil.xe6;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt {
    @DexIgnore
    public static final <T> Object await(Call<T> call, xe6<? super RepoResponse<T>> xe6) {
        cf6 cf6 = new cf6(ef6.a(xe6));
        call.a(new LogEndPointKt$await$Anon2$Anon1_Level2(cf6));
        Object a = cf6.a();
        if (a == ff6.a()) {
            nf6.c(xe6);
        }
        return a;
    }
}
