package com.misfit.frameworks.buttonservice.log;

import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.mn6;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xp6;
import com.fossil.zl6;
import com.fossil.zp6;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ long FILE_LOG_SIZE_THRESHOLD; // = 3145728;
    @DexIgnore
    public static /* final */ String FILE_NAME_PATTERN; // = "app_log_%s.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public FileDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ SynchronizeQueue<String> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public int mCount;
    @DexIgnore
    public /* final */ xp6 mFileLogWriterMutex; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ il6 mFileLogWriterScope; // = jl6.a(zl6.b().plus(mn6.a((rm6) null, 1, (Object) null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String name = FileLogWriter.class.getName();
        wg6.a((Object) name, "FileLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getFilePath(String str) {
        return this.directoryPath + File.separatorChar + LOG_FOLDER + File.separatorChar + str;
    }

    @DexIgnore
    private final rm6 pollLogEvent() {
        return ik6.b(this.mFileLogWriterScope, (af6) null, (ll6) null, new FileLogWriter$pollLogEvent$Anon1(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    private final synchronized void rotateFiles() throws Exception {
        for (int i = 2; i >= 0; i--) {
            nh6 nh6 = nh6.a;
            Object[] objArr = {Integer.valueOf(i)};
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
                FileLock lock = channel.lock();
                if (i >= 2) {
                    file.delete();
                } else {
                    nh6 nh62 = nh6.a;
                    Object[] objArr2 = {Integer.valueOf(i + 1)};
                    String format2 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr2, objArr2.length));
                    wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                    file.renameTo(new File(getFilePath(format2)));
                }
                lock.release();
                channel.close();
            }
        }
        nh6 nh63 = nh6.a;
        Object[] objArr3 = {0};
        String format3 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr3, objArr3.length));
        wg6.a((Object) format3, "java.lang.String.format(format, *args)");
        new File(getFilePath(format3)).createNewFile();
    }

    @DexIgnore
    public final List<File> exportLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 2; i++) {
            nh6 nh6 = nh6.a;
            Object[] objArr = {Integer.valueOf(i)};
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void startWriter(String str) {
        wg6.b(str, "directoryPath");
        this.directoryPath = str;
        pollLogEvent();
    }

    @DexIgnore
    public final void writeLog(String str) {
        wg6.b(str, "logMessage");
        this.logEventQueue.add(str);
        pollLogEvent();
    }

    @DexIgnore
    public final void startWriter(String str, FileDebugOption fileDebugOption) {
        wg6.b(str, "directoryPath");
        this.debugOption = fileDebugOption;
        this.mCount = 0;
        startWriter(str);
    }
}
