package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OtaDetailLog {
    @DexIgnore
    @vu3("battery_levels")
    public int batteryLevel;

    @DexIgnore
    public OtaDetailLog() {
        this(0, 1, (qg6) null);
    }

    @DexIgnore
    public OtaDetailLog(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ OtaDetailLog(int i, int i2, qg6 qg6) {
        this((i2 & 1) != 0 ? 0 : i);
    }
}
