package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.log.model.OtaDetailLog;
import com.misfit.frameworks.buttonservice.log.model.RemoveDeviceLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.PinObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoteFLogger implements IRemoteFLogger, BufferLogWriter.IBufferLogCallback, DBLogWriter.IDBLogWriterCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String LOG_MESSAGE_INTENT_ACTION; // = "fossil.log.action.messages";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_END_SESSION; // = "action:end_session";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_ERROR_RECORDED; // = "action:error_recorded";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FLUSH; // = "action:flush_data";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FULL_BUFFER; // = "action:full_buffer";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_KEY; // = "action";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_START_SESSION; // = "action:start_session";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_ERROR_CODE; // = "param:error";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SERIAL; // = "param:serial";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SUMMARY_KEY; // = "param:summary_key";
    @DexIgnore
    public static /* final */ String MESSAGE_SENDER_KEY; // = "sender";
    @DexIgnore
    public static /* final */ String MESSAGE_TARGET_KEY; // = "target";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveDeviceInfo activeDeviceInfo; // = new ActiveDeviceInfo("", "", "");
    @DexIgnore
    public AppLogInfo appLogInfo; // = new AppLogInfo("", "", "", "", "", "", "");
    @DexIgnore
    public BufferLogWriter bufferLogWriter;
    @DexIgnore
    public Context context;
    @DexIgnore
    public DBLogWriter dbLogWriter;
    @DexIgnore
    public String floggerName; // = "";
    @DexIgnore
    public int flushLogTimeThreshold; // = LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public volatile boolean isFlushing;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public boolean isPrintToConsole; // = true;
    @DexIgnore
    public long lastSyncTime;
    @DexIgnore
    public /* final */ BroadcastReceiver mMessageBroadcastReceiver; // = new RemoteFLogger$mMessageBroadcastReceiver$Anon1(this);
    @DexIgnore
    public IRemoteLogWriter remoteLogWriter;
    @DexIgnore
    public SessionDetailInfo sessionDetailInfo; // = new SessionDetailInfo(-1, 0, 0);
    @DexIgnore
    public /* final */ HashMap<String, SessionSummary> summarySessionMap; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ErrorEvent {
        @DexIgnore
        public /* final */ FLogger.Component component;
        @DexIgnore
        public /* final */ String error;
        @DexIgnore
        public /* final */ String errorCode;
        @DexIgnore
        public /* final */ String step;

        @DexIgnore
        public ErrorEvent(String str, String str2, FLogger.Component component2, String str3) {
            wg6.b(str, "errorCode");
            wg6.b(str2, "step");
            wg6.b(component2, "component");
            wg6.b(str3, Constants.YO_ERROR_POST);
            this.errorCode = str;
            this.step = str2;
            this.component = component2;
            this.error = str3;
        }

        @DexIgnore
        public final FLogger.Component getComponent() {
            return this.component;
        }

        @DexIgnore
        public final String getError() {
            return this.error;
        }

        @DexIgnore
        public final String getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getStep() {
            return this.step;
        }

        @DexIgnore
        public final String toJsonString() {
            String a = new Gson().a(this);
            wg6.a((Object) a, "Gson().toJson(this)");
            return a;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }
    }

    @DexIgnore
    public enum MessageTarget {
        TO_ALL_FLOGGER(1),
        TO_MAIN_FLOGGER(2),
        TO_ALL_EXCEPT_MAIN_FLOGGER(3);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MessageTarget fromValue(int i) {
                MessageTarget messageTarget;
                MessageTarget[] values = MessageTarget.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        messageTarget = null;
                        break;
                    }
                    messageTarget = values[i2];
                    if (messageTarget.getValue() == i) {
                        break;
                    }
                    i2++;
                }
                return messageTarget != null ? messageTarget : MessageTarget.TO_ALL_FLOGGER;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public MessageTarget(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SessionSummary {
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion((qg6) null);
        @DexIgnore
        @vu3("details")
        public Object details;
        @DexIgnore
        @vu3("end_timestamp")
        public long endTimeStamp;
        @DexIgnore
        @vu3("errors")
        public /* final */ List<String> errors; // = new ArrayList();
        @DexIgnore
        @vu3("failure_reason")
        public String failureReason; // = "";
        @DexIgnore
        @vu3("is_success")
        public boolean isSuccess; // = true;
        @DexIgnore
        @vu3("start_timestamp")
        public long startTimeStamp;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final String getSummaryKey(String str, FLogger.Session session) {
                wg6.b(str, "serial");
                wg6.b(session, Constants.SESSION);
                return str + ':' + session;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        @DexIgnore
        public SessionSummary(long j, long j2) {
            this.startTimeStamp = j;
            this.endTimeStamp = j2;
        }

        @DexIgnore
        public final Object getDetails() {
            return this.details;
        }

        @DexIgnore
        public final long getEndTimeStamp() {
            return this.endTimeStamp;
        }

        @DexIgnore
        public final List<String> getErrors() {
            return this.errors;
        }

        @DexIgnore
        public final String getFailureReason() {
            return this.failureReason;
        }

        @DexIgnore
        public final long getStartTimeStamp() {
            return this.startTimeStamp;
        }

        @DexIgnore
        public final boolean isSuccess() {
            return this.isSuccess;
        }

        @DexIgnore
        public final void setDetails(Object obj) {
            this.details = obj;
        }

        @DexIgnore
        public final void setEndTimeStamp(long j) {
            this.endTimeStamp = j;
        }

        @DexIgnore
        public final void setStartTimeStamp(long j) {
            this.startTimeStamp = j;
        }

        @DexIgnore
        public final String toJsonString() {
            String a = FLogUtils.INSTANCE.getGsonForLogEvent().a(this);
            wg6.a((Object) a, "FLogUtils.getGsonForLogEvent().toJson(this)");
            return a;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }

        @DexIgnore
        public final void update(int i) {
            this.isSuccess = i == 0;
            this.failureReason = true ^ this.errors.isEmpty() ? (String) yd6.f(this.errors) : "";
            if (!this.isSuccess && this.errors.isEmpty()) {
                this.errors.add(String.valueOf(i));
                this.failureReason = (String) yd6.f(this.errors);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = RemoteFLogger.TAG;
            local.d(access$getTAG$cp, "failureReason " + this.failureReason + " errors " + this.errors + " finalCode " + i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[MessageTarget.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1; // = new int[FLogger.LogLevel.values().length];

        /*
        static {
            $EnumSwitchMapping$0[MessageTarget.TO_MAIN_FLOGGER.ordinal()] = 1;
            $EnumSwitchMapping$0[MessageTarget.TO_ALL_FLOGGER.ordinal()] = 2;
            $EnumSwitchMapping$0[MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER.ordinal()] = 3;
            $EnumSwitchMapping$1[FLogger.LogLevel.DEBUG.ordinal()] = 1;
            $EnumSwitchMapping$1[FLogger.LogLevel.INFO.ordinal()] = 2;
            $EnumSwitchMapping$1[FLogger.LogLevel.ERROR.ordinal()] = 3;
            $EnumSwitchMapping$1[FLogger.LogLevel.SUMMARY.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        String name = RemoteFLogger.class.getName();
        wg6.a((Object) name, "RemoteFLogger::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getInternalIntentAction() {
        Context context2 = this.context;
        return wg6.a(context2 != null ? context2.getPackageName() : null, (Object) LOG_MESSAGE_INTENT_ACTION);
    }

    @DexIgnore
    private final Object getSessionDetails(String str, String str2) {
        if (yj6.a((CharSequence) str, (CharSequence) FLogger.Session.SYNC.name(), false, 2, (Object) null)) {
            return this.sessionDetailInfo;
        }
        if (yj6.a((CharSequence) str, (CharSequence) FLogger.Session.OTA.name(), false, 2, (Object) null)) {
            return new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel());
        }
        if (yj6.a((CharSequence) str, (CharSequence) FLogger.Session.PAIR.name(), false, 2, (Object) null)) {
            return new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel());
        }
        return yj6.a((CharSequence) str, (CharSequence) FLogger.Session.REMOVE_DEVICE.name(), false, 2, (Object) null) ? new RemoveDeviceLog(str2) : "";
    }

    @DexIgnore
    private final void registerBroadcastMessages(Context context2) {
        context2.registerReceiver(this.mMessageBroadcastReceiver, new IntentFilter(getInternalIntentAction()));
    }

    @DexIgnore
    private final void sendInternalMessage(String str, String str2, MessageTarget messageTarget, Bundle bundle) {
        Intent intent = new Intent();
        intent.setAction(getInternalIntentAction());
        intent.putExtra(MESSAGE_SENDER_KEY, str2);
        intent.putExtra("action", str);
        intent.putExtra(MESSAGE_TARGET_KEY, messageTarget.getValue());
        intent.putExtras(bundle);
        Context context2 = this.context;
        if (context2 != null) {
            context2.sendBroadcast(intent);
        }
    }

    @DexIgnore
    public void d(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        wg6.b(component, "component");
        wg6.b(session, Constants.SESSION);
        wg6.b(str, "serial");
        wg6.b(str2, PinObject.COLUMN_CLASS_NAME);
        wg6.b(str3, "message");
        if (this.isInitialized && this.isDebuggable) {
            log(FLogger.LogLevel.DEBUG, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3, ErrorCodeBuilder.Step step, String str4) {
        List<String> errors;
        wg6.b(component, "component");
        wg6.b(session, Constants.SESSION);
        wg6.b(str, "serial");
        wg6.b(str2, PinObject.COLUMN_CLASS_NAME);
        wg6.b(str3, "errorCode");
        wg6.b(step, "step");
        wg6.b(str4, "errorMessage");
        if (this.isInitialized) {
            if (log(FLogger.LogLevel.ERROR, component, session, str, str2, new ErrorEvent(str3, step.getNameValue(), component, str4).toJsonString())) {
                String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
                SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
                if (!(sessionSummary == null || (errors = sessionSummary.getErrors()) == null)) {
                    errors.add(str3);
                }
                String str5 = this.floggerName;
                MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
                Bundle bundle = new Bundle();
                bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
                bundle.putString(MESSAGE_PARAM_ERROR_CODE, str3);
                sendInternalMessage(MESSAGE_ACTION_ERROR_RECORDED, str5, messageTarget, bundle);
            }
        }
    }

    @DexIgnore
    public List<File> exportAppLogs() {
        Collection collection;
        ArrayList arrayList = new ArrayList();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 == null || (collection = bufferLogWriter2.exportLogs()) == null) {
            collection = new ArrayList();
        }
        arrayList.addAll(collection);
        return arrayList;
    }

    @DexIgnore
    public void flush() {
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new RemoteFLogger$flush$Anon1(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ Object flushBuffer(xe6<? super cd6> xe6) {
        cd6 cd6;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        this.lastSyncTime = instance.getTimeInMillis();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.forceFlushBuffer();
            cd6 = cd6.a;
        } else {
            cd6 = null;
        }
        if (cd6 == ff6.a()) {
            return cd6;
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object flushDB(xe6<? super cd6> xe6) {
        RemoteFLogger$flushDB$Anon1 remoteFLogger$flushDB$Anon1;
        int i;
        IRemoteLogWriter iRemoteLogWriter;
        DBLogWriter dBLogWriter;
        if (xe6 instanceof RemoteFLogger$flushDB$Anon1) {
            remoteFLogger$flushDB$Anon1 = (RemoteFLogger$flushDB$Anon1) xe6;
            int i2 = remoteFLogger$flushDB$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                remoteFLogger$flushDB$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = remoteFLogger$flushDB$Anon1.result;
                Object a = ff6.a();
                i = remoteFLogger$flushDB$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    if (!(!this.isMainFLogger || (iRemoteLogWriter = this.remoteLogWriter) == null || (dBLogWriter = this.dbLogWriter) == null)) {
                        remoteFLogger$flushDB$Anon1.L$0 = this;
                        remoteFLogger$flushDB$Anon1.L$1 = iRemoteLogWriter;
                        remoteFLogger$flushDB$Anon1.label = 1;
                        if (dBLogWriter.flushTo(iRemoteLogWriter, remoteFLogger$flushDB$Anon1) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    IRemoteLogWriter iRemoteLogWriter2 = (IRemoteLogWriter) remoteFLogger$flushDB$Anon1.L$1;
                    RemoteFLogger remoteFLogger = (RemoteFLogger) remoteFLogger$flushDB$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }
        remoteFLogger$flushDB$Anon1 = new RemoteFLogger$flushDB$Anon1(this, xe6);
        Object obj2 = remoteFLogger$flushDB$Anon1.result;
        Object a2 = ff6.a();
        i = remoteFLogger$flushDB$Anon1.label;
        if (i != 0) {
        }
        return cd6.a;
    }

    @DexIgnore
    public void i(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        wg6.b(component, "component");
        wg6.b(session, Constants.SESSION);
        wg6.b(str, "serial");
        wg6.b(str2, PinObject.COLUMN_CLASS_NAME);
        wg6.b(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.INFO, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    public void init(String str, AppLogInfo appLogInfo2, ActiveDeviceInfo activeDeviceInfo2, CloudLogConfig cloudLogConfig, Context context2, boolean z, boolean z2) {
        wg6.b(str, "name");
        wg6.b(appLogInfo2, "appLogInfo");
        wg6.b(activeDeviceInfo2, "activeDeviceInfo");
        wg6.b(cloudLogConfig, "cloudLogConfig");
        wg6.b(context2, "context");
        int i = z2 ? 10 : 100;
        int i2 = z2 ? 50 : 500;
        this.flushLogTimeThreshold = z2 ? LogConfiguration.DEBUG_FLUSH_LOG_TIME_THRESHOLD : LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
        this.floggerName = str;
        this.appLogInfo = appLogInfo2;
        this.activeDeviceInfo = activeDeviceInfo2;
        this.context = context2;
        this.bufferLogWriter = new BufferLogWriter(str, i);
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.setCallback(this);
        }
        BufferLogWriter bufferLogWriter3 = this.bufferLogWriter;
        if (bufferLogWriter3 != null) {
            String file = context2.getFilesDir().toString();
            wg6.a((Object) file, "context.filesDir.toString()");
            bufferLogWriter3.startWriter(file, z, this);
        }
        this.dbLogWriter = new DBLogWriter(context2, i2, this);
        updateCloudLogConfig(cloudLogConfig);
        registerBroadcastMessages(context2);
        this.isMainFLogger = z;
        this.isDebuggable = z2;
        this.isInitialized = true;
    }

    @DexIgnore
    public final boolean log(FLogger.LogLevel logLevel, FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        String str4;
        String str5;
        String str6;
        String str7 = str;
        String str8 = str2;
        String str9 = str3;
        wg6.b(logLevel, "logLevel");
        wg6.b(component, "component");
        wg6.b(session, Constants.SESSION);
        wg6.b(str7, "serial");
        wg6.b(str8, PinObject.COLUMN_CLASS_NAME);
        wg6.b(str9, "message");
        boolean z = false;
        if (!xj6.a(this.appLogInfo.getUserId())) {
            if (this.isPrintToConsole) {
                int i = WhenMappings.$EnumSwitchMapping$1[logLevel.ordinal()];
                if (i == 1) {
                    Log.d("REMOTE - " + str8, str9);
                } else if (i == 2) {
                    Log.i("REMOTE - " + str8, str9);
                } else if (i == 3) {
                    Log.e("REMOTE - " + str8, str9);
                } else if (i == 4) {
                    Log.i("SUMMARY - " + str8, str9);
                }
            }
            if (xj6.b(this.activeDeviceInfo.getDeviceSerial(), str7, true)) {
                str6 = this.activeDeviceInfo.getFwVersion();
                str5 = this.activeDeviceInfo.getDeviceModel();
            } else {
                str6 = "";
                str5 = str6;
            }
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            str4 = "Calendar.getInstance()";
            LogEvent logEvent = r1;
            LogEvent logEvent2 = new LogEvent(logLevel, instance.getTimeInMillis(), this.appLogInfo.getUserId(), this.appLogInfo.getPhoneID(), this.appLogInfo.getAppVersion(), this.appLogInfo.getPlatform(), this.appLogInfo.getPlatformVersion(), this.appLogInfo.getPhoneModel(), str6, this.appLogInfo.getSdkVersion(), str5, component, session, str, str3);
            BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
            if (bufferLogWriter2 != null) {
                bufferLogWriter2.writeLog(logEvent);
                z = true;
            }
        } else {
            str4 = "Calendar.getInstance()";
        }
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, str4);
        if (instance2.getTimeInMillis() - this.lastSyncTime >= ((long) this.flushLogTimeThreshold)) {
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new RemoteFLogger$log$Anon1(this, (xe6) null), 3, (Object) null);
        }
        return z;
    }

    @DexIgnore
    public void onFullBuffer(List<LogEvent> list, boolean z) {
        wg6.b(list, "logLines");
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new RemoteFLogger$onFullBuffer$Anon1(this, list, z, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void onReachDBThreshold() {
        if (this.isMainFLogger) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new RemoteFLogger$onReachDBThreshold$Anon1(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void onWrittenSummaryLog(LogEvent logEvent) {
        wg6.b(logEvent, "logEvent");
        if (this.isDebuggable) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".onWrittenSummaryLog(), logEvent=" + logEvent);
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new RemoteFLogger$onWrittenSummaryLog$Anon1(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void setPrintToConsole(boolean z) {
        this.isPrintToConsole = z;
    }

    @DexIgnore
    public void startSession(FLogger.Session session, String str, String str2) {
        wg6.b(session, Constants.SESSION);
        wg6.b(str, "serial");
        wg6.b(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            startSession(summaryKey, str);
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            bundle.putString(MESSAGE_PARAM_SERIAL, str);
            sendInternalMessage(MESSAGE_ACTION_START_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    public void summary(int i, FLogger.Component component, FLogger.Session session, String str, String str2) {
        wg6.b(component, "component");
        wg6.b(session, Constants.SESSION);
        wg6.b(str, "serial");
        wg6.b(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            long currentTimeMillis = System.currentTimeMillis();
            SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
            if (sessionSummary != null) {
                sessionSummary.update(i);
                sessionSummary.setEndTimeStamp(currentTimeMillis);
                if (this.isInitialized) {
                    log(FLogger.LogLevel.SUMMARY, component, session, str, str2, sessionSummary.toJsonString());
                }
                SessionSummary remove = this.summarySessionMap.remove(summaryKey);
            }
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            sendInternalMessage(MESSAGE_ACTION_END_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    public final void unregisterBroadcastMessage(Context context2) {
        wg6.b(context2, "context");
        context2.unregisterReceiver(this.mMessageBroadcastReceiver);
    }

    @DexIgnore
    public void updateActiveDeviceInfo(ActiveDeviceInfo activeDeviceInfo2) {
        wg6.b(activeDeviceInfo2, "activeDeviceInfo");
        this.activeDeviceInfo = activeDeviceInfo2;
    }

    @DexIgnore
    public void updateAppLogInfo(AppLogInfo appLogInfo2) {
        wg6.b(appLogInfo2, "appLogInfo");
        this.appLogInfo = appLogInfo2;
    }

    @DexIgnore
    public void updateCloudLogConfig(CloudLogConfig cloudLogConfig) {
        wg6.b(cloudLogConfig, "cloudLogConfig");
        LogEndPoint.INSTANCE.init(cloudLogConfig.getLogBrandName(), cloudLogConfig.getEndPointBaseUrl(), cloudLogConfig.getAccessKey(), cloudLogConfig.getSecretKey());
        LogApiService logApiService = LogEndPoint.INSTANCE.getLogApiService();
        if (logApiService != null) {
            this.remoteLogWriter = new CloudLogWriter(logApiService);
        }
    }

    @DexIgnore
    public void updateSessionDetailInfo(SessionDetailInfo sessionDetailInfo2) {
        wg6.b(sessionDetailInfo2, "sessionDetailInfo");
        this.sessionDetailInfo = sessionDetailInfo2;
    }

    @DexIgnore
    private final void startSession(String str, String str2) {
        SessionSummary sessionSummary = this.summarySessionMap.get(str);
        if (sessionSummary != null) {
            sessionSummary.setStartTimeStamp(System.currentTimeMillis());
            sessionSummary.setEndTimeStamp(-1);
            sessionSummary.getErrors().clear();
            SessionSummary sessionSummary2 = this.summarySessionMap.get(str);
            if (sessionSummary2 != null) {
                sessionSummary2.setDetails(getSessionDetails(str, str2));
                return;
            }
            return;
        }
        this.summarySessionMap.put(str, new SessionSummary(System.currentTimeMillis(), -1));
        SessionSummary sessionSummary3 = this.summarySessionMap.get(str);
        if (sessionSummary3 != null) {
            sessionSummary3.setDetails(getSessionDetails(str, str2));
        }
    }

    @DexIgnore
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        wg6.b(component, "component");
        wg6.b(session, Constants.SESSION);
        wg6.b(str, "serial");
        wg6.b(str2, PinObject.COLUMN_CLASS_NAME);
        wg6.b(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.ERROR, component, session, str, str2, str3);
        }
    }
}
