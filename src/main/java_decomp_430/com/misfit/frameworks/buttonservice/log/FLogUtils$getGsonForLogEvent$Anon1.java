package com.misfit.frameworks.buttonservice.log;

import com.fossil.nu3;
import com.fossil.ou3;
import com.fossil.pu3;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FLogUtils$getGsonForLogEvent$Anon1<T> implements pu3<Long> {
    @DexIgnore
    public static /* final */ FLogUtils$getGsonForLogEvent$Anon1 INSTANCE; // = new FLogUtils$getGsonForLogEvent$Anon1();

    @DexIgnore
    public final nu3 serialize(Long l, Type type, ou3 ou3) {
        return new nu3(new BigDecimal(((double) l.longValue()) / ((double) 1000)).setScale(6, RoundingMode.HALF_UP));
    }
}
