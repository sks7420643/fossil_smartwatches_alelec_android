package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SessionDetailInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("battery_levels")
    public int batteryLevel;
    @DexIgnore
    @vu3("daily_steps")
    public int dailySteps;
    @DexIgnore
    @vu3("realtime_steps")
    public int realTimeSteps;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SessionDetailInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public SessionDetailInfo createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new SessionDetailInfo(parcel);
        }

        @DexIgnore
        public SessionDetailInfo[] newArray(int i) {
            return new SessionDetailInfo[i];
        }
    }

    @DexIgnore
    public SessionDetailInfo(int i, int i2, int i3) {
        this.batteryLevel = i;
        this.realTimeSteps = i2;
        this.dailySteps = i3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final int getDailySteps() {
        return this.dailySteps;
    }

    @DexIgnore
    public final int getRealTimeSteps() {
        return this.realTimeSteps;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final void setDailySteps(int i) {
        this.dailySteps = i;
    }

    @DexIgnore
    public final void setRealTimeSteps(int i) {
        this.realTimeSteps = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeInt(this.batteryLevel);
        parcel.writeInt(this.realTimeSteps);
        parcel.writeInt(this.dailySteps);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SessionDetailInfo(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
        wg6.b(parcel, "parcel");
    }
}
