package com.misfit.frameworks.buttonservice.log;

import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter$flushTo$Anon2$sentLogIds$Anon1_Level2 extends xg6 implements hg6<LogEvent, Integer> {
    @DexIgnore
    public static /* final */ DBLogWriter$flushTo$Anon2$sentLogIds$Anon1_Level2 INSTANCE; // = new DBLogWriter$flushTo$Anon2$sentLogIds$Anon1_Level2();

    @DexIgnore
    public DBLogWriter$flushTo$Anon2$sentLogIds$Anon1_Level2() {
        super(1);
    }

    @DexIgnore
    public final Integer invoke(LogEvent logEvent) {
        wg6.b(logEvent, "it");
        Object tag = logEvent.getTag();
        if (!(tag instanceof Integer)) {
            tag = null;
        }
        return (Integer) tag;
    }
}
