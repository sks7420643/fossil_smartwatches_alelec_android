package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.h60;
import com.fossil.pb0;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoBiometricDataSession extends SetAutoSettingsSession {
    @DexIgnore
    public UserBiometricData mNewBiometricData;
    @DexIgnore
    public UserBiometricData mOldBiometricData;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoBiometricDataSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBiometricDataState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetBiometricDataState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareConfigData() {
            b70 b70 = new b70();
            try {
                h60 sDKBiometricProfile = SetAutoBiometricDataSession.this.mNewBiometricData.toSDKBiometricProfile();
                b70.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
                setAutoBiometricDataSession.log("Set Biometric Data: exception=" + e.getMessage());
            }
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.log("Set Biometric Data, " + SetAutoBiometricDataSession.this.mNewBiometricData);
            this.task = SetAutoBiometricDataSession.this.getBleAdapter().setDeviceConfig(SetAutoBiometricDataSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetAutoBiometricDataSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, true);
            SetAutoBiometricDataSession.this.stop(FailureCode.FAILED_TO_SET_BIOMETRIC_DATA);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, false);
            SetAutoBiometricDataSession setAutoBiometricDataSession2 = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession2.enterStateAsync(setAutoBiometricDataSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoBiometricDataSession(UserBiometricData userBiometricData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_BIOMETRIC_DATA, bleAdapterImpl, bleSessionCallback);
        wg6.b(userBiometricData, "mNewBiometricData");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNewBiometricData = userBiometricData;
    }

    @DexIgnore
    private final void storeBiometricData(UserBiometricData userBiometricData, boolean z) {
        DevicePreferenceUtils.setAutoBiometricSettings(getBleAdapter().getContext(), new Gson().a(userBiometricData));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.BIOMETRIC);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoBiometricDataSession setAutoBiometricDataSession = new SetAutoBiometricDataSession(this.mNewBiometricData, getBleAdapter(), getBleSessionCallback());
        setAutoBiometricDataSession.setDevice(getDevice());
        return setAutoBiometricDataSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldBiometricData = DevicePreferenceUtils.getAutoBiometricSettings(getContext());
        if (getBleAdapter().isSupportedFeature(pb0.class) == null) {
            log("This device does not support set complication apps.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (UserBiometricData.CREATOR.isSame(this.mOldBiometricData, this.mNewBiometricData)) {
            log("New biometric data and old biometric data are the same. No need to set again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else {
            storeBiometricData(this.mNewBiometricData, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE;
        String name = SetBiometricDataState.class.getName();
        wg6.a((Object) name, "SetBiometricDataState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
