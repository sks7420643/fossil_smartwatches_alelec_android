package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.c70;
import com.fossil.cd6;
import com.fossil.e70;
import com.fossil.h60;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.x60;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetImplicitDeviceConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE);
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDeviceConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetDeviceConfigState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            b70 b70 = new b70();
            b70.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            b70.d(SetImplicitDeviceConfigSession.this.userProfile.getCurrentSteps());
            b70.e(SetImplicitDeviceConfigSession.this.userProfile.getGoalSteps());
            b70.b(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinute());
            b70.a(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinuteGoal());
            b70.a(SetImplicitDeviceConfigSession.this.userProfile.getCalories());
            b70.b(SetImplicitDeviceConfigSession.this.userProfile.getCaloriesGoal());
            b70.c(SetImplicitDeviceConfigSession.this.userProfile.getDistanceInCentimeter());
            b70.a(SetImplicitDeviceConfigSession.this.userProfile.getTotalSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getAwakeInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getLightSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getDeepSleepInMinute());
            UserDisplayUnit displayUnit = SetImplicitDeviceConfigSession.this.userProfile.getDisplayUnit();
            if (displayUnit != null) {
                b70.a(displayUnit.getTemperatureUnit().toSDKTemperatureUnit(), c70.KCAL, displayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(SetImplicitDeviceConfigSession.this.getBleAdapter().getContext()), e70.MONTH_DAY_YEAR);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No user display unit.");
                cd6 cd6 = cd6.a;
            }
            InactiveNudgeData inactiveNudgeData = SetImplicitDeviceConfigSession.this.userProfile.getInactiveNudgeData();
            if (inactiveNudgeData != null) {
                b70.a(inactiveNudgeData.getStartHour(), inactiveNudgeData.getStartMinute(), inactiveNudgeData.getStopHour(), inactiveNudgeData.getStopMinute(), inactiveNudgeData.getRepeatInterval(), inactiveNudgeData.isEnable() ? x60.a.ENABLE : x60.a.DISABLE);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No inactive nudge config.");
                cd6 cd62 = cd6.a;
            }
            try {
                h60 sDKBiometricProfile = SetImplicitDeviceConfigSession.this.userProfile.getUserBiometricData().toSDKBiometricProfile();
                b70.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWeightInKilogram(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
                setImplicitDeviceConfigSession.log("Set Device Config: exception=" + e.getMessage());
            }
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetImplicitDeviceConfigSession.this.getBleAdapter().setDeviceConfig(SetImplicitDeviceConfigSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetImplicitDeviceConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SetImplicitDeviceConfigSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
            setImplicitDeviceConfigSession.enterStateAsync(setImplicitDeviceConfigSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDeviceConfigSession.this.log("Set Device Config timeout. Cancel.");
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDeviceConfigSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, bleAdapterImpl, bleSessionCallback);
        wg6.b(userProfile2, "userProfile");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = new SetImplicitDeviceConfigSession(this.userProfile, getBleAdapter(), getBleSessionCallback());
        setImplicitDeviceConfigSession.setDevice(getDevice());
        return setImplicitDeviceConfigSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE;
        String name = SetDeviceConfigState.class.getName();
        wg6.a((Object) name, "SetDeviceConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
