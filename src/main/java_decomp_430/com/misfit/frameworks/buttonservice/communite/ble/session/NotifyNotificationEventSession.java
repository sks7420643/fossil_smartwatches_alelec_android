package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotifyNotificationEventSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ NotificationBaseObj mNotifyNotificationEvent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class NotifyNotificationEventState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public NotifyNotificationEventState() {
            super(NotifyNotificationEventSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = NotifyNotificationEventSession.this.getBleAdapter().notifyNotificationEvent(NotifyNotificationEventSession.this.getLogSession(), NotifyNotificationEventSession.this.mNotifyNotificationEvent, this);
            if (this.task == null) {
                NotifyNotificationEventSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onNotifyNotificationEventFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            NotifyNotificationEventSession.this.stop(FailureCode.FAILED_TO_NOTIFY_NOTIFICATION_EVENT);
        }

        @DexIgnore
        public void onNotifyNotificationEventSuccess() {
            stopTimeout();
            NotifyNotificationEventSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout cancel task");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
            NotifyNotificationEventSession.this.stop(FailureCode.FAILED_TO_NOTIFY_NOTIFICATION_EVENT);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyNotificationEventSession(NotificationBaseObj notificationBaseObj, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_COMPLICATION_APPS, bleAdapterImpl, bleSessionCallback);
        wg6.b(notificationBaseObj, "mNotifyNotificationEvent");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNotifyNotificationEvent = notificationBaseObj;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.NOTIFY_NOTIFICATION_EVENT) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        NotifyNotificationEventSession notifyNotificationEventSession = new NotifyNotificationEventSession(this.mNotifyNotificationEvent, getBleAdapter(), getBleSessionCallback());
        notifyNotificationEventSession.setDevice(getDevice());
        return notifyNotificationEventSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.NOTIFY_NOTIFICATION_EVENT_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.NOTIFY_NOTIFICATION_EVENT_STATE;
        String name = NotifyNotificationEventState.class.getName();
        wg6.a((Object) name, "NotifyNotificationEventState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
