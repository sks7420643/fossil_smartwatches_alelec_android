package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.d90;
import com.fossil.q40;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommunicatorAbs$mStateCallback$Anon1 implements q40.b {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$0;

    @DexIgnore
    public BleCommunicatorAbs$mStateCallback$Anon1(BleCommunicatorAbs bleCommunicatorAbs) {
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    public void onDeviceStateChanged(q40 q40, q40.c cVar, q40.c cVar2) {
        wg6.b(q40, "device");
        wg6.b(cVar, "previousState");
        wg6.b(cVar2, "newState");
        this.this$0.handleDeviceStateChanged(q40, cVar, cVar2);
    }

    @DexIgnore
    public void onEventReceived(q40 q40, d90 d90) {
        wg6.b(q40, "device");
        wg6.b(d90, Constants.EVENT);
        this.this$0.handleEventReceived(q40, d90);
    }
}
