package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.util.Base64;
import com.fossil.bc0;
import com.fossil.u40;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseVerifySecretKeySubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ CommunicateMode communicateMode;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapterV2;
    @DexIgnore
    public byte[] mSecretKey;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySubFlow extends BaseExchangeSecretKeySubFlow {
        @DexIgnore
        public ExchangeSecretKeySubFlow() {
            super(BaseVerifySecretKeySubFlow.this.getCommunicateMode(), BaseVerifySecretKeySubFlow.this.getTAG(), BaseVerifySecretKeySubFlow.this.getBleSession(), BaseVerifySecretKeySubFlow.this.getMfLog(), BaseVerifySecretKeySubFlow.this.getLogSession(), BaseVerifySecretKeySubFlow.this.getSerial(), BaseVerifySecretKeySubFlow.this.getBleAdapter(), BaseVerifySecretKeySubFlow.this.getBleSessionCallback());
        }

        @DexIgnore
        public void onStop(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "ExchangeSecretKeySubFlow done code " + i);
            if (i == 0) {
                BaseVerifySecretKeySubFlow.this.initStateMap();
                BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow = BaseVerifySecretKeySubFlow.this;
                baseVerifySecretKeySubFlow.enterSubStateAsync(baseVerifySecretKeySubFlow.createConcreteState(SubFlow.SessionState.PUSH_SECRET_KEY_TO_CLOUD));
                BaseVerifySecretKeySubFlow.this.mSecretKey = getMBleAdapterV2().getTSecretKey();
            } else if (i == u40.REQUEST_UNSUPPORTED.getCode() || i == u40.UNSUPPORTED_FORMAT.getCode()) {
                BaseVerifySecretKeySubFlow.this.stopSubFlow(0);
            } else {
                BaseVerifySecretKeySubFlow.this.stopSubFlow(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PushSecretKeyState extends BleStateAbs {
        @DexIgnore
        public PushSecretKeyState() {
            super(BaseVerifySecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            stopTimeout();
            startTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "enter PushSecretKeyState callback " + BaseVerifySecretKeySubFlow.this.getBleSessionCallback() + " secretKey " + BaseVerifySecretKeySubFlow.this.mSecretKey);
            if (BaseVerifySecretKeySubFlow.this.getBleSessionCallback() == null || BaseVerifySecretKeySubFlow.this.mSecretKey == null) {
                BaseVerifySecretKeySubFlow.this.stopSubFlow(FailureCode.UNKNOWN_ERROR);
                return true;
            }
            String encodeToString = Base64.encodeToString(BaseVerifySecretKeySubFlow.this.mSecretKey, 2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            local2.d(tag2, "enter PushSecretKeyState request to push " + encodeToString + " to server");
            BleSession.BleSessionCallback bleSessionCallback = BaseVerifySecretKeySubFlow.this.getBleSessionCallback();
            String serial = BaseVerifySecretKeySubFlow.this.getSerial();
            wg6.a((Object) encodeToString, "secretKeyString");
            bleSessionCallback.onRequestPushSecretKeyToServer(serial, encodeToString);
            return true;
        }

        @DexIgnore
        public final void onResponse(boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "PushSecretKeyState onResponse " + z);
            if (z) {
                BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow = BaseVerifySecretKeySubFlow.this;
                baseVerifySecretKeySubFlow.enterSubStateAsync(baseVerifySecretKeySubFlow.createConcreteState(SubFlow.SessionState.SET_SECRET_KEY_TO_DEVICE));
                return;
            }
            BaseVerifySecretKeySubFlow.this.stopSubFlow(FailureCode.FAILED_TO_PUSH_SECRET_KEY_TO_SERVER);
        }

        @DexIgnore
        public void onTimeout() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "PushSecretKeyState timeout");
            stopTimeout();
            BaseVerifySecretKeySubFlow.this.stopSubFlow(FailureCode.FAILED_TO_PUSH_SECRET_KEY_TO_SERVER);
            super.onTimeout();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetSecretKeyToDevice extends BleStateAbs {
        @DexIgnore
        public SetSecretKeyToDevice() {
            super(BaseVerifySecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "enter SetSecretKeyToDevice");
            byte[] tSecretKey = BaseVerifySecretKeySubFlow.this.getMBleAdapterV2().getTSecretKey();
            if (tSecretKey != null) {
                BaseVerifySecretKeySubFlow.this.getMBleAdapterV2().setSecretKey(BaseVerifySecretKeySubFlow.this.getLogSession(), tSecretKey);
                BaseVerifySecretKeySubFlow.this.stopSubFlow(0);
                return true;
            }
            BaseVerifySecretKeySubFlow.this.stopSubFlow(FailureCode.UNKNOWN_ERROR);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class VerifySecretKeySessionState extends BleStateAbs {
        @DexIgnore
        public zb0<Boolean> task;

        @DexIgnore
        public VerifySecretKeySessionState() {
            super(BaseVerifySecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "enter VerifySecretKeySessionState currentSecretKey " + BaseVerifySecretKeySubFlow.this.mSecretKey);
            if (BaseVerifySecretKeySubFlow.this.mSecretKey == null) {
                BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow = BaseVerifySecretKeySubFlow.this;
                baseVerifySecretKeySubFlow.enterSubStateAsync(baseVerifySecretKeySubFlow.createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
                return true;
            }
            BleAdapterImpl bleAdapter = BaseVerifySecretKeySubFlow.this.getBleAdapter();
            FLogger.Session logSession = BaseVerifySecretKeySubFlow.this.getLogSession();
            byte[] access$getMSecretKey$p = BaseVerifySecretKeySubFlow.this.mSecretKey;
            if (access$getMSecretKey$p != null) {
                this.task = bleAdapter.verifySecretKey(logSession, access$getMSecretKey$p, this);
                if (this.task == null) {
                    BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow2 = BaseVerifySecretKeySubFlow.this;
                    baseVerifySecretKeySubFlow2.enterSubStateAsync(baseVerifySecretKeySubFlow2.createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
                    return true;
                }
                startTimeout();
                return true;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public void onTimeout() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "VerifySecretKeySessionState timeout");
            super.onTimeout();
            zb0<Boolean> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }

        @DexIgnore
        public void onVerifySecretKeyFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onVerifySecretKeyFail code " + bc0.getCode());
            int code = bc0.getCode();
            if (code == u40.REQUEST_UNSUPPORTED.getCode()) {
                BaseVerifySecretKeySubFlow.this.stopSubFlow(u40.REQUEST_UNSUPPORTED.getCode());
            } else if (code == u40.UNSUPPORTED_FORMAT.getCode()) {
                BaseVerifySecretKeySubFlow.this.stopSubFlow(u40.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow = BaseVerifySecretKeySubFlow.this;
                baseVerifySecretKeySubFlow.enterSubStateAsync(baseVerifySecretKeySubFlow.createConcreteState(SubFlow.SessionState.SET_SECRET_KEY_TO_DEVICE));
            }
        }

        @DexIgnore
        public void onVerifySecretKeySuccess(boolean z) {
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onVerifySecretKeySuccess isValid " + z);
            if (z) {
                BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow = BaseVerifySecretKeySubFlow.this;
                baseVerifySecretKeySubFlow.enterSubStateAsync(baseVerifySecretKeySubFlow.createConcreteState(SubFlow.SessionState.SET_SECRET_KEY_TO_DEVICE));
                return;
            }
            BaseVerifySecretKeySubFlow baseVerifySecretKeySubFlow2 = BaseVerifySecretKeySubFlow.this;
            baseVerifySecretKeySubFlow2.enterSubStateAsync(baseVerifySecretKeySubFlow2.createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseVerifySecretKeySubFlow(CommunicateMode communicateMode2, String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        wg6.b(communicateMode2, "communicateMode");
        wg6.b(str, "tagName");
        wg6.b(bleSession, "bleSession");
        wg6.b(session, "logSession");
        wg6.b(str2, "serial");
        wg6.b(bleAdapterImpl, "mBleAdapterV2");
        this.communicateMode = communicateMode2;
        this.mflog = mFLog;
        this.mBleAdapterV2 = bleAdapterImpl;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        return this.communicateMode;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapterV2() {
        return this.mBleAdapterV2;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.VERIFY_SECRET_KEY;
        String name = VerifySecretKeySessionState.class.getName();
        wg6.a((Object) name, "VerifySecretKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.EXCHANGE_SECRET_KEY;
        String name2 = ExchangeSecretKeySubFlow.class.getName();
        wg6.a((Object) name2, "ExchangeSecretKeySubFlow::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.PUSH_SECRET_KEY_TO_CLOUD;
        String name3 = PushSecretKeyState.class.getName();
        wg6.a((Object) name3, "PushSecretKeyState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.SET_SECRET_KEY_TO_DEVICE;
        String name4 = SetSecretKeyToDevice.class.getName();
        wg6.a((Object) name4, "SetSecretKeyToDevice::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        this.mSecretKey = this.mBleAdapterV2.getTSecretKey();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "currentSecretKey " + this.mSecretKey);
        if (this.mSecretKey != null) {
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.VERIFY_SECRET_KEY));
            return true;
        }
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
        return true;
    }

    @DexIgnore
    public final void onReceivePushSecretKeyResponse(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceivePushSecretKeyResponse isSuccess " + z + " state " + getMCurrentState());
        BleStateAbs mCurrentState = getMCurrentState();
        if (mCurrentState instanceof PushSecretKeyState) {
            ((PushSecretKeyState) mCurrentState).onResponse(z);
        }
    }

    @DexIgnore
    public final void onReceiveRandomKey(byte[] bArr, int i) {
        BleStateAbs mCurrentState = getMCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + mCurrentState);
        if (mCurrentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) mCurrentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    public final void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleStateAbs mCurrentState = getMCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + mCurrentState);
        if (mCurrentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) mCurrentState).onReceiveServerSecretKey(bArr, i);
        }
    }
}
