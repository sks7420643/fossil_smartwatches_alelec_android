package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.a70;
import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.c70;
import com.fossil.cd6;
import com.fossil.e70;
import com.fossil.h60;
import com.fossil.nd0;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.x60;
import com.fossil.yb0;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.SettingsUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferSettingsSubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BackgroundConfig backgroundConfig;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isFullSync;
    @DexIgnore
    public /* final */ LocalizationData localizationData;
    @DexIgnore
    public /* final */ List<MicroAppMapping> microAppMappings;
    @DexIgnore
    public /* final */ List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public /* final */ AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public /* final */ int secondTimezoneOffset;
    @DexIgnore
    public /* final */ UserProfile userProfile;
    @DexIgnore
    public /* final */ WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfig extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetBackgroundImageConfig() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                BackgroundConfig backgroundConfig = BaseTransferSettingsSubFlow.this.getBackgroundConfig();
                if (backgroundConfig != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setBackgroundImage(BaseTransferSettingsSubFlow.this.getLogSession(), backgroundConfig, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                    } else {
                        startTimeout();
                        obj = cd6.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No background image config");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Background Image Config step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetBackgroundConfig=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            return true;
        }

        @DexIgnore
        public void onSetBackgroundImageFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Background Image timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplications extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetComplications() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                ComplicationAppMappingSettings complicationAppMappingSettings = BaseTransferSettingsSubFlow.this.getComplicationAppMappingSettings();
                if (complicationAppMappingSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setComplications(BaseTransferSettingsSubFlow.this.getLogSession(), complicationAppMappingSettings, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                    } else {
                        startTimeout();
                        obj = cd6.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Complication step. No complication settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Complication step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(1920);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Complication timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetDeviceConfigState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:40:0x01fe, code lost:
            if (r0 == null) goto L_0x0201;
         */
        @DexIgnore
        private final r60[] prepareConfigData() {
            UserBiometricData userBiometricData;
            b70 b70;
            InactiveNudgeData inactiveNudgeData;
            UserDisplayUnit displayUnit;
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            b70 b702 = new b70();
            b702.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            if (BaseTransferSettingsSubFlow.this.getBleSession() instanceof PairingNewDeviceSession) {
                FLogger.INSTANCE.getLocal().d(getTAG(), "Pair new device, set vibe strength medium as default");
                b702.a(a70.a.MEDIUM);
            } else if (!BaseTransferSettingsSubFlow.this.getBleAdapter().getVibrationStrength().isDefaultValue()) {
                b702.a(BaseTransferSettingsSubFlow.this.getBleAdapter().getVibrationStrength().toSDKVibrationStrengthLevel());
            } else {
                BaseTransferSettingsSubFlow.this.log("Set Device Config: VibeStrength is default value, do not set it to device.");
            }
            if (SettingsUtils.INSTANCE.isSecondTimezoneInRange((short) BaseTransferSettingsSubFlow.this.getSecondTimezoneOffset())) {
                b702.a((short) BaseTransferSettingsSubFlow.this.getSecondTimezoneOffset());
            } else {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.log("Set Device Config: Timezone is out of range: " + BaseTransferSettingsSubFlow.this.getSecondTimezoneOffset());
            }
            if (BaseTransferSettingsSubFlow.this.getUserProfile() != null) {
                b702.d(BaseTransferSettingsSubFlow.this.getUserProfile().getCurrentSteps());
                b702.e(BaseTransferSettingsSubFlow.this.getUserProfile().getGoalSteps());
                b702.b(BaseTransferSettingsSubFlow.this.getUserProfile().getActiveMinute());
                b702.a(BaseTransferSettingsSubFlow.this.getUserProfile().getActiveMinuteGoal());
                b702.a(BaseTransferSettingsSubFlow.this.getUserProfile().getCalories());
                b702.b(BaseTransferSettingsSubFlow.this.getUserProfile().getCaloriesGoal());
                b702.c(BaseTransferSettingsSubFlow.this.getUserProfile().getDistanceInCentimeter());
                b702.a(BaseTransferSettingsSubFlow.this.getUserProfile().getTotalSleepInMinute(), BaseTransferSettingsSubFlow.this.getUserProfile().getAwakeInMinute(), BaseTransferSettingsSubFlow.this.getUserProfile().getLightSleepInMinute(), BaseTransferSettingsSubFlow.this.getUserProfile().getDeepSleepInMinute());
            } else {
                BaseTransferSettingsSubFlow.this.log("Set Device Config: No user profile data.");
                cd6 cd6 = cd6.a;
            }
            UserProfile userProfile = BaseTransferSettingsSubFlow.this.getUserProfile();
            if (userProfile == null || (displayUnit = userProfile.getDisplayUnit()) == null) {
                BaseTransferSettingsSubFlow.this.log("Set Device Config: No user display unit.");
                cd6 cd62 = cd6.a;
            } else {
                b702.a(displayUnit.getTemperatureUnit().toSDKTemperatureUnit(), c70.KCAL, displayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext()), e70.MONTH_DAY_YEAR);
            }
            UserProfile userProfile2 = BaseTransferSettingsSubFlow.this.getUserProfile();
            if (userProfile2 == null || (inactiveNudgeData = userProfile2.getInactiveNudgeData()) == null) {
                BaseTransferSettingsSubFlow.this.log("Set Device Config: No inactive nudge config.");
                cd6 cd63 = cd6.a;
            } else {
                b702.a(inactiveNudgeData.getStartHour(), inactiveNudgeData.getStartMinute(), inactiveNudgeData.getStopHour(), inactiveNudgeData.getStopMinute(), inactiveNudgeData.getRepeatInterval(), inactiveNudgeData.isEnable() ? x60.a.ENABLE : x60.a.DISABLE);
            }
            UserProfile userProfile3 = BaseTransferSettingsSubFlow.this.getUserProfile();
            if (!(userProfile3 == null || (userBiometricData = userProfile3.getUserBiometricData()) == null)) {
                try {
                    h60 sDKBiometricProfile = userBiometricData.toSDKBiometricProfile();
                    b702.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWeightInKilogram(), sDKBiometricProfile.getWearingPosition());
                    b70 = b702;
                } catch (Exception e) {
                    BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                    baseTransferSettingsSubFlow2.log("Set Device Config: exception=" + e.getMessage());
                    b70 = cd6.a;
                }
            }
            BaseTransferSettingsSubFlow.this.log("Set Device Config: No biometric data.");
            cd6 cd64 = cd6.a;
            return b702.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setDeviceConfig(BaseTransferSettingsSubFlow.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Device Config timeout. Cancel.");
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetListAlarmsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                List<AlarmSetting> multiAlarmSettings = BaseTransferSettingsSubFlow.this.getMultiAlarmSettings();
                if (multiAlarmSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setAlarms(BaseTransferSettingsSubFlow.this.getLogSession(), multiAlarmSettings, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
                    } else {
                        startTimeout();
                        obj = cd6.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Alarm step. Alarm settings null");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Alarm step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetMultiAlarmSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
            return true;
        }

        @DexIgnore
        public void onSetAlarmFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
        }

        @DexIgnore
        public void onSetAlarmSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set List Alarms timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLocalization extends BleStateAbs {
        @DexIgnore
        public yb0<String> task;

        @DexIgnore
        public SetLocalization() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                LocalizationData localizationData = BaseTransferSettingsSubFlow.this.getLocalizationData();
                if (localizationData != null) {
                    if (localizationData.isDataValid()) {
                        this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setLocalizationData(localizationData, BaseTransferSettingsSubFlow.this.getLogSession(), this);
                        if (this.task == null) {
                            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                            obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                        } else {
                            startTimeout();
                            obj = cd6.a;
                        }
                    } else {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                        baseTransferSettingsSubFlow2.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow3.enterSubStateAsync(baseTransferSettingsSubFlow3.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Localization step. No localization settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow5 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow5.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow6 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow6.enterSubStateAsync(baseTransferSettingsSubFlow6.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
            return true;
        }

        @DexIgnore
        public void onSetLocalizationDataFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        public void onSetLocalizationDataSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Localization timeout. Cancel.");
            yb0<String> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public yb0<cd6> task;

        @DexIgnore
        public SetMicroAppMappingState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public void onConfigureMicroAppFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public void onConfigureMicroAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (BaseTransferSettingsSubFlow.this.getMicroAppMappings() == null || !(!BaseTransferSettingsSubFlow.this.getMicroAppMappings().isEmpty())) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
            } else {
                BleAdapterImpl bleAdapter = BaseTransferSettingsSubFlow.this.getBleAdapter();
                FLogger.Session logSession = BaseTransferSettingsSubFlow.this.getLogSession();
                List<nd0> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(BaseTransferSettingsSubFlow.this.getMicroAppMappings());
                wg6.a((Object) convertToSDKMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
                this.task = bleAdapter.configureMicroApp(logSession, convertToSDKMapping, this);
                if (this.task == null) {
                    BaseTransferSettingsSubFlow.this.stopSubFlow(0);
                } else {
                    startTimeout();
                }
            }
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Micro Apps timeout. Cancel.");
            yb0<cd6> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilterSettings extends BleStateAbs {
        @DexIgnore
        public yb0<cd6> task;

        @DexIgnore
        public SetNotificationFilterSettings() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                AppNotificationFilterSettings notificationFilterSettings = BaseTransferSettingsSubFlow.this.getNotificationFilterSettings();
                if (notificationFilterSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setNotificationFilters(BaseTransferSettingsSubFlow.this.getLogSession(), notificationFilterSettings.getNotificationFilters(), this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                    } else {
                        startTimeout();
                        obj = cd6.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No notification filter settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Notification Filter Settings step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetNotificationFilterSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
            return true;
        }

        @DexIgnore
        public void onSetNotificationFilterFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Notification Filter Settings timeout. Cancel.");
            yb0<cd6> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchApps extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetWatchApps() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                WatchAppMappingSettings watchAppMappingSettings = BaseTransferSettingsSubFlow.this.getWatchAppMappingSettings();
                if (watchAppMappingSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchApps(BaseTransferSettingsSubFlow.this.getLogSession(), watchAppMappingSettings, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
                    } else {
                        startTimeout();
                        obj = cd6.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Watch App step. No watch apps settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Watch App step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
            return true;
        }

        @DexIgnore
        public void onSetWatchAppFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Apps timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchParamsState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetWatchParamsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        private final Bundle exportFirmwareVersion() {
            Bundle bundle = new Bundle();
            bundle.putInt(ButtonService.WATCH_PARAMS_MAJOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMajor());
            bundle.putInt(ButtonService.WATCH_PARAMS_MINOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMinor());
            bundle.putFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION, Float.parseFloat(BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion()));
            return bundle;
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            startTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "supportedWatchParamVersion = " + BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamVersion() + ", currentWatchParamVersion=" + BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion());
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferSettingsSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback == null) {
                return true;
            }
            bleSessionCallback.onRequestLatestWatchParams(BaseTransferSettingsSubFlow.this.getSerial(), exportFirmwareVersion());
            return true;
        }

        @DexIgnore
        public void onGetWatchParamsFail() {
            stopTimeout();
            if (!retry(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), BaseTransferSettingsSubFlow.this.getSerial())) {
                BaseTransferSettingsSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            }
        }

        @DexIgnore
        public void onSetWatchParamsFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Param failed");
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onSetWatchParamsSuccess() {
            BaseTransferSettingsSubFlow.this.log("Set Watch Param success");
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Params timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }

        @DexIgnore
        public final void setWatchParamToDevice(String str, WatchParamsFileMapping watchParamsFileMapping) {
            wg6.b(str, "serial");
            wg6.b(watchParamsFileMapping, "watchParamsFileMapping");
            BaseTransferSettingsSubFlow.this.log("Set WatchParam to device");
            this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchParams(BaseTransferSettingsSubFlow.this.getLogSession(), watchParamsFileMapping, this);
            if (this.task == null) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
                return;
            }
            startTimeout();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferSettingsSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, boolean z, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, List<AlarmSetting> list, ComplicationAppMappingSettings complicationAppMappingSettings2, WatchAppMappingSettings watchAppMappingSettings2, BackgroundConfig backgroundConfig2, AppNotificationFilterSettings appNotificationFilterSettings, LocalizationData localizationData2, List<? extends MicroAppMapping> list2, int i, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        wg6.b(str, "tagName");
        wg6.b(bleSession, "bleSession");
        wg6.b(session, "logSession");
        wg6.b(str2, "serial");
        wg6.b(bleAdapterImpl, "mBleAdapterV2");
        this.isFullSync = z;
        this.userProfile = userProfile2;
        this.multiAlarmSettings = list;
        this.complicationAppMappingSettings = complicationAppMappingSettings2;
        this.watchAppMappingSettings = watchAppMappingSettings2;
        this.backgroundConfig = backgroundConfig2;
        this.notificationFilterSettings = appNotificationFilterSettings;
        this.localizationData = localizationData2;
        this.microAppMappings = list2;
        this.secondTimezoneOffset = i;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final void doNextState() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, failed because the current sub state is not an instance of SetWatchParamsState");
        }
    }

    @DexIgnore
    public final BackgroundConfig getBackgroundConfig() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final ComplicationAppMappingSettings getComplicationAppMappingSettings() {
        return this.complicationAppMappingSettings;
    }

    @DexIgnore
    public final LocalizationData getLocalizationData() {
        return this.localizationData;
    }

    @DexIgnore
    public final List<MicroAppMapping> getMicroAppMappings() {
        return this.microAppMappings;
    }

    @DexIgnore
    public final List<AlarmSetting> getMultiAlarmSettings() {
        return this.multiAlarmSettings;
    }

    @DexIgnore
    public final AppNotificationFilterSettings getNotificationFilterSettings() {
        return this.notificationFilterSettings;
    }

    @DexIgnore
    public final int getSecondTimezoneOffset() {
        return this.secondTimezoneOffset;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public final WatchAppMappingSettings getWatchAppMappingSettings() {
        return this.watchAppMappingSettings;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.SET_WATCH_PARAMS;
        String name = SetWatchParamsState.class.getName();
        wg6.a((Object) name, "SetWatchParamsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.SET_DEVICE_CONFIG_STATE;
        String name2 = SetDeviceConfigState.class.getName();
        wg6.a((Object) name2, "SetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.SET_LIST_ALARMS_STATE;
        String name3 = SetListAlarmsState.class.getName();
        wg6.a((Object) name3, "SetListAlarmsState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.SET_COMPLICATIONS_STATE;
        String name4 = SetComplications.class.getName();
        wg6.a((Object) name4, "SetComplications::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.SET_WATCH_APPS_STATE;
        String name5 = SetWatchApps.class.getName();
        wg6.a((Object) name5, "SetWatchApps::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<SubFlow.SessionState, String> sessionStateMap6 = getSessionStateMap();
        SubFlow.SessionState sessionState6 = SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name6 = SetBackgroundImageConfig.class.getName();
        wg6.a((Object) name6, "SetBackgroundImageConfig::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<SubFlow.SessionState, String> sessionStateMap7 = getSessionStateMap();
        SubFlow.SessionState sessionState7 = SubFlow.SessionState.SET_LOCALIZATION_STATE;
        String name7 = SetLocalization.class.getName();
        wg6.a((Object) name7, "SetLocalization::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
        HashMap<SubFlow.SessionState, String> sessionStateMap8 = getSessionStateMap();
        SubFlow.SessionState sessionState8 = SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name8 = SetNotificationFilterSettings.class.getName();
        wg6.a((Object) name8, "SetNotificationFilterSettings::class.java.name");
        sessionStateMap8.put(sessionState8, name8);
        HashMap<SubFlow.SessionState, String> sessionStateMap9 = getSessionStateMap();
        SubFlow.SessionState sessionState9 = SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE;
        String name9 = SetMicroAppMappingState.class.getName();
        wg6.a((Object) name9, "SetMicroAppMappingState::class.java.name");
        sessionStateMap9.put(sessionState9, name9);
    }

    @DexIgnore
    public final boolean isFullSync() {
        return this.isFullSync;
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_WATCH_PARAMS));
        return true;
    }

    @DexIgnore
    public final void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "getWatchParamFailed");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).onGetWatchParamsFail();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }

    @DexIgnore
    public final void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        wg6.b(str, "serial");
        wg6.b(watchParamsFileMapping, "watchParamsData");
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam...");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).setWatchParamToDevice(str, watchParamsFileMapping);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }
}
