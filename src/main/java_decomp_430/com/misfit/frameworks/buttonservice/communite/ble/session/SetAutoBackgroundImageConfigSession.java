package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.pb0;
import com.fossil.wg6;
import com.fossil.zb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoBackgroundImageConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ BackgroundConfig mNewBackgroundImageConfig;
    @DexIgnore
    public BackgroundConfig mOldBackgroundImageConfig;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoBackgroundImageConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            String tag = getTAG();
            MFLogger.d(tag, "All done of " + getTAG());
            SetAutoBackgroundImageConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetBackgroundImageConfigState() {
            super(SetAutoBackgroundImageConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetAutoBackgroundImageConfigSession.this.getBleAdapter().setBackgroundImage(SetAutoBackgroundImageConfigSession.this.getLogSession(), SetAutoBackgroundImageConfigSession.this.mNewBackgroundImageConfig, this);
            if (this.task == null) {
                SetAutoBackgroundImageConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetBackgroundImageFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetAutoBackgroundImageConfigSession.this.getContext(), SetAutoBackgroundImageConfigSession.this.getSerial())) {
                SetAutoBackgroundImageConfigSession.this.log("Reach the limit retry. Stop.");
                SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession = SetAutoBackgroundImageConfigSession.this;
                setAutoBackgroundImageConfigSession.storeMappings(setAutoBackgroundImageConfigSession.mNewBackgroundImageConfig, true);
                SetAutoBackgroundImageConfigSession.this.stop(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            }
        }

        @DexIgnore
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession = SetAutoBackgroundImageConfigSession.this;
            setAutoBackgroundImageConfigSession.storeMappings(setAutoBackgroundImageConfigSession.mNewBackgroundImageConfig, false);
            SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession2 = SetAutoBackgroundImageConfigSession.this;
            setAutoBackgroundImageConfigSession2.enterStateAsync(setAutoBackgroundImageConfigSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoBackgroundImageConfigSession(BackgroundConfig backgroundConfig, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG, bleAdapterImpl, bleSessionCallback);
        wg6.b(backgroundConfig, "mNewBackgroundImageConfig");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNewBackgroundImageConfig = backgroundConfig;
    }

    @DexIgnore
    private final void storeMappings(BackgroundConfig backgroundConfig, boolean z) {
        DevicePreferenceUtils.setAutoBackgroundImageConfig(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().a(backgroundConfig));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoBackgroundImageConfigSession setAutoBackgroundImageConfigSession = new SetAutoBackgroundImageConfigSession(this.mNewBackgroundImageConfig, getBleAdapter(), getBleSessionCallback());
        setAutoBackgroundImageConfigSession.setDevice(getDevice());
        return setAutoBackgroundImageConfigSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldBackgroundImageConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(pb0.class) == null) {
            log("This device does not support set background image.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (wg6.a((Object) this.mNewBackgroundImageConfig, (Object) this.mOldBackgroundImageConfig)) {
            log("New Background image config and the old one are the same, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else {
            long timestamp = this.mNewBackgroundImageConfig.getTimestamp();
            BackgroundConfig backgroundConfig = this.mOldBackgroundImageConfig;
            if (timestamp > (backgroundConfig != null ? backgroundConfig.getTimestamp() : 0)) {
                storeMappings(this.mNewBackgroundImageConfig, true);
                bleState = createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE);
            } else {
                log("Old Background image config timestamp is greater than the new one, no need to store again.");
                bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            }
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name = SetBackgroundImageConfigState.class.getName();
        wg6.a((Object) name, "SetBackgroundImageConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
