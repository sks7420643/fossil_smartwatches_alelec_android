package com.misfit.frameworks.buttonservice.communite.ble;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.BleSession$enterStateAsync$1", f = "BleSession.kt", l = {}, m = "invokeSuspend")
public final class BleSession$enterStateAsync$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleState $newState;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BleSession this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleSession$enterStateAsync$Anon1(BleSession bleSession, BleState bleState, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bleSession;
        this.$newState = bleState;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        BleSession$enterStateAsync$Anon1 bleSession$enterStateAsync$Anon1 = new BleSession$enterStateAsync$Anon1(this.this$0, this.$newState, xe6);
        bleSession$enterStateAsync$Anon1.p$ = (il6) obj;
        return bleSession$enterStateAsync$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BleSession$enterStateAsync$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.enterState(this.$newState);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
