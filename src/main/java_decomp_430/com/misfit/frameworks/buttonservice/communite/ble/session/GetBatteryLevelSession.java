package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.g60;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetBatteryLevelSession extends EnableMaintainingSession {
    @DexIgnore
    public int mBatteryLevel; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetBatteryLevelState extends BleStateAbs {
        @DexIgnore
        public zb0<HashMap<s60, r60>> task;

        @DexIgnore
        public GetBatteryLevelState() {
            super(GetBatteryLevelSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            GetBatteryLevelSession.this.log("Get Battery Level");
            this.task = GetBatteryLevelSession.this.getBleAdapter().getDeviceConfig(GetBatteryLevelSession.this.getLogSession(), this);
            if (this.task == null) {
                GetBatteryLevelSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
            getBatteryLevelSession.mBatteryLevel = getBatteryLevelSession.getBleAdapter().getBatteryLevel();
            GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
            getBatteryLevelSession2.log("Get Battery Level Failed, return the old one:" + GetBatteryLevelSession.this.mBatteryLevel);
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
            wg6.b(hashMap, "deviceConfiguration");
            stopTimeout();
            if (hashMap.containsKey(s60.BATTERY)) {
                GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
                g60 g60 = hashMap.get(s60.BATTERY);
                if (g60 != null) {
                    getBatteryLevelSession.mBatteryLevel = g60.getPercentage();
                    GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
                    getBatteryLevelSession2.log("Get Battery Level Success, value=" + GetBatteryLevelSession.this.mBatteryLevel);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
                }
            } else {
                GetBatteryLevelSession.this.log("Get Battery Level Success, but no value.");
            }
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<HashMap<s60, r60>> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetBatteryLevelSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.GET_BATTERY_LEVEL, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt(Constants.BATTERY, this.mBatteryLevel);
    }

    @DexIgnore
    public BleSession copyObject() {
        GetBatteryLevelSession getBatteryLevelSession = new GetBatteryLevelSession(getBleAdapter(), getBleSessionCallback());
        getBatteryLevelSession.setDevice(getDevice());
        return getBatteryLevelSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_BATTERY_LEVEL_STATE;
        String name = GetBatteryLevelState.class.getName();
        wg6.a((Object) name, "GetBatteryLevelState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
