package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1 extends xg6 implements hg6<s60[], cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ r60[] $deviceConfigItems$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, r60[] r60Arr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$deviceConfigItems$inlined = r60Arr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((s60[]) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(s60[] s60Arr) {
        wg6.b(s60Arr, "it");
        this.this$0.log(this.$logSession$inlined, "Set Device Config Success");
        this.$callback$inlined.onSetDeviceConfigSuccess();
    }
}
