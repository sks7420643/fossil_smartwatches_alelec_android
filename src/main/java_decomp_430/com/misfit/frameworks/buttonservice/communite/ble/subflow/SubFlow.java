package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.ac0;
import com.fossil.af6;
import com.fossil.bc0;
import com.fossil.c90;
import com.fossil.fitness.FitnessData;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.l40;
import com.fossil.ll6;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.common.constants.Constants;
import java.lang.reflect.Constructor;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SubFlow extends BleStateAbs implements ISessionSdkCallback {
    @DexIgnore
    public /* final */ BleAdapterImpl bleAdapter;
    @DexIgnore
    public /* final */ BleSession bleSession;
    @DexIgnore
    public /* final */ FLogger.Session logSession;
    @DexIgnore
    public BleStateAbs mCurrentState; // = new NullBleState(getTAG());
    @DexIgnore
    public /* final */ MFLog mfLog;
    @DexIgnore
    public /* final */ String serial;
    @DexIgnore
    public HashMap<SessionState, String> sessionStateMap;

    @DexIgnore
    public enum SessionState {
        SCANNING_STATE,
        ENABLE_MAINTAINING_CONNECTION_STATE,
        FETCH_DEVICE_INFO_STATE,
        GET_DEVICE_CONFIG_STATE,
        PLAY_DEVICE_ANIMATION_STATE,
        ERASE_DATA_FILE_STATE,
        DONE_STATE,
        OTA_STATE,
        SET_DEVICE_CONFIG_STATE,
        SET_WATCH_PARAMS,
        READ_OR_ERASE_STATE,
        READ_DATA_FILE_STATE,
        PROCESS_AND_STORE_DATA_STATE,
        SET_COMPLICATIONS_STATE,
        SET_WATCH_APPS_STATE,
        SET_BACKGROUND_IMAGE_CONFIG_STATE,
        SET_LOCALIZATION_STATE,
        SET_MICRO_APP_MAPPING,
        CLOSE_CONNECTION_STATE,
        REQUEST_HAND_CONTROL_STATE,
        RESET_HANDS_STATE,
        MOVE_HAND_STATE,
        APPLY_HAND_STATE,
        RELEASE_HAND_CONTROL_STATE,
        SET_STEP_GOAL_STATE,
        READ_RSSI_STATE,
        READ_REAL_TIME_STEPS_STATE,
        UPDATE_CURRENT_TIME_STATE,
        GET_BATTERY_LEVEL_STATE,
        SET_VIBRATION_STRENGTH_STATE,
        GET_VIBRATION_STRENGTH_STATE,
        SET_LIST_ALARMS_STATE,
        SET_BIOMETRIC_DATA_STATE,
        SET_SETTING_DONE_STATE,
        SET_HEART_RATE_MODE_STATE,
        SET_FRONT_LIGHT_ENABLE_STATE,
        READ_CURRENT_WORKOUT_STATE,
        STOP_CURRENT_WORKOUT_STATE,
        SET_NOTIFICATION_FILTERS_STATE,
        VERIFY_SECRET_KEY,
        GENERATE_RANDOM_KEY,
        GET_SECRET_KEY_THROUGH_SDK,
        AUTHENTICATE_DEVICE,
        EXCHANGE_SECRET_KEY,
        PUSH_SECRET_KEY_TO_CLOUD,
        SET_SECRET_KEY_TO_DEVICE,
        PROCESS_MAPPING,
        SET_MICRO_APP_MAPPING_AFTER_OTA_STATE,
        PROCESS_HID
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow(String str, BleSession bleSession2, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl) {
        super(str);
        wg6.b(str, "tagName");
        wg6.b(bleSession2, "bleSession");
        wg6.b(session, "logSession");
        wg6.b(str2, "serial");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.bleSession = bleSession2;
        this.mfLog = mFLog;
        this.logSession = session;
        this.serial = str2;
        this.bleAdapter = bleAdapterImpl;
        initMap();
    }

    @DexIgnore
    private final boolean enterSubState(BleStateAbs bleStateAbs) {
        if (!isExist()) {
            return false;
        }
        if (!BleState.Companion.isNull(this.mCurrentState) && !BleState.Companion.isNull(bleStateAbs) && xj6.b(this.mCurrentState.getClass().getName(), bleStateAbs.getClass().getName(), true)) {
            return true;
        }
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.onExit();
        }
        this.mCurrentState = bleStateAbs;
        if (BleState.Companion.isNull(this.mCurrentState)) {
            return false;
        }
        boolean onEnter = bleStateAbs.onEnter();
        if (!onEnter) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.e(tag, "Failed to enter state: " + bleStateAbs);
            this.mCurrentState = new NullBleState(getTAG());
        }
        return onEnter;
    }

    @DexIgnore
    private final void initMap() {
        this.sessionStateMap = new HashMap<>();
        initStateMap();
    }

    @DexIgnore
    public final void addFailureCode(int i) {
        this.bleSession.addFailureCode(i);
    }

    @DexIgnore
    public final BleStateAbs createConcreteState(SessionState sessionState) {
        BleStateAbs bleStateAbs;
        wg6.b(sessionState, Constants.STATE);
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            String str = hashMap.get(sessionState);
            if (str != null) {
                try {
                    Class<?> cls = Class.forName(str);
                    wg6.a((Object) cls, "Class.forName(stateClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{cls.getDeclaringClass()});
                    wg6.a((Object) declaredConstructor, "innerClass.getDeclaredConstructor(parentClass)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(new Object[]{this});
                    if (newInstance != null) {
                        bleStateAbs = (BleStateAbs) newInstance;
                        return bleStateAbs != null ? bleStateAbs : new NullBleState(getTAG());
                    }
                    throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs");
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = getTAG();
                    local.e(tag, "Inside getState method, cannot instance state " + str + ", e = " + e);
                    bleStateAbs = null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("sessionStateMap");
            throw null;
        }
    }

    @DexIgnore
    public final rm6 enterSubStateAsync(BleStateAbs bleStateAbs) {
        wg6.b(bleStateAbs, "newState");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new SubFlow$enterSubStateAsync$Anon1(this, bleStateAbs, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void errorLog(String str, FLogger.Component component, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        wg6.b(str, "message");
        wg6.b(component, "component");
        wg6.b(step, "step");
        wg6.b(appError, Constants.YO_ERROR_POST);
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.APP, appError);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(component, this.logSession, this.serial, getTAG(), build, step, str);
    }

    @DexIgnore
    public final BleAdapterImpl getBleAdapter() {
        return this.bleAdapter;
    }

    @DexIgnore
    public final BleSession getBleSession() {
        return this.bleSession;
    }

    @DexIgnore
    public final FLogger.Session getLogSession() {
        return this.logSession;
    }

    @DexIgnore
    public final BleStateAbs getMCurrentState() {
        return this.mCurrentState;
    }

    @DexIgnore
    public final MFLog getMfLog() {
        return this.mfLog;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final HashMap<SessionState, String> getSessionStateMap() {
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            return hashMap;
        }
        wg6.d("sessionStateMap");
        throw null;
    }

    @DexIgnore
    public abstract void initStateMap();

    @DexIgnore
    public void log(String str) {
        wg6.b(str, "message");
        FLogger.INSTANCE.getLocal().d(getTAG(), str);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str);
        }
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), str);
    }

    @DexIgnore
    public void onApplyHandPositionFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDeviceFound(q40 q40, int i) {
        wg6.b(q40, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExit() {
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.stopTimeout();
            enterSubState(new NullBleState(getTAG()));
        }
        super.onExit();
    }

    @DexIgnore
    public void onFetchDeviceInfoFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoSuccess(r40 r40) {
        wg6.b(r40, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
        wg6.b(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNotifyNotificationEventFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNotifyNotificationEventSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayDeviceAnimation(boolean z, bc0 bc0) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionSuccess(c90 c90) {
        wg6.b(c90, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        wg6.b(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onScanFail(l40 l40) {
        wg6.b(l40, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public abstract void onStop(int i);

    @DexIgnore
    public void onStopCurrentWorkoutSessionFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeyFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final void setMCurrentState(BleStateAbs bleStateAbs) {
        wg6.b(bleStateAbs, "<set-?>");
        this.mCurrentState = bleStateAbs;
    }

    @DexIgnore
    public final void setSessionStateMap(HashMap<SessionState, String> hashMap) {
        wg6.b(hashMap, "<set-?>");
        this.sessionStateMap = hashMap;
    }

    @DexIgnore
    public final void stopSubFlow(int i) {
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new SubFlow$stopSubFlow$Anon1(this, i, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void errorLog(String str, ErrorCodeBuilder.Step step, ac0 ac0) {
        wg6.b(str, "message");
        wg6.b(step, "step");
        wg6.b(ac0, "sdkError");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, ac0);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), build, step, str);
    }
}
