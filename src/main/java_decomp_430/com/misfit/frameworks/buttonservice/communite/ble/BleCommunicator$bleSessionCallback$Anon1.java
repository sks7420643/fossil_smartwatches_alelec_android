package com.misfit.frameworks.buttonservice.communite.ble;

import android.os.Bundle;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommunicator$bleSessionCallback$Anon1 implements BleSession.BleSessionCallback {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicator this$0;

    @DexIgnore
    public BleCommunicator$bleSessionCallback$Anon1(BleCommunicator bleCommunicator) {
        this.this$0 = bleCommunicator;
    }

    @DexIgnore
    public void broadcastExchangeSecretKeySuccess(String str, String str2) {
        wg6.b(str, "serial");
        wg6.b(str2, "secretKey");
        this.this$0.getCommunicationResultCallback().onExchangeSecretKeySuccess(str, str2);
    }

    @DexIgnore
    public void onAskForCurrentSecretKey(String str) {
        wg6.b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForCurrentSecretKey(str);
    }

    @DexIgnore
    public void onAskForLinkServer(CommunicateMode communicateMode, Bundle bundle) {
        wg6.b(communicateMode, "communicateMode");
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK || this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.SWITCH_DEVICE) {
            this.this$0.getCommunicationResultCallback().onAskForLinkServer(this.this$0.getSerial(), communicateMode, bundle);
        }
    }

    @DexIgnore
    public void onAskForRandomKey(String str) {
        wg6.b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForRandomKey(str);
    }

    @DexIgnore
    public void onAskForSecretKey(Bundle bundle) {
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onAskForServerSecretKey(this.this$0.getSerial(), bundle);
    }

    @DexIgnore
    public void onAskForStopWorkout(String str) {
        wg6.b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForStopWorkout(str);
    }

    @DexIgnore
    public void onAuthorizeDeviceSuccess(String str) {
        wg6.b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAuthorizeDeviceSuccess(str);
    }

    @DexIgnore
    public void onBleStateResult(int i, Bundle bundle) {
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (!BleSession.Companion.isNull(this.this$0.getCurrentSession())) {
            this.this$0.getCommunicationResultCallback().onCommunicatorResult(this.this$0.getCurrentSession().getCommunicateMode(), this.this$0.getSerial(), i, new ArrayList(), bundle);
        }
    }

    @DexIgnore
    public void onFirmwareLatest() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onFirmwareLatest(this.this$0.getSerial());
        }
    }

    @DexIgnore
    public void onNeedStartTimer(String str) {
        wg6.b(str, "serial");
        this.this$0.getCommunicationResultCallback().onNeedStartTimer(str);
    }

    @DexIgnore
    public void onReceivedSyncData(Bundle bundle) {
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.SYNC) {
            this.this$0.getCommunicationResultCallback().onReceivedSyncData(this.this$0.getSerial(), bundle);
        }
    }

    @DexIgnore
    public void onRequestLatestFirmware(Bundle bundle) {
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onRequestLatestFirmware(this.this$0.getSerial(), bundle);
        }
    }

    @DexIgnore
    public void onRequestLatestWatchParams(String str, Bundle bundle) {
        wg6.b(str, "serial");
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onRequestLatestWatchParams(str, bundle);
    }

    @DexIgnore
    public void onRequestPushSecretKeyToServer(String str, String str2) {
        wg6.b(str, "serial");
        wg6.b(str2, "secretKey");
        this.this$0.getCommunicationResultCallback().onRequestPushSecretKeyToServer(str, str2);
    }

    @DexIgnore
    public void onStop(int i, List<Integer> list, Bundle bundle, BleSession bleSession) {
        wg6.b(list, "requiredPermissionCodes");
        wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        wg6.b(bleSession, Constants.SESSION);
        if (wg6.a((Object) bleSession, (Object) this.this$0.getCurrentSession()) || bleSession.requireBroadCastInAnyCase()) {
            this.this$0.getCommunicationResultCallback().onCommunicatorResult(bleSession.getCommunicateMode(), this.this$0.getSerial(), i, list, bundle);
        }
        if (wg6.a((Object) bleSession, (Object) this.this$0.getCurrentSession())) {
            this.this$0.setNullCurrentSession();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.this$0.getTAG();
            local.d(tag, "Inside " + this.this$0.getTAG() + ".bleSessionCallback.onStop");
            this.this$0.startSessionInQueue();
        }
    }

    @DexIgnore
    public void onUpdateFirmwareFailed() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onUpdateFirmwareFailed(this.this$0.getSerial());
        }
    }

    @DexIgnore
    public void onUpdateFirmwareSuccess() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onUpdateFirmwareSuccess(this.this$0.getSerial());
        }
    }
}
