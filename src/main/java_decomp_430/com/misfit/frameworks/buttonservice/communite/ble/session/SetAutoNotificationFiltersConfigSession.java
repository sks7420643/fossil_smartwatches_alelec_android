package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.ob0;
import com.fossil.wg6;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoNotificationFiltersConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ AppNotificationFilterSettings mNewNotificationFilterSettings;
    @DexIgnore
    public AppNotificationFilterSettings mOldNotificationFilterSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoNotificationFiltersConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoNotificationFiltersConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilters extends BleStateAbs {
        @DexIgnore
        public yb0<cd6> task;

        @DexIgnore
        public SetNotificationFilters() {
            super(SetAutoNotificationFiltersConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetAutoNotificationFiltersConfigSession.this.getBleAdapter().setNotificationFilters(SetAutoNotificationFiltersConfigSession.this.getLogSession(), SetAutoNotificationFiltersConfigSession.this.mNewNotificationFilterSettings.getNotificationFilters(), this);
            if (this.task == null) {
                SetAutoNotificationFiltersConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetNotificationFilterFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetAutoNotificationFiltersConfigSession.this.getContext(), SetAutoNotificationFiltersConfigSession.this.getSerial())) {
                SetAutoNotificationFiltersConfigSession.this.log("Reach the limit retry. Stop.");
                SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession = SetAutoNotificationFiltersConfigSession.this;
                setAutoNotificationFiltersConfigSession.storeMappings(setAutoNotificationFiltersConfigSession.mNewNotificationFilterSettings, true);
                SetAutoNotificationFiltersConfigSession.this.stop(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            }
        }

        @DexIgnore
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession = SetAutoNotificationFiltersConfigSession.this;
            setAutoNotificationFiltersConfigSession.storeMappings(setAutoNotificationFiltersConfigSession.mNewNotificationFilterSettings, false);
            SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession2 = SetAutoNotificationFiltersConfigSession.this;
            setAutoNotificationFiltersConfigSession2.enterState(setAutoNotificationFiltersConfigSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            yb0<cd6> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoNotificationFiltersConfigSession(AppNotificationFilterSettings appNotificationFilterSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS, bleAdapterImpl, bleSessionCallback);
        wg6.b(appNotificationFilterSettings, "mNewNotificationFilterSettings");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNewNotificationFilterSettings = appNotificationFilterSettings;
    }

    @DexIgnore
    private final void storeMappings(AppNotificationFilterSettings appNotificationFilterSettings, boolean z) {
        DevicePreferenceUtils.setAutoNotificationFiltersConfig(getBleAdapter().getContext(), getBleAdapter().getSerial(), appNotificationFilterSettings);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession = new SetAutoNotificationFiltersConfigSession(this.mNewNotificationFilterSettings, getBleAdapter(), getBleSessionCallback());
        setAutoNotificationFiltersConfigSession.setDevice(getDevice());
        return setAutoNotificationFiltersConfigSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldNotificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(ob0.class) == null) {
            log("This device does not support set complication apps.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (AppNotificationFilterSettings.CREATOR.isSettingsSame(this.mOldNotificationFilterSettings, this.mNewNotificationFilterSettings)) {
            log("New notification settings and the old one are the same, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE);
        } else if (AppNotificationFilterSettings.CREATOR.compareTimeStamp(this.mNewNotificationFilterSettings, this.mOldNotificationFilterSettings) > 0) {
            storeMappings(this.mNewNotificationFilterSettings, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE);
        } else {
            log("Old notification settings timestamp is greater than the new one, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name = SetNotificationFilters.class.getName();
        wg6.a((Object) name, "SetNotificationFilters::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
