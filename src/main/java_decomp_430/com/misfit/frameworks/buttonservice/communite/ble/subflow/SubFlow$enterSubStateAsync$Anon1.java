package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$enterSubStateAsync$1", f = "SubFlow.kt", l = {}, m = "invokeSuspend")
public final class SubFlow$enterSubStateAsync$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleStateAbs $newState;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SubFlow this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$enterSubStateAsync$Anon1(SubFlow subFlow, BleStateAbs bleStateAbs, xe6 xe6) {
        super(2, xe6);
        this.this$0 = subFlow;
        this.$newState = bleStateAbs;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SubFlow$enterSubStateAsync$Anon1 subFlow$enterSubStateAsync$Anon1 = new SubFlow$enterSubStateAsync$Anon1(this.this$0, this.$newState, xe6);
        subFlow$enterSubStateAsync$Anon1.p$ = (il6) obj;
        return subFlow$enterSubStateAsync$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SubFlow$enterSubStateAsync$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            boolean unused = this.this$0.enterSubState(this.$newState);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
