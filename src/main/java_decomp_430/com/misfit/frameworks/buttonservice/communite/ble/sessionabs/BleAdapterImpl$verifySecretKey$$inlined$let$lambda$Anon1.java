package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1 extends xg6 implements hg6<Boolean, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $secretKey$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, byte[] bArr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$secretKey$inlined = bArr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke(((Boolean) obj).booleanValue());
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(boolean z) {
        this.this$0.log(this.$logSession$inlined, "Verify Secret Key Success");
        this.$callback$inlined.onVerifySecretKeySuccess(z);
    }
}
