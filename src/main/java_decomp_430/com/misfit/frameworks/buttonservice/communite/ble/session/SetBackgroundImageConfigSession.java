package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.wg6;
import com.fossil.zb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetBackgroundImageConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ BackgroundConfig mBackgroundImageConfig;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetBackgroundImageConfigState() {
            super(SetBackgroundImageConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetBackgroundImageConfigSession.this.getBleAdapter().setBackgroundImage(SetBackgroundImageConfigSession.this.getLogSession(), SetBackgroundImageConfigSession.this.mBackgroundImageConfig, this);
            if (this.task == null) {
                SetBackgroundImageConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetBackgroundImageFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetBackgroundImageConfigSession.this.getContext(), SetBackgroundImageConfigSession.this.getSerial())) {
                SetBackgroundImageConfigSession.this.log("Reach the limit retry. Stop.");
                SetBackgroundImageConfigSession.this.stop(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            }
        }

        @DexIgnore
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoBackgroundImageConfig(SetBackgroundImageConfigSession.this.getBleAdapter().getContext(), SetBackgroundImageConfigSession.this.getBleAdapter().getSerial(), new Gson().a(SetBackgroundImageConfigSession.this.mBackgroundImageConfig));
            SetBackgroundImageConfigSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetBackgroundImageConfigSession(BackgroundConfig backgroundConfig, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_BACKGROUND_IMAGE_CONFIG, bleAdapterImpl, bleSessionCallback);
        wg6.b(backgroundConfig, "mBackgroundImageConfig");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mBackgroundImageConfig = backgroundConfig;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetBackgroundImageConfigSession setBackgroundImageConfigSession = new SetBackgroundImageConfigSession(this.mBackgroundImageConfig, getBleAdapter(), getBleSessionCallback());
        setBackgroundImageConfigSession.setDevice(getDevice());
        return setBackgroundImageConfigSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name = SetBackgroundImageConfigState.class.getName();
        wg6.a((Object) name, "SetBackgroundImageConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
