package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum DeviceOwnership {
    DeviceOwnershipUnknown(-1),
    DeviceOwnershipDeactivated(-2),
    DeviceOwnershipNeverLinked(1),
    DeviceOwnershipUsedToLinkToCurrentAcc(2),
    DeviceOwnershipUsedToLinkToAnotherAcc(3),
    DeviceOwnershipAlreadyLinkedToCurrentAcc(4),
    DeviceOwnershipAlreadyLinkedToAnotherAcc(0);
    
    @DexIgnore
    public int value;

    @DexIgnore
    public DeviceOwnership(int i) {
        this.value = i;
    }

    @DexIgnore
    public static DeviceOwnership fromInt(int i) {
        for (DeviceOwnership deviceOwnership : values()) {
            if (deviceOwnership.getValue() == i) {
                return deviceOwnership;
            }
        }
        return null;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
