package com.portfolio.platform.response;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.nd6;
import com.fossil.qg6;
import com.fossil.rx6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.fossil.zq6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerError;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResponseKt {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.response.ResponseKt", f = "Response.kt", l = {15}, m = "handleRequest")
    public static final class a extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public a(xe6 xe6) {
            super(xe6);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return ResponseKt.a((hg6) null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public static final <T> Object a(hg6<? super xe6<? super rx6<T>>, ? extends Object> hg6, xe6<? super ap4<T>> xe6) {
        a aVar;
        int i;
        if (xe6 instanceof a) {
            aVar = (a) xe6;
            int i2 = aVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                aVar.label = i2 - Integer.MIN_VALUE;
                Object obj = aVar.result;
                Object a2 = ff6.a();
                i = aVar.label;
                if (i != 0) {
                    nc6.a((Object) obj);
                    aVar.L$0 = hg6;
                    aVar.label = 1;
                    obj = hg6.invoke(aVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    hg6 hg62 = (hg6) aVar.L$0;
                    try {
                        nc6.a((Object) obj);
                    } catch (Exception e) {
                        Exception exc = e;
                        if (exc instanceof SocketTimeoutException) {
                            return new zo4(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, exc, (String) null, 8, (qg6) null);
                        }
                        if (exc instanceof UnknownHostException) {
                            return new zo4(601, (ServerError) null, exc, (String) null, 8, (qg6) null);
                        }
                        return new zo4(600, (ServerError) null, exc, (String) null, 8, (qg6) null);
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return a((rx6) obj);
            }
        }
        aVar = new a(xe6);
        Object obj2 = aVar.result;
        Object a22 = ff6.a();
        i = aVar.label;
        if (i != 0) {
        }
        return a((rx6) obj2);
    }

    @DexIgnore
    public static final <T> ap4<T> b(rx6<T> rx6) {
        String str;
        String str2;
        zo4 zo4;
        String str3;
        boolean z = true;
        Integer num = null;
        if (rx6.d()) {
            Object a2 = rx6.a();
            if (rx6.f().m() != null) {
                FLogger.INSTANCE.getLocal().d("RepoResponse", "cacheResponse valid");
            } else {
                z = false;
            }
            if (rx6.f().D() != null) {
                Response D = rx6.f().D();
                if (D == null) {
                    wg6.a();
                    throw null;
                } else if (D.n() != 304) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("networkResponse valid httpCode ");
                    Response D2 = rx6.f().D();
                    if (D2 != null) {
                        num = Integer.valueOf(D2.n());
                    }
                    sb.append(num);
                    local.d("RepoResponse", sb.toString());
                    z = false;
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("RepoResponse", "isFromCache=" + z);
            return new cp4(a2, z);
        }
        int b = rx6.b();
        if (nd6.a((T[]) new Integer[]{504, 503, 500, 401, Integer.valueOf(MFNetworkReturnCode.RATE_LIMIT_EXEEDED), 601, Integer.valueOf(MFNetworkReturnCode.CLIENT_TIMEOUT), 413}, Integer.valueOf(b))) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            zq6 c = rx6.c();
            if (c == null || (str3 = c.string()) == null) {
                str3 = rx6.e();
            }
            serverError.setMessage(str3);
            return new zo4(b, serverError, (Throwable) null, (String) null, 8, (qg6) null);
        }
        zq6 c2 = rx6.c();
        if (c2 == null || (str = c2.string()) == null) {
            str = rx6.e();
        }
        try {
            ServerError serverError2 = (ServerError) new Gson().a(str, ServerError.class);
            if (serverError2 != null) {
                Integer code = serverError2.getCode();
                if (code != null) {
                    if (code.intValue() == 0) {
                    }
                }
                zo4 = new zo4(rx6.b(), serverError2, (Throwable) null, (String) null, 8, (qg6) null);
                return zo4;
            }
            zo4 = new zo4(rx6.b(), (ServerError) null, (Throwable) null, str);
            return zo4;
        } catch (Exception unused) {
            zq6 c3 = rx6.c();
            if (c3 == null || (str2 = c3.string()) == null) {
                str2 = rx6.e();
            }
            return new zo4(rx6.b(), new ServerError(b, str2), (Throwable) null, (String) null, 8, (qg6) null);
        }
    }

    @DexIgnore
    public static final <T> ap4<T> a(rx6<T> rx6) {
        wg6.b(rx6, "$this$createRepoResponse");
        try {
            return b(rx6);
        } catch (Throwable th) {
            return a(th);
        }
    }

    @DexIgnore
    public static final <T> zo4<T> a(Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("RepoResponse", "create=" + th.getMessage());
        if (th instanceof SocketTimeoutException) {
            return new zo4(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, th, (String) null, 8, (qg6) null);
        }
        if (th instanceof UnknownHostException) {
            return new zo4(601, (ServerError) null, th, (String) null, 8, (qg6) null);
        }
        return new zo4(600, (ServerError) null, th, (String) null, 8, (qg6) null);
    }
}
