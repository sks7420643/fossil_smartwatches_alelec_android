package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.aj6;
import com.fossil.f7;
import com.fossil.gy5;
import com.fossil.hg6;
import com.fossil.jm4;
import com.fossil.qd6;
import com.fossil.rd6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xg6;
import com.fossil.yd6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeekHeartRateChart extends BaseChart {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public float B;
    @DexIgnore
    public String C; // = "";
    @DexIgnore
    public float D;
    @DexIgnore
    public String E; // = "";
    @DexIgnore
    public String F; // = "";
    @DexIgnore
    public String G; // = "";
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public short S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public Paint U;
    @DexIgnore
    public Paint V;
    @DexIgnore
    public Paint W;
    @DexIgnore
    public Paint a0;
    @DexIgnore
    public Paint b0;
    @DexIgnore
    public int c0;
    @DexIgnore
    public List<Integer> d0;
    @DexIgnore
    public List<PointF> e0;
    @DexIgnore
    public int f0; // = 1;
    @DexIgnore
    public List<String> g0; // = new ArrayList();
    @DexIgnore
    public String u; // = "";
    @DexIgnore
    public String v; // = "";
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public float x;
    @DexIgnore
    public String y; // = "";
    @DexIgnore
    public String z; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements hg6<Integer, Boolean> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke(((Number) obj).intValue()));
        }

        @DexIgnore
        public final boolean invoke(int i) {
            return i > 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<Integer, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke(((Number) obj).intValue()));
        }

        @DexIgnore
        public final boolean invoke(int i) {
            return i > 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeekHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface c;
        String b2;
        Typeface c2;
        Typeface c3;
        String b3;
        String b4;
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.WeekHeartRateChart);
        String string = obtainStyledAttributes.getString(2);
        this.u = string == null ? "dianaHeartRateTab" : string;
        String string2 = obtainStyledAttributes.getString(4);
        this.v = string2 == null ? "nonBrandDisableCalendarDay" : string2;
        String string3 = obtainStyledAttributes.getString(5);
        this.w = string3 == null ? "calendarDate" : string3;
        this.x = obtainStyledAttributes.getDimension(3, gy5.c(13.0f));
        String string4 = obtainStyledAttributes.getString(8);
        this.y = string4 == null ? "onDianaHeartRateTab" : string4;
        String string5 = obtainStyledAttributes.getString(9);
        this.z = string5 == null ? "calendarWeekday" : string5;
        String string6 = obtainStyledAttributes.getString(12);
        this.A = string6 == null ? "calendarWeekday" : string6;
        this.B = obtainStyledAttributes.getDimension(10, gy5.c(13.0f));
        String string7 = obtainStyledAttributes.getString(11);
        this.C = string7 == null ? "secondaryText" : string7;
        this.D = obtainStyledAttributes.getDimension(7, gy5.a(13.0f));
        String string8 = obtainStyledAttributes.getString(0);
        this.E = string8 == null ? "averageRestingHeartRate" : string8;
        String string9 = obtainStyledAttributes.getString(1);
        this.F = string9 == null ? "aboveAverageRestingHeartRate" : string9;
        String string10 = obtainStyledAttributes.getString(6);
        this.G = string10 == null ? "maxHeartRate" : string10;
        obtainStyledAttributes.recycle();
        this.T = new Paint();
        if (!TextUtils.isEmpty(this.v) && (b4 = ThemeManager.l.a().b(this.u)) != null) {
            this.T.setColor(Color.parseColor(b4));
        }
        this.T.setStrokeWidth(2.0f);
        this.T.setStyle(Paint.Style.STROKE);
        this.U = new Paint(1);
        if (!TextUtils.isEmpty(this.v) && (b3 = ThemeManager.l.a().b(this.v)) != null) {
            this.U.setColor(Color.parseColor(b3));
        }
        this.U.setStyle(Paint.Style.FILL);
        this.U.setTextSize(this.x);
        if (!TextUtils.isEmpty(this.w) && (c3 = ThemeManager.l.a().c(this.w)) != null) {
            this.U.setTypeface(c3);
        }
        this.V = new Paint(1);
        this.V.setColor(Color.parseColor(ThemeManager.l.a().b(this.y)));
        this.V.setTextSize(this.D);
        this.V.setStyle(Paint.Style.FILL);
        if (!TextUtils.isEmpty(this.z) && (c2 = ThemeManager.l.a().c(this.z)) != null) {
            this.V.setTypeface(c2);
        }
        this.W = new Paint(1);
        if (!TextUtils.isEmpty(this.C) && (b2 = ThemeManager.l.a().b(this.C)) != null) {
            this.W.setColor(Color.parseColor(b2));
        }
        this.W.setStyle(Paint.Style.FILL);
        this.W.setTextSize(this.B);
        if (!TextUtils.isEmpty(this.A) && (c = ThemeManager.l.a().c(this.A)) != null) {
            this.W.setTypeface(c);
        }
        this.a0 = new Paint(1);
        this.a0.setColor(Color.parseColor(ThemeManager.l.a().b(this.E)));
        this.a0.setStyle(Paint.Style.FILL);
        this.b0 = new Paint(1);
        this.b0.setColor(f7.c(Color.parseColor(ThemeManager.l.a().b(this.G)), 20));
        this.b0.setStyle(Paint.Style.STROKE);
        this.b0.setStrokeWidth(gy5.a(2.0f));
        Rect rect = new Rect();
        this.U.getTextBounds("222", 0, 3, rect);
        this.O = (float) rect.width();
        this.P = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886974);
        this.W.getTextBounds(a2, 0, a2.length(), rect2);
        this.Q = (float) rect2.height();
        ArrayList arrayList = new ArrayList(7);
        for (int i = 0; i < 7; i++) {
            arrayList.add(0);
        }
        this.d0 = arrayList;
        this.e0 = new ArrayList();
        d();
        a();
    }

    @DexIgnore
    public final void a(List<Integer> list) {
        wg6.b(list, "heartRatePointList");
        this.d0.clear();
        this.e0.clear();
        List<Integer> list2 = this.d0;
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        Iterator<T> it = list.iterator();
        while (true) {
            int i = 0;
            if (!it.hasNext()) {
                break;
            }
            Integer num = (Integer) it.next();
            if (num != null) {
                i = num.intValue();
            }
            arrayList.add(Integer.valueOf(i));
        }
        list2.addAll(arrayList);
        d();
        int size = this.d0.size();
        if (size < 7) {
            int i2 = 7 - size;
            ArrayList arrayList2 = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList2.add(0);
            }
            this.d0.addAll(arrayList2);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.b(canvas);
        this.H = ((float) canvas.getHeight()) - (this.Q * ((float) 2));
        this.I = 0.0f;
        this.J = ((float) canvas.getWidth()) - (this.O / ((float) 3));
        float f = (float) 6;
        this.K = this.B + this.D + (this.Q * f);
        float a2 = this.I + gy5.a(10.0f);
        float f2 = this.D;
        this.L = a2 + f2;
        this.N = (this.H - f2) - (this.B * f);
        this.M = (this.J - (this.O * 1.5f)) - f2;
        f(canvas);
    }

    @DexIgnore
    public final void d() {
        int i;
        short s;
        Integer num = (Integer) aj6.e(aj6.a(yd6.b(this.d0), a.INSTANCE));
        int i2 = 0;
        this.R = num != null ? (short) num.intValue() : 0;
        Integer num2 = (Integer) aj6.d(aj6.a(yd6.b(this.d0), b.INSTANCE));
        this.S = num2 != null ? (short) num2.intValue() : 0;
        if (this.S == ((short) 0)) {
            this.S = 100;
        }
        int i3 = 0;
        for (Number intValue : this.d0) {
            int intValue2 = intValue.intValue();
            if (intValue2 > 0) {
                i3 += intValue2;
                i2++;
            }
        }
        if (i2 > 0) {
            i = i3 / i2;
        } else {
            i = (this.S - this.R) / 2;
        }
        this.c0 = i;
        if (Math.abs(this.S - this.c0) >= Math.abs(this.R - this.c0)) {
            s = this.S;
        } else {
            s = this.R;
        }
        int abs = Math.abs(s);
        int i4 = this.c0;
        this.f0 = abs != i4 ? Math.abs(s - i4) * 2 : 1;
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        float f;
        short s = this.S;
        int i = this.c0;
        if (s != ((short) i)) {
            float f2 = this.K;
            f = f2 + (((this.N - f2) / ((float) this.f0)) * ((float) (s - i)));
        } else {
            float f3 = this.K;
            f = f3 + ((this.N - f3) / 2.0f);
        }
        canvas.drawLine(this.I, f, (float) canvas.getWidth(), f, this.T);
        float measureText = this.J - this.U.measureText(String.valueOf(this.c0));
        float f4 = f + (this.P * 1.5f);
        canvas.drawText(String.valueOf(this.c0), measureText, f4, this.U);
        String a2 = jm4.a(getContext(), 2131886548);
        canvas.drawText(a2, this.J - this.U.measureText(a2.toString()), f4 + this.P, this.U);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        a(canvas, (this.M - this.L) / ((float) 6));
    }

    @DexIgnore
    public final List<String> getListWeekDays() {
        return this.g0;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        setMWidth(i);
        setMHeight(i2);
        setMLeftPadding(getPaddingLeft());
        setMTopPadding(getPaddingTop());
        setMRightPadding(getPaddingRight());
        setMBottomPadding(getPaddingBottom());
        getMGraph().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMBackground().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMGraphOverlay().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
    }

    @DexIgnore
    public final void setListWeekDays(List<String> list) {
        wg6.b(list, ServerSetting.VALUE);
        this.g0.clear();
        this.g0.addAll(list);
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void a(Canvas canvas, float f) {
        String str;
        ThemeManager themeManager;
        Rect rect = new Rect();
        int i = 0;
        for (T next : this.d0) {
            int i2 = i + 1;
            if (i >= 0) {
                int intValue = ((Number) next).intValue();
                float f2 = this.L + (((float) i) * f);
                float f3 = this.K;
                float f4 = this.N;
                float f5 = (f4 - f3) / ((float) this.f0);
                short s = this.S;
                float f6 = f3 + (f5 * ((float) (s - intValue)));
                if (this.R == s) {
                    if (intValue > 0) {
                        f6 = f3 + ((f4 - f3) / 2.0f);
                        a(canvas, f2, f6, intValue, Color.parseColor(ThemeManager.l.a().b(this.E)), rect);
                    }
                } else if (intValue > 0) {
                    if (intValue > this.c0) {
                        themeManager = ThemeManager.l.a();
                        str = this.F;
                    } else {
                        themeManager = ThemeManager.l.a();
                        str = this.E;
                    }
                    a(canvas, f2, f6, intValue, Color.parseColor(themeManager.b(str)), rect);
                }
                this.e0.add(new PointF(f2, f6));
                i = i2;
            } else {
                qd6.c();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void d(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.d(canvas);
        if (this.g0.isEmpty()) {
            this.g0.addAll(qd6.d(jm4.a((Context) PortfolioApp.get.instance(), 2131886537), jm4.a((Context) PortfolioApp.get.instance(), 2131886536), jm4.a((Context) PortfolioApp.get.instance(), 2131886539), jm4.a((Context) PortfolioApp.get.instance(), 2131886541), jm4.a((Context) PortfolioApp.get.instance(), 2131886540), jm4.a((Context) PortfolioApp.get.instance(), 2131886535), jm4.a((Context) PortfolioApp.get.instance(), 2131886538)));
        }
        a(canvas, this.g0, (this.M - this.L) / ((float) 6), this.H + (((((float) canvas.getHeight()) - this.H) + this.Q) / ((float) 2)));
    }

    @DexIgnore
    public final void a(Canvas canvas, float f, float f2, int i, int i2, Rect rect) {
        this.a0.setColor(i2);
        canvas.drawCircle(f, f2, this.D, this.a0);
        canvas.drawCircle(f, f2, this.D + gy5.a(3.0f), this.b0);
        String valueOf = String.valueOf(i);
        this.W.getTextBounds(valueOf, 0, valueOf.length(), rect);
        canvas.drawText(valueOf, f - (this.V.measureText(valueOf) / ((float) 2)), f2 + ((float) (rect.height() / 2)), this.V);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, float f, float f2) {
        int i = 0;
        for (T next : list) {
            int i2 = i + 1;
            if (i >= 0) {
                String str = (String) next;
                canvas.drawText(str, (this.L + (((float) i) * f)) - (this.U.measureText(str) / ((float) 2)), f2, this.W);
                i = i2;
            } else {
                qd6.c();
                throw null;
            }
        }
    }
}
