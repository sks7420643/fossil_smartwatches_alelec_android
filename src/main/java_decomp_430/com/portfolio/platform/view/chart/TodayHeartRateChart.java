package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.gy5;
import com.fossil.jm4;
import com.fossil.jz5;
import com.fossil.lc6;
import com.fossil.pc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TodayHeartRateChart extends BaseChart {
    @DexIgnore
    public String A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public short M;
    @DexIgnore
    public short N;
    @DexIgnore
    public Paint O;
    @DexIgnore
    public Paint P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public List<jz5> S;
    @DexIgnore
    public float T;
    @DexIgnore
    public String U;
    @DexIgnore
    public List<pc6<Integer, lc6<Integer, Float>, String>> V; // = new ArrayList();
    @DexIgnore
    public int W;
    @DexIgnore
    public String u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public String x;
    @DexIgnore
    public String y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v25, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TodayHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface c;
        Typeface c2;
        String b;
        String b2;
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        String str = "";
        this.x = str;
        this.y = str;
        this.A = str;
        this.U = str;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.TodayHeartRateChart);
        String string = obtainStyledAttributes.getString(1);
        this.U = string == null ? "#ffffff" : string;
        String string2 = obtainStyledAttributes.getString(0);
        this.u = string2 == null ? str : string2;
        this.v = obtainStyledAttributes.getColor(5, w6.a(context, R.color.steps));
        this.w = obtainStyledAttributes.getColor(4, w6.a(context, 2131099846));
        String string3 = obtainStyledAttributes.getString(6);
        this.x = string3 == null ? str : string3;
        String string4 = obtainStyledAttributes.getString(3);
        this.y = string4 == null ? str : string4;
        this.z = obtainStyledAttributes.getDimension(2, gy5.c(13.0f));
        String string5 = obtainStyledAttributes.getString(8);
        this.A = string5 != null ? string5 : str;
        this.B = obtainStyledAttributes.getDimension(7, gy5.c(13.0f));
        obtainStyledAttributes.recycle();
        this.O = new Paint();
        if (!TextUtils.isEmpty(this.u) && (b2 = ThemeManager.l.a().b(this.u)) != null) {
            this.O.setColor(Color.parseColor(b2));
        }
        this.O.setStrokeWidth(2.0f);
        this.O.setStyle(Paint.Style.STROKE);
        this.P = new Paint(1);
        if (!TextUtils.isEmpty(this.x) && (b = ThemeManager.l.a().b(this.x)) != null) {
            this.P.setColor(Color.parseColor(b));
        }
        this.P.setStyle(Paint.Style.FILL);
        this.P.setTextSize(this.z);
        if (!TextUtils.isEmpty(this.y) && (c2 = ThemeManager.l.a().c(this.y)) != null) {
            this.P.setTypeface(c2);
        }
        this.Q = new Paint(1);
        this.Q.setColor(this.P.getColor());
        this.Q.setStyle(Paint.Style.FILL);
        this.Q.setTextSize(this.B);
        if (!TextUtils.isEmpty(this.A) && (c = ThemeManager.l.a().c(this.A)) != null) {
            this.Q.setTypeface(c);
        }
        this.R = new Paint(1);
        this.R.setStrokeWidth(gy5.a(2.0f));
        this.R.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.P.getTextBounds("222", 0, 3, rect);
        this.J = (float) rect.width();
        this.K = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886974);
        this.Q.getTextBounds(a2, 0, a2.length(), rect2);
        this.L = (float) rect2.height();
        this.S = new ArrayList();
        d();
        a();
        e();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wg6.b(str, "maxHeartRate");
        wg6.b(str2, "lowestHeartRate");
        String b = ThemeManager.l.a().b(str);
        String b2 = ThemeManager.l.a().b(str2);
        if (b != null) {
            this.w = Color.parseColor(b);
        }
        if (b2 != null) {
            this.v = Color.parseColor(b2);
        }
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.b(canvas);
        this.C = ((float) canvas.getHeight()) - (this.L * ((float) 2));
        this.D = 0.0f;
        this.E = (float) canvas.getWidth();
        this.F = this.B;
        this.G = this.D + gy5.a(5.0f);
        this.I = this.C - (this.B * ((float) 6));
        this.H = this.E - (this.J * 2.0f);
        float f = this.H - this.G;
        int i = this.W;
        if (i == 0) {
            i = DateTimeConstants.MINUTES_PER_DAY;
        }
        this.T = f / ((float) i);
        f(canvas);
        e();
    }

    @DexIgnore
    public final void d() {
        Object obj;
        List<jz5> list = this.S;
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((jz5) next).d() <= 0) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        Iterator it2 = arrayList.iterator();
        Object obj2 = null;
        if (!it2.hasNext()) {
            obj = null;
        } else {
            obj = it2.next();
            if (it2.hasNext()) {
                int d = ((jz5) obj).d();
                do {
                    Object next2 = it2.next();
                    int d2 = ((jz5) next2).d();
                    if (d > d2) {
                        obj = next2;
                        d = d2;
                    }
                } while (it2.hasNext());
            }
        }
        jz5 jz5 = (jz5) obj;
        this.M = jz5 != null ? (short) jz5.d() : 0;
        List<jz5> list2 = this.S;
        ArrayList arrayList2 = new ArrayList();
        for (T next3 : list2) {
            if (((jz5) next3).b() > 0) {
                arrayList2.add(next3);
            }
        }
        Iterator it3 = arrayList2.iterator();
        if (it3.hasNext()) {
            obj2 = it3.next();
            if (it3.hasNext()) {
                int b = ((jz5) obj2).b();
                do {
                    Object next4 = it3.next();
                    int b2 = ((jz5) next4).b();
                    if (b < b2) {
                        obj2 = next4;
                        b = b2;
                    }
                } while (it3.hasNext());
            }
        }
        jz5 jz52 = (jz5) obj2;
        this.N = jz52 != null ? (short) jz52.b() : 0;
        if (this.N == ((short) 0)) {
            this.N = 100;
        }
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.D, this.I, (float) canvas.getWidth(), this.I, this.O);
        canvas.drawLine(this.D, this.F, (float) canvas.getWidth(), this.F, this.O);
        float f = (float) 2;
        canvas.drawText(String.valueOf(this.M), this.H + (((((float) canvas.getWidth()) - this.H) - this.P.measureText(String.valueOf(this.M))) / f) + gy5.a(2.0f), this.I + (this.K * 1.5f), this.P);
        canvas.drawText(String.valueOf(this.N), this.H + (((((float) canvas.getWidth()) - this.H) - this.P.measureText(String.valueOf(this.N))) / f) + gy5.a(2.0f), this.F + (this.K * 1.5f), this.P);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        int i;
        ArrayList<Path> arrayList = new ArrayList<>();
        while (true) {
            Path path = null;
            do {
                boolean z2 = true;
                i = 0;
                for (jz5 jz5 : this.S) {
                    float c = this.G + (((float) jz5.c()) * this.T);
                    float f = this.F;
                    short s = this.N;
                    float d = f + (((this.I - f) / ((float) (s - this.M))) * ((float) (s - jz5.d())));
                    if (d <= this.I) {
                        if (z2) {
                            path = new Path();
                            path.moveTo(c, d);
                            z2 = false;
                        } else if (path != null) {
                            path.lineTo(c, d);
                        } else {
                            wg6.a();
                            throw null;
                        }
                        i++;
                    }
                }
                if (path != null) {
                    arrayList.add(path);
                }
                Paint paint = this.R;
                float f2 = this.G;
                paint.setShader(new LinearGradient(f2, this.F, f2, this.I, this.w, this.v, Shader.TileMode.MIRROR));
                for (Path drawPath : arrayList) {
                    canvas.drawPath(drawPath, this.R);
                }
                return;
            } while (path == null);
            if (i > 1) {
                arrayList.add(path);
            }
        }
    }

    @DexIgnore
    public final int getDayInMinuteWithTimeZone() {
        return this.W;
    }

    @DexIgnore
    public final List<pc6<Integer, lc6<Integer, Float>, String>> getListTimeZoneChange() {
        return this.V;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.U;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void setDayInMinuteWithTimeZone(int i) {
        this.W = i;
    }

    @DexIgnore
    public final void setListTimeZoneChange(List<pc6<Integer, lc6<Integer, Float>, String>> list) {
        wg6.b(list, ServerSetting.VALUE);
        this.V = list;
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        wg6.b(str, "<set-?>");
        this.U = str;
    }

    @DexIgnore
    public void a(String str, int i) {
        wg6.b(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " valueColor=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1222874814) {
            if (hashCode == -685682508 && str.equals("lowestHeartRate")) {
                this.v = i;
            }
        } else if (str.equals("maxHeartRate")) {
            this.w = i;
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void e() {
        if (!TextUtils.isEmpty(this.U)) {
            String b = ThemeManager.l.a().b(this.U);
            if (!TextUtils.isEmpty(b)) {
                setBackgroundColor(Color.parseColor(b));
            }
        }
    }

    @DexIgnore
    public final void a(List<jz5> list) {
        wg6.b(list, "heartRatePointList");
        this.S.clear();
        this.S.addAll(list);
        d();
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, List<Float> list2, float f) {
        int i = 0;
        for (T next : list) {
            int i2 = i + 1;
            if (i >= 0) {
                canvas.drawText((String) next, list2.get(i).floatValue(), f, this.Q);
                i = i2;
            } else {
                qd6.c();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void d(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.d(canvas);
        if (this.V.isEmpty()) {
            List d = qd6.d(jm4.a((Context) PortfolioApp.get.instance(), 2131886490), jm4.a((Context) PortfolioApp.get.instance(), 2131886492), jm4.a((Context) PortfolioApp.get.instance(), 2131886491), jm4.a((Context) PortfolioApp.get.instance(), 2131886493), jm4.a((Context) PortfolioApp.get.instance(), 2131886490));
            ArrayList arrayList = new ArrayList();
            int size = d.size();
            float f = (this.H - this.G) / ((float) (size - 1));
            for (int i = 0; i < size; i++) {
                arrayList.add(Float.valueOf(this.G + (((float) i) * f)));
            }
            a(canvas, d, arrayList, this.C + (((((float) canvas.getHeight()) - this.C) + this.L) / ((float) 2)));
            return;
        }
        List<pc6<Integer, lc6<Integer, Float>, String>> list = this.V;
        ArrayList arrayList2 = new ArrayList(rd6.a(list, 10));
        for (pc6 third : list) {
            arrayList2.add((String) third.getThird());
        }
        List d2 = yd6.d(arrayList2);
        List<pc6<Integer, lc6<Integer, Float>, String>> list2 = this.V;
        ArrayList arrayList3 = new ArrayList(rd6.a(list2, 10));
        for (pc6 first : list2) {
            arrayList3.add(Integer.valueOf(((Number) first.getFirst()).intValue()));
        }
        List<Number> d3 = yd6.d(arrayList3);
        ArrayList arrayList4 = new ArrayList();
        for (Number intValue : d3) {
            arrayList4.add(Float.valueOf(this.G + (((float) intValue.intValue()) * this.T)));
        }
        a(canvas, d2, arrayList4, this.C + (((((float) canvas.getHeight()) - this.C) + this.L) / ((float) 2)));
    }
}
