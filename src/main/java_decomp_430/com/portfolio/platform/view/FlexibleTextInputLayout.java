package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.wg6;
import com.fossil.x24;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTextInputLayout extends TextInputLayout {
    @DexIgnore
    public String A0; // = "";
    @DexIgnore
    public String w0; // = "";
    @DexIgnore
    public String x0; // = "";
    @DexIgnore
    public String y0; // = "";
    @DexIgnore
    public String z0; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.LinearLayout, com.portfolio.platform.view.FlexibleTextInputLayout] */
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleTextInputLayout);
            String string = obtainStyledAttributes.getString(3);
            if (string == null) {
                string = "";
            }
            this.w0 = string;
            String string2 = obtainStyledAttributes.getString(4);
            if (string2 == null) {
                string2 = "";
            }
            this.x0 = string2;
            String string3 = obtainStyledAttributes.getString(2);
            if (string3 == null) {
                string3 = "";
            }
            this.y0 = string3;
            String string4 = obtainStyledAttributes.getString(0);
            if (string4 == null) {
                string4 = "";
            }
            this.z0 = string4;
            String string5 = obtainStyledAttributes.getString(1);
            if (string5 == null) {
                string5 = "";
            }
            this.A0 = string5;
            obtainStyledAttributes.recycle();
        }
        if (!TextUtils.isEmpty(this.w0) || !TextUtils.isEmpty(this.x0) || !TextUtils.isEmpty(this.A0)) {
            a(this.w0, this.x0, this.A0);
        }
        setElevation(0.0f);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        String b = ThemeManager.l.a().b(str);
        Typeface c = ThemeManager.l.a().c(str2);
        String b2 = ThemeManager.l.a().b(this.y0);
        String b3 = ThemeManager.l.a().b(this.z0);
        String b4 = ThemeManager.l.a().b(str3);
        if (b != null) {
            setDefaultHintTextColor(ColorStateList.valueOf(Color.parseColor(b)));
        }
        if (b2 != null) {
            setPasswordVisibilityToggleTintList(ColorStateList.valueOf(Color.parseColor(b2)));
        }
        if (c != null) {
            setTypeface(c);
        }
        if (b3 != null) {
            setBoxStrokeColor(Color.parseColor(b3));
        }
        if (b4 != null) {
            setErrorTextColor(ColorStateList.valueOf(Color.parseColor(b4)));
        }
    }
}
