package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import com.fossil.cy5;
import com.fossil.jm4;
import com.fossil.rc6;
import com.fossil.uy5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleEditText extends AppCompatEditText {
    @DexIgnore
    public /* final */ String d; // = "FlexibleEditText";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o; // = FlexibleTextView.t.a();
    @DexIgnore
    public String p; // = "";
    @DexIgnore
    public String q; // = "primaryColor";
    @DexIgnore
    public /* final */ LayerDrawable r; // = ((LayerDrawable) w6.c(getContext(), 2131230870));
    @DexIgnore
    public /* final */ GradientDrawable s;
    @DexIgnore
    public boolean t;

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context) {
        super(context);
        wg6.b(context, "context");
        LayerDrawable layerDrawable = this.r;
        this.s = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131362942) : null);
        this.t = true;
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v0, types: [android.widget.TextView, androidx.appcompat.widget.AppCompatEditText, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void a(AttributeSet attributeSet) {
        this.i = 0;
        CharSequence text = getText();
        if (text != null) {
            CharSequence hint = getHint();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleEditText);
                this.i = obtainStyledAttributes.getInt(7, 0);
                this.j = obtainStyledAttributes.getInt(3, 0);
                this.o = obtainStyledAttributes.getColor(8, FlexibleTextView.t.a());
                String string = obtainStyledAttributes.getString(5);
                String str = "";
                if (string == null) {
                    string = str;
                }
                this.e = string;
                String string2 = obtainStyledAttributes.getString(6);
                if (string2 == null) {
                    string2 = str;
                }
                this.f = string2;
                String string3 = obtainStyledAttributes.getString(4);
                if (string3 == null) {
                    string3 = str;
                }
                this.g = string3;
                String string4 = obtainStyledAttributes.getString(0);
                if (string4 == null) {
                    string4 = str;
                }
                this.p = string4;
                this.t = obtainStyledAttributes.getBoolean(1, true);
                String string5 = obtainStyledAttributes.getString(2);
                if (string5 != null) {
                    str = string5;
                }
                this.h = str;
                obtainStyledAttributes.recycle();
                TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
                int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
                if (resourceId != -1) {
                    text = a(resourceId);
                }
                int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
                if (resourceId2 != -1) {
                    hint = a(resourceId2);
                }
                obtainStyledAttributes2.recycle();
            }
            if (!TextUtils.isEmpty(text)) {
                setText(text);
            }
            if (!TextUtils.isEmpty(hint)) {
                wg6.a((Object) hint, "hint");
                setHint(a(hint, this.j));
            }
            if (this.o != FlexibleTextView.t.a()) {
                uy5.a((TextView) this, this.o);
            }
            if (!TextUtils.isEmpty(this.e) || !TextUtils.isEmpty(this.f) || !TextUtils.isEmpty(this.g) || !TextUtils.isEmpty(this.h) || !TextUtils.isEmpty(this.p)) {
                a(this.e, this.f, this.g, this.h, this.p);
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final LayerDrawable getShape() {
        return this.r;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public boolean onKeyPreIme(int i2, KeyEvent keyEvent) {
        wg6.b(keyEvent, Constants.EVENT);
        if (keyEvent.getKeyCode() != 4) {
            return false;
        }
        clearFocus();
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [androidx.appcompat.widget.AppCompatEditText, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void onSelectionChanged(int i2, int i3) {
        Editable text;
        if (this.t || (text = getText()) == null || (i2 == text.length() && i3 == text.length())) {
            FlexibleEditText.super.onSelectionChanged(i2, i3);
        } else {
            setSelection(text.length(), text.length());
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        wg6.b(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        FlexibleEditText.super.setText(a(charSequence), bufferType);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        LayerDrawable layerDrawable = this.r;
        this.s = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131362942) : null);
        this.t = true;
        a(attributeSet);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        LayerDrawable layerDrawable = this.r;
        this.s = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131362942) : null);
        this.t = true;
        a(attributeSet);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void a(String str, String str2, String str3, String str4, String str5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = this.d;
        local.d(str6, "setStyle colorNameStyle=" + str + " fontNameStyle=" + str2);
        String b = ThemeManager.l.a().b(str);
        Typeface c = ThemeManager.l.a().c(str2);
        String b2 = ThemeManager.l.a().b(str3);
        String b3 = ThemeManager.l.a().b(str4);
        String b4 = ThemeManager.l.a().b(str5);
        if (b != null) {
            setTextColor(Color.parseColor(b));
        }
        if (c != null) {
            setTypeface(c);
        }
        if (b2 != null) {
            setBackgroundColor(Color.parseColor(b2));
        }
        if (b3 != null) {
            setHintTextColor(Color.parseColor(b3));
        }
        if (b4 != null) {
            GradientDrawable gradientDrawable = this.s;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(b4));
            }
            setBackground(this.r);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void a(boolean z) {
        String str;
        String b;
        if (z) {
            str = this.q;
        } else {
            str = this.p;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.d;
        local.d(str2, "switchBorderColor " + str);
        if (!TextUtils.isEmpty(str) && (b = ThemeManager.l.a().b(str)) != null) {
            GradientDrawable gradientDrawable = this.s;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(b));
            }
            setBackground(this.r);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a() {
        Object systemService = PortfolioApp.get.instance().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).showSoftInput(this, 0);
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.i);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i2) {
        if (i2 == 1) {
            return cy5.a(charSequence);
        }
        if (i2 == 2) {
            return cy5.b(charSequence);
        }
        if (i2 == 3) {
            return cy5.d(charSequence);
        }
        if (i2 == 4) {
            return cy5.e(charSequence);
        }
        if (i2 != 5) {
            return charSequence;
        }
        return cy5.c(charSequence);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i2) {
        String a = jm4.a((Context) PortfolioApp.get.instance(), i2);
        wg6.a((Object) a, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a;
    }
}
