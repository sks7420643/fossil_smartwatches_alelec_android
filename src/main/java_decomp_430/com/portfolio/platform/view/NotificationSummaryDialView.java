package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.ft;
import com.fossil.gy5;
import com.fossil.gz;
import com.fossil.jj4;
import com.fossil.mj4;
import com.fossil.mt;
import com.fossil.mz;
import com.fossil.nz;
import com.fossil.oj4;
import com.fossil.pr;
import com.fossil.qa;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.w6;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xj4;
import com.fossil.yz;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSummaryDialView extends ViewGroup {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Paint f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public ViewGroup.LayoutParams p;
    @DexIgnore
    public SparseArray<Bitmap> q; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler r; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public Bitmap s;
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> t; // = new SparseArray<>();
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> u; // = new SparseArray<>();
    @DexIgnore
    public b v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public c(NotificationSummaryDialView notificationSummaryDialView, ArrayList arrayList) {
            this.a = notificationSummaryDialView;
            this.b = arrayList;
        }

        @DexIgnore
        public final void run() {
            this.a.a();
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) it.next();
                if (appCompatImageView != null) {
                    try {
                        this.a.addView(appCompatImageView);
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String e2 = NotificationSummaryDialView.w;
                        local.d(e2, "onLoadBitmapAsyncComplete exception " + e);
                    }
                }
            }
            this.a.requestLayout();
            this.a.u.clear();
            if (this.a.t.size() > 0) {
                FLogger.INSTANCE.getLocal().d(NotificationSummaryDialView.w, "onLoadBitmapAsyncComplete pendingList exists, start set again");
                NotificationSummaryDialView notificationSummaryDialView = this.a;
                SparseArray clone = notificationSummaryDialView.t.clone();
                wg6.a((Object) clone, "mPendingData.clone()");
                notificationSummaryDialView.setDataAsync(clone);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView a;

        @DexIgnore
        public d(NotificationSummaryDialView notificationSummaryDialView) {
            this.a = notificationSummaryDialView;
        }

        @DexIgnore
        public final void onClick(View view) {
            b c = this.a.v;
            if (c != null) {
                Object tag = view.getTag(123456789);
                if (tag != null) {
                    c.a(((Integer) tag).intValue());
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Int");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements mz<Bitmap> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ AppCompatImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList d;

        @DexIgnore
        public e(NotificationSummaryDialView notificationSummaryDialView, int i, AppCompatImageView appCompatImageView, ArrayList arrayList) {
            this.a = notificationSummaryDialView;
            this.b = i;
            this.c = appCompatImageView;
            this.d = arrayList;
        }

        @DexIgnore
        public boolean a(mt mtVar, Object obj, yz<Bitmap> yzVar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = NotificationSummaryDialView.w;
            local.d(e, "setDataAsync onLoadFailed " + obj);
            if (this.a.q.get(this.b) != null) {
                this.c.setImageBitmap((Bitmap) this.a.q.get(this.b));
            }
            this.d.add(this.c);
            this.a.a((ArrayList<AppCompatImageView>) this.d);
            return false;
        }

        @DexIgnore
        public boolean a(Bitmap bitmap, Object obj, yz<Bitmap> yzVar, pr prVar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = NotificationSummaryDialView.w;
            local.d(e, "setDataAsync onResourceReady " + obj);
            if (bitmap != null) {
                this.a.q.put(this.b, bitmap);
                this.c.setImageBitmap(bitmap);
            }
            this.d.add(this.c);
            this.a.a((ArrayList<AppCompatImageView>) this.d);
            return false;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = NotificationSummaryDialView.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationSummaryDialView::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context) {
        super(context);
        wg6.b(context, "context");
        d();
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        wg6.b(canvas, "canvas");
        FLogger.INSTANCE.getLocal().d(w, "dispatchDraw");
        a(canvas);
        super.dispatchDraw(canvas);
        c();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.r.removeCallbacksAndMessages((Object) null);
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        this.o = (Math.min((i4 - i2) - 100, i5 - i3) / 2) - (this.j / 2);
        destroyDrawingCache();
        buildDrawingCache();
        int i6 = 0;
        while (i6 < childCount) {
            AppCompatImageView childAt = getChildAt(i6);
            if (childAt != null) {
                AppCompatImageView appCompatImageView = childAt;
                appCompatImageView.setLayoutParams(this.p);
                Object tag = appCompatImageView.getTag(123456789);
                if (tag != null) {
                    a(appCompatImageView, ((Integer) tag).intValue());
                    i6++;
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.Int");
                }
            } else {
                throw new rc6("null cannot be cast to non-null type androidx.appcompat.widget.AppCompatImageView");
            }
        }
    }

    @DexIgnore
    public final void setDataAsync(SparseArray<List<BaseFeatureModel>> sparseArray) {
        wg6.b(sparseArray, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "setDataAsync data size " + sparseArray.size());
        int size = this.u.size();
        this.t.clear();
        if (size > 0) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            wg6.a((Object) clone, "data.clone()");
            this.t = clone;
            FLogger.INSTANCE.getLocal().d(w, "setDataAsync executing in progress, put new list in pending list");
            return;
        }
        SparseArray<List<BaseFeatureModel>> clone2 = sparseArray.clone();
        wg6.a((Object) clone2, "data.clone()");
        this.u = clone2;
        ArrayList arrayList = new ArrayList();
        mj4 a2 = jj4.a(getContext());
        wg6.a((Object) a2, "GlideApp.with(context)");
        TypedValue typedValue = new TypedValue();
        ColorStateList valueOf = ColorStateList.valueOf(this.b);
        wg6.a((Object) valueOf, "ColorStateList.valueOf(mDialItemTintColor)");
        nz a3 = new nz().a(new xj4()).a(ft.a).a(true);
        wg6.a((Object) a3, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        nz nzVar = a3;
        Context context = getContext();
        wg6.a((Object) context, "context");
        context.getTheme().resolveAttribute(16843534, typedValue, true);
        for (int i2 = 1; i2 <= 12; i2++) {
            AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
            appCompatImageView.setLayoutParams(this.p);
            appCompatImageView.setTag(123456789, Integer.valueOf(i2));
            appCompatImageView.setOnClickListener(new d(this));
            if (sparseArray.get(i2) != null) {
                wg6.a((Object) a2.e().a((Object) new oj4(sparseArray.get(i2))).b(new e(this, i2, appCompatImageView, arrayList)).a((gz) nzVar).a(ft.a).P(), "glideApp.asBitmap()\n    \u2026                .submit()");
            } else {
                appCompatImageView.setImageResource(this.a);
                int i3 = this.d;
                appCompatImageView.setPadding(i3, i3, i3, i3);
                appCompatImageView.setElevation(50.0f);
                int i4 = this.c;
                if (i4 != -1) {
                    appCompatImageView.setBackgroundResource(i4);
                } else {
                    appCompatImageView.setBackgroundResource(typedValue.resourceId);
                }
                qa.a(appCompatImageView, valueOf);
                arrayList.add(appCompatImageView);
            }
        }
        a((ArrayList<AppCompatImageView>) arrayList);
    }

    @DexIgnore
    public final void setOnItemClickListener(b bVar) {
        wg6.b(bVar, "listener");
        this.v = bVar;
    }

    @DexIgnore
    public final void b() {
        setDataAsync(new SparseArray());
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d(w, "initParams");
        this.g = getMeasuredWidth();
        this.h = getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = this.p;
        if (layoutParams != null) {
            layoutParams.width = this.j;
        }
        ViewGroup.LayoutParams layoutParams2 = this.p;
        if (layoutParams2 != null) {
            layoutParams2.height = this.j;
        }
        this.i = Math.min(this.g, this.h) / 2;
        this.j = (int) (((float) this.i) / 3.5f);
    }

    @DexIgnore
    public final void d() {
        this.f = new Paint(1);
        Paint paint = this.f;
        if (paint != null) {
            paint.setStyle(Paint.Style.FILL);
            this.p = new ViewGroup.LayoutParams(-1, -1);
            return;
        }
        wg6.d("mBitmapPaint");
        throw null;
    }

    @DexIgnore
    public final void a(ArrayList<AppCompatImageView> arrayList) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "onLoadBitmapAsyncComplete size " + arrayList.size());
        if (arrayList.size() == 12) {
            this.r.post(new c(this, arrayList));
        }
    }

    @DexIgnore
    public final void a() {
        removeAllViews();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.NotificationSummaryDialView);
        this.a = obtainStyledAttributes.getResourceId(3, 2131231153);
        this.b = obtainStyledAttributes.getColor(4, w6.a(context, 2131100008));
        this.c = obtainStyledAttributes.getResourceId(1, -1);
        this.d = (int) obtainStyledAttributes.getDimension(2, gy5.a(15.0f));
        this.e = obtainStyledAttributes.getResourceId(0, 2131231114);
        this.s = BitmapFactory.decodeResource(getResources(), this.e);
        obtainStyledAttributes.recycle();
        d();
        b();
    }

    @DexIgnore
    public final void a(AppCompatImageView appCompatImageView, int i2) {
        double d2 = ((double) (i2 - 3)) * 0.5235987755982988d;
        int i3 = this.j;
        int width = (int) (((double) ((getWidth() / 2) - (i3 / 2))) + (((double) this.o) * Math.cos(d2)));
        int height = (int) (((double) ((getHeight() / 2) - (i3 / 2))) + (((double) this.o) * Math.sin(d2)));
        appCompatImageView.layout(width, height, width + i3, i3 + height);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        FLogger.INSTANCE.getLocal().d(w, "initClockImage");
        int i2 = this.j;
        float f2 = ((float) i2) * 1.5f;
        float f3 = ((float) this.g) - (((float) i2) * 1.5f);
        float f4 = f3 - f2;
        float f5 = (((float) this.h) - f4) / ((float) 2);
        float f6 = f4 + f5;
        Bitmap bitmap = this.s;
        if (bitmap != null) {
            Rect rect = new Rect((int) f2, (int) f5, (int) f3, (int) f6);
            Paint paint = this.f;
            if (paint != null) {
                canvas.drawBitmap(bitmap, (Rect) null, rect, paint);
            } else {
                wg6.d("mBitmapPaint");
                throw null;
            }
        }
    }
}
