package com.portfolio.platform.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager.widget.ViewPager;
import com.fossil.bl;
import com.fossil.p4;
import com.fossil.q8;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLViewPager extends ViewPager {
    @DexIgnore
    public /* final */ Map<ViewPager.i, d> o0; // = new p4(1);
    @DexIgnore
    public DataSetObserver p0;
    @DexIgnore
    public boolean q0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends bl {
        @DexIgnore
        public /* final */ bl b;

        @DexIgnore
        public a(RTLViewPager rTLViewPager, bl blVar) {
            this.b = blVar;
        }

        @DexIgnore
        public int a() {
            return this.b.a();
        }

        @DexIgnore
        public float b(int i) {
            return this.b.b(i);
        }

        @DexIgnore
        public bl c() {
            return this.b;
        }

        @DexIgnore
        public boolean a(View view, Object obj) {
            return this.b.a(view, obj);
        }

        @DexIgnore
        public void b(ViewGroup viewGroup, int i, Object obj) {
            this.b.b(viewGroup, i, obj);
        }

        @DexIgnore
        public void c(DataSetObserver dataSetObserver) {
            this.b.c(dataSetObserver);
        }

        @DexIgnore
        public CharSequence a(int i) {
            return this.b.a(i);
        }

        @DexIgnore
        public Parcelable b() {
            return this.b.b();
        }

        @DexIgnore
        public int a(Object obj) {
            return this.b.a(obj);
        }

        @DexIgnore
        public void b(ViewGroup viewGroup) {
            this.b.b(viewGroup);
        }

        @DexIgnore
        public Object a(ViewGroup viewGroup, int i) {
            return this.b.a(viewGroup, i);
        }

        @DexIgnore
        public void a(ViewGroup viewGroup, int i, Object obj) {
            this.b.a(viewGroup, i, obj);
        }

        @DexIgnore
        public void a(DataSetObserver dataSetObserver) {
            this.b.a(dataSetObserver);
        }

        @DexIgnore
        public void a(Parcelable parcelable, ClassLoader classLoader) {
            this.b.a(parcelable, classLoader);
        }

        @DexIgnore
        public void a(ViewGroup viewGroup) {
            this.b.a(viewGroup);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends DataSetObserver {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void onChanged() {
            super.onChanged();
            this.a.d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends a {
        @DexIgnore
        public int c;

        @DexIgnore
        public c(bl blVar) {
            super(RTLViewPager.this, blVar);
            this.c = blVar.a();
        }

        @DexIgnore
        public CharSequence a(int i) {
            return super.a(c(i));
        }

        @DexIgnore
        public float b(int i) {
            return super.b(c(i));
        }

        @DexIgnore
        public final int c(int i) {
            return (a() - i) - 1;
        }

        @DexIgnore
        public void d() {
            int a = a();
            int i = this.c;
            if (a != i) {
                RTLViewPager.this.setCurrentItemWithoutNotification(Math.max(0, i - 1));
                this.c = a;
            }
        }

        @DexIgnore
        public int a(Object obj) {
            int a = super.a(obj);
            return a < 0 ? a : c(a);
        }

        @DexIgnore
        public void b(ViewGroup viewGroup, int i, Object obj) {
            super.b(viewGroup, (this.c - i) - 1, obj);
        }

        @DexIgnore
        public Object a(ViewGroup viewGroup, int i) {
            return super.a(viewGroup, c(i));
        }

        @DexIgnore
        public void a(ViewGroup viewGroup, int i, Object obj) {
            super.a(viewGroup, c(i), obj);
        }
    }

    @DexIgnore
    public RTLViewPager(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(bl blVar) {
        if ((blVar instanceof c) && this.p0 == null) {
            c cVar = (c) blVar;
            this.p0 = new b(cVar);
            blVar.a(this.p0);
            cVar.d();
        }
    }

    @DexIgnore
    public void b(float f) {
        if (!n()) {
            f = -f;
        }
        RTLViewPager.super.b(f);
    }

    @DexIgnore
    public final int g(int i) {
        if (i < 0 || !n()) {
            return i;
        }
        if (getAdapter() == null) {
            return 0;
        }
        return (getAdapter().a() - i) - 1;
    }

    @DexIgnore
    public bl getAdapter() {
        c adapter = RTLViewPager.super.getAdapter();
        return adapter instanceof c ? adapter.c() : adapter;
    }

    @DexIgnore
    public int getCurrentItem() {
        return g(RTLViewPager.super.getCurrentItem());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.RTLViewPager, android.view.ViewGroup] */
    public final boolean n() {
        return q8.b(getContext().getResources().getConfiguration().locale) == 1;
    }

    @DexIgnore
    public final void o() {
        DataSetObserver dataSetObserver;
        bl adapter = RTLViewPager.super.getAdapter();
        if ((adapter instanceof c) && (dataSetObserver = this.p0) != null) {
            adapter.c(dataSetObserver);
            this.p0 = null;
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        RTLViewPager.super.onAttachedToWindow();
        a(RTLViewPager.super.getAdapter());
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        o();
        RTLViewPager.super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        e eVar = (e) parcelable;
        RTLViewPager.super.onRestoreInstanceState(eVar.a);
        if (eVar.c != n()) {
            a(eVar.b, false);
        }
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        return new e(RTLViewPager.super.onSaveInstanceState(), getCurrentItem(), n());
    }

    @DexIgnore
    public void setAdapter(bl blVar) {
        o();
        boolean z = blVar != null && n();
        if (z) {
            bl cVar = new c(blVar);
            a(cVar);
            blVar = cVar;
        }
        RTLViewPager.super.setAdapter(blVar);
        if (z) {
            setCurrentItemWithoutNotification(0);
        }
    }

    @DexIgnore
    public void setCurrentItem(int i) {
        RTLViewPager.super.setCurrentItem(g(i));
    }

    @DexIgnore
    public void setCurrentItemWithoutNotification(int i) {
        this.q0 = true;
        a(i, false);
        this.q0 = false;
    }

    @DexIgnore
    public void b(ViewPager.i iVar) {
        if (n()) {
            iVar = (ViewPager.i) this.o0.remove(iVar);
        }
        RTLViewPager.super.b(iVar);
    }

    @DexIgnore
    public RTLViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ViewPager.i {
        @DexIgnore
        public /* final */ ViewPager.i a;
        @DexIgnore
        public int b; // = -1;

        @DexIgnore
        public d(ViewPager.i iVar) {
            this.a = iVar;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            if (!RTLViewPager.this.q0) {
                int i3 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
                if (i3 == 0 && i2 == 0) {
                    this.b = c(i);
                } else {
                    this.b = c(i + 1);
                }
                ViewPager.i iVar = this.a;
                int i4 = this.b;
                if (i3 > 0) {
                    f = 1.0f - f;
                }
                iVar.a(i4, f, i2);
            }
        }

        @DexIgnore
        public void b(int i) {
            if (!RTLViewPager.this.q0) {
                this.a.b(c(i));
            }
        }

        @DexIgnore
        public final int c(int i) {
            bl adapter = RTLViewPager.this.getAdapter();
            return adapter == null ? i : (adapter.a() - i) - 1;
        }

        @DexIgnore
        public void a(int i) {
            if (!RTLViewPager.this.q0) {
                this.a.a(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.ClassLoaderCreator<e> CREATOR; // = new a();
        @DexIgnore
        public /* final */ Parcelable a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.ClassLoaderCreator<e> {
            @DexIgnore
            public e[] newArray(int i) {
                return new e[i];
            }

            @DexIgnore
            public e createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new e(parcel, classLoader);
            }

            @DexIgnore
            public e createFromParcel(Parcel parcel) {
                return new e(parcel, (ClassLoader) null);
            }
        }

        @DexIgnore
        public e(Parcelable parcelable, int i, boolean z) {
            this.a = parcelable;
            this.b = i;
            this.c = z;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.a, i);
            parcel.writeInt(this.b);
            parcel.writeByte(this.c ? (byte) 1 : 0);
        }

        @DexIgnore
        public e(Parcel parcel, ClassLoader classLoader) {
            this.a = parcel.readParcelable(classLoader == null ? e.class.getClassLoader() : classLoader);
            this.b = parcel.readInt();
            this.c = parcel.readByte() != 0;
        }
    }

    @DexIgnore
    public void a(int i, boolean z) {
        RTLViewPager.super.a(g(i), z);
    }

    @DexIgnore
    public void a(ViewPager.i iVar) {
        if (n()) {
            ViewPager.i dVar = new d(iVar);
            this.o0.put(iVar, dVar);
            iVar = dVar;
        }
        RTLViewPager.super.a(iVar);
    }
}
