package com.portfolio.platform.view.ruler;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xz5;
import com.fossil.yz5;
import com.fossil.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RulerValuePicker extends FrameLayout implements xz5.b {
    @DexIgnore
    public View a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public View c;
    @DexIgnore
    public RulerView d;
    @DexIgnore
    public xz5 e;
    @DexIgnore
    public yz5 f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Path h;
    @DexIgnore
    public a i;
    @DexIgnore
    public TextView j;
    @DexIgnore
    public float o; // = 0.6f;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public zh4 q; // = zh4.METRIC;

    @DexIgnore
    public interface a extends Serializable {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a((qg6) null);
        @DexIgnore
        public int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<b> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
                this();
            }

            @DexIgnore
            public b createFromParcel(Parcel parcel) {
                wg6.b(parcel, "in");
                return new b(parcel);
            }

            @DexIgnore
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcelable parcelable) {
            super(parcelable);
            wg6.b(parcelable, "superState");
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            wg6.b(parcel, "out");
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcel parcel) {
            super(parcel);
            wg6.b(parcel, "in");
            this.a = parcel.readInt();
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RulerValuePicker a;

        @DexIgnore
        public c(RulerValuePicker rulerValuePicker) {
            this.a = rulerValuePicker;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            wg6.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            wg6.b(animator, "animation");
            this.a.setMIsChangingValue$app_fossilRelease(false);
            if (this.a.getMListener$app_fossilRelease() != null) {
                yz5 mListener$app_fossilRelease = this.a.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.a(false);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            wg6.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            wg6.b(animator, "animation");
            if (this.a.getMListener$app_fossilRelease() != null) {
                yz5 mListener$app_fossilRelease = this.a.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.a(true);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerValuePicker(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        this.j = new TextView(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 49;
        TextView textView = this.j;
        if (textView != null) {
            textView.setLayoutParams(layoutParams);
            TextView textView2 = this.j;
            if (textView2 != null) {
                textView2.setGravity(17);
                TextView textView3 = this.j;
                if (textView3 != null) {
                    textView3.setBackground(getBackground());
                    b();
                    if (attributeSet != null) {
                        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.RulerValuePicker);
                        try {
                            String str6 = "";
                            if (obtainStyledAttributes.hasValue(7)) {
                                str = obtainStyledAttributes.getString(7);
                                if (str == null) {
                                    str = "primaryText";
                                }
                                TextView textView4 = this.j;
                                if (textView4 != null) {
                                    textView4.setTextColor(this.p);
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                str = str6;
                            }
                            if (obtainStyledAttributes.hasValue(10)) {
                                String string = obtainStyledAttributes.getString(10);
                                if (string == null) {
                                    string = "primaryText";
                                }
                                str2 = string;
                            } else {
                                str2 = str6;
                            }
                            if (obtainStyledAttributes.hasValue(0)) {
                                String string2 = obtainStyledAttributes.getString(0);
                                if (string2 == null) {
                                    string2 = Explore.COLUMN_BACKGROUND;
                                }
                                str3 = string2;
                            } else {
                                str3 = str6;
                            }
                            if (obtainStyledAttributes.hasValue(13)) {
                                String string3 = obtainStyledAttributes.getString(13);
                                if (string3 == null) {
                                    string3 = Explore.COLUMN_BACKGROUND;
                                }
                                str4 = string3;
                            } else {
                                str4 = str6;
                            }
                            if (obtainStyledAttributes.hasValue(11)) {
                                setTextSize(obtainStyledAttributes.getDimensionPixelSize(11, 14));
                            }
                            if (obtainStyledAttributes.hasValue(9)) {
                                setSelectedTextSize((int) obtainStyledAttributes.getDimension(9, 14.0f));
                            }
                            if (obtainStyledAttributes.hasValue(12)) {
                                setTextStyle(obtainStyledAttributes.getInt(12, 0));
                            }
                            if (obtainStyledAttributes.hasValue(1)) {
                                String string4 = obtainStyledAttributes.getString(1);
                                if (string4 == null) {
                                    string4 = "secondaryText";
                                }
                                str5 = string4;
                            } else {
                                str5 = str6;
                            }
                            if (obtainStyledAttributes.hasValue(3)) {
                                setIndicatorWidth(obtainStyledAttributes.getDimensionPixelSize(3, 4));
                            }
                            if (obtainStyledAttributes.hasValue(2)) {
                                setIndicatorIntervalDistance(obtainStyledAttributes.getDimensionPixelSize(2, 4));
                            }
                            if (obtainStyledAttributes.hasValue(4) || obtainStyledAttributes.hasValue(14)) {
                                this.o = obtainStyledAttributes.getFraction(4, 1, 1, 0.6f);
                                a(this.o, obtainStyledAttributes.getFraction(14, 1, 1, 0.4f));
                            }
                            if (obtainStyledAttributes.hasValue(6) || obtainStyledAttributes.hasValue(5)) {
                                a(obtainStyledAttributes.getInteger(6, 0), obtainStyledAttributes.getInteger(5, 100));
                            }
                            if (obtainStyledAttributes.hasValue(8)) {
                                String string5 = obtainStyledAttributes.getString(8);
                                if (string5 == null) {
                                    string5 = "goalNumber";
                                }
                                str6 = string5;
                            }
                            a(str2, str6, str, str5, str3, str4);
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.d("RulerValuePicker", "init - e=" + e2);
                        } catch (Throwable th) {
                            obtainStyledAttributes.recycle();
                            throw th;
                        }
                        obtainStyledAttributes.recycle();
                    }
                    this.g = new Paint(1);
                    d();
                    this.h = new Path();
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void b() {
        this.e = new xz5(getContext(), this);
        xz5 xz5 = this.e;
        if (xz5 != null) {
            xz5.setHorizontalScrollBarEnabled(false);
            LinearLayout linearLayout = new LinearLayout(getContext());
            this.a = new View(getContext());
            linearLayout.addView(this.a);
            Context context = getContext();
            wg6.a((Object) context, "context");
            this.d = new RulerView(context);
            linearLayout.addView(this.d);
            this.c = new View(getContext());
            linearLayout.addView(this.c);
            xz5 xz52 = this.e;
            if (xz52 != null) {
                xz52.removeAllViews();
                xz5 xz53 = this.e;
                if (xz53 != null) {
                    xz53.addView(linearLayout);
                    removeAllViews();
                    addView(this.e);
                    addView(this.j);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void c() {
        Path path = this.h;
        if (path != null) {
            path.reset();
            float height = (float) getHeight();
            path.moveTo((float) (getWidth() / 2), height);
            path.lineTo((float) (getWidth() / 2), height * (((float) 1) - this.o));
        }
    }

    @DexIgnore
    public final void d() {
        Paint paint = this.g;
        if (paint != null) {
            paint.setColor(this.p);
            paint.setStrokeWidth(5.0f);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
        }
    }

    @DexIgnore
    public final int getCurrentValue() {
        xz5 xz5 = this.e;
        if (xz5 == null || this.d == null) {
            return 0;
        }
        if (xz5 != null) {
            int scrollX = xz5.getScrollX();
            RulerView rulerView = this.d;
            if (rulerView != null) {
                int indicatorIntervalWidth = scrollX / rulerView.getIndicatorIntervalWidth();
                RulerView rulerView2 = this.d;
                if (rulerView2 != null) {
                    int minValue = rulerView2.getMinValue() + indicatorIntervalWidth;
                    RulerView rulerView3 = this.d;
                    if (rulerView3 == null) {
                        wg6.a();
                        throw null;
                    } else if (minValue > rulerView3.getMaxValue()) {
                        RulerView rulerView4 = this.d;
                        if (rulerView4 != null) {
                            return rulerView4.getMaxValue();
                        }
                        wg6.a();
                        throw null;
                    } else {
                        RulerView rulerView5 = this.d;
                        if (rulerView5 != null) {
                            if (minValue < rulerView5.getMinValue()) {
                                RulerView rulerView6 = this.d;
                                if (rulerView6 != null) {
                                    minValue = rulerView6.getMinValue();
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            return minValue;
                        }
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final int getIndicatorColor() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getIndicatorColor();
        }
        return 0;
    }

    @DexIgnore
    public final int getIndicatorIntervalWidth() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getIndicatorIntervalWidth();
        }
        return 0;
    }

    @DexIgnore
    public final float getIndicatorWidth() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getIndicatorWidth();
        }
        return 0.0f;
    }

    @DexIgnore
    public final float getLongIndicatorHeightRatio() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getLongIndicatorHeightRatio();
        }
        return 0.0f;
    }

    @DexIgnore
    public final boolean getMIsChangingValue$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    public final yz5 getMListener$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final int getMaxValue() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getMaxValue();
        }
        return 0;
    }

    @DexIgnore
    public final int getMinValue() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getMinValue();
        }
        return 0;
    }

    @DexIgnore
    public final int getNotchColor() {
        return this.p;
    }

    @DexIgnore
    public final float getShortIndicatorHeightRatio() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getShortIndicatorHeightRatio();
        }
        return 0.0f;
    }

    @DexIgnore
    public final int getTextColor() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getTextColor();
        }
        return 0;
    }

    @DexIgnore
    public final float getTextSize() {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            return rulerView.getTextSize();
        }
        return 0.0f;
    }

    @DexIgnore
    public final zh4 getUnit() {
        return this.q;
    }

    @DexIgnore
    public void onDrawForeground(Canvas canvas) {
        Path path;
        wg6.b(canvas, "canvas");
        super.onDrawForeground(canvas);
        Paint paint = this.g;
        if (paint != null && (path = this.h) != null) {
            if (path == null) {
                wg6.a();
                throw null;
            } else if (paint != null) {
                canvas.drawPath(path, paint);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (z) {
            int width = getWidth();
            View view = this.a;
            ViewGroup.LayoutParams layoutParams = null;
            ViewGroup.LayoutParams layoutParams2 = view != null ? view.getLayoutParams() : null;
            if (layoutParams2 != null) {
                layoutParams2.width = width / 2;
            }
            View view2 = this.a;
            if (view2 != null) {
                view2.setLayoutParams(layoutParams2);
            }
            View view3 = this.c;
            if (view3 != null) {
                layoutParams = view3.getLayoutParams();
            }
            if (layoutParams != null) {
                layoutParams.width = width / 2;
            }
            View view4 = this.c;
            if (view4 != null) {
                view4.setLayoutParams(layoutParams);
            }
            c();
            invalidate();
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        wg6.b(parcelable, Constants.STATE);
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        d(bVar.a());
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        b bVar = new b(onSaveInstanceState);
        bVar.a(getCurrentValue());
        return bVar;
    }

    @DexIgnore
    public void onScrollChanged() {
        FLogger.INSTANCE.getLocal().d("RulerValuePicker", "onScrollChanged");
        int currentValue = getCurrentValue();
        TextView textView = this.j;
        if (textView != null) {
            textView.setText(a(currentValue));
            yz5 yz5 = this.f;
            if (yz5 == null) {
                return;
            }
            if (yz5 != null) {
                yz5.b(currentValue);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setFormatter(a aVar) {
        wg6.b(aVar, "formatter");
        if (!wg6.a((Object) aVar, (Object) this.i)) {
            this.i = aVar;
            RulerView rulerView = this.d;
            if (rulerView != null) {
                a aVar2 = this.i;
                if (aVar2 != null) {
                    rulerView.setFormatter(aVar2);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void setIndicatorColor(int i2) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.setIndicatorColor(i2);
        }
    }

    @DexIgnore
    public final void setIndicatorColorRes(int i2) {
        setIndicatorColor(w6.a(getContext(), i2));
    }

    @DexIgnore
    public final void setIndicatorIntervalDistance(int i2) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.setIndicatorIntervalDistance(i2);
        }
    }

    @DexIgnore
    public final void setIndicatorWidth(int i2) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.setIndicatorWidth(i2);
        }
    }

    @DexIgnore
    public final void setIndicatorWidthRes(int i2) {
        Context context = getContext();
        wg6.a((Object) context, "context");
        setIndicatorWidth(context.getResources().getDimensionPixelSize(i2));
    }

    @DexIgnore
    public final void setMIsChangingValue$app_fossilRelease(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(yz5 yz5) {
        this.f = yz5;
    }

    @DexIgnore
    public final void setNotchColor(int i2) {
        this.p = i2;
        TextView textView = this.j;
        if (textView != null) {
            textView.setTextColor(i2);
            d();
            invalidate();
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void setNotchColorRes(int i2) {
        setNotchColor(w6.a(getContext(), i2));
    }

    @DexIgnore
    public final void setSelectedTextSize(int i2) {
        TextView textView = this.j;
        if (textView != null) {
            textView.setTextSize(0, (float) i2);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setTextColor(int i2) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.setTextColor(i2);
        }
    }

    @DexIgnore
    public final void setTextColorRes(int i2) {
        setTextColor(w6.a(getContext(), i2));
    }

    @DexIgnore
    public final void setTextSize(int i2) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.setTextSize(i2);
        }
    }

    @DexIgnore
    public final void setTextStyle(int i2) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.setTextStyle(i2);
        }
        TextView textView = this.j;
        if (textView == null) {
            wg6.a();
            throw null;
        } else if (textView != null) {
            textView.setTypeface(Typeface.create(textView.getTypeface(), i2));
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setTextTypeface(Typeface typeface) {
        if (typeface != null) {
            RulerView rulerView = this.d;
            if (rulerView != null) {
                rulerView.setTextTypeface(typeface);
            }
            TextView textView = this.j;
            if (textView != null) {
                textView.setTypeface(typeface);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setUnit(zh4 zh4) {
        wg6.b(zh4, "<set-?>");
        this.q = zh4;
    }

    @DexIgnore
    public final void setValuePickerListener(yz5 yz5) {
        this.f = yz5;
    }

    @DexIgnore
    public final void d(int i2) {
        RulerView rulerView = this.d;
        int minValue = rulerView != null ? rulerView.getMinValue() : 0;
        RulerView rulerView2 = this.d;
        int maxValue = rulerView2 != null ? rulerView2.getMaxValue() : 0;
        int i3 = maxValue - minValue;
        int i4 = i2 < minValue ? 0 : i2 > maxValue ? i3 : i2 - minValue;
        xz5 xz5 = this.e;
        int[] iArr = new int[2];
        iArr[0] = i3 / 2;
        RulerView rulerView3 = this.d;
        if (rulerView3 != null) {
            iArr[1] = i4 * rulerView3.getIndicatorIntervalWidth();
            ObjectAnimator ofInt = ObjectAnimator.ofInt(xz5, "scrollX", iArr);
            wg6.a((Object) ofInt, "animator");
            ofInt.setDuration(200);
            ofInt.addListener(new c(this));
            ofInt.start();
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerValuePicker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    public final void c(int i2) {
        xz5 xz5 = this.e;
        if (xz5 == null) {
            return;
        }
        if (xz5 != null) {
            int scrollX = xz5.getScrollX() % i2;
            if (scrollX < i2 / 2) {
                xz5 xz52 = this.e;
                if (xz52 != null) {
                    xz52.scrollBy(-scrollX, 0);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                xz5 xz53 = this.e;
                if (xz53 != null) {
                    xz53.scrollBy(i2 - scrollX, 0);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerValuePicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    @TargetApi(21)
    public RulerValuePicker(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        wg6.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    public final String b(int i2) {
        nh6 nh6 = nh6.a;
        Locale locale = Locale.getDefault();
        wg6.a((Object) locale, "Locale.getDefault()");
        Object[] objArr = {Integer.valueOf(i2)};
        String format = String.format(locale, "%d", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
        return format;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4, String str5, String str6) {
        RulerView rulerView;
        TextView textView;
        String b2 = ThemeManager.l.a().b(str);
        Typeface c2 = ThemeManager.l.a().c(str2);
        String b3 = ThemeManager.l.a().b(str4);
        String b4 = ThemeManager.l.a().b(str3);
        String b5 = ThemeManager.l.a().b(str5);
        String b6 = ThemeManager.l.a().b(str6);
        if (!(b5 == null || (textView = this.j) == null)) {
            textView.setBackgroundColor(Color.parseColor(b5));
        }
        if (!(b6 == null || (rulerView = this.d) == null)) {
            rulerView.setBackgroundColor(Color.parseColor(b6));
        }
        if (b2 != null) {
            setTextColor(Color.parseColor(b2));
        }
        if (c2 != null) {
            setTextTypeface(c2);
        }
        if (b3 != null) {
            setIndicatorColor(Color.parseColor(b3));
        }
        if (b4 != null) {
            setNotchColor(Color.parseColor(b4));
            TextView textView2 = this.j;
            if (textView2 != null) {
                textView2.setTextColor(this.p);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d("RulerValuePicker", "onScrollStopped");
        RulerView rulerView = this.d;
        c(rulerView != null ? rulerView.getIndicatorIntervalWidth() : 0);
        int currentValue = getCurrentValue();
        TextView textView = this.j;
        if (textView != null) {
            textView.setText(a(currentValue));
            yz5 yz5 = this.f;
            if (yz5 == null) {
                return;
            }
            if (yz5 != null) {
                yz5.a(currentValue);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, int i3) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.a(i2, i3);
        }
        d(i2);
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4) {
        if (!this.b) {
            this.b = true;
            RulerView rulerView = this.d;
            if (rulerView != null) {
                rulerView.a(i2, i3);
            }
            d(i4);
        }
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        RulerView rulerView = this.d;
        if (rulerView != null) {
            rulerView.a(f2, f3);
        }
    }

    @DexIgnore
    public final String a(int i2) {
        a aVar = this.i;
        if (aVar == null) {
            return b(i2);
        }
        if (aVar != null) {
            return aVar.format(i2);
        }
        wg6.a();
        throw null;
    }
}
