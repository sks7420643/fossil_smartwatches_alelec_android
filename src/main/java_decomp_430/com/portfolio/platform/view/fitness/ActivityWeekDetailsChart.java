package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.fossil.cd6;
import com.fossil.d7;
import com.fossil.dx5;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.jh6;
import com.fossil.lc6;
import com.fossil.qg6;
import com.fossil.rg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xg6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public /* final */ ArrayList<lc6<Float, Float>> A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public /* final */ String[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements ig6<Integer, List<? extends Integer>, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $bottomTextHeight;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $charactersCenterXList;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hh6 hh6, jh6 jh6) {
            super(2);
            this.$bottomTextHeight = hh6;
            this.$charactersCenterXList = jh6;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
            invoke(((Number) obj).intValue(), (List<Integer>) (List) obj2);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(int i, List<Integer> list) {
            wg6.b(list, "list");
            this.$bottomTextHeight.element = i;
            this.$charactersCenterXList.element = list;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ActivityWeekDetailsChart.class.getSimpleName();
        wg6.a((Object) simpleName, "ActivityWeekDetailsChart::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        String[] stringArray = context.getResources().getStringArray(2130903040);
        wg6.a((Object) stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.d = stringArray;
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = context.getResources().getDimensionPixelSize(2131165384);
        this.v = context.getResources().getDimensionPixelSize(2131165384);
        this.w = context.getResources().getDimensionPixelSize(2131165372);
        this.x = context.getResources().getDimensionPixelSize(2131165372);
        this.y = context.getResources().getDimensionPixelSize(2131165418);
        this.z = 4;
        this.A = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, x24.ActivityWeekDetailsChart, 0, 0));
        }
        int a2 = w6.a(context, 2131099838);
        TypedArray mTypedArray = getMTypedArray();
        this.e = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.f = mTypedArray2 != null ? mTypedArray2.getColor(2, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.g = mTypedArray3 != null ? mTypedArray3.getColor(1, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.h = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.i = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.o = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getResourceId(5, 2131296268) : 2131296268;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxSleepMinutes());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Float f2;
        Iterator<T> it = this.A.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((lc6) t2).getSecond()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((lc6) next).getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        lc6 lc6 = (lc6) t2;
        if (lc6 == null || (f2 = (Float) lc6.getSecond()) == null) {
            return this.B;
        }
        return f2.floatValue();
    }

    @DexIgnore
    private final float getMMaxSleepMinutes() {
        T t2;
        Float f2;
        Iterator<T> it = this.A.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((lc6) t2).getFirst()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((lc6) next).getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        lc6 lc6 = (lc6) t2;
        if (lc6 == null || (f2 = (Float) lc6.getFirst()) == null) {
            return this.C;
        }
        return f2.floatValue();
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, ig6<? super Integer, ? super List<Integer>, cd6> ig6) {
        int length = this.d.length;
        Rect rect = new Rect();
        String str = this.d[0];
        rg6 rg6 = rg6.a;
        Paint paint = this.r;
        wg6.a((Object) str, "sundayCharacter");
        paint.getTextBounds(str, 0, yj6.c(str) > 0 ? yj6.c(str) : 1, rect);
        float measureText = this.r.measureText(str);
        float height = (float) rect.height();
        float f2 = (((float) (i2 - (this.v * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float f3 = (float) 2;
        float height2 = ((float) getHeight()) - (height / f3);
        float f4 = (float) this.v;
        LinkedList linkedList = new LinkedList();
        for (String drawText : this.d) {
            canvas.drawText(drawText, f4, height2, this.r);
            linkedList.add(Integer.valueOf((int) ((measureText / f3) + f4)));
            f4 += measureText + f2;
        }
        ig6.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.y * 2);
            hh6 hh6 = new hh6();
            hh6.element = 0;
            jh6 jh6 = new jh6();
            jh6.element = null;
            a(canvas, width, (ig6<? super Integer, ? super List<Integer>, cd6>) new b(hh6, jh6));
            int height = (getHeight() - hh6.element) - this.w;
            List list = (List) jh6.element;
            if (list != null) {
                a(canvas, (List<Integer>) list, height);
                a(canvas, list, width, height, height - 4);
            }
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.u;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.e);
        float f2 = (float) 4;
        this.p.setStrokeWidth(f2);
        this.r.setColor(this.i);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.o);
        this.r.setTypeface(d7.a(getContext(), this.j));
        this.q.setColor(this.f);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f2);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, 0.0f));
        this.s.setColor(this.g);
        this.s.setStrokeWidth((float) this.x);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.h);
        this.t.setStrokeWidth((float) this.x);
        this.t.setStyle(Paint.Style.FILL);
        this.z = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.A.isEmpty()) && this.A.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                int size = list.size();
                float height = (float) getStartBitmap().getHeight();
                int i5 = 1;
                while (i5 < size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = D;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(((Number) this.A.get(i6).getSecond()).floatValue());
                    sb.append(", current goal: ");
                    sb.append(((Number) this.A.get(i5).getSecond()).floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list2.get(i5).intValue();
                    float f2 = (float) i3;
                    float f3 = (float) i4;
                    float max = Math.max(Math.min((1.0f - (((Number) this.A.get(i5).getSecond()).floatValue() / mChartMax)) * f2, f3), (float) this.z);
                    float intValue2 = (float) list2.get(i6).intValue();
                    float max2 = Math.max(Math.min((1.0f - (((Number) this.A.get(i6).getSecond()).floatValue() / mChartMax)) * f2, f3), (float) this.z);
                    if (max == max2) {
                        path.moveTo(intValue2, max2);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (i2 + this.y), max);
                        } else {
                            path.lineTo(intValue, max);
                        }
                        canvas2.drawPath(path, this.q);
                    } else {
                        path.moveTo(intValue2, max2);
                        path.lineTo(intValue, max2);
                        canvas2.drawPath(path, this.q);
                        path.moveTo(intValue, max2);
                        path.lineTo(intValue, max);
                        canvas2.drawPath(path, this.q);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, max);
                            path.lineTo((float) (i2 + this.y), max);
                            canvas2.drawPath(path, this.q);
                        }
                    }
                    i5++;
                    height = max;
                }
                canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.y), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = D;
        local.d(str, "drawBars: " + list.size());
        if (!this.A.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                lc6<Float, Float> lc6 = this.A.get(i3);
                wg6.a((Object) lc6, "mDataList[index]");
                float floatValue = ((Number) lc6.getFirst()).floatValue() / mChartMax;
                float f2 = (float) i2;
                int i4 = intValue - (this.x / 2);
                dx5.c(canvas, new RectF((float) i4, Math.max(f2 - (floatValue * f2), (float) this.z), (float) (i4 + this.x), f2), floatValue < 1.0f ? this.s : this.t, ((float) this.x) / ((float) 3));
            }
        }
    }
}
