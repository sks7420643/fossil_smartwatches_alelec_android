package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.fossil.cd6;
import com.fossil.d7;
import com.fossil.dx5;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.jh6;
import com.fossil.pz5;
import com.fossil.qg6;
import com.fossil.rg6;
import com.fossil.sh4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xg6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String H;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public sh4 F;
    @DexIgnore
    public /* final */ String[] G;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements ig6<Integer, List<? extends Integer>, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $bottomTextHeight;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $charactersCenterXList;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hh6 hh6, jh6 jh6) {
            super(2);
            this.$bottomTextHeight = hh6;
            this.$charactersCenterXList = jh6;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
            invoke(((Number) obj).intValue(), (List<Integer>) (List) obj2);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(int i, List<Integer> list) {
            wg6.b(list, "list");
            this.$bottomTextHeight.element = i;
            this.$charactersCenterXList.element = list;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = SleepWeekDetailsChart.class.getSimpleName();
        wg6.a((Object) simpleName, "SleepWeekDetailsChart::class.java.simpleName");
        H = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = context.getResources().getDimensionPixelSize(2131165384);
        this.w = context.getResources().getDimensionPixelSize(2131165384);
        this.x = context.getResources().getDimensionPixelSize(2131165372);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = context.getResources().getDimensionPixelSize(2131165418);
        this.A = context.getResources().getDimensionPixelSize(2131165405);
        this.B = new ArrayList<>();
        this.F = sh4.TOTAL_SLEEP;
        String[] stringArray = context.getResources().getStringArray(2130903040);
        wg6.a((Object) stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.G = stringArray;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, x24.SleepWeekDetailsChart, 0, 0));
        }
        int a2 = w6.a(context, 2131099838);
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(1, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(0, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(2, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.i = mTypedArray6 != null ? mTypedArray6.getColor(5, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        this.o = mTypedArray8 != null ? mTypedArray8.getResourceId(6, 2131296268) : 2131296268;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = pz5.a[this.F.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = H;
            local.d(str, "Max of awake: " + this.E);
            i2 = this.E;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = H;
            local2.d(str2, "Max of light: " + this.D);
            i2 = this.D;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = H;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = H;
            local4.d(str4, "Max of restful: " + this.C);
            i2 = this.C;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int sleepGoal = ((SleepDayData) t2).getSleepGoal();
                do {
                    T next = it.next();
                    int sleepGoal2 = ((SleepDayData) next).getSleepGoal();
                    if (sleepGoal < sleepGoal2) {
                        t2 = next;
                        sleepGoal = sleepGoal2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int totalSleepMinutes = ((SleepDayData) t2).getTotalSleepMinutes();
                do {
                    T next = it.next();
                    int totalSleepMinutes2 = ((SleepDayData) next).getTotalSleepMinutes();
                    if (totalSleepMinutes < totalSleepMinutes2) {
                        t2 = next;
                        totalSleepMinutes = totalSleepMinutes2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = pz5.b[this.F.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3) {
        List<Integer> list2 = list;
        int i4 = i2;
        if (!this.B.isEmpty()) {
            float mChartMax = (float) getMChartMax();
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                SleepDayData sleepDayData = this.B.get(i5);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = H;
                local.d(str, "Actual sleep: " + sleepDayData.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int intValue = list2.get(i5).intValue();
                int totalSleepMinutes = sleepDayData.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int dayAwake = sleepDayData.getDayAwake();
                    Canvas canvas2 = canvas;
                    a(canvas2, this.s, intValue, i2, (int) ((((float) dayAwake) / mChartMax) * ((float) i4)), i3, dayAwake, totalSleepMinutes);
                } else if (mSleepMode == 1) {
                    int dayLight = sleepDayData.getDayLight();
                    Canvas canvas3 = canvas;
                    a(canvas3, this.t, intValue, i2, (int) ((((float) dayLight) / mChartMax) * ((float) i4)), i3, dayLight, totalSleepMinutes);
                } else if (mSleepMode != 2) {
                    int intValue2 = list2.get(i5).intValue();
                    int i6 = (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i4));
                    SleepDayData sleepDayData2 = this.B.get(i5);
                    wg6.a((Object) sleepDayData2, "mSleepDayDataList[i]");
                    a(canvas, intValue2, i2, i6, i3, sleepDayData2);
                } else {
                    int dayRestful = sleepDayData.getDayRestful();
                    Canvas canvas4 = canvas;
                    a(canvas4, this.u, intValue, i2, (int) ((((float) dayRestful) / mChartMax) * ((float) i4)), i3, dayRestful, totalSleepMinutes);
                }
            }
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.z * 2);
            hh6 hh6 = new hh6();
            hh6.element = 0;
            jh6 jh6 = new jh6();
            jh6.element = null;
            a(canvas, width, (ig6<? super Integer, ? super List<Integer>, cd6>) new b(hh6, jh6));
            int height = (getHeight() - hh6.element) - this.x;
            List list = (List) jh6.element;
            if (list != null) {
                a(canvas, list, height, this.z * 2);
                a(canvas, (List<Integer>) list, width, height, this.z * 2, height);
            }
            a(canvas, 0, height + 4);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.v;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.d);
        float f2 = (float) 4;
        this.p.setStrokeWidth(f2);
        this.r.setColor(this.i);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.j);
        this.r.setTypeface(d7.a(getContext(), this.o));
        this.q.setColor(this.e);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f2);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, 0.0f));
        this.s.setColor(this.f);
        this.s.setStrokeWidth((float) this.y);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.g);
        this.t.setStrokeWidth((float) this.y);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.h);
        this.u.setStrokeWidth((float) this.y);
        this.u.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = i2 - (this.y / 2);
        if (i8 < 0) {
            i8 = 0;
        }
        RectF rectF = new RectF((float) i8, (float) ((i3 - ((int) ((((float) i6) / ((float) i7)) * ((float) i4)))) + i5), (float) (this.y + i8), (float) i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = H;
        local.d(str, "duration: " + i6 + ", totalMinutes: " + i7 + ", rect top: " + rectF.top + ", rect bot: " + rectF.bottom);
        dx5.c(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, SleepDayData sleepDayData) {
        int i6;
        int i7;
        int i8;
        int i9;
        Paint paint;
        int i10 = i2 - (this.y / 2);
        if (i10 < 0) {
            i10 = 0;
        }
        int i11 = this.y + i10;
        int size = (i4 - i5) - (this.A * (sleepDayData.getSessionList().size() - 1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0));
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i12 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = H;
            local.d(str, "session duration: " + component1);
            i12 += component1;
        }
        int size2 = sleepDayData.getSessionList().size();
        int i13 = i3;
        int i14 = 0;
        while (i14 < size2) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i14);
            wg6.a((Object) sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f2 = (float) durationInMinutes;
            int i15 = (int) ((f2 / ((float) i12)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i16 = i13;
            int i17 = 0;
            while (i17 < size3) {
                int i18 = sleepStates.get(i17).state;
                if (i17 < size3 - 1) {
                    i6 = size;
                    i8 = size2;
                    i7 = i12;
                    i9 = ((int) sleepStates.get(i17 + 1).index) - ((int) sleepStates.get(i17).index);
                } else {
                    i6 = size;
                    i8 = size2;
                    i7 = i12;
                    i9 = durationInMinutes - ((int) sleepStates.get(i17).index);
                }
                int i19 = (int) ((((float) i9) / f2) * ((float) i15));
                if (i19 < 1) {
                    i19 = 1;
                }
                int i20 = i16 - i19;
                int i21 = i10;
                RectF rectF = new RectF((float) i10, (float) i20, (float) i11, (float) i16);
                if (i18 == 1) {
                    paint = this.t;
                } else if (i18 != 2) {
                    paint = this.s;
                } else {
                    paint = this.u;
                }
                canvas.drawRect(rectF, paint);
                i17++;
                size = i6;
                i16 = i20;
                size2 = i8;
                i12 = i7;
                i10 = i21;
            }
            Canvas canvas2 = canvas;
            int i22 = i12;
            i13 = i16 - this.A;
            i14++;
            size = size;
            size2 = size2;
            i10 = i10;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, ig6<? super Integer, ? super List<Integer>, cd6> ig6) {
        int length = this.G.length;
        Rect rect = new Rect();
        String str = this.G[0];
        rg6 rg6 = rg6.a;
        Paint paint = this.r;
        wg6.a((Object) str, "sundayCharacter");
        paint.getTextBounds(str, 0, yj6.c(str) > 0 ? yj6.c(str) : 1, rect);
        float measureText = this.r.measureText(str);
        float height = (float) rect.height();
        float f2 = (((float) (i2 - (this.w * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float f3 = (float) 2;
        float height2 = ((float) getHeight()) - (height / f3);
        float f4 = (float) this.w;
        LinkedList linkedList = new LinkedList();
        for (String drawText : this.G) {
            canvas.drawText(drawText, f4, height2, this.r);
            linkedList.add(Integer.valueOf((int) ((measureText / f3) + f4)));
            f4 += measureText + f2;
        }
        ig6.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.B.isEmpty()) && this.B.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            int size = list.size();
            float height = (float) getStartBitmap().getHeight();
            int i6 = 1;
            while (i6 < size) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = H;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.B.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.B.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list2.get(i6).intValue();
                float f2 = (float) i3;
                float f3 = (float) i4;
                float f4 = (float) i5;
                float min = Math.min(((1.0f - (((float) this.B.get(i6).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                float intValue2 = (float) list2.get(i7).intValue();
                float min2 = Math.min(((1.0f - (((float) this.B.get(i7).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                if (min == min2) {
                    path.moveTo(intValue2, min2);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (i2 + this.z), min);
                    } else {
                        path.lineTo(intValue, min);
                    }
                    canvas2.drawPath(path, this.q);
                } else {
                    path.moveTo(intValue2, min2);
                    path.lineTo(intValue, min2);
                    canvas2.drawPath(path, this.q);
                    path.moveTo(intValue, min2);
                    path.lineTo(intValue, min);
                    canvas2.drawPath(path, this.q);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, min);
                        path.lineTo((float) (i2 + this.z), min);
                        canvas2.drawPath(path, this.q);
                    }
                }
                i6++;
                list2 = list;
                height = min;
            }
            canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.z), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
        }
    }
}
