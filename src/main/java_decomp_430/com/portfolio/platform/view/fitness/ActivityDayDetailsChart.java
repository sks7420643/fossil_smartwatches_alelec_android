package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.fossil.cd6;
import com.fossil.d7;
import com.fossil.dx5;
import com.fossil.gh6;
import com.fossil.hg6;
import com.fossil.hh6;
import com.fossil.mz5;
import com.fossil.nh6;
import com.fossil.pc6;
import com.fossil.qg6;
import com.fossil.sh4;
import com.fossil.tk4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xg6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String j0;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public /* final */ Paint H;
    @DexIgnore
    public /* final */ int I;
    @DexIgnore
    public /* final */ String J;
    @DexIgnore
    public /* final */ String K;
    @DexIgnore
    public /* final */ int L;
    @DexIgnore
    public int M;
    @DexIgnore
    public /* final */ int N;
    @DexIgnore
    public /* final */ int O;
    @DexIgnore
    public /* final */ int P;
    @DexIgnore
    public /* final */ Typeface Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public float T;
    @DexIgnore
    public float U;
    @DexIgnore
    public float V;
    @DexIgnore
    public int W;
    @DexIgnore
    public float a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h;
    @DexIgnore
    public int h0;
    @DexIgnore
    public List<pc6<Integer, Float, pc6<Float, Float, Float>>> i;
    @DexIgnore
    public int i0;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public sh4 x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<RectF, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ gh6 $goalDashLineBottom;
        @DexIgnore
        public /* final */ /* synthetic */ gh6 $goalDashLineX;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $goalPosition;
        @DexIgnore
        public /* final */ /* synthetic */ int $leftIndex;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, hh6 hh6, gh6 gh6, gh6 gh62) {
            super(1);
            this.$leftIndex = i;
            this.$goalPosition = hh6;
            this.$goalDashLineBottom = gh6;
            this.$goalDashLineX = gh62;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((RectF) obj);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(RectF rectF) {
            wg6.b(rectF, "drawnRect");
            if (rectF.height() > ((float) 0) && this.$leftIndex == this.$goalPosition.element) {
                this.$goalDashLineBottom.element = rectF.top;
                this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<RectF, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ gh6 $goalDashLineBottom;
        @DexIgnore
        public /* final */ /* synthetic */ gh6 $goalDashLineX;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $goalPosition;
        @DexIgnore
        public /* final */ /* synthetic */ int $rightIndex;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, hh6 hh6, gh6 gh6, gh6 gh62) {
            super(1);
            this.$rightIndex = i;
            this.$goalPosition = hh6;
            this.$goalDashLineBottom = gh6;
            this.$goalDashLineX = gh62;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((RectF) obj);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(RectF rectF) {
            wg6.b(rectF, "drawnRect");
            if (rectF.height() > ((float) 0) && this.$rightIndex == this.$goalPosition.element) {
                this.$goalDashLineBottom.element = rectF.top;
                this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ActivityDayDetailsChart.class.getSimpleName();
        wg6.a((Object) simpleName, "ActivityDayDetailsChart::class.java.simpleName");
        j0 = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.e = 0.5f;
        this.x = sh4.ACTIVE_TIME;
        this.y = new Paint(1);
        this.z = new Paint(1);
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        this.H = new Paint(1);
        this.I = context.getResources().getDimensionPixelSize(2131165418);
        nh6 nh6 = nh6.a;
        String string = context.getString(2131887015);
        wg6.a((Object) string, "context.getString(R.string.am_hour)");
        Object[] objArr = {12};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        this.J = format;
        nh6 nh62 = nh6.a;
        String string2 = context.getString(2131887250);
        wg6.a((Object) string2, "context.getString(R.string.pm_hour)");
        Object[] objArr2 = {12};
        String format2 = String.format(string2, Arrays.copyOf(objArr2, objArr2.length));
        wg6.a((Object) format2, "java.lang.String.format(format, *args)");
        this.K = format2;
        this.L = w6.a(context, 2131099867);
        this.M = 4;
        this.N = context.getResources().getDimensionPixelSize(2131165418);
        this.O = context.getResources().getDimensionPixelSize(2131165424);
        this.P = context.getResources().getDimensionPixelSize(2131165384);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, x24.ActivityDayDetailsChart, 0, 0));
        }
        this.Q = d7.a(context, 2131296261);
        int a2 = w6.a(context, 2131099838);
        int a3 = w6.a(context, 2131099867);
        int a4 = w6.a(context, R.color.steps);
        int a5 = w6.a(context, 2131099863);
        TypedArray mTypedArray = getMTypedArray();
        this.q = mTypedArray != null ? mTypedArray.getColor(4, a3) : a3;
        TypedArray mTypedArray2 = getMTypedArray();
        this.r = mTypedArray2 != null ? mTypedArray2.getColor(5, a4) : a4;
        TypedArray mTypedArray3 = getMTypedArray();
        this.s = mTypedArray3 != null ? mTypedArray3.getColor(3, a5) : a5;
        TypedArray mTypedArray4 = getMTypedArray();
        this.t = mTypedArray4 != null ? mTypedArray4.getColor(1, a4) : a4;
        TypedArray mTypedArray5 = getMTypedArray();
        this.u = mTypedArray5 != null ? mTypedArray5.getColor(0, a4) : a4;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getColor(7, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.o = mTypedArray7 != null ? mTypedArray7.getColor(6, a3) : a3;
        TypedArray mTypedArray8 = getMTypedArray();
        this.p = mTypedArray8 != null ? mTypedArray8.getColor(2, a2) : a2;
        TypedArray mTypedArray9 = getMTypedArray();
        this.v = mTypedArray9 != null ? mTypedArray9.getColor(8, a2) : a2;
        TypedArray mTypedArray10 = getMTypedArray();
        this.w = mTypedArray10 != null ? mTypedArray10.getDimensionPixelSize(9, 40) : 40;
        TypedArray mTypedArray11 = getMTypedArray();
        if (mTypedArray11 != null) {
            mTypedArray11.recycle();
        }
    }

    @DexIgnore
    private final float getGoalForEachBar() {
        return this.h / ((float) 16);
    }

    @DexIgnore
    private final String getMHighLevelText() {
        int i2 = mz5.a[this.x.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? 2131887186 : 2131887179 : 2131887184;
        nh6 nh6 = nh6.a;
        String string = getContext().getString(i3);
        wg6.a((Object) string, "context.getString(rawText)");
        Object[] objArr = {tk4.a(this.g, 3)};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final String getMLowLevelText() {
        int i2 = mz5.b[this.x.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? 2131887186 : 2131887179 : 2131887184;
        nh6 nh6 = nh6.a;
        String string = getContext().getString(i3);
        wg6.a((Object) string, "context.getString(rawText)");
        Object[] objArr = {tk4.a(this.f, 3)};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final float getMMax() {
        float f2;
        T t2;
        Float f3;
        List<pc6<Integer, Float, pc6<Float, Float, Float>>> list = this.i;
        if (list != null) {
            Iterator<T> it = list.iterator();
            if (!it.hasNext()) {
                t2 = null;
            } else {
                t2 = it.next();
                if (it.hasNext()) {
                    float floatValue = ((Number) ((pc6) t2).getSecond()).floatValue();
                    do {
                        T next = it.next();
                        float floatValue2 = ((Number) ((pc6) next).getSecond()).floatValue();
                        if (Float.compare(floatValue, floatValue2) < 0) {
                            t2 = next;
                            floatValue = floatValue2;
                        }
                    } while (it.hasNext());
                }
            }
            pc6 pc6 = (pc6) t2;
            if (!(pc6 == null || (f3 = (Float) pc6.getSecond()) == null)) {
                f2 = f3.floatValue();
                return Math.max(Math.max(getGoalForEachBar(), f2), this.g);
            }
        }
        f2 = 0.0f;
        return Math.max(Math.max(getGoalForEachBar(), f2), this.g);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, float f2, float f3, float f4, float f5, pc6<Integer, Float, pc6<Float, Float, Float>> pc6, sh4 sh4, boolean z2, hg6<? super RectF, cd6> hg6) {
        Paint paint;
        Canvas canvas2 = canvas;
        int i3 = i2;
        float f6 = f5;
        hg6<? super RectF, cd6> hg62 = hg6;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j0;
        local.d(str, "Maximum: " + f6 + ", value: " + pc6);
        float f7 = f2 / ((float) 2);
        float f8 = (float) 0;
        if (pc6.getSecond().floatValue() <= f8) {
            hg62.invoke(new RectF(0.0f, 0.0f, 0.0f, 0.0f));
            return;
        }
        float floatValue = pc6.getSecond().floatValue() / f6;
        if (floatValue > 1.0f) {
            floatValue = 1.0f;
        }
        float f9 = (float) this.M;
        float f10 = (float) i3;
        float max = Math.max(f10 * floatValue, f7);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = j0;
        local2.d(str2, "Percentage: " + floatValue + ", chart height: " + i3 + ", real bar height: " + max);
        float max2 = Math.max((f10 - max) + f9, f9 + ((float) (z2 ? getStartBitmap().getHeight() * 2 : 0)));
        float round = (float) Math.round(f3);
        float round2 = (float) Math.round(f4);
        RectF rectF = new RectF(round, max2, round2, f10);
        pc6 third = pc6.getThird();
        if (third == null) {
            if (mz5.e[sh4.ordinal()] != 1) {
                paint = this.G;
            } else {
                paint = this.H;
            }
            dx5.c(canvas2, rectF, paint, f7);
        } else {
            dx5.c(canvas2, rectF, this.D, f7);
            float floatValue2 = (((Number) third.getThird()).floatValue() / pc6.getSecond().floatValue()) * max;
            float floatValue3 = ((((Number) third.getThird()).floatValue() + ((Number) third.getSecond()).floatValue()) / pc6.getSecond().floatValue()) * max;
            if (floatValue3 > f8) {
                dx5.c(canvas2, new RectF(round, f10 - floatValue3, round2, f10), this.E, f7);
            }
            if (floatValue2 > f8) {
                dx5.c(canvas2, new RectF(round, f10 - floatValue2, round2, f10), this.F, f7);
            }
        }
        hg62.invoke(rectF);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        Paint paint = this.A;
        String str = this.J;
        this.R = paint.measureText(str, 0, yj6.c(str));
        this.S = ((float) getHeight()) - ((float) this.I);
        this.U = ((float) (getWidth() / 2)) - (this.R / ((float) 2));
        this.T = (float) this.I;
        this.V = (((float) getWidth()) - this.R) - ((float) (this.I * 3));
        Rect rect = new Rect();
        Paint paint2 = this.A;
        String str2 = this.J;
        paint2.getTextBounds(str2, 0, yj6.c(str2), rect);
        this.M = rect.height();
        this.W = (getHeight() - rect.height()) - (this.I * 2);
        Rect rect2 = new Rect();
        Rect rect3 = new Rect();
        int i2 = 1;
        this.A.getTextBounds(getMHighLevelText(), 0, yj6.c(getMHighLevelText()) > 0 ? yj6.c(getMHighLevelText()) : 1, rect2);
        Paint paint3 = this.A;
        String mLowLevelText = getMLowLevelText();
        if (yj6.c(getMLowLevelText()) > 0) {
            i2 = yj6.c(getMLowLevelText());
        }
        paint3.getTextBounds(mLowLevelText, 0, i2, rect3);
        this.a0 = (((float) getWidth()) - Math.max(this.A.measureText(getMHighLevelText()), this.A.measureText(getMLowLevelText()))) - ((float) (this.I * 3));
        this.i0 = 0;
        int i3 = this.W;
        this.h0 = i3 - 4;
        this.e0 = Math.max(((float) i3) * this.d, (float) this.M);
        this.b0 = Math.max(((float) this.W) * this.e, (float) this.M);
        if (this.b0 < this.e0 + ((float) rect2.height())) {
            this.e0 = this.b0 - ((float) rect2.height());
        }
        float f2 = this.a0;
        int i4 = this.I;
        this.c0 = ((float) i4) + f2;
        this.f0 = f2 + ((float) i4);
        this.d0 = this.b0 + ((float) (rect3.height() / 2));
        this.g0 = this.e0 + ((float) (rect2.height() / 2));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Canvas canvas2;
        Canvas canvas3 = canvas;
        super.draw(canvas);
        if (canvas3 != null) {
            canvas3.drawText(this.J, this.T, this.S, this.A);
            canvas3.drawText(this.K, this.U, this.S, this.A);
            canvas3.drawText(this.J, this.V, this.S, this.A);
            List<pc6<Integer, Float, pc6<Float, Float, Float>>> list = this.i;
            if (list != null) {
                float f2 = this.a0 / ((float) 2);
                int i2 = this.N;
                float f3 = (float) (i2 / 2);
                float f4 = f2 - f3;
                float f5 = (f2 + ((float) i2)) - f3;
                hh6 hh6 = new hh6();
                hh6.element = -1;
                int size = list.size();
                int i3 = 0;
                float f6 = 0.0f;
                while (true) {
                    if (i3 >= size) {
                        break;
                    }
                    f6 += ((Number) list.get(i3).getSecond()).floatValue();
                    if (f6 >= this.h) {
                        FLogger.INSTANCE.getLocal().d(j0, "Goal reach at " + i3);
                        hh6.element = i3;
                        break;
                    }
                    i3++;
                }
                gh6 gh6 = new gh6();
                gh6.element = -1.0f;
                gh6 gh62 = new gh6();
                gh62.element = -1.0f;
                float mMax = getMMax();
                FLogger.INSTANCE.getLocal().d(j0, "Data max: " + getMMax() + ", goal for each bar: " + getGoalForEachBar() + ", chart max: " + mMax);
                int i4 = this.O;
                float f7 = f4;
                int i5 = 11;
                while (true) {
                    boolean z2 = true;
                    if (i5 < 0) {
                        break;
                    }
                    pc6 pc6 = list.get(i5);
                    int i6 = this.W;
                    float f8 = (float) i4;
                    float f9 = f7 - f8;
                    sh4 sh4 = this.x;
                    if (i5 != hh6.element) {
                        z2 = false;
                    }
                    int i7 = i4;
                    a(canvas, i6, f8, f9, f7, mMax, pc6, sh4, z2, new b(i5, hh6, gh62, gh6));
                    f7 -= (float) (i7 + this.N);
                    i5--;
                    hh6 = hh6;
                    gh62 = gh62;
                    i4 = i7;
                    gh6 = gh6;
                    mMax = mMax;
                    Canvas canvas4 = canvas;
                }
                int i8 = i4;
                float f10 = mMax;
                gh6 gh63 = gh62;
                gh6 gh64 = gh6;
                hh6 hh62 = hh6;
                int i9 = 12;
                for (int i10 = 24; i9 < i10; i10 = 24) {
                    pc6 pc62 = list.get(i9);
                    float f11 = (float) i8;
                    gh6 gh65 = gh64;
                    a(canvas, this.W, f11, f5, f5 + f11, f10, pc62, this.x, i9 == hh62.element, new c(i9, hh62, gh63, gh65));
                    f5 += (float) (i8 + this.N);
                    i9++;
                    gh64 = gh65;
                    hh62 = hh62;
                }
                gh6 gh66 = gh64;
                float f12 = (float) 0;
                if (gh63.element < f12 || gh66.element < f12) {
                    canvas2 = canvas;
                } else {
                    int height = getStartBitmap().getHeight();
                    int width = getStartBitmap().getWidth();
                    float f13 = (float) (height + 10);
                    if (gh63.element >= f13) {
                        canvas2 = canvas;
                        gh6 gh67 = gh66;
                        canvas2.drawBitmap(getStartBitmap(), gh66.element - ((float) (width / 2)), (float) 10, this.B);
                        Path path = new Path();
                        path.moveTo(gh67.element, f13);
                        path.lineTo(gh67.element, gh63.element);
                        canvas2.drawPath(path, this.z);
                    } else {
                        canvas2 = canvas;
                        canvas2.drawBitmap(getStartBitmap(), gh66.element - ((float) (width / 2)), (float) 10, this.B);
                    }
                }
            } else {
                canvas2 = canvas3;
            }
            canvas2.drawRect(new Rect(0, this.h0, getWidth(), this.W), this.y);
            canvas2.drawRect(new Rect(0, this.i0, getWidth(), 4), this.y);
            FLogger.INSTANCE.getLocal().d(j0, "low level x: " + this.c0 + " y: " + this.d0 + ", high level x: " + this.f0 + " y: " + this.g0);
            float f14 = this.d0;
            if (f14 > ((float) this.i0) && f14 < ((float) this.h0)) {
                canvas2.drawText(getMLowLevelText(), this.c0, this.d0, this.A);
            }
            float f15 = this.g0;
            if (f15 > ((float) this.i0) && f15 < ((float) this.h0)) {
                canvas2.drawText(getMHighLevelText(), this.f0, this.g0, this.A);
            }
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 2131231101;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.P;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.y.setColor(this.j);
        float f2 = (float) 4;
        this.y.setStrokeWidth(f2);
        this.C.setColor(this.p);
        this.C.setStrokeWidth((float) this.O);
        a(this.D, this.q, (float) this.O, Paint.Style.FILL);
        a(this.E, this.r, (float) this.O, Paint.Style.FILL);
        a(this.F, this.s, (float) this.O, Paint.Style.FILL);
        a(this.G, this.t, (float) this.O, Paint.Style.FILL);
        a(this.H, this.u, (float) this.O, Paint.Style.FILL);
        this.z.setColor(this.o);
        this.z.setStyle(Paint.Style.STROKE);
        this.z.setStrokeWidth(f2);
        this.z.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, 0.0f));
        this.A.setColor(this.v);
        this.A.setStyle(Paint.Style.FILL);
        this.A.setTextSize((float) this.w);
        this.A.setTypeface(this.Q);
        this.B.setStyle(Paint.Style.FILL);
        this.B.setColorFilter(new PorterDuffColorFilter(this.L, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f2, Paint.Style style) {
        paint.setColor(i2);
        paint.setStrokeWidth(f2);
        paint.setStyle(style);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }
}
