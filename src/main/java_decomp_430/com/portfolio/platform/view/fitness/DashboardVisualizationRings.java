package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardVisualizationRings extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ int D;
    @DexIgnore
    public /* final */ int E;
    @DexIgnore
    public /* final */ int F;
    @DexIgnore
    public Bitmap G;
    @DexIgnore
    public Bitmap H;
    @DexIgnore
    public Bitmap I;
    @DexIgnore
    public Bitmap J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public TypedArray a;
    @DexIgnore
    public /* final */ RectF b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ Paint x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = DashboardVisualizationRings.class.getSimpleName();
        wg6.a((Object) simpleName, "DashboardVisualizationRings::class.java.simpleName");
        S = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.b = new RectF();
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = new Paint(1);
        this.y = new Paint(1);
        this.z = new Paint(1);
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = context.getResources().getDimensionPixelSize(2131165411);
        this.E = context.getResources().getDimensionPixelSize(2131165419);
        this.F = context.getResources().getDimensionPixelSize(2131165395);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            this.a = context.getTheme().obtainStyledAttributes(attributeSet, x24.DashboardVisualizationRings, 0, 0);
        }
        int a2 = w6.a(context, 2131099869);
        int a3 = w6.a(context, 2131099870);
        int a4 = w6.a(context, 2131099871);
        int a5 = w6.a(context, R.color.sleep);
        int a6 = w6.a(context, 2131099858);
        int a7 = w6.a(context, 2131099862);
        int a8 = w6.a(context, 2131099832);
        int a9 = w6.a(context, 2131099831);
        this.r = w6.a(context, 2131099696);
        TypedArray typedArray = this.a;
        this.q = typedArray != null ? typedArray.getColor(2, a9) : a9;
        TypedArray typedArray2 = this.a;
        this.c = typedArray2 != null ? typedArray2.getColor(9, a2) : a2;
        TypedArray typedArray3 = this.a;
        this.d = typedArray3 != null ? typedArray3.getColor(9, a2) : a2;
        TypedArray typedArray4 = this.a;
        this.e = typedArray4 != null ? typedArray4.getColor(0, a3) : a3;
        TypedArray typedArray5 = this.a;
        this.f = typedArray5 != null ? typedArray5.getColor(0, a3) : a3;
        TypedArray typedArray6 = this.a;
        this.g = typedArray6 != null ? typedArray6.getColor(3, a4) : a4;
        TypedArray typedArray7 = this.a;
        this.h = typedArray7 != null ? typedArray7.getColor(3, a4) : a4;
        TypedArray typedArray8 = this.a;
        this.j = typedArray8 != null ? typedArray8.getColor(6, a5) : a5;
        TypedArray typedArray9 = this.a;
        this.o = typedArray9 != null ? typedArray9.getColor(7, a6) : a6;
        TypedArray typedArray10 = this.a;
        this.p = typedArray10 != null ? typedArray10.getColor(8, a7) : a7;
        TypedArray typedArray11 = this.a;
        this.i = typedArray11 != null ? typedArray11.getColor(5, a8) : a8;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        if (r0.isRecycled() != false) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0080, code lost:
        if (r0.isRecycled() != false) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (r0.isRecycled() != false) goto L_0x0017;
     */
    @DexIgnore
    public final void a() {
        Bitmap bitmap = this.G;
        if (bitmap != null) {
            if (bitmap == null) {
                wg6.d("mStepsBitmap");
                throw null;
            }
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), 2131231213);
        int i2 = this.F;
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, i2, i2, false);
        wg6.a((Object) createScaledBitmap, "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)");
        this.G = createScaledBitmap;
        Bitmap bitmap2 = this.G;
        if (bitmap2 != null) {
            if (!wg6.a((Object) decodeResource, (Object) bitmap2)) {
                decodeResource.recycle();
            }
            Bitmap bitmap3 = this.H;
            if (bitmap3 != null) {
                if (bitmap3 == null) {
                    wg6.d("mActiveMinutesBitmap");
                    throw null;
                }
            }
            Bitmap decodeResource2 = BitmapFactory.decodeResource(getResources(), 2131231209);
            int i3 = this.F;
            Bitmap createScaledBitmap2 = Bitmap.createScaledBitmap(decodeResource2, i3, i3, false);
            wg6.a((Object) createScaledBitmap2, "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)");
            this.H = createScaledBitmap2;
            Bitmap bitmap4 = this.H;
            if (bitmap4 != null) {
                if (!wg6.a((Object) decodeResource2, (Object) bitmap4)) {
                    decodeResource2.recycle();
                }
                Bitmap bitmap5 = this.I;
                if (bitmap5 != null) {
                    if (bitmap5 == null) {
                        wg6.d("mCaloriesBitmap");
                        throw null;
                    }
                }
                Bitmap decodeResource3 = BitmapFactory.decodeResource(getResources(), 2131231210);
                int i4 = this.F;
                Bitmap createScaledBitmap3 = Bitmap.createScaledBitmap(decodeResource3, i4, i4, false);
                wg6.a((Object) createScaledBitmap3, "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)");
                this.I = createScaledBitmap3;
                Bitmap bitmap6 = this.I;
                if (bitmap6 != null) {
                    if (!wg6.a((Object) decodeResource3, (Object) bitmap6)) {
                        decodeResource3.recycle();
                    }
                    Bitmap bitmap7 = this.J;
                    if (bitmap7 != null) {
                        if (bitmap7 == null) {
                            wg6.d("mSleepBitmap");
                            throw null;
                        } else if (!bitmap7.isRecycled()) {
                            return;
                        }
                    }
                    Bitmap decodeResource4 = BitmapFactory.decodeResource(getResources(), 2131231212);
                    int i5 = this.F;
                    Bitmap createScaledBitmap4 = Bitmap.createScaledBitmap(decodeResource4, i5, i5, false);
                    wg6.a((Object) createScaledBitmap4, "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)");
                    this.J = createScaledBitmap4;
                    Bitmap bitmap8 = this.J;
                    if (bitmap8 == null) {
                        wg6.d("mSleepBitmap");
                        throw null;
                    } else if (!wg6.a((Object) decodeResource4, (Object) bitmap8)) {
                        decodeResource4.recycle();
                    }
                } else {
                    wg6.d("mCaloriesBitmap");
                    throw null;
                }
            } else {
                wg6.d("mActiveMinutesBitmap");
                throw null;
            }
        } else {
            wg6.d("mStepsBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final void b() {
        Bitmap bitmap = this.G;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
            } else {
                wg6.d("mStepsBitmap");
                throw null;
            }
        }
        Bitmap bitmap2 = this.I;
        if (bitmap2 != null) {
            if (bitmap2 != null) {
                bitmap2.recycle();
            } else {
                wg6.d("mCaloriesBitmap");
                throw null;
            }
        }
        Bitmap bitmap3 = this.H;
        if (bitmap3 != null) {
            if (bitmap3 != null) {
                bitmap3.recycle();
            } else {
                wg6.d("mActiveMinutesBitmap");
                throw null;
            }
        }
        Bitmap bitmap4 = this.J;
        if (bitmap4 == null) {
            return;
        }
        if (bitmap4 != null) {
            bitmap4.recycle();
        } else {
            wg6.d("mSleepBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final void c(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        a(canvas, a(this.b, ((float) this.E) * 1.5f), f2, f3, bitmap, this.A, this.B);
    }

    @DexIgnore
    public final void d(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF rectF = this.b;
        Paint paint = this.u;
        a(canvas, rectF, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(this.q);
            float f2 = this.K;
            float f3 = this.L;
            Bitmap bitmap = this.G;
            if (bitmap != null) {
                d(canvas, f2, f3, bitmap);
                float f4 = this.M;
                float f5 = this.N;
                Bitmap bitmap2 = this.I;
                if (bitmap2 != null) {
                    b(canvas, f4, f5, bitmap2);
                    float f6 = this.O;
                    float f7 = this.P;
                    Bitmap bitmap3 = this.H;
                    if (bitmap3 != null) {
                        a(canvas, f6, f7, bitmap3);
                        float f8 = this.Q;
                        float f9 = this.R;
                        Bitmap bitmap4 = this.J;
                        if (bitmap4 != null) {
                            c(canvas, f8, f9, bitmap4);
                        } else {
                            wg6.d("mSleepBitmap");
                            throw null;
                        }
                    } else {
                        wg6.d("mActiveMinutesBitmap");
                        throw null;
                    }
                } else {
                    wg6.d("mCaloriesBitmap");
                    throw null;
                }
            } else {
                wg6.d("mStepsBitmap");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(S, "onAttachedToWindow, initIcons");
        super.onAttachedToWindow();
        a();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(S, "onDetachedFromWindow, recycleIcons");
        super.onDetachedFromWindow();
        b();
    }

    @DexIgnore
    public void onGlobalLayout() {
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width > height) {
            float f2 = (width - height) / ((float) 2);
            RectF rectF = this.b;
            rectF.left = f2;
            rectF.right = f2 + height;
            rectF.top = 0.0f;
            rectF.bottom = height;
        } else if (height > width) {
            float f3 = (height - width) / ((float) 2);
            RectF rectF2 = this.b;
            rectF2.left = 0.0f;
            rectF2.right = width;
            rectF2.top = f3;
            rectF2.bottom = f3 + width;
        } else {
            RectF rectF3 = this.b;
            rectF3.left = 0.0f;
            rectF3.right = width;
            rectF3.top = 0.0f;
            rectF3.bottom = height;
        }
        float f4 = (float) this.D;
        this.s.setColor(this.i);
        this.s.setDither(true);
        this.s.setStyle(Paint.Style.STROKE);
        this.s.setStrokeWidth(f4);
        this.t.setColor(this.r);
        this.t.setStrokeWidth(f4);
        a(this.u, this.c, f4);
        a(this.v, this.d, f4);
        a(this.y, this.g, f4);
        a(this.z, this.h, f4);
        a(this.w, this.e, f4);
        a(this.x, this.f, f4);
        a(this.A, this.j, f4);
        a(this.B, this.o, f4);
        a(this.C, this.p, f4);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void b(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF a2 = a(this.b, ((float) this.E) / ((float) 2));
        Paint paint = this.y;
        a(canvas, a2, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f2) {
        paint.setColor(i2);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(f2);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF a2 = a(this.b, (float) this.E);
        Paint paint = this.w;
        a(canvas, a2, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, float f2, float f3, Bitmap bitmap, Paint paint, Paint paint2) {
        Canvas canvas2 = canvas;
        RectF rectF2 = rectF;
        Bitmap bitmap2 = bitmap;
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        float strokeWidth = paint.getStrokeWidth();
        float f4 = rectF2.top;
        float f5 = (rectF2.left + rectF2.right) - ((float) width);
        float f6 = (float) 2;
        float f7 = f5 / f6;
        float f8 = (float) height;
        if (f8 >= strokeWidth) {
            rectF2 = a(rectF2, (f8 - strokeWidth) / f6);
        }
        RectF rectF3 = rectF2;
        if (f3 == 0.0f) {
            a(canvas, rectF3, true, 270.0f, 0.0f, paint);
            a(canvas2, bitmap2, f4, f7);
            return;
        }
        if (f2 > f3) {
            a(canvas, rectF3, false, 270.0f, 360.0f, paint);
            float min = Math.min((f2 - f3) / f3, 1.0f);
            a(canvas, a(rectF3, strokeWidth + ((float) 1)), false, 270.0f, Math.min(min * 360.0f, 360.0f), paint2);
        } else {
            float min2 = Math.min((f2 / f3) * 360.0f, 360.0f);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = S;
            local.d(str, "sweepAngle: " + min2);
            a(canvas, rectF3, true, 270.0f, min2, paint);
        }
        a(canvas2, bitmap2, f4, f7);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, boolean z2, float f2, float f3, Paint paint) {
        if (z2) {
            float centerX = rectF.centerX();
            float centerY = rectF.centerY();
            float width = (rectF.width() - ((float) this.D)) / ((float) 2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = S;
            local.d(str, "centerX: " + centerX + ", centerY: " + centerY + ", radius: " + width);
            canvas.drawCircle(centerX, centerY, width, this.s);
        }
        canvas.drawArc(a(rectF, paint.getStrokeWidth() / ((float) 2)), f2, f3, false, paint);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, Bitmap bitmap, float f2, float f3) {
        canvas.drawBitmap(bitmap, f3, f2, this.t);
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        return new RectF(rectF.left + f2, rectF.top + f2, rectF.right - f2, rectF.bottom - f2);
    }
}
