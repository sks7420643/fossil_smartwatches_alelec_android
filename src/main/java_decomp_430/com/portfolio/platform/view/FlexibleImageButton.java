package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageButton;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.x24;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleImageButton extends AppCompatImageButton {
    @DexIgnore
    public String c; // = "#FFFFFF";
    @DexIgnore
    public String d; // = "#FFFFFF";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton] */
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleImageButton);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = Explore.COLUMN_BACKGROUND;
            }
            wg6.a((Object) string, "styledAttrs.getString(R.\u2026          ?: \"background\"");
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "primaryText";
            }
            wg6.a((Object) string2, "styledAttrs.getString(R.\u2026         ?: \"primaryText\"");
            String b = ThemeManager.l.a().b(string);
            if (b == null) {
                b = "#FFFFFF";
            }
            this.c = b;
            String b2 = ThemeManager.l.a().b(string2);
            if (b2 == null) {
                b2 = "#FFFFFF";
            }
            this.d = b2;
            a();
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            FlexibleImageButton.super.setImageDrawable(drawable);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton] */
    public final void a() {
        if (this.c != null) {
            Drawable background = getBackground();
            if (background instanceof ShapeDrawable) {
                Drawable background2 = getBackground();
                if (background2 != null) {
                    Paint paint = ((ShapeDrawable) background2).getPaint();
                    wg6.a((Object) paint, "shapeDrawable.paint");
                    paint.setColor(Color.parseColor(this.c));
                } else {
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.ShapeDrawable");
                }
            } else if (background instanceof GradientDrawable) {
                Drawable background3 = getBackground();
                if (background3 != null) {
                    ((GradientDrawable) background3).setColor(Color.parseColor(this.c));
                } else {
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                }
            } else if (background instanceof ColorDrawable) {
                Drawable background4 = getBackground();
                if (background4 != null) {
                    ((ColorDrawable) background4).setColor(Color.parseColor(this.c));
                } else {
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.ColorDrawable");
                }
            } else if (background instanceof StateListDrawable) {
                Drawable background5 = getBackground();
                if (background5 != null) {
                    ((StateListDrawable) background5).setColorFilter(Color.parseColor(this.c), PorterDuff.Mode.SRC_ATOP);
                } else {
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.StateListDrawable");
                }
            }
        }
        String str = this.d;
        if (str != null) {
            getDrawable().setColorFilter(Color.parseColor(str), PorterDuff.Mode.SRC_ATOP);
        }
    }
}
