package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.fossil.w6;
import com.fossil.wq;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BarChart extends View {
    @DexIgnore
    public Paint A;
    @DexIgnore
    public /* final */ Path B; // = new Path();
    @DexIgnore
    public Bitmap C;
    @DexIgnore
    public Canvas D;
    @DexIgnore
    public /* final */ List<b> a; // = new ArrayList();
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public /* final */ Rect f; // = new Rect();
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public float i;
    @DexIgnore
    public String j;
    @DexIgnore
    public float o;
    @DexIgnore
    public int p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public Drawable t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public GestureDetector w;
    @DexIgnore
    public View.OnClickListener x;
    @DexIgnore
    public Paint y;
    @DexIgnore
    public Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return BarChart.this.x != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            if (BarChart.this.x != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float x2 = BarChart.this.getX();
                float y2 = BarChart.this.getY();
                float measuredWidth = ((float) (BarChart.this.getMeasuredWidth() / 2)) - x2;
                float f = (x - x2) - measuredWidth;
                float f2 = (y - y2) - measuredWidth;
                float f3 = (f * f) + (f2 * f2);
                BarChart barChart = BarChart.this;
                float f4 = barChart.c;
                if (f3 < (measuredWidth + f4) * (measuredWidth + f4)) {
                    barChart.x.onClick(barChart);
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public float b;
        @DexIgnore
        public List<Pair<Float, Integer>> c;

        @DexIgnore
        public b(BarChart barChart) {
        }
    }

    @DexIgnore
    public BarChart(Context context) {
        super(context);
        a();
    }

    @DexIgnore
    public void a(String str, float f2, List<Pair<Float, Integer>> list) {
        b bVar = new b(this);
        bVar.a = str;
        bVar.b = f2;
        bVar.c = new ArrayList(list);
        this.a.add(bVar);
    }

    @DexIgnore
    public void b() {
        for (b next : this.a) {
            float f2 = 0.0f;
            for (Pair<Float, Integer> pair : next.c) {
                f2 = Math.max(f2, ((Float) pair.first).floatValue());
            }
            this.b = Math.max(this.b, f2);
            this.b = Math.max(this.b, next.b);
        }
        invalidate();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        Bitmap bitmap = this.C;
        if (bitmap != null) {
            bitmap.eraseColor(0);
            this.B.reset();
            float f3 = this.g;
            float measuredHeight = ((float) getMeasuredHeight()) - f3;
            float ascent = (((f3 - this.z.ascent()) - this.z.descent()) / 2.0f) + measuredHeight;
            float measuredWidth = ((((float) getMeasuredWidth()) - this.d) - this.e) / ((float) this.a.size());
            float f4 = measuredHeight - this.o;
            float f5 = f4 - (this.u / 2.0f);
            float f6 = this.d + (measuredWidth / 2.0f);
            Iterator<b> it = this.a.iterator();
            float f7 = f6;
            float f8 = 0.0f;
            float f9 = 0.0f;
            while (it.hasNext()) {
                b next = it.next();
                float f10 = f4;
                for (Pair next2 : next.c) {
                    this.y.setColor(((Integer) next2.second).intValue());
                    float floatValue = f10 - ((((Float) next2.first).floatValue() * f5) / this.b);
                    this.D.drawLine(f7, f10, f7, floatValue, this.y);
                    next = next;
                    f10 = floatValue;
                }
                b bVar = next;
                Paint paint = this.z;
                String str = bVar.a;
                paint.getTextBounds(str, 0, str.length(), this.f);
                this.D.drawText(bVar.a, f7 - ((float) (this.f.width() / 2)), ascent, this.z);
                float f11 = bVar.b;
                float f12 = f4 - ((f5 * f11) / this.b);
                if (f12 != f8 && f11 > 0.0f) {
                    this.B.moveTo(f9, f12);
                    f8 = f12;
                }
                float f13 = (this.a.indexOf(bVar) == 0 ? this.d : 0.0f) + measuredWidth;
                if (this.a.indexOf(bVar) == this.a.size() - 1) {
                    f2 = this.e - (this.t != null ? this.u + (this.v * 2.0f) : 0.0f);
                } else {
                    f2 = 0.0f;
                }
                f9 += f13 + f2;
                if (f8 > 0.0f && bVar.b > 0.0f) {
                    this.B.lineTo(f9, f8);
                }
                f7 += measuredWidth;
            }
            this.D.drawPath(this.B, this.A);
            Drawable drawable = this.t;
            if (drawable != null && f8 > 0.0f) {
                float f14 = f9 + this.v;
                float f15 = this.u;
                drawable.setBounds((int) f14, (int) (f8 - (f15 / 2.0f)), (int) (f14 + f15), (int) (f8 + (f15 / 2.0f)));
                this.t.draw(this.D);
            }
            canvas.drawBitmap(this.C, 0.0f, 0.0f, (Paint) null);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Bitmap bitmap = this.C;
        if (bitmap == null || i2 > bitmap.getWidth() || i3 > this.C.getHeight()) {
            this.C = wq.a(getContext()).c().a(i2, i3, Bitmap.Config.ARGB_8888);
        } else {
            this.C.setWidth(i2);
            this.C.setHeight(i3);
        }
        Canvas canvas = this.D;
        if (canvas == null) {
            this.D = new Canvas(this.C);
        } else {
            canvas.setBitmap(this.C);
        }
        b();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.w.onTouchEvent(motionEvent);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public BarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, x24.BarChart, 0, 0);
        try {
            this.c = obtainStyledAttributes.getDimension(14, getResources().getDimension(2131165411));
            this.d = obtainStyledAttributes.getDimension(8, getResources().getDimension(2131165435));
            this.e = obtainStyledAttributes.getDimension(0, getResources().getDimension(2131165391));
            this.g = obtainStyledAttributes.getDimension(11, getResources().getDimension(2131165399));
            this.h = obtainStyledAttributes.getColor(9, w6.a(context, 2131099832));
            this.i = obtainStyledAttributes.getDimension(13, getResources().getDimension(2131165723));
            this.j = obtainStyledAttributes.getString(10);
            this.o = obtainStyledAttributes.getDimension(12, getResources().getDimension(2131165435));
            this.p = obtainStyledAttributes.getColor(1, w6.a(context, 2131099832));
            this.q = obtainStyledAttributes.getDimension(4, getResources().getDimension(2131165371));
            this.r = obtainStyledAttributes.getDimension(3, getResources().getDimension(2131165405));
            this.s = obtainStyledAttributes.getDimension(2, getResources().getDimension(2131165411));
            this.t = obtainStyledAttributes.getDrawable(5);
            this.u = obtainStyledAttributes.getDimension(7, getResources().getDimension(2131165397));
            this.v = obtainStyledAttributes.getDimension(6, getResources().getDimension(2131165424));
            obtainStyledAttributes.recycle();
            a();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public final void a() {
        this.y = new Paint(1);
        this.y.setStrokeWidth(this.c);
        this.z = new Paint(1);
        if (this.j != null) {
            this.z.setTypeface(Typeface.createFromAsset(getContext().getAssets(), this.j));
        }
        this.z.setTextSize(this.i);
        this.z.setColor(this.h);
        this.A = new Paint(1);
        this.A.setColor(this.p);
        this.A.setStrokeWidth(this.q);
        this.A.setStyle(Paint.Style.STROKE);
        this.A.setPathEffect(new DashPathEffect(new float[]{this.r, this.s}, 0.0f));
        this.w = new GestureDetector(getContext(), new a());
        if (isInEditMode()) {
            Context context = getContext();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new Pair(Float.valueOf(123.0f), Integer.valueOf(w6.a(context, 2131099874))));
            arrayList.add(new Pair(Float.valueOf(234.0f), Integer.valueOf(w6.a(context, 2131099834))));
            arrayList.add(new Pair(Float.valueOf(345.0f), Integer.valueOf(w6.a(context, 2131099865))));
            a(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, 500.0f, arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new Pair(Float.valueOf(234.0f), Integer.valueOf(w6.a(context, 2131099874))));
            arrayList2.add(new Pair(Float.valueOf(345.0f), Integer.valueOf(w6.a(context, 2131099834))));
            arrayList2.add(new Pair(Float.valueOf(456.0f), Integer.valueOf(w6.a(context, 2131099865))));
            a("M", 1000.0f, arrayList2);
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new Pair(Float.valueOf(345.0f), Integer.valueOf(w6.a(context, 2131099874))));
            arrayList3.add(new Pair(Float.valueOf(456.0f), Integer.valueOf(w6.a(context, 2131099834))));
            arrayList3.add(new Pair(Float.valueOf(678.0f), Integer.valueOf(w6.a(context, 2131099865))));
            a("T", 1500.0f, arrayList3);
            this.b = 1479.0f;
            this.t = w6.c(context, 2131230919);
        }
    }
}
