package com.portfolio.platform.view;

import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SingleLineTextView extends FlexibleTextView {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.SingleLineTextView, android.widget.TextView] */
    public SingleLineTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [androidx.appcompat.widget.AppCompatTextView, com.portfolio.platform.view.SingleLineTextView, android.widget.TextView] */
    public void onMeasure(int i, int i2) {
        int lineCount;
        SingleLineTextView.super.onMeasure(i, i2);
        Layout layout = getLayout();
        if (layout != null && (lineCount = layout.getLineCount()) > 0 && layout.getEllipsisCount(lineCount - 1) > 0) {
            setTextSize(0, getTextSize() - 1.0f);
            measure(i, i2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.SingleLineTextView, android.widget.TextView] */
    public SingleLineTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.SingleLineTextView, android.widget.TextView] */
    public SingleLineTextView(Context context) {
        super(context);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }
}
