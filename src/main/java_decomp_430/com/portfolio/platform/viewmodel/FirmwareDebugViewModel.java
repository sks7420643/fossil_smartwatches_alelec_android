package com.portfolio.platform.viewmodel;

import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FirmwareDebugViewModel extends td {
    @DexIgnore
    public /* final */ MutableLiveData<List<DebugFirmwareData>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Firmware> b; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1", f = "FirmwareDebugViewModel.kt", l = {16}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ hg6 $block;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FirmwareDebugViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(FirmwareDebugViewModel firmwareDebugViewModel, hg6 hg6, xe6 xe6) {
            super(2, xe6);
            this.this$0 = firmwareDebugViewModel;
            this.$block = hg6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$block, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                hg6 hg6 = this.$block;
                this.L$0 = il6;
                this.label = 1;
                obj = hg6.invoke(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a().a((List) obj);
            return cd6.a;
        }
    }

    @DexIgnore
    public final MutableLiveData<List<DebugFirmwareData>> a() {
        return this.a;
    }

    @DexIgnore
    public final MutableLiveData<Firmware> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean c() {
        if (this.a.a() != null) {
            Object a2 = this.a.a();
            if (a2 != null) {
                wg6.a(a2, "firmwares.value!!");
                if (!((Collection) a2).isEmpty()) {
                    return true;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        MutableLiveData<List<DebugFirmwareData>> mutableLiveData = this.a;
        mutableLiveData.a(mutableLiveData.a());
        MutableLiveData<Firmware> mutableLiveData2 = this.b;
        mutableLiveData2.a(mutableLiveData2.a());
    }

    @DexIgnore
    public final rm6 a(hg6<? super xe6<? super List<DebugFirmwareData>>, ? extends Object> hg6) {
        wg6.b(hg6, "block");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new a(this, hg6, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Firmware firmware) {
        wg6.b(firmware, "firmware");
        this.b.a(firmware);
    }
}
