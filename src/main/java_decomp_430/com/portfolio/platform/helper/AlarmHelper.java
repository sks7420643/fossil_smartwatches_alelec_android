package com.portfolio.platform.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ih6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmHelper {
    @DexIgnore
    public static /* final */ a d; // = new a((qg6) null);
    @DexIgnore
    public /* final */ an4 a;
    @DexIgnore
    public /* final */ UserRepository b;
    @DexIgnore
    public /* final */ AlarmsRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(String str) {
            wg6.b(str, "day");
            String lowerCase = str.toLowerCase();
            wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            switch (lowerCase.hashCode()) {
                case 101661:
                    if (lowerCase.equals("fri")) {
                        return 6;
                    }
                    break;
                case 108300:
                    if (lowerCase.equals("mon")) {
                        return 2;
                    }
                    break;
                case 113638:
                    if (lowerCase.equals("sat")) {
                        return 7;
                    }
                    break;
                case 114252:
                    if (lowerCase.equals("sun")) {
                        return 1;
                    }
                    break;
                case 114817:
                    if (lowerCase.equals("thu")) {
                        return 5;
                    }
                    break;
                case 115204:
                    if (lowerCase.equals("tue")) {
                        return 3;
                    }
                    break;
                case 117590:
                    if (lowerCase.equals("wed")) {
                        return 4;
                    }
                    break;
            }
            return -1;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final long a(long j) {
            int i;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j);
            Calendar instance = Calendar.getInstance();
            if (instance.get(9) == 1) {
                int i2 = instance.get(10);
                i = i2 == 12 ? 12 : i2 + 12;
            } else {
                i = instance.get(10);
            }
            long j2 = ((long) ((((i * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000;
            long j3 = j2 <= j ? j - j2 : LogBuilder.MAX_INTERVAL - (j2 - j);
            long currentTimeMillis = System.currentTimeMillis() + j;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j3);
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("AlarmHelper", "getEndTimeFromAlarmMinute - currentSecond=" + instance.get(13));
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AlarmHelper", "getEndTimeFromAlarmMinute - alarmEnd=" + new Date(currentTimeMillis));
            return currentTimeMillis;
        }

        @DexIgnore
        public final boolean a(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            if (alarm.isRepeated() || !alarm.isActive()) {
                return false;
            }
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(bk4.d(alarm.getUpdatedAt()));
            int i = instance.get(9);
            int i2 = instance.get(10);
            if (i == 1 && i2 < 12) {
                i2 += 12;
            }
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "instanceCalendar");
            long timeInMillis = instance2.getTimeInMillis();
            bk4.d(instance2);
            wg6.a((Object) instance2, "DateHelper.getStartOfDay(instanceCalendar)");
            long timeInMillis2 = timeInMillis - instance2.getTimeInMillis();
            long millisecond = alarm.getMillisecond();
            if ((((long) ((((i2 * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000) + 1 <= millisecond && timeInMillis2 > millisecond) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1", f = "AlarmHelper.kt", l = {210}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $currentMinute;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AlarmHelper alarmHelper, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmHelper;
            this.$currentMinute = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$currentMinute, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0047  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0085 A[EDGE_INSN: B:22:0x0085->B:19:0x0085 ?: BREAK  , SYNTHETIC] */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.ff6.a()
                int r1 = r8.label
                r2 = 1
                if (r1 == 0) goto L_0x0028
                if (r1 != r2) goto L_0x0020
                java.lang.Object r1 = r8.L$3
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r3 = r8.L$2
                com.portfolio.platform.data.source.local.alarm.Alarm r3 = (com.portfolio.platform.data.source.local.alarm.Alarm) r3
                java.lang.Object r4 = r8.L$1
                java.util.List r4 = (java.util.List) r4
                java.lang.Object r5 = r8.L$0
                com.fossil.il6 r5 = (com.fossil.il6) r5
                com.fossil.nc6.a((java.lang.Object) r9)
                r9 = r8
                goto L_0x0076
            L_0x0020:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x0028:
                com.fossil.nc6.a((java.lang.Object) r9)
                com.fossil.il6 r9 = r8.p$
                com.portfolio.platform.helper.AlarmHelper r1 = r8.this$0
                com.portfolio.platform.data.source.AlarmsRepository r1 = r1.b()
                java.util.List r1 = r1.getActiveAlarms()
                if (r1 == 0) goto L_0x0085
                java.util.Iterator r3 = r1.iterator()
                r5 = r9
                r4 = r1
                r1 = r3
                r9 = r8
            L_0x0041:
                boolean r3 = r1.hasNext()
                if (r3 == 0) goto L_0x0085
                java.lang.Object r3 = r1.next()
                com.portfolio.platform.data.source.local.alarm.Alarm r3 = (com.portfolio.platform.data.source.local.alarm.Alarm) r3
                int r6 = r3.getTotalMinutes()
                int r7 = r9.$currentMinute
                if (r6 != r7) goto L_0x0041
                boolean r6 = r3.isRepeated()
                if (r6 != 0) goto L_0x0041
                r6 = 0
                r3.setActive(r6)
                com.portfolio.platform.helper.AlarmHelper r6 = r9.this$0
                com.portfolio.platform.data.source.AlarmsRepository r6 = r6.b()
                r9.L$0 = r5
                r9.L$1 = r4
                r9.L$2 = r3
                r9.L$3 = r1
                r9.label = r2
                java.lang.Object r6 = r6.updateAlarm(r3, r9)
                if (r6 != r0) goto L_0x0076
                return r0
            L_0x0076:
                com.portfolio.platform.PortfolioApp$inner r6 = com.portfolio.platform.PortfolioApp.get
                com.fossil.hi4 r7 = new com.fossil.hi4
                java.lang.String r3 = r3.getUri()
                r7.<init>(r2, r3)
                r6.a((java.lang.Object) r7)
                goto L_0x0041
            L_0x0085:
                com.fossil.cd6 r9 = com.fossil.cd6.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AlarmHelper.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1", f = "AlarmHelper.kt", l = {}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(AlarmHelper alarmHelper, Context context, xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmHelper;
            this.$context = context;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$context, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:136:0x0231  */
        /* JADX WARNING: Removed duplicated region for block: B:138:0x023f  */
        /* JADX WARNING: Removed duplicated region for block: B:75:0x0150  */
        public final Object invokeSuspend(Object obj) {
            T t;
            T t2;
            long j;
            Integer num;
            long intValue;
            int hour;
            int minute;
            Integer num2;
            long j2;
            int i;
            int i2;
            int i3;
            int i4;
            int i5;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                Calendar instance = Calendar.getInstance();
                bk4.d(instance);
                wg6.a((Object) instance, "DateHelper.getStartOfDay(Calendar.getInstance())");
                long timeInMillis = instance.getTimeInMillis();
                int i6 = (Calendar.getInstance().get(11) * 60) + Calendar.getInstance().get(12);
                int i7 = Calendar.getInstance().get(7);
                List<Alarm> inComingActiveAlarms = this.this$0.b().getInComingActiveAlarms();
                Iterator<T> it = inComingActiveAlarms.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    Alarm alarm = (Alarm) t;
                    if (hf6.a(alarm != null && alarm.getTotalMinutes() > i6).booleanValue()) {
                        break;
                    }
                }
                Alarm alarm2 = (Alarm) t;
                Iterator<T> it2 = inComingActiveAlarms.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    Alarm alarm3 = (Alarm) t2;
                    if (hf6.a(alarm3 != null && alarm3.getTotalMinutes() <= i6).booleanValue()) {
                        break;
                    }
                }
                Alarm alarm4 = (Alarm) t2;
                if (alarm2 != null) {
                    if (alarm2.getDays() != null) {
                        int[] days = alarm2.getDays();
                        if (days != null) {
                            if (days.length == 0) {
                                i5 = alarm2.getHour() * 60;
                                i4 = alarm2.getMinute();
                            } else {
                                int[] days2 = alarm2.getDays();
                                if (days2 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (nd6.a(days2, i7)) {
                                    i5 = alarm2.getHour() * 60;
                                    i4 = alarm2.getMinute();
                                }
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        i5 = alarm2.getHour() * 60;
                        i4 = alarm2.getMinute();
                    }
                    j = ((long) ((i5 + i4) * 60 * 1000)) + timeInMillis;
                    if (j == -1) {
                        Iterator<Alarm> it3 = inComingActiveAlarms.iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                break;
                            }
                            Alarm next = it3.next();
                            if (!(next == null || next.getDays() == null)) {
                                int[] days3 = next.getDays();
                                if (days3 != null) {
                                    if (!(!(days3.length == 0))) {
                                        continue;
                                    } else {
                                        int[] days4 = next.getDays();
                                        if (days4 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (nd6.a(days4, i7)) {
                                            continue;
                                        } else {
                                            int[] days5 = next.getDays();
                                            if (days5 != null) {
                                                int length = days5.length;
                                                int i8 = 0;
                                                while (true) {
                                                    if (i8 >= length) {
                                                        num = null;
                                                        break;
                                                    }
                                                    int i9 = days5[i8];
                                                    if (hf6.a(i7 < hf6.a(i9).intValue()).booleanValue()) {
                                                        num = hf6.a(i9);
                                                        break;
                                                    }
                                                    i8++;
                                                }
                                                if (num != null) {
                                                    intValue = timeInMillis + (((long) (num.intValue() - i7)) * LogBuilder.MAX_INTERVAL);
                                                    hour = next.getHour() * 60;
                                                    minute = next.getMinute();
                                                    break;
                                                }
                                                int[] days6 = next.getDays();
                                                if (days6 != null) {
                                                    int length2 = days6.length;
                                                    int i10 = 0;
                                                    while (true) {
                                                        if (i10 >= length2) {
                                                            num2 = null;
                                                            break;
                                                        }
                                                        int i11 = days6[i10];
                                                        if (hf6.a(i7 > hf6.a(i11).intValue()).booleanValue()) {
                                                            num2 = hf6.a(i11);
                                                            break;
                                                        }
                                                        i10++;
                                                    }
                                                    if (num2 != null) {
                                                        intValue = timeInMillis + (((long) (i7 - num2.intValue())) * LogBuilder.MAX_INTERVAL);
                                                        hour = next.getHour() * 60;
                                                        minute = next.getMinute();
                                                        break;
                                                    }
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                        }
                        j = intValue + ((long) ((hour + (minute - 1)) * 60 * 1000));
                    }
                    if (j != -1) {
                        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler - All alarms got active");
                        return cd6.a;
                    }
                    MFUser currentUser = this.this$0.c().getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getUserId())) {
                        return cd6.a;
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
                    bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
                    Intent intent = new Intent(this.$context, AlarmReceiver.class);
                    intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
                    intent.putExtras(bundle);
                    AlarmManager alarmManager = (AlarmManager) this.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
                    if (alarmManager != null) {
                        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler - setExactAlarm at " + new Date(j));
                        alarmManager.setExact(0, j, PendingIntent.getBroadcast(this.$context, 101, intent, 134217728));
                    }
                    return cd6.a;
                } else if (alarm4 != null) {
                    if (alarm4.getDays() != null) {
                        int[] days7 = alarm4.getDays();
                        if (days7 != null) {
                            if (days7.length == 0) {
                                j2 = timeInMillis + LogBuilder.MAX_INTERVAL;
                                i = (alarm4.getHour() * 60) + alarm4.getMinute();
                                j = j2 + ((long) (i * 60 * 1000));
                                if (j == -1) {
                                }
                                if (j != -1) {
                                }
                            } else {
                                int[] days8 = alarm4.getDays();
                                if (days8 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (nd6.a(days8, i7)) {
                                    j2 = timeInMillis + LogBuilder.MAX_INTERVAL;
                                    i3 = alarm4.getHour() * 60;
                                    i2 = alarm4.getMinute();
                                }
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        j2 = timeInMillis + LogBuilder.MAX_INTERVAL;
                        i3 = alarm4.getHour() * 60;
                        i2 = alarm4.getMinute();
                    }
                    i = i3 + i2;
                    j = j2 + ((long) (i * 60 * 1000));
                    if (j == -1) {
                    }
                    if (j != -1) {
                    }
                }
                j = -1;
                if (j == -1) {
                }
                if (j != -1) {
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.helper.AlarmHelper", f = "AlarmHelper.kt", l = {266}, m = "validateAlarms")
    public static final class d extends jf6 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public long J$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(AlarmHelper alarmHelper, xe6 xe6) {
            super(xe6);
            this.this$0 = alarmHelper;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    @DexIgnore
    public AlarmHelper(an4 an4, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.a = an4;
        this.b = userRepository;
        this.c = alarmsRepository;
    }

    @DexIgnore
    public final void a() {
        int i = (Calendar.getInstance().get(11) * 60) + Calendar.getInstance().get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "endJobScheduler - currentMinute=" + i);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, i, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final AlarmsRepository b() {
        return this.c;
    }

    @DexIgnore
    public final UserRepository c() {
        return this.b;
    }

    @DexIgnore
    public final void d(Context context) {
        wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler");
        a(context);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, context, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void e(Context context) {
        wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startRemindSyncAlarm");
        Calendar instance = Calendar.getInstance();
        if (Calendar.getInstance().get(11) >= 15) {
            instance.add(6, 1);
        }
        instance.set(11, 15);
        instance.set(12, 0);
        instance.set(13, 0);
        Bundle bundle = new Bundle();
        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 2);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtras(bundle);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 20, intent, 268435456);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            wg6.a((Object) instance, "calendar");
            alarmManager.setRepeating(0, instance.getTimeInMillis(), LogBuilder.MAX_INTERVAL, broadcast);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "schedule SyncAlarm at = " + new Date(instance.getTimeInMillis()));
        }
    }

    @DexIgnore
    public final void f(Context context) {
        wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm");
        this.a.u("");
        Intent intent = new Intent(context, AlarmReceiver.class);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 0);
        intent2.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 134217728);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 1, intent2, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            alarmManager.cancel(broadcast);
            alarmManager.cancel(broadcast2);
            FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm success");
        }
    }

    @DexIgnore
    public final void b(Context context) {
        wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startExactReplaceBatteryAlarm");
        Calendar instance = Calendar.getInstance();
        instance.set(11, 9);
        instance.set(12, 0);
        instance.set(13, 0);
        wg6.a((Object) instance, "triggerCalendar");
        long timeInMillis = instance.getTimeInMillis();
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 1, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                alarmManager.setExactAndAllowWhileIdle(0, timeInMillis, broadcast);
            } else {
                alarmManager.setExact(0, timeInMillis, broadcast);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "startExactReplaceBatteryAlarm - triggerAtMillis=" + timeInMillis);
        }
    }

    @DexIgnore
    public final void c(Context context) {
        wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startHWLogScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("REQUEST_CODE", 10);
            PendingIntent broadcast = PendingIntent.getBroadcast(context, 10, intent, 134217728);
            alarmManager.cancel(broadcast);
            Calendar instance = Calendar.getInstance();
            instance.set(11, 10);
            instance.set(12, 0);
            instance.set(13, 0);
            wg6.a((Object) instance, "triggerCalendar");
            long timeInMillis = instance.getTimeInMillis();
            alarmManager.setInexactRepeating(0, timeInMillis, LogBuilder.MAX_INTERVAL, broadcast);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "startHWLogScheduler - setInexactRepeating: triggerAtMillis=" + timeInMillis + ", interval=" + LogBuilder.MAX_INTERVAL);
        }
    }

    @DexIgnore
    public final void a(Context context) {
        wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "cancelJobScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 101, intent, 134217728);
        if (!(broadcast == null || alarmManager == null)) {
            alarmManager.cancel(broadcast);
        }
        AlarmManager alarmManager2 = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent2.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 102, intent2, 134217728);
        if (alarmManager2 != null) {
            alarmManager2.cancel(broadcast2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x009a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public final Object a(xe6<? super cd6> xe6) {
        d dVar;
        int i;
        AlarmHelper alarmHelper;
        long j;
        List<Alarm> list;
        Iterable iterable;
        Iterable iterable2;
        String str;
        Iterator it;
        Iterator it2;
        String str2;
        Alarm alarm;
        Iterator it3;
        long j2;
        long j3;
        boolean z;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof d) {
            dVar = (d) xe62;
            int i2 = dVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dVar.label = i2 - Integer.MIN_VALUE;
                Object obj = dVar.result;
                Object a2 = ff6.a();
                i = dVar.label;
                String str3 = "AlarmHelper";
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(str3, "validateAlarms()");
                    Calendar instance = Calendar.getInstance();
                    bk4.d(instance);
                    wg6.a((Object) instance, "DateHelper.getStartOfDay(Calendar.getInstance())");
                    long timeInMillis = instance.getTimeInMillis();
                    List<Alarm> inComingActiveAlarms = this.c.getInComingActiveAlarms();
                    ArrayList arrayList = new ArrayList();
                    for (T next : inComingActiveAlarms) {
                        Alarm alarm2 = (Alarm) next;
                        if ((alarm2 != null ? alarm2.getDays() : null) != null) {
                            int[] days = alarm2.getDays();
                            if (days != null) {
                                if (!(days.length == 0)) {
                                    z = true;
                                    if (hf6.a(z).booleanValue()) {
                                        arrayList.add(next);
                                    }
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        z = false;
                        if (hf6.a(z).booleanValue()) {
                        }
                    }
                    alarmHelper = this;
                    list = inComingActiveAlarms;
                    j = timeInMillis;
                    it2 = arrayList.iterator();
                    iterable2 = arrayList;
                    iterable = iterable2;
                } else if (i == 1) {
                    long j4 = dVar.J$2;
                    long j5 = dVar.J$1;
                    Calendar calendar = (Calendar) dVar.L$10;
                    Calendar calendar2 = (Calendar) dVar.L$9;
                    alarm = (Alarm) dVar.L$8;
                    ih6 ih6 = (ih6) dVar.L$7;
                    Alarm alarm3 = (Alarm) dVar.L$6;
                    Object obj2 = dVar.L$5;
                    iterable2 = (Iterable) dVar.L$3;
                    iterable = (List) dVar.L$2;
                    list = (List) dVar.L$1;
                    j = dVar.J$0;
                    alarmHelper = (AlarmHelper) dVar.L$0;
                    nc6.a(obj);
                    str2 = str3;
                    it = (Iterator) dVar.L$4;
                    str = str2;
                    FLogger.INSTANCE.getLocal().d(str, "validateAlarms() - alarm is updated (" + alarm + ')');
                    it2 = it;
                    str3 = str;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (!it2.hasNext()) {
                    Object next2 = it2.next();
                    alarm = (Alarm) next2;
                    ih6 ih62 = new ih6();
                    Iterator it4 = it2;
                    ih62.element = -1;
                    if (alarm != null) {
                        Calendar instance2 = Calendar.getInstance();
                        wg6.a((Object) instance2, "updateTimeCal");
                        instance2.setTime(bk4.d(alarm.getUpdatedAt()));
                        Calendar instance3 = Calendar.getInstance();
                        wg6.a((Object) instance3, "startTimeCal");
                        instance3.setTime(bk4.d(alarm.getUpdatedAt()));
                        bk4.d(instance3);
                        wg6.a((Object) instance3, "DateHelper.getStartOfDay(startTimeCal)");
                        long timeInMillis2 = instance3.getTimeInMillis();
                        str2 = str3;
                        Calendar calendar3 = instance3;
                        Object obj3 = next2;
                        long hour = ((long) (((alarm.getHour() * 60) + alarm.getMinute()) * 60 * 1000)) + timeInMillis2;
                        if (Math.abs(j - timeInMillis2) > LogBuilder.MAX_INTERVAL) {
                            j2 = hour;
                            alarm.getHour();
                            alarm.getMinute();
                        } else {
                            if (instance2.getTimeInMillis() >= hour) {
                                j2 = hour;
                                j3 = timeInMillis2 + LogBuilder.MAX_INTERVAL + ((long) (((alarm.getHour() * 60) + alarm.getMinute()) * 60 * 1000));
                            } else {
                                j2 = hour;
                                j3 = timeInMillis2 + ((long) (((alarm.getHour() * 60) + alarm.getMinute()) * 60 * 1000));
                            }
                            ih62.element = j3;
                        }
                        if (ih62.element != -1) {
                            Calendar instance4 = Calendar.getInstance();
                            wg6.a((Object) instance4, "Calendar.getInstance()");
                            long j6 = timeInMillis2;
                            if (instance4.getTimeInMillis() >= ih62.element) {
                                alarm.setActive(false);
                                AlarmsRepository alarmsRepository = alarmHelper.c;
                                dVar.L$0 = alarmHelper;
                                dVar.J$0 = j;
                                dVar.L$1 = list;
                                dVar.L$2 = iterable;
                                dVar.L$3 = iterable2;
                                it = it4;
                                dVar.L$4 = it;
                                dVar.L$5 = obj3;
                                dVar.L$6 = alarm;
                                dVar.L$7 = ih62;
                                dVar.L$8 = alarm;
                                dVar.L$9 = instance2;
                                dVar.L$10 = calendar3;
                                dVar.J$1 = j6;
                                dVar.J$2 = j2;
                                dVar.label = 1;
                                if (alarmsRepository.updateAlarm(alarm, dVar) == a2) {
                                    return a2;
                                }
                                str = str2;
                                FLogger.INSTANCE.getLocal().d(str, "validateAlarms() - alarm is updated (" + alarm + ')');
                                it2 = it;
                                str3 = str;
                                if (!it2.hasNext()) {
                                }
                            }
                        }
                        it3 = it4;
                        str = str2;
                    } else {
                        str = str3;
                        it3 = it4;
                    }
                    it2 = it;
                    str3 = str;
                    if (!it2.hasNext()) {
                    }
                }
                return cd6.a;
            }
        }
        dVar = new d(this, xe62);
        Object obj4 = dVar.result;
        Object a22 = ff6.a();
        i = dVar.label;
        String str32 = "AlarmHelper";
        if (i != 0) {
        }
        if (!it2.hasNext()) {
        }
        return cd6.a;
    }
}
