package com.portfolio.platform.helper;

import android.os.Bundle;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hl4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il4;
import com.fossil.il6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.kl4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mj6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zm4;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerSetting;
import com.zendesk.sdk.network.impl.DeviceInfo;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AnalyticsHelper {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static FirebaseAnalytics c;
    @DexIgnore
    public static AnalyticsHelper d;
    @DexIgnore
    public static /* final */ HashMap<String, jl4> e; // = new HashMap<>();
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public volatile String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(AnalyticsHelper analyticsHelper) {
            AnalyticsHelper.d = analyticsHelper;
        }

        @DexIgnore
        public final jl4 b(String str) {
            wg6.b(str, "traceName");
            AnalyticsHelper c = c();
            if (wg6.a((Object) "view_appearance", (Object) str)) {
                return new kl4(c, str, c.a());
            }
            return new jl4(c, str, c.a());
        }

        @DexIgnore
        public final jl4 c(String str) {
            wg6.b(str, "tracingKey");
            return d().get(str);
        }

        @DexIgnore
        public final HashMap<String, jl4> d() {
            return AnalyticsHelper.e;
        }

        @DexIgnore
        public final AnalyticsHelper e() {
            return AnalyticsHelper.d;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(String str, jl4 jl4) {
            wg6.b(str, "tracingKey");
            wg6.b(jl4, "trace");
            d().put(str, jl4);
        }

        @DexIgnore
        public final synchronized AnalyticsHelper c() {
            AnalyticsHelper e;
            if (e() == null) {
                a(new AnalyticsHelper((qg6) null));
            }
            e = e();
            if (e == null) {
                wg6.a();
                throw null;
            }
            return e;
        }

        @DexIgnore
        public final String d(String str) {
            wg6.b(str, "input");
            return new mj6("[^a-zA-Z0-9]+").replace((CharSequence) str, "_");
        }

        @DexIgnore
        public final void e(String str) {
            wg6.b(str, "tracingKey");
            d().remove(str);
        }

        @DexIgnore
        public final hl4 a(String str) {
            wg6.b(str, "eventName");
            return new hl4(c(), str);
        }

        @DexIgnore
        public final il4 a() {
            return new il4(c());
        }

        @DexIgnore
        public final kl4 b() {
            AnalyticsHelper c = c();
            return new kl4(c, "view_appearance", c.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1", f = "AnalyticsHelper.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bundle $data;
        @DexIgnore
        public /* final */ /* synthetic */ String $event;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Bundle bundle, xe6 xe6) {
            super(2, xe6);
            this.$event = str;
            this.$data = bundle;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$event, this.$data, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                FirebaseAnalytics d = AnalyticsHelper.c;
                if (d != null) {
                    d.a(this.$event, this.$data);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = AnalyticsHelper.class.getSimpleName();
        wg6.a((Object) simpleName, "AnalyticsHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public AnalyticsHelper() {
        c = FirebaseAnalytics.getInstance(PortfolioApp.get.instance());
    }

    @DexIgnore
    public final void b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "User id: " + str);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(str);
        }
        this.a = str;
    }

    @DexIgnore
    public final void c(String str, String str2, int i) {
        wg6.b(str, "styleNumber");
        wg6.b(str2, "deviceName");
        a("sync_success", (Map<String, ? extends Object>) a(str, str2, i));
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        b("Auth", "None");
    }

    @DexIgnore
    public /* synthetic */ AnalyticsHelper(qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void a(boolean z) {
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(z);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        b("Optin_Usage", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wg6.b(str, Constants.EVENT);
        wg6.b(str2, ServerSetting.VALUE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "Inside .analyticLogEvent event=" + str + ", value=" + str2);
        Bundle bundle = new Bundle();
        bundle.putString(str, str2);
        a(str, bundle);
    }

    @DexIgnore
    public final boolean b() {
        MFUser b2 = zm4.p.a().n().b();
        return b2 != null && b2.isDiagnosticEnabled();
    }

    @DexIgnore
    public final void b(String str, String str2, String str3) {
        wg6.b(str, Constants.EVENT);
        wg6.b(str2, "paramName");
        wg6.b(str3, "paramValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = b;
        local.d(str4, "Inside .analyticLogEvent event=" + str + ", name=" + str2 + ", value=" + str3);
        Bundle bundle = new Bundle();
        bundle.putString(str2, str3);
        a(str, bundle);
    }

    @DexIgnore
    public final void a(String str, Map<String, ? extends Object> map) {
        wg6.b(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str + ", value=" + map);
        if (map != null && (!map.isEmpty())) {
            Bundle bundle = new Bundle();
            for (Map.Entry next : map.entrySet()) {
                String str3 = (String) next.getKey();
                Object value = next.getValue();
                if (value instanceof Integer) {
                    bundle.putInt(str3, ((Number) value).intValue());
                } else if (value != null) {
                    bundle.putString(str3, (String) value);
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.String");
                }
            }
            a(str, bundle);
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        wg6.b(str, "propertyName");
        wg6.b(str2, ServerSetting.VALUE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "User property " + str + " with value=" + str2);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(str, str2);
        }
    }

    @DexIgnore
    public final void b(String str, String str2, int i) {
        wg6.b(str, "styleNumber");
        wg6.b(str2, "deviceName");
        a("sync_error", (Map<String, ? extends Object>) a(str, str2, i));
    }

    @DexIgnore
    public final void b(boolean z) {
        b("Optin_Emails", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str);
        a(str, (Bundle) null);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(str, bundle, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        if (map != null && (!map.isEmpty())) {
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                String str2 = (String) next.getValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local.d(str3, "User property " + str + " with value=" + str2);
                FirebaseAnalytics firebaseAnalytics = c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.a(str, str2);
                }
            }
        }
    }

    @DexIgnore
    public final Map<String, String> a(String str, String str2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("Style_Number", str);
        hashMap.put("Device_Name", str2);
        hashMap.put("Type", String.valueOf(i));
        return hashMap;
    }

    @DexIgnore
    public final void a(int i, SKUModel sKUModel) {
        if (sKUModel != null) {
            String sku = sKUModel.getSku();
            if (sku != null) {
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                a("sync_start", (Map<String, ? extends Object>) a(sku, deviceName, i));
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        wg6.b(str, "serialPrefix");
        wg6.b(str2, "activeDeviceName");
        wg6.b(str3, "firmwareVersion");
        il4 a2 = f.a();
        a2.a("serial_number_prefix", str);
        a2.a(DeviceInfo.DEVICE_INFO_DEVICE_NAME, str2);
        a2.a(Constants.FIRMWARE_VERSION, str3);
        a2.a();
    }
}
