package com.portfolio.platform.helper;

import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.nu3;
import com.fossil.ou3;
import com.fossil.pu3;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PinObject;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConverterShortDate implements hu3<Date>, pu3<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, ou3 ou3) {
        String str;
        wg6.b(type, "typeOfSrc");
        wg6.b(ou3, "context");
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = bk4.a.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                wg6.a((Object) str, "DateHelper.SHORT_DATE_FO\u2026ATTER.get()!!.format(src)");
            } else {
                wg6.a();
                throw null;
            }
        }
        return new nu3(str);
    }

    @DexIgnore
    public Date deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        wg6.b(jsonElement, PinObject.COLUMN_JSON_DATA);
        wg6.b(type, "typeOfT");
        wg6.b(gu3, "context");
        String f = jsonElement.f();
        if (f != null) {
            try {
                SimpleDateFormat simpleDateFormat = bk4.a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    if (parse != null) {
                        return parse;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(f);
                sb.append(", e=");
                e.printStackTrace();
                sb.append(cd6.a);
                local.e("GsonConverterShortDate", sb.toString());
            }
        }
        return new Date(0);
    }
}
