package com.portfolio.platform.helper;

import com.fossil.bk4;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.nu3;
import com.fossil.ou3;
import com.fossil.pu3;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PinObject;
import java.lang.reflect.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTime implements hu3<DateTime>, pu3<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(DateTime dateTime, Type type, ou3 ou3) {
        String str;
        wg6.b(type, "typeOfSrc");
        wg6.b(ou3, "context");
        if (dateTime == null) {
            str = "";
        } else {
            str = bk4.a(DateTimeZone.UTC, dateTime);
            wg6.a((Object) str, "DateHelper.printServerDa\u2026at(DateTimeZone.UTC, src)");
        }
        return new nu3(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0039 */
    public DateTime deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        wg6.b(jsonElement, PinObject.COLUMN_JSON_DATA);
        wg6.b(type, "typeOfT");
        wg6.b(gu3, "context");
        String f = jsonElement.f();
        wg6.a((Object) f, "dateAsString");
        if (f.length() == 0) {
            return new DateTime(0);
        }
        DateTime a = bk4.a(DateTimeZone.getDefault(), f);
        wg6.a((Object) a, "DateHelper.getServerDate\u2026tDefault(), dateAsString)");
        try {
            DateTime b = bk4.b(jsonElement.f());
            wg6.a((Object) b, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
            return b;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GsonConvertDateTime", "deserialize - json=" + jsonElement.f() + ", e=" + e);
            e.printStackTrace();
            return new DateTime(0);
        }
    }
}
