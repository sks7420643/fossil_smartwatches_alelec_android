package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LogcatActivity extends BaseActivity {
    @DexIgnore
    public static boolean E;
    @DexIgnore
    public c B;
    @DexIgnore
    public RecyclerView C;
    @DexIgnore
    public AsyncTask<Void, Void, Void> D;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements AdapterView.OnItemSelectedListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            LogcatActivity.this.a(i);
        }

        @DexIgnore
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.g<d> {
        @DexIgnore
        public /* final */ List<String> a; // = new ArrayList();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String a;

            @DexIgnore
            public a(String str) {
                this.a = str;
            }

            @DexIgnore
            public void run() {
                if (!c.this.a.contains(this.a)) {
                    c.this.a.add(0, this.a);
                    c.this.notifyItemInserted(0);
                    LogcatActivity.this.C.scrollToPosition(0);
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        /* renamed from: a */
        public void onBindViewHolder(d dVar, int i) {
            String str = this.a.get(i);
            if (str != null && dVar != null) {
                dVar.a.setText(Html.fromHtml(str));
            }
        }

        @DexIgnore
        public void c() {
            this.a.clear();
            notifyDataSetChanged();
        }

        @DexIgnore
        public int getItemCount() {
            return this.a.size();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v1, types: [android.content.Context, com.portfolio.platform.ui.debug.LogcatActivity] */
        public d onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new d(LogcatActivity.this, LayoutInflater.from(LogcatActivity.this).inflate(2131558698, viewGroup, false));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.ui.debug.LogcatActivity, android.app.Activity] */
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.debug.LogcatActivity, android.app.Activity] */
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.ui.debug.LogcatActivity, android.app.Activity] */
        public void a(String str) {
            if (!LogcatActivity.this.isFinishing() && !LogcatActivity.this.isDestroyed()) {
                LogcatActivity.this.runOnUiThread(new a(str));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;

        @DexIgnore
        public d(LogcatActivity logcatActivity, View view) {
            super(view);
            this.a = (TextView) view.findViewById(2131363174);
        }
    }

    @DexIgnore
    public static void a(Context context) {
        context.startActivity(new Intent(context, LogcatActivity.class));
    }

    @DexIgnore
    public static String c(String str) {
        Pattern compile = Pattern.compile("(E/[A-Za-z0-9_-]+)");
        Pattern compile2 = Pattern.compile("(I/[A-Za-z0-9_-]+)");
        Pattern compile3 = Pattern.compile("(W/[A-Za-z0-9_-]+)");
        StringBuffer stringBuffer = new StringBuffer(str.length());
        Matcher matcher = compile.matcher(str);
        while (matcher.find()) {
            matcher.appendReplacement(stringBuffer, "<font color=\"#d8152a\">" + matcher.group(1) + "</font>");
        }
        matcher.appendTail(stringBuffer);
        Matcher matcher2 = compile2.matcher(stringBuffer);
        StringBuffer stringBuffer2 = new StringBuffer(str.length());
        while (matcher2.find()) {
            matcher2.appendReplacement(stringBuffer2, "<font color=\"#46a924\">" + matcher2.group(1) + "</font>");
        }
        matcher2.appendTail(stringBuffer2);
        Matcher matcher3 = compile3.matcher(stringBuffer2);
        StringBuffer stringBuffer3 = new StringBuffer(str.length());
        while (matcher3.find()) {
            matcher3.appendReplacement(stringBuffer3, "<font color=\"#f0da23\">" + matcher3.group(1) + "</font>");
        }
        matcher3.appendTail(stringBuffer3);
        return stringBuffer3.toString();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.ui.debug.LogcatActivity, com.portfolio.platform.ui.BaseActivity, androidx.appcompat.app.AppCompatActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558432);
        Spinner spinner = (Spinner) findViewById(2131362962);
        spinner.getBackground().setColorFilter(Color.parseColor("#AA7744"), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter<CharSequence> createFromResource = ArrayAdapter.createFromResource(this, 2130903041, 17367048);
        createFromResource.setDropDownViewResource(17367049);
        spinner.setAdapter(createFromResource);
        spinner.setOnItemSelectedListener(new a());
        this.C = findViewById(2131362697);
        this.C.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.k(1);
        this.C.setLayoutManager(linearLayoutManager);
        this.B = new c();
        this.C.setAdapter(this.B);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        AsyncTask<Void, Void, Void> asyncTask = this.D;
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.ui.debug.LogcatActivity] */
    public void a(int i) {
        if (!E) {
            this.B.c();
            E = true;
            Toast.makeText(this, "Collecting log...", 0).show();
            this.D = new b(i, this.B);
            this.D.execute(new Void[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ c b;

        @DexIgnore
        public b(int i, c cVar) {
            this.a = i;
            this.b = cVar;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            int i;
            String str = "";
            try {
                if (this.a == 0) {
                    str = "logcat -v time -d " + PortfolioApp.T.getPackageName() + ":V";
                } else if (this.a == 1) {
                    str = "logcat -v time -d ButtonService:V BaseProfile:V TrackerProfile:V LinkProfile:V *:S";
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(str).getInputStream()));
                StringBuilder sb = new StringBuilder();
                loop0:
                while (true) {
                    i = 0;
                    do {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break loop0;
                        }
                        i++;
                        sb.insert(0, readLine + "<br>");
                    } while (i < 50);
                    this.b.a(LogcatActivity.c(sb.toString()));
                    sb.setLength(0);
                    sb = new StringBuilder();
                }
                if (i > 0) {
                    this.b.a(LogcatActivity.c(sb.toString()));
                }
                Runtime.getRuntime().exec("logcat -c");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Void voidR) {
            LogcatActivity.E = false;
        }
    }
}
