package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DebugClearDataWarningActivity extends AppCompatActivity {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a")
        /* renamed from: com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a  reason: collision with other inner class name */
        public class C0064a implements Runnable {
            @DexIgnore
            public C0064a(a aVar) {
            }

            @DexIgnore
            public void run() {
                PortfolioApp.T.a();
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v1, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugClearDataWarningActivity] */
        public void onClick(View view) {
            Toast.makeText(DebugClearDataWarningActivity.this, "The app is being restarted...", 1).show();
            new Handler().postDelayed(new C0064a(this), 1500);
        }
    }

    @DexIgnore
    public static void a(Context context) {
        Intent intent = new Intent(context, DebugClearDataWarningActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        DebugClearDataWarningActivity.super.onCreate(bundle);
        setContentView(2131558431);
        ((Button) findViewById(2131361959)).setOnClickListener(new a());
    }
}
