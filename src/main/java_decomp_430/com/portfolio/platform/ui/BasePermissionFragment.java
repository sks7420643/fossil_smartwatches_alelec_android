package com.portfolio.platform.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.kc6;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.uh4;
import com.fossil.wg6;
import com.fossil.xx5;
import com.fossil.yq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BasePermissionFragment extends BaseFragment implements AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public BlockingQueue<uh4> f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<uh4> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        /* renamed from: a */
        public final int compare(uh4 uh4, uh4 uh42) {
            return uh4.ordinal() - uh42.ordinal();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = BasePermissionFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "BasePermissionFragment::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public final void a(uh4 uh4) {
        BlockingQueue<uh4> blockingQueue = this.f;
        if (blockingQueue == null) {
            wg6.d("mPermissionQueue");
            throw null;
        } else if (!blockingQueue.contains(uh4)) {
            BlockingQueue<uh4> blockingQueue2 = this.f;
            if (blockingQueue2 != null) {
                blockingQueue2.offer(uh4);
            } else {
                wg6.d("mPermissionQueue");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean j1() {
        boolean a2 = xx5.a.a((Context) PortfolioApp.get.instance());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenBackgroundLocationPermission() - isBackgroundLocationPermissionGranted = " + a2);
        if (!a2) {
            v1();
        }
        return !a2;
    }

    @DexIgnore
    public final boolean k1() {
        boolean c = xx5.a.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenBluetoothPermission() - isBluetoothEnabled = " + c);
        if (!c) {
            w1();
        }
        return !c;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean l1() {
        boolean b2 = xx5.a.b(PortfolioApp.get.instance());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenLocationPermission() - isLocationPermissionGranted = " + b2);
        if (!b2) {
            x1();
        }
        return !b2;
    }

    @DexIgnore
    public final boolean m1() {
        boolean d = xx5.a.d();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenLocationService() - isLocationOpen = " + d);
        if (!d) {
            y1();
        }
        return !d;
    }

    @DexIgnore
    public final void n1() {
        startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
    }

    @DexIgnore
    public final void o1() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = new PriorityBlockingQueue(5, b.a);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        wg6.b(strArr, "permissions");
        wg6.b(iArr, "grantResults");
        if (i != 0) {
            super.onRequestPermissionsResult(i, strArr, iArr);
            return;
        }
        boolean z = true;
        if (!(!(iArr.length == 0)) || iArr[0] != 0) {
            z = false;
        }
        if (z) {
            a(new uh4[0]);
        } else {
            s1();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void p1() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", PortfolioApp.get.instance().getPackageName(), (String) null));
        startActivity(intent);
    }

    @DexIgnore
    public abstract void q1();

    @DexIgnore
    public abstract void r1();

    @DexIgnore
    public abstract void s1();

    @DexIgnore
    public abstract void t1();

    @DexIgnore
    public abstract void u1();

    @DexIgnore
    public final void v1() {
        BlockingQueue<uh4> blockingQueue = this.f;
        if (blockingQueue != null) {
            uh4 uh4 = (uh4) blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + uh4);
            if (!isActive()) {
                return;
            }
            if (uh4 == null || uh4 != uh4.BACKGROUND_LOCATION_PERMISSION_OFF) {
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.L(childFragmentManager);
                return;
            }
            lx5 lx52 = lx5.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            wg6.a((Object) childFragmentManager2, "childFragmentManager");
            lx52.J(childFragmentManager2);
            return;
        }
        wg6.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void w1() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.e(childFragmentManager);
        }
    }

    @DexIgnore
    public final void x1() {
        BlockingQueue<uh4> blockingQueue = this.f;
        if (blockingQueue != null) {
            uh4 uh4 = (uh4) blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + uh4);
            if (!isActive()) {
                return;
            }
            if (uh4 == null || uh4 != uh4.LOCATION_PERMISSION_FEATURE_OFF) {
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.L(childFragmentManager);
                return;
            }
            lx5 lx52 = lx5.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            wg6.a((Object) childFragmentManager2, "childFragmentManager");
            lx52.K(childFragmentManager2);
            return;
        }
        wg6.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void y1() {
        BlockingQueue<uh4> blockingQueue = this.f;
        if (blockingQueue != null) {
            uh4 uh4 = (uh4) blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "requestLocationService() - permissionErrorCode = " + uh4);
            if (!isActive()) {
                return;
            }
            if (uh4 == null || uh4 != uh4.LOCATION_SERVICE_FEATURE_OFF) {
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.A(childFragmentManager);
                return;
            }
            lx5 lx52 = lx5.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            wg6.a((Object) childFragmentManager2, "childFragmentManager");
            lx52.z(childFragmentManager2);
            return;
        }
        wg6.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void a(uh4... uh4Arr) {
        boolean z;
        wg6.b(uh4Arr, "permissionCodes");
        for (uh4 a2 : uh4Arr) {
            a(a2);
        }
        BlockingQueue<uh4> blockingQueue = this.f;
        if (blockingQueue != null) {
            uh4 uh4 = (uh4) blockingQueue.peek();
            FLogger.INSTANCE.getLocal().d(h, "processPermissionPopups() - permissionErrorCode = " + uh4);
            if (uh4 == null) {
                u1();
                return;
            }
            switch (yq4.a[uh4.ordinal()]) {
                case 1:
                    z = k1();
                    break;
                case 2:
                case 3:
                    z = l1();
                    break;
                case 4:
                case 5:
                    z = m1();
                    break;
                case 6:
                    z = j1();
                    break;
                default:
                    throw new kc6();
            }
            if (!z) {
                BlockingQueue<uh4> blockingQueue2 = this.f;
                if (blockingQueue2 != null) {
                    blockingQueue2.remove(uh4);
                    a(new uh4[0]);
                    return;
                }
                wg6.d("mPermissionQueue");
                throw null;
            }
            return;
        }
        wg6.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wg6.b(str, "tag");
        if (wg6.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == 2131362354) {
                t1();
                p1();
            } else if (i != 2131363190) {
                s1();
            } else {
                xx5.a aVar = xx5.a;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    aVar.b(activity, 0);
                    return;
                }
                wg6.a();
                throw null;
            }
        } else if (wg6.a((Object) str, (Object) lx5.c.a())) {
            if (i == 2131362354) {
                t1();
                p1();
            } else if (i != 2131363190) {
                s1();
            } else {
                xx5.a aVar2 = xx5.a;
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    wg6.a((Object) activity2, "activity!!");
                    aVar2.a((Activity) activity2, 1);
                    return;
                }
                wg6.a();
                throw null;
            }
        } else if (wg6.a((Object) str, (Object) "REQUEST_OPEN_LOCATION_SERVICE")) {
            if (i != 2131362354) {
                r1();
                return;
            }
            t1();
            o1();
        } else if (!wg6.a((Object) str, (Object) "BLUETOOTH_OFF")) {
        } else {
            if (i != 2131362354) {
                q1();
                return;
            }
            t1();
            n1();
        }
    }
}
