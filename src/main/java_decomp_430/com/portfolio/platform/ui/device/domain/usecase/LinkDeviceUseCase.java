package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.cp4;
import com.fossil.ed6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.kw5;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mr4;
import com.fossil.n24;
import com.fossil.nc6;
import com.fossil.pr4;
import com.fossil.pr4$p$a;
import com.fossil.qg6;
import com.fossil.qr4;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.ws5;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xs5;
import com.fossil.ys5;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingAuthorizeResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LinkDeviceUseCase extends m24<pr4.h, pr4.j, pr4.i> {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ c r; // = new c((qg6) null);
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public Device f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public boolean h;
    @DexIgnore
    public k i;
    @DexIgnore
    public /* final */ g j; // = new g();
    @DexIgnore
    public /* final */ DeviceRepository k;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase l;
    @DexIgnore
    public /* final */ an4 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ kw5 o;
    @DexIgnore
    public /* final */ cj4 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends j {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public a(String str) {
            wg6.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends j {
        @DexIgnore
        public /* final */ boolean a;

        @DexIgnore
        public b(String str, boolean z) {
            wg6.b(str, "serial");
            this.a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final String a() {
            return LinkDeviceUseCase.q;
        }

        @DexIgnore
        public /* synthetic */ c(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(int i, String str, String str2) {
            super(i, str, str2);
            wg6.b(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, String str2) {
            super(-1, str, str2);
            wg6.b(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends j {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public f(String str) {
            wg6.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements BleCommandResultManager.b {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r12v8, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v11, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v12, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v13, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v14, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v15, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v23, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v31, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v37, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v39, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkDeviceUseCase.r.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + LinkDeviceUseCase.this.j() + ", isSuccess=" + intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1));
            if (!TextUtils.isEmpty(stringExtra)) {
                boolean z = true;
                if (xj6.b(stringExtra, LinkDeviceUseCase.this.h(), true) && communicateMode == CommunicateMode.LINK && LinkDeviceUseCase.this.j()) {
                    switch (qr4.a[serviceActionResult.ordinal()]) {
                        case 1:
                            if (intent.getExtras() == null) {
                                z = false;
                            }
                            if (!ed6.a || z) {
                                LinkDeviceUseCase linkDeviceUseCase = LinkDeviceUseCase.this;
                                Bundle extras = intent.getExtras();
                                if (extras != null) {
                                    String string = extras.getString("device_model", "");
                                    wg6.a((Object) string, "intent.extras!!.getStrin\u2026nstants.DEVICE_MODEL, \"\")");
                                    linkDeviceUseCase.a(string);
                                    Object r12 = LinkDeviceUseCase.this;
                                    if (stringExtra != null) {
                                        r12.a(new a(stringExtra));
                                        LinkDeviceUseCase.this.m();
                                        return;
                                    }
                                    wg6.a();
                                    throw null;
                                }
                                wg6.a();
                                throw null;
                            }
                            throw new AssertionError("Assertion failed");
                        case 2:
                            Object r122 = LinkDeviceUseCase.this;
                            if (stringExtra != null) {
                                r122.a(new f(stringExtra));
                                return;
                            } else {
                                wg6.a();
                                throw null;
                            }
                        case 3:
                            Object r123 = LinkDeviceUseCase.this;
                            if (stringExtra != null) {
                                r123.a(new m(stringExtra, true));
                                return;
                            } else {
                                wg6.a();
                                throw null;
                            }
                        case 4:
                            Object r124 = LinkDeviceUseCase.this;
                            if (stringExtra != null) {
                                r124.a(new m(stringExtra, false));
                                return;
                            } else {
                                wg6.a();
                                throw null;
                            }
                        case 5:
                            Object r125 = LinkDeviceUseCase.this;
                            if (stringExtra != null) {
                                r125.a(new b(stringExtra, true));
                                return;
                            } else {
                                wg6.a();
                                throw null;
                            }
                        case 6:
                            Object r126 = LinkDeviceUseCase.this;
                            if (stringExtra != null) {
                                r126.a(new b(stringExtra, false));
                                return;
                            } else {
                                wg6.a();
                                throw null;
                            }
                        case 7:
                            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.r.a(), "onReceive(), ASK_FOR_LINK_SERVER");
                            if (intent.getExtras() == null) {
                                z = false;
                            }
                            if (!ed6.a || z) {
                                Bundle extras2 = intent.getExtras();
                                if (extras2 != null) {
                                    MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                                    if (misfitDeviceProfile != null) {
                                        LinkDeviceUseCase.this.c(misfitDeviceProfile.getDeviceSerial());
                                        LinkDeviceUseCase.this.b(misfitDeviceProfile.getAddress());
                                        LinkDeviceUseCase.this.a(new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), misfitDeviceProfile.getBatteryLevel(), 50, false, 64, (qg6) null));
                                        Device e = LinkDeviceUseCase.this.e();
                                        if (e != null) {
                                            e.appendAdditionalInfo(misfitDeviceProfile);
                                        }
                                        LinkDeviceUseCase.this.d();
                                        return;
                                    }
                                    Object r127 = LinkDeviceUseCase.this;
                                    String h = r127.h();
                                    if (h != null) {
                                        r127.a(new d(FailureCode.UNKNOWN_ERROR, h, "No device profile"));
                                        PortfolioApp instance = PortfolioApp.get.instance();
                                        String h2 = LinkDeviceUseCase.this.h();
                                        if (h2 != null) {
                                            instance.a(h2);
                                            return;
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                throw new AssertionError("Assertion failed");
                            }
                        case 8:
                            LinkDeviceUseCase.this.a(false);
                            PortfolioApp instance2 = PortfolioApp.get.instance();
                            if (stringExtra != null) {
                                instance2.c(stringExtra, LinkDeviceUseCase.this.g());
                                Object r128 = LinkDeviceUseCase.this;
                                Device e2 = r128.e();
                                if (e2 != null) {
                                    r128.a(new l(e2));
                                    return;
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        case 9:
                            LinkDeviceUseCase.this.a(false);
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a3 = LinkDeviceUseCase.r.a();
                            local2.d(a3, "Pair device failed due to " + intExtra + ", remove this device on button service");
                            try {
                                PortfolioApp instance3 = PortfolioApp.get.instance();
                                String h3 = LinkDeviceUseCase.this.h();
                                if (h3 != null) {
                                    instance3.n(h3);
                                    PortfolioApp instance4 = PortfolioApp.get.instance();
                                    String h4 = LinkDeviceUseCase.this.h();
                                    if (h4 != null) {
                                        instance4.m(h4);
                                        if (intExtra > 1000) {
                                            Object r129 = LinkDeviceUseCase.this;
                                            String h5 = r129.h();
                                            if (h5 != null) {
                                                r129.a(new d(intExtra, h5, ""));
                                                return;
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        } else {
                                            LinkDeviceUseCase linkDeviceUseCase2 = LinkDeviceUseCase.this;
                                            String h6 = linkDeviceUseCase2.h();
                                            if (h6 != null) {
                                                linkDeviceUseCase2.a(new k(intExtra, h6, ""));
                                                Object r1210 = LinkDeviceUseCase.this;
                                                k i = r1210.i();
                                                if (i != null) {
                                                    r1210.a(i);
                                                    LinkDeviceUseCase.this.a((k) null);
                                                    return;
                                                }
                                                wg6.a();
                                                throw null;
                                            }
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } catch (Exception e3) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String a4 = LinkDeviceUseCase.r.a();
                                local3.d(a4, "Pair device failed, remove this device on button service exception=" + e3.getMessage());
                            }
                        default:
                            return;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public h(String str, String str2) {
            wg6.b(str, "device");
            wg6.b(str2, "macAddress");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public i(int i, String str, String str2) {
            wg6.b(str, "deviceId");
            this.a = i;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class j implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(int i, String str, String str2) {
            super(i, str, str2);
            wg6.b(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends j {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public l(Device device) {
            wg6.b(device, "device");
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends j {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public m(String str, boolean z) {
            wg6.b(str, "serial");
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements m24.e<ys5, ws5> {
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase a;

        @DexIgnore
        public n(LinkDeviceUseCase linkDeviceUseCase) {
            this.a = linkDeviceUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ys5 ys5) {
            wg6.b(ys5, "responseValue");
            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.r.a(), " get device setting success");
            this.a.l();
        }

        @DexIgnore
        public void a(ws5 ws5) {
            wg6.b(ws5, "errorValue");
            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.r.a(), " get device setting fail!!");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String h = this.a.h();
            if (h != null) {
                String a2 = LinkDeviceUseCase.r.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Get device setting of ");
                String h2 = this.a.h();
                if (h2 != null) {
                    sb.append(h2);
                    sb.append(", server error=");
                    sb.append(ws5.a());
                    sb.append(", error = ");
                    sb.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
                    remote.e(component, session, h, a2, sb.toString());
                    LinkDeviceUseCase linkDeviceUseCase = this.a;
                    int a3 = ws5.a();
                    String h3 = this.a.h();
                    if (h3 != null) {
                        linkDeviceUseCase.a(new k(a3, h3, ""));
                        PortfolioApp instance = PortfolioApp.get.instance();
                        String h4 = this.a.h();
                        if (h4 != null) {
                            instance.a(h4, (PairingResponse) PairingResponse.CREATOR.buildPairingLinkServerResponse(false, ws5.a()));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $deviceModel;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(Device device, xe6 xe6, LinkDeviceUseCase linkDeviceUseCase) {
            super(2, xe6);
            this.$deviceModel = device;
            this.this$0 = linkDeviceUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            o oVar = new o(this.$deviceModel, xe6, this.this$0);
            oVar.p$ = (il6) obj;
            return oVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((o) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r8v15, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.kw5] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0198  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x01ed  */
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            String str;
            MFUser mFUser;
            ap4 ap4;
            MFUser mFUser2;
            Object obj3;
            il6 il6;
            CoroutineUseCase.c cVar;
            Object obj4;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, this.$deviceModel.getDeviceId(), LinkDeviceUseCase.r.a(), "Steal a device with serial " + this.$deviceModel.getDeviceId());
                PortfolioApp.get.instance().a(CommunicateMode.LINK, this.$deviceModel.getDeviceId(), "Force link a device with serial " + this.$deviceModel.getDeviceId());
                DeviceRepository b = this.this$0.k;
                Device device = this.$deviceModel;
                this.L$0 = il6;
                this.label = 1;
                obj4 = b.forceLinkDevice(device, this);
                if (obj4 == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj4 = obj;
            } else if (i == 2) {
                mFUser2 = (MFUser) this.L$2;
                ap4 = (ap4) this.L$1;
                nc6.a(obj);
                str = (String) this.L$4;
                il6 = (il6) this.L$0;
                mFUser = (MFUser) this.L$3;
                obj3 = obj;
                cVar = (CoroutineUseCase.c) obj3;
                if (!(cVar instanceof kw5.d)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = LinkDeviceUseCase.r.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Push secret key ");
                    kw5.d dVar = (kw5.d) cVar;
                    sb.append(dVar.a());
                    sb.append(" to server after pair device success");
                    local.d(a2, sb.toString());
                    DeviceRepository b2 = this.this$0.k;
                    String deviceId = this.$deviceModel.getDeviceId();
                    String a3 = dVar.a();
                    this.L$0 = il6;
                    this.L$1 = ap4;
                    this.L$2 = mFUser2;
                    this.L$3 = mFUser;
                    this.L$4 = str;
                    this.L$5 = cVar;
                    this.label = 3;
                    obj2 = b2.updateDeviceSecretKey(deviceId, a3, this);
                    if (obj2 == a) {
                        return a;
                    }
                    ap4 ap42 = (ap4) obj2;
                    return cd6.a;
                }
                if (cVar instanceof kw5.c) {
                    FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.r.a(), "Push secret key fail due to " + ((kw5.c) cVar).a());
                }
                return cd6.a;
            } else if (i == 3) {
                CoroutineUseCase.c cVar2 = (CoroutineUseCase.c) this.L$5;
                String str2 = (String) this.L$4;
                MFUser mFUser3 = (MFUser) this.L$3;
                MFUser mFUser4 = (MFUser) this.L$2;
                ap4 ap43 = (ap4) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                obj2 = obj;
                ap4 ap422 = (ap4) obj2;
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap44 = (ap4) obj4;
            if (ap44 instanceof cp4) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, this.$deviceModel.getDeviceId(), LinkDeviceUseCase.r.a(), "Steal a device with serial " + this.$deviceModel.getDeviceId() + " success");
                FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.r.a(), "forceLinkDevice onSuccess device=" + this.$deviceModel.getDeviceId());
                PortfolioApp.get.instance().a(this.$deviceModel.getDeviceId(), (PairingResponse) PairingResponse.CREATOR.buildPairingLinkServerResponse(true, 0));
                mFUser2 = this.this$0.n.getCurrentUser();
                if (mFUser2 != null) {
                    String str3 = mFUser2.getUserId() + ':' + this.$deviceModel.getDeviceId();
                    Object a4 = this.this$0.o;
                    kw5.b bVar = new kw5.b(str3);
                    this.L$0 = il6;
                    this.L$1 = ap44;
                    this.L$2 = mFUser2;
                    this.L$3 = mFUser2;
                    this.L$4 = str3;
                    this.label = 2;
                    obj3 = n24.a(a4, bVar, this);
                    if (obj3 == a) {
                        return a;
                    }
                    str = str3;
                    ap4 = ap44;
                    mFUser = mFUser2;
                    cVar = (CoroutineUseCase.c) obj3;
                    if (!(cVar instanceof kw5.d)) {
                    }
                }
            } else if (ap44 instanceof zo4) {
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.API;
                FLogger.Session session = FLogger.Session.PAIR;
                String deviceId2 = this.$deviceModel.getDeviceId();
                String a5 = LinkDeviceUseCase.r.a();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Steal a device with serial ");
                sb2.append(this.$deviceModel.getDeviceId());
                sb2.append(", server error=");
                zo4 zo4 = (zo4) ap44;
                sb2.append(zo4.a());
                sb2.append(", error = ");
                sb2.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
                remote.e(component, session, deviceId2, a5, sb2.toString());
                FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.r.a(), "forceLinkDevice onFail errorCode=" + zo4.a());
                this.this$0.a(new k(zo4.a(), this.$deviceModel.getDeviceId(), ""));
                PortfolioApp.get.instance().a(this.$deviceModel.getDeviceId(), (PairingResponse) PairingResponse.CREATOR.buildPairingLinkServerResponse(false, zo4.a()));
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements m24.e<mr4.d, mr4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase a;

        @DexIgnore
        public p(LinkDeviceUseCase linkDeviceUseCase) {
            this.a = linkDeviceUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DownloadFirmwareByDeviceModelUsecase.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.a.b(), (af6) null, (ll6) null, new pr4$p$a(this, dVar, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(DownloadFirmwareByDeviceModelUsecase.c cVar) {
            wg6.b(cVar, "errorValue");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String h = this.a.h();
            if (h != null) {
                String a2 = LinkDeviceUseCase.r.a();
                StringBuilder sb = new StringBuilder();
                sb.append(" downloadFw FAILED!!!, latestFwVersion=");
                String h2 = this.a.h();
                if (h2 != null) {
                    sb.append(h2);
                    sb.append(" but device is DianaEV1!!!");
                    remote.e(component, session, h, a2, sb.toString());
                    FLogger.INSTANCE.getLocal().e(LinkDeviceUseCase.r.a(), "checkFirmware - downloadFw FAILED!!!");
                    Object r0 = this.a;
                    String h3 = r0.h();
                    if (h3 != null) {
                        r0.a(new e(h3, ""));
                        PortfolioApp instance = PortfolioApp.get.instance();
                        String h4 = this.a.h();
                        if (h4 != null) {
                            instance.a(h4);
                            this.a.a(false);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = LinkDeviceUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "LinkDeviceUseCase::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public LinkDeviceUseCase(DeviceRepository deviceRepository, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, an4 an4, UserRepository userRepository, kw5 kw5, cj4 cj4) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(kw5, "mDecryptValueKeyStoreUseCase");
        wg6.b(cj4, "mDeviceSettingFactory");
        this.k = deviceRepository;
        this.l = downloadFirmwareByDeviceModelUsecase;
        this.m = an4;
        this.n = userRepository;
        this.o = kw5;
        this.p = cj4;
    }

    @DexIgnore
    public final String f() {
        return this.g;
    }

    @DexIgnore
    public final String g() {
        return this.e;
    }

    @DexIgnore
    public final String h() {
        return this.d;
    }

    @DexIgnore
    public final k i() {
        return this.i;
    }

    @DexIgnore
    public final boolean j() {
        return this.h;
    }

    @DexIgnore
    public final void k() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final synchronized void l() {
        Device device = this.f;
        if (device != null) {
            rm6 unused = ik6.b(b(), (af6) null, (ll6) null, new o(device, (xe6) null, this), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$p, com.portfolio.platform.CoroutineUseCase$e] */
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "sendLatestFWResponseToDevice(), deviceModel=" + this.g + ", isSkipOTA=" + this.m.V());
        if (PortfolioApp.get.instance().F() || !this.m.V()) {
            this.l.a(new DownloadFirmwareByDeviceModelUsecase.b(this.g), new p(this));
            return;
        }
        PortfolioApp instance = PortfolioApp.get.instance();
        String str2 = this.d;
        if (str2 != null) {
            instance.a(str2, (PairingResponse) PairingResponse.CREATOR.buildPairingUpdateFWResponse(new SkipFirmwareData()));
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    public final void b(String str) {
        this.e = str;
    }

    @DexIgnore
    public final void c(String str) {
        this.d = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$n] */
    public final synchronized void d() {
        cj4 cj4 = this.p;
        String str = this.d;
        if (str != null) {
            m24<xs5, ys5, ws5> a2 = cj4.a(str);
            String str2 = this.d;
            if (str2 != null) {
                a2.a(new xs5(str2), new n(this));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final Device e() {
        return this.f;
    }

    @DexIgnore
    public final void a(Device device) {
        this.f = device;
    }

    @DexIgnore
    public String c() {
        return q;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "<set-?>");
        this.g = str;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.h = z;
    }

    @DexIgnore
    public final void a(k kVar) {
        this.i = kVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final void a(ShineDevice shineDevice, m24.e<? super pr4.j, ? super pr4.i> eVar) {
        wg6.b(shineDevice, "closestDevice");
        wg6.b(eVar, "caseCallback");
        FLogger.INSTANCE.getLocal().d(q, "onRetrieveLinkAction");
        this.h = true;
        this.d = shineDevice.getSerial();
        this.e = shineDevice.getMacAddress();
        a(eVar);
    }

    @DexIgnore
    public Object a(pr4.h hVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(q, "running UseCase");
        if (hVar == null) {
            return new i(600, "", "");
        }
        this.h = true;
        this.d = hVar.a();
        this.e = "";
        hVar.b();
        this.e = hVar.b();
        try {
            PortfolioApp instance = PortfolioApp.get.instance();
            String str = this.d;
            if (str != null) {
                String str2 = this.e;
                if (str2 != null) {
                    instance.a(str, str2);
                    return new Object();
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = q;
            local.e(str3, "Error inside " + q + ".connectDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final synchronized void a(long j2) {
        FLogger.INSTANCE.getLocal().d(q, "requestAuthorizeDevice()");
        PortfolioApp instance = PortfolioApp.get.instance();
        String str = this.d;
        if (str != null) {
            instance.a(str, (PairingResponse) new PairingAuthorizeResponse(j2));
        } else {
            wg6.a();
            throw null;
        }
    }
}
