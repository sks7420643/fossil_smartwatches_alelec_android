package com.portfolio.platform.ui.device.domain.usecase;

import android.text.TextUtils;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.jf6;
import com.fossil.kc6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.mr4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yj6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.util.DeviceUtils;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DownloadFirmwareByDeviceModelUsecase extends m24<mr4.b, mr4.d, mr4.c> {
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ GuestApiService e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "deviceModel");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wg6.b(str, "latestFwVersion");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {37, 52}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, xe6 xe6) {
            super(xe6);
            this.this$0 = downloadFirmwareByDeviceModelUsecase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((mr4.b) null, (xe6<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {37}, m = "invokeSuspend")
    public static final class f extends sf6 implements hg6<xe6<? super rx6<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceModel;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, String str, xe6 xe6) {
            super(1, xe6);
            this.this$0 = downloadFirmwareByDeviceModelUsecase;
            this.$deviceModel = str;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new f(this.this$0, this.$deviceModel, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((f) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                GuestApiService b = this.this$0.e;
                String h = this.this$0.d.h();
                String str = this.$deviceModel;
                this.label = 1;
                obj = b.getFirmwares(h, str, "android", this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DownloadFirmwareByDeviceModelUsecase(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        wg6.b(portfolioApp, "mApp");
        wg6.b(guestApiService, "mGuestApiService");
        this.d = portfolioApp;
        this.e = guestApiService;
    }

    @DexIgnore
    public String c() {
        return "DownloadFirmwareByDeviceModelUsecase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    public Object a(mr4.b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        Firmware firmware;
        DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase;
        mr4.b bVar2;
        String str;
        ap4 ap4;
        mr4.b bVar3 = bVar;
        xe6<Object> xe62 = xe6;
        if (xe62 instanceof e) {
            eVar = (e) xe62;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                String str2 = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d("DownloadFirmwareByDeviceModelUsecase", "run");
                    if (bVar3 == null) {
                        FLogger.INSTANCE.getLocal().e("DownloadFirmwareByDeviceModelUsecase", "run - requestValues is NULL!!!");
                        return new c();
                    }
                    FLogger.INSTANCE.getLocal().d("DownloadFirmwareByDeviceModelUsecase", "run - getFirmwares of deviceModel=" + bVar.a());
                    String a3 = bVar.a();
                    f fVar = new f(this, a3, (xe6) null);
                    eVar.L$0 = this;
                    eVar.L$1 = bVar3;
                    eVar.L$2 = a3;
                    eVar.label = 1;
                    Object a4 = ResponseKt.a(fVar, eVar);
                    if (a4 == a2) {
                        return a2;
                    }
                    downloadFirmwareByDeviceModelUsecase = this;
                    Object obj2 = a4;
                    bVar2 = bVar3;
                    str = a3;
                    obj = obj2;
                } else if (i == 1) {
                    str = (String) eVar.L$2;
                    bVar2 = (b) eVar.L$1;
                    downloadFirmwareByDeviceModelUsecase = (DownloadFirmwareByDeviceModelUsecase) eVar.L$0;
                    nc6.a(obj);
                } else if (i == 2) {
                    List list = (List) eVar.L$4;
                    ap4 ap42 = (ap4) eVar.L$3;
                    String str3 = (String) eVar.L$2;
                    b bVar4 = (b) eVar.L$1;
                    DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase2 = (DownloadFirmwareByDeviceModelUsecase) eVar.L$0;
                    nc6.a(obj);
                    firmware = (Firmware) eVar.L$5;
                    if (((Boolean) obj).booleanValue()) {
                        return new c();
                    }
                    String versionNumber = firmware.getVersionNumber();
                    wg6.a((Object) versionNumber, "latestFirmware.versionNumber");
                    return new d(versionNumber);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ApiResponse apiResponse = (ApiResponse) ((cp4) ap4).a();
                    List<Firmware> list2 = apiResponse != null ? apiResponse.get_items() : null;
                    if (list2 != null) {
                        firmware = null;
                        for (Firmware firmware2 : list2) {
                            if (!TextUtils.isEmpty(firmware2.getDeviceModel())) {
                                String deviceModel = firmware2.getDeviceModel();
                                wg6.a((Object) deviceModel, "firmware.deviceModel");
                                if (yj6.a((CharSequence) deviceModel, (CharSequence) str, false, 2, (Object) null)) {
                                    firmware = firmware2;
                                }
                            }
                        }
                    } else {
                        firmware = null;
                    }
                    if (firmware != null) {
                        DeviceUtils a5 = DeviceUtils.g.a();
                        eVar.L$0 = downloadFirmwareByDeviceModelUsecase;
                        eVar.L$1 = bVar2;
                        eVar.L$2 = str;
                        eVar.L$3 = ap4;
                        eVar.L$4 = list2;
                        eVar.L$5 = firmware;
                        eVar.label = 2;
                        obj = a5.a(firmware, (xe6<? super Boolean>) eVar);
                        if (obj == a2) {
                            return a2;
                        }
                        if (((Boolean) obj).booleanValue()) {
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().e("DownloadFirmwareByDeviceModelUsecase", "run - getFirmwares of deviceModel=" + str + " not found on server");
                        return new c();
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("run - getFirmwares of deviceModel=");
                    sb.append(str);
                    sb.append(" not found on server code=");
                    zo4 zo4 = (zo4) ap4;
                    ServerError c2 = zo4.c();
                    sb.append(c2 != null ? c2.getCode() : null);
                    sb.append(", message=");
                    ServerError c3 = zo4.c();
                    if (c3 != null) {
                        str2 = c3.getMessage();
                    }
                    sb.append(str2);
                    local.e("DownloadFirmwareByDeviceModelUsecase", sb.toString());
                    return new c();
                } else {
                    throw new kc6();
                }
            }
        }
        eVar = new e(this, xe62);
        Object obj3 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        String str22 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj3;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
