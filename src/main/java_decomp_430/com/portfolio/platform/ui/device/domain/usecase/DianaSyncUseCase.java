package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.text.TextUtils;
import com.fossil.NotificationAppHelper;
import com.fossil.WatchFaceHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.as4;
import com.fossil.bs4;
import com.fossil.cd6;
import com.fossil.cs4;
import com.fossil.d15;
import com.fossil.ff6;
import com.fossil.ik6;
import com.fossil.jf6;
import com.fossil.jl4;
import com.fossil.kr4$e$a;
import com.fossil.kr4$e$b;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u40;
import com.fossil.ui4;
import com.fossil.wg6;
import com.fossil.ww5;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yh4;
import com.fossil.zi4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaSyncUseCase extends m24<bs4, cs4, as4> {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ DianaPresetRepository e;
    @DexIgnore
    public /* final */ an4 f;
    @DexIgnore
    public /* final */ d15 g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase j;
    @DexIgnore
    public /* final */ mz4 k;
    @DexIgnore
    public /* final */ VerifySecretKeyUseCase l;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase m;
    @DexIgnore
    public /* final */ AnalyticsHelper n;
    @DexIgnore
    public /* final */ WatchFaceRepository o;
    @DexIgnore
    public /* final */ AlarmsRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DianaSyncUseCase.q;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase a;

        @DexIgnore
        public b(DianaSyncUseCase dianaSyncUseCase) {
            this.a = dianaSyncUseCase;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v10, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r6v14, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r6v15, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r6v16, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r6v19, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && xj6.b(stringExtra, this.a.h.e(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(DianaSyncUseCase.r.a(), "sync success, remove device now");
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    this.a.a(new cs4());
                } else if (intExtra == 2 || intExtra == 4) {
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = DianaSyncUseCase.r.a();
                    local.d(a2, "sync fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1603) {
                            this.a.a(new as4(yh4.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT, (ArrayList<Integer>) null));
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.a(new as4(yh4.FAIL_DUE_TO_SYNC_FAIL, (ArrayList<Integer>) null));
                            return;
                        }
                    }
                    this.a.a(new as4(yh4.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                } else if (intExtra == 5) {
                    FLogger.INSTANCE.getLocal().d(DianaSyncUseCase.r.a(), "sync pending due to workout");
                    this.a.a(new as4(yh4.FAIL_DUE_TO_PENDING_WORKOUT, (ArrayList<Integer>) null));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {143}, m = "run")
    public static final class c extends jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DianaSyncUseCase dianaSyncUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaSyncUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((bs4) null, (xe6<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {216}, m = "setRuleNotificationFilterToDevice")
    public static final class d extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DianaSyncUseCase dianaSyncUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaSyncUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<ww5.d, ww5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ UserProfile e;

        @DexIgnore
        public e(DianaSyncUseCase dianaSyncUseCase, String str, int i, boolean z, UserProfile userProfile) {
            this.a = dianaSyncUseCase;
            this.b = str;
            this.c = i;
            this.d = z;
            this.e = userProfile;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(VerifySecretKeyUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaSyncUseCase.r.a();
            local.d(a2, "secret key " + dVar.a() + ", start sync");
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b, DianaSyncUseCase.r.a(), "[Sync] Verify secret key success, start sync");
            PortfolioApp.get.instance().e(dVar.a(), this.b);
            rm6 unused = ik6.b(this.a.b(), (af6) null, (ll6) null, new kr4$e$b(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r8v6, types: [com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.kr4$e$a] */
        public void a(VerifySecretKeyUseCase.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaSyncUseCase.r.a();
            local.d(a2, "verify secret key fail, stop sync " + cVar.a());
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.b;
            String a3 = DianaSyncUseCase.r.a();
            remote.i(component, session, str, a3, "[Sync] Verify secret key failed by " + cVar.a());
            if (cVar.a() == u40.REQUEST_UNSUPPORTED.getCode() && this.e.getOriginalSyncMode() == 12) {
                this.a.m.a(new UpdateFirmwareUsecase.b(this.b, true), new kr4$e$a());
            }
            this.a.a(this.e.getOriginalSyncMode(), this.b, 2);
        }
    }

    /*
    static {
        String simpleName = DianaSyncUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "DianaSyncUseCase::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public DianaSyncUseCase(DianaPresetRepository dianaPresetRepository, an4 an4, d15 d15, PortfolioApp portfolioApp, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, mz4 mz4, VerifySecretKeyUseCase verifySecretKeyUseCase, UpdateFirmwareUsecase updateFirmwareUsecase, AnalyticsHelper analyticsHelper, WatchFaceRepository watchFaceRepository, AlarmsRepository alarmsRepository) {
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(an4, "mSharedPrefs");
        wg6.b(d15, "mGetApps");
        wg6.b(portfolioApp, "mApp");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wg6.b(mz4, "mGetAllContactGroup");
        wg6.b(verifySecretKeyUseCase, "mVerifySecretKeyUseCase");
        wg6.b(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        wg6.b(analyticsHelper, "mAnalyticsHelper");
        wg6.b(watchFaceRepository, "watchFaceRepository");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.e = dianaPresetRepository;
        this.f = an4;
        this.g = d15;
        this.h = portfolioApp;
        this.i = deviceRepository;
        this.j = notificationSettingsDatabase;
        this.k = mz4;
        this.l = verifySecretKeyUseCase;
        this.m = updateFirmwareUsecase;
        this.n = analyticsHelper;
        this.o = watchFaceRepository;
        this.p = alarmsRepository;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.usecase.VerifySecretKeyUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final void b(int i2, boolean z, UserProfile userProfile, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.d(str2, "verifySecretKey " + str);
        a(userProfile.getOriginalSyncMode(), str, 0);
        this.l.a(new VerifySecretKeyUseCase.b(str), new e(this, str, i2, z, userProfile));
    }

    @DexIgnore
    public String c() {
        return q;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r13v0, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, java.lang.Object, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0233 A[Catch:{ Exception -> 0x02c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x026f A[Catch:{ Exception -> 0x02c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(bs4 bs4, xe6<Object> xe6) {
        c cVar;
        int i2;
        UserProfile userProfile;
        int i3;
        DianaSyncUseCase dianaSyncUseCase;
        List activeAlarms;
        DianaPreset activePresetBySerial;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i4 = cVar.label;
            if ((i4 & Integer.MIN_VALUE) != 0) {
                cVar.label = i4 - Integer.MIN_VALUE;
                Object obj = cVar.result;
                Object a2 = ff6.a();
                i2 = cVar.label;
                BackgroundConfig backgroundConfig = null;
                if (i2 != 0) {
                    nc6.a(obj);
                    if (bs4 == null) {
                        return new as4(yh4.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
                    }
                    int b2 = bs4.b();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String c2 = c();
                    StringBuilder sb = new StringBuilder();
                    sb.append("start on thread=");
                    Thread currentThread = Thread.currentThread();
                    wg6.a((Object) currentThread, "Thread.currentThread()");
                    sb.append(currentThread.getName());
                    local.d(c2, sb.toString());
                    userProfile = PortfolioApp.get.instance().j();
                    if (userProfile == null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String c3 = c();
                        local2.e(c3, "Error inside " + c() + ".startDeviceSync - user is null");
                        a(b2, bs4.a(), 2);
                        return new as4(yh4.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
                    } else if (TextUtils.isEmpty(bs4.a())) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String c4 = c();
                        local3.e(c4, "Error inside " + c() + ".startDeviceSync - serial is null");
                        a(b2, bs4.a(), 2);
                        return new as4(yh4.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
                    } else {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String c5 = c();
                        local4.d(c5, "Inside " + c() + ".startDeviceSync - serial=" + bs4.a() + "," + " weightInKg=" + userProfile.getUserBiometricData().getWeightInKilogram() + ", heightInMeter=" + userProfile.getUserBiometricData().getHeightInMeter() + ", goal=" + userProfile.getGoalSteps() + ", isNewDevice=" + bs4.c() + ", SyncMode=" + b2);
                        userProfile.setOriginalSyncMode(b2);
                        boolean k2 = this.f.k(bs4.a());
                        if (!k2 || b2 == 13) {
                            b2 = 13;
                        }
                        if (b2 == 13 || !PortfolioApp.get.instance().i(bs4.a())) {
                            FLogger.INSTANCE.getLocal().e(q, "start set localization");
                            PortfolioApp.get.instance().R();
                            if (b2 == 13) {
                                try {
                                    this.f.a(bs4.a(), System.currentTimeMillis(), false);
                                    cVar.L$0 = this;
                                    cVar.L$1 = bs4;
                                    cVar.I$0 = b2;
                                    cVar.L$2 = userProfile;
                                    cVar.Z$0 = k2;
                                    cVar.label = 1;
                                    if (a((xe6<? super cd6>) cVar) == a2) {
                                        return a2;
                                    }
                                    dianaSyncUseCase = this;
                                    i3 = b2;
                                } catch (Exception e2) {
                                    i3 = b2;
                                    e = e2;
                                    dianaSyncUseCase = this;
                                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                                    FLogger.Component component = FLogger.Component.APP;
                                    FLogger.Session session = FLogger.Session.OTHER;
                                    String a3 = bs4.a();
                                    String str = q;
                                    remote.i(component, session, a3, str, "[Sync] Exception when prepare settings " + e);
                                    dianaSyncUseCase.b(i3, bs4.c(), userProfile, bs4.a());
                                    return new Object();
                                }
                            } else {
                                dianaSyncUseCase = this;
                                i3 = b2;
                                dianaSyncUseCase.b(i3, bs4.c(), userProfile, bs4.a());
                                return new Object();
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().e(c(), "Device is syncing, wait for result...");
                            if (a() != null) {
                                BleCommandResultManager.d.a((BleCommandResultManager.b) this.d, CommunicateMode.SYNC);
                            }
                            return new Object();
                        }
                    }
                } else if (i2 == 1) {
                    boolean z = cVar.Z$0;
                    UserProfile userProfile2 = (UserProfile) cVar.L$2;
                    i3 = cVar.I$0;
                    bs4 bs42 = (bs4) cVar.L$1;
                    dianaSyncUseCase = (DianaSyncUseCase) cVar.L$0;
                    try {
                        nc6.a(obj);
                        bs4 bs43 = bs42;
                        userProfile = userProfile2;
                        bs4 = bs43;
                    } catch (Exception e3) {
                        e = e3;
                        bs4 bs44 = bs42;
                        userProfile = userProfile2;
                        bs4 = bs44;
                        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                        FLogger.Component component2 = FLogger.Component.APP;
                        FLogger.Session session2 = FLogger.Session.OTHER;
                        String a32 = bs4.a();
                        String str2 = q;
                        remote2.i(component2, session2, a32, str2, "[Sync] Exception when prepare settings " + e);
                        dianaSyncUseCase.b(i3, bs4.c(), userProfile, bs4.a());
                        return new Object();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String c6 = dianaSyncUseCase.c();
                local5.d(c6, "Inside " + dianaSyncUseCase.c() + ".startDeviceSync - Start full-sync");
                activeAlarms = dianaSyncUseCase.p.getActiveAlarms();
                if (activeAlarms == null) {
                    activeAlarms = new ArrayList();
                }
                PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(activeAlarms));
                activePresetBySerial = dianaSyncUseCase.e.getActivePresetBySerial(bs4.a());
                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                String c7 = dianaSyncUseCase.c();
                local6.d(c7, "startDeviceSync activePreset=" + activePresetBySerial);
                if (activePresetBySerial != null) {
                    PortfolioApp.get.instance().a(zi4.a(activePresetBySerial.getComplications(), new Gson()), bs4.a());
                    PortfolioApp.get.instance().a(zi4.b(activePresetBySerial.getWatchapps(), new Gson()), bs4.a());
                    WatchFace watchFaceWithId = dianaSyncUseCase.o.getWatchFaceWithId(activePresetBySerial.getWatchFaceId());
                    if (watchFaceWithId != null) {
                        backgroundConfig = WatchFaceHelper.loadBackgroundConfig(watchFaceWithId, activePresetBySerial.getComplications());
                    }
                    if (backgroundConfig != null) {
                        PortfolioApp.get.instance().a(backgroundConfig, bs4.a());
                    }
                }
                dianaSyncUseCase.b(i3, bs4.c(), userProfile, bs4.a());
                return new Object();
            }
        }
        cVar = new c(this, xe6);
        Object obj2 = cVar.result;
        Object a22 = ff6.a();
        i2 = cVar.label;
        BackgroundConfig backgroundConfig2 = null;
        if (i2 != 0) {
        }
        try {
            ILocalFLogger local52 = FLogger.INSTANCE.getLocal();
            String c62 = dianaSyncUseCase.c();
            local52.d(c62, "Inside " + dianaSyncUseCase.c() + ".startDeviceSync - Start full-sync");
            activeAlarms = dianaSyncUseCase.p.getActiveAlarms();
            if (activeAlarms == null) {
            }
            PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(activeAlarms));
            activePresetBySerial = dianaSyncUseCase.e.getActivePresetBySerial(bs4.a());
            ILocalFLogger local62 = FLogger.INSTANCE.getLocal();
            String c72 = dianaSyncUseCase.c();
            local62.d(c72, "startDeviceSync activePreset=" + activePresetBySerial);
            if (activePresetBySerial != null) {
            }
        } catch (Exception e4) {
            e = e4;
            IRemoteFLogger remote22 = FLogger.INSTANCE.getRemote();
            FLogger.Component component22 = FLogger.Component.APP;
            FLogger.Session session22 = FLogger.Session.OTHER;
            String a322 = bs4.a();
            String str22 = q;
            remote22.i(component22, session22, a322, str22, "[Sync] Exception when prepare settings " + e);
            dianaSyncUseCase.b(i3, bs4.c(), userProfile, bs4.a());
            return new Object();
        }
        dianaSyncUseCase.b(i3, bs4.c(), userProfile, bs4.a());
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final /* synthetic */ Object a(xe6<? super cd6> xe6) {
        d dVar;
        int i2;
        if (xe6 instanceof d) {
            dVar = (d) xe6;
            int i3 = dVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dVar.label = i3 - Integer.MIN_VALUE;
                d dVar2 = dVar;
                Object obj = dVar2.result;
                Object a2 = ff6.a();
                i2 = dVar2.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                    d15 d15 = this.g;
                    mz4 mz4 = this.k;
                    NotificationSettingsDatabase notificationSettingsDatabase = this.j;
                    an4 an4 = this.f;
                    dVar2.L$0 = this;
                    dVar2.label = 1;
                    obj = notificationAppHelper.a(d15, mz4, notificationSettingsDatabase, an4, dVar2);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    DianaSyncUseCase dianaSyncUseCase = (DianaSyncUseCase) dVar2.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                PortfolioApp.get.instance().a(new AppNotificationFilterSettings((List) obj, System.currentTimeMillis()), PortfolioApp.get.instance().e());
                return cd6.a;
            }
        }
        dVar = new d(this, xe6);
        d dVar22 = dVar;
        Object obj2 = dVar22.result;
        Object a22 = ff6.a();
        i2 = dVar22.label;
        if (i2 != 0) {
        }
        PortfolioApp.get.instance().a(new AppNotificationFilterSettings((List) obj2, System.currentTimeMillis()), PortfolioApp.get.instance().e());
        return cd6.a;
    }

    @DexIgnore
    public final void a(int i2, String str, int i3) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String c2 = c();
        local.d(c2, "broadcastSyncStatus serial=" + str + ' ' + i3);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), i2);
        intent.putExtra("SERIAL", str);
        BleCommandResultManager bleCommandResultManager = BleCommandResultManager.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        bleCommandResultManager.a(communicateMode, new BleCommandResultManager.a(communicateMode, str, intent));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final void a(int i2, boolean z, UserProfile userProfile, String str) {
        wg6.b(userProfile, "userProfile");
        wg6.b(str, "serial");
        if (a() != null) {
            BleCommandResultManager.d.a((BleCommandResultManager.b) this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String c2 = c();
        local.d(c2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.W()) {
            SKUModel skuModelBySerialPrefix = this.i.getSkuModelBySerialPrefix(DeviceHelper.o.b(str));
            this.f.a((Boolean) true);
            this.n.a(i2, skuModelBySerialPrefix);
            jl4 b2 = AnalyticsHelper.f.b("sync_session");
            AnalyticsHelper.f.a("sync_session", b2);
            b2.d();
        }
        if (!PortfolioApp.get.instance().a(str, userProfile)) {
            a(userProfile.getOriginalSyncMode(), str, 2);
        }
    }
}
