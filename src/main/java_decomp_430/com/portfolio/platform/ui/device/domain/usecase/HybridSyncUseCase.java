package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.as4;
import com.fossil.bs4;
import com.fossil.cd6;
import com.fossil.cs4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ui4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yh4;
import com.fossil.zi4;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridSyncUseCase extends m24<bs4, cs4, as4> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ MicroAppRepository e;
    @DexIgnore
    public /* final */ an4 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ HybridPresetRepository i;
    @DexIgnore
    public /* final */ NotificationsRepository j;
    @DexIgnore
    public /* final */ AnalyticsHelper k;
    @DexIgnore
    public /* final */ AlarmsRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncUseCase a;

        @DexIgnore
        public b(HybridSyncUseCase hybridSyncUseCase) {
            this.a = hybridSyncUseCase;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v10, types: [com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r5v14, types: [com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r5v15, types: [com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && xj6.b(stringExtra, this.a.h.e(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(HybridSyncUseCase.m, "sync success, remove device now");
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    this.a.a(new cs4());
                } else if (intExtra == 2 || intExtra == 4) {
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = HybridSyncUseCase.m;
                    local.d(d, "sync fail due to " + intExtra2);
                    if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                        this.a.a(new as4(yh4.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                    } else {
                        this.a.a(new as4(yh4.FAIL_DUE_TO_SYNC_FAIL, (ArrayList<Integer>) null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(HybridSyncUseCase hybridSyncUseCase, SparseArray sparseArray, boolean z, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridSyncUseCase;
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$data, this.$isMovemberModel, this.$serialNumber, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                AppNotificationFilterSettings a = NotificationAppHelper.b.a((SparseArray<List<BaseFeatureModel>>) this.$data, this.$isMovemberModel);
                PortfolioApp.get.instance().a(a, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String c = this.this$0.c();
                local.d(c, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = HybridSyncUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "HybridSyncUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public HybridSyncUseCase(MicroAppRepository microAppRepository, an4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, AnalyticsHelper analyticsHelper, AlarmsRepository alarmsRepository) {
        wg6.b(microAppRepository, "mMicroAppRepository");
        wg6.b(an4, "mSharedPrefs");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(portfolioApp, "mApp");
        wg6.b(hybridPresetRepository, "mPresetRepository");
        wg6.b(notificationsRepository, "mNotificationsRepository");
        wg6.b(analyticsHelper, "mAnalyticsHelper");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.e = microAppRepository;
        this.f = an4;
        this.g = deviceRepository;
        this.h = portfolioApp;
        this.i = hybridPresetRepository;
        this.j = notificationsRepository;
        this.k = analyticsHelper;
        this.l = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return "HybridSyncUseCase";
    }

    @DexIgnore
    public Object a(bs4 bs4, xe6<Object> xe6) {
        if (bs4 == null) {
            return new as4(yh4.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
        }
        int b2 = bs4.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        StringBuilder sb = new StringBuilder();
        sb.append("start on thread=");
        Thread currentThread = Thread.currentThread();
        wg6.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d(str, sb.toString());
        UserProfile j2 = PortfolioApp.get.instance().j();
        if (j2 == null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local2.e(str2, "Error inside " + m + ".startDeviceSync - user is null");
            a(bs4.a());
            return new as4(yh4.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
        } else if (TextUtils.isEmpty(bs4.a())) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = m;
            local3.e(str3, "Error inside " + m + ".startDeviceSync - serial is null");
            a(bs4.a());
            return new as4(yh4.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = m;
            local4.d(str4, "Inside " + m + ".startDeviceSync - serial=" + bs4.a() + "," + " weightInKg=" + j2.getUserBiometricData().getWeightInKilogram() + ", heightInMeter=" + j2.getUserBiometricData().getHeightInMeter() + ", goal=" + j2.getGoalSteps() + ", isNewDevice=" + bs4.c() + ", SyncMode=" + b2);
            j2.setOriginalSyncMode(b2);
            if (!this.f.k(bs4.a()) || b2 == 13) {
                b2 = 13;
            }
            if (b2 == 13 || !PortfolioApp.get.instance().i(bs4.a())) {
                if (b2 == 13) {
                    try {
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        String str5 = m;
                        local5.d(str5, "Inside " + m + ".startDeviceSync - Start full-sync");
                        a(this.j.getAllNotificationsByHour(bs4.a(), MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), bs4.a(), NotificationAppHelper.b.a(this.g.getSkuModelBySerialPrefix(DeviceHelper.o.b(bs4.a())), bs4.a()));
                        List activeAlarms = this.l.getActiveAlarms();
                        if (activeAlarms == null) {
                            activeAlarms = new ArrayList();
                        }
                        PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(activeAlarms));
                        HybridPreset activePresetBySerial = this.i.getActivePresetBySerial(bs4.a());
                        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                        String c2 = c();
                        local6.d(c2, "startDeviceSync activePreset=" + activePresetBySerial);
                        if (activePresetBySerial != null) {
                            for (HybridPresetAppSetting hybridPresetAppSetting : activePresetBySerial.getButtons()) {
                                if (wg6.a((Object) hybridPresetAppSetting.getAppId(), (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                    String settings = hybridPresetAppSetting.getSettings();
                                    if (!TextUtils.isEmpty(settings)) {
                                        try {
                                            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().a(settings, SecondTimezoneSetting.class);
                                            if (secondTimezoneSetting != null && !TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                                                PortfolioApp.get.instance().o(secondTimezoneSetting.getTimeZoneId());
                                            }
                                        } catch (Exception e2) {
                                            ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                                            String str6 = m;
                                            local7.e(str6, "Inside " + m + ".startDeviceSync - parse secondTimezone, ex=" + e2);
                                            e2.printStackTrace();
                                        }
                                    }
                                }
                            }
                            List<MicroAppMapping> a2 = zi4.a(activePresetBySerial, bs4.a(), this.g, this.e);
                            if (!a2.isEmpty()) {
                                PortfolioApp.get.instance().b(bs4.a(), (List<? extends BLEMapping>) a2);
                            }
                        }
                    } catch (Exception e3) {
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.APP;
                        FLogger.Session session = FLogger.Session.OTHER;
                        String a3 = bs4.a();
                        String str7 = m;
                        remote.i(component, session, a3, str7, "[Sync] Exception when prepare settings " + e3);
                    }
                }
                a(b2, bs4.c(), j2, bs4.a());
                return new Object();
            }
            FLogger.INSTANCE.getLocal().e(m, "Device is syncing, returning...");
            return new cs4();
        }
    }

    @DexIgnore
    public final rm6 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new c(this, sparseArray, z, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final void a(int i2, boolean z, UserProfile userProfile, String str) {
        if (a() != null) {
            BleCommandResultManager.d.a((BleCommandResultManager.b) this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.W()) {
            SKUModel skuModelBySerialPrefix = this.g.getSkuModelBySerialPrefix(DeviceHelper.o.b(str));
            this.f.a((Boolean) true);
            this.k.a(i2, skuModelBySerialPrefix);
            jl4 b2 = AnalyticsHelper.f.b("sync_session");
            AnalyticsHelper.f.a("sync_session", b2);
            b2.d();
        }
        if (!PortfolioApp.get.instance().a(str, userProfile)) {
            a(str);
        } else if (i2 == 13) {
            this.f.a(str, System.currentTimeMillis(), false);
        }
    }

    @DexIgnore
    public final void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "broadcastSyncFail serial=" + str);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 2);
        intent.putExtra("SERIAL", str);
    }
}
