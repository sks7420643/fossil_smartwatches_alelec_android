package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.hf6;
import com.fossil.ik6;
import com.fossil.jk4;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.wr4;
import com.fossil.wr4$e$a;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetVibrationStrengthUseCase extends m24<wr4.b, wr4.d, wr4.c> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ e g; // = new e();
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SetVibrationStrengthUseCase.i;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            wg6.b(str, "deviceSerial");
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            wg6.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SetVibrationStrengthUseCase.j.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + SetVibrationStrengthUseCase.this.f() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.SET_VIBRATION_STRENGTH && SetVibrationStrengthUseCase.this.f()) {
                boolean z = false;
                SetVibrationStrengthUseCase.this.a(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    rm6 unused = ik6.b(SetVibrationStrengthUseCase.this.b(), (af6) null, (ll6) null, new wr4$e$a(this, (xe6) null), 3, (Object) null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive failed");
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                SetVibrationStrengthUseCase.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        String simpleName = SetVibrationStrengthUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "SetVibrationStrengthUseCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetVibrationStrengthUseCase(DeviceRepository deviceRepository, an4 an4) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.h = deviceRepository;
    }

    @DexIgnore
    public String c() {
        return i;
    }

    @DexIgnore
    public final int d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.f;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    public final void h() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(wr4.b bVar, xe6<Object> xe6) {
        try {
            FLogger.INSTANCE.getLocal().d(i, "running UseCase");
            this.d = true;
            Integer a2 = bVar != null ? hf6.a(bVar.b()) : null;
            if (a2 != null) {
                this.e = a2.intValue();
                this.f = bVar.a();
                PortfolioApp.get.instance().a(bVar.a(), new VibrationStrengthObj(jk4.a(bVar.b()), false, 2, (qg6) null));
                return new Object();
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.e(str, "Error inside " + i + ".connectDevice - e=" + e2);
            return new c(600, -1, new ArrayList());
        }
    }
}
