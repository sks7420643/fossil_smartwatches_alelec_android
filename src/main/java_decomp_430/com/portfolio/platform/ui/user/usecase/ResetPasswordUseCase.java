package com.portfolio.platform.ui.user.usecase;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.jf6;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.qt4;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResetPasswordUseCase extends m24<qt4.b, qt4.d, qt4.c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ AuthApiGuestService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "email");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase", f = "ResetPasswordUseCase.kt", l = {28}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ResetPasswordUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ResetPasswordUseCase resetPasswordUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = resetPasswordUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((qt4.b) null, (xe6<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$run$response$1", f = "ResetPasswordUseCase.kt", l = {28}, m = "invokeSuspend")
    public static final class f extends sf6 implements hg6<xe6<? super rx6<Void>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ResetPasswordUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ResetPasswordUseCase resetPasswordUseCase, ku3 ku3, xe6 xe6) {
            super(1, xe6);
            this.this$0 = resetPasswordUseCase;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new f(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((f) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                AuthApiGuestService a2 = this.this$0.d;
                ku3 ku3 = this.$jsonObject;
                this.label = 1;
                obj = a2.passwordRequestReset(ku3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ResetPasswordUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "ResetPasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ResetPasswordUseCase(AuthApiGuestService authApiGuestService) {
        wg6.b(authApiGuestService, "mApiGuestService");
        this.d = authApiGuestService;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(qt4.b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        ap4 ap4;
        int i2;
        String str;
        Integer code;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i3 = eVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                eVar.label = i3 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                Integer num = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = e;
                    StringBuilder sb = new StringBuilder();
                    sb.append("running UseCase with email=");
                    sb.append(bVar != null ? bVar.a() : null);
                    local.d(str2, sb.toString());
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    ku3 ku3 = new ku3();
                    ku3.a("email", bVar.a());
                    f fVar = new f(this, ku3, (xe6) null);
                    eVar.L$0 = this;
                    eVar.L$1 = bVar;
                    eVar.L$2 = ku3;
                    eVar.label = 1;
                    obj = ResponseKt.a(fVar, eVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) eVar.L$2;
                    b bVar2 = (b) eVar.L$1;
                    ResetPasswordUseCase resetPasswordUseCase = (ResetPasswordUseCase) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new d();
                }
                if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = e;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Inside .run failed with error=");
                    zo4 zo4 = (zo4) ap4;
                    ServerError c2 = zo4.c();
                    if (c2 != null) {
                        num = c2.getCode();
                    }
                    sb2.append(num);
                    local2.d(str3, sb2.toString());
                    ServerError c3 = zo4.c();
                    if (c3 == null || (code = c3.getCode()) == null) {
                        i2 = zo4.a();
                    } else {
                        i2 = code.intValue();
                    }
                    ServerError c4 = zo4.c();
                    if (c4 == null || (str = c4.getMessage()) == null) {
                        str = "";
                    }
                    return new c(i2, str);
                }
                throw new kc6();
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        Integer num2 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
