package com.portfolio.platform.ui.view.chart.overview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.gh6;
import com.fossil.jm4;
import com.fossil.kc6;
import com.fossil.lc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tk4;
import com.fossil.ue6;
import com.fossil.ut4;
import com.fossil.wg6;
import com.fossil.wt4;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OverviewWeekChart extends BarChart {
    @DexIgnore
    public PointF v0;
    @DexIgnore
    public ObjectAnimator w0;
    @DexIgnore
    public ObjectAnimator x0;
    @DexIgnore
    public ut4 y0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ OverviewWeekChart a;

        @DexIgnore
        public b(OverviewWeekChart overviewWeekChart) {
            this.a = overviewWeekChart;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.a.getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            ObjectAnimator b = this.a.w0;
            sb.append(b != null ? Boolean.valueOf(b.isRunning()) : null);
            local.d(tag, sb.toString());
            OverviewWeekChart overviewWeekChart = this.a;
            ut4 c = overviewWeekChart.y0;
            if (c != null) {
                overviewWeekChart.b(c);
                ObjectAnimator a2 = this.a.x0;
                if (a2 != null) {
                    a2.start();
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((BarChart.b) t).c(), ((BarChart.b) t2).c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((BarChart.b) t).c(), ((BarChart.b) t2).c());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public OverviewWeekChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void e(Canvas canvas) {
        int i;
        wg6.b(canvas, "canvas");
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = yd6.a(arrayList, new c()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    BarChart.b bVar = (BarChart.b) it2.next();
                    if (bVar.e() != 0) {
                        Paint mGraphPaint = getMGraphPaint();
                        if (next.a() <= 0 || bVar.e() < next.a()) {
                            i = getMInActiveColor();
                        } else {
                            int i2 = wt4.a[bVar.c().ordinal()];
                            if (i2 == 1) {
                                i = getMLowestColor();
                            } else if (i2 == 2) {
                                i = getMDefaultColor();
                            } else if (i2 == 3) {
                                i = getMHighestColor();
                            } else {
                                throw new kc6();
                            }
                        }
                        mGraphPaint.setColor(i);
                        canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator f(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofInt("maxValue", new int[]{i, i4 * i}), PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        wg6.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public void g(Canvas canvas) {
        String b2;
        wg6.b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        if (getMGoalIconShow()) {
            canvas.drawPath(getMGoalLinePath(), getMGraphGoalLinePaint());
            int size = getMChartModel().a().size();
            if (size > 0) {
                Rect rect = new Rect();
                String a2 = a(getMChartModel().a().get(size - 1).a());
                getMLegendPaint().getTextBounds(a2, 0, a2.length(), rect);
                getMLegendPaint().setColor(getMActiveColor());
                canvas.drawText(a2, (width - getMGraphLegendMargin()) - ((float) rect.width()), this.v0.y + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
                if (!TextUtils.isEmpty(getMTextColor()) && (b2 = ThemeManager.l.a().b(getMTextColor())) != null) {
                    getMLegendPaint().setColor(Color.parseColor(b2));
                }
            }
        }
    }

    @DexIgnore
    public void h(Canvas canvas) {
        wg6.b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        Iterator<lc6<Integer, PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            lc6 next = it.next();
            Rect rect = new Rect();
            String a2 = a(((Number) next.getFirst()).intValue());
            getMLegendPaint().getTextBounds(a2, 0, a2.length(), rect);
            float f = ((PointF) next.getSecond()).y;
            canvas.drawLine(0.0f, f, width, f, getMLegendLinePaint());
            canvas.drawText(a2, (width - getMGraphLegendMargin()) - ((float) rect.width()), f + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public void a() {
        super.a();
        setMNumberBar(7);
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
        d();
        e();
        h(canvas);
        e(canvas);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.v0 = new PointF();
    }

    @DexIgnore
    public void a(Canvas canvas) {
        Bitmap a2;
        wg6.b(canvas, "canvas");
        super.a(canvas);
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List a3 = yd6.a(arrayList, new d());
            if ((!a3.isEmpty()) && next.d() && (a2 = BarChart.a((BarChart) this, getMLegendIconRes(), 0, 2, (Object) null)) != null) {
                RectF a4 = ((BarChart.b) a3.get(0)).a();
                canvas.drawBitmap(a2, a4.left + ((a4.width() - ((float) a2.getWidth())) * 0.5f), a4.bottom + ((float) getMTextMargin()), new Paint(1));
                a2.recycle();
            }
        }
        f(canvas);
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator b(OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.f(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    public void e() {
        T t;
        ArrayList<BarChart.a> a2 = getMChartModel().a();
        RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMarginEnd(), (float) getMGraphHeight());
        float height = rectF.height();
        float f = rectF.left;
        gh6 gh6 = new gh6();
        gh6 gh62 = new gh6();
        gh6 gh63 = new gh6();
        Iterator<T> it = a2.iterator();
        int i = 0;
        if (!it.hasNext()) {
            t = null;
        } else {
            t = it.next();
            if (it.hasNext()) {
                ArrayList<BarChart.b> arrayList = ((BarChart.a) t).c().get(0);
                wg6.a((Object) arrayList, "it.mListOfBarPoints[0]");
                int i2 = 0;
                for (BarChart.b e : arrayList) {
                    i2 += e.e();
                }
                do {
                    T next = it.next();
                    ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).c().get(0);
                    wg6.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                    int i3 = 0;
                    for (BarChart.b e2 : arrayList2) {
                        i3 += e2.e();
                    }
                    if (i2 < i3) {
                        i2 = i3;
                        t = next;
                    }
                } while (it.hasNext());
            }
        }
        BarChart.a aVar = (BarChart.a) t;
        int size = a2.size() - 1;
        Iterator<T> it2 = a2.iterator();
        float f2 = f;
        int i4 = 0;
        boolean z = false;
        float f3 = 0.0f;
        float f4 = 0.0f;
        while (it2.hasNext()) {
            T next2 = it2.next();
            int i5 = i4 + 1;
            if (i4 >= 0) {
                BarChart.a aVar2 = (BarChart.a) next2;
                ArrayList<BarChart.b> arrayList3 = aVar2.c().get(i);
                wg6.a((Object) arrayList3, "item.mListOfBarPoints[0]");
                Iterator<T> it3 = it2;
                float a3 = (float) aVar2.a();
                String str = "item.mListOfBarPoints[0]";
                gh62.element = 0.0f;
                gh6.element = getMBarWidth() + f2;
                Iterator it4 = arrayList3.iterator();
                while (it4.hasNext()) {
                    Iterator it5 = it4;
                    gh62.element += (float) ((BarChart.b) it4.next()).e();
                    float mMaxValue = (gh62.element * height) / ((float) getMMaxValue());
                    if (mMaxValue != 0.0f && mMaxValue < getMBarRadius()) {
                        mMaxValue = getMBarRadius();
                    }
                    f4 = ((float) getMGraphHeight()) - mMaxValue;
                    it4 = it5;
                }
                setMGoalIconShow(true);
                float mGraphHeight = ((float) getMGraphHeight()) - ((a3 * height) / ((float) getMMaxValue()));
                gh63.element = gh6.element + (getMBarSpace() * 0.5f);
                float f5 = gh63.element;
                float f6 = rectF.right;
                if (f5 > f6) {
                    gh63.element = f6 + (getMBarMargin() * 0.5f);
                }
                if (!z) {
                    getMGoalLinePath().moveTo(0.0f, mGraphHeight);
                    getMGoalLinePath().lineTo(gh63.element, mGraphHeight);
                    z = true;
                } else {
                    getMGoalLinePath().lineTo(f3, mGraphHeight);
                    getMGoalLinePath().lineTo(gh63.element, mGraphHeight);
                    getMGoalIconPoint().set(gh63.element, mGraphHeight - (((float) getMGoalIconSize()) * 0.5f));
                }
                if (i4 == size) {
                    getMGoalLinePath().lineTo((float) getMGraphWidth(), mGraphHeight);
                    this.v0 = new PointF((float) getMGraphWidth(), mGraphHeight);
                }
                f3 = gh63.element;
                if (wg6.a((Object) aVar, (Object) aVar2)) {
                    ArrayList<BarChart.b> arrayList4 = aVar2.c().get(0);
                    wg6.a((Object) arrayList4, str);
                    int i6 = 0;
                    for (BarChart.b e3 : arrayList4) {
                        i6 += e3.e();
                    }
                    if (i6 >= aVar2.a()) {
                        getMStarIconPoint().add(new PointF(f2 - ((float) getMStarIconSize()), f4 - (((float) getMStarIconSize()) * 1.25f)));
                        float mGraphHeight2 = f4 + ((((float) getMGraphHeight()) - f4) * 0.5f);
                        getMStarIconPoint().add(new PointF(gh6.element + (((float) getMStarIconSize()) * 0.5f), mGraphHeight2 - (((float) getMStarIconSize()) * 2.0f)));
                        getMStarIconPoint().add(new PointF(f2 - ((float) getMStarIconSize()), mGraphHeight2));
                    }
                }
                f2 = gh6.element + getMBarSpace();
                i4 = i5;
                it2 = it3;
                i = 0;
            } else {
                qd6.c();
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(float f, float f2) {
        Rect rect = new Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        float f3 = f;
        for (int i = 0; i < 7; i++) {
            if (i < size) {
                String str = getMLegendTexts().get(i);
                wg6.a((Object) str, "mLegendTexts[i]");
                String str2 = str;
                getMLegendPaint().getTextBounds(str2, 0, str2.length(), rect);
                float mBarWidth = getMBarWidth() + f3;
                getMTextPoint().add(new lc6(str2, new PointF(((f3 + mBarWidth) - ((float) rect.width())) * 0.5f, mLegendHeight)));
                f3 = mBarWidth + getMBarSpace();
            }
        }
    }

    @DexIgnore
    public String a(int i) {
        float f = (float) i;
        String valueOf = String.valueOf((int) f);
        float f2 = (float) 1000;
        if (f < f2) {
            return valueOf;
        }
        return tk4.a(f / f2, 1) + jm4.a(getContext(), 2131886461);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0202, code lost:
        return;
     */
    @DexIgnore
    public synchronized void a(ut4 ut4) {
        wg6.b(ut4, "model");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("changeModel - model=");
        sb.append(ut4);
        sb.append(", mOutAnim.isRunning=");
        ObjectAnimator objectAnimator = this.w0;
        sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
        sb.append(", mInAnim.isRunning=");
        ObjectAnimator objectAnimator2 = this.x0;
        sb.append(objectAnimator2 != null ? Boolean.valueOf(objectAnimator2.isRunning()) : null);
        local.d(tag, sb.toString());
        ObjectAnimator objectAnimator3 = this.w0;
        Boolean valueOf = objectAnimator3 != null ? Boolean.valueOf(objectAnimator3.isRunning()) : null;
        ObjectAnimator objectAnimator4 = this.x0;
        Boolean valueOf2 = objectAnimator4 != null ? Boolean.valueOf(objectAnimator4.isRunning()) : null;
        if (!wg6.a((Object) valueOf, (Object) true)) {
            if (!wg6.a((Object) valueOf2, (Object) true)) {
                if (wg6.a((Object) getMChartModel(), (Object) ut4)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - mChartModel == model");
                    return;
                }
                this.y0 = ut4;
                this.w0 = b(this, getMMaxValue(), 255, 0, 0, 8, (Object) null);
                ut4 ut42 = this.y0;
                if (ut42 != null) {
                    this.x0 = a(this, ((BarChart.c) ut42).c(), 0, 255, 0, 8, (Object) null);
                    ObjectAnimator objectAnimator5 = this.w0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.addListener(new b(this));
                    }
                    ObjectAnimator objectAnimator6 = this.w0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.start();
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            }
        }
        if (wg6.a((Object) ut4, (Object) this.y0)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - model == mTempModel");
            return;
        }
        this.y0 = ut4;
        if (wg6.a((Object) valueOf, (Object) true)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - outRunning == true");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("changeModel - outRunning == true - mMaxValue=");
            ut4 ut43 = this.y0;
            if (ut43 != null) {
                sb2.append(((BarChart.c) ut43).c());
                local2.d(tag2, sb2.toString());
                ut4 ut44 = this.y0;
                if (ut44 != null) {
                    this.x0 = a(this, ((BarChart.c) ut44).c(), 0, 255, 0, 8, (Object) null);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - inRunning == true");
            ObjectAnimator objectAnimator7 = this.x0;
            if (objectAnimator7 != null) {
                objectAnimator7.cancel();
            }
            int mMaxValue = getMMaxValue();
            int c2 = getMChartModel().c();
            int mBarAlpha = getMBarAlpha();
            if (c2 <= 0) {
                c2 = 1;
            }
            int i = mMaxValue / c2;
            ut4 ut45 = this.y0;
            if (ut45 != null) {
                b(ut45);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tag3 = getTAG();
                local3.d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", " + "tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().c());
                this.x0 = e(getMChartModel().c(), mBarAlpha, 255, i);
                ObjectAnimator objectAnimator8 = this.x0;
                if (objectAnimator8 != null) {
                    objectAnimator8.start();
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator a(OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.e(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    public final ObjectAnimator e(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofInt("maxValue", new int[]{i4 * i, i}), PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        wg6.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }
}
