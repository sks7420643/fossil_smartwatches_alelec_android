package com.portfolio.platform.ui.view.chart.overview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.jm4;
import com.fossil.lc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tk4;
import com.fossil.ue6;
import com.fossil.ut4;
import com.fossil.wg6;
import com.fossil.yd6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OverviewDayChart extends BarChart {
    @DexIgnore
    public ObjectAnimator v0;
    @DexIgnore
    public ObjectAnimator w0;
    @DexIgnore
    public ut4 x0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ OverviewDayChart a;

        @DexIgnore
        public b(OverviewDayChart overviewDayChart) {
            this.a = overviewDayChart;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.a.getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            ObjectAnimator b = this.a.v0;
            sb.append(b != null ? Boolean.valueOf(b.isRunning()) : null);
            local.d(tag, sb.toString());
            OverviewDayChart overviewDayChart = this.a;
            ut4 c = overviewDayChart.x0;
            if (c != null) {
                overviewDayChart.b(c);
                ObjectAnimator a2 = this.a.w0;
                if (a2 != null) {
                    a2.start();
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((BarChart.b) t).c(), ((BarChart.b) t2).c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((BarChart.b) t).c(), ((BarChart.b) t2).c());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public OverviewDayChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void e(Canvas canvas) {
        Object obj;
        wg6.b(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        int a2 = a((List<BarChart.a>) getMChartModel().a());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List a3 = yd6.a(arrayList, new c());
            Paint mGraphPaint = getMGraphPaint();
            wg6.a((Object) next, "item");
            mGraphPaint.setAlpha(a(next, a2));
            Iterator it2 = a3.iterator();
            if (!it2.hasNext()) {
                obj = null;
            } else {
                obj = it2.next();
                if (it2.hasNext()) {
                    int e = ((BarChart.b) obj).e();
                    do {
                        Object next2 = it2.next();
                        int e2 = ((BarChart.b) next2).e();
                        if (e < e2) {
                            obj = next2;
                            e = e2;
                        }
                    } while (it2.hasNext());
                }
            }
            BarChart.b bVar = (BarChart.b) obj;
            if (bVar != null) {
                canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator f(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofInt("maxValue", new int[]{i, i4 * i}), PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        wg6.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void h(Canvas canvas) {
        wg6.b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        Iterator<lc6<Integer, PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            lc6 next = it.next();
            Rect rect = new Rect();
            float intValue = (float) ((Number) next.getFirst()).intValue();
            String valueOf = String.valueOf((int) intValue);
            float f = (float) 1000;
            if (intValue >= f) {
                valueOf = tk4.a(intValue / f, 1) + jm4.a((Context) PortfolioApp.get.instance(), 2131886461);
            }
            String str = valueOf;
            getMLegendPaint().getTextBounds(str, 0, str.length(), rect);
            float f2 = ((PointF) next.getSecond()).y;
            canvas.drawLine(0.0f, f2, width, f2, getMLegendLinePaint());
            canvas.drawText(str, (width - getMGraphLegendMargin()) - ((float) rect.width()), f2 + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public void setGraphPreviewColor(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor color=" + i);
        Integer.valueOf(i);
        getMGraph().invalidate();
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public void a() {
        super.a();
        setMNumberBar(24);
        h();
    }

    @DexIgnore
    public void b(ut4 ut4) {
        int i;
        wg6.b(ut4, "model");
        setMChartModel((BarChart.c) ut4);
        setMMaxValue(getMChartModel().c());
        if (getMChartModel().a().size() <= 1) {
            i = 24;
        } else {
            i = getMChartModel().a().size();
        }
        setMNumberBar(i);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0202, code lost:
        return;
     */
    @DexIgnore
    public synchronized void a(ut4 ut4) {
        wg6.b(ut4, "model");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("changeModel - model=");
        sb.append(ut4);
        sb.append(", mOutAnim.isRunning=");
        ObjectAnimator objectAnimator = this.v0;
        sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
        sb.append(", mInAnim.isRunning=");
        ObjectAnimator objectAnimator2 = this.w0;
        sb.append(objectAnimator2 != null ? Boolean.valueOf(objectAnimator2.isRunning()) : null);
        local.d(tag, sb.toString());
        ObjectAnimator objectAnimator3 = this.v0;
        Boolean valueOf = objectAnimator3 != null ? Boolean.valueOf(objectAnimator3.isRunning()) : null;
        ObjectAnimator objectAnimator4 = this.w0;
        Boolean valueOf2 = objectAnimator4 != null ? Boolean.valueOf(objectAnimator4.isRunning()) : null;
        if (!wg6.a((Object) valueOf, (Object) true)) {
            if (!wg6.a((Object) valueOf2, (Object) true)) {
                if (wg6.a((Object) getMChartModel(), (Object) ut4)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - mChartModel == model");
                    return;
                }
                this.x0 = ut4;
                this.v0 = b(this, getMMaxValue(), 255, 0, 0, 8, (Object) null);
                ut4 ut42 = this.x0;
                if (ut42 != null) {
                    this.w0 = a(this, ((BarChart.c) ut42).c(), 0, 255, 0, 8, (Object) null);
                    ObjectAnimator objectAnimator5 = this.v0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.addListener(new b(this));
                    }
                    ObjectAnimator objectAnimator6 = this.v0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.start();
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            }
        }
        if (wg6.a((Object) ut4, (Object) this.x0)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - model == mTempModel");
            return;
        }
        this.x0 = ut4;
        if (wg6.a((Object) valueOf, (Object) true)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - outRunning == true");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("changeModel - outRunning == true - mMaxValue=");
            ut4 ut43 = this.x0;
            if (ut43 != null) {
                sb2.append(((BarChart.c) ut43).c());
                local2.d(tag2, sb2.toString());
                ut4 ut44 = this.x0;
                if (ut44 != null) {
                    this.w0 = a(this, ((BarChart.c) ut44).c(), 0, 255, 0, 8, (Object) null);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - inRunning == true");
            ObjectAnimator objectAnimator7 = this.w0;
            if (objectAnimator7 != null) {
                objectAnimator7.cancel();
            }
            int mMaxValue = getMMaxValue();
            int c2 = getMChartModel().c();
            int mBarAlpha = getMBarAlpha();
            if (c2 <= 0) {
                c2 = 1;
            }
            int i = mMaxValue / c2;
            ut4 ut45 = this.x0;
            if (ut45 != null) {
                b(ut45);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tag3 = getTAG();
                local3.d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", " + "tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().c());
                this.w0 = e(getMChartModel().c(), mBarAlpha, 255, i);
                ObjectAnimator objectAnimator8 = this.w0;
                if (objectAnimator8 != null) {
                    objectAnimator8.start();
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator b(OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.f(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
        d();
        e();
        h(canvas);
        e(canvas);
        h();
    }

    @DexIgnore
    public final void h() {
        if (!TextUtils.isEmpty(getMBackgroundColor())) {
            String b2 = ThemeManager.l.a().b(getMBackgroundColor());
            if (!TextUtils.isEmpty(b2)) {
                setBackgroundColor(Color.parseColor(b2));
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator e(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofInt("maxValue", new int[]{i4 * i, i}), PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        wg6.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator a(OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.e(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    public void a(Canvas canvas) {
        Bitmap a2;
        wg6.b(canvas, "canvas");
        super.a(canvas);
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List a3 = yd6.a(arrayList, new d());
            if ((!a3.isEmpty()) && next.d() && (a2 = BarChart.a((BarChart) this, getMLegendIconRes(), 0, 2, (Object) null)) != null) {
                RectF a4 = ((BarChart.b) a3.get(0)).a();
                canvas.drawBitmap(a2, a4.left + ((a4.width() - ((float) a2.getWidth())) * 0.5f), a4.bottom + ((float) getMTextMargin()), new Paint(1));
                a2.recycle();
            }
        }
    }

    @DexIgnore
    public void a(float f, float f2) {
        Rect rect = new Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        if (size < 1) {
            getMTextPoint().clear();
            return;
        }
        float f3 = size == 1 ? 0.0f : (f2 - f) / ((float) (size - 1));
        float mBarWidth = f + (getMBarWidth() * 0.5f);
        int i = 0;
        for (T next : getMLegendTexts()) {
            int i2 = i + 1;
            if (i >= 0) {
                String str = (String) next;
                if (yj6.a((CharSequence) str, (CharSequence) "+", false, 2, (Object) null)) {
                    String str2 = (String) yj6.a((CharSequence) str, new String[]{"+"}, false, 0, 6, (Object) null).get(0);
                    getMLegendPaint().getTextBounds(str2, 0, str2.length(), rect);
                    getMTextPoint().add(new lc6(str, new PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                } else {
                    getMLegendPaint().getTextBounds(str, 0, str.length(), rect);
                    getMTextPoint().add(new lc6(str, new PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                }
                i = i2;
            } else {
                qd6.c();
                throw null;
            }
        }
    }

    @DexIgnore
    public final int a(List<BarChart.a> list) {
        if (list.isEmpty()) {
            return 0;
        }
        int i = Integer.MIN_VALUE;
        for (BarChart.a aVar : list) {
            if (!aVar.c().isEmpty()) {
                ArrayList<BarChart.b> arrayList = aVar.c().get(0);
                wg6.a((Object) arrayList, "model.mListOfBarPoints[0]");
                for (BarChart.b e : arrayList) {
                    int e2 = e.e();
                    if (e2 > i) {
                        i = e2;
                    }
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final int a(BarChart.a aVar, int i) {
        Object obj;
        if (i < 0) {
            return 255;
        }
        int i2 = 0;
        ArrayList<BarChart.b> arrayList = aVar.c().get(0);
        wg6.a((Object) arrayList, "barModel.mListOfBarPoints[0]");
        Iterator it = arrayList.iterator();
        if (!it.hasNext()) {
            obj = null;
        } else {
            obj = it.next();
            if (it.hasNext()) {
                int e = ((BarChart.b) obj).e();
                do {
                    Object next = it.next();
                    int e2 = ((BarChart.b) next).e();
                    if (e < e2) {
                        obj = next;
                        e = e2;
                    }
                } while (it.hasNext());
            }
        }
        BarChart.b bVar = (BarChart.b) obj;
        if (bVar != null) {
            i2 = bVar.e();
        }
        double d2 = (double) (((float) i2) / ((float) i));
        if (d2 >= 0.75d) {
            return 255;
        }
        return (int) ((d2 < 0.5d || d2 >= 0.75d) ? (d2 < 0.25d || d2 >= 0.5d) ? 63.75d : 127.5d : 191.25d);
    }
}
