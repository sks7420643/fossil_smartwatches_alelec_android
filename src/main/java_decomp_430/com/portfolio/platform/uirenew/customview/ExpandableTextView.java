package com.portfolio.platform.uirenew.customview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.appcompat.widget.AppCompatTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ExpandableTextView extends AppCompatTextView {
    @DexIgnore
    public /* final */ List<c> e;
    @DexIgnore
    public TimeInterpolator f;
    @DexIgnore
    public TimeInterpolator g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.TextView, com.portfolio.platform.uirenew.customview.ExpandableTextView] */
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ExpandableTextView.this.setHeight(((Integer) valueAnimator.getAnimatedValue()).intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ExpandableTextView.this.g();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(ExpandableTextView expandableTextView);
    }

    @DexIgnore
    public ExpandableTextView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.widget.TextView, com.portfolio.platform.uirenew.customview.ExpandableTextView] */
    public boolean e() {
        if (this.j || this.i || this.h < 0) {
            return false;
        }
        f();
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        this.o = getMeasuredHeight();
        this.i = true;
        setMaxLines(Integer.MAX_VALUE);
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{this.o, getMeasuredHeight()});
        ofInt.addUpdateListener(new a());
        ofInt.addListener(new b());
        ofInt.setInterpolator(this.f);
        ofInt.setDuration(750).start();
        return true;
    }

    @DexIgnore
    public final void f() {
        for (c a2 : this.e) {
            a2.a(this);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.TextView, com.portfolio.platform.uirenew.customview.ExpandableTextView] */
    public void g() {
        setMaxHeight(Integer.MAX_VALUE);
        setMinHeight(0);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = -2;
        setLayoutParams(layoutParams);
        this.j = true;
        this.i = false;
    }

    @DexIgnore
    public TimeInterpolator getCollapseInterpolator() {
        return this.g;
    }

    @DexIgnore
    public TimeInterpolator getExpandInterpolator() {
        return this.f;
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.h == 0 && !this.j && !this.i) {
            i3 = View.MeasureSpec.makeMeasureSpec(0, 1073741824);
        }
        ExpandableTextView.super.onMeasure(i2, i3);
    }

    @DexIgnore
    public void setCollapseInterpolator(TimeInterpolator timeInterpolator) {
        this.g = timeInterpolator;
    }

    @DexIgnore
    public void setExpandInterpolator(TimeInterpolator timeInterpolator) {
        this.f = timeInterpolator;
    }

    @DexIgnore
    public void setInterpolator(TimeInterpolator timeInterpolator) {
        this.f = timeInterpolator;
        this.g = timeInterpolator;
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.TextView, com.portfolio.platform.uirenew.customview.ExpandableTextView] */
    public ExpandableTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = getMaxLines();
        this.e = new ArrayList();
        this.f = new AccelerateDecelerateInterpolator();
        this.g = new AccelerateDecelerateInterpolator();
    }
}
