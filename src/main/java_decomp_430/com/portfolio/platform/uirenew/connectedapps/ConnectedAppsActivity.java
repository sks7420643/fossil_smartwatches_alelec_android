package com.portfolio.platform.uirenew.connectedapps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ew4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectedAppsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((qg6) null);
    @DexIgnore
    public ConnectedAppsFragment B;
    @DexIgnore
    public ConnectedAppsPresenter C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            Intent intent = new Intent(context, ConnectedAppsActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        ConnectedAppsActivity.super.onActivityResult(i, i2, intent);
        ConnectedAppsFragment connectedAppsFragment = this.B;
        if (connectedAppsFragment != null) {
            connectedAppsFragment.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        this.B = getSupportFragmentManager().b(2131362119);
        if (this.B == null) {
            this.B = ConnectedAppsFragment.j.b();
            ConnectedAppsFragment connectedAppsFragment = this.B;
            if (connectedAppsFragment != null) {
                a((Fragment) connectedAppsFragment, ConnectedAppsFragment.j.a(), 2131362119);
            } else {
                wg6.a();
                throw null;
            }
        }
        y04 g = PortfolioApp.get.instance().g();
        ConnectedAppsFragment connectedAppsFragment2 = this.B;
        if (connectedAppsFragment2 != null) {
            g.a(new ew4(connectedAppsFragment2)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.connectedapps.ConnectedAppsContract.View");
    }
}
