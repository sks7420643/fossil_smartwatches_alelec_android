package com.portfolio.platform.uirenew.ota;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zq5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwareActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a((qg6) null);
    @DexIgnore
    public UpdateFirmwarePresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwareActivity.C;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            wg6.b(context, "context");
            wg6.b(str, "serial");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z + " serial " + str);
            Intent intent = new Intent(context, UpdateFirmwareActivity.class);
            intent.putExtra("IS_ONBOARDING_FLOW", z);
            intent.putExtra("SERIAL", str);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareActivity.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "UpdateFirmwareActivity::class.java.simpleName!!");
            C = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        UpdateFirmwareFragment b = getSupportFragmentManager().b(2131362119);
        Intent intent = getIntent();
        String str = "";
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
            String stringExtra = intent.getStringExtra("SERIAL");
            if (stringExtra != null) {
                str = stringExtra;
            }
        }
        if (b == null) {
            b = UpdateFirmwareFragment.t.a(z, str);
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new zq5(b)).a(this);
    }
}
