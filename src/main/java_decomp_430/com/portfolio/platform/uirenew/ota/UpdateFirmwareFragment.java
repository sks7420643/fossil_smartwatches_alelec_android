package com.portfolio.platform.uirenew.ota;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.k34;
import com.fossil.kb;
import com.fossil.oj3;
import com.fossil.ox5;
import com.fossil.pe4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xq5;
import com.fossil.yq5;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwareFragment extends BaseFragment implements yq5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a t; // = new a((qg6) null);
    @DexIgnore
    public xq5 f;
    @DexIgnore
    public ax5<pe4> g;
    @DexIgnore
    public k34 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String o; // = "";
    @DexIgnore
    public /* final */ String p; // = ThemeManager.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String q; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String r; // = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final UpdateFirmwareFragment a(boolean z, String str) {
            wg6.b(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putString("SERIAL", str);
            UpdateFirmwareFragment updateFirmwareFragment = new UpdateFirmwareFragment();
            updateFirmwareFragment.setArguments(bundle);
            return updateFirmwareFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareFragment a;

        @DexIgnore
        public b(UpdateFirmwareFragment updateFirmwareFragment) {
            this.a = updateFirmwareFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ pe4 a;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareFragment b;

        @DexIgnore
        public c(pe4 pe4, UpdateFirmwareFragment updateFirmwareFragment) {
            this.a = pe4;
            this.b = updateFirmwareFragment;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            UpdateFirmwareFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.q)) {
                int parseColor = Color.parseColor(this.b.q);
                TabLayout.g b4 = this.a.y.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.p) && this.b.j != i) {
                int parseColor2 = Color.parseColor(this.b.p);
                TabLayout.g b5 = this.a.y.b(this.b.j);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareFragment a;

        @DexIgnore
        public d(UpdateFirmwareFragment updateFirmwareFragment) {
            this.a = updateFirmwareFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareFragment a;

        @DexIgnore
        public e(UpdateFirmwareFragment updateFirmwareFragment) {
            this.a = updateFirmwareFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                wg6.a((Object) activity, "it");
                TroubleshootingActivity.a.a(aVar, activity, this.a.o, false, 4, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareFragment a;

        @DexIgnore
        public f(UpdateFirmwareFragment updateFirmwareFragment) {
            this.a = updateFirmwareFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.p) && !TextUtils.isEmpty(this.a.q)) {
                int parseColor = Color.parseColor(this.a.p);
                int parseColor2 = Color.parseColor(this.a.q);
                gVar.b(2131231004);
                if (i == this.a.j) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void D0() {
        if (isActive()) {
            ax5<pe4> ax5 = this.g;
            if (ax5 != null) {
                pe4 a2 = ax5.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    wg6.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wg6.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    Object r1 = a2.s;
                    wg6.a((Object) r1, "it.fbContinue");
                    r1.setVisibility(0);
                    Object r12 = a2.x;
                    wg6.a((Object) r12, "it.ftvUpdateWarning");
                    r12.setVisibility(4);
                    ProgressBar progressBar = a2.A;
                    wg6.a((Object) progressBar, "it.progressUpdate");
                    progressBar.setVisibility(8);
                    Object r13 = a2.u;
                    wg6.a((Object) r13, "it.ftvCountdownTime");
                    r13.setVisibility(8);
                    Object r0 = a2.w;
                    wg6.a((Object) r0, "it.ftvUpdate");
                    r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886550));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d0() {
        ax5<pe4> ax5 = this.g;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            DashBar dashBar = a2 != null ? a2.z : null;
            if (dashBar != null) {
                wg6.a((Object) dashBar, "mBinding.get()?.progressBar!!");
                dashBar.setVisibility(8);
                return;
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g() {
        DashBar dashBar;
        ax5<pe4> ax5 = this.g;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.z) != null) {
                ax5<pe4> ax52 = this.g;
                if (ax52 != null) {
                    pe4 a3 = ax52.a();
                    DashBar dashBar2 = a3 != null ? a3.z : null;
                    if (dashBar2 != null) {
                        wg6.a((Object) dashBar2, "mBinding.get()?.progressBar!!");
                        dashBar2.setVisibility(0);
                        ox5.a aVar = ox5.a;
                        wg6.a((Object) dashBar, "this");
                        aVar.f(dashBar, this.i, 500);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void h(int i2) {
        ProgressBar progressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        ax5<pe4> ax5 = this.g;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null && (progressBar = a2.A) != null) {
                progressBar.setProgress(i2);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final xq5 j1() {
        xq5 xq5 = this.f;
        if (xq5 != null) {
            return xq5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void k1() {
        ax5<pe4> ax5 = this.g;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            TabLayout tabLayout = a2 != null ? a2.y : null;
            if (tabLayout != null) {
                ax5<pe4> ax52 = this.g;
                if (ax52 != null) {
                    pe4 a3 = ax52.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.C : null;
                    if (viewPager2 != null) {
                        new oj3(tabLayout, viewPager2, new f(this)).a();
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void m0() {
        if (isAdded()) {
            AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
            fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886562));
            fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886561));
            fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886564));
            fVar.b(2131363190);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        UpdateFirmwareFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        pe4 a2 = kb.a(layoutInflater, 2131558620, viewGroup, false, e1());
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        UpdateFirmwareFragment.super.onPause();
        xq5 xq5 = this.f;
        if (xq5 != null) {
            xq5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        UpdateFirmwareFragment.super.onResume();
        xq5 xq5 = this.f;
        if (xq5 != null) {
            xq5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r6v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.i = arguments.getBoolean("IS_ONBOARDING_FLOW");
            String string = arguments.getString("SERIAL");
            if (string == null) {
                string = "";
            }
            this.o = string;
            xq5 xq5 = this.f;
            if (xq5 != null) {
                xq5.a(this.i, this.o);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        this.h = new k34(new ArrayList());
        ax5<pe4> ax5 = this.g;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                wg6.a((Object) constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                wg6.a((Object) constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                ProgressBar progressBar = a2.A;
                wg6.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                Object r0 = a2.s;
                wg6.a((Object) r0, "binding.fbContinue");
                r0.setVisibility(8);
                Object r02 = a2.x;
                wg6.a((Object) r02, "binding.ftvUpdateWarning");
                r02.setVisibility(0);
                a2.s.setOnClickListener(new b(this));
                ViewPager2 viewPager2 = a2.C;
                wg6.a((Object) viewPager2, "binding.rvpTutorial");
                k34 k34 = this.h;
                if (k34 != null) {
                    viewPager2.setAdapter(k34);
                    if (!TextUtils.isEmpty(this.r)) {
                        TabLayout tabLayout = a2.y;
                        wg6.a((Object) tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.r)));
                    }
                    k1();
                    a2.C.a(new c(a2, this));
                    a2.t.setOnClickListener(new d(this));
                    a2.v.setOnClickListener(new e(this));
                    return;
                }
                wg6.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void t() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.C;
            wg6.a((Object) activity, "it");
            xq5 xq5 = this.f;
            if (xq5 != null) {
                aVar.a(activity, xq5.j());
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void v0() {
        if (isActive()) {
            ax5<pe4> ax5 = this.g;
            if (ax5 != null) {
                pe4 a2 = ax5.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    wg6.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wg6.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r11v8, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r11v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r11v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void b(boolean z) {
        if (isActive()) {
            ax5<pe4> ax5 = this.g;
            if (ax5 != null) {
                pe4 a2 = ax5.a();
                if (a2 != null) {
                    xq5 xq5 = this.f;
                    if (xq5 == null) {
                        wg6.d("mPresenter");
                        throw null;
                    } else if (xq5.j()) {
                        if (z) {
                            ConstraintLayout constraintLayout = a2.q;
                            wg6.a((Object) constraintLayout, "it.clUpdateFwFail");
                            constraintLayout.setVisibility(8);
                            ConstraintLayout constraintLayout2 = a2.r;
                            wg6.a((Object) constraintLayout2, "it.clUpdatingFw");
                            constraintLayout2.setVisibility(0);
                            Object r11 = a2.s;
                            wg6.a((Object) r11, "it.fbContinue");
                            r11.setVisibility(0);
                            Object r112 = a2.x;
                            wg6.a((Object) r112, "it.ftvUpdateWarning");
                            r112.setVisibility(4);
                            ProgressBar progressBar = a2.A;
                            wg6.a((Object) progressBar, "it.progressUpdate");
                            progressBar.setProgress(1000);
                            Object r113 = a2.w;
                            wg6.a((Object) r113, "it.ftvUpdate");
                            r113.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886802));
                            return;
                        }
                        ConstraintLayout constraintLayout3 = a2.q;
                        wg6.a((Object) constraintLayout3, "it.clUpdateFwFail");
                        constraintLayout3.setVisibility(0);
                        ConstraintLayout constraintLayout4 = a2.r;
                        wg6.a((Object) constraintLayout4, "it.clUpdatingFw");
                        constraintLayout4.setVisibility(8);
                    } else if (getActivity() == null) {
                    } else {
                        if (z) {
                            xq5 xq52 = this.f;
                            if (xq52 != null) {
                                xq52.h();
                                h();
                                return;
                            }
                            wg6.d("mPresenter");
                            throw null;
                        }
                        TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                        Context context = getContext();
                        if (context != null) {
                            wg6.a((Object) context, "context!!");
                            TroubleshootingActivity.a.a(aVar, context, this.o, false, 4, (Object) null);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wg6.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public void a(xq5 xq5) {
        wg6.b(xq5, "presenter");
        this.f = xq5;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -879828873) {
            if (hashCode == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.i);
                if (i2 == 2131362228) {
                    xq5 xq5 = this.f;
                    if (xq5 != null) {
                        xq5.l();
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else if (i2 != 2131362311) {
                    if (i2 == 2131362561) {
                        xq5 xq52 = this.f;
                        if (xq52 != null) {
                            xq52.h();
                            FragmentActivity activity = getActivity();
                            if (activity != null) {
                                activity.finish();
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                } else if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.C;
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        wg6.a((Object) activity2, "activity!!");
                        aVar.a(activity2);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        } else if (str.equals("NETWORK_ERROR") && i2 == 2131362220) {
            xq5 xq53 = this.f;
            if (xq53 != null) {
                xq53.i();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void h(List<? extends Explore> list) {
        wg6.b(list, "data");
        ax5<pe4> ax5 = this.g;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(this.o)) {
                    Object r0 = a2.w;
                    wg6.a((Object) r0, "it.ftvUpdate");
                    r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886792));
                } else {
                    Object r02 = a2.w;
                    wg6.a((Object) r02, "it.ftvUpdate");
                    r02.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886792));
                }
            }
            k34 k34 = this.h;
            if (k34 != null) {
                k34.a(list);
            } else {
                wg6.d("mAdapterUpdateFirmware");
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
