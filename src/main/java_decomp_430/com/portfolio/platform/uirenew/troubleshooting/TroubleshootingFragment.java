package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.le4;
import com.fossil.q34;
import com.fossil.qg6;
import com.fossil.ru5;
import com.fossil.su5;
import com.fossil.wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TroubleshootingFragment extends BaseFragment implements su5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<le4> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return TroubleshootingFragment.j;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final TroubleshootingFragment a(boolean z, boolean z2) {
            TroubleshootingFragment troubleshootingFragment = new TroubleshootingFragment();
            troubleshootingFragment.g = z;
            troubleshootingFragment.h = z2;
            return troubleshootingFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ TroubleshootingFragment a;

        @DexIgnore
        public b(TroubleshootingFragment troubleshootingFragment) {
            this.a = troubleshootingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.h) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                Context context = this.a.getContext();
                if (context != null) {
                    wg6.a((Object) context, "context!!");
                    PairingInstructionsActivity.a.a(aVar, context, false, true, 2, (Object) null);
                    return;
                }
                wg6.a();
                throw null;
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ TroubleshootingFragment a;

        @DexIgnore
        public c(TroubleshootingFragment troubleshootingFragment) {
            this.a = troubleshootingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ TroubleshootingFragment a;

        @DexIgnore
        public d(TroubleshootingFragment troubleshootingFragment) {
            this.a = troubleshootingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            q34.a.a().a((BaseFragment) this.a);
        }
    }

    /*
    static {
        String simpleName = TroubleshootingFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "TroubleshootingFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(ru5 ru5) {
        wg6.b(ru5, "presenter");
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return j;
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        TroubleshootingFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558617, viewGroup, false, e1()));
        ax5<le4> ax5 = this.f;
        if (ax5 != null) {
            le4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<le4> ax5 = this.f;
        if (ax5 != null) {
            le4 a2 = ax5.a();
            if (a2 != null) {
                View findViewById = a2.s.findViewById(2131362671);
                if (this.g) {
                    wg6.a((Object) findViewById, "batteryText");
                    findViewById.setVisibility(8);
                }
                a2.r.setOnClickListener(new b(this));
                a2.t.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
