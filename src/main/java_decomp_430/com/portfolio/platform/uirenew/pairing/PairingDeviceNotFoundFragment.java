package com.portfolio.platform.uirenew.pairing;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.js5;
import com.fossil.kb;
import com.fossil.ks5;
import com.fossil.le4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.uirenew.pairing.scanning.BasePairingSubFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingDeviceNotFoundFragment extends BasePairingSubFragment implements js5 {
    @DexIgnore
    public ax5<le4> g;
    @DexIgnore
    public ks5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingDeviceNotFoundFragment a;

        @DexIgnore
        public b(PairingDeviceNotFoundFragment pairingDeviceNotFoundFragment) {
            this.a = pairingDeviceNotFoundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingDeviceNotFoundFragment.a(this.a).a(3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingDeviceNotFoundFragment a;

        @DexIgnore
        public c(PairingDeviceNotFoundFragment pairingDeviceNotFoundFragment) {
            this.a = pairingDeviceNotFoundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingDeviceNotFoundFragment.a(this.a).a(3);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ ks5 a(PairingDeviceNotFoundFragment pairingDeviceNotFoundFragment) {
        ks5 ks5 = pairingDeviceNotFoundFragment.h;
        if (ks5 != null) {
            return ks5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        le4 a2 = kb.a(layoutInflater, 2131558617, viewGroup, false, e1());
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        Object r3;
        Object r32;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<le4> ax5 = this.g;
        if (ax5 != null) {
            le4 a2 = ax5.a();
            if (!(a2 == null || (r32 = a2.t) == 0)) {
                r32.setOnClickListener(new b(this));
            }
            ax5<le4> ax52 = this.g;
            if (ax52 != null) {
                le4 a3 = ax52.a();
                if (a3 != null && (r3 = a3.r) != 0) {
                    r3.setOnClickListener(new c(this));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ks5 ks5) {
        wg6.b(ks5, "presenter");
        this.h = ks5;
    }
}
