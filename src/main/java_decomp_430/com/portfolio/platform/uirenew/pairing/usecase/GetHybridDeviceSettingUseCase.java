package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hh6;
import com.fossil.hx5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wg6;
import com.fossil.ws5;
import com.fossil.xe6;
import com.fossil.xs5;
import com.fossil.ys5;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetHybridDeviceSettingUseCase extends m24<xs5, ys5, ws5> {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ MicroAppRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ NotificationsRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ AlarmsRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1", f = "GetHybridDeviceSettingUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SparseArray sparseArray, boolean z, String str, xe6 xe6) {
            super(2, xe6);
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$data, this.$isMovemberModel, this.$serialNumber, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                AppNotificationFilterSettings a = NotificationAppHelper.b.a((SparseArray<List<BaseFeatureModel>>) this.$data, this.$isMovemberModel);
                PortfolioApp.get.instance().a(a, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = GetHybridDeviceSettingUseCase.k;
                local.d(e, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1", f = "GetHybridDeviceSettingUseCase.kt", l = {61, 62, 63, 64, 65, 77, 85, 101}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GetHybridDeviceSettingUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = getHybridDeviceSettingUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: type inference failed for: r14v11, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase] */
        /* JADX WARNING: type inference failed for: r14v19, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase] */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0370, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(r13.this$0);
            r0 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x037c, code lost:
            if (r0 == null) goto L_0x03d2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x037e, code lost:
            r14 = r14.getAllNotificationsByHour(r0, com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
            r0 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.c(r13.this$0);
            r1 = com.portfolio.platform.helper.DeviceHelper.o;
            r2 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x0396, code lost:
            if (r2 == null) goto L_0x03ce;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x0398, code lost:
            r0 = r0.getSkuModelBySerialPrefix(r1.b(r2));
            r1 = com.fossil.NotificationAppHelper.b;
            r2 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x03a8, code lost:
            if (r2 == null) goto L_0x03ca;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x03aa, code lost:
            r0 = r1.a(r0, r2);
            r1 = r13.this$0;
            r2 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x03b4, code lost:
            if (r2 == null) goto L_0x03c6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:0x03b6, code lost:
            com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.a(r1, r14, r2, r0);
            r13.this$0.a(new com.fossil.ys5());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x03c5, code lost:
            return com.fossil.cd6.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x03c6, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x03c9, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x03ca, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x03cd, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x03ce, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:115:0x03d1, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:0x03d2, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x03d5, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x03d6, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x03d9, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x03da, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:121:0x03dd, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x03de, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x03e1, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x03e2, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x03e5, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x03e6, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x03e9, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x03ea, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x03ed, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x03ee, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x03f1, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a8, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.d(r13.this$0);
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x00b4, code lost:
            if (r5 == null) goto L_0x03ee;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b6, code lost:
            r13.L$0 = r1;
            r13.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x00bf, code lost:
            if (r14.downloadAllMicroApp(r5, r13) != r0) goto L_0x00c2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x00c1, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00c2, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f(r13.this$0);
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ce, code lost:
            if (r5 == null) goto L_0x03ea;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d0, code lost:
            r13.L$0 = r1;
            r13.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d9, code lost:
            if (r14.downloadRecommendPresetList(r5, r13) != r0) goto L_0x00dc;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00db, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00dc, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f(r13.this$0);
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00e8, code lost:
            if (r5 == null) goto L_0x03e6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ea, code lost:
            r13.L$0 = r1;
            r13.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00f3, code lost:
            if (r14.downloadPresetList(r5, r13) != r0) goto L_0x00f6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f5, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f6, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.a(r13.this$0);
            r13.L$0 = r1;
            r13.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0105, code lost:
            if (r14.downloadAlarms(r13) != r0) goto L_0x0108;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0107, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0108, code lost:
            r14 = new com.fossil.hh6();
            r14.element = 1;
            r5 = new com.fossil.hh6();
            r5.element = 0;
            r6 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.c(r13.this$0);
            r7 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0122, code lost:
            if (r7 == null) goto L_0x03e2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0124, code lost:
            r6 = r6.getDeviceBySerial(r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0128, code lost:
            if (r6 == null) goto L_0x0136;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x012a, code lost:
            r14.element = r6.getMajor();
            r5.element = r6.getMinor();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0136, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(), "download variant with major " + r14.element + " minor " + r5.element);
            r7 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.d(r13.this$0);
            r8 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x016c, code lost:
            if (r8 == null) goto L_0x03de;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x016e, code lost:
            r9 = java.lang.String.valueOf(r14.element);
            r10 = java.lang.String.valueOf(r5.element);
            r13.L$0 = r1;
            r13.L$1 = r14;
            r13.L$2 = r5;
            r13.L$3 = r6;
            r13.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0189, code lost:
            if (r7.downloadMicroAppVariant(r8, r9, r10, r13) != r0) goto L_0x018c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x018b, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x018c, code lost:
            r7 = r14;
            r8 = r1;
            r12 = r6;
            r6 = r5;
            r5 = r12;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0191, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f(r13.this$0);
            r1 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x019d, code lost:
            if (r1 == null) goto L_0x03da;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x019f, code lost:
            r1 = r14.getPresetList(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x01a7, code lost:
            if (r1.isEmpty() == false) goto L_0x01f5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a9, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f(r13.this$0);
            r9 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x01b5, code lost:
            if (r9 == null) goto L_0x01f1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x01b7, code lost:
            r14 = r14.getHybridRecommendPresetList(r9);
            r9 = r14.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x01c3, code lost:
            if (r9.hasNext() == false) goto L_0x01d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x01c5, code lost:
            r1.add(com.portfolio.platform.data.model.room.microapp.HybridPreset.Companion.cloneFromDefault(r9.next()));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x01d5, code lost:
            r9 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f(r13.this$0);
            r13.L$0 = r8;
            r13.L$1 = r7;
            r13.L$2 = r6;
            r13.L$3 = r5;
            r13.L$4 = r1;
            r13.L$5 = r14;
            r13.label = 7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x01ee, code lost:
            if (r9.upsertHybridPresetList(r1, r13) != r0) goto L_0x01f5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x01f0, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x01f1, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x01f4, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x01f5, code lost:
            r14 = r1.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x01fd, code lost:
            if (r14.hasNext() == false) goto L_0x0215;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x01ff, code lost:
            r9 = r14.next();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x0212, code lost:
            if (com.fossil.hf6.a(r9.isActive()).booleanValue() == false) goto L_0x01f9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x0215, code lost:
            r9 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x0216, code lost:
            r9 = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x0218, code lost:
            if (r9 != null) goto L_0x0240;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x021e, code lost:
            if (r1.isEmpty() == false) goto L_0x0240;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x0220, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(), "activePreset is null, preset list is empty?????");
            r13.this$0.a(new com.fossil.ws5(600, ""));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x023f, code lost:
            return com.fossil.cd6.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x0240, code lost:
            if (r9 != null) goto L_0x0287;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x0242, code lost:
            r14 = r1.get(0);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(), "Active preset is null ,pick " + r14);
            r14.setActive(true);
            r2 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f(r13.this$0);
            r13.L$0 = r8;
            r13.L$1 = r7;
            r13.L$2 = r6;
            r13.L$3 = r5;
            r13.L$4 = r1;
            r13.L$5 = r14;
            r13.label = 8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x0283, code lost:
            if (r2.upsertHybridPreset(r14, r13) != r0) goto L_0x0286;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x0285, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x0286, code lost:
            r9 = r14;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x0287, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(), "activePreset=" + r9);
            r14 = r9.getButtons().iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x02b1, code lost:
            if (r14.hasNext() == false) goto L_0x031d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x02b3, code lost:
            r0 = r14.next();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x02c7, code lost:
            if (com.fossil.wg6.a((java.lang.Object) r0.getAppId(), (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue()) == false) goto L_0x02ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x02c9, code lost:
            r0 = r0.getSettings();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x02d1, code lost:
            if (android.text.TextUtils.isEmpty(r0) != false) goto L_0x02ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
            r0 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) new com.google.gson.Gson().a(r0, com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x02e0, code lost:
            if (r0 == null) goto L_0x02ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x02ea, code lost:
            if (android.text.TextUtils.isEmpty(r0.getTimeZoneId()) != false) goto L_0x02ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x02ec, code lost:
            com.portfolio.platform.PortfolioApp.get.instance().o(r0.getTimeZoneId());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x02fa, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x02fb, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(), "parse secondTimezone, ex=" + r0);
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x031d, code lost:
            r14 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0323, code lost:
            if (r14 == null) goto L_0x03d6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0325, code lost:
            r14 = com.fossil.zi4.a(r9, r14, com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.c(r13.this$0), com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.d(r13.this$0));
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.e(), "bleMappingList " + r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0358, code lost:
            if ((!r14.isEmpty()) == false) goto L_0x0370;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x035a, code lost:
            r0 = com.portfolio.platform.PortfolioApp.get.instance();
            r1 = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.g(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x0366, code lost:
            if (r1 == null) goto L_0x036c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0368, code lost:
            r0.b(r1, (java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping>) r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x036c, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x036f, code lost:
            throw null;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            hh6 hh6;
            hh6 hh62;
            Device device;
            il6 il62;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il62 = this.p$;
                    CategoryRepository b = this.this$0.i;
                    this.L$0 = il62;
                    this.label = 1;
                    if (b.downloadCategories(this) == a) {
                        return a;
                    }
                    break;
                case 1:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 4:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 5:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 6:
                    nc6.a(obj);
                    il6 = (il6) this.L$0;
                    hh6 = (hh6) this.L$1;
                    hh62 = (hh6) this.L$2;
                    device = (Device) this.L$3;
                    break;
                case 7:
                    List list = (List) this.L$5;
                    ArrayList<HybridPreset> arrayList = (ArrayList) this.L$4;
                    device = (Device) this.L$3;
                    hh62 = (hh6) this.L$2;
                    hh6 = (hh6) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 8:
                    ArrayList arrayList2 = (ArrayList) this.L$4;
                    Device device2 = (Device) this.L$3;
                    hh6 hh63 = (hh6) this.L$2;
                    hh6 hh64 = (hh6) this.L$1;
                    il6 il63 = (il6) this.L$0;
                    nc6.a(obj);
                    HybridPreset hybridPreset = (HybridPreset) this.L$5;
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = GetHybridDeviceSettingUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "GetHybridDeviceSettingUs\u2026se::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public GetHybridDeviceSettingUseCase(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        wg6.b(hybridPresetRepository, "mPresetRepository");
        wg6.b(microAppRepository, "mMicroAppRepository");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(notificationsRepository, "mNotificationsRepository");
        wg6.b(categoryRepository, "mCategoryRepository");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.e = hybridPresetRepository;
        this.f = microAppRepository;
        this.g = deviceRepository;
        this.h = notificationsRepository;
        this.i = categoryRepository;
        this.j = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return k;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase] */
    public final rm6 d() {
        return ik6.b(b(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public Object a(xs5 xs5, xe6<? super cd6> xe6) {
        if (xs5 == null) {
            Object a2 = a(new ws5(600, ""));
            if (a2 == ff6.a()) {
                return a2;
            }
            return cd6.a;
        }
        this.d = xs5.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .GetHybridDeviceSettingUseCase executing!!! with serial=");
        String str2 = this.d;
        if (str2 != null) {
            sb.append(str2);
            local.d(str, sb.toString());
            if (!hx5.b(PortfolioApp.get.instance())) {
                Object a3 = a(new ws5(601, ""));
                if (a3 == ff6.a()) {
                    return a3;
                }
                return cd6.a;
            }
            if (!TextUtils.isEmpty(this.d)) {
                DeviceHelper.a aVar = DeviceHelper.o;
                String str3 = this.d;
                if (str3 == null) {
                    wg6.a();
                    throw null;
                } else if (aVar.e(str3)) {
                    Object d2 = d();
                    if (d2 == ff6.a()) {
                        return d2;
                    }
                    return cd6.a;
                }
            }
            Object a4 = a(new ws5(600, ""));
            if (a4 == ff6.a()) {
                return a4;
            }
            return cd6.a;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final rm6 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new b(sparseArray, z, str, (xe6) null), 3, (Object) null);
    }
}
