package com.portfolio.platform.uirenew.pairing;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.jm4;
import com.fossil.kl4;
import com.fossil.ks5;
import com.fossil.lc6;
import com.fossil.lx5;
import com.fossil.ns5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.pairing.PairingDeviceFoundFragment;
import com.portfolio.platform.uirenew.pairing.PairingLookForDeviceFragment;
import com.portfolio.platform.uirenew.pairing.scanning.BasePairingSubFragment;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingFragment extends BasePermissionFragment implements ns5, AlertDialogFragment.g, AlertDialogFragment.h {
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ks5 g;
    @DexIgnore
    public BasePairingSubFragment h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = PairingFragment.class.getSimpleName();
            wg6.a((Object) simpleName, "PairingFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final PairingFragment a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            PairingFragment pairingFragment = new PairingFragment();
            pairingFragment.setArguments(bundle);
            return pairingFragment;
        }
    }

    @DexIgnore
    public void B() {
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment == null || !(basePairingSubFragment instanceof PairingLookForDeviceFragment)) {
            PairingLookForDeviceFragment.a aVar = PairingLookForDeviceFragment.j;
            ks5 ks5 = this.g;
            if (ks5 != null) {
                this.h = aVar.a(ks5.j());
                BasePairingSubFragment basePairingSubFragment2 = this.h;
                if (basePairingSubFragment2 != null) {
                    b(basePairingSubFragment2, "", 2131362119);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        }
        BasePairingSubFragment basePairingSubFragment3 = this.h;
        if (basePairingSubFragment3 != null) {
            ks5 ks52 = this.g;
            if (ks52 != null) {
                basePairingSubFragment3.a(ks52);
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void S() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void T(String str) {
    }

    @DexIgnore
    public boolean U() {
        return this.i;
    }

    @DexIgnore
    public void b(String str, boolean z) {
        wg6.b(str, "serial");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment instanceof PairingUpdateFirmwareFragment) {
            ((PairingUpdateFirmwareFragment) basePairingSubFragment).D0();
            return;
        }
        this.h = PairingUpdateFirmwareFragment.v.a(str, z, 1);
        BasePairingSubFragment basePairingSubFragment2 = this.h;
        if (basePairingSubFragment2 != null) {
            b(basePairingSubFragment2, "", 2131362119);
            BasePairingSubFragment basePairingSubFragment3 = this.h;
            if (basePairingSubFragment3 != null) {
                ks5 ks5 = this.g;
                if (ks5 != null) {
                    basePairingSubFragment3.a(ks5);
                } else {
                    wg6.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void c(int i2, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = o.a();
        local.d(a2, "showDeviceConnectFail() - errorCode = " + i2);
        if (!isActive() || getActivity() == null) {
            return;
        }
        if (str != null) {
            o();
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                TroubleshootingActivity.a.a(aVar, activity, str, false, 4, (Object) null);
                return;
            }
            wg6.a();
            throw null;
        }
        o();
        TroubleshootingActivity.a aVar2 = TroubleshootingActivity.C;
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            wg6.a((Object) activity2, "activity!!");
            aVar2.a(activity2);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g(List<lc6<ShineDevice, String>> list) {
        wg6.b(list, "deviceList");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment != null && (basePairingSubFragment instanceof PairingDeviceFoundFragment)) {
            ((PairingDeviceFoundFragment) basePairingSubFragment).g(list);
        }
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity, 1);
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void k(List<String> list) {
        wg6.b(list, "instructions");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment instanceof PairingAuthorizeFragment) {
            ((PairingAuthorizeFragment) basePairingSubFragment).x(list);
            return;
        }
        this.h = PairingAuthorizeFragment.w.a(list);
        BasePairingSubFragment basePairingSubFragment2 = this.h;
        if (basePairingSubFragment2 != null) {
            b(basePairingSubFragment2, "PairingAuthorizeFragment", 2131362119);
            BasePairingSubFragment basePairingSubFragment3 = this.h;
            if (basePairingSubFragment3 != null) {
                ks5 ks5 = this.g;
                if (ks5 != null) {
                    basePairingSubFragment3.a(ks5);
                } else {
                    wg6.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void l(List<lc6<ShineDevice, String>> list) {
        wg6.b(list, "listDevices");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment == null || !(basePairingSubFragment instanceof PairingDeviceFoundFragment)) {
            PairingDeviceFoundFragment.a aVar = PairingDeviceFoundFragment.q;
            ks5 ks5 = this.g;
            if (ks5 != null) {
                PairingDeviceFoundFragment a2 = aVar.a(ks5.j());
                a2.g(list);
                this.h = a2;
                BasePairingSubFragment basePairingSubFragment2 = this.h;
                if (basePairingSubFragment2 != null) {
                    b(basePairingSubFragment2, "", 2131362119);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        }
        BasePairingSubFragment basePairingSubFragment3 = this.h;
        if (basePairingSubFragment3 != null) {
            ks5 ks52 = this.g;
            if (ks52 != null) {
                basePairingSubFragment3.a(ks52);
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void n(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            String e = PortfolioApp.get.instance().e();
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.f(e, childFragmentManager);
        }
    }

    @DexIgnore
    public void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "activity!!");
            if (!activity.isFinishing()) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    wg6.a((Object) activity2, "activity!!");
                    if (!activity2.isDestroyed()) {
                        FragmentActivity activity3 = getActivity();
                        if (activity3 != null) {
                            activity3.finish();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (intent != null && i2 == 111 && i3 == 10) {
            this.i = intent.getBooleanExtra("NEED_TO_SHOW_PERMISSION_POPUP", false);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558589, viewGroup, false);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        PairingFragment.super.onPause();
        ks5 ks5 = this.g;
        if (ks5 != null) {
            ks5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        PairingFragment.super.onResume();
        ks5 ks5 = this.g;
        if (ks5 != null) {
            ks5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        ks5 ks5 = this.g;
        if (ks5 != null) {
            if (ks5.i() != null) {
                ks5 ks52 = this.g;
                if (ks52 != null) {
                    ShineDevice i2 = ks52.i();
                    if (i2 != null) {
                        bundle.putParcelable("PAIRING_DEVICE", i2);
                    }
                } else {
                    wg6.d("mPairingPresenter");
                    throw null;
                }
            }
            PairingFragment.super.onSaveInstanceState(bundle);
            return;
        }
        wg6.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle != null && bundle.containsKey("PAIRING_DEVICE")) {
            ks5 ks5 = this.g;
            if (ks5 != null) {
                Parcelable parcelable = bundle.getParcelable("PAIRING_DEVICE");
                if (parcelable != null) {
                    ks5.b((ShineDevice) parcelable);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            ks5 ks52 = this.g;
            if (ks52 != null) {
                ks52.b(arguments.getBoolean("IS_ONBOARDING_FLOW"));
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        }
        this.h = getChildFragmentManager().b(2131362119);
        if (this.h == null) {
            PairingLookForDeviceFragment.a aVar = PairingLookForDeviceFragment.j;
            ks5 ks53 = this.g;
            if (ks53 != null) {
                this.h = aVar.a(ks53.j());
                BasePairingSubFragment basePairingSubFragment = this.h;
                if (basePairingSubFragment != null) {
                    b(basePairingSubFragment, "", 2131362119);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        }
        BasePairingSubFragment basePairingSubFragment2 = this.h;
        if (basePairingSubFragment2 != null) {
            ks5 ks54 = this.g;
            if (ks54 != null) {
                basePairingSubFragment2.a(ks54);
                ks5 ks55 = this.g;
                if (ks55 != null) {
                    BasePairingSubFragment basePairingSubFragment3 = this.h;
                    ks55.a((basePairingSubFragment3 instanceof PairingLookForDeviceFragment) || (basePairingSubFragment3 instanceof PairingDeviceFoundFragment));
                    W("scan_device_view");
                    return;
                }
                wg6.d("mPairingPresenter");
                throw null;
            }
            wg6.d("mPairingPresenter");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void q(boolean z) {
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment instanceof PairingAuthorizeFragment) {
            ((PairingAuthorizeFragment) basePairingSubFragment).R(z);
        }
    }

    /* JADX WARNING: type inference failed for: r6v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0062, code lost:
        if (r6 != null) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0064, code lost:
        r6.startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("market://details?id=com.google.android.wearable.app")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0073, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(o.a(), "GG play not found, open in browser");
        r6 = getActivity();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0089, code lost:
        if (r6 != null) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008b, code lost:
        r6.startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.wearable.app")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(o.a(), "WearOS app not found, open in GG play");
        r6 = getActivity();
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x004d */
    public void s(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = o.a();
        local.d(a2, "openWearOSApp(), isChineseUser=" + z);
        if (z) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.k(childFragmentManager);
            return;
        }
        Intent launchIntentForPackage = PortfolioApp.get.instance().getPackageManager().getLaunchIntentForPackage("com.google.android.wearable.app");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.startActivity(launchIntentForPackage);
        }
    }

    @DexIgnore
    public void t() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.C;
            wg6.a((Object) activity, "it");
            ks5 ks5 = this.g;
            if (ks5 != null) {
                aVar.a(activity, ks5.j());
            } else {
                wg6.d("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void v(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            String e = PortfolioApp.get.instance().e();
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.e(e, childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void y() {
        if (isActive()) {
            AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
            fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886562));
            fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886561));
            fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886564));
            fVar.b(2131363190);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public void a(ks5 ks5) {
        wg6.b(ks5, "presenter");
        this.g = ks5;
    }

    @DexIgnore
    public void a(String str, boolean z) {
        wg6.b(str, "serial");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment instanceof PairingUpdateFirmwareFragment) {
            ((PairingUpdateFirmwareFragment) basePairingSubFragment).v0();
            return;
        }
        this.h = PairingUpdateFirmwareFragment.v.a(str, z, 0);
        BasePairingSubFragment basePairingSubFragment2 = this.h;
        if (basePairingSubFragment2 != null) {
            b(basePairingSubFragment2, "", 2131362119);
            BasePairingSubFragment basePairingSubFragment3 = this.h;
            if (basePairingSubFragment3 != null) {
                ks5 ks5 = this.g;
                if (ks5 != null) {
                    basePairingSubFragment3.a(ks5);
                } else {
                    wg6.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void o(String str) {
        wg6.b(str, "serial");
        TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
        Context context = getContext();
        if (context != null) {
            wg6.a((Object) context, "context!!");
            aVar.a(context, str, true);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void l(String str) {
        wg6.b(str, "serial");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment instanceof PairingAuthorizeFragment) {
            ((PairingAuthorizeFragment) basePairingSubFragment).Z(str);
            return;
        }
        this.h = PairingAuthorizeFragment.w.a(str);
        BasePairingSubFragment basePairingSubFragment2 = this.h;
        if (basePairingSubFragment2 != null) {
            b(basePairingSubFragment2, "PairingAuthorizeFragment", 2131362119);
            BasePairingSubFragment basePairingSubFragment3 = this.h;
            if (basePairingSubFragment3 != null) {
                ks5 ks5 = this.g;
                if (ks5 != null) {
                    basePairingSubFragment3.a(ks5);
                } else {
                    wg6.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, boolean z, boolean z2) {
        wg6.b(str, "serial");
        BasePairingSubFragment basePairingSubFragment = this.h;
        if (basePairingSubFragment instanceof PairingUpdateFirmwareFragment) {
            ((PairingUpdateFirmwareFragment) basePairingSubFragment).b(z2);
            return;
        }
        this.h = PairingUpdateFirmwareFragment.v.a(str, z, z2 ? 2 : 3);
        BasePairingSubFragment basePairingSubFragment2 = this.h;
        if (basePairingSubFragment2 != null) {
            b(basePairingSubFragment2, "", 2131362119);
            BasePairingSubFragment basePairingSubFragment3 = this.h;
            if (basePairingSubFragment3 != null) {
                ks5 ks5 = this.g;
                if (ks5 != null) {
                    basePairingSubFragment3.a(ks5);
                } else {
                    wg6.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001c, code lost:
        if (r6.equals("SERVER_ERROR") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00f2, code lost:
        if (r6.equals("SERVER_MAINTENANCE") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00f4, code lost:
        o();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f8, code lost:
        r0 = getActivity();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00fc, code lost:
        if (r0 == null) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00fe, code lost:
        r0.a(r6, r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0109, code lost:
        throw new com.fossil.rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        switch (str.hashCode()) {
            case -1636680713:
                break;
            case -879828873:
                if (str.equals("NETWORK_ERROR")) {
                    if (i2 == 2131363190) {
                        o();
                        return;
                    }
                    return;
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    if (i2 == 2131363190) {
                        ks5 ks5 = this.g;
                        if (ks5 != null) {
                            ks5.m();
                            return;
                        } else {
                            wg6.d("mPairingPresenter");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == 2131363105) {
                        ks5 ks52 = this.g;
                        if (ks52 != null) {
                            ks52.b(stringExtra);
                            return;
                        } else {
                            wg6.d("mPairingPresenter");
                            throw null;
                        }
                    } else if (i2 == 2131363190) {
                        ks5 ks53 = this.g;
                        if (ks53 != null) {
                            ks53.a(stringExtra);
                            return;
                        } else {
                            wg6.d("mPairingPresenter");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 927511079:
                if (str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                    if (i2 == 2131362228) {
                        ks5 ks54 = this.g;
                        if (ks54 != null) {
                            ks54.n();
                            return;
                        } else {
                            wg6.d("mPairingPresenter");
                            throw null;
                        }
                    } else if (i2 != 2131362311) {
                        if (i2 == 2131362561) {
                            o();
                            return;
                        }
                        return;
                    } else if (getActivity() != null) {
                        HelpActivity.a aVar = HelpActivity.C;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            wg6.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        wg6.a();
                        throw null;
                    } else {
                        return;
                    }
                }
                break;
            case 1008390942:
                if (str.equals("NO_INTERNET_CONNECTION")) {
                    if (i2 == 2131362354) {
                        BaseActivity activity2 = getActivity();
                        if (activity2 != null) {
                            activity2.m();
                            o();
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                    } else if (i2 == 2131363190) {
                        o();
                        return;
                    } else {
                        return;
                    }
                }
                break;
            case 1178575340:
                break;
        }
    }

    @DexIgnore
    public void a(int i2, String str, String str2) {
        wg6.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = o.a();
        local.d(a2, "showDeviceConnectFail due to network - errorCode = " + i2);
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, i2, str2);
        }
    }
}
