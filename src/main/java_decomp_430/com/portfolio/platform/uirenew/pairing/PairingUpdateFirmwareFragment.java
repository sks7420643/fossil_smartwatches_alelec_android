package com.portfolio.platform.uirenew.pairing;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.k34;
import com.fossil.kb;
import com.fossil.ks5;
import com.fossil.ms5;
import com.fossil.oj3;
import com.fossil.ox5;
import com.fossil.pe4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vs5;
import com.fossil.wg6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.pairing.scanning.BasePairingSubFragment;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingUpdateFirmwareFragment extends BasePairingSubFragment implements ms5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String u; // = u;
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public ks5 g;
    @DexIgnore
    public vs5 h;
    @DexIgnore
    public ax5<pe4> i;
    @DexIgnore
    public k34 j;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public int p;
    @DexIgnore
    public /* final */ String q; // = ThemeManager.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String r; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String s; // = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final PairingUpdateFirmwareFragment a(String str, boolean z, int i) {
            wg6.b(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putString("SERIAL", str);
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putInt(PairingUpdateFirmwareFragment.u, i);
            PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment = new PairingUpdateFirmwareFragment();
            pairingUpdateFirmwareFragment.setArguments(bundle);
            return pairingUpdateFirmwareFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingUpdateFirmwareFragment a;

        @DexIgnore
        public b(PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment, String str) {
            this.a = pairingUpdateFirmwareFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ pe4 a;
        @DexIgnore
        public /* final */ /* synthetic */ PairingUpdateFirmwareFragment b;

        @DexIgnore
        public c(pe4 pe4, PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment, String str) {
            this.a = pe4;
            this.b = pairingUpdateFirmwareFragment;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            PairingUpdateFirmwareFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.r)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "set icon color " + this.b.r);
                int parseColor = Color.parseColor(this.b.r);
                TabLayout.g b4 = this.a.y.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.q) && this.b.p != i) {
                int parseColor2 = Color.parseColor(this.b.q);
                TabLayout.g b5 = this.a.y.b(this.b.p);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.p = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingUpdateFirmwareFragment a;

        @DexIgnore
        public d(PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment, String str) {
            this.a = pairingUpdateFirmwareFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingUpdateFirmwareFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public e(PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment, String str) {
            this.a = pairingUpdateFirmwareFragment;
            this.b = str;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                Context context = this.a.getContext();
                if (context != null) {
                    wg6.a((Object) context, "context!!");
                    TroubleshootingActivity.a.a(aVar, context, this.b, false, 4, (Object) null);
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ PairingUpdateFirmwareFragment a;

        @DexIgnore
        public f(PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment) {
            this.a = pairingUpdateFirmwareFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.q) && !TextUtils.isEmpty(this.a.r)) {
                int parseColor = Color.parseColor(this.a.q);
                int parseColor2 = Color.parseColor(this.a.r);
                gVar.b(2131231004);
                if (i == this.a.p) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void D0() {
        if (isActive()) {
            ax5<pe4> ax5 = this.i;
            if (ax5 != null) {
                pe4 a2 = ax5.a();
                if (a2 != null) {
                    vs5 vs5 = this.h;
                    if (vs5 != null) {
                        vs5.f();
                        ConstraintLayout constraintLayout = a2.q;
                        wg6.a((Object) constraintLayout, "it.clUpdateFwFail");
                        constraintLayout.setVisibility(8);
                        ConstraintLayout constraintLayout2 = a2.r;
                        wg6.a((Object) constraintLayout2, "it.clUpdatingFw");
                        constraintLayout2.setVisibility(0);
                        Object r1 = a2.s;
                        wg6.a((Object) r1, "it.fbContinue");
                        r1.setVisibility(0);
                        Object r12 = a2.x;
                        wg6.a((Object) r12, "it.ftvUpdateWarning");
                        r12.setVisibility(4);
                        ProgressBar progressBar = a2.A;
                        wg6.a((Object) progressBar, "it.progressUpdate");
                        progressBar.setVisibility(8);
                        Object r13 = a2.u;
                        wg6.a((Object) r13, "it.ftvCountdownTime");
                        r13.setVisibility(8);
                        Object r0 = a2.w;
                        wg6.a((Object) r0, "it.ftvUpdate");
                        r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886550));
                        return;
                    }
                    wg6.d("mSubPresenter");
                    throw null;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void g() {
        DashBar dashBar;
        ax5<pe4> ax5 = this.i;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.z) != null) {
                dashBar.setVisibility(0);
                ox5.a aVar = ox5.a;
                wg6.a((Object) dashBar, "this");
                aVar.f(dashBar, this.o, 500);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h(int i2) {
        ProgressBar progressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        ax5<pe4> ax5 = this.i;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null && (progressBar = a2.A) != null) {
                progressBar.setProgress(i2);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final ks5 j1() {
        ks5 ks5 = this.g;
        if (ks5 != null) {
            return ks5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void k1() {
        DashBar dashBar;
        ax5<pe4> ax5 = this.i;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.z) != null) {
                dashBar.setVisibility(0);
                ox5.a aVar = ox5.a;
                wg6.a((Object) dashBar, "this");
                aVar.c(dashBar, this.o, 500);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void l1() {
        ax5<pe4> ax5 = this.i;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            TabLayout tabLayout = a2 != null ? a2.y : null;
            if (tabLayout != null) {
                ax5<pe4> ax52 = this.i;
                if (ax52 != null) {
                    pe4 a3 = ax52.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.C : null;
                    if (viewPager2 != null) {
                        new oj3(tabLayout, viewPager2, new f(this)).a();
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        PairingUpdateFirmwareFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        pe4 a2 = kb.a(layoutInflater, 2131558620, viewGroup, false, e1());
        this.i = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        PairingUpdateFirmwareFragment.super.onPause();
        vs5 vs5 = this.h;
        if (vs5 != null) {
            vs5.e();
        } else {
            wg6.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        PairingUpdateFirmwareFragment.super.onResume();
        vs5 vs5 = this.h;
        if (vs5 != null) {
            vs5.d();
        } else {
            wg6.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        String str;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.o = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        Bundle arguments2 = getArguments();
        if (arguments2 == null || (str = arguments2.getString("SERIAL", "")) == null) {
            str = "";
        }
        Bundle arguments3 = getArguments();
        int i2 = arguments3 != null ? arguments3.getInt(u) : 0;
        this.h = new vs5(str, this);
        this.j = new k34(new ArrayList());
        ax5<pe4> ax5 = this.i;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                wg6.a((Object) constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                wg6.a((Object) constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                ProgressBar progressBar = a2.A;
                wg6.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                Object r4 = a2.s;
                wg6.a((Object) r4, "binding.fbContinue");
                r4.setVisibility(8);
                Object r42 = a2.x;
                wg6.a((Object) r42, "binding.ftvUpdateWarning");
                r42.setVisibility(0);
                a2.s.setOnClickListener(new b(this, str));
                ViewPager2 viewPager2 = a2.C;
                wg6.a((Object) viewPager2, "binding.rvpTutorial");
                k34 k34 = this.j;
                if (k34 != null) {
                    viewPager2.setAdapter(k34);
                    if (a2.C.getChildAt(0) != null) {
                        RecyclerView childAt = a2.C.getChildAt(0);
                        if (childAt != null) {
                            childAt.setOverScrollMode(2);
                        } else {
                            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    if (!TextUtils.isEmpty(this.s)) {
                        TabLayout tabLayout = a2.y;
                        wg6.a((Object) tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.s)));
                    }
                    l1();
                    a2.C.a(new c(a2, this, str));
                    a2.t.setOnClickListener(new d(this, str));
                    a2.v.setOnClickListener(new e(this, str));
                } else {
                    wg6.d("mAdapterUpdateFirmware");
                    throw null;
                }
            }
            if (i2 == 1) {
                D0();
            } else if (i2 == 2) {
                b(true);
            } else if (i2 == 3) {
                b(false);
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void v0() {
        if (isActive()) {
            ax5<pe4> ax5 = this.i;
            if (ax5 != null) {
                pe4 a2 = ax5.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    wg6.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wg6.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v7, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r6v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(boolean z) {
        if (isActive()) {
            ax5<pe4> ax5 = this.i;
            if (ax5 != null) {
                pe4 a2 = ax5.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    wg6.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wg6.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    Object r6 = a2.s;
                    wg6.a((Object) r6, "it.fbContinue");
                    r6.setVisibility(0);
                    Object r62 = a2.x;
                    wg6.a((Object) r62, "it.ftvUpdateWarning");
                    r62.setVisibility(4);
                    ProgressBar progressBar = a2.A;
                    wg6.a((Object) progressBar, "it.progressUpdate");
                    progressBar.setProgress(1000);
                    Object r63 = a2.w;
                    wg6.a((Object) r63, "it.ftvUpdate");
                    r63.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886802));
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.q;
                wg6.a((Object) constraintLayout3, "it.clUpdateFwFail");
                constraintLayout3.setVisibility(0);
                ConstraintLayout constraintLayout4 = a2.r;
                wg6.a((Object) constraintLayout4, "it.clUpdatingFw");
                constraintLayout4.setVisibility(8);
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void h(List<? extends Explore> list) {
        wg6.b(list, "data");
        ax5<pe4> ax5 = this.i;
        if (ax5 != null) {
            pe4 a2 = ax5.a();
            if (a2 != null) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.get.instance().e())) {
                    Object r0 = a2.w;
                    wg6.a((Object) r0, "it.ftvUpdate");
                    r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886792));
                } else {
                    Object r02 = a2.w;
                    wg6.a((Object) r02, "it.ftvUpdate");
                    r02.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886792));
                }
            }
            k34 k34 = this.j;
            if (k34 != null) {
                k34.a(list);
            } else {
                wg6.d("mAdapterUpdateFirmware");
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ks5 ks5) {
        wg6.b(ks5, "presenter");
        this.g = ks5;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if (str.hashCode() == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.o);
            if (i2 == 2131362228) {
                ks5 ks5 = this.g;
                if (ks5 != null) {
                    ks5.n();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131362311) {
                if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.C;
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        wg6.a((Object) activity, "activity!!");
                        aVar.a(activity);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }
    }
}
