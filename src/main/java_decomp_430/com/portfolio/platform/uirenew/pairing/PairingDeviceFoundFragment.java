package com.portfolio.platform.uirenew.pairing;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.fr;
import com.fossil.fu4;
import com.fossil.jh6;
import com.fossil.jm4;
import com.fossil.js5;
import com.fossil.kb;
import com.fossil.ks5;
import com.fossil.lc6;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.ox5;
import com.fossil.pu4;
import com.fossil.qg6;
import com.fossil.tc4;
import com.fossil.vz5;
import com.fossil.wg6;
import com.fossil.wq;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.scanning.BasePairingSubFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingDeviceFoundFragment extends BasePairingSubFragment implements js5, pu4.b, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<tc4> g;
    @DexIgnore
    public ks5 h;
    @DexIgnore
    public pu4 i;
    @DexIgnore
    public fu4 j;
    @DexIgnore
    public List<lc6<ShineDevice, String>> o; // = new ArrayList();
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = PairingDeviceFoundFragment.class.getSimpleName();
            wg6.a((Object) simpleName, "PairingDeviceFoundFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final PairingDeviceFoundFragment a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            PairingDeviceFoundFragment pairingDeviceFoundFragment = new PairingDeviceFoundFragment();
            pairingDeviceFoundFragment.setArguments(bundle);
            return pairingDeviceFoundFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ tc4 a;
        @DexIgnore
        public /* final */ /* synthetic */ PairingDeviceFoundFragment b;

        @DexIgnore
        public b(tc4 tc4, PairingDeviceFoundFragment pairingDeviceFoundFragment, jh6 jh6) {
            this.a = tc4;
            this.b = pairingDeviceFoundFragment;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b.getActivity(), 2130772010);
            this.a.s.startAnimation(loadAnimation);
            this.a.w.startAnimation(loadAnimation);
            this.a.q.startAnimation(loadAnimation);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingDeviceFoundFragment a;

        @DexIgnore
        public c(PairingDeviceFoundFragment pairingDeviceFoundFragment) {
            this.a = pairingDeviceFoundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingDeviceFoundFragment.b(this.a).a(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingDeviceFoundFragment a;

        @DexIgnore
        public d(PairingDeviceFoundFragment pairingDeviceFoundFragment) {
            this.a = pairingDeviceFoundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingDeviceFoundFragment.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingDeviceFoundFragment a;

        @DexIgnore
        public e(PairingDeviceFoundFragment pairingDeviceFoundFragment) {
            this.a = pairingDeviceFoundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (PairingDeviceFoundFragment.c(this.a).d() < this.a.o.size()) {
                PairingDeviceFoundFragment pairingDeviceFoundFragment = this.a;
                pairingDeviceFoundFragment.a((ShineDevice) ((lc6) pairingDeviceFoundFragment.o.get(PairingDeviceFoundFragment.c(this.a).d())).getFirst());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ks5 b(PairingDeviceFoundFragment pairingDeviceFoundFragment) {
        ks5 ks5 = pairingDeviceFoundFragment.h;
        if (ks5 != null) {
            return ks5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ fu4 c(PairingDeviceFoundFragment pairingDeviceFoundFragment) {
        fu4 fu4 = pairingDeviceFoundFragment.j;
        if (fu4 != null) {
            return fu4;
        }
        wg6.d("mSnapHelper");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void g(List<lc6<ShineDevice, String>> list) {
        Object r5;
        Object r52;
        wg6.b(list, "shineDeviceList");
        this.o = list;
        pu4 pu4 = this.i;
        if (pu4 != null) {
            pu4.a(list);
            int size = this.o.size();
            if (size <= 1) {
                ax5<tc4> ax5 = this.g;
                if (ax5 != null) {
                    tc4 a2 = ax5.a();
                    if (a2 != null && (r52 = a2.s) != 0) {
                        nh6 nh6 = nh6.a;
                        Locale locale = Locale.US;
                        wg6.a((Object) locale, "Locale.US");
                        String a3 = jm4.a((Context) getActivity(), 2131886700);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        Object[] objArr = {Integer.valueOf(size)};
                        String format = String.format(locale, a3, Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        r52.setText(format);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            ax5<tc4> ax52 = this.g;
            if (ax52 != null) {
                tc4 a4 = ax52.a();
                if (a4 != null && (r5 = a4.s) != 0) {
                    nh6 nh62 = nh6.a;
                    Locale locale2 = Locale.US;
                    wg6.a((Object) locale2, "Locale.US");
                    String a5 = jm4.a((Context) getActivity(), 2131886700);
                    wg6.a((Object) a5, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    Object[] objArr2 = {Integer.valueOf(size)};
                    String format2 = String.format(locale2, a5, Arrays.copyOf(objArr2, objArr2.length));
                    wg6.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                    r5.setText(format2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        jh6 jh6 = new jh6();
        jh6.element = null;
        if (z) {
            ax5<tc4> ax5 = this.g;
            if (ax5 != null) {
                tc4 a2 = ax5.a();
                if (!(a2 == null || getActivity() == null)) {
                    jh6.element = AnimationUtils.loadAnimation(getActivity(), 2130771999);
                    Animation animation = (Animation) jh6.element;
                    if (animation != null) {
                        animation.setAnimationListener(new b(a2, this, jh6));
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
        return (Animation) jh6.element;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        tc4 a2 = kb.a(layoutInflater, 2131558591, viewGroup, false, e1());
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v20, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r7v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r7v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v25, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        Object r7;
        Object r72;
        RecyclerView recyclerView;
        Object r73;
        Object r74;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<tc4> ax5 = this.g;
        if (ax5 != null) {
            tc4 a2 = ax5.a();
            if (!(a2 == null || (r74 = a2.t) == 0)) {
                r74.setOnClickListener(new c(this));
            }
            ax5<tc4> ax52 = this.g;
            if (ax52 != null) {
                tc4 a3 = ax52.a();
                if (!(a3 == null || (r73 = a3.s) == 0)) {
                    nh6 nh6 = nh6.a;
                    Locale locale = Locale.US;
                    wg6.a((Object) locale, "Locale.US");
                    String a4 = jm4.a(getContext(), 2131886700);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    Object[] objArr = {Integer.valueOf(this.o.size())};
                    String format = String.format(locale, a4, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
                    r73.setText(format);
                }
                fr a5 = wq.a(this);
                wg6.a((Object) a5, "Glide.with(this)");
                this.i = new pu4(a5, this);
                pu4 pu4 = this.i;
                if (pu4 != null) {
                    pu4.a(this.o);
                }
                ax5<tc4> ax53 = this.g;
                if (ax53 != null) {
                    tc4 a6 = ax53.a();
                    if (!(a6 == null || (recyclerView = a6.w) == null)) {
                        wg6.a((Object) recyclerView, "it");
                        recyclerView.setLayoutManager(new LinearLayoutManager(PortfolioApp.get.instance().getApplicationContext(), 0, false));
                        recyclerView.addItemDecoration(new vz5());
                        recyclerView.setAdapter(this.i);
                        this.j = new fu4();
                        fu4 fu4 = this.j;
                        if (fu4 != null) {
                            fu4.a(recyclerView);
                        } else {
                            wg6.d("mSnapHelper");
                            throw null;
                        }
                    }
                    ax5<tc4> ax54 = this.g;
                    if (ax54 != null) {
                        tc4 a7 = ax54.a();
                        if (!(a7 == null || (r72 = a7.r) == 0)) {
                            r72.setOnClickListener(new d(this));
                        }
                        ax5<tc4> ax55 = this.g;
                        if (ax55 != null) {
                            tc4 a8 = ax55.a();
                            if (!(a8 == null || (r7 = a8.q) == 0)) {
                                r7.setOnClickListener(new e(this));
                            }
                            Bundle arguments = getArguments();
                            if (arguments != null) {
                                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                                ax5<tc4> ax56 = this.g;
                                if (ax56 != null) {
                                    tc4 a9 = ax56.a();
                                    if (a9 != null && (dashBar = a9.u) != null) {
                                        ox5.a aVar = ox5.a;
                                        wg6.a((Object) dashBar, "this");
                                        aVar.b(dashBar, z, 500);
                                        return;
                                    }
                                    return;
                                }
                                wg6.d("mBinding");
                                throw null;
                            }
                            return;
                        }
                        wg6.d("mBinding");
                        throw null;
                    }
                    wg6.d("mBinding");
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ks5 ks5) {
        wg6.b(ks5, "presenter");
        this.h = ks5;
    }

    @DexIgnore
    public final void a(ShineDevice shineDevice) {
        String serial = shineDevice.getSerial();
        wg6.a((Object) serial, "device.serial");
        if (serial.length() > 0) {
            ks5 ks5 = this.h;
            if (ks5 != null) {
                ks5.c(shineDevice);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.j(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(View view, pu4.c cVar, int i2) {
        RecyclerView recyclerView;
        wg6.b(view, "view");
        wg6.b(cVar, "viewHolder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = q.a();
        StringBuilder sb = new StringBuilder();
        sb.append("onItemClick - position=");
        sb.append(i2);
        sb.append(", mSnappedPos=");
        fu4 fu4 = this.j;
        if (fu4 != null) {
            sb.append(fu4.d());
            local.d(a2, sb.toString());
            fu4 fu42 = this.j;
            if (fu42 == null) {
                wg6.d("mSnapHelper");
                throw null;
            } else if (fu42.d() != i2) {
                ax5<tc4> ax5 = this.g;
                if (ax5 != null) {
                    tc4 a3 = ax5.a();
                    if (a3 != null && (recyclerView = a3.w) != null) {
                        fu4 fu43 = this.j;
                        if (fu43 != null) {
                            recyclerView.smoothScrollBy((i2 - fu43.d()) * view.getWidth(), view.getHeight());
                        } else {
                            wg6.d("mSnapHelper");
                            throw null;
                        }
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                List<lc6<ShineDevice, String>> list = this.o;
                fu4 fu44 = this.j;
                if (fu44 != null) {
                    a((ShineDevice) list.get(fu44.d()).getFirst());
                } else {
                    wg6.d("mSnapHelper");
                    throw null;
                }
            }
        } else {
            wg6.d("mSnapHelper");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if (str.hashCode() == -2084521848 && str.equals("DOWNLOAD") && i2 == 2131363190) {
            ks5 ks5 = this.h;
            if (ks5 != null) {
                List<lc6<ShineDevice, String>> list = this.o;
                fu4 fu4 = this.j;
                if (fu4 != null) {
                    ks5.a((ShineDevice) list.get(fu4.d()).getFirst());
                } else {
                    wg6.d("mSnapHelper");
                    throw null;
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }
}
