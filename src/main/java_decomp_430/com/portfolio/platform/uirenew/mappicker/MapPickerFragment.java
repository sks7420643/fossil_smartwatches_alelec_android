package com.portfolio.platform.uirenew.mappicker;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.az2;
import com.fossil.ix2;
import com.fossil.jm4;
import com.fossil.jx2;
import com.fossil.kb;
import com.fossil.lb4;
import com.fossil.ld;
import com.fossil.lx2;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.sp5;
import com.fossil.tp5;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerFragment extends BasePermissionFragment implements jx2.b, jx2.c {
    @DexIgnore
    public static /* final */ b p; // = new b((qg6) null);
    @DexIgnore
    public ax5<lb4> g;
    @DexIgnore
    public tp5 h;
    @DexIgnore
    public jx2 i;
    @DexIgnore
    public w04 j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements jx2.a {
        @DexIgnore
        public /* final */ /* synthetic */ lb4 a;

        @DexIgnore
        public a(lb4 lb4, LatLng latLng) {
            this.a = lb4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton, java.lang.Object] */
        public void onCancel() {
            Object r0 = this.a.r;
            wg6.a((Object) r0, "binding.fbCurrentLocation");
            r0.setEnabled(true);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton, java.lang.Object] */
        public void onFinish() {
            Object r0 = this.a.r;
            wg6.a((Object) r0, "binding.fbCurrentLocation");
            r0.setEnabled(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final MapPickerFragment a(double d, double d2, String str) {
            wg6.b(str, "address");
            MapPickerFragment mapPickerFragment = new MapPickerFragment();
            Bundle bundle = new Bundle();
            bundle.putDouble("latitude", d);
            bundle.putDouble("longitude", d2);
            bundle.putString("address", str);
            mapPickerFragment.setArguments(bundle);
            return mapPickerFragment;
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements lx2 {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment a;

        @DexIgnore
        public c(MapPickerFragment mapPickerFragment) {
            this.a = mapPickerFragment;
        }

        @DexIgnore
        public final void a(jx2 jx2) {
            this.a.i = jx2;
            jx2 b = this.a.i;
            if (b != null) {
                b.a(this.a);
            }
            jx2 b2 = this.a.i;
            if (b2 != null) {
                b2.a(this.a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment a;

        @DexIgnore
        public d(MapPickerFragment mapPickerFragment) {
            this.a = mapPickerFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment a;

        @DexIgnore
        public e(MapPickerFragment mapPickerFragment) {
            this.a = mapPickerFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            MapPickerFragment.c(this.a).c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lb4 a;
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment b;

        @DexIgnore
        public f(lb4 lb4, MapPickerFragment mapPickerFragment) {
            this.a = lb4;
            this.b = mapPickerFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Intent intent = new Intent();
            FlexibleTextView flexibleTextView = this.a.u;
            wg6.a((Object) flexibleTextView, "binding.tvTitle");
            String obj = flexibleTextView.getText().toString();
            Bundle bundle = new Bundle();
            Location d = MapPickerFragment.c(this.b).d();
            if (d != null) {
                bundle.putParcelable("location", d);
                bundle.putString("address", obj);
            }
            intent.putExtra(Constants.RESULT, bundle);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.setResult(100, intent);
            }
            FragmentActivity activity2 = this.b.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<tp5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment a;

        @DexIgnore
        public g(MapPickerFragment mapPickerFragment) {
            this.a = mapPickerFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(tp5.b bVar) {
            if (bVar != null) {
                Integer c = bVar.c();
                if (c != null) {
                    this.a.a(c.intValue(), "");
                }
                if (!(bVar.d() == null || bVar.e() == null)) {
                    this.a.a(bVar.d(), bVar.e());
                }
                if (bVar.a() != null) {
                    MapPickerFragment mapPickerFragment = this.a;
                    String a2 = bVar.a();
                    if (a2 != null) {
                        mapPickerFragment.B(a2);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                Boolean b = bVar.b();
                if (b != null) {
                    b.booleanValue();
                    this.a.j1();
                }
                Boolean g = bVar.g();
                if (g != null) {
                    if (g.booleanValue()) {
                        this.a.k();
                    } else {
                        this.a.i();
                    }
                }
                Boolean f = bVar.f();
                if (f != null) {
                    f.booleanValue();
                    Boolean f2 = bVar.f();
                    if (f2 == null) {
                        wg6.a();
                        throw null;
                    } else if (!f2.booleanValue()) {
                        this.a.i0();
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ tp5 c(MapPickerFragment mapPickerFragment) {
        tp5 tp5 = mapPickerFragment.h;
        if (tp5 != null) {
            return tp5;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void B(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showAddress: address = " + str);
        ax5<lb4> ax5 = this.g;
        if (ax5 != null) {
            lb4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.u;
                wg6.a((Object) r0, "it.tvTitle");
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "hide Dialog Loading");
        a();
    }

    @DexIgnore
    public final void i0() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showLocationPermissionError");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public final void j1() {
        ax5<lb4> ax5 = this.g;
        if (ax5 != null) {
            lb4 a2 = ax5.a();
            if (a2 != null) {
                a2.q.a("flexible_button_primary");
                Object r1 = a2.q;
                wg6.a((Object) r1, "it.btConfirm");
                r1.setEnabled(true);
                Object r12 = a2.q;
                wg6.a((Object) r12, "it.btConfirm");
                r12.setClickable(true);
                Object r0 = a2.q;
                wg6.a((Object) r0, "it.btConfirm");
                r0.setFocusable(true);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "show Dialog Loading");
        b();
    }

    @DexIgnore
    public final void k1() {
        tp5 tp5 = this.h;
        if (tp5 != null) {
            tp5.b().a(this, new g(this));
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void l() {
        ImageView imageView;
        ax5<lb4> ax5 = this.g;
        if (ax5 != null) {
            lb4 a2 = ax5.a();
            if (!(a2 == null || (imageView = a2.s) == null)) {
                imageView.setImageResource(2131231121);
            }
            az2 az2 = new az2();
            jx2 jx2 = this.i;
            CameraPosition b2 = jx2 != null ? jx2.b() : null;
            if (b2 != null) {
                az2.a(b2.a);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("onCameraIdle: lat= ");
                wg6.a((Object) az2, "markerOptions");
                sb.append(az2.F().a);
                sb.append(", long= ");
                sb.append(az2.F().b);
                local.d("MapPickerFragment", sb.toString());
                if (az2.F().a != 0.0d && az2.F().b != 0.0d) {
                    tp5 tp5 = this.h;
                    if (tp5 != null) {
                        tp5.a(az2.F().a, az2.F().b);
                    } else {
                        wg6.d("mViewModel");
                        throw null;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FragmentManager supportFragmentManager;
        wg6.b(layoutInflater, "inflater");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCreateView");
        lb4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558573, viewGroup, false);
        PortfolioApp.get.instance().g().a(new sp5()).a(this);
        w04 w04 = this.j;
        Fragment fragment = null;
        if (w04 != null) {
            tp5 a3 = vd.a(this, w04).a(tp5.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026kerViewModel::class.java)");
            this.h = a3;
            a2.q.a("flexible_button_disabled");
            FragmentActivity activity = getActivity();
            if (!(activity == null || (supportFragmentManager = activity.getSupportFragmentManager()) == null)) {
                fragment = supportFragmentManager.b(2131362202);
            }
            SupportMapFragment supportMapFragment = (SupportMapFragment) fragment;
            if (supportMapFragment != null) {
                supportMapFragment.a(new c(this));
            }
            k1();
            this.g = new ax5<>(this, a2);
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("appViewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        MapPickerFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onResume");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton] */
    /* JADX WARNING: type inference failed for: r4v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        double d2;
        double d3;
        double d4;
        double d5;
        wg6.b(view, "view");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onViewCreated");
        super.onViewCreated(view, bundle);
        ax5<lb4> ax5 = this.g;
        if (ax5 != null) {
            lb4 a2 = ax5.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                a2.q.setOnClickListener(new f(a2, this));
                String str = "";
                if (getArguments() != null) {
                    Bundle arguments = getArguments();
                    if (arguments != null) {
                        if (arguments.containsKey("latitude")) {
                            Bundle arguments2 = getArguments();
                            if (arguments2 != null) {
                                d4 = arguments2.getDouble("latitude");
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            d4 = 0.0d;
                        }
                        Bundle arguments3 = getArguments();
                        if (arguments3 != null) {
                            if (arguments3.containsKey("longitude")) {
                                Bundle arguments4 = getArguments();
                                if (arguments4 != null) {
                                    d5 = arguments4.getDouble("longitude");
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                d5 = 0.0d;
                            }
                            Bundle arguments5 = getArguments();
                            if (arguments5 != null) {
                                if (arguments5.containsKey("address")) {
                                    Bundle arguments6 = getArguments();
                                    if (arguments6 != null) {
                                        String string = arguments6.getString("address");
                                        if (string != null) {
                                            str = string;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                                d3 = d4;
                                d2 = d5;
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    d3 = 0.0d;
                    d2 = 0.0d;
                }
                if ((d3 == 0.0d || d2 == 0.0d) && TextUtils.isEmpty(str)) {
                    tp5 tp5 = this.h;
                    if (tp5 == null) {
                        wg6.d("mViewModel");
                        throw null;
                    } else if (tp5.a()) {
                        tp5 tp52 = this.h;
                        if (tp52 != null) {
                            tp52.c();
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("MapPickerFragment", "initStartLocation lat " + d3 + " long " + d2 + " address " + str);
                    tp5 tp53 = this.h;
                    if (tp53 != null) {
                        tp53.a(d3, d2, str);
                    } else {
                        wg6.d("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showErrorDialog");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v1, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton, java.lang.Object] */
    public final void a(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        ax5<lb4> ax5 = this.g;
        if (ax5 != null) {
            lb4 a2 = ax5.a();
            if (a2 != null && d2 != null && d3 != null) {
                ImageView imageView = a2.s;
                wg6.a((Object) imageView, "binding.imgLocationPinUp");
                imageView.setVisibility(0);
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                jx2 jx2 = this.i;
                if (jx2 != null) {
                    Object r7 = a2.r;
                    wg6.a((Object) r7, "binding.fbCurrentLocation");
                    r7.setEnabled(false);
                    jx2.a(ix2.a(latLng, 16.0f), new a(a2, latLng));
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void a(int i2) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCameraMoveStarted");
        ax5<lb4> ax5 = this.g;
        if (ax5 != null) {
            lb4 a2 = ax5.a();
            if (a2 != null) {
                a2.s.setImageResource(2131231120);
                Object r0 = a2.u;
                wg6.a((Object) r0, "it.tvTitle");
                nh6 nh6 = nh6.a;
                String a3 = jm4.a(getContext(), 2131886383);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026on_DropPin_Text__Loading)");
                Object[] objArr = new Object[0];
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r0.setText(format);
                a2.q.a("flexible_button_disabled");
                Object r02 = a2.q;
                wg6.a((Object) r02, "it.btConfirm");
                r02.setEnabled(false);
                Object r03 = a2.q;
                wg6.a((Object) r03, "it.btConfirm");
                r03.setClickable(false);
                Object r6 = a2.q;
                wg6.a((Object) r6, "it.btConfirm");
                r6.setFocusable(false);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
