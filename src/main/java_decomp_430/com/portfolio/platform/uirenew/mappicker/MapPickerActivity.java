package com.portfolio.platform.uirenew.mappicker;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Fragment fragment, double d, double d2, String str, int i, Object obj) {
            double d3 = 0.0d;
            double d4 = (i & 2) != 0 ? 0.0d : d;
            if ((i & 4) == 0) {
                d3 = d2;
            }
            aVar.a(fragment, d4, d3, (i & 8) != 0 ? "" : str);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, double d, double d2, String str) {
            wg6.b(fragment, "context");
            wg6.b(str, "address");
            Intent intent = new Intent(fragment.getContext(), MapPickerActivity.class);
            intent.putExtra("latitude", d);
            intent.putExtra("longitude", d2);
            intent.putExtra("address", str);
            fragment.startActivityForResult(intent, 100);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v0, types: [com.portfolio.platform.uirenew.mappicker.MapPickerActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        String str;
        double d;
        double d2;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (getSupportFragmentManager().b(2131362119) == null) {
            Intent intent = getIntent();
            String str2 = "";
            double d3 = 0.0d;
            if (intent != null) {
                double doubleExtra = intent.hasExtra("latitude") ? intent.getDoubleExtra("latitude", 0.0d) : 0.0d;
                if (intent.hasExtra("longitude")) {
                    d3 = intent.getDoubleExtra("longitude", 0.0d);
                }
                if (intent.hasExtra("address")) {
                    String stringExtra = intent.getStringExtra("address");
                    if (stringExtra == null) {
                        stringExtra = str2;
                    }
                    str2 = stringExtra;
                }
                str = str2;
                d = d3;
                d2 = doubleExtra;
            } else {
                str = str2;
                d2 = 0.0d;
                d = 0.0d;
            }
            a((Fragment) MapPickerFragment.p.a(d2, d, str), "MapPickerFragment", 2131362119);
        }
    }
}
