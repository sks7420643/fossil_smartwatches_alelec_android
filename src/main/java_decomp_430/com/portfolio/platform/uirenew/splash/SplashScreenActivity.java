package com.portfolio.platform.uirenew.splash;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ju5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.welcome.SplashScreenFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SplashScreenActivity extends BaseActivity {
    @DexIgnore
    public SplashPresenter B;

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        SplashScreenFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = SplashScreenFragment.h.a();
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new ju5(b)).a(this);
    }
}
