package com.portfolio.platform.uirenew.splash;

import android.os.Handler;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hu5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.iu5;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.lu5$b$a;
import com.fossil.lu5$b$b;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SplashPresenter extends hu5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((qg6) null);
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ Runnable f; // = new c(this);
    @DexIgnore
    public /* final */ iu5 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ MigrationHelper i;
    @DexIgnore
    public /* final */ ThemeRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SplashPresenter.k;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1", f = "SplashPresenter.kt", l = {62, 71}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SplashPresenter splashPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = splashPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x00b9  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00c3  */
        public final Object invokeSuspend(Object obj) {
            MFUser mFUser;
            il6 il6;
            String str;
            boolean z;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                str = PortfolioApp.get.instance().h();
                z = this.this$0.i.a(str);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SplashPresenter.l.a();
                local.d(a2, "checkToGoToNextStep isMigrationComplete " + z);
                dl6 c = this.this$0.c();
                lu5$b$a lu5_b_a = new lu5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = str;
                this.Z$0 = z;
                this.label = 1;
                if (gk6.a(c, lu5_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                z = this.Z$0;
                str = (String) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                String str2 = (String) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                mFUser = (MFUser) obj;
                if (mFUser != null) {
                    this.this$0.g.x0();
                } else {
                    String email = mFUser.getEmail();
                    boolean z2 = false;
                    if (!(email == null || xj6.a(email))) {
                        String birthday = mFUser.getBirthday();
                        if (birthday == null || xj6.a(birthday)) {
                            z2 = true;
                        }
                        if (!z2) {
                            String firstName = mFUser.getFirstName();
                            wg6.a((Object) firstName, "mCurrentUser.firstName");
                            if (!xj6.a(firstName)) {
                                String lastName = mFUser.getLastName();
                                wg6.a((Object) lastName, "mCurrentUser.lastName");
                                if (!xj6.a(lastName)) {
                                    this.this$0.g.h();
                                }
                            }
                        }
                    }
                    this.this$0.g.k0();
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!z) {
                this.this$0.e.postDelayed(this.this$0.f, 500);
                return cd6.a;
            }
            dl6 b = this.this$0.b();
            lu5$b$b lu5_b_b = new lu5$b$b(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = str;
            this.Z$0 = z;
            this.label = 2;
            obj = gk6.a(b, lu5_b_b, this);
            if (obj == a) {
                return a;
            }
            mFUser = (MFUser) obj;
            if (mFUser != null) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter a;

        @DexIgnore
        public c(SplashPresenter splashPresenter) {
            this.a = splashPresenter;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d(SplashPresenter.l.a(), "runnable");
            this.a.h();
        }
    }

    /*
    static {
        String simpleName = SplashPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "SplashPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public SplashPresenter(iu5 iu5, UserRepository userRepository, MigrationHelper migrationHelper, ThemeRepository themeRepository, an4 an4) {
        wg6.b(iu5, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(migrationHelper, "mMigrationHelper");
        wg6.b(themeRepository, "mThemeRepository");
        wg6.b(an4, "mSharePrefs");
        this.g = iu5;
        this.h = userRepository;
        this.i = migrationHelper;
        this.j = themeRepository;
    }

    @DexIgnore
    public void f() {
        String h2 = PortfolioApp.get.instance().h();
        boolean a2 = this.i.a(h2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "currentAppVersion " + h2 + " complete " + a2);
        if (!a2) {
            this.i.b(h2);
        }
        this.e.postDelayed(this.f, 1000);
    }

    @DexIgnore
    public void g() {
        this.e.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    public final void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }
}
