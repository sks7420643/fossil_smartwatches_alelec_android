package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;

import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.pc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rk4;
import com.fossil.rm6;
import com.fossil.s95;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.t95;
import com.fossil.v3;
import com.fossil.v95$b$a;
import com.fossil.v95$d$a;
import com.fossil.v95$h$a;
import com.fossil.v95$j$a;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppPresenter extends s95 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public HybridCustomizeViewModel e;
    @DexIgnore
    public ArrayList<Category> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<MicroApp>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<lc6<String, Boolean>>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<pc6<String, Boolean, Parcelable>> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<String> l;
    @DexIgnore
    public /* final */ ld<String> m;
    @DexIgnore
    public /* final */ LiveData<List<MicroApp>> n;
    @DexIgnore
    public /* final */ ld<List<MicroApp>> o;
    @DexIgnore
    public /* final */ LiveData<List<lc6<String, Boolean>>> p;
    @DexIgnore
    public /* final */ ld<List<lc6<String, Boolean>>> q;
    @DexIgnore
    public /* final */ ld<pc6<String, Boolean, Parcelable>> r;
    @DexIgnore
    public /* final */ t95 s;
    @DexIgnore
    public /* final */ CategoryRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1", f = "MicroAppPresenter.kt", l = {288}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(MicroAppPresenter microAppPresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = microAppPresenter;
            this.$id = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$id, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            boolean z;
            Object a = ff6.a();
            int i = this.label;
            Parcelable parcelable = null;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                z = rk4.c.d(this.$id);
                if (z) {
                    dl6 a2 = this.this$0.b();
                    v95$b$a v95_b_a = new v95$b$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = null;
                    this.Z$0 = z;
                    this.label = 1;
                    obj = gk6.a(a2, v95_b_a, this);
                    if (obj == a) {
                        return a;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = MicroAppPresenter.u;
                local.d(l, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.k.a(new pc6(this.$id, hf6.a(z), parcelable));
                return cd6.a;
            } else if (i == 1) {
                boolean z2 = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                z = z2;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) obj;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String l2 = MicroAppPresenter.u;
            local2.d(l2, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.k.a(new pc6(this.$id, hf6.a(z), parcelable));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public c(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = MicroAppPresenter.u;
            local.d(l, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.s.e(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public d(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(MicroApp microApp) {
            if (microApp != null) {
                String str = (String) this.a.h.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = MicroAppPresenter.u;
                local.d(l, "transform from selected microApp to category currentCategory=" + str + " compsCategories=" + microApp.getCategories());
                ArrayList<String> categories = microApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.h.a(categories.get(0));
                } else {
                    this.a.h.a(str);
                }
            } else {
                MicroAppPresenter microAppPresenter = this.a;
                rm6 unused = ik6.b(microAppPresenter.e(), (af6) null, (ll6) null, new v95$d$a(microAppPresenter, (xe6) null), 3, (Object) null);
            }
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<List<? extends MicroApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public e(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<MicroApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = MicroAppPresenter.u;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged microApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(l, sb.toString());
            if (list != null) {
                this.a.s.w(list);
                MicroApp microApp = (MicroApp) MicroAppPresenter.f(this.a).d().a();
                if (microApp != null) {
                    this.a.s.b(microApp);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public f(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<MicroApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d(MicroAppPresenter.u, "transform from category to list microApp with category=" + str);
            HybridCustomizeViewModel f = MicroAppPresenter.f(this.a);
            wg6.a((Object) str, "category");
            List<MicroApp> a2 = f.a(str);
            ArrayList arrayList = new ArrayList();
            HybridPreset hybridPreset = (HybridPreset) MicroAppPresenter.f(this.a).a().a();
            MicroApp microApp = (MicroApp) MicroAppPresenter.f(this.a).d().a();
            String id = microApp != null ? microApp.getId() : null;
            if (hybridPreset != null) {
                for (MicroApp next : a2) {
                    Iterator<T> it = hybridPreset.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                        boolean z = true;
                        if (!wg6.a((Object) hybridPresetAppSetting.getAppId(), (Object) next.getId()) || !(!wg6.a((Object) hybridPresetAppSetting.getAppId(), (Object) id))) {
                            z = false;
                            continue;
                        }
                        if (z) {
                            break;
                        }
                    }
                    if (((HybridPresetAppSetting) t) == null || wg6.a((Object) next.getId(), (Object) "empty")) {
                        arrayList.add(next);
                    }
                }
            }
            this.a.i.a(arrayList);
            return this.a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<List<? extends lc6<? extends String, ? extends Boolean>>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public g(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION) == false) goto L_0x00bf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE) != false) goto L_0x008f;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00da  */
        /* renamed from: a */
        public final void onChanged(List<lc6<String, Boolean>> list) {
            lc6 lc6;
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        lc6 = null;
                        break;
                    }
                    lc6 = it.next();
                    if (!((Boolean) lc6.getSecond()).booleanValue()) {
                        break;
                    }
                }
                lc6 lc62 = (lc6) lc6;
                FLogger.INSTANCE.getLocal().d(MicroAppPresenter.u, "onLiveDataChanged permissionsRequired " + lc62 + ' ');
                String str = "";
                int i = 0;
                if (lc62 != null) {
                    String str2 = (String) lc62.getFirst();
                    int hashCode = str2.hashCode();
                    if (hashCode != 385352715) {
                        if (hashCode != 564039755) {
                            if (hashCode == 766697727) {
                            }
                        } else if (str2.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887041);
                        }
                        t95 k = this.a.s;
                        if (!(list instanceof Collection) || !list.isEmpty()) {
                            for (lc6 second : list) {
                                if (((Boolean) second.getSecond()).booleanValue() && (i = i + 1) < 0) {
                                    qd6.b();
                                    throw null;
                                }
                            }
                        }
                        int size = list.size();
                        String a2 = xx5.a.a((String) lc62.getFirst());
                        wg6.a((Object) str, "content");
                        k.a(i, size, a2, str);
                        return;
                    }
                    nh6 nh6 = nh6.a;
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886979);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026nLocationServicesToAllow)");
                    Object[] objArr = {PortfolioApp.get.instance().i()};
                    str = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) str, "java.lang.String.format(format, *args)");
                    t95 k2 = this.a.s;
                    while (r5.hasNext()) {
                    }
                    int size2 = list.size();
                    String a22 = xx5.a.a((String) lc62.getFirst());
                    wg6.a((Object) str, "content");
                    k2.a(i, size2, a22, str);
                    return;
                }
                this.a.s.a(0, 0, str, str);
                MicroApp microApp = (MicroApp) MicroAppPresenter.f(this.a).d().a();
                if (microApp != null) {
                    t95 k3 = this.a.s;
                    String a4 = jm4.a(PortfolioApp.get.instance(), microApp.getDescriptionKey(), microApp.getDescription());
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    k3.C(a4);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public h(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<lc6<String, Boolean>>> apply(MicroApp microApp) {
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new v95$h$a(this, microApp, (xe6) null), 3, (Object) null);
            return this.a.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ld<pc6<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public i(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public final void onChanged(pc6<String, Boolean, ? extends Parcelable> pc6) {
            String str;
            if (pc6 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = MicroAppPresenter.u;
                local.d(l, "onLiveDataChanged setting of " + pc6.getFirst() + " isSettingRequired " + pc6.getSecond().booleanValue() + " setting " + ((Parcelable) pc6.getThird()));
                boolean booleanValue = pc6.getSecond().booleanValue();
                MicroApp microApp = (MicroApp) MicroAppPresenter.f(this.a).d().a();
                if (wg6.a((Object) microApp != null ? microApp.getId() : null, (Object) pc6.getFirst())) {
                    String str2 = "";
                    if (booleanValue) {
                        Parcelable parcelable = (Parcelable) pc6.getThird();
                        if (parcelable == null) {
                            String str3 = str2;
                            str2 = rk4.c.a(pc6.getFirst());
                            str = str3;
                        } else {
                            try {
                                String first = pc6.getFirst();
                                if (wg6.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                                    str = ((CommuteTimeSetting) parcelable).getAddress();
                                } else if (wg6.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                    str = ((SecondTimezoneSetting) parcelable).getTimeZoneName();
                                } else {
                                    if (wg6.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                                        str = ((Ringtone) parcelable).getRingtoneName();
                                    }
                                    str = str2;
                                }
                            } catch (Exception e) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String l2 = MicroAppPresenter.u;
                                local2.d(l2, "exception when parse micro app setting " + e);
                            }
                        }
                        this.a.s.a(true, pc6.getFirst(), str2, str);
                        return;
                    }
                    this.a.s.a(false, pc6.getFirst(), str2, (String) null);
                    if (microApp != null) {
                        t95 k = this.a.s;
                        String a2 = jm4.a(PortfolioApp.get.instance(), microApp.getDescriptionKey(), microApp.getDescription());
                        wg6.a((Object) a2, "LanguageHelper.getString\u2026ptionKey, it.description)");
                        k.C(a2);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1", f = "MicroAppPresenter.kt", l = {245}, m = "invokeSuspend")
    public static final class j extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(MicroAppPresenter microAppPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = microAppPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            j jVar = new j(this.this$0, xe6);
            jVar.p$ = (il6) obj;
            return jVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((j) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                v95$j$a v95_j_a = new v95$j$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, v95_j_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) obj) {
                if (!MicroAppPresenter.f(this.this$0).a(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.f.clear();
            this.this$0.f.addAll(arrayList);
            this.this$0.s.a(this.this$0.f);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ld<MicroApp> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public k(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(MicroApp microApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = MicroAppPresenter.u;
            local.d(l, "onLiveDataChanged selectedMicroApp value=" + microApp);
            this.a.g.a(microApp);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = MicroAppPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppPresenter::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public MicroAppPresenter(t95 t95, CategoryRepository categoryRepository) {
        wg6.b(t95, "mView");
        wg6.b(categoryRepository, "mCategoryRepository");
        this.s = t95;
        this.t = categoryRepository;
        LiveData<String> b2 = sd.b(this.g, new d(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.l = b2;
        this.m = new c(this);
        LiveData<List<MicroApp>> b3 = sd.b(this.l, new f(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.n = b3;
        this.o = new e(this);
        LiveData<List<lc6<String, Boolean>>> b4 = sd.b(this.g, new h(this));
        wg6.a((Object) b4, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.p = b4;
        this.q = new g(this);
        this.r = new i(this);
    }

    @DexIgnore
    public static final /* synthetic */ HybridCustomizeViewModel f(MicroAppPresenter microAppPresenter) {
        HybridCustomizeViewModel hybridCustomizeViewModel = microAppPresenter.e;
        if (hybridCustomizeViewModel != null) {
            return hybridCustomizeViewModel;
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final rm6 b(String str) {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(u, "onStart");
        this.l.a(this.m);
        this.n.a(this.o);
        this.p.a(this.q);
        this.k.a(this.r);
        if (this.f.isEmpty()) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new j(this, (xe6) null), 3, (Object) null);
        } else {
            this.s.a(this.f);
        }
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            LiveData<MicroApp> d2 = hybridCustomizeViewModel.d();
            t95 t95 = this.s;
            if (t95 != null) {
                d2.a((MicroAppFragment) t95, new k(this));
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            LiveData<MicroApp> d2 = hybridCustomizeViewModel.d();
            t95 t95 = this.s;
            if (t95 != null) {
                d2.a((MicroAppFragment) t95);
                this.i.b(this.o);
                this.h.b(this.m);
                this.j.b(this.q);
                this.k.b(this.r);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        T t2;
        String str;
        T t3;
        String str2;
        String str3;
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        T t4 = null;
        if (hybridCustomizeViewModel != null) {
            HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel.a().a();
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wg6.a((Object) ((HybridPresetAppSetting) t2).getPosition(), (Object) "top")) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                if (hybridPresetAppSetting == null || (str = hybridPresetAppSetting.getAppId()) == null) {
                    str = "empty";
                }
                Iterator<T> it2 = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it2.next();
                    if (wg6.a((Object) ((HybridPresetAppSetting) t3).getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t3;
                if (hybridPresetAppSetting2 == null || (str2 = hybridPresetAppSetting2.getAppId()) == null) {
                    str2 = "empty";
                }
                Iterator<T> it3 = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (wg6.a((Object) ((HybridPresetAppSetting) next).getPosition(), (Object) "bottom")) {
                        t4 = next;
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting3 = (HybridPresetAppSetting) t4;
                if (hybridPresetAppSetting3 == null || (str3 = hybridPresetAppSetting3.getAppId()) == null) {
                    str3 = "empty";
                }
                this.s.a(str, str2, str3);
                return;
            }
            this.s.a("empty", "empty", "empty");
            return;
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    public void i() {
        String str;
        ArrayList<HybridPresetAppSetting> buttons;
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        T t2 = null;
        if (hybridCustomizeViewModel != null) {
            MicroApp microApp = (MicroApp) hybridCustomizeViewModel.d().a();
            if (microApp != null) {
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel2.a().a();
                    if (!(hybridPreset == null || (buttons = hybridPreset.getButtons()) == null)) {
                        Iterator<T> it = buttons.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            T next = it.next();
                            if (wg6.a((Object) microApp.getId(), (Object) ((HybridPresetAppSetting) next).getAppId())) {
                                t2 = next;
                                break;
                            }
                        }
                        HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                        if (hybridPresetAppSetting != null) {
                            str = hybridPresetAppSetting.getSettings();
                        }
                    }
                    str = "";
                    String id = microApp.getId();
                    if (wg6.a((Object) id, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                        this.s.c(str);
                    } else if (wg6.a((Object) id, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                        this.s.g(str);
                    } else if (wg6.a((Object) id, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                        this.s.I(str);
                    }
                } else {
                    wg6.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        Object obj;
        List list = (List) this.j.a();
        if (list != null) {
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!((Boolean) ((lc6) obj).getSecond()).booleanValue()) {
                    break;
                }
            }
            lc6 lc6 = (lc6) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = u;
            local.d(str, "processRequiredPermission " + lc6);
            if (lc6 != null) {
                this.s.f((String) lc6.getFirst());
            }
        }
    }

    @DexIgnore
    public void k() {
        this.s.a(this);
    }

    @DexIgnore
    public void a(HybridCustomizeViewModel hybridCustomizeViewModel) {
        wg6.b(hybridCustomizeViewModel, "viewModel");
        this.e = hybridCustomizeViewModel;
    }

    @DexIgnore
    public void a(Category category) {
        wg6.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "category change " + category);
        this.h.a(category.getId());
    }

    @DexIgnore
    public void a(String str) {
        T t2;
        wg6.b(str, "newMicroAppId");
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            MicroApp b2 = hybridCustomizeViewModel.b(str);
            FLogger.INSTANCE.getLocal().d(u, "onUserChooseMicroApp " + b2);
            if (b2 != null) {
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel2.a().a();
                    if (hybridPreset != null) {
                        HybridPreset clone = hybridPreset.clone();
                        ArrayList arrayList = new ArrayList();
                        HybridCustomizeViewModel hybridCustomizeViewModel3 = this.e;
                        if (hybridCustomizeViewModel3 != null) {
                            Object a2 = hybridCustomizeViewModel3.e().a();
                            if (a2 != null) {
                                wg6.a(a2, "mHybridCustomizeViewMode\u2026ctedMicroAppPos().value!!");
                                String str2 = (String) a2;
                                HybridCustomizeViewModel hybridCustomizeViewModel4 = this.e;
                                if (hybridCustomizeViewModel4 == null) {
                                    wg6.d("mHybridCustomizeViewModel");
                                    throw null;
                                } else if (!hybridCustomizeViewModel4.d(str)) {
                                    Iterator<HybridPresetAppSetting> it = clone.getButtons().iterator();
                                    while (it.hasNext()) {
                                        HybridPresetAppSetting next = it.next();
                                        if (wg6.a((Object) next.getPosition(), (Object) str2)) {
                                            arrayList.add(new HybridPresetAppSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getButtons().clear();
                                    clone.getButtons().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(u, "Update current preset=" + clone);
                                    HybridCustomizeViewModel hybridCustomizeViewModel5 = this.e;
                                    if (hybridCustomizeViewModel5 != null) {
                                        hybridCustomizeViewModel5.a(clone);
                                    } else {
                                        wg6.d("mHybridCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = hybridPreset.getButtons().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t2 = null;
                                            break;
                                        }
                                        t2 = it2.next();
                                        if (wg6.a((Object) ((HybridPresetAppSetting) t2).getAppId(), (Object) str)) {
                                            break;
                                        }
                                    }
                                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                                    if (hybridPresetAppSetting != null) {
                                        HybridCustomizeViewModel hybridCustomizeViewModel6 = this.e;
                                        if (hybridCustomizeViewModel6 != null) {
                                            hybridCustomizeViewModel6.e(hybridPresetAppSetting.getPosition());
                                        } else {
                                            wg6.d("mHybridCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.d("mHybridCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    wg6.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        T t2;
        wg6.b(str, "microAppId");
        wg6.b(str2, MicroAppSetting.SETTING);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel.a().a();
            if (hybridPreset != null) {
                HybridPreset clone = hybridPreset.clone();
                Iterator<T> it = clone.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wg6.a((Object) ((HybridPresetAppSetting) t2).getAppId(), (Object) str)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                if (hybridPresetAppSetting != null) {
                    hybridPresetAppSetting.setSettings(str2);
                }
                FLogger.INSTANCE.getLocal().d(u, "update new setting " + str2 + " of " + str);
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.a(clone);
                } else {
                    wg6.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
