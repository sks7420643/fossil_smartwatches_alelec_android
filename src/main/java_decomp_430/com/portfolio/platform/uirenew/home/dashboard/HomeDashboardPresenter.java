package com.portfolio.platform.uirenew.home.dashboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ua5;
import com.fossil.uh4;
import com.fossil.va5;
import com.fossil.wa5$b$a;
import com.fossil.wa5$b$b;
import com.fossil.wa5$b$c;
import com.fossil.wa5$b$d;
import com.fossil.wa5$d$a;
import com.fossil.wa5$d$b;
import com.fossil.wa5$e$a;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.HomeDashboardFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDashboardPresenter extends ua5 {
    @DexIgnore
    public /* final */ SummariesRepository A;
    @DexIgnore
    public /* final */ GoalTrackingRepository B;
    @DexIgnore
    public /* final */ SleepSummariesRepository C;
    @DexIgnore
    public /* final */ SetReplyMessageMappingUseCase D;
    @DexIgnore
    public /* final */ QuickResponseRepository E;
    @DexIgnore
    public /* final */ HeartRateSampleRepository F;
    @DexIgnore
    public Date e;
    @DexIgnore
    public int f;
    @DexIgnore
    public String g; // = this.x.e();
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE h; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.g);
    @DexIgnore
    public MFSleepDay i;
    @DexIgnore
    public ActivitySummary j;
    @DexIgnore
    public GoalTrackingSummary k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public List<HeartRateSample> m;
    @DexIgnore
    public HashMap<Integer, Boolean> n; // = new HashMap<>();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public volatile boolean p;
    @DexIgnore
    public /* final */ c q; // = new c();
    @DexIgnore
    public LiveData<yx5<ActivitySummary>> r; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<MFSleepDay>> s; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<GoalTrackingSummary>> t; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<Integer>> u; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<HeartRateSample>>> v; // = new MutableLiveData();
    @DexIgnore
    public /* final */ va5 w;
    @DexIgnore
    public /* final */ PortfolioApp x;
    @DexIgnore
    public /* final */ DeviceRepository y;
    @DexIgnore
    public /* final */ cj4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1", f = "HomeDashboardPresenter.kt", l = {106, 107, 108, 109}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HomeDashboardPresenter homeDashboardPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00b0 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00d1 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00d2  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00e9  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00eb  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0117  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0120  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x012b  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0134  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0156  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0207  */
        public final Object invokeSuspend(Object obj) {
            SleepStatistic sleepStatistic;
            ActivityStatistic activityStatistic;
            List list;
            Object obj2;
            il6 il6;
            Object a;
            Object a2;
            il6 il62;
            Object a3 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il62 = this.p$;
                dl6 a4 = this.this$0.c();
                wa5$b$a wa5_b_a = new wa5$b$a(this, (xe6) null);
                this.L$0 = il62;
                this.label = 1;
                obj = gk6.a(a4, wa5_b_a, this);
                if (obj == a3) {
                    return a3;
                }
            } else if (i == 1) {
                il62 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                activityStatistic = (ActivityStatistic) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                SleepStatistic sleepStatistic2 = (SleepStatistic) obj;
                dl6 a5 = this.this$0.c();
                wa5$b$d wa5_b_d = new wa5$b$d(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = activityStatistic;
                this.L$2 = sleepStatistic2;
                this.label = 3;
                a2 = gk6.a(a5, wa5_b_d, this);
                if (a2 != a3) {
                    return a3;
                }
                Object obj3 = a2;
                sleepStatistic = sleepStatistic2;
                obj = obj3;
                List list2 = (List) obj;
                dl6 a6 = this.this$0.c();
                wa5$b$b wa5_b_b = new wa5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = activityStatistic;
                this.L$2 = sleepStatistic;
                this.L$3 = list2;
                this.label = 4;
                a = gk6.a(a6, wa5_b_b, this);
                if (a != a3) {
                }
            } else if (i == 3) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                ActivityStatistic activityStatistic2 = (ActivityStatistic) this.L$1;
                sleepStatistic = (SleepStatistic) this.L$2;
                activityStatistic = activityStatistic2;
                List list22 = (List) obj;
                dl6 a62 = this.this$0.c();
                wa5$b$b wa5_b_b2 = new wa5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = activityStatistic;
                this.L$2 = sleepStatistic;
                this.L$3 = list22;
                this.label = 4;
                a = gk6.a(a62, wa5_b_b2, this);
                if (a != a3) {
                    return a3;
                }
                list = list22;
                obj = a;
                String str = (String) obj;
                if (PortfolioApp.get.instance().h(this.this$0.g) != 2) {
                }
                if (activityStatistic == null || activityStatistic.getTotalSteps() == 0) {
                    this.this$0.o = (activityStatistic == null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("checkDeviceStatus activity ");
                    sb.append(activityStatistic != null ? hf6.a(activityStatistic.getTotalSteps()) : null);
                    sb.append(" sleep ");
                    sb.append(sleepStatistic != null ? hf6.a(sleepStatistic.getTotalSleeps()) : null);
                    sb.append(" totalDevices ");
                    sb.append(list.size());
                    local.d("HomeDashboardPresenter", sb.toString());
                    if (!list.isEmpty()) {
                    }
                    this.this$0.a("release");
                    this.this$0.p();
                    return cd6.a;
                }
                this.this$0.o = (activityStatistic == null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("checkDeviceStatus activity ");
                sb2.append(activityStatistic != null ? hf6.a(activityStatistic.getTotalSteps()) : null);
                sb2.append(" sleep ");
                sb2.append(sleepStatistic != null ? hf6.a(sleepStatistic.getTotalSleeps()) : null);
                sb2.append(" totalDevices ");
                sb2.append(list.size());
                local2.d("HomeDashboardPresenter", sb2.toString());
                if (!list.isEmpty()) {
                }
                this.this$0.a("release");
                this.this$0.p();
                return cd6.a;
            } else if (i == 4) {
                list = (List) this.L$3;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                sleepStatistic = (SleepStatistic) this.L$2;
                activityStatistic = (ActivityStatistic) this.L$1;
                String str2 = (String) obj;
                boolean z = PortfolioApp.get.instance().h(this.this$0.g) != 2;
                this.this$0.o = (activityStatistic == null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                StringBuilder sb22 = new StringBuilder();
                sb22.append("checkDeviceStatus activity ");
                sb22.append(activityStatistic != null ? hf6.a(activityStatistic.getTotalSteps()) : null);
                sb22.append(" sleep ");
                sb22.append(sleepStatistic != null ? hf6.a(sleepStatistic.getTotalSleeps()) : null);
                sb22.append(" totalDevices ");
                sb22.append(list.size());
                local22.d("HomeDashboardPresenter", sb22.toString());
                if (!list.isEmpty()) {
                    Iterator it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        obj2 = it.next();
                        if (hf6.a(wg6.a((Object) ((Device) obj2).getDeviceId(), (Object) this.this$0.g)).booleanValue()) {
                            break;
                        }
                    }
                    Device device = (Device) obj2;
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("checkDeviceStatus activeDevice ");
                    sb3.append(device != null ? device.getDeviceId() : null);
                    sb3.append(" currentSerial ");
                    sb3.append(this.this$0.g);
                    local3.d("HomeDashboardPresenter", sb3.toString());
                    if (device != null) {
                        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "checkDeviceStatus isActiveDeviceConnected " + z);
                        this.this$0.w.a(this.this$0.g, str2);
                        this.this$0.w.a(true, true, this.this$0.o);
                    } else {
                        this.this$0.w.a((String) null, "");
                        this.this$0.w.a(true, false, this.this$0.o);
                    }
                } else {
                    this.this$0.w.a((String) null, "");
                    this.this$0.w.a(false, false, this.this$0.o);
                }
                this.this$0.a("release");
                this.this$0.p();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ActivityStatistic activityStatistic3 = (ActivityStatistic) obj;
            dl6 a7 = this.this$0.c();
            wa5$b$c wa5_b_c = new wa5$b$c(this, (xe6) null);
            this.L$0 = il62;
            this.L$1 = activityStatistic3;
            this.label = 2;
            Object a8 = gk6.a(a7, wa5_b_c, this);
            if (a8 == a3) {
                return a3;
            }
            il6 il64 = il62;
            activityStatistic = activityStatistic3;
            obj = a8;
            il6 = il64;
            SleepStatistic sleepStatistic22 = (SleepStatistic) obj;
            dl6 a52 = this.this$0.c();
            wa5$b$d wa5_b_d2 = new wa5$b$d(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = activityStatistic;
            this.L$2 = sleepStatistic22;
            this.label = 3;
            a2 = gk6.a(a52, wa5_b_d2, this);
            if (a2 != a3) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$setReplyMessageToDevice$1", f = "HomeDashboardPresenter.kt", l = {309}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HomeDashboardPresenter homeDashboardPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.wa5$d$a] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                wa5$d$b wa5_d_b = new wa5$d$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, wa5_d_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.D.a(new SetReplyMessageMappingUseCase.b((List) obj), new wa5$d$a());
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1", f = "HomeDashboardPresenter.kt", l = {140}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $buildMode;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(HomeDashboardPresenter homeDashboardPresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
            this.$buildMode = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$buildMode, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                wa5$e$a wa5_e_a = new wa5$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, wa5_e_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) obj;
            if (device != null) {
                if (wg6.a((Object) this.$buildMode, (Object) Constants.DEBUG) || wg6.a((Object) this.$buildMode, (Object) "staging")) {
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= PortfolioApp.get.instance().w().x() || device.getBatteryLevel() <= 0) {
                        this.this$0.w.o(false);
                    } else {
                        this.this$0.w.o(true);
                    }
                } else if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 10 || device.getBatteryLevel() <= 0) {
                    this.this$0.w.o(false);
                } else {
                    this.this$0.w.o(true);
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<yx5<? extends ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public f(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<ActivitySummary> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSummaryLiveData -- summary=" + yx5 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(0)));
            ActivitySummary activitySummary = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    activitySummary = yx5.d();
                }
                if ((!wg6.a((Object) (Boolean) this.a.n.get(0), (Object) true)) || (!wg6.a((Object) this.a.j, (Object) activitySummary))) {
                    this.a.j = activitySummary;
                    this.a.p();
                    this.a.n.put(0, true);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<yx5<? extends MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public g(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<MFSleepDay> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSleepSummaryLiveData -- summary=" + yx5 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(5)));
            MFSleepDay mFSleepDay = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    mFSleepDay = yx5.d();
                }
                if ((!wg6.a((Object) (Boolean) this.a.n.get(5), (Object) true)) || (!wg6.a((Object) this.a.i, (Object) mFSleepDay))) {
                    this.a.i = mFSleepDay;
                    this.a.p();
                    this.a.n.put(5, true);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<yx5<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public h(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<GoalTrackingSummary> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalTrackingSummaryLiveData -- summary=" + yx5 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(4)));
            GoalTrackingSummary goalTrackingSummary = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    goalTrackingSummary = yx5.d();
                }
                if ((!wg6.a((Object) (Boolean) this.a.n.get(4), (Object) true)) || (!wg6.a((Object) this.a.k, (Object) goalTrackingSummary))) {
                    this.a.k = goalTrackingSummary;
                    this.a.p();
                    this.a.n.put(4, true);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ld<yx5<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public i(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<Integer> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- resource=" + yx5);
            Integer num = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    num = yx5.d();
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- goal=" + num);
                if (!wg6.a((Object) this.a.l, (Object) num)) {
                    this.a.l = num;
                    this.a.p();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ld<yx5<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public j(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<HeartRateSample>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSampleLiveData -- resource.status=");
            sb.append(a2);
            sb.append(", ");
            sb.append("data.size=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", mFirstDBLoad=");
            sb.append((Boolean) this.a.n.get(3));
            local.d("HomeDashboardPresenter", sb.toString());
            if (a2 == wh4.DATABASE_LOADING) {
                return;
            }
            if ((!wg6.a((Object) (Boolean) this.a.n.get(3), (Object) true)) || (!wg6.a((Object) this.a.m, (Object) list))) {
                this.a.m = list;
                this.a.p();
                this.a.n.put(3, true);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HomeDashboardPresenter(va5 va5, PortfolioApp portfolioApp, DeviceRepository deviceRepository, cj4 cj4, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase, QuickResponseRepository quickResponseRepository, HeartRateSampleRepository heartRateSampleRepository) {
        wg6.b(va5, "mView");
        wg6.b(portfolioApp, "mApp");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(cj4, "mDeviceSettingFactory");
        wg6.b(summariesRepository, "mSummaryRepository");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(sleepSummariesRepository, "mSleepSummaryRepository");
        wg6.b(setReplyMessageMappingUseCase, "mReplyMessageUseCase");
        wg6.b(quickResponseRepository, "mQRRepository");
        wg6.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        this.w = va5;
        this.x = portfolioApp;
        this.y = deviceRepository;
        this.z = cj4;
        this.A = summariesRepository;
        this.B = goalTrackingRepository;
        this.C = sleepSummariesRepository;
        this.D = setReplyMessageMappingUseCase;
        this.E = quickResponseRepository;
        this.F = heartRateSampleRepository;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.w.b(z2);
    }

    @DexIgnore
    public final void c(int i2) {
        this.w.c(i2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "start - this=" + hashCode());
        l();
        Object r0 = this.x;
        c cVar = this.q;
        r0.registerReceiver(cVar, new IntentFilter(this.x.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        k();
        LiveData<yx5<ActivitySummary>> liveData = this.r;
        va5 va5 = this.w;
        if (va5 != null) {
            liveData.a((HomeDashboardFragment) va5, new f(this));
            this.s.a(this.w, new g(this));
            this.t.a(this.w, new h(this));
            this.u.a(this.w, new i(this));
            this.v.a(this.w, new j(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void g() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "stop");
        try {
            this.x.unregisterReceiver(this.q);
            va5 va5 = this.w;
            if (va5 != null) {
                HomeDashboardFragment homeDashboardFragment = (HomeDashboardFragment) va5;
                this.r.a(homeDashboardFragment);
                this.s.a(homeDashboardFragment);
                this.t.a(homeDashboardFragment);
                this.u.a(homeDashboardFragment);
                this.v.a(homeDashboardFragment);
                PortfolioApp.get.instance().f().a(homeDashboardFragment);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeDashboardPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.h;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public int i() {
        return this.f;
    }

    @DexIgnore
    public void j() {
        String e2 = this.x.e();
        int g2 = this.x.g(e2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "Inside .onRefresh currentDeviceSession=" + g2);
        if (TextUtils.isEmpty(e2) || g2 == CommunicateMode.OTA.getValue()) {
            this.w.i(false);
        } else {
            this.p = true;
            this.w.b0();
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.x.e(), "HomeDashboardPresenter", "[Sync Start] PULL TO SYNC");
            this.x.a(this.z, false, 12);
        }
        n();
    }

    @DexIgnore
    public final rm6 k() {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "loadData");
        this.e = new Date();
        this.n = new HashMap<>();
        SummariesRepository summariesRepository = this.A;
        Date date = this.e;
        if (date != null) {
            this.r = summariesRepository.getSummary(date);
            SleepSummariesRepository sleepSummariesRepository = this.C;
            Date date2 = this.e;
            if (date2 != null) {
                this.s = sleepSummariesRepository.getSleepSummary(date2);
                GoalTrackingRepository goalTrackingRepository = this.B;
                Date date3 = this.e;
                if (date3 != null) {
                    this.t = goalTrackingRepository.getSummary(date3);
                    this.u = this.B.getLastGoalSetting();
                    HeartRateSampleRepository heartRateSampleRepository = this.F;
                    Date date4 = this.e;
                    if (date4 == null) {
                        wg6.d("mDate");
                        throw null;
                    } else if (date4 != null) {
                        this.v = heartRateSampleRepository.getHeartRateSamples(date4, date4, true);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("loadData - mDate=");
                        Date date5 = this.e;
                        if (date5 != null) {
                            sb.append(date5);
                            local.d("HomeDashboardPresenter", sb.toString());
                            return;
                        }
                        wg6.d("mDate");
                        throw null;
                    } else {
                        wg6.d("mDate");
                        throw null;
                    }
                } else {
                    wg6.d("mDate");
                    throw null;
                }
            } else {
                wg6.d("mDate");
                throw null;
            }
        } else {
            wg6.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        this.w.Y();
    }

    @DexIgnore
    public final void n() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "setReplyMessageToDevice");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void o() {
        this.w.a(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        r0 = r4.getResting();
     */
    @DexIgnore
    public final void p() {
        Integer num;
        HeartRateSample heartRateSample;
        Resting resting;
        boolean z2;
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "updateHomeInfo");
        this.w.a(this.j, this.i, this.k);
        List<HeartRateSample> list = this.m;
        if (list != null) {
            ListIterator<HeartRateSample> listIterator = list.listIterator(list.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    heartRateSample = null;
                    break;
                }
                heartRateSample = listIterator.previous();
                if (heartRateSample.getResting() != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            HeartRateSample heartRateSample2 = heartRateSample;
            int value = (heartRateSample2 == null || resting == null) ? 0 : resting.getValue();
            num = Integer.valueOf(value);
        } else {
            num = null;
        }
        this.w.a(this.j, this.i, this.k, this.l, num, (this.g.length() == 0) && this.o);
        va5 va5 = this.w;
        Date date = this.e;
        if (date != null) {
            va5.a(date);
        } else {
            wg6.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2) {
        if (i2 == this.f) {
            this.w.A();
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.f = i2;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "buildMode");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str, boolean z2) {
        wg6.b(str, "activeId");
        this.x.a(str, z2);
    }

    @DexIgnore
    public void a(Intent intent) {
        wg6.b(intent, "intent");
        if (this.w.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && xj6.b(stringExtra, this.g, true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardPresenter", "mSyncReceiver - syncStatus: " + intExtra + " - mIntendingSync: " + this.p);
                if (intExtra != 0) {
                    if (intExtra == 1) {
                        this.p = false;
                        this.w.i(true);
                    } else if (intExtra == 2 || intExtra == 4) {
                        this.p = false;
                        this.w.i(false);
                        if (intExtra2 == 12) {
                            int intExtra3 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                            if (integerArrayListExtra == null) {
                                integerArrayListExtra = new ArrayList<>();
                            }
                            if (intExtra3 != 1101) {
                                if (intExtra3 == 1603) {
                                    this.w.i(false);
                                    return;
                                } else if (!(intExtra3 == 1112 || intExtra3 == 1113)) {
                                    this.w.a0();
                                    return;
                                }
                            }
                            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(integerArrayListExtra);
                            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                            va5 va5 = this.w;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                            if (array != null) {
                                uh4[] uh4Arr = (uh4[]) array;
                                va5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                                return;
                            }
                            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    } else if (intExtra == 5) {
                        this.w.E();
                    }
                } else if (!this.p) {
                    this.p = true;
                    this.w.b0();
                }
            }
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        this.p = z2;
    }
}
