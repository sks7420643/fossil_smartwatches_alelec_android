package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.fossil.k65;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationSearchActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public ComplicationSearchPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3, String str4) {
            wg6.b(fragment, "fragment");
            wg6.b(str, "topComplication");
            wg6.b(str2, "bottomComplication");
            wg6.b(str3, "leftComplication");
            wg6.b(str4, "rightComplication");
            Intent intent = new Intent(fragment.getContext(), ComplicationSearchActivity.class);
            intent.putExtra("top", str);
            intent.putExtra("bottom", str2);
            intent.putExtra("left", str3);
            intent.putExtra("right", str4);
            fragment.startActivityForResult(intent, 102, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v0, types: [com.portfolio.platform.ui.BaseActivity, com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ComplicationSearchFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = ComplicationSearchFragment.o.b();
            a((Fragment) b, ComplicationSearchFragment.o.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new k65(b)).a(this);
            Intent intent = getIntent();
            wg6.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                ComplicationSearchPresenter complicationSearchPresenter = this.B;
                if (complicationSearchPresenter != null) {
                    String string = extras.getString("top");
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("bottom");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("right");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    String string4 = extras.getString("left");
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    complicationSearchPresenter.a(string, string2, string3, string4);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                ComplicationSearchPresenter complicationSearchPresenter2 = this.B;
                if (complicationSearchPresenter2 != null) {
                    String string5 = bundle.getString("top");
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    String string7 = bundle.getString("right");
                    if (string7 == null) {
                        string7 = "empty";
                    }
                    String string8 = bundle.getString("left");
                    if (string8 == null) {
                        string8 = "empty";
                    }
                    complicationSearchPresenter2.a(string5, string6, string7, string8);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchContract.View");
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        ComplicationSearchPresenter complicationSearchPresenter = this.B;
        if (complicationSearchPresenter != null) {
            complicationSearchPresenter.a(bundle);
            if (bundle != null) {
                ComplicationSearchActivity.super.onSaveInstanceState(bundle);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }
}
