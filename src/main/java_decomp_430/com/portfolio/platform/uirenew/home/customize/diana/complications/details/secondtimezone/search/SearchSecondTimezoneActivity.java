package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.b65;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.SearchSecondTimezoneFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public SearchSecondTimezonePresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wg6.b(fragment, "fragment");
            wg6.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), SearchSecondTimezoneActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 100);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(f(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        SearchSecondTimezoneFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = SearchSecondTimezoneFragment.o.a();
            a((Fragment) b, SearchSecondTimezoneFragment.o.b(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new b65(b)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                str = intent.getStringExtra(Constants.USER_SETTING);
                wg6.a((Object) str, "it.getStringExtra(Constants.JSON_KEY_SETTINGS)");
            } else {
                str = "";
            }
            SearchSecondTimezonePresenter searchSecondTimezonePresenter = this.B;
            if (searchSecondTimezonePresenter != null) {
                searchSecondTimezonePresenter.a(str);
            } else {
                wg6.d("mSearchSecondTimezonePresenter");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneContract.View");
        }
    }
}
