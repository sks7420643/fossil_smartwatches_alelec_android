package com.portfolio.platform.uirenew.home.details.heartrate;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.cf;
import com.fossil.ha4;
import com.fossil.jm4;
import com.fossil.jz5;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.oi5;
import com.fossil.pc6;
import com.fossil.pi5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sh5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.yk4;
import com.fossil.zg5;
import com.fossil.zh4;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateDetailFragment extends BaseFragment implements pi5, View.OnClickListener {
    @DexIgnore
    public static /* final */ a w; // = new a((qg6) null);
    @DexIgnore
    public ax5<ha4> f;
    @DexIgnore
    public oi5 g;
    @DexIgnore
    public Date h; // = new Date();
    @DexIgnore
    public sh5 i;
    @DexIgnore
    public /* final */ Calendar j; // = Calendar.getInstance();
    @DexIgnore
    public String o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HeartRateDetailFragment a(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            HeartRateDetailFragment heartRateDetailFragment = new HeartRateDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            heartRateDetailFragment.setArguments(bundle);
            return heartRateDetailFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ cf b;

        @DexIgnore
        public b(HeartRateDetailFragment heartRateDetailFragment, boolean z, cf cfVar, zh4 zh4) {
            this.a = z;
            this.b = cfVar;
        }

        @DexIgnore
        public boolean a(AppBarLayout appBarLayout) {
            wg6.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public HeartRateDetailFragment() {
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        this.p = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("backgroundDashboard");
        this.q = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("secondaryText");
        this.r = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("primaryText");
        this.s = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.t = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.u = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void c(int i2, int i3) {
        ax5<ha4> ax5 = this.f;
        if (ax5 != null) {
            ha4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (i2 == 0 && i3 == 0) {
                Object r7 = a2.z;
                wg6.a((Object) r7, "it.ftvNoRecord");
                r7.setVisibility(0);
                ConstraintLayout constraintLayout = a2.s;
                wg6.a((Object) constraintLayout, "it.clContainer");
                constraintLayout.setVisibility(8);
                return;
            }
            Object r5 = a2.z;
            wg6.a((Object) r5, "it.ftvNoRecord");
            r5.setVisibility(8);
            ConstraintLayout constraintLayout2 = a2.s;
            wg6.a((Object) constraintLayout2, "it.clContainer");
            constraintLayout2.setVisibility(0);
            Object r1 = a2.C;
            wg6.a((Object) r1, "it.ftvRestingValue");
            r1.setText(String.valueOf(i2));
            Object r72 = a2.y;
            wg6.a((Object) r72, "it.ftvMaxValue");
            r72.setText(String.valueOf(i3));
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "HeartRateDetailFragment";
    }

    @DexIgnore
    public final void j1() {
        TodayHeartRateChart todayHeartRateChart;
        ax5<ha4> ax5 = this.f;
        if (ax5 != null) {
            ha4 a2 = ax5.a();
            if (a2 != null && (todayHeartRateChart = a2.u) != null) {
                todayHeartRateChart.a("maxHeartRate", "lowestHeartRate");
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        MFLogger.d("HeartRateDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362542:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362543:
                    oi5 oi5 = this.g;
                    if (oi5 != null) {
                        oi5.j();
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131362609:
                    oi5 oi52 = this.g;
                    if (oi52 != null) {
                        oi52.i();
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        wg6.b(layoutInflater, "inflater");
        HeartRateDetailFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        ha4 a2 = kb.a(layoutInflater, 2131558557, viewGroup, false, e1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.h = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.h = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        wg6.a((Object) a2, "binding");
        a(a2);
        oi5 oi5 = this.g;
        if (oi5 != null) {
            oi5.a(this.h);
            this.f = new ax5<>(this, a2);
            j1();
            ax5<ha4> ax5 = this.f;
            if (ax5 != null) {
                ha4 a3 = ax5.a();
                if (a3 != null) {
                    return a3.d();
                }
                return null;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        oi5 oi5 = this.g;
        if (oi5 != null) {
            oi5.h();
            super.onDestroyView();
            d1();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        HeartRateDetailFragment.super.onPause();
        oi5 oi5 = this.g;
        if (oi5 != null) {
            oi5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        HeartRateDetailFragment.super.onResume();
        j1();
        oi5 oi5 = this.g;
        if (oi5 != null) {
            oi5.b(this.h);
            oi5 oi52 = this.g;
            if (oi52 != null) {
                oi52.f();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        oi5 oi5 = this.g;
        if (oi5 != null) {
            oi5.a(bundle);
            HeartRateDetailFragment.super.onSaveInstanceState(bundle);
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(ha4 ha4) {
        ha4.D.setOnClickListener(this);
        ha4.E.setOnClickListener(this);
        ha4.F.setOnClickListener(this);
        this.o = ThemeManager.l.a().b("dianaHeartRateTab");
        this.i = new sh5(sh5.c.HEART_RATE, zh4.IMPERIAL, new WorkoutSessionDifference(), this.o);
        RecyclerView recyclerView = ha4.I;
        wg6.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.i);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = w6.c(recyclerView.getContext(), 2131230877);
        if (c != null) {
            zg5 zg5 = new zg5(linearLayoutManager.Q(), false, false, 6, (qg6) null);
            wg6.a((Object) c, ResourceManager.DRAWABLE);
            zg5.a(c);
            recyclerView.addItemDecoration(zg5);
        }
        ha4.G.setBackgroundColor(this.q);
        ha4.r.setBackgroundColor(this.p);
        ha4.C.setTextColor(this.s);
        ha4.B.setTextColor(this.t);
        ha4.y.setTextColor(this.s);
        ha4.x.setTextColor(this.t);
        ha4.w.setTextColor(this.r);
        ha4.v.setTextColor(this.s);
        ha4.t.setBackgroundColor(this.u);
        ha4.u.setBackgroundColor(this.q);
    }

    @DexIgnore
    public void a(oi5 oi5) {
        wg6.b(oi5, "presenter");
        this.g = oi5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        this.h = date;
        Calendar calendar = this.j;
        wg6.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.j.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.j);
        ax5<ha4> ax5 = this.f;
        if (ax5 != null) {
            ha4 a2 = ax5.a();
            if (a2 != null) {
                a2.q.a(true, true);
                Object r1 = a2.v;
                wg6.a((Object) r1, "binding.ftvDayOfMonth");
                r1.setText(String.valueOf(this.j.get(5)));
                if (z) {
                    ImageView imageView = a2.E;
                    wg6.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.E;
                    wg6.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.F;
                    wg6.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        Object r5 = a2.w;
                        wg6.a((Object) r5, "binding.ftvDayOfWeek");
                        r5.setText(jm4.a(getContext(), 2131886449));
                        return;
                    }
                    Object r52 = a2.w;
                    wg6.a((Object) r52, "binding.ftvDayOfWeek");
                    r52.setText(yk4.b.b(i2));
                    return;
                }
                ImageView imageView4 = a2.F;
                wg6.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                Object r53 = a2.w;
                wg6.a((Object) r53, "binding.ftvDayOfWeek");
                r53.setText(yk4.b.b(i2));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i2, List<jz5> list, List<pc6<Integer, lc6<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        wg6.b(list, "listTodayHeartRateModel");
        wg6.b(list2, "listTimeZoneChange");
        ax5<ha4> ax5 = this.f;
        if (ax5 != null) {
            ha4 a2 = ax5.a();
            if (a2 != null && (todayHeartRateChart = a2.u) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i2);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.a(list);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(boolean z, zh4 zh4, cf<WorkoutSession> cfVar) {
        wg6.b(zh4, MFUser.DISTANCE_UNIT);
        wg6.b(cfVar, "workoutSessions");
        ax5<ha4> ax5 = this.f;
        if (ax5 != null) {
            ha4 a2 = ax5.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.G;
                    wg6.a((Object) linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!cfVar.isEmpty()) {
                        Object r3 = a2.A;
                        wg6.a((Object) r3, "it.ftvNoWorkoutRecorded");
                        r3.setVisibility(8);
                        RecyclerView recyclerView = a2.I;
                        wg6.a((Object) recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        sh5 sh5 = this.i;
                        if (sh5 != null) {
                            sh5.a(zh4, cfVar);
                        }
                    } else {
                        Object r32 = a2.A;
                        wg6.a((Object) r32, "it.ftvNoWorkoutRecorded");
                        r32.setVisibility(0);
                        RecyclerView recyclerView2 = a2.I;
                        wg6.a((Object) recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        sh5 sh52 = this.i;
                        if (sh52 != null) {
                            sh52.a(zh4, cfVar);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.G;
                    wg6.a((Object) linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                wg6.a((Object) appBarLayout, "it.appBarLayout");
                CoordinatorLayout.e layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = layoutParams;
                    AppBarLayout.Behavior d = eVar.d();
                    if (d == null) {
                        d = new AppBarLayout.Behavior();
                    }
                    d.setDragCallback(new b(this, z, cfVar, zh4));
                    eVar.a(d);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
