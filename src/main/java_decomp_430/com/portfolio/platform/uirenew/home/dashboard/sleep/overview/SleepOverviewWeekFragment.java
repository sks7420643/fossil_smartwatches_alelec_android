package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.he4;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.ug5;
import com.fossil.ut4;
import com.fossil.vg5;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewWeekFragment extends BaseFragment implements vg5 {
    @DexIgnore
    public ax5<he4> f;
    @DexIgnore
    public ug5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "SleepOverviewWeekFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        he4 a2;
        OverviewSleepWeekChart overviewSleepWeekChart;
        ax5<he4> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewSleepWeekChart = a2.q) != null) {
            ug5 ug5 = this.g;
            if ((ug5 != null ? ug5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewSleepWeekChart.a("dianaSleepTab", "nonBrandNonReachGoal");
            } else {
                overviewSleepWeekChart.a("hybridSleepTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        he4 a2;
        wg6.b(layoutInflater, "inflater");
        SleepOverviewWeekFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onCreateView");
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558614, viewGroup, false, e1()));
        j1();
        ax5<he4> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        SleepOverviewWeekFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onResume");
        j1();
        ug5 ug5 = this.g;
        if (ug5 != null) {
            ug5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        SleepOverviewWeekFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onStop");
        ug5 ug5 = this.g;
        if (ug5 != null) {
            ug5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(ut4 ut4) {
        he4 a2;
        OverviewSleepWeekChart overviewSleepWeekChart;
        wg6.b(ut4, "baseModel");
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "showWeekDetails");
        ax5<he4> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewSleepWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            ut4.a aVar = ut4.a;
            wg6.a((Object) overviewSleepWeekChart, "it");
            Context context = overviewSleepWeekChart.getContext();
            wg6.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewSleepWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewSleepWeekChart.a(ut4);
        }
    }

    @DexIgnore
    public void a(ug5 ug5) {
        wg6.b(ug5, "presenter");
        this.g = ug5;
    }
}
