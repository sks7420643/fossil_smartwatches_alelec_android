package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.d7;
import com.fossil.fc4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.mv4;
import com.fossil.qg6;
import com.fossil.r25;
import com.fossil.rc6;
import com.fossil.s25;
import com.fossil.wg6;
import com.fossil.wx4;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridContactFragment extends BaseFragment implements s25, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public r25 f;
    @DexIgnore
    public ax5<fc4> g;
    @DexIgnore
    public mv4 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridContactFragment.j;
        }

        @DexIgnore
        public final NotificationHybridContactFragment b() {
            return new NotificationHybridContactFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements mv4.b {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridContactFragment a;

        @DexIgnore
        public b(NotificationHybridContactFragment notificationHybridContactFragment) {
            this.a = notificationHybridContactFragment;
        }

        @DexIgnore
        public void a(wx4 wx4) {
            wg6.b(wx4, "contactWrapper");
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, wx4, wx4.getCurrentHandGroup(), NotificationHybridContactFragment.b(this.a).h());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridContactFragment a;

        @DexIgnore
        public c(NotificationHybridContactFragment notificationHybridContactFragment) {
            this.a = notificationHybridContactFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationHybridContactFragment.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridContactFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ fc4 b;

        @DexIgnore
        public d(NotificationHybridContactFragment notificationHybridContactFragment, fc4 fc4) {
            this.a = notificationHybridContactFragment;
            this.b = fc4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Object r2 = this.b.s;
            wg6.a((Object) r2, "binding.ivClear");
            r2.setVisibility(i3 > 0 ? 0 : 4);
            mv4 a2 = this.a.h;
            if (a2 != null) {
                a2.a(String.valueOf(charSequence));
            }
            AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = this.b.u;
            wg6.a((Object) alphabetFastScrollRecyclerView, "binding.rvContacts");
            LinearLayoutManager layoutManager = alphabetFastScrollRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                layoutManager.f(0, 0);
                return;
            }
            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ fc4 a;

        @DexIgnore
        public e(fc4 fc4) {
            this.a = fc4;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                fc4 fc4 = this.a;
                wg6.a((Object) fc4, "binding");
                fc4.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fc4 a;

        @DexIgnore
        public f(fc4 fc4) {
            this.a = fc4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = NotificationHybridContactFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationHybridContac\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ r25 b(NotificationHybridContactFragment notificationHybridContactFragment) {
        r25 r25 = notificationHybridContactFragment.f;
        if (r25 != null) {
            return r25;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void F0() {
        mv4 mv4 = this.h;
        if (mv4 != null) {
            mv4.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void J() {
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        r25 r25 = this.f;
        if (r25 != null) {
            r25.i();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        fc4 a2 = kb.a(layoutInflater, 2131558583, viewGroup, false, e1());
        a2.r.setOnClickListener(new c(this));
        a2.q.addTextChangedListener(new d(this, a2));
        a2.q.setOnFocusChangeListener(new e(a2));
        a2.s.setOnClickListener(new f(a2));
        a2.u.setIndexBarVisibility(true);
        a2.u.setIndexBarHighLateTextVisibility(true);
        a2.u.setIndexbarHighLateTextColor(2131099843);
        a2.u.setIndexBarTransparentValue(0.0f);
        a2.u.setIndexTextSize(9);
        Context context = getContext();
        if (context != null) {
            Typeface a3 = d7.a(context, 2131296257);
            String b2 = ThemeManager.l.a().b("primaryText");
            Typeface c2 = ThemeManager.l.a().c("nonBrandTextStyle5");
            if (c2 != null) {
                a3 = c2;
            }
            if (!TextUtils.isEmpty(b2)) {
                int parseColor = Color.parseColor(b2);
                a2.u.setIndexBarTextColor(parseColor);
                a2.u.setIndexbarHighLateTextColor(parseColor);
            }
            if (a3 != null) {
                a2.u.setTypeface(a3);
            }
            RecyclerView recyclerView = a2.u;
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            recyclerView.setAdapter(this.h);
            this.g = new ax5<>(this, a2);
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        r25 r25 = this.f;
        if (r25 != null) {
            r25.g();
            NotificationHybridContactFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationHybridContactFragment.super.onResume();
        r25 r25 = this.f;
        if (r25 != null) {
            r25.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(r25 r25) {
        wg6.b(r25, "presenter");
        this.f = r25;
    }

    @DexIgnore
    public void a(List<wx4> list, FilterQueryProvider filterQueryProvider, int i2) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        wg6.b(list, "listContactWrapper");
        wg6.b(filterQueryProvider, "filterQueryProvider");
        mv4 mv4 = new mv4((Cursor) null, list, i2);
        mv4.a((mv4.b) new b(this));
        this.h = mv4;
        mv4 mv42 = this.h;
        if (mv42 != null) {
            mv42.a(filterQueryProvider);
            ax5<fc4> ax5 = this.g;
            if (ax5 != null) {
                fc4 a2 = ax5.a();
                if (a2 != null && (alphabetFastScrollRecyclerView = a2.u) != null) {
                    alphabetFastScrollRecyclerView.setAdapter(this.h);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(ArrayList<wx4> arrayList) {
        wg6.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(Cursor cursor) {
        mv4 mv4 = this.h;
        if (mv4 != null) {
            mv4.c(cursor);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wx4 wx4;
        wg6.b(str, "tag");
        if (str.hashCode() != 1018078562 || !str.equals("CONFIRM_REASSIGN_CONTACT")) {
            return;
        }
        if (i2 != 2131363190) {
            mv4 mv4 = this.h;
            if (mv4 != null) {
                mv4.notifyDataSetChanged();
            } else {
                wg6.a();
                throw null;
            }
        } else {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (wx4 = (wx4) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                r25 r25 = this.f;
                if (r25 != null) {
                    r25.a(wx4);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }
}
