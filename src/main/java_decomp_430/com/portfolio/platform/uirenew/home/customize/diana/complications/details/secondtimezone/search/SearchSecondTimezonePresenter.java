package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.a65;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.d65$b$a;
import com.fossil.d65$b$b;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.z55;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezonePresenter extends z55 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public SecondTimezoneSetting e;
    @DexIgnore
    public ArrayList<SecondTimezoneSetting> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ a65 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1", f = "SearchSecondTimezonePresenter.kt", l = {37, 38}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchSecondTimezonePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SearchSecondTimezonePresenter searchSecondTimezonePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchSecondTimezonePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 a2 = this.this$0.b();
                d65$b$a d65_b_a = new d65$b$a((xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, d65_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                ArrayList arrayList = (ArrayList) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.f.addAll((ArrayList) obj);
                this.this$0.h.q(this.this$0.f);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList2 = (ArrayList) obj;
            dl6 a3 = this.this$0.b();
            d65$b$b d65_b_b = new d65$b$b(arrayList2, (xe6) null);
            this.L$0 = il6;
            this.L$1 = arrayList2;
            this.label = 2;
            obj = gk6.a(a3, d65_b_b, this);
            if (obj == a) {
                return a;
            }
            this.this$0.f.addAll((ArrayList) obj);
            this.this$0.h.q(this.this$0.f);
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = SearchSecondTimezonePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "SearchSecondTimezonePres\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SearchSecondTimezonePresenter(a65 a65) {
        wg6.b(a65, "mView");
        this.h = a65;
    }

    @DexIgnore
    public void f() {
        if (this.f.isEmpty()) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
        } else {
            this.h.q(this.f);
        }
        SecondTimezoneSetting secondTimezoneSetting = this.e;
        if (secondTimezoneSetting != null) {
            this.h.K(secondTimezoneSetting.getTimeZoneName());
        }
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        try {
            this.e = (SecondTimezoneSetting) this.g.a(str, SecondTimezoneSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse second timezone setting " + e2);
        }
    }
}
