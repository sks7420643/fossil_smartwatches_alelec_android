package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.ff5;
import com.fossil.fi4;
import com.fossil.gf5;
import com.fossil.jm4;
import com.fossil.jz5;
import com.fossil.kb;
import com.fossil.la4;
import com.fossil.lc6;
import com.fossil.pc6;
import com.fossil.qg6;
import com.fossil.rg4;
import com.fossil.rh6;
import com.fossil.wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewDayFragment extends BaseFragment implements gf5 {
    @DexIgnore
    public ax5<la4> f;
    @DexIgnore
    public ff5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
            Date date = new Date();
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "HeartRateOverviewDayFragment";
    }

    @DexIgnore
    public final void j1() {
        la4 a2;
        TodayHeartRateChart todayHeartRateChart;
        ax5<la4> ax5 = this.f;
        if (ax5 == null) {
            wg6.d("mBinding");
            throw null;
        } else if (ax5 != null && (a2 = ax5.a()) != null && (todayHeartRateChart = a2.u) != null) {
            todayHeartRateChart.a("maxHeartRate", "lowestHeartRate");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HeartRateOverviewDayFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewDayFragment", "onCreateView");
        la4 a2 = kb.a(layoutInflater, 2131558559, viewGroup, false, e1());
        a2.s.setOnClickListener(b.a);
        this.f = new ax5<>(this, a2);
        j1();
        ax5<la4> ax5 = this.f;
        if (ax5 != null) {
            la4 a3 = ax5.a();
            if (a3 != null) {
                return a3.d();
            }
            return null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ff5 ff5 = this.g;
        if (ff5 != null) {
            ff5.g();
            HeartRateOverviewDayFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        HeartRateOverviewDayFragment.super.onResume();
        j1();
        ff5 ff5 = this.g;
        if (ff5 != null) {
            ff5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ff5 ff5) {
        wg6.b(ff5, "presenter");
        this.g = ff5;
    }

    @DexIgnore
    public void a(int i, List<jz5> list, List<pc6<Integer, lc6<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        wg6.b(list, "listTodayHeartRateModel");
        wg6.b(list2, "listTimeZoneChange");
        ax5<la4> ax5 = this.f;
        if (ax5 != null) {
            la4 a2 = ax5.a();
            if (a2 != null && (todayHeartRateChart = a2.u) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.a(list);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        View d;
        View d2;
        wg6.b(list, "workoutSessions");
        ax5<la4> ax5 = this.f;
        if (ax5 != null) {
            la4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.t;
                wg6.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.q, list.get(0));
                if (size == 1) {
                    rg4 rg4 = a2.r;
                    if (rg4 != null && (d2 = rg4.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                rg4 rg42 = a2.r;
                if (!(rg42 == null || (d = rg42.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.r, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.t;
            wg6.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void a(rg4 rg4, WorkoutSession workoutSession) {
        if (rg4 != null) {
            View d = rg4.d();
            wg6.a((Object) d, "binding.root");
            Context context = d.getContext();
            lc6<Integer, Integer> a2 = fi4.Companion.a(workoutSession.getWorkoutType());
            String a3 = jm4.a(context, a2.getSecond().intValue());
            rg4.u.setImageResource(a2.getFirst().intValue());
            Object r1 = rg4.t;
            wg6.a((Object) r1, "it.ftvWorkoutTitle");
            r1.setText(a3);
            Object r0 = rg4.r;
            wg6.a((Object) r0, "it.ftvRestingValue");
            WorkoutHeartRate heartRate = workoutSession.getHeartRate();
            int i = 0;
            r0.setText(String.valueOf(heartRate != null ? rh6.a(heartRate.getAverage()) : 0));
            Object r02 = rg4.q;
            wg6.a((Object) r02, "it.ftvMaxValue");
            WorkoutHeartRate heartRate2 = workoutSession.getHeartRate();
            if (heartRate2 != null) {
                i = heartRate2.getMax();
            }
            r02.setText(String.valueOf(i));
            Object r03 = rg4.s;
            wg6.a((Object) r03, "it.ftvWorkoutTime");
            r03.setText(bk4.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            String b2 = ThemeManager.l.a().b("dianaHeartRateTab");
            if (b2 != null) {
                rg4.u.setColorFilter(Color.parseColor(b2));
            }
            String b3 = ThemeManager.l.a().b("nonBrandLineColor");
            if (b3 != null) {
                rg4.v.setBackgroundColor(Color.parseColor(b3));
            }
        }
    }
}
