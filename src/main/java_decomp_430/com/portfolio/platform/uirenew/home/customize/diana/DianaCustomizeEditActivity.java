package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.DianaCustomizeEditPresenter;
import com.fossil.b45;
import com.fossil.du3;
import com.fossil.h6;
import com.fossil.i6;
import com.fossil.o35;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.u8;
import com.fossil.vd;
import com.fossil.vq4;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.y04;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a E; // = new a((qg6) null);
    @DexIgnore
    public DianaCustomizeEditPresenter B;
    @DexIgnore
    public w04 C;
    @DexIgnore
    public DianaCustomizeViewModel D;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, int i, String str2, String str3) {
            wg6.b(context, "context");
            wg6.b(str, "presetId");
            wg6.b(str2, "complicationPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "start - presetId=" + str + ", complicationPos=" + str2 + ", watchAppPos=" + str3);
            Intent intent = new Intent(context, DianaCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_CUSTOMIZE_TAB", i);
            intent.putExtra("KEY_PRESET_COMPLICATION_POS_SELECTED", str2);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v5, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<u8<View, String>> arrayList, List<? extends u8<CustomizeWidget, String>> list, CopyOnWriteArrayList<CustomizeRealData> copyOnWriteArrayList, int i, String str2, String str3) {
            wg6.b(fragmentActivity, "context");
            wg6.b(str, "presetId");
            wg6.b(arrayList, "views");
            wg6.b(list, "customizeWidgetViews");
            wg6.b(copyOnWriteArrayList, "customizeRealDataList");
            wg6.b(str2, "complicationPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", complicationPos=" + str2 + ", watchAppPos=" + str3);
            Intent intent = new Intent(fragmentActivity, DianaCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_CUSTOMIZE_TAB", i);
            intent.putExtra("KEY_PRESET_COMPLICATION_POS_SELECTED", str2);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
            intent.putExtra("KEY_CUSTOMIZE_REAL_DATA_LIST", copyOnWriteArrayList);
            for (u8 next : list) {
                arrayList.add(new u8(next.a, next.b));
                Bundle bundle = new Bundle();
                vq4.a aVar = vq4.b;
                Object obj = next.a;
                if (obj != null) {
                    wg6.a(obj, "wcPair.first!!");
                    aVar.a((CustomizeWidget) obj, bundle);
                    Object obj2 = next.a;
                    if (obj2 != null) {
                        wg6.a(obj2, "wcPair.first!!");
                        intent.putExtra(((CustomizeWidget) obj2).getTransitionName(), bundle);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new u8[0]);
            if (array != null) {
                u8[] u8VarArr = (u8[]) array;
                i6 a = i6.a(fragmentActivity, (u8[]) Arrays.copyOf(u8VarArr, u8VarArr.length));
                wg6.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                h6.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        DianaCustomizeEditActivity.super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362119);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r19v0, types: [com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x017f  */
    public void onCreate(Bundle bundle) {
        DianaPreset dianaPreset;
        DianaPreset dianaPreset2;
        DianaCustomizeViewModel dianaCustomizeViewModel;
        DianaPreset dianaPreset3;
        Bundle bundle2 = bundle;
        Class<DianaPreset> cls = DianaPreset.class;
        super.onCreate(bundle);
        setContentView(2131558428);
        int intExtra = getIntent().getIntExtra("KEY_CUSTOMIZE_TAB", 1);
        String stringExtra = getIntent().getStringExtra("KEY_PRESET_ID");
        String stringExtra2 = getIntent().getStringExtra("KEY_PRESET_COMPLICATION_POS_SELECTED");
        String stringExtra3 = getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
        DianaCustomizeEditFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = new DianaCustomizeEditFragment();
            a((Fragment) b, "DianaCustomizeEditFragment", 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        wg6.a((Object) stringExtra, "presetId");
        String str = "KEY_PRESET_WATCH_APP_POS_SELECTED";
        b45 b45 = r8;
        String str2 = "KEY_PRESET_COMPLICATION_POS_SELECTED";
        String str3 = stringExtra3;
        b45 b452 = new b45(b, stringExtra, intExtra, stringExtra2, stringExtra3);
        g.a(b45).a(this);
        if (!(bundle2 == null || intExtra == -1)) {
            DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.B;
            if (dianaCustomizeEditPresenter != null) {
                dianaCustomizeEditPresenter.b(intExtra);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("KEY_CUSTOMIZE_REAL_DATA_LIST")) {
            DianaCustomizeEditPresenter dianaCustomizeEditPresenter2 = this.B;
            if (dianaCustomizeEditPresenter2 != null) {
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("KEY_CUSTOMIZE_REAL_DATA_LIST");
                wg6.a((Object) parcelableArrayListExtra, "it.getParcelableArrayLis\u2026CUSTOMIZE_REAL_DATA_LIST)");
                dianaCustomizeEditPresenter2.a((ArrayList<CustomizeRealData>) parcelableArrayListExtra);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        w04 w04 = this.C;
        if (w04 != null) {
            DianaCustomizeViewModel a2 = vd.a(this, w04).a(DianaCustomizeViewModel.class);
            wg6.a((Object) a2, "ViewModelProviders.of(th\u2026izeViewModel::class.java)");
            this.D = a2;
            if (bundle2 == null) {
                FLogger.INSTANCE.getLocal().d(f(), "init from initialize state");
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.D;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.a(stringExtra, stringExtra2, str3);
                } else {
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
            } else {
                String str4 = str3;
                FLogger.INSTANCE.getLocal().d(f(), "init from savedInstanceState");
                du3 du3 = new du3();
                du3.a(cls, new DianaPresetDeserializer());
                Gson a3 = du3.a();
                try {
                    dianaPreset3 = bundle2.containsKey("KEY_CURRENT_PRESET") ? (DianaPreset) a3.a(bundle2.getString("KEY_CURRENT_PRESET"), cls) : null;
                    try {
                        if (bundle2.containsKey("KEY_ORIGINAL_PRESET")) {
                            dianaPreset2 = (DianaPreset) a3.a(bundle2.getString("KEY_ORIGINAL_PRESET"), cls);
                            dianaPreset = dianaPreset3;
                            String str5 = str2;
                            String string = !bundle2.containsKey(str5) ? bundle2.getString(str5) : stringExtra2;
                            String str6 = str;
                            String string2 = !bundle2.containsKey(str6) ? bundle2.getString(str6) : str4;
                            dianaCustomizeViewModel = this.D;
                            if (dianaCustomizeViewModel == null) {
                                dianaCustomizeViewModel.a(stringExtra, dianaPreset2, dianaPreset, string, string2);
                                return;
                            } else {
                                wg6.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                        }
                    } catch (Exception e) {
                        e = e;
                        FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                        dianaPreset = dianaPreset3;
                        dianaPreset2 = null;
                        String str52 = str2;
                        if (!bundle2.containsKey(str52)) {
                        }
                        String str62 = str;
                        if (!bundle2.containsKey(str62)) {
                        }
                        dianaCustomizeViewModel = this.D;
                        if (dianaCustomizeViewModel == null) {
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    dianaPreset3 = null;
                    FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                    dianaPreset = dianaPreset3;
                    dianaPreset2 = null;
                    String str522 = str2;
                    if (!bundle2.containsKey(str522)) {
                    }
                    String str622 = str;
                    if (!bundle2.containsKey(str622)) {
                    }
                    dianaCustomizeViewModel = this.D;
                    if (dianaCustomizeViewModel == null) {
                    }
                }
                dianaPreset = dianaPreset3;
                dianaPreset2 = null;
                String str5222 = str2;
                if (!bundle2.containsKey(str5222)) {
                }
                String str6222 = str;
                if (!bundle2.containsKey(str6222)) {
                }
                dianaCustomizeViewModel = this.D;
                if (dianaCustomizeViewModel == null) {
                }
            }
        } else {
            wg6.d("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.B;
        if (dianaCustomizeEditPresenter != null) {
            dianaCustomizeEditPresenter.a(bundle);
            if (bundle != null) {
                DianaCustomizeEditActivity.super.onSaveInstanceState(bundle);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final ArrayList<o35> s() {
        DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.B;
        if (dianaCustomizeEditPresenter == null) {
            return null;
        }
        if (dianaCustomizeEditPresenter != null) {
            return dianaCustomizeEditPresenter.j();
        }
        wg6.d("mPresenter");
        throw null;
    }
}
