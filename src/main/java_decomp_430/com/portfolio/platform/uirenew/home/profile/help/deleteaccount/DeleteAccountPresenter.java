package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import androidx.fragment.app.FragmentActivity;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ht4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll5;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.ml5;
import com.fossil.nc6;
import com.fossil.pl5$c$a;
import com.fossil.pl5$d$a;
import com.fossil.pl5$e$a;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.ws4;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAccountPresenter extends ll5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ml5 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ AnalyticsHelper g;
    @DexIgnore
    public /* final */ ws4 h;
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteAccountPresenter.j;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<ht4.d, ht4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter a;

        @DexIgnore
        public b(DeleteAccountPresenter deleteAccountPresenter) {
            this.a = deleteAccountPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "deleteUser - onSuccess");
            this.a.e.d();
            if (((DeleteAccountFragment) this.a.e).isActive()) {
                this.a.e.T0();
            }
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "deleteUser - onError");
            this.a.e.d();
            this.a.e.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1", f = "DeleteAccountPresenter.kt", l = {107}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DeleteAccountPresenter deleteAccountPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteAccountPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                pl5$c$a pl5_c_a = new pl5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, pl5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                this.this$0.g.a("feedback_submit", (Map<String, ? extends Object>) hashMap);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1", f = "DeleteAccountPresenter.kt", l = {92}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DeleteAccountPresenter deleteAccountPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteAccountPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                pl5$d$a pl5_d_a = new pl5$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, pl5_d_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.g.a("feedback_open", (Map<String, ? extends Object>) hashMap);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<ws4.d, ws4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter a;

        @DexIgnore
        public e(DeleteAccountPresenter deleteAccountPresenter) {
            this.a = deleteAccountPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ws4.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "sendFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
            this.a.e.b(new pl5$e$a(dVar));
        }

        @DexIgnore
        public void a(ws4.b bVar) {
            wg6.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "sendFeedback onError");
        }
    }

    /*
    static {
        String simpleName = DeleteAccountPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "DeleteAccountPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public DeleteAccountPresenter(ml5 ml5, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, ws4 ws4, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        wg6.b(ml5, "mView");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(analyticsHelper, "mAnalyticsHelper");
        wg6.b(ws4, "mGetZendeskInformation");
        wg6.b(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        this.e = ml5;
        this.f = deviceRepository;
        this.g = analyticsHelper;
        this.h = ws4;
        this.i = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.e.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$b, com.portfolio.platform.CoroutineUseCase$e] */
    public void a(MFUser mFUser) {
        wg6.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d(j, "deleteUser");
        this.e.e();
        Object r4 = this.i;
        ml5 ml5 = this.e;
        if (ml5 != null) {
            FragmentActivity activity = ((DeleteAccountFragment) ml5).getActivity();
            if (activity != null) {
                r4.a(new DeleteLogoutUserUseCase.b(0, new WeakReference(activity)), new b(this));
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.app.Activity");
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.ws4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$e] */
    public void a(String str) {
        wg6.b(str, "subject");
        this.h.a(new ws4.c(str), new e(this));
    }
}
