package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cx5;
import com.fossil.dl6;
import com.fossil.dt4;
import com.fossil.ff6;
import com.fossil.ft4;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.lk4;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mk5;
import com.fossil.mk5$d$a;
import com.fossil.nc6;
import com.fossil.nk5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rh4;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yj6;
import com.fossil.zh4;
import com.fossil.zj4;
import com.fossil.zl6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.ReadablePartial;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileEditViewModel extends td {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((qg6) null);
    @DexIgnore
    public MFUser a;
    @DexIgnore
    public MFUser b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public MutableLiveData<mk5.b> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UpdateUser i;
    @DexIgnore
    public /* final */ dt4 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ProfileEditViewModel.k;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public MFUser a;
        @DexIgnore
        public Uri b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public lc6<Integer, String> d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public MFUser f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public Bundle h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;

        @DexIgnore
        public b(MFUser mFUser, Uri uri, Boolean bool, lc6<Integer, String> lc6, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2) {
            this.a = mFUser;
            this.b = uri;
            this.c = bool;
            this.d = lc6;
            this.e = z;
            this.f = mFUser2;
            this.g = z2;
            this.h = bundle;
            this.i = str;
            this.j = str2;
        }

        @DexIgnore
        public final String a() {
            return this.i;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final Bundle c() {
            return this.h;
        }

        @DexIgnore
        public final Uri d() {
            return this.b;
        }

        @DexIgnore
        public final boolean e() {
            return this.g;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && wg6.a((Object) this.b, (Object) bVar.b) && wg6.a((Object) this.c, (Object) bVar.c) && wg6.a((Object) this.d, (Object) bVar.d) && this.e == bVar.e && wg6.a((Object) this.f, (Object) bVar.f) && this.g == bVar.g && wg6.a((Object) this.h, (Object) bVar.h) && wg6.a((Object) this.i, (Object) bVar.i) && wg6.a((Object) this.j, (Object) bVar.j);
        }

        @DexIgnore
        public final boolean f() {
            return this.e;
        }

        @DexIgnore
        public final lc6<Integer, String> g() {
            return this.d;
        }

        @DexIgnore
        public final MFUser h() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            MFUser mFUser = this.a;
            int i2 = 0;
            int hashCode = (mFUser != null ? mFUser.hashCode() : 0) * 31;
            Uri uri = this.b;
            int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
            Boolean bool = this.c;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            lc6<Integer, String> lc6 = this.d;
            int hashCode4 = (hashCode3 + (lc6 != null ? lc6.hashCode() : 0)) * 31;
            boolean z = this.e;
            if (z) {
                z = true;
            }
            int i3 = (hashCode4 + (z ? 1 : 0)) * 31;
            MFUser mFUser2 = this.f;
            int hashCode5 = (i3 + (mFUser2 != null ? mFUser2.hashCode() : 0)) * 31;
            boolean z2 = this.g;
            if (z2) {
                z2 = true;
            }
            int i4 = (hashCode5 + (z2 ? 1 : 0)) * 31;
            Bundle bundle = this.h;
            int hashCode6 = (i4 + (bundle != null ? bundle.hashCode() : 0)) * 31;
            String str = this.i;
            int hashCode7 = (hashCode6 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.j;
            if (str2 != null) {
                i2 = str2.hashCode();
            }
            return hashCode7 + i2;
        }

        @DexIgnore
        public final MFUser i() {
            return this.a;
        }

        @DexIgnore
        public final Boolean j() {
            return this.c;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(user=" + this.a + ", imageUri=" + this.b + ", isUserChanged=" + this.c + ", showServerError=" + this.d + ", showProcessImageError=" + this.e + ", showSuccess=" + this.f + ", showLoading=" + this.g + ", dobBundle=" + this.h + ", birthday=" + this.i + ", birthdayErrorMsg=" + this.j + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<dt4.a, m24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel a;

        @DexIgnore
        public c(ProfileEditViewModel profileEditViewModel) {
            this.a = profileEditViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dt4.a aVar) {
            wg6.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside mGetUser onSuccess");
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 959, (Object) null);
            if (aVar.a() != null) {
                this.a.b = aVar.a();
                this.a.a = new MFUser(aVar.a());
                ProfileEditViewModel profileEditViewModel = this.a;
                ProfileEditViewModel.a(profileEditViewModel, profileEditViewModel.b, (Uri) null, (Boolean) null, (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1022, (Object) null);
            } else {
                FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), "loadUserFirstTime user is null");
            }
            this.a.f = true;
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wg6.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside mGetUser onError");
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 959, (Object) null);
            this.a.f = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1", f = "ProfileEditViewModel.kt", l = {197}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $data;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ProfileEditViewModel profileEditViewModel, Intent intent, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileEditViewModel;
            this.$data = intent;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$data, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                Uri a2 = lk4.a(this.$data, (Context) PortfolioApp.get.instance());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = ProfileEditViewModel.l.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .onActivityResult imageUri=");
                if (a2 != null) {
                    sb.append(a2);
                    local.d(a3, sb.toString());
                    if (!PortfolioApp.get.instance().a(this.$data, a2)) {
                        return cd6.a;
                    }
                    ProfileEditViewModel.a(this.this$0, (MFUser) null, a2, (Boolean) null, (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1021, (Object) null);
                    dl6 b = zl6.b();
                    mk5$d$a mk5_d_a = new mk5$d$a(a2, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = a2;
                    this.label = 1;
                    obj2 = gk6.a(b, mk5_d_a, this);
                    if (obj2 == a) {
                        return a;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                Uri uri = (Uri) this.L$1;
                il6 il62 = (il6) this.L$0;
                try {
                    nc6.a(obj);
                    obj2 = obj;
                } catch (Exception e) {
                    e.printStackTrace();
                    ProfileEditViewModel.a(this.this$0, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, true, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1007, (Object) null);
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Bitmap bitmap = (Bitmap) obj2;
            MFUser a4 = this.this$0.b;
            if (a4 != null) {
                if (bitmap != null) {
                    a4.setProfilePicture(cx5.a(bitmap));
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.this$0.c = true;
            ProfileEditViewModel.a(this.this$0, (MFUser) null, (Uri) null, hf6.a(this.this$0.c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1019, (Object) null);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel a;

        @DexIgnore
        public e(ProfileEditViewModel profileEditViewModel) {
            this.a = profileEditViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside updateUser onSuccess");
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, false, dVar.a(), false, (Bundle) null, (String) null, (String) null, 927, (Object) null);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ProfileEditViewModel.l.a();
            local.d(a2, ".Inside updateUser onError, errorCode=" + cVar.a());
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, new lc6(Integer.valueOf(cVar.a()), ""), false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 951, (Object) null);
        }
    }

    /*
    static {
        String simpleName = ProfileEditViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "ProfileEditViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public ProfileEditViewModel(UpdateUser updateUser, dt4 dt4) {
        wg6.b(updateUser, "mUpdateUser");
        wg6.b(dt4, "mGetUser");
        this.i = updateUser;
        this.j = dt4;
    }

    @DexIgnore
    public final boolean c() {
        MFUser mFUser = this.a;
        if (mFUser == null || this.b == null) {
            return false;
        }
        if (mFUser != null) {
            String firstName = mFUser.getFirstName();
            wg6.a((Object) firstName, "mDefaultUser!!.firstName");
            if (firstName != null) {
                String obj = yj6.d(firstName).toString();
                MFUser mFUser2 = this.b;
                if (mFUser2 != null) {
                    if (!(!wg6.a((Object) obj, (Object) mFUser2.getFirstName()))) {
                        MFUser mFUser3 = this.a;
                        if (mFUser3 != null) {
                            String lastName = mFUser3.getLastName();
                            wg6.a((Object) lastName, "mDefaultUser!!.lastName");
                            if (lastName != null) {
                                String obj2 = yj6.d(lastName).toString();
                                MFUser mFUser4 = this.b;
                                if (mFUser4 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (!(!wg6.a((Object) obj2, (Object) mFUser4.getLastName())) && !b()) {
                                    MFUser mFUser5 = this.a;
                                    if (mFUser5 != null) {
                                        String rh4 = mFUser5.getGender().toString();
                                        MFUser mFUser6 = this.b;
                                        if (mFUser6 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (!(!wg6.a((Object) rh4, (Object) mFUser6.getGender().toString())) && !this.c) {
                                            MFUser mFUser7 = this.a;
                                            if (mFUser7 != null) {
                                                String birthday = mFUser7.getBirthday();
                                                MFUser mFUser8 = this.b;
                                                if (mFUser8 == null) {
                                                    wg6.a();
                                                    throw null;
                                                } else if (!(!wg6.a((Object) birthday, (Object) mFUser8.getBirthday()))) {
                                                    return false;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            } else {
                                throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    if (this.g) {
                        return true;
                    }
                    return false;
                }
                wg6.a();
                throw null;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.dt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$c] */
    public final void d() {
        if (!this.f) {
            a(this, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, false, (MFUser) null, true, (Bundle) null, (String) null, (String) null, 959, (Object) null);
            this.j.a(null, new c(this));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        r9 = r0.getBirthday();
     */
    @DexIgnore
    public final void e() {
        List list;
        String birthday;
        Bundle bundle = new Bundle();
        String str = this.d;
        if (str == null || (list = yj6.a((CharSequence) str, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, (Object) null)) == null) {
            MFUser mFUser = this.b;
            list = (mFUser == null || birthday == null) ? null : yj6.a((CharSequence) birthday, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, (Object) null);
        }
        FLogger.INSTANCE.getLocal().d(k, String.valueOf(list));
        if (list != null) {
            bundle.putInt("DAY", Integer.parseInt((String) list.get(2)));
            bundle.putInt("MONTH", Integer.parseInt((String) list.get(1)));
            bundle.putInt("YEAR", Integer.parseInt((String) list.get(0)));
        }
        a(this, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, false, (MFUser) null, false, bundle, (String) null, (String) null, 895, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$e] */
    public final void f() {
        if (this.b != null) {
            a(this, (MFUser) null, (Uri) null, (Boolean) null, (lc6) null, false, (MFUser) null, true, (Bundle) null, (String) null, (String) null, 959, (Object) null);
            MFUser mFUser = this.b;
            if (mFUser != null) {
                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                    MFUser mFUser2 = this.b;
                    if (mFUser2 != null) {
                        String profilePicture = mFUser2.getProfilePicture();
                        wg6.a((Object) profilePicture, "mUser!!.profilePicture");
                        if (yj6.a((CharSequence) profilePicture, (CharSequence) "https", false, 2, (Object) null)) {
                            MFUser mFUser3 = this.b;
                            if (mFUser3 != null) {
                                mFUser3.setProfilePicture("");
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                MFUser mFUser4 = this.b;
                if (mFUser4 != null) {
                    if (mFUser4.isUseDefaultBiometric() && this.e) {
                        MFUser mFUser5 = this.b;
                        if (mFUser5 != null) {
                            mFUser5.setUseDefaultBiometric(false);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    Object r0 = this.i;
                    MFUser mFUser6 = this.b;
                    if (mFUser6 != null) {
                        r0.a(new UpdateUser.b(mFUser6), new e(this));
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b3, code lost:
        if (r5 != (r0 + r4.intValue())) goto L_0x00d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d2, code lost:
        if (r0 != r4.getHeightInCentimeters()) goto L_0x00d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d6, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0115, code lost:
        if (java.lang.Math.round(r4 * r5) != java.lang.Math.round(com.fossil.zj4.f((float) r6.getWeightInGrams()) * r5)) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0119, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0146, code lost:
        if (java.lang.Math.round(r4 * r5) != java.lang.Math.round((((float) r7.getWeightInGrams()) / 1000.0f) * r5)) goto L_0x0117;
     */
    @DexIgnore
    public final boolean b() {
        MFUser mFUser = this.a;
        if (mFUser == null || this.b == null) {
            return false;
        }
        if (mFUser != null) {
            zh4 heightUnit = mFUser.getHeightUnit();
            if (heightUnit != null && nk5.a[heightUnit.ordinal()] == 1) {
                MFUser mFUser2 = this.a;
                if (mFUser2 != null) {
                    int heightInCentimeters = mFUser2.getHeightInCentimeters();
                    MFUser mFUser3 = this.b;
                    if (mFUser3 == null) {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("isUserChangeSomething ");
                MFUser mFUser4 = this.a;
                if (mFUser4 != null) {
                    sb.append((float) mFUser4.getHeightInCentimeters());
                    printStream.println(sb.toString());
                    MFUser mFUser5 = this.a;
                    if (mFUser5 != null) {
                        lc6<Integer, Integer> b2 = zj4.b((float) mFUser5.getHeightInCentimeters());
                        MFUser mFUser6 = this.b;
                        if (mFUser6 != null) {
                            lc6<Integer, Integer> b3 = zj4.b((float) mFUser6.getHeightInCentimeters());
                            PrintStream printStream2 = System.out;
                            printStream2.println("isUserChangeSomething " + b2);
                            Integer first = b2.getFirst();
                            wg6.a((Object) first, "defaultHeightInFeetAndInches.first");
                            int a2 = zj4.a(first.intValue());
                            Integer second = b2.getSecond();
                            wg6.a((Object) second, "defaultHeightInFeetAndInches.second");
                            int intValue = a2 + second.intValue();
                            Integer first2 = b3.getFirst();
                            wg6.a((Object) first2, "currentHeightInFeetAndInches.first");
                            int a3 = zj4.a(first2.intValue());
                            Integer second2 = b3.getSecond();
                            wg6.a((Object) second2, "currentHeightInFeetAndInches.second");
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            boolean z = true;
            MFUser mFUser7 = this.a;
            if (mFUser7 != null) {
                zh4 weightUnit = mFUser7.getWeightUnit();
                if (weightUnit != null && nk5.b[weightUnit.ordinal()] == 1) {
                    MFUser mFUser8 = this.a;
                    if (mFUser8 != null) {
                        float weightInGrams = ((float) mFUser8.getWeightInGrams()) / 1000.0f;
                        MFUser mFUser9 = this.b;
                        if (mFUser9 != null) {
                            float f2 = (float) 10;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    MFUser mFUser10 = this.a;
                    if (mFUser10 != null) {
                        float f3 = zj4.f((float) mFUser10.getWeightInGrams());
                        MFUser mFUser11 = this.b;
                        if (mFUser11 != null) {
                            float f4 = (float) 10;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                boolean z2 = true;
                if (z || z2) {
                    return true;
                }
                return false;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<mk5.b> a() {
        return this.h;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setFirstName(str);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1019, (Object) null);
    }

    @DexIgnore
    public final void a(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1019, (Object) null);
    }

    @DexIgnore
    public final void a(rh4 rh4) {
        wg6.b(rh4, "gender");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setGender(rh4.toString());
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1019, (Object) null);
    }

    @DexIgnore
    public final void a(Intent intent) {
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new d(this, intent, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ void a(ProfileEditViewModel profileEditViewModel, MFUser mFUser, Uri uri, Boolean bool, lc6 lc6, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            mFUser = null;
        }
        if ((i2 & 2) != 0) {
            uri = null;
        }
        if ((i2 & 4) != 0) {
            bool = null;
        }
        if ((i2 & 8) != 0) {
            lc6 = null;
        }
        if ((i2 & 16) != 0) {
            z = false;
        }
        if ((i2 & 32) != 0) {
            mFUser2 = null;
        }
        if ((i2 & 64) != 0) {
            z2 = false;
        }
        if ((i2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0) {
            bundle = null;
        }
        if ((i2 & 256) != 0) {
            str = null;
        }
        if ((i2 & 512) != 0) {
            str2 = null;
        }
        profileEditViewModel.a(mFUser, uri, bool, lc6, z, mFUser2, z2, bundle, str, str2);
    }

    @DexIgnore
    public final void a(MFUser mFUser, Uri uri, Boolean bool, lc6<Integer, String> lc6, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2) {
        this.h.a(new b(mFUser, uri, bool, lc6, z, mFUser2, z2, bundle, str, str2));
    }

    @DexIgnore
    public final boolean a(Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        Years yearsBetween = Years.yearsBetween((ReadablePartial) LocalDate.fromCalendarFields(instance), (ReadablePartial) LocalDate.now());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("age=");
        wg6.a((Object) yearsBetween, "age");
        sb.append(yearsBetween.getYears());
        local.d(str, sb.toString());
        return yearsBetween.getYears() >= 16;
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setLastName(str);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1019, (Object) null);
    }

    @DexIgnore
    public final void b(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, (String) null, (String) null, 1019, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r15v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(Date date) {
        wg6.b(date, "birthDay");
        FLogger.INSTANCE.getLocal().d(k, "onBirthDayChanged");
        this.d = bk4.e(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "mBirthday=" + this.d);
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setBirthday(this.d);
        }
        if (!a(date)) {
            this.g = false;
            a(this, (MFUser) null, (Uri) null, false, (lc6) null, false, (MFUser) null, false, (Bundle) null, bk4.d(date), jm4.a((Context) PortfolioApp.get.instance(), 2131886722), 251, (Object) null);
            return;
        }
        this.g = true;
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(c()), (lc6) null, false, (MFUser) null, false, (Bundle) null, bk4.d(date), (String) null, 763, (Object) null);
    }
}
