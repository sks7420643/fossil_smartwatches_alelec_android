package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.gn5;
import com.fossil.hn5;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.ly5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.x74;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeBackgroundFragment extends BaseFragment implements ly5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public CustomizeBackgroundViewModel g;
    @DexIgnore
    public ax5<x74> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CustomizeBackgroundFragment.o;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<hn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeBackgroundFragment a;

        @DexIgnore
        public b(CustomizeBackgroundFragment customizeBackgroundFragment) {
            this.a = customizeBackgroundFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CustomizeBackgroundViewModel.b bVar) {
            if (bVar != null) {
                Integer b = bVar.b();
                if (b != null) {
                    this.a.s(b.intValue());
                }
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.r(a2.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeBackgroundFragment a;

        @DexIgnore
        public c(CustomizeBackgroundFragment customizeBackgroundFragment) {
            this.a = customizeBackgroundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, (int) Action.Presenter.NEXT);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeBackgroundFragment a;

        @DexIgnore
        public d(CustomizeBackgroundFragment customizeBackgroundFragment) {
            this.a = customizeBackgroundFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.g(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = CustomizeBackgroundFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeBackgroundFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363190) {
            CustomizeBackgroundViewModel customizeBackgroundViewModel = this.g;
            if (customizeBackgroundViewModel != null) {
                customizeBackgroundViewModel.a(UserCustomizeThemeFragment.p.a(), o);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf(i3 & 16777215)};
        String format = String.format("#%06X", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        CustomizeBackgroundViewModel customizeBackgroundViewModel = this.g;
        if (customizeBackgroundViewModel != null) {
            customizeBackgroundViewModel.a(i2, Color.parseColor(format));
            if (i2 == 301) {
                o = format;
                return;
            }
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        x74 a2 = kb.a(LayoutInflater.from(getContext()), 2131558526, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new gn5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            CustomizeBackgroundViewModel a3 = vd.a(this, w04).a(CustomizeBackgroundViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026undViewModel::class.java)");
            this.g = a3;
            CustomizeBackgroundViewModel customizeBackgroundViewModel = this.g;
            if (customizeBackgroundViewModel != null) {
                customizeBackgroundViewModel.b().a(getViewLifecycleOwner(), new b(this));
                CustomizeBackgroundViewModel customizeBackgroundViewModel2 = this.g;
                if (customizeBackgroundViewModel2 != null) {
                    customizeBackgroundViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        CustomizeBackgroundFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        o = null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CustomizeBackgroundFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        CustomizeBackgroundViewModel customizeBackgroundViewModel = this.g;
        if (customizeBackgroundViewModel != null) {
            customizeBackgroundViewModel.c();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<x74> ax5 = this.h;
        if (ax5 != null) {
            x74 a2 = ax5.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void r(int i2) {
        ax5<x74> ax5 = this.h;
        if (ax5 != null) {
            x74 a2 = ax5.a();
            if (a2 != null) {
                a2.t.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        ax5<x74> ax5 = this.h;
        if (ax5 != null) {
            x74 a2 = ax5.a();
            if (a2 != null) {
                a2.s.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
