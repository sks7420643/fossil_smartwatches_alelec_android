package com.portfolio.platform.uirenew.home.customize.diana.complications;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.gu4;
import com.fossil.iu4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.n74;
import com.fossil.nh6;
import com.fossil.p45;
import com.fossil.pw6;
import com.fossil.q45;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.s45;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationsFragment extends BaseFragment implements q45, AlertDialogFragment.g {
    @DexIgnore
    public ax5<n74> f;
    @DexIgnore
    public p45 g;
    @DexIgnore
    public gu4 h;
    @DexIgnore
    public iu4 i;
    @DexIgnore
    public w04 j;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iu4.c {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsFragment a;

        @DexIgnore
        public b(ComplicationsFragment complicationsFragment) {
            this.a = complicationsFragment;
        }

        @DexIgnore
        public void a(Complication complication) {
            wg6.b(complication, "complication");
            ComplicationsFragment.a(this.a).b(complication.getComplicationId());
        }

        @DexIgnore
        public void b(Complication complication) {
            wg6.b(complication, "complication");
            this.a.c(complication);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements gu4.c {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsFragment a;

        @DexIgnore
        public c(ComplicationsFragment complicationsFragment) {
            this.a = complicationsFragment;
        }

        @DexIgnore
        public void a() {
            ComplicationsFragment.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            wg6.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "onItemClicked category=" + category);
            ComplicationsFragment.a(this.a).a(category.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsFragment a;

        @DexIgnore
        public d(ComplicationsFragment complicationsFragment) {
            this.a = complicationsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ComplicationsFragment.a(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsFragment a;

        @DexIgnore
        public e(ComplicationsFragment complicationsFragment) {
            this.a = complicationsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ComplicationsFragment.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n74 a;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsFragment b;

        @DexIgnore
        public f(n74 n74, ComplicationsFragment complicationsFragment) {
            this.a = n74;
            this.b = complicationsFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        public final void onClick(View view) {
            p45 a2 = ComplicationsFragment.a(this.b);
            Object r0 = this.a.v;
            wg6.a((Object) r0, "binding.switchRing");
            a2.a(r0.isChecked());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ p45 a(ComplicationsFragment complicationsFragment) {
        p45 p45 = complicationsFragment.g;
        if (p45 != null) {
            return p45;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void D(String str) {
        wg6.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        lx5 lx5 = lx5.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager, "childFragmentManager");
                        lx5.K(childFragmentManager);
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    lx5 lx52 = lx5.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    wg6.a((Object) childFragmentManager2, "childFragmentManager");
                    lx52.J(childFragmentManager2);
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                lx5 lx53 = lx5.c;
                FragmentManager childFragmentManager3 = getChildFragmentManager();
                wg6.a((Object) childFragmentManager3, "childFragmentManager");
                lx53.z(childFragmentManager3);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void F(String str) {
        wg6.b(str, "content");
        ax5<n74> ax5 = this.f;
        if (ax5 != null) {
            n74 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.w;
                wg6.a((Object) r0, "it.tvComplicationDetail");
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(Complication complication) {
        if (complication != null) {
            iu4 iu4 = this.i;
            if (iu4 != null) {
                iu4.b(complication.getComplicationId());
                d(complication);
                return;
            }
            wg6.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(Complication complication) {
        nh6 nh6 = nh6.a;
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886603);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026iresLocationSettingsToBe)");
        Object[] objArr = {jm4.a(PortfolioApp.get.instance(), complication.getNameKey(), complication.getName())};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.a(2131363129, format);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886931));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886376));
        fVar.a(2131363190);
        fVar.b(2131363105);
        fVar.b(2131363190);
        fVar.a(getChildFragmentManager(), "WARNING_DIALOG");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d(Complication complication) {
        ax5<n74> ax5 = this.f;
        if (ax5 != null) {
            n74 a2 = ax5.a();
            if (a2 != null) {
                Object r2 = a2.A;
                wg6.a((Object) r2, "binding.tvSelectedComplication");
                r2.setText(jm4.a(PortfolioApp.get.instance(), complication.getNameKey(), complication.getName()));
                Object r22 = a2.w;
                wg6.a((Object) r22, "binding.tvComplicationDetail");
                r22.setText(jm4.a(PortfolioApp.get.instance(), complication.getDescriptionKey(), complication.getDescription()));
                iu4 iu4 = this.i;
                if (iu4 != null) {
                    int a3 = iu4.a(complication.getComplicationId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "updateDetailComplication complicationId=" + complication.getComplicationId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.u.scrollToPosition(a3);
                        return;
                    }
                    return;
                }
                wg6.d("mComplicationAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e(String str) {
        wg6.b(str, "category");
        ax5<n74> ax5 = this.f;
        if (ax5 != null) {
            n74 a2 = ax5.a();
            if (a2 != null) {
                gu4 gu4 = this.h;
                if (gu4 != null) {
                    int a3 = gu4.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        gu4 gu42 = this.h;
                        if (gu42 != null) {
                            gu42.a(a3);
                            a2.t.smoothScrollToPosition(a3);
                            return;
                        }
                        wg6.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                wg6.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        wg6.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        xx5.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    xx5.a.a((Fragment) this, (int) MFNetworkReturnCode.RESPONSE_OK, "android.permission.ACCESS_BACKGROUND_LOCATION");
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                xx5.a aVar = xx5.a;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void g(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.C.a(this, str);
    }

    @DexIgnore
    public String h1() {
        return "ComplicationsFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final void j1() {
        if (isActive()) {
            iu4 iu4 = this.i;
            if (iu4 != null) {
                iu4.c();
            } else {
                wg6.d("mComplicationAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void n(List<Complication> list) {
        wg6.b(list, "complications");
        iu4 iu4 = this.i;
        if (iu4 != null) {
            iu4.a(list);
        } else {
            wg6.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        ComplicationsFragment.super.onActivityCreated(bundle);
        DianaCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = activity;
            w04 w04 = this.j;
            if (w04 != null) {
                DianaCustomizeViewModel a2 = vd.a(dianaCustomizeEditActivity, w04).a(DianaCustomizeViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = a2;
                p45 p45 = this.g;
                if (p45 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        p45.a(dianaCustomizeViewModel);
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        CommuteTimeSetting commuteTimeSetting;
        ComplicationsFragment.super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onActivityResult requestCode " + i2);
        if (i2 != 100) {
            if (i2 != 102) {
                if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                    p45 p45 = this.g;
                    if (p45 != null) {
                        p45.a("commute-time", commuteTimeSetting);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_COMPLICATION_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    p45 p452 = this.g;
                    if (p452 != null) {
                        wg6.a((Object) stringExtra, "selectedComplicationId");
                        p452.b(stringExtra);
                        return;
                    }
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            p45 p453 = this.g;
            if (p453 != null) {
                p453.a("second-timezone", secondTimezoneSetting);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        n74 a2 = kb.a(layoutInflater, 2131558520, viewGroup, false, e1());
        PortfolioApp.get.instance().g().a(new s45(this)).a(this);
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        p45 p45 = this.g;
        if (p45 != null) {
            p45.g();
            ComplicationsFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        ComplicationsFragment.super.onResume();
        p45 p45 = this.g;
        if (p45 != null) {
            p45.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v5, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        iu4 iu4 = new iu4((ArrayList) null, (iu4.c) null, 3, (qg6) null);
        iu4.a((iu4.c) new b(this));
        this.i = iu4;
        gu4 gu4 = new gu4((ArrayList) null, (gu4.c) null, 3, (qg6) null);
        gu4.a((gu4.c) new c(this));
        this.h = gu4;
        ax5<n74> ax5 = this.f;
        if (ax5 != null) {
            n74 a2 = ax5.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.t;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                gu4 gu42 = this.h;
                if (gu42 != null) {
                    recyclerView.setAdapter(gu42);
                    RecyclerView recyclerView2 = a2.u;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    iu4 iu42 = this.i;
                    if (iu42 != null) {
                        recyclerView2.setAdapter(iu42);
                        a2.x.setOnClickListener(new d(this));
                        a2.y.setOnClickListener(new e(this));
                        a2.v.setOnClickListener(new f(a2, this));
                        return;
                    }
                    wg6.d("mComplicationAdapter");
                    throw null;
                }
                wg6.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(p45 p45) {
        wg6.b(p45, "presenter");
        this.g = p45;
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onPermissionsGranted:" + i2 + ':' + list.size());
    }

    @DexIgnore
    public void a(List<Category> list) {
        wg6.b(list, HelpRequest.INCLUDE_CATEGORIES);
        gu4 gu4 = this.h;
        if (gu4 != null) {
            gu4.a(list);
        } else {
            wg6.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v3, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v10, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v17, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(boolean z, String str, String str2, Parcelable parcelable) {
        wg6.b(str, "complicationId");
        wg6.b(str2, "emptySettingRequestContent");
        ax5<n74> ax5 = this.f;
        if (ax5 != null) {
            n74 a2 = ax5.a();
            if (a2 != null) {
                Object r2 = a2.z;
                wg6.a((Object) r2, "it.tvPermissionOrder");
                r2.setVisibility(8);
                Object r22 = a2.x;
                wg6.a((Object) r22, "it.tvComplicationPermission");
                r22.setVisibility(8);
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    wg6.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(0);
                    a2.y.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    Object r9 = a2.y;
                    wg6.a((Object) r9, "it.tvComplicationSetting");
                    FragmentActivity activity = getActivity();
                    Resources resources = activity != null ? activity.getResources() : null;
                    if (resources != null) {
                        r9.setCompoundDrawablePadding((int) resources.getDimension(2131165418));
                        int hashCode = str.hashCode();
                        if (hashCode != -829740640) {
                            if (hashCode == 134170930 && str.equals("second-timezone")) {
                                Object r92 = a2.v;
                                wg6.a((Object) r92, "it.switchRing");
                                r92.setVisibility(8);
                                ImageView imageView = a2.r;
                                wg6.a((Object) imageView, "it.ivComplicationSetting");
                                imageView.setVisibility(8);
                                a2.y.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2131231060, 0);
                                if (parcelable != null) {
                                    Object r93 = a2.y;
                                    wg6.a((Object) r93, "it.tvComplicationSetting");
                                    r93.setText(((SecondTimezoneSetting) parcelable).getTimeZoneName());
                                    return;
                                }
                                Object r94 = a2.y;
                                wg6.a((Object) r94, "it.tvComplicationSetting");
                                r94.setText(str2);
                            }
                        } else if (str.equals("commute-time")) {
                            Object r95 = a2.v;
                            wg6.a((Object) r95, "it.switchRing");
                            r95.setVisibility(8);
                            ImageView imageView2 = a2.r;
                            wg6.a((Object) imageView2, "it.ivComplicationSetting");
                            imageView2.setVisibility(8);
                            a2.y.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2131231060, 0);
                            if (parcelable != null) {
                                Object r96 = a2.y;
                                wg6.a((Object) r96, "it.tvComplicationSetting");
                                r96.setText(((CommuteTimeSetting) parcelable).getAddress());
                                return;
                            }
                            Object r97 = a2.y;
                            wg6.a((Object) r97, "it.tvComplicationSetting");
                            r97.setText(str2);
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    ConstraintLayout constraintLayout2 = a2.q;
                    wg6.a((Object) constraintLayout2, "it.clComplicationSetting");
                    constraintLayout2.setVisibility(8);
                    Object r98 = a2.v;
                    wg6.a((Object) r98, "it.switchRing");
                    r98.setVisibility(8);
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void c(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.C.a(this, str);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(int i2, int i3, String str, String str2) {
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "showPermissionRequired current " + i2 + " total " + i3 + " title " + str + " content " + str2);
            ax5<n74> ax5 = this.f;
            if (ax5 != null) {
                n74 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.v;
                    wg6.a((Object) r1, "it.switchRing");
                    r1.setVisibility(8);
                    ConstraintLayout constraintLayout = a2.q;
                    wg6.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(8);
                    if (i3 == 0 || i2 == i3) {
                        Object r6 = a2.x;
                        wg6.a((Object) r6, "it.tvComplicationPermission");
                        r6.setVisibility(8);
                        Object r62 = a2.z;
                        wg6.a((Object) r62, "it.tvPermissionOrder");
                        r62.setVisibility(8);
                        return;
                    }
                    Object r2 = a2.x;
                    wg6.a((Object) r2, "it.tvComplicationPermission");
                    r2.setVisibility(0);
                    Object r22 = a2.x;
                    wg6.a((Object) r22, "it.tvComplicationPermission");
                    r22.setText(str);
                    if (str2.length() > 0) {
                        Object r8 = a2.w;
                        wg6.a((Object) r8, "it.tvComplicationDetail");
                        r8.setText(str2);
                    }
                    if (i3 > 1) {
                        Object r82 = a2.z;
                        wg6.a((Object) r82, "it.tvPermissionOrder");
                        r82.setVisibility(0);
                        Object r83 = a2.z;
                        wg6.a((Object) r83, "it.tvPermissionOrder");
                        nh6 nh6 = nh6.a;
                        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887240);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                        Object[] objArr = {Integer.valueOf(i2), Integer.valueOf(i3)};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        r83.setText(format);
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onPermissionsDenied:" + i2 + ':' + list.size());
        if (pw6.a(this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        D(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    D(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        BaseActivity activity;
        BaseActivity activity2;
        BaseActivity activity3;
        wg6.b(str, "tag");
        if (wg6.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363190 && (activity3 = getActivity()) != null) {
                if (activity3 != null) {
                    activity3.l();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (wg6.a((Object) str, (Object) lx5.c.a())) {
            if (i2 == 2131363190 && (activity2 = getActivity()) != null) {
                if (!pw6.a(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    xx5.a.a((Fragment) this, (int) MFNetworkReturnCode.RESPONSE_OK, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity2 != null) {
                    activity2.l();
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (wg6.a((Object) str, (Object) "WARNING_DIALOG") && i2 == 2131363190 && (activity = getActivity()) != null) {
            if (activity != null) {
                activity.l();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3, String str4) {
        wg6.b(str, "topComplication");
        wg6.b(str2, "bottomComplication");
        wg6.b(str3, "rightComlication");
        wg6.b(str4, "leftComplication");
        ComplicationSearchActivity.C.a(this, str, str2, str4, str3);
    }
}
