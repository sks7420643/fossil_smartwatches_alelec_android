package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            wg6.b(context, "context");
            wg6.b(str, "id");
            Intent intent = new Intent(context, UserCustomizeThemeActivity.class);
            intent.setFlags(536870912);
            Bundle bundle = new Bundle();
            bundle.putString("THEME_ID", str);
            bundle.putBoolean("THEME_MODE_EDIT", z);
            intent.addFlags(32768);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        UserCustomizeThemeFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = UserCustomizeThemeFragment.p.c();
            a((Fragment) b, 2131362119);
        }
        Intent intent = getIntent();
        wg6.a((Object) intent, "intent");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("THEME_ID", extras.getString("THEME_ID"));
            bundle2.putBoolean("THEME_MODE_EDIT", extras.getBoolean("THEME_MODE_EDIT"));
            b.setArguments(bundle2);
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
