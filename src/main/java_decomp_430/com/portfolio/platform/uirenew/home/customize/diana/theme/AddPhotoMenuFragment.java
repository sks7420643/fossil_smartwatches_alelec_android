package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.j64;
import com.fossil.jb;
import com.fossil.kb;
import com.fossil.o34;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddPhotoMenuFragment extends BaseBottomSheetDialogFragment {
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public /* final */ jb j; // = new o34(this);
    @DexIgnore
    public ax5<j64> o;
    @DexIgnore
    public b p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final AddPhotoMenuFragment a() {
            return new AddPhotoMenuFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void Y0();

        @DexIgnore
        void c1();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog a;
        @DexIgnore
        public /* final */ /* synthetic */ AddPhotoMenuFragment b;

        @DexIgnore
        public c(Dialog dialog, j64 j64, AddPhotoMenuFragment addPhotoMenuFragment) {
            this.a = dialog;
            this.b = addPhotoMenuFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            AddPhotoMenuFragment addPhotoMenuFragment = this.b;
            Dialog dialog = this.a;
            wg6.a((Object) dialog, "dialog");
            addPhotoMenuFragment.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog a;
        @DexIgnore
        public /* final */ /* synthetic */ AddPhotoMenuFragment b;

        @DexIgnore
        public d(Dialog dialog, j64 j64, AddPhotoMenuFragment addPhotoMenuFragment) {
            this.a = dialog;
            this.b = addPhotoMenuFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            b a2 = this.b.p;
            if (a2 != null) {
                a2.Y0();
            }
            AddPhotoMenuFragment addPhotoMenuFragment = this.b;
            Dialog dialog = this.a;
            wg6.a((Object) dialog, "dialog");
            addPhotoMenuFragment.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog a;
        @DexIgnore
        public /* final */ /* synthetic */ AddPhotoMenuFragment b;

        @DexIgnore
        public e(Dialog dialog, j64 j64, AddPhotoMenuFragment addPhotoMenuFragment) {
            this.a = dialog;
            this.b = addPhotoMenuFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            b a2 = this.b.p;
            if (a2 != null) {
                a2.c1();
            }
            AddPhotoMenuFragment addPhotoMenuFragment = this.b;
            Dialog dialog = this.a;
            wg6.a((Object) dialog, "dialog");
            addPhotoMenuFragment.onDismiss(dialog);
        }
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Dialog dialog;
        wg6.b(layoutInflater, "inflater");
        j64 a2 = kb.a(layoutInflater, 2131558504, viewGroup, false, this.j);
        this.o = new ax5<>(this, a2);
        ax5<j64> ax5 = this.o;
        if (ax5 != null) {
            j64 a3 = ax5.a();
            if (!(a3 == null || (dialog = getDialog()) == null)) {
                a3.q.setOnClickListener(new c(dialog, a3, this));
                a3.s.setOnClickListener(new d(dialog, a3, this));
                a3.r.setOnClickListener(new e(dialog, a3, this));
            }
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public final void a(b bVar) {
        this.p = bVar;
    }
}
