package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.g85;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.WeatherSettingFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public WeatherSettingPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wg6.b(fragment, "fragment");
            wg6.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), WeatherSettingActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            fragment.startActivityForResult(intent, 105);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(f(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        WeatherSettingFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = WeatherSettingFragment.p.a();
            a((Fragment) b, WeatherSettingFragment.p.b(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new g85(b)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                str = intent.getStringExtra(Constants.USER_SETTING);
                wg6.a((Object) str, "it.getStringExtra(Constants.JSON_KEY_SETTINGS)");
            } else {
                str = "";
            }
            WeatherSettingPresenter weatherSettingPresenter = this.B;
            if (weatherSettingPresenter != null) {
                weatherSettingPresenter.a(str);
            } else {
                wg6.d("mWeatherSettingPresenter");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingContract.View");
        }
    }
}
