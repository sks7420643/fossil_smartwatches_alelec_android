package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.CommuteTimeSettingsFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.CommuteTimeWatchAppSettingsFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSettingsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wg6.b(fragment, "fragment");
            wg6.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeWatchAppSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(2131558428);
        if (getSupportFragmentManager().b(2131362119) == null) {
            if (getIntent() != null) {
                str = getIntent().getStringExtra(Constants.USER_SETTING);
                wg6.a((Object) str, "intent.getStringExtra(Constants.JSON_KEY_SETTINGS)");
            } else {
                str = "";
            }
            a((Fragment) CommuteTimeWatchAppSettingsFragment.j.a(str), CommuteTimeSettingsFragment.q.b(), 2131362119);
        }
    }
}
