package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.fj5;
import com.fossil.fo5;
import com.fossil.go5;
import com.fossil.j84;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.ly5;
import com.fossil.nh6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeSleepChartFragment extends BaseFragment implements ly5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String o;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static String q;
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public CustomizeSleepChartViewModel g;
    @DexIgnore
    public ax5<j84> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CustomizeSleepChartFragment.o;
        }

        @DexIgnore
        public final String b() {
            return CustomizeSleepChartFragment.q;
        }

        @DexIgnore
        public final String c() {
            return CustomizeSleepChartFragment.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<go5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeSleepChartFragment a;

        @DexIgnore
        public b(CustomizeSleepChartFragment customizeSleepChartFragment) {
            this.a = customizeSleepChartFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CustomizeSleepChartViewModel.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.r(a2.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.u(b.intValue());
                }
                Integer e = bVar.e();
                if (e != null) {
                    this.a.t(e.intValue());
                }
                Integer f = bVar.f();
                if (f != null) {
                    this.a.w(f.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.s(c.intValue());
                }
                Integer d = bVar.d();
                if (d != null) {
                    this.a.v(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeSleepChartFragment a;

        @DexIgnore
        public c(CustomizeSleepChartFragment customizeSleepChartFragment) {
            this.a = customizeSleepChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 701);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeSleepChartFragment a;

        @DexIgnore
        public d(CustomizeSleepChartFragment customizeSleepChartFragment) {
            this.a = customizeSleepChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 702);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeSleepChartFragment a;

        @DexIgnore
        public e(CustomizeSleepChartFragment customizeSleepChartFragment) {
            this.a = customizeSleepChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 703);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeSleepChartFragment a;

        @DexIgnore
        public f(CustomizeSleepChartFragment customizeSleepChartFragment) {
            this.a = customizeSleepChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.g(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = CustomizeSleepChartFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeSleepChartFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363190) {
            CustomizeSleepChartViewModel customizeSleepChartViewModel = this.g;
            if (customizeSleepChartViewModel != null) {
                customizeSleepChartViewModel.a(UserCustomizeThemeFragment.p.a(), o, p, q);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf(i3 & 16777215)};
        String format = String.format("#%06X", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        CustomizeSleepChartViewModel customizeSleepChartViewModel = this.g;
        if (customizeSleepChartViewModel != null) {
            customizeSleepChartViewModel.a(i2, Color.parseColor(format));
            switch (i2) {
                case 701:
                    o = format;
                    return;
                case 702:
                    p = format;
                    return;
                case 703:
                    q = format;
                    return;
                default:
                    return;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void j1() {
        OverviewSleepDayChart overviewSleepDayChart;
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(0, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(8, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(56, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(104, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(111, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(117, BarChart.f.LOWEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(122, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(MFNetworkReturnCode.RESPONSE_OK, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(211, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(229, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(247, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(273, BarChart.f.LOWEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(286, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(305, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(316, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(325, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(337, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(410, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(474, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(486, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            arrayList.add(new BarChart.b(495, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a3 = qd6.a((T[]) new ArrayList[]{arrayList});
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.a(390, a3, 0, false, 12, (qg6) null));
            fj5.b bVar = new fj5.b(new BarChart.c(390, 390, arrayList2), 0.00996016f, 0.6055777f, 0.38446212f, 5, 304, 193, 25200);
            if (a2 != null && (overviewSleepDayChart = a2.u) != null) {
                overviewSleepDayChart.a(bVar.g());
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        j84 a2 = kb.a(LayoutInflater.from(getContext()), 2131558532, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new fo5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            CustomizeSleepChartViewModel a3 = vd.a(this, w04).a(CustomizeSleepChartViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            this.g = a3;
            CustomizeSleepChartViewModel customizeSleepChartViewModel = this.g;
            if (customizeSleepChartViewModel != null) {
                customizeSleepChartViewModel.b().a(getViewLifecycleOwner(), new b(this));
                CustomizeSleepChartViewModel customizeSleepChartViewModel2 = this.g;
                if (customizeSleepChartViewModel2 != null) {
                    customizeSleepChartViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    j1();
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        CustomizeSleepChartFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        o = null;
        p = null;
        q = null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CustomizeSleepChartFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        CustomizeSleepChartViewModel customizeSleepChartViewModel = this.g;
        if (customizeSleepChartViewModel != null) {
            customizeSleepChartViewModel.c();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.s.setOnClickListener(new e(this));
                a2.q.setOnClickListener(new f(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void r(int i2) {
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.u.a("awakeSleep", i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.u.a("deepSleep", i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t(int i2) {
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.u.a("lightSleep", i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void u(int i2) {
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.v.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void v(int i2) {
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.w.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void w(int i2) {
        ax5<j84> ax5 = this.h;
        if (ax5 != null) {
            j84 a2 = ax5.a();
            if (a2 != null) {
                a2.x.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
