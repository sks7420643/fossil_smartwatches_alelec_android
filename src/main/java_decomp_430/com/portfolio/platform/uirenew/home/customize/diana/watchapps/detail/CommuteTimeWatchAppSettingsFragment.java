package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.a85;
import com.fossil.ax5;
import com.fossil.cg;
import com.fossil.du4;
import com.fossil.j74;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.wz5;
import com.fossil.yd6;
import com.fossil.z75;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSettingsFragment extends BaseFragment {
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel f;
    @DexIgnore
    public w04 g;
    @DexIgnore
    public du4 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CommuteTimeWatchAppSettingsFragment a(String str) {
            wg6.b(str, MicroAppSetting.SETTING);
            CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment = new CommuteTimeWatchAppSettingsFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_SETTING, str);
            commuteTimeWatchAppSettingsFragment.setArguments(bundle);
            return commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements du4.b {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsFragment a;

        @DexIgnore
        public b(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            this.a = commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        public void a(AddressWrapper addressWrapper) {
            wg6.b(addressWrapper, "address");
            this.a.a(addressWrapper, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends wz5 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsFragment f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, int i2, CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            super(i, i2);
            this.f = commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        public void b(RecyclerView.ViewHolder viewHolder, int i) {
            wg6.b(viewHolder, "viewHolder");
            int adapterPosition = viewHolder.getAdapterPosition();
            du4 b = this.f.h;
            CommuteTimeWatchAppSettingsFragment.c(this.f).b(b != null ? b.a(adapterPosition) : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsFragment a;

        @DexIgnore
        public d(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            this.a = commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsFragment a;

        @DexIgnore
        public e(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            this.a = commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<a85.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsFragment a;

        @DexIgnore
        public f(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            this.a = commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CommuteTimeWatchAppSettingsViewModel.b bVar) {
            List<AddressWrapper> a2 = bVar.a();
            if (a2 != null) {
                this.a.x(a2);
            }
        }
    }

    /*
    static {
        wg6.a((Object) CommuteTimeWatchAppSettingsFragment.class.getSimpleName(), "CommuteTimeWatchAppSetti\u2026nt::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeWatchAppSettingsViewModel c(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = commuteTimeWatchAppSettingsFragment.f;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            return commuteTimeWatchAppSettingsViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        u(true);
        return true;
    }

    @DexIgnore
    public final void j1() {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
        if (commuteTimeWatchAppSettingsViewModel == null) {
            wg6.d("mViewModel");
            throw null;
        } else if (commuteTimeWatchAppSettingsViewModel.d()) {
            a(new AddressWrapper(), true);
        } else {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.s(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        CommuteTimeWatchAppSettingsFragment.super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 113) {
            AddressWrapper addressWrapper = (AddressWrapper) intent.getParcelableExtra("KEY_SELECTED_ADDRESS");
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                commuteTimeWatchAppSettingsViewModel.a(addressWrapper);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().g().a(new z75()).a(this);
        w04 w04 = this.g;
        String str = null;
        if (w04 != null) {
            CommuteTimeWatchAppSettingsViewModel a2 = vd.a(this, w04).a(CommuteTimeWatchAppSettingsViewModel.class);
            wg6.a((Object) a2, "ViewModelProviders.of(th\u2026ngsViewModel::class.java)");
            this.f = a2;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    str = arguments.getString(Constants.USER_SETTING);
                }
                commuteTimeWatchAppSettingsViewModel.a(str);
                return;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        j74 a2 = kb.a(layoutInflater, 2131558517, viewGroup, false, e1());
        a2.r.setOnClickListener(new d(this));
        a2.q.setOnClickListener(new e(this));
        du4 du4 = new du4();
        du4.a((du4.b) new b(this));
        this.h = du4;
        RecyclerView recyclerView = a2.t;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        new cg(new c(0, 4, this)).a(recyclerView);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.c().a(getViewLifecycleOwner(), new f(this));
            new ax5(this, a2);
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CommuteTimeWatchAppSettingsFragment.super.onResume();
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.e();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void u(boolean z) {
        if (z) {
            Intent intent = new Intent();
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                intent.putExtra("COMMUTE_TIME_WATCH_APP_SETTING", commuteTimeWatchAppSettingsViewModel.b());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public final void x(List<AddressWrapper> list) {
        du4 du4 = this.h;
        if (du4 != null) {
            du4.a((List<AddressWrapper>) yd6.d(list));
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY_SELECTED_ADDRESS", addressWrapper);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.f;
        ArrayList<String> arrayList = null;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            CommuteTimeWatchAppSetting a2 = commuteTimeWatchAppSettingsViewModel.a();
            if (a2 != null) {
                arrayList = a2.getListAddressNameExceptOf(addressWrapper);
            }
            bundle.putStringArrayList("KEY_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_HAVING_MAP_RESULT", !z);
            CommuteTimeSettingsDetailActivity.B.a(this, bundle, 113);
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }
}
