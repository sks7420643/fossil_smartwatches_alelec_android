package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.n85;
import com.fossil.nc6;
import com.fossil.o85;
import com.fossil.r85$a$a;
import com.fossil.r85$b$a;
import com.fossil.r85$b$b;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppSearchPresenter extends n85 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public ArrayList<WatchApp> i; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<WatchApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ o85 k;
    @DexIgnore
    public /* final */ WatchAppRepository l;
    @DexIgnore
    public /* final */ an4 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WatchAppSearchPresenter watchAppSearchPresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppSearchPresenter;
            this.$query = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$query, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            List list;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                list = new ArrayList();
                if (this.$query.length() > 0) {
                    dl6 a2 = this.this$0.c();
                    r85$a$a r85_a_a = new r85$a$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = list;
                    this.label = 1;
                    obj = gk6.a(a2, r85_a_a, this);
                    if (obj == a) {
                        return a;
                    }
                }
                this.this$0.k.b((List<lc6<WatchApp, String>>) this.this$0.a((List<WatchApp>) list));
                this.this$0.h = this.$query;
                return cd6.a;
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list = (List) obj;
            this.this$0.k.b((List<lc6<WatchApp, String>>) this.this$0.a((List<WatchApp>) list));
            this.this$0.h = this.$query;
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1", f = "WatchAppSearchPresenter.kt", l = {40, 43}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchAppSearchPresenter watchAppSearchPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppSearchPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 a2 = this.this$0.c();
                r85$b$b r85_b_b = new r85$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, r85_b_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                List list = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.j.clear();
                this.this$0.j.addAll((List) obj);
                this.this$0.i();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj;
            this.this$0.i.clear();
            this.this$0.i.addAll(list2);
            dl6 a3 = this.this$0.c();
            r85$b$a r85_b_a = new r85$b$a(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = list2;
            this.label = 2;
            obj = gk6.a(a3, r85_b_a, this);
            if (obj == a) {
                return a;
            }
            this.this$0.j.clear();
            this.this$0.j.addAll((List) obj);
            this.this$0.i();
            return cd6.a;
        }
    }

    @DexIgnore
    public WatchAppSearchPresenter(o85 o85, WatchAppRepository watchAppRepository, an4 an4) {
        wg6.b(o85, "mView");
        wg6.b(watchAppRepository, "mWatchAppRepository");
        wg6.b(an4, "sharedPreferencesManager");
        this.k = o85;
        this.l = watchAppRepository;
        this.m = an4;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.h = "";
        this.k.u();
        i();
    }

    @DexIgnore
    public final void i() {
        if (this.j.isEmpty()) {
            this.k.b(a((List<WatchApp>) yd6.d(this.i)));
        } else {
            this.k.d(a((List<WatchApp>) yd6.d(this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            o85 o85 = this.k;
            String str = this.h;
            if (str != null) {
                o85.b(str);
                String str2 = this.h;
                if (str2 != null) {
                    a(str2);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        wg6.b(str, "watchAppTop");
        wg6.b(str2, "watchAppMiddle");
        wg6.b(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new a(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(WatchApp watchApp) {
        wg6.b(watchApp, "selectedWatchApp");
        List<String> z = this.m.z();
        wg6.a((Object) z, "sharedPreferencesManager.watchAppSearchedIdsRecent");
        if (!z.contains(watchApp.getWatchappId())) {
            z.add(0, watchApp.getWatchappId());
            if (z.size() > 5) {
                z = z.subList(0, 5);
            }
            this.m.d(z);
        }
        this.k.a(watchApp);
    }

    @DexIgnore
    public final List<lc6<WatchApp, String>> a(List<WatchApp> list) {
        ArrayList arrayList = new ArrayList();
        for (WatchApp next : list) {
            if (!wg6.a((Object) next.getWatchappId(), (Object) "empty")) {
                String watchappId = next.getWatchappId();
                if (wg6.a((Object) watchappId, (Object) this.e)) {
                    arrayList.add(new lc6(next, "top"));
                } else if (wg6.a((Object) watchappId, (Object) this.f)) {
                    arrayList.add(new lc6(next, "middle"));
                } else if (wg6.a((Object) watchappId, (Object) this.g)) {
                    arrayList.add(new lc6(next, "bottom"));
                } else {
                    arrayList.add(new lc6(next, ""));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString("top", this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }
}
