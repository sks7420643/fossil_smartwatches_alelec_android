package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.kb;
import com.fossil.na4;
import com.fossil.pf5;
import com.fossil.qf5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewMonthFragment extends BaseFragment implements qf5 {
    @DexIgnore
    public ax5<na4> f;
    @DexIgnore
    public pf5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewHeartRateCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthFragment a;

        @DexIgnore
        public b(HeartRateOverviewMonthFragment heartRateOverviewMonthFragment) {
            this.a = heartRateOverviewMonthFragment;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            wg6.b(calendar, "calendar");
            pf5 a2 = HeartRateOverviewMonthFragment.a(this.a);
            Date time = calendar.getTime();
            wg6.a((Object) time, "calendar.time");
            a2.a(time);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewHeartRateCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthFragment a;

        @DexIgnore
        public c(HeartRateOverviewMonthFragment heartRateOverviewMonthFragment) {
            this.a = heartRateOverviewMonthFragment;
        }

        @DexIgnore
        public void a(int i, Calendar calendar) {
            wg6.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
                Date time = calendar.getTime();
                wg6.a((Object) time, "it.time");
                wg6.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ pf5 a(HeartRateOverviewMonthFragment heartRateOverviewMonthFragment) {
        pf5 pf5 = heartRateOverviewMonthFragment.g;
        if (pf5 != null) {
            return pf5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "HeartRateOverviewMonthFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HeartRateOverviewMonthFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onCreateView");
        na4 a2 = kb.a(layoutInflater, 2131558560, viewGroup, false, e1());
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = a2.q;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        recyclerViewHeartRateCalendar.setEndDate(instance);
        a2.q.setOnCalendarMonthChanged(new b(this));
        a2.q.setOnCalendarItemClickListener(new c(this));
        this.f = new ax5<>(this, a2);
        ax5<na4> ax5 = this.f;
        if (ax5 != null) {
            na4 a3 = ax5.a();
            if (a3 != null) {
                return a3.d();
            }
            return null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        HeartRateOverviewMonthFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onResume");
        pf5 pf5 = this.g;
        if (pf5 != null) {
            pf5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        HeartRateOverviewMonthFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onStop");
        pf5 pf5 = this.g;
        if (pf5 != null) {
            pf5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(pf5 pf5) {
        wg6.b(pf5, "presenter");
        this.g = pf5;
    }

    @DexIgnore
    public void a(TreeMap<Long, Integer> treeMap) {
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar;
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar2;
        wg6.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ax5<na4> ax5 = this.f;
        if (ax5 != null) {
            na4 a2 = ax5.a();
            if (!(a2 == null || (recyclerViewHeartRateCalendar2 = a2.q) == null)) {
                recyclerViewHeartRateCalendar2.setData(treeMap);
            }
            ax5<na4> ax52 = this.f;
            if (ax52 != null) {
                na4 a3 = ax52.a();
                if (a3 != null && (recyclerViewHeartRateCalendar = a3.q) != null) {
                    recyclerViewHeartRateCalendar.setEnableButtonNextAndPrevMonth(true);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        wg6.b(date, "selectDate");
        wg6.b(date2, "startDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ax5<na4> ax5 = this.f;
        if (ax5 != null) {
            na4 a2 = ax5.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                wg6.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                wg6.a((Object) instance2, "startCalendar");
                instance2.setTime(bk4.o(date2));
                wg6.a((Object) instance3, "endCalendar");
                instance3.setTime(bk4.j(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
