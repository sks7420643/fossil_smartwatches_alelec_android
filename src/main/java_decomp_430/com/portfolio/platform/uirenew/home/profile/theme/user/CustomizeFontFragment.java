package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.b84;
import com.fossil.kb;
import com.fossil.wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeFontFragment extends BaseFragment {
    @DexIgnore
    public ax5<b84> f;
    @DexIgnore
    public HashMap g;

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        b84 a = kb.a(LayoutInflater.from(getContext()), 2131558528, (ViewGroup) null, false, e1());
        this.f = new ax5<>(this, a);
        wg6.a((Object) a, "binding");
        return a.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<b84> ax5 = this.f;
        if (ax5 != null) {
            b84 a = ax5.a();
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
