package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.gy5;
import com.fossil.h05;
import com.fossil.i05;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lc4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.v05;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationWatchRemindersFragment extends BaseFragment implements i05 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public h05 f;
    @DexIgnore
    public ax5<lc4> g;
    @DexIgnore
    public InactivityNudgeTimeFragment h;
    @DexIgnore
    public RemindTimeFragment i;
    @DexIgnore
    public InactivityNudgeTimePresenter j;
    @DexIgnore
    public RemindTimePresenter o;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationWatchRemindersFragment.q;
        }

        @DexIgnore
        public final NotificationWatchRemindersFragment b() {
            return new NotificationWatchRemindersFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public b(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersFragment.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public c(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersFragment.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public d(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersFragment.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public e(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersFragment.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public f(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public g(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            gy5.a(view);
            InactivityNudgeTimeFragment a2 = this.a.h;
            if (a2 != null) {
                a2.r(0);
            }
            InactivityNudgeTimeFragment a3 = this.a.h;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, InactivityNudgeTimeFragment.s.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public h(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            gy5.a(view);
            InactivityNudgeTimeFragment a2 = this.a.h;
            if (a2 != null) {
                a2.r(1);
            }
            InactivityNudgeTimeFragment a3 = this.a.h;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, InactivityNudgeTimeFragment.s.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public i(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            gy5.a(view);
            RemindTimeFragment c = this.a.i;
            if (c != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                c.show(childFragmentManager, RemindTimeFragment.s.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersFragment a;

        @DexIgnore
        public j(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            this.a = notificationWatchRemindersFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersFragment.b(this.a).l();
        }
    }

    /*
    static {
        String simpleName = NotificationWatchRemindersFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationWatchReminde\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ h05 b(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
        h05 h05 = notificationWatchRemindersFragment.f;
        if (h05 != null) {
            return h05;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void F(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.F) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void G(boolean z) {
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.D;
                if (flexibleSwitchCompat != null) {
                    flexibleSwitchCompat.setChecked(z);
                }
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setAlpha(z ? 1.0f : 0.5f);
                }
                ConstraintLayout constraintLayout2 = a2.q;
                wg6.a((Object) constraintLayout2, "clInactivityNudgeContainer");
                int childCount = constraintLayout2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = a2.q.getChildAt(i2);
                    wg6.a((Object) childAt, "child");
                    childAt.setEnabled(z);
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void N(String str) {
        Object r0;
        wg6.b(str, LogBuilder.KEY_TIME);
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.v) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void d(SpannableString spannableString) {
        Object r0;
        wg6.b(spannableString, LogBuilder.KEY_TIME);
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.t) != 0) {
                r0.setText(spannableString);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        h05 h05 = this.f;
        if (h05 != null) {
            h05.l();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v13, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v14, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r4v15, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r4v16, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r4v17, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r4v21, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        lc4 a2 = kb.a(layoutInflater, 2131558586, viewGroup, false, e1());
        this.h = getChildFragmentManager().b(InactivityNudgeTimeFragment.s.a());
        if (this.h == null) {
            this.h = InactivityNudgeTimeFragment.s.b();
        }
        this.i = getChildFragmentManager().b(RemindTimeFragment.s.a());
        if (this.i == null) {
            this.i = RemindTimeFragment.s.b();
        }
        String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            a2.G.setBackgroundColor(parseColor);
            a2.H.setBackgroundColor(parseColor);
            a2.I.setBackgroundColor(parseColor);
        }
        Object r4 = a2.w;
        wg6.a((Object) r4, "binding.ftvTitle");
        r4.setSelected(true);
        a2.x.setOnClickListener(new b(this));
        a2.D.setOnClickListener(new c(this));
        a2.E.setOnClickListener(new d(this));
        a2.F.setOnClickListener(new e(this));
        a2.C.setOnClickListener(new f(this));
        a2.z.setOnClickListener(new g(this));
        a2.y.setOnClickListener(new h(this));
        a2.A.setOnClickListener(new i(this));
        a2.s.setOnClickListener(new j(this));
        this.g = new ax5<>(this, a2);
        y04 g2 = PortfolioApp.get.instance().g();
        InactivityNudgeTimeFragment inactivityNudgeTimeFragment = this.h;
        if (inactivityNudgeTimeFragment != null) {
            RemindTimeFragment remindTimeFragment = this.i;
            if (remindTimeFragment != null) {
                g2.a(new v05(inactivityNudgeTimeFragment, remindTimeFragment)).a(this);
                W("reminder_view");
                wg6.a((Object) a2, "binding");
                return a2.d();
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimeContract.View");
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimeContract.View");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        h05 h05 = this.f;
        if (h05 != null) {
            h05.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
            }
            NotificationWatchRemindersFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationWatchRemindersFragment.super.onResume();
        h05 h05 = this.f;
        if (h05 != null) {
            h05.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                wg6.a((Object) constraintLayout, "clRemindersContainer");
                constraintLayout.setVisibility(8);
                Object r2 = a2.s;
                wg6.a((Object) r2, "fbSave");
                r2.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void x(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.C) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void z(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.E) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void c(SpannableString spannableString) {
        Object r0;
        wg6.b(spannableString, LogBuilder.KEY_TIME);
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.u) != 0) {
                r0.setText(spannableString);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void d(boolean z) {
        ax5<lc4> ax5 = this.g;
        if (ax5 != null) {
            lc4 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.s;
                if (r1 != 0) {
                    r1.setEnabled(z);
                }
                if (z) {
                    FlexibleButton flexibleButton = a2.s;
                    if (flexibleButton != null) {
                        flexibleButton.a("flexible_button_primary");
                        return;
                    }
                    return;
                }
                FlexibleButton flexibleButton2 = a2.s;
                if (flexibleButton2 != null) {
                    flexibleButton2.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(h05 h05) {
        wg6.b(h05, "presenter");
        this.f = h05;
    }
}
