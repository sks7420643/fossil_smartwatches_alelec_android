package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.n85;
import com.fossil.nh6;
import com.fossil.nk4;
import com.fossil.o85;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.su4;
import com.fossil.te4;
import com.fossil.tq4;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppSearchFragment extends BaseFragment implements o85 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<te4> f;
    @DexIgnore
    public su4 g;
    @DexIgnore
    public n85 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WatchAppSearchFragment.j;
        }

        @DexIgnore
        public final WatchAppSearchFragment b() {
            return new WatchAppSearchFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchFragment a;

        @DexIgnore
        public b(WatchAppSearchFragment watchAppSearchFragment) {
            this.a = watchAppSearchFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            te4 a2 = this.a.k1().a();
            if (a2 != null) {
                a2.s.setText("");
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchFragment a;

        @DexIgnore
        public c(WatchAppSearchFragment watchAppSearchFragment) {
            this.a = watchAppSearchFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            ImageView imageView2;
            if (TextUtils.isEmpty(charSequence)) {
                te4 a2 = this.a.k1().a();
                if (!(a2 == null || (imageView2 = a2.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.a.b("");
                this.a.l1().h();
                return;
            }
            te4 a3 = this.a.k1().a();
            if (!(a3 == null || (imageView = a3.r) == null)) {
                imageView.setVisibility(0);
            }
            this.a.b(String.valueOf(charSequence));
            this.a.l1().a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchFragment a;

        @DexIgnore
        public d(WatchAppSearchFragment watchAppSearchFragment) {
            this.a = watchAppSearchFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements su4.d {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchFragment a;

        @DexIgnore
        public e(WatchAppSearchFragment watchAppSearchFragment) {
            this.a = watchAppSearchFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void a(String str) {
            wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            te4 a2 = this.a.k1().a();
            if (a2 != null) {
                Object r1 = a2.v;
                wg6.a((Object) r1, "it.tvNotFound");
                r1.setVisibility(0);
                Object r0 = a2.v;
                wg6.a((Object) r0, "it.tvNotFound");
                nh6 nh6 = nh6.a;
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886579);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object[] objArr = {str};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r0.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements su4.e {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchFragment a;

        @DexIgnore
        public f(WatchAppSearchFragment watchAppSearchFragment) {
            this.a = watchAppSearchFragment;
        }

        @DexIgnore
        public void a(WatchApp watchApp) {
            wg6.b(watchApp, "item");
            this.a.l1().a(watchApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ te4 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(te4 te4, long j) {
            this.a = te4;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r4v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onTransitionStart(Transition transition) {
            Object r4 = this.a.q;
            wg6.a((Object) r4, "binding.btnCancel");
            if (r4.getAlpha() == 0.0f) {
                this.a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.a.q.animate().setDuration(this.b).alpha(0.0f);
            }
        }
    }

    /*
    static {
        String simpleName = WatchAppSearchFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchAppSearchFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void b(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        su4 su4 = this.g;
        if (su4 != null) {
            su4.a(str);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void d(List<lc6<WatchApp, String>> list) {
        wg6.b(list, "recentSearchResult");
        su4 su4 = this.g;
        if (su4 != null) {
            su4.a(list);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return j;
    }

    @DexIgnore
    public boolean i1() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: com.portfolio.platform.view.FlexibleTextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: android.view.View} */
    /* JADX WARNING: Multi-variable type inference failed */
    public void j1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            nk4 nk4 = nk4.a;
            ax5<te4> ax5 = this.f;
            View view = null;
            if (ax5 != null) {
                te4 a2 = ax5.a();
                if (a2 != null) {
                    view = a2.q;
                }
                if (view != null) {
                    wg6.a((Object) activity, "it");
                    nk4.a(view, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.view.View");
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final ax5<te4> k1() {
        ax5<te4> ax5 = this.f;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final n85 l1() {
        n85 n85 = this.h;
        if (n85 != null) {
            return n85;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        WatchAppSearchFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558623, viewGroup, false, e1()));
        ax5<te4> ax5 = this.f;
        if (ax5 != null) {
            te4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        WatchAppSearchFragment.super.onResume();
        n85 n85 = this.h;
        if (n85 != null) {
            n85.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        WatchAppSearchFragment.super.onStop();
        n85 n85 = this.h;
        if (n85 != null) {
            n85.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.view.View, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        W("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "it");
            a(activity, 550);
        }
        this.g = new su4();
        ax5<te4> ax5 = this.f;
        if (ax5 != null) {
            te4 a2 = ax5.a();
            if (a2 != null) {
                te4 te4 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = te4.u;
                wg6.a((Object) recyclerViewEmptySupport, "this.rvResults");
                su4 su4 = this.g;
                if (su4 != null) {
                    recyclerViewEmptySupport.setAdapter(su4);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = te4.u;
                    wg6.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = te4.u;
                    Object r1 = te4.v;
                    wg6.a((Object) r1, "this.tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(r1);
                    ImageView imageView = te4.r;
                    wg6.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    te4.r.setOnClickListener(new b(this));
                    te4.s.addTextChangedListener(new c(this));
                    te4.q.setOnClickListener(new d(this));
                    su4 su42 = this.g;
                    if (su42 != null) {
                        su42.a((su4.d) new e(this));
                        su4 su43 = this.g;
                        if (su43 != null) {
                            su43.a((su4.e) new f(this));
                        } else {
                            wg6.d("mAdapter");
                            throw null;
                        }
                    } else {
                        wg6.d("mAdapter");
                        throw null;
                    }
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u() {
        su4 su4 = this.g;
        if (su4 != null) {
            su4.b((List<lc6<WatchApp, String>>) null);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = tq4.a.a(j2);
        Window window = fragmentActivity.getWindow();
        wg6.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        ax5<te4> ax5 = this.f;
        if (ax5 != null) {
            te4 a3 = ax5.a();
            if (a3 != null) {
                wg6.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(List<lc6<WatchApp, String>> list) {
        wg6.b(list, "results");
        su4 su4 = this.g;
        if (su4 != null) {
            su4.b(list);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final TransitionSet a(TransitionSet transitionSet, long j2, te4 te4) {
        Object r0 = te4.q;
        wg6.a((Object) r0, "binding.btnCancel");
        r0.setAlpha(0.0f);
        return transitionSet.addListener(new g(te4, j2));
    }

    @DexIgnore
    public void a(n85 n85) {
        wg6.b(n85, "presenter");
        this.h = n85;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: com.portfolio.platform.view.FlexibleTextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: android.view.View} */
    /* JADX WARNING: Multi-variable type inference failed */
    public void a(WatchApp watchApp) {
        wg6.b(watchApp, "selectedWatchApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            nk4 nk4 = nk4.a;
            ax5<te4> ax5 = this.f;
            View view = null;
            if (ax5 != null) {
                te4 a2 = ax5.a();
                if (a2 != null) {
                    view = a2.q;
                }
                if (view != null) {
                    wg6.a((Object) activity, "it");
                    nk4.a(view, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_WATCH_APP_RESULT_ID", watchApp.getWatchappId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.view.View");
            }
            wg6.d("mBinding");
            throw null;
        }
    }
}
