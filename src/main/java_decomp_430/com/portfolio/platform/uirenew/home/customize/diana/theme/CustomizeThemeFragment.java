package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.af6;
import com.fossil.ai4;
import com.fossil.ax5;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.kb;
import com.fossil.ku4;
import com.fossil.lk4;
import com.fossil.ll6;
import com.fossil.lx5;
import com.fossil.n84;
import com.fossil.nc6;
import com.fossil.o35;
import com.fossil.q65;
import com.fossil.qg6;
import com.fossil.r65;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.t65;
import com.fossil.ul6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x65;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.theme.AddPhotoMenuFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeThemeFragment extends BaseFragment implements r65, AddPhotoMenuFragment.b, AlertDialogFragment.g {
    @DexIgnore
    public ax5<n84> f;
    @DexIgnore
    public q65 g;
    @DexIgnore
    public ku4 h;
    @DexIgnore
    public x65 i;
    @DexIgnore
    public long j; // = 500;
    @DexIgnore
    public w04 o;
    @DexIgnore
    public DianaCustomizeViewModel p;
    @DexIgnore
    public AddPhotoMenuFragment q;
    @DexIgnore
    public WatchFaceWrapper r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public b(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.M0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public c(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.R0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public d(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            x65 b = this.a.i;
            if (b != null) {
                b.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ku4.c {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public e(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public void a(WatchFaceWrapper watchFaceWrapper) {
            wg6.b(watchFaceWrapper, "watchFaceWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemeFragment", "onItemClicked backgroundWrapper=" + watchFaceWrapper);
            CustomizeThemeFragment.c(this.a).c(watchFaceWrapper);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Integer $selectedPos$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView $this_with;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(RecyclerView recyclerView, xe6 xe6, CustomizeThemeFragment customizeThemeFragment, Integer num) {
            super(2, xe6);
            this.$this_with = recyclerView;
            this.this$0 = customizeThemeFragment;
            this.$selectedPos$inlined = num;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.$this_with, xe6, this.this$0, this.$selectedPos$inlined);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                long a2 = this.this$0.j;
                this.L$0 = il6;
                this.label = 1;
                if (ul6.a(a2, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j = 0;
            this.$this_with.smoothScrollToPosition(this.$selectedPos$inlined.intValue());
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Integer $selectedPos$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView $this_with;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(RecyclerView recyclerView, xe6 xe6, CustomizeThemeFragment customizeThemeFragment, Integer num) {
            super(2, xe6);
            this.$this_with = recyclerView;
            this.this$0 = customizeThemeFragment;
            this.$selectedPos$inlined = num;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.$this_with, xe6, this.this$0, this.$selectedPos$inlined);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                long a2 = this.this$0.j;
                this.L$0 = il6;
                this.label = 1;
                if (ul6.a(a2, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j = 0;
            this.$this_with.smoothScrollToPosition(this.$selectedPos$inlined.intValue());
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ q65 c(CustomizeThemeFragment customizeThemeFragment) {
        q65 q65 = customizeThemeFragment.g;
        if (q65 != null) {
            return q65;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void A(boolean z) {
        ax5<n84> ax5 = this.f;
        if (ax5 != null) {
            n84 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.u;
                wg6.a((Object) r1, "it.tvPhoto");
                r1.setClickable(z);
                Object r12 = a2.t;
                wg6.a((Object) r12, "it.tvBackground");
                r12.setClickable(z);
                RecyclerView recyclerView = a2.r;
                wg6.a((Object) recyclerView, "it.rvBackground");
                recyclerView.setClickable(z);
                RecyclerView recyclerView2 = a2.s;
                wg6.a((Object) recyclerView2, "it.rvPhoto");
                recyclerView2.setClickable(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void J0() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "showRemoveConfirmationDialog");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.H(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void M0() {
        ax5<n84> ax5 = this.f;
        if (ax5 != null) {
            n84 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.t;
                wg6.a((Object) r1, "binding.tvBackground");
                a((TextView) r1, true);
                Object r12 = a2.u;
                wg6.a((Object) r12, "binding.tvPhoto");
                a((TextView) r12, false);
                RecyclerView recyclerView = a2.s;
                wg6.a((Object) recyclerView, "binding.rvPhoto");
                recyclerView.setVisibility(4);
                RecyclerView recyclerView2 = a2.r;
                wg6.a((Object) recyclerView2, "binding.rvBackground");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void P0() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "showPickupPhotoType()");
        if (isActive()) {
            this.q = AddPhotoMenuFragment.r.a();
            AddPhotoMenuFragment addPhotoMenuFragment = this.q;
            if (addPhotoMenuFragment != null) {
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                addPhotoMenuFragment.show(childFragmentManager, "AddPhotoMenuFragment");
            }
            AddPhotoMenuFragment addPhotoMenuFragment2 = this.q;
            if (addPhotoMenuFragment2 != null) {
                addPhotoMenuFragment2.a((AddPhotoMenuFragment.b) this);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void R0() {
        ax5<n84> ax5 = this.f;
        if (ax5 != null) {
            n84 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.u;
                wg6.a((Object) r1, "binding.tvPhoto");
                a((TextView) r1, true);
                Object r12 = a2.t;
                wg6.a((Object) r12, "binding.tvBackground");
                a((TextView) r12, false);
                RecyclerView recyclerView = a2.r;
                wg6.a((Object) recyclerView, "binding.rvBackground");
                recyclerView.setVisibility(4);
                RecyclerView recyclerView2 = a2.s;
                wg6.a((Object) recyclerView2, "binding.rvPhoto");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void Y0() {
        FragmentActivity activity;
        if (xm4.a(xm4.d, getActivity(), "CAPTURE_IMAGE", false, false, false, 28, (Object) null) && (activity = getActivity()) != null) {
            wg6.a((Object) activity, "it");
            Intent a2 = lk4.a((Context) activity, activity.getPackageManager());
            if (a2 != null) {
                startActivityForResult(a2, Action.Selfie.TAKE_BURST);
            }
        }
    }

    @DexIgnore
    public void c1() {
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, MFNetworkReturnCode.RESPONSE_OK);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void h0() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "showPhotoListReachToLimitedSizeDialog()");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.D(childFragmentManager);
        }
    }

    @DexIgnore
    public String h1() {
        return "CustomizeThemeFragment";
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri data;
        Bundle extras;
        if (i3 == -1) {
            switch (i2) {
                case MFNetworkReturnCode.RESPONSE_OK:
                    if (intent != null && (data = intent.getData()) != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("CustomizeThemeFragment", "From Gallery, uri == " + data);
                        wg6.a((Object) data, "uri");
                        a(data);
                        return;
                    }
                    return;
                case 201:
                    if (intent != null && (extras = intent.getExtras()) != null) {
                        String string = extras.getString("WATCH_FACE_ID");
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("CustomizeThemeFragment", "Image is cropped and saved with watchFaceId = " + string);
                        if (string != null) {
                            q65 q65 = this.g;
                            if (q65 != null) {
                                q65.a(string);
                                return;
                            } else {
                                wg6.d("mPresenter");
                                throw null;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case Action.Selfie.TAKE_BURST:
                    Uri a2 = lk4.a(intent, (Context) PortfolioApp.get.instance());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("CustomizeThemeFragment", "From Camera, uri = " + a2);
                    if (a2 != null) {
                        a(a2);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        n84 a2 = kb.a(layoutInflater, 2131558534, viewGroup, false, e1());
        PortfolioApp.get.instance().g().a(new t65(this)).a(this);
        this.f = new ax5<>(this, a2);
        ax5<n84> ax5 = this.f;
        if (ax5 != null) {
            n84 a3 = ax5.a();
            if (a3 != null) {
                a3.t.setOnClickListener(new b(this));
                a3.u.setOnClickListener(new c(this));
                a3.q.setOnClickListener(new d(this));
            }
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        AddPhotoMenuFragment addPhotoMenuFragment = this.q;
        if (addPhotoMenuFragment != null) {
            addPhotoMenuFragment.a((AddPhotoMenuFragment.b) null);
        }
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        q65 q65 = this.g;
        if (q65 != null) {
            q65.g();
            CustomizeThemeFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        CustomizeThemeFragment.super.onResume();
        q65 q65 = this.g;
        if (q65 != null) {
            q65.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        DianaCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = activity;
            w04 w04 = this.o;
            if (w04 != null) {
                DianaCustomizeViewModel a2 = vd.a(dianaCustomizeEditActivity, w04).a(DianaCustomizeViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.p = a2;
                q65 q65 = this.g;
                if (q65 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.p;
                    if (dianaCustomizeViewModel != null) {
                        q65.a(dianaCustomizeViewModel);
                        ku4 ku4 = new ku4((ArrayList) null, (ku4.c) null, 3, (qg6) null);
                        ku4.a((ku4.c) new e(this));
                        this.h = ku4;
                        x65 x65 = new x65((ArrayList) null, (x65.d) null, 3, (qg6) null);
                        x65.a((x65.d) new f(this));
                        this.i = x65;
                        ax5<n84> ax5 = this.f;
                        if (ax5 != null) {
                            n84 a3 = ax5.a();
                            if (a3 != null) {
                                RecyclerView recyclerView = a3.r;
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setItemAnimator((RecyclerView.j) null);
                                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                                recyclerView.setAdapter(this.h);
                                RecyclerView recyclerView2 = a3.s;
                                recyclerView2.setHasFixedSize(true);
                                recyclerView2.setItemAnimator((RecyclerView.j) null);
                                recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                                recyclerView2.setAdapter(this.i);
                                return;
                            }
                            return;
                        }
                        wg6.d("mBinding");
                        throw null;
                    }
                    wg6.d("mShareViewModel");
                    throw null;
                }
                wg6.d("mPresenter");
                throw null;
            }
            wg6.d("viewModelFactory");
            throw null;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
    }

    @DexIgnore
    public void t0() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.x(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements x65.d {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public f(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public void a(WatchFaceWrapper watchFaceWrapper) {
            wg6.b(watchFaceWrapper, "watchFaceWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemeFragment", "onItemClicked photoWrapper=" + watchFaceWrapper);
            CustomizeThemeFragment.c(this.a).c(watchFaceWrapper);
        }

        @DexIgnore
        public void b(WatchFaceWrapper watchFaceWrapper) {
            wg6.b(watchFaceWrapper, "watchFaceWrapper");
            FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "onItemRemoved");
            this.a.r = watchFaceWrapper;
            CustomizeThemeFragment.c(this.a).a(watchFaceWrapper);
        }

        @DexIgnore
        public void a() {
            CustomizeThemeFragment.c(this.a).h();
        }
    }

    @DexIgnore
    public void b(WatchFaceWrapper watchFaceWrapper) {
        wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "removePhotoItem() " + watchFaceWrapper);
        x65 x65 = this.i;
        if (x65 != null) {
            x65.a(watchFaceWrapper);
        }
    }

    @DexIgnore
    public void a(q65 q65) {
        wg6.b(q65, "presenter");
        this.g = q65;
    }

    @DexIgnore
    public void a(List<WatchFaceWrapper> list, ai4 ai4) {
        x65 x65;
        wg6.b(list, "watchFaceWrappers");
        wg6.b(ai4, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "showWatchFaces watchFaceWrappers size=" + list.size() + ", type = " + ai4);
        if (ai4 == ai4.BACKGROUND) {
            ku4 ku4 = this.h;
            if (ku4 != null) {
                ku4.a(list);
            }
        } else if (ai4 == ai4.PHOTO && (x65 = this.i) != null) {
            x65.a(list);
        }
    }

    @DexIgnore
    public void a(String str, ai4 ai4) {
        wg6.b(str, "watchFaceId");
        wg6.b(ai4, "type");
        if (ai4 == ai4.BACKGROUND) {
            ku4 ku4 = this.h;
            if (ku4 != null) {
                ku4.notifyDataSetChanged();
            }
            ku4 ku42 = this.h;
            Integer valueOf = ku42 != null ? Integer.valueOf(ku42.a(str)) : null;
            if (valueOf != null && valueOf.intValue() >= 0) {
                ax5<n84> ax5 = this.f;
                if (ax5 != null) {
                    n84 a2 = ax5.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.r;
                        LinearLayoutManager layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            int J = layoutManager.J();
                            LinearLayoutManager layoutManager2 = recyclerView.getLayoutManager();
                            if (layoutManager2 != null) {
                                if (valueOf.intValue() < layoutManager2.G() || valueOf.intValue() > J) {
                                    rm6 unused = ik6.b(jl6.a(zl6.c()), (af6) null, (ll6) null, new g(recyclerView, (xe6) null, this, valueOf), 3, (Object) null);
                                    return;
                                }
                                return;
                            }
                            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
        } else if (ai4 == ai4.PHOTO) {
            x65 x65 = this.i;
            if (x65 != null) {
                x65.notifyDataSetChanged();
            }
            x65 x652 = this.i;
            Integer valueOf2 = x652 != null ? Integer.valueOf(x652.a(str)) : null;
            if (valueOf2 != null && valueOf2.intValue() >= 0) {
                ax5<n84> ax52 = this.f;
                if (ax52 != null) {
                    n84 a3 = ax52.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.s;
                        LinearLayoutManager layoutManager3 = recyclerView2.getLayoutManager();
                        if (layoutManager3 != null) {
                            int J2 = layoutManager3.J();
                            LinearLayoutManager layoutManager4 = recyclerView2.getLayoutManager();
                            if (layoutManager4 != null) {
                                if (valueOf2.intValue() < layoutManager4.G() || valueOf2.intValue() > J2) {
                                    rm6 unused2 = ik6.b(jl6.a(zl6.c()), (af6) null, (ll6) null, new h(recyclerView2, (xe6) null, this, valueOf2), 3, (Object) null);
                                    return;
                                }
                                return;
                            }
                            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        WatchFaceWrapper watchFaceWrapper;
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "onDialogFragmentResult tag=" + str + ", actionId=" + i2);
        if (str.hashCode() == 1111782057 && str.equals("CONFIRM_REMOVE_WATCH_FACE") && i2 == 2131363190 && (watchFaceWrapper = this.r) != null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("CustomizeThemeFragment", "onItemRemoved " + watchFaceWrapper);
            q65 q65 = this.g;
            if (q65 != null) {
                q65.b(watchFaceWrapper);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(Uri uri) {
        DianaCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            ArrayList<o35> s2 = activity.s();
            Intent intent = new Intent(getContext(), EditPhotoActivity.class);
            intent.putExtra("IMAGE_URI_EXTRA", uri);
            if (s2 != null) {
                intent.putExtra("COMPLICATION_EXTRA", s2);
            }
            startActivityForResult(intent, 201);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(TextView textView, boolean z) {
        Object instance = PortfolioApp.get.instance();
        int a2 = w6.a(instance, 2131099823);
        int a3 = w6.a(instance, 2131100008);
        Drawable c2 = w6.c(instance, 2131231253);
        Drawable c3 = w6.c(instance, 2131231254);
        if (z) {
            textView.setTextColor(a3);
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, c3);
        } else {
            textView.setTextColor(a2);
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, c2);
        }
        textView.setClickable(!z);
    }
}
