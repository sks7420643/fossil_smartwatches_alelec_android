package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.d7;
import com.fossil.jv4;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.ty4;
import com.fossil.uy4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.zb4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsFragment extends BaseFragment implements uy4 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<zb4> f;
    @DexIgnore
    public ty4 g;
    @DexIgnore
    public jv4 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsFragment.j;
        }

        @DexIgnore
        public final NotificationContactsFragment b() {
            return new NotificationContactsFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements jv4.b {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsFragment a;

        @DexIgnore
        public b(NotificationContactsFragment notificationContactsFragment) {
            this.a = notificationContactsFragment;
        }

        @DexIgnore
        public void a() {
            NotificationContactsFragment.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsFragment a;

        @DexIgnore
        public c(NotificationContactsFragment notificationContactsFragment) {
            this.a = notificationContactsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsFragment a;

        @DexIgnore
        public d(NotificationContactsFragment notificationContactsFragment) {
            this.a = notificationContactsFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            jv4 a2 = this.a.h;
            if (a2 != null) {
                a2.a(String.valueOf(charSequence));
            }
            this.a.R(!TextUtils.isEmpty(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ zb4 a;

        @DexIgnore
        public e(zb4 zb4) {
            this.a = zb4;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                zb4 zb4 = this.a;
                wg6.a((Object) zb4, "binding");
                zb4.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsFragment a;

        @DexIgnore
        public f(NotificationContactsFragment notificationContactsFragment) {
            this.a = notificationContactsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zb4 a;

        @DexIgnore
        public g(zb4 zb4) {
            this.a = zb4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            this.a.r.setText("");
        }
    }

    /*
    static {
        String simpleName = NotificationContactsFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationContactsFrag\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ty4 b(NotificationContactsFragment notificationContactsFragment) {
        ty4 ty4 = notificationContactsFragment.g;
        if (ty4 != null) {
            return ty4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void J() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void N(boolean z) {
        Object r0;
        int i2;
        Object r02;
        ax5<zb4> ax5 = this.f;
        if (ax5 != null) {
            zb4 a2 = ax5.a();
            if (!(a2 == null || (r02 = a2.q) == 0)) {
                r02.setEnabled(z);
            }
            ax5<zb4> ax52 = this.f;
            if (ax52 != null) {
                zb4 a3 = ax52.a();
                if (a3 != null && (r0 = a3.q) != 0) {
                    if (z) {
                        i2 = w6.a(PortfolioApp.get.instance(), 2131099723);
                    } else {
                        i2 = w6.a(PortfolioApp.get.instance(), 2131099832);
                    }
                    r0.setBackgroundColor(i2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void R(boolean z) {
        ax5<zb4> ax5 = this.f;
        if (ax5 != null) {
            zb4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageView imageView = a2.s;
                wg6.a((Object) imageView, "ivClearSearch");
                imageView.setVisibility(0);
                return;
            }
            ImageView imageView2 = a2.s;
            wg6.a((Object) imageView2, "ivClearSearch");
            imageView2.setVisibility(8);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        ty4 ty4 = this.g;
        if (ty4 != null) {
            ty4.h();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r6v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        zb4 a2 = kb.a(layoutInflater, 2131558579, viewGroup, false, e1());
        a2.t.setOnClickListener(new c(this));
        a2.r.addTextChangedListener(new d(this));
        a2.r.setOnFocusChangeListener(new e(a2));
        a2.q.setOnClickListener(new f(this));
        a2.s.setOnClickListener(new g(a2));
        a2.v.setIndexBarVisibility(true);
        a2.v.setIndexBarHighLateTextVisibility(true);
        a2.v.setIndexbarHighLateTextColor(2131099843);
        a2.v.setIndexBarTransparentValue(0.0f);
        a2.v.setIndexTextSize(9);
        a2.v.setIndexBarTextColor(2131099696);
        Context context = getContext();
        if (context != null) {
            Typeface a3 = d7.a(context, 2131296257);
            String b2 = ThemeManager.l.a().b("primaryText");
            Typeface c2 = ThemeManager.l.a().c("nonBrandTextStyle5");
            if (c2 != null) {
                a3 = c2;
            }
            if (!TextUtils.isEmpty(b2)) {
                int parseColor = Color.parseColor(b2);
                a2.v.setIndexBarTextColor(parseColor);
                a2.v.setIndexbarHighLateTextColor(parseColor);
            }
            if (a3 != null) {
                a2.v.setTypeface(a3);
            }
            RecyclerView recyclerView = a2.v;
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 1, false));
            this.f = new ax5<>(this, a2);
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ty4 ty4 = this.g;
        if (ty4 != null) {
            ty4.g();
            NotificationContactsFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationContactsFragment.super.onResume();
        ty4 ty4 = this.g;
        if (ty4 != null) {
            ty4.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ty4 ty4) {
        wg6.b(ty4, "presenter");
        this.g = ty4;
    }

    @DexIgnore
    public void a(List<wx4> list, FilterQueryProvider filterQueryProvider) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        wg6.b(list, "listContactWrapper");
        wg6.b(filterQueryProvider, "filterQueryProvider");
        this.h = new jv4((Cursor) null, list);
        jv4 jv4 = this.h;
        if (jv4 != null) {
            jv4.a((jv4.b) new b(this));
            jv4 jv42 = this.h;
            if (jv42 != null) {
                jv42.a(filterQueryProvider);
                ax5<zb4> ax5 = this.f;
                if (ax5 != null) {
                    zb4 a2 = ax5.a();
                    if (a2 != null && (alphabetFastScrollRecyclerView = a2.v) != null) {
                        alphabetFastScrollRecyclerView.setAdapter(this.h);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        jv4 jv4 = this.h;
        if (jv4 != null) {
            jv4.c(cursor);
        }
    }
}
