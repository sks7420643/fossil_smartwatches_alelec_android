package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.fossil.ae;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.pz4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ty4;
import com.fossil.uy4;
import com.fossil.vd6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.y24;
import com.fossil.yd6;
import com.fossil.yy4$c$a;
import com.fossil.yy4$c$b;
import com.fossil.z24;
import com.fossil.zd;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsPresenter extends ty4 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((qg6) null);
    @DexIgnore
    public /* final */ List<wx4> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<wx4> f; // = new ArrayList();
    @DexIgnore
    public /* final */ uy4 g;
    @DexIgnore
    public /* final */ z24 h;
    @DexIgnore
    public /* final */ mz4 i;
    @DexIgnore
    public /* final */ pz4 j;
    @DexIgnore
    public /* final */ LoaderManager k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsPresenter.l;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.d<pz4.c, y24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter a;

        @DexIgnore
        public b(NotificationContactsPresenter notificationContactsPresenter) {
            this.a = notificationContactsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pz4.c cVar) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), ".Inside mSaveContactGroupsNotification onSuccess");
            this.a.g.close();
        }

        @DexIgnore
        public void a(y24.a aVar) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), ".Inside mSaveContactGroupsNotification onError");
            this.a.g.close();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1", f = "NotificationContactsPresenter.kt", l = {49}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(NotificationContactsPresenter notificationContactsPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationContactsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v6, types: [com.fossil.mz4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.yy4$c$b] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (!PortfolioApp.get.instance().w().P()) {
                    dl6 a2 = this.this$0.b();
                    yy4$c$a yy4_c_a = new yy4$c$a((xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(a2, yy4_c_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.j().isEmpty()) {
                this.this$0.i.a(null, new yy4$c$b(this));
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationContactsPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationContactsPres\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsPresenter(uy4 uy4, z24 z24, mz4 mz4, pz4 pz4, LoaderManager loaderManager) {
        wg6.b(uy4, "mView");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(mz4, "mGetAllContactGroup");
        wg6.b(pz4, "mSaveContactGroupsNotification");
        wg6.b(loaderManager, "mLoaderManager");
        this.g = uy4;
        this.h = z24;
        this.i = mz4;
        this.j = pz4;
        this.k = loaderManager;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, "start");
        xm4 xm4 = xm4.d;
        uy4 uy4 = this.g;
        if (uy4 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsFragment");
        } else if (xm4.a(xm4, ((NotificationContactsFragment) uy4).getContext(), "NOTIFICATION_CONTACTS", false, false, false, 28, (Object) null)) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    public void h() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List<T> c2 = yd6.c(this.f, this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator<T> it = c2.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            Contact contact = ((wx4) next).getContact();
            if (contact != null) {
                num = Integer.valueOf(contact.getContactId());
            }
            Object obj = linkedHashMap.get(num);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(num, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it2 = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it2.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it2.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<wx4> arrayList3 = new ArrayList<>();
        for (Map.Entry value : linkedHashMap2.entrySet()) {
            vd6.a(arrayList3, (List) value.getValue());
        }
        for (wx4 wx4 : arrayList3) {
            boolean z2 = false;
            for (wx4 contact2 : this.f) {
                Contact contact3 = contact2.getContact();
                Integer valueOf = contact3 != null ? Integer.valueOf(contact3.getContactId()) : null;
                Contact contact4 = wx4.getContact();
                if (wg6.a((Object) valueOf, (Object) contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                    arrayList.add(wx4);
                    z2 = true;
                }
            }
            if (!z2) {
                arrayList2.add(wx4);
            }
        }
        this.h.a(this.j, new pz4.b(arrayList, arrayList2), new b(this));
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d(l, "onListContactWrapperChanged");
        List<T> c2 = yd6.c(this.f, this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (T next : c2) {
            Contact contact = ((wx4) next).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj = linkedHashMap.get(valueOf);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(valueOf, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry value : linkedHashMap2.entrySet()) {
            vd6.a(arrayList, (List) value.getValue());
        }
        if (arrayList.isEmpty()) {
            this.g.N(false);
        } else {
            this.g.N(true);
        }
    }

    @DexIgnore
    public final List<wx4> j() {
        return this.e;
    }

    @DexIgnore
    public final List<wx4> k() {
        return this.f;
    }

    @DexIgnore
    public void l() {
        this.g.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public ae<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new zd(PortfolioApp.get.instance(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(ae<Cursor> aeVar, Cursor cursor) {
        wg6.b(aeVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.g.a(cursor);
    }

    @DexIgnore
    public void a(ae<Cursor> aeVar) {
        wg6.b(aeVar, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.g.J();
    }
}
