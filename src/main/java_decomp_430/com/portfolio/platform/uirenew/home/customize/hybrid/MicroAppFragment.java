package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.gu4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lu4;
import com.fossil.lx5;
import com.fossil.nb4;
import com.fossil.nh6;
import com.fossil.pw6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rk4;
import com.fossil.s95;
import com.fossil.t95;
import com.fossil.u95;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xx5;
import com.fossil.yi4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppFragment extends BaseFragment implements t95, AlertDialogFragment.g {
    @DexIgnore
    public ax5<nb4> f;
    @DexIgnore
    public s95 g;
    @DexIgnore
    public gu4 h;
    @DexIgnore
    public lu4 i;
    @DexIgnore
    public w04 j;
    @DexIgnore
    public HybridCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements lu4.c {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppFragment a;

        @DexIgnore
        public b(MicroAppFragment microAppFragment) {
            this.a = microAppFragment;
        }

        @DexIgnore
        public void a(MicroApp microApp) {
            wg6.b(microApp, "microApp");
            MicroAppFragment.a(this.a).a(microApp.getId());
        }

        @DexIgnore
        public void b(MicroApp microApp) {
            wg6.b(microApp, "microApp");
            this.a.c(microApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements gu4.c {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppFragment a;

        @DexIgnore
        public c(MicroAppFragment microAppFragment) {
            this.a = microAppFragment;
        }

        @DexIgnore
        public void a() {
            MicroAppFragment.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            wg6.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppsFragment", "onItemClicked category=" + category);
            MicroAppFragment.a(this.a).a(category);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppFragment a;

        @DexIgnore
        public d(MicroAppFragment microAppFragment) {
            this.a = microAppFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            MicroAppFragment.a(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppFragment a;

        @DexIgnore
        public e(MicroAppFragment microAppFragment) {
            this.a = microAppFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            MicroAppFragment.a(this.a).i();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ s95 a(MicroAppFragment microAppFragment) {
        s95 s95 = microAppFragment.g;
        if (s95 != null) {
            return s95;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void C(String str) {
        wg6.b(str, "content");
        ax5<nb4> ax5 = this.f;
        if (ax5 != null) {
            nb4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.t;
                wg6.a((Object) r0, "it.tvMicroAppDetail");
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void D(String str) {
        wg6.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        lx5 lx5 = lx5.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager, "childFragmentManager");
                        lx5.K(childFragmentManager);
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    lx5 lx52 = lx5.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    wg6.a((Object) childFragmentManager2, "childFragmentManager");
                    lx52.J(childFragmentManager2);
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                lx5 lx53 = lx5.c;
                FragmentManager childFragmentManager3 = getChildFragmentManager();
                wg6.a((Object) childFragmentManager3, "childFragmentManager");
                lx53.z(childFragmentManager3);
            }
        }
    }

    @DexIgnore
    public void I(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        SearchRingPhoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void b(MicroApp microApp) {
        if (microApp != null) {
            lu4 lu4 = this.i;
            if (lu4 != null) {
                lu4.b(microApp.getId());
                d(microApp);
                return;
            }
            wg6.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(MicroApp microApp) {
        String str;
        List<String> b2 = rk4.c.b(microApp.getId());
        String str2 = InAppPermission.NOTIFICATION_ACCESS;
        if (b2.contains(str2)) {
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887129);
            wg6.a((Object) str, "LanguageHelper.getString\u2026ification_access_warning)");
        } else {
            nh6 nh6 = nh6.a;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886603);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026iresLocationSettingsToBe)");
            Object[] objArr = {jm4.a(PortfolioApp.get.instance(), microApp.getNameKey(), microApp.getName())};
            str = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) str, "java.lang.String.format(format, *args)");
            str2 = "LOCATION_PERMISSION_TAG";
        }
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.a(2131363129, str);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886931));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886376));
        fVar.a(2131363190);
        fVar.b(2131363105);
        fVar.b(2131363190);
        fVar.a(getChildFragmentManager(), str2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d(MicroApp microApp) {
        ax5<nb4> ax5 = this.f;
        if (ax5 != null) {
            nb4 a2 = ax5.a();
            if (a2 != null) {
                Object r2 = a2.x;
                wg6.a((Object) r2, "binding.tvSelectedMicroApp");
                r2.setText(jm4.a(PortfolioApp.get.instance(), microApp.getNameKey(), microApp.getName()));
                Object r22 = a2.t;
                wg6.a((Object) r22, "binding.tvMicroAppDetail");
                r22.setText(jm4.a(PortfolioApp.get.instance(), microApp.getDescriptionKey(), microApp.getDescription()));
                lu4 lu4 = this.i;
                if (lu4 != null) {
                    int a3 = lu4.a(microApp.getId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppsFragment", "updateDetailMicroApp microAppId=" + microApp.getId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.s.scrollToPosition(a3);
                        return;
                    }
                    return;
                }
                wg6.d("mMicroAppAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e(String str) {
        wg6.b(str, "category");
        ax5<nb4> ax5 = this.f;
        if (ax5 != null) {
            nb4 a2 = ax5.a();
            if (a2 != null) {
                gu4 gu4 = this.h;
                if (gu4 != null) {
                    int a3 = gu4.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        gu4 gu42 = this.h;
                        if (gu42 != null) {
                            gu42.a(a3);
                            a2.r.smoothScrollToPosition(a3);
                            return;
                        }
                        wg6.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                wg6.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        wg6.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        xx5.a aVar = xx5.a;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            wg6.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        xx5.a.a((Fragment) this, (int) MFNetworkReturnCode.RESPONSE_OK, "android.permission.ACCESS_BACKGROUND_LOCATION");
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        xx5.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        PermissionActivity.a aVar2 = PermissionActivity.C;
                        Context context = getContext();
                        if (context != null) {
                            wg6.a((Object) context, "context!!");
                            aVar2.a(context, qd6.a((T[]) new Integer[]{10}));
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void g(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.C.a(this, str);
    }

    @DexIgnore
    public String h1() {
        return "MicroAppsFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final void j1() {
        if (isActive()) {
            lu4 lu4 = this.i;
            if (lu4 != null) {
                lu4.c();
            } else {
                wg6.d("mMicroAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        MicroAppFragment.super.onActivityCreated(bundle);
        HybridCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = activity;
            w04 w04 = this.j;
            if (w04 != null) {
                HybridCustomizeViewModel a2 = vd.a(hybridCustomizeEditActivity, w04).a(HybridCustomizeViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = a2;
                s95 s95 = this.g;
                if (s95 != null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.o;
                    if (hybridCustomizeViewModel != null) {
                        s95.a(hybridCustomizeViewModel);
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        Ringtone ringtone;
        CommuteTimeSetting commuteTimeSetting;
        MicroAppFragment.super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onActivityResult requestCode " + i2 + " resultCode " + i3);
        if (i2 != 100) {
            if (i2 != 102) {
                if (i2 != 104) {
                    if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                        s95 s95 = this.g;
                        if (s95 != null) {
                            s95.a(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), yi4.a(commuteTimeSetting));
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                } else if (intent != null && i3 == -1 && (ringtone = (Ringtone) intent.getParcelableExtra("KEY_SELECTED_RINGPHONE")) != null) {
                    s95 s952 = this.g;
                    if (s952 != null) {
                        s952.a(MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue(), yi4.a(ringtone));
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_MICRO_APP_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    s95 s953 = this.g;
                    if (s953 != null) {
                        wg6.a((Object) stringExtra, "selectedMicroAppId");
                        s953.a(stringExtra);
                        return;
                    }
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            s95 s954 = this.g;
            if (s954 != null) {
                s954.a(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), yi4.a(secondTimezoneSetting));
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        nb4 a2 = kb.a(layoutInflater, 2131558574, viewGroup, false, e1());
        PortfolioApp.get.instance().g().a(new u95(this)).a(this);
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        s95 s95 = this.g;
        if (s95 != null) {
            s95.g();
            MicroAppFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        MicroAppFragment.super.onResume();
        s95 s95 = this.g;
        if (s95 != null) {
            s95.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        lu4 lu4 = new lu4((ArrayList) null, (lu4.c) null, 3, (qg6) null);
        lu4.a((lu4.c) new b(this));
        this.i = lu4;
        gu4 gu4 = new gu4((ArrayList) null, (gu4.c) null, 3, (qg6) null);
        gu4.a((gu4.c) new c(this));
        this.h = gu4;
        ax5<nb4> ax5 = this.f;
        if (ax5 != null) {
            nb4 a2 = ax5.a();
            if (a2 != null) {
                String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b2)) {
                    a2.q.setBackgroundColor(Color.parseColor(b2));
                }
                RecyclerView recyclerView = a2.r;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                gu4 gu42 = this.h;
                if (gu42 != null) {
                    recyclerView.setAdapter(gu42);
                    RecyclerView recyclerView2 = a2.s;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    lu4 lu42 = this.i;
                    if (lu42 != null) {
                        recyclerView2.setAdapter(lu42);
                        a2.u.setOnClickListener(new d(this));
                        a2.v.setOnClickListener(new e(this));
                        return;
                    }
                    wg6.d("mMicroAppAdapter");
                    throw null;
                }
                wg6.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void w(List<MicroApp> list) {
        wg6.b(list, "microApps");
        lu4 lu4 = this.i;
        if (lu4 != null) {
            lu4.a(list);
        } else {
            wg6.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(s95 s95) {
        wg6.b(s95, "presenter");
        this.g = s95;
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onPermissionsGranted:" + i2 + ':' + list.size());
    }

    @DexIgnore
    public void a(List<Category> list) {
        wg6.b(list, HelpRequest.INCLUDE_CATEGORIES);
        gu4 gu4 = this.h;
        if (gu4 != null) {
            gu4.a(list);
        } else {
            wg6.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(boolean z, String str, String str2, String str3) {
        wg6.b(str, "microAppId");
        wg6.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "updateSetting of microAppId " + str + " requestContent " + str2 + " setting " + str3);
        ax5<nb4> ax5 = this.f;
        if (ax5 != null) {
            nb4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.w;
                wg6.a((Object) r0, "it.tvPermissionOrder");
                r0.setVisibility(8);
                Object r02 = a2.u;
                wg6.a((Object) r02, "it.tvMicroAppPermission");
                r02.setVisibility(8);
                if (z) {
                    Object r4 = a2.v;
                    wg6.a((Object) r4, "it.tvMicroAppSetting");
                    r4.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        Object r42 = a2.v;
                        wg6.a((Object) r42, "it.tvMicroAppSetting");
                        r42.setText(str3);
                        return;
                    }
                    Object r43 = a2.v;
                    wg6.a((Object) r43, "it.tvMicroAppSetting");
                    r43.setText(str2);
                    return;
                }
                Object r44 = a2.v;
                wg6.a((Object) r44, "it.tvMicroAppSetting");
                r44.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.C.a(this, str);
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onPermissionsDenied:" + i2 + ':' + list.size());
        if (pw6.a(this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        D(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    D(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r10v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r9v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(int i2, int i3, String str, String str2) {
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppsFragment", "showPermissionRequired current " + i2 + " total " + i3 + " title " + str + " content " + str2);
            ax5<nb4> ax5 = this.f;
            if (ax5 != null) {
                nb4 a2 = ax5.a();
                if (a2 == null) {
                    return;
                }
                if (i3 == 0 || i2 == i3) {
                    Object r7 = a2.u;
                    wg6.a((Object) r7, "it.tvMicroAppPermission");
                    r7.setVisibility(8);
                    Object r72 = a2.w;
                    wg6.a((Object) r72, "it.tvPermissionOrder");
                    r72.setVisibility(8);
                    return;
                }
                Object r4 = a2.v;
                wg6.a((Object) r4, "it.tvMicroAppSetting");
                r4.setVisibility(8);
                Object r2 = a2.u;
                wg6.a((Object) r2, "it.tvMicroAppPermission");
                r2.setVisibility(0);
                Object r22 = a2.u;
                wg6.a((Object) r22, "it.tvMicroAppPermission");
                r22.setText(str);
                if (str2.length() > 0) {
                    Object r9 = a2.t;
                    wg6.a((Object) r9, "it.tvMicroAppDetail");
                    r9.setText(str2);
                }
                if (i3 > 1) {
                    Object r92 = a2.w;
                    wg6.a((Object) r92, "it.tvPermissionOrder");
                    r92.setVisibility(0);
                    Object r93 = a2.w;
                    wg6.a((Object) r93, "it.tvPermissionOrder");
                    nh6 nh6 = nh6.a;
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887240);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                    Object[] objArr = {Integer.valueOf(i2), Integer.valueOf(i3)};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    r93.setText(format);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        BaseActivity activity;
        BaseActivity activity2;
        BaseActivity activity3;
        wg6.b(str, "tag");
        if (wg6.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363190 && (activity3 = getActivity()) != null) {
                if (activity3 != null) {
                    activity3.l();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (wg6.a((Object) str, (Object) lx5.c.a())) {
            if (i2 == 2131363190 && (activity2 = getActivity()) != null) {
                if (!pw6.a(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    xx5.a.a((Fragment) this, (int) MFNetworkReturnCode.RESPONSE_OK, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity2 != null) {
                    activity2.l();
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (wg6.a((Object) str, (Object) "LOCATION_PERMISSION_TAG")) {
            if (i2 != 2131363190 || (activity = getActivity()) == null) {
                return;
            }
            if (activity != null) {
                activity.l();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (wg6.a((Object) str, (Object) InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363190) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        wg6.b(str, "topMicroApp");
        wg6.b(str2, "middleMicroApp");
        wg6.b(str3, "bottomMicroApp");
        SearchMicroAppActivity.C.a(this, str, str2, str3);
    }
}
