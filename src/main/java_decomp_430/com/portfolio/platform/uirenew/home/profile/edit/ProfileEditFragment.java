package com.portfolio.platform.uirenew.home.profile.edit;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.c06;
import com.fossil.ft;
import com.fossil.hd4;
import com.fossil.ij4;
import com.fossil.jj4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kk5;
import com.fossil.kl4;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lj4;
import com.fossil.lk4;
import com.fossil.lx5;
import com.fossil.mj4;
import com.fossil.mk5;
import com.fossil.nw6;
import com.fossil.nz;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rh4;
import com.fossil.rh6;
import com.fossil.rr5;
import com.fossil.tr5;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wj4;
import com.fossil.xj4;
import com.fossil.xm4;
import com.fossil.y04;
import com.fossil.yj6;
import com.fossil.yz5;
import com.fossil.zh4;
import com.fossil.zj4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.BirthdayFragment;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileEditFragment extends BaseFragment implements AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public ProfileEditViewModel f;
    @DexIgnore
    public ax5<hd4> g;
    @DexIgnore
    public mj4 h;
    @DexIgnore
    public BirthdayFragment i;
    @DexIgnore
    public c06 j;
    @DexIgnore
    public w04 o;
    @DexIgnore
    public tr5 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ProfileEditFragment a() {
            return new ProfileEditFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public b(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b(rh4.FEMALE);
            ProfileEditFragment.d(this.a).a(rh4.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public c(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b(rh4.OTHER);
            ProfileEditFragment.d(this.a).a(rh4.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public d(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileEditFragment.d(this.a).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<mk5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public e(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v5, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
        /* renamed from: a */
        public final void onChanged(ProfileEditViewModel.b bVar) {
            FlexibleTextInputLayout flexibleTextInputLayout;
            FlexibleTextInputLayout flexibleTextInputLayout2;
            FlexibleTextInputLayout flexibleTextInputLayout3;
            hd4 hd4;
            Object r1;
            MFUser i = bVar.i();
            if (i != null) {
                this.a.c(i);
            }
            Boolean j = bVar.j();
            if (j != null) {
                this.a.R(j.booleanValue());
            }
            Uri d = bVar.d();
            if (d != null) {
                this.a.a(d);
            }
            if (bVar.e()) {
                this.a.e();
            } else {
                this.a.d();
            }
            if (bVar.h() != null) {
                this.a.o();
            }
            lc6<Integer, String> g = bVar.g();
            if (g != null) {
                this.a.a(g.getFirst().intValue(), g.getSecond());
            }
            if (bVar.f()) {
                this.a.z();
            }
            Bundle c = bVar.c();
            if (c != null) {
                this.a.a(c);
            }
            String a2 = bVar.a();
            if (!(a2 == null || (hd4 = (hd4) ProfileEditFragment.c(this.a).a()) == null || (r1 = hd4.E) == 0)) {
                r1.setText(a2);
            }
            if (bVar.b() == null) {
                hd4 hd42 = (hd4) ProfileEditFragment.c(this.a).a();
                if (hd42 != null && (flexibleTextInputLayout3 = hd42.y) != null) {
                    flexibleTextInputLayout3.setErrorEnabled(false);
                    return;
                }
                return;
            }
            hd4 hd43 = (hd4) ProfileEditFragment.c(this.a).a();
            if (!(hd43 == null || (flexibleTextInputLayout2 = hd43.y) == null)) {
                flexibleTextInputLayout2.setErrorEnabled(true);
            }
            hd4 hd44 = (hd4) ProfileEditFragment.c(this.a).a();
            if (hd44 != null && (flexibleTextInputLayout = hd44.y) != null) {
                flexibleTextInputLayout.setError(bVar.b());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public f(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Date date) {
            if (date != null) {
                ProfileEditFragment.d(this.a).b(date);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public g(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public h(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX WARNING: type inference failed for: r2v11, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0014, code lost:
            r2 = r2.r;
         */
        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Object r2;
            ProfileEditViewModel d = ProfileEditFragment.d(this.a);
            hd4 hd4 = (hd4) ProfileEditFragment.c(this.a).a();
            Editable editableText = (hd4 == null || r2 == 0) ? null : r2.getEditableText();
            String valueOf = String.valueOf(editableText);
            if (valueOf != null) {
                d.a(yj6.d(valueOf).toString());
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public i(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX WARNING: type inference failed for: r2v11, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0014, code lost:
            r2 = r2.s;
         */
        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Object r2;
            ProfileEditViewModel d = ProfileEditFragment.d(this.a);
            hd4 hd4 = (hd4) ProfileEditFragment.c(this.a).a();
            Editable editableText = (hd4 == null || r2 == 0) ? null : r2.getEditableText();
            String valueOf = String.valueOf(editableText);
            if (valueOf != null) {
                d.b(yj6.d(valueOf).toString());
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public j(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public k(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements yz5 {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ hd4 b;

        @DexIgnore
        public l(ProfileEditFragment profileEditFragment, hd4 hd4) {
            this.a = profileEditFragment;
            this.b = hd4;
        }

        @DexIgnore
        public void a(int i) {
            if (this.b.B.getUnit() == zh4.METRIC) {
                ProfileEditFragment.d(this.a).a(i);
                return;
            }
            ProfileEditFragment.d(this.a).a(rh6.a(zj4.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements yz5 {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ hd4 b;

        @DexIgnore
        public m(ProfileEditFragment profileEditFragment, hd4 hd4) {
            this.a = profileEditFragment;
            this.b = hd4;
        }

        @DexIgnore
        public void a(int i) {
            if (this.b.C.getUnit() == zh4.METRIC) {
                ProfileEditFragment.d(this.a).b(rh6.a((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            ProfileEditFragment.d(this.a).b(rh6.a(zj4.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditFragment a;

        @DexIgnore
        public n(ProfileEditFragment profileEditFragment) {
            this.a = profileEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b(rh4.MALE);
            ProfileEditFragment.d(this.a).a(rh4.MALE);
        }
    }

    /*
    static {
        String simpleName = ProfileEditFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "ProfileEditFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ax5 c(ProfileEditFragment profileEditFragment) {
        ax5<hd4> ax5 = profileEditFragment.g;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProfileEditViewModel d(ProfileEditFragment profileEditFragment) {
        ProfileEditViewModel profileEditViewModel = profileEditFragment.f;
        if (profileEditViewModel != null) {
            return profileEditViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @nw6(1222)
    public final void doCameraTask() {
        FragmentActivity activity;
        Intent b2;
        if (xm4.a(xm4.d, getActivity(), "EDIT_AVATAR", false, false, false, 28, (Object) null) && (activity = getActivity()) != null && (b2 = lk4.b(activity)) != null) {
            startActivityForResult(b2, 1234);
        }
    }

    /* JADX WARNING: type inference failed for: r5v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.ProgressButton] */
    /* JADX WARNING: type inference failed for: r5v6, types: [java.lang.Object, android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r5v11, types: [java.lang.Object, android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004c, code lost:
        if ((com.fossil.yj6.d(r5).length() > 0) != false) goto L_0x0050;
     */
    @DexIgnore
    public final void R(boolean z) {
        ax5<hd4> ax5 = this.g;
        if (ax5 != null) {
            hd4 a2 = ax5.a();
            if (a2 != null) {
                boolean z2 = true;
                if (z) {
                    Object r5 = a2.r;
                    wg6.a((Object) r5, "it.etFirstName");
                    Editable editableText = r5.getEditableText();
                    wg6.a((Object) editableText, "it.etFirstName.editableText");
                    if (yj6.d(editableText).length() > 0) {
                        Object r52 = a2.s;
                        wg6.a((Object) r52, "it.etLastName");
                        Editable editableText2 = r52.getEditableText();
                        wg6.a((Object) editableText2, "it.etLastName.editableText");
                    }
                }
                z2 = false;
                Object r53 = a2.D;
                wg6.a((Object) r53, "it.save");
                r53.setEnabled(z2);
                if (z2) {
                    a2.D.a("flexible_button_primary");
                } else {
                    a2.D.a("flexible_button_disabled");
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        wg6.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            ProfileEditViewModel profileEditViewModel = this.f;
                            if (profileEditViewModel != null) {
                                if (profileEditViewModel.c()) {
                                    lx5 lx5 = lx5.c;
                                    FragmentManager childFragmentManager = getChildFragmentManager();
                                    wg6.a((Object) childFragmentManager, "childFragmentManager");
                                    lx5.S(childFragmentManager);
                                } else {
                                    FragmentActivity activity3 = getActivity();
                                    if (activity3 != null) {
                                        activity3.finish();
                                    }
                                }
                                return true;
                            }
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        return true;
    }

    @DexIgnore
    public final void j1() {
        ProfileEditViewModel profileEditViewModel = this.f;
        if (profileEditViewModel != null) {
            profileEditViewModel.f();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void o() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        wg6.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        ProfileEditFragment.super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            ProfileEditViewModel profileEditViewModel = this.f;
            if (profileEditViewModel != null) {
                profileEditViewModel.a(intent);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v21, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v22, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r4v23, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r4v24, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    /* JADX WARNING: type inference failed for: r4v25, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    /* JADX WARNING: type inference failed for: r4v28, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v29, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v30, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v31, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        hd4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558598, (ViewGroup) null, false, e1());
        this.i = getChildFragmentManager().b(BirthdayFragment.s.a());
        if (this.i == null) {
            this.i = BirthdayFragment.s.b();
        }
        y04 g2 = PortfolioApp.get.instance().g();
        BirthdayFragment birthdayFragment = this.i;
        if (birthdayFragment != null) {
            g2.a(new rr5(birthdayFragment)).a(this);
            w04 w04 = this.o;
            if (w04 != null) {
                ProfileEditViewModel a3 = vd.a(this, w04).a(ProfileEditViewModel.class);
                wg6.a((Object) a3, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
                this.f = a3;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    c06 a4 = vd.a(activity).a(c06.class);
                    wg6.a((Object) a4, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
                    this.j = a4;
                    c06 c06 = this.j;
                    if (c06 != null) {
                        c06.a().a(this, new f(this));
                        a2.z.setOnClickListener(new g(this));
                        a2.r.addTextChangedListener(new h(this));
                        a2.s.addTextChangedListener(new i(this));
                        a2.D.setOnClickListener(new j(this));
                        a2.q.setOnClickListener(new k(this));
                        a2.B.setValuePickerListener(new l(this, a2));
                        a2.C.setValuePickerListener(new m(this, a2));
                        a2.u.setOnClickListener(new n(this));
                        a2.t.setOnClickListener(new b(this));
                        a2.v.setOnClickListener(new c(this));
                        a2.E.setOnClickListener(new d(this));
                        ProfileEditViewModel profileEditViewModel = this.f;
                        if (profileEditViewModel != null) {
                            profileEditViewModel.a().a(getViewLifecycleOwner(), new e(this));
                            this.g = new ax5<>(this, a2);
                            wg6.a((Object) a2, "binding");
                            return a2.d();
                        }
                        wg6.d("mViewModel");
                        throw null;
                    }
                    wg6.d("mUserBirthDayViewModel");
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.d("viewModelFactory");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ProfileEditFragment.super.onPause();
        kl4 g1 = g1();
        if (g1 != null) {
            g1.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        ProfileEditFragment.super.onResume();
        ProfileEditViewModel profileEditViewModel = this.f;
        if (profileEditViewModel != null) {
            profileEditViewModel.d();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        W("edit_profile_view");
        mj4 a2 = jj4.a((Fragment) this);
        wg6.a((Object) a2, "GlideApp.with(this)");
        this.h = a2;
    }

    @DexIgnore
    public final void z() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.E(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(int i2, zh4 zh4) {
        int i3 = kk5.b[zh4.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "updateData weight=" + i2 + " metric");
            ax5<hd4> ax5 = this.g;
            if (ax5 != null) {
                hd4 a2 = ax5.a();
                if (a2 != null) {
                    Object r0 = a2.x;
                    wg6.a((Object) r0, "it.ftvWeightUnit");
                    r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886712));
                    a2.C.setUnit(zh4.METRIC);
                    a2.C.setFormatter(new ProfileFormatter(4));
                    a2.C.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            ax5<hd4> ax52 = this.g;
            if (ax52 != null) {
                hd4 a3 = ax52.a();
                if (a3 != null) {
                    Object r02 = a3.x;
                    wg6.a((Object) r02, "it.ftvWeightUnit");
                    r02.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886713));
                    float f2 = zj4.f((float) i2);
                    a3.C.setUnit(zh4.IMPERIAL);
                    a3.C.setFormatter(new ProfileFormatter(4));
                    a3.C.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v24, resolved type: android.widget.ImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v32, resolved type: android.widget.ImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v33, resolved type: com.portfolio.platform.view.FossilCircleImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v34, resolved type: android.widget.ImageView} */
    /* JADX WARNING: type inference failed for: r1v10, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r0v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v23, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r0v24, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r0v37, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    /* JADX WARNING: type inference failed for: r1v26, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Multi-variable type inference failed */
    @SuppressLint({"SimpleDateFormat"})
    public final void c(MFUser mFUser) {
        Object r1;
        Object r0;
        Object r02;
        Object r03;
        Object r04;
        FossilCircleImageView fossilCircleImageView;
        FossilCircleImageView fossilCircleImageView2;
        FossilCircleImageView fossilCircleImageView3;
        String profilePicture = mFUser.getProfilePicture();
        String str = mFUser.getFirstName() + " " + mFUser.getLastName();
        if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
            mj4 mj4 = this.h;
            if (mj4 != null) {
                lj4 a2 = mj4.a((Object) new ij4("", str)).a(new nz().a(new xj4()));
                ax5<hd4> ax5 = this.g;
                if (ax5 != null) {
                    hd4 a3 = ax5.a();
                    ImageView imageView = a3 != null ? a3.q : null;
                    if (imageView != null) {
                        a2.a(imageView);
                        ax5<hd4> ax52 = this.g;
                        if (ax52 != null) {
                            hd4 a4 = ax52.a();
                            if (!(a4 == null || (fossilCircleImageView2 = a4.q) == null)) {
                                fossilCircleImageView2.setBorderColor(w6.a(PortfolioApp.get.instance(), 2131099837));
                            }
                            ax5<hd4> ax53 = this.g;
                            if (ax53 != null) {
                                hd4 a5 = ax53.a();
                                if (!(a5 == null || (fossilCircleImageView = a5.q) == null)) {
                                    fossilCircleImageView.setBorderWidth(3);
                                }
                                ax5<hd4> ax54 = this.g;
                                if (ax54 != null) {
                                    hd4 a6 = ax54.a();
                                    if (!(a6 == null || (r04 = a6.q) == 0)) {
                                        r04.setBackground(w6.c(PortfolioApp.get.instance(), 2131231300));
                                    }
                                } else {
                                    wg6.d("mBinding");
                                    throw null;
                                }
                            } else {
                                wg6.d("mBinding");
                                throw null;
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                wg6.d("mGlideRequests");
                throw null;
            }
        } else {
            ax5<hd4> ax55 = this.g;
            if (ax55 != null) {
                hd4 a7 = ax55.a();
                if (!(a7 == null || (fossilCircleImageView3 = a7.q) == null)) {
                    mj4 mj42 = this.h;
                    if (mj42 != null) {
                        fossilCircleImageView3.a(mj42, profilePicture, str);
                    } else {
                        wg6.d("mGlideRequests");
                        throw null;
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
        ax5<hd4> ax56 = this.g;
        if (ax56 != null) {
            hd4 a8 = ax56.a();
            if (!(a8 == null || (r03 = a8.r) == 0)) {
                r03.setText(mFUser.getFirstName());
            }
            ax5<hd4> ax57 = this.g;
            if (ax57 != null) {
                hd4 a9 = ax57.a();
                if (!(a9 == null || (r02 = a9.s) == 0)) {
                    r02.setText(mFUser.getLastName());
                }
                ax5<hd4> ax58 = this.g;
                if (ax58 != null) {
                    hd4 a10 = ax58.a();
                    if (!(a10 == null || (r0 = a10.F) == 0)) {
                        r0.setText(mFUser.getEmail());
                    }
                    wj4 wj4 = wj4.a;
                    rh4 gender = mFUser.getGender();
                    wg6.a((Object) gender, "user.gender");
                    lc6<Integer, Integer> a11 = wj4.a(gender, MFUser.getAge(mFUser.getBirthday()));
                    if (mFUser.getHeightInCentimeters() == 0) {
                        mFUser.setHeightInCentimeters(a11.getFirst().intValue());
                    }
                    if (mFUser.getHeightInCentimeters() > 0) {
                        int heightInCentimeters = mFUser.getHeightInCentimeters();
                        zh4 heightUnit = mFUser.getHeightUnit();
                        wg6.a((Object) heightUnit, "user.heightUnit");
                        a(heightInCentimeters, heightUnit);
                    }
                    if (mFUser.getWeightInGrams() == 0) {
                        mFUser.setWeightInGrams(a11.getSecond().intValue() * 1000);
                    }
                    if (mFUser.getWeightInGrams() > 0) {
                        int weightInGrams = mFUser.getWeightInGrams();
                        zh4 weightUnit = mFUser.getWeightUnit();
                        wg6.a((Object) weightUnit, "user.weightUnit");
                        b(weightInGrams, weightUnit);
                    }
                    String birthday = mFUser.getBirthday();
                    if (!TextUtils.isEmpty(birthday)) {
                        try {
                            Date e2 = bk4.e(birthday);
                            ax5<hd4> ax59 = this.g;
                            if (ax59 != null) {
                                hd4 a12 = ax59.a();
                                if (!(a12 == null || (r1 = a12.E) == 0)) {
                                    r1.setText(bk4.b(e2));
                                }
                            } else {
                                wg6.d("mBinding");
                                throw null;
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                    rh4 gender2 = mFUser.getGender();
                    wg6.a((Object) gender2, "user.gender");
                    b(gender2);
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void d() {
        ProgressButton progressButton;
        ax5<hd4> ax5 = this.g;
        if (ax5 != null) {
            hd4 a2 = ax5.a();
            if (a2 != null && (progressButton = a2.D) != null) {
                progressButton.b();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e() {
        ProgressButton progressButton;
        ax5<hd4> ax5 = this.g;
        if (ax5 != null) {
            hd4 a2 = ax5.a();
            if (a2 != null && (progressButton = a2.D) != null) {
                progressButton.c();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        BirthdayFragment birthdayFragment = this.i;
        if (birthdayFragment != null) {
            birthdayFragment.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            birthdayFragment.show(childFragmentManager, BirthdayFragment.s.a());
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(int i2, zh4 zh4) {
        int i3 = kk5.a[zh4.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "updateData height=" + i2 + " metric");
            ax5<hd4> ax5 = this.g;
            if (ax5 != null) {
                hd4 a2 = ax5.a();
                if (a2 != null) {
                    Object r0 = a2.w;
                    wg6.a((Object) r0, "it.ftvHeightUnit");
                    r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886953));
                    a2.B.setUnit(zh4.METRIC);
                    a2.B.setFormatter(new ProfileFormatter(-1));
                    a2.B.a(100, 251, i2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            ax5<hd4> ax52 = this.g;
            if (ax52 != null) {
                hd4 a3 = ax52.a();
                if (a3 != null) {
                    Object r02 = a3.w;
                    wg6.a((Object) r02, "it.ftvHeightUnit");
                    r02.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886954));
                    lc6<Integer, Integer> b2 = zj4.b((float) i2);
                    a3.B.setUnit(zh4.IMPERIAL);
                    a3.B.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.B;
                    Number first = b2.getFirst();
                    wg6.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = zj4.a(first.intValue());
                    Number second = b2.getSecond();
                    wg6.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void b(rh4 rh4) {
        ax5<hd4> ax5 = this.g;
        if (ax5 != null) {
            hd4 a2 = ax5.a();
            if (a2 != null) {
                a2.t.a("flexible_button_secondary");
                a2.u.a("flexible_button_secondary");
                a2.v.a("flexible_button_secondary");
                int i2 = kk5.c[rh4.ordinal()];
                if (i2 == 1) {
                    ax5<hd4> ax52 = this.g;
                    if (ax52 != null) {
                        hd4 a3 = ax52.a();
                        if (a3 != null && a3.t != null) {
                            a2.t.a("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    wg6.d("mBinding");
                    throw null;
                } else if (i2 != 2) {
                    ax5<hd4> ax53 = this.g;
                    if (ax53 != null) {
                        hd4 a4 = ax53.a();
                        if (a4 != null && a4.v != null) {
                            a2.v.a("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    wg6.d("mBinding");
                    throw null;
                } else {
                    ax5<hd4> ax54 = this.g;
                    if (ax54 != null) {
                        hd4 a5 = ax54.a();
                        if (a5 != null && a5.u != null) {
                            a2.u.a("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    wg6.d("mBinding");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: android.widget.ImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: android.widget.ImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: com.portfolio.platform.view.FossilCircleImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: android.widget.ImageView} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(Uri uri) {
        if (isActive()) {
            mj4 mj4 = this.h;
            if (mj4 != null) {
                lj4 a2 = mj4.a(uri).a(ft.a).a(new nz().a(new xj4()));
                ax5<hd4> ax5 = this.g;
                if (ax5 != null) {
                    hd4 a3 = ax5.a();
                    ImageView imageView = a3 != null ? a3.q : null;
                    if (imageView != null) {
                        a2.a(imageView);
                        R(true);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mGlideRequests");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        wg6.b(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.a(str, i2, intent);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363105) {
            o();
        } else if (i2 == 2131363190) {
            j1();
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }
}
