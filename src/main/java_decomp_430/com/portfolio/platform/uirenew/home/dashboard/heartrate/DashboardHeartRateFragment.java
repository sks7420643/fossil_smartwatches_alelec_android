package com.portfolio.platform.uirenew.home.dashboard.heartrate;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.af5;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.bu4;
import com.fossil.cf;
import com.fossil.dv4;
import com.fossil.ev4;
import com.fossil.fv4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.la5;
import com.fossil.pg;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tz5;
import com.fossil.vd;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.z84;
import com.fossil.ze5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardHeartRateFragment extends BaseFragment implements af5, fv4, bu4 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<z84> f;
    @DexIgnore
    public ze5 g;
    @DexIgnore
    public ev4 h;
    @DexIgnore
    public HeartRateOverviewFragment i;
    @DexIgnore
    public tz5 j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DashboardHeartRateFragment.p;
        }

        @DexIgnore
        public final DashboardHeartRateFragment b() {
            return new DashboardHeartRateFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends tz5 {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardHeartRateFragment e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DashboardHeartRateFragment dashboardHeartRateFragment, LinearLayoutManager linearLayoutManager, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager2);
            this.e = dashboardHeartRateFragment;
        }

        @DexIgnore
        public void a(int i) {
            DashboardHeartRateFragment.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = DashboardHeartRateFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "DashboardHeartRateFragme\u2026::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ze5 a(DashboardHeartRateFragment dashboardHeartRateFragment) {
        ze5 ze5 = dashboardHeartRateFragment.g;
        if (ze5 != null) {
            return ze5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Q(boolean z) {
        ax5<z84> ax5;
        RecyclerView.ViewHolder findViewHolderForAdapterPosition;
        View view;
        if (isVisible() && (ax5 = this.f) != null) {
            RecyclerView recyclerView = null;
            if (ax5 != null) {
                z84 a2 = ax5.a();
                if (a2 != null) {
                    recyclerView = a2.q;
                }
                if (recyclerView == null || (findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0)) == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != 0.0f) {
                    if (recyclerView != null) {
                        recyclerView.smoothScrollToPosition(0);
                    }
                    tz5 tz5 = this.j;
                    if (tz5 != null) {
                        tz5.a();
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
            wg6.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f() {
        tz5 tz5 = this.j;
        if (tz5 != null) {
            tz5.a();
        }
    }

    @DexIgnore
    public String h1() {
        return p;
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        DashboardHeartRateFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        z84 a2 = kb.a(layoutInflater, 2131558540, viewGroup, false, e1());
        this.i = getChildFragmentManager().b("HeartRateOverviewFragment");
        if (this.i == null) {
            this.i = new HeartRateOverviewFragment();
        }
        dv4 dv4 = new dv4();
        PortfolioApp instance = PortfolioApp.get.instance();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        HeartRateOverviewFragment heartRateOverviewFragment = this.i;
        if (heartRateOverviewFragment != null) {
            this.h = new ev4(dv4, instance, this, childFragmentManager, heartRateOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            this.j = new b(this, linearLayoutManager, linearLayoutManager);
            la5 la5 = new la5(linearLayoutManager.Q());
            Context context = getContext();
            if (context != null) {
                Drawable c = w6.c(context, 2131230876);
                if (c != null) {
                    wg6.a((Object) c, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                    la5.a(c);
                    RecyclerView recyclerView = a2.q;
                    wg6.a((Object) recyclerView, "this");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    ev4 ev4 = this.h;
                    if (ev4 != null) {
                        recyclerView.setAdapter(ev4);
                        tz5 tz5 = this.j;
                        if (tz5 != null) {
                            recyclerView.addOnScrollListener(tz5);
                            recyclerView.setItemViewCacheSize(0);
                            recyclerView.addItemDecoration(la5);
                            if (recyclerView.getItemAnimator() instanceof pg) {
                                pg itemAnimator = recyclerView.getItemAnimator();
                                if (itemAnimator != null) {
                                    itemAnimator.setSupportsChangeAnimations(false);
                                } else {
                                    throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.SimpleItemAnimator");
                                }
                            }
                            ze5 ze5 = this.g;
                            if (ze5 != null) {
                                ze5.h();
                                this.f = new ax5<>(this, a2);
                                ax5<z84> ax5 = this.f;
                                if (ax5 != null) {
                                    z84 a3 = ax5.a();
                                    if (a3 != null) {
                                        wg6.a((Object) a3, "mBinding.get()!!");
                                        return a3.d();
                                    }
                                    wg6.a();
                                    throw null;
                                }
                                wg6.d("mBinding");
                                throw null;
                            }
                            wg6.d("mPresenter");
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.d("mDashboardHeartRatesAdapter");
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(p, "onDestroy");
        DashboardHeartRateFragment.super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        ze5 ze5 = this.g;
        if (ze5 != null) {
            ze5.i();
            super.onDestroyView();
            d1();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        ze5 ze5 = this.g;
        if (ze5 != null) {
            ze5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
            }
            DashboardHeartRateFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        DashboardHeartRateFragment.super.onResume();
        ze5 ze5 = this.g;
        if (ze5 != null) {
            ze5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        W("heart_rate_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            b06 a2 = vd.a(activity).a(b06.class);
            wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            b06 b06 = a2;
        }
    }

    @DexIgnore
    public void a(ze5 ze5) {
        wg6.b(ze5, "presenter");
        this.g = ze5;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wg6.b(date, "startWeekDate");
        wg6.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    public void b(cf<DailyHeartRateSummary> cfVar) {
        ev4 ev4 = this.h;
        if (ev4 != null) {
            ev4.c(cfVar);
        } else {
            wg6.d("mDashboardHeartRatesAdapter");
            throw null;
        }
    }
}
