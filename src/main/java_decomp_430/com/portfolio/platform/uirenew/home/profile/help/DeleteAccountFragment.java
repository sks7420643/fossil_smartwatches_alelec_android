package com.portfolio.platform.uirenew.home.profile.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.d94;
import com.fossil.jk3;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.ll5;
import com.fossil.lx5;
import com.fossil.ml5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAccountFragment extends BaseFragment implements ml5, View.OnClickListener, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public ax5<d94> f;
    @DexIgnore
    public ll5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DeleteAccountFragment a() {
            return new DeleteAccountFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountFragment a;

        @DexIgnore
        public b(DeleteAccountFragment deleteAccountFragment) {
            this.a = deleteAccountFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountFragment a;

        @DexIgnore
        public c(DeleteAccountFragment deleteAccountFragment) {
            this.a = deleteAccountFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.h(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountFragment a;

        @DexIgnore
        public d(DeleteAccountFragment deleteAccountFragment) {
            this.a = deleteAccountFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().i();
            this.a.j1().a("Delete Account - Contact Us - From app [Fossil] - [Android]");
        }
    }

    /*
    static {
        String simpleName = DeleteAccountFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "DeleteAccountFragment::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public void T0() {
        f1().c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "it");
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                FLogger.INSTANCE.getLocal().d(i, "deleteUser - successfully");
                WelcomeActivity.C.b(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        wg6.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public final ll5 j1() {
        ll5 ll5 = this.g;
        if (ll5 != null) {
            return ll5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            DeleteAccountFragment.super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            ll5 ll5 = this.g;
            if (ll5 != null) {
                ll5.h();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wg6.b(view, "v");
        if (view.getId() == 2131361851) {
            o();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        d94 a2 = kb.a(LayoutInflater.from(getContext()), 2131558542, (ViewGroup) null, false, e1());
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        DeleteAccountFragment.super.onPause();
        ll5 ll5 = this.g;
        if (ll5 != null) {
            ll5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        DeleteAccountFragment.super.onResume();
        ll5 ll5 = this.g;
        if (ll5 != null) {
            ll5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r8v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<d94> ax5 = this.f;
        if (ax5 != null) {
            d94 a2 = ax5.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                String a3 = xj6.a(jm4.a((Context) PortfolioApp.get.instance(), 2131886848).toString(), "contact_our_support_team", "", false, 4, (Object) null);
                Object r0 = a2.s;
                wg6.a((Object) r0, "binding.tvDescription");
                r0.setText(Html.fromHtml(a3));
                a2.s.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i2, String str) {
        wg6.b(str, "message");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(ll5 ll5) {
        wg6.b(ll5, "presenter");
        jk3.a(ll5);
        wg6.a((Object) ll5, "Preconditions.checkNotNull(presenter)");
        this.g = ll5;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0)) {
            if (str.hashCode() != 1069400824 || !str.equals("CONFIRM_DELETE_ACCOUNT")) {
                BaseActivity activity = getActivity();
                if (activity != null) {
                    activity.a(str, i2, intent);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i2 == 2131363190) {
                ll5 ll5 = this.g;
                if (ll5 != null) {
                    MFUser b2 = zm4.p.a().n().b();
                    wg6.a((Object) b2, "ProviderManager.getInsta\u2026serProvider().currentUser");
                    ll5.a(b2);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
        }
    }
}
