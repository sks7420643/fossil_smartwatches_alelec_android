package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.l24;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yd6;
import com.fossil.yz4;
import com.fossil.yz4$c$a;
import com.fossil.yz4$d$a;
import com.fossil.yz4$e$a;
import com.fossil.yz4$f$a;
import com.fossil.yz4$g$a;
import com.fossil.yz4$g$b;
import com.fossil.zl6;
import com.j256.ormlite.logger.Logger;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseViewModel extends l24 {
    @DexIgnore
    public MutableLiveData<yz4.b> f; // = new MutableLiveData<>();
    @DexIgnore
    public b g; // = new b((List) null, (Integer) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, 255, (qg6) null);
    @DexIgnore
    public /* final */ QuickResponseRepository h;
    @DexIgnore
    public /* final */ SetReplyMessageMappingUseCase i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Boolean b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public b() {
            this((List) null, (Integer) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, 255, (qg6) null);
        }

        @DexIgnore
        public b(List<QuickResponseMessage> list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6) {
            this.a = num;
            this.b = bool;
            this.c = bool2;
            this.d = bool3;
            this.e = bool4;
            this.f = bool5;
            this.g = bool6;
        }

        @DexIgnore
        public final Boolean a() {
            return this.g;
        }

        @DexIgnore
        public final Boolean b() {
            return this.f;
        }

        @DexIgnore
        public final Boolean c() {
            return this.d;
        }

        @DexIgnore
        public final Boolean d() {
            return this.b;
        }

        @DexIgnore
        public final Boolean e() {
            return this.c;
        }

        @DexIgnore
        public final Boolean f() {
            return this.e;
        }

        @DexIgnore
        public final Integer g() {
            return this.a;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(List list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, int i, qg6 qg6) {
            this(r1, (r0 & 2) != 0 ? null : num, (r0 & 4) != 0 ? null : bool, (r0 & 8) != 0 ? null : bool2, (r0 & 16) != 0 ? false : bool3, (r0 & 32) != 0 ? false : bool4, (r0 & 64) != 0 ? null : bool5, (r0 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 0 ? bool6 : r2);
            int i2 = i;
            Boolean bool7 = null;
            List list2 = (i2 & 1) != 0 ? null : list;
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, List list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, int i, Object obj) {
            int i2 = i;
            bVar.a((i2 & 1) != 0 ? new ArrayList() : list, (i2 & 2) != 0 ? 0 : num, (i2 & 4) != 0 ? false : bool, (i2 & 8) != 0 ? false : bool2, (i2 & 16) != 0 ? false : bool3, (i2 & 32) != 0 ? false : bool4, (i2 & 64) != 0 ? false : bool5, (i2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? false : bool6);
        }

        @DexIgnore
        public final void a(List<QuickResponseMessage> list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6) {
            this.a = num;
            this.b = bool;
            this.c = bool2;
            this.d = bool3;
            this.e = bool4;
            this.f = bool5;
            this.g = bool6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$addResponse$1", f = "QuickResponseViewModel.kt", l = {54}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $text;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(QuickResponseViewModel quickResponseViewModel, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = quickResponseViewModel;
            this.$text = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$text, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                yz4$c$a yz4_c_a = new yz4$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, yz4_c_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$backPressHandle$1", f = "QuickResponseViewModel.kt", l = {98}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $messages;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(QuickResponseViewModel quickResponseViewModel, ArrayList arrayList, xe6 xe6) {
            super(2, xe6);
            this.this$0 = quickResponseViewModel;
            this.$messages = arrayList;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$messages, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                yz4$d$a yz4_d_a = new yz4$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, yz4_d_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!wg6.a((Object) (List) obj, (Object) yd6.m(this.$messages))) {
                b.a(this.this$0.g, (List) null, (Integer) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, hf6.a(true), 127, (Object) null);
                this.this$0.e();
            } else {
                b.a(this.this$0.g, (List) null, (Integer) null, (Boolean) null, (Boolean) null, hf6.a(true), (Boolean) null, (Boolean) null, (Boolean) null, 239, (Object) null);
                this.this$0.e();
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$removeResponse$1", f = "QuickResponseViewModel.kt", l = {68}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseMessage $qr;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(QuickResponseViewModel quickResponseViewModel, QuickResponseMessage quickResponseMessage, xe6 xe6) {
            super(2, xe6);
            this.this$0 = quickResponseViewModel;
            this.$qr = quickResponseMessage;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$qr, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                yz4$e$a yz4_e_a = new yz4$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, yz4_e_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$setQuickResponseMessages$1", f = "QuickResponseViewModel.kt", l = {78}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(QuickResponseViewModel quickResponseViewModel, xe6 xe6) {
            super(2, xe6);
            this.this$0 = quickResponseViewModel;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                yz4$f$a yz4_f_a = new yz4$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, yz4_f_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$setReplyMessageToDevice$1", f = "QuickResponseViewModel.kt", l = {118}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $messages;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(QuickResponseViewModel quickResponseViewModel, ArrayList arrayList, xe6 xe6) {
            super(2, xe6);
            this.this$0 = quickResponseViewModel;
            this.$messages = arrayList;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$messages, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v2, types: [com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.yz4$g$b] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                l24.a(this.this$0, true, false, 2, (Object) null);
                dl6 b = zl6.b();
                yz4$g$a yz4_g_a = new yz4$g$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, yz4_g_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.a(new SetReplyMessageMappingUseCase.b(this.$messages), new yz4$g$b(this));
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public QuickResponseViewModel(QuickResponseRepository quickResponseRepository, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase) {
        wg6.b(quickResponseRepository, "mQRRepository");
        wg6.b(setReplyMessageMappingUseCase, "mReplyMessageUseCase");
        this.h = quickResponseRepository;
        this.i = setReplyMessageMappingUseCase;
    }

    @DexIgnore
    public final void e() {
        this.f.a(this.g);
    }

    @DexIgnore
    public final LiveData<List<QuickResponseMessage>> f() {
        return this.h.getAllQuickResponseLiveData();
    }

    @DexIgnore
    public final MutableLiveData<yz4.b> g() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean b(ArrayList<QuickResponseMessage> arrayList) {
        int i2 = 0;
        for (T next : arrayList) {
            int i3 = i2 + 1;
            if (i2 < 0) {
                qd6.c();
                throw null;
            } else if (xj6.a(((QuickResponseMessage) next).getResponse())) {
                return true;
            } else {
                i2 = i3;
            }
        }
        return false;
    }

    @DexIgnore
    public final void c(ArrayList<QuickResponseMessage> arrayList) {
        ArrayList<QuickResponseMessage> arrayList2 = arrayList;
        wg6.b(arrayList2, "messages");
        if (b(arrayList)) {
            b.a(this.g, (List) null, (Integer) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, true, (Boolean) null, 191, (Object) null);
            e();
            return;
        }
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new g(this, arrayList2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(String str, int i2) {
        wg6.b(str, "text");
        int length = str.length();
        b.a(this.g, (List) null, Integer.valueOf(length), Boolean.valueOf(length >= i2), (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, 240, (Object) null);
        e();
    }

    @DexIgnore
    public final void a(String str, int i2, int i3) {
        String str2 = str;
        wg6.b(str2, "text");
        if (i2 >= i3) {
            b.a(this.g, (List) null, (Integer) null, (Boolean) null, true, (Boolean) null, (Boolean) null, (Boolean) null, (Boolean) null, 240, (Object) null);
            e();
        } else if (true ^ xj6.a(str)) {
            rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new c(this, str2, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void a(QuickResponseMessage quickResponseMessage) {
        wg6.b(quickResponseMessage, "qr");
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new e(this, quickResponseMessage, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(ArrayList<QuickResponseMessage> arrayList) {
        wg6.b(arrayList, "messages");
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new d(this, arrayList, (xe6) null), 3, (Object) null);
    }
}
