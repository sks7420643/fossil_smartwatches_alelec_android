package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.f64;
import com.fossil.kb;
import com.fossil.pc5;
import com.fossil.qc5;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewMonthFragment extends BaseFragment implements qc5 {
    @DexIgnore
    public ax5<f64> f;
    @DexIgnore
    public pc5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewMonthFragment a;

        @DexIgnore
        public b(ActivityOverviewMonthFragment activityOverviewMonthFragment) {
            this.a = activityOverviewMonthFragment;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            wg6.b(calendar, "calendar");
            pc5 a2 = this.a.g;
            if (a2 != null) {
                Date time = calendar.getTime();
                wg6.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewMonthFragment a;

        @DexIgnore
        public c(ActivityOverviewMonthFragment activityOverviewMonthFragment) {
            this.a = activityOverviewMonthFragment;
        }

        @DexIgnore
        public void a(int i, Calendar calendar) {
            wg6.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ActivityDetailActivity.a aVar = ActivityDetailActivity.D;
                Date time = calendar.getTime();
                wg6.a((Object) time, "it.time");
                wg6.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "ActivityOverviewMonthFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        f64 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        ax5<f64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            pc5 pc5 = this.g;
            if ((pc5 != null ? pc5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                recyclerViewCalendar.b("dianaStepsTab");
            } else {
                recyclerViewCalendar.b("hybridStepsTab");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        f64 a2;
        wg6.b(layoutInflater, "inflater");
        ActivityOverviewMonthFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onCreateView");
        f64 a3 = kb.a(layoutInflater, 2131558502, viewGroup, false, e1());
        RecyclerViewCalendar recyclerViewCalendar = a3.q;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        a3.q.setOnCalendarMonthChanged(new b(this));
        a3.q.setOnCalendarItemClickListener(new c(this));
        this.f = new ax5<>(this, a3);
        j1();
        ax5<f64> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        ActivityOverviewMonthFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onResume");
        j1();
        pc5 pc5 = this.g;
        if (pc5 != null) {
            pc5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        ActivityOverviewMonthFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onStop");
        pc5 pc5 = this.g;
        if (pc5 != null) {
            pc5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(TreeMap<Long, Float> treeMap) {
        f64 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        wg6.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ax5<f64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setTintColor(w6.a(PortfolioApp.get.instance(), R.color.steps));
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(true);
        }
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        f64 a2;
        wg6.b(date, "selectDate");
        wg6.b(date2, "startDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ax5<f64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            wg6.a((Object) instance, "selectCalendar");
            instance.setTime(date);
            wg6.a((Object) instance2, "startCalendar");
            instance2.setTime(bk4.o(date2));
            wg6.a((Object) instance3, "endCalendar");
            instance3.setTime(bk4.j(instance3.getTime()));
            a2.q.a(instance, instance2, instance3);
        }
    }

    @DexIgnore
    public void a(pc5 pc5) {
        wg6.b(pc5, "presenter");
        this.g = pc5;
    }
}
