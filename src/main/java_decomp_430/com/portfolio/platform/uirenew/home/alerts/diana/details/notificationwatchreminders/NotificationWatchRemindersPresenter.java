package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.text.SpannableString;
import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.al4;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.h05;
import com.fossil.hh6;
import com.fossil.i05;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m05$b$a;
import com.fossil.m05$b$b;
import com.fossil.m05$c$a;
import com.fossil.m05$d$a;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationWatchRemindersPresenter extends h05 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public boolean e; // = this.p.R();
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i; // = this.e;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public /* final */ LiveData<List<InactivityNudgeTimeModel>> m; // = this.q.getInactivityNudgeTimeDao().getListInactivityNudgeTime();
    @DexIgnore
    public /* final */ LiveData<RemindTimeModel> n; // = this.q.getRemindTimeDao().getRemindTime();
    @DexIgnore
    public /* final */ i05 o;
    @DexIgnore
    public /* final */ an4 p;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1", f = "NotificationWatchRemindersPresenter.kt", l = {151, 160}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00fd, code lost:
            r2 = com.fossil.hf6.a((short) (r2 = com.fossil.hf6.a(r3.getMinutes())).intValue());
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0108  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x010e  */
        public final Object invokeSuspend(Object obj) {
            boolean z;
            hh6 hh6;
            Object obj2;
            hh6 hh62;
            Integer a;
            Short a2;
            Object obj3;
            il6 il6;
            boolean z2;
            Object a3 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                hh62 = new hh6();
                hh62.element = 0;
                hh6 = new hh6();
                hh6.element = 0;
                z2 = this.this$0.i;
                dl6 a4 = this.this$0.c();
                m05$b$a m05_b_a = new m05$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = hh62;
                this.L$2 = hh6;
                this.Z$0 = z2;
                this.label = 1;
                obj3 = gk6.a(a4, m05_b_a, this);
                if (obj3 == a3) {
                    return a3;
                }
            } else if (i == 1) {
                boolean z3 = this.Z$0;
                hh6 = (hh6) this.L$2;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj3 = obj;
                z2 = z3;
                hh62 = (hh6) this.L$1;
            } else if (i == 2) {
                List list = (List) this.L$3;
                boolean z4 = this.Z$0;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                z = z4;
                hh6 = (hh6) this.L$2;
                hh62 = (hh6) this.L$1;
                obj2 = obj;
                RemindTimeModel remindTimeModel = (RemindTimeModel) obj2;
                PortfolioApp instance = PortfolioApp.get.instance();
                String e = PortfolioApp.get.instance().e();
                int i2 = hh62.element;
                byte b = (byte) (i2 / 60);
                byte b2 = (byte) (i2 % 60);
                int i3 = hh6.element;
                byte b3 = (byte) (i3 / 60);
                byte b4 = (byte) (i3 % 60);
                short shortValue = (remindTimeModel == null || a == null || a2 == null) ? 0 : a2.shortValue();
                instance.a(e, new InactiveNudgeData(b, b2, b3, b4, shortValue, z));
                this.this$0.p.v(this.this$0.i);
                this.this$0.p.w(this.this$0.j);
                this.this$0.p.x(this.this$0.k);
                this.this$0.p.u(this.this$0.l);
                this.this$0.o.close();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<InactivityNudgeTimeModel> list2 = (List) obj3;
            for (InactivityNudgeTimeModel inactivityNudgeTimeModel : list2) {
                if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                    hh62.element = inactivityNudgeTimeModel.getMinutes();
                } else if (inactivityNudgeTimeModel.getNudgeTimeType() == 1) {
                    hh6.element = inactivityNudgeTimeModel.getMinutes();
                }
            }
            dl6 a5 = this.this$0.c();
            m05$b$b m05_b_b = new m05$b$b(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = hh62;
            this.L$2 = hh6;
            this.Z$0 = z2;
            this.L$3 = list2;
            this.label = 2;
            obj2 = gk6.a(a5, m05_b_b, this);
            if (obj2 == a3) {
                return a3;
            }
            z = z2;
            RemindTimeModel remindTimeModel2 = (RemindTimeModel) obj2;
            PortfolioApp instance2 = PortfolioApp.get.instance();
            String e2 = PortfolioApp.get.instance().e();
            int i22 = hh62.element;
            byte b5 = (byte) (i22 / 60);
            byte b22 = (byte) (i22 % 60);
            int i32 = hh6.element;
            byte b32 = (byte) (i32 / 60);
            byte b42 = (byte) (i32 % 60);
            if (remindTimeModel2 == null || a == null || a2 == null) {
            }
            instance2.a(e2, new InactiveNudgeData(b5, b22, b32, b42, shortValue, z));
            this.this$0.p.v(this.this$0.i);
            this.this$0.p.w(this.this$0.j);
            this.this$0.p.x(this.this$0.k);
            this.this$0.p.u(this.this$0.l);
            this.this$0.o.close();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public c(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<InactivityNudgeTimeModel> list) {
            if (list == null || list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                InactivityNudgeTimeModel inactivityNudgeTimeModel = new InactivityNudgeTimeModel("Start", 660, 0);
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = new InactivityNudgeTimeModel("End", 1260, 1);
                arrayList.add(inactivityNudgeTimeModel);
                arrayList.add(inactivityNudgeTimeModel2);
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new m05$c$a(this, arrayList, (xe6) null), 3, (Object) null);
                return;
            }
            for (InactivityNudgeTimeModel inactivityNudgeTimeModel3 : list) {
                if (inactivityNudgeTimeModel3.getNudgeTimeType() == 0) {
                    i05 h = this.a.o;
                    SpannableString e = al4.e(inactivityNudgeTimeModel3.getMinutes());
                    wg6.a((Object) e, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    h.c(e);
                } else {
                    i05 h2 = this.a.o;
                    SpannableString e2 = al4.e(inactivityNudgeTimeModel3.getMinutes());
                    wg6.a((Object) e2, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    h2.d(e2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public d(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(RemindTimeModel remindTimeModel) {
            if (remindTimeModel == null) {
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new m05$d$a(this, new RemindTimeModel("RemindTime", 20), (xe6) null), 3, (Object) null);
                return;
            }
            i05 h = this.a.o;
            String d = al4.d(remindTimeModel.getMinutes());
            wg6.a((Object) d, "TimeUtils.getRemindTimeS\u2026(remindTimeModel.minutes)");
            h.N(d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public e(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<InactivityNudgeTimeModel> list) {
            i05 unused = this.a.o;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public f(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(RemindTimeModel remindTimeModel) {
            i05 unused = this.a.o;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = NotificationWatchRemindersPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationWatchReminde\u2026er::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public NotificationWatchRemindersPresenter(i05 i05, an4 an4, RemindersSettingsDatabase remindersSettingsDatabase) {
        wg6.b(i05, "mView");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.o = i05;
        this.p = an4;
        this.q = remindersSettingsDatabase;
    }

    @DexIgnore
    public void j() {
        this.j = !this.j;
        if (m()) {
            this.o.d(true);
        } else {
            this.o.d(false);
        }
    }

    @DexIgnore
    public void k() {
        this.k = !this.k;
        if (m()) {
            this.o.d(true);
        } else {
            this.o.d(false);
        }
    }

    @DexIgnore
    public void l() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean m() {
        return (this.i == this.e && this.j == this.f && this.k == this.g && this.l == this.h) ? false : true;
    }

    @DexIgnore
    public void n() {
        this.o.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(r, "start");
        LiveData<List<InactivityNudgeTimeModel>> liveData = this.m;
        i05 i05 = this.o;
        if (i05 != null) {
            liveData.a((NotificationWatchRemindersFragment) i05, new c(this));
            this.n.a(this.o, new d(this));
            this.f = this.p.S();
            this.j = this.f;
            this.g = this.p.T();
            this.k = this.g;
            this.h = this.p.Q();
            this.l = this.h;
            this.o.G(this.i);
            this.o.z(this.j);
            this.o.F(this.k);
            this.o.x(this.l);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        this.m.b(new e(this));
        this.n.b(new f(this));
    }

    @DexIgnore
    public void h() {
        this.l = !this.l;
        if (m()) {
            this.o.d(true);
        } else {
            this.o.d(false);
        }
    }

    @DexIgnore
    public void i() {
        this.i = !this.i;
        if (m()) {
            this.o.d(true);
        } else {
            this.o.d(false);
        }
        this.o.G(this.i);
    }
}
