package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.rc6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoActivity extends BaseActivity {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (getSupportFragmentManager().b(2131362119) == null && (intent = getIntent()) != null) {
            ArrayList arrayList = null;
            Uri uri = intent.hasExtra("IMAGE_URI_EXTRA") ? (Uri) intent.getParcelableExtra("IMAGE_URI_EXTRA") : null;
            if (intent.hasExtra("COMPLICATION_EXTRA")) {
                arrayList = intent.getParcelableArrayListExtra("COMPLICATION_EXTRA");
            }
            if (uri == null || arrayList == null) {
                FLogger.INSTANCE.getLocal().d(f(), "Can not start EditPhotoFragment with invalid params");
                return;
            }
            EditPhotoFragment a2 = EditPhotoFragment.s.a(uri, arrayList);
            if (a2 != null) {
                a((Fragment) a2, "EditPhotoFragment", 2131362119);
                return;
            }
            throw new rc6("null cannot be cast to non-null type androidx.fragment.app.Fragment");
        }
    }
}
