package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.be4;
import com.fossil.kb;
import com.fossil.lg5;
import com.fossil.mg;
import com.fossil.qg6;
import com.fossil.uz5;
import com.fossil.wg6;
import com.fossil.x9;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewFragment extends BaseFragment {
    @DexIgnore
    public ax5<be4> f;
    @DexIgnore
    public SleepOverviewDayPresenter g;
    @DexIgnore
    public SleepOverviewWeekPresenter h;
    @DexIgnore
    public SleepOverviewMonthPresenter i;
    @DexIgnore
    public SleepOverviewDayFragment j;
    @DexIgnore
    public SleepOverviewWeekFragment o;
    @DexIgnore
    public SleepOverviewMonthFragment p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment a;

        @DexIgnore
        public b(SleepOverviewFragment sleepOverviewFragment) {
            this.a = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.a;
            ax5 a2 = sleepOverviewFragment.f;
            sleepOverviewFragment.a(7, a2 != null ? (be4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment a;

        @DexIgnore
        public c(SleepOverviewFragment sleepOverviewFragment) {
            this.a = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.a;
            ax5 a2 = sleepOverviewFragment.f;
            sleepOverviewFragment.a(4, a2 != null ? (be4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment a;

        @DexIgnore
        public d(SleepOverviewFragment sleepOverviewFragment) {
            this.a = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.a;
            ax5 a2 = sleepOverviewFragment.f;
            sleepOverviewFragment.a(2, a2 != null ? (be4) a2.a() : null);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "SleepOverviewFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void j1() {
        String str;
        String str2;
        String str3;
        ax5<be4> ax5;
        be4 a2;
        ax5<be4> ax52;
        be4 a3;
        ax5<be4> ax53;
        be4 a4;
        ax5<be4> ax54;
        be4 a5;
        SleepOverviewDayPresenter sleepOverviewDayPresenter = this.g;
        if (sleepOverviewDayPresenter != null) {
            if (sleepOverviewDayPresenter.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                str = ThemeManager.l.a().b("dianaSleepTab");
            } else {
                str = ThemeManager.l.a().b("hybridSleepTab");
            }
            SleepOverviewDayPresenter sleepOverviewDayPresenter2 = this.g;
            if (sleepOverviewDayPresenter2 != null) {
                if (sleepOverviewDayPresenter2.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    str2 = ThemeManager.l.a().b("onDianaSleepTab");
                } else {
                    str2 = ThemeManager.l.a().b("onHybridSleepTab");
                }
                if (!(str == null || (ax54 = this.f) == null || (a5 = ax54.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(str));
                    a5.y.setBackgroundColor(Color.parseColor(str));
                }
                if (!(str2 == null || (ax53 = this.f) == null || (a4 = ax53.a()) == null)) {
                    a4.t.setTextColor(Color.parseColor(str2));
                }
                SleepOverviewDayPresenter sleepOverviewDayPresenter3 = this.g;
                if (sleepOverviewDayPresenter3 != null) {
                    if (sleepOverviewDayPresenter3.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                        str3 = ThemeManager.l.a().b("onDianaInactiveTab");
                    } else {
                        str3 = ThemeManager.l.a().b("onHybridInactiveTab");
                    }
                    this.r = str3;
                    String b2 = ThemeManager.l.a().b("nonBrandSurface");
                    this.s = ThemeManager.l.a().b("primaryText");
                    if (!(b2 == null || (ax52 = this.f) == null || (a3 = ax52.a()) == null)) {
                        a3.q.setBackgroundColor(Color.parseColor(b2));
                    }
                    if (!TextUtils.isEmpty(this.r) && !TextUtils.isEmpty(this.s) && (ax5 = this.f) != null && (a2 = ax5.a()) != null) {
                        Object r1 = a2.u;
                        wg6.a((Object) r1, "it.ftvToday");
                        if (r1.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.s));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.r));
                        }
                        Object r12 = a2.r;
                        wg6.a((Object) r12, "it.ftv7Days");
                        if (r12.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.s));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.r));
                        }
                        Object r13 = a2.s;
                        wg6.a((Object) r13, "it.ftvMonth");
                        if (r13.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.s));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.r));
                        }
                    }
                } else {
                    wg6.d("mSleepOverviewDayPresenter");
                    throw null;
                }
            } else {
                wg6.d("mSleepOverviewDayPresenter");
                throw null;
            }
        } else {
            wg6.d("mSleepOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        be4 a2;
        wg6.b(layoutInflater, "inflater");
        SleepOverviewFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onCreateView");
        be4 a3 = kb.a(layoutInflater, 2131558611, viewGroup, false, e1());
        x9.d(a3.w, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wg6.a((Object) a3, "binding");
        a(a3);
        this.f = new ax5<>(this, a3);
        j1();
        ax5<be4> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        SleepOverviewFragment.super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r10v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(be4 be4) {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "initUI");
        this.j = getChildFragmentManager().b("SleepOverviewDayFragment");
        this.o = getChildFragmentManager().b("SleepOverviewWeekFragment");
        this.p = getChildFragmentManager().b("SleepOverviewMonthFragment");
        if (this.j == null) {
            this.j = new SleepOverviewDayFragment();
        }
        if (this.o == null) {
            this.o = new SleepOverviewWeekFragment();
        }
        if (this.p == null) {
            this.p = new SleepOverviewMonthFragment();
        }
        ArrayList arrayList = new ArrayList();
        SleepOverviewDayFragment sleepOverviewDayFragment = this.j;
        if (sleepOverviewDayFragment != null) {
            arrayList.add(sleepOverviewDayFragment);
            SleepOverviewWeekFragment sleepOverviewWeekFragment = this.o;
            if (sleepOverviewWeekFragment != null) {
                arrayList.add(sleepOverviewWeekFragment);
                SleepOverviewMonthFragment sleepOverviewMonthFragment = this.p;
                if (sleepOverviewMonthFragment != null) {
                    arrayList.add(sleepOverviewMonthFragment);
                    RecyclerView recyclerView = be4.w;
                    wg6.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new uz5(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new SleepOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mg().a(recyclerView);
                    a(this.q, be4);
                    y04 g2 = PortfolioApp.get.instance().g();
                    SleepOverviewDayFragment sleepOverviewDayFragment2 = this.j;
                    if (sleepOverviewDayFragment2 != null) {
                        SleepOverviewWeekFragment sleepOverviewWeekFragment2 = this.o;
                        if (sleepOverviewWeekFragment2 != null) {
                            SleepOverviewMonthFragment sleepOverviewMonthFragment2 = this.p;
                            if (sleepOverviewMonthFragment2 != null) {
                                g2.a(new lg5(sleepOverviewDayFragment2, sleepOverviewWeekFragment2, sleepOverviewMonthFragment2)).a(this);
                                be4.u.setOnClickListener(new b(this));
                                be4.r.setOnClickListener(new c(this));
                                be4.s.setOnClickListener(new d(this));
                                return;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v29, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void a(int i2, be4 be4) {
        ax5<be4> ax5;
        be4 a2;
        be4 a3;
        RecyclerView recyclerView;
        be4 a4;
        RecyclerView recyclerView2;
        be4 a5;
        RecyclerView recyclerView3;
        be4 a6;
        RecyclerView recyclerView4;
        if (be4 != null) {
            Object r0 = be4.u;
            wg6.a((Object) r0, "it.ftvToday");
            r0.setSelected(false);
            Object r02 = be4.r;
            wg6.a((Object) r02, "it.ftv7Days");
            r02.setSelected(false);
            Object r03 = be4.s;
            wg6.a((Object) r03, "it.ftvMonth");
            r03.setSelected(false);
            Object r04 = be4.u;
            wg6.a((Object) r04, "it.ftvToday");
            r04.setPaintFlags(0);
            Object r05 = be4.r;
            wg6.a((Object) r05, "it.ftv7Days");
            r05.setPaintFlags(0);
            Object r06 = be4.s;
            wg6.a((Object) r06, "it.ftvMonth");
            r06.setPaintFlags(0);
            if (i2 == 2) {
                Object r7 = be4.s;
                wg6.a((Object) r7, "it.ftvMonth");
                r7.setSelected(true);
                Object r72 = be4.s;
                wg6.a((Object) r72, "it.ftvMonth");
                Object r8 = be4.r;
                wg6.a((Object) r8, "it.ftv7Days");
                r72.setPaintFlags(r8.getPaintFlags() | 8 | 1);
                ax5<be4> ax52 = this.f;
                if (!(ax52 == null || (a3 = ax52.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                Object r73 = be4.r;
                wg6.a((Object) r73, "it.ftv7Days");
                r73.setSelected(true);
                Object r74 = be4.r;
                wg6.a((Object) r74, "it.ftv7Days");
                Object r82 = be4.r;
                wg6.a((Object) r82, "it.ftv7Days");
                r74.setPaintFlags(r82.getPaintFlags() | 8 | 1);
                ax5<be4> ax53 = this.f;
                if (!(ax53 == null || (a4 = ax53.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                Object r75 = be4.u;
                wg6.a((Object) r75, "it.ftvToday");
                r75.setSelected(true);
                Object r76 = be4.u;
                wg6.a((Object) r76, "it.ftvToday");
                Object r83 = be4.r;
                wg6.a((Object) r83, "it.ftv7Days");
                r76.setPaintFlags(r83.getPaintFlags() | 8 | 1);
                ax5<be4> ax54 = this.f;
                if (!(ax54 == null || (a6 = ax54.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                Object r77 = be4.u;
                wg6.a((Object) r77, "it.ftvToday");
                r77.setSelected(true);
                Object r78 = be4.u;
                wg6.a((Object) r78, "it.ftvToday");
                Object r84 = be4.r;
                wg6.a((Object) r84, "it.ftv7Days");
                r78.setPaintFlags(r84.getPaintFlags() | 8 | 1);
                ax5<be4> ax55 = this.f;
                if (!(ax55 == null || (a5 = ax55.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.r) && !TextUtils.isEmpty(this.s) && (ax5 = this.f) != null && (a2 = ax5.a()) != null) {
                Object r85 = a2.u;
                wg6.a((Object) r85, "it.ftvToday");
                if (r85.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.s));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.r));
                }
                Object r86 = a2.r;
                wg6.a((Object) r86, "it.ftv7Days");
                if (r86.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.s));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.r));
                }
                Object r87 = a2.s;
                wg6.a((Object) r87, "it.ftvMonth");
                if (r87.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.s));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.r));
                }
            }
        }
    }
}
