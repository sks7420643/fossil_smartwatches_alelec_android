package com.portfolio.platform.uirenew.home;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.al4;
import com.fossil.ax5;
import com.fossil.bl4;
import com.fossil.bu4;
import com.fossil.cd6;
import com.fossil.fr;
import com.fossil.hh6;
import com.fossil.ik4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.na5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rh6;
import com.fossil.sh4;
import com.fossil.ua5;
import com.fossil.uw4;
import com.fossil.uz5;
import com.fossil.va5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wq;
import com.fossil.xj6;
import com.fossil.y04;
import com.fossil.za4;
import com.fossil.zz5;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDashboardFragment extends BasePermissionFragment implements va5, bu4, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a W; // = new a((qg6) null);
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public DashboardActivityPresenter E;
    @DexIgnore
    public DashboardActiveTimePresenter F;
    @DexIgnore
    public DashboardCaloriesPresenter G;
    @DexIgnore
    public DashboardHeartRatePresenter H;
    @DexIgnore
    public DashboardSleepPresenter I;
    @DexIgnore
    public DashboardGoalTrackingPresenter J;
    @DexIgnore
    public ax5<za4> K;
    @DexIgnore
    public ua5 L;
    @DexIgnore
    public /* final */ ArrayList<Fragment> M; // = new ArrayList<>();
    @DexIgnore
    public int N;
    @DexIgnore
    public zz5 O;
    @DexIgnore
    public ObjectAnimator P;
    @DexIgnore
    public int Q; // = -1;
    @DexIgnore
    public fr R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public HashMap V;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeDashboardFragment a() {
            return new HomeDashboardFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar b;

        @DexIgnore
        public b(HomeDashboardFragment homeDashboardFragment, ProgressBar progressBar) {
            this.a = homeDashboardFragment;
            this.b = progressBar;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.b.setProgress(0);
            this.b.setVisibility(4);
            this.a.Q = -1;
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ za4 a;
        @DexIgnore
        public /* final */ /* synthetic */ Animation b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r4v3, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
            /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
            public void onImageCallback(String str, String str2) {
                wg6.b(str, "serial");
                wg6.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardFragment", "onImageCallback, filePath=" + str2);
                if (!TextUtils.isEmpty(str2)) {
                    FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onImageCallback, new logo");
                    HomeDashboardFragment.c(this.a.c).a(str2).a(this.a.a.I);
                    c cVar = this.a;
                    cVar.a.I.startAnimation(cVar.b);
                }
            }
        }

        @DexIgnore
        public c(za4 za4, Animation animation, HomeDashboardFragment homeDashboardFragment) {
            this.a = za4;
            this.b = animation;
            this.c = homeDashboardFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "activeDeviceSerialLiveData onChange " + str);
            if (!TextUtils.isEmpty(str)) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                wg6.a((Object) str, "serial");
                with.setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.DeviceType.TYPE_BRAND_LOGO).setImageCallback(new a(this)).download();
                return;
            }
            this.a.I.setImageResource(2131231256);
            this.a.I.startAnimation(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public d(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.b(this.a).b(4);
            HomeDashboardFragment.b(this.a).a(4);
            this.a.s(4);
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public f(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wg6.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 4, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public g(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final boolean onLongClick(View view) {
            DebugActivity.a aVar = DebugActivity.P;
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                aVar.a(activity);
                return false;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public h(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.b(this.a).b(0);
            HomeDashboardFragment.b(this.a).a(0);
            this.a.s(0);
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public i(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.b(this.a).b(1);
            HomeDashboardFragment.b(this.a).a(1);
            this.a.s(1);
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public j(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.b(this.a).b(2);
            HomeDashboardFragment.b(this.a).a(2);
            this.a.s(2);
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public k(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.b(this.a).b(3);
            HomeDashboardFragment.b(this.a).a(3);
            this.a.s(3);
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment a;

        @DexIgnore
        public l(HomeDashboardFragment homeDashboardFragment) {
            this.a = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.b(this.a).b(5);
            HomeDashboardFragment.b(this.a).a(5);
            this.a.s(5);
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements AppBarLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ za4 a;

        @DexIgnore
        public m(za4 za4) {
            this.a = za4;
        }

        @DexIgnore
        public void a(AppBarLayout appBarLayout, int i) {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a.R;
            wg6.a((Object) customSwipeRefreshLayout, "binding.srlPullToSync");
            customSwipeRefreshLayout.setEnabled(Math.abs(i) == 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 b;

        @DexIgnore
        public n(hh6 hh6, FlexibleProgressBar flexibleProgressBar, hh6 hh62, HomeDashboardFragment homeDashboardFragment, int i) {
            this.a = flexibleProgressBar;
            this.b = hh62;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FlexibleProgressBar flexibleProgressBar = this.a;
            wg6.a((Object) flexibleProgressBar, "it");
            int i = this.b.element;
            wg6.a((Object) valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                flexibleProgressBar.setProgress(i + ((Integer) animatedValue).intValue());
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public o(hh6 hh6, FlexibleProgressBar flexibleProgressBar, hh6 hh62, HomeDashboardFragment homeDashboardFragment, int i) {
            this.a = flexibleProgressBar;
            this.b = homeDashboardFragment;
            this.c = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            wg6.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            wg6.b(animator, "animation");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "onAnimationEnd " + this.c);
            HomeDashboardFragment homeDashboardFragment = this.b;
            FlexibleProgressBar flexibleProgressBar = this.a;
            wg6.a((Object) flexibleProgressBar, "it");
            homeDashboardFragment.a((ProgressBar) flexibleProgressBar, this.c);
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            wg6.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            wg6.b(animator, "animation");
        }
    }

    @DexIgnore
    public HomeDashboardFragment() {
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        this.S = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("hybridInactiveTab");
        this.T = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        this.U = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
    }

    @DexIgnore
    public static final /* synthetic */ ua5 b(HomeDashboardFragment homeDashboardFragment) {
        ua5 ua5 = homeDashboardFragment.L;
        if (ua5 != null) {
            return ua5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ fr c(HomeDashboardFragment homeDashboardFragment) {
        fr frVar = homeDashboardFragment.R;
        if (frVar != null) {
            return frVar;
        }
        wg6.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public void A() {
        AppBarLayout appBarLayout;
        if (isActive()) {
            ax5<za4> ax5 = this.K;
            if (ax5 != null) {
                za4 a2 = ax5.a();
                if (a2 != null && (appBarLayout = a2.q) != null) {
                    appBarLayout.a(true, true);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void E() {
        if (isActive()) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.c(childFragmentManager);
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
        }
    }

    @DexIgnore
    public void Q(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("HomeDashboardFragment visible=");
        sb.append(z2);
        sb.append(", tracer=");
        sb.append(g1());
        sb.append(", isRunning=");
        kl4 g1 = g1();
        sb.append(g1 != null ? Boolean.valueOf(g1.b()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z2) {
            kl4 g12 = g1();
            if (g12 != null) {
                g12.d();
            }
            if (this.K != null) {
                int i2 = this.Q;
                if (i2 == 2 || i2 == -1) {
                    k1();
                    return;
                }
                return;
            }
            return;
        }
        kl4 g13 = g1();
        if (g13 != null) {
            g13.a("");
        }
    }

    @DexIgnore
    public final void R(boolean z2) {
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                if (z2) {
                    FlexibleProgressBar flexibleProgressBar = a2.S;
                    wg6.a((Object) flexibleProgressBar, "syncProgress");
                    flexibleProgressBar.setVisibility(0);
                } else {
                    FlexibleProgressBar flexibleProgressBar2 = a2.S;
                    wg6.a((Object) flexibleProgressBar2, "syncProgress");
                    flexibleProgressBar2.setVisibility(4);
                }
                View view = a2.U;
                wg6.a((Object) view, "vBorderBottom");
                view.setVisibility(4);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void Y() {
        if (isActive()) {
            ax5<za4> ax5 = this.K;
            if (ax5 != null) {
                za4 a2 = ax5.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.w;
                    wg6.a((Object) constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(0);
                    FlexibleProgressBar flexibleProgressBar = a2.K;
                    wg6.a((Object) flexibleProgressBar, "it.pbProgress");
                    flexibleProgressBar.setMax(100);
                    a2.R.setDisableSwipe(true);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a0() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.R(childFragmentManager);
        }
    }

    @DexIgnore
    public void b0() {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "showAutoSync");
        r(0);
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null && (customSwipeRefreshLayout = a2.R) != null) {
                customSwipeRefreshLayout.g();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.V;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "HomeDashboardFragment";
    }

    @DexIgnore
    public void i(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDashboardFragment", "syncCompleted - success: " + z2);
        if (!z2) {
            k1();
        } else {
            r(1);
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final void j1() {
        ua5 ua5 = this.L;
        if (ua5 == null) {
            return;
        }
        if (ua5 != null) {
            int i2 = ua5.i();
            if (!this.M.isEmpty()) {
                int size = this.M.size();
                int i3 = 0;
                while (i3 < size) {
                    if (this.M.get(i3) instanceof bu4) {
                        bu4 bu4 = this.M.get(i3);
                        if (bu4 != null) {
                            bu4.Q(i3 == i2);
                        } else {
                            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                        }
                    }
                    i3++;
                }
            }
            if (i2 == 0) {
                W("steps_view");
            } else if (i2 == 1) {
                W("active_minutes_view");
            } else if (i2 == 2) {
                W("calories_view");
            } else if (i2 == 3) {
                W("heart_rate_view");
            } else if (i2 == 5) {
                W("sleep_view");
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void k1() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "cancelSyncProgress");
        ObjectAnimator objectAnimator = this.P;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.S;
                wg6.a((Object) flexibleProgressBar, "it.syncProgress");
                flexibleProgressBar.setVisibility(4);
                FlexibleProgressBar flexibleProgressBar2 = a2.S;
                wg6.a((Object) flexibleProgressBar2, "it.syncProgress");
                flexibleProgressBar2.setProgress(0);
                a2.R.c();
            }
            ua5 ua5 = this.L;
            if (ua5 != null) {
                ua5.a(false);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v9, types: [com.portfolio.platform.view.FlexibleFitnessTab, java.lang.Object, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r7v10, types: [com.portfolio.platform.view.FlexibleFitnessTab, java.lang.Object, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r7v11, types: [com.portfolio.platform.view.FlexibleFitnessTab, java.lang.Object, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r7v12, types: [com.portfolio.platform.view.FlexibleFitnessTab, java.lang.Object, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r7v13, types: [com.portfolio.platform.view.FlexibleFitnessTab, java.lang.Object, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r7v14, types: [com.portfolio.platform.view.FlexibleFitnessTab, java.lang.Object, android.view.ViewGroup] */
    public final void l1() {
        DashboardActivityFragment b2 = getChildFragmentManager().b(DashboardActivityFragment.q.a());
        DashboardActiveTimeFragment b3 = getChildFragmentManager().b("DashboardActiveTimeFragment");
        DashboardCaloriesFragment b4 = getChildFragmentManager().b("DashboardCaloriesFragment");
        DashboardHeartRateFragment b5 = getChildFragmentManager().b(DashboardHeartRateFragment.q.a());
        DashboardSleepFragment b6 = getChildFragmentManager().b(DashboardSleepFragment.q.a());
        DashboardGoalTrackingFragment b7 = getChildFragmentManager().b(DashboardGoalTrackingFragment.q.a());
        if (b2 == null) {
            b2 = DashboardActivityFragment.q.b();
        }
        if (b3 == null) {
            b3 = DashboardActiveTimeFragment.p.a();
        }
        if (b4 == null) {
            b4 = new DashboardCaloriesFragment();
        }
        if (b5 == null) {
            b5 = DashboardHeartRateFragment.q.b();
        }
        if (b6 == null) {
            b6 = DashboardSleepFragment.q.b();
        }
        if (b7 == null) {
            b7 = DashboardGoalTrackingFragment.q.b();
        }
        this.M.clear();
        this.M.add(b2);
        this.M.add(b3);
        this.M.add(b4);
        this.M.add(b5);
        this.M.add(b7);
        this.M.add(b6);
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                ua5 ua5 = this.L;
                if (ua5 != null) {
                    if (ua5 == null) {
                        wg6.d("mPresenter");
                        throw null;
                    } else if (ua5.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                        Object r7 = a2.z;
                        wg6.a((Object) r7, "flexibleTabActiveTime");
                        r7.setVisibility(0);
                        Object r72 = a2.D;
                        wg6.a((Object) r72, "flexibleTabHeartRate");
                        r72.setVisibility(0);
                        Object r73 = a2.C;
                        wg6.a((Object) r73, "flexibleTabGoalTracking");
                        r73.setVisibility(8);
                    } else {
                        Object r74 = a2.z;
                        wg6.a((Object) r74, "flexibleTabActiveTime");
                        r74.setVisibility(8);
                        Object r75 = a2.D;
                        wg6.a((Object) r75, "flexibleTabHeartRate");
                        r75.setVisibility(8);
                        Object r76 = a2.C;
                        wg6.a((Object) r76, "flexibleTabGoalTracking");
                        r76.setVisibility(0);
                    }
                }
                ViewPager2 viewPager2 = a2.Q;
                wg6.a((Object) viewPager2, "rvTabs");
                viewPager2.setAdapter(new uz5(getChildFragmentManager(), this.M));
                if (a2.Q.getChildAt(0) != null) {
                    RecyclerView childAt = a2.Q.getChildAt(0);
                    if (childAt != null) {
                        childAt.setItemViewCacheSize(3);
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.Q;
                wg6.a((Object) viewPager22, "rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            y04 g2 = PortfolioApp.get.instance().g();
            if (b2 != null) {
                DashboardActivityFragment dashboardActivityFragment = b2;
                if (b3 != null) {
                    DashboardActiveTimeFragment dashboardActiveTimeFragment = b3;
                    DashboardCaloriesFragment dashboardCaloriesFragment = b4;
                    if (b5 != null) {
                        DashboardHeartRateFragment dashboardHeartRateFragment = b5;
                        if (b6 != null) {
                            DashboardSleepFragment dashboardSleepFragment = b6;
                            if (b7 != null) {
                                g2.a(new na5(dashboardActivityFragment, dashboardActiveTimeFragment, dashboardCaloriesFragment, dashboardHeartRateFragment, dashboardSleepFragment, b7)).a(this);
                                return;
                            }
                            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                        }
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v26, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void m1() {
        String b2 = ThemeManager.l.a().b("onDianaStepsTab");
        if (b2 != null) {
            this.g = Color.parseColor(b2);
            cd6 cd6 = cd6.a;
        } else {
            Context context = getContext();
            if (context != null) {
                this.g = w6.a(context, 2131099980);
                cd6 cd62 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b3 = ThemeManager.l.a().b("onDianaActiveMinutesTab");
        if (b3 != null) {
            this.h = Color.parseColor(b3);
            cd6 cd63 = cd6.a;
        } else {
            Context context2 = getContext();
            if (context2 != null) {
                this.h = w6.a(context2, 2131099976);
                cd6 cd64 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b4 = ThemeManager.l.a().b("onDianaActiveCaloriesTab");
        if (b4 != null) {
            this.i = Color.parseColor(b4);
            cd6 cd65 = cd6.a;
        } else {
            Context context3 = getContext();
            if (context3 != null) {
                this.i = w6.a(context3, 2131099975);
                cd6 cd66 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b5 = ThemeManager.l.a().b("onDianaHeartRateTab");
        if (b5 != null) {
            this.j = Color.parseColor(b5);
            cd6 cd67 = cd6.a;
        } else {
            Context context4 = getContext();
            if (context4 != null) {
                this.j = w6.a(context4, 2131099977);
                cd6 cd68 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b6 = ThemeManager.l.a().b("onDianaSleepTab");
        if (b6 != null) {
            this.o = Color.parseColor(b6);
            cd6 cd69 = cd6.a;
        } else {
            Context context5 = getContext();
            if (context5 != null) {
                this.o = w6.a(context5, 2131099979);
                cd6 cd610 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b7 = ThemeManager.l.a().b("onDianaInactiveTab");
        if (b7 != null) {
            this.p = Color.parseColor(b7);
            cd6 cd611 = cd6.a;
        } else {
            Context context6 = getContext();
            if (context6 != null) {
                this.p = w6.a(context6, 2131099978);
                cd6 cd612 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b8 = ThemeManager.l.a().b("onHybridStepsTab");
        if (b8 != null) {
            this.q = Color.parseColor(b8);
            cd6 cd613 = cd6.a;
        } else {
            Context context7 = getContext();
            if (context7 != null) {
                this.q = w6.a(context7, 2131099982);
                cd6 cd614 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b9 = ThemeManager.l.a().b("onHybridActiveCaloriesTab");
        if (b9 != null) {
            this.r = Color.parseColor(b9);
            cd6 cd615 = cd6.a;
        } else {
            Context context8 = getContext();
            if (context8 != null) {
                this.r = w6.a(context8, 2131099981);
                cd6 cd616 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b10 = ThemeManager.l.a().b("onHybridSleepTab");
        if (b10 != null) {
            this.s = Color.parseColor(b10);
            cd6 cd617 = cd6.a;
        } else {
            Context context9 = getContext();
            if (context9 != null) {
                this.s = w6.a(context9, 2131099984);
                cd6 cd618 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b11 = ThemeManager.l.a().b("onHybridInactiveTab");
        if (b11 != null) {
            this.t = Color.parseColor(b11);
            cd6 cd619 = cd6.a;
        } else {
            Context context10 = getContext();
            if (context10 != null) {
                this.t = w6.a(context10, 2131099983);
                cd6 cd620 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b12 = ThemeManager.l.a().b("onHybridGoalTrackingTab");
        if (b12 != null) {
            this.u = Color.parseColor(b12);
            cd6 cd621 = cd6.a;
        } else {
            Context context11 = getContext();
            if (context11 != null) {
                this.u = w6.a(context11, 2131099982);
                cd6 cd622 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b13 = ThemeManager.l.a().b("dianaStepsTab");
        if (b13 != null) {
            this.v = Color.parseColor(b13);
            cd6 cd623 = cd6.a;
        } else {
            Context context12 = getContext();
            if (context12 != null) {
                this.v = w6.a(context12, 2131099815);
                cd6 cd624 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b14 = ThemeManager.l.a().b("dianaActiveMinutesTab");
        if (b14 != null) {
            this.w = Color.parseColor(b14);
            cd6 cd625 = cd6.a;
        } else {
            Context context13 = getContext();
            if (context13 != null) {
                this.w = w6.a(context13, 2131099809);
                cd6 cd626 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b15 = ThemeManager.l.a().b("dianaActiveCaloriesTab");
        if (b15 != null) {
            this.x = Color.parseColor(b15);
            cd6 cd627 = cd6.a;
        } else {
            Context context14 = getContext();
            if (context14 != null) {
                this.x = w6.a(context14, 2131099807);
                cd6 cd628 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b16 = ThemeManager.l.a().b("dianaHeartRateTab");
        if (b16 != null) {
            this.y = Color.parseColor(b16);
            cd6 cd629 = cd6.a;
        } else {
            Context context15 = getContext();
            if (context15 != null) {
                this.y = w6.a(context15, 2131099810);
                cd6 cd630 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b17 = ThemeManager.l.a().b("dianaSleepTab");
        if (b17 != null) {
            this.z = Color.parseColor(b17);
            cd6 cd631 = cd6.a;
        } else {
            Context context16 = getContext();
            if (context16 != null) {
                this.z = w6.a(context16, 2131099813);
                cd6 cd632 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b18 = ThemeManager.l.a().b("dianaInactiveTab");
        if (b18 != null) {
            Color.parseColor(b18);
            cd6 cd633 = cd6.a;
        } else {
            Context context17 = getContext();
            if (context17 != null) {
                w6.a(context17, 2131099811);
                cd6 cd634 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b19 = ThemeManager.l.a().b("hybridStepsTab");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "ABCD=" + b19);
        String b20 = ThemeManager.l.a().b("hybridStepsTab");
        if (b20 != null) {
            this.A = Color.parseColor(b20);
            cd6 cd635 = cd6.a;
        } else {
            Context context18 = getContext();
            if (context18 != null) {
                this.A = w6.a(context18, 2131099985);
                cd6 cd636 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HomeDashboardFragment", "hybridStepsColor=" + this.A);
        String b21 = ThemeManager.l.a().b("hybridActiveCaloriesTab");
        if (b21 != null) {
            this.B = Color.parseColor(b21);
            cd6 cd637 = cd6.a;
        } else {
            Context context19 = getContext();
            if (context19 != null) {
                this.B = w6.a(context19, 2131099981);
                cd6 cd638 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b22 = ThemeManager.l.a().b("hybridSleepTab");
        if (b22 != null) {
            this.C = Color.parseColor(b22);
            cd6 cd639 = cd6.a;
        } else {
            Context context20 = getContext();
            if (context20 != null) {
                this.C = w6.a(context20, 2131099984);
                cd6 cd640 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b23 = ThemeManager.l.a().b("hybridInactiveTab");
        if (b23 != null) {
            Color.parseColor(b23);
            cd6 cd641 = cd6.a;
        } else {
            Context context21 = getContext();
            if (context21 != null) {
                w6.a(context21, 2131099983);
                cd6 cd642 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        String b24 = ThemeManager.l.a().b("hybridGoalTrackingTab");
        if (b24 != null) {
            this.D = Color.parseColor(b24);
            cd6 cd643 = cd6.a;
        } else {
            Context context22 = getContext();
            if (context22 != null) {
                this.D = w6.a(context22, 2131099879);
                cd6 cd644 = cd6.a;
            } else {
                wg6.a();
                throw null;
            }
        }
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                String b25 = ThemeManager.l.a().b("primaryText");
                if (b25 != null) {
                    a2.I.setColorFilter(Color.parseColor(b25), PorterDuff.Mode.SRC_ATOP);
                    cd6 cd645 = cd6.a;
                }
                a2.t.setBackgroundColor(this.S);
                a2.V.setBackgroundColor(this.U);
                a2.r.setBackgroundColor(this.U);
                a2.w.setBackgroundColor(this.U);
                a2.s.setBackgroundColor(this.U);
                a2.v.setBackgroundColor(this.U);
                cd6 cd646 = cd6.a;
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v23, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v26, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v29, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v21, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void n1() {
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                RingProgressBar ringProgressBar = a2.N;
                wg6.a((Object) ringProgressBar, "it.rpbBiggest");
                a(ringProgressBar, RingProgressBar.b.STEPS);
                ua5 ua5 = this.L;
                if (ua5 == null) {
                    wg6.d("mPresenter");
                    throw null;
                } else if (ua5.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    RingProgressBar ringProgressBar2 = a2.P;
                    wg6.a((Object) ringProgressBar2, "it.rpbSmallest");
                    ringProgressBar2.setVisibility(0);
                    RingProgressBar ringProgressBar3 = a2.M;
                    wg6.a((Object) ringProgressBar3, "it.rpbBig");
                    a(ringProgressBar3, RingProgressBar.b.ACTIVE_TIME);
                    RingProgressBar ringProgressBar4 = a2.O;
                    wg6.a((Object) ringProgressBar4, "it.rpbMedium");
                    a(ringProgressBar4, RingProgressBar.b.CALORIES);
                    RingProgressBar ringProgressBar5 = a2.P;
                    wg6.a((Object) ringProgressBar5, "it.rpbSmallest");
                    a(ringProgressBar5, RingProgressBar.b.SLEEP);
                    FlexibleFitnessTab flexibleFitnessTab = a2.z;
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886463);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026n_StepsToday_Label__Mins)");
                    flexibleFitnessTab.b(a3);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.A;
                    String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886465);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab2.b(a4);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.B;
                    String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886459);
                    wg6.a((Object) a5, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab3.b(a5);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.D;
                    String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886464);
                    wg6.a((Object) a6, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
                    flexibleFitnessTab4.b(a6);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.E;
                    String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886460);
                    wg6.a((Object) a7, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab5.b(a7);
                } else {
                    RingProgressBar ringProgressBar6 = a2.P;
                    wg6.a((Object) ringProgressBar6, "it.rpbSmallest");
                    ringProgressBar6.setVisibility(0);
                    RingProgressBar ringProgressBar7 = a2.M;
                    wg6.a((Object) ringProgressBar7, "it.rpbBig");
                    a(ringProgressBar7, RingProgressBar.b.CALORIES);
                    RingProgressBar ringProgressBar8 = a2.O;
                    wg6.a((Object) ringProgressBar8, "it.rpbMedium");
                    a(ringProgressBar8, RingProgressBar.b.SLEEP);
                    RingProgressBar ringProgressBar9 = a2.P;
                    wg6.a((Object) ringProgressBar9, "it.rpbSmallest");
                    a(ringProgressBar9, RingProgressBar.b.GOAL);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.A;
                    String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886545);
                    wg6.a((Object) a8, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab6.b(a8);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.B;
                    String a9 = jm4.a((Context) PortfolioApp.get.instance(), 2131886542);
                    wg6.a((Object) a9, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab7.b(a9);
                    FlexibleFitnessTab flexibleFitnessTab8 = a2.C;
                    String a10 = jm4.a((Context) PortfolioApp.get.instance(), 2131886544);
                    wg6.a((Object) a10, "LanguageHelper.getString\u2026epsToday_Label__OfNumber)");
                    flexibleFitnessTab8.b(a10);
                    FlexibleFitnessTab flexibleFitnessTab9 = a2.E;
                    String a11 = jm4.a((Context) PortfolioApp.get.instance(), 2131886543);
                    wg6.a((Object) a11, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab9.b(a11);
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void o(boolean z2) {
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                Object r7 = a2.G;
                wg6.a((Object) r7, "it.ftvLowBattery");
                nh6 nh6 = nh6.a;
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886612);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                Object[] objArr = {"25%"};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r7.setText(format);
                NestedScrollView nestedScrollView = a2.J;
                wg6.a((Object) nestedScrollView, "it.nsvLowBattery");
                nestedScrollView.setVisibility(0);
                return;
            }
            NestedScrollView nestedScrollView2 = a2.J;
            wg6.a((Object) nestedScrollView2, "it.nsvLowBattery");
            nestedScrollView2.setVisibility(8);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        HomeDashboardFragment.super.onActivityCreated(bundle);
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onActivityCreated");
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeDashboardFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.K = new ax5<>(this, kb.a(layoutInflater, 2131558566, viewGroup, false, e1()));
        m1();
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HomeDashboardFragment.super.onPause();
        ua5 ua5 = this.L;
        if (ua5 == null) {
            return;
        }
        if (ua5 != null) {
            ua5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        HomeDashboardFragment.super.onResume();
        ua5 ua5 = this.L;
        if (ua5 == null) {
            return;
        }
        if (ua5 != null) {
            s(ua5.i());
            n1();
            ua5 ua52 = this.L;
            if (ua52 != null) {
                ua52.f();
                kl4 g1 = g1();
                if (g1 != null) {
                    g1.d();
                    return;
                }
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: com.fossil.zz5} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: com.fossil.zz5} */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r10v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r10v10, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r10v11, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r10v12, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r10v13, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r10v14, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r9v6, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r10v16, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: Multi-variable type inference failed */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        l1();
        fr a2 = wq.a(this);
        wg6.a((Object) a2, "Glide.with(this)");
        this.R = a2;
        ax5<za4> ax5 = this.K;
        View view2 = null;
        if (ax5 != null) {
            za4 a3 = ax5.a();
            if (a3 != null) {
                Context context = getContext();
                if (context != null) {
                    PortfolioApp.get.instance().f().a(getViewLifecycleOwner(), new c(a3, AnimationUtils.loadAnimation(context, 2130771996), this));
                    String a4 = jm4.a(getContext(), 2131886823);
                    wg6.a((Object) a4, "withoutDeviceText");
                    String a5 = xj6.a(a4, "%s", "", false, 4, (Object) null);
                    Object r1 = a3.F;
                    wg6.a((Object) r1, "binding.ftvDescription");
                    r1.setText(a5);
                    FlexibleProgressBar flexibleProgressBar = a3.S;
                    wg6.a((Object) flexibleProgressBar, "binding.syncProgress");
                    flexibleProgressBar.setMax(10000);
                    a3.R.setOnRefreshListener(new e(a3, this));
                    View headView = a3.R.getHeadView();
                    if (headView instanceof zz5) {
                        view2 = headView;
                    }
                    this.O = (zz5) view2;
                    a3.y.setOnClickListener(new f(this));
                    if (!PortfolioApp.get.instance().F()) {
                        a3.I.setOnLongClickListener(new g(this));
                    }
                    a3.q.a(new m(a3));
                    a3.A.setOnClickListener(new h(this));
                    a3.z.setOnClickListener(new i(this));
                    a3.B.setOnClickListener(new j(this));
                    a3.D.setOnClickListener(new k(this));
                    a3.E.setOnClickListener(new l(this));
                    a3.C.setOnClickListener(new d(this));
                    return;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final void r(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("runProgress state ");
        sb.append(i2);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        wg6.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d("HomeDashboardFragment", sb.toString());
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null && (flexibleProgressBar = a2.S) != null) {
                hh6 hh6 = new hh6();
                hh6.element = 0;
                hh6 hh62 = new hh6();
                hh62.element = 0;
                this.Q = i2;
                int i3 = 1000;
                if (i2 == 0) {
                    hh6.element = 0;
                    hh62.element = FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
                    i3 = FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
                } else if (i2 == 1) {
                    hh6.element = FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
                    i3 = 7000;
                    hh62.element = 300;
                } else if (i2 != 2) {
                    i3 = 0;
                } else {
                    hh6.element = 9000;
                    hh62.element = 1000;
                }
                wg6.a((Object) flexibleProgressBar, "it");
                flexibleProgressBar.setProgress(hh6.element);
                this.P = ObjectAnimator.ofInt(this, "", new int[]{i3});
                ObjectAnimator objectAnimator = this.P;
                if (objectAnimator != null) {
                    objectAnimator.setDuration((long) hh62.element);
                    hh6 hh63 = hh62;
                    FlexibleProgressBar flexibleProgressBar2 = flexibleProgressBar;
                    hh6 hh64 = hh6;
                    int i4 = i2;
                    objectAnimator.addUpdateListener(new n(hh63, flexibleProgressBar2, hh64, this, i4));
                    objectAnimator.addListener(new o(hh63, flexibleProgressBar2, hh64, this, i4));
                    objectAnimator.start();
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void s(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "scroll to position=" + i2);
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                a2.A.d(this.T);
                a2.E.d(this.T);
                a2.B.d(this.T);
                a2.z.d(this.T);
                a2.D.d(this.T);
                a2.C.d(this.T);
                ua5 ua5 = this.L;
                if (ua5 != null) {
                    if (ua5.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                        a2.A.c(this.p);
                        a2.z.c(this.p);
                        a2.B.c(this.p);
                        a2.D.c(this.p);
                        a2.E.c(this.p);
                        if (i2 == 0) {
                            a2.V.setBackgroundColor(this.v);
                            a2.A.c(this.g);
                            a2.A.d(this.v);
                        } else if (i2 == 1) {
                            a2.V.setBackgroundColor(this.w);
                            a2.z.d(this.w);
                            a2.z.c(this.h);
                        } else if (i2 == 2) {
                            a2.V.setBackgroundColor(this.x);
                            a2.B.d(this.x);
                            a2.B.c(this.i);
                        } else if (i2 == 3) {
                            a2.V.setBackgroundColor(this.y);
                            a2.D.d(this.y);
                            a2.D.c(this.j);
                        } else if (i2 == 5) {
                            a2.V.setBackgroundColor(this.z);
                            a2.E.d(this.z);
                            a2.E.c(this.o);
                        }
                    } else {
                        a2.A.c(this.t);
                        a2.C.c(this.t);
                        a2.B.c(this.t);
                        a2.E.c(this.t);
                        if (i2 == 0) {
                            a2.V.setBackgroundColor(this.A);
                            a2.A.c(this.q);
                            a2.A.d(this.A);
                        } else if (i2 == 2) {
                            a2.V.setBackgroundColor(this.B);
                            a2.B.d(this.B);
                            a2.B.c(this.r);
                        } else if (i2 == 4) {
                            a2.V.setBackgroundColor(this.D);
                            a2.C.d(this.D);
                            a2.C.c(this.u);
                        } else if (i2 == 5) {
                            a2.V.setBackgroundColor(this.C);
                            a2.E.d(this.C);
                            a2.E.c(this.s);
                        }
                    }
                    ax5<za4> ax52 = this.K;
                    if (ax52 != null) {
                        za4 a3 = ax52.a();
                        if (!(a3 == null || (viewPager2 = a3.Q) == null)) {
                            viewPager2.a(i2, false);
                        }
                        this.N = i2;
                        ua5 ua52 = this.L;
                        if (ua52 != null) {
                            ua52.a(this.N);
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    } else {
                        wg6.d("mBinding");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CustomSwipeRefreshLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ za4 a;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public e(za4 za4, HomeDashboardFragment homeDashboardFragment) {
            this.a = za4;
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public void a(boolean z) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onEndSwipe");
            this.b.R(z);
        }

        @DexIgnore
        public void b() {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onStartSwipe");
            FlexibleProgressBar flexibleProgressBar = this.a.S;
            wg6.a((Object) flexibleProgressBar, "binding.syncProgress");
            flexibleProgressBar.setVisibility(4);
            View view = this.a.U;
            wg6.a((Object) view, "binding.vBorderBottom");
            view.setVisibility(0);
        }

        @DexIgnore
        public void a() {
            FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onRefresh");
            HomeDashboardFragment.b(this.b).j();
        }
    }

    @DexIgnore
    public void c(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "updateOtaProgress " + i2 + " isActive " + isActive());
        if (isActive()) {
            ax5<za4> ax5 = this.K;
            if (ax5 != null) {
                za4 a2 = ax5.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.w;
                    wg6.a((Object) constraintLayout, "it.clUpdateFw");
                    if (constraintLayout.getVisibility() != 0) {
                        ConstraintLayout constraintLayout2 = a2.w;
                        wg6.a((Object) constraintLayout2, "it.clUpdateFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleProgressBar flexibleProgressBar = a2.K;
                        wg6.a((Object) flexibleProgressBar, "it.pbProgress");
                        flexibleProgressBar.setMax(100);
                    }
                    FlexibleProgressBar flexibleProgressBar2 = a2.S;
                    wg6.a((Object) flexibleProgressBar2, "it.syncProgress");
                    if (flexibleProgressBar2.getVisibility() == 0) {
                        k1();
                    }
                    a2.R.setDisableSwipe(true);
                    FlexibleProgressBar flexibleProgressBar3 = a2.K;
                    wg6.a((Object) flexibleProgressBar3, "it.pbProgress");
                    flexibleProgressBar3.setProgress(i2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        if (isActive()) {
            ax5<za4> ax5 = this.K;
            if (ax5 != null) {
                za4 a2 = ax5.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.w;
                    wg6.a((Object) constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(8);
                    a2.R.setDisableSwipe(false);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(RingProgressBar ringProgressBar, RingProgressBar.b bVar) {
        int i2 = uw4.a[bVar.ordinal()];
        if (i2 == 1) {
            ringProgressBar.setIconSource(2131231209);
            ringProgressBar.c(this.w, this.U);
            ringProgressBar.a("dianaActiveMinuteRing", "dianaUnfilledRing");
        } else if (i2 == 2) {
            ringProgressBar.setIconSource(2131231213);
            ringProgressBar.c(this.v, this.U);
            ua5 ua5 = this.L;
            if (ua5 == null) {
                wg6.d("mPresenter");
                throw null;
            } else if (ua5.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                ringProgressBar.a("dianaStepsRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.a("hybridStepsRing", "hybridUnfilledRing");
            }
        } else if (i2 == 3) {
            ringProgressBar.setIconSource(2131231210);
            ringProgressBar.c(this.x, this.U);
            ua5 ua52 = this.L;
            if (ua52 == null) {
                wg6.d("mPresenter");
                throw null;
            } else if (ua52.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                ringProgressBar.a("dianaActiveCaloriesRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.a("hybridActiveCaloriesRing", "hybridUnfilledRing");
            }
        } else if (i2 == 4) {
            ringProgressBar.setIconSource(2131231212);
            ringProgressBar.c(this.z, this.U);
            ua5 ua53 = this.L;
            if (ua53 == null) {
                wg6.d("mPresenter");
                throw null;
            } else if (ua53.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                ringProgressBar.a("dianaSleepRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.a("hybridSleepRing", "hybridUnfilledRing");
            }
        } else if (i2 == 5) {
            ringProgressBar.setIconSource(2131231211);
            ringProgressBar.c(this.D, this.U);
            ringProgressBar.a("hybridGoalTrackingRing", "hybridUnfilledRing");
        }
    }

    @DexIgnore
    public void a(ua5 ua5) {
        wg6.b(ua5, "presenter");
        this.L = ua5;
    }

    @DexIgnore
    public void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary) {
        float f2;
        float f3;
        float f4;
        float f5;
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                int a3 = ik4.d.a(activitySummary, sh4.ACTIVE_TIME);
                float f6 = 0.0f;
                if (a3 > 0) {
                    f2 = (activitySummary != null ? (float) activitySummary.getActiveTime() : 0.0f) / ((float) a3);
                } else {
                    f2 = 0.0f;
                }
                int a4 = ik4.d.a(activitySummary, sh4.TOTAL_STEPS);
                if (a4 > 0) {
                    f3 = (activitySummary != null ? (float) activitySummary.getSteps() : 0.0f) / ((float) a4);
                } else {
                    f3 = 0.0f;
                }
                int a5 = ik4.d.a(activitySummary, sh4.CALORIES);
                if (a5 > 0) {
                    f4 = (activitySummary != null ? (float) activitySummary.getCalories() : 0.0f) / ((float) a5);
                } else {
                    f4 = 0.0f;
                }
                int a6 = ik4.d.a(mFSleepDay);
                if (a6 > 0) {
                    f5 = (mFSleepDay != null ? (float) mFSleepDay.getSleepMinutes() : 0.0f) / ((float) a6);
                } else {
                    f5 = 0.0f;
                }
                int a7 = ik4.d.a(goalTrackingSummary);
                if (a7 > 0) {
                    if (goalTrackingSummary != null) {
                        f6 = (float) goalTrackingSummary.getTotalTracked();
                    }
                    f6 /= (float) a7;
                }
                boolean z2 = true;
                boolean z3 = f3 >= 1.0f && f4 >= 1.0f && f5 >= 1.0f;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardFragment", "updateVisualization steps: " + f3 + ", time: " + f2 + ", calories: " + f4 + ", sleep: " + f5);
                ua5 ua5 = this.L;
                if (ua5 == null) {
                    wg6.d("mPresenter");
                    throw null;
                } else if (ua5.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    if (!z3 || f2 < 1.0f) {
                        z2 = false;
                    }
                    a2.N.a(f3, z2);
                    a2.M.a(f2, z2);
                    a2.O.a(f4, z2);
                    a2.P.a(f5, z2);
                } else {
                    if (!z3 || f6 < 1.0f) {
                        z2 = false;
                    }
                    a2.N.a(f3, z2);
                    a2.M.a(f4, z2);
                    a2.O.a(f5, z2);
                    a2.P.a(f6, z2);
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z2) {
        double d2;
        double d3;
        int i2;
        int i3;
        int i4;
        Context context;
        String str;
        String str2;
        Integer num3 = num;
        Integer num4 = num2;
        boolean z3 = z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("setDataSummaryForTabs - latestGoalTrackingTarget=");
        sb.append(num3);
        sb.append(", ");
        sb.append("heartRateResting=");
        sb.append(num4);
        sb.append(", isNewSession=");
        sb.append(z3);
        sb.append(", mBinding.get()=");
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            sb.append(ax5.a());
            sb.append(", hashCode=");
            sb.append(hashCode());
            local.d("HomeDashboardFragment", sb.toString());
            ax5<za4> ax52 = this.K;
            if (ax52 != null) {
                za4 a2 = ax52.a();
                if (a2 != null) {
                    Object instance = PortfolioApp.get.instance();
                    int sleepMinutes = mFSleepDay != null ? mFSleepDay.getSleepMinutes() : 0;
                    int intValue = num4 != null ? num2.intValue() : 0;
                    if (activitySummary != null) {
                        d3 = activitySummary.getSteps();
                        i2 = activitySummary.getActiveTime();
                        d2 = activitySummary.getCalories();
                    } else {
                        i2 = 0;
                        d3 = 0.0d;
                        d2 = 0.0d;
                    }
                    if (goalTrackingSummary != null) {
                        i3 = goalTrackingSummary.getGoalTarget();
                        i4 = goalTrackingSummary.getTotalTracked();
                    } else {
                        i4 = 0;
                        i3 = 0;
                    }
                    if (i3 == 0 && num3 != null) {
                        i3 = num.intValue();
                    }
                    int i5 = i3;
                    String a3 = jm4.a((Context) instance, 2131887066);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = a3;
                    StringBuilder sb2 = new StringBuilder();
                    Context context2 = instance;
                    sb2.append("setDataSummaryForTabs - steps=");
                    sb2.append(d3);
                    sb2.append(", activeTime=");
                    sb2.append(i2);
                    sb2.append(", ");
                    sb2.append("calories=");
                    sb2.append(d2);
                    sb2.append(", goalTarget=");
                    sb2.append(i5);
                    sb2.append(", goalTotalTracked=");
                    sb2.append(i4);
                    sb2.append(", ");
                    sb2.append("sleepMinutes=");
                    sb2.append(sleepMinutes);
                    sb2.append(", resting=");
                    sb2.append(intValue);
                    local2.d("HomeDashboardFragment", sb2.toString());
                    FlexibleFitnessTab flexibleFitnessTab = a2.A;
                    int i6 = (d3 > 0.0d ? 1 : (d3 == 0.0d ? 0 : -1));
                    String b2 = (i6 != 0 || !z3) ? bl4.a.b(Integer.valueOf(rh6.a(d3))) : str3;
                    wg6.a((Object) b2, "if (steps == 0.0 && isNe\u2026ormat(steps.roundToInt())");
                    flexibleFitnessTab.c(b2);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.z;
                    String a4 = (i2 != 0 || !z3) ? bl4.a.a(Integer.valueOf(i2)) : str3;
                    wg6.a((Object) a4, "if (activeTime == 0 && i\u2026iveTimeFormat(activeTime)");
                    flexibleFitnessTab2.c(a4);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.B;
                    String a5 = (d2 != 0.0d || !z3) ? bl4.a.a(Float.valueOf((float) d2)) : str3;
                    wg6.a((Object) a5, "if (calories == 0.0 && i\u2026ormat(calories.toFloat())");
                    flexibleFitnessTab3.c(a5);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.E;
                    if (sleepMinutes != 0 || !z3) {
                        context = context2;
                        StringBuilder sb3 = new StringBuilder();
                        nh6 nh6 = nh6.a;
                        Locale locale = Locale.US;
                        wg6.a((Object) locale, "Locale.US");
                        Object[] objArr = {Integer.valueOf(sleepMinutes / 60)};
                        String format = String.format(locale, "%d", Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format);
                        sb3.append(":");
                        nh6 nh62 = nh6.a;
                        Locale locale2 = Locale.US;
                        wg6.a((Object) locale2, "Locale.US");
                        Object[] objArr2 = {Integer.valueOf(sleepMinutes % 60)};
                        String format2 = String.format(locale2, "%02d", Arrays.copyOf(objArr2, objArr2.length));
                        wg6.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format2);
                        str = sb3.toString();
                    } else {
                        context = context2;
                        str = jm4.a(context, 2131887068);
                    }
                    wg6.a((Object) str, "if (sleepMinutes == 0 &&\u2026              .toString()");
                    flexibleFitnessTab4.c(str);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.C;
                    if (i5 != 0 || !z3) {
                        nh6 nh63 = nh6.a;
                        String a6 = jm4.a(context, 2131886523);
                        wg6.a((Object) a6, "LanguageHelper.getString\u2026ingToday_Label__OfNumber)");
                        Object[] objArr3 = {Integer.valueOf(i5)};
                        str2 = String.format(a6, Arrays.copyOf(objArr3, objArr3.length));
                        wg6.a((Object) str2, "java.lang.String.format(format, *args)");
                    } else {
                        str2 = str3;
                    }
                    wg6.a((Object) str2, "if (goalTarget == 0 && i\u2026l__OfNumber), goalTarget)");
                    flexibleFitnessTab5.b(str2);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.C;
                    String valueOf = (i4 != 0 || !z3) ? String.valueOf(i4) : str3;
                    wg6.a((Object) valueOf, "if (goalTotalTracked == \u2026alTotalTracked.toString()");
                    flexibleFitnessTab6.c(valueOf);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.D;
                    String valueOf2 = (i6 != 0 || !z3) ? String.valueOf(intValue) : str3;
                    wg6.a((Object) valueOf2, "if (steps == 0.0 && isNe\u2026e else resting.toString()");
                    flexibleFitnessTab7.c(valueOf2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.T;
                wg6.a((Object) r0, "tvToday");
                nh6 nh6 = nh6.a;
                String string = PortfolioApp.get.instance().getString(2131886478);
                wg6.a((Object) string, "PortfolioApp.instance.ge\u2026ay_Title__TodayMonthDate)");
                Object[] objArr = {al4.a(date)};
                String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r0.setText(format);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if ((str.length() > 0) && wg6.a((Object) str, (Object) "ASK_TO_CANCEL_WORKOUT") && getActivity() != null) {
            if (i2 == 2131363105) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "don't quit");
                ua5 ua5 = this.L;
                if (ua5 != null) {
                    ua5.a(PortfolioApp.get.instance().e(), false);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131363190) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "quit and sync");
                ua5 ua52 = this.L;
                if (ua52 != null) {
                    ua52.a(PortfolioApp.get.instance().e(), true);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        zz5 zz5 = this.O;
        if (zz5 != null) {
            zz5.a(str, str2);
        }
    }

    @DexIgnore
    public final void a(ProgressBar progressBar, int i2) {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "animateSyncProgressEnd " + progressBar);
        if (i2 == 0) {
            progressBar.setVisibility(0);
        } else if (i2 == 1) {
            r(2);
        } else if (i2 == 2) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, progressBar.getY(), progressBar.getY() - ((float) progressBar.getHeight()));
            translateAnimation.setDuration((long) 300);
            translateAnimation.setAnimationListener(new b(this, progressBar));
            progressBar.startAnimation(translateAnimation);
            ax5<za4> ax5 = this.K;
            if (ax5 != null) {
                za4 a2 = ax5.a();
                if (a2 != null && (customSwipeRefreshLayout = a2.R) != null) {
                    customSwipeRefreshLayout.c();
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(boolean z2, boolean z3, boolean z4) {
        ax5<za4> ax5 = this.K;
        if (ax5 != null) {
            za4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z2 || !z4) {
                ConstraintLayout constraintLayout = a2.x;
                wg6.a((Object) constraintLayout, "binding.clVisualization");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.u;
                wg6.a((Object) constraintLayout2, "binding.clNoDevice");
                constraintLayout2.setVisibility(8);
                a2.R.setByPass(!z3);
                if (!z4 && !z3) {
                    ImageView imageView = a2.H;
                    wg6.a((Object) imageView, "binding.ivNoWatchFound");
                    imageView.setVisibility(0);
                    return;
                }
                return;
            }
            ConstraintLayout constraintLayout3 = a2.x;
            wg6.a((Object) constraintLayout3, "binding.clVisualization");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.u;
            wg6.a((Object) constraintLayout4, "binding.clNoDevice");
            constraintLayout4.setVisibility(0);
            a2.R.setByPass(true);
            ImageView imageView2 = a2.H;
            wg6.a((Object) imageView2, "binding.ivNoWatchFound");
            imageView2.setVisibility(8);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
