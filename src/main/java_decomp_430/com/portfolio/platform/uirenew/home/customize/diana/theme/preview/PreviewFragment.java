package com.portfolio.platform.uirenew.home.customize.diana.theme.preview;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.PreviewViewModel;
import com.fossil.WatchFacePreviewFragmentBinding;
import com.fossil.ax5;
import com.fossil.cl4;
import com.fossil.dd4;
import com.fossil.h75;
import com.fossil.i75;
import com.fossil.imagefilters.FilterType;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.o35;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.w6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.CustomizeWidget;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreviewFragment extends BaseFragment {
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public PreviewViewModel g;
    @DexIgnore
    public ax5<dd4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final PreviewFragment a(ArrayList<o35> arrayList, FilterType filterType) {
            wg6.b(arrayList, "complications");
            wg6.b(filterType, "filterType");
            PreviewFragment previewFragment = new PreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", arrayList);
            bundle.putSerializable("FILTER_TYPE_ARG", filterType);
            previewFragment.setArguments(bundle);
            return previewFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<i75.b> {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public b(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(PreviewViewModel.b bVar) {
            ImageView imageView;
            WatchFacePreviewFragmentBinding a2 = this.a.k1();
            if (a2 != null && (imageView = a2.r) != null) {
                imageView.setImageDrawable(bVar.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<i75.c> {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public c(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(PreviewViewModel.c cVar) {
            int i;
            this.a.a();
            if (cVar.c()) {
                FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive loadComplicationSettingResult Success");
                try {
                    i = Color.parseColor(cVar.a());
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("PreviewFragment", e.getMessage());
                    e.printStackTrace();
                    i = Color.parseColor("#FFFFFF");
                }
                this.a.a(cVar.b(), Integer.valueOf(i));
                return;
            }
            FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive loadComplicationSettingResult Fail");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<i75.d> {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public d(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(PreviewViewModel.d dVar) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                this.a.a();
                if (dVar.b()) {
                    FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive saveThemeResult Success");
                    Intent intent = new Intent();
                    intent.putExtra("WATCH_FACE_ID", dVar.a());
                    activity.setResult(-1, intent);
                    activity.finish();
                    return;
                }
                FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive saveThemeResult Fail");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public e(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public f(PreviewFragment previewFragment, int i) {
            this.a = previewFragment;
            this.b = i;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b();
            PreviewFragment.b(this.a).c(this.b);
        }
    }

    @DexIgnore
    public static final /* synthetic */ PreviewViewModel b(PreviewFragment previewFragment) {
        PreviewViewModel previewViewModel = previewFragment.g;
        if (previewViewModel != null) {
            return previewViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) {
            return true;
        }
        fragmentManager.E();
        return true;
    }

    @DexIgnore
    public final void j1() {
        PreviewViewModel previewViewModel = this.g;
        if (previewViewModel != null) {
            previewViewModel.c().a(getViewLifecycleOwner(), new b(this));
            PreviewViewModel previewViewModel2 = this.g;
            if (previewViewModel2 != null) {
                previewViewModel2.d().a(getViewLifecycleOwner(), new c(this));
                PreviewViewModel previewViewModel3 = this.g;
                if (previewViewModel3 != null) {
                    previewViewModel3.e().a(getViewLifecycleOwner(), new d(this));
                } else {
                    wg6.d("mViewModel");
                    throw null;
                }
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final WatchFacePreviewFragmentBinding k1() {
        ax5<dd4> ax5 = this.h;
        if (ax5 != null) {
            return ax5.a();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        PreviewFragment.super.onActivityCreated(bundle);
        if (bundle != null) {
            b(bundle);
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().g().a(new h75()).a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            w04 w04 = this.f;
            if (w04 != null) {
                PreviewViewModel a2 = vd.a(activity, w04).a(PreviewViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026iewViewModel::class.java)");
                this.g = a2;
                return;
            }
            wg6.d("viewModelFactory");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i2;
        PreviewViewModel previewViewModel;
        TextView textView;
        TextView textView2;
        wg6.b(layoutInflater, "inflater");
        PreviewFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        WatchFacePreviewFragmentBinding a2 = kb.a(LayoutInflater.from(getContext()), 2131558596, (ViewGroup) null, true, e1());
        this.h = new ax5<>(this, a2);
        WatchFacePreviewFragmentBinding k1 = k1();
        if (!(k1 == null || (textView2 = k1.t) == null)) {
            cl4.a(textView2, new e(this));
        }
        j1();
        Drawable c2 = w6.c(PortfolioApp.get.instance(), 2131231278);
        if (c2 != null) {
            c2.getIntrinsicHeight();
            Drawable c3 = w6.c(PortfolioApp.get.instance(), 2131231278);
            Integer valueOf = c3 != null ? Integer.valueOf(c3.getMinimumWidth()) : null;
            if (valueOf != null) {
                i2 = valueOf.intValue();
                WatchFacePreviewFragmentBinding k12 = k1();
                if (!(k12 == null || (textView = k12.s) == null)) {
                    cl4.a(textView, new f(this, i2));
                }
                previewViewModel = this.g;
                if (previewViewModel == null) {
                    previewViewModel.a(i2);
                    Bundle arguments = getArguments();
                    if (arguments != null) {
                        wg6.a((Object) arguments, "it");
                        b(arguments);
                    }
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
        }
        i2 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165417);
        WatchFacePreviewFragmentBinding k122 = k1();
        cl4.a(textView, new f(this, i2));
        previewViewModel = this.g;
        if (previewViewModel == null) {
        }
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        PreviewFragment.super.onSaveInstanceState(bundle);
        PreviewViewModel previewViewModel = this.g;
        if (previewViewModel != null) {
            bundle.putSerializable("FILTER_TYPE_ARG", previewViewModel.b());
            PreviewViewModel previewViewModel2 = this.g;
            if (previewViewModel2 != null) {
                bundle.putParcelableArrayList("COMPLICATIONS_ARG", previewViewModel2.a());
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void b(Bundle bundle) {
        Bundle arguments;
        ArrayList parcelableArrayList;
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "initialize()");
        Serializable serializable = null;
        if (!(!bundle.containsKey("COMPLICATIONS_ARG") || (arguments = getArguments()) == null || (parcelableArrayList = arguments.getParcelableArrayList("COMPLICATIONS_ARG")) == null)) {
            PreviewViewModel previewViewModel = this.g;
            if (previewViewModel != null) {
                previewViewModel.a((ArrayList<o35>) parcelableArrayList);
                int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165419);
                b();
                PreviewViewModel previewViewModel2 = this.g;
                if (previewViewModel2 != null) {
                    previewViewModel2.b(dimensionPixelSize);
                } else {
                    wg6.d("mViewModel");
                    throw null;
                }
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
        if (bundle.containsKey("FILTER_TYPE_ARG")) {
            PreviewViewModel previewViewModel3 = this.g;
            if (previewViewModel3 != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    serializable = arguments2.getSerializable("FILTER_TYPE_ARG");
                }
                if (serializable != null) {
                    previewViewModel3.a((FilterType) serializable);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.fossil.imagefilters.FilterType");
            }
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Drawable drawable, Integer num) {
        String c2;
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "showComplications()");
        WatchFacePreviewFragmentBinding k1 = k1();
        if (k1 != null) {
            PreviewViewModel previewViewModel = this.g;
            if (previewViewModel != null) {
                ArrayList<o35> a2 = previewViewModel.a();
                if (a2 != null) {
                    for (o35 o35 : a2) {
                        if ((!wg6.a((Object) o35.a(), (Object) "empty")) && (c2 = o35.c()) != null) {
                            switch (c2.hashCode()) {
                                case -1383228885:
                                    if (!c2.equals("bottom")) {
                                        break;
                                    } else {
                                        CustomizeWidget customizeWidget = k1.wc_bottom;
                                        wg6.a((Object) customizeWidget, "it.wcBottom");
                                        a(customizeWidget, o35.a(), o35.d(), num, drawable);
                                        break;
                                    }
                                case 115029:
                                    if (!c2.equals("top")) {
                                        break;
                                    } else {
                                        CustomizeWidget customizeWidget2 = k1.wc_top;
                                        wg6.a((Object) customizeWidget2, "it.wcTop");
                                        a(customizeWidget2, o35.a(), o35.d(), num, drawable);
                                        break;
                                    }
                                case 3317767:
                                    if (!c2.equals("left")) {
                                        break;
                                    } else {
                                        CustomizeWidget customizeWidget3 = k1.wc_start;
                                        wg6.a((Object) customizeWidget3, "it.wcStart");
                                        a(customizeWidget3, o35.a(), o35.d(), num, drawable);
                                        break;
                                    }
                                case 108511772:
                                    if (!c2.equals("right")) {
                                        break;
                                    } else {
                                        CustomizeWidget customizeWidget4 = k1.wc_end;
                                        wg6.a((Object) customizeWidget4, "it.wcEnd");
                                        a(customizeWidget4, o35.a(), o35.d(), num, drawable);
                                        break;
                                    }
                            }
                        }
                    }
                    return;
                }
                return;
            }
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(CustomizeWidget r4, String str, String str2, Integer num, Drawable drawable) {
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "updateComplicationButton()");
        r4.setVisibility(0);
        r4.b(str);
        if (str2 != null) {
            r4.setBottomContent(str2);
        }
        if (num != null) {
            r4.setDefaultColorRes(Integer.valueOf(num.intValue()));
        }
        if (drawable != null) {
            r4.setBackgroundDrawableCus(drawable);
        }
        r4.h();
    }
}
