package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.fossil.a85;
import com.fossil.a85$c$a;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSettingsViewModel extends td {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public Gson a; // = new Gson();
    @DexIgnore
    public CommuteTimeWatchAppSetting b;
    @DexIgnore
    public MFUser c;
    @DexIgnore
    public MutableLiveData<a85.b> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public List<AddressWrapper> a;

        @DexIgnore
        public b(List<AddressWrapper> list) {
            this.a = list;
        }

        @DexIgnore
        public final List<AddressWrapper> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && wg6.a((Object) this.a, (Object) ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<AddressWrapper> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(addresses=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {69}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel, xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeWatchAppSettingsViewModel;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel;
            Object a = ff6.a();
            int i = this.label;
            List<AddressWrapper> list = null;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel2 = this.this$0;
                dl6 b = zl6.b();
                a85$c$a a85_c_a = new a85$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = commuteTimeWatchAppSettingsViewModel2;
                this.label = 1;
                obj = gk6.a(b, a85_c_a, this);
                if (obj == a) {
                    return a;
                }
                commuteTimeWatchAppSettingsViewModel = commuteTimeWatchAppSettingsViewModel2;
            } else if (i == 1) {
                commuteTimeWatchAppSettingsViewModel = (CommuteTimeWatchAppSettingsViewModel) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            commuteTimeWatchAppSettingsViewModel.c = (MFUser) obj;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel3 = this.this$0;
            CommuteTimeWatchAppSetting a2 = commuteTimeWatchAppSettingsViewModel3.b;
            if (a2 != null) {
                list = a2.getAddresses();
            }
            commuteTimeWatchAppSettingsViewModel3.a(list);
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = CommuteTimeWatchAppSettingsViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeWatchAppSetti\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel(an4 an4, UserRepository userRepository) {
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(userRepository, "mUserRepository");
        this.e = userRepository;
    }

    @DexIgnore
    public final MutableLiveData<a85.b> c() {
        return this.d;
    }

    @DexIgnore
    public final boolean d() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        if (commuteTimeWatchAppSetting == null || commuteTimeWatchAppSetting.getAddresses().size() >= 10) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void e() {
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting b() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List<AddressWrapper>) new ArrayList());
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
        if (commuteTimeWatchAppSetting2 != null) {
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting2.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T next : addresses) {
                if (!TextUtils.isEmpty(((AddressWrapper) next).getAddress())) {
                    arrayList.add(next);
                }
            }
            commuteTimeWatchAppSetting.setAddresses(yd6.d(arrayList));
        }
        return commuteTimeWatchAppSetting;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting a() {
        return this.b;
    }

    @DexIgnore
    public final void a(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.a.a(str, CommuteTimeWatchAppSetting.class);
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = addresses.iterator();
            while (true) {
                boolean z = false;
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (((AddressWrapper) next).getType() == AddressWrapper.AddressType.HOME) {
                    z = true;
                }
                if (z) {
                    arrayList.add(next);
                }
            }
            List<AddressWrapper> addresses2 = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList2 = new ArrayList();
            for (T next2 : addresses2) {
                if (((AddressWrapper) next2).getType() == AddressWrapper.AddressType.WORK) {
                    arrayList2.add(next2);
                }
            }
            if (arrayList.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(0, new AddressWrapper(AddressWrapper.AddressType.HOME.getValue(), AddressWrapper.AddressType.HOME));
            }
            if (arrayList2.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(1, new AddressWrapper(AddressWrapper.AddressType.WORK.getValue(), AddressWrapper.AddressType.WORK));
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local.d(str2, "exception when parse commute time setting " + e2);
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
        }
        this.b = commuteTimeWatchAppSetting;
        if (this.b == null) {
            this.b = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
        }
    }

    @DexIgnore
    public final void b(AddressWrapper addressWrapper) {
        if (addressWrapper != null) {
            List<AddressWrapper> list = null;
            if (addressWrapper.getType() == AddressWrapper.AddressType.OTHER) {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
                if (commuteTimeWatchAppSetting != null) {
                    commuteTimeWatchAppSetting.getAddresses().remove(addressWrapper);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                if (commuteTimeWatchAppSetting2 != null) {
                    commuteTimeWatchAppSetting2.getAddresses();
                    if (wg6.a((Object) addressWrapper.getId(), (Object) addressWrapper.getId())) {
                        addressWrapper.setAddress("");
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
            if (commuteTimeWatchAppSetting3 != null) {
                list = commuteTimeWatchAppSetting3.getAddresses();
            }
            a(list);
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        if (commuteTimeWatchAppSetting != null && addressWrapper != null) {
            List<AddressWrapper> list = null;
            if (commuteTimeWatchAppSetting != null) {
                int i = -1;
                int i2 = 0;
                for (T next : commuteTimeWatchAppSetting.getAddresses()) {
                    int i3 = i2 + 1;
                    if (i2 >= 0) {
                        if (wg6.a((Object) addressWrapper.getId(), (Object) ((AddressWrapper) next).getId())) {
                            i = i2;
                        }
                        i2 = i3;
                    } else {
                        qd6.c();
                        throw null;
                    }
                }
                if (i != -1) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                    if (commuteTimeWatchAppSetting2 != null) {
                        commuteTimeWatchAppSetting2.getAddresses().remove(i);
                        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
                        if (commuteTimeWatchAppSetting3 != null) {
                            commuteTimeWatchAppSetting3.getAddresses().add(i, addressWrapper);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting4 = this.b;
                    if (commuteTimeWatchAppSetting4 != null) {
                        commuteTimeWatchAppSetting4.getAddresses().add(addressWrapper);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting5 = this.b;
                if (commuteTimeWatchAppSetting5 != null) {
                    list = commuteTimeWatchAppSetting5.getAddresses();
                }
                a(list);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<AddressWrapper> list) {
        this.d.a(new b(list));
    }
}
