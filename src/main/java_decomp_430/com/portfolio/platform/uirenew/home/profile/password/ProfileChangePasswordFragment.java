package com.portfolio.platform.uirenew.home.profile.password;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.dm5;
import com.fossil.em5;
import com.fossil.fd4;
import com.fossil.jk3;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileChangePasswordFragment extends BaseFragment implements em5 {
    @DexIgnore
    public static /* final */ Pattern x;
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public dm5 f;
    @DexIgnore
    public TextInputEditText g;
    @DexIgnore
    public TextInputEditText h;
    @DexIgnore
    public TextInputEditText i;
    @DexIgnore
    public TextInputLayout j;
    @DexIgnore
    public TextInputLayout o;
    @DexIgnore
    public TextInputLayout p;
    @DexIgnore
    public ProgressButton q;
    @DexIgnore
    public FlexibleTextView r;
    @DexIgnore
    public FlexibleTextView s;
    @DexIgnore
    public RTLImageView t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ProfileChangePasswordFragment a() {
            return new ProfileChangePasswordFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileChangePasswordFragment a;

        @DexIgnore
        public b(ProfileChangePasswordFragment profileChangePasswordFragment) {
            this.a = profileChangePasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileChangePasswordFragment.c(this.a).clearFocus();
            ProfileChangePasswordFragment.b(this.a).clearFocus();
            ProfileChangePasswordFragment.a(this.a).clearFocus();
            wg6.a((Object) view, "it");
            view.setFocusable(true);
            if (this.a.j1()) {
                ProfileChangePasswordFragment.d(this.a).a(ProfileChangePasswordFragment.b(this.a).getEditableText().toString(), ProfileChangePasswordFragment.a(this.a).getEditableText().toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileChangePasswordFragment a;

        @DexIgnore
        public c(ProfileChangePasswordFragment profileChangePasswordFragment) {
            this.a = profileChangePasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    if (!activity.isFinishing()) {
                        FragmentActivity activity2 = this.a.getActivity();
                        if (activity2 != null) {
                            wg6.a((Object) activity2, "activity!!");
                            if (!activity2.isDestroyed()) {
                                FragmentActivity activity3 = this.a.getActivity();
                                if (activity3 != null) {
                                    activity3.finish();
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileChangePasswordFragment a;

        @DexIgnore
        public d(ProfileChangePasswordFragment profileChangePasswordFragment) {
            this.a = profileChangePasswordFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
        public void afterTextChanged(Editable editable) {
            wg6.b(editable, "s");
            ProfileChangePasswordFragment.e(this.a).setEnabled(this.a.j1());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileChangePasswordFragment a;

        @DexIgnore
        public e(ProfileChangePasswordFragment profileChangePasswordFragment) {
            this.a = profileChangePasswordFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
        public void afterTextChanged(Editable editable) {
            wg6.b(editable, "s");
            ProfileChangePasswordFragment.e(this.a).setEnabled(this.a.j1());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileChangePasswordFragment a;

        @DexIgnore
        public f(ProfileChangePasswordFragment profileChangePasswordFragment) {
            this.a = profileChangePasswordFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r3v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public final void onFocusChange(View view, boolean z) {
            boolean z2 = false;
            if (z) {
                ProfileChangePasswordFragment.f(this.a).setVisibility(0);
                ProfileChangePasswordFragment.g(this.a).setVisibility(0);
                return;
            }
            Editable editableText = ProfileChangePasswordFragment.a(this.a).getEditableText();
            wg6.a((Object) editableText, "mEdtNew.editableText");
            if (editableText.length() == 0) {
                z2 = true;
            }
            if (z2 || this.a.v) {
                ProfileChangePasswordFragment.f(this.a).setVisibility(8);
                ProfileChangePasswordFragment.g(this.a).setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileChangePasswordFragment a;

        @DexIgnore
        public g(ProfileChangePasswordFragment profileChangePasswordFragment) {
            this.a = profileChangePasswordFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
        public void afterTextChanged(Editable editable) {
            wg6.b(editable, "s");
            ProfileChangePasswordFragment.e(this.a).setEnabled(this.a.j1());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }
    }

    /*
    static {
        wg6.a((Object) ProfileChangePasswordFragment.class.getSimpleName(), "ProfileChangePasswordFra\u2026nt::class.java.simpleName");
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            x = compile;
        } else {
            wg6.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public static final /* synthetic */ TextInputEditText a(ProfileChangePasswordFragment profileChangePasswordFragment) {
        TextInputEditText textInputEditText = profileChangePasswordFragment.h;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        wg6.d("mEdtNew");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText b(ProfileChangePasswordFragment profileChangePasswordFragment) {
        TextInputEditText textInputEditText = profileChangePasswordFragment.g;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        wg6.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText c(ProfileChangePasswordFragment profileChangePasswordFragment) {
        TextInputEditText textInputEditText = profileChangePasswordFragment.i;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        wg6.d("mEdtRepeat");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ dm5 d(ProfileChangePasswordFragment profileChangePasswordFragment) {
        dm5 dm5 = profileChangePasswordFragment.f;
        if (dm5 != null) {
            return dm5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProgressButton e(ProfileChangePasswordFragment profileChangePasswordFragment) {
        ProgressButton progressButton = profileChangePasswordFragment.q;
        if (progressButton != null) {
            return progressButton;
        }
        wg6.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView f(ProfileChangePasswordFragment profileChangePasswordFragment) {
        FlexibleTextView flexibleTextView = profileChangePasswordFragment.r;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        wg6.d("mTvCheckChar");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView g(ProfileChangePasswordFragment profileChangePasswordFragment) {
        FlexibleTextView flexibleTextView = profileChangePasswordFragment.s;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        wg6.d("mTvCheckCombine");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    public void I0() {
        if (isActive()) {
            Object r0 = this.q;
            if (r0 != 0) {
                r0.setEnabled(false);
                TextInputEditText textInputEditText = this.g;
                if (textInputEditText != null) {
                    this.u = textInputEditText.getEditableText().toString();
                    R(true);
                    return;
                }
                wg6.d("mEdtOld");
                throw null;
            }
            wg6.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void R(boolean z) {
        TextInputLayout textInputLayout = this.j;
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(z);
            if (z) {
                TextInputLayout textInputLayout2 = this.j;
                if (textInputLayout2 != null) {
                    textInputLayout2.setError(jm4.a((Context) PortfolioApp.get.instance(), 2131886840));
                } else {
                    wg6.d("mTvOldError");
                    throw null;
                }
            } else {
                TextInputLayout textInputLayout3 = this.j;
                if (textInputLayout3 != null) {
                    textInputLayout3.setError("");
                } else {
                    wg6.d("mTvOldError");
                    throw null;
                }
            }
        } else {
            wg6.d("mTvOldError");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void W0() {
        if (isActive()) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886743);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026urPasswordHasBeenChanged)");
            Y(a2);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        if (getActivity() == null) {
            return false;
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "activity!!");
            if (activity.isFinishing()) {
                return false;
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                wg6.a((Object) activity2, "activity!!");
                if (activity2.isDestroyed()) {
                    return false;
                }
                FragmentActivity activity3 = getActivity();
                if (activity3 != null) {
                    activity3.finish();
                    return true;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v49, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v53, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v56, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean j1() {
        boolean z;
        TextInputEditText textInputEditText = this.g;
        if (textInputEditText != null) {
            String obj = textInputEditText.getEditableText().toString();
            TextInputEditText textInputEditText2 = this.h;
            if (textInputEditText2 != null) {
                String obj2 = textInputEditText2.getEditableText().toString();
                TextInputEditText textInputEditText3 = this.i;
                if (textInputEditText3 != null) {
                    String obj3 = textInputEditText3.getEditableText().toString();
                    boolean z2 = true;
                    if (obj2.length() >= 7) {
                        FlexibleTextView flexibleTextView = this.r;
                        if (flexibleTextView != null) {
                            flexibleTextView.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231066), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = true;
                        } else {
                            wg6.d("mTvCheckChar");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = this.r;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231005), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = false;
                        } else {
                            wg6.d("mTvCheckChar");
                            throw null;
                        }
                    }
                    if (x.matcher(obj2).matches()) {
                        FlexibleTextView flexibleTextView3 = this.s;
                        if (flexibleTextView3 != null) {
                            flexibleTextView3.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231066), (Drawable) null, (Drawable) null, (Drawable) null);
                        } else {
                            wg6.d("mTvCheckCombine");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView4 = this.s;
                        if (flexibleTextView4 != null) {
                            flexibleTextView4.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231005), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = false;
                        } else {
                            wg6.d("mTvCheckCombine");
                            throw null;
                        }
                    }
                    if (obj.length() == 0) {
                        TextInputLayout textInputLayout = this.j;
                        if (textInputLayout != null) {
                            textInputLayout.setErrorEnabled(false);
                            TextInputLayout textInputLayout2 = this.j;
                            if (textInputLayout2 != null) {
                                textInputLayout2.setError("");
                            } else {
                                wg6.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            wg6.d("mTvOldError");
                            throw null;
                        }
                    } else {
                        if (wg6.a((Object) this.u, (Object) obj)) {
                            TextInputLayout textInputLayout3 = this.j;
                            if (textInputLayout3 != null) {
                                textInputLayout3.setErrorEnabled(true);
                                TextInputLayout textInputLayout4 = this.j;
                                if (textInputLayout4 != null) {
                                    textInputLayout4.setError(jm4.a((Context) PortfolioApp.get.instance(), 2131886840));
                                } else {
                                    wg6.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout5 = this.j;
                            if (textInputLayout5 != null) {
                                textInputLayout5.setErrorEnabled(false);
                                TextInputLayout textInputLayout6 = this.j;
                                if (textInputLayout6 != null) {
                                    textInputLayout6.setError("");
                                } else {
                                    wg6.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvOldError");
                                throw null;
                            }
                        }
                        if ((obj2.length() == 0) || !wg6.a((Object) obj2, (Object) obj)) {
                            TextInputLayout textInputLayout7 = this.o;
                            if (textInputLayout7 != null) {
                                textInputLayout7.setErrorEnabled(false);
                                TextInputLayout textInputLayout8 = this.o;
                                if (textInputLayout8 != null) {
                                    textInputLayout8.setError("");
                                } else {
                                    wg6.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout9 = this.o;
                            if (textInputLayout9 != null) {
                                textInputLayout9.setErrorEnabled(true);
                                TextInputLayout textInputLayout10 = this.o;
                                if (textInputLayout10 != null) {
                                    textInputLayout10.setError(jm4.a((Context) PortfolioApp.get.instance(), 2131886749));
                                } else {
                                    wg6.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (obj2.length() == 0) {
                        TextInputLayout textInputLayout11 = this.o;
                        if (textInputLayout11 != null) {
                            textInputLayout11.setErrorEnabled(false);
                            TextInputLayout textInputLayout12 = this.o;
                            if (textInputLayout12 != null) {
                                textInputLayout12.setError("");
                                TextInputLayout textInputLayout13 = this.p;
                                if (textInputLayout13 != null) {
                                    textInputLayout13.setErrorEnabled(false);
                                    TextInputLayout textInputLayout14 = this.p;
                                    if (textInputLayout14 != null) {
                                        textInputLayout14.setError("");
                                    } else {
                                        wg6.d("mTvRepeatError");
                                        throw null;
                                    }
                                } else {
                                    wg6.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            wg6.d("mTvNewError");
                            throw null;
                        }
                    }
                    if ((obj3.length() == 0) || wg6.a((Object) obj3, (Object) obj2)) {
                        TextInputLayout textInputLayout15 = this.p;
                        if (textInputLayout15 != null) {
                            textInputLayout15.setErrorEnabled(false);
                            TextInputLayout textInputLayout16 = this.p;
                            if (textInputLayout16 != null) {
                                textInputLayout16.setError("");
                            } else {
                                wg6.d("mTvRepeatError");
                                throw null;
                            }
                        } else {
                            wg6.d("mTvRepeatError");
                            throw null;
                        }
                    } else {
                        if (!(obj2.length() == 0) && (!wg6.a((Object) obj3, (Object) obj2))) {
                            TextInputLayout textInputLayout17 = this.p;
                            if (textInputLayout17 != null) {
                                textInputLayout17.setErrorEnabled(true);
                                TextInputLayout textInputLayout18 = this.p;
                                if (textInputLayout18 != null) {
                                    textInputLayout18.setError(jm4.a((Context) PortfolioApp.get.instance(), 2131886843));
                                } else {
                                    wg6.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvRepeatError");
                                throw null;
                            }
                        }
                    }
                    this.v = z;
                    if (this.v) {
                        if (!(obj.length() == 0)) {
                            if (!(obj2.length() == 0) && (!wg6.a((Object) obj, (Object) obj2)) && wg6.a((Object) obj2, (Object) obj3) && (!wg6.a((Object) obj, (Object) this.u))) {
                                TextInputLayout textInputLayout19 = this.o;
                                if (textInputLayout19 != null) {
                                    textInputLayout19.setErrorEnabled(false);
                                    TextInputLayout textInputLayout20 = this.o;
                                    if (textInputLayout20 != null) {
                                        textInputLayout20.setError("");
                                        TextInputLayout textInputLayout21 = this.p;
                                        if (textInputLayout21 != null) {
                                            textInputLayout21.setErrorEnabled(false);
                                            TextInputLayout textInputLayout22 = this.p;
                                            if (textInputLayout22 != null) {
                                                textInputLayout22.setError("");
                                                return true;
                                            }
                                            wg6.d("mTvRepeatError");
                                            throw null;
                                        }
                                        wg6.d("mTvRepeatError");
                                        throw null;
                                    }
                                    wg6.d("mTvNewError");
                                    throw null;
                                }
                                wg6.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (!this.v) {
                        if (obj2.length() != 0) {
                            z2 = false;
                        }
                        if (!z2) {
                            Object r0 = this.r;
                            if (r0 != 0) {
                                r0.setVisibility(0);
                                Object r02 = this.s;
                                if (r02 != 0) {
                                    r02.setVisibility(0);
                                } else {
                                    wg6.d("mTvCheckCombine");
                                    throw null;
                                }
                            } else {
                                wg6.d("mTvCheckChar");
                                throw null;
                            }
                        }
                    }
                    return false;
                }
                wg6.d("mEdtRepeat");
                throw null;
            }
            wg6.d("mEdtNew");
            throw null;
        }
        wg6.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        fd4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558597, (ViewGroup) null, false, e1());
        wg6.a((Object) a2, "binding");
        a(a2);
        new ax5(this, a2);
        Object r4 = this.q;
        if (r4 != 0) {
            r4.setEnabled(false);
            return a2.d();
        }
        wg6.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ProfileChangePasswordFragment.super.onPause();
        dm5 dm5 = this.f;
        if (dm5 != null) {
            dm5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        ProfileChangePasswordFragment.super.onResume();
        dm5 dm5 = this.f;
        if (dm5 != null) {
            dm5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void u0() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                wg6.a((Object) fragmentManager, "fragmentManager!!");
                lx5.C(fragmentManager);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void d() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.b();
            } else {
                wg6.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void e() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.c();
            } else {
                wg6.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(dm5 dm5) {
        wg6.b(dm5, "presenter");
        jk3.a(dm5);
        wg6.a((Object) dm5, "checkNotNull(presenter)");
        this.f = dm5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a(fd4 fd4) {
        FlexibleTextInputEditText flexibleTextInputEditText = fd4.r;
        wg6.a((Object) flexibleTextInputEditText, "binding.etOldPass");
        this.g = flexibleTextInputEditText;
        FlexibleTextInputEditText flexibleTextInputEditText2 = fd4.q;
        wg6.a((Object) flexibleTextInputEditText2, "binding.etNewPass");
        this.h = flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3 = fd4.s;
        wg6.a((Object) flexibleTextInputEditText3, "binding.etRepeatPass");
        this.i = flexibleTextInputEditText3;
        FlexibleTextInputLayout flexibleTextInputLayout = fd4.u;
        wg6.a((Object) flexibleTextInputLayout, "binding.inputOldPass");
        this.j = flexibleTextInputLayout;
        FlexibleTextInputLayout flexibleTextInputLayout2 = fd4.t;
        wg6.a((Object) flexibleTextInputLayout2, "binding.inputNewPass");
        this.o = flexibleTextInputLayout2;
        FlexibleTextInputLayout flexibleTextInputLayout3 = fd4.v;
        wg6.a((Object) flexibleTextInputLayout3, "binding.inputRepeatPass");
        this.p = flexibleTextInputLayout3;
        ProgressButton progressButton = fd4.y;
        wg6.a((Object) progressButton, "binding.save");
        this.q = progressButton;
        FlexibleTextView flexibleTextView = fd4.z;
        wg6.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
        this.r = flexibleTextView;
        FlexibleTextView flexibleTextView2 = fd4.A;
        wg6.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
        this.s = flexibleTextView2;
        RTLImageView rTLImageView = fd4.w;
        wg6.a((Object) rTLImageView, "binding.ivBack");
        this.t = rTLImageView;
        Object r4 = this.q;
        if (r4 != 0) {
            r4.setOnClickListener(new b(this));
            Object r42 = this.t;
            if (r42 != 0) {
                r42.setOnClickListener(new c(this));
                TextInputEditText textInputEditText = this.g;
                if (textInputEditText != null) {
                    textInputEditText.addTextChangedListener(new d(this));
                    TextInputEditText textInputEditText2 = this.h;
                    if (textInputEditText2 != null) {
                        textInputEditText2.addTextChangedListener(new e(this));
                        TextInputEditText textInputEditText3 = this.h;
                        if (textInputEditText3 != null) {
                            textInputEditText3.setOnFocusChangeListener(new f(this));
                            TextInputEditText textInputEditText4 = this.i;
                            if (textInputEditText4 != null) {
                                textInputEditText4.addTextChangedListener(new g(this));
                            } else {
                                wg6.d("mEdtRepeat");
                                throw null;
                            }
                        } else {
                            wg6.d("mEdtNew");
                            throw null;
                        }
                    } else {
                        wg6.d("mEdtNew");
                        throw null;
                    }
                } else {
                    wg6.d("mEdtOld");
                    throw null;
                }
            } else {
                wg6.d("mBackButton");
                throw null;
            }
        } else {
            wg6.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public void e(int i2, String str) {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }
}
