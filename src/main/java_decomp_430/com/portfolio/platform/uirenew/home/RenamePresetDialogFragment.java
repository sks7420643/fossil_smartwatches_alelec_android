package com.portfolio.platform.uirenew.home;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.DialogFragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RenamePresetDialogFragment extends DialogFragment implements View.OnClickListener {
    @DexIgnore
    public static /* final */ a g; // = new a((qg6) null);
    @DexIgnore
    public b a;
    @DexIgnore
    public String b;
    @DexIgnore
    public FlexibleTextView c;
    @DexIgnore
    public FlexibleEditText d;
    @DexIgnore
    public ImageView e;
    @DexIgnore
    public HashMap f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final RenamePresetDialogFragment a(String str, b bVar) {
            wg6.b(bVar, "listener");
            RenamePresetDialogFragment renamePresetDialogFragment = new RenamePresetDialogFragment();
            renamePresetDialogFragment.a = bVar;
            renamePresetDialogFragment.b = str;
            return renamePresetDialogFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);

        @DexIgnore
        void onCancel();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ RenamePresetDialogFragment a;

        @DexIgnore
        public c(RenamePresetDialogFragment renamePresetDialogFragment) {
            this.a = renamePresetDialogFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void afterTextChanged(Editable editable) {
            wg6.b(editable, "text");
            RenamePresetDialogFragment renamePresetDialogFragment = this.a;
            String obj = editable.toString();
            int length = obj.length() - 1;
            int i = 0;
            boolean z = false;
            while (i <= length) {
                boolean z2 = obj.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            renamePresetDialogFragment.b = obj.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.a.b);
            RenamePresetDialogFragment.a(this.a).setEnabled(z3);
            if (z3) {
                RenamePresetDialogFragment.b(this.a).setVisibility(0);
            } else {
                RenamePresetDialogFragment.b(this.a).setVisibility(8);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "text");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "text");
        }
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView a(RenamePresetDialogFragment renamePresetDialogFragment) {
        FlexibleTextView flexibleTextView = renamePresetDialogFragment.c;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        wg6.d("ftvRename");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ImageView b(RenamePresetDialogFragment renamePresetDialogFragment) {
        ImageView imageView = renamePresetDialogFragment.e;
        if (imageView != null) {
            return imageView;
        }
        wg6.d("ivClearName");
        throw null;
    }

    @DexIgnore
    public final void R(boolean z) {
        b bVar = this.a;
        if (bVar != null) {
            if (z) {
                bVar.onCancel();
            } else {
                String str = this.b;
                if (str != null) {
                    bVar.a(str);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362307) {
                R(true);
            } else if (id != 2131362413) {
                if (id == 2131362559) {
                    Object r2 = this.d;
                    if (r2 != 0) {
                        r2.setText("");
                    } else {
                        wg6.d("fetRename");
                        throw null;
                    }
                }
            } else if (!TextUtils.isEmpty(this.b)) {
                R(false);
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        RenamePresetDialogFragment.super.onCreate(bundle);
        setStyle(1, 16973830);
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = RenamePresetDialogFragment.super.onCreateDialog(bundle);
        wg6.a((Object) onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v14, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r6v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558603, viewGroup);
        ViewGroup viewGroup2 = (ViewGroup) inflate.findViewById(2131362851);
        String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        if (b2 != null) {
            viewGroup2.setBackgroundColor(Color.parseColor(b2));
        }
        View findViewById = inflate.findViewById(2131362238);
        wg6.a((Object) findViewById, "view.findViewById(R.id.fet_rename)");
        this.d = (FlexibleEditText) findViewById;
        View findViewById2 = inflate.findViewById(2131362413);
        wg6.a((Object) findViewById2, "view.findViewById(R.id.ftv_rename)");
        this.c = (FlexibleTextView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362559);
        wg6.a((Object) findViewById3, "view.findViewById(R.id.iv_clear_name)");
        this.e = (ImageView) findViewById3;
        String str = this.b;
        if (str != null) {
            Object r2 = this.d;
            if (r2 != 0) {
                r2.setText(str);
                Object r22 = this.d;
                if (r22 != 0) {
                    r22.setSelection(str.length());
                    if (str.length() == 0) {
                        ImageView imageView = this.e;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        } else {
                            wg6.d("ivClearName");
                            throw null;
                        }
                    } else {
                        ImageView imageView2 = this.e;
                        if (imageView2 != null) {
                            imageView2.setVisibility(0);
                        } else {
                            wg6.d("ivClearName");
                            throw null;
                        }
                    }
                } else {
                    wg6.d("fetRename");
                    throw null;
                }
            } else {
                wg6.d("fetRename");
                throw null;
            }
        }
        Object r6 = this.d;
        if (r6 != 0) {
            r6.addTextChangedListener(new c(this));
            ImageView imageView3 = this.e;
            if (imageView3 != null) {
                imageView3.setOnClickListener(this);
                Object r62 = this.c;
                if (r62 != 0) {
                    r62.setOnClickListener(this);
                    inflate.findViewById(2131362307).setOnClickListener(this);
                    return inflate;
                }
                wg6.d("ftvRename");
                throw null;
            }
            wg6.d("ivClearName");
            throw null;
        }
        wg6.d("fetRename");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        RenamePresetDialogFragment.super.onDestroyView();
        d1();
    }
}
