package com.portfolio.platform.uirenew.home.customize.diana.complications.details;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.a65;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.ru4;
import com.fossil.td4;
import com.fossil.wg6;
import com.fossil.z55;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezoneFragment extends BaseFragment implements a65 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<td4> f;
    @DexIgnore
    public z55 g;
    @DexIgnore
    public ru4 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SearchSecondTimezoneFragment a() {
            return new SearchSecondTimezoneFragment();
        }

        @DexIgnore
        public final String b() {
            return SearchSecondTimezoneFragment.j;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ru4.b {
        @DexIgnore
        public /* final */ /* synthetic */ SearchSecondTimezoneFragment a;

        @DexIgnore
        public b(SearchSecondTimezoneFragment searchSecondTimezoneFragment) {
            this.a = searchSecondTimezoneFragment;
        }

        @DexIgnore
        public void a(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = SearchSecondTimezoneFragment.o.b();
            local.d(b, "onItemClick position=" + i);
            ru4 a2 = this.a.h;
            if (a2 != null) {
                ru4.c cVar = a2.c().get(i);
                if (cVar.b() == ru4.c.a.TYPE_VALUE) {
                    SecondTimezoneSetting c = cVar.c();
                    if (c != null) {
                        c.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(c.getTimeZoneId()));
                        Intent intent = new Intent();
                        intent.putExtra("SECOND_TIMEZONE", c);
                        FragmentActivity activity = this.a.getActivity();
                        if (activity != null) {
                            activity.setResult(-1, intent);
                        }
                        FragmentActivity activity2 = this.a.getActivity();
                        if (activity2 != null) {
                            activity2.finish();
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewAlphabetIndex.b {
        @DexIgnore
        public /* final */ /* synthetic */ SearchSecondTimezoneFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ td4 b;

        @DexIgnore
        public c(SearchSecondTimezoneFragment searchSecondTimezoneFragment, td4 td4) {
            this.a = searchSecondTimezoneFragment;
            this.b = td4;
        }

        @DexIgnore
        public void a(View view, int i, String str) {
            wg6.b(view, "view");
            wg6.b(str, "character");
            ru4 a2 = this.a.h;
            Integer valueOf = a2 != null ? Integer.valueOf(a2.b(str)) : null;
            if (valueOf != null && valueOf.intValue() != -1) {
                RecyclerView recyclerView = this.b.w;
                wg6.a((Object) recyclerView, "binding.timezoneRecyclerView");
                LinearLayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.f(valueOf.intValue(), 0);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ SearchSecondTimezoneFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ td4 b;

        @DexIgnore
        public d(SearchSecondTimezoneFragment searchSecondTimezoneFragment, td4 td4) {
            this.a = searchSecondTimezoneFragment;
            this.b = td4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FLogger.INSTANCE.getLocal().d(SearchSecondTimezoneFragment.o.b(), "afterTextChanged s=" + editable);
            ru4 a2 = this.a.h;
            if (a2 != null) {
                String valueOf = String.valueOf(editable);
                int length = valueOf.length() - 1;
                int i = 0;
                boolean z = false;
                while (i <= length) {
                    boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                a2.a(valueOf.subSequence(i, length + 1).toString());
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = SearchSecondTimezoneFragment.o.b();
            local.d(b2, "beforeTextChanged s=" + charSequence + " start=" + i + " count=" + i2 + " after=" + i3);
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = SearchSecondTimezoneFragment.o.b();
            local.d(b2, "onTextChanged s=" + charSequence + " start=" + i + " before=" + i2 + " count=" + i3);
            ImageView imageView = this.b.r;
            wg6.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(!TextUtils.isEmpty(charSequence) ? 0 : 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SearchSecondTimezoneFragment a;

        @DexIgnore
        public e(SearchSecondTimezoneFragment searchSecondTimezoneFragment) {
            this.a = searchSecondTimezoneFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ td4 a;

        @DexIgnore
        public f(td4 td4) {
            this.a = td4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            this.a.v.setText("");
        }
    }

    /*
    static {
        String simpleName = SearchSecondTimezoneFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "SearchSecondTimezoneFrag\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void K(String str) {
        Object r0;
        wg6.b(str, "cityName");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local.d(str2, "cityName=" + str);
        ax5<td4> ax5 = this.f;
        if (ax5 != null) {
            td4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.s) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        td4 a2 = kb.a(layoutInflater, 2131558607, viewGroup, false, e1());
        ru4 ru4 = new ru4();
        ru4.a((ru4.b) new b(this));
        this.h = ru4;
        RecyclerView recyclerView = a2.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        RecyclerViewAlphabetIndex recyclerViewAlphabetIndex = a2.u;
        recyclerViewAlphabetIndex.a();
        recyclerViewAlphabetIndex.setOnSectionIndexClickListener(new c(this, a2));
        a2.v.addTextChangedListener(new d(this, a2));
        a2.q.setOnClickListener(new e(this));
        a2.r.setOnClickListener(new f(a2));
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        z55 z55 = this.g;
        if (z55 != null) {
            z55.g();
            SearchSecondTimezoneFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        SearchSecondTimezoneFragment.super.onResume();
        z55 z55 = this.g;
        if (z55 != null) {
            z55.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void q(List<SecondTimezoneSetting> list) {
        wg6.b(list, "secondTimezones");
        ru4 ru4 = this.h;
        if (ru4 != null) {
            ru4.a(list);
        }
    }

    @DexIgnore
    public void a(z55 z55) {
        wg6.b(z55, "presenter");
        this.g = z55;
    }
}
