package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse;

import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.MutableLiveData;
import com.fossil.ax5;
import com.fossil.jb;
import com.fossil.kb;
import com.fossil.l94;
import com.fossil.ld;
import com.fossil.nh6;
import com.fossil.o34;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tz4;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xz4;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseEditDialogViewModel;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseEditDialogFragment extends DialogFragment implements View.OnClickListener {
    @DexIgnore
    public w04 a;
    @DexIgnore
    public /* final */ jb b; // = new o34(this);
    @DexIgnore
    public ax5<l94> c;
    @DexIgnore
    public QuickResponseMessage d;
    @DexIgnore
    public String e;
    @DexIgnore
    public QuickResponseEditDialogViewModel f;
    @DexIgnore
    public Integer g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseEditDialogFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l94 b;

        @DexIgnore
        public b(QuickResponseEditDialogFragment quickResponseEditDialogFragment, l94 l94) {
            this.a = quickResponseEditDialogFragment;
            this.b = l94;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            QuickResponseEditDialogViewModel b2 = this.a.f;
            if (b2 != null) {
                String valueOf = String.valueOf(editable);
                Integer a2 = this.a.g;
                if (a2 != null) {
                    b2.a(valueOf, a2.intValue());
                } else {
                    wg6.a();
                    throw null;
                }
            }
            QuickResponseEditDialogFragment quickResponseEditDialogFragment = this.a;
            String valueOf2 = String.valueOf(editable);
            if (valueOf2 != null) {
                quickResponseEditDialogFragment.e = yj6.d(valueOf2).toString();
                if (!TextUtils.isEmpty(String.valueOf(editable))) {
                    ImageView imageView = this.b.w;
                    wg6.a((Object) imageView, "view.ivEditClearMessage");
                    imageView.setVisibility(0);
                    return;
                }
                ImageView imageView2 = this.b.w;
                wg6.a((Object) imageView2, "view.ivEditClearMessage");
                imageView2.setVisibility(8);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<tz4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseEditDialogFragment a;

        @DexIgnore
        public c(QuickResponseEditDialogFragment quickResponseEditDialogFragment) {
            this.a = quickResponseEditDialogFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(QuickResponseEditDialogViewModel.a aVar) {
            if (aVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("QuickResponseEditDialogFragment", "data changed " + this.a.f);
                this.a.a(aVar);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public final void R(boolean z) {
        if (!z) {
            QuickResponseMessage quickResponseMessage = this.d;
            if ((!wg6.a((Object) quickResponseMessage != null ? quickResponseMessage.getResponse() : null, (Object) this.e)) && !TextUtils.isEmpty(this.e)) {
                QuickResponseMessage quickResponseMessage2 = this.d;
                if (quickResponseMessage2 != null) {
                    String str = this.e;
                    if (str != null) {
                        quickResponseMessage2.setResponse(str);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                QuickResponseEditDialogViewModel quickResponseEditDialogViewModel = this.f;
                if (quickResponseEditDialogViewModel != null) {
                    QuickResponseMessage quickResponseMessage3 = this.d;
                    if (quickResponseMessage3 != null) {
                        quickResponseEditDialogViewModel.a(quickResponseMessage3);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void onClick(View view) {
        Object r2;
        if (view != null) {
            int id = view.getId();
            if (id == 2131362307) {
                R(true);
            } else if (id != 2131362418) {
                ax5<l94> ax5 = this.c;
                if (ax5 != null) {
                    l94 a2 = ax5.a();
                    if (a2 != null && (r2 = a2.q) != 0) {
                        r2.setText("");
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            } else {
                R(false);
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        QuickResponseEditDialogFragment.super.onCreate(bundle);
        setStyle(1, 16973830);
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = QuickResponseEditDialogFragment.super.onCreateDialog(bundle);
        wg6.a((Object) onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v18, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r7v19, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r7v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r2v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r2v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        MutableLiveData<tz4.a> b2;
        wg6.b(layoutInflater, "inflater");
        l94 a2 = kb.a(LayoutInflater.from(getContext()), 2131558546, (ViewGroup) null, false, this.b);
        String b3 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        if (b3 != null) {
            a2.x.setBackgroundColor(Color.parseColor(b3));
        }
        PortfolioApp.get.instance().g().a(new xz4()).a(this);
        this.h = ThemeManager.l.a().b(Constants.YO_ERROR_POST);
        this.i = ThemeManager.l.a().b("nonBrandSurface");
        Object r7 = a2.s;
        wg6.a((Object) r7, "view.ftvEditLimit");
        nh6 nh6 = nh6.a;
        boolean z = true;
        Object[] objArr = {this.g};
        String format = String.format("/%d", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        r7.setText(format);
        QuickResponseMessage quickResponseMessage = this.d;
        if (quickResponseMessage != null) {
            this.e = quickResponseMessage.getResponse();
        }
        Object r72 = a2.q;
        wg6.a((Object) r72, "view.fetMessage");
        InputFilter.LengthFilter[] lengthFilterArr = new InputFilter.LengthFilter[1];
        Integer num = this.g;
        if (num != null) {
            lengthFilterArr[0] = new InputFilter.LengthFilter(num.intValue());
            r72.setFilters(lengthFilterArr);
            a2.q.addTextChangedListener(new b(this, a2));
            String str = this.e;
            if (str != null) {
                a2.q.setText(str);
                a2.q.setSelection(str.length());
                Object r2 = a2.t;
                wg6.a((Object) r2, "view.ftvEditLimitValue");
                r2.setText(String.valueOf(str.length()));
                if (str.length() != 0) {
                    z = false;
                }
                if (z) {
                    ImageView imageView = a2.w;
                    wg6.a((Object) imageView, "view.ivEditClearMessage");
                    imageView.setVisibility(8);
                } else {
                    ImageView imageView2 = a2.w;
                    wg6.a((Object) imageView2, "view.ivEditClearMessage");
                    imageView2.setVisibility(0);
                }
            }
            a2.w.setOnClickListener(this);
            a2.r.setOnClickListener(this);
            a2.v.setOnClickListener(this);
            w04 w04 = this.a;
            if (w04 != null) {
                this.f = vd.a(this, w04).a(QuickResponseEditDialogViewModel.class);
                QuickResponseEditDialogViewModel quickResponseEditDialogViewModel = this.f;
                if (!(quickResponseEditDialogViewModel == null || (b2 = quickResponseEditDialogViewModel.b()) == null)) {
                    b2.a(getViewLifecycleOwner(), new c(this));
                }
                this.c = new ax5<>(this, a2);
                wg6.a((Object) a2, "view");
                return a2.d();
            }
            wg6.d("viewModelFactory");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        QuickResponseEditDialogFragment.super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v27, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v38, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v42, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void a(QuickResponseEditDialogViewModel.a aVar) {
        Object r0;
        Object r5;
        Object r02;
        Object r03;
        Object r04;
        Object r05;
        Object r06;
        Object r07;
        Object r08;
        Boolean a2 = aVar.a();
        if (a2 != null) {
            if (a2.booleanValue()) {
                if (!TextUtils.isEmpty(this.h)) {
                    ax5<l94> ax5 = this.c;
                    if (ax5 != null) {
                        l94 a3 = ax5.a();
                        if (!(a3 == null || (r08 = a3.q) == 0)) {
                            r08.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.h)));
                        }
                    } else {
                        wg6.d("mBinding");
                        throw null;
                    }
                } else {
                    ax5<l94> ax52 = this.c;
                    if (ax52 != null) {
                        l94 a4 = ax52.a();
                        if (!(a4 == null || (r07 = a4.q) == 0)) {
                            r07.setBackgroundTintList(ColorStateList.valueOf(-65536));
                        }
                    } else {
                        wg6.d("mBinding");
                        throw null;
                    }
                }
                ax5<l94> ax53 = this.c;
                if (ax53 != null) {
                    l94 a5 = ax53.a();
                    if (!(a5 == null || (r06 = a5.u) == 0)) {
                        r06.setVisibility(0);
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                ax5<l94> ax54 = this.c;
                if (ax54 != null) {
                    l94 a6 = ax54.a();
                    if (!(a6 == null || (r02 = a6.u) == 0 || r02.getVisibility() != 0)) {
                        ax5<l94> ax55 = this.c;
                        if (ax55 != null) {
                            l94 a7 = ax55.a();
                            if (!(a7 == null || (r05 = a7.u) == 0)) {
                                r05.setVisibility(8);
                            }
                            if (!TextUtils.isEmpty(this.i)) {
                                ax5<l94> ax56 = this.c;
                                if (ax56 != null) {
                                    l94 a8 = ax56.a();
                                    if (!(a8 == null || (r04 = a8.q) == 0)) {
                                        r04.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.i)));
                                    }
                                } else {
                                    wg6.d("mBinding");
                                    throw null;
                                }
                            } else {
                                ax5<l94> ax57 = this.c;
                                if (ax57 != null) {
                                    l94 a9 = ax57.a();
                                    if (!(a9 == null || (r03 = a9.q) == 0)) {
                                        r03.setBackgroundTintList(ColorStateList.valueOf(-1));
                                    }
                                } else {
                                    wg6.d("mBinding");
                                    throw null;
                                }
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            }
        }
        Integer b2 = aVar.b();
        if (b2 != null) {
            int intValue = b2.intValue();
            Integer num = this.g;
            if (num == null) {
                wg6.a();
                throw null;
            } else if (intValue >= num.intValue()) {
                ax5<l94> ax58 = this.c;
                if (ax58 != null) {
                    l94 a10 = ax58.a();
                    if (a10 != null && (r5 = a10.t) != 0) {
                        r5.setText(String.valueOf(this.g));
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            } else {
                ax5<l94> ax59 = this.c;
                if (ax59 != null) {
                    l94 a11 = ax59.a();
                    if (a11 != null && (r0 = a11.t) != 0) {
                        r0.setText(String.valueOf(intValue));
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
        }
    }
}
