package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bo5;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRingChartViewModel extends td {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<bo5.b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b((Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, 255, (qg6) null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;
        @DexIgnore
        public Integer g;
        @DexIgnore
        public Integer h;

        @DexIgnore
        public b() {
            this((Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, 255, (qg6) null);
        }

        @DexIgnore
        public b(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }

        @DexIgnore
        public final Integer a() {
            return this.d;
        }

        @DexIgnore
        public final Integer b() {
            return this.c;
        }

        @DexIgnore
        public final Integer c() {
            return this.b;
        }

        @DexIgnore
        public final Integer d() {
            return this.a;
        }

        @DexIgnore
        public final Integer e() {
            return this.f;
        }

        @DexIgnore
        public final Integer f() {
            return this.e;
        }

        @DexIgnore
        public final Integer g() {
            return this.h;
        }

        @DexIgnore
        public final Integer h() {
            return this.g;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, qg6 qg6) {
            this(r1, (r0 & 2) != 0 ? null : num2, (r0 & 4) != 0 ? null : num3, (r0 & 8) != 0 ? null : num4, (r0 & 16) != 0 ? null : num5, (r0 & 32) != 0 ? null : num6, (r0 & 64) != 0 ? null : num7, (r0 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 0 ? num8 : r2);
            int i2 = i;
            Integer num9 = null;
            Integer num10 = (i2 & 1) != 0 ? null : num;
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, Object obj) {
            if ((i & 1) != 0) {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = null;
            }
            if ((i & 16) != 0) {
                num5 = null;
            }
            if ((i & 32) != 0) {
                num6 = null;
            }
            if ((i & 64) != 0) {
                num7 = null;
            }
            if ((i & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0) {
                num8 = null;
            }
            bVar.a(num, num2, num3, num4, num5, num6, num7, num8);
        }

        @DexIgnore
        public final void a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel$saveColor$1", f = "CustomizeRingChartViewModel.kt", l = {76, 77, 120}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $bigColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $biggestColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $mediumColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $smallColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CustomizeRingChartViewModel customizeRingChartViewModel, String str, String str2, String str3, String str4, String str5, xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeRingChartViewModel;
            this.$id = str;
            this.$biggestColor = str2;
            this.$bigColor = str3;
            this.$mediumColor = str4;
            this.$smallColor = str5;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$id, this.$biggestColor, this.$bigColor, this.$mediumColor, this.$smallColor, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00a0  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00c5  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x010b  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0151  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x0197  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x0223 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x022c  */
        public final Object invokeSuspend(Object obj) {
            jh6 jh6;
            il6 il6;
            T t;
            jh6 jh62;
            T t2;
            T t3;
            T a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                jh6 jh63 = new jh6();
                ThemeRepository b = this.this$0.c;
                String str = this.$id;
                this.L$0 = il62;
                this.L$1 = jh63;
                this.L$2 = jh63;
                this.label = 1;
                t3 = b.getThemeById(str, this);
                if (t3 == a) {
                    return a;
                }
                jh6 = jh63;
                il6 = il62;
                jh62 = jh6;
            } else if (i == 1) {
                jh62 = (jh6) this.L$2;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                jh6 = (jh6) this.L$1;
                t3 = obj;
            } else if (i == 2) {
                jh62 = (jh6) this.L$2;
                nc6.a(obj);
                jh6 = (jh6) this.L$1;
                il6 = (il6) this.L$0;
                t2 = obj;
                if (t2 == null) {
                    t = (Theme) t2;
                    jh62.element = t;
                    jh6 jh64 = new jh6();
                    jh64.element = null;
                    jh6 jh65 = new jh6();
                    jh65.element = null;
                    jh6 jh66 = new jh6();
                    jh66.element = null;
                    jh6 jh67 = new jh6();
                    jh67.element = null;
                    if (this.$biggestColor != null) {
                        for (Style style : ((Theme) jh6.element).getStyles()) {
                            if (wg6.a((Object) style.getKey(), (Object) "dianaStepsRing") || wg6.a((Object) style.getKey(), (Object) "hybridStepsRing")) {
                                style.setValue(this.$biggestColor);
                                jh64.element = hf6.a(Color.parseColor(this.$biggestColor));
                            }
                        }
                    }
                    if (this.$bigColor != null) {
                        for (Style style2 : ((Theme) jh6.element).getStyles()) {
                            if (wg6.a((Object) style2.getKey(), (Object) "dianaActiveMinuteRing") || wg6.a((Object) style2.getKey(), (Object) "hybridActiveCaloriesRing")) {
                                style2.setValue(this.$bigColor);
                                jh65.element = hf6.a(Color.parseColor(this.$bigColor));
                            }
                        }
                    }
                    if (this.$mediumColor != null) {
                        for (Style style3 : ((Theme) jh6.element).getStyles()) {
                            if (wg6.a((Object) style3.getKey(), (Object) "dianaActiveCaloriesRing") || wg6.a((Object) style3.getKey(), (Object) "hybridSleepRing")) {
                                style3.setValue(this.$mediumColor);
                                jh66.element = hf6.a(Color.parseColor(this.$mediumColor));
                            }
                        }
                    }
                    if (this.$smallColor != null) {
                        for (Style style4 : ((Theme) jh6.element).getStyles()) {
                            if (wg6.a((Object) style4.getKey(), (Object) "dianaSleepRing") || wg6.a((Object) style4.getKey(), (Object) "hybridGoalTrackingRing")) {
                                style4.setValue(this.$smallColor);
                                jh67.element = hf6.a(Color.parseColor(this.$smallColor));
                            }
                        }
                    }
                    b c = this.this$0.b;
                    T t4 = jh64.element;
                    T t5 = jh65.element;
                    T t6 = jh66.element;
                    T t7 = jh67.element;
                    c.a((Integer) t4, (Integer) t4, (Integer) t5, (Integer) t5, (Integer) t6, (Integer) t6, (Integer) t7, (Integer) t7);
                    this.L$0 = il6;
                    this.L$1 = jh6;
                    this.L$2 = jh64;
                    this.L$3 = jh65;
                    this.L$4 = jh66;
                    this.L$5 = jh67;
                    this.label = 3;
                    if (this.this$0.c.upsertUserTheme((Theme) jh6.element, this) == a) {
                        return a;
                    }
                    this.this$0.a();
                    return cd6.a;
                }
                wg6.a();
                throw null;
            } else if (i == 3) {
                jh6 jh68 = (jh6) this.L$5;
                jh6 jh69 = (jh6) this.L$4;
                jh6 jh610 = (jh6) this.L$3;
                jh6 jh611 = (jh6) this.L$2;
                jh6 jh612 = (jh6) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.a();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            t = (Theme) t3;
            if (t == null) {
                ThemeRepository b2 = this.this$0.c;
                this.L$0 = il6;
                this.L$1 = jh6;
                this.L$2 = jh62;
                this.label = 2;
                t2 = b2.getCurrentTheme(this);
                if (t2 == a) {
                    return a;
                }
                if (t2 == null) {
                }
            }
            jh62.element = t;
            jh6 jh642 = new jh6();
            jh642.element = null;
            jh6 jh652 = new jh6();
            jh652.element = null;
            jh6 jh662 = new jh6();
            jh662.element = null;
            jh6 jh672 = new jh6();
            jh672.element = null;
            if (this.$biggestColor != null) {
            }
            if (this.$bigColor != null) {
            }
            if (this.$mediumColor != null) {
            }
            if (this.$smallColor != null) {
            }
            b c2 = this.this$0.b;
            T t42 = jh642.element;
            T t52 = jh652.element;
            T t62 = jh662.element;
            T t72 = jh672.element;
            c2.a((Integer) t42, (Integer) t42, (Integer) t52, (Integer) t52, (Integer) t62, (Integer) t62, (Integer) t72, (Integer) t72);
            this.L$0 = il6;
            this.L$1 = jh6;
            this.L$2 = jh642;
            this.L$3 = jh652;
            this.L$4 = jh662;
            this.L$5 = jh672;
            this.label = 3;
            if (this.this$0.c.upsertUserTheme((Theme) jh6.element, this) == a) {
            }
            this.this$0.a();
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = CustomizeRingChartViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeRingChartViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CustomizeRingChartViewModel(ThemeRepository themeRepository) {
        wg6.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<bo5.b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        int i;
        int i2;
        int i3;
        int i4;
        String b2 = CustomizeRingChartFragment.s.b();
        if (b2 != null) {
            i = Color.parseColor(b2);
        } else {
            i = Color.parseColor(e);
        }
        String a2 = CustomizeRingChartFragment.s.a();
        if (a2 != null) {
            i2 = Color.parseColor(a2);
        } else {
            i2 = Color.parseColor(e);
        }
        String c2 = CustomizeRingChartFragment.s.c();
        if (c2 != null) {
            i3 = Color.parseColor(c2);
        } else {
            i3 = Color.parseColor(e);
        }
        String d2 = CustomizeRingChartFragment.s.d();
        if (d2 != null) {
            i4 = Color.parseColor(d2);
        } else {
            i4 = Color.parseColor(e);
        }
        this.b.a(Integer.valueOf(i), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i4));
        a();
    }

    @DexIgnore
    public final void a(int i, int i2) {
        switch (i) {
            case 401:
                b.a(this.b, Integer.valueOf(i2), Integer.valueOf(i2), (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, 252, (Object) null);
                a();
                return;
            case Action.ActivityTracker.TAG_ACTIVITY:
                b.a(this.b, (Integer) null, (Integer) null, Integer.valueOf(i2), Integer.valueOf(i2), (Integer) null, (Integer) null, (Integer) null, (Integer) null, 243, (Object) null);
                a();
                return;
            case MFNetworkReturnCode.WRONG_PASSWORD:
                b.a(this.b, (Integer) null, (Integer) null, (Integer) null, (Integer) null, Integer.valueOf(i2), Integer.valueOf(i2), (Integer) null, (Integer) null, 207, (Object) null);
                a();
                return;
            case MFNetworkReturnCode.NOT_FOUND:
                b.a(this.b, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, (Integer) null, Integer.valueOf(i2), Integer.valueOf(i2), 63, (Object) null);
                a();
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4, String str5) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = d;
        local.d(str6, "saveColor biggestColor=" + str2 + " bigColor=" + str3 + " mediumColor=" + str4 + " smallColor=" + str5);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, str, str2, str3, str4, str5, (xe6) null), 3, (Object) null);
    }
}
