package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.ub5;
import com.fossil.ut4;
import com.fossil.vb5;
import com.fossil.wg6;
import com.fossil.x54;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewWeekFragment extends BaseFragment implements vb5 {
    @DexIgnore
    public ax5<x54> f;
    @DexIgnore
    public ub5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "ActiveTimeOverviewWeekFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        x54 a2;
        OverviewWeekChart overviewWeekChart;
        ax5<x54> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.q) != null) {
            ub5 ub5 = this.g;
            if ((ub5 != null ? ub5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewWeekChart.a("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.a("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        x54 a2;
        wg6.b(layoutInflater, "inflater");
        ActiveTimeOverviewWeekFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onCreateView");
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558498, viewGroup, false, e1()));
        j1();
        ax5<x54> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        ActiveTimeOverviewWeekFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onResume");
        j1();
        ub5 ub5 = this.g;
        if (ub5 != null) {
            ub5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        ActiveTimeOverviewWeekFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onStop");
        ub5 ub5 = this.g;
        if (ub5 != null) {
            ub5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(ut4 ut4) {
        x54 a2;
        OverviewWeekChart overviewWeekChart;
        wg6.b(ut4, "baseModel");
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "showWeekDetails");
        ax5<x54> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            ut4.a aVar = ut4.a;
            wg6.a((Object) overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            wg6.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewWeekChart.a(ut4);
        }
    }

    @DexIgnore
    public void a(ub5 ub5) {
        wg6.b(ub5, "presenter");
        this.g = ub5;
    }
}
