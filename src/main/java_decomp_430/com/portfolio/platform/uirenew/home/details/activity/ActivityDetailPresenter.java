package com.portfolio.platform.uirenew.home.details.activity;

import android.os.Bundle;
import android.util.Pair;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.aj6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gg6;
import com.fossil.gk6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.lh5;
import com.fossil.ll6;
import com.fossil.mh5;
import com.fossil.nc6;
import com.fossil.ph5$e$a;
import com.fossil.ph5$e$b;
import com.fossil.ph5$e$c;
import com.fossil.ph5$f$a;
import com.fossil.ph5$f$b;
import com.fossil.ph5$g$a;
import com.fossil.ph5$g$b;
import com.fossil.ph5$g$c;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.sh4;
import com.fossil.ti6;
import com.fossil.u04;
import com.fossil.ut4;
import com.fossil.v3;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xg6;
import com.fossil.yd6;
import com.fossil.yx5;
import com.fossil.zh4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDetailPresenter extends lh5 implements vk4.a {
    @DexIgnore
    public /* final */ u04 A;
    @DexIgnore
    public /* final */ PortfolioApp B;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.B.e());
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<lc6<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public List<ActivitySummary> k; // = new ArrayList();
    @DexIgnore
    public List<ActivitySample> l; // = new ArrayList();
    @DexIgnore
    public ActivitySummary m;
    @DexIgnore
    public List<ActivitySample> n;
    @DexIgnore
    public zh4 o; // = zh4.METRIC;
    @DexIgnore
    public LiveData<yx5<List<ActivitySummary>>> p;
    @DexIgnore
    public LiveData<yx5<List<ActivitySample>>> q;
    @DexIgnore
    public Listing<WorkoutSession> r;
    @DexIgnore
    public /* final */ mh5 s;
    @DexIgnore
    public /* final */ SummariesRepository t;
    @DexIgnore
    public /* final */ ActivitiesRepository u;
    @DexIgnore
    public /* final */ UserRepository v;
    @DexIgnore
    public /* final */ WorkoutSessionRepository w;
    @DexIgnore
    public /* final */ FitnessDataDao x;
    @DexIgnore
    public /* final */ WorkoutDao y;
    @DexIgnore
    public /* final */ FitnessDatabase z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<ActivitySample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke((ActivitySample) obj));
        }

        @DexIgnore
        public final boolean invoke(ActivitySample activitySample) {
            wg6.b(activitySample, "it");
            return bk4.d(activitySample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<cf<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore
        public c(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cf<WorkoutSession> cfVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityDetailPresenter", "getWorkoutSessionsPaging observed size = " + cfVar.size());
            if (DeviceHelper.o.g(PortfolioApp.get.instance().e())) {
                wg6.a((Object) cfVar, "pageList");
                if (yd6.d(cfVar).isEmpty()) {
                    this.a.s.a(false, this.a.o, cfVar);
                    return;
                }
            }
            mh5 o = this.a.s;
            zh4 h = this.a.o;
            wg6.a((Object) cfVar, "pageList");
            o.a(true, h, cfVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore
        public d(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<ActivitySample>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.u.getActivityList((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1", f = "ActivityDetailPresenter.kt", l = {179, 203, 204}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ActivityDetailPresenter activityDetailPresenter, Date date, xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$date, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x015d  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x019e A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x019f  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x01b0  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01c2  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x01fb  */
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            ActivitySummary activitySummary;
            List list;
            lc6 lc6;
            il6 il6;
            Boolean bool;
            Pair<Date, Date> pair;
            Object obj3;
            boolean z;
            il6 il62;
            Pair<Date, Date> a;
            Object obj4;
            ActivityDetailPresenter activityDetailPresenter;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il62 = this.p$;
                if (this.this$0.f == null) {
                    activityDetailPresenter = this.this$0;
                    dl6 a3 = activityDetailPresenter.b();
                    ph5$e$a ph5_e_a = new ph5$e$a((xe6) null);
                    this.L$0 = il62;
                    this.L$1 = activityDetailPresenter;
                    this.label = 1;
                    obj4 = gk6.a(a3, ph5_e_a, this);
                    if (obj4 == a2) {
                        return a2;
                    }
                }
                il6 = il62;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f);
                this.this$0.g = this.$date;
                z = bk4.c(this.this$0.f, this.$date);
                Boolean t = bk4.t(this.$date);
                mh5 o = this.this$0.s;
                Date date = this.$date;
                wg6.a((Object) t, "isToday");
                o.a(date, z, t.booleanValue(), !bk4.c(new Date(), this.$date));
                a = bk4.a(this.$date, this.this$0.f);
                wg6.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                lc6 = (lc6) this.this$0.h.a();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("ActivityDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new lc6(a.first, a.second));
                if (lc6 == null || !bk4.d((Date) lc6.getFirst(), (Date) a.first) || !bk4.d((Date) lc6.getSecond(), (Date) a.second)) {
                    this.this$0.i = false;
                    this.this$0.j = false;
                    ActivityDetailPresenter activityDetailPresenter2 = this.this$0;
                    activityDetailPresenter2.c(activityDetailPresenter2.g);
                    this.this$0.h.a(new lc6(a.first, a.second));
                    return cd6.a;
                }
                dl6 a4 = this.this$0.b();
                ph5$e$c ph5_e_c = new ph5$e$c(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = t;
                this.L$2 = a;
                this.L$3 = lc6;
                this.label = 2;
                obj3 = gk6.a(a4, ph5_e_c, this);
                if (obj3 == a2) {
                    return a2;
                }
                pair = a;
                bool = t;
                ActivitySummary activitySummary2 = (ActivitySummary) obj3;
                dl6 a5 = this.this$0.b();
                ph5$e$b ph5_e_b = new ph5$e$b(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair;
                this.L$3 = lc6;
                this.L$4 = activitySummary2;
                this.label = 3;
                obj2 = gk6.a(a5, ph5_e_b, this);
                if (obj2 != a2) {
                }
            } else if (i == 1) {
                activityDetailPresenter = (ActivityDetailPresenter) this.L$1;
                il62 = (il6) this.L$0;
                nc6.a(obj);
                obj4 = obj;
            } else if (i == 2) {
                bool = (Boolean) this.L$1;
                boolean z2 = this.Z$0;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                lc6 = (lc6) this.L$3;
                pair = (Pair) this.L$2;
                z = z2;
                obj3 = obj;
                ActivitySummary activitySummary22 = (ActivitySummary) obj3;
                dl6 a52 = this.this$0.b();
                ph5$e$b ph5_e_b2 = new ph5$e$b(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair;
                this.L$3 = lc6;
                this.L$4 = activitySummary22;
                this.label = 3;
                obj2 = gk6.a(a52, ph5_e_b2, this);
                if (obj2 != a2) {
                    return a2;
                }
                activitySummary = activitySummary22;
                list = (List) obj2;
                if (!wg6.a((Object) this.this$0.m, (Object) activitySummary)) {
                }
                if (!wg6.a((Object) this.this$0.n, (Object) list)) {
                }
                this.this$0.s.a(this.this$0.o, this.this$0.m);
                ActivityDetailPresenter activityDetailPresenter3 = this.this$0;
                activityDetailPresenter3.c(activityDetailPresenter3.g);
                rm6 unused = this.this$0.m();
                return cd6.a;
            } else if (i == 3) {
                activitySummary = (ActivitySummary) this.L$4;
                lc6 lc62 = (lc6) this.L$3;
                Pair pair2 = (Pair) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                obj2 = obj;
                list = (List) obj2;
                if (!wg6.a((Object) this.this$0.m, (Object) activitySummary)) {
                    this.this$0.m = activitySummary;
                }
                if (!wg6.a((Object) this.this$0.n, (Object) list)) {
                    this.this$0.n = list;
                }
                this.this$0.s.a(this.this$0.o, this.this$0.m);
                ActivityDetailPresenter activityDetailPresenter32 = this.this$0;
                activityDetailPresenter32.c(activityDetailPresenter32.g);
                if (this.this$0.i && this.this$0.j) {
                    rm6 unused2 = this.this$0.m();
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            activityDetailPresenter.f = (Date) obj4;
            il6 = il62;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("ActivityDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f);
            this.this$0.g = this.$date;
            z = bk4.c(this.this$0.f, this.$date);
            Boolean t2 = bk4.t(this.$date);
            mh5 o2 = this.this$0.s;
            Date date2 = this.$date;
            wg6.a((Object) t2, "isToday");
            o2.a(date2, z, t2.booleanValue(), !bk4.c(new Date(), this.$date));
            a = bk4.a(this.$date, this.this$0.f);
            wg6.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            lc6 = (lc6) this.this$0.h.a();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("ActivityDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new lc6(a.first, a.second));
            if (lc6 == null || !bk4.d((Date) lc6.getFirst(), (Date) a.first) || !bk4.d((Date) lc6.getSecond(), (Date) a.second)) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1", f = "ActivityDetailPresenter.kt", l = {246, 248}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ActivityDetailPresenter activityDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x008f  */
        public final Object invokeSuspend(Object obj) {
            lc6 lc6;
            ArrayList arrayList;
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 a2 = this.this$0.b();
                ph5$f$b ph5_f_b = new ph5$f$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, ph5_f_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                arrayList = (ArrayList) this.L$2;
                lc6 = (lc6) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                Integer num = (Integer) obj;
                int a3 = ik4.d.a(this.this$0.m, sh4.TOTAL_STEPS);
                this.this$0.s.a((ut4) new BarChart.c(Math.max(num == null ? num.intValue() : 0, a3 / 16), a3, arrayList), (ArrayList<String>) (ArrayList) lc6.getSecond());
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc62 = (lc6) obj;
            ArrayList arrayList2 = (ArrayList) lc62.getFirst();
            dl6 a4 = this.this$0.b();
            ph5$f$a ph5_f_a = new ph5$f$a(arrayList2, (xe6) null);
            this.L$0 = il6;
            this.L$1 = lc62;
            this.L$2 = arrayList2;
            this.label = 2;
            Object a5 = gk6.a(a4, ph5_f_a, this);
            if (a5 == a) {
                return a;
            }
            arrayList = arrayList2;
            Object obj2 = a5;
            lc6 = lc62;
            obj = obj2;
            Integer num2 = (Integer) obj;
            int a32 = ik4.d.a(this.this$0.m, sh4.TOTAL_STEPS);
            this.this$0.s.a((ut4) new BarChart.c(Math.max(num2 == null ? num2.intValue() : 0, a32 / 16), a32, arrayList), (ArrayList<String>) (ArrayList) lc6.getSecond());
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1", f = "ActivityDetailPresenter.kt", l = {120}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ActivityDetailPresenter activityDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ActivityDetailPresenter activityDetailPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ActivityDetailPresenter activityDetailPresenter2 = this.this$0;
                dl6 a2 = activityDetailPresenter2.b();
                ph5$g$a ph5_g_a = new ph5$g$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = activityDetailPresenter2;
                this.label = 1;
                obj = gk6.a(a2, ph5_g_a, this);
                if (obj == a) {
                    return a;
                }
                activityDetailPresenter = activityDetailPresenter2;
            } else if (i == 1) {
                activityDetailPresenter = (ActivityDetailPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            wg6.a(obj, "withContext(commonPool) \u2026ntUser()!!.distanceUnit }");
            activityDetailPresenter.o = (zh4) obj;
            LiveData q = this.this$0.p;
            mh5 o = this.this$0.s;
            if (o != null) {
                q.a((ActivityDetailFragment) o, new ph5$g$b(this));
                this.this$0.q.a(this.this$0.s, new ph5$g$c(this));
                return cd6.a;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore
        public h(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<ActivitySummary>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.t.getSummaries((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public ActivityDetailPresenter(mh5 mh5, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, u04 u04, PortfolioApp portfolioApp) {
        wg6.b(mh5, "mView");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(activitiesRepository, "mActivitiesRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(workoutSessionRepository, "mWorkoutSessionRepository");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(workoutDao, "mWorkoutDao");
        wg6.b(fitnessDatabase, "mWorkoutDatabase");
        wg6.b(u04, "appExecutors");
        wg6.b(portfolioApp, "mApp");
        this.s = mh5;
        this.t = summariesRepository;
        this.u = activitiesRepository;
        this.v = userRepository;
        this.w = workoutSessionRepository;
        this.x = fitnessDataDao;
        this.y = workoutDao;
        this.z = fitnessDatabase;
        this.A = u04;
        this.B = portfolioApp;
        LiveData<yx5<List<ActivitySummary>>> b2 = sd.b(this.h, new h(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.p = b2;
        LiveData<yx5<List<ActivitySample>>> b3 = sd.b(this.h, new d(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.q = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "start");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new g(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "stop");
        LiveData<yx5<List<ActivitySummary>>> liveData = this.p;
        mh5 mh5 = this.s;
        if (mh5 != null) {
            liveData.a((ActivityDetailFragment) mh5);
            this.q.a(this.s);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        LiveData<cf<WorkoutSession>> pagedList;
        try {
            Listing<WorkoutSession> listing = this.r;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                mh5 mh5 = this.s;
                if (mh5 != null) {
                    pagedList.a((ActivityDetailFragment) mh5);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
                }
            }
            this.w.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("ActivityDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        Date m2 = bk4.m(this.g);
        wg6.a((Object) m2, "DateHelper.getNextDate(mDate)");
        b(m2);
    }

    @DexIgnore
    public void k() {
        Date n2 = bk4.n(this.g);
        wg6.a((Object) n2, "DateHelper.getPrevDate(mDate)");
        b(n2);
    }

    @DexIgnore
    public void l() {
        this.s.a(this);
    }

    @DexIgnore
    public final rm6 m() {
        return ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Date date) {
        i();
        WorkoutSessionRepository workoutSessionRepository = this.w;
        this.r = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository, this.x, this.y, this.z, this.A, this);
        Listing<WorkoutSession> listing = this.r;
        if (listing != null) {
            LiveData<cf<WorkoutSession>> pagedList = listing.getPagedList();
            mh5 mh5 = this.s;
            if (mh5 != null) {
                pagedList.a((ActivityDetailFragment) mh5, new c(this));
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, date, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final ActivitySummary b(Date date, List<ActivitySummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (bk4.d(((ActivitySummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (ActivitySummary) t2;
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        c(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wg6.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final List<ActivitySample> a(Date date, List<ActivitySample> list) {
        ti6<T> b2;
        ti6<T> a2;
        if (list == null || (b2 = yd6.b(list)) == null || (a2 = aj6.a(b2, new b(date))) == null) {
            return null;
        }
        return aj6.g(a2);
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        gg6<cd6> retry;
        wg6.b(gVar, "report");
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "retry all failed request");
        Listing<WorkoutSession> listing = this.r;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }
}
