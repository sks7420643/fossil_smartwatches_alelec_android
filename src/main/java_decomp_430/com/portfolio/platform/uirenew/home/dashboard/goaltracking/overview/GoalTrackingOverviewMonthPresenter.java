package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.pe5;
import com.fossil.qe5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.re5$b$a;
import com.fossil.re5$d$a;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewMonthPresenter extends pe5 {
    @DexIgnore
    public MutableLiveData<Date> e; // = new MutableLiveData<>();
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public List<GoalTrackingSummary> j; // = new ArrayList();
    @DexIgnore
    public LiveData<yx5<List<GoalTrackingSummary>>> k; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<GoalTrackingSummary>>> l;
    @DexIgnore
    public TreeMap<Long, Float> m;
    @DexIgnore
    public /* final */ qe5 n;
    @DexIgnore
    public /* final */ UserRepository o;
    @DexIgnore
    public /* final */ an4 p;
    @DexIgnore
    public /* final */ GoalTrackingRepository q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$loadData$2", f = "GoalTrackingOverviewMonthPresenter.kt", l = {91}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingOverviewMonthPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                re5$b$a re5_b_a = new re5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, re5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                this.this$0.i = bk4.d(mFUser.getCreatedAt());
                qe5 m = this.this$0.n;
                Date d = GoalTrackingOverviewMonthPresenter.d(this.this$0);
                Date b = this.this$0.i;
                if (b == null) {
                    b = new Date();
                }
                m.a(d, b);
                this.this$0.e.a(new Date());
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter a;

        @DexIgnore
        public c(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            this.a = goalTrackingOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<GoalTrackingSummary>>> apply(Date date) {
            GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter = this.a;
            wg6.a((Object) date, "it");
            if (goalTrackingOverviewMonthPresenter.b(date)) {
                GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter2 = this.a;
                goalTrackingOverviewMonthPresenter2.k = goalTrackingOverviewMonthPresenter2.q.getSummaries(GoalTrackingOverviewMonthPresenter.k(this.a), GoalTrackingOverviewMonthPresenter.g(this.a), true);
            }
            return this.a.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<yx5<? extends List<GoalTrackingSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter a;

        @DexIgnore
        public d(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            this.a = goalTrackingOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<GoalTrackingSummary>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "mDateTransformations observer");
            if (a2 != wh4.DATABASE_LOADING) {
                if (list != null && (!wg6.a((Object) this.a.j, (Object) list))) {
                    this.a.j = list;
                    rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new re5$d$a(this, list, (xe6) null), 3, (Object) null);
                }
                this.a.n.c(!this.a.p.I());
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewMonthPresenter(qe5 qe5, UserRepository userRepository, an4 an4, GoalTrackingRepository goalTrackingRepository) {
        wg6.b(qe5, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.n = qe5;
        this.o = userRepository;
        this.p = an4;
        this.q = goalTrackingRepository;
        LiveData<yx5<List<GoalTrackingSummary>>> b2 = sd.b(this.e, new c(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026alTrackingSummaries\n    }");
        this.l = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        Date date = goalTrackingOverviewMonthPresenter.f;
        if (date != null) {
            return date;
        }
        wg6.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date g(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        Date date = goalTrackingOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wg6.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date k(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        Date date = goalTrackingOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wg6.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "start");
        h();
        LiveData<yx5<List<GoalTrackingSummary>>> liveData = this.l;
        qe5 qe5 = this.n;
        if (qe5 != null) {
            liveData.a((GoalTrackingOverviewMonthFragment) qe5, new d(this));
            this.n.c(!this.p.I());
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "stop");
        try {
            LiveData<yx5<List<GoalTrackingSummary>>> liveData = this.l;
            qe5 qe5 = this.n;
            if (qe5 != null) {
                liveData.a((GoalTrackingOverviewMonthFragment) qe5);
                this.k.a(this.n);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                wg6.d("mCurrentDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("GoalTrackingOverviewMonthPresenter", sb.toString());
                    return;
                }
                wg6.d("mCurrentDate");
                throw null;
            }
        }
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("GoalTrackingOverviewMonthPresenter", sb2.toString());
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
            return;
        }
        wg6.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.n.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.i;
        if (date3 == null) {
            date3 = new Date();
        }
        this.g = date3;
        Date date4 = this.g;
        if (date4 != null) {
            if (!bk4.a(date4.getTime(), date.getTime())) {
                Calendar p2 = bk4.p(date);
                wg6.a((Object) p2, "DateHelper.getStartOfMonth(date)");
                Date time = p2.getTime();
                wg6.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.g = time;
            }
            Boolean s = bk4.s(date);
            wg6.a((Object) s, "DateHelper.isThisMonth(date)");
            if (s.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar k2 = bk4.k(date);
                wg6.a((Object) k2, "DateHelper.getEndOfMonth(date)");
                date2 = k2.getTime();
                wg6.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.h = date2;
            Date date5 = this.h;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.g;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wg6.d("mStartDate");
                throw null;
            }
            wg6.d("mEndDate");
            throw null;
        }
        wg6.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        if (this.e.a() == null || !bk4.d((Date) this.e.a(), date)) {
            this.e.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<GoalTrackingSummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        if (list != null) {
            for (GoalTrackingSummary next : list) {
                Date component1 = next.component1();
                int component2 = next.component2();
                int component3 = next.component3();
                if (component3 > 0) {
                    Date o2 = bk4.o(component1);
                    wg6.a((Object) o2, "DateHelper.getStartOfDay(date1)");
                    treeMap.put(Long.valueOf(o2.getTime()), Float.valueOf(((float) component2) / ((float) component3)));
                } else {
                    Date o3 = bk4.o(component1);
                    wg6.a((Object) o3, "DateHelper.getStartOfDay(date1)");
                    treeMap.put(Long.valueOf(o3.getTime()), Float.valueOf(0.0f));
                }
            }
        }
        return treeMap;
    }
}
