package com.portfolio.platform.uirenew.home.customize.diana;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.ft;
import com.fossil.ia5;
import com.fossil.ja5;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.p84;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.wq;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeTutorialFragment extends BaseFragment {
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public ax5<p84> g;
    @DexIgnore
    public ja5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CustomizeTutorialFragment a(String str) {
            wg6.b(str, "watchAppId");
            CustomizeTutorialFragment customizeTutorialFragment = new CustomizeTutorialFragment();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCHAPP_ID", str);
            customizeTutorialFragment.setArguments(bundle);
            return customizeTutorialFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeTutorialFragment a;

        @DexIgnore
        public b(CustomizeTutorialFragment customizeTutorialFragment) {
            this.a = customizeTutorialFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<ja5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeTutorialFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ p84 b;

        @DexIgnore
        public c(CustomizeTutorialFragment customizeTutorialFragment, p84 p84) {
            this.a = customizeTutorialFragment;
            this.b = p84;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ja5.b bVar) {
            Integer a2 = bVar.a();
            if (a2 != null) {
                wq.a(this.a).a(Integer.valueOf(a2.intValue())).a(ft.a).a(true).a(this.b.r);
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v9, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        CustomizeTutorialFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        p84 a2 = kb.a(layoutInflater, 2131558535, viewGroup, false, e1());
        PortfolioApp.get.instance().g().a(new ia5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            ja5 a3 = vd.a(this, w04).a(ja5.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026ialViewModel::class.java)");
            this.h = a3;
            a2.q.setOnClickListener(new b(this));
            Bundle arguments = getArguments();
            if (arguments != null) {
                ja5 ja5 = this.h;
                if (ja5 != null) {
                    String string = arguments.getString("EXTRA_WATCHAPP_ID");
                    if (string != null) {
                        wg6.a((Object) string, "it.getString(EXTRA_WATCHAPP_ID)!!");
                        ja5.a(string);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mViewModel");
                    throw null;
                }
            }
            ja5 ja52 = this.h;
            if (ja52 != null) {
                ja52.a().a(getViewLifecycleOwner(), new c(this, a2));
                this.g = new ax5<>(this, a2);
                ax5<p84> ax5 = this.g;
                if (ax5 != null) {
                    p84 a4 = ax5.a();
                    if (a4 != null) {
                        wg6.a((Object) a4, "mBinding.get()!!");
                        return a4.d();
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }
}
