package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bv5;
import com.fossil.ce;
import com.fossil.ik6;
import com.fossil.is4;
import com.fossil.ks4;
import com.fossil.ld;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.ns4;
import com.fossil.os4;
import com.fossil.qg6;
import com.fossil.qv5;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.rv5;
import com.fossil.sd;
import com.fossil.uv5$g$a;
import com.fossil.uv5$h$a;
import com.fossil.v3;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xm4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDevicePresenter extends qv5 {
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<bv5.c> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<bv5.c> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Runnable j;
    @DexIgnore
    public /* final */ f k;
    @DexIgnore
    public /* final */ e l;
    @DexIgnore
    public /* final */ ce m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ an4 o;
    @DexIgnore
    public /* final */ rv5 p;
    @DexIgnore
    public /* final */ ks4 q;
    @DexIgnore
    public /* final */ ns4 r;
    @DexIgnore
    public /* final */ GetAddress s;
    @DexIgnore
    public /* final */ os4 t;
    @DexIgnore
    public /* final */ PortfolioApp u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<ks4.d, ks4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public b(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ks4.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onSuccess");
            this.a.a(dVar.a());
        }

        @DexIgnore
        public void a(ks4.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onError");
            this.a.p.X0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<is4.d, is4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public c(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetAddress.d dVar) {
            wg6.b(dVar, "responseValue");
            String a2 = dVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "GetCityName onSuccess - address: " + a2);
            this.a.p.B(a2);
        }

        @DexIgnore
        public void a(GetAddress.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName onError");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<ns4.d, ns4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public d(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ns4.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onSuccess");
            if (wg6.a((Object) this.a.k(), (Object) dVar.a().getDeviceSerial())) {
                this.a.a(dVar.a());
            }
        }

        @DexIgnore
        public void a(ns4.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onError");
            this.a.h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public e(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            DeviceLocation serializableExtra = intent.getSerializableExtra("device_location");
            String stringExtra = intent.getStringExtra("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "onReceive - location: " + serializableExtra + ",serial: " + stringExtra);
            if (wg6.a((Object) stringExtra, (Object) this.a.k()) && serializableExtra != null) {
                this.a.a(serializableExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public f(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (wg6.a((Object) stringExtra, (Object) this.a.k()) && wg6.a((Object) stringExtra, (Object) this.a.i())) {
                this.a.j().a(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public g(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<bv5.c> apply(String str) {
            this.a.j.run();
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new uv5$g$a(this, str, (xe6) null), 3, (Object) null);
            return this.a.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public h(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.fossil.os4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.uv5$h$a] */
        public final void run() {
            String k = this.a.k();
            if (k != null) {
                this.a.t.a(new os4.d(k), new uv5$h$a(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ld<bv5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public i(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bv5.c cVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "observer device " + cVar);
            if (cVar != null) {
                this.a.p.a(cVar);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public FindDevicePresenter(ce ceVar, DeviceRepository deviceRepository, an4 an4, rv5 rv5, ks4 ks4, ns4 ns4, GetAddress getAddress, os4 os4, PortfolioApp portfolioApp) {
        wg6.b(ceVar, "mLocalBroadcastManager");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(rv5, "mView");
        wg6.b(ks4, "mGetLocation");
        wg6.b(ns4, "mLoadLocation");
        wg6.b(getAddress, "mGetAddress");
        wg6.b(os4, "mGetRssi");
        wg6.b(portfolioApp, "mApp");
        this.m = ceVar;
        this.n = deviceRepository;
        this.o = an4;
        this.p = rv5;
        this.q = ks4;
        this.r = ns4;
        this.s = getAddress;
        this.t = os4;
        this.u = portfolioApp;
        LiveData<bv5.c> b2 = sd.b(this.e, new g(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026viceWrapperLiveData\n    }");
        this.g = b2;
        this.i = new Handler();
        this.j = new h(this);
        this.k = new f(this);
        this.l = new e(this);
    }

    @DexIgnore
    public final String i() {
        return PortfolioApp.get.instance().e();
    }

    @DexIgnore
    public final MutableLiveData<String> j() {
        return this.e;
    }

    @DexIgnore
    public String k() {
        return (String) this.e.a();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.fossil.ns4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$d] */
    public final void l() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation");
        this.r.a(new ns4.b(), new d(this));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void m() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "locateOnMap");
        if (!NetworkUtils.isNetworkAvailable(this.u)) {
            this.p.a(601, (String) null);
            return;
        }
        xm4 xm4 = xm4.d;
        rv5 rv5 = this.p;
        if (rv5 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment");
        } else if (!xm4.a(xm4, ((FindDeviceFragment) rv5).getContext(), "FIND_DEVICE", false, false, false, 28, (Object) null)) {
            this.p.i0();
        } else {
            String k2 = k();
            if (k2 != null) {
                if (TextUtils.equals(k2, i()) && a(k2)) {
                    Context applicationContext = this.u.getApplicationContext();
                    wg6.a((Object) applicationContext, "mApp.applicationContext");
                    if (a(applicationContext)) {
                        l();
                        return;
                    }
                }
                h();
            }
        }
    }

    @DexIgnore
    public void n() {
        this.p.a(this);
    }

    @DexIgnore
    public final void a(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "serial");
        this.e.a(str);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void f() {
        this.m.a(this.l, new IntentFilter(MFDeviceService.V.a()));
        Object r0 = this.u;
        f fVar = this.k;
        r0.registerReceiver(fVar, new IntentFilter(this.u.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<bv5.c> liveData = this.g;
        rv5 rv5 = this.p;
        if (rv5 != null) {
            liveData.a((BaseFragment) rv5, new i(this));
            this.t.f();
            if (wg6.a((Object) this.u.e(), (Object) (String) this.e.a())) {
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "update RSSI only for active device");
                b(this.h);
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void g() {
        try {
            MutableLiveData<bv5.c> mutableLiveData = this.f;
            rv5 rv5 = this.p;
            if (rv5 != null) {
                mutableLiveData.a((BaseFragment) rv5);
                this.e.a(this.p);
                this.g.a(this.p);
                this.m.a(this.l);
                this.u.unregisterReceiver(this.k);
                this.t.g();
                this.i.removeCallbacksAndMessages((Object) null);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("FindDevicePresenter", "stop with " + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.ks4] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$b] */
    public final void h() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation");
        String k2 = k();
        if (k2 != null) {
            this.q.a(new ks4.b(k2), new b(this));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v8, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.device.locate.map.usecase.GetAddress] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$c] */
    public final void a(DeviceLocation deviceLocation) {
        wg6.b(deviceLocation, "location");
        this.p.a(deviceLocation.getTimeStamp());
        double latitude = deviceLocation.getLatitude();
        double longitude = deviceLocation.getLongitude();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "handleLocation latitude=" + latitude + ", longitude=" + longitude);
        if (latitude != 0.0d && longitude != 0.0d) {
            this.p.a(Double.valueOf(latitude), Double.valueOf(longitude));
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName");
            this.s.a(new GetAddress.b(latitude, longitude), new c(this));
        }
    }

    @DexIgnore
    public void b(boolean z) {
        this.o.f(z);
    }

    @DexIgnore
    public final void b(int i2) {
        if (i2 != -9999) {
            this.p.l(i2);
        }
        this.i.removeCallbacks(this.j);
        this.i.postDelayed(this.j, v);
    }

    @DexIgnore
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "enableLocate: enable = " + z);
        b(z);
        this.p.c(z, this.o.H());
        if (z) {
            m();
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        IButtonConnectivity b2 = PortfolioApp.get.b();
        if (b2 == null || b2.getGattState(str) != 2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean a(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(), "location_mode") != 0;
    }
}
