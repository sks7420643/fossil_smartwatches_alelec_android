package com.portfolio.platform.uirenew.permission;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.gt5;
import com.fossil.hg6;
import com.fossil.ht5;
import com.fossil.kt5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vd6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public kt5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, ArrayList<Integer> arrayList) {
            wg6.b(context, "context");
            wg6.b(arrayList, "listPermissions");
            Intent intent = new Intent(context, PermissionActivity.class);
            intent.putIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, ArrayList arrayList, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.a(context, arrayList, z);
        }

        @DexIgnore
        public final void a(Context context, ArrayList<Integer> arrayList, boolean z) {
            wg6.b(context, "context");
            wg6.b(arrayList, "listPermissions");
            Intent intent = new Intent(context, PermissionActivity.class);
            intent.putIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS", arrayList);
            if (z) {
                intent.putExtra("PERMISSION_REQUEST_RESPONSE_FLAG", true);
            }
            intent.setFlags(536870912);
            ((Activity) context).startActivityForResult(intent, 111);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<Integer, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke((Integer) obj));
        }

        @DexIgnore
        public final boolean invoke(Integer num) {
            return num == null || num.intValue() != 10;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.permission.PermissionActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        ArrayList<Integer> arrayList;
        gt5 gt5;
        gt5 gt52;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (Build.VERSION.SDK_INT < 26) {
            setRequestedOrientation(1);
        }
        boolean booleanExtra = getIntent().getBooleanExtra("PERMISSION_REQUEST_RESPONSE_FLAG", false);
        if (booleanExtra) {
            arrayList = getIntent().getIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS");
            if (arrayList != null) {
                gt5 = (PermissionFragment) getSupportFragmentManager().b(2131362119);
                if (gt5 == null) {
                    gt5 = PermissionFragment.p.a(booleanExtra);
                    a((Fragment) gt5, PermissionFragment.p.a(), 2131362119);
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            arrayList = getIntent().getIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS");
            if (arrayList != null) {
                if (arrayList.contains(10)) {
                    gt52 = (AllowNotificationServiceFragment) getSupportFragmentManager().b(2131362119);
                    if (gt52 == null) {
                        gt52 = AllowNotificationServiceFragment.i.a();
                        a((Fragment) gt52, "AllowNotificationServiceFragment", 2131362119);
                    }
                    vd6.a(arrayList, b.INSTANCE);
                } else {
                    gt52 = (PermissionFragment) getSupportFragmentManager().b(2131362119);
                    if (gt52 == null) {
                        gt52 = PermissionFragment.p.b();
                        a((Fragment) gt52, PermissionFragment.p.a(), 2131362119);
                    }
                }
                gt5 = gt52;
            } else {
                wg6.a();
                throw null;
            }
        }
        y04 g = PortfolioApp.get.instance().g();
        if (gt5 != null) {
            g.a(new ht5(gt5, arrayList)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionContract.View");
    }
}
