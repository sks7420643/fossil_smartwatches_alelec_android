package com.portfolio.platform.uirenew.permission;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.ft5;
import com.fossil.gt5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.n64;
import com.fossil.nh6;
import com.fossil.nu4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AllowNotificationServiceFragment extends BaseFragment implements gt5 {
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public ax5<n64> f;
    @DexIgnore
    public ft5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final AllowNotificationServiceFragment a() {
            return new AllowNotificationServiceFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AllowNotificationServiceFragment a;

        @DexIgnore
        public b(AllowNotificationServiceFragment allowNotificationServiceFragment) {
            this.a = allowNotificationServiceFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            AllowNotificationServiceFragment.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AllowNotificationServiceFragment a;

        @DexIgnore
        public c(AllowNotificationServiceFragment allowNotificationServiceFragment) {
            this.a = allowNotificationServiceFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    public static final /* synthetic */ ft5 a(AllowNotificationServiceFragment allowNotificationServiceFragment) {
        ft5 ft5 = allowNotificationServiceFragment.g;
        if (ft5 != null) {
            return ft5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        ft5 ft5 = this.g;
        if (ft5 != null) {
            ft5.i();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        n64 a2 = kb.a(layoutInflater, 2131558506, viewGroup, false, e1());
        Object r5 = a2.q;
        wg6.a((Object) r5, "binding.ftvDescription");
        nh6 nh6 = nh6.a;
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886572);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026AccessToYourNotification)");
        Object[] objArr = {PortfolioApp.get.instance().i()};
        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        r5.setText(format);
        a2.s.setOnClickListener(new b(this));
        a2.r.setOnClickListener(new c(this));
        this.f = new ax5<>(this, a2);
        ax5<n64> ax5 = this.f;
        if (ax5 != null) {
            n64 a4 = ax5.a();
            if (a4 != null) {
                wg6.a((Object) a4, "mBinding.get()!!");
                return a4.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ft5 ft5 = this.g;
        if (ft5 != null) {
            ft5.g();
            AllowNotificationServiceFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        AllowNotificationServiceFragment.super.onResume();
        ft5 ft5 = this.g;
        if (ft5 != null) {
            ft5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void t(List<nu4.c> list) {
        wg6.b(list, "listPermissionModel");
    }

    @DexIgnore
    public void a(ft5 ft5) {
        wg6.b(ft5, "presenter");
        this.g = ft5;
    }
}
