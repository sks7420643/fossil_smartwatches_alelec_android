package com.portfolio.platform.uirenew.welcome;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.fossil.aw5;
import com.fossil.kl4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zv5;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WelcomeFragment extends BaseFragment implements aw5, View.OnClickListener {
    @DexIgnore
    public zv5 f;
    @DexIgnore
    public ShimmerFrameLayout g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void N0() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.B;
        Context context = getContext();
        if (context != null) {
            wg6.a((Object) context, "context!!");
            aVar.a(context);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void S0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.G;
            wg6.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362216) {
                zv5 zv5 = this.f;
                if (zv5 != null) {
                    zv5.h();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362612) {
                zv5 zv52 = this.f;
                if (zv52 != null) {
                    zv52.i();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558627, viewGroup, false);
        ((FlexibleButton) inflate.findViewById(2131362216)).setOnClickListener(this);
        ((ImageView) inflate.findViewById(2131362612)).setOnClickListener(this);
        ShimmerFrameLayout findViewById = inflate.findViewById(2131362943);
        wg6.a((Object) findViewById, "view.findViewById(R.id.shm_note)");
        this.g = findViewById;
        W("tutorial_view");
        return inflate;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        WelcomeFragment.super.onPause();
        ShimmerFrameLayout shimmerFrameLayout = this.g;
        if (shimmerFrameLayout != null) {
            shimmerFrameLayout.stopShimmerAnimation();
            zv5 zv5 = this.f;
            if (zv5 != null) {
                zv5.g();
                kl4 g1 = g1();
                if (g1 != null) {
                    g1.a("");
                    return;
                }
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("shimmerContainer");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        WelcomeFragment.super.onResume();
        ShimmerFrameLayout shimmerFrameLayout = this.g;
        if (shimmerFrameLayout != null) {
            shimmerFrameLayout.startShimmerAnimation();
            zv5 zv5 = this.f;
            if (zv5 != null) {
                zv5.f();
                kl4 g1 = g1();
                if (g1 != null) {
                    g1.d();
                    return;
                }
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("shimmerContainer");
        throw null;
    }

    @DexIgnore
    public void a(zv5 zv5) {
        wg6.b(zv5, "presenter");
        this.f = zv5;
    }
}
