package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.dt4;
import com.fossil.dy5;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hl4;
import com.fossil.hr5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ir5;
import com.fossil.jh6;
import com.fossil.jm4;
import com.fossil.lf6;
import com.fossil.lh4;
import com.fossil.ll6;
import com.fossil.lr5$b$a;
import com.fossil.lr5$c$a;
import com.fossil.lr5$d$a;
import com.fossil.lr5$d$b;
import com.fossil.lr5$e$a;
import com.fossil.lr5$f$a;
import com.fossil.nc6;
import com.fossil.q34;
import com.fossil.qg6;
import com.fossil.rh4;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sx5;
import com.fossil.tj4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.ReadablePartial;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupPresenter extends hr5 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public /* final */ ServerSettingRepository A;
    @DexIgnore
    public SignUpEmailUseCase e;
    @DexIgnore
    public SignUpSocialUseCase f;
    @DexIgnore
    public dt4 g;
    @DexIgnore
    public AnalyticsHelper h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Calendar u; // = Calendar.getInstance();
    @DexIgnore
    public SignUpSocialAuth v;
    @DexIgnore
    public SignUpEmailAuth w;
    @DexIgnore
    public /* final */ ir5 x;
    @DexIgnore
    public /* final */ GetRecommendedGoalUseCase y;
    @DexIgnore
    public /* final */ UserRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(jh6 jh6, jh6 jh62, jh6 jh63, SignUpSocialAuth signUpSocialAuth, xe6 xe6, ProfileSetupPresenter profileSetupPresenter) {
            super(2, xe6);
            this.$dataLocationSharingPrivacyVersionLatest = jh6;
            this.$privacyVersionLatest = jh62;
            this.$termOfUseVersionLatest = jh63;
            this.$it = signUpSocialAuth;
            this.this$0 = profileSetupPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, xe6, this.this$0);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                lr5$b$a lr5_b_a = new lr5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, lr5_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(jh6 jh6, jh6 jh62, jh6 jh63, SignUpEmailAuth signUpEmailAuth, xe6 xe6, ProfileSetupPresenter profileSetupPresenter) {
            super(2, xe6);
            this.$dataLocationSharingPrivacyVersionLatest = jh6;
            this.$privacyVersionLatest = jh62;
            this.$termOfUseVersionLatest = jh63;
            this.$it = signUpEmailAuth;
            this.this$0 = profileSetupPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, xe6, this.this$0);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                lr5$c$a lr5_c_a = new lr5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, lr5_c_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1", f = "ProfileSetupPresenter.kt", l = {399, 406}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $service;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ProfileSetupPresenter profileSetupPresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileSetupPresenter;
            this.$service = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$service, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.usecase.GetRecommendedGoalUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r8v4, types: [com.fossil.lr5$d$b, com.portfolio.platform.CoroutineUseCase$e] */
        public final Object invokeSuspend(Object obj) {
            MFUser mFUser;
            int i;
            il6 il6;
            Object a = ff6.a();
            int i2 = this.label;
            if (i2 == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 a2 = this.this$0.b();
                lr5$d$a lr5_d_a = new lr5$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, lr5_d_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i2 == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i2 == 2) {
                i = this.I$0;
                MFUser mFUser2 = (MFUser) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                mFUser = (MFUser) this.L$2;
                Object b = this.this$0.y;
                int heightInCentimeters = mFUser.getHeightInCentimeters();
                int weightInGrams = mFUser.getWeightInGrams();
                rh4 gender = mFUser.getGender();
                wg6.a((Object) gender, "it.gender");
                b.a(new GetRecommendedGoalUseCase.c(i, heightInCentimeters, weightInGrams, gender), new lr5$d$b(this));
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            mFUser = (MFUser) obj;
            if (mFUser != null) {
                int age = MFUser.getAge(mFUser.getBirthday());
                this.this$0.o().b(mFUser.getUserId());
                PortfolioApp instance = PortfolioApp.get.instance();
                String userId = mFUser.getUserId();
                wg6.a((Object) userId, "it.userId");
                instance.q(userId);
                PortfolioApp instance2 = PortfolioApp.get.instance();
                this.L$0 = il6;
                this.L$1 = mFUser;
                this.L$2 = mFUser;
                this.I$0 = age;
                this.label = 2;
                if (instance2.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
                i = age;
                Object b2 = this.this$0.y;
                int heightInCentimeters2 = mFUser.getHeightInCentimeters();
                int weightInGrams2 = mFUser.getWeightInGrams();
                rh4 gender2 = mFUser.getGender();
                wg6.a((Object) gender2, "it.gender");
                b2.a(new GetRecommendedGoalUseCase.c(i, heightInCentimeters2, weightInGrams2, gender2), new lr5$d$b(this));
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3", f = "ProfileSetupPresenter.kt", l = {147}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ProfileSetupPresenter profileSetupPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileSetupPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ProfileSetupPresenter profileSetupPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ProfileSetupPresenter profileSetupPresenter2 = this.this$0;
                dl6 a2 = profileSetupPresenter2.b();
                lr5$e$a lr5_e_a = new lr5$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = profileSetupPresenter2;
                this.label = 1;
                obj = gk6.a(a2, lr5_e_a, this);
                if (obj == a) {
                    return a;
                }
                profileSetupPresenter = profileSetupPresenter2;
            } else if (i == 1) {
                profileSetupPresenter = (ProfileSetupPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            profileSetupPresenter.j = (MFUser) obj;
            MFUser d = this.this$0.j;
            if (d != null) {
                this.this$0.x.b(d);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1", f = "ProfileSetupPresenter.kt", l = {558}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ProfileSetupPresenter profileSetupPresenter, MFUser mFUser, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileSetupPresenter;
            this.$user = mFUser;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$user, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            String str;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                this.this$0.x.U0();
                this.this$0.x.k();
                dl6 a2 = this.this$0.b();
                lr5$f$a lr5_f_a = new lr5$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, lr5_f_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap4 = (ap4) obj;
            if (ap4 instanceof cp4) {
                FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.B, "updateAccount successfully");
                PortfolioApp.get.instance().g().a(this.this$0);
                ProfileSetupPresenter profileSetupPresenter = this.this$0;
                lh4 authType = this.$user.getAuthType();
                wg6.a((Object) authType, "user.authType");
                String value = authType.getValue();
                wg6.a((Object) value, "user.authType.value");
                profileSetupPresenter.d(value);
            } else if (ap4 instanceof zo4) {
                FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.B, "updateAccount failed");
                ir5 f = this.this$0.x;
                zo4 zo4 = (zo4) ap4;
                int a3 = zo4.a();
                ServerError c = zo4.c();
                if (c == null || (str = c.getMessage()) == null) {
                    str = "";
                }
                f.g(a3, str);
                this.this$0.x.i();
                this.this$0.x.B0();
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ProfileSetupPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "ProfileSetupPresenter::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public ProfileSetupPresenter(ir5 ir5, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository, ServerSettingRepository serverSettingRepository) {
        wg6.b(ir5, "mView");
        wg6.b(getRecommendedGoalUseCase, "mGetRecommendedGoalUseCase");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(serverSettingRepository, "mServerSettingRepository");
        this.x = ir5;
        this.y = getRecommendedGoalUseCase;
        this.z = userRepository;
        this.A = serverSettingRepository;
    }

    @DexIgnore
    public void A() {
        this.x.a(this);
    }

    @DexIgnore
    public final void B() {
        if (!w()) {
            this.x.U0();
        } else if (!x()) {
            this.x.U0();
        } else if (!y()) {
            this.x.U0();
        } else if (!v()) {
            this.x.U0();
        } else if (!q34.a.a().a(m())) {
            this.x.U0();
        } else {
            this.x.B0();
        }
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void j() {
        Bundle bundle = new Bundle();
        bundle.putInt("DAY", this.u.get(5));
        bundle.putInt("MONTH", this.u.get(2) + 1);
        bundle.putInt("YEAR", this.u.get(1));
        this.x.a(bundle);
    }

    @DexIgnore
    public void k() {
        Bundle bundle = new Bundle();
        Calendar instance = Calendar.getInstance();
        bundle.putInt("DAY", instance.get(5));
        bundle.putInt("MONTH", instance.get(2) + 1);
        bundle.putInt("YEAR", instance.get(1) - 32);
        this.x.a(bundle);
    }

    @DexIgnore
    public SignUpEmailAuth l() {
        return this.w;
    }

    @DexIgnore
    public final List<String> m() {
        ArrayList arrayList = new ArrayList();
        if (this.p) {
            arrayList.add("Agree terms of use and privacy");
        }
        if (this.q) {
            arrayList.add("Allow Gather data usage");
        }
        if (this.r) {
            arrayList.add("Agree term of use");
        }
        if (this.s) {
            arrayList.add("Agree privacy");
        }
        if (this.t) {
            arrayList.add("Allow location data processing");
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String n() {
        String a2 = dy5.a(dy5.c.PRIVACY, (dy5.b) null);
        String str = jm4.a((Context) PortfolioApp.get.instance(), 2131886719).toString();
        wg6.a((Object) a2, "privacyPolicyUrl");
        return xj6.a(str, "privacy_policy", a2, false, 4, (Object) null);
    }

    @DexIgnore
    public final AnalyticsHelper o() {
        AnalyticsHelper analyticsHelper = this.h;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wg6.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final SignUpEmailUseCase p() {
        SignUpEmailUseCase signUpEmailUseCase = this.e;
        if (signUpEmailUseCase != null) {
            return signUpEmailUseCase;
        }
        wg6.d("mSignUpEmailUseCase");
        throw null;
    }

    @DexIgnore
    public final SignUpSocialUseCase q() {
        SignUpSocialUseCase signUpSocialUseCase = this.f;
        if (signUpSocialUseCase != null) {
            return signUpSocialUseCase;
        }
        wg6.d("mSignUpSocialUseCase");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String r() {
        String a2 = dy5.a(dy5.c.PRIVACY, (dy5.b) null);
        String str = jm4.a((Context) PortfolioApp.get.instance(), 2131886720).toString();
        wg6.a((Object) a2, "privacyPolicyUrl");
        return xj6.a(str, "privacy_policy", a2, false, 4, (Object) null);
    }

    @DexIgnore
    public SignUpSocialAuth s() {
        return this.v;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String t() {
        String a2 = dy5.a(dy5.c.TERMS, (dy5.b) null);
        String a3 = dy5.a(dy5.c.PRIVACY, (dy5.b) null);
        String str = jm4.a((Context) PortfolioApp.get.instance(), 2131886734).toString();
        wg6.a((Object) a2, "termOfUseUrl");
        String a4 = xj6.a(str, "term_of_use_url", a2, false, 4, (Object) null);
        wg6.a((Object) a3, "privacyPolicyUrl");
        return xj6.a(a4, "privacy_policy", a3, false, 4, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String u() {
        String a2 = dy5.a(dy5.c.TERMS, (dy5.b) null);
        String str = jm4.a((Context) PortfolioApp.get.instance(), 2131886721).toString();
        wg6.a((Object) a2, "termOfUseUrl");
        return xj6.a(str, "term_of_use_url", a2, false, 4, (Object) null);
    }

    @DexIgnore
    public final boolean v() {
        Calendar calendar = this.u;
        if (calendar == null) {
            return false;
        }
        if (calendar != null) {
            Years yearsBetween = Years.yearsBetween((ReadablePartial) LocalDate.fromCalendarFields(calendar), (ReadablePartial) LocalDate.now());
            wg6.a((Object) yearsBetween, "age");
            if (yearsBetween.getYears() >= 16) {
                return true;
            }
            return false;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean w() {
        if (this.k.length() == 0) {
            this.x.a(false, false, "");
            return false;
        } else if (!sx5.a(this.k)) {
            ir5 ir5 = this.x;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886766);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            ir5.a(false, true, a2);
            return false;
        } else {
            this.x.a(true, false, "");
            return true;
        }
    }

    @DexIgnore
    public final boolean x() {
        String str = this.l;
        if (!(str == null || xj6.a(str))) {
            this.x.E(true);
            return true;
        }
        this.x.E(false);
        return false;
    }

    @DexIgnore
    public final boolean y() {
        String str = this.m;
        if (!(str == null || xj6.a(str))) {
            this.x.D(true);
            return true;
        }
        this.x.D(false);
        return false;
    }

    @DexIgnore
    public boolean z() {
        return this.i;
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "firstName");
        this.l = str;
        x();
        B();
    }

    @DexIgnore
    public void c(String str) {
        wg6.b(str, "lastName");
        this.m = str;
        y();
        B();
    }

    @DexIgnore
    public final rm6 d(String str) {
        wg6.b(str, Constants.SERVICE);
        return ik6.b(e(), (af6) null, (ll6) null, new d(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void e(String str) {
        wg6.b(str, Constants.SERVICE);
        hl4 a2 = AnalyticsHelper.f.a("user_signup");
        String lowerCase = "Source".toLowerCase();
        wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
        a2.a(lowerCase, str);
        a2.a();
        this.x.i();
        this.x.n0();
    }

    @DexIgnore
    public void f() {
        ir5 ir5 = this.x;
        Spanned fromHtml = Html.fromHtml(t());
        wg6.a((Object) fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        ir5.d(fromHtml);
        ir5 ir52 = this.x;
        Spanned fromHtml2 = Html.fromHtml(u());
        wg6.a((Object) fromHtml2, "Html.fromHtml(getTermsOfUseString())");
        ir52.c(fromHtml2);
        ir5 ir53 = this.x;
        Spanned fromHtml3 = Html.fromHtml(r());
        wg6.a((Object) fromHtml3, "Html.fromHtml(getPrivacyPolicyString())");
        ir53.a(fromHtml3);
        ir5 ir54 = this.x;
        Spanned fromHtml4 = Html.fromHtml(n());
        wg6.a((Object) fromHtml4, "Html.fromHtml(getLocationDataString())");
        ir54.b(fromHtml4);
        this.x.g();
        if (!this.i) {
            SignUpSocialAuth signUpSocialAuth = this.v;
            if (signUpSocialAuth != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = B;
                local.d(str, "start - mSocialAuth=" + signUpSocialAuth);
                this.x.a(signUpSocialAuth);
            }
            SignUpEmailAuth signUpEmailAuth = this.w;
            if (signUpEmailAuth != null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = B;
                local2.d(str2, "start - mEmailAuth=" + signUpEmailAuth);
                this.x.c(signUpEmailAuth);
            }
        } else if (this.j == null) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void h() {
        if (this.i) {
            MFUser mFUser = this.j;
            if (mFUser != null) {
                mFUser.setEmail(this.k);
                String str = this.l;
                if (str != null) {
                    mFUser.setFirstName(str);
                    String str2 = this.m;
                    if (str2 != null) {
                        mFUser.setLastName(str2);
                        mFUser.setBirthday(this.o);
                        mFUser.setDiagnosticEnabled(this.q);
                        String str3 = this.n;
                        if (str3 == null) {
                            str3 = rh4.OTHER.toString();
                        }
                        mFUser.setGender(str3);
                        a(mFUser);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = B;
        local.d(str4, "create account socialAuth=" + this.v + " emailAuth=" + this.w);
        this.x.k();
        SignUpSocialAuth signUpSocialAuth = this.v;
        if (signUpSocialAuth != null) {
            signUpSocialAuth.setEmail(this.k);
            String str5 = this.n;
            if (str5 == null) {
                str5 = rh4.OTHER.toString();
            }
            signUpSocialAuth.setGender(str5);
            String str6 = this.o;
            if (str6 != null) {
                signUpSocialAuth.setBirthday(str6);
                signUpSocialAuth.setClientId(tj4.f.a(""));
                String str7 = this.l;
                if (str7 != null) {
                    signUpSocialAuth.setFirstName(str7);
                    String str8 = this.m;
                    if (str8 != null) {
                        signUpSocialAuth.setLastName(str8);
                        signUpSocialAuth.setDiagnosticEnabled(this.q);
                        jh6 jh6 = new jh6();
                        jh6.element = "";
                        jh6 jh62 = new jh6();
                        jh62.element = "";
                        jh6 jh63 = new jh6();
                        jh63.element = "";
                        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(jh6, jh62, jh63, signUpSocialAuth, (xe6) null, this), 3, (Object) null);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        SignUpEmailAuth signUpEmailAuth = this.w;
        if (signUpEmailAuth != null) {
            String str9 = this.n;
            if (str9 == null) {
                str9 = rh4.OTHER.toString();
            }
            signUpEmailAuth.setGender(str9);
            String str10 = this.o;
            if (str10 != null) {
                signUpEmailAuth.setBirthday(str10);
                signUpEmailAuth.setClientId(tj4.f.a(""));
                String str11 = this.l;
                if (str11 != null) {
                    signUpEmailAuth.setFirstName(str11);
                    String str12 = this.m;
                    if (str12 != null) {
                        signUpEmailAuth.setLastName(str12);
                        signUpEmailAuth.setDiagnosticEnabled(this.q);
                        jh6 jh64 = new jh6();
                        jh64.element = "";
                        jh6 jh65 = new jh6();
                        jh65.element = "";
                        jh6 jh66 = new jh6();
                        jh66.element = "";
                        rm6 unused2 = ik6.b(e(), (af6) null, (ll6) null, new c(jh64, jh65, jh66, signUpEmailAuth, (xe6) null, this), 3, (Object) null);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public Calendar i() {
        Calendar calendar = this.u;
        wg6.a((Object) calendar, "mBirthdayCalendar");
        return calendar;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "email");
        this.k = str;
        w();
        B();
    }

    @DexIgnore
    public void d(boolean z2) {
        this.r = z2;
        B();
    }

    @DexIgnore
    public void b(boolean z2) {
        this.t = z2;
        B();
    }

    @DexIgnore
    public void c(boolean z2) {
        this.s = z2;
        B();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(Date date, Calendar calendar) {
        wg6.b(date, "data");
        wg6.b(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d(B, "onBirthDayChanged");
        this.o = bk4.e(date);
        this.u = calendar;
        ir5 ir5 = this.x;
        String d2 = bk4.d(date);
        wg6.a((Object) d2, "DateHelper.formatLocalDateMonth(data)");
        ir5.M(d2);
        if (!v()) {
            ir5 ir52 = this.x;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886722);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026Text__YouCantUseTheAppIf)");
            ir52.b(false, a2);
        } else {
            this.x.b(true, "");
        }
        B();
    }

    @DexIgnore
    public void e(boolean z2) {
        this.p = z2;
        B();
    }

    @DexIgnore
    public void a(rh4 rh4) {
        wg6.b(rh4, "gender");
        this.n = rh4.toString();
        this.x.a(rh4);
    }

    @DexIgnore
    public void f(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.q = z2;
        B();
    }

    @DexIgnore
    public void a(SignUpEmailAuth signUpEmailAuth) {
        wg6.b(signUpEmailAuth, "auth");
        this.w = signUpEmailAuth;
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        this.v = signUpSocialAuth;
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "updateAccount - userId = " + mFUser.getUserId());
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new f(this, mFUser, (xe6) null), 3, (Object) null);
    }
}
