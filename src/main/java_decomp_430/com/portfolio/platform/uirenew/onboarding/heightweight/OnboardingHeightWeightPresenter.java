package com.portfolio.platform.uirenew.onboarding.heightweight;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.nw5;
import com.fossil.pq5;
import com.fossil.qg6;
import com.fossil.qq5;
import com.fossil.rh4;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.tq5$c$a;
import com.fossil.tq5$d$a;
import com.fossil.wg6;
import com.fossil.wj4;
import com.fossil.xe6;
import com.fossil.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OnboardingHeightWeightPresenter extends pq5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ qq5 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ GetRecommendedGoalUseCase h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return OnboardingHeightWeightPresenter.i;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<nw5.d, nw5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser a;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public b(MFUser mFUser, OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, boolean z) {
            this.a = mFUser;
            this.b = onboardingHeightWeightPresenter;
            this.c = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetRecommendedGoalUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            this.b.a(this.a, this.c);
        }

        @DexIgnore
        public void a(GetRecommendedGoalUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.b.a(this.a, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1", f = "OnboardingHeightWeightPresenter.kt", l = {123}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, MFUser mFUser, boolean z, xe6 xe6) {
            super(2, xe6);
            this.this$0 = onboardingHeightWeightPresenter;
            this.$user = mFUser;
            this.$isDefaultValuesUsed = z;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$user, this.$isDefaultValuesUsed, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.$user.isUseDefaultBiometric()) {
                    this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
                }
                dl6 a2 = this.this$0.b();
                tq5$c$a tq5_c_a = new tq5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, tq5_c_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.i();
            this.this$0.f.q0();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1", f = "OnboardingHeightWeightPresenter.kt", l = {31}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = onboardingHeightWeightPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0079  */
        public final Object invokeSuspend(Object obj) {
            MFUser b;
            OnboardingHeightWeightPresenter onboardingHeightWeightPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.e == null) {
                    OnboardingHeightWeightPresenter onboardingHeightWeightPresenter2 = this.this$0;
                    dl6 a2 = onboardingHeightWeightPresenter2.b();
                    tq5$d$a tq5_d_a = new tq5$d$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = onboardingHeightWeightPresenter2;
                    this.label = 1;
                    obj = gk6.a(a2, tq5_d_a, this);
                    if (obj == a) {
                        return a;
                    }
                    onboardingHeightWeightPresenter = onboardingHeightWeightPresenter2;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = OnboardingHeightWeightPresenter.j.a();
                local.d(a3, "start with currentUser=" + this.this$0.e);
                b = this.this$0.e;
                if (b != null) {
                    if (b.getHeightInCentimeters() == 0 || b.getWeightInGrams() == 0) {
                        wj4 wj4 = wj4.a;
                        rh4 gender = b.getGender();
                        wg6.a((Object) gender, "it.gender");
                        lc6<Integer, Integer> a4 = wj4.a(gender, MFUser.getAge(b.getBirthday()));
                        if (b.getHeightInCentimeters() == 0) {
                            b.setHeightInCentimeters(a4.getFirst().intValue());
                        }
                        if (b.getWeightInGrams() == 0) {
                            b.setWeightInGrams(a4.getSecond().intValue() * 1000);
                        }
                    }
                    if (b.getHeightInCentimeters() > 0) {
                        qq5 d = this.this$0.f;
                        int heightInCentimeters = b.getHeightInCentimeters();
                        zh4 heightUnit = b.getHeightUnit();
                        wg6.a((Object) heightUnit, "it.heightUnit");
                        d.a(heightInCentimeters, heightUnit);
                    }
                    if (b.getWeightInGrams() > 0) {
                        qq5 d2 = this.this$0.f;
                        int weightInGrams = b.getWeightInGrams();
                        zh4 weightUnit = b.getWeightUnit();
                        wg6.a((Object) weightUnit, "it.weightUnit");
                        d2.b(weightInGrams, weightUnit);
                    }
                }
                this.this$0.f.g();
                return cd6.a;
            } else if (i == 1) {
                onboardingHeightWeightPresenter = (OnboardingHeightWeightPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            onboardingHeightWeightPresenter.e = (MFUser) obj;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a32 = OnboardingHeightWeightPresenter.j.a();
            local2.d(a32, "start with currentUser=" + this.this$0.e);
            b = this.this$0.e;
            if (b != null) {
            }
            this.this$0.f.g();
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = OnboardingHeightWeightPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "OnboardingHeightWeightPr\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public OnboardingHeightWeightPresenter(qq5 qq5, UserRepository userRepository, GetRecommendedGoalUseCase getRecommendedGoalUseCase) {
        wg6.b(qq5, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(getRecommendedGoalUseCase, "mGetRecommendedGoalUseCase");
        this.f = qq5;
        this.g = userRepository;
        this.h = getRecommendedGoalUseCase;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onWeightChanged weightInGram=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onHeightChanged heightInCentimeters=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
    }

    @DexIgnore
    public void b(zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitWeightChanged unit=" + zh4);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setWeightUnit(zh4.getValue());
            qq5 qq5 = this.f;
            int weightInGrams = mFUser.getWeightInGrams();
            zh4 weightUnit = mFUser.getWeightUnit();
            wg6.a((Object) weightUnit, "it.weightUnit");
            qq5.b(weightInGrams, weightUnit);
        }
    }

    @DexIgnore
    public void a(zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitHeightChanged unit=" + zh4);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setHeightUnit(zh4.getValue());
            qq5 qq5 = this.f;
            int heightInCentimeters = mFUser.getHeightInCentimeters();
            zh4 heightUnit = mFUser.getHeightUnit();
            wg6.a((Object) heightUnit, "it.heightUnit");
            qq5.a(heightInCentimeters, heightUnit);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.usecase.GetRecommendedGoalUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$b] */
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "completeOnboarding currentUser=" + this.e);
        this.f.k();
        MFUser mFUser = this.e;
        if (mFUser != null) {
            int age = MFUser.getAge(mFUser.getBirthday());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local2.d(str2, "completeOnboarding update heightUnit=" + mFUser.getHeightUnit() + ", weightUnit=" + mFUser.getWeightUnit() + ',' + " height=" + mFUser.getHeightInCentimeters() + ", weight=" + mFUser.getWeightInGrams());
            Object r2 = this.h;
            int heightInCentimeters = mFUser.getHeightInCentimeters();
            int weightInGrams = mFUser.getWeightInGrams();
            rh4 gender = mFUser.getGender();
            wg6.a((Object) gender, "it.gender");
            r2.a(new GetRecommendedGoalUseCase.c(age, heightInCentimeters, weightInGrams, gender), new b(mFUser, this, z));
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser, boolean z) {
        wg6.b(mFUser, "user");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onSetUpHeightWeightComplete register date: " + mFUser.getRegisterDate());
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, mFUser, z, (xe6) null), 3, (Object) null);
    }
}
