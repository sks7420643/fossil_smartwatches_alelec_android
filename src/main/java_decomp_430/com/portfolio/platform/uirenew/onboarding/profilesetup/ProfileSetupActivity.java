package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.jr5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.ProfileSetupFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public ProfileSetupPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            wg6.b(context, "context");
            wg6.b(signUpEmailAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "start with auth=" + signUpEmailAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpSocialAuth signUpSocialAuth) {
            wg6.b(context, "context");
            wg6.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "startSetUpSocial with " + signUpSocialAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("SOCIAL_AUTH", signUpSocialAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            FLogger.INSTANCE.getLocal().d("ProfileSetupActivity", "startUpdateProfile");
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("IS_UPDATE_PROFILE_PROCESS", true);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v0, types: [com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ProfileSetupFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = ProfileSetupFragment.r.a();
            a((Fragment) b, "ProfileSetupFragment", 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new jr5(b)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent socialAuth");
                    ProfileSetupPresenter profileSetupPresenter = this.B;
                    if (profileSetupPresenter != null) {
                        Parcelable parcelableExtra = intent.getParcelableExtra("SOCIAL_AUTH");
                        wg6.a((Object) parcelableExtra, "it.getParcelableExtra(co\u2026ms.Constants.SOCIAL_AUTH)");
                        profileSetupPresenter.a((SignUpSocialAuth) parcelableExtra);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent emailAuth");
                    ProfileSetupPresenter profileSetupPresenter2 = this.B;
                    if (profileSetupPresenter2 != null) {
                        Parcelable parcelableExtra2 = intent.getParcelableExtra("EMAIL_AUTH");
                        wg6.a((Object) parcelableExtra2, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                        profileSetupPresenter2.a((SignUpEmailAuth) parcelableExtra2);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean booleanExtra = intent.getBooleanExtra("IS_UPDATE_PROFILE_PROCESS", false);
                    ProfileSetupPresenter profileSetupPresenter3 = this.B;
                    if (profileSetupPresenter3 != null) {
                        profileSetupPresenter3.f(booleanExtra);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            }
            if (bundle != null) {
                if (bundle.containsKey("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from savedInstanceState socialAuth");
                    SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) bundle.getParcelable("SOCIAL_AUTH");
                    if (signUpSocialAuth != null) {
                        ProfileSetupPresenter profileSetupPresenter4 = this.B;
                        if (profileSetupPresenter4 != null) {
                            profileSetupPresenter4.a(signUpSocialAuth);
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from savedInstanceState emailAuth");
                    SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) bundle.getParcelable("EMAIL_AUTH");
                    if (signUpEmailAuth != null) {
                        ProfileSetupPresenter profileSetupPresenter5 = this.B;
                        if (profileSetupPresenter5 != null) {
                            profileSetupPresenter5.a(signUpEmailAuth);
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean z = bundle.getBoolean("IS_UPDATE_PROFILE_PROCESS");
                    ProfileSetupPresenter profileSetupPresenter6 = this.B;
                    if (profileSetupPresenter6 != null) {
                        profileSetupPresenter6.f(z);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        ProfileSetupPresenter profileSetupPresenter = this.B;
        if (profileSetupPresenter != null) {
            SignUpEmailAuth l = profileSetupPresenter.l();
            if (l != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f = f();
                local.d(f, "onSaveInstanceState put emailAuth " + l);
                bundle.putParcelable("EMAIL_AUTH", l);
            }
            ProfileSetupPresenter profileSetupPresenter2 = this.B;
            if (profileSetupPresenter2 != null) {
                SignUpSocialAuth s = profileSetupPresenter2.s();
                if (s != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String f2 = f();
                    local2.d(f2, "onSaveInstanceState put socialAuth " + s);
                    bundle.putParcelable("SOCIAL_AUTH", s);
                }
                ProfileSetupPresenter profileSetupPresenter3 = this.B;
                if (profileSetupPresenter3 != null) {
                    bundle.putBoolean("IS_UPDATE_PROFILE_PROCESS", profileSetupPresenter3.z());
                    ProfileSetupActivity.super.onSaveInstanceState(bundle);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("mPresenter");
        throw null;
    }
}
