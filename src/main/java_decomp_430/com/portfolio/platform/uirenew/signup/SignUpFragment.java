package com.portfolio.platform.uirenew.signup;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.bx5;
import com.fossil.cd6;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.pt5;
import com.fossil.qg6;
import com.fossil.qt5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.xd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpFragment extends BaseFragment implements qt5 {
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public pt5 f;
    @DexIgnore
    public ax5<xd4> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SignUpFragment a() {
            return new SignUpFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ xd4 a;

            @DexIgnore
            public a(xd4 xd4) {
                this.a = xd4;
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
            /* JADX WARNING: type inference failed for: r0v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
            public final void run() {
                Object r0 = this.a.G;
                wg6.a((Object) r0, "it.tvHaveAccount");
                r0.setVisibility(0);
                Object r02 = this.a.H;
                wg6.a((Object) r02, "it.tvLogin");
                r02.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, SignUpFragment signUpFragment) {
            this.a = view;
            this.b = signUpFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v20, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                xd4 xd4 = (xd4) SignUpFragment.a(this.b).a();
                if (xd4 != null) {
                    try {
                        Object r1 = xd4.G;
                        wg6.a((Object) r1, "it.tvHaveAccount");
                        r1.setVisibility(8);
                        Object r0 = xd4.H;
                        wg6.a((Object) r0, "it.tvLogin");
                        r0.setVisibility(8);
                        cd6 cd6 = cd6.a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SignUpFragment", "onCreateView - e=" + e);
                        cd6 cd62 = cd6.a;
                    }
                }
            } else {
                xd4 xd42 = (xd4) SignUpFragment.a(this.b).a();
                if (xd42 != null) {
                    try {
                        wg6.a((Object) xd42, "it");
                        Boolean.valueOf(xd42.d().postDelayed(new a(xd42), 100));
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SignUpFragment", "onCreateView - e=" + e2);
                        cd6 cd63 = cd6.a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public c(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public d(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public e(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            LoginActivity.a aVar = LoginActivity.G;
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xd4 a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public f(xd4 xd4, SignUpFragment signUpFragment) {
            this.a = xd4;
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.a.u;
            wg6.a((Object) flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.a.t;
            wg6.a((Object) flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.b.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public g(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            SignUpFragment.b(this.a).a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public h(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            SignUpFragment.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public i(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            SignUpFragment.b(this.a).b(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public j(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d("SignUpFragment", "Password DONE key, trigger sign up flow");
            this.a.j1();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public k(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public l(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public m(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public n(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ xd4 a;

        @DexIgnore
        public o(xd4 xd4) {
            this.a = xd4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                Object r3 = this.a.E;
                wg6.a((Object) r3, "binding.tvErrorCheckCharacter");
                r3.setVisibility(0);
                Object r32 = this.a.F;
                wg6.a((Object) r32, "binding.tvErrorCheckCombine");
                r32.setVisibility(0);
                return;
            }
            Object r33 = this.a.E;
            wg6.a((Object) r33, "binding.tvErrorCheckCharacter");
            r33.setVisibility(8);
            Object r34 = this.a.F;
            wg6.a((Object) r34, "binding.tvErrorCheckCombine");
            r34.setVisibility(8);
        }
    }

    @DexIgnore
    public static final /* synthetic */ ax5 a(SignUpFragment signUpFragment) {
        ax5<xd4> ax5 = signUpFragment.g;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ pt5 b(SignUpFragment signUpFragment) {
        pt5 pt5 = signUpFragment.f;
        if (pt5 != null) {
            return pt5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void C(boolean z) {
        if (isActive()) {
            ax5<xd4> ax5 = this.g;
            if (ax5 != null) {
                xd4 a2 = ax5.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.B;
                    wg6.a((Object) constraintLayout, "it.ivWeibo");
                    constraintLayout.setVisibility(0);
                    ConstraintLayout constraintLayout2 = a2.A;
                    wg6.a((Object) constraintLayout2, "it.ivWechat");
                    constraintLayout2.setVisibility(0);
                    ImageView imageView = a2.z;
                    wg6.a((Object) imageView, "it.ivGoogle");
                    imageView.setVisibility(8);
                    ImageView imageView2 = a2.y;
                    wg6.a((Object) imageView2, "it.ivFacebook");
                    imageView2.setVisibility(8);
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.B;
                wg6.a((Object) constraintLayout3, "it.ivWeibo");
                constraintLayout3.setVisibility(8);
                ConstraintLayout constraintLayout4 = a2.A;
                wg6.a((Object) constraintLayout4, "it.ivWechat");
                constraintLayout4.setVisibility(8);
                ImageView imageView3 = a2.z;
                wg6.a((Object) imageView3, "it.ivGoogle");
                imageView3.setVisibility(0);
                ImageView imageView4 = a2.y;
                wg6.a((Object) imageView4, "it.ivFacebook");
                imageView4.setVisibility(0);
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void H() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = bx5.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.F;
            wg6.a((Object) activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void K0() {
        if (isActive()) {
            ax5<xd4> ax5 = this.g;
            if (ax5 != null) {
                xd4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.q;
                    wg6.a((Object) r1, "it.btContinue");
                    r1.setEnabled(false);
                    Object r12 = a2.q;
                    wg6.a((Object) r12, "it.btContinue");
                    r12.setClickable(false);
                    Object r13 = a2.q;
                    wg6.a((Object) r13, "it.btContinue");
                    r13.setFocusable(false);
                    a2.q.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.view.View, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void P(String str) {
        wg6.b(str, "errorMessage");
        ax5<xd4> ax5 = this.g;
        if (ax5 != null) {
            xd4 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.w;
                wg6.a((Object) r1, "it.ivCheckedEmail");
                if (r1.getVisibility() == 0) {
                    Object r12 = a2.w;
                    wg6.a((Object) r12, "it.ivCheckedEmail");
                    r12.setVisibility(8);
                }
                FlexibleTextInputLayout flexibleTextInputLayout = a2.t;
                wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.t;
                wg6.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void d(boolean z, boolean z2) {
        if (isActive()) {
            ax5<xd4> ax5 = this.g;
            if (ax5 != null) {
                xd4 a2 = ax5.a();
                if (a2 != null) {
                    if (z) {
                        a2.E.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231133), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.E.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231005), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                    if (z2) {
                        a2.F.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231133), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.F.setCompoundDrawablesWithIntrinsicBounds(w6.c(PortfolioApp.get.instance(), 2131231005), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f(int i2, String str) {
        wg6.b(str, "errorMessage");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    public void g() {
        ax5<xd4> ax5 = this.g;
        if (ax5 != null) {
            xd4 a2 = ax5.a();
            if (a2 != null) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(a2.C, "progress", new int[]{0, 10});
                wg6.a((Object) ofInt, "progressAnimator");
                ofInt.setDuration(500);
                ofInt.start();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void h() {
        if (getActivity() != null) {
            HomeActivity.a aVar = HomeActivity.C;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public String h1() {
        return "SignUpFragment";
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public final void j1() {
        ax5<xd4> ax5 = this.g;
        if (ax5 != null) {
            xd4 a2 = ax5.a();
            if (a2 != null) {
                Object r2 = a2.q;
                wg6.a((Object) r2, "this.btContinue");
                if (r2.isEnabled()) {
                    pt5 pt5 = this.f;
                    if (pt5 != null) {
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.r;
                        wg6.a((Object) flexibleTextInputEditText, "etEmail");
                        String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.s;
                        wg6.a((Object) flexibleTextInputEditText2, "etPassword");
                        pt5.a(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                        return;
                    }
                    wg6.d("mPresenter");
                    throw null;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void k() {
        String string = getString(2131886759);
        wg6.a((Object) string, "getString(R.string.Onboa\u2026Account_Text__PleaseWait)");
        X(string);
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e("SignUpFragment", "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SignUpFragment", "Apple Auth Info = " + signUpSocialAuth);
            pt5 pt5 = this.f;
            if (pt5 != null) {
                wg6.a((Object) signUpSocialAuth, "authCode");
                pt5.a(signUpSocialAuth);
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        SignUpFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        xd4 a2 = kb.a(layoutInflater, 2131558609, viewGroup, false, e1());
        wg6.a((Object) a2, "bindingLocal");
        View d2 = a2.d();
        wg6.a((Object) d2, "bindingLocal.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        this.g = new ax5<>(this, a2);
        ax5<xd4> ax5 = this.g;
        if (ax5 != null) {
            xd4 a3 = ax5.a();
            if (a3 != null) {
                wg6.a((Object) a3, "mBinding.get()!!");
                return a3.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        SignUpFragment.super.onPause();
        pt5 pt5 = this.f;
        if (pt5 != null) {
            pt5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        SignUpFragment.super.onResume();
        pt5 pt5 = this.f;
        if (pt5 != null) {
            pt5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v5, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r2v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<xd4> ax5 = this.g;
        if (ax5 != null) {
            xd4 a2 = ax5.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new f(a2, this));
                a2.r.addTextChangedListener(new g(this));
                a2.r.setOnFocusChangeListener(new h(this));
                a2.s.addTextChangedListener(new i(this));
                a2.s.setOnFocusChangeListener(new o(a2));
                a2.s.setOnEditorActionListener(new j(this));
                a2.x.setOnClickListener(new k(this));
                a2.y.setOnClickListener(new l(this));
                a2.z.setOnClickListener(new m(this));
                a2.v.setOnClickListener(new n(this));
                a2.A.setOnClickListener(new c(this));
                a2.B.setOnClickListener(new d(this));
                a2.H.setOnClickListener(new e(this));
            }
            W("sign_up_view");
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void r0() {
        if (isActive()) {
            ax5<xd4> ax5 = this.g;
            if (ax5 != null) {
                xd4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.q;
                    wg6.a((Object) r1, "it.btContinue");
                    r1.setEnabled(true);
                    Object r12 = a2.q;
                    wg6.a((Object) r12, "it.btContinue");
                    r12.setClickable(true);
                    Object r13 = a2.q;
                    wg6.a((Object) r13, "it.btContinue");
                    r13.setFocusable(true);
                    a2.q.a("flexible_button_primary");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2, String str) {
        wg6.b(str, "errorMessage");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(pt5 pt5) {
        wg6.b(pt5, "presenter");
        this.f = pt5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v4, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void a(boolean z, boolean z2, String str) {
        Object r7;
        wg6.b(str, "errorMessage");
        if (isActive()) {
            ax5<xd4> ax5 = this.g;
            if (ax5 != null) {
                xd4 a2 = ax5.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.t;
                    wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.t;
                    wg6.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                }
                ax5<xd4> ax52 = this.g;
                if (ax52 != null) {
                    xd4 a3 = ax52.a();
                    if (a3 != null && (r7 = a3.w) != 0) {
                        wg6.a((Object) r7, "it");
                        r7.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(SignUpEmailAuth signUpEmailAuth) {
        FragmentActivity activity;
        wg6.b(signUpEmailAuth, "emailAuth");
        if (isActive() && (activity = getActivity()) != null) {
            EmailOtpVerificationActivity.a aVar = EmailOtpVerificationActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity, signUpEmailAuth);
        }
    }

    @DexIgnore
    public void b(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a((Context) activity, signUpSocialAuth);
            activity.finish();
        }
    }
}
