package com.portfolio.platform.uirenew.signup.verification;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.n94;
import com.fossil.nk4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.yt5;
import com.fossil.zt5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EmailOtpVerificationFragment extends BaseFragment implements zt5, View.OnClickListener, TextWatcher, View.OnKeyListener, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a u; // = new a((qg6) null);
    @DexIgnore
    public yt5 f;
    @DexIgnore
    public ax5<n94> g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public boolean j; // = true;
    @DexIgnore
    public ScaleAnimation o; // = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation p; // = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation q; // = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation r; // = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public TranslateAnimation s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final EmailOtpVerificationFragment a() {
            return new EmailOtpVerificationFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ EmailOtpVerificationFragment a;

        @DexIgnore
        public b(EmailOtpVerificationFragment emailOtpVerificationFragment) {
            this.a = emailOtpVerificationFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onAnimationEnd(Animation animation) {
            n94 k1 = this.a.k1();
            if (k1 != null) {
                Object r0 = k1.F;
                wg6.a((Object) r0, "it.tvTitle");
                r0.setVisibility(8);
                ViewPropertyAnimator translationY = k1.E.animate().translationY(-this.a.p1());
                wg6.a((Object) translationY, "it.scVerifyContent.anima\u2026slationY(-mtvTitleHeight)");
                translationY.setDuration(125);
                ViewPropertyAnimator translationY2 = k1.y.animate().translationY(-this.a.o1());
                wg6.a((Object) translationY2, "it.ftvEmailNotification.\u2026slationY(-mivEmailHeight)");
                translationY2.setDuration(125);
                ViewPropertyAnimator translationY3 = k1.x.animate().translationY(-this.a.o1());
                wg6.a((Object) translationY3, "it.ftvEmail.animate()\n  \u2026slationY(-mivEmailHeight)");
                translationY3.setDuration(125);
                ViewPropertyAnimator translationY4 = k1.r.animate().translationY(-this.a.o1());
                wg6.a((Object) translationY4, "it.clVerificationCode.an\u2026slationY(-mivEmailHeight)");
                translationY4.setDuration(125);
                ViewPropertyAnimator translationY5 = k1.z.animate().translationY(-this.a.o1());
                wg6.a((Object) translationY5, "it.ftvEnterCodesGuide.an\u2026slationY(-mivEmailHeight)");
                translationY5.setDuration(125);
                ViewPropertyAnimator translationY6 = k1.A.animate().translationY(-this.a.o1());
                wg6.a((Object) translationY6, "it.ftvInvalidCode.animat\u2026slationY(-mivEmailHeight)");
                translationY6.setDuration(125);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onAnimationStart(Animation animation) {
            n94 k1 = this.a.k1();
            if (k1 != null) {
                Object r0 = k1.F;
                wg6.a((Object) r0, "it.tvTitle");
                r0.setVisibility(0);
                Object r02 = k1.C;
                wg6.a((Object) r02, "it.ivEmail");
                r02.setVisibility(0);
                this.a.m1().setDuration(250);
                k1.F.startAnimation(this.a.m1());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ EmailOtpVerificationFragment b;

        @DexIgnore
        public c(View view, EmailOtpVerificationFragment emailOtpVerificationFragment) {
            this.a = view;
            this.b = emailOtpVerificationFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r2v4, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v11, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                n94 k1 = this.b.k1();
                if (k1 != null && !this.b.j) {
                    k1.C.startAnimation(this.b.q);
                    this.b.j = true;
                    return;
                }
                return;
            }
            n94 k12 = this.b.k1();
            if (k12 != null && this.b.j) {
                EmailOtpVerificationFragment emailOtpVerificationFragment = this.b;
                Object r2 = k12.F;
                wg6.a((Object) r2, "it.tvTitle");
                emailOtpVerificationFragment.b((float) r2.getHeight());
                EmailOtpVerificationFragment emailOtpVerificationFragment2 = this.b;
                Object r22 = k12.C;
                wg6.a((Object) r22, "it.ivEmail");
                emailOtpVerificationFragment2.a((float) r22.getHeight());
                if (this.b.s == null) {
                    EmailOtpVerificationFragment emailOtpVerificationFragment3 = this.b;
                    Object r4 = k12.F;
                    wg6.a((Object) r4, "it.tvTitle");
                    float x = r4.getX();
                    Object r42 = k12.F;
                    wg6.a((Object) r42, "it.tvTitle");
                    emailOtpVerificationFragment3.s = new TranslateAnimation(1, x, 1, r42.getX(), 1, 0.0f, 1, this.b.p1());
                    this.b.q1();
                }
                k12.E.startAnimation(this.b.s);
                this.b.j = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ n94 a;

        @DexIgnore
        public d(n94 n94) {
            this.a = n94;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.s.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ n94 a;

        @DexIgnore
        public e(n94 n94) {
            this.a = n94;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.u.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ n94 a;

        @DexIgnore
        public f(n94 n94) {
            this.a = n94;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.v.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ n94 a;

        @DexIgnore
        public g(n94 n94) {
            this.a = n94;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.t.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ EmailOtpVerificationFragment a;

        @DexIgnore
        public h(EmailOtpVerificationFragment emailOtpVerificationFragment) {
            this.a = emailOtpVerificationFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r0v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onAnimationEnd(Animation animation) {
            n94 k1 = this.a.k1();
            if (k1 != null) {
                Object r0 = k1.F;
                wg6.a((Object) r0, "it.tvTitle");
                r0.setVisibility(0);
                Object r02 = k1.C;
                wg6.a((Object) r02, "it.ivEmail");
                r02.setVisibility(0);
                k1.C.startAnimation(this.a.l1());
                this.a.m1().setDuration(500);
                k1.F.startAnimation(this.a.n1());
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onAnimationStart(Animation animation) {
            n94 k1 = this.a.k1();
            if (k1 != null) {
                Object r0 = k1.F;
                wg6.a((Object) r0, "it.tvTitle");
                r0.setVisibility(4);
                Object r02 = k1.C;
                wg6.a((Object) r02, "it.ivEmail");
                r02.setVisibility(4);
                ViewPropertyAnimator translationY = k1.E.animate().translationY(0.0f);
                wg6.a((Object) translationY, "it.scVerifyContent.anima\u2026        .translationY(0f)");
                translationY.setDuration(500);
                ViewPropertyAnimator translationY2 = k1.y.animate().translationY(0.0f);
                wg6.a((Object) translationY2, "it.ftvEmailNotification.\u2026        .translationY(0f)");
                translationY2.setDuration(500);
                ViewPropertyAnimator translationY3 = k1.x.animate().translationY(0.0f);
                wg6.a((Object) translationY3, "it.ftvEmail.animate()\n  \u2026        .translationY(0f)");
                translationY3.setDuration(500);
                ViewPropertyAnimator translationY4 = k1.r.animate().translationY(0.0f);
                wg6.a((Object) translationY4, "it.clVerificationCode.an\u2026        .translationY(0f)");
                translationY4.setDuration(500);
                ViewPropertyAnimator translationY5 = k1.z.animate().translationY(0.0f);
                wg6.a((Object) translationY5, "it.ftvEnterCodesGuide.an\u2026        .translationY(0f)");
                translationY5.setDuration(500);
                ViewPropertyAnimator translationY6 = k1.A.animate().translationY(0.0f);
                wg6.a((Object) translationY6, "it.ftvInvalidCode.animat\u2026        .translationY(0f)");
                translationY6.setDuration(500);
            }
        }
    }

    @DexIgnore
    public EmailOtpVerificationFragment() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void L(boolean z) {
        n94 k1;
        Object r0;
        if (isActive() && (k1 = k1()) != null && (r0 = k1.A) != 0) {
            if (z) {
                wg6.a((Object) r0, "it");
                r0.setVisibility(0);
                return;
            }
            wg6.a((Object) r0, "it");
            if (r0.getVisibility() != 4) {
                r0.setVisibility(4);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void O0() {
        RTLImageView rTLImageView;
        Object r0;
        if (isActive()) {
            n94 k1 = k1();
            if (!(k1 == null || (r0 = k1.F) == 0)) {
                r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886765));
            }
            n94 k12 = k1();
            if (k12 != null && (rTLImageView = k12.C) != null) {
                rTLImageView.setImageDrawable(PortfolioApp.get.instance().getDrawable(2131231362));
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v5, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r12v9, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r12v14, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v17, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r12v22, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v25, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r12v30, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v18, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void afterTextChanged(Editable editable) {
        FragmentActivity activity = getActivity();
        View currentFocus = activity != null ? activity.getCurrentFocus() : null;
        n94 k1 = k1();
        if (k1 != null) {
            Integer valueOf = currentFocus != null ? Integer.valueOf(currentFocus.getId()) : null;
            boolean z = false;
            if (valueOf != null && valueOf.intValue() == 2131362186) {
                if (currentFocus != null) {
                    EditText editText = (EditText) currentFocus;
                    Editable text = editText.getText();
                    int length = text.length();
                    if (length == 0) {
                        Object r12 = k1.q;
                        wg6.a((Object) r12, "it.btContinue");
                        r12.setEnabled(false);
                        k1.q.a("flexible_button_disabled");
                    } else if (length == 1) {
                        editText.setSelection(1);
                        FlexibleEditText flexibleEditText = k1.u;
                        wg6.a((Object) flexibleEditText, "it.etSecondCode");
                        Editable text2 = flexibleEditText.getText();
                        if (text2 != null) {
                            wg6.a((Object) text2, "it.etSecondCode.text!!");
                            if (text2.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                k1.u.requestFocus();
                            }
                            j1();
                            return;
                        }
                        wg6.a();
                        throw null;
                    } else if (length == 2) {
                        char charAt = text.charAt(0);
                        char charAt2 = text.charAt(1);
                        FlexibleEditText flexibleEditText2 = k1.u;
                        wg6.a((Object) flexibleEditText2, "it.etSecondCode");
                        Editable text3 = flexibleEditText2.getText();
                        if (text3 != null) {
                            wg6.a((Object) text3, "it.etSecondCode.text!!");
                            if (text3.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                k1.s.setTextKeepState(String.valueOf(charAt));
                                k1.s.clearFocus();
                                k1.u.requestFocus();
                                k1.u.setText(String.valueOf(charAt2));
                            } else {
                                k1.s.setTextKeepState(String.valueOf(charAt2));
                            }
                            editText.setSelection(1);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf != null && valueOf.intValue() == 2131362197) {
                if (currentFocus != null) {
                    EditText editText2 = (EditText) currentFocus;
                    Editable text4 = editText2.getText();
                    int length2 = text4.length();
                    if (length2 == 0) {
                        Object r122 = k1.q;
                        wg6.a((Object) r122, "it.btContinue");
                        r122.setEnabled(false);
                        k1.q.a("flexible_button_disabled");
                    } else if (length2 == 1) {
                        editText2.setSelection(1);
                        FlexibleEditText flexibleEditText3 = k1.v;
                        wg6.a((Object) flexibleEditText3, "it.etThirdCode");
                        Editable text5 = flexibleEditText3.getText();
                        if (text5 != null) {
                            wg6.a((Object) text5, "it.etThirdCode.text!!");
                            if (text5.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                k1.v.requestFocus();
                            }
                            j1();
                            return;
                        }
                        wg6.a();
                        throw null;
                    } else if (length2 == 2) {
                        char charAt3 = text4.charAt(0);
                        char charAt4 = text4.charAt(1);
                        FlexibleEditText flexibleEditText4 = k1.v;
                        wg6.a((Object) flexibleEditText4, "it.etThirdCode");
                        Editable text6 = flexibleEditText4.getText();
                        if (text6 != null) {
                            wg6.a((Object) text6, "it.etThirdCode.text!!");
                            if (text6.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                editText2.setTextKeepState(String.valueOf(charAt3));
                                editText2.clearFocus();
                                k1.v.requestFocus();
                                k1.v.setText(String.valueOf(charAt4));
                            } else {
                                editText2.setTextKeepState(String.valueOf(charAt4));
                            }
                            editText2.setSelection(1);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf != null && valueOf.intValue() == 2131362198) {
                if (currentFocus != null) {
                    EditText editText3 = (EditText) currentFocus;
                    Editable text7 = editText3.getText();
                    int length3 = text7.length();
                    if (length3 == 0) {
                        Object r123 = k1.q;
                        wg6.a((Object) r123, "it.btContinue");
                        r123.setEnabled(false);
                        k1.q.a("flexible_button_disabled");
                    } else if (length3 == 1) {
                        editText3.setSelection(1);
                        FlexibleEditText flexibleEditText5 = k1.t;
                        wg6.a((Object) flexibleEditText5, "it.etFourthCode");
                        Editable text8 = flexibleEditText5.getText();
                        if (text8 != null) {
                            wg6.a((Object) text8, "it.etFourthCode.text!!");
                            if (text8.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                k1.t.requestFocus();
                            }
                            j1();
                            return;
                        }
                        wg6.a();
                        throw null;
                    } else if (length3 == 2) {
                        char charAt5 = text7.charAt(0);
                        char charAt6 = text7.charAt(1);
                        FlexibleEditText flexibleEditText6 = k1.t;
                        wg6.a((Object) flexibleEditText6, "it.etFourthCode");
                        Editable text9 = flexibleEditText6.getText();
                        if (text9 != null) {
                            wg6.a((Object) text9, "it.etFourthCode.text!!");
                            if (text9.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                editText3.setText(String.valueOf(charAt5));
                                editText3.clearFocus();
                                k1.t.requestFocus();
                                k1.t.setText(String.valueOf(charAt6));
                            } else {
                                editText3.setText(String.valueOf(charAt6));
                            }
                            editText3.setSelection(1);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf == null || valueOf.intValue() != 2131362188) {
            } else {
                if (currentFocus != null) {
                    EditText editText4 = (EditText) currentFocus;
                    int length4 = editText4.getText().length();
                    if (length4 == 0) {
                        Object r124 = k1.q;
                        wg6.a((Object) r124, "it.btContinue");
                        r124.setEnabled(false);
                        k1.q.a("flexible_button_disabled");
                    } else if (length4 == 1) {
                        editText4.setSelection(1);
                        j1();
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type android.widget.EditText");
                }
            }
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void i() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public final void j1() {
        n94 k1;
        if (isActive() && (k1 = k1()) != null) {
            FlexibleEditText flexibleEditText = k1.s;
            wg6.a((Object) flexibleEditText, "it.etFirstCode");
            FlexibleEditText flexibleEditText2 = k1.u;
            wg6.a((Object) flexibleEditText2, "it.etSecondCode");
            FlexibleEditText flexibleEditText3 = k1.v;
            wg6.a((Object) flexibleEditText3, "it.etThirdCode");
            FlexibleEditText flexibleEditText4 = k1.t;
            wg6.a((Object) flexibleEditText4, "it.etFourthCode");
            String[] strArr = {String.valueOf(flexibleEditText.getText()), String.valueOf(flexibleEditText2.getText()), String.valueOf(flexibleEditText3.getText()), String.valueOf(flexibleEditText4.getText())};
            yt5 yt5 = this.f;
            if (yt5 != null) {
                yt5.a(strArr);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void k() {
        if (isActive()) {
            b();
        }
    }

    @DexIgnore
    public final n94 k1() {
        ax5<n94> ax5 = this.g;
        if (ax5 != null) {
            return ax5.a();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final ScaleAnimation l1() {
        return this.r;
    }

    @DexIgnore
    public final ScaleAnimation m1() {
        return this.o;
    }

    @DexIgnore
    public final ScaleAnimation n1() {
        return this.p;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: com.portfolio.platform.view.RTLImageView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: android.view.View} */
    /* JADX WARNING: Multi-variable type inference failed */
    public void o0() {
        FragmentActivity activity;
        if (!isActive() || (activity = getActivity()) == null) {
            return;
        }
        if (this.j) {
            nk4 nk4 = nk4.a;
            n94 k1 = k1();
            View view = k1 != null ? k1.B : null;
            if (view != null) {
                wg6.a((Object) activity, "it");
                nk4.a(view, activity);
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.view.View");
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final float o1() {
        return this.i;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131361934) {
                yt5 yt5 = this.f;
                if (yt5 != null) {
                    yt5.k();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362234) {
                yt5 yt52 = this.f;
                if (yt52 != null) {
                    yt52.i();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362542) {
                yt5 yt53 = this.f;
                if (yt53 != null) {
                    yt53.h();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.o.setDuration(500);
        this.o.setFillAfter(true);
        this.p.setDuration(500);
        this.p.setFillAfter(true);
        this.q.setDuration(500);
        this.q.setFillAfter(true);
        this.r.setDuration(500);
        this.r.setFillAfter(true);
        this.q.setAnimationListener(new b(this));
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        EmailOtpVerificationFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        n94 a2 = kb.a(layoutInflater, 2131558547, viewGroup, false, e1());
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, this));
        this.g = new ax5<>(this, a2);
        n94 k1 = k1();
        if (k1 != null) {
            return k1.d();
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        n94 k1;
        wg6.b(view, "view");
        wg6.b(keyEvent, "keyEvent");
        if (i2 == 67 && keyEvent.getAction() == 0 && (k1 = k1()) != null) {
            boolean z = true;
            if (view.getId() == 2131362197) {
                FlexibleEditText flexibleEditText = k1.u;
                wg6.a((Object) flexibleEditText, "it.etSecondCode");
                Editable text = flexibleEditText.getText();
                if (text != null) {
                    wg6.a((Object) text, "it.etSecondCode.text!!");
                    if (text.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        k1.s.requestFocus();
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (view.getId() == 2131362198) {
                FlexibleEditText flexibleEditText2 = k1.v;
                wg6.a((Object) flexibleEditText2, "it.etThirdCode");
                Editable text2 = flexibleEditText2.getText();
                if (text2 != null) {
                    wg6.a((Object) text2, "it.etThirdCode.text!!");
                    if (text2.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        k1.u.requestFocus();
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (view.getId() == 2131362188) {
                FlexibleEditText flexibleEditText3 = k1.t;
                wg6.a((Object) flexibleEditText3, "it.etFourthCode");
                Editable text3 = flexibleEditText3.getText();
                if (text3 != null) {
                    wg6.a((Object) text3, "it.etFourthCode.text!!");
                    if (text3.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        k1.v.requestFocus();
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        return super.onKey(view, i2, keyEvent);
    }

    @DexIgnore
    public void onPause() {
        FragmentActivity activity;
        Window window;
        EmailOtpVerificationFragment.super.onPause();
        if (!(this.j || (activity = getActivity()) == null || (window = activity.getWindow()) == null)) {
            window.setSoftInputMode(3);
        }
        yt5 yt5 = this.f;
        if (yt5 != null) {
            yt5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        EmailOtpVerificationFragment.super.onResume();
        yt5 yt5 = this.f;
        if (yt5 != null) {
            yt5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v8, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v10, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v11, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v12, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v13, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v15, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        n94 k1 = k1();
        if (k1 != null) {
            k1.B.setOnClickListener(this);
            k1.w.setOnClickListener(this);
            k1.q.setOnClickListener(this);
            k1.s.addTextChangedListener(this);
            k1.u.addTextChangedListener(this);
            k1.v.addTextChangedListener(this);
            k1.t.addTextChangedListener(this);
            k1.s.setOnKeyListener(this);
            k1.u.setOnKeyListener(this);
            k1.v.setOnKeyListener(this);
            k1.t.setOnKeyListener(this);
            k1.s.setOnFocusChangeListener(new d(k1));
            k1.u.setOnFocusChangeListener(new e(k1));
            k1.v.setOnFocusChangeListener(new f(k1));
            k1.t.setOnFocusChangeListener(new g(k1));
        }
    }

    @DexIgnore
    public final float p1() {
        return this.h;
    }

    @DexIgnore
    public final void q1() {
        TranslateAnimation translateAnimation = this.s;
        if (translateAnimation != null) {
            translateAnimation.setAnimationListener(new h(this));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void w(String str) {
        n94 k1;
        wg6.b(str, "emailAddress");
        if (isActive() && (k1 = k1()) != null) {
            Object r0 = k1.x;
            wg6.a((Object) r0, "it.ftvEmail");
            r0.setText(str);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void y(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        Object r0;
        if (isActive()) {
            n94 k1 = k1();
            if (!(k1 == null || (r0 = k1.q) == 0)) {
                r0.setEnabled(z);
            }
            if (z) {
                n94 k12 = k1();
                if (k12 != null && (flexibleButton2 = k12.q) != null) {
                    flexibleButton2.a("flexible_button_primary");
                    return;
                }
                return;
            }
            n94 k13 = k1();
            if (k13 != null && (flexibleButton = k13.q) != null) {
                flexibleButton.a("flexible_button_disabled");
            }
        }
    }

    @DexIgnore
    public final void b(float f2) {
        this.h = f2;
    }

    @DexIgnore
    public void d(int i2, String str) {
        wg6.b(str, "errorMessage");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    public final void a(float f2) {
        this.i = f2;
    }

    @DexIgnore
    public void a(yt5 yt5) {
        wg6.b(yt5, "presenter");
        this.f = yt5;
    }

    @DexIgnore
    public void a(String str, int i2, int i3) {
        View d2;
        wg6.b(str, "emailAddress");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, i2, i3, str);
            n94 k1 = k1();
            if (k1 != null && (d2 = k1.d()) != null) {
                d2.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if (str.hashCode() == 766014770 && str.equals("EMAIL_OTP_VERIFICATION") && i2 == 2131361935) {
            yt5 yt5 = this.f;
            if (yt5 != null) {
                yt5.j();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(SignUpEmailAuth signUpEmailAuth) {
        wg6.b(signUpEmailAuth, "emailAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a((Context) activity, signUpEmailAuth);
        }
    }
}
