package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.hn4;
import com.fossil.in4;
import com.fossil.kn4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rt5;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a G; // = new a((qg6) null);
    @DexIgnore
    public SignUpPresenter B;
    @DexIgnore
    public hn4 C;
    @DexIgnore
    public in4 D;
    @DexIgnore
    public kn4 E;
    @DexIgnore
    public MFLoginWechatManager F;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            context.startActivity(new Intent(context, SignUpActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        SignUpActivity.super.onActivityResult(i, i2, intent);
        if (intent != null) {
            hn4 hn4 = this.C;
            if (hn4 != null) {
                hn4.a(i, i2, intent);
                in4 in4 = this.D;
                if (in4 != null) {
                    in4.a(i, i2, intent);
                    kn4 kn4 = this.E;
                    if (kn4 != null) {
                        kn4.a(i, i2, intent);
                        Fragment b = getSupportFragmentManager().b(2131362119);
                        if (b != null) {
                            b.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    wg6.d("mLoginWeiboManager");
                    throw null;
                }
                wg6.d("mLoginGoogleManager");
                throw null;
            }
            wg6.d("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        SignUpFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = SignUpFragment.i.a();
            a((Fragment) b, f(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new rt5(this, b)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.SignUpContract.View");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.uirenew.signup.SignUpActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onNewIntent(Intent intent) {
        wg6.b(intent, "intent");
        SignUpActivity.super.onNewIntent(intent);
        MFLoginWechatManager mFLoginWechatManager = this.F;
        if (mFLoginWechatManager != null) {
            Intent intent2 = getIntent();
            wg6.a((Object) intent2, "getIntent()");
            mFLoginWechatManager.a(intent2);
            return;
        }
        wg6.d("mLoginWechatManager");
        throw null;
    }
}
