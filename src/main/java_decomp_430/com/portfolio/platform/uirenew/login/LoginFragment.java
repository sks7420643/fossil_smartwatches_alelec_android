package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.bx5;
import com.fossil.cd6;
import com.fossil.ip5;
import com.fossil.jm4;
import com.fossil.jp5;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.vd4;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginFragment extends BaseFragment implements jp5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public ip5 f;
    @DexIgnore
    public ax5<vd4> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LoginFragment.i;
        }

        @DexIgnore
        public final LoginFragment b() {
            return new LoginFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ vd4 a;

            @DexIgnore
            public a(vd4 vd4) {
                this.a = vd4;
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
            /* JADX WARNING: type inference failed for: r0v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
            public final void run() {
                Object r0 = this.a.D;
                wg6.a((Object) r0, "it.tvHaveAccount");
                r0.setVisibility(0);
                Object r02 = this.a.E;
                wg6.a((Object) r02, "it.tvSignup");
                r02.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, LoginFragment loginFragment) {
            this.a = view;
            this.b = loginFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v20, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                vd4 vd4 = (vd4) LoginFragment.a(this.b).a();
                if (vd4 != null) {
                    try {
                        Object r1 = vd4.D;
                        wg6.a((Object) r1, "it.tvHaveAccount");
                        r1.setVisibility(8);
                        Object r0 = vd4.E;
                        wg6.a((Object) r0, "it.tvSignup");
                        r0.setVisibility(8);
                        cd6 cd6 = cd6.a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = LoginFragment.j.a();
                        local.d(a2, "onCreateView - e=" + e);
                        cd6 cd62 = cd6.a;
                    }
                }
            } else {
                vd4 vd42 = (vd4) LoginFragment.a(this.b).a();
                if (vd42 != null) {
                    try {
                        wg6.a((Object) vd42, "it");
                        Boolean.valueOf(vd42.d().postDelayed(new a(vd42), 100));
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = LoginFragment.j.a();
                        local2.d(a3, "onCreateView - e=" + e2);
                        cd6 cd63 = cd6.a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public c(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public d(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                SignUpActivity.a aVar = SignUpActivity.G;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public e(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public f(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(LoginFragment.j.a(), "Password DONE key, trigger login flow");
            this.a.k1();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vd4 a;
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment b;

        @DexIgnore
        public g(vd4 vd4, LoginFragment loginFragment) {
            this.a = vd4;
            this.b = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.a.u;
            wg6.a((Object) flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.a.t;
            wg6.a((Object) flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.b.k1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public h(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.a.j1().a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public i(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.j1().a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public j(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.a.j1().b(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public k(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public l(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public m(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public n(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ LoginFragment a;

        @DexIgnore
        public o(LoginFragment loginFragment) {
            this.a = loginFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().l();
        }
    }

    /*
    static {
        String simpleName = LoginFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "LoginFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ax5 a(LoginFragment loginFragment) {
        ax5<vd4> ax5 = loginFragment.g;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void H() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = bx5.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.F;
            wg6.a((Object) activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    public void I(boolean z) {
        ax5<vd4> ax5 = this.g;
        if (ax5 != null) {
            vd4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ConstraintLayout constraintLayout = a2.A;
                wg6.a((Object) constraintLayout, "it.ivWeibo");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.z;
                wg6.a((Object) constraintLayout2, "it.ivWechat");
                constraintLayout2.setVisibility(0);
                ImageView imageView = a2.y;
                wg6.a((Object) imageView, "it.ivGoogle");
                imageView.setVisibility(8);
                ImageView imageView2 = a2.x;
                wg6.a((Object) imageView2, "it.ivFacebook");
                imageView2.setVisibility(8);
                return;
            }
            ConstraintLayout constraintLayout3 = a2.A;
            wg6.a((Object) constraintLayout3, "it.ivWeibo");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.z;
            wg6.a((Object) constraintLayout4, "it.ivWechat");
            constraintLayout4.setVisibility(8);
            ImageView imageView3 = a2.y;
            wg6.a((Object) imageView3, "it.ivGoogle");
            imageView3.setVisibility(0);
            ImageView imageView4 = a2.x;
            wg6.a((Object) imageView4, "it.ivFacebook");
            imageView4.setVisibility(0);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void L(String str) {
        wg6.b(str, "email");
        Context context = getContext();
        if (context != null) {
            ForgotPasswordActivity.a aVar = ForgotPasswordActivity.C;
            wg6.a((Object) context, "it");
            aVar.a(context, str);
        }
    }

    @DexIgnore
    public void c(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a((Context) activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void e0() {
        FlexibleButton flexibleButton;
        Object r0;
        ax5<vd4> ax5 = this.g;
        if (ax5 != null) {
            vd4 a2 = ax5.a();
            if (!(a2 == null || (r0 = a2.q) == 0)) {
                r0.setEnabled(false);
            }
            ax5<vd4> ax52 = this.g;
            if (ax52 != null) {
                vd4 a3 = ax52.a();
                if (a3 != null && (flexibleButton = a3.q) != null) {
                    flexibleButton.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wg6.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
            activity.finish();
        }
    }

    @DexIgnore
    public String h1() {
        return i;
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final ip5 j1() {
        ip5 ip5 = this.f;
        if (ip5 != null) {
            return ip5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void k() {
        String string = getString(2131886679);
        wg6.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        X(string);
    }

    @DexIgnore
    public final void k1() {
        ax5<vd4> ax5 = this.g;
        if (ax5 != null) {
            vd4 a2 = ax5.a();
            if (a2 != null) {
                ip5 ip5 = this.f;
                if (ip5 != null) {
                    FlexibleTextInputEditText flexibleTextInputEditText = a2.r;
                    wg6.a((Object) flexibleTextInputEditText, "etEmail");
                    String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                    FlexibleTextInputEditText flexibleTextInputEditText2 = a2.s;
                    wg6.a((Object) flexibleTextInputEditText2, "etPassword");
                    ip5.a(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e(i, "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "Apple Auth Info = " + signUpSocialAuth);
            ip5 ip5 = this.f;
            if (ip5 != null) {
                wg6.a((Object) signUpSocialAuth, "authCode");
                ip5.a(signUpSocialAuth);
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        LoginFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        vd4 a2 = kb.a(layoutInflater, 2131558608, viewGroup, false, e1());
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        this.g = new ax5<>(this, a2);
        ax5<vd4> ax5 = this.g;
        if (ax5 != null) {
            vd4 a3 = ax5.a();
            if (a3 != null) {
                wg6.a((Object) a3, "mBinding.get()!!");
                return a3.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        LoginFragment.super.onPause();
        ip5 ip5 = this.f;
        if (ip5 != null) {
            ip5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        LoginFragment.super.onResume();
        ip5 ip5 = this.f;
        if (ip5 != null) {
            ip5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r3v11, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v7, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<vd4> ax5 = this.g;
        if (ax5 != null) {
            vd4 a2 = ax5.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new g(a2, this));
                a2.r.addTextChangedListener(new h(this));
                a2.r.setOnFocusChangeListener(new i(this));
                a2.s.addTextChangedListener(new j(this));
                a2.w.setOnClickListener(new k(this));
                a2.x.setOnClickListener(new l(this));
                a2.y.setOnClickListener(new m(this));
                a2.v.setOnClickListener(new n(this));
                a2.z.setOnClickListener(new o(this));
                a2.A.setOnClickListener(new c(this));
                a2.E.setOnClickListener(new d(this));
                a2.C.setOnClickListener(new e(this));
                a2.s.setOnEditorActionListener(new f(this));
            }
            W("login_view");
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void p0() {
        if (isActive()) {
            ax5<vd4> ax5 = this.g;
            if (ax5 != null) {
                vd4 a2 = ax5.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.t;
                    wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setError(" ");
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.u;
                    wg6.a((Object) flexibleTextInputLayout2, "it.inputPassword");
                    flexibleTextInputLayout2.setError(jm4.a((Context) PortfolioApp.get.instance(), 2131886680));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void y0() {
        FlexibleButton flexibleButton;
        Object r0;
        ax5<vd4> ax5 = this.g;
        if (ax5 != null) {
            vd4 a2 = ax5.a();
            if (!(a2 == null || (r0 = a2.q) == 0)) {
                r0.setEnabled(true);
            }
            ax5<vd4> ax52 = this.g;
            if (ax52 != null) {
                vd4 a3 = ax52.a();
                if (a3 != null && (flexibleButton = a3.q) != null) {
                    flexibleButton.a("flexible_button_primary");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(int i2, String str) {
        wg6.b(str, "errorMessage");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(boolean z, String str) {
        FlexibleTextInputLayout flexibleTextInputLayout;
        wg6.b(str, "errorMessage");
        if (isActive()) {
            ax5<vd4> ax5 = this.g;
            if (ax5 != null) {
                vd4 a2 = ax5.a();
                if (a2 != null && (flexibleTextInputLayout = a2.t) != null) {
                    wg6.a((Object) flexibleTextInputLayout, "it");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    flexibleTextInputLayout.setError(str);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ip5 ip5) {
        wg6.b(ip5, "presenter");
        this.f = ip5;
    }
}
