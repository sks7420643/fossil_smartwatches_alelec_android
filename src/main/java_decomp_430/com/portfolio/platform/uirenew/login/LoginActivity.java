package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.hn4;
import com.fossil.in4;
import com.fossil.kn4;
import com.fossil.kp5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a G; // = new a((qg6) null);
    @DexIgnore
    public hn4 B;
    @DexIgnore
    public in4 C;
    @DexIgnore
    public kn4 D;
    @DexIgnore
    public MFLoginWechatManager E;
    @DexIgnore
    public LoginPresenter F;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            context.startActivity(new Intent(context, LoginActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        LoginActivity.super.onActivityResult(i, i2, intent);
        if (intent != null) {
            hn4 hn4 = this.B;
            if (hn4 != null) {
                hn4.a(i, i2, intent);
                in4 in4 = this.C;
                if (in4 != null) {
                    in4.a(i, i2, intent);
                    kn4 kn4 = this.D;
                    if (kn4 != null) {
                        kn4.a(i, i2, intent);
                        Fragment b = getSupportFragmentManager().b(2131362119);
                        if (b != null) {
                            b.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    wg6.d("mLoginWeiboManager");
                    throw null;
                }
                wg6.d("mLoginGoogleManager");
                throw null;
            }
            wg6.d("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        LoginFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = LoginFragment.j.b();
            a((Fragment) b, LoginFragment.j.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new kp5(this, b)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.login.LoginContract.View");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.app.Activity, com.portfolio.platform.uirenew.login.LoginActivity, androidx.fragment.app.FragmentActivity] */
    public void onNewIntent(Intent intent) {
        wg6.b(intent, "intent");
        LoginActivity.super.onNewIntent(intent);
        MFLoginWechatManager mFLoginWechatManager = this.E;
        if (mFLoginWechatManager != null) {
            Intent intent2 = getIntent();
            wg6.a((Object) intent2, "getIntent()");
            mFLoginWechatManager.a(intent2);
            return;
        }
        wg6.d("mLoginWechatManager");
        throw null;
    }
}
