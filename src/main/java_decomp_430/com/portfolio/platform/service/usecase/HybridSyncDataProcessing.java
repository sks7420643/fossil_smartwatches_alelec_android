package com.portfolio.platform.service.usecase;

import com.fossil.SyncDataExtensions;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jh6;
import com.fossil.jk4;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.oq4;
import com.fossil.oq4$d$a;
import com.fossil.oq4$e$a;
import com.fossil.pc6;
import com.fossil.rh6;
import com.fossil.rl6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridSyncDataProcessing {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ HybridSyncDataProcessing b; // = new HybridSyncDataProcessing();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<WrapperTapEventSummary> b;
        @DexIgnore
        public /* final */ List<ActivitySample> c;
        @DexIgnore
        public /* final */ List<GFitSample> d;
        @DexIgnore
        public /* final */ List<ActivitySummary> e;
        @DexIgnore
        public /* final */ List<MFSleepSession> f;

        @DexIgnore
        public a(long j, List<WrapperTapEventSummary> list, List<ActivitySample> list2, List<GFitSample> list3, List<ActivitySummary> list4, List<MFSleepSession> list5) {
            wg6.b(list, "tapEventSummaryList");
            wg6.b(list2, "sampleRawList");
            wg6.b(list3, "gFitSampleList");
            wg6.b(list4, "summaryList");
            wg6.b(list5, "sleepSessionList");
            this.a = j;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
        }

        @DexIgnore
        public final List<GFitSample> a() {
            return this.d;
        }

        @DexIgnore
        public final long b() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> c() {
            return this.c;
        }

        @DexIgnore
        public final List<MFSleepSession> d() {
            return this.f;
        }

        @DexIgnore
        public final List<ActivitySummary> e() {
            return this.e;
        }

        @DexIgnore
        public final List<WrapperTapEventSummary> f() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {322}, m = "processGoalTrackingData")
    public static final class b extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncDataProcessing this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HybridSyncDataProcessing hybridSyncDataProcessing, xe6 xe6) {
            super(xe6);
            this.this$0 = hybridSyncDataProcessing;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((List<? extends WrapperTapEventSummary>) null, (String) null, (HybridPresetRepository) null, (GoalTrackingRepository) null, (UserRepository) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {177}, m = "saveSyncResult")
    public static final class c extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncDataProcessing this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(HybridSyncDataProcessing hybridSyncDataProcessing, xe6 xe6) {
            super(xe6);
            this.this$0 = hybridSyncDataProcessing;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((oq4.a) null, (String) null, (SleepSessionsRepository) null, (SummariesRepository) null, (SleepSummariesRepository) null, (FitnessDataRepository) null, (ActivitiesRepository) null, (HybridPresetRepository) null, (GoalTrackingRepository) null, (ThirdPartyRepository) null, (UserRepository) null, (PortfolioApp) null, (ik4) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2", f = "HybridSyncDataProcessing.kt", l = {148}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super rm6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(a aVar, ThirdPartyRepository thirdPartyRepository, xe6 xe6) {
            super(2, xe6);
            this.$finalResult = aVar;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.$finalResult, this.$thirdPartyRepository, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                List<GFitSample> a2 = this.$finalResult.a();
                ArrayList arrayList = new ArrayList();
                for (ActivitySample next : this.$finalResult.c()) {
                    DateTime component3 = next.component3();
                    next.component4();
                    double component5 = next.component5();
                    arrayList.add(new UASample(rh6.a(component5), next.component7(), next.component6(), component3.getMillis() / ((long) 1000)));
                }
                List<MFSleepSession> d = this.$finalResult.d();
                dl6 b = zl6.b();
                oq4$d$a oq4_d_a = new oq4$d$a(this, a2, arrayList, d, (xe6) null);
                this.L$0 = il6;
                this.L$1 = a2;
                this.L$2 = arrayList;
                this.L$3 = d;
                this.label = 1;
                Object a3 = gk6.a(b, oq4_d_a, this);
                return a3 == a ? a : a3;
            } else if (i == 1) {
                List list = (List) this.L$3;
                List list2 = (List) this.L$2;
                List list3 = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3", f = "HybridSyncDataProcessing.kt", l = {188, 195}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDataRepository $fitnessDataRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, xe6 xe6) {
            super(2, xe6);
            this.$fitnessDataRepository = fitnessDataRepository;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.$fitnessDataRepository, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
                this.L$0 = il6;
                this.label = 1;
                obj = fitnessDataRepository.pushPendingFitnessData(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                jh6 jh6 = (jh6) this.L$4;
                jh6 jh62 = (jh6) this.L$3;
                List list = (List) this.L$2;
                ap4 ap4 = (ap4) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap42 = (ap4) obj;
            if (ap42 instanceof cp4) {
                List list2 = (List) ((cp4) ap42).a();
                if (list2 != null && (true ^ list2.isEmpty())) {
                    jh6 jh63 = new jh6();
                    jh63.element = ((FitnessDataWrapper) list2.get(0)).getStartTimeTZ();
                    jh6 jh64 = new jh6();
                    jh64.element = ((FitnessDataWrapper) list2.get(0)).getEndTimeTZ();
                    dl6 a2 = zl6.a();
                    oq4$e$a oq4_e_a = new oq4$e$a(this, list2, jh63, jh64, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = ap42;
                    this.L$2 = list2;
                    this.L$3 = jh63;
                    this.L$4 = jh64;
                    this.label = 2;
                    if (gk6.a(a2, oq4_e_a, this) == a) {
                        return a;
                    }
                }
            } else {
                boolean z = ap42 instanceof zo4;
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = HybridSyncDataProcessing.class.getSimpleName();
        wg6.a((Object) simpleName, "HybridSyncDataProcessing::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final oq4.a a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        String str2 = str;
        List<FitnessDataWrapper> list2 = list;
        long j3 = j;
        PortfolioApp portfolioApp2 = portfolioApp;
        wg6.b(str2, "serial");
        wg6.b(list2, "syncData");
        wg6.b(mFUser, "user");
        wg6.b(userProfile, "userProfile");
        wg6.b(portfolioApp2, "portfolioApp");
        FLogger.INSTANCE.getLocal().d(a, ".buildSyncResult(), get all data files, synctime=" + j3 + ", data=" + list2);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            portfolioApp2.a(CommunicateMode.SYNC, str2, "Calculating sleep and activity...");
            String userId = mFUser.getUserId();
            wg6.a((Object) userId, "user.userId");
            pc6<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a2 = SyncDataExtensions.a(list2, str2, userId, j3 * 1000);
            List<ActivitySample> first = a2.getFirst();
            List second = a2.getSecond();
            List third = a2.getThird();
            List<MFSleepSession> a3 = SyncDataExtensions.a(list2, str2);
            List<WrapperTapEventSummary> b2 = SyncDataExtensions.b(list);
            int size = a3.size();
            double d2 = 0.0d;
            double d3 = 0.0d;
            double d4 = 0.0d;
            double d5 = 0.0d;
            for (ActivitySample activitySample : first) {
                d4 += activitySample.getCalories();
                d5 += activitySample.getDistance();
                d2 += activitySample.getSteps();
                activitySample.getActiveTime();
                Boolean t = bk4.t(activitySample.getDate());
                wg6.a((Object) t, "DateHelper.isToday(it.date)");
                if (t.booleanValue()) {
                    d3 += activitySample.getSteps();
                    activitySample.getActiveTime();
                }
            }
            nh6 nh6 = nh6.a;
            Object[] objArr = {String.valueOf(size)};
            String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            a(str2, format);
            nh6 nh62 = nh6.a;
            Object[] objArr2 = {Double.valueOf(d2), Double.valueOf(d3), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d4), Double.valueOf(d5), c(b2)};
            String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s. Calories=%s. DistanceWrapper=%s. Taps=%s", Arrays.copyOf(objArr2, objArr2.length));
            wg6.a((Object) format2, "java.lang.String.format(format, *args)");
            a(str2, format2);
            FLogger.INSTANCE.getLocal().d(a, "Release=" + jk4.a());
            if (!jk4.a()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("onSyncCompleted - Sleep session details: ");
                sb.append(a3.isEmpty() ? b(a3) : ", no sleep data");
                local.d(str3, sb.toString());
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Minute data details: " + a(first));
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Tap event details: " + c(b2));
            }
            return new a(j2, b2, first, third, second, a3);
        }
        a(str2, "Sync data is empty");
        return aVar;
    }

    @DexIgnore
    public final String b(List<MFSleepSession> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (MFSleepSession next : list) {
            sb.append(next.toString());
            sb.append("\n");
            sb.append("State: ");
            String sleepStates = next.getSleepStates();
            int length = sleepStates.length();
            for (int i = 0; i < length; i++) {
                sb.append(String.valueOf(sleepStates.charAt(i)));
                sb.append(" -- ");
            }
            sb.append("\n");
        }
        sb.append("\n");
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final String c(List<? extends WrapperTapEventSummary> list) {
        if (list == null || list.isEmpty()) {
            return "null\n";
        }
        StringBuilder sb = new StringBuilder();
        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
            sb.append("[startTime:");
            sb.append(wrapperTapEventSummary.startTime);
            sb.append(", timezoneOffsetInSecond=");
            sb.append(wrapperTapEventSummary.timezoneOffsetInSecond);
            sb.append(", goalTrackingIds=");
            sb.append(wrapperTapEventSummary.goalId);
            sb.append("]");
            sb.append("\n");
        }
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0036  */
    public final Object a(oq4.a aVar, String str, SleepSessionsRepository sleepSessionsRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, HybridPresetRepository hybridPresetRepository, GoalTrackingRepository goalTrackingRepository, ThirdPartyRepository thirdPartyRepository, UserRepository userRepository, PortfolioApp portfolioApp, ik4 ik4, xe6<? super cd6> xe6) {
        c cVar;
        int i;
        FitnessDataRepository fitnessDataRepository2;
        SleepSummariesRepository sleepSummariesRepository2;
        PortfolioApp portfolioApp2;
        HybridSyncDataProcessing hybridSyncDataProcessing;
        SleepSessionsRepository sleepSessionsRepository2;
        SummariesRepository summariesRepository2;
        SleepSummariesRepository sleepSummariesRepository3;
        FitnessDataRepository fitnessDataRepository3;
        ActivitiesRepository activitiesRepository2;
        oq4.a aVar2 = aVar;
        String str2 = str;
        SleepSessionsRepository sleepSessionsRepository3 = sleepSessionsRepository;
        SummariesRepository summariesRepository3 = summariesRepository;
        ActivitiesRepository activitiesRepository3 = activitiesRepository;
        ThirdPartyRepository thirdPartyRepository2 = thirdPartyRepository;
        ik4 ik42 = ik4;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof c) {
            cVar = (c) xe62;
            int i2 = cVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                cVar.label = i2 - Integer.MIN_VALUE;
                c cVar2 = cVar;
                Object obj = cVar2.result;
                Object a2 = ff6.a();
                i = cVar2.label;
                if (i != 0) {
                    nc6.a(obj);
                    String str3 = "Save sync result - size of sleepSessions=" + aVar.d().size() + ", size of sampleRaws=" + aVar.c().size() + ", realTimeSteps=" + aVar.b();
                    FLogger.INSTANCE.getLocal().i(a, str3);
                    a(str2, str3);
                    if (xj6.a(PortfolioApp.get.instance().e()) || !xj6.b(PortfolioApp.get.instance().e(), str2, true)) {
                        FLogger.INSTANCE.getLocal().e(a, "Error inside " + a + ".saveSyncResult - Sync data does not match any user's device");
                        return cd6.a;
                    }
                    a(str2, "Saving sleep data");
                    try {
                        sleepSessionsRepository3.insertFromDevice(aVar.d());
                    } catch (Exception e2) {
                        a(str2, "Saving sleep data. error=" + e2.getMessage());
                    }
                    a(str2, "Saving sleep data. OK");
                    FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Upload sleep sessions to server");
                    a(str2, "Saving activity data");
                    rl6 unused = ik6.a(jl6.a(zl6.a()), (af6) null, (ll6) null, new d(aVar2, thirdPartyRepository2, (xe6) null), 3, (Object) null);
                    ik42.a(new Date(), aVar.b());
                    FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartbeat step by syncing");
                    try {
                        activitiesRepository3.insertFromDevice(aVar.c());
                        a(str2, "Saving activity data. OK");
                    } catch (Exception e3) {
                        a(str2, "Saving activity data. error=" + e3.getMessage());
                    }
                    FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
                    try {
                        summariesRepository3.insertFromDevice(aVar.e());
                        a(str2, "Saving activity summaries data. OK");
                    } catch (Exception e4) {
                        a(str2, "Saving activity summaries data. error=" + e4.getMessage());
                    }
                    a(str2, "Saving goal tracking data");
                    try {
                        if (!aVar.f().isEmpty()) {
                            List<WrapperTapEventSummary> f = aVar.f();
                            cVar2.L$0 = this;
                            cVar2.L$1 = aVar2;
                            cVar2.L$2 = str2;
                            cVar2.L$3 = sleepSessionsRepository3;
                            cVar2.L$4 = summariesRepository3;
                            sleepSummariesRepository2 = sleepSummariesRepository;
                            try {
                                cVar2.L$5 = sleepSummariesRepository2;
                                fitnessDataRepository2 = fitnessDataRepository;
                                try {
                                    cVar2.L$6 = fitnessDataRepository2;
                                    cVar2.L$7 = activitiesRepository3;
                                    cVar2.L$8 = hybridPresetRepository;
                                    cVar2.L$9 = goalTrackingRepository;
                                    cVar2.L$10 = thirdPartyRepository2;
                                    cVar2.L$11 = userRepository;
                                    cVar2.L$12 = portfolioApp;
                                    cVar2.L$13 = ik42;
                                    cVar2.L$14 = str3;
                                    cVar2.label = 1;
                                    if (a(f, str, hybridPresetRepository, goalTrackingRepository, userRepository, cVar2) == a2) {
                                        return a2;
                                    }
                                    portfolioApp2 = portfolioApp;
                                    hybridSyncDataProcessing = this;
                                    sleepSessionsRepository2 = sleepSessionsRepository3;
                                    summariesRepository2 = summariesRepository3;
                                    activitiesRepository2 = activitiesRepository3;
                                    sleepSummariesRepository3 = sleepSummariesRepository2;
                                    fitnessDataRepository3 = fitnessDataRepository2;
                                } catch (Exception e5) {
                                    e = e5;
                                    portfolioApp2 = portfolioApp;
                                    hybridSyncDataProcessing = this;
                                    hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                                    rl6 unused2 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (xe6) null), 3, (Object) null);
                                    FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                                    portfolioApp2.b(str2);
                                    return cd6.a;
                                }
                            } catch (Exception e6) {
                                e = e6;
                                fitnessDataRepository2 = fitnessDataRepository;
                                portfolioApp2 = portfolioApp;
                                hybridSyncDataProcessing = this;
                                hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                                rl6 unused3 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (xe6) null), 3, (Object) null);
                                FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                                portfolioApp2.b(str2);
                                return cd6.a;
                            }
                        } else {
                            sleepSummariesRepository2 = sleepSummariesRepository;
                            fitnessDataRepository2 = fitnessDataRepository;
                            a(str2, "No goal tracking data");
                            portfolioApp2 = portfolioApp;
                            rl6 unused4 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (xe6) null), 3, (Object) null);
                            FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                            portfolioApp2.b(str2);
                            return cd6.a;
                        }
                    } catch (Exception e7) {
                        e = e7;
                        sleepSummariesRepository2 = sleepSummariesRepository;
                        fitnessDataRepository2 = fitnessDataRepository;
                        portfolioApp2 = portfolioApp;
                        hybridSyncDataProcessing = this;
                        hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                        rl6 unused5 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (xe6) null), 3, (Object) null);
                        FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                        portfolioApp2.b(str2);
                        return cd6.a;
                    }
                } else if (i == 1) {
                    String str4 = (String) cVar2.L$14;
                    ik4 ik43 = (ik4) cVar2.L$13;
                    portfolioApp2 = (PortfolioApp) cVar2.L$12;
                    UserRepository userRepository2 = (UserRepository) cVar2.L$11;
                    ThirdPartyRepository thirdPartyRepository3 = (ThirdPartyRepository) cVar2.L$10;
                    GoalTrackingRepository goalTrackingRepository2 = (GoalTrackingRepository) cVar2.L$9;
                    HybridPresetRepository hybridPresetRepository2 = (HybridPresetRepository) cVar2.L$8;
                    activitiesRepository2 = (ActivitiesRepository) cVar2.L$7;
                    fitnessDataRepository3 = (FitnessDataRepository) cVar2.L$6;
                    sleepSummariesRepository3 = (SleepSummariesRepository) cVar2.L$5;
                    summariesRepository2 = (SummariesRepository) cVar2.L$4;
                    sleepSessionsRepository2 = (SleepSessionsRepository) cVar2.L$3;
                    str2 = (String) cVar2.L$2;
                    a aVar3 = (a) cVar2.L$1;
                    hybridSyncDataProcessing = (HybridSyncDataProcessing) cVar2.L$0;
                    try {
                        nc6.a(obj);
                    } catch (Exception e8) {
                        e = e8;
                        activitiesRepository3 = activitiesRepository2;
                        fitnessDataRepository2 = fitnessDataRepository3;
                        sleepSummariesRepository2 = sleepSummariesRepository3;
                        summariesRepository3 = summariesRepository2;
                        sleepSessionsRepository3 = sleepSessionsRepository2;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
                activitiesRepository3 = activitiesRepository2;
                fitnessDataRepository2 = fitnessDataRepository3;
                sleepSummariesRepository2 = sleepSummariesRepository3;
                summariesRepository3 = summariesRepository2;
                sleepSessionsRepository3 = sleepSessionsRepository2;
                rl6 unused6 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (xe6) null), 3, (Object) null);
                FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
                portfolioApp2.b(str2);
                return cd6.a;
            }
        }
        cVar = new c(this, xe62);
        c cVar22 = cVar;
        Object obj2 = cVar22.result;
        Object a22 = ff6.a();
        i = cVar22.label;
        if (i != 0) {
        }
        hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
        activitiesRepository3 = activitiesRepository2;
        fitnessDataRepository2 = fitnessDataRepository3;
        sleepSummariesRepository2 = sleepSummariesRepository3;
        summariesRepository3 = summariesRepository2;
        sleepSessionsRepository3 = sleepSessionsRepository2;
        rl6 unused7 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(fitnessDataRepository2, activitiesRepository3, summariesRepository3, sleepSessionsRepository3, sleepSummariesRepository2, (xe6) null), 3, (Object) null);
        FLogger.INSTANCE.getLocal().i(a, "DONE save to database, delete data file");
        portfolioApp2.b(str2);
        return cd6.a;
    }

    @DexIgnore
    public final String a(List<ActivitySample> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (ActivitySample next : list) {
            sb.append("[");
            sb.append("startTime:");
            sb.append(next.getStartTime());
            sb.append(", step:");
            sb.append(next.getSteps());
            sb.append("]");
        }
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v27, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v30, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: com.portfolio.platform.service.usecase.HybridSyncDataProcessing} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final /* synthetic */ Object a(List<? extends WrapperTapEventSummary> list, String str, HybridPresetRepository hybridPresetRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, xe6<? super cd6> xe6) {
        b bVar;
        int i;
        HybridSyncDataProcessing hybridSyncDataProcessing;
        String str2 = str;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof b) {
            bVar = (b) xe62;
            int i2 = bVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                bVar.label = i2 - Integer.MIN_VALUE;
                Object obj = bVar.result;
                Object a2 = ff6.a();
                i = bVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(a, "start process goal data, get active preset to check goal tracking");
                    PortfolioApp instance = PortfolioApp.get.instance();
                    HybridPreset activePresetBySerial = hybridPresetRepository2.getActivePresetBySerial(str2);
                    if (activePresetBySerial != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str3 = a;
                        local.d(str3, "processGoalTrackingData getHybridActivePreset success activePreset=" + activePresetBySerial);
                        ArrayList arrayList = new ArrayList();
                        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
                            Date date = new Date(((long) wrapperTapEventSummary.startTime) * 1000);
                            String uuid = UUID.randomUUID().toString();
                            wg6.a((Object) uuid, "UUID.randomUUID().toString()");
                            DateTime a3 = bk4.a(date, wrapperTapEventSummary.timezoneOffsetInSecond);
                            wg6.a((Object) a3, "DateHelper.createDateTim\u2026y.timezoneOffsetInSecond)");
                            Date date2 = date;
                            arrayList.add(new GoalTrackingData(uuid, a3, wrapperTapEventSummary.timezoneOffsetInSecond, date2, new Date().getTime(), new Date().getTime()));
                        }
                        try {
                            List d2 = yd6.d(arrayList);
                            bVar.L$0 = this;
                            bVar.L$1 = list;
                            bVar.L$2 = str2;
                            bVar.L$3 = hybridPresetRepository2;
                            bVar.L$4 = goalTrackingRepository2;
                            bVar.L$5 = userRepository;
                            bVar.L$6 = instance;
                            bVar.L$7 = activePresetBySerial;
                            bVar.L$8 = arrayList;
                            bVar.label = 1;
                            if (goalTrackingRepository2.insertFromDevice(d2, bVar) == a2) {
                                return a2;
                            }
                            hybridSyncDataProcessing = this;
                        } catch (Exception e2) {
                            e = e2;
                            hybridSyncDataProcessing = this;
                            hybridSyncDataProcessing.a(str2, "Saving goal tracking data. error=" + e.getMessage());
                            return cd6.a;
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().d(a, "processGoalTrackingData getHybridActivePreset onFail");
                        instance.a(CommunicateMode.SYNC, str2, "Goal tracking is not set as active app since user don't have active device.");
                        return cd6.a;
                    }
                } else if (i == 1) {
                    ArrayList arrayList2 = (ArrayList) bVar.L$8;
                    HybridPreset hybridPreset = (HybridPreset) bVar.L$7;
                    PortfolioApp portfolioApp = (PortfolioApp) bVar.L$6;
                    UserRepository userRepository2 = (UserRepository) bVar.L$5;
                    GoalTrackingRepository goalTrackingRepository3 = (GoalTrackingRepository) bVar.L$4;
                    HybridPresetRepository hybridPresetRepository3 = (HybridPresetRepository) bVar.L$3;
                    str2 = bVar.L$2;
                    List list2 = (List) bVar.L$1;
                    hybridSyncDataProcessing = bVar.L$0;
                    try {
                        nc6.a(obj);
                    } catch (Exception e3) {
                        e = e3;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
                return cd6.a;
            }
        }
        bVar = new b(this, xe62);
        Object obj2 = bVar.result;
        Object a22 = ff6.a();
        i = bVar.label;
        if (i != 0) {
        }
        hybridSyncDataProcessing.a(str2, "Saving goal tracking data. OK");
        return cd6.a;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        PortfolioApp.get.instance().a(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }
}
