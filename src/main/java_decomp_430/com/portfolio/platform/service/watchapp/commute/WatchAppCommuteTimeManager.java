package com.portfolio.platform.service.watchapp.commute;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cd0;
import com.fossil.cd6;
import com.fossil.ce;
import com.fossil.cp4;
import com.fossil.dd0;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.kc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.pq4$b$a;
import com.fossil.pq4$c$a;
import com.fossil.qg6;
import com.fossil.qq4;
import com.fossil.rh6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xs4;
import com.fossil.zl6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.Address;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.Arrays;
import kotlinx.coroutines.TimeoutKt;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppCommuteTimeManager {
    @DexIgnore
    public static WatchAppCommuteTimeManager s;
    @DexIgnore
    public static /* final */ a t; // = new a((qg6) null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public an4 e;
    @DexIgnore
    public ce f;
    @DexIgnore
    public xs4 g;
    @DexIgnore
    public DianaPresetRepository h;
    @DexIgnore
    public /* final */ Gson i;
    @DexIgnore
    public String j;
    @DexIgnore
    public TrafficResponse k;
    @DexIgnore
    public Address l;
    @DexIgnore
    public AddressWrapper m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public AddressWrapper o;
    @DexIgnore
    public b p;
    @DexIgnore
    public b q;
    @DexIgnore
    public boolean r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final synchronized WatchAppCommuteTimeManager a() {
            WatchAppCommuteTimeManager i;
            if (WatchAppCommuteTimeManager.s == null) {
                WatchAppCommuteTimeManager.s = new WatchAppCommuteTimeManager((qg6) null);
            }
            i = WatchAppCommuteTimeManager.s;
            if (i == null) {
                wg6.a();
                throw null;
            }
            return i;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements LocationSource.LocationListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onLocationResult(Location location) {
            wg6.b(location, "location");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppCommuteTimeManager", "onLocationResult lastLocation=" + location);
            rm6 unused = ik6.b(jl6.a(zl6.a()), zl6.c(), (ll6) null, new pq4$b$a(this, location, (xe6) null), 2, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1", f = "WatchAppCommuteTimeManager.kt", l = {120}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $destinationAlias;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchAppCommuteTimeManager watchAppCommuteTimeManager, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppCommuteTimeManager;
            this.$destinationAlias = str;
            this.$serial = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$destinationAlias, this.$serial, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                pq4$c$a pq4_c_a = new pq4$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (TimeoutKt.a(DeviceAppResponse.LIFE_TIME, pq4_c_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sf6 implements hg6<xe6<? super rx6<TrafficResponse>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $location$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ TrafficRequest $trafficRequest;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(TrafficRequest trafficRequest, xe6 xe6, WatchAppCommuteTimeManager watchAppCommuteTimeManager, Location location, xe6 xe62) {
            super(1, xe6);
            this.$trafficRequest = trafficRequest;
            this.this$0 = watchAppCommuteTimeManager;
            this.$location$inlined = location;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new d(this.$trafficRequest, xe6, this.this$0, this.$location$inlined, this.$continuation$inlined);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((d) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                ApiServiceV2 a2 = this.this$0.a();
                TrafficRequest trafficRequest = this.$trafficRequest;
                this.label = 1;
                obj = a2.getTrafficStatus(trafficRequest, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {203}, m = "getDurationTime")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WatchAppCommuteTimeManager watchAppCommuteTimeManager, xe6 xe6) {
            super(xe6);
            this.this$0 = watchAppCommuteTimeManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((Location) null, (xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {159, 179}, m = "getDurationTimeBaseOnLocation")
    public static final class f extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WatchAppCommuteTimeManager watchAppCommuteTimeManager, xe6 xe6) {
            super(xe6);
            this.this$0 = watchAppCommuteTimeManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    @DexIgnore
    public WatchAppCommuteTimeManager() {
        this.i = new Gson();
        this.p = new b();
        this.q = new b();
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void e() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            Object r2 = this.a;
            if (r2 != 0) {
                locationSource.observerLocation(r2, this.q, false);
            } else {
                wg6.d("mPortfolioApp");
                throw null;
            }
        } else {
            wg6.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            Object r2 = this.a;
            if (r2 != 0) {
                locationSource.observerLocation(r2, this.p, true);
            } else {
                wg6.d("mPortfolioApp");
                throw null;
            }
        } else {
            wg6.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.q, false);
        } else {
            wg6.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.p, true);
        } else {
            wg6.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final DianaPresetRepository b() {
        DianaPresetRepository dianaPresetRepository = this.h;
        if (dianaPresetRepository != null) {
            return dianaPresetRepository;
        }
        wg6.d("mDianaPresetRepository");
        throw null;
    }

    @DexIgnore
    public final void c() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d() {
        String str;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse");
        AddressWrapper addressWrapper = this.o;
        if (addressWrapper != null) {
            try {
                this.r = true;
                int i2 = qq4.a[addressWrapper.getType().ordinal()];
                if (i2 == 1) {
                    Object r0 = this.a;
                    if (r0 != 0) {
                        str = jm4.a((Context) r0, 2131886187);
                    } else {
                        wg6.d("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 2) {
                    Object r02 = this.a;
                    if (r02 != 0) {
                        str = jm4.a((Context) r02, 2131886185);
                    } else {
                        wg6.d("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 3) {
                    nh6 nh6 = nh6.a;
                    Object r3 = this.a;
                    if (r3 != 0) {
                        String a2 = jm4.a((Context) r3, 2131886184);
                        wg6.a((Object) a2, "LanguageHelper.getString\u2026Popup___ArrivedToAddress)");
                        Object[] objArr = {addressWrapper.getName()};
                        str = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) str, "java.lang.String.format(format, *args)");
                    } else {
                        wg6.d("mPortfolioApp");
                        throw null;
                    }
                } else {
                    throw new kc6();
                }
                wg6.a((Object) str, "message");
                CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(str, dd0.END);
                PortfolioApp instance = PortfolioApp.get.instance();
                String str2 = this.j;
                if (str2 != null) {
                    instance.a((DeviceAppResponse) commuteTimeWatchAppMessage, str2);
                    JSONObject jSONObject = new JSONObject();
                    AddressWrapper addressWrapper2 = this.o;
                    jSONObject.put("des", addressWrapper2 != null ? addressWrapper2.getId() : null);
                    jSONObject.put("type", "end");
                    jSONObject.put("end_type", "arrived");
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                    String str3 = this.j;
                    if (str3 != null) {
                        String jSONObject2 = jSONObject.toString();
                        wg6.a((Object) jSONObject2, "jsonObject.toString()");
                        remote.i(component, session, str3, "WatchAppCommuteTimeManager", jSONObject2);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            } catch (IllegalArgumentException e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse exception exception=" + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public final ApiServiceV2 a() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        wg6.d("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        wg6.b(str, "serial");
        wg6.b(str2, "destination");
        if (i2 == cd0.START.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process start commute time");
            a(str, str2);
        } else if (i2 == cd0.STOP.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process stop commute time");
            JSONObject jSONObject = new JSONObject();
            AddressWrapper addressWrapper = this.o;
            jSONObject.put("des", addressWrapper != null ? addressWrapper.getId() : null);
            jSONObject.put("type", "end");
            jSONObject.put("end_type", "user_exit");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
            String jSONObject2 = jSONObject.toString();
            wg6.a((Object) jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            FLogger.INSTANCE.getRemote().summary(this.r ? 0 : FailureCode.USER_CANCELLED, FLogger.Component.APP, FLogger.Session.DIANA_COMMUTE_TIME, str, "WatchAppCommuteTimeManager");
            this.o = null;
            g();
            h();
        }
    }

    @DexIgnore
    public /* synthetic */ WatchAppCommuteTimeManager(qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp serial " + str + " destinationAlias " + str2);
        this.j = str;
        this.r = false;
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new c(this, str2, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r9v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public final /* synthetic */ Object a(xe6<? super cd6> xe6) {
        f fVar;
        int i2;
        WatchAppCommuteTimeManager watchAppCommuteTimeManager;
        String str;
        CommuteTimeWatchAppMessage commuteTimeWatchAppMessage;
        LocationSource.Result result;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof f) {
            fVar = (f) xe62;
            int i3 = fVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                fVar.label = i3 - Integer.MIN_VALUE;
                Object obj = fVar.result;
                Object a2 = ff6.a();
                i2 = fVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886186);
                    wg6.a((Object) a3, "message");
                    commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(a3, dd0.IN_PROGRESS);
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String str2 = this.j;
                    if (str2 != null) {
                        instance.a((DeviceAppResponse) commuteTimeWatchAppMessage, str2);
                        LocationSource locationSource = this.c;
                        if (locationSource != null) {
                            Object r9 = this.a;
                            if (r9 != 0) {
                                fVar.L$0 = this;
                                fVar.L$1 = a3;
                                fVar.L$2 = commuteTimeWatchAppMessage;
                                fVar.label = 1;
                                Object location = locationSource.getLocation(r9, false, fVar);
                                if (location == a2) {
                                    return a2;
                                }
                                watchAppCommuteTimeManager = this;
                                Object obj2 = location;
                                str = a3;
                                obj = obj2;
                            } else {
                                wg6.d("mPortfolioApp");
                                throw null;
                            }
                        } else {
                            wg6.d("mLocationSource");
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    commuteTimeWatchAppMessage = (CommuteTimeWatchAppMessage) fVar.L$2;
                    str = (String) fVar.L$1;
                    watchAppCommuteTimeManager = (WatchAppCommuteTimeManager) fVar.L$0;
                    nc6.a(obj);
                } else if (i2 == 2) {
                    Location location2 = (Location) fVar.L$4;
                    LocationSource.Result result2 = (LocationSource.Result) fVar.L$3;
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage2 = (CommuteTimeWatchAppMessage) fVar.L$2;
                    String str3 = (String) fVar.L$1;
                    WatchAppCommuteTimeManager watchAppCommuteTimeManager2 = (WatchAppCommuteTimeManager) fVar.L$0;
                    nc6.a(obj);
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                result = (LocationSource.Result) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getDurationTimeBaseOnLocation currentUserLocation ");
                Location location3 = result.getLocation();
                sb.append(location3 == null ? hf6.a(location3.getLatitude()) : null);
                sb.append(':');
                Location location4 = result.getLocation();
                sb.append(location4 == null ? hf6.a(location4.getLongitude()) : null);
                sb.append(" cacheDestination ");
                AddressWrapper addressWrapper = watchAppCommuteTimeManager.m;
                sb.append(addressWrapper == null ? addressWrapper.getAddress() : null);
                sb.append(" currentDestination ");
                AddressWrapper addressWrapper2 = watchAppCommuteTimeManager.o;
                sb.append(addressWrapper2 == null ? addressWrapper2.getAddress() : null);
                local.d("WatchAppCommuteTimeManager", sb.toString());
                watchAppCommuteTimeManager.n = result.getFromCache();
                if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
                    Location location5 = result.getLocation();
                    if (location5 != null) {
                        if (!(watchAppCommuteTimeManager.l == null || watchAppCommuteTimeManager.k == null || !wg6.a((Object) watchAppCommuteTimeManager.m, (Object) watchAppCommuteTimeManager.o))) {
                            Address address = new Address(location5.getLatitude(), location5.getLongitude());
                            Address address2 = watchAppCommuteTimeManager.l;
                            if (address2 != null) {
                                Float a4 = watchAppCommuteTimeManager.a(address, address2);
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                local2.d("WatchAppCommuteTimeManager", "Cache and current destination is the same, distance is " + a4);
                                if (a4 != null) {
                                    a4.floatValue();
                                    if (a4.floatValue() < ((float) 300)) {
                                        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation fromCache");
                                        AddressWrapper addressWrapper3 = watchAppCommuteTimeManager.o;
                                        if (addressWrapper3 != null) {
                                            String name = addressWrapper3.getName();
                                            TrafficResponse trafficResponse = watchAppCommuteTimeManager.k;
                                            if (trafficResponse != null) {
                                                long durationInTraffic = trafficResponse.getDurationInTraffic();
                                                TrafficResponse trafficResponse2 = watchAppCommuteTimeManager.k;
                                                if (trafficResponse2 != null) {
                                                    String status = trafficResponse2.getStatus();
                                                    if (status == null) {
                                                        status = "unknown";
                                                    }
                                                    watchAppCommuteTimeManager.a(name, durationInTraffic, status);
                                                    Address address3 = watchAppCommuteTimeManager.l;
                                                    if (address3 != null) {
                                                        double lat = address3.getLat();
                                                        Address address4 = watchAppCommuteTimeManager.l;
                                                        if (address4 != null) {
                                                            watchAppCommuteTimeManager.a(lat, address4.getLng());
                                                            return cd6.a;
                                                        }
                                                        wg6.a();
                                                        throw null;
                                                    }
                                                    wg6.a();
                                                    throw null;
                                                }
                                                wg6.a();
                                                throw null;
                                            }
                                            wg6.a();
                                            throw null;
                                        }
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "Cache and current destination is the different, start get duration");
                        fVar.L$0 = watchAppCommuteTimeManager;
                        fVar.L$1 = str;
                        fVar.L$2 = commuteTimeWatchAppMessage;
                        fVar.L$3 = result;
                        fVar.L$4 = location5;
                        fVar.label = 2;
                        if (watchAppCommuteTimeManager.a(location5, (xe6<? super cd6>) fVar) == a2) {
                            return a2;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation getLocation error=" + result.getErrorState());
                    watchAppCommuteTimeManager.a(result.getErrorState());
                    FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getDurationTimeBaseOnLocation - current location is null, stop service.");
                }
                return cd6.a;
            }
        }
        fVar = new f(this, xe62);
        Object obj3 = fVar.result;
        Object a22 = ff6.a();
        i2 = fVar.label;
        if (i2 != 0) {
        }
        result = (LocationSource.Result) obj3;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("getDurationTimeBaseOnLocation currentUserLocation ");
        Location location32 = result.getLocation();
        sb2.append(location32 == null ? hf6.a(location32.getLatitude()) : null);
        sb2.append(':');
        Location location42 = result.getLocation();
        sb2.append(location42 == null ? hf6.a(location42.getLongitude()) : null);
        sb2.append(" cacheDestination ");
        AddressWrapper addressWrapper4 = watchAppCommuteTimeManager.m;
        sb2.append(addressWrapper4 == null ? addressWrapper4.getAddress() : null);
        sb2.append(" currentDestination ");
        AddressWrapper addressWrapper22 = watchAppCommuteTimeManager.o;
        sb2.append(addressWrapper22 == null ? addressWrapper22.getAddress() : null);
        local4.d("WatchAppCommuteTimeManager", sb2.toString());
        watchAppCommuteTimeManager.n = result.getFromCache();
        if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0148 A[Catch:{ Exception -> 0x021f }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x014d A[Catch:{ Exception -> 0x021f }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0164 A[Catch:{ Exception -> 0x021f }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0216 A[Catch:{ Exception -> 0x021d }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    public final /* synthetic */ Object a(Location location, xe6<? super cd6> xe6) {
        e eVar;
        int i2;
        String str;
        String str2;
        AddressWrapper addressWrapper;
        Location location2;
        WatchAppCommuteTimeManager watchAppCommuteTimeManager;
        String str3;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof e) {
            eVar = (e) xe62;
            int i3 = eVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                eVar.label = i3 - Integer.MIN_VALUE;
                e eVar2 = eVar;
                Object obj = eVar2.result;
                Object a2 = ff6.a();
                i2 = eVar2.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886183);
                    wg6.a((Object) a3, "message");
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(a3, dd0.IN_PROGRESS);
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String str4 = this.j;
                    if (str4 != null) {
                        instance.a((DeviceAppResponse) commuteTimeWatchAppMessage, str4);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("WatchAppCommuteTimeManager", "getDurationTime location lng=" + location.getLongitude() + " lat=" + location.getLatitude());
                        AddressWrapper addressWrapper2 = this.o;
                        if (addressWrapper2 != null) {
                            str2 = "WatchAppCommuteTimeManager";
                            String str5 = a3;
                            TrafficRequest trafficRequest = new TrafficRequest(addressWrapper2.getAvoidTolls(), new Address(addressWrapper2.getLat(), addressWrapper2.getLng()), new Address(location.getLatitude(), location.getLongitude()));
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("des", addressWrapper2 != null ? addressWrapper2.getId() : null);
                            jSONObject.put("type", "begin_request");
                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                            FLogger.Component component = FLogger.Component.APP;
                            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                            String str6 = this.j;
                            if (str6 != null) {
                                String jSONObject2 = jSONObject.toString();
                                wg6.a((Object) jSONObject2, "jsonObject.toString()");
                                remote.i(component, session, str6, "WatchAppCommuteTimeManager", jSONObject2);
                                try {
                                    addressWrapper = addressWrapper2;
                                    d dVar = new d(trafficRequest, (xe6) null, this, location, eVar2);
                                    eVar2.L$0 = this;
                                    location2 = location;
                                    eVar2.L$1 = location2;
                                    eVar2.L$2 = str5;
                                    eVar2.L$3 = commuteTimeWatchAppMessage;
                                    eVar2.L$4 = addressWrapper;
                                    eVar2.L$5 = trafficRequest;
                                    eVar2.L$6 = jSONObject;
                                    eVar2.label = 1;
                                    obj = ResponseKt.a(dVar, eVar2);
                                    if (obj == a2) {
                                        return a2;
                                    }
                                    watchAppCommuteTimeManager = this;
                                } catch (Exception e2) {
                                    e = e2;
                                    str = str2;
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    local2.e(str, "getDurationTime exception=" + e);
                                    e.printStackTrace();
                                    cd6 cd6 = cd6.a;
                                    return cd6.a;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        return cd6.a;
                    }
                    wg6.a();
                    throw null;
                } else if (i2 == 1) {
                    JSONObject jSONObject3 = (JSONObject) eVar2.L$6;
                    TrafficRequest trafficRequest2 = (TrafficRequest) eVar2.L$5;
                    AddressWrapper addressWrapper3 = (AddressWrapper) eVar2.L$4;
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage2 = (CommuteTimeWatchAppMessage) eVar2.L$3;
                    String str7 = (String) eVar2.L$2;
                    location2 = (Location) eVar2.L$1;
                    watchAppCommuteTimeManager = (WatchAppCommuteTimeManager) eVar2.L$0;
                    try {
                        nc6.a(obj);
                        addressWrapper = addressWrapper3;
                        str2 = "WatchAppCommuteTimeManager";
                    } catch (Exception e3) {
                        e = e3;
                        str = "WatchAppCommuteTimeManager";
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        local22.e(str, "getDurationTime exception=" + e);
                        e.printStackTrace();
                        cd6 cd62 = cd6.a;
                        return cd6.a;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 ap4 = (ap4) obj;
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("des", addressWrapper == null ? addressWrapper.getId() : null);
                jSONObject4.put("type", "response_received");
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.DIANA_COMMUTE_TIME;
                str3 = watchAppCommuteTimeManager.j;
                if (str3 == null) {
                    String jSONObject5 = jSONObject4.toString();
                    wg6.a((Object) jSONObject5, "json.toString()");
                    remote2.i(component2, session2, str3, "WatchAppCommuteTimeManager", jSONObject5);
                    if (ap4 instanceof cp4) {
                        TrafficResponse trafficResponse = (TrafficResponse) ((cp4) ap4).a();
                        if (trafficResponse != null) {
                            watchAppCommuteTimeManager.m = addressWrapper;
                            watchAppCommuteTimeManager.k = trafficResponse;
                            watchAppCommuteTimeManager.l = new Address(location2.getLatitude(), location2.getLongitude());
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            str = str2;
                            try {
                                local3.d(str, "getDurationTime trafficResponse distance " + trafficResponse.getDistance() + "  status " + trafficResponse.getStatus() + " durationInTraffic " + trafficResponse.getDurationInTraffic());
                                String name = addressWrapper.getName();
                                long durationInTraffic = trafficResponse.getDurationInTraffic();
                                String status = trafficResponse.getStatus();
                                if (status == null) {
                                    status = "unknown";
                                }
                                watchAppCommuteTimeManager.a(name, durationInTraffic, status);
                                watchAppCommuteTimeManager.a(location2.getLatitude(), location2.getLongitude());
                                cd6 cd63 = cd6.a;
                            } catch (Exception e4) {
                                e = e4;
                                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                                local222.e(str, "getDurationTime exception=" + e);
                                e.printStackTrace();
                                cd6 cd622 = cd6.a;
                                return cd6.a;
                            }
                        }
                    } else {
                        String str8 = str2;
                        if (ap4 instanceof zo4) {
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            local4.d(str8, "getDurationTime failed: " + ap4);
                            cd6 cd64 = cd6.a;
                        } else {
                            throw new kc6();
                        }
                    }
                    return cd6.a;
                }
                String str9 = str2;
                wg6.a();
                throw null;
            }
        }
        eVar = new e(this, xe62);
        e eVar22 = eVar;
        Object obj2 = eVar22.result;
        Object a22 = ff6.a();
        i2 = eVar22.label;
        if (i2 != 0) {
        }
        ap4 ap42 = (ap4) obj2;
        JSONObject jSONObject42 = new JSONObject();
        jSONObject42.put("des", addressWrapper == null ? addressWrapper.getId() : null);
        jSONObject42.put("type", "response_received");
        IRemoteFLogger remote22 = FLogger.INSTANCE.getRemote();
        FLogger.Component component22 = FLogger.Component.APP;
        FLogger.Session session22 = FLogger.Session.DIANA_COMMUTE_TIME;
        str3 = watchAppCommuteTimeManager.j;
        if (str3 == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(String str, long j2, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse destination " + str + ", durationInTraffic " + j2 + ", trafficStatus " + str2);
        AddressWrapper addressWrapper = this.o;
        if (addressWrapper == null) {
            return;
        }
        if (addressWrapper == null) {
            wg6.a();
            throw null;
        } else if (!TextUtils.isEmpty(addressWrapper.getName())) {
            int a2 = rh6.a(((float) j2) / 60.0f);
            if (xj6.b(str2, "not_found", true)) {
                Object r5 = this.a;
                if (r5 != 0) {
                    String a3 = jm4.a((Context) r5, 2131886182);
                    wg6.a((Object) a3, "message");
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(a3, dd0.END);
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String str3 = this.j;
                    if (str3 != null) {
                        instance.a((DeviceAppResponse) commuteTimeWatchAppMessage, str3);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mPortfolioApp");
                    throw null;
                }
            } else {
                try {
                    CommuteTimeWatchAppInfo commuteTimeWatchAppInfo = new CommuteTimeWatchAppInfo(str, a2, a(str2));
                    PortfolioApp instance2 = PortfolioApp.get.instance();
                    String str4 = this.j;
                    if (str4 != null) {
                        instance2.a((DeviceAppResponse) commuteTimeWatchAppInfo, str4);
                        jl4 c2 = AnalyticsHelper.f.c("commute-time");
                        if (c2 != null) {
                            String str5 = this.j;
                            if (str5 != null) {
                                c2.a(str5, this.n, "");
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        AnalyticsHelper.f.e("commute-time");
                        return;
                    }
                    wg6.a();
                    throw null;
                } catch (IllegalArgumentException e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse exception exception=" + e2.getMessage());
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(String str) {
        switch (str.hashCode()) {
            case -1078030475:
                if (str.equals("medium")) {
                    Object r4 = this.a;
                    if (r4 != 0) {
                        String a2 = jm4.a((Context) r4, 2131887336);
                        wg6.a((Object) a2, "LanguageHelper.getString\u2026ng.traffic_status_medium)");
                        return a2;
                    }
                    wg6.d("mPortfolioApp");
                    throw null;
                }
                break;
            case -284840886:
                if (str.equals("unknown")) {
                    Object r42 = this.a;
                    if (r42 != 0) {
                        String a3 = jm4.a((Context) r42, 2131887337);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026g.traffic_status_unknown)");
                        return a3;
                    }
                    wg6.d("mPortfolioApp");
                    throw null;
                }
                break;
            case 99152071:
                if (str.equals("heavy")) {
                    Object r43 = this.a;
                    if (r43 != 0) {
                        String a4 = jm4.a((Context) r43, 2131887334);
                        wg6.a((Object) a4, "LanguageHelper.getString\u2026ing.traffic_status_heavy)");
                        return a4;
                    }
                    wg6.d("mPortfolioApp");
                    throw null;
                }
                break;
            case 102970646:
                if (str.equals("light")) {
                    Object r44 = this.a;
                    if (r44 != 0) {
                        String a5 = jm4.a((Context) r44, 2131887335);
                        wg6.a((Object) a5, "LanguageHelper.getString\u2026ing.traffic_status_light)");
                        return a5;
                    }
                    wg6.d("mPortfolioApp");
                    throw null;
                }
                break;
        }
        return "--";
    }

    @DexIgnore
    public final Float a(Address address, Address address2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getSimpleDistance between " + address + " and " + address2);
        float[] fArr = new float[1];
        Location.distanceBetween(address2.getLat(), address2.getLng(), address.getLat(), address.getLng(), fArr);
        return Float.valueOf(fArr[0]);
    }

    @DexIgnore
    public final void a(double d2, double d3) {
        Float f2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver updateObserver=" + d2 + ',' + d3);
        AddressWrapper addressWrapper = this.o;
        if (addressWrapper == null) {
            f2 = null;
        } else if (addressWrapper != null) {
            double lat = addressWrapper.getLat();
            AddressWrapper addressWrapper2 = this.o;
            if (addressWrapper2 != null) {
                f2 = a(new Address(lat, addressWrapper2.getLng()), new Address(d2, d3));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver distance=" + f2);
        JSONObject jSONObject = new JSONObject();
        AddressWrapper addressWrapper3 = this.o;
        jSONObject.put("des", addressWrapper3 != null ? addressWrapper3.getId() : null);
        jSONObject.put("type", "loc_update");
        jSONObject.put("dis_to_des", f2);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
        String str = this.j;
        if (str != null) {
            String jSONObject2 = jSONObject.toString();
            wg6.a((Object) jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            if (f2 != null) {
                if (f2 == null) {
                    wg6.a();
                    throw null;
                } else if (f2.floatValue() < ((float) FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION)) {
                    if (f2 == null) {
                        wg6.a();
                        throw null;
                    } else if (f2.floatValue() < ((float) 100)) {
                        g();
                        h();
                        d();
                        return;
                    } else {
                        h();
                        e();
                        return;
                    }
                }
            }
            g();
            f();
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a(LocationSource.ErrorState errorState) {
        jl4 c2 = AnalyticsHelper.f.c("commute-time");
        if (c2 != null) {
            String str = this.j;
            if (str != null) {
                c2.a(str, false, ErrorCodeBuilder.AppError.NOT_FETCH_CURRENT_LOCATION.getValue());
            } else {
                wg6.a();
                throw null;
            }
        }
        AnalyticsHelper.f.e("commute-time");
    }
}
