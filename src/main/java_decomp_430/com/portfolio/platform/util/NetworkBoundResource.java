package com.portfolio.platform.util;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jd;
import com.fossil.jl6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.tx5$a$a;
import com.fossil.tx5$b$a;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.zl6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NetworkBoundResource<ResultType, RequestType> {
    @DexIgnore
    public boolean isFromCache; // = true;
    @DexIgnore
    public /* final */ jd<yx5<ResultType>> result; // = new jd<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ld<S> {
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexIgnore
        public a(NetworkBoundResource networkBoundResource, LiveData liveData) {
            this.a = networkBoundResource;
            this.b = liveData;
        }

        @DexIgnore
        public final void onChanged(ResultType resulttype) {
            this.a.result.a(this.b);
            if (this.a.shouldFetch(resulttype)) {
                this.a.fetchFromNetwork(this.b);
            } else {
                rm6 unused = ik6.b(jl6.a(zl6.c()), (af6) null, (ll6) null, new tx5$a$a(this, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1", f = "NetworkBoundResource.kt", l = {62, 69, 72, 92, 102, 115}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $dbSource;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NetworkBoundResource networkBoundResource, LiveData liveData, xe6 xe6) {
            super(2, xe6);
            this.this$0 = networkBoundResource;
            this.$dbSource = liveData;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$dbSource, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x004a, code lost:
            r7 = new com.fossil.tx5$b$f(r6, (com.fossil.xe6) null);
            r6.L$0 = r1;
            r6.label = 2;
            r7 = com.portfolio.platform.response.ResponseKt.a(r7, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0058, code lost:
            if (r7 != r0) goto L_0x005b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x005a, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x005b, code lost:
            r7 = (com.fossil.ap4) r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x005f, code lost:
            if ((r7 instanceof com.fossil.cp4) == false) goto L_0x010c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
            r3 = (com.fossil.cp4) r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
            if (r3.a() != null) goto L_0x0081;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
            r3 = com.fossil.zl6.c();
            r4 = new com.fossil.tx5$b$b(r6, (com.fossil.xe6) null);
            r6.L$0 = r1;
            r6.L$1 = r7;
            r6.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x007e, code lost:
            if (com.fossil.gk6.a(r3, r4, r6) != r0) goto L_0x0140;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0081, code lost:
            r4 = r6.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
            if (r4.processContinueFetching(r4.processResponse(r3)) == false) goto L_0x00b6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0093, code lost:
            if (com.portfolio.platform.util.NetworkBoundResource.access$isFromCache$p(r6.this$0) == false) goto L_0x009e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0095, code lost:
            com.portfolio.platform.util.NetworkBoundResource.access$setFromCache$p(r6.this$0, r3.b());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
            if (r3.b() != false) goto L_0x00ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a4, code lost:
            r7 = r6.this$0;
            r7.saveCallResult(r7.processResponse(r3));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ad, code lost:
            com.portfolio.platform.util.NetworkBoundResource.access$fetchFromNetwork(r6.this$0, r6.$dbSource);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bc, code lost:
            if (com.portfolio.platform.util.NetworkBoundResource.access$isFromCache$p(r6.this$0) == false) goto L_0x00c7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x00be, code lost:
            com.portfolio.platform.util.NetworkBoundResource.access$setFromCache$p(r6.this$0, r3.b());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00cb, code lost:
            if (r3.b() != false) goto L_0x00d6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x00cd, code lost:
            r4 = r6.this$0;
            r4.saveCallResult(r4.processResponse(r3));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00dc, code lost:
            if (com.portfolio.platform.util.NetworkBoundResource.access$isFromCache$p(r6.this$0) != false) goto L_0x00f5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00de, code lost:
            r3 = com.fossil.zl6.c();
            r4 = new com.fossil.tx5$b$c(r6, (com.fossil.xe6) null);
            r6.L$0 = r1;
            r6.L$1 = r7;
            r6.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f2, code lost:
            if (com.fossil.gk6.a(r3, r4, r6) != r0) goto L_0x0140;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f4, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f5, code lost:
            r3 = com.fossil.zl6.c();
            r4 = new com.fossil.tx5$b$d(r6, (com.fossil.xe6) null);
            r6.L$0 = r1;
            r6.L$1 = r7;
            r6.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0109, code lost:
            if (com.fossil.gk6.a(r3, r4, r6) != r0) goto L_0x0140;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x010b, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x010e, code lost:
            if ((r7 instanceof com.fossil.zo4) == false) goto L_0x0140;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0110, code lost:
            r6.this$0.onFetchFailed(((com.fossil.zo4) r7).d());
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network failed");
            r3 = com.fossil.zl6.c();
            r4 = new com.fossil.tx5$b$e(r6, r7, (com.fossil.xe6) null);
            r6.L$0 = r1;
            r6.L$1 = r7;
            r6.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x013d, code lost:
            if (com.fossil.gk6.a(r3, r4, r6) != r0) goto L_0x0140;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x013f, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x0142, code lost:
            return com.fossil.cd6.a;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il6 il62 = this.p$;
                    cn6 c = zl6.c();
                    tx5$b$a tx5_b_a = new tx5$b$a(this, (xe6) null);
                    this.L$0 = il62;
                    this.label = 1;
                    if (gk6.a(c, tx5_b_a, this) != a) {
                        il6 = il62;
                        break;
                    } else {
                        return a;
                    }
                case 1:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    ap4 ap4 = (ap4) this.L$1;
                    il6 il63 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public NetworkBoundResource() {
        this.result.b(yx5.e.a(null));
        this.isFromCache = true;
        LiveData loadFromDb = loadFromDb();
        this.result.a(loadFromDb, new a(this, loadFromDb));
    }

    @DexIgnore
    private final void fetchFromNetwork(LiveData<ResultType> liveData) {
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, liveData, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    private final void setValue(yx5<? extends ResultType> yx5) {
        if (!wg6.a((Object) (yx5) this.result.a(), (Object) yx5)) {
            this.result.b(yx5);
        }
    }

    @DexIgnore
    public final LiveData<yx5<ResultType>> asLiveData() {
        jd<yx5<ResultType>> jdVar = this.result;
        if (jdVar != null) {
            return jdVar;
        }
        throw new rc6("null cannot be cast to non-null type androidx.lifecycle.LiveData<com.portfolio.platform.util.Resource<ResultType>>");
    }

    @DexIgnore
    public abstract Object createCall(xe6<? super rx6<RequestType>> xe6);

    @DexIgnore
    public abstract LiveData<ResultType> loadFromDb();

    @DexIgnore
    public abstract void onFetchFailed(Throwable th);

    @DexIgnore
    public boolean processContinueFetching(RequestType requesttype) {
        return false;
    }

    @DexIgnore
    public RequestType processResponse(cp4<RequestType> cp4) {
        wg6.b(cp4, "response");
        RequestType a2 = cp4.a();
        if (a2 != null) {
            return a2;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public abstract void saveCallResult(RequestType requesttype);

    @DexIgnore
    public abstract boolean shouldFetch(ResultType resulttype);
}
