package com.portfolio.platform.manager;

import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mn6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.qm4;
import com.fossil.qm4$d$a;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.uk6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xp6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.fossil.zp6;
import com.fossil.zq6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.ChecksumUtil;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDownloadManager {
    @DexIgnore
    public static /* final */ dl6 h; // = zl6.b();
    @DexIgnore
    public int a;
    @DexIgnore
    public xp6 b; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public ApiServiceV2 c;
    @DexIgnore
    public /* final */ LinkedBlockingDeque<qm4.b> d; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public /* final */ List<String> e; // = new ArrayList();
    @DexIgnore
    public /* final */ uk6 f; // = mn6.a((rm6) null, 1, (Object) null);
    @DexIgnore
    public /* final */ il6 g; // = jl6.a(this.f.plus(h));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ LocalFile b;
        @DexIgnore
        public /* final */ c c;

        @DexIgnore
        public b(LocalFile localFile, c cVar) {
            wg6.b(localFile, "localFile");
            this.b = localFile;
            this.c = cVar;
        }

        @DexIgnore
        public final c a() {
            return this.c;
        }

        @DexIgnore
        public final LocalFile b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof b)) {
                return false;
            }
            return wg6.a((Object) this.b.getRemoteUrl(), (Object) ((b) obj).b.getRemoteUrl());
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = this.b.hashCode() * 31;
            c cVar = this.c;
            return hashCode + (cVar != null ? cVar.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "DownloadFileJobInfo(localFile=" + this.b + ", callback=" + this.c + ")";
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void onComplete(boolean z, LocalFile localFile);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1", f = "FileDownloadManager.kt", l = {90}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ b $job;
        @DexIgnore
        public /* final */ /* synthetic */ LocalFile $localFile;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FileDownloadManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(FileDownloadManager fileDownloadManager, LocalFile localFile, b bVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = fileDownloadManager;
            this.$localFile = localFile;
            this.$job = bVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$localFile, this.$job, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.lang.String} */
        /* JADX WARNING: type inference failed for: r2v0 */
        /* JADX WARNING: type inference failed for: r2v3, types: [java.io.InputStream] */
        /* JADX WARNING: type inference failed for: r7v17, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r2v18 */
        /* JADX WARNING: type inference failed for: r2v19 */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x016d, code lost:
            if (r2 != 0) goto L_0x016f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x016f, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0172, code lost:
            r11.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0198, code lost:
            if (r2 == 0) goto L_0x0172;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x01a5, code lost:
            if (android.text.TextUtils.isEmpty(r10.$localFile.getChecksum()) != false) goto L_0x0218;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x01bb, code lost:
            if (com.portfolio.platform.cloudimage.ChecksumUtil.INSTANCE.verifyDownloadFile(r10.$localFile.getLocalUri(), r10.$localFile.getChecksum()) == false) goto L_0x01f3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x01bd, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("FileDownloadManager", "Callback of " + r10.$localFile.getRemoteUrl() + ": true");
            r10.$localFile.setPinType(0);
            r11 = r10.$job.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x01eb, code lost:
            if (r11 == null) goto L_0x022a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x01ed, code lost:
            r11.onComplete(true, r10.$localFile);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x01f3, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e("FileDownloadManager", "Callback of " + r10.$localFile.getRemoteUrl() + ": false");
            r1 = 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0218, code lost:
            r10.$localFile.setPinType(0);
            r11 = r10.$job.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0223, code lost:
            if (r11 == null) goto L_0x022a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0225, code lost:
            r11.onComplete(true, r10.$localFile);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x022a, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("FileDownloadManager", r10.$localFile.getRemoteUrl() + " downloaded, having " + com.portfolio.platform.manager.FileDownloadManager.b(r10.this$0) + " download running");
            r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            r0 = new java.lang.StringBuilder();
            r0.append("size of mQueue: ");
            r0.append(com.portfolio.platform.manager.FileDownloadManager.d(r10.this$0).size());
            r11.d("FileDownloadManager", r0.toString());
            com.portfolio.platform.manager.FileDownloadManager.e(r10.this$0).remove(r10.$localFile.getRemoteUrl());
            r11 = r10.this$0;
            com.portfolio.platform.manager.FileDownloadManager.a(r11, com.portfolio.platform.manager.FileDownloadManager.b(r11) - 1);
            com.portfolio.platform.manager.FileDownloadManager.a(r10.this$0);
         */
        @DexIgnore
        /* JADX WARNING: Multi-variable type inference failed */
        public final Object invokeSuspend(Object obj) {
            int i;
            String str;
            Object a = ff6.a();
            int i2 = this.label;
            Object r2 = 0;
            if (i2 == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d("FileDownloadManager", "Downloading " + this.$localFile.getRemoteUrl());
                String str2 = PortfolioApp.get.instance().getFilesDir() + File.separator + this.$localFile.getFileName();
                if (new File(str2).exists()) {
                    this.$localFile.setLocalUri(str2);
                    if (TextUtils.isEmpty(this.$localFile.getChecksum()) || ChecksumUtil.INSTANCE.verifyDownloadFile(this.$localFile.getLocalUri(), this.$localFile.getChecksum())) {
                        FLogger.INSTANCE.getLocal().d("FileDownloadManager", "File  " + this.$localFile.getRemoteUrl() + " exists, downloaded successful");
                        this.$localFile.setPinType(0);
                        c a2 = this.$job.a();
                        if (a2 != null) {
                            a2.onComplete(true, this.$localFile);
                        }
                        this.this$0.e.remove(this.$localFile.getRemoteUrl());
                        FileDownloadManager fileDownloadManager = this.this$0;
                        fileDownloadManager.a = fileDownloadManager.a - 1;
                        this.this$0.a();
                        return cd6.a;
                    }
                }
                qm4$d$a qm4_d_a = new qm4$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.I$0 = 0;
                this.L$1 = str2;
                this.label = 1;
                obj = ResponseKt.a(qm4_d_a, this);
                if (obj == a) {
                    return a;
                }
                str = str2;
                i = 0;
            } else if (i2 == 1) {
                str = (String) this.L$1;
                i = this.I$0;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap4 = (ap4) obj;
            if (ap4 instanceof cp4) {
                FLogger.INSTANCE.getLocal().d("FileDownloadManager", "isSuccessful of " + this.$localFile.getRemoteUrl() + " onResponse: response = " + ap4);
                byte[] bArr = new byte[4096];
                zq6 zq6 = (zq6) ((cp4) ap4).a();
                if (zq6 != null) {
                    r2 = zq6.byteStream();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(str);
                if (r2 != 0) {
                    while (true) {
                        try {
                            int read = r2.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("exception ");
                            e.printStackTrace();
                            sb.append(cd6.a);
                            local.d("FileDownloadManager", sb.toString());
                            e.printStackTrace();
                        } catch (Throwable th) {
                            if (r2 != 0) {
                                r2.close();
                            }
                            fileOutputStream.close();
                            throw th;
                        }
                    }
                }
                fileOutputStream.flush();
                this.$localFile.setLocalUri(str);
            } else if (ap4 instanceof zo4) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("onFailure of ");
                sb2.append(this.$localFile.getRemoteUrl());
                sb2.append(": Failure code=");
                zo4 zo4 = (zo4) ap4;
                sb2.append(zo4.a());
                sb2.append(" message=");
                ServerError c = zo4.c();
                if (c != null) {
                    r2 = c.getMessage();
                }
                sb2.append(r2);
                local2.e("FileDownloadManager", sb2.toString());
                this.this$0.e.remove(this.$localFile.getRemoteUrl());
                FileDownloadManager fileDownloadManager2 = this.this$0;
                fileDownloadManager2.a = fileDownloadManager2.a - 1;
                this.this$0.a();
                i = 1;
            }
            if (i != 0) {
                if (this.$job.c() < 1) {
                    b bVar = this.$job;
                    bVar.a(bVar.c() + 1);
                    this.this$0.b(this.$job);
                } else {
                    c a3 = this.$job.a();
                    if (a3 != null) {
                        a3.onComplete(false, this.$localFile);
                    }
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.FileDownloadManager$enqueue$1", f = "FileDownloadManager.kt", l = {189}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FileDownloadManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(FileDownloadManager fileDownloadManager, xe6 xe6) {
            super(2, xe6);
            this.this$0 = fileDownloadManager;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final Object invokeSuspend(Object obj) {
            xp6 xp6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                xp6 c = this.this$0.b;
                this.L$0 = il6;
                this.L$1 = c;
                this.label = 1;
                if (c.a((Object) null, this) == a) {
                    return a;
                }
                xp6 = c;
            } else if (i == 1) {
                xp6 = (xp6) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.a();
                cd6 cd6 = cd6.a;
                xp6.a((Object) null);
                return cd6.a;
            } catch (Throwable th) {
                xp6.a((Object) null);
                throw th;
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public FileDownloadManager() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final ApiServiceV2 b() {
        ApiServiceV2 apiServiceV2 = this.c;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        wg6.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final synchronized void a() {
        if (this.d.size() > 0 && this.a < 3) {
            b pop = this.d.pop();
            wg6.a((Object) pop, "jobToRun");
            a(pop);
        }
    }

    @DexIgnore
    public final synchronized void b(b bVar) {
        wg6.b(bVar, "job");
        if (!this.e.contains(bVar.b().getRemoteUrl())) {
            if (!this.d.contains(bVar)) {
                this.d.add(bVar);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FileDownloadManager", bVar.b().getRemoteUrl() + " added to mQueue at times: " + bVar.c() + ", having " + this.a + " download running");
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
                return;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("FileDownloadManager", bVar.b().getRemoteUrl() + " is already added");
    }

    @DexIgnore
    public final void a(b bVar) {
        LocalFile b2 = bVar.b();
        this.a++;
        this.e.add(b2.getRemoteUrl());
        rm6 unused = ik6.b(this.g, (af6) null, (ll6) null, new d(this, b2, bVar, (xe6) null), 3, (Object) null);
    }
}
