package com.portfolio.platform.manager.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.fossil.af6;
import com.fossil.c36;
import com.fossil.gv1;
import com.fossil.hy5;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.jn4$c$a;
import com.fossil.ll6;
import com.fossil.ln4;
import com.fossil.lp4;
import com.fossil.m26;
import com.fossil.n26;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.WechatApiService;
import okhttp3.Interceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFLoginWechatManager implements c36 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((qg6) null);
    @DexIgnore
    public WechatApiService a; // = ((WechatApiService) lp4.g.a(WechatApiService.class));
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return MFLoginWechatManager.c;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager a;
        @DexIgnore
        public /* final */ /* synthetic */ ln4 b;

        @DexIgnore
        public b(MFLoginWechatManager mFLoginWechatManager, ln4 ln4) {
            this.a = mFLoginWechatManager;
            this.b = ln4;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.b()) {
                this.b.a(500, (gv1) null, "");
            }
        }
    }

    /*
    static {
        String simpleName = MFLoginWechatManager.class.getSimpleName();
        wg6.a((Object) simpleName, "MFLoginWechatManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public MFLoginWechatManager() {
        lp4.g.a("https://api.weixin.qq.com/sns/");
        lp4.g.a((Interceptor) null);
    }

    @DexIgnore
    public final WechatApiService a() {
        return this.a;
    }

    @DexIgnore
    public final void a(String str) {
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "appId");
        hy5.a().a(str);
    }

    @DexIgnore
    public final void a(Intent intent) {
        wg6.b(intent, "intent");
        hy5.a().a(intent, this);
    }

    @DexIgnore
    public final void a(Activity activity, ln4 ln4) {
        wg6.b(activity, Constants.ACTIVITY);
        wg6.b(ln4, Constants.CALLBACK);
        hy5.a().a((Context) activity);
        hy5.a().a(activity.getIntent(), this);
        this.b = false;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new b(this, ln4), ScanService.BLE_SCAN_TIMEOUT);
        hy5.a().a((hy5.a) new c(this, handler, ln4));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements hy5.a {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ ln4 c;

        @DexIgnore
        public c(MFLoginWechatManager mFLoginWechatManager, Handler handler, ln4 ln4) {
            this.a = mFLoginWechatManager;
            this.b = handler;
            this.c = ln4;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "authToken");
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MFLoginWechatManager.d.a();
            local.d(a2, "Wechat onAuthSuccess: " + str);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "Wechat step 1: Login using wechat success");
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new jn4$c$a(this, str, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void b() {
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "Wechat onAuthAppNotInstalled");
            this.c.a(600, (gv1) null, "");
        }

        @DexIgnore
        public void c() {
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "Wechat onAuthCancel");
            this.c.a(2, (gv1) null, "");
        }

        @DexIgnore
        public void a() {
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "Wechat onAuthFailed");
            this.c.a(600, (gv1) null, "");
        }
    }

    @DexIgnore
    public void a(m26 m26) {
        wg6.b(m26, "baseReq");
        hy5.a().a(m26);
    }

    @DexIgnore
    public void a(n26 n26) {
        wg6.b(n26, "baseResp");
        hy5.a().a(n26);
    }
}
