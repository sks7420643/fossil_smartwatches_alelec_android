package com.portfolio.platform.manager;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.bn4;
import com.fossil.cd6;
import com.fossil.cq4;
import com.fossil.e90;
import com.fossil.eq4;
import com.fossil.f90;
import com.fossil.ff6;
import com.fossil.h90;
import com.fossil.hf6;
import com.fossil.id0;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mi4;
import com.fossil.n90;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.ni4;
import com.fossil.p06;
import com.fossil.q70;
import com.fossil.qg6;
import com.fossil.qi4;
import com.fossil.r70;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.si4;
import com.fossil.tj4;
import com.fossil.u70;
import com.fossil.ui4;
import com.fossil.v70;
import com.fossil.vh4;
import com.fossil.vm4$d$a;
import com.fossil.w70;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.ym4;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.RingPhoneComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.RingMyPhoneMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LinkStreamingManager implements eq4 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a((qg6) null);
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<cq4> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ HybridPresetRepository b;
    @DexIgnore
    public /* final */ AlarmsRepository c;
    @DexIgnore
    public /* final */ QuickResponseRepository d;
    @DexIgnore
    public /* final */ SetNotificationUseCase e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LinkStreamingManager.f;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager c;

        @DexIgnore
        public b(LinkStreamingManager linkStreamingManager, String str, int i) {
            wg6.b(str, "serial");
            this.c = linkStreamingManager;
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void run() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkStreamingManager.g.a();
            local.d(a2, "Inside SteamingAction .run - eventId=" + this.b + ", serial=" + this.a);
            if (TextUtils.isEmpty(this.a)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = LinkStreamingManager.g.a();
                local2.e(a3, "Inside .onStreamingEvent of " + this.a + " no active device");
                return;
            }
            HybridPreset activePresetBySerial = this.c.b.getActivePresetBySerial(this.a);
            if (activePresetBySerial != null) {
                Iterator<HybridPresetAppSetting> it = activePresetBySerial.getButtons().iterator();
                while (it.hasNext()) {
                    HybridPresetAppSetting next = it.next();
                    if (wg6.a((Object) next.getAppId(), (Object) MicroAppInstruction.MicroAppID.Companion.getMicroAppIdFromDeviceEventId(this.b).getValue())) {
                        int i = this.b;
                        if (i == e90.RING_MY_PHONE_MICRO_APP.ordinal()) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = LinkStreamingManager.g.a();
                            local3.d(a4, "MicroAppAction.run UAPP_RING_PHONE microAppSetting " + next.getSettings());
                            LinkStreamingManager linkStreamingManager = this.c;
                            String str = this.a;
                            String settings = next.getSettings();
                            if (settings != null) {
                                linkStreamingManager.a(str, settings);
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else if (i == e90.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == e90.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.EXTRA_INFO, next.getSettings());
                            CommuteTimeService.A.a(PortfolioApp.get.instance(), bundle, this.c);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(LinkStreamingManager linkStreamingManager, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = linkStreamingManager;
            this.$action = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$action, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (this.$action == f90.SET.ordinal()) {
                    List activeAlarms = this.this$0.c.getActiveAlarms();
                    if (activeAlarms == null) {
                        activeAlarms = new ArrayList();
                    }
                    PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(activeAlarms));
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ n90 $notificationEvent$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ r70 $subEvent;
        @DexIgnore
        public byte B$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(r70 r70, xe6 xe6, LinkStreamingManager linkStreamingManager, n90 n90) {
            super(2, xe6);
            this.$subEvent = r70;
            this.this$0 = linkStreamingManager;
            this.$notificationEvent$inlined = n90;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.$subEvent, xe6, this.this$0, this.$notificationEvent$inlined);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: com.portfolio.platform.data.model.QuickResponseMessage} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: com.portfolio.platform.data.model.QuickResponseMessage} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v13, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: com.portfolio.platform.data.model.QuickResponseMessage} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v14, resolved type: com.portfolio.platform.data.model.QuickResponseMessage} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00a8  */
        public final Object invokeSuspend(Object obj) {
            byte b;
            QuickResponseMessage quickResponseMessage;
            QuickResponseSender quickResponseSender;
            il6 il6;
            QuickResponseMessage quickResponseMessage2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                byte messageId = this.$subEvent.getMessageId();
                QuickResponseRepository c = this.this$0.d;
                this.L$0 = il6;
                this.B$0 = messageId;
                this.label = 1;
                Object allQuickResponse = c.getAllQuickResponse(this);
                if (allQuickResponse == a) {
                    return a;
                }
                Object obj2 = allQuickResponse;
                b = messageId;
                obj = obj2;
            } else if (i == 1) {
                b = this.B$0;
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                QuickResponseMessage quickResponseMessage3 = (QuickResponseMessage) this.L$2;
                quickResponseMessage = (QuickResponseMessage) this.L$1;
                b = this.B$0;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                quickResponseSender = (QuickResponseSender) obj;
                if (quickResponseSender != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = LinkStreamingManager.g.a();
                    local.d(a2, "process to send sms to " + quickResponseSender.getContent() + " with id " + b + " senderId " + this.$subEvent.getSenderId());
                    ym4.c.a().a(quickResponseSender.getContent(), quickResponseMessage.getResponse(), new vm4$d$a(this, b, quickResponseMessage));
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Iterator it = ((Iterable) obj).iterator();
            while (true) {
                if (!it.hasNext()) {
                    quickResponseMessage2 = null;
                    break;
                }
                Object next = it.next();
                if (hf6.a(next.getId() == b).booleanValue()) {
                    quickResponseMessage2 = next;
                    break;
                }
            }
            QuickResponseMessage quickResponseMessage4 = quickResponseMessage2;
            if (quickResponseMessage4 != null) {
                QuickResponseRepository c2 = this.this$0.d;
                int senderId = this.$subEvent.getSenderId();
                this.L$0 = il6;
                this.B$0 = b;
                this.L$1 = quickResponseMessage4;
                this.L$2 = quickResponseMessage4;
                this.label = 2;
                Object quickResponseSender2 = c2.getQuickResponseSender(senderId, this);
                if (quickResponseSender2 == a) {
                    return a;
                }
                quickResponseMessage = quickResponseMessage4;
                obj = quickResponseSender2;
                quickResponseSender = (QuickResponseSender) obj;
                if (quickResponseSender != null) {
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(int i, String str, xe6 xe6) {
            super(2, xe6);
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.$action, this.$deviceId, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (this.$action == f90.SET.ordinal()) {
                    PortfolioApp.get.instance().p(this.$deviceId);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(LinkStreamingManager linkStreamingManager, int i, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = linkStreamingManager;
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$action, this.$deviceId, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.usecase.SetNotificationUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (this.$action == f90.SET.ordinal()) {
                    this.this$0.e.a(new SetNotificationUseCase.b(this.$deviceId), (CoroutineUseCase.e) null);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $ringtoneName;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str, xe6 xe6) {
            super(2, xe6);
            this.$ringtoneName = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.$ringtoneName, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                List<Ringtone> d = tj4.f.d();
                if (!d.isEmpty()) {
                    for (Ringtone next : d) {
                        if (wg6.a((Object) next.getRingtoneName(), (Object) this.$ringtoneName)) {
                            bn4.o.a().b(next);
                            return cd6.a;
                        }
                    }
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String canonicalName = LinkStreamingManager.class.getCanonicalName();
        if (canonicalName != null) {
            wg6.a((Object) canonicalName, "LinkStreamingManager::class.java.canonicalName!!");
            f = canonicalName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public LinkStreamingManager(HybridPresetRepository hybridPresetRepository, AlarmsRepository alarmsRepository, QuickResponseRepository quickResponseRepository, SetNotificationUseCase setNotificationUseCase) {
        wg6.b(hybridPresetRepository, "mPresetRepository");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        wg6.b(quickResponseRepository, "mQuickResponseRepository");
        wg6.b(setNotificationUseCase, "mSetNotificationUseCase");
        this.b = hybridPresetRepository;
        this.c = alarmsRepository;
        this.d = quickResponseRepository;
        this.e = setNotificationUseCase;
        PortfolioApp.get.b((Object) this);
    }

    @DexIgnore
    @p06
    public final void onDeviceAppEvent(mi4 mi4) {
        wg6.b(mi4, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        local.d(str, "Inside " + f + ".onDeviceAppEvent - appId=" + mi4.a() + ", serial=" + mi4.c());
        int a2 = mi4.a();
        if (a2 == e90.WEATHER_COMPLICATION.ordinal()) {
            jl4 b2 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("weather")));
            b2.d();
            AnalyticsHelper.f.a("weather", b2);
            WeatherManager a3 = WeatherManager.n.a();
            String c2 = mi4.c();
            wg6.a((Object) c2, "event.serial");
            a3.b(c2);
        } else if (a2 == e90.CHANCE_OF_RAIN_COMPLICATION.ordinal()) {
            jl4 b3 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("chance-of-rain")));
            b3.d();
            AnalyticsHelper.f.a("chance-of-rain", b3);
            WeatherManager a4 = WeatherManager.n.a();
            String c3 = mi4.c();
            wg6.a((Object) c3, "event.serial");
            a4.a(c3);
        } else if (a2 == e90.RING_PHONE.ordinal()) {
            int i = mi4.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c4 = mi4.c();
            wg6.a((Object) c4, "event.serial");
            d(c4, i);
        } else if (a2 == e90.WEATHER_WATCH_APP.ordinal()) {
            WeatherManager a5 = WeatherManager.n.a();
            String c5 = mi4.c();
            wg6.a((Object) c5, "event.serial");
            a5.c(c5);
        } else if (a2 == e90.NOTIFICATION_FILTER_SYNC.ordinal()) {
            int i2 = mi4.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c6 = mi4.c();
            wg6.a((Object) c6, "event.serial");
            c(c6, i2);
        } else if (a2 == e90.ALARM_SYNC.ordinal()) {
            int i3 = mi4.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c7 = mi4.c();
            wg6.a((Object) c7, "event.serial");
            a(c7, i3);
        } else if (a2 == e90.DEVICE_CONFIG_SYNC.ordinal()) {
            int i4 = mi4.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c8 = mi4.c();
            wg6.a((Object) c8, "event.serial");
            b(c8, i4);
        } else if (a2 == e90.MUSIC_CONTROL.ordinal()) {
            NotifyMusicEventResponse.MusicMediaAction musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.values()[mi4.b().getInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.NONE.ordinal())];
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "Inside " + f + ".onMusicActionEvent - musicActionEvent=" + musicMediaAction + ", serial=" + mi4.c());
            ((MusicControlComponent) MusicControlComponent.o.a(PortfolioApp.get.instance())).a(musicMediaAction);
        } else if (a2 == e90.APP_NOTIFICATION_CONTROL.ordinal()) {
            FLogger.INSTANCE.getLocal().d(f, "on app notification control received");
            a(mi4.b().getParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA));
        } else if (a2 == e90.COMMUTE_TIME_WATCH_APP.ordinal()) {
            jl4 b4 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("commute-time")));
            b4.d();
            AnalyticsHelper.f.a("commute-time", b4);
            int i5 = mi4.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String string = mi4.b().getString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, "");
            WatchAppCommuteTimeManager a6 = WatchAppCommuteTimeManager.t.a();
            String c9 = mi4.c();
            wg6.a((Object) c9, "event.serial");
            wg6.a((Object) string, "destination");
            a6.a(c9, string, i5);
        } else {
            String c10 = mi4.c();
            wg6.a((Object) c10, "event.serial");
            new Thread(new b(this, c10, mi4.a())).start();
        }
    }

    @DexIgnore
    @p06
    public final void onMicroAppCancelEvent(ni4 ni4) {
        wg6.b(ni4, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        local.d(str, "Inside " + f + ".onMicroAppCancelEvent - event=" + ni4.a() + ", serial=" + ni4.b());
        a((List<? extends cq4>) this.a);
    }

    @DexIgnore
    @p06
    public final void onMusicActionEvent(qi4 qi4) {
        wg6.b(qi4, Constants.EVENT);
        FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Inside ");
        sb.append(f);
        sb.append(".onMusicActionEvent - musicActionEvent=");
        qi4.a();
        throw null;
    }

    @DexIgnore
    @p06
    public final void onStreamingEvent(si4 si4) {
        wg6.b(si4, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        local.d(str, "Inside " + f + ".onStreamingEvent - event=" + si4.a() + ", serial=" + si4.b());
        String b2 = si4.b();
        wg6.a((Object) b2, "event.serial");
        new Thread(new b(this, b2, si4.a())).start();
    }

    @DexIgnore
    public final void a() {
        PortfolioApp.get.c(this);
    }

    @DexIgnore
    public final rm6 c(String str, int i) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new f(this, i, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void d(String str, int i) {
        id0 id0;
        FLogger.INSTANCE.getLocal().d(f, "startRingMyPhone");
        if (i == id0.ON.ordinal()) {
            bn4.o.a().a(new Ringtone(Constants.RINGTONE_DEFAULT, ""), 60000);
            id0 = id0.ON;
        } else {
            bn4.o.a().b();
            id0 = id0.OFF;
        }
        PortfolioApp.get.instance().a((DeviceAppResponse) new RingPhoneComplicationAppInfo(id0), str);
    }

    @DexIgnore
    public final void a(n90 n90) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        StringBuilder sb = new StringBuilder();
        sb.append("processCallMessageNotificationEvent event ");
        sb.append(n90);
        sb.append(" subEvent ");
        h90 h90 = (h90) n90;
        sb.append(h90 != null ? h90.getAction() : null);
        local.d(str, sb.toString());
        if (n90 != null) {
            r70 action = h90.getAction();
            if (action instanceof v70) {
                if (DeviceHelper.o.l()) {
                    ym4.c.a().b();
                }
            } else if (action instanceof q70) {
                if (DeviceHelper.o.l()) {
                    ym4.c.a().a();
                }
            } else if (action instanceof w70) {
                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new d(action, (xe6) null, this, n90), 3, (Object) null);
            } else {
                if (action instanceof u70) {
                }
            }
        }
    }

    @DexIgnore
    public void b(cq4 cq4) {
        wg6.b(cq4, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(f, "removeObject");
        if (!this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "removeObject mServiceListSize=" + this.a.size());
            Iterator<cq4> it = this.a.iterator();
            while (it.hasNext()) {
                cq4 next = it.next();
                wg6.a((Object) next, "autoStopBaseService");
                if (next.c() == cq4.c()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = f;
                    local2.d(str2, "removeObject actionId " + cq4.c());
                    this.a.remove(next);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, vh4 vh4) {
        wg6.b(vh4, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        local.d(str, "onStatusChanged action " + i);
    }

    @DexIgnore
    public final rm6 b(String str, int i) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new e(i, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(cq4 cq4) {
        wg6.b(cq4, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(f, "addObject");
        if (!this.a.contains(cq4)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "addObject service actionId " + cq4.c());
            this.a.add(cq4);
        }
    }

    @DexIgnore
    public final void a(List<? extends cq4> list) {
        if (list != null && !this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "stopServices serviceListSize=" + this.a.size());
            for (cq4 b2 : list) {
                b2.b();
            }
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wg6.b(str, "deviceId");
        wg6.b(str2, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = f;
        local.d(str3, "startRingMyPhone, setting=" + str2);
        try {
            if (TextUtils.isEmpty(str2)) {
                List<Ringtone> d2 = tj4.f.d();
                Ringtone ringtone = d2.get(0);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = f;
                local2.d(str4, "ringtones=" + d2 + ", defaultRingtone=" + ringtone);
                bn4.o.a().b(ringtone);
                return;
            }
            String component1 = ((Ringtone) new Gson().a(str2, Ringtone.class)).component1();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str5 = f;
            local3.d(str5, "MicroAppAction.run - ringtone " + str2);
            PortfolioApp.get.instance().a((DeviceAppResponse) new RingMyPhoneMicroAppResponse(), str);
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new g(component1, (xe6) null), 3, (Object) null);
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AppUtil", "Error when read asset file - ex=" + e2);
        }
    }

    @DexIgnore
    public final rm6 a(String str, int i) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, i, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final String a(String str) {
        nh6 nh6 = nh6.a;
        Object[] objArr = {str};
        String format = String.format("update_%s", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }
}
