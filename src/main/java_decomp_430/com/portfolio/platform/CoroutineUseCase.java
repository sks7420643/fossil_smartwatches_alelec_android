package com.portfolio.platform;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.m24.a;
import com.fossil.m24.b;
import com.fossil.m24.d;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class CoroutineUseCase<Q extends m24.b, R extends m24.d, E extends m24.a> {
    @DexIgnore
    public m24.e<? super R, ? super E> a;
    @DexIgnore
    public String b; // = "CoroutineUseCase";
    @DexIgnore
    public /* final */ il6 c; // = jl6.a(zl6.a());

    @DexIgnore
    public interface a extends c {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d extends c {
    }

    @DexIgnore
    public interface e<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$1", f = "CoroutineUseCase.kt", l = {35}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ e $callBack;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(CoroutineUseCase coroutineUseCase, e eVar, b bVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = coroutineUseCase;
            this.$callBack = eVar;
            this.$requestValues = bVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$callBack, this.$requestValues, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                this.this$0.a = this.$callBack;
                CoroutineUseCase coroutineUseCase = this.this$0;
                coroutineUseCase.b = coroutineUseCase.c();
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Start UseCase");
                CoroutineUseCase coroutineUseCase2 = this.this$0;
                b bVar = this.$requestValues;
                this.L$0 = il6;
                this.label = 1;
                obj = coroutineUseCase2.a(bVar, (xe6<Object>) this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (obj instanceof d) {
                this.this$0.a((d) obj);
            } else if (obj instanceof a) {
                this.this$0.a((a) obj);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.CoroutineUseCase$onError$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $errorValue;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(CoroutineUseCase coroutineUseCase, a aVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = coroutineUseCase;
            this.$errorValue = aVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$errorValue, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback failed");
                e a = this.this$0.a;
                if (a != null) {
                    a.a(this.$errorValue);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.CoroutineUseCase$onSuccess$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ d $response;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(CoroutineUseCase coroutineUseCase, d dVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = coroutineUseCase;
            this.$response = dVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, this.$response, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback success");
                e a = this.this$0.a;
                if (a != null) {
                    a.onSuccess(this.$response);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public abstract Object a(Q q, xe6<Object> xe6);

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final il6 b() {
        return this.c;
    }

    @DexIgnore
    public final rm6 a(Q q, m24.e<? super R, ? super E> eVar) {
        return ik6.b(this.c, (af6) null, (ll6) null, new f(this, eVar, q, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final m24.e<R, E> a() {
        return this.a;
    }

    @DexIgnore
    public final void a(m24.e<? super R, ? super E> eVar) {
        wg6.b(eVar, Constants.CALLBACK);
        this.a = eVar;
    }

    @DexIgnore
    public final rm6 a(R r) {
        wg6.b(r, "response");
        return ik6.b(this.c, zl6.c(), (ll6) null, new h(this, r, (xe6) null), 2, (Object) null);
    }

    @DexIgnore
    public final rm6 a(E e2) {
        wg6.b(e2, "errorValue");
        return ik6.b(this.c, zl6.c(), (ll6) null, new g(this, e2, (xe6) null), 2, (Object) null);
    }
}
