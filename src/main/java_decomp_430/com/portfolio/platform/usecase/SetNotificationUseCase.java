package com.portfolio.platform.usecase;

import com.fossil.NotificationAppHelper;
import com.fossil.an4;
import com.fossil.d15;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.tw5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetNotificationUseCase extends m24<tw5.b, Object, tw5.c> {
    @DexIgnore
    public /* final */ d15 d;
    @DexIgnore
    public /* final */ mz4 e;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase f;
    @DexIgnore
    public /* final */ NotificationsRepository g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ an4 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(String str) {
            wg6.b(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.SetNotificationUseCase", f = "SetNotificationUseCase.kt", l = {38}, m = "run")
    public static final class d extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetNotificationUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(SetNotificationUseCase setNotificationUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = setNotificationUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((tw5.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SetNotificationUseCase(d15 d15, mz4 mz4, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, an4 an4) {
        wg6.b(d15, "mGetApp");
        wg6.b(mz4, "mGetAllContactGroups");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wg6.b(notificationsRepository, "mNotificationsRepository");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(an4, "mSharePref");
        this.d = d15;
        this.e = mz4;
        this.f = notificationSettingsDatabase;
        this.g = notificationsRepository;
        this.h = deviceRepository;
        this.i = an4;
    }

    @DexIgnore
    public String c() {
        return "SetNotificationUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(tw5.b bVar, xe6<Object> xe6) {
        d dVar;
        int i2;
        String str;
        AppNotificationFilterSettings appNotificationFilterSettings;
        Object obj;
        if (xe6 instanceof d) {
            dVar = (d) xe6;
            int i3 = dVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dVar.label = i3 - Integer.MIN_VALUE;
                d dVar2 = dVar;
                Object obj2 = dVar2.result;
                Object a2 = ff6.a();
                i2 = dVar2.label;
                if (i2 != 0) {
                    nc6.a(obj2);
                    if (bVar == null || xj6.a(bVar.a())) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SetNotificationUseCase", "Invalid request: Request = " + bVar);
                        return new c("Invalid request");
                    }
                    str = bVar.a();
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        d15 d15 = this.d;
                        mz4 mz4 = this.e;
                        NotificationSettingsDatabase notificationSettingsDatabase = this.f;
                        an4 an4 = this.i;
                        dVar2.L$0 = this;
                        dVar2.L$1 = bVar;
                        dVar2.L$2 = str;
                        dVar2.label = 1;
                        obj = notificationAppHelper.a(d15, mz4, notificationSettingsDatabase, an4, dVar2);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        appNotificationFilterSettings = NotificationAppHelper.b.a(this.g.getAllNotificationsByHour(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), NotificationAppHelper.b.a(this.h.getSkuModelBySerialPrefix(DeviceHelper.o.b(str)), str));
                        PortfolioApp.get.instance().a(appNotificationFilterSettings, str);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
                        return new Object();
                    }
                } else if (i2 == 1) {
                    b bVar2 = (b) dVar2.L$1;
                    SetNotificationUseCase setNotificationUseCase = (SetNotificationUseCase) dVar2.L$0;
                    nc6.a(obj2);
                    Object obj3 = obj2;
                    str = (String) dVar2.L$2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                appNotificationFilterSettings = new AppNotificationFilterSettings((List) obj, System.currentTimeMillis());
                PortfolioApp.get.instance().a(appNotificationFilterSettings, str);
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
                return new Object();
            }
        }
        dVar = new d(this, xe6);
        d dVar22 = dVar;
        Object obj22 = dVar22.result;
        Object a22 = ff6.a();
        i2 = dVar22.label;
        if (i2 != 0) {
        }
        appNotificationFilterSettings = new AppNotificationFilterSettings((List) obj, System.currentTimeMillis());
        PortfolioApp.get.instance().a(appNotificationFilterSettings, str);
        ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
        local222.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
        return new Object();
    }
}
