package com.portfolio.platform.usecase;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.kc6;
import com.fossil.kw5;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.n24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.ww5;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VerifySecretKeyUseCase extends m24<ww5.b, ww5.d, ww5.c> {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ kw5 h;
    @DexIgnore
    public /* final */ PortfolioApp i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            wg6.b(str, "errorMesagge");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {73}, m = "getLocalSecretKey")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ VerifySecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(VerifySecretKeyUseCase verifySecretKeyUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = verifySecretKeyUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {89}, m = "getServerSecretKey")
    public static final class f extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ VerifySecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(VerifySecretKeyUseCase verifySecretKeyUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = verifySecretKeyUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {36, 37}, m = "run")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ VerifySecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(VerifySecretKeyUseCase verifySecretKeyUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = verifySecretKeyUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((ww5.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public VerifySecretKeyUseCase(DeviceRepository deviceRepository, UserRepository userRepository, kw5 kw5, PortfolioApp portfolioApp) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(kw5, "mDecryptValueKeyStoreUseCase");
        wg6.b(portfolioApp, "mApp");
        this.f = deviceRepository;
        this.g = userRepository;
        this.h = kw5;
        this.i = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object b(xe6<? super String> xe6) {
        f fVar;
        int i2;
        ap4 ap4;
        if (xe6 instanceof f) {
            fVar = (f) xe6;
            int i3 = fVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                fVar.label = i3 - Integer.MIN_VALUE;
                Object obj = fVar.result;
                Object a2 = ff6.a();
                i2 = fVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    DeviceRepository deviceRepository = this.f;
                    String str = this.d;
                    fVar.L$0 = this;
                    fVar.label = 1;
                    obj = deviceRepository.getDeviceSecretKey(str, fVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    VerifySecretKeyUseCase verifySecretKeyUseCase = (VerifySecretKeyUseCase) fVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("VerifySecretKeyUseCase", "getServerSecretKey " + ap4);
                if (!(ap4 instanceof cp4)) {
                    Object a3 = ((cp4) ap4).a();
                    if (a3 != null) {
                        return (String) a3;
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    return null;
                } else {
                    throw new kc6();
                }
            }
        }
        fVar = new f(this, xe6);
        Object obj2 = fVar.result;
        Object a22 = ff6.a();
        i2 = fVar.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("VerifySecretKeyUseCase", "getServerSecretKey " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public String c() {
        return "VerifySecretKeyUseCase";
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v0, resolved type: com.portfolio.platform.usecase.VerifySecretKeyUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: com.portfolio.platform.usecase.VerifySecretKeyUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: com.portfolio.platform.usecase.VerifySecretKeyUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: com.portfolio.platform.usecase.VerifySecretKeyUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0125, code lost:
        if (r10 == null) goto L_0x0129;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(ww5.b bVar, xe6<Object> xe6) {
        g gVar;
        Object a2;
        int i2;
        CoroutineUseCase coroutineUseCase;
        String str;
        Object obj;
        CoroutineUseCase coroutineUseCase2;
        String str2;
        VerifySecretKeyUseCase verifySecretKeyUseCase;
        MFUser mFUser;
        MFUser mFUser2;
        if (xe6 instanceof g) {
            gVar = (g) xe6;
            int i3 = gVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                gVar.label = i3 - Integer.MIN_VALUE;
                Object obj2 = gVar.result;
                a2 = ff6.a();
                i2 = gVar.label;
                if (i2 != 0) {
                    nc6.a(obj2);
                    if (bVar != null) {
                        this.d = bVar.a();
                        MFUser currentUser = this.g.getCurrentUser();
                        if (currentUser != null) {
                            this.e = currentUser.getUserId() + ':' + this.i.e();
                            gVar.L$0 = this;
                            gVar.L$1 = bVar;
                            gVar.L$2 = currentUser;
                            gVar.L$3 = currentUser;
                            gVar.label = 1;
                            Object b2 = b(gVar);
                            if (b2 == a2) {
                                return a2;
                            }
                            verifySecretKeyUseCase = this;
                            mFUser = currentUser;
                            obj2 = b2;
                            mFUser2 = mFUser;
                        } else {
                            coroutineUseCase = this;
                            coroutineUseCase.a(new c(600, ""));
                            return new Object();
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    mFUser2 = (MFUser) gVar.L$2;
                    verifySecretKeyUseCase = (VerifySecretKeyUseCase) gVar.L$0;
                    nc6.a(obj2);
                    ww5.b bVar2 = (b) gVar.L$1;
                    mFUser = (MFUser) gVar.L$3;
                    bVar = bVar2;
                } else if (i2 == 2) {
                    MFUser mFUser3 = (MFUser) gVar.L$3;
                    MFUser mFUser4 = (MFUser) gVar.L$2;
                    b bVar3 = (b) gVar.L$1;
                    nc6.a(obj2);
                    Object obj3 = obj2;
                    str = (String) gVar.L$4;
                    obj = obj3;
                    coroutineUseCase2 = (VerifySecretKeyUseCase) gVar.L$0;
                    str2 = (String) obj;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("VerifySecretKeyUseCase", "localSecretKey " + str2 + " serverSecretKey " + str);
                    if (str != null && ((str.hashCode() != 0 || !str.equals("")) && (!wg6.a((Object) str2, (Object) str)))) {
                        str2 = str;
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("VerifySecretKeyUseCase", "secret key " + str2);
                    rm6 a3 = coroutineUseCase2.a(new d(str2));
                    coroutineUseCase = coroutineUseCase2;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                str = (String) obj2;
                gVar.L$0 = verifySecretKeyUseCase;
                gVar.L$1 = bVar;
                gVar.L$2 = mFUser2;
                gVar.L$3 = mFUser;
                gVar.L$4 = str;
                gVar.label = 2;
                obj = verifySecretKeyUseCase.a(gVar);
                if (obj != a2) {
                    return a2;
                }
                coroutineUseCase2 = verifySecretKeyUseCase;
                str2 = (String) obj;
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("VerifySecretKeyUseCase", "localSecretKey " + str2 + " serverSecretKey " + str);
                str2 = str;
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("VerifySecretKeyUseCase", "secret key " + str2);
                rm6 a32 = coroutineUseCase2.a(new d(str2));
                coroutineUseCase = coroutineUseCase2;
            }
        }
        gVar = new g(this, xe6);
        Object obj22 = gVar.result;
        a2 = ff6.a();
        i2 = gVar.label;
        if (i2 != 0) {
        }
        str = (String) obj22;
        gVar.L$0 = verifySecretKeyUseCase;
        gVar.L$1 = bVar;
        gVar.L$2 = mFUser2;
        gVar.L$3 = mFUser;
        gVar.L$4 = str;
        gVar.label = 2;
        obj = verifySecretKeyUseCase.a(gVar);
        if (obj != a2) {
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v8, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.kw5] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final /* synthetic */ Object a(xe6<? super String> xe6) {
        e eVar;
        int i2;
        CoroutineUseCase.c cVar;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i3 = eVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                eVar.label = i3 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i2 = eVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d("VerifySecretKeyUseCase", "getLocalSecretKey");
                    Object r8 = this.h;
                    String str = this.e;
                    if (str != null) {
                        kw5.b bVar = new kw5.b(str);
                        eVar.L$0 = this;
                        eVar.label = 1;
                        obj = n24.a(r8, bVar, eVar);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    VerifySecretKeyUseCase verifySecretKeyUseCase = (VerifySecretKeyUseCase) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                cVar = (CoroutineUseCase.c) obj;
                if (!(cVar instanceof kw5.d)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Get local key success ");
                    kw5.d dVar = (kw5.d) cVar;
                    sb.append(dVar.a());
                    local.d("VerifySecretKeyUseCase", sb.toString());
                    return dVar.a();
                }
                boolean z = cVar instanceof kw5.c;
                return null;
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i2 = eVar.label;
        if (i2 != 0) {
        }
        cVar = (CoroutineUseCase.c) obj2;
        if (!(cVar instanceof kw5.d)) {
        }
    }
}
