package com.portfolio.platform.usecase;

import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.gp4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.pw5;
import com.fossil.pw5$e$a;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xh4;
import com.fossil.y24;
import com.fossil.yu3;
import com.fossil.zl6;
import com.fossil.zo4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetWeather extends y24<pw5.c, pw5.d, pw5.b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return GetWeather.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.a {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "errorMessage");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.b {
        @DexIgnore
        public /* final */ WeatherSettings.TEMP_UNIT a;
        @DexIgnore
        public /* final */ LatLng b;

        @DexIgnore
        public c(LatLng latLng, WeatherSettings.TEMP_UNIT temp_unit) {
            wg6.b(latLng, "latLng");
            wg6.b(temp_unit, "tempUnit");
            yu3.a(temp_unit);
            wg6.a((Object) temp_unit, "checkNotNull(tempUnit)");
            this.a = temp_unit;
            yu3.a(latLng);
            wg6.a((Object) latLng, "checkNotNull(latLng)");
            this.b = latLng;
        }

        @DexIgnore
        public final LatLng a() {
            return this.b;
        }

        @DexIgnore
        public final WeatherSettings.TEMP_UNIT b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements y24.c {
        @DexIgnore
        public /* final */ Weather a;

        @DexIgnore
        public d(Weather weather) {
            wg6.b(weather, "weather");
            this.a = weather;
        }

        @DexIgnore
        public final Weather a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1", f = "GetWeather.kt", l = {31}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ c $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GetWeather this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(GetWeather getWeather, c cVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = getWeather;
            this.$requestValues = cVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$requestValues, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                pw5$e$a pw5_e_a = new pw5$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = ResponseKt.a(pw5_e_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap4 = (ap4) obj;
            if (ap4 instanceof cp4) {
                gp4 gp4 = new gp4();
                gp4.a((ku3) ((cp4) ap4).a());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = GetWeather.f.a();
                local.d(a2, "onSuccess" + gp4.a());
                y24.d b = this.this$0.a();
                Weather a3 = gp4.a();
                wg6.a((Object) a3, "mfWeatherResponse.weather");
                b.onSuccess(new d(a3));
            } else if (ap4 instanceof zo4) {
                this.this$0.a().a(new b(xh4.NETWORK_ERROR.name()));
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = GetWeather.class.getSimpleName();
        wg6.a((Object) simpleName, "GetWeather::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public GetWeather(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.d = apiServiceV2;
    }

    @DexIgnore
    public void a(c cVar) {
        wg6.b(cVar, "requestValues");
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase");
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new e(this, cVar, (xe6) null), 3, (Object) null);
    }
}
