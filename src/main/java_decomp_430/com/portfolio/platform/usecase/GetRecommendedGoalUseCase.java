package com.portfolio.platform.usecase;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.kc6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.nw5;
import com.fossil.qg6;
import com.fossil.rh4;
import com.fossil.wg6;
import com.fossil.wj4;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetRecommendedGoalUseCase extends m24<nw5.c, nw5.d, nw5.b> {
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ SleepSummariesRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ rh4 d;

        @DexIgnore
        public c(int i, int i2, int i3, rh4 rh4) {
            wg6.b(rh4, "gender");
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = rh4;
        }

        @DexIgnore
        public final rh4 a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.GetRecommendedGoalUseCase", f = "GetRecommendedGoalUseCase.kt", l = {30, 32, 64, 65}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetRecommendedGoalUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(GetRecommendedGoalUseCase getRecommendedGoalUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = getRecommendedGoalUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((nw5.c) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GetRecommendedGoalUseCase(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        wg6.b(summariesRepository, "mSummaryRepository");
        wg6.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wg6.b(sleepSessionsRepository, "mSleepSessionsRepository");
        this.d = summariesRepository;
        this.e = sleepSummariesRepository;
        this.f = sleepSessionsRepository;
    }

    @DexIgnore
    public String c() {
        return "GetRecommendedGoalUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0139 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01b8  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0253 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0271 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    public Object a(nw5.c cVar, xe6<Object> xe6) {
        e eVar;
        Object a2;
        int i;
        GetRecommendedGoalUseCase getRecommendedGoalUseCase;
        nw5.c cVar2;
        ap4 ap4;
        ap4 ap42;
        ActivityRecommendedGoals activityRecommendedGoals;
        SleepRecommendedGoal sleepRecommendedGoal;
        ActivitySettings activitySettings;
        SleepSummariesRepository sleepSummariesRepository;
        int recommendedSleepGoal;
        ActivityRecommendedGoals activityRecommendedGoals2;
        SleepRecommendedGoal sleepRecommendedGoal2;
        SummariesRepository summariesRepository;
        nw5.c cVar3;
        GetRecommendedGoalUseCase getRecommendedGoalUseCase2;
        Object downloadRecommendedGoals;
        nw5.c cVar4 = cVar;
        xe6<Object> xe62 = xe6;
        if (xe62 instanceof e) {
            eVar = (e) xe62;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("height ");
                    if (cVar4 != null) {
                        sb.append(cVar.b());
                        sb.append(" weight ");
                        sb.append(cVar.d());
                        sb.append(" age ");
                        sb.append(cVar.c());
                        sb.append(" gender ");
                        sb.append(cVar.a());
                        local.d("GetRecommendedGoalUseCase", sb.toString());
                        SummariesRepository summariesRepository2 = this.d;
                        int c2 = cVar.c();
                        int d2 = cVar.d();
                        int b2 = cVar.b();
                        String value = cVar.a().getValue();
                        eVar.L$0 = this;
                        eVar.L$1 = cVar4;
                        eVar.label = 1;
                        obj = summariesRepository2.downloadRecommendedGoals(c2, d2, b2, value, eVar);
                        if (obj == a2) {
                            return a2;
                        }
                        cVar3 = cVar4;
                        getRecommendedGoalUseCase2 = this;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i == 1) {
                    nc6.a(obj);
                    cVar3 = (c) eVar.L$1;
                    getRecommendedGoalUseCase2 = (GetRecommendedGoalUseCase) eVar.L$0;
                } else if (i == 2) {
                    nc6.a(obj);
                    ap4 = (ap4) eVar.L$2;
                    cVar2 = (c) eVar.L$1;
                    getRecommendedGoalUseCase = (GetRecommendedGoalUseCase) eVar.L$0;
                    ap42 = (ap4) obj;
                    if (!(ap4 instanceof cp4)) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init activity recommended goal from server");
                        Object a3 = ((cp4) ap4).a();
                        if (a3 != null) {
                            activityRecommendedGoals2 = (ActivityRecommendedGoals) a3;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else if (ap4 instanceof zo4) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init activity recommended goal from client");
                        activityRecommendedGoals2 = new ActivityRecommendedGoals(wj4.a.b(), wj4.a.a(cVar2.c(), cVar2.d() / 1000, cVar2.b(), cVar2.a()), wj4.a.a());
                    } else {
                        throw new kc6();
                    }
                    activityRecommendedGoals = activityRecommendedGoals2;
                    if (!(ap42 instanceof cp4)) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init sleepRecommendedGoals from server");
                        Object a4 = ((cp4) ap42).a();
                        if (a4 != null) {
                            sleepRecommendedGoal2 = (SleepRecommendedGoal) a4;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else if (ap42 instanceof zo4) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init sleepRecommendedGoals from client");
                        sleepRecommendedGoal2 = new SleepRecommendedGoal(wj4.a.a(cVar2.c()));
                    } else {
                        throw new kc6();
                    }
                    sleepRecommendedGoal = sleepRecommendedGoal2;
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("GetRecommendedGoalUseCase", "Update recommendActivityGoals " + activityRecommendedGoals + " recommendSleepGoal " + sleepRecommendedGoal);
                    getRecommendedGoalUseCase.d.upsertRecommendGoals(activityRecommendedGoals);
                    getRecommendedGoalUseCase.f.upsertRecommendedGoals(sleepRecommendedGoal);
                    activitySettings = new ActivitySettings(activityRecommendedGoals.getRecommendedStepsGoal(), activityRecommendedGoals.getRecommendedCaloriesGoal(), activityRecommendedGoals.getRecommendedActiveTimeGoal());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("GetRecommendedGoalUseCase", "Update ActivitySettings " + activitySettings + " sleepSetting " + sleepRecommendedGoal.getRecommendedSleepGoal());
                    summariesRepository = getRecommendedGoalUseCase.d;
                    eVar.L$0 = getRecommendedGoalUseCase;
                    eVar.L$1 = cVar2;
                    eVar.L$2 = ap4;
                    eVar.L$3 = ap42;
                    eVar.L$4 = activityRecommendedGoals;
                    eVar.L$5 = sleepRecommendedGoal;
                    eVar.L$6 = activitySettings;
                    eVar.label = 3;
                    if (summariesRepository.pushActivitySettingsToServer(activitySettings, eVar) == a2) {
                        return a2;
                    }
                    sleepSummariesRepository = getRecommendedGoalUseCase.e;
                    recommendedSleepGoal = sleepRecommendedGoal.getRecommendedSleepGoal();
                    eVar.L$0 = getRecommendedGoalUseCase;
                    eVar.L$1 = cVar2;
                    eVar.L$2 = ap4;
                    eVar.L$3 = ap42;
                    eVar.L$4 = activityRecommendedGoals;
                    eVar.L$5 = sleepRecommendedGoal;
                    eVar.L$6 = activitySettings;
                    eVar.label = 4;
                    if (sleepSummariesRepository.pushLastSleepGoalToServer(recommendedSleepGoal, eVar) == a2) {
                    }
                    return new d();
                } else if (i == 3) {
                    activitySettings = (ActivitySettings) eVar.L$6;
                    sleepRecommendedGoal = (SleepRecommendedGoal) eVar.L$5;
                    activityRecommendedGoals = (ActivityRecommendedGoals) eVar.L$4;
                    ap42 = (ap4) eVar.L$3;
                    ap4 = (ap4) eVar.L$2;
                    cVar2 = (c) eVar.L$1;
                    getRecommendedGoalUseCase = (GetRecommendedGoalUseCase) eVar.L$0;
                    nc6.a(obj);
                    sleepSummariesRepository = getRecommendedGoalUseCase.e;
                    recommendedSleepGoal = sleepRecommendedGoal.getRecommendedSleepGoal();
                    eVar.L$0 = getRecommendedGoalUseCase;
                    eVar.L$1 = cVar2;
                    eVar.L$2 = ap4;
                    eVar.L$3 = ap42;
                    eVar.L$4 = activityRecommendedGoals;
                    eVar.L$5 = sleepRecommendedGoal;
                    eVar.L$6 = activitySettings;
                    eVar.label = 4;
                    if (sleepSummariesRepository.pushLastSleepGoalToServer(recommendedSleepGoal, eVar) == a2) {
                        return a2;
                    }
                    return new d();
                } else if (i == 4) {
                    ActivitySettings activitySettings2 = (ActivitySettings) eVar.L$6;
                    SleepRecommendedGoal sleepRecommendedGoal3 = (SleepRecommendedGoal) eVar.L$5;
                    ActivityRecommendedGoals activityRecommendedGoals3 = (ActivityRecommendedGoals) eVar.L$4;
                    ap4 ap43 = (ap4) eVar.L$3;
                    ap4 ap44 = (ap4) eVar.L$2;
                    c cVar5 = (c) eVar.L$1;
                    GetRecommendedGoalUseCase getRecommendedGoalUseCase3 = (GetRecommendedGoalUseCase) eVar.L$0;
                    nc6.a(obj);
                    return new d();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 ap45 = (ap4) obj;
                SleepSessionsRepository sleepSessionsRepository = getRecommendedGoalUseCase2.f;
                int c3 = cVar3.c();
                int d3 = cVar3.d();
                int b3 = cVar3.b();
                String value2 = cVar3.a().getValue();
                eVar.L$0 = getRecommendedGoalUseCase2;
                eVar.L$1 = cVar3;
                eVar.L$2 = ap45;
                eVar.label = 2;
                nw5.c cVar6 = cVar3;
                downloadRecommendedGoals = sleepSessionsRepository.downloadRecommendedGoals(c3, d3, b3, value2, eVar);
                if (downloadRecommendedGoals != a2) {
                    return a2;
                }
                getRecommendedGoalUseCase = getRecommendedGoalUseCase2;
                ap4 = ap45;
                obj = downloadRecommendedGoals;
                cVar2 = cVar6;
                ap42 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                }
                activityRecommendedGoals = activityRecommendedGoals2;
                if (!(ap42 instanceof cp4)) {
                }
                sleepRecommendedGoal = sleepRecommendedGoal2;
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("GetRecommendedGoalUseCase", "Update recommendActivityGoals " + activityRecommendedGoals + " recommendSleepGoal " + sleepRecommendedGoal);
                getRecommendedGoalUseCase.d.upsertRecommendGoals(activityRecommendedGoals);
                getRecommendedGoalUseCase.f.upsertRecommendedGoals(sleepRecommendedGoal);
                activitySettings = new ActivitySettings(activityRecommendedGoals.getRecommendedStepsGoal(), activityRecommendedGoals.getRecommendedCaloriesGoal(), activityRecommendedGoals.getRecommendedActiveTimeGoal());
                ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                local32.d("GetRecommendedGoalUseCase", "Update ActivitySettings " + activitySettings + " sleepSetting " + sleepRecommendedGoal.getRecommendedSleepGoal());
                summariesRepository = getRecommendedGoalUseCase.d;
                eVar.L$0 = getRecommendedGoalUseCase;
                eVar.L$1 = cVar2;
                eVar.L$2 = ap4;
                eVar.L$3 = ap42;
                eVar.L$4 = activityRecommendedGoals;
                eVar.L$5 = sleepRecommendedGoal;
                eVar.L$6 = activitySettings;
                eVar.label = 3;
                if (summariesRepository.pushActivitySettingsToServer(activitySettings, eVar) == a2) {
                }
                sleepSummariesRepository = getRecommendedGoalUseCase.e;
                recommendedSleepGoal = sleepRecommendedGoal.getRecommendedSleepGoal();
                eVar.L$0 = getRecommendedGoalUseCase;
                eVar.L$1 = cVar2;
                eVar.L$2 = ap4;
                eVar.L$3 = ap42;
                eVar.L$4 = activityRecommendedGoals;
                eVar.L$5 = sleepRecommendedGoal;
                eVar.L$6 = activitySettings;
                eVar.label = 4;
                if (sleepSummariesRepository.pushLastSleepGoalToServer(recommendedSleepGoal, eVar) == a2) {
                }
                return new d();
            }
        }
        eVar = new e(this, xe62);
        Object obj2 = eVar.result;
        a2 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        ap4 ap452 = (ap4) obj2;
        SleepSessionsRepository sleepSessionsRepository2 = getRecommendedGoalUseCase2.f;
        int c32 = cVar3.c();
        int d32 = cVar3.d();
        int b32 = cVar3.b();
        String value22 = cVar3.a().getValue();
        eVar.L$0 = getRecommendedGoalUseCase2;
        eVar.L$1 = cVar3;
        eVar.L$2 = ap452;
        eVar.label = 2;
        nw5.c cVar62 = cVar3;
        downloadRecommendedGoals = sleepSessionsRepository2.downloadRecommendedGoals(c32, d32, b32, value22, eVar);
        if (downloadRecommendedGoals != a2) {
        }
    }
}
