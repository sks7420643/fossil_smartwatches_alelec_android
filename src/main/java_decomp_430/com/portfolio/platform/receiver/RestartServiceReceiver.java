package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zx5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RestartServiceReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = RestartServiceReceiver.class.getSimpleName();
        wg6.a((Object) simpleName, "RestartServiceReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        FLogger.INSTANCE.getLocal().e(a, "Inside ---onReceive RestartServiceReceiver");
        if (!(PortfolioApp.get.instance().e().length() == 0)) {
            FLogger.INSTANCE.getLocal().e(a, "Inside ---onReceive RestartServiceReceiver start MFDeviceService & ButtonService ");
            Context context2 = context;
            zx5.a.a(zx5.a, context2, MFDeviceService.class, (String) null, 4, (Object) null);
            zx5.a.a(zx5.a, context2, ButtonService.class, (String) null, 4, (Object) null);
        }
    }
}
