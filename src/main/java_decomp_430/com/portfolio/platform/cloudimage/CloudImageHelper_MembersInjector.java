package com.portfolio.platform.cloudimage;

import com.fossil.u04;
import com.portfolio.platform.PortfolioApp;
import dagger.MembersInjector;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper_MembersInjector implements MembersInjector<CloudImageHelper> {
    @DexIgnore
    public /* final */ Provider<u04> mAppExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mAppProvider;

    @DexIgnore
    public CloudImageHelper_MembersInjector(Provider<u04> provider, Provider<PortfolioApp> provider2) {
        this.mAppExecutorsProvider = provider;
        this.mAppProvider = provider2;
    }

    @DexIgnore
    public static MembersInjector<CloudImageHelper> create(Provider<u04> provider, Provider<PortfolioApp> provider2) {
        return new CloudImageHelper_MembersInjector(provider, provider2);
    }

    @DexIgnore
    public static void injectMApp(CloudImageHelper cloudImageHelper, PortfolioApp portfolioApp) {
        cloudImageHelper.mApp = portfolioApp;
    }

    @DexIgnore
    public static void injectMAppExecutors(CloudImageHelper cloudImageHelper, u04 u04) {
        cloudImageHelper.mAppExecutors = u04;
    }

    @DexIgnore
    public void injectMembers(CloudImageHelper cloudImageHelper) {
        injectMAppExecutors(cloudImageHelper, this.mAppExecutorsProvider.get());
        injectMApp(cloudImageHelper, this.mAppProvider.get());
    }
}
