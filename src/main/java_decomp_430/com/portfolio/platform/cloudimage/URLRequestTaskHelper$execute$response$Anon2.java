package com.portfolio.platform.cloudimage;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$2", f = "URLRequestTaskHelper.kt", l = {64}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$response$Anon2 extends sf6 implements hg6<xe6<? super rx6<ApiResponse<ku3>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$response$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, xe6 xe6) {
        super(1, xe6);
        this.this$0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new URLRequestTaskHelper$execute$response$Anon2(this.this$0, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((URLRequestTaskHelper$execute$response$Anon2) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 mApiService = this.this$0.getMApiService();
            String serialNumber$app_fossilRelease = this.this$0.getSerialNumber$app_fossilRelease();
            if (serialNumber$app_fossilRelease != null) {
                String feature$app_fossilRelease = this.this$0.getFeature$app_fossilRelease();
                if (feature$app_fossilRelease != null) {
                    String access$getResolution$p = this.this$0.resolution;
                    if (access$getResolution$p != null) {
                        this.label = 1;
                        obj = ApiServiceV2.DefaultImpls.getDeviceAssets$default(mApiService, 20, 0, serialNumber$app_fossilRelease, feature$app_fossilRelease, access$getResolution$p, "ANDROID", (String) null, this, 64, (Object) null);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
