package com.portfolio.platform.cloudimage;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$1", f = "CloudImageRunnable.kt", l = {75}, m = "invokeSuspend")
public final class CloudImageRunnable$downloadingDeviceAssets$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public /* final */ /* synthetic */ String $feature;
    @DexIgnore
    public /* final */ /* synthetic */ String $zipFilePath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$downloadingDeviceAssets$Anon1(CloudImageRunnable cloudImageRunnable, String str, String str2, String str3, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cloudImageRunnable;
        this.$zipFilePath = str;
        this.$destinationUnzipPath = str2;
        this.$feature = str3;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        CloudImageRunnable$downloadingDeviceAssets$Anon1 cloudImageRunnable$downloadingDeviceAssets$Anon1 = new CloudImageRunnable$downloadingDeviceAssets$Anon1(this.this$0, this.$zipFilePath, this.$destinationUnzipPath, this.$feature, xe6);
        cloudImageRunnable$downloadingDeviceAssets$Anon1.p$ = (il6) obj;
        return cloudImageRunnable$downloadingDeviceAssets$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CloudImageRunnable$downloadingDeviceAssets$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            URLRequestTaskHelper access$prepareURLRequestTask = this.this$0.prepareURLRequestTask();
            URLRequestTaskHelper.init$default(access$prepareURLRequestTask, this.$zipFilePath, this.$destinationUnzipPath, CloudImageRunnable.access$getSerialNumber$p(this.this$0), this.$feature, CloudImageRunnable.access$getResolution$p(this.this$0), (String) null, 32, (Object) null);
            this.L$0 = il6;
            this.L$1 = access$prepareURLRequestTask;
            this.label = 1;
            if (access$prepareURLRequestTask.execute(this) == a) {
                return a;
            }
        } else if (i == 1) {
            URLRequestTaskHelper uRLRequestTaskHelper = (URLRequestTaskHelper) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
