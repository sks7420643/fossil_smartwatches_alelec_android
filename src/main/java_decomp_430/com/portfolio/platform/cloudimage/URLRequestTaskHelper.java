package com.portfolio.platform.cloudimage;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class URLRequestTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + URLRequestTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public String fastPairId;
    @DexIgnore
    public String feature;
    @DexIgnore
    public OnNextTaskListener listener;
    @DexIgnore
    public ApiServiceV2 mApiService;
    @DexIgnore
    public String resolution;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return URLRequestTaskHelper.TAG;
        }

        @DexIgnore
        public final URLRequestTaskHelper newInstance() {
            return new URLRequestTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface OnNextTaskListener {
        @DexIgnore
        void downloadFile(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        void onGetDeviceAssetFailed();
    }

    @DexIgnore
    public URLRequestTaskHelper() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public static /* synthetic */ void init$default(URLRequestTaskHelper uRLRequestTaskHelper, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        if ((i & 32) != 0) {
            str6 = "";
        }
        uRLRequestTaskHelper.init(str, str2, str3, str4, str5, str6);
    }

    @DexIgnore
    public static final URLRequestTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object execute(xe6<? super cd6> xe6) {
        URLRequestTaskHelper$execute$Anon1 uRLRequestTaskHelper$execute$Anon1;
        int i;
        URLRequestTaskHelper uRLRequestTaskHelper;
        ap4 ap4;
        if (xe6 instanceof URLRequestTaskHelper$execute$Anon1) {
            uRLRequestTaskHelper$execute$Anon1 = (URLRequestTaskHelper$execute$Anon1) xe6;
            int i2 = uRLRequestTaskHelper$execute$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                uRLRequestTaskHelper$execute$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = uRLRequestTaskHelper$execute$Anon1.result;
                Object a = ff6.a();
                i = uRLRequestTaskHelper$execute$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "execute() called with serialNumber = [" + this.serialNumber + "], feature = [" + this.feature + "], fastPairId = [" + this.fastPairId + ']');
                    String str3 = this.serialNumber;
                    if (str3 == null || str3.length() == 0) {
                        URLRequestTaskHelper$execute$response$Anon1 uRLRequestTaskHelper$execute$response$Anon1 = new URLRequestTaskHelper$execute$response$Anon1(this, (xe6) null);
                        uRLRequestTaskHelper$execute$Anon1.L$0 = this;
                        uRLRequestTaskHelper$execute$Anon1.label = 1;
                        obj = ResponseKt.a(uRLRequestTaskHelper$execute$response$Anon1, uRLRequestTaskHelper$execute$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        URLRequestTaskHelper$execute$response$Anon2 uRLRequestTaskHelper$execute$response$Anon2 = new URLRequestTaskHelper$execute$response$Anon2(this, (xe6) null);
                        uRLRequestTaskHelper$execute$Anon1.L$0 = this;
                        uRLRequestTaskHelper$execute$Anon1.label = 2;
                        obj = ResponseKt.a(uRLRequestTaskHelper$execute$response$Anon2, uRLRequestTaskHelper$execute$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    }
                    uRLRequestTaskHelper = this;
                } else if (i == 1 || i == 2) {
                    uRLRequestTaskHelper = (URLRequestTaskHelper) uRLRequestTaskHelper$execute$Anon1.L$0;
                    nc6.a(obj);
                } else if (i == 3) {
                    AssetsDeviceResponse assetsDeviceResponse = (AssetsDeviceResponse) uRLRequestTaskHelper$execute$Anon1.L$2;
                    ap4 ap42 = (ap4) uRLRequestTaskHelper$execute$Anon1.L$1;
                    URLRequestTaskHelper uRLRequestTaskHelper2 = (URLRequestTaskHelper) uRLRequestTaskHelper$execute$Anon1.L$0;
                    nc6.a(obj);
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 == null) {
                        wg6.a();
                        throw null;
                    } else if (!((ApiResponse) a2).get_items().isEmpty()) {
                        AssetsDeviceResponse assetsDeviceResponse2 = (AssetsDeviceResponse) new Gson().a((JsonElement) ((ApiResponse) cp4.a()).get_items().get(0), AssetsDeviceResponse.class);
                        AssetUtil assetUtil = AssetUtil.INSTANCE;
                        String str4 = uRLRequestTaskHelper.zipFilePath;
                        if (str4 != null) {
                            if (assetUtil.checkFileExist(str4)) {
                                ChecksumUtil checksumUtil = ChecksumUtil.INSTANCE;
                                String str5 = uRLRequestTaskHelper.zipFilePath;
                                if (str5 != null) {
                                    Metadata metadata = assetsDeviceResponse2.getMetadata();
                                    if (checksumUtil.verifyDownloadFile(str5, metadata != null ? metadata.getChecksum() : null)) {
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String str6 = TAG;
                                        local2.d(str6, "onSuccess: The assets with serialNumber = [" + uRLRequestTaskHelper.serialNumber + "] for feature =[" + uRLRequestTaskHelper.feature + "] is the latest, no need to download!");
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            FLogger.INSTANCE.getLocal().d(TAG, "onSuccess: need to download new asset for this");
                            cn6 c = zl6.c();
                            URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(uRLRequestTaskHelper, assetsDeviceResponse2, (xe6) null);
                            uRLRequestTaskHelper$execute$Anon1.L$0 = uRLRequestTaskHelper;
                            uRLRequestTaskHelper$execute$Anon1.L$1 = ap4;
                            uRLRequestTaskHelper$execute$Anon1.L$2 = assetsDeviceResponse2;
                            uRLRequestTaskHelper$execute$Anon1.label = 3;
                            if (gk6.a(c, uRLRequestTaskHelper$execute$Anon2, uRLRequestTaskHelper$execute$Anon1) == a) {
                                return a;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onFail: serialNumber = [");
                    sb.append(uRLRequestTaskHelper.serialNumber);
                    sb.append("], feature = [");
                    sb.append(uRLRequestTaskHelper.feature);
                    sb.append("], fastPairId = [");
                    sb.append(uRLRequestTaskHelper.fastPairId);
                    sb.append("], error = [");
                    Throwable d = ((zo4) ap4).d();
                    if (d != null) {
                        str = d.getMessage();
                    }
                    sb.append(str);
                    sb.append("]");
                    local3.e(str7, sb.toString());
                    OnNextTaskListener onNextTaskListener = uRLRequestTaskHelper.listener;
                    if (onNextTaskListener != null) {
                        onNextTaskListener.onGetDeviceAssetFailed();
                    }
                }
                return cd6.a;
            }
        }
        uRLRequestTaskHelper$execute$Anon1 = new URLRequestTaskHelper$execute$Anon1(this, xe6);
        Object obj2 = uRLRequestTaskHelper$execute$Anon1.result;
        Object a3 = ff6.a();
        i = uRLRequestTaskHelper$execute$Anon1.label;
        String str8 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final String getDestinationUnzipPath$app_fossilRelease() {
        return this.destinationUnzipPath;
    }

    @DexIgnore
    public final String getFastPairId$app_fossilRelease() {
        return this.fastPairId;
    }

    @DexIgnore
    public final String getFeature$app_fossilRelease() {
        return this.feature;
    }

    @DexIgnore
    public final OnNextTaskListener getListener$app_fossilRelease() {
        return this.listener;
    }

    @DexIgnore
    public final ApiServiceV2 getMApiService() {
        ApiServiceV2 apiServiceV2 = this.mApiService;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        wg6.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final String getSerialNumber$app_fossilRelease() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getZipFilePath$app_fossilRelease() {
        return this.zipFilePath;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4, String str5, String str6) {
        wg6.b(str, "zipFilePath");
        wg6.b(str2, "destinationUnzipPath");
        wg6.b(str3, "serialNumber");
        wg6.b(str4, "feature");
        wg6.b(str5, "resolution");
        wg6.b(str6, "fastPairId");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.serialNumber = str3;
        this.feature = str4;
        this.resolution = str5;
        this.fastPairId = str6;
    }

    @DexIgnore
    public final void setDestinationUnzipPath$app_fossilRelease(String str) {
        this.destinationUnzipPath = str;
    }

    @DexIgnore
    public final void setFastPairId$app_fossilRelease(String str) {
        this.fastPairId = str;
    }

    @DexIgnore
    public final void setFeature$app_fossilRelease(String str) {
        this.feature = str;
    }

    @DexIgnore
    public final void setListener$app_fossilRelease(OnNextTaskListener onNextTaskListener) {
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setMApiService(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "<set-?>");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final void setOnNextTaskListener(OnNextTaskListener onNextTaskListener) {
        wg6.b(onNextTaskListener, "listener");
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setSerialNumber$app_fossilRelease(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setZipFilePath$app_fossilRelease(String str) {
        this.zipFilePath = str;
    }
}
