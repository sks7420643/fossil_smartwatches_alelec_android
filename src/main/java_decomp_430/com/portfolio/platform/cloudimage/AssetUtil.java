package com.portfolio.platform.cloudimage;

import com.fossil.cn6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.nc6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AssetUtil {
    @DexIgnore
    public static /* final */ AssetUtil INSTANCE; // = new AssetUtil();
    @DexIgnore
    public static /* final */ String TAG; // = "AssetUtil";

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0210  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x02cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0036  */
    public final Object checkAssetExist(File file, String str, String str2, String str3, String str4, String str5, CloudImageHelper.OnImageCallbackListener onImageCallbackListener, xe6<? super Boolean> xe6) {
        AssetUtil$checkAssetExist$Anon1 assetUtil$checkAssetExist$Anon1;
        int i;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        boolean z;
        File file2;
        String str11;
        String str12;
        String str13;
        CloudImageHelper.OnImageCallbackListener onImageCallbackListener2;
        String str14;
        String str15;
        AssetUtil assetUtil;
        boolean booleanValue;
        String str16;
        String str17;
        String str18;
        CloudImageHelper.OnImageCallbackListener onImageCallbackListener3;
        String str19;
        String str20;
        AssetUtil assetUtil2;
        Object obj;
        boolean booleanValue2;
        String str21 = str2;
        String str22 = str3;
        String str23 = str4;
        String str24 = str5;
        xe6<? super Boolean> xe62 = xe6;
        if (xe62 instanceof AssetUtil$checkAssetExist$Anon1) {
            assetUtil$checkAssetExist$Anon1 = (AssetUtil$checkAssetExist$Anon1) xe62;
            int i2 = assetUtil$checkAssetExist$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                assetUtil$checkAssetExist$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = assetUtil$checkAssetExist$Anon1.result;
                Object a = ff6.a();
                i = assetUtil$checkAssetExist$Anon1.label;
                if (i != 0) {
                    nc6.a(obj2);
                    String str25 = file.getAbsolutePath() + '/' + str21 + '-' + str22 + '-' + str23;
                    String str26 = str25 + '/' + str24 + ".webp";
                    dl6 b = zl6.b();
                    AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 assetUtil$checkAssetExist$isFilePath1Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(str26, (xe6) null);
                    assetUtil$checkAssetExist$Anon1.L$0 = this;
                    file2 = file;
                    assetUtil$checkAssetExist$Anon1.L$1 = file2;
                    String str27 = str;
                    assetUtil$checkAssetExist$Anon1.L$2 = str27;
                    assetUtil$checkAssetExist$Anon1.L$3 = str21;
                    assetUtil$checkAssetExist$Anon1.L$4 = str22;
                    assetUtil$checkAssetExist$Anon1.L$5 = str23;
                    assetUtil$checkAssetExist$Anon1.L$6 = str24;
                    onImageCallbackListener3 = onImageCallbackListener;
                    assetUtil$checkAssetExist$Anon1.L$7 = onImageCallbackListener3;
                    assetUtil$checkAssetExist$Anon1.L$8 = str25;
                    assetUtil$checkAssetExist$Anon1.L$9 = str26;
                    assetUtil$checkAssetExist$Anon1.label = 1;
                    obj = gk6.a(b, assetUtil$checkAssetExist$isFilePath1Exist$Anon1, assetUtil$checkAssetExist$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    str12 = str21;
                    str11 = str24;
                    str14 = str25;
                    assetUtil2 = this;
                    String str28 = str27;
                    str10 = str22;
                    str8 = str28;
                    String str29 = str26;
                    str19 = str23;
                    str20 = str29;
                } else if (i == 1) {
                    str19 = (String) assetUtil$checkAssetExist$Anon1.L$5;
                    file2 = (File) assetUtil$checkAssetExist$Anon1.L$1;
                    nc6.a(obj2);
                    obj = obj2;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener4 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$7;
                    str20 = (String) assetUtil$checkAssetExist$Anon1.L$9;
                    assetUtil2 = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$0;
                    str10 = (String) assetUtil$checkAssetExist$Anon1.L$4;
                    str12 = (String) assetUtil$checkAssetExist$Anon1.L$3;
                    onImageCallbackListener3 = onImageCallbackListener4;
                    String str30 = (String) assetUtil$checkAssetExist$Anon1.L$6;
                    str14 = (String) assetUtil$checkAssetExist$Anon1.L$8;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$2;
                    str11 = str30;
                } else if (i == 2) {
                    boolean z2 = assetUtil$checkAssetExist$Anon1.Z$0;
                    str18 = (String) assetUtil$checkAssetExist$Anon1.L$9;
                    String str31 = (String) assetUtil$checkAssetExist$Anon1.L$8;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener5 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$7;
                    String str32 = (String) assetUtil$checkAssetExist$Anon1.L$6;
                    String str33 = (String) assetUtil$checkAssetExist$Anon1.L$5;
                    String str34 = (String) assetUtil$checkAssetExist$Anon1.L$4;
                    String str35 = (String) assetUtil$checkAssetExist$Anon1.L$3;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$2;
                    File file3 = (File) assetUtil$checkAssetExist$Anon1.L$1;
                    AssetUtil assetUtil3 = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$0;
                    nc6.a(obj2);
                    str16 = " serialNumber=";
                    str17 = TAG;
                    FLogger.INSTANCE.getLocal().d(str17, "filePath1=" + str18 + str16 + str8);
                    return hf6.a(true);
                } else if (i == 3) {
                    boolean z3 = assetUtil$checkAssetExist$Anon1.Z$0;
                    str14 = (String) assetUtil$checkAssetExist$Anon1.L$8;
                    onImageCallbackListener2 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$7;
                    str11 = (String) assetUtil$checkAssetExist$Anon1.L$6;
                    str10 = (String) assetUtil$checkAssetExist$Anon1.L$4;
                    str12 = (String) assetUtil$checkAssetExist$Anon1.L$3;
                    String str36 = (String) assetUtil$checkAssetExist$Anon1.L$10;
                    File file4 = (File) assetUtil$checkAssetExist$Anon1.L$1;
                    nc6.a(obj2);
                    assetUtil = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$0;
                    str6 = " serialNumber=";
                    str7 = TAG;
                    str9 = str36;
                    z = z3;
                    str13 = (String) assetUtil$checkAssetExist$Anon1.L$9;
                    str15 = (String) assetUtil$checkAssetExist$Anon1.L$5;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$2;
                    file2 = file4;
                    booleanValue = ((Boolean) obj2).booleanValue();
                    if (!booleanValue) {
                        Object obj3 = a;
                        AssetUtil$checkAssetExist$Anon3 assetUtil$checkAssetExist$Anon3 = new AssetUtil$checkAssetExist$Anon3(onImageCallbackListener2, str8, str9, (xe6) null);
                        assetUtil$checkAssetExist$Anon1.L$0 = assetUtil;
                        assetUtil$checkAssetExist$Anon1.L$1 = file2;
                        assetUtil$checkAssetExist$Anon1.L$2 = str8;
                        assetUtil$checkAssetExist$Anon1.L$3 = str12;
                        assetUtil$checkAssetExist$Anon1.L$4 = str10;
                        assetUtil$checkAssetExist$Anon1.L$5 = str15;
                        assetUtil$checkAssetExist$Anon1.L$6 = str11;
                        assetUtil$checkAssetExist$Anon1.L$7 = onImageCallbackListener2;
                        assetUtil$checkAssetExist$Anon1.L$8 = str14;
                        assetUtil$checkAssetExist$Anon1.L$9 = str13;
                        assetUtil$checkAssetExist$Anon1.Z$0 = z;
                        assetUtil$checkAssetExist$Anon1.L$10 = str9;
                        assetUtil$checkAssetExist$Anon1.Z$1 = booleanValue;
                        assetUtil$checkAssetExist$Anon1.label = 4;
                        Object obj4 = obj3;
                        if (gk6.a(zl6.c(), assetUtil$checkAssetExist$Anon3, assetUtil$checkAssetExist$Anon1) == obj4) {
                            return obj4;
                        }
                        FLogger.INSTANCE.getLocal().d(str7, "filePath2=" + str9 + str6 + str8);
                        return hf6.a(true);
                    }
                    FLogger.INSTANCE.getLocal().d(str7, "file is not exist serialNumber = " + str8);
                    return hf6.a(false);
                } else if (i == 4) {
                    boolean z4 = assetUtil$checkAssetExist$Anon1.Z$1;
                    str9 = (String) assetUtil$checkAssetExist$Anon1.L$10;
                    boolean z5 = assetUtil$checkAssetExist$Anon1.Z$0;
                    String str37 = (String) assetUtil$checkAssetExist$Anon1.L$9;
                    String str38 = (String) assetUtil$checkAssetExist$Anon1.L$8;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener6 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkAssetExist$Anon1.L$7;
                    String str39 = (String) assetUtil$checkAssetExist$Anon1.L$6;
                    String str40 = (String) assetUtil$checkAssetExist$Anon1.L$5;
                    String str41 = (String) assetUtil$checkAssetExist$Anon1.L$4;
                    String str42 = (String) assetUtil$checkAssetExist$Anon1.L$3;
                    str8 = (String) assetUtil$checkAssetExist$Anon1.L$2;
                    File file5 = (File) assetUtil$checkAssetExist$Anon1.L$1;
                    AssetUtil assetUtil4 = (AssetUtil) assetUtil$checkAssetExist$Anon1.L$0;
                    nc6.a(obj2);
                    str6 = " serialNumber=";
                    str7 = TAG;
                    FLogger.INSTANCE.getLocal().d(str7, "filePath2=" + str9 + str6 + str8);
                    return hf6.a(true);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                booleanValue2 = ((Boolean) obj).booleanValue();
                if (!booleanValue2) {
                    cn6 c = zl6.c();
                    str17 = TAG;
                    str16 = " serialNumber=";
                    AssetUtil$checkAssetExist$Anon2 assetUtil$checkAssetExist$Anon2 = new AssetUtil$checkAssetExist$Anon2(onImageCallbackListener3, str8, str20, (xe6) null);
                    assetUtil$checkAssetExist$Anon1.L$0 = assetUtil2;
                    assetUtil$checkAssetExist$Anon1.L$1 = file2;
                    assetUtil$checkAssetExist$Anon1.L$2 = str8;
                    assetUtil$checkAssetExist$Anon1.L$3 = str12;
                    assetUtil$checkAssetExist$Anon1.L$4 = str10;
                    assetUtil$checkAssetExist$Anon1.L$5 = str19;
                    assetUtil$checkAssetExist$Anon1.L$6 = str11;
                    assetUtil$checkAssetExist$Anon1.L$7 = onImageCallbackListener3;
                    assetUtil$checkAssetExist$Anon1.L$8 = str14;
                    assetUtil$checkAssetExist$Anon1.L$9 = str20;
                    assetUtil$checkAssetExist$Anon1.Z$0 = booleanValue2;
                    assetUtil$checkAssetExist$Anon1.label = 2;
                    if (gk6.a(c, assetUtil$checkAssetExist$Anon2, assetUtil$checkAssetExist$Anon1) == a) {
                        return a;
                    }
                    str18 = str20;
                    FLogger.INSTANCE.getLocal().d(str17, "filePath1=" + str18 + str16 + str8);
                    return hf6.a(true);
                }
                String str43 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append(str14);
                str7 = str43;
                sb.append('/');
                sb.append(str11);
                sb.append(".png");
                String sb2 = sb.toString();
                dl6 b2 = zl6.b();
                str6 = " serialNumber=";
                AssetUtil$checkAssetExist$isFilePath2Exist$Anon1 assetUtil$checkAssetExist$isFilePath2Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath2Exist$Anon1(sb2, (xe6) null);
                assetUtil$checkAssetExist$Anon1.L$0 = assetUtil2;
                assetUtil$checkAssetExist$Anon1.L$1 = file2;
                assetUtil$checkAssetExist$Anon1.L$2 = str8;
                assetUtil$checkAssetExist$Anon1.L$3 = str12;
                assetUtil$checkAssetExist$Anon1.L$4 = str10;
                assetUtil$checkAssetExist$Anon1.L$5 = str19;
                assetUtil$checkAssetExist$Anon1.L$6 = str11;
                assetUtil$checkAssetExist$Anon1.L$7 = onImageCallbackListener3;
                assetUtil$checkAssetExist$Anon1.L$8 = str14;
                assetUtil$checkAssetExist$Anon1.L$9 = str20;
                assetUtil$checkAssetExist$Anon1.Z$0 = booleanValue2;
                assetUtil$checkAssetExist$Anon1.L$10 = sb2;
                assetUtil$checkAssetExist$Anon1.label = 3;
                obj2 = gk6.a(b2, assetUtil$checkAssetExist$isFilePath2Exist$Anon1, assetUtil$checkAssetExist$Anon1);
                a = a;
                if (obj2 == a) {
                    return a;
                }
                z = booleanValue2;
                assetUtil = assetUtil2;
                str9 = sb2;
                str13 = str20;
                str15 = str19;
                onImageCallbackListener2 = onImageCallbackListener3;
                booleanValue = ((Boolean) obj2).booleanValue();
                if (!booleanValue) {
                }
            }
        }
        assetUtil$checkAssetExist$Anon1 = new AssetUtil$checkAssetExist$Anon1(this, xe62);
        Object obj22 = assetUtil$checkAssetExist$Anon1.result;
        Object a2 = ff6.a();
        i = assetUtil$checkAssetExist$Anon1.label;
        if (i != 0) {
        }
        booleanValue2 = ((Boolean) obj).booleanValue();
        if (!booleanValue2) {
        }
    }

    @DexIgnore
    public final boolean checkFileExist(String str) {
        wg6.b(str, "filePath");
        return new File(str).exists();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x020a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0034  */
    public final Object checkWearOSAssetExist(File file, String str, String str2, CloudImageHelper.OnImageCallbackListener onImageCallbackListener, xe6<? super Boolean> xe6) {
        AssetUtil$checkWearOSAssetExist$Anon1 assetUtil$checkWearOSAssetExist$Anon1;
        int i;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        File file2;
        AssetUtil assetUtil;
        CloudImageHelper.OnImageCallbackListener onImageCallbackListener2;
        String str8;
        String str9;
        String str10;
        boolean z;
        boolean booleanValue;
        String str11;
        String str12;
        boolean booleanValue2;
        String str13 = str;
        String str14 = str2;
        xe6<? super Boolean> xe62 = xe6;
        if (xe62 instanceof AssetUtil$checkWearOSAssetExist$Anon1) {
            assetUtil$checkWearOSAssetExist$Anon1 = (AssetUtil$checkWearOSAssetExist$Anon1) xe62;
            int i2 = assetUtil$checkWearOSAssetExist$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                assetUtil$checkWearOSAssetExist$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = assetUtil$checkWearOSAssetExist$Anon1.result;
                Object a = ff6.a();
                i = assetUtil$checkWearOSAssetExist$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    String str15 = file.getAbsolutePath() + '/' + str13 + '-' + str14;
                    String str16 = str15 + '/' + "WearOS" + ".webp";
                    dl6 b = zl6.b();
                    AssetUtil$checkWearOSAssetExist$isFilePath1Exist$Anon1 assetUtil$checkWearOSAssetExist$isFilePath1Exist$Anon1 = new AssetUtil$checkWearOSAssetExist$isFilePath1Exist$Anon1(str16, (xe6) null);
                    assetUtil$checkWearOSAssetExist$Anon1.L$0 = this;
                    file2 = file;
                    assetUtil$checkWearOSAssetExist$Anon1.L$1 = file2;
                    assetUtil$checkWearOSAssetExist$Anon1.L$2 = str13;
                    assetUtil$checkWearOSAssetExist$Anon1.L$3 = str14;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener3 = onImageCallbackListener;
                    assetUtil$checkWearOSAssetExist$Anon1.L$4 = onImageCallbackListener3;
                    assetUtil$checkWearOSAssetExist$Anon1.L$5 = "WearOS";
                    assetUtil$checkWearOSAssetExist$Anon1.L$6 = str15;
                    assetUtil$checkWearOSAssetExist$Anon1.L$7 = str16;
                    assetUtil$checkWearOSAssetExist$Anon1.label = 1;
                    Object a2 = gk6.a(b, assetUtil$checkWearOSAssetExist$isFilePath1Exist$Anon1, assetUtil$checkWearOSAssetExist$Anon1);
                    if (a2 == a) {
                        return a;
                    }
                    assetUtil = this;
                    onImageCallbackListener2 = onImageCallbackListener3;
                    str7 = str14;
                    str5 = str13;
                    str11 = str16;
                    String str17 = str15;
                    str12 = "WearOS";
                    obj = a2;
                    str9 = str17;
                } else if (i == 1) {
                    str11 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$7;
                    str12 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$5;
                    nc6.a(obj);
                    onImageCallbackListener2 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkWearOSAssetExist$Anon1.L$4;
                    AssetUtil assetUtil2 = (AssetUtil) assetUtil$checkWearOSAssetExist$Anon1.L$0;
                    str9 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$6;
                    str5 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$2;
                    file2 = (File) assetUtil$checkWearOSAssetExist$Anon1.L$1;
                    str7 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$3;
                    assetUtil = assetUtil2;
                } else if (i == 2) {
                    boolean z2 = assetUtil$checkWearOSAssetExist$Anon1.Z$0;
                    str11 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$7;
                    String str18 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$6;
                    String str19 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$5;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener4 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkWearOSAssetExist$Anon1.L$4;
                    String str20 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$3;
                    str5 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$2;
                    File file3 = (File) assetUtil$checkWearOSAssetExist$Anon1.L$1;
                    AssetUtil assetUtil3 = (AssetUtil) assetUtil$checkWearOSAssetExist$Anon1.L$0;
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "filePath1=" + str11 + " fastPairId=" + str5);
                    return hf6.a(true);
                } else if (i == 3) {
                    str6 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$8;
                    boolean z3 = assetUtil$checkWearOSAssetExist$Anon1.Z$0;
                    str10 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$7;
                    str9 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$6;
                    str8 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$5;
                    onImageCallbackListener2 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkWearOSAssetExist$Anon1.L$4;
                    str7 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$3;
                    file2 = (File) assetUtil$checkWearOSAssetExist$Anon1.L$1;
                    assetUtil = (AssetUtil) assetUtil$checkWearOSAssetExist$Anon1.L$0;
                    nc6.a(obj);
                    z = z3;
                    str5 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$2;
                    str3 = TAG;
                    booleanValue = ((Boolean) obj).booleanValue();
                    if (!booleanValue) {
                        cn6 c = zl6.c();
                        str4 = " fastPairId=";
                        AssetUtil$checkWearOSAssetExist$Anon3 assetUtil$checkWearOSAssetExist$Anon3 = new AssetUtil$checkWearOSAssetExist$Anon3(onImageCallbackListener2, str5, str6, (xe6) null);
                        assetUtil$checkWearOSAssetExist$Anon1.L$0 = assetUtil;
                        assetUtil$checkWearOSAssetExist$Anon1.L$1 = file2;
                        assetUtil$checkWearOSAssetExist$Anon1.L$2 = str5;
                        assetUtil$checkWearOSAssetExist$Anon1.L$3 = str7;
                        assetUtil$checkWearOSAssetExist$Anon1.L$4 = onImageCallbackListener2;
                        assetUtil$checkWearOSAssetExist$Anon1.L$5 = str8;
                        assetUtil$checkWearOSAssetExist$Anon1.L$6 = str9;
                        assetUtil$checkWearOSAssetExist$Anon1.L$7 = str10;
                        assetUtil$checkWearOSAssetExist$Anon1.Z$0 = z;
                        assetUtil$checkWearOSAssetExist$Anon1.L$8 = str6;
                        assetUtil$checkWearOSAssetExist$Anon1.Z$1 = booleanValue;
                        assetUtil$checkWearOSAssetExist$Anon1.label = 4;
                        if (gk6.a(c, assetUtil$checkWearOSAssetExist$Anon3, assetUtil$checkWearOSAssetExist$Anon1) == a) {
                            return a;
                        }
                        FLogger.INSTANCE.getLocal().d(str3, "filePath2=" + str6 + str4 + str5);
                        return hf6.a(true);
                    }
                    FLogger.INSTANCE.getLocal().d(str3, "file is not exist fastPairId = " + str5);
                    return hf6.a(false);
                } else if (i == 4) {
                    boolean z4 = assetUtil$checkWearOSAssetExist$Anon1.Z$1;
                    str6 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$8;
                    boolean z5 = assetUtil$checkWearOSAssetExist$Anon1.Z$0;
                    String str21 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$7;
                    String str22 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$6;
                    String str23 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$5;
                    CloudImageHelper.OnImageCallbackListener onImageCallbackListener5 = (CloudImageHelper.OnImageCallbackListener) assetUtil$checkWearOSAssetExist$Anon1.L$4;
                    String str24 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$3;
                    str5 = (String) assetUtil$checkWearOSAssetExist$Anon1.L$2;
                    File file4 = (File) assetUtil$checkWearOSAssetExist$Anon1.L$1;
                    AssetUtil assetUtil4 = (AssetUtil) assetUtil$checkWearOSAssetExist$Anon1.L$0;
                    nc6.a(obj);
                    str4 = " fastPairId=";
                    str3 = TAG;
                    FLogger.INSTANCE.getLocal().d(str3, "filePath2=" + str6 + str4 + str5);
                    return hf6.a(true);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                booleanValue2 = ((Boolean) obj).booleanValue();
                if (!booleanValue2) {
                    cn6 c2 = zl6.c();
                    AssetUtil$checkWearOSAssetExist$Anon2 assetUtil$checkWearOSAssetExist$Anon2 = new AssetUtil$checkWearOSAssetExist$Anon2(onImageCallbackListener2, str5, str11, (xe6) null);
                    assetUtil$checkWearOSAssetExist$Anon1.L$0 = assetUtil;
                    assetUtil$checkWearOSAssetExist$Anon1.L$1 = file2;
                    assetUtil$checkWearOSAssetExist$Anon1.L$2 = str5;
                    assetUtil$checkWearOSAssetExist$Anon1.L$3 = str7;
                    assetUtil$checkWearOSAssetExist$Anon1.L$4 = onImageCallbackListener2;
                    assetUtil$checkWearOSAssetExist$Anon1.L$5 = str12;
                    assetUtil$checkWearOSAssetExist$Anon1.L$6 = str9;
                    assetUtil$checkWearOSAssetExist$Anon1.L$7 = str11;
                    assetUtil$checkWearOSAssetExist$Anon1.Z$0 = booleanValue2;
                    assetUtil$checkWearOSAssetExist$Anon1.label = 2;
                    if (gk6.a(c2, assetUtil$checkWearOSAssetExist$Anon2, assetUtil$checkWearOSAssetExist$Anon1) == a) {
                        return a;
                    }
                    FLogger.INSTANCE.getLocal().d(TAG, "filePath1=" + str11 + " fastPairId=" + str5);
                    return hf6.a(true);
                }
                String str25 = str9 + '/' + str12 + ".png";
                dl6 b2 = zl6.b();
                str3 = TAG;
                AssetUtil$checkWearOSAssetExist$isFilePath2Exist$Anon1 assetUtil$checkWearOSAssetExist$isFilePath2Exist$Anon1 = new AssetUtil$checkWearOSAssetExist$isFilePath2Exist$Anon1(str25, (xe6) null);
                assetUtil$checkWearOSAssetExist$Anon1.L$0 = assetUtil;
                assetUtil$checkWearOSAssetExist$Anon1.L$1 = file2;
                assetUtil$checkWearOSAssetExist$Anon1.L$2 = str5;
                assetUtil$checkWearOSAssetExist$Anon1.L$3 = str7;
                assetUtil$checkWearOSAssetExist$Anon1.L$4 = onImageCallbackListener2;
                assetUtil$checkWearOSAssetExist$Anon1.L$5 = str12;
                assetUtil$checkWearOSAssetExist$Anon1.L$6 = str9;
                assetUtil$checkWearOSAssetExist$Anon1.L$7 = str11;
                assetUtil$checkWearOSAssetExist$Anon1.Z$0 = booleanValue2;
                assetUtil$checkWearOSAssetExist$Anon1.L$8 = str25;
                assetUtil$checkWearOSAssetExist$Anon1.label = 3;
                Object a3 = gk6.a(b2, assetUtil$checkWearOSAssetExist$isFilePath2Exist$Anon1, assetUtil$checkWearOSAssetExist$Anon1);
                if (a3 == a) {
                    return a;
                }
                str8 = str12;
                str10 = str11;
                str6 = str25;
                z = booleanValue2;
                obj = a3;
                booleanValue = ((Boolean) obj).booleanValue();
                if (!booleanValue) {
                }
            }
        }
        assetUtil$checkWearOSAssetExist$Anon1 = new AssetUtil$checkWearOSAssetExist$Anon1(this, xe62);
        Object obj2 = assetUtil$checkWearOSAssetExist$Anon1.result;
        Object a4 = ff6.a();
        i = assetUtil$checkWearOSAssetExist$Anon1.label;
        if (i != 0) {
        }
        booleanValue2 = ((Boolean) obj2).booleanValue();
        if (!booleanValue2) {
        }
    }
}
