package com.portfolio.platform.gson;

import com.fossil.bk4;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.ku3;
import com.fossil.wg6;
import com.fossil.yi4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridRecommendPresetDeserializer implements hu3<HybridRecommendPreset> {
    @DexIgnore
    public HybridRecommendPreset deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        String str;
        String str2;
        String str3;
        Iterator it;
        String str4;
        boolean z;
        String str5;
        String str6;
        if (jsonElement != null) {
            ku3 d = jsonElement.d();
            JsonElement a = d.a("name");
            wg6.a((Object) a, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = a.f();
            JsonElement a2 = d.a("id");
            wg6.a((Object) a2, "jsonObject.get(\"id\")");
            String f2 = a2.f();
            String str7 = "";
            if (d.d("serialNumber")) {
                JsonElement a3 = d.a("serialNumber");
                wg6.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
                str = a3.f();
            } else {
                str = str7;
            }
            JsonElement a4 = d.a("isDefault");
            wg6.a((Object) a4, "jsonObject.get(Constants.JSON_KEY_IS_DEFAULT)");
            boolean a5 = a4.a();
            String str8 = "updatedAt";
            JsonElement a6 = d.a(str8);
            wg6.a((Object) a6, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a6.f();
            JsonElement a7 = d.a("createdAt");
            wg6.a((Object) a7, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a7.f();
            ArrayList arrayList = new ArrayList();
            if (gu3 != null) {
                Iterator it2 = d.b("buttons").iterator();
                while (it2.hasNext()) {
                    JsonElement jsonElement2 = (JsonElement) it2.next();
                    wg6.a((Object) jsonElement2, "item");
                    ku3 d2 = jsonElement2.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a8 = d2.a("buttonPosition");
                        wg6.a((Object) a8, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str2 = a8.f();
                    } else {
                        str2 = str7;
                    }
                    if (d2.d("appId")) {
                        it = it2;
                        JsonElement a9 = d2.a("appId");
                        str3 = str7;
                        wg6.a((Object) a9, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str7 = a9.f();
                    } else {
                        it = it2;
                        str3 = str7;
                    }
                    if (d2.d("localUpdatedAt")) {
                        z = a5;
                        JsonElement a10 = d2.a("localUpdatedAt");
                        str4 = f;
                        wg6.a((Object) a10, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str5 = a10.f();
                    } else {
                        str4 = f;
                        z = a5;
                        Calendar instance = Calendar.getInstance();
                        wg6.a((Object) instance, "Calendar.getInstance()");
                        str5 = bk4.u(instance.getTime());
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        JsonElement a11 = d2.a(Constants.USER_SETTING);
                        wg6.a((Object) a11, "settingJsonObject");
                        if (!a11.h()) {
                            str6 = yi4.a(a11.d());
                            wg6.a((Object) str2, "position");
                            wg6.a((Object) str7, "appId");
                            wg6.a((Object) str5, "localUpdatedAt");
                            arrayList.add(new HybridPresetAppSetting(str2, str7, str5, str6));
                            it2 = it;
                            str7 = str3;
                            a5 = z;
                            f = str4;
                            str8 = str8;
                        }
                    }
                    str6 = str3;
                    wg6.a((Object) str2, "position");
                    wg6.a((Object) str7, "appId");
                    wg6.a((Object) str5, "localUpdatedAt");
                    arrayList.add(new HybridPresetAppSetting(str2, str7, str5, str6));
                    it2 = it;
                    str7 = str3;
                    a5 = z;
                    f = str4;
                    str8 = str8;
                }
            }
            wg6.a((Object) f2, "id");
            wg6.a((Object) str, "serialNumber");
            wg6.a((Object) f4, "createdAt");
            wg6.a((Object) f3, str8);
            return new HybridRecommendPreset(f2, f, str, arrayList, a5, f4, f3);
        }
        wg6.a();
        throw null;
    }
}
