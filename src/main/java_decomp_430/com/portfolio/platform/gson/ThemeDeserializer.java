package com.portfolio.platform.gson;

import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.ku3;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDeserializer implements hu3<Theme> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public Theme deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        ku3 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        if (d.d("id")) {
            JsonElement a2 = d.a("id");
            wg6.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_ID)");
            str = a2.f();
        } else {
            str = "";
        }
        if (d.d("name")) {
            JsonElement a3 = d.a("name");
            wg6.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_NAME)");
            str2 = a3.f();
        } else {
            str2 = "";
        }
        ArrayList arrayList = new ArrayList();
        if (gu3 != null && d.d("styles")) {
            Iterator it = d.b("styles").iterator();
            while (it.hasNext()) {
                JsonElement jsonElement2 = (JsonElement) it.next();
                wg6.a((Object) jsonElement2, "item");
                ku3 d2 = jsonElement2.d();
                if (d2.d("key")) {
                    JsonElement a4 = d2.a("key");
                    wg6.a((Object) a4, "itemJsonObject.get(Constants.JSON_KEY_KEY)");
                    str3 = a4.f();
                } else {
                    str3 = "";
                }
                if (d2.d("type")) {
                    JsonElement a5 = d2.a("type");
                    wg6.a((Object) a5, "itemJsonObject.get(Constants.JSON_KEY_TYPE)");
                    str4 = a5.f();
                } else {
                    str4 = "";
                }
                if (d2.d(ServerSetting.VALUE)) {
                    ku3 a6 = d2.a(ServerSetting.VALUE);
                    if (a6 instanceof ku3) {
                        ku3 ku3 = a6;
                        if (ku3.d("fileName")) {
                            JsonElement a7 = ku3.a("fileName");
                            wg6.a((Object) a7, "valueJsonElement.get(Constants.JSON_KEY_FILE_NAME)");
                            str5 = a7.f();
                            wg6.a((Object) str5, "valueJsonElement.get(Con\u2026N_KEY_FILE_NAME).asString");
                        }
                    } else {
                        wg6.a((Object) a6, "valueJsonElement");
                        str5 = a6.f();
                        wg6.a((Object) str5, "valueJsonElement.asString");
                    }
                    wg6.a((Object) str3, "key");
                    wg6.a((Object) str4, "type");
                    arrayList.add(new Style(str3, str4, str5));
                }
                str5 = "";
                wg6.a((Object) str3, "key");
                wg6.a((Object) str4, "type");
                arrayList.add(new Style(str3, str4, str5));
            }
        }
        wg6.a((Object) str, "id");
        wg6.a((Object) str2, "name");
        return new Theme(str, str2, arrayList);
    }
}
