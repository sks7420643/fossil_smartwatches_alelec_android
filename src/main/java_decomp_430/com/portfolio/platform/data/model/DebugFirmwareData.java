package com.portfolio.platform.data.model;

import com.fossil.qg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DebugFirmwareData {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int DOWNLOADED; // = 2;
    @DexIgnore
    public static /* final */ int DOWNLOADING; // = 1;
    @DexIgnore
    public static /* final */ int NONE; // = 0;
    @DexIgnore
    public /* final */ Firmware firmware;
    @DexIgnore
    public int state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DebugFirmwareData(Firmware firmware2, int i) {
        wg6.b(firmware2, "firmware");
        this.firmware = firmware2;
        this.state = i;
    }

    @DexIgnore
    public final Firmware getFirmware() {
        return this.firmware;
    }

    @DexIgnore
    public final int getState() {
        return this.state;
    }

    @DexIgnore
    public final void setState(int i) {
        this.state = i;
    }
}
