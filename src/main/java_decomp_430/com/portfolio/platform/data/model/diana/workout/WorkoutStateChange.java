package com.portfolio.platform.data.model.diana.workout;

import com.fossil.di4;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStateChange {
    @DexIgnore
    public /* final */ di4 state;
    @DexIgnore
    public /* final */ Date time;

    @DexIgnore
    public WorkoutStateChange(di4 di4, Date date) {
        wg6.b(di4, Constants.STATE);
        wg6.b(date, LogBuilder.KEY_TIME);
        this.state = di4;
        this.time = date;
    }

    @DexIgnore
    private final di4 component1() {
        return this.state;
    }

    @DexIgnore
    private final Date component2() {
        return this.time;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutStateChange copy$default(WorkoutStateChange workoutStateChange, di4 di4, Date date, int i, Object obj) {
        if ((i & 1) != 0) {
            di4 = workoutStateChange.state;
        }
        if ((i & 2) != 0) {
            date = workoutStateChange.time;
        }
        return workoutStateChange.copy(di4, date);
    }

    @DexIgnore
    public final WorkoutStateChange copy(di4 di4, Date date) {
        wg6.b(di4, Constants.STATE);
        wg6.b(date, LogBuilder.KEY_TIME);
        return new WorkoutStateChange(di4, date);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutStateChange)) {
            return false;
        }
        WorkoutStateChange workoutStateChange = (WorkoutStateChange) obj;
        return wg6.a((Object) this.state, (Object) workoutStateChange.state) && wg6.a((Object) this.time, (Object) workoutStateChange.time);
    }

    @DexIgnore
    public int hashCode() {
        di4 di4 = this.state;
        int i = 0;
        int hashCode = (di4 != null ? di4.hashCode() : 0) * 31;
        Date date = this.time;
        if (date != null) {
            i = date.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChange(state=" + this.state + ", time=" + this.time + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutStateChange(int i, com.fossil.fitness.WorkoutStateChange workoutStateChange) {
        this(di4.Companion.a(workoutStateChange.getState().name()), new Date(((long) (i + workoutStateChange.getIndexInSecond())) * 1000));
        wg6.b(workoutStateChange, "workoutStageChange");
    }
}
