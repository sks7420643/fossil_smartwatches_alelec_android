package com.portfolio.platform.data.model.microapp.weather;

import com.fossil.bi4;
import com.fossil.bk4;
import com.fossil.qg6;
import com.fossil.rh6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.CurrentWeatherInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherDayForecast;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherHourForecast;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Weather {
    @DexIgnore
    public String address; // = "";
    @DexIgnore
    @vu3("currently")
    public /* final */ Currently currently;
    @DexIgnore
    @vu3("daily")
    public /* final */ ArrayList<Daily> daily;
    @DexIgnore
    @vu3("expiredAt")
    public /* final */ DateTime expiredAt;
    @DexIgnore
    @vu3("hourly")
    public /* final */ ArrayList<Hourly> hourly;
    @DexIgnore
    @vu3("location")
    public /* final */ Location location;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Currently {
        @DexIgnore
        @vu3("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @vu3("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @vu3("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @vu3("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @vu3("temperature")
        public /* final */ Temperature temperature;
        @DexIgnore
        @vu3("time")
        public /* final */ DateTime time;

        @DexIgnore
        public Currently() {
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final DateTime getTime() {
            return this.time;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Daily {
        @DexIgnore
        @vu3("date")
        public /* final */ Date date;
        @DexIgnore
        @vu3("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @vu3("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @vu3("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @vu3("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @vu3("temperature")
        public /* final */ Temperature temperature;

        @DexIgnore
        public Daily() {
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Hourly {
        @DexIgnore
        @vu3("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @vu3("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @vu3("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @vu3("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @vu3("temperature")
        public /* final */ Temperature temperature;
        @DexIgnore
        @vu3("time")
        public /* final */ DateTime time;

        @DexIgnore
        public Hourly() {
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final DateTime getTime() {
            return this.time;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Latlong {
        @DexIgnore
        @vu3("lat")
        public /* final */ float lat;
        @DexIgnore
        @vu3("lng")
        public /* final */ float lng;

        @DexIgnore
        public Latlong() {
        }

        @DexIgnore
        public final float getLat() {
            return this.lat;
        }

        @DexIgnore
        public final float getLng() {
            return this.lng;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Location {
        @DexIgnore
        @vu3("cachedAt")
        public /* final */ Latlong cachedAt;
        @DexIgnore
        @vu3("requestedAt")
        public /* final */ Latlong requestedAt;

        @DexIgnore
        public Location() {
        }

        @DexIgnore
        public final Latlong getCachedAt() {
            return this.cachedAt;
        }

        @DexIgnore
        public final Latlong getRequestedAt() {
            return this.requestedAt;
        }
    }

    @DexIgnore
    public enum TEMP_UNIT {
        CELSIUS("c"),
        FAHRENHEIT("f");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final TEMP_UNIT getTempUnit(String str) {
                wg6.b(str, ServerSetting.VALUE);
                for (TEMP_UNIT temp_unit : TEMP_UNIT.values()) {
                    if (wg6.a((Object) temp_unit.getValue(), (Object) str)) {
                        return temp_unit;
                    }
                }
                return TEMP_UNIT.CELSIUS;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public TEMP_UNIT(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[TEMP_UNIT.values().length];

        /*
        static {
            $EnumSwitchMapping$0[TEMP_UNIT.CELSIUS.ordinal()] = 1;
        }
        */
    }

    @DexIgnore
    public Weather(Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2) {
        wg6.b(location2, "location");
        wg6.b(currently2, "currently");
        wg6.b(dateTime, Constants.PROFILE_KEY_EXPIRED_AT);
        wg6.b(arrayList, "daily");
        wg6.b(arrayList2, "hourly");
        this.location = location2;
        this.currently = currently2;
        this.expiredAt = dateTime;
        this.daily = arrayList;
        this.hourly = arrayList2;
    }

    @DexIgnore
    public static /* synthetic */ Weather copy$default(Weather weather, Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2, int i, Object obj) {
        if ((i & 1) != 0) {
            location2 = weather.location;
        }
        if ((i & 2) != 0) {
            currently2 = weather.currently;
        }
        Currently currently3 = currently2;
        if ((i & 4) != 0) {
            dateTime = weather.expiredAt;
        }
        DateTime dateTime2 = dateTime;
        if ((i & 8) != 0) {
            arrayList = weather.daily;
        }
        ArrayList<Daily> arrayList3 = arrayList;
        if ((i & 16) != 0) {
            arrayList2 = weather.hourly;
        }
        return weather.copy(location2, currently3, dateTime2, arrayList3, arrayList2);
    }

    @DexIgnore
    private final long getExpiredTimeInSecondForWatchApp() {
        Calendar c = bk4.c(Long.valueOf(this.expiredAt.getMillis()));
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        if (c.compareTo(bk4.c(Long.valueOf(instance.getTimeInMillis()))) <= 0) {
            return this.expiredAt.getMillis() / ((long) 1000);
        }
        Calendar c2 = bk4.c(Long.valueOf(this.expiredAt.getMillis()));
        wg6.a((Object) c2, "calendar");
        return c2.getTimeInMillis() / ((long) 1000);
    }

    @DexIgnore
    private final WeatherDayForecast.WeatherWeekDay getWeatherWeekDay(Date date) {
        switch (bk4.a(Long.valueOf(date.getTime()))) {
            case 1:
                return WeatherDayForecast.WeatherWeekDay.SUNDAY;
            case 2:
                return WeatherDayForecast.WeatherWeekDay.MONDAY;
            case 3:
                return WeatherDayForecast.WeatherWeekDay.TUESDAY;
            case 4:
                return WeatherDayForecast.WeatherWeekDay.WEDNESDAY;
            case 5:
                return WeatherDayForecast.WeatherWeekDay.THURSDAY;
            case 6:
                return WeatherDayForecast.WeatherWeekDay.FRIDAY;
            default:
                return WeatherDayForecast.WeatherWeekDay.SATURDAY;
        }
    }

    @DexIgnore
    public final Location component1() {
        return this.location;
    }

    @DexIgnore
    public final Currently component2() {
        return this.currently;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.expiredAt;
    }

    @DexIgnore
    public final ArrayList<Daily> component4() {
        return this.daily;
    }

    @DexIgnore
    public final ArrayList<Hourly> component5() {
        return this.hourly;
    }

    @DexIgnore
    public final Weather copy(Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2) {
        wg6.b(location2, "location");
        wg6.b(currently2, "currently");
        wg6.b(dateTime, Constants.PROFILE_KEY_EXPIRED_AT);
        wg6.b(arrayList, "daily");
        wg6.b(arrayList2, "hourly");
        return new Weather(location2, currently2, dateTime, arrayList, arrayList2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Weather)) {
            return false;
        }
        Weather weather = (Weather) obj;
        return wg6.a((Object) this.location, (Object) weather.location) && wg6.a((Object) this.currently, (Object) weather.currently) && wg6.a((Object) this.expiredAt, (Object) weather.expiredAt) && wg6.a((Object) this.daily, (Object) weather.daily) && wg6.a((Object) this.hourly, (Object) weather.hourly);
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final Currently getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public final ArrayList<Daily> getDaily() {
        return this.daily;
    }

    @DexIgnore
    public final DateTime getExpiredAt() {
        return this.expiredAt;
    }

    @DexIgnore
    public final ArrayList<Hourly> getHourly() {
        return this.hourly;
    }

    @DexIgnore
    public final Location getLocation() {
        return this.location;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        Location location2 = this.location;
        int i = 0;
        int hashCode = (location2 != null ? location2.hashCode() : 0) * 31;
        Currently currently2 = this.currently;
        int hashCode2 = (hashCode + (currently2 != null ? currently2.hashCode() : 0)) * 31;
        DateTime dateTime = this.expiredAt;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        ArrayList<Daily> arrayList = this.daily;
        int hashCode4 = (hashCode3 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<Hourly> arrayList2 = this.hourly;
        if (arrayList2 != null) {
            i = arrayList2.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        wg6.b(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public final ChanceOfRainComplicationAppInfo toChanceOfRainComplicationAppInfo() {
        return new ChanceOfRainComplicationAppInfo(rh6.a(this.currently.getRainProbability() * ((float) 100)), this.expiredAt.getMillis() / ((long) 1000));
    }

    @DexIgnore
    public String toString() {
        return "Weather(location=" + this.location + ", currently=" + this.currently + ", expiredAt=" + this.expiredAt + ", daily=" + this.daily + ", hourly=" + this.hourly + ")";
    }

    @DexIgnore
    public final WeatherComplicationAppInfo toWeatherComplicationAppInfo() {
        WeatherComplicationAppInfo.TemperatureUnit temperatureUnit;
        WeatherComplicationAppInfo.WeatherCondition a = bi4.Companion.a(this.currently.getForecast());
        Temperature temperature = this.currently.getTemperature();
        if (temperature != null) {
            if (wg6.a((Object) temperature.getUnit(), (Object) TEMP_UNIT.FAHRENHEIT.getValue())) {
                temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.F;
            } else {
                temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.C;
            }
            return new WeatherComplicationAppInfo(this.currently.getTemperature().getCurrently(), temperatureUnit, a, this.expiredAt.getMillis() / ((long) 1000));
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final WeatherWatchAppInfo toWeatherWatchAppInfo() {
        UserDisplayUnit.TemperatureUnit temperatureUnit;
        int a = rh6.a(this.currently.getRainProbability() * ((float) 100));
        Temperature temperature = this.currently.getTemperature();
        if (temperature != null) {
            CurrentWeatherInfo currentWeatherInfo = new CurrentWeatherInfo(a, temperature.getCurrently(), bi4.Companion.a(this.currently.getForecast()), this.currently.getTemperature().getMax(), this.currently.getTemperature().getMin());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator<Daily> it = this.daily.iterator();
            while (it.hasNext()) {
                Daily next = it.next();
                Temperature temperature2 = next.getTemperature();
                if (temperature2 != null) {
                    float max = temperature2.getMax();
                    float min = next.getTemperature().getMin();
                    WeatherComplicationAppInfo.WeatherCondition a2 = bi4.Companion.a(next.getForecast());
                    Date date = next.getDate();
                    if (date != null) {
                        arrayList.add(new WeatherDayForecast(max, min, a2, getWeatherWeekDay(date)));
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            int b = bk4.b(Long.valueOf(instance.getTimeInMillis()));
            Iterator<Hourly> it2 = this.hourly.iterator();
            while (it2.hasNext()) {
                Hourly next2 = it2.next();
                DateTime time = next2.getTime();
                if (time != null) {
                    int b2 = bk4.b(Long.valueOf(time.getMillis()));
                    if ((((b2 - b) + 24) - 1) % 4 == 0) {
                        Temperature temperature3 = next2.getTemperature();
                        if (temperature3 != null) {
                            arrayList2.add(new WeatherHourForecast(b2, temperature3.getCurrently(), bi4.Companion.a(next2.getForecast())));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            TEMP_UNIT.Companion companion = TEMP_UNIT.Companion;
            String unit = this.currently.getTemperature().getUnit();
            wg6.a((Object) unit, "currently.temperature.unit");
            if (WhenMappings.$EnumSwitchMapping$0[companion.getTempUnit(unit).ordinal()] != 1) {
                temperatureUnit = UserDisplayUnit.TemperatureUnit.F;
            } else {
                temperatureUnit = UserDisplayUnit.TemperatureUnit.C;
            }
            return new WeatherWatchAppInfo(this.address, temperatureUnit, currentWeatherInfo, arrayList, arrayList2, getExpiredTimeInSecondForWatchApp());
        }
        wg6.a();
        throw null;
    }
}
