package com.portfolio.platform.data.model.goaltracking;

import com.fossil.bk4;
import com.fossil.lc6;
import com.fossil.wg6;
import com.fossil.yd6;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataKt {
    @DexIgnore
    public static final lc6<Date, Date> calculateRangeDownload(List<GoalTrackingData> list, Date date, Date date2) {
        wg6.b(list, "$this$calculateRangeDownload");
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        if (list.isEmpty()) {
            return new lc6<>(date, date2);
        }
        if (!bk4.d(((GoalTrackingData) yd6.f(list)).getDate(), date2)) {
            return new lc6<>(((GoalTrackingData) yd6.f(list)).getDate(), date2);
        }
        if (bk4.d(((GoalTrackingData) yd6.d(list)).getDate(), date)) {
            return null;
        }
        return new lc6<>(date, bk4.n(((GoalTrackingData) yd6.d(list)).getDate()));
    }
}
