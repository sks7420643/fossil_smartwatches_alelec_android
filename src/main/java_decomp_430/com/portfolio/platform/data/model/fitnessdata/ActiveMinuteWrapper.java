package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.d;
import com.fossil.fitness.ActiveMinute;
import com.fossil.wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveMinuteWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public int total;
    @DexIgnore
    public List<Boolean> values;

    @DexIgnore
    public ActiveMinuteWrapper(int i, List<Boolean> list, int i2) {
        wg6.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = i2;
    }

    @DexIgnore
    public static /* synthetic */ ActiveMinuteWrapper copy$default(ActiveMinuteWrapper activeMinuteWrapper, int i, List<Boolean> list, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = activeMinuteWrapper.resolutionInSecond;
        }
        if ((i3 & 2) != 0) {
            list = activeMinuteWrapper.values;
        }
        if ((i3 & 4) != 0) {
            i2 = activeMinuteWrapper.total;
        }
        return activeMinuteWrapper.copy(i, list, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Boolean> component2() {
        return this.values;
    }

    @DexIgnore
    public final int component3() {
        return this.total;
    }

    @DexIgnore
    public final ActiveMinuteWrapper copy(int i, List<Boolean> list, int i2) {
        wg6.b(list, "values");
        return new ActiveMinuteWrapper(i, list, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActiveMinuteWrapper)) {
            return false;
        }
        ActiveMinuteWrapper activeMinuteWrapper = (ActiveMinuteWrapper) obj;
        return this.resolutionInSecond == activeMinuteWrapper.resolutionInSecond && wg6.a((Object) this.values, (Object) activeMinuteWrapper.values) && this.total == activeMinuteWrapper.total;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Boolean> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolutionInSecond) * 31;
        List<Boolean> list = this.values;
        return ((a + (list != null ? list.hashCode() : 0)) * 31) + d.a(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public final void setValues(List<Boolean> list) {
        wg6.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "ActiveMinuteWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ActiveMinuteWrapper(ActiveMinute activeMinute) {
        this(r0, r1, activeMinute.getTotal());
        wg6.b(activeMinute, "activeMinute");
        int resolutionInSecond2 = activeMinute.getResolutionInSecond();
        ArrayList values2 = activeMinute.getValues();
        wg6.a((Object) values2, "activeMinute.values");
    }
}
