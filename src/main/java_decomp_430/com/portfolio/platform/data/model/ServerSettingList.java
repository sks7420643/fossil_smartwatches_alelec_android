package com.portfolio.platform.data.model;

import com.fossil.vu3;
import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingList {
    @DexIgnore
    @vu3("offset")
    public int offset;
    @DexIgnore
    @vu3("_items")
    public List<ServerSetting> serverSettings;
    @DexIgnore
    @vu3("size")
    public int size;
    @DexIgnore
    @vu3("total")
    public int total;

    @DexIgnore
    public final int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public final List<ServerSetting> getServerSettings() {
        return this.serverSettings;
    }

    @DexIgnore
    public final int getSize() {
        return this.size;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final void setOffset(int i) {
        this.offset = i;
    }

    @DexIgnore
    public final void setServerSettings(List<ServerSetting> list) {
        this.serverSettings = list;
    }

    @DexIgnore
    public final void setSize(int i) {
        this.size = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public String toString() {
        if (this.serverSettings == null) {
            return "serverSettings is null";
        }
        StringBuffer stringBuffer = new StringBuffer();
        List<ServerSetting> list = this.serverSettings;
        if (list != null) {
            for (ServerSetting valueOf : list) {
                stringBuffer.append(String.valueOf(valueOf) + "\n");
            }
        }
        String stringBuffer2 = stringBuffer.toString();
        wg6.a((Object) stringBuffer2, "s.toString()");
        return stringBuffer2;
    }
}
