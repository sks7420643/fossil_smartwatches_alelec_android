package com.portfolio.platform.data.model.room.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.b;
import com.fossil.bk4;
import com.fossil.d;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.ActivityIntensities;
import java.io.Serializable;
import java.net.URI;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRaw implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public double calories;
    @DexIgnore
    public double distance;
    @DexIgnore
    public Date endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public String intensity;
    @DexIgnore
    public ActivityIntensities intensityDistInSteps;
    @DexIgnore
    public String movementTypeValue;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public String sourceId;
    @DexIgnore
    public String sourceTypeValue;
    @DexIgnore
    public Date startTime;
    @DexIgnore
    public double steps;
    @DexIgnore
    public String timeZoneID;
    @DexIgnore
    public int uaPinType;
    @DexIgnore
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SampleRaw> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public SampleRaw createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new SampleRaw(parcel);
        }

        @DexIgnore
        public SampleRaw[] newArray(int i) {
            return new SampleRaw[i];
        }
    }

    @DexIgnore
    public SampleRaw(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4) {
        wg6.b(date, "startTime");
        wg6.b(date2, "endTime");
        wg6.b(str, "sourceId");
        wg6.b(str2, "sourceTypeValue");
        wg6.b(activityIntensities, "intensityDistInSteps");
        this.startTime = date;
        this.endTime = date2;
        this.sourceId = str;
        this.sourceTypeValue = str2;
        this.movementTypeValue = str3;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.activeTime = i;
        this.intensityDistInSteps = activityIntensities;
        this.timeZoneID = str4;
        TimeZone timeZone = TimeZone.getTimeZone(this.timeZoneID);
        if (timeZone == null || (!wg6.a((Object) timeZone.getID(), (Object) this.timeZoneID))) {
            TimeZone timeZone2 = TimeZone.getDefault();
            wg6.a((Object) timeZone2, "TimeZone.getDefault()");
            this.timeZoneID = timeZone2.getID();
        }
        String aSCIIString = generateSampleRawURI().toASCIIString();
        wg6.a((Object) aSCIIString, "generateSampleRawURI().toASCIIString()");
        this.uri = aSCIIString;
        this.id = this.uri;
        this.pinType = 0;
        this.uaPinType = 0;
    }

    @DexIgnore
    public static /* synthetic */ SampleRaw copy$default(SampleRaw sampleRaw, Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4, int i2, Object obj) {
        SampleRaw sampleRaw2 = sampleRaw;
        int i3 = i2;
        return sampleRaw.copy((i3 & 1) != 0 ? sampleRaw2.startTime : date, (i3 & 2) != 0 ? sampleRaw2.endTime : date2, (i3 & 4) != 0 ? sampleRaw2.sourceId : str, (i3 & 8) != 0 ? sampleRaw2.sourceTypeValue : str2, (i3 & 16) != 0 ? sampleRaw2.movementTypeValue : str3, (i3 & 32) != 0 ? sampleRaw2.steps : d, (i3 & 64) != 0 ? sampleRaw2.calories : d2, (i3 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? sampleRaw2.distance : d3, (i3 & 256) != 0 ? sampleRaw2.activeTime : i, (i3 & 512) != 0 ? sampleRaw2.intensityDistInSteps : activityIntensities, (i3 & 1024) != 0 ? sampleRaw2.timeZoneID : str4);
    }

    @DexIgnore
    private final URI generateSampleRawURI() {
        StringBuilder sb = new StringBuilder();
        sb.append("sample:");
        String str = this.sourceTypeValue;
        if (str != null) {
            String lowerCase = str.toLowerCase();
            wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            sb.append(lowerCase);
            String sb2 = sb.toString();
            String u = bk4.u(getStartTimeLocal());
            URI create = URI.create("urn:fsl:fitness:" + sb2 + ':' + this.sourceId + ':' + u);
            wg6.a((Object) create, "URI.create(\"urn:fsl:fitn\u2026ty:$sourceId:$timestamp\")");
            return create;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    private final Date getEndTimeLocal() {
        Date b = bk4.b(this.endTime, TimeZone.getTimeZone(this.timeZoneID));
        wg6.a((Object) b, "DateHelper.getDateByTime\u2026.getTimeZone(timeZoneID))");
        return b;
    }

    @DexIgnore
    private final Date getStartTimeLocal() {
        Date b = bk4.b(this.startTime, TimeZone.getTimeZone(this.timeZoneID));
        wg6.a((Object) b, "DateHelper.getDateByTime\u2026.getTimeZone(timeZoneID))");
        return b;
    }

    @DexIgnore
    private final int getTimeZoneOffset() {
        return bk4.a(this.timeZoneID, this.startTime, false);
    }

    @DexIgnore
    public final Date component1() {
        return this.startTime;
    }

    @DexIgnore
    public final ActivityIntensities component10() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String component11() {
        return this.timeZoneID;
    }

    @DexIgnore
    public final Date component2() {
        return this.endTime;
    }

    @DexIgnore
    public final String component3() {
        return this.sourceId;
    }

    @DexIgnore
    public final String component4() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public final String component5() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public final double component6() {
        return this.steps;
    }

    @DexIgnore
    public final double component7() {
        return this.calories;
    }

    @DexIgnore
    public final double component8() {
        return this.distance;
    }

    @DexIgnore
    public final int component9() {
        return this.activeTime;
    }

    @DexIgnore
    public final SampleRaw copy(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4) {
        Date date3 = date;
        wg6.b(date3, "startTime");
        Date date4 = date2;
        wg6.b(date4, "endTime");
        String str5 = str;
        wg6.b(str5, "sourceId");
        String str6 = str2;
        wg6.b(str6, "sourceTypeValue");
        ActivityIntensities activityIntensities2 = activityIntensities;
        wg6.b(activityIntensities2, "intensityDistInSteps");
        return new SampleRaw(date3, date4, str5, str6, str3, d, d2, d3, i, activityIntensities2, str4);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SampleRaw)) {
            return false;
        }
        SampleRaw sampleRaw = (SampleRaw) obj;
        return wg6.a((Object) this.startTime, (Object) sampleRaw.startTime) && wg6.a((Object) this.endTime, (Object) sampleRaw.endTime) && wg6.a((Object) this.sourceId, (Object) sampleRaw.sourceId) && wg6.a((Object) this.sourceTypeValue, (Object) sampleRaw.sourceTypeValue) && wg6.a((Object) this.movementTypeValue, (Object) sampleRaw.movementTypeValue) && Double.compare(this.steps, sampleRaw.steps) == 0 && Double.compare(this.calories, sampleRaw.calories) == 0 && Double.compare(this.distance, sampleRaw.distance) == 0 && this.activeTime == sampleRaw.activeTime && wg6.a((Object) this.intensityDistInSteps, (Object) sampleRaw.intensityDistInSteps) && wg6.a((Object) this.timeZoneID, (Object) sampleRaw.timeZoneID);
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final double getAverageSteps() {
        return this.steps / ((double) TimeUnit.MILLISECONDS.toMinutes(this.endTime.getTime() - this.startTime.getTime()));
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final Date getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityIntensities getIntensityDistInSteps() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String getMovementTypeValue() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public final String getSourceTypeValue() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public final Date getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final String getTimeZoneID() {
        return this.timeZoneID;
    }

    @DexIgnore
    public final int getUaPinType() {
        return this.uaPinType;
    }

    @DexIgnore
    public final String getUri() {
        return this.uri;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.startTime;
        int i = 0;
        int hashCode = (date != null ? date.hashCode() : 0) * 31;
        Date date2 = this.endTime;
        int hashCode2 = (hashCode + (date2 != null ? date2.hashCode() : 0)) * 31;
        String str = this.sourceId;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.sourceTypeValue;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.movementTypeValue;
        int hashCode5 = (((((((((hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31) + b.a(this.steps)) * 31) + b.a(this.calories)) * 31) + b.a(this.distance)) * 31) + d.a(this.activeTime)) * 31;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int hashCode6 = (hashCode5 + (activityIntensities != null ? activityIntensities.hashCode() : 0)) * 31;
        String str4 = this.timeZoneID;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setEndTime(Date date) {
        wg6.b(date, "<set-?>");
        this.endTime = date;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setIntensityDistInSteps(ActivityIntensities activityIntensities) {
        wg6.b(activityIntensities, "<set-?>");
        this.intensityDistInSteps = activityIntensities;
    }

    @DexIgnore
    public final void setMovementTypeValue(String str) {
        this.movementTypeValue = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSourceId(String str) {
        wg6.b(str, "<set-?>");
        this.sourceId = str;
    }

    @DexIgnore
    public final void setSourceTypeValue(String str) {
        wg6.b(str, "<set-?>");
        this.sourceTypeValue = str;
    }

    @DexIgnore
    public final void setStartTime(Date date) {
        wg6.b(date, "<set-?>");
        this.startTime = date;
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setTimeZoneID(String str) {
        this.timeZoneID = str;
    }

    @DexIgnore
    public final void setUaPinType(int i) {
        this.uaPinType = i;
    }

    @DexIgnore
    public final void setUri(String str) {
        wg6.b(str, "<set-?>");
        this.uri = str;
    }

    @DexIgnore
    public final ActivitySample toActivitySample() {
        FLogger.INSTANCE.getLocal().d("SampleRaw", "toActivitySample");
        DateTimeZone forTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(this.timeZoneID));
        Date date = this.startTime;
        DateTime dateTime = r3;
        DateTime dateTime2 = new DateTime(getStartTimeLocal().getTime(), forTimeZone);
        DateTime dateTime3 = r3;
        DateTime dateTime4 = new DateTime((Object) getEndTimeLocal(), forTimeZone);
        return new ActivitySample("", date, dateTime, dateTime3, this.steps, this.calories, this.distance, this.activeTime, this.intensityDistInSteps, getTimeZoneOffset(), this.sourceId, 0, 0, 0);
    }

    @DexIgnore
    public String toString() {
        return "SampleRaw(startTime=" + this.startTime + ", endTime=" + this.endTime + ", sourceId=" + this.sourceId + ", sourceTypeValue=" + this.sourceTypeValue + ", movementTypeValue=" + this.movementTypeValue + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timeZoneID=" + this.timeZoneID + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeSerializable(this.startTime);
        parcel.writeSerializable(this.endTime);
        parcel.writeString(this.sourceId);
        parcel.writeString(this.sourceTypeValue);
        parcel.writeString(this.movementTypeValue);
        parcel.writeDouble(this.steps);
        parcel.writeDouble(this.calories);
        parcel.writeDouble(this.distance);
        parcel.writeInt(this.activeTime);
        parcel.writeParcelable(this.intensityDistInSteps, i);
        parcel.writeString(this.id);
        parcel.writeString(this.uri);
        parcel.writeInt(this.pinType);
        parcel.writeInt(this.uaPinType);
        parcel.writeString(this.intensity);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ SampleRaw(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4, int i2, qg6 qg6) {
        this(date, date2, str, str2, str3, d, d2, d3, i, activityIntensities, r15);
        String str5;
        if ((i2 & 1024) != 0) {
            TimeZone timeZone = TimeZone.getDefault();
            wg6.a((Object) timeZone, "TimeZone.getDefault()");
            str5 = timeZone.getID();
        } else {
            str5 = str4;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SampleRaw(Parcel parcel) {
        this(r2, r3, r4, r5, r6, r7, r9, r18, r20, r21, (String) null, 1024, (qg6) null);
        String str;
        ActivityIntensities activityIntensities;
        String str2;
        SampleRaw sampleRaw;
        Parcel parcel2 = parcel;
        wg6.b(parcel2, "parcel");
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            Date date = (Date) readSerializable;
            Serializable readSerializable2 = parcel.readSerializable();
            if (readSerializable2 != null) {
                Date date2 = (Date) readSerializable2;
                String readString = parcel.readString();
                String str3 = readString != null ? readString : "";
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    str = readString2;
                } else {
                    str = "";
                }
                String readString3 = parcel.readString();
                double readDouble = parcel.readDouble();
                double readDouble2 = parcel.readDouble();
                double readDouble3 = parcel.readDouble();
                int readInt = parcel.readInt();
                ActivityIntensities activityIntensities2 = (ActivityIntensities) parcel2.readParcelable(ActivityIntensities.class.getClassLoader());
                if (activityIntensities2 != null) {
                    activityIntensities = activityIntensities2;
                } else {
                    activityIntensities = new ActivityIntensities(0.0d, 0.0d, 0.0d);
                }
                String readString4 = parcel.readString();
                if (readString4 != null) {
                    str2 = readString4;
                    sampleRaw = this;
                } else {
                    sampleRaw = this;
                    str2 = "";
                }
                sampleRaw.id = str2;
                String readString5 = parcel.readString();
                sampleRaw.uri = readString5 == null ? "" : readString5;
                sampleRaw.pinType = parcel.readInt();
                sampleRaw.uaPinType = parcel.readInt();
                sampleRaw.intensity = parcel.readString();
                return;
            }
            throw new rc6("null cannot be cast to non-null type java.util.Date");
        }
        throw new rc6("null cannot be cast to non-null type java.util.Date");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SampleRaw(Date date, Date date2, String str, String str2, String str3, String str4, double d, double d2, double d3, int i, ActivityIntensities activityIntensities) {
        this(r1, r2, r3, r4, str4, d, d2, d3, i, r13, r14);
        String str5 = str;
        Date date3 = date;
        wg6.b(date3, "start");
        Date date4 = date2;
        wg6.b(date4, "end");
        wg6.b(str5, "timeZoneID");
        String str6 = str2;
        wg6.b(str6, "sourceId");
        String str7 = str3;
        wg6.b(str7, "sourceType");
        ActivityIntensities activityIntensities2 = activityIntensities;
        wg6.b(activityIntensities2, "intensityDistInSteps");
        String str8 = str5;
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone == null || (!wg6.a((Object) timeZone.getID(), (Object) str8))) {
            TimeZone timeZone2 = TimeZone.getDefault();
            wg6.a((Object) timeZone2, "TimeZone.getDefault()");
            this.timeZoneID = timeZone2.getID();
            return;
        }
        this.timeZoneID = timeZone.getID();
    }
}
