package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.fossil.yd6;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("locations")
    public List<WeatherLocationWrapper> locations; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppSetting createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new WeatherWatchAppSetting(parcel);
        }

        @DexIgnore
        public WeatherWatchAppSetting[] newArray(int i) {
            return new WeatherWatchAppSetting[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppSetting() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<WeatherLocationWrapper> getLocations() {
        return this.locations;
    }

    @DexIgnore
    public final void setLocations(List<WeatherLocationWrapper> list) {
        wg6.b(list, "<set-?>");
        this.locations = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeList(yd6.m(this.locations));
    }

    @DexIgnore
    public WeatherWatchAppSetting(Parcel parcel) {
        wg6.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, WeatherLocationWrapper.class.getClassLoader());
        this.locations = arrayList;
    }
}
