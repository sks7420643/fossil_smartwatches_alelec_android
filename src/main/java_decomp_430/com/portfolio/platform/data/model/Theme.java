package com.portfolio.platform.data.model;

import com.fossil.wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Theme {
    @DexIgnore
    public String id;
    @DexIgnore
    public boolean isCurrentTheme; // = true;
    @DexIgnore
    public String name;
    @DexIgnore
    public ArrayList<Style> styles;
    @DexIgnore
    public String type; // = "fixed";

    @DexIgnore
    public Theme(String str, String str2, ArrayList<Style> arrayList) {
        wg6.b(str, "id");
        wg6.b(str2, "name");
        wg6.b(arrayList, "styles");
        this.id = str;
        this.name = str2;
        this.styles = arrayList;
    }

    @DexIgnore
    public static /* synthetic */ Theme copy$default(Theme theme, String str, String str2, ArrayList<Style> arrayList, int i, Object obj) {
        if ((i & 1) != 0) {
            str = theme.id;
        }
        if ((i & 2) != 0) {
            str2 = theme.name;
        }
        if ((i & 4) != 0) {
            arrayList = theme.styles;
        }
        return theme.copy(str, str2, arrayList);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<Style> component3() {
        return this.styles;
    }

    @DexIgnore
    public final Theme copy(String str, String str2, ArrayList<Style> arrayList) {
        wg6.b(str, "id");
        wg6.b(str2, "name");
        wg6.b(arrayList, "styles");
        return new Theme(str, str2, arrayList);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Theme)) {
            return false;
        }
        Theme theme = (Theme) obj;
        return wg6.a((Object) this.id, (Object) theme.id) && wg6.a((Object) this.name, (Object) theme.name) && wg6.a((Object) this.styles, (Object) theme.styles);
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<Style> getStyles() {
        return this.styles;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<Style> arrayList = this.styles;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final boolean isCurrentTheme() {
        return this.isCurrentTheme;
    }

    @DexIgnore
    public final void setCurrentTheme(boolean z) {
        this.isCurrentTheme = z;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setStyles(ArrayList<Style> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.styles = arrayList;
    }

    @DexIgnore
    public final void setType(String str) {
        wg6.b(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public String toString() {
        return "Theme(id=" + this.id + ", name=" + this.name + ", styles=" + this.styles + ")";
    }
}
