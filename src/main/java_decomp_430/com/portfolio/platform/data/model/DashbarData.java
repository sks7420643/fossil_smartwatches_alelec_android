package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.d;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashbarData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("endProgress")
    public int endProgress;
    @DexIgnore
    @vu3("startProgress")
    public int startProgress;
    @DexIgnore
    @vu3("viewId")
    public int viewId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<DashbarData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public DashbarData createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new DashbarData(parcel);
        }

        @DexIgnore
        public DashbarData[] newArray(int i) {
            return new DashbarData[i];
        }
    }

    @DexIgnore
    public DashbarData(int i, int i2, int i3) {
        this.viewId = i;
        this.startProgress = i2;
        this.endProgress = i3;
    }

    @DexIgnore
    public static /* synthetic */ DashbarData copy$default(DashbarData dashbarData, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = dashbarData.viewId;
        }
        if ((i4 & 2) != 0) {
            i2 = dashbarData.startProgress;
        }
        if ((i4 & 4) != 0) {
            i3 = dashbarData.endProgress;
        }
        return dashbarData.copy(i, i2, i3);
    }

    @DexIgnore
    public final int component1() {
        return this.viewId;
    }

    @DexIgnore
    public final int component2() {
        return this.startProgress;
    }

    @DexIgnore
    public final int component3() {
        return this.endProgress;
    }

    @DexIgnore
    public final DashbarData copy(int i, int i2, int i3) {
        return new DashbarData(i, i2, i3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DashbarData)) {
            return false;
        }
        DashbarData dashbarData = (DashbarData) obj;
        return this.viewId == dashbarData.viewId && this.startProgress == dashbarData.startProgress && this.endProgress == dashbarData.endProgress;
    }

    @DexIgnore
    public final int getEndProgress() {
        return this.endProgress;
    }

    @DexIgnore
    public final int getStartProgress() {
        return this.startProgress;
    }

    @DexIgnore
    public final int getViewId() {
        return this.viewId;
    }

    @DexIgnore
    public int hashCode() {
        return (((d.a(this.viewId) * 31) + d.a(this.startProgress)) * 31) + d.a(this.endProgress);
    }

    @DexIgnore
    public final void setEndProgress(int i) {
        this.endProgress = i;
    }

    @DexIgnore
    public final void setStartProgress(int i) {
        this.startProgress = i;
    }

    @DexIgnore
    public final void setViewId(int i) {
        this.viewId = i;
    }

    @DexIgnore
    public String toString() {
        return "DashbarData(viewId=" + this.viewId + ", startProgress=" + this.startProgress + ", endProgress=" + this.endProgress + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.viewId);
        }
        if (parcel != null) {
            parcel.writeInt(this.startProgress);
        }
        if (parcel != null) {
            parcel.writeInt(this.endProgress);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashbarData(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
        wg6.b(parcel, "parcel");
    }
}
