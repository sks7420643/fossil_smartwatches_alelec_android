package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.d;
import com.fossil.e;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWorkoutSession {
    @DexIgnore
    public List<GFitWOCalorie> calories;
    @DexIgnore
    public List<GFitWODistance> distances;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public List<GFitWOHeartRate> heartRates;
    @DexIgnore
    public int id;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public List<GFitWOStep> steps;
    @DexIgnore
    public int workoutType;

    @DexIgnore
    public GFitWorkoutSession(long j, long j2, int i, List<GFitWOStep> list, List<GFitWOCalorie> list2, List<GFitWODistance> list3, List<GFitWOHeartRate> list4) {
        wg6.b(list, "steps");
        wg6.b(list2, Constants.CALORIES);
        wg6.b(list3, "distances");
        wg6.b(list4, "heartRates");
        this.startTime = j;
        this.endTime = j2;
        this.workoutType = i;
        this.steps = list;
        this.calories = list2;
        this.distances = list3;
        this.heartRates = list4;
    }

    @DexIgnore
    public static /* synthetic */ GFitWorkoutSession copy$default(GFitWorkoutSession gFitWorkoutSession, long j, long j2, int i, List list, List list2, List list3, List list4, int i2, Object obj) {
        GFitWorkoutSession gFitWorkoutSession2 = gFitWorkoutSession;
        return gFitWorkoutSession.copy((i2 & 1) != 0 ? gFitWorkoutSession2.startTime : j, (i2 & 2) != 0 ? gFitWorkoutSession2.endTime : j2, (i2 & 4) != 0 ? gFitWorkoutSession2.workoutType : i, (i2 & 8) != 0 ? gFitWorkoutSession2.steps : list, (i2 & 16) != 0 ? gFitWorkoutSession2.calories : list2, (i2 & 32) != 0 ? gFitWorkoutSession2.distances : list3, (i2 & 64) != 0 ? gFitWorkoutSession2.heartRates : list4);
    }

    @DexIgnore
    public final long component1() {
        return this.startTime;
    }

    @DexIgnore
    public final long component2() {
        return this.endTime;
    }

    @DexIgnore
    public final int component3() {
        return this.workoutType;
    }

    @DexIgnore
    public final List<GFitWOStep> component4() {
        return this.steps;
    }

    @DexIgnore
    public final List<GFitWOCalorie> component5() {
        return this.calories;
    }

    @DexIgnore
    public final List<GFitWODistance> component6() {
        return this.distances;
    }

    @DexIgnore
    public final List<GFitWOHeartRate> component7() {
        return this.heartRates;
    }

    @DexIgnore
    public final GFitWorkoutSession copy(long j, long j2, int i, List<GFitWOStep> list, List<GFitWOCalorie> list2, List<GFitWODistance> list3, List<GFitWOHeartRate> list4) {
        List<GFitWOStep> list5 = list;
        wg6.b(list5, "steps");
        List<GFitWOCalorie> list6 = list2;
        wg6.b(list6, Constants.CALORIES);
        List<GFitWODistance> list7 = list3;
        wg6.b(list7, "distances");
        List<GFitWOHeartRate> list8 = list4;
        wg6.b(list8, "heartRates");
        return new GFitWorkoutSession(j, j2, i, list5, list6, list7, list8);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GFitWorkoutSession)) {
            return false;
        }
        GFitWorkoutSession gFitWorkoutSession = (GFitWorkoutSession) obj;
        return this.startTime == gFitWorkoutSession.startTime && this.endTime == gFitWorkoutSession.endTime && this.workoutType == gFitWorkoutSession.workoutType && wg6.a((Object) this.steps, (Object) gFitWorkoutSession.steps) && wg6.a((Object) this.calories, (Object) gFitWorkoutSession.calories) && wg6.a((Object) this.distances, (Object) gFitWorkoutSession.distances) && wg6.a((Object) this.heartRates, (Object) gFitWorkoutSession.heartRates);
    }

    @DexIgnore
    public final List<GFitWOCalorie> getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final List<GFitWODistance> getDistances() {
        return this.distances;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<GFitWOHeartRate> getHeartRates() {
        return this.heartRates;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<GFitWOStep> getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final int getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        int a = ((((e.a(this.startTime) * 31) + e.a(this.endTime)) * 31) + d.a(this.workoutType)) * 31;
        List<GFitWOStep> list = this.steps;
        int i = 0;
        int hashCode = (a + (list != null ? list.hashCode() : 0)) * 31;
        List<GFitWOCalorie> list2 = this.calories;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<GFitWODistance> list3 = this.distances;
        int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<GFitWOHeartRate> list4 = this.heartRates;
        if (list4 != null) {
            i = list4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setCalories(List<GFitWOCalorie> list) {
        wg6.b(list, "<set-?>");
        this.calories = list;
    }

    @DexIgnore
    public final void setDistances(List<GFitWODistance> list) {
        wg6.b(list, "<set-?>");
        this.distances = list;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setHeartRates(List<GFitWOHeartRate> list) {
        wg6.b(list, "<set-?>");
        this.heartRates = list;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setSteps(List<GFitWOStep> list) {
        wg6.b(list, "<set-?>");
        this.steps = list;
    }

    @DexIgnore
    public final void setWorkoutType(int i) {
        this.workoutType = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitWorkoutSession(startTime=" + this.startTime + ", endTime=" + this.endTime + ", workoutType=" + this.workoutType + ", steps=" + this.steps + ", calories=" + this.calories + ", distances=" + this.distances + ", heartRates=" + this.heartRates + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GFitWorkoutSession(long j, long j2, int i, List list, List list2, List list3, List list4, int i2, qg6 qg6) {
        this(j, j2, i, (i2 & 8) != 0 ? qd6.a() : list, (i2 & 16) != 0 ? qd6.a() : list2, (i2 & 32) != 0 ? qd6.a() : list3, (i2 & 64) != 0 ? qd6.a() : list4);
    }
}
