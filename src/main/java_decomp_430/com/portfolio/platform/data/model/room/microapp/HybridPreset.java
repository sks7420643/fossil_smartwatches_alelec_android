package com.portfolio.platform.data.model.room.microapp;

import android.content.Context;
import com.fossil.bk4;
import com.fossil.jm4;
import com.fossil.pj4;
import com.fossil.qg6;
import com.fossil.uu3;
import com.fossil.vu3;
import com.fossil.wg6;
import com.fossil.zi4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.HybridPresetAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    @uu3(HybridPresetAppSettingSerializer.class)
    @vu3("buttons")
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("isActive")
    public boolean isActive;
    @DexIgnore
    @vu3("name")
    public String name;
    @DexIgnore
    @pj4
    public int pinType;
    @DexIgnore
    @vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @vu3("updatedAt")
    public String updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final HybridPreset cloneFrom(HybridPreset hybridPreset) {
            wg6.b(hybridPreset, "preset");
            String u = bk4.u(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wg6.a((Object) uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset2 = new HybridPreset(uuid, jm4.a((Context) PortfolioApp.get.instance(), 2131886370), hybridPreset.getSerialNumber(), hybridPreset.getButtons(), false);
            hybridPreset2.setCreatedAt(u);
            hybridPreset2.setUpdatedAt(u);
            return hybridPreset2;
        }

        @DexIgnore
        public final HybridPreset cloneFromDefault(HybridRecommendPreset hybridRecommendPreset) {
            wg6.b(hybridRecommendPreset, "preset");
            String u = bk4.u(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wg6.a((Object) uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset = new HybridPreset(uuid, hybridRecommendPreset.getName(), hybridRecommendPreset.getSerialNumber(), hybridRecommendPreset.getButtons(), hybridRecommendPreset.isDefault());
            hybridPreset.setCreatedAt(u);
            hybridPreset.setUpdatedAt(u);
            for (HybridPresetAppSetting localUpdateAt : hybridPreset.getButtons()) {
                wg6.a((Object) u, "timestamp");
                localUpdateAt.setLocalUpdateAt(u);
            }
            return hybridPreset;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public HybridPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        wg6.b(str, "id");
        wg6.b(str3, "serialNumber");
        wg6.b(arrayList, "buttons");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isActive = z;
        this.pinType = 1;
        this.createdAt = "";
        this.updatedAt = "";
    }

    @DexIgnore
    public static /* synthetic */ HybridPreset copy$default(HybridPreset hybridPreset, String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = hybridPreset.name;
        }
        String str4 = str2;
        if ((i & 4) != 0) {
            str3 = hybridPreset.serialNumber;
        }
        String str5 = str3;
        if ((i & 8) != 0) {
            arrayList = hybridPreset.buttons;
        }
        ArrayList<HybridPresetAppSetting> arrayList2 = arrayList;
        if ((i & 16) != 0) {
            z = hybridPreset.isActive;
        }
        return hybridPreset.copy(str, str4, str5, arrayList2, z);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, zi4.a((List<HybridPresetAppSetting>) this.buttons), this.isActive);
        hybridPreset.createdAt = this.createdAt;
        hybridPreset.updatedAt = this.updatedAt;
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isActive;
    }

    @DexIgnore
    public final HybridPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        wg6.b(str, "id");
        wg6.b(str3, "serialNumber");
        wg6.b(arrayList, "buttons");
        return new HybridPreset(str, str2, str3, arrayList, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HybridPreset)) {
            return false;
        }
        HybridPreset hybridPreset = (HybridPreset) obj;
        return wg6.a((Object) this.id, (Object) hybridPreset.id) && wg6.a((Object) this.name, (Object) hybridPreset.name) && wg6.a((Object) this.serialNumber, (Object) hybridPreset.serialNumber) && wg6.a((Object) this.buttons, (Object) hybridPreset.buttons) && this.isActive == hybridPreset.isActive;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.serialNumber;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        int i2 = (hashCode3 + i) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isActive=" + this.isActive + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, int i, qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z);
    }
}
