package com.portfolio.platform.data.model.room.fitness;

import com.fossil.b;
import com.fossil.d;
import com.fossil.qg6;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummary {
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public int activeTimeGoal;
    @DexIgnore
    public double calories;
    @DexIgnore
    public int caloriesGoal;
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public int day;
    @DexIgnore
    public double distance;
    @DexIgnore
    public Integer dstOffset;
    @DexIgnore
    public List<Integer> intensities;
    @DexIgnore
    public int month;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int stepGoal;
    @DexIgnore
    public double steps;
    @DexIgnore
    public String timezoneName;
    @DexIgnore
    public TotalValuesOfWeek totalValuesOfWeek;
    @DexIgnore
    public DateTime updatedAt;
    @DexIgnore
    public int year;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class TotalValuesOfWeek {
        @DexIgnore
        public int totalActiveTimeOfWeek;
        @DexIgnore
        public double totalCaloriesOfWeek;
        @DexIgnore
        public double totalStepsOfWeek;

        @DexIgnore
        public TotalValuesOfWeek(double d, double d2, int i) {
            this.totalStepsOfWeek = d;
            this.totalCaloriesOfWeek = d2;
            this.totalActiveTimeOfWeek = i;
        }

        @DexIgnore
        public static /* synthetic */ TotalValuesOfWeek copy$default(TotalValuesOfWeek totalValuesOfWeek, double d, double d2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                d = totalValuesOfWeek.totalStepsOfWeek;
            }
            double d3 = d;
            if ((i2 & 2) != 0) {
                d2 = totalValuesOfWeek.totalCaloriesOfWeek;
            }
            double d4 = d2;
            if ((i2 & 4) != 0) {
                i = totalValuesOfWeek.totalActiveTimeOfWeek;
            }
            return totalValuesOfWeek.copy(d3, d4, i);
        }

        @DexIgnore
        public final double component1() {
            return this.totalStepsOfWeek;
        }

        @DexIgnore
        public final double component2() {
            return this.totalCaloriesOfWeek;
        }

        @DexIgnore
        public final int component3() {
            return this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final TotalValuesOfWeek copy(double d, double d2, int i) {
            return new TotalValuesOfWeek(d, d2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TotalValuesOfWeek)) {
                return false;
            }
            TotalValuesOfWeek totalValuesOfWeek = (TotalValuesOfWeek) obj;
            return Double.compare(this.totalStepsOfWeek, totalValuesOfWeek.totalStepsOfWeek) == 0 && Double.compare(this.totalCaloriesOfWeek, totalValuesOfWeek.totalCaloriesOfWeek) == 0 && this.totalActiveTimeOfWeek == totalValuesOfWeek.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final int getTotalActiveTimeOfWeek() {
            return this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final double getTotalCaloriesOfWeek() {
            return this.totalCaloriesOfWeek;
        }

        @DexIgnore
        public final double getTotalStepsOfWeek() {
            return this.totalStepsOfWeek;
        }

        @DexIgnore
        public int hashCode() {
            return (((b.a(this.totalStepsOfWeek) * 31) + b.a(this.totalCaloriesOfWeek)) * 31) + d.a(this.totalActiveTimeOfWeek);
        }

        @DexIgnore
        public final void setTotalActiveTimeOfWeek(int i) {
            this.totalActiveTimeOfWeek = i;
        }

        @DexIgnore
        public final void setTotalCaloriesOfWeek(double d) {
            this.totalCaloriesOfWeek = d;
        }

        @DexIgnore
        public final void setTotalStepsOfWeek(double d) {
            this.totalStepsOfWeek = d;
        }

        @DexIgnore
        public String toString() {
            return "TotalValuesOfWeek(totalStepsOfWeek=" + this.totalStepsOfWeek + ", totalCaloriesOfWeek=" + this.totalCaloriesOfWeek + ", totalActiveTimeOfWeek=" + this.totalActiveTimeOfWeek + ")";
        }
    }

    @DexIgnore
    public ActivitySummary(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List<Integer> list, int i4, int i5, int i6, int i7) {
        List<Integer> list2 = list;
        wg6.b(list2, "intensities");
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.timezoneName = str;
        this.dstOffset = num;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.intensities = list2;
        this.activeTime = i4;
        this.stepGoal = i5;
        this.caloriesGoal = i6;
        this.activeTimeGoal = i7;
    }

    @DexIgnore
    public static /* synthetic */ ActivitySummary copy$default(ActivitySummary activitySummary, int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List list, int i4, int i5, int i6, int i7, int i8, Object obj) {
        ActivitySummary activitySummary2 = activitySummary;
        int i9 = i8;
        return activitySummary.copy((i9 & 1) != 0 ? activitySummary2.year : i, (i9 & 2) != 0 ? activitySummary2.month : i2, (i9 & 4) != 0 ? activitySummary2.day : i3, (i9 & 8) != 0 ? activitySummary2.timezoneName : str, (i9 & 16) != 0 ? activitySummary2.dstOffset : num, (i9 & 32) != 0 ? activitySummary2.steps : d, (i9 & 64) != 0 ? activitySummary2.calories : d2, (i9 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? activitySummary2.distance : d3, (i9 & 256) != 0 ? activitySummary2.intensities : list, (i9 & 512) != 0 ? activitySummary2.activeTime : i4, (i9 & 1024) != 0 ? activitySummary2.stepGoal : i5, (i9 & 2048) != 0 ? activitySummary2.caloriesGoal : i6, (i9 & 4096) != 0 ? activitySummary2.activeTimeGoal : i7);
    }

    @DexIgnore
    public final int component1() {
        return this.year;
    }

    @DexIgnore
    public final int component10() {
        return this.activeTime;
    }

    @DexIgnore
    public final int component11() {
        return this.stepGoal;
    }

    @DexIgnore
    public final int component12() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final int component13() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public final int component2() {
        return this.month;
    }

    @DexIgnore
    public final int component3() {
        return this.day;
    }

    @DexIgnore
    public final String component4() {
        return this.timezoneName;
    }

    @DexIgnore
    public final Integer component5() {
        return this.dstOffset;
    }

    @DexIgnore
    public final double component6() {
        return this.steps;
    }

    @DexIgnore
    public final double component7() {
        return this.calories;
    }

    @DexIgnore
    public final double component8() {
        return this.distance;
    }

    @DexIgnore
    public final List<Integer> component9() {
        return this.intensities;
    }

    @DexIgnore
    public final ActivitySummary copy(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List<Integer> list, int i4, int i5, int i6, int i7) {
        int i8 = i;
        wg6.b(list, "intensities");
        return new ActivitySummary(i, i2, i3, str, num, d, d2, d3, list, i4, i5, i6, i7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivitySummary)) {
            return false;
        }
        ActivitySummary activitySummary = (ActivitySummary) obj;
        return this.year == activitySummary.year && this.month == activitySummary.month && this.day == activitySummary.day && wg6.a((Object) this.timezoneName, (Object) activitySummary.timezoneName) && wg6.a((Object) this.dstOffset, (Object) activitySummary.dstOffset) && Double.compare(this.steps, activitySummary.steps) == 0 && Double.compare(this.calories, activitySummary.calories) == 0 && Double.compare(this.distance, activitySummary.distance) == 0 && wg6.a((Object) this.intensities, (Object) activitySummary.intensities) && this.activeTime == activitySummary.activeTime && this.stepGoal == activitySummary.stepGoal && this.caloriesGoal == activitySummary.caloriesGoal && this.activeTimeGoal == activitySummary.activeTimeGoal;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final int getActiveTimeGoal() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final int getCaloriesGoal() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.year, this.month - 1, this.day);
        wg6.a((Object) instance, "calendar");
        Date time = instance.getTime();
        wg6.a((Object) time, "calendar.time");
        return time;
    }

    @DexIgnore
    public final int getDay() {
        return this.day;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final Integer getDstOffset() {
        return this.dstOffset;
    }

    @DexIgnore
    public final List<Integer> getIntensities() {
        return this.intensities;
    }

    @DexIgnore
    public final int getMonth() {
        return this.month;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getStepGoal() {
        return this.stepGoal;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final String getTimezoneName() {
        return this.timezoneName;
    }

    @DexIgnore
    public final TotalValuesOfWeek getTotalValuesOfWeek() {
        return this.totalValuesOfWeek;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int getYear() {
        return this.year;
    }

    @DexIgnore
    public int hashCode() {
        int a = ((((d.a(this.year) * 31) + d.a(this.month)) * 31) + d.a(this.day)) * 31;
        String str = this.timezoneName;
        int i = 0;
        int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
        Integer num = this.dstOffset;
        int hashCode2 = (((((((hashCode + (num != null ? num.hashCode() : 0)) * 31) + b.a(this.steps)) * 31) + b.a(this.calories)) * 31) + b.a(this.distance)) * 31;
        List<Integer> list = this.intensities;
        if (list != null) {
            i = list.hashCode();
        }
        return ((((((((hashCode2 + i) * 31) + d.a(this.activeTime)) * 31) + d.a(this.stepGoal)) * 31) + d.a(this.caloriesGoal)) * 31) + d.a(this.activeTimeGoal);
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setActiveTimeGoal(int i) {
        this.activeTimeGoal = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setCaloriesGoal(int i) {
        this.caloriesGoal = i;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDay(int i) {
        this.day = i;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setDstOffset(Integer num) {
        this.dstOffset = num;
    }

    @DexIgnore
    public final void setIntensities(List<Integer> list) {
        wg6.b(list, "<set-?>");
        this.intensities = list;
    }

    @DexIgnore
    public final void setMonth(int i) {
        this.month = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setStepGoal(int i) {
        this.stepGoal = i;
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setTimezoneName(String str) {
        this.timezoneName = str;
    }

    @DexIgnore
    public final void setTotalValuesOfWeek(TotalValuesOfWeek totalValuesOfWeek2) {
        this.totalValuesOfWeek = totalValuesOfWeek2;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public final void setYear(int i) {
        this.year = i;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySummary(year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", timezoneName=" + this.timezoneName + ", dstOffset=" + this.dstOffset + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", intensities=" + this.intensities + ", activeTime=" + this.activeTime + ", stepGoal=" + this.stepGoal + ", caloriesGoal=" + this.caloriesGoal + ", activeTimeGoal=" + this.activeTimeGoal + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ ActivitySummary(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List list, int i4, int i5, int i6, int i7, int i8, qg6 qg6) {
        this(i, i2, i3, str, num, d, d2, d3, list, (r0 & 512) != 0 ? 0 : i4, (r0 & 1024) != 0 ? 0 : i5, (r0 & 2048) != 0 ? 0 : i6, (r0 & 4096) != 0 ? 0 : i7);
        int i9 = i8;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ActivitySummary(ActivitySummary activitySummary) {
        this(r1, r2, r3, r4, r5, r6, r8, r10, r12, r13, r14, r15, r16);
        ActivitySummary activitySummary2 = activitySummary;
        wg6.b(activitySummary2, "activitySummary");
        int i = activitySummary2.year;
        int i2 = activitySummary2.month;
        int i3 = activitySummary2.day;
        String str = activitySummary2.timezoneName;
        Integer num = activitySummary2.dstOffset;
        double d = activitySummary2.steps;
        double d2 = activitySummary2.calories;
        double d3 = activitySummary2.distance;
        List<Integer> list = activitySummary2.intensities;
        int i4 = activitySummary2.activeTime;
        ActivitySummary activitySummary3 = activitySummary2;
        int i5 = activitySummary2.stepGoal;
        int i6 = activitySummary3.caloriesGoal;
        int i7 = activitySummary3.activeTimeGoal;
        ActivitySummary activitySummary4 = activitySummary;
        this.createdAt = activitySummary4.createdAt;
        this.updatedAt = activitySummary4.updatedAt;
        this.pinType = activitySummary4.pinType;
        this.totalValuesOfWeek = activitySummary4.totalValuesOfWeek;
    }
}
