package com.portfolio.platform.data.model.diana.heartrate;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.c;
import com.fossil.d;
import com.fossil.e;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.yj6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.model.ServerSetting;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSample implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public float average;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int minuteCount;
    @DexIgnore
    public Resting resting;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<HeartRateSample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public HeartRateSample createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new HeartRateSample(parcel);
        }

        @DexIgnore
        public HeartRateSample[] newArray(int i) {
            return new HeartRateSample[i];
        }
    }

    @DexIgnore
    public HeartRateSample(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2) {
        wg6.b(str, "id");
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        wg6.b(dateTime, "endTime");
        wg6.b(dateTime2, "startTime");
        this.id = str;
        this.average = f;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.endTime = dateTime;
        this.startTime = dateTime2;
        this.timezoneOffsetInSecond = i;
        this.min = i2;
        this.max = i3;
        this.minuteCount = i4;
        this.resting = resting2;
    }

    @DexIgnore
    public static /* synthetic */ HeartRateSample copy$default(HeartRateSample heartRateSample, String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2, int i5, Object obj) {
        HeartRateSample heartRateSample2 = heartRateSample;
        int i6 = i5;
        return heartRateSample.copy((i6 & 1) != 0 ? heartRateSample2.id : str, (i6 & 2) != 0 ? heartRateSample2.average : f, (i6 & 4) != 0 ? heartRateSample2.date : date2, (i6 & 8) != 0 ? heartRateSample2.createdAt : j, (i6 & 16) != 0 ? heartRateSample2.updatedAt : j2, (i6 & 32) != 0 ? heartRateSample2.endTime : dateTime, (i6 & 64) != 0 ? heartRateSample2.startTime : dateTime2, (i6 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? heartRateSample2.timezoneOffsetInSecond : i, (i6 & 256) != 0 ? heartRateSample2.min : i2, (i6 & 512) != 0 ? heartRateSample2.max : i3, (i6 & 1024) != 0 ? heartRateSample2.minuteCount : i4, (i6 & 2048) != 0 ? heartRateSample2.resting : resting2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final int component10() {
        return this.max;
    }

    @DexIgnore
    public final int component11() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting component12() {
        return this.resting;
    }

    @DexIgnore
    public final float component2() {
        return this.average;
    }

    @DexIgnore
    public final Date component3() {
        return this.date;
    }

    @DexIgnore
    public final long component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime component7() {
        return this.startTime;
    }

    @DexIgnore
    public final int component8() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component9() {
        return this.min;
    }

    @DexIgnore
    public final HeartRateSample copy(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2) {
        String str2 = str;
        wg6.b(str2, "id");
        Date date3 = date2;
        wg6.b(date3, HardwareLog.COLUMN_DATE);
        DateTime dateTime3 = dateTime;
        wg6.b(dateTime3, "endTime");
        DateTime dateTime4 = dateTime2;
        wg6.b(dateTime4, "startTime");
        return new HeartRateSample(str2, f, date3, j, j2, dateTime3, dateTime4, i, i2, i3, i4, resting2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HeartRateSample)) {
            return false;
        }
        HeartRateSample heartRateSample = (HeartRateSample) obj;
        return wg6.a((Object) this.id, (Object) heartRateSample.id) && Float.compare(this.average, heartRateSample.average) == 0 && wg6.a((Object) this.date, (Object) heartRateSample.date) && this.createdAt == heartRateSample.createdAt && this.updatedAt == heartRateSample.updatedAt && wg6.a((Object) this.endTime, (Object) heartRateSample.endTime) && wg6.a((Object) this.startTime, (Object) heartRateSample.startTime) && this.timezoneOffsetInSecond == heartRateSample.timezoneOffsetInSecond && this.min == heartRateSample.min && this.max == heartRateSample.max && this.minuteCount == heartRateSample.minuteCount && wg6.a((Object) this.resting, (Object) heartRateSample.resting);
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getMinuteCount() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting getResting() {
        return this.resting;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime getStartTimeId() {
        return this.startTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + c.a(this.average)) * 31;
        Date date2 = this.date;
        int hashCode2 = (((((hashCode + (date2 != null ? date2.hashCode() : 0)) * 31) + e.a(this.createdAt)) * 31) + e.a(this.updatedAt)) * 31;
        DateTime dateTime = this.endTime;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.startTime;
        int hashCode4 = (((((((((hashCode3 + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + d.a(this.timezoneOffsetInSecond)) * 31) + d.a(this.min)) * 31) + d.a(this.max)) * 31) + d.a(this.minuteCount)) * 31;
        Resting resting2 = this.resting;
        if (resting2 != null) {
            i = resting2.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        wg6.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setMinuteCount(int i) {
        this.minuteCount = i;
    }

    @DexIgnore
    public final void setResting(Resting resting2) {
        this.resting = resting2;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStartTimeId(DateTime dateTime) {
        wg6.b(dateTime, ServerSetting.VALUE);
        this.startTime = dateTime;
        String str = this.id;
        int b = yj6.b((CharSequence) str, ':', 0, false, 6, (Object) null) + 1;
        if (str != null) {
            String substring = str.substring(0, b);
            wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            this.id = substring + (this.startTime.getMillis() / ((long) 1000));
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "id=" + this.id + ", average=" + this.average + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", endTime=" + this.endTime + ", startTime=" + this.startTime + ", " + "timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", min=" + this.min + ", max=" + this.max + ", minuteCount=" + this.minuteCount + ", resting=" + this.resting;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeFloat(this.average);
        parcel.writeSerializable(this.date);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeSerializable(this.endTime);
        parcel.writeSerializable(this.startTime);
        parcel.writeInt(this.timezoneOffsetInSecond);
        parcel.writeInt(this.min);
        parcel.writeInt(this.max);
        parcel.writeInt(this.minuteCount);
        Resting resting2 = this.resting;
        if (resting2 != null) {
            parcel.writeSerializable(resting2);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateSample(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2, int i5, qg6 qg6) {
        this(str, f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, (i5 & 2048) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateSample(float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, String str, int i4, Resting resting2, int i5, qg6 qg6) {
        this(f, date2, j, j2, dateTime, dateTime2, i, i2, i3, str, i4, (i5 & 2048) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public HeartRateSample(float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, String str, int i4, Resting resting2) {
        this(r0 + ":device:" + (dateTime2.getMillis() / ((long) 1000)), f, r5, j, j2, r10, r11, i, i2, i3, i4, resting2);
        String str2 = str;
        Date date3 = date2;
        wg6.b(date3, HardwareLog.COLUMN_DATE);
        DateTime dateTime3 = dateTime;
        wg6.b(dateTime3, "endTime");
        DateTime dateTime4 = dateTime2;
        wg6.b(dateTime4, "startTime");
        wg6.b(str2, ButtonService.USER_ID);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public HeartRateSample(Parcel parcel) {
        this(r2, r3, r4, r5, r7, r0, (DateTime) r10, parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), (Resting) parcel.readSerializable());
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        String str = readString == null ? "" : readString;
        float readFloat = parcel.readFloat();
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            Date date2 = (Date) readSerializable;
            long readLong = parcel.readLong();
            long readLong2 = parcel.readLong();
            Serializable readSerializable2 = parcel.readSerializable();
            if (readSerializable2 != null) {
                DateTime dateTime = (DateTime) readSerializable2;
                Serializable readSerializable3 = parcel.readSerializable();
                if (readSerializable3 != null) {
                    return;
                }
                throw new rc6("null cannot be cast to non-null type org.joda.time.DateTime");
            }
            throw new rc6("null cannot be cast to non-null type org.joda.time.DateTime");
        }
        throw new rc6("null cannot be cast to non-null type java.util.Date");
    }
}
