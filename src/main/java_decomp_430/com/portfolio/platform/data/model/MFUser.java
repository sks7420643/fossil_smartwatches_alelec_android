package com.portfolio.platform.data.model;

import android.text.TextUtils;
import com.fossil.bk4;
import com.fossil.lh4;
import com.fossil.rh4;
import com.fossil.vu3;
import com.fossil.zh4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.ReadablePartial;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "user")
public class MFUser {
    @DexIgnore
    public static /* final */ String ACCESS_TOKEN_EXPIRED_AT; // = "accessTokenExpiresAt";
    @DexIgnore
    public static /* final */ String ACCESS_TOKEN_EXPIRED_IN; // = "accessTokenExpiresIn";
    @DexIgnore
    public static /* final */ String ACTIVE_DEVICE_ID; // = "activeDeviceId";
    @DexIgnore
    public static /* final */ String AUTH_TYPE; // = "authType";
    @DexIgnore
    public static /* final */ String AVERAGE_SLEEP; // = "averageSleep";
    @DexIgnore
    public static /* final */ String AVERAGE_STEP; // = "averageStep";
    @DexIgnore
    public static /* final */ String BIRTHDAY; // = "birthday";
    @DexIgnore
    public static /* final */ String BRAND; // = "brand";
    @DexIgnore
    public static /* final */ String CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String DIAGNOSTIC_ENABLED; // = "diagnosticEnabled";
    @DexIgnore
    public static /* final */ String DISTANCE_UNIT; // = "distanceUnit";
    @DexIgnore
    public static /* final */ String EMAIL; // = "email";
    @DexIgnore
    public static /* final */ String EMAIL_OPT_IN; // = "emailOptIn";
    @DexIgnore
    public static /* final */ String FIRST_NAME; // = "firstName";
    @DexIgnore
    public static /* final */ String GENDER; // = "gender";
    @DexIgnore
    public static /* final */ String HEIGHT_IN_CM; // = "heightInCentimeters";
    @DexIgnore
    public static /* final */ String HEIGHT_UNIT; // = "heightUnit";
    @DexIgnore
    public static /* final */ String INTEGRATIONS; // = "integrations";
    @DexIgnore
    public static /* final */ String LAST_NAME; // = "lastName";
    @DexIgnore
    public static /* final */ String ONBOARDING_COMPLETE; // = "isOnboardingComplete";
    @DexIgnore
    public static /* final */ String PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String PROFILE_PIC; // = "profilePicture";
    @DexIgnore
    public static /* final */ String REFRESH_TOKEN; // = "refreshToken";
    @DexIgnore
    public static /* final */ String REGISTER_DATE; // = "registerDate";
    @DexIgnore
    public static /* final */ String REGISTRATION_COMPLETE; // = "registrationComplete";
    @DexIgnore
    public static /* final */ String TEMPERATURE_UNIT; // = "temperatureUnit";
    @DexIgnore
    public static /* final */ String UID; // = "uid";
    @DexIgnore
    public static /* final */ String UPDATED_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String USER_ACCESS_TOKEN; // = "userAccessToken";
    @DexIgnore
    public static /* final */ String USER_NAME; // = "username";
    @DexIgnore
    public static /* final */ String USE_DEFAULT_BIOMETRIC; // = "useDefaultBiometric";
    @DexIgnore
    public static /* final */ String USE_DEFAULT_GOALS; // = "useDefaultGoals";
    @DexIgnore
    public static /* final */ String WEIGHT_IN_GRAMS; // = "weightInGrams";
    @DexIgnore
    public static /* final */ String WEIGHT_UNIT; // = "weightUnit";
    @DexIgnore
    @vu3("accessTokenExpiresAt")
    @DatabaseField
    public String accessTokenExpiresAt;
    @DexIgnore
    @vu3("accessTokenExpiresIn")
    @DatabaseField
    public int accessTokenExpiresIn;
    @DexIgnore
    @vu3("activeDeviceId")
    @DatabaseField
    public String activeDeviceId;
    @DexIgnore
    @vu3("authType")
    @DatabaseField
    public String authType;
    @DexIgnore
    @vu3("averageSleep")
    @DatabaseField
    public int averageSleep;
    @DexIgnore
    @vu3("averageStep")
    @DatabaseField
    public int averageStep;
    @DexIgnore
    @vu3("birthday")
    @DatabaseField
    public String birthday;
    @DexIgnore
    @vu3("brand")
    @DatabaseField
    public String brand;
    @DexIgnore
    @vu3("createdAt")
    @DatabaseField
    public String createdAt;
    @DexIgnore
    @vu3("diagnosticEnabled")
    @DatabaseField
    public boolean diagnosticEnabled;
    @DexIgnore
    @vu3("distanceUnit")
    @DatabaseField
    public String distanceUnit; // = zh4.METRIC.getValue();
    @DexIgnore
    @vu3("email")
    @DatabaseField
    public String email;
    @DexIgnore
    @vu3("emailOptIn")
    @DatabaseField
    public boolean emailOptIn;
    @DexIgnore
    @vu3("firstName")
    @DatabaseField
    public String firstName;
    @DexIgnore
    @vu3("gender")
    @DatabaseField
    public String gender;
    @DexIgnore
    @vu3("heightInCentimeters")
    @DatabaseField
    public int heightInCentimeters;
    @DexIgnore
    @vu3("heightUnit")
    @DatabaseField
    public String heightUnit; // = zh4.METRIC.getValue();
    @DexIgnore
    @vu3("home")
    @DatabaseField
    public String home;
    @DexIgnore
    @vu3("integrations")
    @DatabaseField
    public String integrations;
    @DexIgnore
    @vu3("isOnboardingComplete")
    @DatabaseField
    public boolean isOnboardingComplete;
    @DexIgnore
    @vu3("lastName")
    @DatabaseField
    public String lastName;
    @DexIgnore
    @vu3("pinType")
    @DatabaseField
    public String pinType;
    @DexIgnore
    @vu3("profilePicture")
    @DatabaseField
    public String profilePicture;
    @DexIgnore
    @vu3("refreshToken")
    @DatabaseField
    public String refreshToken;
    @DexIgnore
    @vu3("registerDate")
    @DatabaseField
    public String registerDate;
    @DexIgnore
    @vu3("registrationComplete")
    @DatabaseField
    public boolean registrationComplete;
    @DexIgnore
    @vu3("temperatureUnit")
    @DatabaseField
    public String temperatureUnit; // = zh4.METRIC.getValue();
    @DexIgnore
    @vu3("uid")
    @DatabaseField(id = true)
    public String uid;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField
    public String updatedAt;
    @DexIgnore
    @vu3("useDefaultBiometric")
    @DatabaseField
    public boolean useDefaultBiometric;
    @DexIgnore
    @vu3("useDefaultGoals")
    @DatabaseField
    public boolean useDefaultGoals;
    @DexIgnore
    @vu3("userAccessToken")
    @DatabaseField
    public String userAccessToken;
    @DexIgnore
    @vu3("username")
    @DatabaseField
    public String username;
    @DexIgnore
    @vu3("weightInGrams")
    @DatabaseField
    public int weightInGrams;
    @DexIgnore
    @vu3("weightUnit")
    @DatabaseField
    public String weightUnit; // = zh4.METRIC.getValue();
    @DexIgnore
    @vu3("work")
    @DatabaseField
    public String work;

    @DexIgnore
    public MFUser() {
    }

    @DexIgnore
    public static int getAge(String str) {
        Calendar instance = Calendar.getInstance();
        try {
            instance.setTime(bk4.e(str));
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("MFUser", "getAge - e=" + e);
        }
        return Years.yearsBetween((ReadablePartial) LocalDate.fromCalendarFields(instance), (ReadablePartial) LocalDate.now()).getYears();
    }

    @DexIgnore
    public String getAccessTokenExpiresAt() {
        return this.accessTokenExpiresAt;
    }

    @DexIgnore
    public int getAccessTokenExpiresIn() {
        return this.accessTokenExpiresIn;
    }

    @DexIgnore
    public String getActiveDeviceId() {
        return this.activeDeviceId;
    }

    @DexIgnore
    public lh4 getAuthType() {
        return lh4.fromString(this.authType);
    }

    @DexIgnore
    public int getAverageSleep() {
        return this.averageSleep;
    }

    @DexIgnore
    public int getAverageStep() {
        return this.averageStep;
    }

    @DexIgnore
    public String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public String getBrand() {
        return this.brand;
    }

    @DexIgnore
    public String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public zh4 getDistanceUnit() {
        return zh4.fromString(this.distanceUnit);
    }

    @DexIgnore
    public String getEmail() {
        return this.email;
    }

    @DexIgnore
    public String getFirstName() {
        return TextUtils.isEmpty(this.firstName) ? "" : this.firstName;
    }

    @DexIgnore
    public rh4 getGender() {
        return rh4.Companion.a(this.gender);
    }

    @DexIgnore
    public int getHeightInCentimeters() {
        return this.heightInCentimeters;
    }

    @DexIgnore
    public zh4 getHeightUnit() {
        return zh4.fromString(this.heightUnit);
    }

    @DexIgnore
    public String getHome() {
        return this.home;
    }

    @DexIgnore
    public List<String> getIntegrations() {
        if (TextUtils.isEmpty(this.integrations)) {
            return new ArrayList();
        }
        return new ArrayList(Arrays.asList(this.integrations.split("_")));
    }

    @DexIgnore
    public String getIntegrationsRaw() {
        return this.integrations;
    }

    @DexIgnore
    public String getLastName() {
        return TextUtils.isEmpty(this.lastName) ? "" : this.lastName;
    }

    @DexIgnore
    public String getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getProfilePicture() {
        return this.profilePicture;
    }

    @DexIgnore
    public String getRefreshToken() {
        return this.refreshToken;
    }

    @DexIgnore
    public String getRegisterDate() {
        return this.registerDate;
    }

    @DexIgnore
    public zh4 getTemperatureUnit() {
        return zh4.fromString(this.temperatureUnit);
    }

    @DexIgnore
    public String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public String getUserAccessToken() {
        return this.userAccessToken;
    }

    @DexIgnore
    public String getUserId() {
        return this.uid;
    }

    @DexIgnore
    public String getUsername() {
        return this.username;
    }

    @DexIgnore
    public int getWeightInGrams() {
        return this.weightInGrams;
    }

    @DexIgnore
    public zh4 getWeightUnit() {
        return zh4.fromString(this.weightUnit);
    }

    @DexIgnore
    public String getWork() {
        return this.work;
    }

    @DexIgnore
    public boolean isAuthSSO() {
        return getAuthType().isSSO();
    }

    @DexIgnore
    public boolean isDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public boolean isEmailOptIn() {
        return this.emailOptIn;
    }

    @DexIgnore
    public boolean isOnboardingComplete() {
        return this.isOnboardingComplete;
    }

    @DexIgnore
    public boolean isRegistrationComplete() {
        return this.registrationComplete;
    }

    @DexIgnore
    public boolean isUseDefaultBiometric() {
        return this.useDefaultBiometric;
    }

    @DexIgnore
    public boolean isUseDefaultGoals() {
        return this.useDefaultGoals;
    }

    @DexIgnore
    public void setAccessTokenExpiresAt(String str) {
        this.accessTokenExpiresAt = str;
    }

    @DexIgnore
    public void setAccessTokenExpiresIn(Integer num) {
        this.accessTokenExpiresIn = num.intValue();
    }

    @DexIgnore
    public void setActiveDeviceId(String str) {
        this.activeDeviceId = str;
    }

    @DexIgnore
    public void setAuthType(String str) {
        this.authType = str;
    }

    @DexIgnore
    public void setAverageSleep(int i) {
        this.averageSleep = i;
    }

    @DexIgnore
    public void setAverageStep(int i) {
        this.averageStep = i;
    }

    @DexIgnore
    public void setBirthday(String str) {
        this.birthday = str;
    }

    @DexIgnore
    public void setBrand(String str) {
        this.brand = str;
    }

    @DexIgnore
    public void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public void setDistanceUnit(String str) {
        this.distanceUnit = str;
    }

    @DexIgnore
    public void setEmail(String str) {
        this.email = str;
    }

    @DexIgnore
    public void setEmailOptIn(boolean z) {
        this.emailOptIn = z;
    }

    @DexIgnore
    public void setFirstName(String str) {
        this.firstName = str;
    }

    @DexIgnore
    public void setGender(String str) {
        this.gender = str;
    }

    @DexIgnore
    public void setHeightInCentimeters(int i) {
        this.heightInCentimeters = i;
    }

    @DexIgnore
    public void setHeightUnit(String str) {
        this.heightUnit = str;
    }

    @DexIgnore
    public void setHome(String str) {
        this.home = str;
    }

    @DexIgnore
    public void setIntegrations(List<String> list) {
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            for (String append : list) {
                sb.append(append);
                sb.append('_');
            }
            this.integrations = sb.toString();
        }
    }

    @DexIgnore
    public void setLastName(String str) {
        this.lastName = str;
    }

    @DexIgnore
    public void setOnboardingComplete(boolean z) {
        this.isOnboardingComplete = z;
    }

    @DexIgnore
    public void setPinType(String str) {
        this.pinType = str;
    }

    @DexIgnore
    public void setProfilePicture(String str) {
        this.profilePicture = str;
    }

    @DexIgnore
    public void setRefreshToken(String str) {
        this.refreshToken = str;
    }

    @DexIgnore
    public void setRegisterDate(String str) {
        this.registerDate = str;
    }

    @DexIgnore
    public void setRegistrationComplete(boolean z) {
        this.registrationComplete = z;
    }

    @DexIgnore
    public void setTemperatureUnit(String str) {
        this.temperatureUnit = str;
    }

    @DexIgnore
    public void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public void setUseDefaultBiometric(boolean z) {
        this.useDefaultBiometric = z;
    }

    @DexIgnore
    public void setUseDefaultGoals(boolean z) {
        this.useDefaultGoals = z;
    }

    @DexIgnore
    public void setUserAccessToken(String str) {
        this.userAccessToken = str;
    }

    @DexIgnore
    public void setUserId(String str) {
        this.uid = str;
    }

    @DexIgnore
    public void setUsername(String str) {
        this.username = str;
    }

    @DexIgnore
    public void setWeightInGrams(int i) {
        this.weightInGrams = i;
    }

    @DexIgnore
    public void setWeightUnit(String str) {
        this.weightUnit = str;
    }

    @DexIgnore
    public void setWork(String str) {
        this.work = str;
    }

    @DexIgnore
    public void setIntegrations(String str) {
        this.integrations = str;
    }

    @DexIgnore
    public MFUser(MFUser mFUser) {
        this.userAccessToken = mFUser.userAccessToken;
        this.refreshToken = mFUser.refreshToken;
        this.uid = mFUser.uid;
        this.createdAt = mFUser.createdAt;
        this.updatedAt = mFUser.updatedAt;
        this.email = mFUser.email;
        this.authType = mFUser.getAuthType().getValue();
        this.username = mFUser.username;
        this.activeDeviceId = mFUser.activeDeviceId;
        this.firstName = mFUser.getFirstName();
        this.lastName = mFUser.getLastName();
        this.weightInGrams = mFUser.weightInGrams;
        this.heightInCentimeters = mFUser.heightInCentimeters;
        this.heightUnit = mFUser.getHeightUnit().getValue();
        this.weightUnit = mFUser.getWeightUnit().getValue();
        this.distanceUnit = mFUser.getDistanceUnit().getValue();
        this.temperatureUnit = mFUser.getTemperatureUnit().getValue();
        this.birthday = mFUser.birthday;
        this.gender = mFUser.getGender().toString();
        this.profilePicture = mFUser.profilePicture;
        this.brand = mFUser.brand;
        this.registrationComplete = mFUser.registrationComplete;
        this.isOnboardingComplete = mFUser.isOnboardingComplete;
        this.accessTokenExpiresAt = mFUser.accessTokenExpiresAt;
        this.accessTokenExpiresIn = mFUser.accessTokenExpiresIn;
        StringBuilder sb = new StringBuilder();
        for (String append : mFUser.getIntegrations()) {
            sb.append(append);
            sb.append('_');
        }
        this.integrations = sb.toString();
        this.emailOptIn = mFUser.emailOptIn;
        this.diagnosticEnabled = mFUser.diagnosticEnabled;
        this.registerDate = mFUser.registerDate;
        this.pinType = mFUser.pinType;
        this.averageSleep = mFUser.averageSleep;
        this.averageStep = mFUser.averageStep;
    }
}
