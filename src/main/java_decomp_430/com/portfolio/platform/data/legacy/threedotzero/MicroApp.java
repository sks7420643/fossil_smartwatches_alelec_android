package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.vu3;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "microApp")
public class MicroApp implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_APP_ID; // = "appId";
    @DexIgnore
    public static /* final */ String COLUMN_APP_SETTING; // = "appSetting";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DESCRIPTION; // = "description";
    @DexIgnore
    public static /* final */ String COLUMN_ICON; // = "icon";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_LIKE; // = "like";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_PLATFORM; // = "platform";
    @DexIgnore
    public static /* final */ String COLUMN_RELEASE_DATE; // = "releaseDate";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<MicroApp> CREATOR; // = new Anon1();
    @DexIgnore
    @DatabaseField(columnName = "appId")
    public String appId;
    @DexIgnore
    @DatabaseField(columnName = "appSetting")
    public String appSettings;
    @DexIgnore
    @vu3("createdAt")
    @DatabaseField(columnName = "createdAt")
    public long createAt;
    @DexIgnore
    @vu3("description")
    @DatabaseField(columnName = "description")
    public String description;
    @DexIgnore
    @vu3("icon")
    @DatabaseField(columnName = "icon")
    public String iconUrl;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @vu3("like")
    @DatabaseField(columnName = "like")
    public int like;
    @DexIgnore
    @vu3("name")
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @vu3("platform")
    @DatabaseField(columnName = "platform")
    public String platform;
    @DexIgnore
    @vu3("releaseDate")
    @DatabaseField(columnName = "releaseDate")
    public long releaseDate;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MicroApp> {
        @DexIgnore
        public MicroApp createFromParcel(Parcel parcel) {
            return new MicroApp(parcel);
        }

        @DexIgnore
        public MicroApp[] newArray(int i) {
            return new MicroApp[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon2 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID; // = new int[MicroAppInstruction.MicroAppID.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|(3:29|30|32)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|32) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
            $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public MicroApp() {
        this.id = UUID.randomUUID().toString();
        this.appId = MicroAppInstruction.MicroAppID.UAPP_UNKNOWN.getValue();
        this.description = "";
        this.name = "";
        this.iconUrl = "";
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public String getAppSettings() {
        return this.appSettings;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public int getDefaultIconId() {
        switch (Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.appId).ordinal()]) {
            case 1:
                return 2131231124;
            case 2:
                return 2131231188;
            case 3:
                return 2131231180;
            case 4:
                return 2131231181;
            case 5:
                return 2131231186;
            case 6:
                return 2131231184;
            case 7:
                return 2131231183;
            case 8:
                return 2131231185;
            case 9:
                return 2131231187;
            case 10:
                return 2131231191;
            case 11:
                return 2131231192;
            case 12:
                return 2131231179;
            case 13:
                return 2131231193;
            case 14:
                return 2131231189;
            case 15:
                return 2131231190;
            default:
                return 2131231124;
        }
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getIconUrl() {
        return this.iconUrl;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public int getLike() {
        return this.like;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public long getReleaseDate() {
        return this.releaseDate;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setAppId(String str) {
        this.appId = str;
    }

    @DexIgnore
    public void setAppSettings(String str) {
        this.appSettings = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setIconUrl(String str) {
        this.iconUrl = str;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setLike(int i) {
        this.like = i;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setPlatform(String str) {
        this.platform = str;
    }

    @DexIgnore
    public void setReleaseDate(long j) {
        this.releaseDate = j;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.appId);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeString(this.platform);
        parcel.writeInt(this.like);
        parcel.writeString(this.appSettings);
        parcel.writeString(this.iconUrl);
        parcel.writeLong(this.releaseDate);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
    }

    @DexIgnore
    public MicroApp(Parcel parcel) {
        this.id = UUID.randomUUID().toString();
        this.appId = parcel.readString();
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.platform = parcel.readString();
        this.like = parcel.readInt();
        this.appSettings = parcel.readString();
        this.iconUrl = parcel.readString();
        this.releaseDate = parcel.readLong();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
    }
}
