package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.cd6;
import com.fossil.dx6;
import com.fossil.rx6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1 implements dx6<ApiResponse<MicroApp>> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;

    @DexIgnore
    public MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    public void onFailure(Call<ApiResponse<MicroApp>> call, Throwable th) {
        wg6.b(call, "call");
        wg6.b(th, "throwable");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("getMicroAppGallery deviceSerial=");
        sb.append(this.$deviceSerial);
        sb.append(" onFailure ex=");
        th.printStackTrace();
        sb.append(cd6.a);
        local.d(tag, sb.toString());
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    public void onResponse(Call<ApiResponse<MicroApp>> call, rx6<ApiResponse<MicroApp>> rx6) {
        wg6.b(call, "call");
        wg6.b(rx6, "response");
        if (rx6.d()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
            local.d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " onSuccess response=" + ((ApiResponse) rx6.a()));
            ApiResponse apiResponse = (ApiResponse) rx6.a();
            List list = apiResponse != null ? apiResponse.get_items() : null;
            if (list == null || !(!list.isEmpty())) {
                MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
                if (getMicroAppGalleryCallback != null) {
                    getMicroAppGalleryCallback.onFail();
                    return;
                }
                return;
            }
            MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback2 = this.$callback;
            if (getMicroAppGalleryCallback2 != null) {
                getMicroAppGalleryCallback2.onSuccess(list);
                return;
            }
            return;
        }
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback3 = this.$callback;
        if (getMicroAppGalleryCallback3 != null) {
            getMicroAppGalleryCallback3.onFail();
        }
    }
}
