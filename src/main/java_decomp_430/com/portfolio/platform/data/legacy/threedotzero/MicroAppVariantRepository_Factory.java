package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.u04;
import com.portfolio.platform.data.source.UAppSystemVersionRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository_Factory implements Factory<MicroAppVariantRepository> {
    @DexIgnore
    public /* final */ Provider<u04> mAppExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppVariantDataSource> mMicroAppVariantLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppVariantDataSource> mMicroAppVariantRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<UAppSystemVersionRepository> mUAppSystemVersionRepositoryProvider;

    @DexIgnore
    public MicroAppVariantRepository_Factory(Provider<MicroAppVariantDataSource> provider, Provider<MicroAppVariantDataSource> provider2, Provider<UAppSystemVersionRepository> provider3, Provider<u04> provider4) {
        this.mMicroAppVariantRemoteDataSourceProvider = provider;
        this.mMicroAppVariantLocalDataSourceProvider = provider2;
        this.mUAppSystemVersionRepositoryProvider = provider3;
        this.mAppExecutorsProvider = provider4;
    }

    @DexIgnore
    public static MicroAppVariantRepository_Factory create(Provider<MicroAppVariantDataSource> provider, Provider<MicroAppVariantDataSource> provider2, Provider<UAppSystemVersionRepository> provider3, Provider<u04> provider4) {
        return new MicroAppVariantRepository_Factory(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static MicroAppVariantRepository newMicroAppVariantRepository(MicroAppVariantDataSource microAppVariantDataSource, MicroAppVariantDataSource microAppVariantDataSource2, UAppSystemVersionRepository uAppSystemVersionRepository, u04 u04) {
        return new MicroAppVariantRepository(microAppVariantDataSource, microAppVariantDataSource2, uAppSystemVersionRepository, u04);
    }

    @DexIgnore
    public static MicroAppVariantRepository provideInstance(Provider<MicroAppVariantDataSource> provider, Provider<MicroAppVariantDataSource> provider2, Provider<UAppSystemVersionRepository> provider3, Provider<u04> provider4) {
        return new MicroAppVariantRepository(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public MicroAppVariantRepository get() {
        return provideInstance(this.mMicroAppVariantRemoteDataSourceProvider, this.mMicroAppVariantLocalDataSourceProvider, this.mUAppSystemVersionRepositoryProvider, this.mAppExecutorsProvider);
    }
}
