package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ce6;
import com.fossil.hp4;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.u04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.UAppSystemVersionRepository;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppVariantRepository extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ u04 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantRemoteDataSource;
    @DexIgnore
    public List<ShortcutDownloadingObserver> mShortcutDownloadingObservers;
    @DexIgnore
    public /* final */ UAppSystemVersionRepository mUAppSystemVersionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wg6.b(str, "<set-?>");
            MicroAppVariantRepository.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[MicroAppInstruction.MicroAppID.values().length];

        /*
        static {
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = MicroAppVariantRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppVariantRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRepository(@Remote MicroAppVariantDataSource microAppVariantDataSource, @Local MicroAppVariantDataSource microAppVariantDataSource2, UAppSystemVersionRepository uAppSystemVersionRepository, u04 u04) {
        wg6.b(microAppVariantDataSource, "mMicroAppVariantRemoteDataSource");
        wg6.b(microAppVariantDataSource2, "mMicroAppVariantLocalDataSource");
        wg6.b(uAppSystemVersionRepository, "mUAppSystemVersionRepository");
        wg6.b(u04, "mAppExecutors");
        this.mMicroAppVariantRemoteDataSource = microAppVariantDataSource;
        this.mMicroAppVariantLocalDataSource = microAppVariantDataSource2;
        this.mUAppSystemVersionRepository = uAppSystemVersionRepository;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    private final void saveDeclarationFileList(List<? extends DeclarationFile> list, MicroAppVariant microAppVariant) {
        for (DeclarationFile declarationFile : list) {
            declarationFile.setMicroAppVariant(microAppVariant);
            addOrUpdateDeclarationFile(declarationFile, new MicroAppVariantRepository$saveDeclarationFileList$Anon1());
        }
    }

    @DexIgnore
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        wg6.b(declarationFile, "declarationFile");
        wg6.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile fileId=" + declarationFile.getId());
        this.mMicroAppVariantLocalDataSource.addOrUpdateDeclarationFile(declarationFile, addOrUpdateDeclarationFileCallback);
    }

    @DexIgnore
    public final void addStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        wg6.b(shortcutDownloadingObserver, "observer");
        if (this.mShortcutDownloadingObservers == null) {
            this.mShortcutDownloadingObservers = new CopyOnWriteArrayList();
        }
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list != null) {
            list.add(shortcutDownloadingObserver);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void downloadAllVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wg6.b(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$downloadAllVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final ArrayList<hp4> filterVariantList$app_fossilRelease(List<? extends hp4> list) {
        wg6.b(list, "microAppVariants");
        HashMap hashMap = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            hp4 hp4 = (hp4) list.get(i);
            Iterator it = qd6.a((Collection<?>) list).iterator();
            while (it.hasNext()) {
                hp4 hp42 = (hp4) list.get(((ce6) it).a());
                if (wg6.a((Object) hp42.b(), (Object) hp4.b()) && wg6.a((Object) hp42.e(), (Object) hp4.e()) && hp42.d() > hp4.d()) {
                    hp4 = hp42;
                }
            }
            hashMap.put(hp4.b() + hp4.e(), hp4);
        }
        return new ArrayList<>(hashMap.values());
    }

    @DexIgnore
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wg6.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str + " major=" + i + " minor=" + i2);
        this.mMicroAppVariantLocalDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$getAllMicroAppVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        wg6.b(str4, "serialNumber");
        wg6.b(str5, "microAppId");
        wg6.b(str6, "variantName");
        String str7 = TAG;
        MFLogger.d(str7, "getMicroAppVariant serialNumber=" + str4 + "microAppId=" + str5 + " major=" + i + " minor=" + i2 + " name=" + str6);
        this.mMicroAppVariantLocalDataSource.getMicroAppVariant(str, str2, str3, i, i2, new MicroAppVariantRepository$getMicroAppVariant$Anon1(this, str, i, i2, getVariantCallback, str2, str3));
    }

    @DexIgnore
    public final void migrateMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback) {
        wg6.b(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$migrateMicroAppVariants$Anon1(this, str, i, i2, migrateVariantCallback));
    }

    @DexIgnore
    public final void notifyStatusChanged(String str, String str2) {
        wg6.b(str, "status");
        wg6.b(str2, "serial");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            return;
        }
        if (list == null) {
            wg6.a();
            throw null;
        } else if (!list.isEmpty()) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                for (ShortcutDownloadingObserver onStatusChanged : list2) {
                    onStatusChanged.onStatusChanged(str, str2);
                }
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void removeStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        wg6.b(shortcutDownloadingObserver, "observer");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            wg6.a();
            throw null;
        } else if (list.contains(shortcutDownloadingObserver)) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                list2.remove(shortcutDownloadingObserver);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void saveMicroAppVariant$app_fossilRelease(String str, List<? extends hp4> list) {
        wg6.b(str, "serialNumber");
        wg6.b(list, "filter");
        for (hp4 hp4 : list) {
            hp4.d(str);
            MicroAppVariant a = hp4.a();
            wg6.a((Object) a, "parser.convertToMicroAppVariant()");
            String str2 = TAG;
            MFLogger.d(str2, "saveMicroAppVariant variantId=" + a.getId());
            MicroAppVariantDataSource microAppVariantDataSource = this.mMicroAppVariantLocalDataSource;
            if (microAppVariantDataSource != null) {
                ((MicroAppVariantLocalDataSource) microAppVariantDataSource).addOrUpDateVariant(a);
                List<DeclarationFile> c = hp4.c();
                wg6.a((Object) c, "declarationFileList");
                saveDeclarationFileList(c, a);
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantLocalDataSource");
            }
        }
    }
}
