package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zm4;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantLocalDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wg6.b(str, "<set-?>");
            MicroAppVariantLocalDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppVariantLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public final boolean addOrUpDateVariant(MicroAppVariant microAppVariant) {
        wg6.b(microAppVariant, "microAppVariant");
        String str = TAG;
        MFLogger.d(str, ".addOrUpDateVariant - serial=" + microAppVariant.getSerialNumbers() + ", major=" + microAppVariant.getMajorNumber() + " minor=" + microAppVariant.getMinorNumber() + " id=" + microAppVariant.getId());
        return zm4.p.a().d().addOrUpdateMicroAppVariant(microAppVariant);
    }

    @DexIgnore
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        wg6.b(declarationFile, "declarationFile");
        wg6.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile declarationFileId=" + declarationFile.getFileId());
        if (zm4.p.a().d().addOrUpdateDeclarationFile(declarationFile)) {
            addOrUpdateDeclarationFileCallback.onSuccess(declarationFile);
        } else {
            addOrUpdateDeclarationFileCallback.onFail();
        }
    }

    @DexIgnore
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wg6.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serial=" + str + " major=" + i + " minor=" + i2);
        List<MicroAppVariant> allMicroAppVariant = zm4.p.a().d().getAllMicroAppVariant(str, i);
        if (allMicroAppVariant == null || !(!allMicroAppVariant.isEmpty())) {
            String str3 = TAG;
            MFLogger.d(str3, "getAllMicroAppVariants onFail serial=" + str);
            if (getVariantListCallback != null) {
                getVariantListCallback.onFail(600);
                return;
            }
            return;
        }
        String str4 = TAG;
        MFLogger.d(str4, "getAllMicroAppVariants onSuccess serial=" + str);
        if (getVariantListCallback != null) {
            getVariantListCallback.onSuccess(allMicroAppVariant);
        }
    }

    @DexIgnore
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        wg6.b(str, "serialNumber");
        wg6.b(str2, "microAppId");
        wg6.b(str3, "variantName");
        String str4 = TAG;
        MFLogger.d(str4, "getMicroAppVariant serial=" + str + " microAppId=" + str2 + " major=" + i + " minor=" + i2);
        MicroAppVariant microAppVariant = zm4.p.a().d().getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            String str5 = TAG;
            MFLogger.d(str5, "getMicroAppVariant onSuccess serial=" + str + " microAppId=" + str2);
            if (getVariantCallback != null) {
                getVariantCallback.onSuccess(microAppVariant);
                return;
            }
            return;
        }
        String str6 = TAG;
        MFLogger.d(str6, "getMicroAppVariant onFail serial=" + str + " microAppId=" + str2);
        if (getVariantCallback != null) {
            getVariantCallback.onFail(600);
        }
    }

    @DexIgnore
    public void removeMicroAppVariants(String str, int i, int i2) {
        wg6.b(str, "serialNumber");
        zm4.p.a().d().deleteMicroAppVariants(str, i, i2);
    }
}
