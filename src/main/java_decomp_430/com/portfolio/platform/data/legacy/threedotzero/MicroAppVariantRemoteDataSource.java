package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.dx6;
import com.fossil.ku3;
import com.fossil.qg6;
import com.fossil.u04;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRemoteDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ u04 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wg6.b(str, "<set-?>");
            MicroAppVariantRemoteDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppVariantRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRemoteDataSource(ShortcutApiService shortcutApiService, u04 u04) {
        wg6.b(shortcutApiService, "mShortcutApiService");
        wg6.b(u04, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wg6.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str);
        getAllMicroAppVariants$app_fossilRelease(str, i, i2, 0, 100, new MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(this, str, new ArrayList(), i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final void getAllMicroAppVariants$app_fossilRelease(String str, int i, int i2, int i3, int i4, dx6<ku3> dx6) {
        wg6.b(str, "serialNumber");
        wg6.b(dx6, Constants.CALLBACK);
    }
}
