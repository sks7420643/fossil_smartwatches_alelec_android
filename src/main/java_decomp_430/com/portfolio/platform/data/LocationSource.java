package com.portfolio.platform.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import com.fossil.aw2;
import com.fossil.bw2;
import com.fossil.dl6;
import com.fossil.ef6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.mk6;
import com.fossil.nc6;
import com.fossil.nf6;
import com.fossil.qg6;
import com.fossil.tc3;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wv2;
import com.fossil.xe6;
import com.fossil.yv2;
import com.fossil.zl6;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.source.local.AddressDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import kotlinx.coroutines.TimeoutKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource {
    @DexIgnore
    public static /* final */ int CACHE_THRESHOLD_TIME; // = 60000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ long LOCATION_WAITING_THRESHOLD_TIME; // = 5000;
    @DexIgnore
    public static /* final */ float SIGNIFICANT_DISPLACEMENT; // = 300.0f;
    @DexIgnore
    public static /* final */ float SMALLEST_DISPLACEMENT; // = 50.0f;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ int TWO_MINUTES; // = 120000;
    @DexIgnore
    public AddressDao addressDao;
    @DexIgnore
    public boolean isMonitoringObserved;
    @DexIgnore
    public boolean isSignificantObserved;
    @DexIgnore
    public long lastRetrievedTime; // = -1;
    @DexIgnore
    public Location locationCache;
    @DexIgnore
    public MonitoringLocationCallback mMonitoringCallback; // = new MonitoringLocationCallback();
    @DexIgnore
    public wv2 mMonitoringClient;
    @DexIgnore
    public List<LocationListener> mMonitoringLocationListeners; // = new ArrayList();
    @DexIgnore
    public SignificantLocationCallback mSignificantCallback; // = new SignificantLocationCallback();
    @DexIgnore
    public wv2 mSignificantClient;
    @DexIgnore
    public List<LocationListener> mSignificantLocationListeners; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return LocationSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum ErrorState {
        SUCCESS,
        LOCATION_PERMISSION_OFF,
        BACKGROUND_PERMISSION_OFF,
        LOCATION_SERVICE_OFF,
        UNKNOWN
    }

    @DexIgnore
    public interface LocationListener {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class DefaultImpls {
            @DexIgnore
            public static void onLocationResult(LocationListener locationListener, Location location) {
                wg6.b(location, "location");
            }
        }

        @DexIgnore
        void onLocationResult(Location location);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class MonitoringLocationCallback extends yv2 {
        @DexIgnore
        public MonitoringLocationCallback() {
        }

        @DexIgnore
        public void onLocationResult(LocationResult locationResult) {
            Location p = locationResult != null ? locationResult.p() : null;
            if (p != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "MonitoringLocationCallback onLocationResult lastLocation=" + p);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                wg6.a((Object) instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = p;
                for (LocationListener onLocationResult : LocationSource.this.mMonitoringLocationListeners) {
                    onLocationResult.onLocationResult(p);
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "MonitoringLocationCallback onLocationResult lastLocation is null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result {
        @DexIgnore
        public /* final */ ErrorState errorState;
        @DexIgnore
        public /* final */ boolean fromCache;
        @DexIgnore
        public /* final */ Location location;

        @DexIgnore
        public Result(Location location2, ErrorState errorState2, boolean z) {
            wg6.b(errorState2, "errorState");
            this.location = location2;
            this.errorState = errorState2;
            this.fromCache = z;
        }

        @DexIgnore
        public final ErrorState getErrorState() {
            return this.errorState;
        }

        @DexIgnore
        public final boolean getFromCache() {
            return this.fromCache;
        }

        @DexIgnore
        public final Location getLocation() {
            return this.location;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(Location location2, ErrorState errorState2, boolean z, int i, qg6 qg6) {
            this(location2, errorState2, (i & 4) != 0 ? false : z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SignificantLocationCallback extends yv2 {
        @DexIgnore
        public SignificantLocationCallback() {
        }

        @DexIgnore
        public void onLocationResult(LocationResult locationResult) {
            Location p = locationResult != null ? locationResult.p() : null;
            if (p != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "SignificantLocationCallback onLocationResult lastLocation=" + p);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                wg6.a((Object) instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = p;
                for (LocationListener onLocationResult : LocationSource.this.mSignificantLocationListeners) {
                    onLocationResult.onLocationResult(p);
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "SignificantLocationCallback onLocationResult lastLocation is null");
        }
    }

    /*
    static {
        String simpleName = LocationSource.class.getSimpleName();
        wg6.a((Object) simpleName, "LocationSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final Location isBetterLocation(Location location, Location location2) {
        if (location == null && location2 == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation both newLocation null and currentBestLocation null");
            return null;
        } else if (location != null && location2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "isBetterLocation newLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
            long time = location.getTime() - location2.getTime();
            boolean z = true;
            boolean z2 = time > ((long) 120000);
            boolean z3 = time < ((long) -120000);
            boolean z4 = time > 0;
            if (z2) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyNewer");
                return location;
            } else if (z3) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyOlder");
                return location2;
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
                int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
                boolean z5 = accuracy > 0;
                boolean z6 = accuracy < 0;
                if (accuracy <= 200) {
                    z = false;
                }
                boolean isSameProvider = isSameProvider(location.getProvider(), location2.getProvider());
                if (z6) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isMoreAccurate");
                    return location;
                } else if (z4 && !z5) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isLessAccurate=false");
                    return location;
                } else if (!z4 || z || !isSameProvider) {
                    return location2;
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                    return location;
                }
            }
        } else if (location != null) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.d(str4, "isBetterLocation bestLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            return location;
        } else {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("isBetterLocation bestLocation long=");
            if (location2 != null) {
                sb.append(location2.getLongitude());
                sb.append(" lat=");
                sb.append(location2.getLatitude());
                sb.append(" time=");
                sb.append(location2.getTime());
                local5.d(str5, sb.toString());
                return location2;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    private final boolean isSameProvider(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(TAG, "isSameProvider");
        if (str == null) {
            return str2 == null;
        }
        return wg6.a((Object) str, (Object) str2);
    }

    @DexIgnore
    public final AddressDao getAddressDao$app_fossilRelease() {
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            return addressDao2;
        }
        wg6.d("addressDao");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0107, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00fa  */
    public final synchronized Object getLocation(Context context, boolean z, xe6<? super Result> xe6) {
        LocationSource$getLocation$Anon1 locationSource$getLocation$Anon1;
        int i;
        Result result;
        LocationSource locationSource;
        Result result2;
        Location isBetterLocation;
        if (xe6 instanceof LocationSource$getLocation$Anon1) {
            locationSource$getLocation$Anon1 = (LocationSource$getLocation$Anon1) xe6;
            if ((locationSource$getLocation$Anon1.label & Integer.MIN_VALUE) != 0) {
                locationSource$getLocation$Anon1.label -= Integer.MIN_VALUE;
                Object obj = locationSource$getLocation$Anon1.result;
                Object a = ff6.a();
                i = locationSource$getLocation$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    if (this.locationCache != null) {
                        Calendar instance = Calendar.getInstance();
                        wg6.a((Object) instance, "Calendar.getInstance()");
                        if (instance.getTimeInMillis() - this.lastRetrievedTime < ((long) 60000) && !z) {
                            FLogger.INSTANCE.getLocal().d(TAG, "getLocation from cache");
                            result = new Result(this.locationCache, ErrorState.SUCCESS, true);
                        }
                    }
                    dl6 b = zl6.b();
                    LocationSource$getLocation$result$Anon1 locationSource$getLocation$result$Anon1 = new LocationSource$getLocation$result$Anon1(this, context, (xe6) null);
                    locationSource$getLocation$Anon1.L$0 = this;
                    locationSource$getLocation$Anon1.L$1 = context;
                    locationSource$getLocation$Anon1.Z$0 = z;
                    locationSource$getLocation$Anon1.label = 1;
                    obj = gk6.a(b, locationSource$getLocation$result$Anon1, locationSource$getLocation$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    locationSource = this;
                } else if (i == 1) {
                    boolean z2 = locationSource$getLocation$Anon1.Z$0;
                    Context context2 = (Context) locationSource$getLocation$Anon1.L$1;
                    locationSource = (LocationSource) locationSource$getLocation$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                result2 = (Result) obj;
                FLogger.INSTANCE.getLocal().d(TAG, "getLocation result=" + result2.getErrorState() + " location=" + result2.getLocation());
                if (result2.getErrorState() != ErrorState.SUCCESS) {
                    if (locationSource.locationCache != null) {
                    }
                    result = result2;
                }
                isBetterLocation = locationSource.isBetterLocation(result2.getLocation(), locationSource.locationCache);
                if (isBetterLocation == null) {
                    Calendar instance2 = Calendar.getInstance();
                    wg6.a((Object) instance2, "Calendar.getInstance()");
                    locationSource.lastRetrievedTime = instance2.getTimeInMillis();
                    locationSource.locationCache = isBetterLocation;
                    result2 = new Result(locationSource.locationCache, ErrorState.SUCCESS, false, 4, (qg6) null);
                    result = result2;
                } else {
                    result = new Result((Location) null, ErrorState.UNKNOWN, false, 4, (qg6) null);
                }
            }
        }
        locationSource$getLocation$Anon1 = new LocationSource$getLocation$Anon1(this, xe6);
        Object obj2 = locationSource$getLocation$Anon1.result;
        Object a2 = ff6.a();
        i = locationSource$getLocation$Anon1.label;
        if (i != 0) {
        }
        result2 = (Result) obj2;
        FLogger.INSTANCE.getLocal().d(TAG, "getLocation result=" + result2.getErrorState() + " location=" + result2.getLocation());
        if (result2.getErrorState() != ErrorState.SUCCESS) {
        }
        isBetterLocation = locationSource.isBetterLocation(result2.getLocation(), locationSource.locationCache);
        if (isBetterLocation == null) {
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: android.location.Location} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v10, resolved type: android.location.Location} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    public final /* synthetic */ Object getLocationFromFuseLocationManager(Context context, xe6<? super Result> xe6) {
        LocationSource$getLocationFromFuseLocationManager$Anon1 locationSource$getLocationFromFuseLocationManager$Anon1;
        int i;
        Location location;
        Result result;
        Location location2;
        wv2 wv2;
        Object obj;
        Context context2 = context;
        xe6<? super Result> xe62 = xe6;
        if (xe62 instanceof LocationSource$getLocationFromFuseLocationManager$Anon1) {
            locationSource$getLocationFromFuseLocationManager$Anon1 = (LocationSource$getLocationFromFuseLocationManager$Anon1) xe62;
            int i2 = locationSource$getLocationFromFuseLocationManager$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                locationSource$getLocationFromFuseLocationManager$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = locationSource$getLocationFromFuseLocationManager$Anon1.result;
                Object a = ff6.a();
                i = locationSource$getLocationFromFuseLocationManager$Anon1.label;
                Location location3 = null;
                if (i != 0) {
                    nc6.a(obj2);
                    FLogger.INSTANCE.getLocal().d(TAG, "getLocationFromFuseLocationManager");
                    wv2 = aw2.a(context);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.k(0);
                    locationRequest.j(0);
                    locationRequest.d(100);
                    locationRequest.a(0.0f);
                    bw2.a aVar = new bw2.a();
                    aVar.a(locationRequest);
                    if (w6.a(context2, "android.permission.ACCESS_FINE_LOCATION") != 0 || w6.a(context2, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
                        return new Result((Location) null, ErrorState.LOCATION_PERMISSION_OFF, false, 4, (qg6) null);
                    }
                    if (Build.VERSION.SDK_INT >= 29 && w6.a(context2, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0) {
                        return new Result((Location) null, ErrorState.BACKGROUND_PERMISSION_OFF, false, 4, (qg6) null);
                    }
                    if (!LocationUtils.isLocationEnable(context)) {
                        return new Result((Location) null, ErrorState.LOCATION_SERVICE_OFF, false, 4, (qg6) null);
                    }
                    LocationSource$getLocationFromFuseLocationManager$Anon2 locationSource$getLocationFromFuseLocationManager$Anon2 = new LocationSource$getLocationFromFuseLocationManager$Anon2(this, wv2, locationRequest, (xe6) null);
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$0 = this;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$1 = context2;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$2 = null;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$3 = wv2;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$4 = locationRequest;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$5 = aVar;
                    locationSource$getLocationFromFuseLocationManager$Anon1.label = 1;
                    obj = TimeoutKt.a(LOCATION_WAITING_THRESHOLD_TIME, locationSource$getLocationFromFuseLocationManager$Anon2, locationSource$getLocationFromFuseLocationManager$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    bw2.a aVar2 = (bw2.a) locationSource$getLocationFromFuseLocationManager$Anon1.L$5;
                    LocationRequest locationRequest2 = (LocationRequest) locationSource$getLocationFromFuseLocationManager$Anon1.L$4;
                    wv2 wv22 = (wv2) locationSource$getLocationFromFuseLocationManager$Anon1.L$3;
                    location3 = locationSource$getLocationFromFuseLocationManager$Anon1.L$2;
                    Context context3 = (Context) locationSource$getLocationFromFuseLocationManager$Anon1.L$1;
                    LocationSource locationSource = (LocationSource) locationSource$getLocationFromFuseLocationManager$Anon1.L$0;
                    try {
                        nc6.a(obj2);
                        Object obj3 = obj2;
                        wv2 = wv22;
                        obj = obj3;
                    } catch (ExecutionException e) {
                        e = e;
                        location2 = location3;
                    } catch (InterruptedException e2) {
                        e = e2;
                        location2 = location3;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.e(str, ".getLocationFromFuseLocationManager(), e2=" + e);
                        location = location2;
                        if (location == null) {
                        }
                        return result;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                location2 = (Location) obj;
                if (location2 == null) {
                    try {
                        wg6.a((Object) wv2, "fusedLocationClient");
                        location = tc3.a(wv2.i());
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.d(str2, "getLocationFromFuseLocationManager fusedLocationClient.lastLocation=" + location);
                    } catch (ExecutionException e3) {
                        e = e3;
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local3.e(str3, ".getLocationFromFuseLocationManager(), e1=" + e);
                        location = location2;
                        if (location == null) {
                        }
                        return result;
                    } catch (InterruptedException e4) {
                        e = e4;
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        local4.e(str4, ".getLocationFromFuseLocationManager(), e2=" + e);
                        location = location2;
                        if (location == null) {
                        }
                        return result;
                    }
                    if (location == null) {
                        ErrorState errorState = ErrorState.SUCCESS;
                    } else {
                        new Result((Location) null, ErrorState.UNKNOWN, false, 4, (qg6) null);
                    }
                    return result;
                }
                location = location2;
                if (location == null) {
                }
                return result;
            }
        }
        locationSource$getLocationFromFuseLocationManager$Anon1 = new LocationSource$getLocationFromFuseLocationManager$Anon1(this, xe62);
        Object obj22 = locationSource$getLocationFromFuseLocationManager$Anon1.result;
        Object a2 = ff6.a();
        i = locationSource$getLocationFromFuseLocationManager$Anon1.label;
        Location location32 = null;
        if (i != 0) {
        }
        location2 = (Location) obj;
        if (location2 == null) {
        }
        location = location2;
        if (location == null) {
        }
        return result;
    }

    @DexIgnore
    public final List<AddressOfWeather> getWeatherAddressList() {
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            return addressDao2.getAllSavedAddress();
        }
        wg6.d("addressDao");
        throw null;
    }

    @DexIgnore
    public final void observerLocation(Context context, LocationListener locationListener, boolean z) {
        wg6.b(context, "context");
        wg6.b(locationListener, "listener");
        if (!z || !this.isSignificantObserved) {
            if (z || !this.isMonitoringObserved) {
                wv2 a = aw2.a(context);
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.k(1000);
                locationRequest.j(1000);
                locationRequest.d(100);
                locationRequest.a(z ? 300.0f : 50.0f);
                new bw2.a().a(locationRequest);
                if (w6.a(context, "android.permission.ACCESS_FINE_LOCATION") != 0 || w6.a(context, "android.permission.ACCESS_COARSE_LOCATION") != 0 || (Build.VERSION.SDK_INT >= 29 && w6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation permission missing");
                } else if (!LocationUtils.isLocationEnable(context)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation location disable");
                } else if (z) {
                    try {
                        FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start significantObserved");
                        this.mSignificantLocationListeners.add(locationListener);
                        a.a(locationRequest, this.mSignificantCallback, Looper.getMainLooper());
                        this.isSignificantObserved = true;
                        this.mSignificantClient = a;
                    } catch (ExecutionException e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.e(str, ".getLocationFromFuseLocationManager(), e1=" + e);
                    } catch (InterruptedException e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.e(str2, ".getLocationFromFuseLocationManager(), e2=" + e2);
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start monitoringObserved");
                    this.mMonitoringLocationListeners.add(locationListener);
                    a.a(locationRequest, this.mMonitoringCallback, Looper.getMainLooper());
                    this.isMonitoringObserved = true;
                    this.mMonitoringClient = a;
                }
            } else if (!this.mMonitoringLocationListeners.contains(locationListener)) {
                this.mMonitoringLocationListeners.add(locationListener);
            }
        } else if (!this.mSignificantLocationListeners.contains(locationListener)) {
            this.mSignificantLocationListeners.add(locationListener);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Object requestLocationUpdates(wv2 wv2, LocationRequest locationRequest, xe6<? super Location> xe6) {
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        wv2.a(locationRequest, new LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(mk6, wv2, locationRequest), Looper.getMainLooper());
        Object e = mk6.e();
        if (e == ff6.a()) {
            nf6.c(xe6);
        }
        return e;
    }

    @DexIgnore
    public final void saveWeatherAddress(AddressOfWeather addressOfWeather) {
        wg6.b(addressOfWeather, "addressOfWeather");
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            addressDao2.saveAddress(addressOfWeather);
        } else {
            wg6.d("addressDao");
            throw null;
        }
    }

    @DexIgnore
    public final void setAddressDao$app_fossilRelease(AddressDao addressDao2) {
        wg6.b(addressDao2, "<set-?>");
        this.addressDao = addressDao2;
    }

    @DexIgnore
    public final void unObserverLocation(LocationListener locationListener, boolean z) {
        wg6.b(locationListener, "listener");
        if (z) {
            this.mSignificantLocationListeners.remove(locationListener);
            if (this.isSignificantObserved && this.mSignificantLocationListeners.isEmpty()) {
                FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end significantObserved");
                wv2 wv2 = this.mSignificantClient;
                if (wv2 != null) {
                    wv2.a(this.mSignificantCallback);
                }
                this.isSignificantObserved = false;
                return;
            }
            return;
        }
        this.mMonitoringLocationListeners.remove(locationListener);
        if (this.isMonitoringObserved && this.mMonitoringLocationListeners.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end monitoringObserved");
            wv2 wv22 = this.mMonitoringClient;
            if (wv22 != null) {
                wv22.a(this.mMonitoringCallback);
            }
            this.isMonitoringObserved = false;
        }
    }
}
