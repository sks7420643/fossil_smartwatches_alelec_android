package com.portfolio.platform.data;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.jm4;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum NotificationType {
    CONTACT(2131887232),
    CODE_WORD(2131887231),
    CALL(2131887011),
    SMS(2131887012),
    EMAIL(2131887135),
    APP_FILTER(2131887230),
    FITNESS_GOAL_ACHIEVED(2131887233),
    APP_MODE(2131887228),
    OTHER(2131887226),
    CONTACT_EMPTY(2131887229),
    APP_EMPTY(2131887227);
    
    @DexIgnore
    public /* final */ int sectionTitleResId;

    @DexIgnore
    public NotificationType(int i) {
        this.sectionTitleResId = i;
    }

    @DexIgnore
    public static NotificationType find(String str) {
        if (!TextUtils.isEmpty(str)) {
            return valueOf(str);
        }
        return null;
    }

    @DexIgnore
    public int getSectionTitleResId() {
        return this.sectionTitleResId;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public String getSectionTitleString() {
        return jm4.a((Context) PortfolioApp.T, this.sectionTitleResId);
    }
}
