package com.portfolio.platform.data;

import com.fossil.rh4;
import com.fossil.vu3;
import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatUserinfo {
    @DexIgnore
    @vu3("city")
    public /* final */ String mCity;
    @DexIgnore
    @vu3("country")
    public /* final */ String mCountry;
    @DexIgnore
    @vu3("headimgurl")
    public /* final */ String mHeadimgurl;
    @DexIgnore
    @vu3("nickname")
    public /* final */ String mNickname;
    @DexIgnore
    @vu3("openid")
    public /* final */ String mOpenid;
    @DexIgnore
    @vu3("privilege")
    public /* final */ List<String> mPrivilege;
    @DexIgnore
    @vu3("province")
    public /* final */ String mProvince;
    @DexIgnore
    @vu3("sex")
    public /* final */ int mSex;
    @DexIgnore
    @vu3("unionId")
    public /* final */ String mUnionId;

    @DexIgnore
    public WechatUserinfo(String str, String str2, int i, String str3, String str4, String str5, String str6, List<String> list, String str7) {
        wg6.b(str, "mOpenid");
        wg6.b(str2, "mNickname");
        wg6.b(str3, "mCity");
        wg6.b(str4, "mProvince");
        wg6.b(str5, "mCountry");
        wg6.b(str6, "mHeadimgurl");
        wg6.b(list, "mPrivilege");
        wg6.b(str7, "mUnionId");
        this.mOpenid = str;
        this.mNickname = str2;
        this.mSex = i;
        this.mCity = str3;
        this.mProvince = str4;
        this.mCountry = str5;
        this.mHeadimgurl = str6;
        this.mPrivilege = list;
        this.mUnionId = str7;
    }

    @DexIgnore
    public final String getHeadimgurl() {
        return this.mHeadimgurl;
    }

    @DexIgnore
    public final String getSex() {
        int i = this.mSex;
        if (i == 1) {
            return rh4.MALE.toString();
        }
        if (i != 2) {
            return rh4.OTHER.toString();
        }
        return rh4.FEMALE.toString();
    }
}
