package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.b;
import com.fossil.ik4;
import com.fossil.qg6;
import com.fossil.wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityIntensities implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public double intense;
    @DexIgnore
    public double light;
    @DexIgnore
    public double moderate;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivityIntensities> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public ActivityIntensities createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new ActivityIntensities(parcel);
        }

        @DexIgnore
        public ActivityIntensities[] newArray(int i) {
            return new ActivityIntensities[i];
        }
    }

    @DexIgnore
    public ActivityIntensities(double d, double d2, double d3) {
        this.light = d;
        this.moderate = d2;
        this.intense = d3;
    }

    @DexIgnore
    public static /* synthetic */ ActivityIntensities copy$default(ActivityIntensities activityIntensities, double d, double d2, double d3, int i, Object obj) {
        if ((i & 1) != 0) {
            d = activityIntensities.light;
        }
        double d4 = d;
        if ((i & 2) != 0) {
            d2 = activityIntensities.moderate;
        }
        double d5 = d2;
        if ((i & 4) != 0) {
            d3 = activityIntensities.intense;
        }
        return activityIntensities.copy(d4, d5, d3);
    }

    @DexIgnore
    public final double component1() {
        return this.light;
    }

    @DexIgnore
    public final double component2() {
        return this.moderate;
    }

    @DexIgnore
    public final double component3() {
        return this.intense;
    }

    @DexIgnore
    public final ActivityIntensities copy(double d, double d2, double d3) {
        return new ActivityIntensities(d, d2, d3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityIntensities)) {
            return false;
        }
        ActivityIntensities activityIntensities = (ActivityIntensities) obj;
        return Double.compare(this.light, activityIntensities.light) == 0 && Double.compare(this.moderate, activityIntensities.moderate) == 0 && Double.compare(this.intense, activityIntensities.intense) == 0;
    }

    @DexIgnore
    public final double getIntense() {
        return this.intense;
    }

    @DexIgnore
    public final List<Integer> getIntensitiesInArray() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf((int) this.light));
        arrayList.add(Integer.valueOf((int) this.moderate));
        arrayList.add(Integer.valueOf((int) this.intense));
        return arrayList;
    }

    @DexIgnore
    public final double getLight() {
        return this.light;
    }

    @DexIgnore
    public final double getModerate() {
        return this.moderate;
    }

    @DexIgnore
    public int hashCode() {
        return (((b.a(this.light) * 31) + b.a(this.moderate)) * 31) + b.a(this.intense);
    }

    @DexIgnore
    public final void setIntense(double d) {
        this.intense = d;
    }

    @DexIgnore
    public final void setIntensity(double d) {
        int a = ik4.d.a((long) d);
        if (a == 0) {
            this.light += d;
        } else if (a == 1) {
            this.moderate += d;
        } else if (a == 2) {
            this.intense += d;
        }
    }

    @DexIgnore
    public final void setLight(double d) {
        this.light = d;
    }

    @DexIgnore
    public final void setModerate(double d) {
        this.moderate = d;
    }

    @DexIgnore
    public String toString() {
        return "ActivityIntensities(light=" + this.light + ", moderate=" + this.moderate + ", intense=" + this.intense + ")";
    }

    @DexIgnore
    public final void updateActivityIntensities(ActivityIntensities activityIntensities) {
        wg6.b(activityIntensities, "intensities");
        this.light += activityIntensities.light;
        this.moderate += activityIntensities.moderate;
        this.intense += activityIntensities.intense;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeDouble(this.light);
        parcel.writeDouble(this.moderate);
        parcel.writeDouble(this.intense);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ ActivityIntensities(double d, double d2, double d3, int i, qg6 qg6) {
        this(r2, (i & 2) != 0 ? 0.0d : d2, (i & 4) != 0 ? 0.0d : d3);
        double d4 = (i & 1) != 0 ? 0.0d : d;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityIntensities(Parcel parcel) {
        this(parcel.readDouble(), parcel.readDouble(), parcel.readDouble());
        wg6.b(parcel, "parcel");
    }

    @DexIgnore
    public ActivityIntensities() {
        this(0.0d, 0.0d, 0.0d);
    }
}
