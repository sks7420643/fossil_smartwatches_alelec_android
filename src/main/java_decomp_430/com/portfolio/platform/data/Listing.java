package com.portfolio.platform.data;

import androidx.lifecycle.LiveData;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.gg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Listing<T> {
    @DexIgnore
    public /* final */ LiveData<NetworkState> networkState;
    @DexIgnore
    public /* final */ LiveData<cf<T>> pagedList;
    @DexIgnore
    public /* final */ gg6<cd6> refresh;
    @DexIgnore
    public /* final */ gg6<cd6> retry;

    @DexIgnore
    public Listing(LiveData<cf<T>> liveData, LiveData<NetworkState> liveData2, gg6<cd6> gg6, gg6<cd6> gg62) {
        wg6.b(liveData, "pagedList");
        wg6.b(liveData2, "networkState");
        wg6.b(gg6, "refresh");
        wg6.b(gg62, "retry");
        this.pagedList = liveData;
        this.networkState = liveData2;
        this.refresh = gg6;
        this.retry = gg62;
    }

    @DexIgnore
    public static /* synthetic */ Listing copy$default(Listing listing, LiveData<cf<T>> liveData, LiveData<NetworkState> liveData2, gg6<cd6> gg6, gg6<cd6> gg62, int i, Object obj) {
        if ((i & 1) != 0) {
            liveData = listing.pagedList;
        }
        if ((i & 2) != 0) {
            liveData2 = listing.networkState;
        }
        if ((i & 4) != 0) {
            gg6 = listing.refresh;
        }
        if ((i & 8) != 0) {
            gg62 = listing.retry;
        }
        return listing.copy(liveData, liveData2, gg6, gg62);
    }

    @DexIgnore
    public final LiveData<cf<T>> component1() {
        return this.pagedList;
    }

    @DexIgnore
    public final LiveData<NetworkState> component2() {
        return this.networkState;
    }

    @DexIgnore
    public final gg6<cd6> component3() {
        return this.refresh;
    }

    @DexIgnore
    public final gg6<cd6> component4() {
        return this.retry;
    }

    @DexIgnore
    public final Listing<T> copy(LiveData<cf<T>> liveData, LiveData<NetworkState> liveData2, gg6<cd6> gg6, gg6<cd6> gg62) {
        wg6.b(liveData, "pagedList");
        wg6.b(liveData2, "networkState");
        wg6.b(gg6, "refresh");
        wg6.b(gg62, "retry");
        return new Listing<>(liveData, liveData2, gg6, gg62);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Listing)) {
            return false;
        }
        Listing listing = (Listing) obj;
        return wg6.a((Object) this.pagedList, (Object) listing.pagedList) && wg6.a((Object) this.networkState, (Object) listing.networkState) && wg6.a((Object) this.refresh, (Object) listing.refresh) && wg6.a((Object) this.retry, (Object) listing.retry);
    }

    @DexIgnore
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @DexIgnore
    public final LiveData<cf<T>> getPagedList() {
        return this.pagedList;
    }

    @DexIgnore
    public final gg6<cd6> getRefresh() {
        return this.refresh;
    }

    @DexIgnore
    public final gg6<cd6> getRetry() {
        return this.retry;
    }

    @DexIgnore
    public int hashCode() {
        LiveData<cf<T>> liveData = this.pagedList;
        int i = 0;
        int hashCode = (liveData != null ? liveData.hashCode() : 0) * 31;
        LiveData<NetworkState> liveData2 = this.networkState;
        int hashCode2 = (hashCode + (liveData2 != null ? liveData2.hashCode() : 0)) * 31;
        gg6<cd6> gg6 = this.refresh;
        int hashCode3 = (hashCode2 + (gg6 != null ? gg6.hashCode() : 0)) * 31;
        gg6<cd6> gg62 = this.retry;
        if (gg62 != null) {
            i = gg62.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Listing(pagedList=" + this.pagedList + ", networkState=" + this.networkState + ", refresh=" + this.refresh + ", retry=" + this.retry + ")";
    }
}
