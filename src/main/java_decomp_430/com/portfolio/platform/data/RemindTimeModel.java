package com.portfolio.platform.data;

import com.fossil.tu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeModel {
    @DexIgnore
    @tu3
    public int minutes;
    @DexIgnore
    @tu3
    public String remindTimeName;

    @DexIgnore
    public RemindTimeModel(String str, int i) {
        wg6.b(str, "remindTimeName");
        this.remindTimeName = str;
        this.minutes = i;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getRemindTimeName() {
        return this.remindTimeName;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setRemindTimeName(String str) {
        wg6.b(str, "<set-?>");
        this.remindTimeName = str;
    }
}
