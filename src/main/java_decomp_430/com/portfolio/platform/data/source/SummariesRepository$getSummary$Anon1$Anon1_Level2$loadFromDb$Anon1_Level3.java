package com.portfolio.platform.data.source;

import com.fossil.v3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon1;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummary$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3<I, O> implements v3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon1.Anon1_Level2 this$0;

    @DexIgnore
    public SummariesRepository$getSummary$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3(SummariesRepository$getSummary$Anon1.Anon1_Level2 anon1_Level2) {
        this.this$0 = anon1_Level2;
    }

    @DexIgnore
    public final ActivitySummary apply(ActivitySummary activitySummary) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - loadFromDb -- date=" + this.this$0.this$0.$date + ", summary=" + activitySummary);
        if (activitySummary != null) {
            activitySummary.setSteps(Math.max((double) this.this$0.this$0.this$0.mFitnessHelper.a(new Date()), activitySummary.getSteps()));
        }
        return activitySummary;
    }
}
