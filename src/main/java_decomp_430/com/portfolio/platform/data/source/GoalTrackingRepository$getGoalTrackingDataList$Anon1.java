package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.hh6;
import com.fossil.lc6;
import com.fossil.rd6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$getGoalTrackingDataList$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<GoalTrackingData>, ApiResponse<GoalEvent>> {
        @DexIgnore
        public /* final */ /* synthetic */ lc6 $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $offset;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getGoalTrackingDataList$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getGoalTrackingDataList$Anon1 goalTrackingRepository$getGoalTrackingDataList$Anon1, hh6 hh6, int i, lc6 lc6) {
            this.this$0 = goalTrackingRepository$getGoalTrackingDataList$Anon1;
            this.$offset = hh6;
            this.$limit = i;
            this.$downloadingDate = lc6;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ApiResponse<GoalEvent>>> xe6) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            lc6 lc6 = this.$downloadingDate;
            if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                date = this.this$0.$startDate;
            }
            String e = bk4.e(date);
            wg6.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            lc6 lc62 = this.$downloadingDate;
            if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                date2 = this.this$0.$endDate;
            }
            String e2 = bk4.e(date2);
            wg6.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiServiceV2$p.getGoalTrackingDataList(e, e2, this.$offset.element, this.$limit, xe6);
        }

        @DexIgnore
        public LiveData<List<GoalTrackingData>> loadFromDb() {
            GoalTrackingDao access$getMGoalTrackingDao$p = this.this$0.this$0.mGoalTrackingDao;
            GoalTrackingRepository$getGoalTrackingDataList$Anon1 goalTrackingRepository$getGoalTrackingDataList$Anon1 = this.this$0;
            return access$getMGoalTrackingDao$p.getGoalTrackingDataListLiveData(goalTrackingRepository$getGoalTrackingDataList$Anon1.$startDate, goalTrackingRepository$getGoalTrackingDataList$Anon1.$endDate);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ApiResponse<GoalEvent> apiResponse) {
            wg6.b(apiResponse, "item");
            Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<GoalEvent> apiResponse) {
            wg6.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getGoalTrackingDataList startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " saveCallResult onResponse: response = " + apiResponse);
            try {
                List<GoalEvent> list = apiResponse.get_items();
                ArrayList arrayList = new ArrayList(rd6.a(list, 10));
                for (GoalEvent goalTrackingData : list) {
                    GoalTrackingData goalTrackingData2 = goalTrackingData.toGoalTrackingData();
                    if (goalTrackingData2 != null) {
                        arrayList.add(goalTrackingData2);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                this.this$0.this$0.mGoalTrackingDao.upsertGoalTrackingDataList(yd6.d(arrayList));
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tag2 = GoalTrackingRepository.Companion.getTAG();
                local2.e(tag2, "getGoalTrackingDataList startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " exception=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<GoalTrackingData> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public GoalTrackingRepository$getGoalTrackingDataList$Anon1(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z) {
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1$Anon1_Level2, com.portfolio.platform.util.NetworkBoundResource] */
    public final LiveData<yx5<List<GoalTrackingData>>> apply(List<GoalTrackingData> list) {
        wg6.a((Object) list, "pendingList");
        lc6<Date, Date> calculateRangeDownload = GoalTrackingDataKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
        hh6 hh6 = new hh6();
        hh6.element = 0;
        return new Anon1_Level2(this, hh6, 300, calculateRangeDownload).asLiveData();
    }
}
