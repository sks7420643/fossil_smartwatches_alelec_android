package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryDataSourceFactory extends xe.b<Date, GoalTrackingSummary> {
    @DexIgnore
    public GoalTrackingSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ u04 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ vk4.a mListener;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingSummaryDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(date, "mCreatedDate");
        wg6.b(u04, "mAppExecutors");
        wg6.b(aVar, "mListener");
        wg6.b(calendar, "mStartCalendar");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = u04;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public xe<Date, GoalTrackingSummary> create() {
        this.localDataSource = new GoalTrackingSummaryLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDao, this.mGoalTrackingDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.localDataSource;
        if (goalTrackingSummaryLocalDataSource != null) {
            return goalTrackingSummaryLocalDataSource;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.localDataSource = goalTrackingSummaryLocalDataSource;
    }
}
