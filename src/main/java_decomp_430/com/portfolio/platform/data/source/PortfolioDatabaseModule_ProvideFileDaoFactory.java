package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideFileDaoFactory implements Factory<FileDao> {
    @DexIgnore
    public /* final */ Provider<FileDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFileDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FileDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFileDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FileDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideFileDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static FileDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FileDatabase> provider) {
        return proxyProvideFileDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static FileDao proxyProvideFileDao(PortfolioDatabaseModule portfolioDatabaseModule, FileDatabase fileDatabase) {
        FileDao provideFileDao = portfolioDatabaseModule.provideFileDao(fileDatabase);
        z76.a(provideFileDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideFileDao;
    }

    @DexIgnore
    public FileDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
