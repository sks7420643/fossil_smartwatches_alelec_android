package com.portfolio.platform.data.source.remote;

import com.fossil.ku3;
import com.fossil.ny6;
import com.fossil.rx6;
import com.fossil.xe6;
import com.fossil.zy6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GoogleApiService {
    @DexIgnore
    @ny6("geocode/json")
    Object getAddress(@zy6("latlng") String str, @zy6("language") String str2, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ny6("geocode/json")
    Object getAddressWithType(@zy6("latlng") String str, @zy6("result_type") String str2, xe6<? super rx6<ku3>> xe6);
}
