package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon2_Level3<I, O> implements v3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ LiveData $resultList;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1.Anon1_Level2 this$0;

    @DexIgnore
    public SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon2_Level3(SummariesRepository$getSummaries$Anon1.Anon1_Level2 anon1_Level2, LiveData liveData) {
        this.this$0 = anon1_Level2;
        this.$resultList = liveData;
    }

    @DexIgnore
    public final List<ActivitySummary> apply(List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday - resultList=" + ((List) this.$resultList.a()));
        wg6.a((Object) list, "activitySummaries");
        if (!list.isEmpty()) {
            ((ActivitySummary) yd6.f(list)).setSteps(Math.max((double) this.this$0.this$0.this$0.mFitnessHelper.a(new Date()), ((ActivitySummary) yd6.f(list)).getSteps()));
        }
        return list;
    }
}
