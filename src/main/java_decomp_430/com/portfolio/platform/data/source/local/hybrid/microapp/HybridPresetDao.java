package com.portfolio.platform.data.source.local.hybrid.microapp;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface HybridPresetDao {
    @DexIgnore
    void clearAllPresetBySerial(String str);

    @DexIgnore
    void clearAllPresetTable();

    @DexIgnore
    void clearAllRecommendPresetTable();

    @DexIgnore
    void deletePreset(String str);

    @DexIgnore
    HybridPreset getActivePresetBySerial(String str);

    @DexIgnore
    List<HybridPreset> getAllPendingPreset(String str);

    @DexIgnore
    List<HybridPreset> getAllPreset(String str);

    @DexIgnore
    LiveData<List<HybridPreset>> getAllPresetAsLiveData(String str);

    @DexIgnore
    HybridPreset getPresetById(String str);

    @DexIgnore
    List<HybridRecommendPreset> getRecommendPresetList(String str);

    @DexIgnore
    void removeAllDeletePinTypePreset();

    @DexIgnore
    void upsertPreset(HybridPreset hybridPreset);

    @DexIgnore
    void upsertPresetList(List<HybridPreset> list);

    @DexIgnore
    void upsertRecommendPresetList(List<HybridRecommendPreset> list);
}
