package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.hf6;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.response.ResponseKt;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmsRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final fu3 intArray2JsonArray(int[] iArr) {
        fu3 fu3 = new fu3();
        if (iArr == null) {
            iArr = new int[0];
        }
        Calendar instance = Calendar.getInstance();
        int length = iArr.length;
        int i = 0;
        while (i < length) {
            instance.set(7, iArr[i]);
            String displayName = instance.getDisplayName(7, 2, Locale.US);
            wg6.a((Object) displayName, "calendar.getDisplayName(\u2026Calendar.LONG, Locale.US)");
            if (displayName != null) {
                String substring = displayName.substring(0, 3);
                wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (substring != null) {
                    String lowerCase = substring.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    fu3.a(lowerCase);
                    i++;
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        return fu3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteAlarm(String str, xe6<? super ap4<Void>> xe6) {
        AlarmsRemoteDataSource$deleteAlarm$Anon1 alarmsRemoteDataSource$deleteAlarm$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof AlarmsRemoteDataSource$deleteAlarm$Anon1) {
            alarmsRemoteDataSource$deleteAlarm$Anon1 = (AlarmsRemoteDataSource$deleteAlarm$Anon1) xe6;
            int i2 = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$deleteAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$deleteAlarm$Anon1.result;
                Object a = ff6.a();
                i = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 alarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 = new AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1(this, str, (xe6) null);
                    alarmsRemoteDataSource$deleteAlarm$Anon1.L$0 = this;
                    alarmsRemoteDataSource$deleteAlarm$Anon1.L$1 = str;
                    alarmsRemoteDataSource$deleteAlarm$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1, alarmsRemoteDataSource$deleteAlarm$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) alarmsRemoteDataSource$deleteAlarm$Anon1.L$1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$deleteAlarm$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "deleteAlarm onResponse: response = " + ap4);
                if (!(ap4 instanceof cp4)) {
                    return new cp4((Object) null, false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        alarmsRemoteDataSource$deleteAlarm$Anon1 = new AlarmsRemoteDataSource$deleteAlarm$Anon1(this, xe6);
        Object obj2 = alarmsRemoteDataSource$deleteAlarm$Anon1.result;
        Object a2 = ff6.a();
        i = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "deleteAlarm onResponse: response = " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteAlarms(List<Alarm> list, xe6<? super ap4<Void>> xe6) {
        AlarmsRemoteDataSource$deleteAlarms$Anon1 alarmsRemoteDataSource$deleteAlarms$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof AlarmsRemoteDataSource$deleteAlarms$Anon1) {
            alarmsRemoteDataSource$deleteAlarms$Anon1 = (AlarmsRemoteDataSource$deleteAlarms$Anon1) xe6;
            int i2 = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$deleteAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$deleteAlarms$Anon1.result;
                Object a = ff6.a();
                i = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    fu3 fu3 = new fu3();
                    for (Alarm id : list) {
                        fu3.a(id.getId());
                    }
                    ku3.a("_ids", fu3);
                    AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 alarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1(this, ku3, (xe6) null);
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$0 = this;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$1 = list;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$2 = ku3;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$3 = fu3;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1, alarmsRemoteDataSource$deleteAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    fu3 fu32 = (fu3) alarmsRemoteDataSource$deleteAlarms$Anon1.L$3;
                    ku3 ku32 = (ku3) alarmsRemoteDataSource$deleteAlarms$Anon1.L$2;
                    List list2 = (List) alarmsRemoteDataSource$deleteAlarms$Anon1.L$1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$deleteAlarms$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "deleteAlarms onResponse: response = " + ap4);
                if (!(ap4 instanceof cp4)) {
                    return new cp4((Object) null, false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        alarmsRemoteDataSource$deleteAlarms$Anon1 = new AlarmsRemoteDataSource$deleteAlarms$Anon1(this, xe6);
        Object obj2 = alarmsRemoteDataSource$deleteAlarms$Anon1.result;
        Object a2 = ff6.a();
        i = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "deleteAlarms onResponse: response = " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getAlarms(xe6<? super ap4<List<Alarm>>> xe6) {
        AlarmsRemoteDataSource$getAlarms$Anon1 alarmsRemoteDataSource$getAlarms$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof AlarmsRemoteDataSource$getAlarms$Anon1) {
            alarmsRemoteDataSource$getAlarms$Anon1 = (AlarmsRemoteDataSource$getAlarms$Anon1) xe6;
            int i2 = alarmsRemoteDataSource$getAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$getAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$getAlarms$Anon1.result;
                Object a = ff6.a();
                i = alarmsRemoteDataSource$getAlarms$Anon1.label;
                List list = null;
                if (i != 0) {
                    nc6.a(obj);
                    AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1 alarmsRemoteDataSource$getAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1(this, (xe6) null);
                    alarmsRemoteDataSource$getAlarms$Anon1.L$0 = this;
                    alarmsRemoteDataSource$getAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$getAlarms$repoResponse$Anon1, alarmsRemoteDataSource$getAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$getAlarms$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "getAlarms onResponse: response = " + ap4);
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    ApiResponse apiResponse = (ApiResponse) cp4.a();
                    if (apiResponse != null) {
                        list = apiResponse.get_items();
                    }
                    return new cp4(list, cp4.b());
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        alarmsRemoteDataSource$getAlarms$Anon1 = new AlarmsRemoteDataSource$getAlarms$Anon1(this, xe6);
        Object obj2 = alarmsRemoteDataSource$getAlarms$Anon1.result;
        Object a2 = ff6.a();
        i = alarmsRemoteDataSource$getAlarms$Anon1.label;
        List list2 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getAlarms onResponse: response = " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object upsertAlarms(List<Alarm> list, xe6<? super ap4<List<Alarm>>> xe6) {
        AlarmsRemoteDataSource$upsertAlarms$Anon1 alarmsRemoteDataSource$upsertAlarms$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof AlarmsRemoteDataSource$upsertAlarms$Anon1) {
            alarmsRemoteDataSource$upsertAlarms$Anon1 = (AlarmsRemoteDataSource$upsertAlarms$Anon1) xe6;
            int i2 = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$upsertAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$upsertAlarms$Anon1.result;
                Object a = ff6.a();
                i = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    fu3 fu3 = new fu3();
                    for (Alarm next : list) {
                        ku3 ku3 = new ku3();
                        ku3.a("hour", hf6.a(next.getHour()));
                        ku3.a("minute", hf6.a(next.getMinute()));
                        ku3.a("id", next.getUri());
                        ku3.a("isRepeated", hf6.a(next.isRepeated()));
                        ku3.a("isActive", hf6.a(next.isActive()));
                        ku3.a(Explore.COLUMN_TITLE, next.getTitle());
                        ku3.a("message", next.getMessage());
                        ku3.a(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS, intArray2JsonArray(next.getDays()));
                        fu3.a(ku3);
                    }
                    ku3 ku32 = new ku3();
                    ku32.a(CloudLogWriter.ITEMS_PARAM, fu3);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "upsertAlarms: body " + ku32);
                    AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 alarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1(this, ku32, (xe6) null);
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$0 = this;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$1 = list;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$2 = fu3;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$3 = ku32;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1, alarmsRemoteDataSource$upsertAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku33 = (ku3) alarmsRemoteDataSource$upsertAlarms$Anon1.L$3;
                    fu3 fu32 = (fu3) alarmsRemoteDataSource$upsertAlarms$Anon1.L$2;
                    List list2 = (List) alarmsRemoteDataSource$upsertAlarms$Anon1.L$1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$upsertAlarms$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "upsertAlarms onResponse: response = " + ap4);
                if (!(ap4 instanceof cp4)) {
                    ApiResponse apiResponse = (ApiResponse) ((cp4) ap4).a();
                    return new cp4(apiResponse != null ? apiResponse.get_items() : null, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        alarmsRemoteDataSource$upsertAlarms$Anon1 = new AlarmsRemoteDataSource$upsertAlarms$Anon1(this, xe6);
        Object obj2 = alarmsRemoteDataSource$upsertAlarms$Anon1.result;
        Object a2 = ff6.a();
        i = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local22.d(str22, "upsertAlarms onResponse: response = " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }
}
