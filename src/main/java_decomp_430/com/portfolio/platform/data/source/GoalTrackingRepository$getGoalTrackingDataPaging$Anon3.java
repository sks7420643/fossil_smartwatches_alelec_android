package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.gg6;
import com.fossil.vk4;
import com.fossil.xg6;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$getGoalTrackingDataPaging$Anon3 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataPaging$Anon3(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
        super(0);
        this.$sourceFactory = goalTrackingDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        vk4 mHelper;
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = (GoalTrackingDataLocalDataSource) this.$sourceFactory.getSourceLiveData().a();
        if (goalTrackingDataLocalDataSource != null && (mHelper = goalTrackingDataLocalDataSource.getMHelper()) != null) {
            mHelper.b();
        }
    }
}
