package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.gg6;
import com.fossil.vk4;
import com.fossil.xg6;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummariesPaging$Anon3 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummariesPaging$Anon3(ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = activitySummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        vk4 mHelper;
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = (ActivitySummaryLocalDataSource) this.$sourceFactory.getSourceLiveData().a();
        if (activitySummaryLocalDataSource != null && (mHelper = activitySummaryLocalDataSource.getMHelper()) != null) {
            mHelper.b();
        }
    }
}
