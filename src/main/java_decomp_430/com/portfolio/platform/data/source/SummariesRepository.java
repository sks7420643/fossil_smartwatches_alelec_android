package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.cp4;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.ik4;
import com.fossil.ji;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.qj4;
import com.fossil.sd;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.ze;
import com.fossil.zo4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "SummariesRepository";
    @DexIgnore
    public /* final */ ActivitySummaryDao mActivitySummaryDao;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ ik4 mFitnessHelper;
    @DexIgnore
    public List<ActivitySummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public SummariesRepository(ApiServiceV2 apiServiceV2, ActivitySummaryDao activitySummaryDao, FitnessDataDao fitnessDataDao, FitnessDatabase fitnessDatabase, ik4 ik4) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        wg6.b(activitySummaryDao, "mActivitySummaryDao");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(fitnessDatabase, "mFitnessDatabase");
        wg6.b(ik4, "mFitnessHelper");
        this.mApiServiceV2 = apiServiceV2;
        this.mActivitySummaryDao = activitySummaryDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mFitnessHelper = ik4;
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mActivitySummaryDao.deleteAllActivitySummaries();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, xe6<? super ap4<ActivityRecommendedGoals>> xe6) {
        SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon1;
        int i4;
        ap4 ap4;
        xe6<? super ap4<ActivityRecommendedGoals>> xe62 = xe6;
        if (xe62 instanceof SummariesRepository$downloadRecommendedGoals$Anon1) {
            summariesRepository$downloadRecommendedGoals$Anon1 = (SummariesRepository$downloadRecommendedGoals$Anon1) xe62;
            int i5 = summariesRepository$downloadRecommendedGoals$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                summariesRepository$downloadRecommendedGoals$Anon1.label = i5 - Integer.MIN_VALUE;
                SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon12 = summariesRepository$downloadRecommendedGoals$Anon1;
                Object obj = summariesRepository$downloadRecommendedGoals$Anon12.result;
                Object a = ff6.a();
                i4 = summariesRepository$downloadRecommendedGoals$Anon12.label;
                if (i4 != 0) {
                    nc6.a(obj);
                    SummariesRepository$downloadRecommendedGoals$response$Anon1 summariesRepository$downloadRecommendedGoals$response$Anon1 = new SummariesRepository$downloadRecommendedGoals$response$Anon1(this, i, i2, i3, str, (xe6) null);
                    summariesRepository$downloadRecommendedGoals$Anon12.L$0 = this;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$0 = i;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$1 = i2;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$2 = i3;
                    summariesRepository$downloadRecommendedGoals$Anon12.L$1 = str;
                    summariesRepository$downloadRecommendedGoals$Anon12.label = 1;
                    obj = ResponseKt.a(summariesRepository$downloadRecommendedGoals$response$Anon1, summariesRepository$downloadRecommendedGoals$Anon12);
                    if (obj == a) {
                        return a;
                    }
                } else if (i4 == 1) {
                    String str2 = (String) summariesRepository$downloadRecommendedGoals$Anon12.L$1;
                    int i6 = summariesRepository$downloadRecommendedGoals$Anon12.I$2;
                    int i7 = summariesRepository$downloadRecommendedGoals$Anon12.I$1;
                    int i8 = summariesRepository$downloadRecommendedGoals$Anon12.I$0;
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$downloadRecommendedGoals$Anon12.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    Object a2 = ((cp4) ap4).a();
                    if (a2 != null) {
                        return new cp4((ActivityRecommendedGoals) a2, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null);
                } else {
                    throw new kc6();
                }
            }
        }
        summariesRepository$downloadRecommendedGoals$Anon1 = new SummariesRepository$downloadRecommendedGoals$Anon1(this, xe62);
        SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon122 = summariesRepository$downloadRecommendedGoals$Anon1;
        Object obj2 = summariesRepository$downloadRecommendedGoals$Anon122.result;
        Object a3 = ff6.a();
        i4 = summariesRepository$downloadRecommendedGoals$Anon122.label;
        if (i4 != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object fetchActivitySettings(xe6<? super ap4<ActivitySettings>> xe6) {
        SummariesRepository$fetchActivitySettings$Anon1 summariesRepository$fetchActivitySettings$Anon1;
        int i;
        SummariesRepository summariesRepository;
        ap4 ap4;
        String message;
        if (xe6 instanceof SummariesRepository$fetchActivitySettings$Anon1) {
            summariesRepository$fetchActivitySettings$Anon1 = (SummariesRepository$fetchActivitySettings$Anon1) xe6;
            int i2 = summariesRepository$fetchActivitySettings$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$fetchActivitySettings$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$fetchActivitySettings$Anon1.result;
                Object a = ff6.a();
                i = summariesRepository$fetchActivitySettings$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchActivitySettings");
                    SummariesRepository$fetchActivitySettings$response$Anon1 summariesRepository$fetchActivitySettings$response$Anon1 = new SummariesRepository$fetchActivitySettings$response$Anon1(this, (xe6) null);
                    summariesRepository$fetchActivitySettings$Anon1.L$0 = this;
                    summariesRepository$fetchActivitySettings$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$fetchActivitySettings$response$Anon1, summariesRepository$fetchActivitySettings$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    summariesRepository = (SummariesRepository) summariesRepository$fetchActivitySettings$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null) {
                        summariesRepository.saveActivitySettingsToDB$app_fossilRelease(new Date(), (ActivitySettings) cp4.a());
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchActivitySettings - Failure -- code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(", message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str = c2.getUserMessage();
                        }
                    } else {
                        str = message;
                    }
                    if (str == null) {
                        str = "";
                    }
                    sb.append(str);
                    local.e(TAG, sb.toString());
                }
                return ap4;
            }
        }
        summariesRepository$fetchActivitySettings$Anon1 = new SummariesRepository$fetchActivitySettings$Anon1(this, xe6);
        Object obj2 = summariesRepository$fetchActivitySettings$Anon1.result;
        Object a2 = ff6.a();
        i = summariesRepository$fetchActivitySettings$Anon1.label;
        String str2 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchActivityStatistic(xe6<? super ActivityStatistic> xe6) {
        SummariesRepository$fetchActivityStatistic$Anon1 summariesRepository$fetchActivityStatistic$Anon1;
        int i;
        SummariesRepository summariesRepository;
        ap4 ap4;
        if (xe6 instanceof SummariesRepository$fetchActivityStatistic$Anon1) {
            summariesRepository$fetchActivityStatistic$Anon1 = (SummariesRepository$fetchActivityStatistic$Anon1) xe6;
            int i2 = summariesRepository$fetchActivityStatistic$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$fetchActivityStatistic$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$fetchActivityStatistic$Anon1.result;
                Object a = ff6.a();
                i = summariesRepository$fetchActivityStatistic$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    SummariesRepository$fetchActivityStatistic$response$Anon1 summariesRepository$fetchActivityStatistic$response$Anon1 = new SummariesRepository$fetchActivityStatistic$response$Anon1(this, (xe6) null);
                    summariesRepository$fetchActivityStatistic$Anon1.L$0 = this;
                    summariesRepository$fetchActivityStatistic$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$fetchActivityStatistic$response$Anon1, summariesRepository$fetchActivityStatistic$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    summariesRepository = (SummariesRepository) summariesRepository$fetchActivityStatistic$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null) {
                        summariesRepository.mActivitySummaryDao.upsertActivityStatistic((ActivityStatistic) cp4.a());
                        return cp4.a();
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getActivityStatisticAwait - Failure -- code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(", message=");
                    ServerError c = zo4.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local.e(TAG, sb.toString());
                }
                return null;
            }
        }
        summariesRepository$fetchActivityStatistic$Anon1 = new SummariesRepository$fetchActivityStatistic$Anon1(this, xe6);
        Object obj2 = summariesRepository$fetchActivityStatistic$Anon1.result;
        Object a2 = ff6.a();
        i = summariesRepository$fetchActivityStatistic$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$Anon1] */
    public final LiveData<yx5<ActivitySettings>> getActivitySettings() {
        return new SummariesRepository$getActivitySettings$Anon1(this).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1] */
    public final LiveData<yx5<ActivityStatistic>> getActivityStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getActivityStatistic - shouldFetch=" + z);
        return new SummariesRepository$getActivityStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getActivityStatisticAwait(xe6<? super ActivityStatistic> xe6) {
        SummariesRepository$getActivityStatisticAwait$Anon1 summariesRepository$getActivityStatisticAwait$Anon1;
        int i;
        if (xe6 instanceof SummariesRepository$getActivityStatisticAwait$Anon1) {
            summariesRepository$getActivityStatisticAwait$Anon1 = (SummariesRepository$getActivityStatisticAwait$Anon1) xe6;
            int i2 = summariesRepository$getActivityStatisticAwait$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$getActivityStatisticAwait$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$getActivityStatisticAwait$Anon1.result;
                Object a = ff6.a();
                i = summariesRepository$getActivityStatisticAwait$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "getActivityStatisticAwait");
                    ActivityStatistic activityStatistic = this.mActivitySummaryDao.getActivityStatistic();
                    if (activityStatistic != null) {
                        return activityStatistic;
                    }
                    summariesRepository$getActivityStatisticAwait$Anon1.L$0 = this;
                    summariesRepository$getActivityStatisticAwait$Anon1.label = 1;
                    obj = fetchActivityStatistic(summariesRepository$getActivityStatisticAwait$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$getActivityStatisticAwait$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (ActivityStatistic) obj;
            }
        }
        summariesRepository$getActivityStatisticAwait$Anon1 = new SummariesRepository$getActivityStatisticAwait$Anon1(this, xe6);
        Object obj2 = summariesRepository$getActivityStatisticAwait$Anon1.result;
        Object a2 = ff6.a();
        i = summariesRepository$getActivityStatisticAwait$Anon1.label;
        if (i != 0) {
        }
        return (ActivityStatistic) obj2;
    }

    @DexIgnore
    public final ActivitySettings getCurrentActivitySettings() {
        ActivitySettings activitySetting = this.mActivitySummaryDao.getActivitySetting();
        if (activitySetting == null) {
            return new ActivitySettings(5000, 140, 30);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getCurrentActivitySettings - " + "stepGoal=" + activitySetting.getCurrentStepGoal() + ", caloriesGoal=" + activitySetting.getCurrentCaloriesGoal() + ", " + "activeTimeGoal=" + activitySetting.getCurrentActiveTimeGoal());
        return new ActivitySettings(activitySetting.getCurrentStepGoal(), activitySetting.getCurrentCaloriesGoal(), activitySetting.getCurrentActiveTimeGoal());
    }

    @DexIgnore
    public final LiveData<yx5<List<ActivitySummary>>> getSummaries(Date date, Date date2, boolean z) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummaries - startDate=" + date + ", endDate=" + date2);
        LiveData<yx5<List<ActivitySummary>>> b = sd.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SummariesRepository$getSummaries$Anon1(this, date, date2, z));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<ActivitySummary> getSummariesPaging(SummariesRepository summariesRepository, ik4 ik4, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, Date date, u04 u04, vk4.a aVar) {
        Date date2 = date;
        wg6.b(summariesRepository, "summariesRepository");
        wg6.b(ik4, "fitnessHelper");
        FitnessDataRepository fitnessDataRepository2 = fitnessDataRepository;
        wg6.b(fitnessDataRepository2, "fitnessDataRepository");
        ActivitySummaryDao activitySummaryDao2 = activitySummaryDao;
        wg6.b(activitySummaryDao2, "activitySummaryDao");
        FitnessDatabase fitnessDatabase2 = fitnessDatabase;
        wg6.b(fitnessDatabase2, "fitnessDatabase");
        wg6.b(date2, "createdDate");
        u04 u042 = u04;
        wg6.b(u042, "appExecutors");
        vk4.a aVar2 = aVar;
        wg6.b(aVar2, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getSummariesPaging - createdDate=");
        sb.append(date2);
        sb.append("  DBNAME= ");
        ji openHelper = this.mFitnessDatabase.getOpenHelper();
        wg6.a((Object) openHelper, "mFitnessDatabase.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d(TAG, sb.toString());
        ActivitySummaryLocalDataSource.Companion companion = ActivitySummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        wg6.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date2);
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory = new ActivitySummaryDataSourceFactory(summariesRepository, ik4, fitnessDataRepository2, activitySummaryDao2, fitnessDatabase2, date2, u042, aVar2, instance2);
        this.mSourceFactoryList.add(activitySummaryDataSourceFactory);
        cf.h.a aVar3 = new cf.h.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        cf.h a = aVar3.a();
        wg6.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new ze(activitySummaryDataSourceFactory, a).a();
        wg6.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = sd.b(activitySummaryDataSourceFactory.getSourceLiveData(), SummariesRepository$getSummariesPaging$Anon1.INSTANCE);
        wg6.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new SummariesRepository$getSummariesPaging$Anon2(activitySummaryDataSourceFactory), new SummariesRepository$getSummariesPaging$Anon3(activitySummaryDataSourceFactory));
    }

    @DexIgnore
    public final LiveData<yx5<ActivitySummary>> getSummary(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummary - date=" + date);
        LiveData<yx5<ActivitySummary>> b = sd.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date), new SummariesRepository$getSummary$Anon1(this, date));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insertFromDevice(List<ActivitySummary> list) {
        wg6.b(list, "summaries");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "insertFromDevice: summaries = " + list.size());
        this.mActivitySummaryDao.insertActivitySummaries(list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v19, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.util.Date} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    public final Object loadSummaries(Date date, Date date2, xe6<? super ap4<ku3>> xe6) {
        SummariesRepository$loadSummaries$Anon1 summariesRepository$loadSummaries$Anon1;
        int i;
        SummariesRepository summariesRepository;
        ap4 ap4;
        String message;
        List<FitnessDayData> list;
        if (xe6 instanceof SummariesRepository$loadSummaries$Anon1) {
            summariesRepository$loadSummaries$Anon1 = (SummariesRepository$loadSummaries$Anon1) xe6;
            int i2 = summariesRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$loadSummaries$Anon1.result;
                Object a = ff6.a();
                i = summariesRepository$loadSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadSummaries - startDate=");
                    sb.append(date);
                    sb.append(", endDate=");
                    sb.append(date2);
                    sb.append(" DBNAME= ");
                    ji openHelper = this.mFitnessDatabase.getOpenHelper();
                    wg6.a((Object) openHelper, "mFitnessDatabase.openHelper");
                    sb.append(openHelper.getDatabaseName());
                    local.d(TAG, sb.toString());
                    SummariesRepository$loadSummaries$response$Anon1 summariesRepository$loadSummaries$response$Anon1 = new SummariesRepository$loadSummaries$response$Anon1(this, date, date2, (xe6) null);
                    summariesRepository$loadSummaries$Anon1.L$0 = this;
                    summariesRepository$loadSummaries$Anon1.L$1 = date;
                    summariesRepository$loadSummaries$Anon1.L$2 = date2;
                    summariesRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$loadSummaries$response$Anon1, summariesRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    date2 = summariesRepository$loadSummaries$Anon1.L$2;
                    date = (Date) summariesRepository$loadSummaries$Anon1.L$1;
                    summariesRepository = (SummariesRepository) summariesRepository$loadSummaries$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null && !cp4.b()) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            du3 du3 = new du3();
                            du3.a(Date.class, new GsonConvertDate());
                            du3.a(DateTime.class, new GsonConvertDateTime());
                            ApiResponse apiResponse = (ApiResponse) du3.a().a(((ku3) ((cp4) ap4).a()).toString(), new SummariesRepository$loadSummaries$Anon2().getType());
                            if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                                for (FitnessDayData activitySummary : list) {
                                    ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                                    wg6.a((Object) activitySummary2, "it.toActivitySummary()");
                                    arrayList.add(activitySummary2);
                                }
                            }
                            List<FitnessDataWrapper> fitnessData = summariesRepository.mFitnessDataDao.getFitnessData(date, date2);
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("loadSummaries fitnessDataSize ");
                            sb2.append(fitnessData.size());
                            sb2.append(" from ");
                            sb2.append(date);
                            sb2.append(" to ");
                            sb2.append(date2);
                            sb2.append(" DBNAME=");
                            ji openHelper2 = summariesRepository.mFitnessDatabase.getOpenHelper();
                            wg6.a((Object) openHelper2, "mFitnessDatabase.openHelper");
                            sb2.append(openHelper2.getDatabaseName());
                            local2.d(TAG, sb2.toString());
                            if (fitnessData.isEmpty()) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                local3.d(TAG, "loadSummaries fitness data file is empty, add new summaries from server" + arrayList);
                                summariesRepository.mActivitySummaryDao.upsertActivitySummaries(arrayList);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("loadSummaries - e=");
                            e.printStackTrace();
                            sb3.append(cd6.a);
                            local4.d(TAG, sb3.toString());
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("loadSummaries - Failure -- code=");
                    zo4 zo4 = (zo4) ap4;
                    sb4.append(zo4.a());
                    sb4.append(", message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str = c2.getUserMessage();
                        }
                    } else {
                        str = message;
                    }
                    if (str == null) {
                        str = "";
                    }
                    sb4.append(str);
                    local5.d(TAG, sb4.toString());
                }
                return ap4;
            }
        }
        summariesRepository$loadSummaries$Anon1 = new SummariesRepository$loadSummaries$Anon1(this, xe6);
        Object obj2 = summariesRepository$loadSummaries$Anon1.result;
        Object a2 = ff6.a();
        i = summariesRepository$loadSummaries$Anon1.label;
        String str2 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    public final Object pushActivitySettingsToServer(ActivitySettings activitySettings, xe6<? super cd6> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "pushActivitySettingsToServer - settings=" + activitySettings);
        du3 du3 = new du3();
        du3.b(new qj4());
        JsonElement b = du3.a().b(activitySettings);
        wg6.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
        ku3 d = b.d();
        ApiServiceV2 apiServiceV2 = this.mApiServiceV2;
        wg6.a((Object) d, "jsonObject");
        Object updateActivitySetting = apiServiceV2.updateActivitySetting(d, xe6);
        if (updateActivitySetting == ff6.a()) {
            return updateActivitySetting;
        }
        return cd6.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (ActivitySummaryDataSourceFactory localDataSource : this.mSourceFactoryList) {
            ActivitySummaryLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    public final void saveActivitySettingsToDB$app_fossilRelease(Date date, ActivitySettings activitySettings) {
        Date date2 = date;
        ActivitySettings activitySettings2 = activitySettings;
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        wg6.b(activitySettings2, "activitySettings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "saveActivitySettingsToDB - date=" + date2 + ", stepGoal=" + activitySettings.getCurrentStepGoal() + ", " + "caloriesGoal=" + activitySettings.getCurrentCaloriesGoal() + ", activeTimeGoal=" + activitySettings.getCurrentActiveTimeGoal());
        ActivitySummary activitySummary = this.mActivitySummaryDao.getActivitySummary(date2);
        if (activitySummary == null) {
            DateTime dateTime = new DateTime();
            TimeZone timeZone = dateTime.getZone().toTimeZone();
            int year = dateTime.getYear();
            int monthOfYear = dateTime.getMonthOfYear();
            int dayOfMonth = dateTime.getDayOfMonth();
            wg6.a((Object) timeZone, "timeZone");
            activitySummary = new ActivitySummary(year, monthOfYear, dayOfMonth, timeZone.getID(), Integer.valueOf(timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0), 0.0d, 0.0d, 0.0d, qd6.d(0, 0, 0), 0, 0, 0, 0, 7680, (qg6) null);
        }
        activitySummary.setStepGoal(activitySettings.getCurrentStepGoal());
        activitySummary.setCaloriesGoal(activitySettings.getCurrentCaloriesGoal());
        activitySummary.setActiveTimeGoal(activitySettings.getCurrentActiveTimeGoal());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "updateDb activitySetting " + activitySettings2);
        this.mActivitySummaryDao.upsertActivitySettings(activitySettings2);
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d(TAG, "updateDb activitySummary " + activitySummary);
        this.mActivitySummaryDao.upsertActivitySummary(activitySummary);
        ActivitySummary activitySummary2 = this.mActivitySummaryDao.getActivitySummary(date2);
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        local4.d(TAG, "after upsertDb summary " + activitySummary2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1] */
    public final LiveData<yx5<ActivitySettings>> updateActivitySettings(ActivitySettings activitySettings) {
        wg6.b(activitySettings, "activitySettings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "updateActivitySettings - settings=" + activitySettings);
        return new SummariesRepository$updateActivitySettings$Anon1(this, activitySettings).asLiveData();
    }

    @DexIgnore
    public final void upsertRecommendGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        wg6.b(activityRecommendedGoals, "recommendedGoals");
        this.mActivitySummaryDao.upsertActivityRecommendedGoals(activityRecommendedGoals);
    }

    @DexIgnore
    public final ActivitySummary getSummary(Calendar calendar) {
        wg6.b(calendar, HardwareLog.COLUMN_DATE);
        ActivitySummaryDao activitySummaryDao = this.mActivitySummaryDao;
        Date time = calendar.getTime();
        wg6.a((Object) time, "date.time");
        return activitySummaryDao.getActivitySummary(time);
    }
}
