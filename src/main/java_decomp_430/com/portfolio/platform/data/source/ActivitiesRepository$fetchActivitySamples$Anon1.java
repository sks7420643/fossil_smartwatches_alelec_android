package com.portfolio.platform.data.source;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.ActivitiesRepository", f = "ActivitiesRepository.kt", l = {156, 170}, m = "fetchActivitySamples")
public final class ActivitiesRepository$fetchActivitySamples$Anon1 extends jf6 {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$Anon1(ActivitiesRepository activitiesRepository, xe6 xe6) {
        super(xe6);
        this.this$0 = activitiesRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.fetchActivitySamples((Date) null, (Date) null, 0, 0, this);
    }
}
