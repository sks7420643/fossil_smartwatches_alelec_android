package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.wg6;
import com.fossil.zf;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDifference extends zf.d<WorkoutSession> {
    @DexIgnore
    public boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        wg6.b(workoutSession, "oldItem");
        wg6.b(workoutSession2, "newItem");
        return wg6.a((Object) workoutSession, (Object) workoutSession2);
    }

    @DexIgnore
    public boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        wg6.b(workoutSession, "oldItem");
        wg6.b(workoutSession2, "newItem");
        return wg6.a((Object) workoutSession.getId(), (Object) workoutSession2.getId());
    }
}
