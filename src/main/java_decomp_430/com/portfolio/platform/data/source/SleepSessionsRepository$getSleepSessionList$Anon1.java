package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.du3;
import com.fossil.hh6;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.response.sleep.SleepSessionParse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionsRepository$getSleepSessionList$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<MFSleepSession>, ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ lc6 $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $offset;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository$getSleepSessionList$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(SleepSessionsRepository$getSleepSessionList$Anon1 sleepSessionsRepository$getSleepSessionList$Anon1, hh6 hh6, int i, lc6 lc6) {
            this.this$0 = sleepSessionsRepository$getSleepSessionList$Anon1;
            this.$offset = hh6;
            this.$limit = i;
            this.$downloadingDate = lc6;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ku3>> xe6) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            lc6 lc6 = this.$downloadingDate;
            if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                date = this.this$0.$startDate;
            }
            String e = bk4.e(date);
            wg6.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            lc6 lc62 = this.$downloadingDate;
            if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                date2 = this.this$0.$endDate;
            }
            String e2 = bk4.e(date2);
            wg6.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getSleepSessions(e, e2, this.$offset.element, this.$limit, xe6);
        }

        @DexIgnore
        public LiveData<List<MFSleepSession>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getSleepSessionList: startMilli = ");
            Date date = this.this$0.$startDate;
            wg6.a((Object) date, "startDate");
            sb.append(date.getTime());
            sb.append(", endMilli = ");
            Date date2 = this.this$0.$endDate;
            wg6.a((Object) date2, "endDate");
            sb.append(date2.getTime());
            local.d(tAG$app_fossilRelease, sb.toString());
            SleepDao access$getMSleepDao$p = this.this$0.this$0.mSleepDao;
            Date date3 = this.this$0.$startDate;
            wg6.a((Object) date3, "startDate");
            long time = date3.getTime();
            Date date4 = this.this$0.$endDate;
            wg6.a((Object) date4, "endDate");
            return access$getMSleepDao$p.getSleepSessionsLiveData(time, date4.getTime());
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "getSleepSessionList onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ku3 ku3) {
            wg6.b(ku3, "item");
            du3 du3 = new du3();
            du3.a(DateTime.class, new GsonConvertDateTime());
            du3.a(Date.class, new GsonConverterShortDate());
            Range range = ((ApiResponse) du3.a().a(ku3.toString(), new SleepSessionsRepository$getSleepSessionList$Anon1$Anon1_Level2$processContinueFetching$sleepSessionList$Anon1_Level3().getType())).get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "getSleepSessionList getActivityList processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ku3 ku3) {
            List<SleepSessionParse> list;
            wg6.b(ku3, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getSleepSessionList saveCallResult onResponse: response = " + ku3);
            ArrayList arrayList = new ArrayList();
            try {
                du3 du3 = new du3();
                du3.a(DateTime.class, new GsonConvertDateTime());
                du3.a(Date.class, new GsonConverterShortDate());
                ApiResponse apiResponse = (ApiResponse) du3.a().a(ku3.toString(), new SleepSessionsRepository$getSleepSessionList$Anon1$Anon1_Level2$saveCallResult$Anon1_Level3().getType());
                if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                    for (SleepSessionParse mfSleepSessionBySleepSessionParse : list) {
                        arrayList.add(mfSleepSessionBySleepSessionParse.getMfSleepSessionBySleepSessionParse());
                    }
                }
                this.this$0.this$0.insert$app_fossilRelease(arrayList);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease2 = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getSleepSessionList saveCallResult exception=");
                e.printStackTrace();
                sb.append(cd6.a);
                local2.e(tAG$app_fossilRelease2, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<MFSleepSession> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SleepSessionsRepository$getSleepSessionList$Anon1(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, boolean z) {
        this.this$0 = sleepSessionsRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1$Anon1_Level2] */
    public final LiveData<yx5<List<MFSleepSession>>> apply(List<FitnessDataWrapper> list) {
        hh6 hh6 = new hh6();
        hh6.element = 0;
        wg6.a((Object) list, "fitnessDataList");
        Date date = this.$startDate;
        wg6.a((Object) date, "startDate");
        Date date2 = this.$endDate;
        wg6.a((Object) date2, "endDate");
        return new Anon1_Level2(this, hh6, 300, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
    }
}
