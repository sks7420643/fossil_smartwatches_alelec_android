package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.du3;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ku3;
import com.fossil.ll6;
import com.fossil.qj4;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$updateActivitySettings$Anon1 extends tx5<ActivitySettings, ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $activitySettings;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    public SummariesRepository$updateActivitySettings$Anon1(SummariesRepository summariesRepository, ActivitySettings activitySettings) {
        this.this$0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    public Object createCall(xe6<? super rx6<ActivitySettings>> xe6) {
        du3 du3 = new du3();
        du3.b(new qj4());
        JsonElement b = du3.a().b(this.$activitySettings);
        wg6.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
        ku3 d = b.d();
        ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
        wg6.a((Object) d, "jsonObject");
        return access$getMApiServiceV2$p.updateActivitySetting(d, xe6);
    }

    @DexIgnore
    public LiveData<ActivitySettings> loadFromDb() {
        return this.this$0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "updateActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(ActivitySettings activitySettings) {
        wg6.b(activitySettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "updateActivitySettings - saveCallResult -- item=" + activitySettings);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1_Level2(this, activitySettings, (xe6) null), 3, (Object) null);
    }
}
