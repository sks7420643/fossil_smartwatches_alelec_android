package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.fossil.zq6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRemoteDataSource {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore
    public WatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "api");
        this.api = apiServiceV2;
        String simpleName = WatchFaceRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchFaceRemoteDataSource::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final Object downloadWatchFaceFile(String str, String str2, xe6<? super ap4<zq6>> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, "download: " + str + " path: " + str2);
        return ResponseKt.a(new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this, str, (xe6) null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getWatchFacesFromServer(String str, xe6<? super ap4<ArrayList<WatchFace>>> xe6) {
        WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1;
        int i;
        WatchFaceRemoteDataSource watchFaceRemoteDataSource;
        ap4 ap4;
        if (xe6 instanceof WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) {
            watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 = (WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) xe6;
            int i2 = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.result;
                Object a = ff6.a();
                i = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 watchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 = new WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1(this, str, (xe6) null);
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$0 = this;
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$1 = str;
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label = 1;
                    obj = ResponseKt.a(watchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1, watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchFaceRemoteDataSource = this;
                } else if (i == 1) {
                    String str2 = (String) watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$1;
                    watchFaceRemoteDataSource = (WatchFaceRemoteDataSource) watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = watchFaceRemoteDataSource.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("data: ");
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        sb.append(((ApiResponse) a2).get_items());
                        sb.append(" - isFromCache: ");
                        sb.append(cp4.b());
                        local.d(str3, sb.toString());
                        List list = ((ApiResponse) cp4.a()).get_items();
                        if (!(list instanceof ArrayList)) {
                            list = null;
                        }
                        return new cp4((ArrayList) list, cp4.b());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 = new WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1(this, xe6);
        Object obj2 = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.result;
        Object a3 = ff6.a();
        i = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
