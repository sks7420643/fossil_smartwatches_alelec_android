package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.yd;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseLoader<T> extends yd<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return BaseLoader.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = BaseLoader.class.getSimpleName();
        wg6.a((Object) simpleName, "BaseLoader::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseLoader(Context context) {
        super(context);
        if (context != null) {
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void deliverResult(T t) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .deliverResult data=" + t + ", isReset=" + isReset() + ". isStarted=" + isStarted());
        if (!isReset() && isStarted()) {
            BaseLoader.super.deliverResult(t);
        }
    }

    @DexIgnore
    public void onReset() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .onReset");
        onStopLoading();
        BaseLoader.super.onReset();
    }

    @DexIgnore
    public void onStopLoading() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .onStopLoading");
        cancelLoad();
        BaseLoader.super.onStopLoading();
    }
}
