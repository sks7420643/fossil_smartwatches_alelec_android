package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.gg6;
import com.fossil.vk4;
import com.fossil.xg6;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$getSummariesPaging$Anon3 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummariesPaging$Anon3(GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        vk4 mHelper;
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = (GoalTrackingSummaryLocalDataSource) this.$sourceFactory.getSourceLiveData().a();
        if (goalTrackingSummaryLocalDataSource != null && (mHelper = goalTrackingSummaryLocalDataSource.getMHelper()) != null) {
            mHelper.b();
        }
    }
}
