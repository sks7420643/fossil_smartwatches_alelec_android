package com.portfolio.platform.data.source;

import com.fossil.an4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalizationRepository_Factory implements Factory<WatchLocalizationRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ Provider<an4> sharedPreferencesManagerProvider;

    @DexIgnore
    public WatchLocalizationRepository_Factory(Provider<ApiServiceV2> provider, Provider<an4> provider2) {
        this.apiProvider = provider;
        this.sharedPreferencesManagerProvider = provider2;
    }

    @DexIgnore
    public static WatchLocalizationRepository_Factory create(Provider<ApiServiceV2> provider, Provider<an4> provider2) {
        return new WatchLocalizationRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static WatchLocalizationRepository newWatchLocalizationRepository(ApiServiceV2 apiServiceV2, an4 an4) {
        return new WatchLocalizationRepository(apiServiceV2, an4);
    }

    @DexIgnore
    public static WatchLocalizationRepository provideInstance(Provider<ApiServiceV2> provider, Provider<an4> provider2) {
        return new WatchLocalizationRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public WatchLocalizationRepository get() {
        return provideInstance(this.apiProvider, this.sharedPreferencesManagerProvider);
    }
}
