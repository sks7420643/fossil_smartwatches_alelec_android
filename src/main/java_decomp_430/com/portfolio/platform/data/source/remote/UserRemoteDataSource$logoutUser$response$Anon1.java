package com.portfolio.platform.data.source.remote;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$response$1", f = "UserRemoteDataSource.kt", l = {110}, m = "invokeSuspend")
public final class UserRemoteDataSource$logoutUser$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<Void>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$logoutUser$response$Anon1(UserRemoteDataSource userRemoteDataSource, xe6 xe6) {
        super(1, xe6);
        this.this$0 = userRemoteDataSource;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new UserRemoteDataSource$logoutUser$response$Anon1(this.this$0, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((UserRemoteDataSource$logoutUser$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            AuthApiUserService access$getMAuthApiUserService$p = this.this$0.mAuthApiUserService;
            ku3 ku3 = new ku3();
            this.label = 1;
            obj = access$getMAuthApiUserService$p.logout(ku3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
