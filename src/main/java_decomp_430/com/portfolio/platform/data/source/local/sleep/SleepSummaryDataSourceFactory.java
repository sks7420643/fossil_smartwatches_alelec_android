package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.MutableLiveData;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryDataSourceFactory extends xe.b<Date, SleepSummary> {
    @DexIgnore
    public SleepSummaryLocalDataSource localSource;
    @DexIgnore
    public /* final */ u04 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ vk4.a mListener;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<SleepSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public SleepSummaryDataSourceFactory(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wg6.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(sleepDao, "mSleepDao");
        wg6.b(sleepDatabase, "mSleepDatabase");
        wg6.b(date, "mCreatedDate");
        wg6.b(u04, "mAppExecutors");
        wg6.b(aVar, "mListener");
        wg6.b(calendar, "mStartCalendar");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDao = sleepDao;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = u04;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public xe<Date, SleepSummary> create() {
        this.localSource = new SleepSummaryLocalDataSource(this.mSleepSummariesRepository, this.mSleepSessionsRepository, this.mFitnessDataRepository, this.mSleepDao, this.mSleepDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.sourceLiveData.a(this.localSource);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.localSource;
        if (sleepSummaryLocalDataSource != null) {
            return sleepSummaryLocalDataSource;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final SleepSummaryLocalDataSource getLocalSource() {
        return this.localSource;
    }

    @DexIgnore
    public final MutableLiveData<SleepSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalSource(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.localSource = sleepSummaryLocalDataSource;
    }
}
