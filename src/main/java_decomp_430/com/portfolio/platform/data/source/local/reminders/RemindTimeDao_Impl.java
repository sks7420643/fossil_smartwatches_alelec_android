package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.RemindTimeModel;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeDao_Impl implements RemindTimeDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<RemindTimeModel> __insertionAdapterOfRemindTimeModel;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<RemindTimeModel> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `remindTimeModel` (`remindTimeName`,`minutes`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, RemindTimeModel remindTimeModel) {
            if (remindTimeModel.getRemindTimeName() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, remindTimeModel.getRemindTimeName());
            }
            miVar.a(2, (long) remindTimeModel.getMinutes());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM remindTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public RemindTimeModel call() throws Exception {
            RemindTimeModel remindTimeModel = null;
            Cursor a = bi.a(RemindTimeDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "remindTimeName");
                int b2 = ai.b(a, "minutes");
                if (a.moveToFirst()) {
                    remindTimeModel = new RemindTimeModel(a.getString(b), a.getInt(b2));
                }
                return remindTimeModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public RemindTimeDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfRemindTimeModel = new Anon1(ohVar);
        this.__preparedStmtOfDelete = new Anon2(ohVar);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<RemindTimeModel> getRemindTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"remindTimeModel"}, false, new Anon3(rh.b("SELECT * FROM remindTimeModel", 0)));
    }

    @DexIgnore
    public RemindTimeModel getRemindTimeModel() {
        rh b = rh.b("SELECT * FROM remindTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        RemindTimeModel remindTimeModel = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "remindTimeName");
            int b3 = ai.b(a, "minutes");
            if (a.moveToFirst()) {
                remindTimeModel = new RemindTimeModel(a.getString(b2), a.getInt(b3));
            }
            return remindTimeModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertRemindTimeModel(RemindTimeModel remindTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfRemindTimeModel.insert(remindTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
