package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.ServerSettingLocalDataSource;
import com.portfolio.platform.data.source.local.UserLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchAppSettingRemoteDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RepositoriesModule {
    @DexIgnore
    public ComplicationRemoteDataSource provideComplicationRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new ComplicationRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public DianaPresetRemoteDataSource provideDianaPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new DianaPresetRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Local
    public ServerSettingDataSource provideServerSettingLocalDataSource() {
        return new ServerSettingLocalDataSource();
    }

    @DexIgnore
    @Remote
    public ServerSettingDataSource provideServerSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new ServerSettingRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Local
    public UserDataSource provideUserLocalDataSource() {
        return new UserLocalDataSource();
    }

    @DexIgnore
    @Remote
    public UserDataSource provideUserRemoteDataSource(ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        return new UserRemoteDataSource(apiServiceV2, authApiGuestService, authApiUserService);
    }

    @DexIgnore
    public WatchAppSettingRemoteDataSource provideWatchAppSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new WatchAppSettingRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public WatchAppRemoteDataSource provideWatchAppsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new WatchAppRemoteDataSource(apiServiceV2);
    }
}
