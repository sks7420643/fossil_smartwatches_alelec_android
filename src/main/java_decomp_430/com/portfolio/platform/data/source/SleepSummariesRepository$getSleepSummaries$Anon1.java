package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSleepSummaries$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ String $end;
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ String $start;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<MFSleepDay>, ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ lc6 $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummaries$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummaries$Anon1 sleepSummariesRepository$getSleepSummaries$Anon1, lc6 lc6) {
            this.this$0 = sleepSummariesRepository$getSleepSummaries$Anon1;
            this.$downloadingDate = lc6;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ku3>> xe6) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            lc6 lc6 = this.$downloadingDate;
            if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                date = this.this$0.$startDate;
            }
            String e = bk4.e(date);
            wg6.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            lc6 lc62 = this.$downloadingDate;
            if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                date2 = this.this$0.$endDate;
            }
            String e2 = bk4.e(date2);
            wg6.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, xe6);
        }

        @DexIgnore
        public LiveData<List<MFSleepDay>> loadFromDb() {
            SleepDao access$getMSleepDao$p = this.this$0.this$0.mSleepDao;
            String str = this.this$0.$start;
            wg6.a((Object) str, "start");
            String str2 = this.this$0.$end;
            wg6.a((Object) str2, "end");
            return access$getMSleepDao$p.getSleepDaysLiveData(str, str2);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(ku3 ku3) {
            wg6.b(ku3, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getSleepSummaries saveCallResult onResponse: response = " + ku3);
            SleepSummariesRepository$getSleepSummaries$Anon1 sleepSummariesRepository$getSleepSummaries$Anon1 = this.this$0;
            sleepSummariesRepository$getSleepSummaries$Anon1.this$0.saveSleepSummaries$app_fossilRelease(ku3, sleepSummariesRepository$getSleepSummaries$Anon1.$startDate, sleepSummariesRepository$getSleepSummaries$Anon1.$endDate, this.$downloadingDate);
        }

        @DexIgnore
        public boolean shouldFetch(List<MFSleepDay> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SleepSummariesRepository$getSleepSummaries$Anon1(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, boolean z, String str, String str2) {
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
        this.$start = str;
        this.$end = str2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1$Anon1_Level2] */
    public final LiveData<yx5<List<MFSleepDay>>> apply(List<FitnessDataWrapper> list) {
        wg6.a((Object) list, "fitnessDataList");
        return new Anon1_Level2(this, FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
