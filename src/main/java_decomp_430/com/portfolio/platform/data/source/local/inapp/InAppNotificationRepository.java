package com.portfolio.platform.data.source.local.inapp;

import androidx.lifecycle.LiveData;
import com.fossil.cd6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "InAppNotificationRepository";
    @DexIgnore
    public /* final */ InAppNotificationDao mInAppNotificationDao;
    @DexIgnore
    public /* final */ List<InAppNotification> mNotificationList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public InAppNotificationRepository(InAppNotificationDao inAppNotificationDao) {
        wg6.b(inAppNotificationDao, "mInAppNotificationDao");
        this.mInAppNotificationDao = inAppNotificationDao;
    }

    @DexIgnore
    public final Object delete(InAppNotification inAppNotification, xe6<? super cd6> xe6) {
        this.mInAppNotificationDao.delete(inAppNotification);
        return cd6.a;
    }

    @DexIgnore
    public final LiveData<List<InAppNotification>> getAllInAppNotifications() {
        return this.mInAppNotificationDao.getAllInAppNotification();
    }

    @DexIgnore
    public final void handleReceivingNotification(InAppNotification inAppNotification) {
        wg6.b(inAppNotification, "inAppNotification");
        this.mNotificationList.add(inAppNotification);
    }

    @DexIgnore
    public final Object insert(List<InAppNotification> list, xe6<? super cd6> xe6) {
        this.mInAppNotificationDao.insertListInAppNotification(list);
        return cd6.a;
    }

    @DexIgnore
    public final void removeNotificationAfterSending(InAppNotification inAppNotification) {
        wg6.b(inAppNotification, "inAppNotification");
        this.mNotificationList.remove(inAppNotification);
    }
}
