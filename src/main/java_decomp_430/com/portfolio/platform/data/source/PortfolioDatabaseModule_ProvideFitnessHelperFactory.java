package com.portfolio.platform.data.source;

import com.fossil.an4;
import com.fossil.ik4;
import com.fossil.z76;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideFitnessHelperFactory implements Factory<ik4> {
    @DexIgnore
    public /* final */ Provider<ActivitySummaryDao> activitySummaryDaoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;
    @DexIgnore
    public /* final */ Provider<an4> sharedPreferencesManagerProvider;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessHelperFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<an4> provider, Provider<ActivitySummaryDao> provider2) {
        this.module = portfolioDatabaseModule;
        this.sharedPreferencesManagerProvider = provider;
        this.activitySummaryDaoProvider = provider2;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessHelperFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<an4> provider, Provider<ActivitySummaryDao> provider2) {
        return new PortfolioDatabaseModule_ProvideFitnessHelperFactory(portfolioDatabaseModule, provider, provider2);
    }

    @DexIgnore
    public static ik4 provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<an4> provider, Provider<ActivitySummaryDao> provider2) {
        return proxyProvideFitnessHelper(portfolioDatabaseModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ik4 proxyProvideFitnessHelper(PortfolioDatabaseModule portfolioDatabaseModule, an4 an4, ActivitySummaryDao activitySummaryDao) {
        ik4 provideFitnessHelper = portfolioDatabaseModule.provideFitnessHelper(an4, activitySummaryDao);
        z76.a(provideFitnessHelper, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessHelper;
    }

    @DexIgnore
    public ik4 get() {
        return provideInstance(this.module, this.sharedPreferencesManagerProvider, this.activitySummaryDaoProvider);
    }
}
