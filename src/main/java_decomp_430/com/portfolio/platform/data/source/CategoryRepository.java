package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "CategoryRepository";
    @DexIgnore
    public /* final */ CategoryDao mCategoryDao;
    @DexIgnore
    public /* final */ CategoryRemoteDataSource mCategoryRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public CategoryRepository(CategoryDao categoryDao, CategoryRemoteDataSource categoryRemoteDataSource) {
        wg6.b(categoryDao, "mCategoryDao");
        wg6.b(categoryRemoteDataSource, "mCategoryRemoteDataSource");
        this.mCategoryDao = categoryDao;
        this.mCategoryRemoteDataSource = categoryRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadCategories(xe6<? super cd6> xe6) {
        CategoryRepository$downloadCategories$Anon1 categoryRepository$downloadCategories$Anon1;
        int i;
        CategoryRepository categoryRepository;
        ap4 ap4;
        if (xe6 instanceof CategoryRepository$downloadCategories$Anon1) {
            categoryRepository$downloadCategories$Anon1 = (CategoryRepository$downloadCategories$Anon1) xe6;
            int i2 = categoryRepository$downloadCategories$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                categoryRepository$downloadCategories$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = categoryRepository$downloadCategories$Anon1.result;
                Object a = ff6.a();
                i = categoryRepository$downloadCategories$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadCategories ");
                    CategoryRemoteDataSource categoryRemoteDataSource = this.mCategoryRemoteDataSource;
                    categoryRepository$downloadCategories$Anon1.L$0 = this;
                    categoryRepository$downloadCategories$Anon1.label = 1;
                    obj = categoryRemoteDataSource.getAllCategory(categoryRepository$downloadCategories$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    categoryRepository = this;
                } else if (i == 1) {
                    categoryRepository = (CategoryRepository) categoryRepository$downloadCategories$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                Integer num = null;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadCategories success isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    sb.append(" response ");
                    sb.append((List) cp4.a());
                    local.d(TAG, sb.toString());
                    if (!cp4.b()) {
                        Object a2 = cp4.a();
                        if (a2 == null) {
                            wg6.a();
                            throw null;
                        } else if (!((Collection) a2).isEmpty()) {
                            categoryRepository.mCategoryDao.upsertCategoryList((List) cp4.a());
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadCategories fail!!! error=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local2.d(TAG, sb2.toString());
                }
                return cd6.a;
            }
        }
        categoryRepository$downloadCategories$Anon1 = new CategoryRepository$downloadCategories$Anon1(this, xe6);
        Object obj2 = categoryRepository$downloadCategories$Anon1.result;
        Object a3 = ff6.a();
        i = categoryRepository$downloadCategories$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        Integer num2 = null;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final List<Category> getAllCategories() {
        return this.mCategoryDao.getAllCategory();
    }
}
