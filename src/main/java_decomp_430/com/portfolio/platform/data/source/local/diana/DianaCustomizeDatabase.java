package com.portfolio.platform.data.source.local.diana;

import com.fossil.oh;
import com.fossil.qg6;
import com.fossil.xh;
import com.portfolio.platform.data.source.local.RingStyleDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DianaCustomizeDatabase extends oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ xh MIGRATION_FROM_13_TO_14; // = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1(13, 14);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaCustomizeDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final xh getMIGRATION_FROM_13_TO_14() {
            return DianaCustomizeDatabase.MIGRATION_FROM_13_TO_14;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public abstract ComplicationDao getComplicationDao();

    @DexIgnore
    public abstract ComplicationLastSettingDao getComplicationSettingDao();

    @DexIgnore
    public abstract DianaPresetDao getPresetDao();

    @DexIgnore
    public abstract RingStyleDao getRingStyleDao();

    @DexIgnore
    public abstract WatchAppDao getWatchAppDao();

    @DexIgnore
    public abstract WatchAppLastSettingDao getWatchAppSettingDao();

    @DexIgnore
    public abstract WatchFaceDao getWatchFaceDao();
}
