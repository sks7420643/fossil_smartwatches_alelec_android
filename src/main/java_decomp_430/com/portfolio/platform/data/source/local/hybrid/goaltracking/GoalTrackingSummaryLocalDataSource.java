package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.fossil.af;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.lc6;
import com.fossil.lh;
import com.fossil.ll6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wk4;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource extends af<Date, GoalTrackingSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "GoalTrackingSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public vk4 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = wk4.a(this.mHelper);
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public List<lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends lh.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingSummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wg6.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            wg6.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(GoalTrackingSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = bk4.c(instance);
            if (bk4.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            wg6.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(date, "mCreatedDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        wg6.b(calendar, "key");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new vk4(u04.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingDay", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = bk4.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        wg6.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (bk4.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<GoalTrackingSummary> calculateSummaries(List<GoalTrackingSummary> list) {
        int i;
        int i2;
        List<GoalTrackingSummary> list2 = list;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "endCalendar");
            instance.setTime(((GoalTrackingSummary) yd6.f(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar q = bk4.q(instance.getTime());
                wg6.a((Object) q, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                GoalTrackingDao goalTrackingDao = this.mGoalTrackingDao;
                Date time = q.getTime();
                wg6.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                wg6.a((Object) time2, "endCalendar.time");
                GoalTrackingDao.TotalSummary goalTrackingValueAndTarget = goalTrackingDao.getGoalTrackingValueAndTarget(time, time2);
                i = goalTrackingValueAndTarget.getValues();
                i2 = goalTrackingValueAndTarget.getTargets();
            } else {
                i2 = 0;
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "calendar");
            instance2.setTime(((GoalTrackingSummary) yd6.d(list)).getDate());
            Calendar q2 = bk4.q(instance2.getTime());
            wg6.a((Object) q2, "DateHelper.getStartOfWeek(calendar.time)");
            q2.add(5, -1);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (T next : list) {
                int i7 = i6 + 1;
                if (i6 >= 0) {
                    GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) next;
                    Date component1 = goalTrackingSummary.component1();
                    int component2 = goalTrackingSummary.component2();
                    int component3 = goalTrackingSummary.component3();
                    if (bk4.d(component1, q2.getTime())) {
                        list2.get(i3).setTotalValueOfWeek(i4);
                        list2.get(i3).setTotalTargetOfWeek(i5);
                        q2.add(5, -7);
                        i3 = i6;
                        i4 = 0;
                        i5 = 0;
                    }
                    i4 += component2;
                    i5 += component3;
                    if (i6 == list.size() - 1) {
                        i4 += i;
                        i5 += i2;
                    }
                    i6 = i7;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            list2.get(i3).setTotalValueOfWeek(i4);
            list2.get(i3).setTotalTargetOfWeek(i5);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "calculateSummaries summaries.size=" + list.size());
        }
        return list2;
    }

    @DexIgnore
    private final GoalTrackingSummary dummySummary(Date date) {
        return new GoalTrackingSummary(date, 0, this.mGoalTrackingDao.getNearestGoalTrackingTargetFromDate(date), new Date().getTime(), new Date().getTime());
    }

    @DexIgnore
    private final List<GoalTrackingSummary> getDataInDatabase(Date date, Date date2) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<GoalTrackingSummary> goalTrackingSummaries = this.mGoalTrackingRepository.getGoalTrackingSummaries(date, date2);
        ArrayList arrayList = new ArrayList();
        int size = goalTrackingSummaries.size() + -1;
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - summaries.size=" + goalTrackingSummaries.size() + ", " + "startDate=" + date + ", endDateToFill=" + date2);
        while (bk4.c(date2, date)) {
            if (size < 0 || !bk4.d(goalTrackingSummaries.get(size).getDate(), date2)) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(goalTrackingSummaries.get(size));
                size--;
            }
            date2 = bk4.n(date2);
            wg6.a((Object) date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        calculateSummaries(arrayList);
        if (!arrayList.isEmpty()) {
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) yd6.d(arrayList);
            Boolean t = bk4.t(goalTrackingSummary.getDate());
            wg6.a((Object) t, "DateHelper.isToday(todaySummary.date)");
            if (t.booleanValue()) {
                arrayList.add(0, GoalTrackingSummary.copy$default(goalTrackingSummary, goalTrackingSummary.getDate(), goalTrackingSummary.getTotalTracked(), goalTrackingSummary.getGoalTarget(), 0, 0, 24, (Object) null));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final rm6 loadData(vk4.d dVar, Date date, Date date2, vk4.b.a aVar) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final vk4 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().b();
        return GoalTrackingSummaryLocalDataSource.super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(af.f<Date> fVar, af.a<Date, GoalTrackingSummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (bk4.b((Date) fVar.a, this.mCreatedDate)) {
            Object obj = fVar.a;
            wg6.a(obj, "params.key");
            Date date = (Date) obj;
            Companion companion = Companion;
            Object obj2 = fVar.a;
            wg6.a(obj2, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) obj2, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date m = bk4.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : bk4.m(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + m + ", endQueryDate=" + date);
            wg6.a((Object) m, "startQueryDate");
            aVar.a(getDataInDatabase(m, date), calculateNextKey);
            if (bk4.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new lc6(this.mStartDate, this.mEndDate));
                this.mHelper.a(vk4.d.AFTER, (vk4.b) new GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(af.f<Date> fVar, af.a<Date, GoalTrackingSummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(af.e<Date> eVar, af.c<Date, GoalTrackingSummary> cVar) {
        wg6.b(eVar, "params");
        wg6.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date m = bk4.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : bk4.m(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + m + ", endQueryDate=" + date);
        wg6.a((Object) m, "startQueryDate");
        cVar.a(getDataInDatabase(m, date), (Object) null, this.key.getTime());
        this.mHelper.a(vk4.d.INITIAL, (vk4.b) new GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(vk4 vk4) {
        wg6.b(vk4, "<set-?>");
        this.mHelper = vk4;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wg6.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
