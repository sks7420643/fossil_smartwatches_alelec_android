package com.portfolio.platform.data.source.local;

import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsLocalDataSource_Factory implements Factory<AlarmsLocalDataSource> {
    @DexIgnore
    public /* final */ Provider<AlarmDao> alarmDaoProvider;

    @DexIgnore
    public AlarmsLocalDataSource_Factory(Provider<AlarmDao> provider) {
        this.alarmDaoProvider = provider;
    }

    @DexIgnore
    public static AlarmsLocalDataSource_Factory create(Provider<AlarmDao> provider) {
        return new AlarmsLocalDataSource_Factory(provider);
    }

    @DexIgnore
    public static AlarmsLocalDataSource newAlarmsLocalDataSource(AlarmDao alarmDao) {
        return new AlarmsLocalDataSource(alarmDao);
    }

    @DexIgnore
    public static AlarmsLocalDataSource provideInstance(Provider<AlarmDao> provider) {
        return new AlarmsLocalDataSource(provider.get());
    }

    @DexIgnore
    public AlarmsLocalDataSource get() {
        return provideInstance(this.alarmDaoProvider);
    }
}
