package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cf;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.sd;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.ze;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public List<WorkoutSessionDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();
    @DexIgnore
    public /* final */ WorkoutDao mWorkoutDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return WorkoutSessionRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "WorkoutSessionRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionRepository(WorkoutDao workoutDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        wg6.b(workoutDao, "mWorkoutDao");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(apiServiceV2, "mApiService");
        this.mWorkoutDao = workoutDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchWorkoutSessions$default(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, xe6 xe6, int i3, Object obj) {
        return workoutSessionRepository.fetchWorkoutSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mWorkoutDao.deleteAllWorkoutSession();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchWorkoutSessions(Date date, Date date2, int i, int i2, xe6<? super ap4<ApiResponse<ServerWorkoutSession>>> xe6) {
        WorkoutSessionRepository$fetchWorkoutSessions$Anon1 workoutSessionRepository$fetchWorkoutSessions$Anon1;
        int i3;
        int i4;
        Date date3;
        WorkoutSessionRepository workoutSessionRepository;
        Date date4;
        int i5;
        ap4 ap4;
        String message;
        Date date5 = date;
        Date date6 = date2;
        xe6<? super ap4<ApiResponse<ServerWorkoutSession>>> xe62 = xe6;
        if (xe62 instanceof WorkoutSessionRepository$fetchWorkoutSessions$Anon1) {
            workoutSessionRepository$fetchWorkoutSessions$Anon1 = (WorkoutSessionRepository$fetchWorkoutSessions$Anon1) xe62;
            int i6 = workoutSessionRepository$fetchWorkoutSessions$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                workoutSessionRepository$fetchWorkoutSessions$Anon1.label = i6 - Integer.MIN_VALUE;
                WorkoutSessionRepository$fetchWorkoutSessions$Anon1 workoutSessionRepository$fetchWorkoutSessions$Anon12 = workoutSessionRepository$fetchWorkoutSessions$Anon1;
                Object obj = workoutSessionRepository$fetchWorkoutSessions$Anon12.result;
                Object a = ff6.a();
                i3 = workoutSessionRepository$fetchWorkoutSessions$Anon12.label;
                if (i3 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchWorkoutSessions - start=" + date5 + ", end=" + date6);
                    WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1 workoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1 = new WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1(this, date, date2, i, i2, (xe6) null);
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.L$0 = this;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.L$1 = date5;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.L$2 = date6;
                    i5 = i;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.I$0 = i5;
                    int i7 = i2;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.I$1 = i7;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.label = 1;
                    Object a2 = ResponseKt.a(workoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1, workoutSessionRepository$fetchWorkoutSessions$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj = a2;
                    date4 = date6;
                    date3 = date5;
                    workoutSessionRepository = this;
                } else if (i3 == 1) {
                    int i8 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$1;
                    i5 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$0;
                    date4 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$2;
                    nc6.a(obj);
                    i4 = i8;
                    date3 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$1;
                    workoutSessionRepository = (WorkoutSessionRepository) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$0;
                } else if (i3 == 2) {
                    ap4 ap42 = (ap4) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$3;
                    int i9 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$1;
                    int i10 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$0;
                    Date date7 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$2;
                    Date date8 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$1;
                    WorkoutSessionRepository workoutSessionRepository2 = (WorkoutSessionRepository) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$0;
                    nc6.a(obj);
                    return (ap4) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() == null) {
                        return ap4;
                    }
                    if (!cp4.b()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("fetchWorkoutSessions - Success -- item.size=");
                        sb.append(((ApiResponse) cp4.a()).get_items().size());
                        sb.append(", hasNext=");
                        Range range = ((ApiResponse) cp4.a()).get_range();
                        sb.append(range != null ? hf6.a(range.isHasNext()) : null);
                        local2.d(str2, sb.toString());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local3.d(str3, "fetchWorkoutSessions - Success -- items=" + ((ApiResponse) cp4.a()).get_items());
                        ArrayList arrayList = new ArrayList();
                        for (ServerWorkoutSession workoutSession : ((ApiResponse) cp4.a()).get_items()) {
                            WorkoutSession workoutSession2 = workoutSession.toWorkoutSession();
                            if (workoutSession2 != null) {
                                arrayList.add(workoutSession2);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            workoutSessionRepository.mWorkoutDao.upsertListWorkoutSession(arrayList);
                        }
                        FLogger.INSTANCE.getLocal().d(TAG, "fetchWorkoutSessions - saveCallResult -- DONE!!!");
                    }
                    if (((ApiResponse) cp4.a()).get_range() == null) {
                        return ap4;
                    }
                    Range range2 = ((ApiResponse) cp4.a()).get_range();
                    if (range2 == null) {
                        wg6.a();
                        throw null;
                    } else if (!range2.isHasNext()) {
                        return ap4;
                    } else {
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$0 = workoutSessionRepository;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$1 = date3;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$2 = date4;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.I$0 = i5;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.I$1 = i4;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$3 = ap4;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.label = 2;
                        obj = workoutSessionRepository.fetchWorkoutSessions(date3, date4, i5 + i4, i4, workoutSessionRepository$fetchWorkoutSessions$Anon12);
                        if (obj == a) {
                            return a;
                        }
                        return (ap4) obj;
                    }
                } else {
                    String str4 = null;
                    if (ap4 instanceof zo4) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("fetchWorkoutSessions - Failure -- code=");
                        zo4 zo4 = (zo4) ap4;
                        sb2.append(zo4.a());
                        sb2.append(", message=");
                        ServerError c = zo4.c();
                        if (c == null || (message = c.getMessage()) == null) {
                            ServerError c2 = zo4.c();
                            if (c2 != null) {
                                str4 = c2.getUserMessage();
                            }
                        } else {
                            str4 = message;
                        }
                        if (str4 == null) {
                            str4 = "";
                        }
                        sb2.append(str4);
                        local4.d(str5, sb2.toString());
                    }
                    return ap4;
                }
            }
        }
        workoutSessionRepository$fetchWorkoutSessions$Anon1 = new WorkoutSessionRepository$fetchWorkoutSessions$Anon1(this, xe62);
        WorkoutSessionRepository$fetchWorkoutSessions$Anon1 workoutSessionRepository$fetchWorkoutSessions$Anon122 = workoutSessionRepository$fetchWorkoutSessions$Anon1;
        Object obj2 = workoutSessionRepository$fetchWorkoutSessions$Anon122.result;
        Object a3 = ff6.a();
        i3 = workoutSessionRepository$fetchWorkoutSessions$Anon122.label;
        if (i3 != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final LiveData<yx5<List<WorkoutSession>>> getWorkoutSessions(Date date, Date date2, boolean z) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        Date o = bk4.o(date);
        Date j = bk4.j(date2);
        FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
        wg6.a((Object) o, "startDate");
        wg6.a((Object) j, "endDate");
        LiveData<yx5<List<WorkoutSession>>> b = sd.b(fitnessDataDao.getFitnessDataLiveData(o, j), new WorkoutSessionRepository$getWorkoutSessions$Anon1(this, z, date2, o, j));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<WorkoutSession> getWorkoutSessionsPaging(Date date, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, u04 u04, vk4.a aVar) {
        wg6.b(date, "currentDate");
        wg6.b(workoutSessionRepository, "workoutSessionRepository");
        wg6.b(fitnessDataDao, "fitnessDataDao");
        wg6.b(workoutDao, "workoutDao");
        wg6.b(fitnessDatabase, "workoutDatabase");
        wg6.b(u04, "appExecutors");
        vk4.a aVar2 = aVar;
        wg6.b(aVar2, "listener");
        WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory = new WorkoutSessionDataSourceFactory(workoutSessionRepository, fitnessDataDao, workoutDao, fitnessDatabase, date, u04, aVar2);
        this.mSourceDataFactoryList.add(workoutSessionDataSourceFactory);
        cf.h.a aVar3 = new cf.h.a();
        aVar3.a(100);
        aVar3.a(false);
        aVar3.b(100);
        aVar3.c(5);
        cf.h a = aVar3.a();
        wg6.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new ze(workoutSessionDataSourceFactory, a).a();
        wg6.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = sd.b(workoutSessionDataSourceFactory.getSourceLiveData(), WorkoutSessionRepository$getWorkoutSessionsPaging$Anon1.INSTANCE);
        wg6.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(workoutSessionDataSourceFactory), new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon3(workoutSessionDataSourceFactory));
    }

    @DexIgnore
    public final void insertFromDevice(List<WorkoutSession> list) {
        wg6.b(list, "workoutSessions");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: workoutSessions = " + list);
        this.mWorkoutDao.upsertListWorkoutSession(list);
        FLogger.INSTANCE.getLocal().d(TAG, "insertFromDevice: workoutSessions = DONE!!!");
    }

    @DexIgnore
    public final void removePagingListener() {
        for (WorkoutSessionDataSourceFactory localDataSource : this.mSourceDataFactoryList) {
            WorkoutSessionLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
    }
}
