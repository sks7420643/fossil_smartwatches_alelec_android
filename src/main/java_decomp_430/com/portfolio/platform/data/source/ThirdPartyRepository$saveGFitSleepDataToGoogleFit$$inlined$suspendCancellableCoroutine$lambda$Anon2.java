package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.g72;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lc3;
import com.fossil.lk6;
import com.fossil.ll6;
import com.fossil.mc6;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements lc3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ g72 $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitSLeepDataList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $listInsertGFitSuccessFul$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSleepData$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2) {
                this.this$0 = anon1_Level2;
            }

            @DexIgnore
            public final void run() {
                this.this$0.this$0.this$0.getMThirdPartyDatabase().getGFitSleepDao().deleteListGFitSleep(this.this$0.this$0.$listInsertGFitSuccessFul$inlined);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d(ThirdPartyRepository.TAG, "Delete " + this.this$0.$gFitSLeepDataList$inlined + " after pushing GoogleFit successful!");
                this.this$0.this$0.getMThirdPartyDatabase().runInTransaction(new Anon1_Level3(this));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(g72 g72, GoogleSignInAccount googleSignInAccount, hh6 hh6, ArrayList arrayList, int i, lk6 lk6, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$device$inlined = g72;
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfLists$inlined = hh6;
        this.$listInsertGFitSuccessFul$inlined = arrayList;
        this.$sizeOfListsOfGFitSleepData$inlined = i;
        this.$continuation$inlined = lk6;
        this.this$0 = thirdPartyRepository;
        this.$gFitSLeepDataList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onFailure(Exception exc) {
        wg6.b(exc, "it");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("There was a problem inserting the Sleep session: ");
        exc.printStackTrace();
        sb.append(cd6.a);
        local.e(ThirdPartyRepository.TAG, sb.toString());
        hh6 hh6 = this.$countSizeOfLists$inlined;
        hh6.element++;
        if (hh6.element >= this.$sizeOfListsOfGFitSleepData$inlined && this.$continuation$inlined.isActive()) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new Anon1_Level2(this, (xe6) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitSleepDataToGoogleFit");
            lk6 lk6 = this.$continuation$inlined;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
    }
}
