package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1", f = "GoalTrackingRepository.kt", l = {641}, m = "invokeSuspend")
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2(GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3, jh6 jh6, jh6 jh62, xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3;
        this.$startDate = jh6;
        this.$endDate = jh62;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2 = new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2(this.this$0, this.$startDate, this.$endDate, xe6);
        goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2.p$ = (il6) obj;
        return goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            this.L$0 = this.p$;
            this.label = 1;
            if (this.this$0.this$0.loadSummaries((Date) this.$startDate.element, (Date) this.$endDate.element, this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il6 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
