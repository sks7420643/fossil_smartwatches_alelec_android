package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerFitnessData;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "FitnessDataRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataRepository(FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mFitnessDataDao.deleteAllFitnessData();
    }

    @DexIgnore
    public final List<FitnessDataWrapper> getFitnessData(Date date, Date date2) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        return this.mFitnessDataDao.getFitnessData(date, date2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object insert$app_fossilRelease(List<FitnessDataWrapper> list, xe6<? super ap4<List<FitnessDataWrapper>>> xe6) {
        FitnessDataRepository$insert$Anon1 fitnessDataRepository$insert$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof FitnessDataRepository$insert$Anon1) {
            fitnessDataRepository$insert$Anon1 = (FitnessDataRepository$insert$Anon1) xe6;
            int i2 = fitnessDataRepository$insert$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fitnessDataRepository$insert$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fitnessDataRepository$insert$Anon1.result;
                Object a = ff6.a();
                i = fitnessDataRepository$insert$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    ApiResponse apiResponse = new ApiResponse();
                    for (FitnessDataWrapper serverFitnessData : list) {
                        apiResponse.get_items().add(new ServerFitnessData(serverFitnessData));
                    }
                    FitnessDataRepository$insert$repoResponse$Anon1 fitnessDataRepository$insert$repoResponse$Anon1 = new FitnessDataRepository$insert$repoResponse$Anon1(this, apiResponse, (xe6) null);
                    fitnessDataRepository$insert$Anon1.L$0 = this;
                    fitnessDataRepository$insert$Anon1.L$1 = list;
                    fitnessDataRepository$insert$Anon1.L$2 = apiResponse;
                    fitnessDataRepository$insert$Anon1.label = 1;
                    obj = ResponseKt.a(fitnessDataRepository$insert$repoResponse$Anon1, fitnessDataRepository$insert$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ApiResponse apiResponse2 = (ApiResponse) fitnessDataRepository$insert$Anon1.L$2;
                    list = (List) fitnessDataRepository$insert$Anon1.L$1;
                    FitnessDataRepository fitnessDataRepository = (FitnessDataRepository) fitnessDataRepository$insert$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "insert onResponse: response = " + ap4);
                    return new cp4(list, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("insert Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local2.d(str3, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        fitnessDataRepository$insert$Anon1 = new FitnessDataRepository$insert$Anon1(this, xe6);
        Object obj2 = fitnessDataRepository$insert$Anon1.result;
        Object a2 = ff6.a();
        i = fitnessDataRepository$insert$Anon1.label;
        String str4 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public final Object pushFitnessData$app_fossilRelease(List<FitnessDataWrapper> list, xe6<? super ap4<List<FitnessDataWrapper>>> xe6) {
        FitnessDataRepository fitnessDataRepository;
        FitnessDataRepository$pushFitnessData$Anon1 fitnessDataRepository$pushFitnessData$Anon1;
        int i;
        List<FitnessDataWrapper> list2;
        Object obj;
        int i2;
        List list3;
        FitnessDataRepository$pushFitnessData$Anon1 fitnessDataRepository$pushFitnessData$Anon12;
        int i3;
        ap4 ap4;
        FitnessDataRepository fitnessDataRepository2;
        ap4 ap42;
        FitnessDataRepository$pushFitnessData$Anon1 fitnessDataRepository$pushFitnessData$Anon13;
        List<FitnessDataWrapper> list4;
        xe6<? super ap4<List<FitnessDataWrapper>>> xe62 = xe6;
        if (xe62 instanceof FitnessDataRepository$pushFitnessData$Anon1) {
            fitnessDataRepository$pushFitnessData$Anon1 = (FitnessDataRepository$pushFitnessData$Anon1) xe62;
            int i4 = fitnessDataRepository$pushFitnessData$Anon1.label;
            if ((i4 & Integer.MIN_VALUE) != 0) {
                fitnessDataRepository$pushFitnessData$Anon1.label = i4 - Integer.MIN_VALUE;
                fitnessDataRepository = this;
                Object obj2 = fitnessDataRepository$pushFitnessData$Anon1.result;
                Object a = ff6.a();
                i = fitnessDataRepository$pushFitnessData$Anon1.label;
                boolean z = false;
                int i5 = 1;
                if (i != 0) {
                    nc6.a(obj2);
                    list3 = new ArrayList();
                    fitnessDataRepository$pushFitnessData$Anon13 = fitnessDataRepository$pushFitnessData$Anon1;
                    fitnessDataRepository2 = fitnessDataRepository;
                    obj = a;
                    ap4 = null;
                    i2 = 0;
                    list4 = list;
                } else if (i == 1) {
                    List list5 = (List) fitnessDataRepository$pushFitnessData$Anon1.L$4;
                    i3 = fitnessDataRepository$pushFitnessData$Anon1.I$1;
                    list3 = (List) fitnessDataRepository$pushFitnessData$Anon1.L$3;
                    i2 = fitnessDataRepository$pushFitnessData$Anon1.I$0;
                    list2 = (List) fitnessDataRepository$pushFitnessData$Anon1.L$1;
                    nc6.a(obj2);
                    fitnessDataRepository$pushFitnessData$Anon12 = fitnessDataRepository$pushFitnessData$Anon1;
                    fitnessDataRepository2 = (FitnessDataRepository) fitnessDataRepository$pushFitnessData$Anon1.L$0;
                    obj = a;
                    ap4 = (ap4) fitnessDataRepository$pushFitnessData$Anon1.L$2;
                    ap42 = (ap4) obj2;
                    i2 += 10;
                    if (ap42 instanceof cp4) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.d(str, "pushFitnessData success, bravo!!! startIndex=" + i2 + " endIndex=" + i3);
                        Object a2 = ((cp4) ap42).a();
                        if (a2 != null) {
                            List list6 = (List) a2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            local2.d(str2, "pushFitnessData success, new fitness data " + list6);
                            list3.addAll(list6);
                            if (i2 >= list2.size()) {
                                fitnessDataRepository2.mFitnessDataDao.deleteFitnessData(list3);
                            }
                            fitnessDataRepository2.mFitnessDataDao.deleteFitnessData(list3);
                            return new cp4(list3, z);
                        }
                        wg6.a();
                        throw null;
                    } else if (ap42 instanceof zo4) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local3.d(str3, "pushFitnessData failed, errorCode=" + ((zo4) ap42).a() + ' ' + "startIndex=" + i2 + " endIndex=" + i3);
                        if (i2 >= list2.size()) {
                            return ap42;
                        }
                    }
                    fitnessDataRepository$pushFitnessData$Anon13 = fitnessDataRepository$pushFitnessData$Anon12;
                    list4 = list2;
                    z = false;
                    i5 = 1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (i2 >= list4.size()) {
                    return ap4;
                }
                int i6 = i2 + 10;
                if (i6 > list4.size()) {
                    i6 = list4.size();
                }
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = TAG;
                local4.d(str4, "pushFitnessData listSize=" + list4.size() + ", startIndex=" + i2 + " endIndex=" + i6);
                List<FitnessDataWrapper> subList = list4.subList(i2, i6);
                fitnessDataRepository$pushFitnessData$Anon13.L$0 = fitnessDataRepository2;
                fitnessDataRepository$pushFitnessData$Anon13.L$1 = list4;
                fitnessDataRepository$pushFitnessData$Anon13.L$2 = ap4;
                fitnessDataRepository$pushFitnessData$Anon13.I$0 = i2;
                fitnessDataRepository$pushFitnessData$Anon13.L$3 = list3;
                fitnessDataRepository$pushFitnessData$Anon13.I$1 = i6;
                fitnessDataRepository$pushFitnessData$Anon13.L$4 = subList;
                fitnessDataRepository$pushFitnessData$Anon13.label = i5;
                Object insert$app_fossilRelease = fitnessDataRepository2.insert$app_fossilRelease(subList, fitnessDataRepository$pushFitnessData$Anon13);
                if (insert$app_fossilRelease == obj) {
                    return obj;
                }
                int i7 = i6;
                list2 = list4;
                obj2 = insert$app_fossilRelease;
                fitnessDataRepository$pushFitnessData$Anon12 = fitnessDataRepository$pushFitnessData$Anon13;
                i3 = i7;
                ap42 = (ap4) obj2;
                i2 += 10;
                if (ap42 instanceof cp4) {
                }
                fitnessDataRepository$pushFitnessData$Anon13 = fitnessDataRepository$pushFitnessData$Anon12;
                list4 = list2;
                z = false;
                i5 = 1;
                if (i2 >= list4.size()) {
                }
                return obj;
                return ap4;
            }
        }
        fitnessDataRepository = this;
        fitnessDataRepository$pushFitnessData$Anon1 = new FitnessDataRepository$pushFitnessData$Anon1(fitnessDataRepository, xe62);
        Object obj22 = fitnessDataRepository$pushFitnessData$Anon1.result;
        Object a3 = ff6.a();
        i = fitnessDataRepository$pushFitnessData$Anon1.label;
        boolean z2 = false;
        int i52 = 1;
        if (i != 0) {
        }
        if (i2 >= list4.size()) {
        }
        return ap4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object pushPendingFitnessData(xe6<? super ap4<List<FitnessDataWrapper>>> xe6) {
        FitnessDataRepository$pushPendingFitnessData$Anon1 fitnessDataRepository$pushPendingFitnessData$Anon1;
        int i;
        if (xe6 instanceof FitnessDataRepository$pushPendingFitnessData$Anon1) {
            fitnessDataRepository$pushPendingFitnessData$Anon1 = (FitnessDataRepository$pushPendingFitnessData$Anon1) xe6;
            int i2 = fitnessDataRepository$pushPendingFitnessData$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fitnessDataRepository$pushPendingFitnessData$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fitnessDataRepository$pushPendingFitnessData$Anon1.result;
                Object a = ff6.a();
                i = fitnessDataRepository$pushPendingFitnessData$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "pushPendingFitnessData");
                    List<FitnessDataWrapper> pendingFitnessData = this.mFitnessDataDao.getPendingFitnessData();
                    if (pendingFitnessData.size() <= 0) {
                        return new cp4(new ArrayList(), false, 2, (qg6) null);
                    }
                    fitnessDataRepository$pushPendingFitnessData$Anon1.L$0 = this;
                    fitnessDataRepository$pushPendingFitnessData$Anon1.L$1 = pendingFitnessData;
                    fitnessDataRepository$pushPendingFitnessData$Anon1.label = 1;
                    obj = pushFitnessData$app_fossilRelease(pendingFitnessData, fitnessDataRepository$pushPendingFitnessData$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    List list = (List) fitnessDataRepository$pushPendingFitnessData$Anon1.L$1;
                    FitnessDataRepository fitnessDataRepository = (FitnessDataRepository) fitnessDataRepository$pushPendingFitnessData$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (ap4) obj;
            }
        }
        fitnessDataRepository$pushPendingFitnessData$Anon1 = new FitnessDataRepository$pushPendingFitnessData$Anon1(this, xe6);
        Object obj2 = fitnessDataRepository$pushPendingFitnessData$Anon1.result;
        Object a2 = ff6.a();
        i = fitnessDataRepository$pushPendingFitnessData$Anon1.label;
        if (i != 0) {
        }
        return (ap4) obj2;
    }

    @DexIgnore
    public final void saveFitnessData(List<FitnessDataWrapper> list) {
        wg6.b(list, "fitnessDataList");
        this.mFitnessDataDao.insertFitnessDataList(list);
    }
}
