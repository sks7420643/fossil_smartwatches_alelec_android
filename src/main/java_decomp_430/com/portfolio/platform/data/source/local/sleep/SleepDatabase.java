package com.portfolio.platform.data.source.local.sleep;

import com.fossil.oh;
import com.fossil.qg6;
import com.fossil.xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SleepDatabase extends oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ xh MIGRATION_FROM_2_TO_5; // = new SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1(2, 5);
    @DexIgnore
    public static /* final */ xh MIGRATION_FROM_3_TO_9; // = new SleepDatabase$Companion$MIGRATION_FROM_3_TO_9$Anon1(3, 9);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_2_TO_5$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_3_TO_9$annotations() {
        }

        @DexIgnore
        public final xh getMIGRATION_FROM_2_TO_5() {
            return SleepDatabase.MIGRATION_FROM_2_TO_5;
        }

        @DexIgnore
        public final xh getMIGRATION_FROM_3_TO_9() {
            return SleepDatabase.MIGRATION_FROM_3_TO_9;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public abstract SleepDao sleepDao();
}
