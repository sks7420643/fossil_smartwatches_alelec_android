package com.portfolio.platform.data.source.local;

import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface RingStyleDao {
    @DexIgnore
    void clearAllData();

    @DexIgnore
    List<DianaComplicationRingStyle> getRingStyles();

    @DexIgnore
    DianaComplicationRingStyle getRingStylesBySerial(String str);

    @DexIgnore
    void insertRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle);

    @DexIgnore
    void insertRingStyles(List<DianaComplicationRingStyle> list);
}
