package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.u44;
import com.fossil.vh;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceDao_Impl implements WatchFaceDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<WatchFace> __insertionAdapterOfWatchFace;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAll;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllBackgroundWatchface;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteWatchFace;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteWatchFacesWithSerial;
    @DexIgnore
    public /* final */ u44 __watchFaceTypeConverter; // = new u44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<WatchFace> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watch_face` (`id`,`name`,`ringStyleItems`,`background`,`previewUrl`,`serial`,`watchFaceType`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, WatchFace watchFace) {
            if (watchFace.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, watchFace.getId());
            }
            if (watchFace.getName() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, watchFace.getName());
            }
            String a = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getRingStyleItems());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            String a2 = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getBackground());
            if (a2 == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a2);
            }
            if (watchFace.getPreviewUrl() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, watchFace.getPreviewUrl());
            }
            if (watchFace.getSerial() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, watchFace.getSerial());
            }
            miVar.a(7, (long) watchFace.getWatchFaceType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watch_face WHERE serial = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watch_face WHERE id = ? and watchFaceType = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watch_face WHERE watchFaceType = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watch_face";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon6(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchFace> call() throws Exception {
            Cursor a = bi.a(WatchFaceDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "name");
                int b3 = ai.b(a, "ringStyleItems");
                int b4 = ai.b(a, Explore.COLUMN_BACKGROUND);
                int b5 = ai.b(a, "previewUrl");
                int b6 = ai.b(a, "serial");
                int b7 = ai.b(a, "watchFaceType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchFace(a.getString(b), a.getString(b2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(a.getString(b3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6), a.getInt(b7)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon7(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchFace> call() throws Exception {
            Cursor a = bi.a(WatchFaceDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "name");
                int b3 = ai.b(a, "ringStyleItems");
                int b4 = ai.b(a, Explore.COLUMN_BACKGROUND);
                int b5 = ai.b(a, "previewUrl");
                int b6 = ai.b(a, "serial");
                int b7 = ai.b(a, "watchFaceType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchFace(a.getString(b), a.getString(b2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(a.getString(b3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6), a.getInt(b7)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchFaceDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfWatchFace = new Anon1(ohVar);
        this.__preparedStmtOfDeleteWatchFacesWithSerial = new Anon2(ohVar);
        this.__preparedStmtOfDeleteWatchFace = new Anon3(ohVar);
        this.__preparedStmtOfDeleteAllBackgroundWatchface = new Anon4(ohVar);
        this.__preparedStmtOfDeleteAll = new Anon5(ohVar);
    }

    @DexIgnore
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteAllBackgroundWatchface() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllBackgroundWatchface.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllBackgroundWatchface.release(acquire);
        }
    }

    @DexIgnore
    public void deleteWatchFace(String str, int i) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteWatchFace.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        acquire.a(2, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFace.release(acquire);
        }
    }

    @DexIgnore
    public void deleteWatchFacesWithSerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteWatchFacesWithSerial.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFacesWithSerial.release(acquire);
        }
    }

    @DexIgnore
    public List<WatchFace> getAllWatchFaces() {
        rh b = rh.b("SELECT * FROM watch_face", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "ringStyleItems");
            int b5 = ai.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = ai.b(a, "previewUrl");
            int b7 = ai.b(a, "serial");
            int b8 = ai.b(a, "watchFaceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public String getLatestWatchFaceName(int i) {
        rh b = rh.b("SELECT name FROM watch_face WHERE watchFaceType = ? ORDER BY id DESC LIMIT 1", 1);
        b.a(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        String str = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                str = a.getString(0);
            }
            return str;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public WatchFace getWatchFaceWithId(String str) {
        WatchFace watchFace;
        String str2 = str;
        rh b = rh.b("SELECT * FROM watch_face WHERE id = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "ringStyleItems");
            int b5 = ai.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = ai.b(a, "previewUrl");
            int b7 = ai.b(a, "serial");
            int b8 = ai.b(a, "watchFaceType");
            if (a.moveToFirst()) {
                watchFace = new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8));
            } else {
                watchFace = null;
            }
            return watchFace;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<WatchFace>> getWatchFacesLiveData(String str) {
        rh b = rh.b("SELECT * FROM watch_face WHERE serial = ? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"watch_face"}, false, new Anon6(b));
    }

    @DexIgnore
    public LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, int i) {
        rh b = rh.b("SELECT * FROM watch_face WHERE serial = ? and watchFaceType = ? ", 2);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        b.a(2, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"watch_face"}, false, new Anon7(b));
    }

    @DexIgnore
    public List<WatchFace> getWatchFacesWithSerial(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM watch_face WHERE serial = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "ringStyleItems");
            int b5 = ai.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = ai.b(a, "previewUrl");
            int b7 = ai.b(a, "serial");
            int b8 = ai.b(a, "watchFaceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<WatchFace> getWatchFacesWithType(String str, int i) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM watch_face WHERE  serial = ? and watchFaceType = ?", 2);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        b.a(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "ringStyleItems");
            int b5 = ai.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = ai.b(a, "previewUrl");
            int b7 = ai.b(a, "serial");
            int b8 = ai.b(a, "watchFaceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertAllWatchFaces(List<WatchFace> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertWatchFace(WatchFace watchFace) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(watchFace);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
