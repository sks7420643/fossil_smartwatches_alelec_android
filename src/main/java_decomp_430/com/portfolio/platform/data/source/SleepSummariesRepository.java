package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.cp4;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.sd;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.ze;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.response.sleep.SleepDayParse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public List<SleepSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return SleepSummariesRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = SleepSummariesRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "SleepSummariesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSummariesRepository(SleepDao sleepDao, ApiServiceV2 apiServiceV2, FitnessDataDao fitnessDataDao) {
        wg6.b(sleepDao, "mSleepDao");
        wg6.b(apiServiceV2, "mApiService");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        this.mSleepDao = sleepDao;
        this.mApiService = apiServiceV2;
        this.mFitnessDataDao = fitnessDataDao;
    }

    @DexIgnore
    private final SleepStatistic getSleepStatisticDB() {
        FLogger.INSTANCE.getLocal().d(TAG, "getSleepStatisticDB");
        return this.mSleepDao.getSleepStatistic();
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mSleepDao.deleteAllSleepDays();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchLastSleepGoal(xe6<? super ap4<MFSleepSettings>> xe6) {
        SleepSummariesRepository$fetchLastSleepGoal$Anon1 sleepSummariesRepository$fetchLastSleepGoal$Anon1;
        int i;
        SleepSummariesRepository sleepSummariesRepository;
        ap4 ap4;
        String message;
        if (xe6 instanceof SleepSummariesRepository$fetchLastSleepGoal$Anon1) {
            sleepSummariesRepository$fetchLastSleepGoal$Anon1 = (SleepSummariesRepository$fetchLastSleepGoal$Anon1) xe6;
            int i2 = sleepSummariesRepository$fetchLastSleepGoal$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$fetchLastSleepGoal$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$fetchLastSleepGoal$Anon1.result;
                Object a = ff6.a();
                i = sleepSummariesRepository$fetchLastSleepGoal$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchLastSleepGoal");
                    SleepSummariesRepository$fetchLastSleepGoal$response$Anon1 sleepSummariesRepository$fetchLastSleepGoal$response$Anon1 = new SleepSummariesRepository$fetchLastSleepGoal$response$Anon1(this, (xe6) null);
                    sleepSummariesRepository$fetchLastSleepGoal$Anon1.L$0 = this;
                    sleepSummariesRepository$fetchLastSleepGoal$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSummariesRepository$fetchLastSleepGoal$response$Anon1, sleepSummariesRepository$fetchLastSleepGoal$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    sleepSummariesRepository = this;
                } else if (i == 1) {
                    sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$fetchLastSleepGoal$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null && !cp4.b()) {
                        sleepSummariesRepository.saveSleepSettingToDB$app_fossilRelease(((MFSleepSettings) cp4.a()).getSleepGoal());
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchActivitySettings Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str = c2.getUserMessage();
                        }
                    } else {
                        str = message;
                    }
                    if (str == null) {
                        str = "";
                    }
                    sb.append(str);
                    local.d(str2, sb.toString());
                }
                return ap4;
            }
        }
        sleepSummariesRepository$fetchLastSleepGoal$Anon1 = new SleepSummariesRepository$fetchLastSleepGoal$Anon1(this, xe6);
        Object obj2 = sleepSummariesRepository$fetchLastSleepGoal$Anon1.result;
        Object a2 = ff6.a();
        i = sleepSummariesRepository$fetchLastSleepGoal$Anon1.label;
        String str3 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchSleepSummaries(Date date, Date date2, xe6<? super ap4<ku3>> xe6) {
        SleepSummariesRepository$fetchSleepSummaries$Anon1 sleepSummariesRepository$fetchSleepSummaries$Anon1;
        int i;
        SleepSummariesRepository sleepSummariesRepository;
        ap4 ap4;
        String message;
        List<SleepDayParse> list;
        if (xe6 instanceof SleepSummariesRepository$fetchSleepSummaries$Anon1) {
            sleepSummariesRepository$fetchSleepSummaries$Anon1 = (SleepSummariesRepository$fetchSleepSummaries$Anon1) xe6;
            int i2 = sleepSummariesRepository$fetchSleepSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$fetchSleepSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$fetchSleepSummaries$Anon1.result;
                Object a = ff6.a();
                i = sleepSummariesRepository$fetchSleepSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "loadSummaries");
                    SleepSummariesRepository$fetchSleepSummaries$response$Anon1 sleepSummariesRepository$fetchSleepSummaries$response$Anon1 = new SleepSummariesRepository$fetchSleepSummaries$response$Anon1(this, date, date2, (xe6) null);
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.L$0 = this;
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.L$1 = date;
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.L$2 = date2;
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSummariesRepository$fetchSleepSummaries$response$Anon1, sleepSummariesRepository$fetchSleepSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    sleepSummariesRepository = this;
                } else if (i == 1) {
                    Date date3 = (Date) sleepSummariesRepository$fetchSleepSummaries$Anon1.L$2;
                    Date date4 = (Date) sleepSummariesRepository$fetchSleepSummaries$Anon1.L$1;
                    sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$fetchSleepSummaries$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null && !cp4.b()) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            ApiResponse apiResponse = (ApiResponse) new du3().a().a(((ku3) ((cp4) ap4).a()).toString(), new SleepSummariesRepository$fetchSleepSummaries$Anon2().getType());
                            if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                                for (SleepDayParse mFSleepBySleepDayParse : list) {
                                    MFSleepDay mFSleepBySleepDayParse2 = mFSleepBySleepDayParse.getMFSleepBySleepDayParse();
                                    if (mFSleepBySleepDayParse2 != null) {
                                        arrayList.add(mFSleepBySleepDayParse2);
                                    }
                                }
                            }
                            sleepSummariesRepository.mSleepDao.upsertSleepDays(arrayList);
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("loadSummaries exception=");
                            e.printStackTrace();
                            sb.append(cd6.a);
                            local.d(str2, sb.toString());
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loadSummaries Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str = c2.getUserMessage();
                        }
                    } else {
                        str = message;
                    }
                    if (str == null) {
                        str = "";
                    }
                    sb2.append(str);
                    local2.d(str3, sb2.toString());
                }
                return ap4;
            }
        }
        sleepSummariesRepository$fetchSleepSummaries$Anon1 = new SleepSummariesRepository$fetchSleepSummaries$Anon1(this, xe6);
        Object obj2 = sleepSummariesRepository$fetchSleepSummaries$Anon1.result;
        Object a2 = ff6.a();
        i = sleepSummariesRepository$fetchSleepSummaries$Anon1.label;
        String str4 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    public final int getCurrentLastSleepGoal() {
        MFSleepSettings sleepSettings = this.mSleepDao.getSleepSettings();
        if (sleepSettings != null) {
            return sleepSettings.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$Anon1] */
    public final LiveData<yx5<Integer>> getLastSleepGoal() {
        return new SleepSummariesRepository$getLastSleepGoal$Anon1(this).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon1] */
    public final LiveData<yx5<SleepStatistic>> getSleepStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepStatistic - shouldFetch=" + z);
        return new SleepSummariesRepository$getSleepStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getSleepStatisticAwait(xe6<? super SleepStatistic> xe6) {
        SleepSummariesRepository$getSleepStatisticAwait$Anon1 sleepSummariesRepository$getSleepStatisticAwait$Anon1;
        int i;
        SleepSummariesRepository sleepSummariesRepository;
        ap4 ap4;
        if (xe6 instanceof SleepSummariesRepository$getSleepStatisticAwait$Anon1) {
            sleepSummariesRepository$getSleepStatisticAwait$Anon1 = (SleepSummariesRepository$getSleepStatisticAwait$Anon1) xe6;
            int i2 = sleepSummariesRepository$getSleepStatisticAwait$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$getSleepStatisticAwait$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$getSleepStatisticAwait$Anon1.result;
                Object a = ff6.a();
                i = sleepSummariesRepository$getSleepStatisticAwait$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "getSleepStatisticAwait");
                    SleepStatistic sleepStatisticDB = getSleepStatisticDB();
                    if (sleepStatisticDB != null) {
                        return sleepStatisticDB;
                    }
                    SleepSummariesRepository$getSleepStatisticAwait$response$Anon1 sleepSummariesRepository$getSleepStatisticAwait$response$Anon1 = new SleepSummariesRepository$getSleepStatisticAwait$response$Anon1(this, (xe6) null);
                    sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$0 = this;
                    sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$1 = sleepStatisticDB;
                    sleepSummariesRepository$getSleepStatisticAwait$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSummariesRepository$getSleepStatisticAwait$response$Anon1, sleepSummariesRepository$getSleepStatisticAwait$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    sleepSummariesRepository = this;
                } else if (i == 1) {
                    SleepStatistic sleepStatistic = (SleepStatistic) sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$1;
                    sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null) {
                        sleepSummariesRepository.mSleepDao.upsertSleepStatistic((SleepStatistic) cp4.a());
                        return cp4.a();
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("getSleepStatisticAwait - Failure -- code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(", message=");
                    ServerError c = zo4.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local.e(str, sb.toString());
                }
                return null;
            }
        }
        sleepSummariesRepository$getSleepStatisticAwait$Anon1 = new SleepSummariesRepository$getSleepStatisticAwait$Anon1(this, xe6);
        Object obj2 = sleepSummariesRepository$getSleepStatisticAwait$Anon1.result;
        Object a2 = ff6.a();
        i = sleepSummariesRepository$getSleepStatisticAwait$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return null;
    }

    @DexIgnore
    public final LiveData<yx5<List<MFSleepDay>>> getSleepSummaries(Date date, Date date2, boolean z) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepSummaries: startDate = " + date + ", endDate = " + date2);
        LiveData<yx5<List<MFSleepDay>>> b = sd.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SleepSummariesRepository$getSleepSummaries$Anon1(this, date, date2, z, bk4.e(date), bk4.e(date2)));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final LiveData<yx5<MFSleepDay>> getSleepSummary(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepSummary date=" + date);
        LiveData<yx5<MFSleepDay>> b = sd.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date), new SleepSummariesRepository$getSleepSummary$Anon1(this, date));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final MFSleepDay getSleepSummaryFromDb(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "getSummary: calendar = " + date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.mSleepDao.getSleepDay(date);
    }

    @DexIgnore
    public final Listing<SleepSummary> getSummariesPaging(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, u04 u04, vk4.a aVar) {
        Date date2 = date;
        wg6.b(sleepSummariesRepository, "summariesRepository");
        wg6.b(sleepSessionsRepository, "sessionsRepository");
        wg6.b(fitnessDataRepository, "fitnessDataRepository");
        SleepDao sleepDao2 = sleepDao;
        wg6.b(sleepDao2, "sleepDao");
        SleepDatabase sleepDatabase2 = sleepDatabase;
        wg6.b(sleepDatabase2, "sleepDatabase");
        wg6.b(date2, "createdDate");
        u04 u042 = u04;
        wg6.b(u042, "appExecutors");
        vk4.a aVar2 = aVar;
        wg6.b(aVar2, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSummariesPaging - createdDate=" + date2);
        SleepSummaryLocalDataSource.Companion companion = SleepSummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        wg6.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date2);
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory = new SleepSummaryDataSourceFactory(sleepSummariesRepository, sleepSessionsRepository, fitnessDataRepository, sleepDao2, sleepDatabase2, date2, u042, aVar2, instance2);
        this.mSourceFactoryList.add(sleepSummaryDataSourceFactory);
        cf.h.a aVar3 = new cf.h.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        cf.h a = aVar3.a();
        wg6.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new ze(sleepSummaryDataSourceFactory, a).a();
        wg6.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = sd.b(sleepSummaryDataSourceFactory.getSourceLiveData(), SleepSummariesRepository$getSummariesPaging$Anon1.INSTANCE);
        wg6.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new SleepSummariesRepository$getSummariesPaging$Anon2(sleepSummaryDataSourceFactory), new SleepSummariesRepository$getSummariesPaging$Anon3(sleepSummaryDataSourceFactory));
    }

    @DexIgnore
    public final Object pushLastSleepGoalToServer(int i, xe6<? super cd6> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushLastSleepGoalToServer sleepGoal=" + i);
        ku3 ku3 = new ku3();
        try {
            ku3.a("currentGoalMinutes", hf6.a(i));
            TimeZone timeZone = TimeZone.getDefault();
            wg6.a((Object) timeZone, "TimeZone.getDefault()");
            ku3.a("timezoneOffset", hf6.a(timeZone.getRawOffset() / 1000));
        } catch (Exception unused) {
        }
        Object a = ResponseKt.a(new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this, ku3, (xe6) null), xe6);
        if (a == ff6.a()) {
            return a;
        }
        return cd6.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (SleepSummaryDataSourceFactory localSource : this.mSourceFactoryList) {
            SleepSummaryLocalDataSource localSource2 = localSource.getLocalSource();
            if (localSource2 != null) {
                localSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    public final void saveSleepSettingToDB$app_fossilRelease(int i) {
        this.mSleepDao.upsertSleepSettings(i);
        MFSleepDay sleepDay = this.mSleepDao.getSleepDay(new Date());
        if (sleepDay == null) {
            SleepDistribution sleepDistribution = new SleepDistribution(0, 0, 0);
            Date date = new Date();
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            DateTime dateTime = new DateTime(instance.getTimeInMillis());
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "Calendar.getInstance()");
            sleepDay = new MFSleepDay(date, i, 0, sleepDistribution, dateTime, new DateTime(instance2.getTimeInMillis()));
        } else {
            sleepDay.setGoalMinutes(i);
        }
        this.mSleepDao.upsertSleepDay(sleepDay);
    }

    @DexIgnore
    public final void saveSleepSummaries$app_fossilRelease(ku3 ku3, Date date, Date date2, lc6<? extends Date, ? extends Date> lc6) {
        Date date3;
        Date date4;
        List<SleepDayParse> list;
        wg6.b(ku3, "item");
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        try {
            ArrayList arrayList = new ArrayList();
            ApiResponse apiResponse = (ApiResponse) new du3().a().a(ku3.toString(), new SleepSummariesRepository$saveSleepSummaries$Anon1().getType());
            if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                for (SleepDayParse mFSleepBySleepDayParse : list) {
                    MFSleepDay mFSleepBySleepDayParse2 = mFSleepBySleepDayParse.getMFSleepBySleepDayParse();
                    if (mFSleepBySleepDayParse2 != null) {
                        arrayList.add(mFSleepBySleepDayParse2);
                    }
                }
            }
            FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
            if (!(lc6 == null || (date4 = (Date) lc6.getFirst()) == null)) {
                date = date4;
            }
            if (!(lc6 == null || (date3 = (Date) lc6.getSecond()) == null)) {
                date2 = date3;
            }
            List<FitnessDataWrapper> fitnessData = fitnessDataDao.getFitnessData(date, date2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "fitnessDatasSize " + fitnessData.size());
            if (fitnessData.isEmpty()) {
                this.mSleepDao.upsertSleepDays(arrayList);
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getSleepSummaries saveCallResult exception=");
            e.printStackTrace();
            sb.append(cd6.a);
            local2.e(str2, sb.toString());
        }
    }

    @DexIgnore
    public final void saveSleepSummary$app_fossilRelease(ku3 ku3, Date date) {
        List<SleepDayParse> list;
        wg6.b(ku3, "item");
        wg6.b(date, HardwareLog.COLUMN_DATE);
        try {
            ArrayList arrayList = new ArrayList();
            ApiResponse apiResponse = (ApiResponse) new du3().a().a(ku3.toString(), new SleepSummariesRepository$saveSleepSummary$Anon1().getType());
            if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                for (SleepDayParse mFSleepBySleepDayParse : list) {
                    MFSleepDay mFSleepBySleepDayParse2 = mFSleepBySleepDayParse.getMFSleepBySleepDayParse();
                    if (mFSleepBySleepDayParse2 != null) {
                        arrayList.add(mFSleepBySleepDayParse2);
                    }
                }
            }
            List<FitnessDataWrapper> fitnessData = this.mFitnessDataDao.getFitnessData(date, date);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "fitnessDatasSize " + fitnessData.size());
            if ((!arrayList.isEmpty()) && fitnessData.isEmpty()) {
                SleepDao sleepDao = this.mSleepDao;
                Object obj = arrayList.get(0);
                wg6.a(obj, "summaryList[0]");
                sleepDao.upsertSleepDay((MFSleepDay) obj);
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getSleepSummary exception=");
            e.printStackTrace();
            sb.append(cd6.a);
            local2.d(str2, sb.toString());
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$Anon1] */
    public final LiveData<yx5<Integer>> updateLastSleepGoal(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "updateLastSleepGoal sleepGoal=" + i);
        return new SleepSummariesRepository$updateLastSleepGoal$Anon1(this, i).asLiveData();
    }
}
