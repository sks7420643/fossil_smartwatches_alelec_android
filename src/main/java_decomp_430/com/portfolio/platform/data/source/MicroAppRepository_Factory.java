package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRepository_Factory implements Factory<MicroAppRepository> {
    @DexIgnore
    public /* final */ Provider<MicroAppDao> mMicroAppDaoProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppRemoteDataSource> mMicroAppRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;

    @DexIgnore
    public MicroAppRepository_Factory(Provider<MicroAppDao> provider, Provider<MicroAppRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        this.mMicroAppDaoProvider = provider;
        this.mMicroAppRemoteDataSourceProvider = provider2;
        this.mPortfolioAppProvider = provider3;
    }

    @DexIgnore
    public static MicroAppRepository_Factory create(Provider<MicroAppDao> provider, Provider<MicroAppRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new MicroAppRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static MicroAppRepository newMicroAppRepository(MicroAppDao microAppDao, MicroAppRemoteDataSource microAppRemoteDataSource, PortfolioApp portfolioApp) {
        return new MicroAppRepository(microAppDao, microAppRemoteDataSource, portfolioApp);
    }

    @DexIgnore
    public static MicroAppRepository provideInstance(Provider<MicroAppDao> provider, Provider<MicroAppRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new MicroAppRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public MicroAppRepository get() {
        return provideInstance(this.mMicroAppDaoProvider, this.mMicroAppRemoteDataSourceProvider, this.mPortfolioAppProvider);
    }
}
