package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.sd;
import com.fossil.v3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitiesRepository$getActivityList$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon1.Anon1_Level2 this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level4<I, O> implements v3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ List $resultList;
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3 this$0;

        @DexIgnore
        public Anon1_Level4(ActivitiesRepository$getActivityList$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3 activitiesRepository$getActivityList$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3, List list) {
            this.this$0 = activitiesRepository$getActivityList$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3;
            this.$resultList = list;
        }

        @DexIgnore
        public final List<ActivitySample> apply(List<ActivitySample> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getActivityList getActivityList Transformations activitySamples=" + list);
            wg6.a((Object) list, "activitySamples");
            if (!list.isEmpty()) {
                List list2 = this.$resultList;
                this.this$0.this$0.this$0.this$0.mFitnessHelper.a(list, list.get(0).getUid());
                list2.addAll(list);
            }
            return this.$resultList;
        }
    }

    @DexIgnore
    public ActivitiesRepository$getActivityList$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3(ActivitiesRepository$getActivityList$Anon1.Anon1_Level2 anon1_Level2) {
        this.this$0 = anon1_Level2;
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> apply(List<ActivitySample> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getActivityList loadFromDb: resultList=" + list);
        ActivitiesRepository$getActivityList$Anon1 activitiesRepository$getActivityList$Anon1 = this.this$0.this$0;
        ActivitiesRepository activitiesRepository = activitiesRepository$getActivityList$Anon1.this$0;
        Date date = activitiesRepository$getActivityList$Anon1.$endDate;
        wg6.a((Object) date, "endDate");
        return sd.a(activitiesRepository.getActivitySamplesInDate$app_fossilRelease(date), new Anon1_Level4(this, list));
    }
}
