package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.sd;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ HeartRateSampleDao mHeartRateSampleDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSampleRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSampleRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "HeartRateSampleRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSampleRepository(HeartRateSampleDao heartRateSampleDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        wg6.b(heartRateSampleDao, "mHeartRateSampleDao");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(apiServiceV2, "mApiService");
        this.mHeartRateSampleDao = heartRateSampleDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchHeartRateSamples$default(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, int i, int i2, xe6 xe6, int i3, Object obj) {
        return heartRateSampleRepository.fetchHeartRateSamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    public final void cleanUp() {
        this.mHeartRateSampleDao.deleteAllHeartRateSamples();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchHeartRateSamples(Date date, Date date2, int i, int i2, xe6<? super ap4<ApiResponse<HeartRate>>> xe6) {
        HeartRateSampleRepository$fetchHeartRateSamples$Anon1 heartRateSampleRepository$fetchHeartRateSamples$Anon1;
        int i3;
        ap4 ap4;
        ap4 ap42;
        Object obj;
        int i4;
        HeartRateSampleRepository heartRateSampleRepository;
        Date date3;
        int i5;
        String message;
        Date date4 = date;
        Date date5 = date2;
        xe6<? super ap4<ApiResponse<HeartRate>>> xe62 = xe6;
        if (xe62 instanceof HeartRateSampleRepository$fetchHeartRateSamples$Anon1) {
            heartRateSampleRepository$fetchHeartRateSamples$Anon1 = (HeartRateSampleRepository$fetchHeartRateSamples$Anon1) xe62;
            int i6 = heartRateSampleRepository$fetchHeartRateSamples$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                heartRateSampleRepository$fetchHeartRateSamples$Anon1.label = i6 - Integer.MIN_VALUE;
                HeartRateSampleRepository$fetchHeartRateSamples$Anon1 heartRateSampleRepository$fetchHeartRateSamples$Anon12 = heartRateSampleRepository$fetchHeartRateSamples$Anon1;
                Object obj2 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.result;
                Object a = ff6.a();
                i3 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.label;
                if (i3 != 0) {
                    nc6.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchHeartRateSamples: start = " + date4 + ", end = " + date5);
                    HeartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1 heartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1 = new HeartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1(this, date, date2, i, i2, (xe6) null);
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$0 = this;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$1 = date4;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$2 = date5;
                    i5 = i;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$0 = i5;
                    int i7 = i2;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$1 = i7;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.label = 1;
                    Object a2 = ResponseKt.a(heartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1, heartRateSampleRepository$fetchHeartRateSamples$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    heartRateSampleRepository = this;
                } else if (i3 == 1) {
                    int i8 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$1;
                    i5 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$0;
                    date3 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$2;
                    nc6.a(obj2);
                    i4 = i8;
                    date4 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$1;
                    heartRateSampleRepository = (HeartRateSampleRepository) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$0;
                } else if (i3 == 2) {
                    ap42 = (ap4) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$3;
                    int i9 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$1;
                    int i10 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$0;
                    Date date6 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$2;
                    Date date7 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$1;
                    HeartRateSampleRepository heartRateSampleRepository2 = (HeartRateSampleRepository) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$0;
                    try {
                        nc6.a(obj2);
                        obj = obj2;
                        return (ap4) obj;
                    } catch (Exception e) {
                        e = e;
                        ap4 = ap42;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj2;
                String str2 = null;
                if (ap4 instanceof cp4) {
                    if (ap4 instanceof zo4) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("fetchHeartRateSamples Failure code=");
                        zo4 zo4 = (zo4) ap4;
                        sb.append(zo4.a());
                        sb.append(" message=");
                        ServerError c = zo4.c();
                        if (c == null || (message = c.getMessage()) == null) {
                            ServerError c2 = zo4.c();
                            if (c2 != null) {
                                str2 = c2.getUserMessage();
                            }
                        } else {
                            str2 = message;
                        }
                        if (str2 == null) {
                            str2 = "";
                        }
                        sb.append(str2);
                        local2.d(str3, sb.toString());
                    }
                    return ap4;
                } else if (((cp4) ap4).a() == null) {
                    return ap4;
                } else {
                    try {
                        if (!((cp4) ap4).b()) {
                            ArrayList arrayList = new ArrayList();
                            for (HeartRate heartRateSample : ((ApiResponse) ((cp4) ap4).a()).get_items()) {
                                HeartRateSample heartRateSample2 = heartRateSample.toHeartRateSample();
                                if (heartRateSample2 != null) {
                                    arrayList.add(heartRateSample2);
                                }
                            }
                            if (!arrayList.isEmpty()) {
                                heartRateSampleRepository.mHeartRateSampleDao.upsertHeartRateSampleList(arrayList);
                            }
                        }
                        if (((ApiResponse) ((cp4) ap4).a()).get_range() == null) {
                            return ap4;
                        }
                        Range range = ((ApiResponse) ((cp4) ap4).a()).get_range();
                        if (range == null) {
                            wg6.a();
                            throw null;
                        } else if (!range.isHasNext()) {
                            return ap4;
                        } else {
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$0 = heartRateSampleRepository;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$1 = date4;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$2 = date3;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$0 = i5;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$1 = i4;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$3 = ap4;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.label = 2;
                            obj = heartRateSampleRepository.fetchHeartRateSamples(date4, date3, i5 + i4, i4, heartRateSampleRepository$fetchHeartRateSamples$Anon12);
                            if (obj == a) {
                                return a;
                            }
                            ap42 = ap4;
                            return (ap4) obj;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("fetchHeartRateSamples exception=");
                        e.printStackTrace();
                        sb2.append(cd6.a);
                        local3.e(str4, sb2.toString());
                        return ap4;
                    }
                }
            }
        }
        heartRateSampleRepository$fetchHeartRateSamples$Anon1 = new HeartRateSampleRepository$fetchHeartRateSamples$Anon1(this, xe62);
        HeartRateSampleRepository$fetchHeartRateSamples$Anon1 heartRateSampleRepository$fetchHeartRateSamples$Anon122 = heartRateSampleRepository$fetchHeartRateSamples$Anon1;
        Object obj22 = heartRateSampleRepository$fetchHeartRateSamples$Anon122.result;
        Object a3 = ff6.a();
        i3 = heartRateSampleRepository$fetchHeartRateSamples$Anon122.label;
        if (i3 != 0) {
        }
        ap4 = (ap4) obj22;
        String str22 = null;
        if (ap4 instanceof cp4) {
        }
    }

    @DexIgnore
    public final LiveData<yx5<List<HeartRateSample>>> getHeartRateSamples(Date date, Date date2, boolean z) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        Date o = bk4.o(date);
        Date j = bk4.j(date2);
        FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
        wg6.a((Object) o, "startDate");
        wg6.a((Object) j, "endDate");
        LiveData<yx5<List<HeartRateSample>>> b = sd.b(fitnessDataDao.getFitnessDataLiveData(o, j), new HeartRateSampleRepository$getHeartRateSamples$Anon1(this, o, j, z, date2));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insertFromDevice(List<HeartRateSample> list) {
        wg6.b(list, "heartRateSamples");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: heartRateSamples = " + list);
        this.mHeartRateSampleDao.insertHeartRateSamples(list);
    }
}
