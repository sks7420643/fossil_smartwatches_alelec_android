package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.cp4;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.sd;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.ze;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao mHeartRateSummaryDao;
    @DexIgnore
    public List<HeartRateSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSummaryRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "HeartRateSummaryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryRepository(HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        wg6.b(heartRateDailySummaryDao, "mHeartRateSummaryDao");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(apiServiceV2, "mApiService");
        this.mHeartRateSummaryDao = heartRateDailySummaryDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mHeartRateSummaryDao.deleteAllHeartRateSummaries();
    }

    @DexIgnore
    public final LiveData<yx5<List<DailyHeartRateSummary>>> getHeartRateSummaries(Date date, Date date2, boolean z) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getHeartRateSummaries: startDate = " + date + ", endDate = " + date2);
        LiveData<yx5<List<DailyHeartRateSummary>>> b = sd.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new HeartRateSummaryRepository$getHeartRateSummaries$Anon1(this, date, date2, z));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<DailyHeartRateSummary> getSummariesPaging(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, Date date, u04 u04, vk4.a aVar) {
        wg6.b(heartRateSummaryRepository, "summariesRepository");
        wg6.b(fitnessDataRepository, "fitnessDataRepository");
        wg6.b(heartRateDailySummaryDao, "heartRateDailySummaryDao");
        wg6.b(fitnessDatabase, "heartRateDatabase");
        wg6.b(date, "createdDate");
        u04 u042 = u04;
        wg6.b(u042, "appExecutors");
        vk4.a aVar2 = aVar;
        wg6.b(aVar2, "listener");
        HeartRateSummaryLocalDataSource.Companion companion = HeartRateSummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        wg6.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date);
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        HeartRateSummaryDataSourceFactory heartRateSummaryDataSourceFactory = new HeartRateSummaryDataSourceFactory(heartRateSummaryRepository, fitnessDataRepository, heartRateDailySummaryDao, fitnessDatabase, date, u042, aVar2, instance2);
        this.mSourceFactoryList.add(heartRateSummaryDataSourceFactory);
        cf.h.a aVar3 = new cf.h.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        cf.h a = aVar3.a();
        wg6.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new ze(heartRateSummaryDataSourceFactory, a).a();
        wg6.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = sd.b(heartRateSummaryDataSourceFactory.getSourceLiveData(), HeartRateSummaryRepository$getSummariesPaging$Anon1.INSTANCE);
        wg6.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new HeartRateSummaryRepository$getSummariesPaging$Anon2(heartRateSummaryDataSourceFactory), new HeartRateSummaryRepository$getSummariesPaging$Anon3(heartRateSummaryDataSourceFactory));
    }

    @DexIgnore
    public final void insertFromDevice(List<DailyHeartRateSummary> list) {
        wg6.b(list, "heartRateSummaries");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: heartRateSummaries = " + list);
        this.mHeartRateSummaryDao.insertHeartRateSummaries(list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: java.util.List} */
    /* JADX WARNING: type inference failed for: r4v0 */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r4v6, types: [java.util.List, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v9 */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object loadSummaries(Date date, Date date2, xe6<? super ap4<ku3>> xe6) {
        HeartRateSummaryRepository$loadSummaries$Anon1 heartRateSummaryRepository$loadSummaries$Anon1;
        int i;
        HeartRateSummaryRepository heartRateSummaryRepository;
        ap4 ap4;
        String message;
        if (xe6 instanceof HeartRateSummaryRepository$loadSummaries$Anon1) {
            heartRateSummaryRepository$loadSummaries$Anon1 = (HeartRateSummaryRepository$loadSummaries$Anon1) xe6;
            int i2 = heartRateSummaryRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                heartRateSummaryRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = heartRateSummaryRepository$loadSummaries$Anon1.result;
                Object a = ff6.a();
                i = heartRateSummaryRepository$loadSummaries$Anon1.label;
                List list = 0;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "loadSummaries startDate=" + date + ", endDate=" + date2);
                    HeartRateSummaryRepository$loadSummaries$response$Anon1 heartRateSummaryRepository$loadSummaries$response$Anon1 = new HeartRateSummaryRepository$loadSummaries$response$Anon1(this, date, date2, (xe6) null);
                    heartRateSummaryRepository$loadSummaries$Anon1.L$0 = this;
                    heartRateSummaryRepository$loadSummaries$Anon1.L$1 = date;
                    heartRateSummaryRepository$loadSummaries$Anon1.L$2 = date2;
                    heartRateSummaryRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(heartRateSummaryRepository$loadSummaries$response$Anon1, heartRateSummaryRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    heartRateSummaryRepository = this;
                } else if (i == 1) {
                    Date date3 = (Date) heartRateSummaryRepository$loadSummaries$Anon1.L$2;
                    Date date4 = (Date) heartRateSummaryRepository$loadSummaries$Anon1.L$1;
                    heartRateSummaryRepository = (HeartRateSummaryRepository) heartRateSummaryRepository$loadSummaries$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null && !cp4.b()) {
                        try {
                            du3 du3 = new du3();
                            du3.a(Long.TYPE, new GsonConvertDateTimeToLong());
                            du3.a(DateTime.class, new GsonConvertDateTime());
                            du3.a(Date.class, new GsonConverterShortDate());
                            ApiResponse apiResponse = (ApiResponse) du3.a().a(((ku3) ((cp4) ap4).a()).toString(), new HeartRateSummaryRepository$loadSummaries$summaries$Anon1().getType());
                            if (apiResponse != null) {
                                list = apiResponse.get_items();
                            }
                            if (list != 0) {
                                FLogger.INSTANCE.getLocal().d(TAG, String.valueOf(list));
                                heartRateSummaryRepository.mHeartRateSummaryDao.insertListDailyHeartRateSummary(list);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("loadSummaries exception=");
                            e.printStackTrace();
                            sb.append(cd6.a);
                            local2.e(str2, sb.toString());
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loadSummaries Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            list = c2.getUserMessage();
                        }
                    } else {
                        list = message;
                    }
                    if (list == 0) {
                        list = "";
                    }
                    sb2.append(list);
                    local3.d(str3, sb2.toString());
                }
                return ap4;
            }
        }
        heartRateSummaryRepository$loadSummaries$Anon1 = new HeartRateSummaryRepository$loadSummaries$Anon1(this, xe6);
        Object obj2 = heartRateSummaryRepository$loadSummaries$Anon1.result;
        Object a2 = ff6.a();
        i = heartRateSummaryRepository$loadSummaries$Anon1.label;
        List list2 = 0;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (HeartRateSummaryDataSourceFactory localDataSource : this.mSourceFactoryList) {
            HeartRateSummaryLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }
}
