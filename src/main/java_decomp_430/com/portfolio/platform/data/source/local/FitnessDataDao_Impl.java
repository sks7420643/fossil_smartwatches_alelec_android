package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.a44;
import com.fossil.ai;
import com.fossil.b44;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataDao_Impl extends FitnessDataDao {
    @DexIgnore
    public /* final */ a44 __dateTimeUTCStringConverter; // = new a44();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<FitnessDataWrapper> __deletionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ b44 __fitnessDataConverter; // = new b44();
    @DexIgnore
    public /* final */ hh<FitnessDataWrapper> __insertionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllFitnessData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<FitnessDataWrapper> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `fitness_data` (`step`,`activeMinute`,`calorie`,`distance`,`stress`,`resting`,`heartRate`,`sleeps`,`workouts`,`startTime`,`endTime`,`syncTime`,`timezoneOffsetInSecond`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, FitnessDataWrapper fitnessDataWrapper) {
            String a = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.step);
            if (a == null) {
                miVar.a(1);
            } else {
                miVar.a(1, a);
            }
            String a2 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.activeMinute);
            if (a2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, a2);
            }
            String a3 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.calorie);
            if (a3 == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a3);
            }
            String a4 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.distance);
            if (a4 == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a4);
            }
            String a5 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getStress());
            if (a5 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a5);
            }
            String a6 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getResting());
            if (a6 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a6);
            }
            String a7 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getHeartRate());
            if (a7 == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a7);
            }
            String b = FitnessDataDao_Impl.this.__fitnessDataConverter.b(fitnessDataWrapper.getSleeps());
            if (b == null) {
                miVar.a(8);
            } else {
                miVar.a(8, b);
            }
            String c = FitnessDataDao_Impl.this.__fitnessDataConverter.c(fitnessDataWrapper.getWorkouts());
            if (c == null) {
                miVar.a(9);
            } else {
                miVar.a(9, c);
            }
            String a8 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a8 == null) {
                miVar.a(10);
            } else {
                miVar.a(10, a8);
            }
            String a9 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getEndTime());
            if (a9 == null) {
                miVar.a(11);
            } else {
                miVar.a(11, a9);
            }
            String a10 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getSyncTime());
            if (a10 == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a10);
            }
            miVar.a(13, (long) fitnessDataWrapper.getTimezoneOffsetInSecond());
            if (fitnessDataWrapper.getSerialNumber() == null) {
                miVar.a(14);
            } else {
                miVar.a(14, fitnessDataWrapper.getSerialNumber());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<FitnessDataWrapper> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `fitness_data` WHERE `startTime` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, FitnessDataWrapper fitnessDataWrapper) {
            String a = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a == null) {
                miVar.a(1);
            } else {
                miVar.a(1, a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM fitness_data";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<FitnessDataWrapper>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<FitnessDataWrapper> call() throws Exception {
            Cursor a = bi.a(FitnessDataDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "step");
                int b2 = ai.b(a, "activeMinute");
                int b3 = ai.b(a, "calorie");
                int b4 = ai.b(a, "distance");
                int b5 = ai.b(a, "stress");
                int b6 = ai.b(a, "resting");
                int b7 = ai.b(a, "heartRate");
                int b8 = ai.b(a, "sleeps");
                int b9 = ai.b(a, "workouts");
                int b10 = ai.b(a, "startTime");
                int b11 = ai.b(a, "endTime");
                int b12 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
                int b13 = ai.b(a, "timezoneOffsetInSecond");
                int b14 = ai.b(a, "serialNumber");
                int i = b9;
                int i2 = b8;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b10;
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b10)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b11)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b12)), a.getInt(b13), a.getString(b14));
                    int i4 = b;
                    fitnessDataWrapper.step = FitnessDataDao_Impl.this.__fitnessDataConverter.g(a.getString(b));
                    fitnessDataWrapper.activeMinute = FitnessDataDao_Impl.this.__fitnessDataConverter.a(a.getString(b2));
                    fitnessDataWrapper.calorie = FitnessDataDao_Impl.this.__fitnessDataConverter.b(a.getString(b3));
                    fitnessDataWrapper.distance = FitnessDataDao_Impl.this.__fitnessDataConverter.c(a.getString(b4));
                    fitnessDataWrapper.setStress(FitnessDataDao_Impl.this.__fitnessDataConverter.h(a.getString(b5)));
                    fitnessDataWrapper.setResting(FitnessDataDao_Impl.this.__fitnessDataConverter.e(a.getString(b6)));
                    fitnessDataWrapper.setHeartRate(FitnessDataDao_Impl.this.__fitnessDataConverter.d(a.getString(b7)));
                    int i5 = i2;
                    i2 = i5;
                    fitnessDataWrapper.setSleeps(FitnessDataDao_Impl.this.__fitnessDataConverter.f(a.getString(i5)));
                    int i6 = i;
                    i = i6;
                    fitnessDataWrapper.setWorkouts(FitnessDataDao_Impl.this.__fitnessDataConverter.i(a.getString(i6)));
                    arrayList.add(fitnessDataWrapper);
                    b10 = i3;
                    b = i4;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public FitnessDataDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfFitnessDataWrapper = new Anon1(ohVar);
        this.__deletionAdapterOfFitnessDataWrapper = new Anon2(ohVar);
        this.__preparedStmtOfDeleteAllFitnessData = new Anon3(ohVar);
    }

    @DexIgnore
    public void deleteAllFitnessData() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllFitnessData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllFitnessData.release(acquire);
        }
    }

    @DexIgnore
    public void deleteFitnessData(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfFitnessDataWrapper.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2) {
        rh rhVar;
        rh b = rh.b("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "step");
            int b3 = ai.b(a3, "activeMinute");
            int b4 = ai.b(a3, "calorie");
            int b5 = ai.b(a3, "distance");
            int b6 = ai.b(a3, "stress");
            int b7 = ai.b(a3, "resting");
            int b8 = ai.b(a3, "heartRate");
            int b9 = ai.b(a3, "sleeps");
            int b10 = ai.b(a3, "workouts");
            int b11 = ai.b(a3, "startTime");
            int b12 = ai.b(a3, "endTime");
            int b13 = ai.b(a3, DataFile.COLUMN_SYNC_TIME);
            int b14 = ai.b(a3, "timezoneOffsetInSecond");
            rhVar = b;
            try {
                int b15 = ai.b(a3, "serialNumber");
                int i = b10;
                int i2 = b9;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    int i3 = b11;
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.a(a3.getString(b11)), this.__dateTimeUTCStringConverter.a(a3.getString(b12)), this.__dateTimeUTCStringConverter.a(a3.getString(b13)), a3.getInt(b14), a3.getString(b15));
                    int i4 = b2;
                    fitnessDataWrapper.step = this.__fitnessDataConverter.g(a3.getString(b2));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.a(a3.getString(b3));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.b(a3.getString(b4));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.c(a3.getString(b5));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.h(a3.getString(b6)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.e(a3.getString(b7)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.d(a3.getString(b8)));
                    int i5 = i2;
                    i2 = i5;
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.f(a3.getString(i5)));
                    int i6 = i;
                    i = i6;
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.i(a3.getString(i6)));
                    arrayList.add(fitnessDataWrapper);
                    b11 = i3;
                    b2 = i4;
                }
                a3.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a3.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2) {
        rh b = rh.b("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"fitness_data"}, false, new Anon4(b));
    }

    @DexIgnore
    public List<FitnessDataWrapper> getPendingFitnessData() {
        rh rhVar;
        rh b = rh.b("SELECT * FROM fitness_data ORDER BY startTime ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "step");
            int b3 = ai.b(a, "activeMinute");
            int b4 = ai.b(a, "calorie");
            int b5 = ai.b(a, "distance");
            int b6 = ai.b(a, "stress");
            int b7 = ai.b(a, "resting");
            int b8 = ai.b(a, "heartRate");
            int b9 = ai.b(a, "sleeps");
            int b10 = ai.b(a, "workouts");
            int b11 = ai.b(a, "startTime");
            int b12 = ai.b(a, "endTime");
            int b13 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            int b14 = ai.b(a, "timezoneOffsetInSecond");
            rhVar = b;
            try {
                int b15 = ai.b(a, "serialNumber");
                int i = b10;
                int i2 = b9;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b11;
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.a(a.getString(b11)), this.__dateTimeUTCStringConverter.a(a.getString(b12)), this.__dateTimeUTCStringConverter.a(a.getString(b13)), a.getInt(b14), a.getString(b15));
                    int i4 = b2;
                    fitnessDataWrapper.step = this.__fitnessDataConverter.g(a.getString(b2));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.a(a.getString(b3));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.b(a.getString(b4));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.c(a.getString(b5));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.h(a.getString(b6)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.e(a.getString(b7)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.d(a.getString(b8)));
                    int i5 = i2;
                    i2 = i5;
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.f(a.getString(i5)));
                    int i6 = i;
                    i = i6;
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.i(a.getString(i6)));
                    arrayList.add(fitnessDataWrapper);
                    b11 = i3;
                    b2 = i4;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public void insertFitnessDataList(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfFitnessDataWrapper.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
