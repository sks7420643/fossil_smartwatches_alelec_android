package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.t34;
import com.fossil.vh;
import com.fossil.w34;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRawDao_Impl extends SampleRawDao {
    @DexIgnore
    public /* final */ t34 __activityIntensitiesConverter; // = new t34();
    @DexIgnore
    public /* final */ w34 __dateLongStringConverter; // = new w34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<SampleRaw> __insertionAdapterOfSampleRaw;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<SampleRaw> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleraw` (`id`,`pinType`,`uaPinType`,`startTime`,`endTime`,`sourceId`,`sourceTypeValue`,`movementTypeValue`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, SampleRaw sampleRaw) {
            if (sampleRaw.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, sampleRaw.getId());
            }
            miVar.a(2, (long) sampleRaw.getPinType());
            miVar.a(3, (long) sampleRaw.getUaPinType());
            String a = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getStartTime());
            if (a == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a);
            }
            String a2 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getEndTime());
            if (a2 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a2);
            }
            if (sampleRaw.getSourceId() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, sampleRaw.getSourceId());
            }
            if (sampleRaw.getSourceTypeValue() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, sampleRaw.getSourceTypeValue());
            }
            if (sampleRaw.getMovementTypeValue() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, sampleRaw.getMovementTypeValue());
            }
            miVar.a(9, sampleRaw.getSteps());
            miVar.a(10, sampleRaw.getCalories());
            miVar.a(11, sampleRaw.getDistance());
            miVar.a(12, (long) sampleRaw.getActiveTime());
            String a3 = SampleRawDao_Impl.this.__activityIntensitiesConverter.a(sampleRaw.getIntensityDistInSteps());
            if (a3 == null) {
                miVar.a(13);
            } else {
                miVar.a(13, a3);
            }
            if (sampleRaw.getTimeZoneID() == null) {
                miVar.a(14);
            } else {
                miVar.a(14, sampleRaw.getTimeZoneID());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sampleraw";
        }
    }

    @DexIgnore
    public SampleRawDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfSampleRaw = new Anon1(ohVar);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(ohVar);
    }

    @DexIgnore
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    public List<SampleRaw> getListActivitySampleByUaType(int i) {
        rh rhVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        rh b = rh.b("SELECT * FROM sampleraw WHERE uaPinType = ?", 1);
        b.a(1, (long) i);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(sampleRawDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "pinType");
            int b4 = ai.b(a, "uaPinType");
            int b5 = ai.b(a, "startTime");
            int b6 = ai.b(a, "endTime");
            int b7 = ai.b(a, "sourceId");
            int b8 = ai.b(a, "sourceTypeValue");
            int b9 = ai.b(a, "movementTypeValue");
            int b10 = ai.b(a, "steps");
            int b11 = ai.b(a, Constants.CALORIES);
            int b12 = ai.b(a, "distance");
            int b13 = ai.b(a, "activeTime");
            int b14 = ai.b(a, "intensityDistInSteps");
            rhVar = b;
            try {
                int b15 = ai.b(a, "timeZoneID");
                int i2 = b4;
                int i3 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i4 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    int i5 = i3;
                    int i6 = b2;
                    sampleRaw.setPinType(a.getInt(i5));
                    int i7 = i2;
                    sampleRaw.setUaPinType(a.getInt(i7));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i2 = i7;
                    b2 = i6;
                    i3 = i5;
                    b5 = i4;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<SampleRaw> getPendingActivitySamples() {
        rh rhVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        rh b = rh.b("SELECT * FROM sampleraw WHERE pinType <> 0", 0);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(sampleRawDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "pinType");
            int b4 = ai.b(a, "uaPinType");
            int b5 = ai.b(a, "startTime");
            int b6 = ai.b(a, "endTime");
            int b7 = ai.b(a, "sourceId");
            int b8 = ai.b(a, "sourceTypeValue");
            int b9 = ai.b(a, "movementTypeValue");
            int b10 = ai.b(a, "steps");
            int b11 = ai.b(a, Constants.CALORIES);
            int b12 = ai.b(a, "distance");
            int b13 = ai.b(a, "activeTime");
            int b14 = ai.b(a, "intensityDistInSteps");
            rhVar = b;
            try {
                int b15 = ai.b(a, "timeZoneID");
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    int i4 = i2;
                    int i5 = b2;
                    sampleRaw.setPinType(a.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a.getInt(i6));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i6;
                    b2 = i5;
                    i2 = i4;
                    b5 = i3;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public void upsertListActivitySample(List<SampleRaw> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSampleRaw.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<SampleRaw> getPendingActivitySamples(Date date, Date date2) {
        rh rhVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        rh b = rh.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? AND pinType <> 0", 2);
        String a = sampleRawDao_Impl.__dateLongStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = sampleRawDao_Impl.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(sampleRawDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "id");
            int b3 = ai.b(a3, "pinType");
            int b4 = ai.b(a3, "uaPinType");
            int b5 = ai.b(a3, "startTime");
            int b6 = ai.b(a3, "endTime");
            int b7 = ai.b(a3, "sourceId");
            int b8 = ai.b(a3, "sourceTypeValue");
            int b9 = ai.b(a3, "movementTypeValue");
            int b10 = ai.b(a3, "steps");
            int b11 = ai.b(a3, Constants.CALORIES);
            int b12 = ai.b(a3, "distance");
            int b13 = ai.b(a3, "activeTime");
            int b14 = ai.b(a3, "intensityDistInSteps");
            rhVar = b;
            try {
                int b15 = ai.b(a3, "timeZoneID");
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    int i3 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b6)), a3.getString(b7), a3.getString(b8), a3.getString(b9), a3.getDouble(b10), a3.getDouble(b11), a3.getDouble(b12), a3.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a3.getString(b14)), a3.getString(b15));
                    sampleRaw.setId(a3.getString(b2));
                    int i4 = i2;
                    int i5 = b2;
                    sampleRaw.setPinType(a3.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a3.getInt(i6));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i6;
                    b2 = i5;
                    i2 = i4;
                    b5 = i3;
                }
                a3.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a3.close();
            rhVar.c();
            throw th;
        }
    }
}
