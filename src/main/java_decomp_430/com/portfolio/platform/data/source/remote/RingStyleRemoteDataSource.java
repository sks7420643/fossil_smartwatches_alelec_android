package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "RingStyleRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public RingStyleRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mService");
        this.mService = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getRingStyleList(String str, boolean z, xe6<? super ap4<ArrayList<DianaComplicationRingStyle>>> xe6) {
        RingStyleRemoteDataSource$getRingStyleList$Anon1 ringStyleRemoteDataSource$getRingStyleList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof RingStyleRemoteDataSource$getRingStyleList$Anon1) {
            ringStyleRemoteDataSource$getRingStyleList$Anon1 = (RingStyleRemoteDataSource$getRingStyleList$Anon1) xe6;
            int i2 = ringStyleRemoteDataSource$getRingStyleList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                ringStyleRemoteDataSource$getRingStyleList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = ringStyleRemoteDataSource$getRingStyleList$Anon1.result;
                Object a = ff6.a();
                i = ringStyleRemoteDataSource$getRingStyleList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    RingStyleRemoteDataSource$getRingStyleList$response$Anon1 ringStyleRemoteDataSource$getRingStyleList$response$Anon1 = new RingStyleRemoteDataSource$getRingStyleList$response$Anon1(this, z, str, (xe6) null);
                    ringStyleRemoteDataSource$getRingStyleList$Anon1.L$0 = this;
                    ringStyleRemoteDataSource$getRingStyleList$Anon1.L$1 = str;
                    ringStyleRemoteDataSource$getRingStyleList$Anon1.Z$0 = z;
                    ringStyleRemoteDataSource$getRingStyleList$Anon1.label = 1;
                    obj = ResponseKt.a(ringStyleRemoteDataSource$getRingStyleList$response$Anon1, ringStyleRemoteDataSource$getRingStyleList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    boolean z2 = ringStyleRemoteDataSource$getRingStyleList$Anon1.Z$0;
                    String str2 = (String) ringStyleRemoteDataSource$getRingStyleList$Anon1.L$1;
                    RingStyleRemoteDataSource ringStyleRemoteDataSource = (RingStyleRemoteDataSource) ringStyleRemoteDataSource$getRingStyleList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getRingStyleList() data: ");
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        sb.append(((ApiResponse) a2).get_items());
                        sb.append(" - isFromCache: ");
                        sb.append(cp4.b());
                        local.d(TAG, sb.toString());
                        List list = ((ApiResponse) cp4.a()).get_items();
                        if (!(list instanceof ArrayList)) {
                            list = null;
                        }
                        return new cp4((ArrayList) list, cp4.b());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("getRingStyleList() error=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.c());
                    local2.e(TAG, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        ringStyleRemoteDataSource$getRingStyleList$Anon1 = new RingStyleRemoteDataSource$getRingStyleList$Anon1(this, xe6);
        Object obj2 = ringStyleRemoteDataSource$getRingStyleList$Anon1.result;
        Object a3 = ff6.a();
        i = ringStyleRemoteDataSource$getRingStyleList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
