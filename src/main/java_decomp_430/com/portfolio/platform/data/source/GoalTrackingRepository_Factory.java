package com.portfolio.platform.data.source;

import com.fossil.an4;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository_Factory implements Factory<GoalTrackingRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<GoalTrackingDao> mGoalTrackingDaoProvider;
    @DexIgnore
    public /* final */ Provider<GoalTrackingDatabase> mGoalTrackingDatabaseProvider;
    @DexIgnore
    public /* final */ Provider<an4> mSharedPreferencesManagerProvider;
    @DexIgnore
    public /* final */ Provider<UserRepository> mUserRepositoryProvider;

    @DexIgnore
    public GoalTrackingRepository_Factory(Provider<GoalTrackingDatabase> provider, Provider<GoalTrackingDao> provider2, Provider<UserRepository> provider3, Provider<an4> provider4, Provider<ApiServiceV2> provider5) {
        this.mGoalTrackingDatabaseProvider = provider;
        this.mGoalTrackingDaoProvider = provider2;
        this.mUserRepositoryProvider = provider3;
        this.mSharedPreferencesManagerProvider = provider4;
        this.mApiServiceV2Provider = provider5;
    }

    @DexIgnore
    public static GoalTrackingRepository_Factory create(Provider<GoalTrackingDatabase> provider, Provider<GoalTrackingDao> provider2, Provider<UserRepository> provider3, Provider<an4> provider4, Provider<ApiServiceV2> provider5) {
        return new GoalTrackingRepository_Factory(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static GoalTrackingRepository newGoalTrackingRepository(GoalTrackingDatabase goalTrackingDatabase, GoalTrackingDao goalTrackingDao, UserRepository userRepository, an4 an4, ApiServiceV2 apiServiceV2) {
        return new GoalTrackingRepository(goalTrackingDatabase, goalTrackingDao, userRepository, an4, apiServiceV2);
    }

    @DexIgnore
    public static GoalTrackingRepository provideInstance(Provider<GoalTrackingDatabase> provider, Provider<GoalTrackingDao> provider2, Provider<UserRepository> provider3, Provider<an4> provider4, Provider<ApiServiceV2> provider5) {
        return new GoalTrackingRepository(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get());
    }

    @DexIgnore
    public GoalTrackingRepository get() {
        return provideInstance(this.mGoalTrackingDatabaseProvider, this.mGoalTrackingDaoProvider, this.mUserRepositoryProvider, this.mSharedPreferencesManagerProvider, this.mApiServiceV2Provider);
    }
}
