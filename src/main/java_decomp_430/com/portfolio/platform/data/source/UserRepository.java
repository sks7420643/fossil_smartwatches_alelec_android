package com.portfolio.platform.data.source;

import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.lc6;
import com.fossil.lh4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rh4;
import com.fossil.wg6;
import com.fossil.wj4;
import com.fossil.xe6;
import com.fossil.zj4;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRepository extends UserDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ an4 mSharedPreferencesManager;
    @DexIgnore
    public /* final */ UserDataSource mUserLocalDataSource;
    @DexIgnore
    public /* final */ UserDataSource mUserRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "UserRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRepository(@Remote UserDataSource userDataSource, @Local UserDataSource userDataSource2, an4 an4) {
        wg6.b(userDataSource, "mUserRemoteDataSource");
        wg6.b(userDataSource2, "mUserLocalDataSource");
        wg6.b(an4, "mSharedPreferencesManager");
        this.mUserRemoteDataSource = userDataSource;
        this.mUserLocalDataSource = userDataSource2;
        this.mSharedPreferencesManager = an4;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, xe6<? super ap4<Boolean>> xe6) {
        return this.mUserRemoteDataSource.checkAuthenticationEmailExisting(str, xe6);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, xe6<? super ap4<Boolean>> xe6) {
        return this.mUserRemoteDataSource.checkAuthenticationSocialExisting(str, str2, xe6);
    }

    @DexIgnore
    public void clearAllUser() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearAllUser");
        this.mUserLocalDataSource.clearAllUser();
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, xe6<? super Integer> xe6) {
        return this.mUserRemoteDataSource.deleteUser(mFUser, xe6);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return this.mUserLocalDataSource.getCurrentUser();
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        wg6.b(mFUser, "user");
        this.mUserLocalDataSource.insertUser(mFUser);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object loadUserInfo(xe6<? super ap4<MFUser>> xe6) {
        UserRepository$loadUserInfo$Anon1 userRepository$loadUserInfo$Anon1;
        int i;
        UserRepository userRepository;
        MFUser mFUser;
        MFUser mFUser2;
        ap4 ap4;
        if (xe6 instanceof UserRepository$loadUserInfo$Anon1) {
            userRepository$loadUserInfo$Anon1 = (UserRepository$loadUserInfo$Anon1) xe6;
            int i2 = userRepository$loadUserInfo$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$loadUserInfo$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$loadUserInfo$Anon1.result;
                Object a = ff6.a();
                i = userRepository$loadUserInfo$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    mFUser2 = getCurrentUser();
                    if (mFUser2 != null) {
                        UserDataSource userDataSource = this.mUserRemoteDataSource;
                        userRepository$loadUserInfo$Anon1.L$0 = this;
                        userRepository$loadUserInfo$Anon1.L$1 = mFUser2;
                        userRepository$loadUserInfo$Anon1.L$2 = mFUser2;
                        userRepository$loadUserInfo$Anon1.label = 1;
                        obj = userDataSource.loadUserInfo(mFUser2, userRepository$loadUserInfo$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        userRepository = this;
                        mFUser = mFUser2;
                    } else {
                        FLogger.INSTANCE.getLocal().e(TAG, "user is null");
                        return new zo4(600, (ServerError) null, (Throwable) null, (String) null, 8, (qg6) null);
                    }
                } else if (i == 1) {
                    userRepository = (UserRepository) userRepository$loadUserInfo$Anon1.L$0;
                    nc6.a(obj);
                    MFUser mFUser3 = (MFUser) userRepository$loadUserInfo$Anon1.L$1;
                    mFUser = (MFUser) userRepository$loadUserInfo$Anon1.L$2;
                    mFUser2 = mFUser3;
                } else if (i == 2) {
                    ap4 ap42 = (ap4) userRepository$loadUserInfo$Anon1.L$3;
                    MFUser mFUser4 = (MFUser) userRepository$loadUserInfo$Anon1.L$2;
                    MFUser mFUser5 = (MFUser) userRepository$loadUserInfo$Anon1.L$1;
                    UserRepository userRepository2 = (UserRepository) userRepository$loadUserInfo$Anon1.L$0;
                    nc6.a(obj);
                    return (ap4) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (ap4 instanceof cp4) {
                    return ap4;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("loadUserInfo success isFromCache ");
                cp4 cp4 = (cp4) ap4;
                sb.append(cp4.b());
                local.d(str, sb.toString());
                if (cp4.b()) {
                    return ap4;
                }
                UserDataSource userDataSource2 = userRepository.mUserLocalDataSource;
                Object a2 = cp4.a();
                if (a2 != null) {
                    userRepository$loadUserInfo$Anon1.L$0 = userRepository;
                    userRepository$loadUserInfo$Anon1.L$1 = mFUser2;
                    userRepository$loadUserInfo$Anon1.L$2 = mFUser;
                    userRepository$loadUserInfo$Anon1.L$3 = ap4;
                    userRepository$loadUserInfo$Anon1.label = 2;
                    obj = userDataSource2.updateUser((MFUser) a2, false, userRepository$loadUserInfo$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    return (ap4) obj;
                }
                wg6.a();
                throw null;
            }
        }
        userRepository$loadUserInfo$Anon1 = new UserRepository$loadUserInfo$Anon1(this, xe6);
        Object obj2 = userRepository$loadUserInfo$Anon1.result;
        Object a3 = ff6.a();
        i = userRepository$loadUserInfo$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (ap4 instanceof cp4) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public Object loginEmail(String str, String str2, xe6<? super ap4<Auth>> xe6) {
        UserRepository$loginEmail$Anon1 userRepository$loginEmail$Anon1;
        int i;
        UserRepository userRepository;
        ap4 ap4;
        Integer accessTokenExpiresIn;
        String str3 = str;
        String str4 = str2;
        xe6<? super ap4<Auth>> xe62 = xe6;
        if (xe62 instanceof UserRepository$loginEmail$Anon1) {
            userRepository$loginEmail$Anon1 = (UserRepository$loginEmail$Anon1) xe62;
            int i2 = userRepository$loginEmail$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$loginEmail$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$loginEmail$Anon1.result;
                Object a = ff6.a();
                i = userRepository$loginEmail$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$loginEmail$Anon1.L$0 = this;
                    userRepository$loginEmail$Anon1.L$1 = str3;
                    userRepository$loginEmail$Anon1.L$2 = str4;
                    userRepository$loginEmail$Anon1.label = 1;
                    obj = userDataSource.loginEmail(str3, str4, userRepository$loginEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    String str5 = (String) userRepository$loginEmail$Anon1.L$2;
                    String str6 = (String) userRepository$loginEmail$Anon1.L$1;
                    userRepository = (UserRepository) userRepository$loginEmail$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                String str7 = null;
                if (!(ap4 instanceof cp4)) {
                    Auth auth = (Auth) ((cp4) ap4).a();
                    MFUser mFUser = new MFUser();
                    mFUser.setAuthType(lh4.EMAIL.getValue());
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    mFUser.setAccessTokenExpiresAt(bk4.u(auth != null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setAccessTokenExpiresIn(hf6.a((auth == null || (accessTokenExpiresIn = auth.getAccessTokenExpiresIn()) == null) ? 0 : accessTokenExpiresIn.intValue()));
                    mFUser.setUserId(auth != null ? auth.getUid() : null);
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.w(auth != null ? auth.getAccessToken() : null);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.get.instance().J();
                    return new cp4(auth, false, 2, (qg6) null);
                } else if (!(ap4 instanceof zo4)) {
                    return new zo4(600, new ServerError(), (Throwable) null, (String) null, 8, (qg6) null);
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loginEmail Failure error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str7 = c.getMessage();
                    }
                    sb.append(str7);
                    local.d(str8, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
            }
        }
        userRepository$loginEmail$Anon1 = new UserRepository$loginEmail$Anon1(this, xe62);
        Object obj2 = userRepository$loginEmail$Anon1.result;
        Object a2 = ff6.a();
        i = userRepository$loginEmail$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        String str72 = null;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public Object loginWithSocial(String str, String str2, String str3, xe6<? super ap4<Auth>> xe6) {
        UserRepository$loginWithSocial$Anon1 userRepository$loginWithSocial$Anon1;
        int i;
        UserRepository userRepository;
        ap4 ap4;
        Integer accessTokenExpiresIn;
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        xe6<? super ap4<Auth>> xe62 = xe6;
        if (xe62 instanceof UserRepository$loginWithSocial$Anon1) {
            userRepository$loginWithSocial$Anon1 = (UserRepository$loginWithSocial$Anon1) xe62;
            int i2 = userRepository$loginWithSocial$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$loginWithSocial$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$loginWithSocial$Anon1.result;
                Object a = ff6.a();
                i = userRepository$loginWithSocial$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$loginWithSocial$Anon1.L$0 = this;
                    userRepository$loginWithSocial$Anon1.L$1 = str4;
                    userRepository$loginWithSocial$Anon1.L$2 = str5;
                    userRepository$loginWithSocial$Anon1.L$3 = str6;
                    userRepository$loginWithSocial$Anon1.label = 1;
                    obj = userDataSource.loginWithSocial(str4, str5, str6, userRepository$loginWithSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    String str7 = (String) userRepository$loginWithSocial$Anon1.L$3;
                    String str8 = (String) userRepository$loginWithSocial$Anon1.L$2;
                    str4 = (String) userRepository$loginWithSocial$Anon1.L$1;
                    userRepository = (UserRepository) userRepository$loginWithSocial$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                String str9 = null;
                if (!(ap4 instanceof cp4)) {
                    Auth auth = (Auth) ((cp4) ap4).a();
                    MFUser mFUser = new MFUser();
                    switch (str4.hashCode()) {
                        case -1240244679:
                            if (str4.equals("google")) {
                                mFUser.setAuthType(lh4.GOOGLE.getValue());
                                break;
                            }
                            break;
                        case -791770330:
                            if (str4.equals("wechat")) {
                                mFUser.setAuthType(lh4.WECHAT.getValue());
                                break;
                            }
                            break;
                        case 93029210:
                            if (str4.equals("apple")) {
                                mFUser.setAuthType(lh4.APPLE.getValue());
                                break;
                            }
                            break;
                        case 113011944:
                            if (str4.equals("weibo")) {
                                mFUser.setAuthType(lh4.WEIBO.getValue());
                                break;
                            }
                            break;
                        case 497130182:
                            if (str4.equals(Constants.FACEBOOK)) {
                                mFUser.setAuthType(lh4.FACEBOOK.getValue());
                                break;
                            }
                            break;
                    }
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    mFUser.setAccessTokenExpiresIn(hf6.a((auth == null || (accessTokenExpiresIn = auth.getAccessTokenExpiresIn()) == null) ? 0 : accessTokenExpiresIn.intValue()));
                    mFUser.setAccessTokenExpiresAt(bk4.u(auth != null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setUserId(auth != null ? auth.getUid() : null);
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.w(auth != null ? auth.getAccessToken() : null);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.get.instance().J();
                    return new cp4(auth, false, 2, (qg6) null);
                } else if (!(ap4 instanceof zo4)) {
                    return new zo4(600, new ServerError(), (Throwable) null, (String) null, 8, (qg6) null);
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str10 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loginWithSocial Failure error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str9 = c.getMessage();
                    }
                    sb.append(str9);
                    local.d(str10, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
            }
        }
        userRepository$loginWithSocial$Anon1 = new UserRepository$loginWithSocial$Anon1(this, xe62);
        Object obj2 = userRepository$loginWithSocial$Anon1.result;
        Object a2 = ff6.a();
        i = userRepository$loginWithSocial$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        String str92 = null;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public Object logoutUser(xe6<? super Integer> xe6) {
        return this.mUserRemoteDataSource.logoutUser(xe6);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, xe6<? super ap4<Void>> xe6) {
        return this.mUserRemoteDataSource.requestEmailOtp(str, xe6);
    }

    @DexIgnore
    public Object resetPassword(String str, xe6<? super ap4<Integer>> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "resetPassword");
        return this.mUserRemoteDataSource.resetPassword(str, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, xe6<? super ap4<Auth>> xe6) {
        UserRepository$signUpEmail$Anon1 userRepository$signUpEmail$Anon1;
        int i;
        UserRepository userRepository;
        ap4 ap4;
        Integer accessTokenExpiresIn;
        if (xe6 instanceof UserRepository$signUpEmail$Anon1) {
            userRepository$signUpEmail$Anon1 = (UserRepository$signUpEmail$Anon1) xe6;
            int i2 = userRepository$signUpEmail$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$signUpEmail$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$signUpEmail$Anon1.result;
                Object a = ff6.a();
                i = userRepository$signUpEmail$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$signUpEmail$Anon1.L$0 = this;
                    userRepository$signUpEmail$Anon1.L$1 = signUpEmailAuth;
                    userRepository$signUpEmail$Anon1.label = 1;
                    obj = userDataSource.signUpEmail(signUpEmailAuth, userRepository$signUpEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    signUpEmailAuth = (SignUpEmailAuth) userRepository$signUpEmail$Anon1.L$1;
                    userRepository = (UserRepository) userRepository$signUpEmail$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    Auth auth = (Auth) cp4.a();
                    MFUser mFUser = new MFUser();
                    lc6<Integer, Integer> a2 = wj4.a.a(rh4.Companion.a(signUpEmailAuth.getGender()), MFUser.getAge(signUpEmailAuth.getBirthday()));
                    mFUser.setAuthType(lh4.EMAIL.getValue());
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    mFUser.setAccessTokenExpiresIn(hf6.a((auth == null || (accessTokenExpiresIn = auth.getAccessTokenExpiresIn()) == null) ? 0 : accessTokenExpiresIn.intValue()));
                    mFUser.setAccessTokenExpiresAt(bk4.u(auth != null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setUserId(auth != null ? auth.getUid() : null);
                    mFUser.setEmail(signUpEmailAuth.getEmail());
                    mFUser.setFirstName(signUpEmailAuth.getFirstName());
                    mFUser.setLastName(signUpEmailAuth.getLastName());
                    mFUser.setGender(signUpEmailAuth.getGender());
                    mFUser.setBirthday(signUpEmailAuth.getBirthday());
                    mFUser.setDiagnosticEnabled(signUpEmailAuth.getDiagnosticEnabled());
                    mFUser.setCreatedAt(bk4.u(new Date()));
                    mFUser.setUpdatedAt(mFUser.getCreatedAt());
                    mFUser.setOnboardingComplete(true);
                    mFUser.setHeightInCentimeters(a2.getFirst().intValue());
                    mFUser.setUseDefaultBiometric(true);
                    mFUser.setUseDefaultGoals(true);
                    mFUser.setWeightInGrams((int) zj4.h((float) a2.getSecond().intValue()));
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.get.instance().J();
                    return new cp4(cp4.a(), false, 2, (qg6) null);
                } else if (!(ap4 instanceof zo4)) {
                    return null;
                } else {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
            }
        }
        userRepository$signUpEmail$Anon1 = new UserRepository$signUpEmail$Anon1(this, xe6);
        Object obj2 = userRepository$signUpEmail$Anon1.result;
        Object a3 = ff6.a();
        i = userRepository$signUpEmail$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, xe6<? super ap4<Auth>> xe6) {
        UserRepository$signUpSocial$Anon1 userRepository$signUpSocial$Anon1;
        int i;
        UserRepository userRepository;
        ap4 ap4;
        Integer accessTokenExpiresIn;
        if (xe6 instanceof UserRepository$signUpSocial$Anon1) {
            userRepository$signUpSocial$Anon1 = (UserRepository$signUpSocial$Anon1) xe6;
            int i2 = userRepository$signUpSocial$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$signUpSocial$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$signUpSocial$Anon1.result;
                Object a = ff6.a();
                i = userRepository$signUpSocial$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$signUpSocial$Anon1.L$0 = this;
                    userRepository$signUpSocial$Anon1.L$1 = signUpSocialAuth;
                    userRepository$signUpSocial$Anon1.label = 1;
                    obj = userDataSource.signUpSocial(signUpSocialAuth, userRepository$signUpSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    signUpSocialAuth = (SignUpSocialAuth) userRepository$signUpSocial$Anon1.L$1;
                    userRepository = (UserRepository) userRepository$signUpSocial$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    Auth auth = (Auth) cp4.a();
                    MFUser mFUser = new MFUser();
                    wj4 wj4 = wj4.a;
                    rh4 a2 = rh4.Companion.a(signUpSocialAuth.getGender());
                    if (a2 == null) {
                        a2 = rh4.OTHER;
                    }
                    lc6<Integer, Integer> a3 = wj4.a(a2, MFUser.getAge(signUpSocialAuth.getBirthday()));
                    mFUser.setAuthType(lh4.EMAIL.getValue());
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    mFUser.setAccessTokenExpiresIn(hf6.a((auth == null || (accessTokenExpiresIn = auth.getAccessTokenExpiresIn()) == null) ? 0 : accessTokenExpiresIn.intValue()));
                    mFUser.setAccessTokenExpiresAt(bk4.u(auth != null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setUserId(auth != null ? auth.getUid() : null);
                    mFUser.setEmail(signUpSocialAuth.getEmail());
                    mFUser.setFirstName(signUpSocialAuth.getFirstName());
                    mFUser.setLastName(signUpSocialAuth.getLastName());
                    mFUser.setGender(signUpSocialAuth.getGender());
                    mFUser.setBirthday(signUpSocialAuth.getBirthday());
                    mFUser.setDiagnosticEnabled(signUpSocialAuth.getDiagnosticEnabled());
                    mFUser.setCreatedAt(bk4.u(new Date()));
                    mFUser.setUpdatedAt(mFUser.getCreatedAt());
                    mFUser.setOnboardingComplete(true);
                    mFUser.setHeightInCentimeters(a3.getFirst().intValue());
                    mFUser.setUseDefaultBiometric(true);
                    mFUser.setWeightInGrams((int) zj4.h((float) a3.getSecond().intValue()));
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.get.instance().J();
                    return new cp4(cp4.a(), false, 2, (qg6) null);
                } else if (!(ap4 instanceof zo4)) {
                    return null;
                } else {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
            }
        }
        userRepository$signUpSocial$Anon1 = new UserRepository$signUpSocial$Anon1(this, xe6);
        Object obj2 = userRepository$signUpSocial$Anon1.result;
        Object a4 = ff6.a();
        i = userRepository$signUpSocial$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object updateUser(MFUser mFUser, boolean z, xe6<? super ap4<MFUser>> xe6) {
        UserRepository$updateUser$Anon1 userRepository$updateUser$Anon1;
        int i;
        UserRepository userRepository;
        ap4 ap4;
        if (xe6 instanceof UserRepository$updateUser$Anon1) {
            userRepository$updateUser$Anon1 = (UserRepository$updateUser$Anon1) xe6;
            int i2 = userRepository$updateUser$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$updateUser$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$updateUser$Anon1.result;
                Object a = ff6.a();
                i = userRepository$updateUser$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    if (z) {
                        FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateUser updateUserInfo on server");
                        UserDataSource userDataSource = this.mUserRemoteDataSource;
                        userRepository$updateUser$Anon1.L$0 = this;
                        userRepository$updateUser$Anon1.L$1 = mFUser;
                        userRepository$updateUser$Anon1.Z$0 = z;
                        userRepository$updateUser$Anon1.label = 1;
                        obj = userDataSource.updateUser(mFUser, true, userRepository$updateUser$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        userRepository = this;
                    } else {
                        UserDataSource userDataSource2 = this.mUserLocalDataSource;
                        userRepository$updateUser$Anon1.L$0 = this;
                        userRepository$updateUser$Anon1.L$1 = mFUser;
                        userRepository$updateUser$Anon1.Z$0 = z;
                        userRepository$updateUser$Anon1.label = 3;
                        obj = userDataSource2.updateUser(mFUser, z, userRepository$updateUser$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        return (ap4) obj;
                    }
                } else if (i == 1) {
                    z = userRepository$updateUser$Anon1.Z$0;
                    mFUser = (MFUser) userRepository$updateUser$Anon1.L$1;
                    userRepository = (UserRepository) userRepository$updateUser$Anon1.L$0;
                    nc6.a(obj);
                } else if (i == 2) {
                    MFUser mFUser2 = (MFUser) userRepository$updateUser$Anon1.L$3;
                    ap4 ap42 = (ap4) userRepository$updateUser$Anon1.L$2;
                    boolean z2 = userRepository$updateUser$Anon1.Z$0;
                    MFUser mFUser3 = (MFUser) userRepository$updateUser$Anon1.L$1;
                    UserRepository userRepository2 = (UserRepository) userRepository$updateUser$Anon1.L$0;
                    nc6.a(obj);
                    return (ap4) obj;
                } else if (i == 3) {
                    boolean z3 = userRepository$updateUser$Anon1.Z$0;
                    MFUser mFUser4 = (MFUser) userRepository$updateUser$Anon1.L$1;
                    UserRepository userRepository3 = (UserRepository) userRepository$updateUser$Anon1.L$0;
                    nc6.a(obj);
                    return (ap4) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateUser updateUserInfo success on server");
                    Object a2 = ((cp4) ap4).a();
                    if (a2 != null) {
                        MFUser mFUser5 = (MFUser) a2;
                        mFUser5.setOnboardingComplete(mFUser.isOnboardingComplete());
                        UserDataSource userDataSource3 = userRepository.mUserLocalDataSource;
                        userRepository$updateUser$Anon1.L$0 = userRepository;
                        userRepository$updateUser$Anon1.L$1 = mFUser;
                        userRepository$updateUser$Anon1.Z$0 = z;
                        userRepository$updateUser$Anon1.L$2 = ap4;
                        userRepository$updateUser$Anon1.L$3 = mFUser5;
                        userRepository$updateUser$Anon1.label = 2;
                        obj = userDataSource3.updateUser(mFUser5, false, userRepository$updateUser$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        return (ap4) obj;
                    }
                    wg6.a();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateUser updateUserInfo error on server");
                return ap4;
            }
        }
        userRepository$updateUser$Anon1 = new UserRepository$updateUser$Anon1(this, xe6);
        Object obj2 = userRepository$updateUser$Anon1.result;
        Object a3 = ff6.a();
        i = userRepository$updateUser$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, xe6<? super ap4<Void>> xe6) {
        return this.mUserRemoteDataSource.verifyEmailOtp(str, str2, xe6);
    }
}
