package com.portfolio.platform.data.source.remote;

import android.text.TextUtils;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cp4;
import com.fossil.ej6;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.hf6;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rh4;
import com.fossil.tj4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yj6;
import com.fossil.zh4;
import com.fossil.zo4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.User;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserDataSource;
import com.portfolio.platform.response.ResponseKt;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRemoteDataSource extends UserDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ AuthApiGuestService mAuthApiGuestService;
    @DexIgnore
    public /* final */ AuthApiUserService mAuthApiUserService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "UserRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRemoteDataSource(ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        wg6.b(apiServiceV2, "mApiService");
        wg6.b(authApiGuestService, "mAuthApiGuestService");
        wg6.b(authApiUserService, "mAuthApiUserService");
        this.mApiService = apiServiceV2;
        this.mAuthApiGuestService = authApiGuestService;
        this.mAuthApiUserService = authApiUserService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object checkAuthenticationEmailExisting(String str, xe6<? super ap4<Boolean>> xe6) {
        UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1 userRemoteDataSource$checkAuthenticationEmailExisting$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1) {
            userRemoteDataSource$checkAuthenticationEmailExisting$Anon1 = (UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1) xe6;
            int i2 = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "checkAuthenticationEmailExisting for " + str);
                    ku3 ku3 = new ku3();
                    try {
                        ku3.a("email", str);
                    } catch (Exception e) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        local2.d(str4, "Exception when generate jsonObject=" + e);
                    }
                    UserRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1 userRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1 = new UserRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$0 = this;
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$1 = str;
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$2 = ku3;
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1, userRemoteDataSource$checkAuthenticationEmailExisting$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$2;
                    String str5 = (String) userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "checkAuthenticationEmailExisting Success");
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() == null || !((ku3) cp4.a()).d("existing")) {
                        return new cp4(hf6.a(false), false, 2, (qg6) null);
                    }
                    JsonElement a2 = ((ku3) cp4.a()).a("existing");
                    wg6.a((Object) a2, "repoResponse.response.get(JSON_KEY_IS_EXISTING)");
                    return new cp4(hf6.a(a2.a()), false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str6 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("checkAuthenticationEmailExisting failed with error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local3.d(str6, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$checkAuthenticationEmailExisting$Anon1 = new UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.result;
        Object a3 = ff6.a();
        i = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object checkAuthenticationSocialExisting(String str, String str2, xe6<? super ap4<Boolean>> xe6) {
        UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1 userRemoteDataSource$checkAuthenticationSocialExisting$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1) {
            userRemoteDataSource$checkAuthenticationSocialExisting$Anon1 = (UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1) xe6;
            int i2 = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label;
                String str3 = null;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    try {
                        ku3.a(Constants.SERVICE, str);
                        ku3.a("token", str2);
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        local.d(str4, "Exception when generate jsonObject=" + e);
                    }
                    UserRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1 userRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1 = new UserRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$0 = this;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$1 = str;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$2 = str2;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$3 = ku3;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1, userRemoteDataSource$checkAuthenticationSocialExisting$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$3;
                    String str5 = (String) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$2;
                    String str6 = (String) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "checkAuthenticationSocialExisting Success");
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() == null || !((ku3) cp4.a()).d("existing")) {
                        return new cp4(hf6.a(false), false, 2, (qg6) null);
                    }
                    JsonElement a2 = ((ku3) cp4.a()).a("existing");
                    wg6.a((Object) a2, "repoResponse.response.get(JSON_KEY_IS_EXISTING)");
                    return new cp4(hf6.a(a2.a()), false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("checkAuthenticationSocialExisting failed with error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str3 = c.getMessage();
                    }
                    sb.append(str3);
                    local2.d(str7, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$checkAuthenticationSocialExisting$Anon1 = new UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.result;
        Object a3 = ff6.a();
        i = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label;
        String str32 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object deleteUser(MFUser mFUser, xe6<? super Integer> xe6) {
        UserRemoteDataSource$deleteUser$Anon1 userRemoteDataSource$deleteUser$Anon1;
        int i;
        ap4 ap4;
        int i2;
        if (xe6 instanceof UserRemoteDataSource$deleteUser$Anon1) {
            userRemoteDataSource$deleteUser$Anon1 = (UserRemoteDataSource$deleteUser$Anon1) xe6;
            int i3 = userRemoteDataSource$deleteUser$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$deleteUser$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$deleteUser$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$deleteUser$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "deleteUser");
                    UserRemoteDataSource$deleteUser$response$Anon1 userRemoteDataSource$deleteUser$response$Anon1 = new UserRemoteDataSource$deleteUser$response$Anon1(this, (xe6) null);
                    userRemoteDataSource$deleteUser$Anon1.L$0 = this;
                    userRemoteDataSource$deleteUser$Anon1.L$1 = mFUser;
                    userRemoteDataSource$deleteUser$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$deleteUser$response$Anon1, userRemoteDataSource$deleteUser$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    MFUser mFUser2 = (MFUser) userRemoteDataSource$deleteUser$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$deleteUser$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    i2 = MFNetworkReturnCode.RESPONSE_OK;
                } else if (ap4 instanceof zo4) {
                    i2 = ((zo4) ap4).a();
                } else {
                    throw new kc6();
                }
                return hf6.a(i2);
            }
        }
        userRemoteDataSource$deleteUser$Anon1 = new UserRemoteDataSource$deleteUser$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$deleteUser$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$deleteUser$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return hf6.a(i2);
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        wg6.b(mFUser, "user");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object loadUserInfo(MFUser mFUser, xe6<? super ap4<MFUser>> xe6) {
        UserRemoteDataSource$loadUserInfo$Anon1 userRemoteDataSource$loadUserInfo$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$loadUserInfo$Anon1) {
            userRemoteDataSource$loadUserInfo$Anon1 = (UserRemoteDataSource$loadUserInfo$Anon1) xe6;
            int i2 = userRemoteDataSource$loadUserInfo$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$loadUserInfo$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$loadUserInfo$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$loadUserInfo$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "loadUserInfo");
                    UserRemoteDataSource$loadUserInfo$response$Anon1 userRemoteDataSource$loadUserInfo$response$Anon1 = new UserRemoteDataSource$loadUserInfo$response$Anon1(this, (xe6) null);
                    userRemoteDataSource$loadUserInfo$Anon1.L$0 = this;
                    userRemoteDataSource$loadUserInfo$Anon1.L$1 = mFUser;
                    userRemoteDataSource$loadUserInfo$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$loadUserInfo$response$Anon1, userRemoteDataSource$loadUserInfo$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    mFUser = (MFUser) userRemoteDataSource$loadUserInfo$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$loadUserInfo$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "loadUserInfo Success");
                    User user = (User) ((cp4) ap4).a();
                    if (user != null) {
                        MFUser mFUser2 = user.toMFUser(mFUser);
                        String updatedAt = mFUser.getUpdatedAt();
                        if (updatedAt != null) {
                            String updatedAt2 = mFUser2.getUpdatedAt();
                            if (updatedAt2 != null) {
                                Date d = bk4.d(updatedAt2);
                                wg6.a((Object) d, "DateHelper.parseJodaTime(it)");
                                long time = d.getTime();
                                Date d2 = bk4.d(updatedAt);
                                wg6.a((Object) d2, "DateHelper.parseJodaTime(localUpdateAt)");
                                if (time > d2.getTime() + 60000) {
                                    return new cp4(mFUser2, false, 2, (qg6) null);
                                }
                                return new cp4(mFUser, false, 2, (qg6) null);
                            }
                            FLogger.INSTANCE.getLocal().e(TAG, "remoteUpdateAt is null");
                            return new zo4(600, (ServerError) null, (Throwable) null, (String) null, 8, (qg6) null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local.e(str2, "localCreateAt is null - remoteUpdateAt: " + mFUser2.getUpdatedAt());
                        if (mFUser2.getUpdatedAt() != null) {
                            return new cp4(mFUser2, false, 2, (qg6) null);
                        }
                        return new zo4(600, (ServerError) null, (Throwable) null, (String) null, 8, (qg6) null);
                    }
                    FLogger.INSTANCE.getLocal().e(TAG, "server return null-response");
                    return new zo4(600, (ServerError) null, (Throwable) null, (String) null, 8, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadUserInfo Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local2.d(str3, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$loadUserInfo$Anon1 = new UserRemoteDataSource$loadUserInfo$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$loadUserInfo$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$loadUserInfo$Anon1.label;
        String str4 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object loginEmail(String str, String str2, xe6<? super ap4<Auth>> xe6) {
        UserRemoteDataSource$loginEmail$Anon1 userRemoteDataSource$loginEmail$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$loginEmail$Anon1) {
            userRemoteDataSource$loginEmail$Anon1 = (UserRemoteDataSource$loginEmail$Anon1) xe6;
            int i2 = userRemoteDataSource$loginEmail$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$loginEmail$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$loginEmail$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$loginEmail$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "loginEmail");
                    ku3 ku3 = new ku3();
                    ku3.a("email", str);
                    ku3.a("password", str2);
                    ku3.a("clientId", tj4.f.a(""));
                    UserRemoteDataSource$loginEmail$response$Anon1 userRemoteDataSource$loginEmail$response$Anon1 = new UserRemoteDataSource$loginEmail$response$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$loginEmail$Anon1.L$0 = this;
                    userRemoteDataSource$loginEmail$Anon1.L$1 = str;
                    userRemoteDataSource$loginEmail$Anon1.L$2 = str2;
                    userRemoteDataSource$loginEmail$Anon1.L$3 = ku3;
                    userRemoteDataSource$loginEmail$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$loginEmail$response$Anon1, userRemoteDataSource$loginEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$loginEmail$Anon1.L$3;
                    String str3 = (String) userRemoteDataSource$loginEmail$Anon1.L$2;
                    String str4 = (String) userRemoteDataSource$loginEmail$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$loginEmail$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new cp4(((cp4) ap4).a(), false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        userRemoteDataSource$loginEmail$Anon1 = new UserRemoteDataSource$loginEmail$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$loginEmail$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$loginEmail$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object loginWithSocial(String str, String str2, String str3, xe6<? super ap4<Auth>> xe6) {
        UserRemoteDataSource$loginWithSocial$Anon1 userRemoteDataSource$loginWithSocial$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$loginWithSocial$Anon1) {
            userRemoteDataSource$loginWithSocial$Anon1 = (UserRemoteDataSource$loginWithSocial$Anon1) xe6;
            int i2 = userRemoteDataSource$loginWithSocial$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$loginWithSocial$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$loginWithSocial$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$loginWithSocial$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "loginWithSocial service " + str + " token " + str2 + " clientId " + str3);
                    ku3 ku3 = new ku3();
                    ku3.a(Constants.SERVICE, str);
                    ku3.a("token", str2);
                    ku3.a("clientId", tj4.f.a(""));
                    UserRemoteDataSource$loginWithSocial$response$Anon1 userRemoteDataSource$loginWithSocial$response$Anon1 = new UserRemoteDataSource$loginWithSocial$response$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$loginWithSocial$Anon1.L$0 = this;
                    userRemoteDataSource$loginWithSocial$Anon1.L$1 = str;
                    userRemoteDataSource$loginWithSocial$Anon1.L$2 = str2;
                    userRemoteDataSource$loginWithSocial$Anon1.L$3 = str3;
                    userRemoteDataSource$loginWithSocial$Anon1.L$4 = ku3;
                    userRemoteDataSource$loginWithSocial$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$loginWithSocial$response$Anon1, userRemoteDataSource$loginWithSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$loginWithSocial$Anon1.L$4;
                    String str5 = (String) userRemoteDataSource$loginWithSocial$Anon1.L$3;
                    String str6 = (String) userRemoteDataSource$loginWithSocial$Anon1.L$2;
                    String str7 = (String) userRemoteDataSource$loginWithSocial$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$loginWithSocial$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new cp4(((cp4) ap4).a(), false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        userRemoteDataSource$loginWithSocial$Anon1 = new UserRemoteDataSource$loginWithSocial$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$loginWithSocial$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$loginWithSocial$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object logoutUser(xe6<? super Integer> xe6) {
        UserRemoteDataSource$logoutUser$Anon1 userRemoteDataSource$logoutUser$Anon1;
        int i;
        ap4 ap4;
        int i2;
        if (xe6 instanceof UserRemoteDataSource$logoutUser$Anon1) {
            userRemoteDataSource$logoutUser$Anon1 = (UserRemoteDataSource$logoutUser$Anon1) xe6;
            int i3 = userRemoteDataSource$logoutUser$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$logoutUser$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$logoutUser$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$logoutUser$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "logoutUser");
                    UserRemoteDataSource$logoutUser$response$Anon1 userRemoteDataSource$logoutUser$response$Anon1 = new UserRemoteDataSource$logoutUser$response$Anon1(this, (xe6) null);
                    userRemoteDataSource$logoutUser$Anon1.L$0 = this;
                    userRemoteDataSource$logoutUser$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$logoutUser$response$Anon1, userRemoteDataSource$logoutUser$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$logoutUser$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    i2 = MFNetworkReturnCode.RESPONSE_OK;
                } else if (ap4 instanceof zo4) {
                    i2 = ((zo4) ap4).a();
                } else {
                    throw new kc6();
                }
                return hf6.a(i2);
            }
        }
        userRemoteDataSource$logoutUser$Anon1 = new UserRemoteDataSource$logoutUser$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$logoutUser$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$logoutUser$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return hf6.a(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object requestEmailOtp(String str, xe6<? super ap4<Void>> xe6) {
        UserRemoteDataSource$requestEmailOtp$Anon1 userRemoteDataSource$requestEmailOtp$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$requestEmailOtp$Anon1) {
            userRemoteDataSource$requestEmailOtp$Anon1 = (UserRemoteDataSource$requestEmailOtp$Anon1) xe6;
            int i2 = userRemoteDataSource$requestEmailOtp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$requestEmailOtp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$requestEmailOtp$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$requestEmailOtp$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "requestEmailOtp email: " + str);
                    ku3 ku3 = new ku3();
                    ku3.a("email", str);
                    UserRemoteDataSource$requestEmailOtp$repoResponse$Anon1 userRemoteDataSource$requestEmailOtp$repoResponse$Anon1 = new UserRemoteDataSource$requestEmailOtp$repoResponse$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$requestEmailOtp$Anon1.L$0 = this;
                    userRemoteDataSource$requestEmailOtp$Anon1.L$1 = str;
                    userRemoteDataSource$requestEmailOtp$Anon1.L$2 = ku3;
                    userRemoteDataSource$requestEmailOtp$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$requestEmailOtp$repoResponse$Anon1, userRemoteDataSource$requestEmailOtp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$requestEmailOtp$Anon1.L$2;
                    String str4 = (String) userRemoteDataSource$requestEmailOtp$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$requestEmailOtp$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "requestEmailOtp Success");
                    return new cp4((Object) null, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("requestEmailOtp failed with error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local2.d(str5, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$requestEmailOtp$Anon1 = new UserRemoteDataSource$requestEmailOtp$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$requestEmailOtp$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$requestEmailOtp$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object resetPassword(String str, xe6<? super ap4<Integer>> xe6) {
        UserRemoteDataSource$resetPassword$Anon1 userRemoteDataSource$resetPassword$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$resetPassword$Anon1) {
            userRemoteDataSource$resetPassword$Anon1 = (UserRemoteDataSource$resetPassword$Anon1) xe6;
            int i2 = userRemoteDataSource$resetPassword$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$resetPassword$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$resetPassword$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$resetPassword$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "resetPassword");
                    ku3 ku3 = new ku3();
                    ku3.a("email", str);
                    UserRemoteDataSource$resetPassword$repoResponse$Anon1 userRemoteDataSource$resetPassword$repoResponse$Anon1 = new UserRemoteDataSource$resetPassword$repoResponse$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$resetPassword$Anon1.L$0 = this;
                    userRemoteDataSource$resetPassword$Anon1.L$1 = str;
                    userRemoteDataSource$resetPassword$Anon1.L$2 = ku3;
                    userRemoteDataSource$resetPassword$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$resetPassword$repoResponse$Anon1, userRemoteDataSource$resetPassword$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$resetPassword$Anon1.L$2;
                    String str3 = (String) userRemoteDataSource$resetPassword$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$resetPassword$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "resetPassword Success");
                    return new cp4(hf6.a((int) MFNetworkReturnCode.RESPONSE_OK), false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("resetPassword failed with error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local.d(str4, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$resetPassword$Anon1 = new UserRemoteDataSource$resetPassword$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$resetPassword$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$resetPassword$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, xe6<? super ap4<Auth>> xe6) {
        UserRemoteDataSource$signUpEmail$Anon1 userRemoteDataSource$signUpEmail$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$signUpEmail$Anon1) {
            userRemoteDataSource$signUpEmail$Anon1 = (UserRemoteDataSource$signUpEmail$Anon1) xe6;
            int i2 = userRemoteDataSource$signUpEmail$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$signUpEmail$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$signUpEmail$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$signUpEmail$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "signUpEmail auth " + signUpEmailAuth);
                    UserRemoteDataSource$signUpEmail$response$Anon1 userRemoteDataSource$signUpEmail$response$Anon1 = new UserRemoteDataSource$signUpEmail$response$Anon1(this, signUpEmailAuth, (xe6) null);
                    userRemoteDataSource$signUpEmail$Anon1.L$0 = this;
                    userRemoteDataSource$signUpEmail$Anon1.L$1 = signUpEmailAuth;
                    userRemoteDataSource$signUpEmail$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$signUpEmail$response$Anon1, userRemoteDataSource$signUpEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SignUpEmailAuth signUpEmailAuth2 = (SignUpEmailAuth) userRemoteDataSource$signUpEmail$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$signUpEmail$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new cp4(((cp4) ap4).a(), false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        userRemoteDataSource$signUpEmail$Anon1 = new UserRemoteDataSource$signUpEmail$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$signUpEmail$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$signUpEmail$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, xe6<? super ap4<Auth>> xe6) {
        UserRemoteDataSource$signUpSocial$Anon1 userRemoteDataSource$signUpSocial$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$signUpSocial$Anon1) {
            userRemoteDataSource$signUpSocial$Anon1 = (UserRemoteDataSource$signUpSocial$Anon1) xe6;
            int i2 = userRemoteDataSource$signUpSocial$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$signUpSocial$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$signUpSocial$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$signUpSocial$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "signUpSocial auth " + signUpSocialAuth);
                    UserRemoteDataSource$signUpSocial$response$Anon1 userRemoteDataSource$signUpSocial$response$Anon1 = new UserRemoteDataSource$signUpSocial$response$Anon1(this, signUpSocialAuth, (xe6) null);
                    userRemoteDataSource$signUpSocial$Anon1.L$0 = this;
                    userRemoteDataSource$signUpSocial$Anon1.L$1 = signUpSocialAuth;
                    userRemoteDataSource$signUpSocial$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$signUpSocial$response$Anon1, userRemoteDataSource$signUpSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SignUpSocialAuth signUpSocialAuth2 = (SignUpSocialAuth) userRemoteDataSource$signUpSocial$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$signUpSocial$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new cp4(((cp4) ap4).a(), false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        userRemoteDataSource$signUpSocial$Anon1 = new UserRemoteDataSource$signUpSocial$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$signUpSocial$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$signUpSocial$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x028c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    public Object updateUser(MFUser mFUser, boolean z, xe6<? super ap4<MFUser>> xe6) {
        UserRemoteDataSource$updateUser$Anon1 userRemoteDataSource$updateUser$Anon1;
        int i;
        MFUser mFUser2;
        ap4 ap4;
        xe6<? super ap4<MFUser>> xe62 = xe6;
        if (xe62 instanceof UserRemoteDataSource$updateUser$Anon1) {
            userRemoteDataSource$updateUser$Anon1 = (UserRemoteDataSource$updateUser$Anon1) xe62;
            int i2 = userRemoteDataSource$updateUser$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$updateUser$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$updateUser$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$updateUser$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "updateUser");
                    ku3 ku3 = new ku3();
                    try {
                        String firstName = mFUser.getFirstName();
                        wg6.a((Object) firstName, "user.firstName");
                        Charset charset = ej6.a;
                        if (firstName != null) {
                            byte[] bytes = firstName.getBytes(charset);
                            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                            Charset charset2 = StandardCharsets.UTF_8;
                            wg6.a((Object) charset2, "StandardCharsets.UTF_8");
                            ku3.a("firstName", new String(bytes, charset2));
                            String lastName = mFUser.getLastName();
                            wg6.a((Object) lastName, "user.lastName");
                            Charset charset3 = ej6.a;
                            if (lastName != null) {
                                byte[] bytes2 = lastName.getBytes(charset3);
                                wg6.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                                Charset charset4 = StandardCharsets.UTF_8;
                                wg6.a((Object) charset4, "StandardCharsets.UTF_8");
                                ku3.a("lastName", new String(bytes2, charset4));
                                if (mFUser.getHeightInCentimeters() != 0) {
                                    ku3.a("heightInCentimeters", hf6.a(mFUser.getHeightInCentimeters()));
                                }
                                if (mFUser.getWeightInGrams() != 0) {
                                    ku3.a("weightInGrams", hf6.a(mFUser.getWeightInGrams()));
                                }
                                rh4 gender = mFUser.getGender();
                                ku3.a("gender", gender != null ? gender.toString() : null);
                                ku3 ku32 = new ku3();
                                zh4 heightUnit = mFUser.getHeightUnit();
                                wg6.a((Object) heightUnit, "user.heightUnit");
                                ku32.a(Constants.PROFILE_KEY_UNITS_HEIGHT, heightUnit.getValue());
                                zh4 weightUnit = mFUser.getWeightUnit();
                                wg6.a((Object) weightUnit, "user.weightUnit");
                                ku32.a(Constants.PROFILE_KEY_UNITS_WEIGHT, weightUnit.getValue());
                                zh4 distanceUnit = mFUser.getDistanceUnit();
                                wg6.a((Object) distanceUnit, "user.distanceUnit");
                                ku32.a("distance", distanceUnit.getValue());
                                zh4 temperatureUnit = mFUser.getTemperatureUnit();
                                wg6.a((Object) temperatureUnit, "user.temperatureUnit");
                                ku32.a("temperature", temperatureUnit.getValue());
                                ku3.a(Constants.PROFILE_KEY_UNIT_GROUP, ku32);
                                ku3.a("heightInCentimeters", hf6.a(mFUser.getHeightInCentimeters()));
                                ku3.a("weightInGrams", hf6.a(mFUser.getWeightInGrams()));
                                ku3.a("activeDeviceId", mFUser.getActiveDeviceId());
                                ku3.a("emailOptIn", hf6.a(mFUser.isEmailOptIn()));
                                ku3.a("diagnosticEnabled", hf6.a(mFUser.isDiagnosticEnabled()));
                                ku3.a("isOnboardingComplete", hf6.a(mFUser.isOnboardingComplete()));
                                ku3.a(MFUser.USE_DEFAULT_BIOMETRIC, hf6.a(mFUser.isUseDefaultBiometric()));
                                ku3.a(MFUser.USE_DEFAULT_GOALS, hf6.a(mFUser.isUseDefaultGoals()));
                                if (!TextUtils.isEmpty(mFUser.getBirthday())) {
                                    ku3.a("birthday", mFUser.getBirthday());
                                }
                                fu3 fu3 = new fu3();
                                for (String a2 : mFUser.getIntegrations()) {
                                    fu3.a(a2);
                                }
                                ku3.a("integrations", fu3);
                                ku3 ku33 = new ku3();
                                String home = mFUser.getHome();
                                String str2 = "";
                                if (home == null) {
                                    home = str2;
                                }
                                ku33.a("home", home);
                                String work = mFUser.getWork();
                                if (work != null) {
                                    str2 = work;
                                }
                                ku33.a("work", str2);
                                ku3.a("addresses", ku33);
                                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                                    String profilePicture = mFUser.getProfilePicture();
                                    wg6.a((Object) profilePicture, "user.profilePicture");
                                    if (!yj6.a((CharSequence) profilePicture, (CharSequence) "https://", false, 2, (Object) null)) {
                                        String profilePicture2 = mFUser.getProfilePicture();
                                        wg6.a((Object) profilePicture2, "user.profilePicture");
                                        if (!yj6.a((CharSequence) profilePicture2, (CharSequence) "http://", false, 2, (Object) null)) {
                                            ku3.a("profilePicture", mFUser.getProfilePicture());
                                        }
                                    }
                                }
                                UserRemoteDataSource$updateUser$response$Anon1 userRemoteDataSource$updateUser$response$Anon1 = new UserRemoteDataSource$updateUser$response$Anon1(this, ku3, (xe6) null);
                                userRemoteDataSource$updateUser$Anon1.L$0 = this;
                                mFUser2 = mFUser;
                                userRemoteDataSource$updateUser$Anon1.L$1 = mFUser2;
                                userRemoteDataSource$updateUser$Anon1.Z$0 = z;
                                userRemoteDataSource$updateUser$Anon1.L$2 = ku3;
                                userRemoteDataSource$updateUser$Anon1.label = 1;
                                obj = ResponseKt.a(userRemoteDataSource$updateUser$response$Anon1, userRemoteDataSource$updateUser$Anon1);
                                if (obj == a) {
                                    return a;
                                }
                            } else {
                                throw new rc6("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local.d(str3, "Exception when generating user json object " + e);
                    }
                } else if (i == 1) {
                    ku3 ku34 = (ku3) userRemoteDataSource$updateUser$Anon1.L$2;
                    boolean z2 = userRemoteDataSource$updateUser$Anon1.Z$0;
                    mFUser2 = (MFUser) userRemoteDataSource$updateUser$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$updateUser$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "updateUser Success");
                    User user = (User) ((cp4) ap4).a();
                    return new cp4(user != null ? user.toMFUser(mFUser2) : null, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("updateUser Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local2.d(str4, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$updateUser$Anon1 = new UserRemoteDataSource$updateUser$Anon1(this, xe62);
        Object obj2 = userRemoteDataSource$updateUser$Anon1.result;
        Object a3 = ff6.a();
        i = userRemoteDataSource$updateUser$Anon1.label;
        String str5 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object verifyEmailOtp(String str, String str2, xe6<? super ap4<Void>> xe6) {
        UserRemoteDataSource$verifyEmailOtp$Anon1 userRemoteDataSource$verifyEmailOtp$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof UserRemoteDataSource$verifyEmailOtp$Anon1) {
            userRemoteDataSource$verifyEmailOtp$Anon1 = (UserRemoteDataSource$verifyEmailOtp$Anon1) xe6;
            int i2 = userRemoteDataSource$verifyEmailOtp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$verifyEmailOtp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$verifyEmailOtp$Anon1.result;
                Object a = ff6.a();
                i = userRemoteDataSource$verifyEmailOtp$Anon1.label;
                String str3 = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "verifyEmailOtp email: " + str);
                    ku3 ku3 = new ku3();
                    ku3.a("email", str);
                    ku3.a("otp", str2);
                    UserRemoteDataSource$verifyEmailOtp$repoResponse$Anon1 userRemoteDataSource$verifyEmailOtp$repoResponse$Anon1 = new UserRemoteDataSource$verifyEmailOtp$repoResponse$Anon1(this, ku3, (xe6) null);
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$0 = this;
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$1 = str;
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$2 = str2;
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$3 = ku3;
                    userRemoteDataSource$verifyEmailOtp$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$verifyEmailOtp$repoResponse$Anon1, userRemoteDataSource$verifyEmailOtp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) userRemoteDataSource$verifyEmailOtp$Anon1.L$3;
                    String str5 = (String) userRemoteDataSource$verifyEmailOtp$Anon1.L$2;
                    String str6 = (String) userRemoteDataSource$verifyEmailOtp$Anon1.L$1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$verifyEmailOtp$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "verifyEmailOtp Success");
                    return new cp4((Object) null, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("verifyEmailOtp failed with error=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str3 = c.getMessage();
                    }
                    sb.append(str3);
                    local2.d(str7, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        userRemoteDataSource$verifyEmailOtp$Anon1 = new UserRemoteDataSource$verifyEmailOtp$Anon1(this, xe6);
        Object obj2 = userRemoteDataSource$verifyEmailOtp$Anon1.result;
        Object a2 = ff6.a();
        i = userRemoteDataSource$verifyEmailOtp$Anon1.label;
        String str32 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
