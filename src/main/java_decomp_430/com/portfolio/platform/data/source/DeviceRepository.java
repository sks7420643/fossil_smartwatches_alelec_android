package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ DeviceDao mDeviceDao;
    @DexIgnore
    public /* final */ DeviceRemoteDataSource mDeviceRemoteDataSource;
    @DexIgnore
    public /* final */ SkuDao mSkuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return DeviceRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "DeviceRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRepository(DeviceDao deviceDao, SkuDao skuDao, DeviceRemoteDataSource deviceRemoteDataSource) {
        wg6.b(deviceDao, "mDeviceDao");
        wg6.b(skuDao, "mSkuDao");
        wg6.b(deviceRemoteDataSource, "mDeviceRemoteDataSource");
        this.mDeviceDao = deviceDao;
        this.mSkuDao = skuDao;
        this.mDeviceRemoteDataSource = deviceRemoteDataSource;
    }

    @DexIgnore
    public static /* synthetic */ Object downloadSupportedSku$default(DeviceRepository deviceRepository, int i, xe6 xe6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return deviceRepository.downloadSupportedSku(i, xe6);
    }

    @DexIgnore
    private final boolean isHasDevice(List<Device> list, String str) {
        if (list == null) {
            return false;
        }
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        for (Device deviceId : list) {
            if (xj6.b(deviceId.getDeviceId(), str, true)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private final void removeStealDevice(List<Device> list) {
        for (Device component1 : this.mDeviceDao.getAllDevice()) {
            String component12 = component1.component1();
            if (!isHasDevice(list, component12)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "Inside .removeStealDevice device=" + component12 + " was stealed, remove it on local");
                this.mDeviceDao.removeDeviceByDeviceId(component12);
                if (!TextUtils.isEmpty(component12) && xj6.b(component12, PortfolioApp.get.instance().e(), true)) {
                    PortfolioApp.get.instance().I();
                }
            }
        }
    }

    @DexIgnore
    public final void cleanUp() {
        this.mDeviceDao.cleanUp();
        this.mSkuDao.cleanUpSku();
        this.mSkuDao.cleanUpWatchParam();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadDeviceList(xe6<? super ap4<ApiResponse<Device>>> xe6) {
        DeviceRepository$downloadDeviceList$Anon1 deviceRepository$downloadDeviceList$Anon1;
        int i;
        DeviceRepository deviceRepository;
        ap4 ap4;
        if (xe6 instanceof DeviceRepository$downloadDeviceList$Anon1) {
            deviceRepository$downloadDeviceList$Anon1 = (DeviceRepository$downloadDeviceList$Anon1) xe6;
            int i2 = deviceRepository$downloadDeviceList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRepository$downloadDeviceList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRepository$downloadDeviceList$Anon1.result;
                Object a = ff6.a();
                i = deviceRepository$downloadDeviceList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    DeviceRemoteDataSource deviceRemoteDataSource = this.mDeviceRemoteDataSource;
                    deviceRepository$downloadDeviceList$Anon1.L$0 = this;
                    deviceRepository$downloadDeviceList$Anon1.label = 1;
                    obj = deviceRemoteDataSource.getAllDevice(deviceRepository$downloadDeviceList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    deviceRepository = this;
                } else if (i == 1) {
                    deviceRepository = (DeviceRepository) deviceRepository$downloadDeviceList$Anon1.L$0;
                    nc6.a(obj);
                } else if (i == 2) {
                    DeviceRepository deviceRepository2 = (DeviceRepository) deviceRepository$downloadDeviceList$Anon1.L$0;
                    nc6.a(obj);
                    return (ap4) deviceRepository$downloadDeviceList$Anon1.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4) || ((cp4) ap4).b()) {
                    return ap4;
                }
                dl6 a2 = zl6.a();
                DeviceRepository$downloadDeviceList$Anon2 deviceRepository$downloadDeviceList$Anon2 = new DeviceRepository$downloadDeviceList$Anon2(deviceRepository, ap4, (xe6) null);
                deviceRepository$downloadDeviceList$Anon1.L$0 = deviceRepository;
                deviceRepository$downloadDeviceList$Anon1.L$1 = ap4;
                deviceRepository$downloadDeviceList$Anon1.label = 2;
                return gk6.a(a2, deviceRepository$downloadDeviceList$Anon2, deviceRepository$downloadDeviceList$Anon1) == a ? a : ap4;
            }
        }
        deviceRepository$downloadDeviceList$Anon1 = new DeviceRepository$downloadDeviceList$Anon1(this, xe6);
        Object obj2 = deviceRepository$downloadDeviceList$Anon1.result;
        Object a3 = ff6.a();
        i = deviceRepository$downloadDeviceList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4) || ((cp4) ap4).b()) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadSupportedSku(int i, xe6<? super cd6> xe6) {
        DeviceRepository$downloadSupportedSku$Anon1 deviceRepository$downloadSupportedSku$Anon1;
        int i2;
        DeviceRepository deviceRepository;
        ap4 ap4;
        if (xe6 instanceof DeviceRepository$downloadSupportedSku$Anon1) {
            deviceRepository$downloadSupportedSku$Anon1 = (DeviceRepository$downloadSupportedSku$Anon1) xe6;
            int i3 = deviceRepository$downloadSupportedSku$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRepository$downloadSupportedSku$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRepository$downloadSupportedSku$Anon1.result;
                Object a = ff6.a();
                i2 = deviceRepository$downloadSupportedSku$Anon1.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    DeviceRemoteDataSource deviceRemoteDataSource = this.mDeviceRemoteDataSource;
                    deviceRepository$downloadSupportedSku$Anon1.L$0 = this;
                    deviceRepository$downloadSupportedSku$Anon1.I$0 = i;
                    deviceRepository$downloadSupportedSku$Anon1.label = 1;
                    obj = deviceRemoteDataSource.getSupportedSku(i, deviceRepository$downloadSupportedSku$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    deviceRepository = this;
                } else if (i2 == 1) {
                    i = deviceRepository$downloadSupportedSku$Anon1.I$0;
                    deviceRepository = (DeviceRepository) deviceRepository$downloadSupportedSku$Anon1.L$0;
                    nc6.a(obj);
                } else if (i2 == 2) {
                    ap4 ap42 = (ap4) deviceRepository$downloadSupportedSku$Anon1.L$1;
                    int i4 = deviceRepository$downloadSupportedSku$Anon1.I$0;
                    DeviceRepository deviceRepository2 = (DeviceRepository) deviceRepository$downloadSupportedSku$Anon1.L$0;
                    nc6.a(obj);
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "downloadSupportedSku() - offset = " + i);
                if (ap4 instanceof cp4) {
                    cp4 cp4 = (cp4) ap4;
                    if (!cp4.b()) {
                        SkuDao skuDao = deviceRepository.mSkuDao;
                        ApiResponse apiResponse = (ApiResponse) cp4.a();
                        List list = apiResponse != null ? apiResponse.get_items() : null;
                        if (list != null) {
                            skuDao.addOrUpdateSkuList(list);
                            Range range = ((ApiResponse) cp4.a()).get_range();
                            if (range != null && range.isHasNext()) {
                                dl6 b = zl6.b();
                                DeviceRepository$downloadSupportedSku$Anon2 deviceRepository$downloadSupportedSku$Anon2 = new DeviceRepository$downloadSupportedSku$Anon2(deviceRepository, i, (xe6) null);
                                deviceRepository$downloadSupportedSku$Anon1.L$0 = deviceRepository;
                                deviceRepository$downloadSupportedSku$Anon1.I$0 = i;
                                deviceRepository$downloadSupportedSku$Anon1.L$1 = ap4;
                                deviceRepository$downloadSupportedSku$Anon1.label = 2;
                                if (gk6.a(b, deviceRepository$downloadSupportedSku$Anon2, deviceRepository$downloadSupportedSku$Anon1) == a) {
                                    return a;
                                }
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                }
                return cd6.a;
            }
        }
        deviceRepository$downloadSupportedSku$Anon1 = new DeviceRepository$downloadSupportedSku$Anon1(this, xe6);
        Object obj2 = deviceRepository$downloadSupportedSku$Anon1.result;
        Object a2 = ff6.a();
        i2 = deviceRepository$downloadSupportedSku$Anon1.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "downloadSupportedSku() - offset = " + i);
        if (ap4 instanceof cp4) {
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object forceLinkDevice(Device device, xe6<? super ap4<Void>> xe6) {
        DeviceRepository$forceLinkDevice$Anon1 deviceRepository$forceLinkDevice$Anon1;
        int i;
        DeviceRepository deviceRepository;
        ap4 ap4;
        if (xe6 instanceof DeviceRepository$forceLinkDevice$Anon1) {
            deviceRepository$forceLinkDevice$Anon1 = (DeviceRepository$forceLinkDevice$Anon1) xe6;
            int i2 = deviceRepository$forceLinkDevice$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRepository$forceLinkDevice$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRepository$forceLinkDevice$Anon1.result;
                Object a = ff6.a();
                i = deviceRepository$forceLinkDevice$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "Inside .forceLinkDevice device=" + device);
                    device.setActive(true);
                    if (TextUtils.isEmpty(device.getDeviceId())) {
                        FLogger.INSTANCE.getLocal().d(TAG, "Inside .forceLinkDevice can't link device without serial number");
                        return new zo4(MFNetworkReturnCode.CLIENT_TIMEOUT, new ServerError(), (Throwable) null, (String) null, 8, (qg6) null);
                    }
                    DeviceRemoteDataSource deviceRemoteDataSource = this.mDeviceRemoteDataSource;
                    deviceRepository$forceLinkDevice$Anon1.L$0 = this;
                    deviceRepository$forceLinkDevice$Anon1.L$1 = device;
                    deviceRepository$forceLinkDevice$Anon1.label = 1;
                    obj = deviceRemoteDataSource.forceLinkDevice(device, deviceRepository$forceLinkDevice$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    deviceRepository = this;
                } else if (i == 1) {
                    device = (Device) deviceRepository$forceLinkDevice$Anon1.L$1;
                    deviceRepository = (DeviceRepository) deviceRepository$forceLinkDevice$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.d(str2, "Inside .forceLinkDevice device=" + device.getDeviceId() + "on server success");
                    deviceRepository.mDeviceDao.addOrUpdateDevice(device);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "Inside .forceLinkDevice device=" + device.getDeviceId() + "on server fail");
                }
                return ap4;
            }
        }
        deviceRepository$forceLinkDevice$Anon1 = new DeviceRepository$forceLinkDevice$Anon1(this, xe6);
        Object obj2 = deviceRepository$forceLinkDevice$Anon1.result;
        Object a2 = ff6.a();
        i = deviceRepository$forceLinkDevice$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    public final Object generatePairingKey(String str, xe6<? super ap4<String>> xe6) {
        return this.mDeviceRemoteDataSource.generatePairingKey(str, xe6);
    }

    @DexIgnore
    public final List<Device> getAllDevice() {
        return this.mDeviceDao.getAllDevice();
    }

    @DexIgnore
    public final LiveData<List<Device>> getAllDeviceAsLiveData() {
        return this.mDeviceDao.getAllDeviceAsLiveData();
    }

    @DexIgnore
    public final Device getDeviceBySerial(String str) {
        wg6.b(str, "serial");
        return this.mDeviceDao.getDeviceByDeviceId(str);
    }

    @DexIgnore
    public final LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        wg6.b(str, "serial");
        return this.mDeviceDao.getDeviceBySerialAsLiveData(str);
    }

    @DexIgnore
    public final String getDeviceNameBySerial(String str) {
        wg6.b(str, "serial");
        SKUModel skuByDeviceIdPrefix = this.mSkuDao.getSkuByDeviceIdPrefix(DeviceHelper.o.b(str));
        if (skuByDeviceIdPrefix != null && !TextUtils.isEmpty(skuByDeviceIdPrefix.getDeviceName())) {
            String deviceName = skuByDeviceIdPrefix.getDeviceName();
            if (deviceName != null) {
                return deviceName;
            }
            wg6.a();
            throw null;
        } else if (TextUtils.isEmpty(str) || str.length() < 6) {
            return "UNKNOWN";
        } else {
            return DeviceIdentityUtils.isDianaDevice(str) ? "Hybrid HR" : "Hybrid Smartwatch";
        }
    }

    @DexIgnore
    public final Object getDeviceSecretKey(String str, xe6<? super ap4<String>> xe6) {
        return this.mDeviceRemoteDataSource.getDeviceSecretKey(str, xe6);
    }

    @DexIgnore
    public final Object getLatestWatchParamFromServer(String str, int i, xe6<? super WatchParameterResponse> xe6) {
        return this.mDeviceRemoteDataSource.getLatestWatchParamFromServer(str, i, xe6);
    }

    @DexIgnore
    public final SKUModel getSkuModelBySerialPrefix(String str) {
        wg6.b(str, "prefix");
        return this.mSkuDao.getSkuByDeviceIdPrefix(str);
    }

    @DexIgnore
    public final List<SKUModel> getSupportedSku() {
        return this.mSkuDao.getAllSkus();
    }

    @DexIgnore
    public final Object getWatchParamBySerialId(String str, xe6<? super WatchParam> xe6) {
        return this.mSkuDao.getWatchParamById(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object removeDevice(Device device, xe6<? super ap4<Void>> xe6) {
        DeviceRepository$removeDevice$Anon1 deviceRepository$removeDevice$Anon1;
        int i;
        DeviceRepository deviceRepository;
        if (xe6 instanceof DeviceRepository$removeDevice$Anon1) {
            deviceRepository$removeDevice$Anon1 = (DeviceRepository$removeDevice$Anon1) xe6;
            int i2 = deviceRepository$removeDevice$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRepository$removeDevice$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRepository$removeDevice$Anon1.result;
                Object a = ff6.a();
                i = deviceRepository$removeDevice$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "Inside .removeDevice deviceId=" + device.getDeviceId());
                    DeviceRemoteDataSource deviceRemoteDataSource = this.mDeviceRemoteDataSource;
                    deviceRepository$removeDevice$Anon1.L$0 = this;
                    deviceRepository$removeDevice$Anon1.L$1 = device;
                    deviceRepository$removeDevice$Anon1.label = 1;
                    obj = deviceRemoteDataSource.removeDevice(device, deviceRepository$removeDevice$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    deviceRepository = this;
                } else if (i == 1) {
                    device = (Device) deviceRepository$removeDevice$Anon1.L$1;
                    deviceRepository = (DeviceRepository) deviceRepository$removeDevice$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 ap4 = (ap4) obj;
                if ((ap4 instanceof cp4) || ((ap4 instanceof zo4) && ((zo4) ap4).a() == 404)) {
                    deviceRepository.mDeviceDao.removeDeviceByDeviceId(device.getDeviceId());
                }
                return ap4;
            }
        }
        deviceRepository$removeDevice$Anon1 = new DeviceRepository$removeDevice$Anon1(this, xe6);
        Object obj2 = deviceRepository$removeDevice$Anon1.result;
        Object a2 = ff6.a();
        i = deviceRepository$removeDevice$Anon1.label;
        if (i != 0) {
        }
        ap4 ap42 = (ap4) obj2;
        deviceRepository.mDeviceDao.removeDeviceByDeviceId(device.getDeviceId());
        return ap42;
    }

    @DexIgnore
    public final Object saveWatchParamModel(WatchParam watchParam, xe6<? super cd6> xe6) {
        this.mSkuDao.addOrUpdateWatchParam(watchParam);
        return cd6.a;
    }

    @DexIgnore
    public final Object swapPairingKey(String str, String str2, xe6<? super ap4<String>> xe6) {
        return this.mDeviceRemoteDataSource.swapPairingKey(str, str2, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object updateDevice(Device device, boolean z, xe6<? super ap4<Void>> xe6) {
        DeviceRepository$updateDevice$Anon1 deviceRepository$updateDevice$Anon1;
        int i;
        DeviceRepository deviceRepository;
        Device deviceByDeviceId;
        if (xe6 instanceof DeviceRepository$updateDevice$Anon1) {
            deviceRepository$updateDevice$Anon1 = (DeviceRepository$updateDevice$Anon1) xe6;
            int i2 = deviceRepository$updateDevice$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRepository$updateDevice$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRepository$updateDevice$Anon1.result;
                Object a = ff6.a();
                i = deviceRepository$updateDevice$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "updateDevice " + device + " isRemote " + z);
                    if (!z) {
                        if (!TextUtils.isEmpty(device.getDeviceId())) {
                            this.mDeviceDao.addOrUpdateDevice(device);
                        }
                        return new cp4((Object) null, false, 2, (qg6) null);
                    } else if (TextUtils.isEmpty(device.getDeviceId())) {
                        FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateDevice can't update device without serial number");
                        return new zo4(600, new ServerError(), (Throwable) null, (String) null, 8, (qg6) null);
                    } else {
                        DeviceRemoteDataSource deviceRemoteDataSource = this.mDeviceRemoteDataSource;
                        deviceRepository$updateDevice$Anon1.L$0 = this;
                        deviceRepository$updateDevice$Anon1.L$1 = device;
                        deviceRepository$updateDevice$Anon1.Z$0 = z;
                        deviceRepository$updateDevice$Anon1.label = 1;
                        obj = deviceRemoteDataSource.updateDevice(device, deviceRepository$updateDevice$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        deviceRepository = this;
                    }
                } else if (i == 1) {
                    boolean z2 = deviceRepository$updateDevice$Anon1.Z$0;
                    device = (Device) deviceRepository$updateDevice$Anon1.L$1;
                    deviceRepository = (DeviceRepository) deviceRepository$updateDevice$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 ap4 = (ap4) obj;
                deviceByDeviceId = deviceRepository.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                if (deviceByDeviceId == null) {
                    deviceRepository.mDeviceDao.addOrUpdateDevice(deviceByDeviceId);
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "device is removed, no need to update to DB");
                }
                return ap4;
            }
        }
        deviceRepository$updateDevice$Anon1 = new DeviceRepository$updateDevice$Anon1(this, xe6);
        Object obj2 = deviceRepository$updateDevice$Anon1.result;
        Object a2 = ff6.a();
        i = deviceRepository$updateDevice$Anon1.label;
        if (i != 0) {
        }
        ap4 ap42 = (ap4) obj2;
        deviceByDeviceId = deviceRepository.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
        if (deviceByDeviceId == null) {
        }
        return ap42;
    }

    @DexIgnore
    public final Object updateDeviceSecretKey(String str, String str2, xe6<? super ap4<ku3>> xe6) {
        return this.mDeviceRemoteDataSource.updateDeviceSecretKey(str, str2, xe6);
    }
}
