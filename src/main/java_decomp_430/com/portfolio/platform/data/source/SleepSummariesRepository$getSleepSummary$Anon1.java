package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.ku3;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSleepSummary$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<MFSleepDay, ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummary$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummary$Anon1 sleepSummariesRepository$getSleepSummary$Anon1, List list) {
            this.this$0 = sleepSummariesRepository$getSleepSummary$Anon1;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ku3>> xe6) {
            Date o = bk4.o(this.this$0.$date);
            Date j = bk4.j(this.this$0.$date);
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            String e = bk4.e(o);
            wg6.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = bk4.e(j);
            wg6.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, xe6);
        }

        @DexIgnore
        public LiveData<MFSleepDay> loadFromDb() {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(this.this$0.$date);
            SleepDao access$getMSleepDao$p = this.this$0.this$0.mSleepDao;
            String e = bk4.e(this.this$0.$date);
            wg6.a((Object) e, "DateHelper.formatShortDate(date)");
            return access$getMSleepDao$p.getSleepDayLiveData(e);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(ku3 ku3) {
            wg6.b(ku3, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getSleepSummary saveCallResult onResponse: response = " + ku3);
            SleepSummariesRepository$getSleepSummary$Anon1 sleepSummariesRepository$getSleepSummary$Anon1 = this.this$0;
            sleepSummariesRepository$getSleepSummary$Anon1.this$0.saveSleepSummary$app_fossilRelease(ku3, sleepSummariesRepository$getSleepSummary$Anon1.$date);
        }

        @DexIgnore
        public boolean shouldFetch(MFSleepDay mFSleepDay) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SleepSummariesRepository$getSleepSummary$Anon1(SleepSummariesRepository sleepSummariesRepository, Date date) {
        this.this$0 = sleepSummariesRepository;
        this.$date = date;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1$Anon1_Level2] */
    public final LiveData<yx5<MFSleepDay>> apply(List<FitnessDataWrapper> list) {
        return new Anon1_Level2(this, list).asLiveData();
    }
}
