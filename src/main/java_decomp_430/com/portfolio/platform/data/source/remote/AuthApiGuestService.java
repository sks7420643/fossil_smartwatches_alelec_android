package com.portfolio.platform.data.source.remote;

import com.fossil.jy6;
import com.fossil.ku3;
import com.fossil.rx6;
import com.fossil.uy6;
import com.fossil.xe6;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiGuestService {
    @DexIgnore
    @uy6("rpc/auth/check-account-existence-by-email")
    Object checkAuthenticationEmailExisting(@jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @uy6("rpc/auth/check-account-existence-by-social")
    Object checkAuthenticationSocialExisting(@jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @uy6("rpc/auth/login")
    Object loginWithEmail(@jy6 ku3 ku3, xe6<? super rx6<Auth>> xe6);

    @DexIgnore
    @uy6("rpc/auth/login-with")
    Object loginWithSocial(@jy6 ku3 ku3, xe6<? super rx6<Auth>> xe6);

    @DexIgnore
    @uy6("rpc/auth/password/request-reset")
    Object passwordRequestReset(@jy6 ku3 ku3, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @uy6("rpc/auth/register")
    Object registerEmail(@jy6 SignUpEmailAuth signUpEmailAuth, xe6<? super rx6<Auth>> xe6);

    @DexIgnore
    @uy6("rpc/auth/register-with")
    Object registerSocial(@jy6 SignUpSocialAuth signUpSocialAuth, xe6<? super rx6<Auth>> xe6);

    @DexIgnore
    @uy6("rpc/auth/request-email-otp")
    Object requestEmailOtp(@jy6 ku3 ku3, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @uy6("rpc/auth/token/exchange-legacy")
    Call<Auth> tokenExchangeLegacy(@jy6 ku3 ku3);

    @DexIgnore
    @uy6("rpc/auth/token/refresh")
    Call<Auth> tokenRefresh(@jy6 ku3 ku3);

    @DexIgnore
    @uy6("rpc/auth/verify-email-otp")
    Object verifyEmailOtp(@jy6 ku3 ku3, xe6<? super rx6<Void>> xe6);
}
