package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.ei;
import com.fossil.hh;
import com.fossil.k44;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.s44;
import com.fossil.vh;
import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppDao_Impl implements MicroAppDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<DeclarationFile> __insertionAdapterOfDeclarationFile;
    @DexIgnore
    public /* final */ hh<MicroApp> __insertionAdapterOfMicroApp;
    @DexIgnore
    public /* final */ hh<MicroAppSetting> __insertionAdapterOfMicroAppSetting;
    @DexIgnore
    public /* final */ hh<MicroAppVariant> __insertionAdapterOfMicroAppVariant;
    @DexIgnore
    public /* final */ k44 __millisecondDateConverter; // = new k44();
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllDeclarationFileTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllMicroAppGalleryTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllMicroAppSettingTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllMicroAppVariantTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteMicroAppsBySerial;
    @DexIgnore
    public /* final */ s44 __stringArrayConverter; // = new s44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<MicroApp> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microApp` (`id`,`name`,`nameKey`,`serialNumber`,`categories`,`description`,`descriptionKey`,`icon`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MicroApp microApp) {
            if (microApp.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, microApp.getId());
            }
            if (microApp.getName() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, microApp.getName());
            }
            if (microApp.getNameKey() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, microApp.getNameKey());
            }
            if (microApp.getSerialNumber() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, microApp.getSerialNumber());
            }
            String a = MicroAppDao_Impl.this.__stringArrayConverter.a(microApp.getCategories());
            if (a == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a);
            }
            if (microApp.getDescription() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, microApp.getDescription());
            }
            if (microApp.getDescriptionKey() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, microApp.getDescriptionKey());
            }
            if (microApp.getIcon() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, microApp.getIcon());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<MicroAppVariant> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppVariant` (`id`,`appId`,`name`,`description`,`createdAt`,`updatedAt`,`majorNumber`,`minorNumber`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MicroAppVariant microAppVariant) {
            if (microAppVariant.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, microAppVariant.getId());
            }
            if (microAppVariant.getAppId() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, microAppVariant.getAppId());
            }
            if (microAppVariant.getName() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, microAppVariant.getName());
            }
            if (microAppVariant.getDescription() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, microAppVariant.getDescription());
            }
            miVar.a(5, MicroAppDao_Impl.this.__millisecondDateConverter.a(microAppVariant.getCreatedAt()));
            miVar.a(6, MicroAppDao_Impl.this.__millisecondDateConverter.a(microAppVariant.getUpdatedAt()));
            miVar.a(7, (long) microAppVariant.getMajorNumber());
            miVar.a(8, (long) microAppVariant.getMinorNumber());
            if (microAppVariant.getSerialNumber() == null) {
                miVar.a(9);
            } else {
                miVar.a(9, microAppVariant.getSerialNumber());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends hh<MicroAppSetting> {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppSetting` (`id`,`appId`,`setting`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MicroAppSetting microAppSetting) {
            if (microAppSetting.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, microAppSetting.getId());
            }
            if (microAppSetting.getAppId() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, microAppSetting.getAppId());
            }
            if (microAppSetting.getSetting() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, microAppSetting.getSetting());
            }
            if (microAppSetting.getCreatedAt() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, microAppSetting.getCreatedAt());
            }
            if (microAppSetting.getUpdatedAt() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, microAppSetting.getUpdatedAt());
            }
            miVar.a(6, (long) microAppSetting.getPinType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends hh<DeclarationFile> {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `declarationFile` (`appId`,`serialNumber`,`variantName`,`id`,`description`,`content`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, DeclarationFile declarationFile) {
            String str = declarationFile.appId;
            if (str == null) {
                miVar.a(1);
            } else {
                miVar.a(1, str);
            }
            String str2 = declarationFile.serialNumber;
            if (str2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, str2);
            }
            String str3 = declarationFile.variantName;
            if (str3 == null) {
                miVar.a(3);
            } else {
                miVar.a(3, str3);
            }
            if (declarationFile.getId() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, declarationFile.getId());
            }
            if (declarationFile.getDescription() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, declarationFile.getDescription());
            }
            if (declarationFile.getContent() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, declarationFile.getContent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microApp";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends vh {
        @DexIgnore
        public Anon6(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends vh {
        @DexIgnore
        public Anon7(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppVariant";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends vh {
        @DexIgnore
        public Anon8(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM declarationFile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends vh {
        @DexIgnore
        public Anon9(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microApp WHERE serialNumber = ?";
        }
    }

    @DexIgnore
    public MicroAppDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfMicroApp = new Anon1(ohVar);
        this.__insertionAdapterOfMicroAppVariant = new Anon2(ohVar);
        this.__insertionAdapterOfMicroAppSetting = new Anon3(ohVar);
        this.__insertionAdapterOfDeclarationFile = new Anon4(ohVar);
        this.__preparedStmtOfClearAllMicroAppGalleryTable = new Anon5(ohVar);
        this.__preparedStmtOfClearAllMicroAppSettingTable = new Anon6(ohVar);
        this.__preparedStmtOfClearAllMicroAppVariantTable = new Anon7(ohVar);
        this.__preparedStmtOfClearAllDeclarationFileTable = new Anon8(ohVar);
        this.__preparedStmtOfDeleteMicroAppsBySerial = new Anon9(ohVar);
    }

    @DexIgnore
    public void clearAllDeclarationFileTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllDeclarationFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllDeclarationFileTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroApp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroAppGalleryTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroAppSettingTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllMicroAppSettingTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppSettingTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroAppVariantTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllMicroAppVariantTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppVariantTable.release(acquire);
        }
    }

    @DexIgnore
    public void deleteMicroAppsBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteMicroAppsBySerial.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppsBySerial.release(acquire);
        }
    }

    @DexIgnore
    public List<DeclarationFile> getAllDeclarationFile() {
        rh b = rh.b("SELECT*FROM declarationFile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "appId");
            int b3 = ai.b(a, "serialNumber");
            int b4 = ai.b(a, "variantName");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "description");
            int b7 = ai.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(a.getString(b5), a.getString(b6), a.getString(b7));
                declarationFile.appId = a.getString(b2);
                declarationFile.serialNumber = a.getString(b3);
                declarationFile.variantName = a.getString(b4);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppVariant> getAllMicroAppVariant(String str, int i) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM microAppVariant WHERE serialNumber = ? AND majorNumber = ?", 2);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        b.a(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "description");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "updatedAt");
            int b8 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ai.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i2 = b2;
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = i2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial() {
        rh b = rh.b("SELECT * FROM microAppVariant GROUP BY serialNumber", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "description");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "updatedAt");
            int b8 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ai.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppVariant> getAllMicroAppVariants() {
        rh b = rh.b("SELECT*FROM microAppVariant", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "description");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "updatedAt");
            int b8 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ai.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> getAllMicroApps() {
        rh b = rh.b("SELECT*FROM microApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, "serialNumber");
            int b6 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b7 = ai.b(a, "description");
            int b8 = ai.b(a, "descriptionKey");
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroApp.COLUMN_ICON);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<DeclarationFile> getDeclarationFiles(String str, String str2, String str3) {
        rh b = rh.b("SELECT * FROM declarationFile WHERE appId = ? AND serialNumber = ? AND variantName = ? ORDER BY rowid ASC", 3);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        if (str2 == null) {
            b.a(2);
        } else {
            b.a(2, str2);
        }
        if (str3 == null) {
            b.a(3);
        } else {
            b.a(3, str3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "appId");
            int b3 = ai.b(a, "serialNumber");
            int b4 = ai.b(a, "variantName");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "description");
            int b7 = ai.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(a.getString(b5), a.getString(b6), a.getString(b7));
                declarationFile.appId = a.getString(b2);
                declarationFile.serialNumber = a.getString(b3);
                declarationFile.variantName = a.getString(b4);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> getListMicroApp(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM microApp WHERE serialNumber = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, "serialNumber");
            int b6 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b7 = ai.b(a, "description");
            int b8 = ai.b(a, "descriptionKey");
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroApp.COLUMN_ICON);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppSetting> getListMicroAppSetting() {
        rh b = rh.b("SELECT * FROM microAppSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = ai.b(a, "createdAt");
            int b6 = ai.b(a, "updatedAt");
            int b7 = ai.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroApp getMicroApp(String str, String str2) {
        MicroApp microApp;
        String str3 = str;
        String str4 = str2;
        rh b = rh.b("SELECT * FROM microApp WHERE serialNumber = ? AND id = ?", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, "serialNumber");
            int b6 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b7 = ai.b(a, "description");
            int b8 = ai.b(a, "descriptionKey");
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroApp.COLUMN_ICON);
            if (a.moveToFirst()) {
                microApp = new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9));
            } else {
                microApp = null;
            }
            return microApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroApp getMicroAppById(String str, String str2) {
        MicroApp microApp;
        String str3 = str;
        String str4 = str2;
        rh b = rh.b("SELECT * FROM microApp WHERE id=? AND serialNumber=?", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, "serialNumber");
            int b6 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b7 = ai.b(a, "description");
            int b8 = ai.b(a, "descriptionKey");
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroApp.COLUMN_ICON);
            if (a.moveToFirst()) {
                microApp = new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9));
            } else {
                microApp = null;
            }
            return microApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        String str2 = str;
        StringBuilder a = ei.a();
        a.append("SELECT ");
        a.append("*");
        a.append(" FROM microApp WHERE id IN (");
        int size = list.size();
        ei.a(a, size);
        a.append(") AND serialNumber=");
        a.append("?");
        int i = 1;
        int i2 = size + 1;
        rh b = rh.b(a.toString(), i2);
        for (String next : list) {
            if (next == null) {
                b.a(i);
            } else {
                b.a(i, next);
            }
            i++;
        }
        if (str2 == null) {
            b.a(i2);
        } else {
            b.a(i2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "id");
            int b3 = ai.b(a2, "name");
            int b4 = ai.b(a2, "nameKey");
            int b5 = ai.b(a2, "serialNumber");
            int b6 = ai.b(a2, HelpRequest.INCLUDE_CATEGORIES);
            int b7 = ai.b(a2, "description");
            int b8 = ai.b(a2, "descriptionKey");
            int b9 = ai.b(a2, com.portfolio.platform.data.legacy.threedotzero.MicroApp.COLUMN_ICON);
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new MicroApp(a2.getString(b2), a2.getString(b3), a2.getString(b4), a2.getString(b5), this.__stringArrayConverter.a(a2.getString(b6)), a2.getString(b7), a2.getString(b8), a2.getString(b9)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroAppSetting getMicroAppSetting(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM microAppSetting WHERE appId= ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            return a.moveToFirst() ? new MicroAppSetting(a.getString(ai.b(a, "id")), a.getString(ai.b(a, "appId")), a.getString(ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING)), a.getString(ai.b(a, "createdAt")), a.getString(ai.b(a, "updatedAt")), a.getInt(ai.b(a, "pinType"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3) {
        MicroAppVariant microAppVariant;
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        rh b = rh.b("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ? AND name = ?", 4);
        if (str4 == null) {
            b.a(1);
        } else {
            b.a(1, str4);
        }
        if (str5 == null) {
            b.a(2);
        } else {
            b.a(2, str5);
        }
        b.a(3, (long) i);
        if (str6 == null) {
            b.a(4);
        } else {
            b.a(4, str6);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "description");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "updatedAt");
            int b8 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ai.b(a, "serialNumber");
            if (a.moveToFirst()) {
                microAppVariant = new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10));
            } else {
                microAppVariant = null;
            }
            return microAppVariant;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        rh b = rh.b("SELECT * FROM microAppSetting WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = ai.b(a, "createdAt");
            int b6 = ai.b(a, "updatedAt");
            int b7 = ai.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> queryMicroAppByName(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        rh b = rh.b("SELECT * FROM microApp WHERE name LIKE '%' || ? || '%' AND serialNumber=?", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, "serialNumber");
            int b6 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b7 = ai.b(a, "description");
            int b8 = ai.b(a, "descriptionKey");
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroApp.COLUMN_ICON);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertDeclarationFileList(List<DeclarationFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDeclarationFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListMicroApp(List<MicroApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroApp(MicroApp microApp) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(microApp);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppSetting(MicroAppSetting microAppSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(microAppSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppSettingList(List<MicroAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppVariant(MicroAppVariant microAppVariant) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(microAppVariant);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppVariantList(List<MicroAppVariant> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        MicroAppVariant microAppVariant;
        String str3 = str;
        String str4 = str2;
        rh b = rh.b("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ?", 3);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        b.a(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "appId");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "description");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "updatedAt");
            int b8 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ai.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ai.b(a, "serialNumber");
            if (a.moveToFirst()) {
                microAppVariant = new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10));
            } else {
                microAppVariant = null;
            }
            return microAppVariant;
        } finally {
            a.close();
            b.c();
        }
    }
}
