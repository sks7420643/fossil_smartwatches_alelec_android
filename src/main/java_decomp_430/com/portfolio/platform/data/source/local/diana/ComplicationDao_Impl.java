package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.ei;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.s44;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationDao_Impl implements ComplicationDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<Complication> __insertionAdapterOfComplication;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ s44 __stringArrayConverter; // = new s44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<Complication> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complication` (`complicationId`,`name`,`nameKey`,`categories`,`description`,`descriptionKey`,`icon`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, Complication complication) {
            if (complication.getComplicationId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, complication.getComplicationId());
            }
            if (complication.getName() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, complication.getName());
            }
            if (complication.getNameKey() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, complication.getNameKey());
            }
            String a = ComplicationDao_Impl.this.__stringArrayConverter.a(complication.getCategories());
            if (a == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a);
            }
            if (complication.getDescription() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, complication.getDescription());
            }
            if (complication.getDescriptionKey() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, complication.getDescriptionKey());
            }
            if (complication.getIcon() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, complication.getIcon());
            }
            if (complication.getCreatedAt() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, complication.getCreatedAt());
            }
            if (complication.getUpdatedAt() == null) {
                miVar.a(9);
            } else {
                miVar.a(9, complication.getUpdatedAt());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM complication";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<Complication> call() throws Exception {
            Cursor a = bi.a(ComplicationDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "complicationId");
                int b2 = ai.b(a, "name");
                int b3 = ai.b(a, "nameKey");
                int b4 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
                int b5 = ai.b(a, "description");
                int b6 = ai.b(a, "descriptionKey");
                int b7 = ai.b(a, MicroApp.COLUMN_ICON);
                int b8 = ai.b(a, "createdAt");
                int b9 = ai.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Complication(a.getString(b), a.getString(b2), a.getString(b3), ComplicationDao_Impl.this.__stringArrayConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ComplicationDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfComplication = new Anon1(ohVar);
        this.__preparedStmtOfClearAll = new Anon2(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public List<Complication> getAllComplications() {
        rh b = rh.b("SELECT * FROM complication", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "complicationId");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b6 = ai.b(a, "description");
            int b7 = ai.b(a, "descriptionKey");
            int b8 = ai.b(a, MicroApp.COLUMN_ICON);
            int b9 = ai.b(a, "createdAt");
            int b10 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Complication(a.getString(b2), a.getString(b3), a.getString(b4), this.__stringArrayConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<Complication>> getAllComplicationsAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"complication"}, false, new Anon3(rh.b("SELECT * FROM complication", 0)));
    }

    @DexIgnore
    public Complication getComplicationById(String str) {
        Complication complication;
        String str2 = str;
        rh b = rh.b("SELECT * FROM complication WHERE complicationId=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "complicationId");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b6 = ai.b(a, "description");
            int b7 = ai.b(a, "descriptionKey");
            int b8 = ai.b(a, MicroApp.COLUMN_ICON);
            int b9 = ai.b(a, "createdAt");
            int b10 = ai.b(a, "updatedAt");
            if (a.moveToFirst()) {
                complication = new Complication(a.getString(b2), a.getString(b3), a.getString(b4), this.__stringArrayConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10));
            } else {
                complication = null;
            }
            return complication;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Complication> getComplicationByIds(List<String> list) {
        StringBuilder a = ei.a();
        a.append("SELECT ");
        a.append("*");
        a.append(" FROM complication WHERE complicationId IN (");
        int size = list.size();
        ei.a(a, size);
        a.append(")");
        rh b = rh.b(a.toString(), size + 0);
        int i = 1;
        for (String next : list) {
            if (next == null) {
                b.a(i);
            } else {
                b.a(i, next);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "complicationId");
            int b3 = ai.b(a2, "name");
            int b4 = ai.b(a2, "nameKey");
            int b5 = ai.b(a2, HelpRequest.INCLUDE_CATEGORIES);
            int b6 = ai.b(a2, "description");
            int b7 = ai.b(a2, "descriptionKey");
            int b8 = ai.b(a2, MicroApp.COLUMN_ICON);
            int b9 = ai.b(a2, "createdAt");
            int b10 = ai.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new Complication(a2.getString(b2), a2.getString(b3), a2.getString(b4), this.__stringArrayConverter.a(a2.getString(b5)), a2.getString(b6), a2.getString(b7), a2.getString(b8), a2.getString(b9), a2.getString(b10)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Complication> queryComplicationByName(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM complication WHERE name LIKE '%' || ? || '%'", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "complicationId");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "nameKey");
            int b5 = ai.b(a, HelpRequest.INCLUDE_CATEGORIES);
            int b6 = ai.b(a, "description");
            int b7 = ai.b(a, "descriptionKey");
            int b8 = ai.b(a, MicroApp.COLUMN_ICON);
            int b9 = ai.b(a, "createdAt");
            int b10 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Complication(a.getString(b2), a.getString(b3), a.getString(b4), this.__stringArrayConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertComplicationList(List<Complication> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplication.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
