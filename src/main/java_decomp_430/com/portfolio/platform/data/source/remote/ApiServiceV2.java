package com.portfolio.platform.data.source.remote;

import com.fossil.dz6;
import com.fossil.jy6;
import com.fossil.ku3;
import com.fossil.ky6;
import com.fossil.ny6;
import com.fossil.py6;
import com.fossil.rx6;
import com.fossil.ty6;
import com.fossil.uy6;
import com.fossil.vy6;
import com.fossil.xe6;
import com.fossil.yy6;
import com.fossil.zq6;
import com.fossil.zy6;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.ServerFitnessData;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.User;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ApiServiceV2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getDeviceAssets$default(ApiServiceV2 apiServiceV2, int i, int i2, String str, String str2, String str3, String str4, String str5, xe6 xe6, int i3, Object obj) {
            if (obj == null) {
                return apiServiceV2.getDeviceAssets(i, i2, str, str2, str3, str4, (i3 & 64) != 0 ? null : str5, xe6);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getDeviceAssets");
        }
    }

    @DexIgnore
    @py6(hasBody = true, method = "DELETE", path = "users/me/diana-presets")
    Object batchDeleteDianaPresetList(@jy6 ku3 ku3, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @py6(hasBody = true, method = "DELETE", path = "users/me/hybrid-presets")
    Object batchDeleteHybridPresetList(@jy6 ku3 ku3, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @ky6("users/me/alarms/{id}")
    Object deleteAlarm(@yy6("id") String str, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @py6(hasBody = true, method = "DELETE", path = "users/me/alarms")
    Object deleteAlarms(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<ku3>>> xe6);

    @DexIgnore
    @ky6("users/me/devices/{deviceId}")
    Object deleteDevice(@yy6("deviceId") String str, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @py6(hasBody = true, method = "DELETE", path = "users/me/goal-events")
    Object deleteGoalTrackingData(@jy6 ku3 ku3, xe6<? super rx6<GoalEvent>> xe6);

    @DexIgnore
    @ky6("users/me")
    Object deleteUser(xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @ny6
    Object downloadFile(@dz6 String str, xe6<? super rx6<zq6>> xe6);

    @DexIgnore
    @ny6("users/me/heart-rate-daily-summaries")
    Object fetchDailyHeartRateSummaries(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @uy6("rpc/device/generate-pairing-key")
    Object generatePairingKey(@jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ny6("users/me/activities")
    Object getActivities(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ApiResponse<Activity>>> xe6);

    @DexIgnore
    @ny6("users/me/activity-settings")
    Object getActivitySetting(xe6<? super rx6<ActivitySettings>> xe6);

    @DexIgnore
    @ny6("users/me/activity-statistic")
    Object getActivityStatistic(xe6<? super rx6<ActivityStatistic>> xe6);

    @DexIgnore
    @ny6("users/me/alarms")
    Object getAlarms(@zy6("limit") int i, xe6<? super rx6<ApiResponse<Alarm>>> xe6);

    @DexIgnore
    @ny6("diana-complication-apps")
    Object getAllComplication(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<Complication>>> xe6);

    @DexIgnore
    @ny6("hybrid-apps")
    Object getAllMicroApp(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<MicroApp>>> xe6);

    @DexIgnore
    @ny6("hybrid-app-variants")
    Object getAllMicroAppVariant(@zy6("serialNumber") String str, @zy6("majorNumber") String str2, @zy6("minorNumber") String str3, xe6<? super rx6<ApiResponse<MicroAppVariant>>> xe6);

    @DexIgnore
    @ny6("diana-pusher-apps")
    Object getAllWatchApp(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<WatchApp>>> xe6);

    @DexIgnore
    @ny6("app-categories")
    Object getCategories(xe6<? super rx6<ApiResponse<Category>>> xe6);

    @DexIgnore
    @ny6("users/me/heart-rate-daily-summaries")
    Object getDailyHeartRateSummaries(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ApiResponse<ku3>>> xe6);

    @DexIgnore
    @ny6("users/me/devices/{deviceId}")
    Object getDevice(@yy6("deviceId") String str, xe6<? super rx6<Device>> xe6);

    @DexIgnore
    @ny6("assets/app-sku-images")
    Object getDeviceAssets(@zy6("size") int i, @zy6("offset") int i2, @zy6("metadata.serialNumber") String str, @zy6("metadata.feature") String str2, @zy6("metadata.resolution") String str3, @zy6("metadata.platform") String str4, @zy6("metadata.fastPairId") String str5, xe6<? super rx6<ApiResponse<ku3>>> xe6);

    @DexIgnore
    @ny6("users/me/devices/{id}/secret-key")
    Object getDeviceSecretKey(@yy6("id") String str, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ny6("users/me/devices")
    Object getDevices(xe6<? super rx6<ApiResponse<Device>>> xe6);

    @DexIgnore
    @ny6("/v2/assets/diana-complication-ringstyles")
    Object getDianaComplicationRingStyles(@zy6("metadata.isDefault") boolean z, @zy6("metadata.serialNumber") String str, xe6<? super rx6<ApiResponse<DianaComplicationRingStyle>>> xe6);

    @DexIgnore
    @ny6("users/me/diana-presets")
    Object getDianaPresetList(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<DianaPreset>>> xe6);

    @DexIgnore
    @ny6("diana-recommended-presets")
    Object getDianaRecommendPresetList(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<DianaRecommendPreset>>> xe6);

    @DexIgnore
    @ny6("users/me/goal-settings")
    Object getGoalSetting(xe6<? super rx6<GoalSetting>> xe6);

    @DexIgnore
    @ny6("users/me/goal-events")
    Object getGoalTrackingDataList(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ApiResponse<GoalEvent>>> xe6);

    @DexIgnore
    @ny6("users/me/goal-daily-summaries")
    Object getGoalTrackingSummaries(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ApiResponse<GoalDailySummary>>> xe6);

    @DexIgnore
    @ny6("users/me/goal-daily-summaries/{date}")
    Object getGoalTrackingSummary(@yy6("date") String str, xe6<? super rx6<GoalDailySummary>> xe6);

    @DexIgnore
    @ny6("users/me/heart-rates")
    Object getHeartRateSamples(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ApiResponse<HeartRate>>> xe6);

    @DexIgnore
    @ny6("users/me/hybrid-presets")
    Object getHybridPresetList(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<HybridPreset>>> xe6);

    @DexIgnore
    @ny6("hybrid-recommended-presets")
    Object getHybridRecommendPresetList(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<HybridRecommendPreset>>> xe6);

    @DexIgnore
    @ny6("users/me/devices/latest-active")
    Object getLastActiveDevice(xe6<? super rx6<Device>> xe6);

    @DexIgnore
    @ny6("assets/watch-params")
    Object getLatestWatchParams(@zy6("metadata.serialNumber") String str, @zy6("metadata.version.major") int i, @zy6("sortBy") String str2, @zy6("offset") int i2, @zy6("limit") int i3, xe6<? super rx6<ApiResponse<WatchParameterResponse>>> xe6);

    @DexIgnore
    @ny6("rpc/activity/get-recommended-goals")
    Object getRecommendedGoalsRaw(@zy6("age") int i, @zy6("weightInGrams") int i2, @zy6("heightInCentimeters") int i3, @zy6("gender") String str, xe6<? super rx6<ActivityRecommendedGoals>> xe6);

    @DexIgnore
    @ny6("rpc/sleep/get-recommended-goals")
    Object getRecommendedSleepGoalRaw(@zy6("age") int i, @zy6("weightInGrams") int i2, @zy6("heightInCentimeters") int i3, @zy6("gender") String str, xe6<? super rx6<SleepRecommendedGoal>> xe6);

    @DexIgnore
    @ny6("server-settings")
    Object getServerSettingList(@zy6("limit") int i, @zy6("offset") int i2, xe6<? super rx6<ServerSettingList>> xe6);

    @DexIgnore
    @ny6("skus")
    Object getSkus(@zy6("limit") int i, @zy6("offset") int i2, xe6<? super rx6<ApiResponse<SKUModel>>> xe6);

    @DexIgnore
    @ny6("users/me/sleep-sessions")
    Object getSleepSessions(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ny6("users/me/sleep-settings")
    Object getSleepSetting(xe6<? super rx6<MFSleepSettings>> xe6);

    @DexIgnore
    @ny6("users/me/sleep-statistic")
    Object getSleepStatistic(xe6<? super rx6<SleepStatistic>> xe6);

    @DexIgnore
    @ny6("users/me/sleep-daily-summaries")
    Object getSleepSummaries(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ny6("users/me/activity-daily-summaries")
    Object getSummaries(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @uy6("rpc/commute-time/calc-traffic-status")
    Object getTrafficStatus(@jy6 TrafficRequest trafficRequest, xe6<? super rx6<TrafficResponse>> xe6);

    @DexIgnore
    @ny6("users/me/profile")
    Object getUser(xe6<? super rx6<User>> xe6);

    @DexIgnore
    @ny6("/v2/diana-watch-faces")
    Object getWatchFaces(@zy6("serialNumber") String str, xe6<? super rx6<ApiResponse<WatchFace>>> xe6);

    @DexIgnore
    @ny6("/v2/assets/diana-watch-localizations")
    Object getWatchLocalizationData(@zy6("metadata.locale") String str, xe6<? super rx6<ApiResponse<WatchLocalization>>> xe6);

    @DexIgnore
    @ny6("weather-info")
    Object getWeather(@zy6("lat") String str, @zy6("lng") String str2, @zy6("temperatureUnit") String str3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ny6("users/me/workout-sessions")
    Object getWorkoutSessions(@zy6("startDate") String str, @zy6("endDate") String str2, @zy6("offset") int i, @zy6("limit") int i2, xe6<? super rx6<ApiResponse<ServerWorkoutSession>>> xe6);

    @DexIgnore
    @uy6("users/me/activities")
    Object insertActivities(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<Activity>>> xe6);

    @DexIgnore
    @uy6("users/me/fitness-files")
    Object insertFitnessDataFiles(@jy6 ApiResponse<ServerFitnessData> apiResponse, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @uy6("users/me/goal-events")
    Object insertGoalTrackingDataList(@jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @vy6("users/me/installations")
    Object insertInstallation(@jy6 Installation installation, xe6<? super rx6<Installation>> xe6);

    @DexIgnore
    @uy6("users/me/sleep-sessions")
    Object insertSleepSessions(@jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @vy6("users/me/devices")
    Object linkDevice(@jy6 Device device, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @vy6("users/me/diana-presets")
    Object replaceDianaPresetList(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<DianaPreset>>> xe6);

    @DexIgnore
    @vy6("users/me/hybrid-presets")
    Object replaceHybridPresetList(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<HybridPreset>>> xe6);

    @DexIgnore
    @ty6("users/me/sleep-settings")
    Object setSleepSetting(@jy6 ku3 ku3, xe6<? super rx6<MFSleepSettings>> xe6);

    @DexIgnore
    @uy6("rpc/device/swap-pairing-keys")
    Object swapPairingKey(@jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ty6("users/me/activity-settings")
    Object updateActivitySetting(@jy6 ku3 ku3, xe6<? super rx6<ActivitySettings>> xe6);

    @DexIgnore
    @ty6("users/me/devices/{deviceId}")
    Object updateDevice(@yy6("deviceId") String str, @jy6 Device device, xe6<? super rx6<Void>> xe6);

    @DexIgnore
    @ty6("users/me/devices/{id}/secret-key")
    Object updateDeviceSecretKey(@yy6("id") String str, @jy6 ku3 ku3, xe6<? super rx6<ku3>> xe6);

    @DexIgnore
    @ty6("users/me/profile")
    Object updateUser(@jy6 ku3 ku3, xe6<? super rx6<User>> xe6);

    @DexIgnore
    @ty6("users/me/alarms")
    Object upsertAlarms(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<Alarm>>> xe6);

    @DexIgnore
    @ty6("users/me/diana-presets")
    Object upsertDianaPresetList(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<DianaPreset>>> xe6);

    @DexIgnore
    @ty6("users/me/goal-settings")
    Object upsertGoalSetting(@jy6 ku3 ku3, xe6<? super rx6<GoalSetting>> xe6);

    @DexIgnore
    @ty6("users/me/hybrid-presets")
    Object upsertHybridPresetList(@jy6 ku3 ku3, xe6<? super rx6<ApiResponse<HybridPreset>>> xe6);
}
