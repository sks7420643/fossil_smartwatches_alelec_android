package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.du3;
import com.fossil.ku3;
import com.fossil.rx6;
import com.fossil.sd;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummary$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<ActivitySummary, ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(SummariesRepository$getSummary$Anon1 summariesRepository$getSummary$Anon1, List list) {
            this.this$0 = summariesRepository$getSummary$Anon1;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ku3>> xe6) {
            Date o = bk4.o(this.this$0.$date);
            Date j = bk4.j(this.this$0.$date);
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            String e = bk4.e(o);
            wg6.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = bk4.e(j);
            wg6.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiServiceV2$p.getSummaries(e, e2, 0, 100, xe6);
        }

        @DexIgnore
        public LiveData<ActivitySummary> loadFromDb() {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(this.this$0.$date);
            LiveData<ActivitySummary> activitySummaryLiveData = this.this$0.this$0.mActivitySummaryDao.getActivitySummaryLiveData(instance.get(1), instance.get(2) + 1, instance.get(5));
            if (!bk4.t(this.this$0.$date).booleanValue()) {
                return activitySummaryLiveData;
            }
            LiveData<ActivitySummary> a = sd.a(activitySummaryLiveData, new SummariesRepository$getSummary$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3(this));
            wg6.a((Object) a, "Transformations.map(acti\u2026ary\n                    }");
            return a;
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "getSummary - onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(ku3 ku3) {
            List<FitnessDayData> list;
            wg6.b(ku3, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummary - saveCallResult -- date=" + this.this$0.$date + ", item=" + ku3);
            try {
                ArrayList arrayList = new ArrayList();
                du3 du3 = new du3();
                du3.a(Date.class, new GsonConvertDate());
                du3.a(DateTime.class, new GsonConvertDateTime());
                ApiResponse apiResponse = (ApiResponse) du3.a().a(ku3.toString(), new SummariesRepository$getSummary$Anon1$Anon1_Level2$saveCallResult$Anon1_Level3().getType());
                if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                    for (FitnessDayData activitySummary : list) {
                        ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                        wg6.a((Object) activitySummary2, "it.toActivitySummary()");
                        arrayList.add(activitySummary2);
                    }
                }
                List<FitnessDataWrapper> fitnessData = this.this$0.this$0.mFitnessDataDao.getFitnessData(this.this$0.$date, this.this$0.$date);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                if ((!arrayList.isEmpty()) && fitnessData.isEmpty()) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d(SummariesRepository.TAG, "upsert " + ((ActivitySummary) arrayList.get(0)));
                    ActivitySummaryDao access$getMActivitySummaryDao$p = this.this$0.this$0.mActivitySummaryDao;
                    Object obj = arrayList.get(0);
                    wg6.a(obj, "summaryList[0]");
                    access$getMActivitySummaryDao$p.upsertActivitySummary((ActivitySummary) obj);
                }
            } catch (Exception e) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummary - saveCallResult -- e=");
                e.printStackTrace();
                sb.append(cd6.a);
                local4.e(SummariesRepository.TAG, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(ActivitySummary activitySummary) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SummariesRepository$getSummary$Anon1(SummariesRepository summariesRepository, Date date) {
        this.this$0 = summariesRepository;
        this.$date = date;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon1$Anon1_Level2] */
    public final LiveData<yx5<ActivitySummary>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - date=" + this.$date + " fitnessDataList=" + list.size());
        return new Anon1_Level2(this, list).asLiveData();
    }
}
