package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cp4;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.qj4;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2Dot1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DianaPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2Dot1");
        this.mApiServiceV2Dot1 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteDianaPreset(DianaPreset dianaPreset, xe6<? super ap4<Void>> xe6) {
        DianaPresetRemoteDataSource$deleteDianaPreset$Anon1 dianaPresetRemoteDataSource$deleteDianaPreset$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DianaPresetRemoteDataSource$deleteDianaPreset$Anon1) {
            dianaPresetRemoteDataSource$deleteDianaPreset$Anon1 = (DianaPresetRemoteDataSource$deleteDianaPreset$Anon1) xe6;
            int i2 = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.result;
                Object a = ff6.a();
                i = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    fu3 fu3 = new fu3();
                    fu3.a(dianaPreset.getId());
                    ku3.a("_ids", fu3);
                    DianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1 dianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1 = new DianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1(this, ku3, (xe6) null);
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$0 = this;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$1 = dianaPreset;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$2 = ku3;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$3 = fu3;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1, dianaPresetRemoteDataSource$deleteDianaPreset$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    fu3 fu32 = (fu3) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$3;
                    ku3 ku32 = (ku3) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$2;
                    DianaPreset dianaPreset2 = (DianaPreset) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new cp4((Object) null, false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                }
                throw new kc6();
            }
        }
        dianaPresetRemoteDataSource$deleteDianaPreset$Anon1 = new DianaPresetRemoteDataSource$deleteDianaPreset$Anon1(this, xe6);
        Object obj2 = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.result;
        Object a2 = ff6.a();
        i = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadDianaPresetList(String str, xe6<? super ap4<ArrayList<DianaPreset>>> xe6) {
        DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1) xe6;
            int i2 = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.result;
                Object a = ff6.a();
                i = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1(this, str, (xe6) null);
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$0 = this;
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$1 = str;
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    cp4 cp4 = (cp4) ap4;
                    if (!cp4.b()) {
                        Object a2 = cp4.a();
                        if (a2 != null) {
                            for (DianaPreset dianaPreset : ((ApiResponse) a2).get_items()) {
                                dianaPreset.setPinType(0);
                                dianaPreset.setSerialNumber(str);
                                arrayList.add(dianaPreset);
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    return new cp4(arrayList, cp4.b());
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1(this, xe6);
        Object obj2 = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.result;
        Object a3 = ff6.a();
        i = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object downloadDianaRecommendPresetList(String str, xe6<? super ap4<ArrayList<DianaRecommendPreset>>> xe6) {
        DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1) {
            dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 = (DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1) xe6;
            int i2 = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.result;
                Object a = ff6.a();
                i = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadDianaRecommendPresetList " + str);
                    DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1 dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1 = new DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1(this, str, (xe6) null);
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$0 = this;
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$1 = str;
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1, dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadDianaRecommendPresetList success ");
                    sb.append(arrayList);
                    sb.append(" isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    local2.d(TAG, sb.toString());
                    String u = bk4.u(new Date(System.currentTimeMillis()));
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        for (DianaRecommendPreset dianaRecommendPreset : ((ApiResponse) a2).get_items()) {
                            dianaRecommendPreset.setSerialNumber(str);
                            wg6.a((Object) u, "timestamp");
                            dianaRecommendPreset.setCreatedAt(u);
                            dianaRecommendPreset.setUpdatedAt(u);
                            arrayList.add(dianaRecommendPreset);
                        }
                        return new cp4(arrayList, cp4.b());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadDianaRecommendPresetList fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverError ");
                    sb2.append(zo4.c());
                    local3.d(TAG, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 = new DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1(this, xe6);
        Object obj2 = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.result;
        Object a3 = ff6.a();
        i = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object replaceDianaPresetList(List<DianaPreset> list, xe6<? super ap4<ArrayList<DianaPreset>>> xe6) {
        DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1) xe6;
            int i2 = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.result;
                Object a = ff6.a();
                i = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    du3 du3 = new du3();
                    du3.b(new qj4());
                    Gson a2 = du3.a();
                    ku3 ku3 = new ku3();
                    Object[] array = list.toArray(new DianaPreset[0]);
                    if (array != null) {
                        ku3.a(CloudLogWriter.ITEMS_PARAM, a2.b(array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "replaceDianaPresetList jsonObject " + ku3);
                        DianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1(this, ku3, (xe6) null);
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$0 = this;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$1 = list;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$2 = a2;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$3 = ku3;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(dianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$3;
                    Gson gson = (Gson) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$2;
                    List list2 = (List) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((cp4) ap4).a();
                    if (a3 != null) {
                        for (DianaPreset dianaPreset : ((ApiResponse) a3).get_items()) {
                            dianaPreset.setPinType(0);
                            arrayList.add(dianaPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "replaceDianaPresetList success " + arrayList);
                        return new cp4(arrayList, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("replaceDianaPresetList fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" serverError ");
                    sb.append(zo4.c());
                    local3.d(TAG, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1(this, xe6);
        Object obj2 = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.result;
        Object a4 = ff6.a();
        i = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object upsertDianaPresetList(List<DianaPreset> list, xe6<? super ap4<ArrayList<DianaPreset>>> xe6) {
        DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1;
        int i;
        ap4 ap4;
        List<DianaPreset> list2 = list;
        xe6<? super ap4<ArrayList<DianaPreset>>> xe62 = xe6;
        if (xe62 instanceof DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1) xe62;
            int i2 = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.result;
                Object a = ff6.a();
                i = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    du3 du3 = new du3();
                    du3.b(new qj4());
                    Gson a2 = du3.a();
                    ku3 ku3 = new ku3();
                    Object[] array = list2.toArray(new DianaPreset[0]);
                    if (array != null) {
                        ku3.a(CloudLogWriter.ITEMS_PARAM, a2.b(array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "upsertPresetList jsonObject " + ku3);
                        DianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1(this, ku3, (xe6) null);
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$0 = this;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$1 = list2;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$2 = a2;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$3 = ku3;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(dianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$3;
                    Gson gson = (Gson) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$2;
                    List list3 = (List) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("success isCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    local2.d(TAG, sb.toString());
                    Object a3 = cp4.a();
                    if (a3 != null) {
                        for (DianaPreset dianaPreset : ((ApiResponse) a3).get_items()) {
                            FLogger.INSTANCE.getLocal().d(TAG, "update pin tpye as synced");
                            dianaPreset.setPinType(0);
                            arrayList.add(dianaPreset);
                        }
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d(TAG, "upsertPresetList success " + arrayList);
                        return new cp4(arrayList, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("upsertPresetList fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverError ");
                    sb2.append(zo4.c());
                    local4.d(TAG, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1(this, xe62);
        Object obj2 = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.result;
        Object a4 = ff6.a();
        i = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
