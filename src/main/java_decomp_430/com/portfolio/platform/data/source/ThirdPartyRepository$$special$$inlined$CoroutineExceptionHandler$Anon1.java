package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.ve6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1 extends ve6 implements CoroutineExceptionHandler {
    @DexIgnore
    public ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(af6.c cVar) {
        super(cVar);
    }

    @DexIgnore
    public void handleException(af6 af6, Throwable th) {
        wg6.b(af6, "context");
        wg6.b(th, "exception");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "exception when push third party database " + th);
    }
}
