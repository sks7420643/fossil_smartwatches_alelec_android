package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UASampleDao_Impl implements UASampleDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<UASample> __deletionAdapterOfUASample;
    @DexIgnore
    public /* final */ hh<UASample> __insertionAdapterOfUASample;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<UASample> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `uaSample` (`id`,`step`,`distance`,`calorie`,`time`) VALUES (nullif(?, 0),?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, UASample uASample) {
            miVar.a(1, (long) uASample.getId());
            miVar.a(2, (long) uASample.getStep());
            miVar.a(3, uASample.getDistance());
            miVar.a(4, uASample.getCalorie());
            miVar.a(5, uASample.getTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<UASample> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `uaSample` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, UASample uASample) {
            miVar.a(1, (long) uASample.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM uaSample";
        }
    }

    @DexIgnore
    public UASampleDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfUASample = new Anon1(ohVar);
        this.__deletionAdapterOfUASample = new Anon2(ohVar);
        this.__preparedStmtOfClearAll = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListUASample(List<UASample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfUASample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<UASample> getAllUASample() {
        rh b = rh.b("SELECT * FROM uaSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "step");
            int b4 = ai.b(a, "distance");
            int b5 = ai.b(a, "calorie");
            int b6 = ai.b(a, LogBuilder.KEY_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                UASample uASample = new UASample(a.getInt(b3), a.getDouble(b4), a.getDouble(b5), a.getLong(b6));
                uASample.setId(a.getInt(b2));
                arrayList.add(uASample);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertListUASample(List<UASample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUASample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertUASample(UASample uASample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUASample.insert(uASample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
