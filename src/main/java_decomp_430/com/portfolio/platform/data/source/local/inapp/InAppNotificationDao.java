package com.portfolio.platform.data.source.local.inapp;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.InAppNotification;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class InAppNotificationDao {
    @DexIgnore
    public abstract void delete(InAppNotification inAppNotification);

    @DexIgnore
    public abstract LiveData<List<InAppNotification>> getAllInAppNotification();

    @DexIgnore
    public abstract void insertListInAppNotification(List<InAppNotification> list);
}
