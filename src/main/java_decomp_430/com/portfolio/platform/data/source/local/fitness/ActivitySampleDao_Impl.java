package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.t34;
import com.fossil.vh;
import com.fossil.w34;
import com.fossil.x34;
import com.fossil.z34;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySampleDao_Impl extends ActivitySampleDao {
    @DexIgnore
    public /* final */ t34 __activityIntensitiesConverter; // = new t34();
    @DexIgnore
    public /* final */ w34 __dateLongStringConverter; // = new w34();
    @DexIgnore
    public /* final */ x34 __dateShortStringConverter; // = new x34();
    @DexIgnore
    public /* final */ z34 __dateTimeISOStringConverter; // = new z34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<ActivitySample> __insertionAdapterOfActivitySample;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<ActivitySample> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_sample` (`id`,`uid`,`date`,`startTime`,`endTime`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneOffsetInSecond`,`sourceId`,`syncTime`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, ActivitySample activitySample) {
            if (activitySample.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, activitySample.getId());
            }
            if (activitySample.getUid() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, activitySample.getUid());
            }
            String a = ActivitySampleDao_Impl.this.__dateShortStringConverter.a(activitySample.getDate());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            String a2 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getStartTime());
            if (a2 == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a2);
            }
            String a3 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getEndTime());
            if (a3 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a3);
            }
            miVar.a(6, activitySample.getSteps());
            miVar.a(7, activitySample.getCalories());
            miVar.a(8, activitySample.getDistance());
            miVar.a(9, (long) activitySample.getActiveTime());
            String a4 = ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(activitySample.getIntensityDistInSteps());
            if (a4 == null) {
                miVar.a(10);
            } else {
                miVar.a(10, a4);
            }
            miVar.a(11, (long) activitySample.getTimeZoneOffsetInSecond());
            if (activitySample.getSourceId() == null) {
                miVar.a(12);
            } else {
                miVar.a(12, activitySample.getSourceId());
            }
            miVar.a(13, activitySample.getSyncTime());
            miVar.a(14, activitySample.getCreatedAt());
            miVar.a(15, activitySample.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM activity_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<ActivitySample>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<ActivitySample> call() throws Exception {
            Anon3 anon3 = this;
            Cursor a = bi.a(ActivitySampleDao_Impl.this.__db, anon3.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "uid");
                int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b4 = ai.b(a, "startTime");
                int b5 = ai.b(a, "endTime");
                int b6 = ai.b(a, "steps");
                int b7 = ai.b(a, Constants.CALORIES);
                int b8 = ai.b(a, "distance");
                int b9 = ai.b(a, "activeTime");
                int b10 = ai.b(a, "intensityDistInSteps");
                int b11 = ai.b(a, "timeZoneOffsetInSecond");
                int b12 = ai.b(a, "sourceId");
                int b13 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
                int b14 = ai.b(a, "createdAt");
                int i = b;
                int b15 = ai.b(a, "updatedAt");
                int i2 = b14;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b2;
                    int i4 = i2;
                    int i5 = b15;
                    ActivitySample activitySample = new ActivitySample(a.getString(b2), ActivitySampleDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b4)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b5)), a.getDouble(b6), a.getDouble(b7), a.getDouble(b8), a.getInt(b9), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(a.getString(b10)), a.getInt(b11), a.getString(b12), a.getLong(b13), a.getLong(i4), a.getLong(i5));
                    i2 = i4;
                    int i6 = i;
                    int i7 = b3;
                    activitySample.setId(a.getString(i6));
                    arrayList.add(activitySample);
                    anon3 = this;
                    b15 = i5;
                    b3 = i7;
                    i = i6;
                    b2 = i3;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<SampleRaw>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<SampleRaw> call() throws Exception {
            Anon4 anon4 = this;
            Cursor a = bi.a(ActivitySampleDao_Impl.this.__db, anon4.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "pinType");
                int b3 = ai.b(a, "uaPinType");
                int b4 = ai.b(a, "startTime");
                int b5 = ai.b(a, "endTime");
                int b6 = ai.b(a, "sourceId");
                int b7 = ai.b(a, "sourceTypeValue");
                int b8 = ai.b(a, "movementTypeValue");
                int b9 = ai.b(a, "steps");
                int b10 = ai.b(a, Constants.CALORIES);
                int b11 = ai.b(a, "distance");
                int b12 = ai.b(a, "activeTime");
                int b13 = ai.b(a, "intensityDistInSteps");
                int b14 = ai.b(a, "timeZoneID");
                int i = b3;
                int i2 = b2;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b4;
                    SampleRaw sampleRaw = new SampleRaw(ActivitySampleDao_Impl.this.__dateLongStringConverter.a(a.getString(b4)), ActivitySampleDao_Impl.this.__dateLongStringConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), a.getInt(b12), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(a.getString(b13)), a.getString(b14));
                    sampleRaw.setId(a.getString(b));
                    int i4 = i2;
                    int i5 = b;
                    sampleRaw.setPinType(a.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a.getInt(i6));
                    arrayList.add(sampleRaw);
                    anon4 = this;
                    i = i6;
                    b = i5;
                    i2 = i4;
                    b4 = i3;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ActivitySampleDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfActivitySample = new Anon1(ohVar);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(ohVar);
    }

    @DexIgnore
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    public ActivitySample getActivitySample(String str) {
        rh rhVar;
        ActivitySample activitySample;
        String str2 = str;
        rh b = rh.b("SELECT * FROM activity_sample WHERE id = ? LIMIT 1", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uid");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "startTime");
            int b6 = ai.b(a, "endTime");
            int b7 = ai.b(a, "steps");
            int b8 = ai.b(a, Constants.CALORIES);
            int b9 = ai.b(a, "distance");
            int b10 = ai.b(a, "activeTime");
            int b11 = ai.b(a, "intensityDistInSteps");
            int b12 = ai.b(a, "timeZoneOffsetInSecond");
            int b13 = ai.b(a, "sourceId");
            int b14 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            rhVar = b;
            try {
                int b15 = ai.b(a, "createdAt");
                int i = b2;
                int b16 = ai.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activitySample = new ActivitySample(a.getString(b3), this.__dateShortStringConverter.a(a.getString(b4)), this.__dateTimeISOStringConverter.a(a.getString(b5)), this.__dateTimeISOStringConverter.a(a.getString(b6)), a.getDouble(b7), a.getDouble(b8), a.getDouble(b9), a.getInt(b10), this.__activityIntensitiesConverter.a(a.getString(b11)), a.getInt(b12), a.getString(b13), a.getLong(b14), a.getLong(b15), a.getLong(b16));
                    activitySample.setId(a.getString(i));
                } else {
                    activitySample = null;
                }
                a.close();
                rhVar.c();
                return activitySample;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? ORDER BY startTime ASC", 2);
        String a = this.__dateLongStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"sampleraw"}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM activity_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{ActivitySample.TABLE_NAME}, false, new Anon3(b));
    }

    @DexIgnore
    public void upsertListActivitySample(List<ActivitySample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
