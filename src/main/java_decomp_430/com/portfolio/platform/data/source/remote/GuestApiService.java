package com.portfolio.platform.data.source.remote;

import com.fossil.ku3;
import com.fossil.ny6;
import com.fossil.rx6;
import com.fossil.xe6;
import com.fossil.zy6;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GuestApiService {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getFirmwares$default(GuestApiService guestApiService, String str, String str2, String str3, xe6 xe6, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    str3 = "android";
                }
                return guestApiService.getFirmwares(str, str2, str3, xe6);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getFirmwares");
        }
    }

    @DexIgnore
    @ny6("firmwares")
    Object getFirmwares(@zy6("appVersion") String str, @zy6("deviceModel") String str2, @zy6("os") String str3, xe6<? super rx6<ApiResponse<Firmware>>> xe6);

    @DexIgnore
    @ny6("assets/app-localizations")
    Object getLocalizations(@zy6("metadata.appVersion") String str, @zy6("metadata.platform") String str2, xe6<? super rx6<ApiResponse<ku3>>> xe6);
}
