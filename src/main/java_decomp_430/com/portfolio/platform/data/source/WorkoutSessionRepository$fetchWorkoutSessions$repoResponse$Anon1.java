package com.portfolio.platform.data.source;

import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$1", f = "WorkoutSessionRepository.kt", l = {135}, m = "invokeSuspend")
public final class WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1 extends sf6 implements hg6<xe6<? super rx6<ApiResponse<ServerWorkoutSession>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, xe6 xe6) {
        super(1, xe6);
        this.this$0 = workoutSessionRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1(this.this$0, this.$start, this.$end, this.$offset, this.$limit, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            String e = bk4.e(this.$start);
            wg6.a((Object) e, "DateHelper.formatShortDate(start)");
            String e2 = bk4.e(this.$end);
            wg6.a((Object) e2, "DateHelper.formatShortDate(end)");
            int i2 = this.$offset;
            int i3 = this.$limit;
            this.label = 1;
            obj = access$getMApiService$p.getWorkoutSessions(e, e2, i2, i3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
