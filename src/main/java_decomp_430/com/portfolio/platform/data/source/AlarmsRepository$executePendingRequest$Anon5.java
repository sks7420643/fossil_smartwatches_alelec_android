package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$5", f = "AlarmsRepository.kt", l = {}, m = "invokeSuspend")
public final class AlarmsRepository$executePendingRequest$Anon5 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $deleteAlarmPendingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$executePendingRequest$Anon5(AlarmsRepository alarmsRepository, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = alarmsRepository;
        this.$deleteAlarmPendingList = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AlarmsRepository$executePendingRequest$Anon5 alarmsRepository$executePendingRequest$Anon5 = new AlarmsRepository$executePendingRequest$Anon5(this.this$0, this.$deleteAlarmPendingList, xe6);
        alarmsRepository$executePendingRequest$Anon5.p$ = (il6) obj;
        return alarmsRepository$executePendingRequest$Anon5;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$executePendingRequest$Anon5) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            for (Alarm deleteAlarm : this.$deleteAlarmPendingList) {
                this.this$0.mAlarmsLocalDataSource.deleteAlarm(deleteAlarm);
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
