package com.portfolio.platform.data.source.local.alarm;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.data.model.Explore;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase_Impl extends AlarmDatabase {
    @DexIgnore
    public volatile AlarmDao _alarmDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `alarm` (`id` TEXT, `uri` TEXT NOT NULL, `title` TEXT NOT NULL, `message` TEXT NOT NULL, `hour` INTEGER NOT NULL, `minute` INTEGER NOT NULL, `days` TEXT, `isActive` INTEGER NOT NULL, `isRepeated` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uri`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0b8d5ee1081d415aedfc0c7985d749f7')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `alarm`");
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = AlarmDatabase_Impl.this.mDatabase = iiVar;
            AlarmDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(12);
            hashMap.put("id", new fi.a("id", "TEXT", false, 0, (String) null, 1));
            hashMap.put("uri", new fi.a("uri", "TEXT", true, 1, (String) null, 1));
            hashMap.put(Explore.COLUMN_TITLE, new fi.a(Explore.COLUMN_TITLE, "TEXT", true, 0, (String) null, 1));
            hashMap.put("message", new fi.a("message", "TEXT", true, 0, (String) null, 1));
            hashMap.put("hour", new fi.a("hour", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("minute", new fi.a("minute", "INTEGER", true, 0, (String) null, 1));
            hashMap.put(Alarm.COLUMN_DAYS, new fi.a(Alarm.COLUMN_DAYS, "TEXT", false, 0, (String) null, 1));
            hashMap.put("isActive", new fi.a("isActive", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("isRepeated", new fi.a("isRepeated", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("createdAt", new fi.a("createdAt", "TEXT", false, 0, (String) null, 1));
            hashMap.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi(Alarm.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, Alarm.TABLE_NAME);
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "alarm(com.portfolio.platform.data.source.local.alarm.Alarm).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public AlarmDao alarmDao() {
        AlarmDao alarmDao;
        if (this._alarmDao != null) {
            return this._alarmDao;
        }
        synchronized (this) {
            if (this._alarmDao == null) {
                this._alarmDao = new AlarmDao_Impl(this);
            }
            alarmDao = this._alarmDao;
        }
        return alarmDao;
    }

    @DexIgnore
    public void clearAllTables() {
        AlarmDatabase_Impl.super.assertNotMainThread();
        ii a = AlarmDatabase_Impl.super.getOpenHelper().a();
        try {
            AlarmDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `alarm`");
            AlarmDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            AlarmDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{Alarm.TABLE_NAME});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(6), "0b8d5ee1081d415aedfc0c7985d749f7", "7e35df0dd907bb841b2567561f725af0");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }
}
