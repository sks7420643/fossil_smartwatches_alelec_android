package com.portfolio.platform.data.source;

import com.fossil.ii;
import com.fossil.wg6;
import com.fossil.xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1 extends xh {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(ii iiVar) {
        wg6.b(iiVar, "database");
        iiVar.u();
        iiVar.b("ALTER TABLE SKU ADD COLUMN fastPairId TEXT");
        iiVar.x();
        iiVar.y();
    }
}
