package com.portfolio.platform.data.source.local;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataDatabase_Impl extends CustomizeRealDataDatabase {
    @DexIgnore
    public volatile CustomizeRealDataDao _customizeRealDataDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `customizeRealData` (`id` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6ca914ed7b753a02dc3ff84030dc3e24')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `customizeRealData`");
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = CustomizeRealDataDatabase_Impl.this.mDatabase = iiVar;
            CustomizeRealDataDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put(ServerSetting.VALUE, new fi.a(ServerSetting.VALUE, "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("customizeRealData", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "customizeRealData");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "customizeRealData(com.portfolio.platform.data.model.CustomizeRealData).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        CustomizeRealDataDatabase_Impl.super.assertNotMainThread();
        ii a = CustomizeRealDataDatabase_Impl.super.getOpenHelper().a();
        try {
            CustomizeRealDataDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `customizeRealData`");
            CustomizeRealDataDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            CustomizeRealDataDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"customizeRealData"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "6ca914ed7b753a02dc3ff84030dc3e24", "8e8005f3461a63f640274ec577069e4f");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public CustomizeRealDataDao realDataDao() {
        CustomizeRealDataDao customizeRealDataDao;
        if (this._customizeRealDataDao != null) {
            return this._customizeRealDataDao;
        }
        synchronized (this) {
            if (this._customizeRealDataDao == null) {
                this._customizeRealDataDao = new CustomizeRealDataDao_Impl(this);
            }
            customizeRealDataDao = this._customizeRealDataDao;
        }
        return customizeRealDataDao;
    }
}
