package com.portfolio.platform.data.source;

import com.fossil.oh;
import com.fossil.qg6;
import com.fossil.xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceDatabase extends oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ xh MIGRATION_FROM_1_TO_2; // = new DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(1, 2);
    @DexIgnore
    public static /* final */ xh MIGRATION_FROM_2_TO_3; // = new DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1(2, 3);
    @DexIgnore
    public static /* final */ String TAG; // = "DeviceDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final xh getMIGRATION_FROM_1_TO_2() {
            return DeviceDatabase.MIGRATION_FROM_1_TO_2;
        }

        @DexIgnore
        public final xh getMIGRATION_FROM_2_TO_3() {
            return DeviceDatabase.MIGRATION_FROM_2_TO_3;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public abstract DeviceDao deviceDao();

    @DexIgnore
    public abstract SkuDao skuDao();
}
