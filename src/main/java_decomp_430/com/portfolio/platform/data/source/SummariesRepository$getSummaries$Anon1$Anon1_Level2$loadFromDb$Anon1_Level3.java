package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1.Anon1_Level2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3(SummariesRepository$getSummaries$Anon1.Anon1_Level2 anon1_Level2, xe6 xe6) {
        super(2, xe6);
        this.this$0 = anon1_Level2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3 summariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3 = new SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3(this.this$0, xe6);
        summariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3.p$ = (il6) obj;
        return summariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ActivitySummary activitySummary = this.this$0.this$0.this$0.mActivitySummaryDao.getActivitySummary(this.this$0.this$0.$endDate);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "todayRawSummary " + activitySummary);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
