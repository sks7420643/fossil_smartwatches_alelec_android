package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jm4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "ComplicationRepository";
    @DexIgnore
    public /* final */ ComplicationDao mComplicationDao;
    @DexIgnore
    public /* final */ ComplicationRemoteDataSource mComplicationRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public ComplicationRepository(ComplicationDao complicationDao, ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        wg6.b(complicationDao, "mComplicationDao");
        wg6.b(complicationRemoteDataSource, "mComplicationRemoteDataSource");
        wg6.b(portfolioApp, "mPortfolioApp");
        this.mComplicationDao = complicationDao;
        this.mComplicationRemoteDataSource = complicationRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mComplicationDao.clearAll();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00bc, code lost:
        if ((!((java.util.Collection) r1).isEmpty()) == false) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c7, code lost:
        if (r8.isEmpty() != false) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00c9, code lost:
        r0.mComplicationDao.clearAll();
        r8 = r0.mComplicationDao;
        r9 = r9.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d4, code lost:
        if (r9 == null) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d6, code lost:
        r8.upsertComplicationList((java.util.List) r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00dc, code lost:
        com.fossil.wg6.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00df, code lost:
        throw null;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadAllComplication(String str, xe6<? super cd6> xe6) {
        ComplicationRepository$downloadAllComplication$Anon1 complicationRepository$downloadAllComplication$Anon1;
        int i;
        ComplicationRepository complicationRepository;
        ap4 ap4;
        if (xe6 instanceof ComplicationRepository$downloadAllComplication$Anon1) {
            complicationRepository$downloadAllComplication$Anon1 = (ComplicationRepository$downloadAllComplication$Anon1) xe6;
            int i2 = complicationRepository$downloadAllComplication$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                complicationRepository$downloadAllComplication$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = complicationRepository$downloadAllComplication$Anon1.result;
                Object a = ff6.a();
                i = complicationRepository$downloadAllComplication$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadAllComplication of " + str);
                    ComplicationRemoteDataSource complicationRemoteDataSource = this.mComplicationRemoteDataSource;
                    complicationRepository$downloadAllComplication$Anon1.L$0 = this;
                    complicationRepository$downloadAllComplication$Anon1.L$1 = str;
                    complicationRepository$downloadAllComplication$Anon1.label = 1;
                    obj = complicationRemoteDataSource.getAllComplication(str, complicationRepository$downloadAllComplication$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    complicationRepository = this;
                } else if (i == 1) {
                    str = (String) complicationRepository$downloadAllComplication$Anon1.L$1;
                    complicationRepository = (ComplicationRepository) complicationRepository$downloadAllComplication$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                Integer num = null;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadAllComplication of ");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    sb.append(" response ");
                    sb.append((List) cp4.a());
                    local2.d(TAG, sb.toString());
                    List<Complication> allComplications = complicationRepository.mComplicationDao.getAllComplications();
                    if (!cp4.b()) {
                        Object a2 = cp4.a();
                        if (a2 == null) {
                            wg6.a();
                            throw null;
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadAllComplication of ");
                    sb2.append(str);
                    sb2.append(" fail!!! error=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(TAG, sb2.toString());
                }
                return cd6.a;
            }
        }
        complicationRepository$downloadAllComplication$Anon1 = new ComplicationRepository$downloadAllComplication$Anon1(this, xe6);
        Object obj2 = complicationRepository$downloadAllComplication$Anon1.result;
        Object a3 = ff6.a();
        i = complicationRepository$downloadAllComplication$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        Integer num2 = null;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final List<Complication> getAllComplicationRaw() {
        return this.mComplicationDao.getAllComplications();
    }

    @DexIgnore
    public final List<Complication> getComplicationByIds(List<String> list) {
        wg6.b(list, "ids");
        if (!list.isEmpty()) {
            return yd6.a(this.mComplicationDao.getComplicationByIds(list), new ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1(list));
        }
        return new ArrayList();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final List<Complication> queryComplicationByName(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (Complication next : this.mComplicationDao.getAllComplications()) {
            String normalize = Normalizer.normalize(jm4.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            wg6.a((Object) normalize, "name");
            wg6.a((Object) normalize2, "searchQuery");
            if (yj6.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
