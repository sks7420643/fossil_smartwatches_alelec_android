package com.portfolio.platform.data.source;

import com.fossil.qm4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.manager.FileDownloadManager;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository_Factory implements Factory<FileRepository> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mAppProvider;
    @DexIgnore
    public /* final */ Provider<FileDao> mFileDaoProvider;
    @DexIgnore
    public /* final */ Provider<qm4> mFileDownloadManagerProvider;

    @DexIgnore
    public FileRepository_Factory(Provider<FileDao> provider, Provider<qm4> provider2, Provider<PortfolioApp> provider3) {
        this.mFileDaoProvider = provider;
        this.mFileDownloadManagerProvider = provider2;
        this.mAppProvider = provider3;
    }

    @DexIgnore
    public static FileRepository_Factory create(Provider<FileDao> provider, Provider<qm4> provider2, Provider<PortfolioApp> provider3) {
        return new FileRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static FileRepository newFileRepository(FileDao fileDao, FileDownloadManager fileDownloadManager, PortfolioApp portfolioApp) {
        return new FileRepository(fileDao, fileDownloadManager, portfolioApp);
    }

    @DexIgnore
    public static FileRepository provideInstance(Provider<FileDao> provider, Provider<qm4> provider2, Provider<PortfolioApp> provider3) {
        return new FileRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public FileRepository get() {
        return provideInstance(this.mFileDaoProvider, this.mFileDownloadManagerProvider, this.mAppProvider);
    }
}
