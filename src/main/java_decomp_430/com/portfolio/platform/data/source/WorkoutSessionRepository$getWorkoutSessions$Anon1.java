package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.hh6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository$getWorkoutSessions$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<WorkoutSession>, ApiResponse<ServerWorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $offset;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository$getWorkoutSessions$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(WorkoutSessionRepository$getWorkoutSessions$Anon1 workoutSessionRepository$getWorkoutSessions$Anon1, hh6 hh6, int i, List list) {
            this.this$0 = workoutSessionRepository$getWorkoutSessions$Anon1;
            this.$offset = hh6;
            this.$limit = i;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ApiResponse<ServerWorkoutSession>>> xe6) {
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            String e = bk4.e(this.this$0.$startDate);
            wg6.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = bk4.e(this.this$0.$endDate);
            wg6.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiService$p.getWorkoutSessions(e, e2, this.$offset.element, this.$limit, xe6);
        }

        @DexIgnore
        public LiveData<List<WorkoutSession>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getWorkoutSessions - loadFromDb -- end=" + this.this$0.$end + ", startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
            WorkoutDao access$getMWorkoutDao$p = this.this$0.this$0.mWorkoutDao;
            Date date = this.this$0.$startDate;
            wg6.a((Object) date, "startDate");
            Date date2 = this.this$0.$endDate;
            wg6.a((Object) date2, "endDate");
            return access$getMWorkoutDao$p.getWorkoutSessionsDesc(date, date2);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse) {
            wg6.b(apiResponse, "item");
            Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - processContinueFetching -- hasNext=TRUE");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse) {
            wg6.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getWorkoutSessions - saveCallResult -- item.size=");
            sb.append(apiResponse.get_items().size());
            sb.append(", hasNext=");
            Range range = apiResponse.get_range();
            sb.append(range != null ? Boolean.valueOf(range.isHasNext()) : null);
            local.d(tAG$app_fossilRelease, sb.toString());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease2 = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local2.d(tAG$app_fossilRelease2, "getWorkoutSessions - saveCallResult -- items=" + apiResponse.get_items());
            ArrayList arrayList = new ArrayList();
            for (ServerWorkoutSession workoutSession : apiResponse.get_items()) {
                WorkoutSession workoutSession2 = workoutSession.toWorkoutSession();
                if (workoutSession2 != null) {
                    arrayList.add(workoutSession2);
                }
            }
            if (!apiResponse.get_items().isEmpty()) {
                this.this$0.this$0.mWorkoutDao.upsertListWorkoutSession(arrayList);
            }
            FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - saveCallResult -- DONE!!!");
        }

        @DexIgnore
        public boolean shouldFetch(List<WorkoutSession> list) {
            return this.this$0.$shouldFetch && this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public WorkoutSessionRepository$getWorkoutSessions$Anon1(WorkoutSessionRepository workoutSessionRepository, boolean z, Date date, Date date2, Date date3) {
        this.this$0 = workoutSessionRepository;
        this.$shouldFetch = z;
        this.$end = date;
        this.$startDate = date2;
        this.$endDate = date3;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1$Anon1_Level2] */
    public final LiveData<yx5<List<WorkoutSession>>> apply(List<FitnessDataWrapper> list) {
        hh6 hh6 = new hh6();
        hh6.element = 0;
        return new Anon1_Level2(this, hh6, 100, list).asLiveData();
    }
}
