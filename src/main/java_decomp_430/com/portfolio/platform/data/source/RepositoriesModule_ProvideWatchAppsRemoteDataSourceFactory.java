package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory implements Factory<WatchAppRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceV2Provider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.apiServiceV2Provider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static WatchAppRemoteDataSource provideInstance(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return proxyProvideWatchAppsRemoteDataSource(repositoriesModule, provider.get());
    }

    @DexIgnore
    public static WatchAppRemoteDataSource proxyProvideWatchAppsRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        WatchAppRemoteDataSource provideWatchAppsRemoteDataSource = repositoriesModule.provideWatchAppsRemoteDataSource(apiServiceV2);
        z76.a(provideWatchAppsRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideWatchAppsRemoteDataSource;
    }

    @DexIgnore
    public WatchAppRemoteDataSource get() {
        return provideInstance(this.module, this.apiServiceV2Provider);
    }
}
