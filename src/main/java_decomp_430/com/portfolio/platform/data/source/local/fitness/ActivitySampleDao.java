package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.fossil.qg6;
import com.fossil.sd;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ActivitySampleDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySampleDao.class.getSimpleName();
        wg6.a((Object) simpleName, "ActivitySampleDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateSample(ActivitySample activitySample, ActivitySample activitySample2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSample - currentSample=" + activitySample + ", newSample=" + activitySample2);
        double steps = activitySample2.getSteps() + activitySample.getSteps();
        double calories = activitySample2.getCalories() + activitySample.getCalories();
        double distance = activitySample2.getDistance() + activitySample.getDistance();
        activitySample2.setSteps(steps);
        activitySample2.setCalories(calories);
        activitySample2.setDistance(distance);
        activitySample2.setActiveTime(activitySample2.getActiveTime() + activitySample.getActiveTime());
        activitySample2.setCreatedAt(activitySample.getCreatedAt());
        activitySample2.getIntensityDistInSteps().updateActivityIntensities(activitySample.getIntensityDistInSteps());
        if (activitySample2.getSteps() != activitySample.getSteps()) {
            activitySample2.setUpdatedAt(new Date().getTime());
        }
    }

    @DexIgnore
    public abstract void deleteAllActivitySamples();

    @DexIgnore
    public abstract ActivitySample getActivitySample(String str);

    @DexIgnore
    public final LiveData<List<ActivitySample>> getActivitySamplesLiveData(Date date, Date date2) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        LiveData<List<ActivitySample>> b = sd.b(getActivitySamplesLiveDataV2(date, date2), new ActivitySampleDao$getActivitySamplesLiveData$Anon1(this, date, date2));
        wg6.a((Object) b, "Transformations.switchMa\u2026}\n            }\n        }");
        return b;
    }

    @DexIgnore
    public abstract LiveData<List<SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2);

    @DexIgnore
    public abstract LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2);

    @DexIgnore
    public final void insertActivitySamples(List<ActivitySample> list) {
        wg6.b(list, "activitySamples");
        for (ActivitySample activitySample : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "addActivitySample activity=" + activitySample);
            ActivitySample activitySample2 = getActivitySample(activitySample.getId());
            if (activitySample2 != null) {
                calculateSample(activitySample2, activitySample);
            } else {
                activitySample.setCreatedAt(new Date().getTime());
                activitySample.setUpdatedAt(new Date().getTime());
            }
        }
        upsertListActivitySample(list);
    }

    @DexIgnore
    public abstract void upsertListActivitySample(List<ActivitySample> list);
}
