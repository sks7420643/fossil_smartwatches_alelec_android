package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRepository$getServerSettingList$Anon1 implements ServerSettingDataSource.OnGetServerSettingList {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository this$0;

    @DexIgnore
    public ServerSettingRepository$getServerSettingList$Anon1(ServerSettingRepository serverSettingRepository, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        this.this$0 = serverSettingRepository;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public void onFailed(int i) {
        this.$callback.onFailed(i);
    }

    @DexIgnore
    public void onSuccess(ServerSettingList serverSettingList) {
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this, serverSettingList, (xe6) null), 3, (Object) null);
        this.$callback.onSuccess(serverSettingList);
    }
}
