package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getLastSleepGoal$Anon1 extends tx5<Integer, MFSleepSettings> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$getLastSleepGoal$Anon1(SleepSummariesRepository sleepSummariesRepository) {
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    public Object createCall(xe6<? super rx6<MFSleepSettings>> xe6) {
        return this.this$0.mApiService.getSleepSetting(xe6);
    }

    @DexIgnore
    public LiveData<Integer> loadFromDb() {
        return this.this$0.mSleepDao.getLastSleepGoal();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivitySettings onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(MFSleepSettings mFSleepSettings) {
        wg6.b(mFSleepSettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getActivitySettings saveCallResult goal: " + mFSleepSettings);
        this.this$0.saveSleepSettingToDB$app_fossilRelease(mFSleepSettings.getSleepGoal());
    }
}
