package com.portfolio.platform.data.source.local.quickresponse;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.QuickResponseMessage;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class QuickResponseMessageDao {
    @DexIgnore
    public abstract List<QuickResponseMessage> getAllRawResponse();

    @DexIgnore
    public abstract LiveData<List<QuickResponseMessage>> getAllResponse();

    @DexIgnore
    public abstract void insertResponse(QuickResponseMessage quickResponseMessage);

    @DexIgnore
    public abstract void insertResponses(List<QuickResponseMessage> list);

    @DexIgnore
    public abstract void removeAll();

    @DexIgnore
    public abstract void removeById(int i);

    @DexIgnore
    public abstract void update(QuickResponseMessage quickResponseMessage);

    @DexIgnore
    public abstract void update(List<QuickResponseMessage> list);
}
