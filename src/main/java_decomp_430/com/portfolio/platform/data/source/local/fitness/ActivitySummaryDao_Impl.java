package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.i44;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.u34;
import com.fossil.vh;
import com.fossil.y34;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDao_Impl extends ActivitySummaryDao {
    @DexIgnore
    public /* final */ u34 __activityStatisticConverter; // = new u34();
    @DexIgnore
    public /* final */ y34 __dateTimeConverter; // = new y34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<ActivityRecommendedGoals> __insertionAdapterOfActivityRecommendedGoals;
    @DexIgnore
    public /* final */ hh<ActivitySettings> __insertionAdapterOfActivitySettings;
    @DexIgnore
    public /* final */ hh<ActivityStatistic> __insertionAdapterOfActivityStatistic;
    @DexIgnore
    public /* final */ hh<ActivitySummary> __insertionAdapterOfActivitySummary;
    @DexIgnore
    public /* final */ i44 __integerArrayConverter; // = new i44();
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllActivitySummaries;
    @DexIgnore
    public /* final */ vh __preparedStmtOfUpdateActivitySettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<ActivitySummary> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleday` (`createdAt`,`updatedAt`,`pinType`,`year`,`month`,`day`,`timezoneName`,`dstOffset`,`steps`,`calories`,`distance`,`intensities`,`activeTime`,`stepGoal`,`caloriesGoal`,`activeTimeGoal`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, ActivitySummary activitySummary) {
            miVar.a(1, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activitySummary.getCreatedAt()));
            miVar.a(2, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activitySummary.getUpdatedAt()));
            miVar.a(3, (long) activitySummary.getPinType());
            miVar.a(4, (long) activitySummary.getYear());
            miVar.a(5, (long) activitySummary.getMonth());
            miVar.a(6, (long) activitySummary.getDay());
            if (activitySummary.getTimezoneName() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, activitySummary.getTimezoneName());
            }
            if (activitySummary.getDstOffset() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, (long) activitySummary.getDstOffset().intValue());
            }
            miVar.a(9, activitySummary.getSteps());
            miVar.a(10, activitySummary.getCalories());
            miVar.a(11, activitySummary.getDistance());
            String a = ActivitySummaryDao_Impl.this.__integerArrayConverter.a(activitySummary.getIntensities());
            if (a == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a);
            }
            miVar.a(13, (long) activitySummary.getActiveTime());
            miVar.a(14, (long) activitySummary.getStepGoal());
            miVar.a(15, (long) activitySummary.getCaloriesGoal());
            miVar.a(16, (long) activitySummary.getActiveTimeGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<ActivityStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon10(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public ActivityStatistic call() throws Exception {
            ActivityStatistic activityStatistic;
            Cursor a = bi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "uid");
                int b3 = ai.b(a, "activeTimeBestDay");
                int b4 = ai.b(a, "activeTimeBestStreak");
                int b5 = ai.b(a, "caloriesBestDay");
                int b6 = ai.b(a, "caloriesBestStreak");
                int b7 = ai.b(a, "stepsBestDay");
                int b8 = ai.b(a, "stepsBestStreak");
                int b9 = ai.b(a, "totalActiveTime");
                int b10 = ai.b(a, "totalCalories");
                int b11 = ai.b(a, "totalDays");
                int b12 = ai.b(a, "totalDistance");
                int b13 = ai.b(a, "totalSteps");
                int b14 = ai.b(a, "totalIntensityDistInStep");
                int b15 = ai.b(a, "createdAt");
                int b16 = ai.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(a.getString(b), a.getString(b2), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b3)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b4)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.b(a.getString(b5)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b6)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b7)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b8)), a.getInt(b9), a.getDouble(b10), a.getInt(b11), a.getDouble(b12), a.getInt(b13), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b14)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b15)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b16)));
                } else {
                    activityStatistic = null;
                }
                return activityStatistic;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<ActivitySettings> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activitySettings` (`id`,`currentStepGoal`,`currentCaloriesGoal`,`currentActiveTimeGoal`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, ActivitySettings activitySettings) {
            miVar.a(1, (long) activitySettings.getId());
            miVar.a(2, (long) activitySettings.getCurrentStepGoal());
            miVar.a(3, (long) activitySettings.getCurrentCaloriesGoal());
            miVar.a(4, (long) activitySettings.getCurrentActiveTimeGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends hh<ActivityRecommendedGoals> {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activityRecommendedGoals` (`id`,`recommendedStepsGoal`,`recommendedCaloriesGoal`,`recommendedActiveTimeGoal`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, ActivityRecommendedGoals activityRecommendedGoals) {
            miVar.a(1, (long) activityRecommendedGoals.getId());
            miVar.a(2, (long) activityRecommendedGoals.getRecommendedStepsGoal());
            miVar.a(3, (long) activityRecommendedGoals.getRecommendedCaloriesGoal());
            miVar.a(4, (long) activityRecommendedGoals.getRecommendedActiveTimeGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends hh<ActivityStatistic> {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_statistic` (`id`,`uid`,`activeTimeBestDay`,`activeTimeBestStreak`,`caloriesBestDay`,`caloriesBestStreak`,`stepsBestDay`,`stepsBestStreak`,`totalActiveTime`,`totalCalories`,`totalDays`,`totalDistance`,`totalSteps`,`totalIntensityDistInStep`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, ActivityStatistic activityStatistic) {
            if (activityStatistic.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, activityStatistic.getId());
            }
            if (activityStatistic.getUid() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, activityStatistic.getUid());
            }
            String a = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestDay());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            String a2 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestStreak());
            if (a2 == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a2);
            }
            String a3 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestDay());
            if (a3 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a3);
            }
            String a4 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestStreak());
            if (a4 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a4);
            }
            String a5 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestDay());
            if (a5 == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a5);
            }
            String a6 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestStreak());
            if (a6 == null) {
                miVar.a(8);
            } else {
                miVar.a(8, a6);
            }
            miVar.a(9, (long) activityStatistic.getTotalActiveTime());
            miVar.a(10, activityStatistic.getTotalCalories());
            miVar.a(11, (long) activityStatistic.getTotalDays());
            miVar.a(12, activityStatistic.getTotalDistance());
            miVar.a(13, (long) activityStatistic.getTotalSteps());
            String a7 = ActivitySummaryDao_Impl.this.__integerArrayConverter.a(activityStatistic.getTotalIntensityDistInStep());
            if (a7 == null) {
                miVar.a(14);
            } else {
                miVar.a(14, a7);
            }
            miVar.a(15, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activityStatistic.getCreatedAt()));
            miVar.a(16, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activityStatistic.getUpdatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sampleday";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends vh {
        @DexIgnore
        public Anon6(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE activitySettings SET currentStepGoal = ?, currentCaloriesGoal = ?, currentActiveTimeGoal = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<ActivitySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon7(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public ActivitySummary call() throws Exception {
            ActivitySummary activitySummary;
            Integer num;
            Cursor a = bi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "createdAt");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, "pinType");
                int b4 = ai.b(a, "year");
                int b5 = ai.b(a, "month");
                int b6 = ai.b(a, "day");
                int b7 = ai.b(a, "timezoneName");
                int b8 = ai.b(a, "dstOffset");
                int b9 = ai.b(a, "steps");
                int b10 = ai.b(a, Constants.CALORIES);
                int b11 = ai.b(a, "distance");
                int b12 = ai.b(a, "intensities");
                int b13 = ai.b(a, "activeTime");
                int b14 = ai.b(a, "stepGoal");
                int i = b3;
                int b15 = ai.b(a, "caloriesGoal");
                int i2 = b2;
                int b16 = ai.b(a, "activeTimeGoal");
                if (a.moveToFirst()) {
                    int i3 = a.getInt(b4);
                    int i4 = a.getInt(b5);
                    int i5 = a.getInt(b6);
                    String string = a.getString(b7);
                    if (a.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b8));
                    }
                    ActivitySummary activitySummary2 = new ActivitySummary(i3, i4, i5, string, num, a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b12)), a.getInt(b13), a.getInt(b14), a.getInt(b15), a.getInt(b16));
                    activitySummary2.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b)));
                    activitySummary2.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i2)));
                    activitySummary2.setPinType(a.getInt(i));
                    activitySummary = activitySummary2;
                } else {
                    activitySummary = null;
                }
                return activitySummary;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon8(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<ActivitySummary> call() throws Exception {
            Integer num;
            Cursor a = bi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "createdAt");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, "pinType");
                int b4 = ai.b(a, "year");
                int b5 = ai.b(a, "month");
                int b6 = ai.b(a, "day");
                int b7 = ai.b(a, "timezoneName");
                int b8 = ai.b(a, "dstOffset");
                int b9 = ai.b(a, "steps");
                int b10 = ai.b(a, Constants.CALORIES);
                int b11 = ai.b(a, "distance");
                int b12 = ai.b(a, "intensities");
                int b13 = ai.b(a, "activeTime");
                int b14 = ai.b(a, "stepGoal");
                int i = b3;
                int b15 = ai.b(a, "caloriesGoal");
                int i2 = b2;
                int i3 = b;
                int b16 = ai.b(a, "activeTimeGoal");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i4 = a.getInt(b4);
                    int i5 = a.getInt(b5);
                    int i6 = a.getInt(b6);
                    String string = a.getString(b7);
                    if (a.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b8));
                    }
                    int i7 = b4;
                    int i8 = b16;
                    ActivitySummary activitySummary = new ActivitySummary(i4, i5, i6, string, num, a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b12)), a.getInt(b13), a.getInt(b14), a.getInt(b15), a.getInt(i8));
                    int i9 = b14;
                    int i10 = i8;
                    int i11 = i3;
                    int i12 = b15;
                    int i13 = i11;
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i11)));
                    int i14 = i2;
                    i2 = i14;
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i14)));
                    int i15 = i;
                    activitySummary.setPinType(a.getInt(i15));
                    arrayList.add(activitySummary);
                    i = i15;
                    b15 = i12;
                    b14 = i9;
                    b16 = i10;
                    i3 = i13;
                    b4 = i7;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<ActivitySettings> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon9(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public ActivitySettings call() throws Exception {
            ActivitySettings activitySettings = null;
            Cursor a = bi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "currentStepGoal");
                int b3 = ai.b(a, "currentCaloriesGoal");
                int b4 = ai.b(a, "currentActiveTimeGoal");
                if (a.moveToFirst()) {
                    ActivitySettings activitySettings2 = new ActivitySettings(a.getInt(b2), a.getInt(b3), a.getInt(b4));
                    activitySettings2.setId(a.getInt(b));
                    activitySettings = activitySettings2;
                }
                return activitySettings;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ActivitySummaryDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfActivitySummary = new Anon1(ohVar);
        this.__insertionAdapterOfActivitySettings = new Anon2(ohVar);
        this.__insertionAdapterOfActivityRecommendedGoals = new Anon3(ohVar);
        this.__insertionAdapterOfActivityStatistic = new Anon4(ohVar);
        this.__preparedStmtOfDeleteAllActivitySummaries = new Anon5(ohVar);
        this.__preparedStmtOfUpdateActivitySettings = new Anon6(ohVar);
    }

    @DexIgnore
    public void deleteAllActivitySummaries() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllActivitySummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySummaries.release(acquire);
        }
    }

    @DexIgnore
    public ActivitySettings getActivitySetting() {
        rh b = rh.b("SELECT * FROM activitySettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        ActivitySettings activitySettings = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "currentStepGoal");
            int b4 = ai.b(a, "currentCaloriesGoal");
            int b5 = ai.b(a, "currentActiveTimeGoal");
            if (a.moveToFirst()) {
                ActivitySettings activitySettings2 = new ActivitySettings(a.getInt(b3), a.getInt(b4), a.getInt(b5));
                activitySettings2.setId(a.getInt(b2));
                activitySettings = activitySettings2;
            }
            return activitySettings;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<ActivitySettings> getActivitySettingLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"activitySettings"}, false, new Anon9(rh.b("SELECT * FROM activitySettings LIMIT 1", 0)));
    }

    @DexIgnore
    public ActivityStatistic getActivityStatistic() {
        rh rhVar;
        ActivityStatistic activityStatistic;
        rh b = rh.b("SELECT * FROM activity_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uid");
            int b4 = ai.b(a, "activeTimeBestDay");
            int b5 = ai.b(a, "activeTimeBestStreak");
            int b6 = ai.b(a, "caloriesBestDay");
            int b7 = ai.b(a, "caloriesBestStreak");
            int b8 = ai.b(a, "stepsBestDay");
            int b9 = ai.b(a, "stepsBestStreak");
            int b10 = ai.b(a, "totalActiveTime");
            int b11 = ai.b(a, "totalCalories");
            int b12 = ai.b(a, "totalDays");
            int b13 = ai.b(a, "totalDistance");
            int b14 = ai.b(a, "totalSteps");
            rhVar = b;
            try {
                int b15 = ai.b(a, "totalIntensityDistInStep");
                int b16 = ai.b(a, "createdAt");
                int b17 = ai.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(a.getString(b2), a.getString(b3), this.__activityStatisticConverter.a(a.getString(b4)), this.__activityStatisticConverter.a(a.getString(b5)), this.__activityStatisticConverter.b(a.getString(b6)), this.__activityStatisticConverter.a(a.getString(b7)), this.__activityStatisticConverter.a(a.getString(b8)), this.__activityStatisticConverter.a(a.getString(b9)), a.getInt(b10), a.getDouble(b11), a.getInt(b12), a.getDouble(b13), a.getInt(b14), this.__integerArrayConverter.a(a.getString(b15)), this.__dateTimeConverter.a(a.getLong(b16)), this.__dateTimeConverter.a(a.getLong(b17)));
                } else {
                    activityStatistic = null;
                }
                a.close();
                rhVar.c();
                return activityStatistic;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<ActivityStatistic> getActivityStatisticLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{ActivityStatistic.TABLE_NAME}, false, new Anon10(rh.b("SELECT * FROM activity_statistic LIMIT 1", 0)));
    }

    @DexIgnore
    public List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6) {
        rh rhVar;
        Integer num;
        rh b = rh.b("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year DESC, month DESC, day DESC\n    ", 12);
        long j = (long) i3;
        b.a(1, j);
        b.a(2, j);
        long j2 = (long) i2;
        b.a(3, j2);
        b.a(4, j);
        b.a(5, j2);
        b.a(6, (long) i);
        long j3 = (long) i6;
        b.a(7, j3);
        b.a(8, j3);
        long j4 = (long) i5;
        b.a(9, j4);
        b.a(10, j3);
        b.a(11, j4);
        b.a(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "year");
            int b6 = ai.b(a, "month");
            int b7 = ai.b(a, "day");
            int b8 = ai.b(a, "timezoneName");
            int b9 = ai.b(a, "dstOffset");
            int b10 = ai.b(a, "steps");
            int b11 = ai.b(a, Constants.CALORIES);
            int b12 = ai.b(a, "distance");
            int b13 = ai.b(a, "intensities");
            int b14 = ai.b(a, "activeTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "stepGoal");
                int i7 = b4;
                int b16 = ai.b(a, "caloriesGoal");
                int i8 = b3;
                int i9 = b2;
                int b17 = ai.b(a, "activeTimeGoal");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i10 = a.getInt(b5);
                    int i11 = a.getInt(b6);
                    int i12 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    int i13 = b5;
                    int i14 = b17;
                    ActivitySummary activitySummary = new ActivitySummary(i10, i11, i12, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(i14));
                    int i15 = b15;
                    int i16 = i14;
                    int i17 = i9;
                    int i18 = b16;
                    int i19 = i17;
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(i17)));
                    int i20 = i8;
                    i8 = i20;
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i20)));
                    int i21 = i7;
                    activitySummary.setPinType(a.getInt(i21));
                    arrayList.add(activitySummary);
                    b16 = i18;
                    b5 = i13;
                    i7 = i21;
                    i9 = i19;
                    b15 = i15;
                    b17 = i16;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6) {
        rh b = rh.b("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year ASC, month ASC, day ASC\n    ", 12);
        long j = (long) i3;
        b.a(1, j);
        b.a(2, j);
        long j2 = (long) i2;
        b.a(3, j2);
        b.a(4, j);
        b.a(5, j2);
        b.a(6, (long) i);
        long j3 = (long) i6;
        b.a(7, j3);
        b.a(8, j3);
        long j4 = (long) i5;
        b.a(9, j4);
        b.a(10, j3);
        b.a(11, j4);
        b.a(12, (long) i4);
        return this.__db.getInvalidationTracker().a(new String[]{"sampleday"}, false, new Anon8(b));
    }

    @DexIgnore
    public ActivitySummary getActivitySummary(int i, int i2, int i3) {
        rh rhVar;
        ActivitySummary activitySummary;
        Integer num;
        rh b = rh.b("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ? LIMIT 1", 3);
        b.a(1, (long) i);
        b.a(2, (long) i2);
        b.a(3, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "year");
            int b6 = ai.b(a, "month");
            int b7 = ai.b(a, "day");
            int b8 = ai.b(a, "timezoneName");
            int b9 = ai.b(a, "dstOffset");
            int b10 = ai.b(a, "steps");
            int b11 = ai.b(a, Constants.CALORIES);
            int b12 = ai.b(a, "distance");
            int b13 = ai.b(a, "intensities");
            int b14 = ai.b(a, "activeTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "stepGoal");
                int i4 = b4;
                int b16 = ai.b(a, "caloriesGoal");
                int i5 = b3;
                int b17 = ai.b(a, "activeTimeGoal");
                if (a.moveToFirst()) {
                    int i6 = a.getInt(b5);
                    int i7 = a.getInt(b6);
                    int i8 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    activitySummary = new ActivitySummary(i6, i7, i8, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i5)));
                    activitySummary.setPinType(a.getInt(i4));
                } else {
                    activitySummary = null;
                }
                a.close();
                rhVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3) {
        rh b = rh.b("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ?", 3);
        b.a(1, (long) i);
        b.a(2, (long) i2);
        b.a(3, (long) i3);
        return this.__db.getInvalidationTracker().a(new String[]{"sampleday"}, false, new Anon7(b));
    }

    @DexIgnore
    public ActivitySummary getLastSummary() {
        rh rhVar;
        ActivitySummary activitySummary;
        Integer num;
        rh b = rh.b("SELECT * FROM sampleday ORDER BY year ASC, month ASC, day ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "year");
            int b6 = ai.b(a, "month");
            int b7 = ai.b(a, "day");
            int b8 = ai.b(a, "timezoneName");
            int b9 = ai.b(a, "dstOffset");
            int b10 = ai.b(a, "steps");
            int b11 = ai.b(a, Constants.CALORIES);
            int b12 = ai.b(a, "distance");
            int b13 = ai.b(a, "intensities");
            int b14 = ai.b(a, "activeTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "stepGoal");
                int i = b4;
                int b16 = ai.b(a, "caloriesGoal");
                int i2 = b3;
                int b17 = ai.b(a, "activeTimeGoal");
                if (a.moveToFirst()) {
                    int i3 = a.getInt(b5);
                    int i4 = a.getInt(b6);
                    int i5 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    ActivitySummary activitySummary2 = new ActivitySummary(i3, i4, i5, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary2.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary2.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i2)));
                    activitySummary2.setPinType(a.getInt(i));
                    activitySummary = activitySummary2;
                } else {
                    activitySummary = null;
                }
                a.close();
                rhVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3) {
        rh rhVar;
        ActivitySummary activitySummary;
        Integer num;
        rh b = rh.b("SELECT * FROM sampleday\n        WHERE year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?) ORDER BY year DESC, month DESC, day DESC LIMIT 1", 6);
        long j = (long) i;
        b.a(1, j);
        b.a(2, j);
        long j2 = (long) i2;
        b.a(3, j2);
        b.a(4, j);
        b.a(5, j2);
        b.a(6, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "year");
            int b6 = ai.b(a, "month");
            int b7 = ai.b(a, "day");
            int b8 = ai.b(a, "timezoneName");
            int b9 = ai.b(a, "dstOffset");
            int b10 = ai.b(a, "steps");
            int b11 = ai.b(a, Constants.CALORIES);
            int b12 = ai.b(a, "distance");
            int b13 = ai.b(a, "intensities");
            int b14 = ai.b(a, "activeTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "stepGoal");
                int i4 = b4;
                int b16 = ai.b(a, "caloriesGoal");
                int i5 = b3;
                int b17 = ai.b(a, "activeTimeGoal");
                if (a.moveToFirst()) {
                    int i6 = a.getInt(b5);
                    int i7 = a.getInt(b6);
                    int i8 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    activitySummary = new ActivitySummary(i6, i7, i8, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(i5)));
                    activitySummary.setPinType(a.getInt(i4));
                } else {
                    activitySummary = null;
                }
                a.close();
                rhVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6) {
        rh b = rh.b("SELECT SUM(steps) as totalStepsOfWeek,  SUM(calories) as totalCaloriesOfWeek,  SUM(activeTime) as totalActiveTimeOfWeek FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ", 12);
        long j = (long) i3;
        b.a(1, j);
        b.a(2, j);
        long j2 = (long) i2;
        b.a(3, j2);
        b.a(4, j);
        b.a(5, j2);
        b.a(6, (long) i);
        long j3 = (long) i6;
        b.a(7, j3);
        b.a(8, j3);
        long j4 = (long) i5;
        b.a(9, j4);
        b.a(10, j3);
        b.a(11, j4);
        b.a(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "totalStepsOfWeek");
            int b3 = ai.b(a, "totalCaloriesOfWeek");
            int b4 = ai.b(a, "totalActiveTimeOfWeek");
            if (a.moveToFirst()) {
                totalValuesOfWeek = new ActivitySummary.TotalValuesOfWeek(a.getDouble(b2), a.getDouble(b3), a.getInt(b4));
            }
            return totalValuesOfWeek;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public long insertActivitySettings(ActivitySettings activitySettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivitySettings.insertAndReturnId(activitySettings);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateActivitySettings(int i, int i2, int i3) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfUpdateActivitySettings.acquire();
        acquire.a(1, (long) i);
        acquire.a(2, (long) i2);
        acquire.a(3, (long) i3);
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateActivitySettings.release(acquire);
        }
    }

    @DexIgnore
    public void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivityRecommendedGoals.insert(activityRecommendedGoals);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public long upsertActivityStatistic(ActivityStatistic activityStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivityStatistic.insertAndReturnId(activityStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertActivitySummaries(List<ActivitySummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertActivitySummary(ActivitySummary activitySummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(activitySummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
