package com.portfolio.platform.data.source.local.sleep;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.ei;
import com.fossil.hh;
import com.fossil.i44;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.p4;
import com.fossil.p44;
import com.fossil.q44;
import com.fossil.r44;
import com.fossil.rh;
import com.fossil.vh;
import com.fossil.x34;
import com.fossil.y34;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDao_Impl extends SleepDao {
    @DexIgnore
    public /* final */ x34 __dateShortStringConverter; // = new x34();
    @DexIgnore
    public /* final */ y34 __dateTimeConverter; // = new y34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<MFSleepDay> __insertionAdapterOfMFSleepDay;
    @DexIgnore
    public /* final */ hh<MFSleepSession> __insertionAdapterOfMFSleepSession;
    @DexIgnore
    public /* final */ hh<MFSleepSession> __insertionAdapterOfMFSleepSession_1;
    @DexIgnore
    public /* final */ hh<MFSleepSettings> __insertionAdapterOfMFSleepSettings;
    @DexIgnore
    public /* final */ hh<SleepRecommendedGoal> __insertionAdapterOfSleepRecommendedGoal;
    @DexIgnore
    public /* final */ hh<SleepStatistic> __insertionAdapterOfSleepStatistic;
    @DexIgnore
    public /* final */ i44 __integerArrayConverter; // = new i44();
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllSleepDays;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllSleepSessions;
    @DexIgnore
    public /* final */ vh __preparedStmtOfUpdateSleepSettings;
    @DexIgnore
    public /* final */ p44 __sleepDistributionConverter; // = new p44();
    @DexIgnore
    public /* final */ q44 __sleepSessionHeartRateConverter; // = new q44();
    @DexIgnore
    public /* final */ r44 __sleepStatisticConverter; // = new r44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<MFSleepSession> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `sleep_session` (`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MFSleepSession mFSleepSession) {
            miVar.a(1, (long) mFSleepSession.getPinType());
            miVar.a(2, mFSleepSession.getDate());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            miVar.a(7, mFSleepSession.getNormalizedSleepQuality());
            miVar.a(8, (long) mFSleepSession.getSource());
            miVar.a(9, (long) mFSleepSession.getRealStartTime());
            miVar.a(10, (long) mFSleepSession.getRealEndTime());
            miVar.a(11, (long) mFSleepSession.getRealSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getRealSleepStateDistInMinute());
            if (a2 == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a2);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                miVar.a(13);
            } else {
                miVar.a(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                miVar.a(14);
            } else {
                miVar.a(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                miVar.a(15);
            } else {
                miVar.a(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getEditedSleepStateDistInMinute());
            if (a3 == null) {
                miVar.a(16);
            } else {
                miVar.a(16, a3);
            }
            if (mFSleepSession.getSleepStates() == null) {
                miVar.a(17);
            } else {
                miVar.a(17, mFSleepSession.getSleepStates());
            }
            String a4 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(mFSleepSession.getHeartRate());
            if (a4 == null) {
                miVar.a(18);
            } else {
                miVar.a(18, a4);
            }
            miVar.a(19, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getCreatedAt()));
            miVar.a(20, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getUpdatedAt()));
            miVar.a(21, (long) mFSleepSession.getTimezoneOffset());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<MFSleepSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon10(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<MFSleepSession> call() throws Exception {
            Integer num;
            Integer num2;
            Integer num3;
            int i;
            Integer num4;
            int i2;
            Integer num5;
            int i3;
            Anon10 anon10 = this;
            Cursor a = bi.a(SleepDao_Impl.this.__db, anon10.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(a, "day");
                int b4 = ai.b(a, "deviceSerialNumber");
                int b5 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
                int b6 = ai.b(a, "bookmarkTime");
                int b7 = ai.b(a, "normalizedSleepQuality");
                int b8 = ai.b(a, "source");
                int b9 = ai.b(a, "realStartTime");
                int b10 = ai.b(a, "realEndTime");
                int b11 = ai.b(a, "realSleepMinutes");
                int b12 = ai.b(a, "realSleepStateDistInMinute");
                int b13 = ai.b(a, "editedStartTime");
                int b14 = ai.b(a, "editedEndTime");
                int i4 = b;
                int b15 = ai.b(a, "editedSleepMinutes");
                int b16 = ai.b(a, "editedSleepStateDistInMinute");
                int b17 = ai.b(a, "sleepStates");
                int b18 = ai.b(a, "heartRate");
                int b19 = ai.b(a, "createdAt");
                int b20 = ai.b(a, "updatedAt");
                int b21 = ai.b(a, "timezoneOffset");
                int i5 = b14;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b2);
                    int i6 = b2;
                    Date a2 = SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3));
                    String string = a.getString(b4);
                    if (a.isNull(b5)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b5));
                    }
                    if (a.isNull(b6)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b6));
                    }
                    double d = a.getDouble(b7);
                    int i7 = a.getInt(b8);
                    int i8 = a.getInt(b9);
                    int i9 = a.getInt(b10);
                    int i10 = a.getInt(b11);
                    SleepDistribution a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b12));
                    if (a.isNull(b13)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b13));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b15;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b15;
                    }
                    if (a.isNull(i2)) {
                        i5 = i;
                        b15 = i2;
                        i3 = b16;
                        num5 = null;
                    } else {
                        i5 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i3 = b16;
                        b15 = i2;
                    }
                    b16 = i3;
                    SleepDistribution a4 = SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(i3));
                    int i11 = b17;
                    String string2 = a.getString(i11);
                    b17 = i11;
                    int i12 = b18;
                    b18 = i12;
                    SleepSessionHeartRate a5 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(a.getString(i12));
                    int i13 = b19;
                    int i14 = b3;
                    int i15 = i13;
                    int i16 = b20;
                    b20 = i16;
                    int i17 = b21;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, num5, a4, string2, a5, SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(i13)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(i16)), a.getInt(i17));
                    int i18 = i4;
                    mFSleepSession.setPinType(a.getInt(i18));
                    arrayList.add(mFSleepSession);
                    anon10 = this;
                    b21 = i17;
                    i4 = i18;
                    b3 = i14;
                    b2 = i6;
                    b19 = i15;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<MFSleepDay> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon11(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public MFSleepDay call() throws Exception {
            MFSleepDay mFSleepDay = null;
            Cursor a = bi.a(SleepDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, "timezoneOffset");
                int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b4 = ai.b(a, "goalMinutes");
                int b5 = ai.b(a, "sleepMinutes");
                int b6 = ai.b(a, "sleepStateDistInMinute");
                int b7 = ai.b(a, "createdAt");
                int b8 = ai.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b6)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b7)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b8)));
                    mFSleepDay.setPinType(a.getInt(b));
                    mFSleepDay.setTimezoneOffset(a.getInt(b2));
                }
                return mFSleepDay;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon12(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<MFSleepDay> call() throws Exception {
            Cursor a = bi.a(SleepDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, "timezoneOffset");
                int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b4 = ai.b(a, "goalMinutes");
                int b5 = ai.b(a, "sleepMinutes");
                int b6 = ai.b(a, "sleepStateDistInMinute");
                int b7 = ai.b(a, "createdAt");
                int b8 = ai.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    MFSleepDay mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b6)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b7)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b8)));
                    mFSleepDay.setPinType(a.getInt(b));
                    mFSleepDay.setTimezoneOffset(a.getInt(b2));
                    arrayList.add(mFSleepDay);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon13(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public Integer call() throws Exception {
            Integer num = null;
            Cursor a = bi.a(SleepDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                if (a.moveToFirst()) {
                    if (!a.isNull(0)) {
                        num = Integer.valueOf(a.getInt(0));
                    }
                }
                return num;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<SleepRecommendedGoal> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon14(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public SleepRecommendedGoal call() throws Exception {
            SleepRecommendedGoal sleepRecommendedGoal = null;
            Cursor a = bi.a(SleepDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "recommendedSleepGoal");
                if (a.moveToFirst()) {
                    SleepRecommendedGoal sleepRecommendedGoal2 = new SleepRecommendedGoal(a.getInt(b2));
                    sleepRecommendedGoal2.setId(a.getInt(b));
                    sleepRecommendedGoal = sleepRecommendedGoal2;
                }
                return sleepRecommendedGoal;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon15 implements Callable<SleepStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon15(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public SleepStatistic call() throws Exception {
            SleepStatistic sleepStatistic;
            Cursor a = bi.a(SleepDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "uid");
                int b3 = ai.b(a, "sleepTimeBestDay");
                int b4 = ai.b(a, "sleepTimeBestStreak");
                int b5 = ai.b(a, "totalDays");
                int b6 = ai.b(a, "totalSleeps");
                int b7 = ai.b(a, "totalSleepMinutes");
                int b8 = ai.b(a, "totalSleepStateDistInMinute");
                int b9 = ai.b(a, "createdAt");
                int b10 = ai.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    sleepStatistic = new SleepStatistic(a.getString(b), a.getString(b2), SleepDao_Impl.this.__sleepStatisticConverter.a(a.getString(b3)), SleepDao_Impl.this.__sleepStatisticConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), a.getInt(b7), SleepDao_Impl.this.__integerArrayConverter.a(a.getString(b8)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b9)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b10)));
                } else {
                    sleepStatistic = null;
                }
                return sleepStatistic;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<MFSleepSession> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_session` (`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MFSleepSession mFSleepSession) {
            miVar.a(1, (long) mFSleepSession.getPinType());
            miVar.a(2, mFSleepSession.getDate());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            miVar.a(7, mFSleepSession.getNormalizedSleepQuality());
            miVar.a(8, (long) mFSleepSession.getSource());
            miVar.a(9, (long) mFSleepSession.getRealStartTime());
            miVar.a(10, (long) mFSleepSession.getRealEndTime());
            miVar.a(11, (long) mFSleepSession.getRealSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getRealSleepStateDistInMinute());
            if (a2 == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a2);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                miVar.a(13);
            } else {
                miVar.a(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                miVar.a(14);
            } else {
                miVar.a(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                miVar.a(15);
            } else {
                miVar.a(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getEditedSleepStateDistInMinute());
            if (a3 == null) {
                miVar.a(16);
            } else {
                miVar.a(16, a3);
            }
            if (mFSleepSession.getSleepStates() == null) {
                miVar.a(17);
            } else {
                miVar.a(17, mFSleepSession.getSleepStates());
            }
            String a4 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(mFSleepSession.getHeartRate());
            if (a4 == null) {
                miVar.a(18);
            } else {
                miVar.a(18, a4);
            }
            miVar.a(19, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getCreatedAt()));
            miVar.a(20, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getUpdatedAt()));
            miVar.a(21, (long) mFSleepSession.getTimezoneOffset());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends hh<MFSleepDay> {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_date` (`pinType`,`timezoneOffset`,`date`,`goalMinutes`,`sleepMinutes`,`sleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MFSleepDay mFSleepDay) {
            miVar.a(1, (long) mFSleepDay.getPinType());
            miVar.a(2, (long) mFSleepDay.getTimezoneOffset());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepDay.getDate());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            miVar.a(4, (long) mFSleepDay.getGoalMinutes());
            miVar.a(5, (long) mFSleepDay.getSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepDay.getSleepStateDistInMinute());
            if (a2 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a2);
            }
            miVar.a(7, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepDay.getCreatedAt()));
            miVar.a(8, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepDay.getUpdatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends hh<MFSleepSettings> {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR FAIL INTO `sleep_settings` (`id`,`sleepGoal`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MFSleepSettings mFSleepSettings) {
            miVar.a(1, (long) mFSleepSettings.getId());
            miVar.a(2, (long) mFSleepSettings.getSleepGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends hh<SleepRecommendedGoal> {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleepRecommendedGoals` (`id`,`recommendedSleepGoal`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, SleepRecommendedGoal sleepRecommendedGoal) {
            miVar.a(1, (long) sleepRecommendedGoal.getId());
            miVar.a(2, (long) sleepRecommendedGoal.getRecommendedSleepGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends hh<SleepStatistic> {
        @DexIgnore
        public Anon6(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_statistic` (`id`,`uid`,`sleepTimeBestDay`,`sleepTimeBestStreak`,`totalDays`,`totalSleeps`,`totalSleepMinutes`,`totalSleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, SleepStatistic sleepStatistic) {
            if (sleepStatistic.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, sleepStatistic.getId());
            }
            if (sleepStatistic.getUid() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, sleepStatistic.getUid());
            }
            String a = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestDay());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            String a2 = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestStreak());
            if (a2 == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a2);
            }
            miVar.a(5, (long) sleepStatistic.getTotalDays());
            miVar.a(6, (long) sleepStatistic.getTotalSleeps());
            miVar.a(7, (long) sleepStatistic.getTotalSleepMinutes());
            String a3 = SleepDao_Impl.this.__integerArrayConverter.a(sleepStatistic.getTotalSleepStateDistInMinute());
            if (a3 == null) {
                miVar.a(8);
            } else {
                miVar.a(8, a3);
            }
            miVar.a(9, SleepDao_Impl.this.__dateTimeConverter.a(sleepStatistic.getCreatedAt()));
            miVar.a(10, SleepDao_Impl.this.__dateTimeConverter.a(sleepStatistic.getUpdatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends vh {
        @DexIgnore
        public Anon7(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sleep_session";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends vh {
        @DexIgnore
        public Anon8(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sleep_date";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends vh {
        @DexIgnore
        public Anon9(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE sleep_settings SET sleepGoal = ?";
        }
    }

    @DexIgnore
    public SleepDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfMFSleepSession = new Anon1(ohVar);
        this.__insertionAdapterOfMFSleepSession_1 = new Anon2(ohVar);
        this.__insertionAdapterOfMFSleepDay = new Anon3(ohVar);
        this.__insertionAdapterOfMFSleepSettings = new Anon4(ohVar);
        this.__insertionAdapterOfSleepRecommendedGoal = new Anon5(ohVar);
        this.__insertionAdapterOfSleepStatistic = new Anon6(ohVar);
        this.__preparedStmtOfDeleteAllSleepSessions = new Anon7(ohVar);
        this.__preparedStmtOfDeleteAllSleepDays = new Anon8(ohVar);
        this.__preparedStmtOfUpdateSleepSettings = new Anon9(ohVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:101:0x025c A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0263 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x026f A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0276 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0289 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0293 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x02a8 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x02af A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02c2 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x02c5 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x02d9 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0186 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0199 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x019e A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01a5 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01a8 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01b0 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01b3 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01bb A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01be A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01c6 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01c9 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01d1 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01d7 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01ef A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x020d A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x022b A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0242 A[Catch:{ all -> 0x030f }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0249 A[Catch:{ all -> 0x030f }] */
    private void __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(p4<String, ArrayList<MFSleepSession>> p4Var) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        long j;
        Date date;
        String str;
        Integer num;
        Integer num2;
        double d;
        int i7;
        int i8;
        int i9;
        int i10;
        SleepDistribution sleepDistribution;
        int i11;
        int i12;
        Integer num3;
        int i13;
        Integer num4;
        int i14;
        Integer num5;
        int i15;
        SleepDistribution sleepDistribution2;
        int i16;
        String str2;
        int i17;
        SleepSessionHeartRate sleepSessionHeartRate;
        int i18;
        DateTime dateTime;
        int i19;
        DateTime dateTime2;
        int i20;
        int i21;
        int i22;
        p4<String, ArrayList<MFSleepSession>> p4Var2 = p4Var;
        Set<String> keySet = p4Var.keySet();
        if (!keySet.isEmpty()) {
            if (p4Var.size() > 999) {
                p4 p4Var3 = new p4(999);
                int size = p4Var.size();
                p4 p4Var4 = p4Var3;
                int i23 = 0;
                loop0:
                while (true) {
                    i22 = 0;
                    while (i23 < size) {
                        p4Var4.put(p4Var2.c(i23), p4Var2.e(i23));
                        i23++;
                        i22++;
                        if (i22 == 999) {
                            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(p4Var4);
                            p4Var4 = new p4(999);
                        }
                    }
                    break loop0;
                }
                if (i22 > 0) {
                    __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(p4Var4);
                    return;
                }
                return;
            }
            StringBuilder a = ei.a();
            a.append("SELECT `pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset` FROM `sleep_session` WHERE `day` IN (");
            int size2 = keySet.size();
            ei.a(a, size2);
            a.append(")");
            rh b = rh.b(a.toString(), size2 + 0);
            int i24 = 1;
            for (String str3 : keySet) {
                if (str3 == null) {
                    b.a(i24);
                } else {
                    b.a(i24, str3);
                }
                i24++;
            }
            Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
            try {
                int a3 = ai.a(a2, "day");
                if (a3 != -1) {
                    int a4 = ai.a(a2, "pinType");
                    int a5 = ai.a(a2, HardwareLog.COLUMN_DATE);
                    int a6 = ai.a(a2, "day");
                    int a7 = ai.a(a2, "deviceSerialNumber");
                    int a8 = ai.a(a2, DataFile.COLUMN_SYNC_TIME);
                    int a9 = ai.a(a2, "bookmarkTime");
                    int a10 = ai.a(a2, "normalizedSleepQuality");
                    int a11 = ai.a(a2, "source");
                    int a12 = ai.a(a2, "realStartTime");
                    int a13 = ai.a(a2, "realEndTime");
                    int a14 = ai.a(a2, "realSleepMinutes");
                    int a15 = ai.a(a2, "realSleepStateDistInMinute");
                    int i25 = a4;
                    int a16 = ai.a(a2, "editedStartTime");
                    int a17 = ai.a(a2, "editedEndTime");
                    int a18 = ai.a(a2, "editedSleepMinutes");
                    int a19 = ai.a(a2, "editedSleepStateDistInMinute");
                    int a20 = ai.a(a2, "sleepStates");
                    int a21 = ai.a(a2, "heartRate");
                    int a22 = ai.a(a2, "createdAt");
                    int a23 = ai.a(a2, "updatedAt");
                    int a24 = ai.a(a2, "timezoneOffset");
                    while (a2.moveToNext()) {
                        if (!a2.isNull(a3)) {
                            int i26 = a24;
                            ArrayList arrayList = (ArrayList) p4Var2.get(a2.getString(a3));
                            if (arrayList != null) {
                                int i27 = -1;
                                if (a5 == -1) {
                                    j = 0;
                                } else {
                                    j = a2.getLong(a5);
                                }
                                long j2 = j;
                                if (a6 == -1) {
                                    i = a6;
                                    date = null;
                                } else {
                                    i = a6;
                                    date = this.__dateShortStringConverter.a(a2.getString(a6));
                                    i27 = -1;
                                }
                                if (a7 == i27) {
                                    str = null;
                                } else {
                                    str = a2.getString(a7);
                                }
                                if (a8 != i27) {
                                    if (!a2.isNull(a8)) {
                                        num = Integer.valueOf(a2.getInt(a8));
                                        if (a9 != i27) {
                                            if (!a2.isNull(a9)) {
                                                num2 = Integer.valueOf(a2.getInt(a9));
                                                if (a10 == i27) {
                                                    d = 0.0d;
                                                } else {
                                                    d = a2.getDouble(a10);
                                                }
                                                double d2 = d;
                                                if (a11 == i27) {
                                                    i7 = 0;
                                                } else {
                                                    i7 = a2.getInt(a11);
                                                }
                                                if (a12 == i27) {
                                                    i8 = 0;
                                                } else {
                                                    i8 = a2.getInt(a12);
                                                }
                                                if (a13 == i27) {
                                                    i9 = 0;
                                                } else {
                                                    i9 = a2.getInt(a13);
                                                }
                                                if (a14 == i27) {
                                                    i10 = 0;
                                                } else {
                                                    i10 = a2.getInt(a14);
                                                }
                                                if (a15 == i27) {
                                                    i12 = a16;
                                                    i11 = -1;
                                                    sleepDistribution = null;
                                                } else {
                                                    sleepDistribution = this.__sleepDistributionConverter.a(a2.getString(a15));
                                                    i12 = a16;
                                                    i11 = -1;
                                                }
                                                if (i12 != i11) {
                                                    if (!a2.isNull(i12)) {
                                                        a16 = i12;
                                                        num3 = Integer.valueOf(a2.getInt(i12));
                                                        i13 = a17;
                                                        if (i13 != i11) {
                                                            if (!a2.isNull(i13)) {
                                                                a17 = i13;
                                                                num4 = Integer.valueOf(a2.getInt(i13));
                                                                i14 = a18;
                                                                if (i14 != i11) {
                                                                    if (!a2.isNull(i14)) {
                                                                        a18 = i14;
                                                                        num5 = Integer.valueOf(a2.getInt(i14));
                                                                        i15 = a19;
                                                                        if (i15 != i11) {
                                                                            a19 = i15;
                                                                            i16 = a20;
                                                                            sleepDistribution2 = null;
                                                                        } else {
                                                                            a19 = i15;
                                                                            sleepDistribution2 = this.__sleepDistributionConverter.a(a2.getString(i15));
                                                                            i16 = a20;
                                                                            i11 = -1;
                                                                        }
                                                                        if (i16 != i11) {
                                                                            a20 = i16;
                                                                            i17 = a21;
                                                                            str2 = null;
                                                                        } else {
                                                                            a20 = i16;
                                                                            str2 = a2.getString(i16);
                                                                            i17 = a21;
                                                                        }
                                                                        if (i17 != i11) {
                                                                            a21 = i17;
                                                                            i18 = a22;
                                                                            sleepSessionHeartRate = null;
                                                                        } else {
                                                                            a21 = i17;
                                                                            sleepSessionHeartRate = this.__sleepSessionHeartRateConverter.a(a2.getString(i17));
                                                                            i18 = a22;
                                                                            i11 = -1;
                                                                        }
                                                                        if (i18 != i11) {
                                                                            i5 = a3;
                                                                            i3 = a13;
                                                                            i2 = a23;
                                                                            i19 = -1;
                                                                            dateTime = null;
                                                                        } else {
                                                                            i5 = a3;
                                                                            i3 = a13;
                                                                            dateTime = this.__dateTimeConverter.a(a2.getLong(i18));
                                                                            i2 = a23;
                                                                            i19 = -1;
                                                                        }
                                                                        if (i2 != i19) {
                                                                            i4 = i18;
                                                                            i20 = i26;
                                                                            dateTime2 = null;
                                                                        } else {
                                                                            i4 = i18;
                                                                            dateTime2 = this.__dateTimeConverter.a(a2.getLong(i2));
                                                                            i20 = i26;
                                                                            i19 = -1;
                                                                        }
                                                                        if (i20 != i19) {
                                                                            i21 = 0;
                                                                        } else {
                                                                            i21 = a2.getInt(i20);
                                                                        }
                                                                        MFSleepSession mFSleepSession = new MFSleepSession(j2, date, str, num, num2, d2, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                                                        i26 = i20;
                                                                        i6 = i25;
                                                                        if (i6 != -1) {
                                                                            mFSleepSession.setPinType(a2.getInt(i6));
                                                                        }
                                                                        arrayList.add(mFSleepSession);
                                                                    }
                                                                }
                                                                a18 = i14;
                                                                i15 = a19;
                                                                num5 = null;
                                                                if (i15 != i11) {
                                                                }
                                                                if (i16 != i11) {
                                                                }
                                                                if (i17 != i11) {
                                                                }
                                                                if (i18 != i11) {
                                                                }
                                                                if (i2 != i19) {
                                                                }
                                                                if (i20 != i19) {
                                                                }
                                                                MFSleepSession mFSleepSession2 = new MFSleepSession(j2, date, str, num, num2, d2, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                                                i26 = i20;
                                                                i6 = i25;
                                                                if (i6 != -1) {
                                                                }
                                                                arrayList.add(mFSleepSession2);
                                                            }
                                                        }
                                                        a17 = i13;
                                                        i14 = a18;
                                                        num4 = null;
                                                        if (i14 != i11) {
                                                        }
                                                        a18 = i14;
                                                        i15 = a19;
                                                        num5 = null;
                                                        if (i15 != i11) {
                                                        }
                                                        if (i16 != i11) {
                                                        }
                                                        if (i17 != i11) {
                                                        }
                                                        if (i18 != i11) {
                                                        }
                                                        if (i2 != i19) {
                                                        }
                                                        if (i20 != i19) {
                                                        }
                                                        MFSleepSession mFSleepSession22 = new MFSleepSession(j2, date, str, num, num2, d2, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                                        i26 = i20;
                                                        i6 = i25;
                                                        if (i6 != -1) {
                                                        }
                                                        arrayList.add(mFSleepSession22);
                                                    }
                                                }
                                                a16 = i12;
                                                i13 = a17;
                                                num3 = null;
                                                if (i13 != i11) {
                                                }
                                                a17 = i13;
                                                i14 = a18;
                                                num4 = null;
                                                if (i14 != i11) {
                                                }
                                                a18 = i14;
                                                i15 = a19;
                                                num5 = null;
                                                if (i15 != i11) {
                                                }
                                                if (i16 != i11) {
                                                }
                                                if (i17 != i11) {
                                                }
                                                if (i18 != i11) {
                                                }
                                                if (i2 != i19) {
                                                }
                                                if (i20 != i19) {
                                                }
                                                MFSleepSession mFSleepSession222 = new MFSleepSession(j2, date, str, num, num2, d2, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                                i26 = i20;
                                                i6 = i25;
                                                if (i6 != -1) {
                                                }
                                                arrayList.add(mFSleepSession222);
                                            }
                                        }
                                        num2 = null;
                                        if (a10 == i27) {
                                        }
                                        double d22 = d;
                                        if (a11 == i27) {
                                        }
                                        if (a12 == i27) {
                                        }
                                        if (a13 == i27) {
                                        }
                                        if (a14 == i27) {
                                        }
                                        if (a15 == i27) {
                                        }
                                        if (i12 != i11) {
                                        }
                                        a16 = i12;
                                        i13 = a17;
                                        num3 = null;
                                        if (i13 != i11) {
                                        }
                                        a17 = i13;
                                        i14 = a18;
                                        num4 = null;
                                        if (i14 != i11) {
                                        }
                                        a18 = i14;
                                        i15 = a19;
                                        num5 = null;
                                        if (i15 != i11) {
                                        }
                                        if (i16 != i11) {
                                        }
                                        if (i17 != i11) {
                                        }
                                        if (i18 != i11) {
                                        }
                                        if (i2 != i19) {
                                        }
                                        if (i20 != i19) {
                                        }
                                        MFSleepSession mFSleepSession2222 = new MFSleepSession(j2, date, str, num, num2, d22, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                        i26 = i20;
                                        i6 = i25;
                                        if (i6 != -1) {
                                        }
                                        arrayList.add(mFSleepSession2222);
                                    }
                                }
                                num = null;
                                if (a9 != i27) {
                                }
                                num2 = null;
                                if (a10 == i27) {
                                }
                                double d222 = d;
                                if (a11 == i27) {
                                }
                                if (a12 == i27) {
                                }
                                if (a13 == i27) {
                                }
                                if (a14 == i27) {
                                }
                                if (a15 == i27) {
                                }
                                if (i12 != i11) {
                                }
                                a16 = i12;
                                i13 = a17;
                                num3 = null;
                                if (i13 != i11) {
                                }
                                a17 = i13;
                                i14 = a18;
                                num4 = null;
                                if (i14 != i11) {
                                }
                                a18 = i14;
                                i15 = a19;
                                num5 = null;
                                if (i15 != i11) {
                                }
                                if (i16 != i11) {
                                }
                                if (i17 != i11) {
                                }
                                if (i18 != i11) {
                                }
                                if (i2 != i19) {
                                }
                                if (i20 != i19) {
                                }
                                MFSleepSession mFSleepSession22222 = new MFSleepSession(j2, date, str, num, num2, d222, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                i26 = i20;
                                i6 = i25;
                                if (i6 != -1) {
                                }
                                arrayList.add(mFSleepSession22222);
                            } else {
                                i = a6;
                                i3 = a13;
                                i6 = i25;
                                i2 = a23;
                                i4 = a22;
                                i5 = a3;
                            }
                            p4Var2 = p4Var;
                            i25 = i6;
                            a3 = i5;
                            a22 = i4;
                            a24 = i26;
                            a13 = i3;
                        } else {
                            i = a6;
                            i2 = a23;
                            int i28 = a22;
                            p4Var2 = p4Var;
                        }
                        a23 = i2;
                        a6 = i;
                    }
                    a2.close();
                }
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore
    public void deleteAllSleepDays() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllSleepDays.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepDays.release(acquire);
        }
    }

    @DexIgnore
    public void deleteAllSleepSessions() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllSleepSessions.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepSessions.release(acquire);
        }
    }

    @DexIgnore
    public MFSleepDay getLastSleepDay() {
        rh b = rh.b("SELECT * FROM sleep_date ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "timezoneOffset");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "goalMinutes");
            int b6 = ai.b(a, "sleepMinutes");
            int b7 = ai.b(a, "sleepStateDistInMinute");
            int b8 = ai.b(a, "createdAt");
            int b9 = ai.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<Integer> getLastSleepGoal() {
        return this.__db.getInvalidationTracker().a(new String[]{"sleep_settings"}, false, new Anon13(rh.b("SELECT sleepGoal FROM sleep_settings LIMIT 1", 0)));
    }

    @DexIgnore
    public MFSleepDay getNearestSleepDayFromDate(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM sleep_date WHERE date <= ? ORDER BY date ASC LIMIT 1", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "timezoneOffset");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "goalMinutes");
            int b6 = ai.b(a, "sleepMinutes");
            int b7 = ai.b(a, "sleepStateDistInMinute");
            int b8 = ai.b(a, "createdAt");
            int b9 = ai.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MFSleepSession> getPendingSleepSessions(long j, long j2) {
        rh rhVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        int i3;
        Integer valueOf;
        SleepDao_Impl sleepDao_Impl = this;
        rh b = rh.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? AND pinType <> 0 ORDER BY editedStartTime ASC", 2);
        b.a(1, j);
        b.a(2, j2);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(sleepDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a, "day");
            int b5 = ai.b(a, "deviceSerialNumber");
            int b6 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            int b7 = ai.b(a, "bookmarkTime");
            int b8 = ai.b(a, "normalizedSleepQuality");
            int b9 = ai.b(a, "source");
            int b10 = ai.b(a, "realStartTime");
            int b11 = ai.b(a, "realEndTime");
            int b12 = ai.b(a, "realSleepMinutes");
            int b13 = ai.b(a, "realSleepStateDistInMinute");
            int b14 = ai.b(a, "editedStartTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "editedEndTime");
                int i4 = b2;
                int b16 = ai.b(a, "editedSleepMinutes");
                int b17 = ai.b(a, "editedSleepStateDistInMinute");
                int b18 = ai.b(a, "sleepStates");
                int b19 = ai.b(a, "heartRate");
                int b20 = ai.b(a, "createdAt");
                int b21 = ai.b(a, "updatedAt");
                int b22 = ai.b(a, "timezoneOffset");
                int i5 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j3 = a.getLong(b3);
                    int i6 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    int i10 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i3 = i;
                        valueOf = null;
                    } else {
                        i3 = i;
                        valueOf = Integer.valueOf(a.getInt(i2));
                    }
                    int i11 = b17;
                    int i12 = b14;
                    int i13 = i11;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i11));
                    int i14 = b18;
                    String string2 = a.getString(i14);
                    b18 = i14;
                    int i15 = b19;
                    b19 = i15;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i15));
                    int i16 = b4;
                    int i17 = b20;
                    int i18 = i2;
                    int i19 = i17;
                    int i20 = b21;
                    b21 = i20;
                    int i21 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j3, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, valueOf, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i17)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i20)), a.getInt(i21));
                    int i22 = i4;
                    mFSleepSession.setPinType(a.getInt(i22));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i21;
                    i4 = i22;
                    b3 = i6;
                    b4 = i16;
                    b14 = i12;
                    b17 = i13;
                    i5 = i3;
                    b16 = i18;
                    b20 = i19;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public MFSleepDay getSleepDay(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "timezoneOffset");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "goalMinutes");
            int b6 = ai.b(a, "sleepMinutes");
            int b7 = ai.b(a, "sleepStateDistInMinute");
            int b8 = ai.b(a, "createdAt");
            int b9 = ai.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<MFSleepDay> getSleepDayLiveData(String str) {
        rh b = rh.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"sleep_date"}, false, new Anon11(b));
    }

    @DexIgnore
    public List<MFSleepDay> getSleepDays(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        rh b = rh.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "timezoneOffset");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "goalMinutes");
            int b6 = ai.b(a, "sleepMinutes");
            int b7 = ai.b(a, "sleepStateDistInMinute");
            int b8 = ai.b(a, "createdAt");
            int b9 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                MFSleepDay mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
                arrayList.add(mFSleepDay);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<MFSleepDay>> getSleepDaysLiveData(String str, String str2) {
        rh b = rh.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        if (str2 == null) {
            b.a(2);
        } else {
            b.a(2, str2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"sleep_date"}, false, new Anon12(b));
    }

    @DexIgnore
    public LiveData<SleepRecommendedGoal> getSleepRecommendedGoalLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"sleepRecommendedGoals"}, false, new Anon14(rh.b("SELECT * FROM sleepRecommendedGoals LIMIT 1", 0)));
    }

    @DexIgnore
    public MFSleepSession getSleepSession(long j) {
        rh rhVar;
        MFSleepSession mFSleepSession;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        int i;
        Integer num5;
        int i2;
        rh b = rh.b("SELECT * FROM sleep_session WHERE realEndTime = ?", 1);
        b.a(1, j);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a, "day");
            int b5 = ai.b(a, "deviceSerialNumber");
            int b6 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            int b7 = ai.b(a, "bookmarkTime");
            int b8 = ai.b(a, "normalizedSleepQuality");
            int b9 = ai.b(a, "source");
            int b10 = ai.b(a, "realStartTime");
            int b11 = ai.b(a, "realEndTime");
            int b12 = ai.b(a, "realSleepMinutes");
            int b13 = ai.b(a, "realSleepStateDistInMinute");
            int b14 = ai.b(a, "editedStartTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "editedEndTime");
                int i3 = b2;
                int b16 = ai.b(a, "editedSleepMinutes");
                int b17 = ai.b(a, "editedSleepStateDistInMinute");
                int b18 = ai.b(a, "sleepStates");
                int b19 = ai.b(a, "heartRate");
                int b20 = ai.b(a, "createdAt");
                int b21 = ai.b(a, "updatedAt");
                int b22 = ai.b(a, "timezoneOffset");
                if (a.moveToFirst()) {
                    long j2 = a.getLong(b3);
                    Date a2 = this.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i4 = a.getInt(b9);
                    int i5 = a.getInt(b10);
                    int i6 = a.getInt(b11);
                    int i7 = a.getInt(b12);
                    SleepDistribution a3 = this.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                    }
                    if (a.isNull(b15)) {
                        i = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(b15));
                        i = b16;
                    }
                    if (a.isNull(i)) {
                        i2 = b17;
                        num5 = null;
                    } else {
                        num5 = Integer.valueOf(a.getInt(i));
                        i2 = b17;
                    }
                    mFSleepSession = new MFSleepSession(j2, a2, string, num, num2, d, i4, i5, i6, i7, a3, num3, num4, num5, this.__sleepDistributionConverter.a(a.getString(i2)), a.getString(b18), this.__sleepSessionHeartRateConverter.a(a.getString(b19)), this.__dateTimeConverter.a(a.getLong(b20)), this.__dateTimeConverter.a(a.getLong(b21)), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(i3));
                } else {
                    mFSleepSession = null;
                }
                a.close();
                rhVar.c();
                return mFSleepSession;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(String str) {
        rh rhVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        Integer num5;
        int i3;
        int i4;
        SleepDao_Impl sleepDao_Impl = this;
        String str2 = str;
        rh b = rh.b("SELECT * FROM sleep_session WHERE day = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(sleepDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a, "day");
            int b5 = ai.b(a, "deviceSerialNumber");
            int b6 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            int b7 = ai.b(a, "bookmarkTime");
            int b8 = ai.b(a, "normalizedSleepQuality");
            int b9 = ai.b(a, "source");
            int b10 = ai.b(a, "realStartTime");
            int b11 = ai.b(a, "realEndTime");
            int b12 = ai.b(a, "realSleepMinutes");
            int b13 = ai.b(a, "realSleepStateDistInMinute");
            int b14 = ai.b(a, "editedStartTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "editedEndTime");
                int i5 = b2;
                int b16 = ai.b(a, "editedSleepMinutes");
                int b17 = ai.b(a, "editedSleepStateDistInMinute");
                int b18 = ai.b(a, "sleepStates");
                int b19 = ai.b(a, "heartRate");
                int b20 = ai.b(a, "createdAt");
                int b21 = ai.b(a, "updatedAt");
                int b22 = ai.b(a, "timezoneOffset");
                int i6 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b3);
                    int i7 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i8 = a.getInt(b9);
                    int i9 = a.getInt(b10);
                    int i10 = a.getInt(b11);
                    int i11 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i6;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i6;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i6 = i;
                        i3 = b14;
                        i4 = b17;
                        num5 = null;
                    } else {
                        i6 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i4 = b17;
                        i3 = b14;
                    }
                    b17 = i4;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i4));
                    int i12 = b18;
                    String string2 = a.getString(i12);
                    b18 = i12;
                    int i13 = b19;
                    b19 = i13;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i13));
                    int i14 = b20;
                    int i15 = i2;
                    int i16 = i14;
                    int i17 = b21;
                    b21 = i17;
                    int i18 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i8, i9, i10, i11, a3, num3, num4, num5, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i14)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i17)), a.getInt(i18));
                    int i19 = i5;
                    mFSleepSession.setPinType(a.getInt(i19));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i18;
                    i5 = i19;
                    b14 = i3;
                    b16 = i15;
                    b3 = i7;
                    b20 = i16;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<MFSleepSession>> getSleepSessionsLiveData(long j, long j2) {
        rh b = rh.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        b.a(1, j);
        b.a(2, j2);
        return this.__db.getInvalidationTracker().a(new String[]{"sleep_session"}, false, new Anon10(b));
    }

    @DexIgnore
    public MFSleepSettings getSleepSettings() {
        rh b = rh.b("SELECT * FROM sleep_settings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        MFSleepSettings mFSleepSettings = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "sleepGoal");
            if (a.moveToFirst()) {
                MFSleepSettings mFSleepSettings2 = new MFSleepSettings(a.getInt(b3));
                mFSleepSettings2.setId(a.getInt(b2));
                mFSleepSettings = mFSleepSettings2;
            }
            return mFSleepSettings;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public SleepStatistic getSleepStatistic() {
        SleepStatistic sleepStatistic;
        rh b = rh.b("SELECT * FROM sleep_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uid");
            int b4 = ai.b(a, "sleepTimeBestDay");
            int b5 = ai.b(a, "sleepTimeBestStreak");
            int b6 = ai.b(a, "totalDays");
            int b7 = ai.b(a, "totalSleeps");
            int b8 = ai.b(a, "totalSleepMinutes");
            int b9 = ai.b(a, "totalSleepStateDistInMinute");
            int b10 = ai.b(a, "createdAt");
            int b11 = ai.b(a, "updatedAt");
            if (a.moveToFirst()) {
                sleepStatistic = new SleepStatistic(a.getString(b2), a.getString(b3), this.__sleepStatisticConverter.a(a.getString(b4)), this.__sleepStatisticConverter.a(a.getString(b5)), a.getInt(b6), a.getInt(b7), a.getInt(b8), this.__integerArrayConverter.a(a.getString(b9)), this.__dateTimeConverter.a(a.getLong(b10)), this.__dateTimeConverter.a(a.getLong(b11)));
            } else {
                sleepStatistic = null;
            }
            return sleepStatistic;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<SleepStatistic> getSleepStatisticLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{SleepStatistic.TABLE_NAME}, false, new Anon15(rh.b("SELECT * FROM sleep_statistic LIMIT 1", 0)));
    }

    @DexIgnore
    public List<SleepSummary> getSleepSummariesDesc(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        rh b = rh.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = bi.a(this.__db, b, true, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "timezoneOffset");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "goalMinutes");
            int b6 = ai.b(a, "sleepMinutes");
            int b7 = ai.b(a, "sleepStateDistInMinute");
            int b8 = ai.b(a, "createdAt");
            int b9 = ai.b(a, "updatedAt");
            p4 p4Var = new p4();
            while (a.moveToNext()) {
                if (!a.isNull(b4)) {
                    String string = a.getString(b4);
                    if (((ArrayList) p4Var.get(string)) == null) {
                        p4Var.put(string, new ArrayList());
                    }
                }
            }
            a.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(p4Var);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                if (!a.isNull(b2) || !a.isNull(b3) || !a.isNull(b4) || !a.isNull(b5) || !a.isNull(b6) || !a.isNull(b7) || !a.isNull(b8) || !a.isNull(b9)) {
                    mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                    mFSleepDay.setPinType(a.getInt(b2));
                    mFSleepDay.setTimezoneOffset(a.getInt(b3));
                }
                ArrayList arrayList2 = !a.isNull(b4) ? (ArrayList) p4Var.get(a.getString(b4)) : null;
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList();
                }
                arrayList.add(new SleepSummary(mFSleepDay, arrayList2));
                mFSleepDay = null;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0100 A[Catch:{ all -> 0x0120 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010d A[Catch:{ all -> 0x0120 }] */
    public SleepSummary getSleepSummary(String str) {
        SleepSummary sleepSummary;
        MFSleepDay mFSleepDay;
        String str2 = str;
        rh b = rh.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        ArrayList arrayList = null;
        Cursor a = bi.a(this.__db, b, true, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "timezoneOffset");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "goalMinutes");
            int b6 = ai.b(a, "sleepMinutes");
            int b7 = ai.b(a, "sleepStateDistInMinute");
            int b8 = ai.b(a, "createdAt");
            int b9 = ai.b(a, "updatedAt");
            p4 p4Var = new p4();
            while (a.moveToNext()) {
                if (!a.isNull(b4)) {
                    String string = a.getString(b4);
                    if (((ArrayList) p4Var.get(string)) == null) {
                        p4Var.put(string, new ArrayList());
                    }
                }
            }
            a.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(p4Var);
            if (a.moveToFirst()) {
                if (a.isNull(b2) && a.isNull(b3) && a.isNull(b4) && a.isNull(b5) && a.isNull(b6) && a.isNull(b7) && a.isNull(b8)) {
                    if (a.isNull(b9)) {
                        mFSleepDay = null;
                        if (!a.isNull(b4)) {
                            arrayList = p4Var.get(a.getString(b4));
                        }
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        sleepSummary = new SleepSummary(mFSleepDay, arrayList);
                    }
                }
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
                if (!a.isNull(b4)) {
                }
                if (arrayList == null) {
                }
                sleepSummary = new SleepSummary(mFSleepDay, arrayList);
            } else {
                sleepSummary = null;
            }
            return sleepSummary;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public int getTotalSleep(String str, String str2) {
        rh b = rh.b("SELECT SUM(sleepMinutes) FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        if (str2 == null) {
            b.a(2);
        } else {
            b.a(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertSleepSettings(MFSleepSettings mFSleepSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSettings.insert(mFSleepSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateSleepSettings(int i) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfUpdateSleepSettings.acquire();
        acquire.a(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateSleepSettings.release(acquire);
        }
    }

    @DexIgnore
    public void upsertSleepDay(MFSleepDay mFSleepDay) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(mFSleepDay);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepDays(List<MFSleepDay> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepRecommendedGoal(SleepRecommendedGoal sleepRecommendedGoal) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSleepRecommendedGoal.insert(sleepRecommendedGoal);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepSession(MFSleepSession mFSleepSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession.insert(mFSleepSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepSessionList(List<MFSleepSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public long upsertSleepStatistic(SleepStatistic sleepStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfSleepStatistic.insertAndReturnId(sleepStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<MFSleepSession> getPendingSleepSessions() {
        rh rhVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        Integer num5;
        int i3;
        int i4;
        SleepDao_Impl sleepDao_Impl = this;
        rh b = rh.b("SELECT * FROM sleep_session WHERE pinType <> 0", 0);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(sleepDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a, "day");
            int b5 = ai.b(a, "deviceSerialNumber");
            int b6 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            int b7 = ai.b(a, "bookmarkTime");
            int b8 = ai.b(a, "normalizedSleepQuality");
            int b9 = ai.b(a, "source");
            int b10 = ai.b(a, "realStartTime");
            int b11 = ai.b(a, "realEndTime");
            int b12 = ai.b(a, "realSleepMinutes");
            int b13 = ai.b(a, "realSleepStateDistInMinute");
            int b14 = ai.b(a, "editedStartTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "editedEndTime");
                int i5 = b2;
                int b16 = ai.b(a, "editedSleepMinutes");
                int b17 = ai.b(a, "editedSleepStateDistInMinute");
                int b18 = ai.b(a, "sleepStates");
                int b19 = ai.b(a, "heartRate");
                int b20 = ai.b(a, "createdAt");
                int b21 = ai.b(a, "updatedAt");
                int b22 = ai.b(a, "timezoneOffset");
                int i6 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b3);
                    int i7 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i8 = a.getInt(b9);
                    int i9 = a.getInt(b10);
                    int i10 = a.getInt(b11);
                    int i11 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i6;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i6;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i6 = i;
                        i3 = b14;
                        i4 = b17;
                        num5 = null;
                    } else {
                        i6 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i4 = b17;
                        i3 = b14;
                    }
                    b17 = i4;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i4));
                    int i12 = b18;
                    String string2 = a.getString(i12);
                    b18 = i12;
                    int i13 = b19;
                    b19 = i13;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i13));
                    int i14 = b20;
                    int i15 = i2;
                    int i16 = i14;
                    int i17 = b21;
                    b21 = i17;
                    int i18 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i8, i9, i10, i11, a3, num3, num4, num5, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i14)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i17)), a.getInt(i18));
                    int i19 = i5;
                    mFSleepSession.setPinType(a.getInt(i19));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i18;
                    i5 = i19;
                    b14 = i3;
                    b16 = i15;
                    b3 = i7;
                    b20 = i16;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(long j, long j2) {
        rh rhVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        int i3;
        Integer valueOf;
        SleepDao_Impl sleepDao_Impl = this;
        rh b = rh.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        b.a(1, j);
        b.a(2, j2);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(sleepDao_Impl.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a, "day");
            int b5 = ai.b(a, "deviceSerialNumber");
            int b6 = ai.b(a, DataFile.COLUMN_SYNC_TIME);
            int b7 = ai.b(a, "bookmarkTime");
            int b8 = ai.b(a, "normalizedSleepQuality");
            int b9 = ai.b(a, "source");
            int b10 = ai.b(a, "realStartTime");
            int b11 = ai.b(a, "realEndTime");
            int b12 = ai.b(a, "realSleepMinutes");
            int b13 = ai.b(a, "realSleepStateDistInMinute");
            int b14 = ai.b(a, "editedStartTime");
            rhVar = b;
            try {
                int b15 = ai.b(a, "editedEndTime");
                int i4 = b2;
                int b16 = ai.b(a, "editedSleepMinutes");
                int b17 = ai.b(a, "editedSleepStateDistInMinute");
                int b18 = ai.b(a, "sleepStates");
                int b19 = ai.b(a, "heartRate");
                int b20 = ai.b(a, "createdAt");
                int b21 = ai.b(a, "updatedAt");
                int b22 = ai.b(a, "timezoneOffset");
                int i5 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j3 = a.getLong(b3);
                    int i6 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    int i10 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i3 = i;
                        valueOf = null;
                    } else {
                        i3 = i;
                        valueOf = Integer.valueOf(a.getInt(i2));
                    }
                    int i11 = b17;
                    int i12 = b14;
                    int i13 = i11;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i11));
                    int i14 = b18;
                    String string2 = a.getString(i14);
                    b18 = i14;
                    int i15 = b19;
                    b19 = i15;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i15));
                    int i16 = b4;
                    int i17 = b20;
                    int i18 = i2;
                    int i19 = i17;
                    int i20 = b21;
                    b21 = i20;
                    int i21 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j3, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, valueOf, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i17)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i20)), a.getInt(i21));
                    int i22 = i4;
                    mFSleepSession.setPinType(a.getInt(i22));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i21;
                    i4 = i22;
                    b3 = i6;
                    b4 = i16;
                    b14 = i12;
                    b17 = i13;
                    i5 = i3;
                    b16 = i18;
                    b20 = i19;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }
}
