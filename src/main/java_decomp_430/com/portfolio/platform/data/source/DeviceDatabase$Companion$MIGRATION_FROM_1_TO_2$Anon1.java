package com.portfolio.platform.data.source;

import com.fossil.ii;
import com.fossil.wg6;
import com.fossil.xh;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends xh {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(ii iiVar) {
        wg6.b(iiVar, "database");
        iiVar.u();
        try {
            iiVar.b("CREATE TABLE `watchParam` (`prefixSerial` TEXT, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXTPRIMARY KEY(`prefixSerial`))");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(DeviceDatabase.TAG, "migrate DeviceDatabase from 1->2 failed");
            iiVar.b("DROP TABLE IF EXISTS watchParam");
            iiVar.b("CREATE TABLE IF NOT EXISTS watchParam (prefixSerial TEXT PRIMARY KEY NOT NULL, versionMajor TEXT, versionMinor TEXT, data TEXT)");
        }
        iiVar.x();
        iiVar.y();
    }
}
