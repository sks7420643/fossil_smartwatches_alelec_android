package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$getSummary$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<GoalTrackingSummary, GoalDailySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ List $pendingList;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getSummary$Anon1 goalTrackingRepository$getSummary$Anon1, List list) {
            this.this$0 = goalTrackingRepository$getSummary$Anon1;
            this.$pendingList = list;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<GoalDailySummary>> xe6) {
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            String e = bk4.e(this.this$0.$date);
            wg6.a((Object) e, "DateHelper.formatShortDate(date)");
            return access$getMApiServiceV2$p.getGoalTrackingSummary(e, xe6);
        }

        @DexIgnore
        public LiveData<GoalTrackingSummary> loadFromDb() {
            return this.this$0.this$0.mGoalTrackingDao.getGoalTrackingSummaryLiveData(this.this$0.$date);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummary onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(GoalDailySummary goalDailySummary) {
            wg6.b(goalDailySummary, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getSummary date=" + this.this$0.$date + " saveCallResult onResponse: response = " + goalDailySummary);
            try {
                GoalTrackingDao access$getMGoalTrackingDao$p = this.this$0.this$0.mGoalTrackingDao;
                GoalTrackingSummary goalTrackingSummary = goalDailySummary.toGoalTrackingSummary();
                if (goalTrackingSummary != null) {
                    access$getMGoalTrackingDao$p.upsertGoalTrackingSummary(goalTrackingSummary);
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tag2 = GoalTrackingRepository.Companion.getTAG();
                local2.e(tag2, "getSummary date=" + this.this$0.$date + " exception=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public boolean shouldFetch(GoalTrackingSummary goalTrackingSummary) {
            return this.$pendingList.isEmpty();
        }
    }

    @DexIgnore
    public GoalTrackingRepository$getSummary$Anon1(GoalTrackingRepository goalTrackingRepository, Date date) {
        this.this$0 = goalTrackingRepository;
        this.$date = date;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1$Anon1_Level2] */
    public final LiveData<yx5<GoalTrackingSummary>> apply(List<GoalTrackingData> list) {
        return new Anon1_Level2(this, list).asLiveData();
    }
}
