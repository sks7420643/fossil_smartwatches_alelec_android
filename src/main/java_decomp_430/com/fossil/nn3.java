package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nn3<E> extends tl3<E> {
    @DexIgnore
    public /* final */ vl3<E> delegate;
    @DexIgnore
    public /* final */ zl3<? extends E> delegateList;

    @DexIgnore
    public nn3(vl3<E> vl3, zl3<? extends E> zl3) {
        this.delegate = vl3;
        this.delegateList = zl3;
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        return this.delegateList.copyIntoArray(objArr, i);
    }

    @DexIgnore
    public vl3<E> delegateCollection() {
        return this.delegate;
    }

    @DexIgnore
    public zl3<? extends E> delegateList() {
        return this.delegateList;
    }

    @DexIgnore
    public E get(int i) {
        return this.delegateList.get(i);
    }

    @DexIgnore
    public ko3<E> listIterator(int i) {
        return this.delegateList.listIterator(i);
    }

    @DexIgnore
    public nn3(vl3<E> vl3, Object[] objArr) {
        this(vl3, zl3.asImmutableList(objArr));
    }
}
