package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ w40 b;
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<md0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                wg6.a(createByteArray, "parcel.createByteArray()!!");
                return new md0(createByteArray);
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new md0[i];
        }
    }

    @DexIgnore
    public md0(byte[] bArr) throws IllegalArgumentException {
        this.a = bArr;
        if (this.a.length >= 16) {
            this.c = ByteBuffer.wrap(md6.a(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
            this.b = new w40(bArr[2], bArr[3]);
            return;
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("data.size("), this.a.length, ") is not equal or larger ", "than 16"));
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.FILE_HANDLE, (Object) cw0.a(this.c)), bm0.FILE_VERSION, (Object) this.b.toString()), bm0.FILE_CRC_C, (Object) Long.valueOf(h51.a.a(this.a, q11.CRC32C))), bm0.FILE_SIZE, (Object) Integer.valueOf(this.a.length));
    }

    @DexIgnore
    public final short b() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.a;
    }

    @DexIgnore
    public final w40 getWatchParameterVersion() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
    }
}
