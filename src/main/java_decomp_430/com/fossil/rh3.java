package com.fossil;

import android.content.Context;
import android.graphics.Color;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rh3 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public rh3(Context context) {
        this.a = oi3.a(context, nf3.elevationOverlayEnabled, false);
        this.b = yg3.a(context, nf3.elevationOverlayColor, 0);
        this.c = yg3.a(context, nf3.colorSurface, 0);
        this.d = context.getResources().getDisplayMetrics().density;
    }

    @DexIgnore
    public int a(int i, float f) {
        float a2 = a(f);
        return f7.c(yg3.a(f7.c(i, 255), this.b, a2), Color.alpha(i));
    }

    @DexIgnore
    public int b(int i, float f) {
        return (!this.a || !a(i)) ? i : a(i, f);
    }

    @DexIgnore
    public float a(float f) {
        float f2 = this.d;
        if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return Math.min(((((float) Math.log1p((double) (f / f2))) * 4.5f) + 2.0f) / 100.0f, 1.0f);
    }

    @DexIgnore
    public boolean a() {
        return this.a;
    }

    @DexIgnore
    public final boolean a(int i) {
        return f7.c(i, 255) == this.c;
    }
}
