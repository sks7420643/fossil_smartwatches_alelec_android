package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vd0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public byte d;
    @DexIgnore
    public byte e;
    @DexIgnore
    public byte f;
    @DexIgnore
    public byte g;
    @DexIgnore
    public /* final */ byte h;
    @DexIgnore
    public byte i;
    @DexIgnore
    public /* final */ td0 j; // = qa1.b.a(this.c);
    @DexIgnore
    public /* final */ ud0 k; // = qa1.b.b(this.c);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<vd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                wg6.a(createByteArray, "parcel.createByteArray()!!");
                return new vd0(createByteArray);
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new vd0[i];
        }
    }

    @DexIgnore
    public vd0(byte[] bArr) {
        this.a = bArr;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.b = order.get(0);
        this.c = order.getShort(1);
        this.d = order.get(3);
        this.e = order.get(4);
        this.f = order.get(5);
        this.g = order.get(6);
        this.h = order.get(7);
        this.i = order.get(8);
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.MICRO_APP_ID, (Object) this.j.name()), bm0.VARIANT, (Object) this.k), bm0.MICRO_APP_VERSION, (Object) Byte.valueOf(this.b)), bm0.VARIATION_NUMBER, (Object) Byte.valueOf(this.d)), bm0.CONTEXT_NUMBER, (Object) Byte.valueOf(this.e)), bm0.ACTIVITY_ID, (Object) Byte.valueOf(this.f)), bm0.EVENT_ID, (Object) Byte.valueOf(this.g)), bm0.REQUEST_ID, (Object) Byte.valueOf(this.h)), bm0.MICRO_APP_EVENT, (Object) Byte.valueOf(this.i));
    }

    @DexIgnore
    public final byte[] b() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(vd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.a, ((vd0) obj).a);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppEvent");
    }

    @DexIgnore
    public final td0 getMicroAppId() {
        return this.j;
    }

    @DexIgnore
    public final byte getRequestId() {
        return this.h;
    }

    @DexIgnore
    public final ud0 getVariant() {
        return this.k;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.a);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
    }
}
