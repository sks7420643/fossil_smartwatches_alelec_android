package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk1 extends p21 {
    @DexIgnore
    public /* final */ kd0 S;

    @DexIgnore
    public tk1(ue1 ue1, q41 q41, kd0 kd0) {
        super(ue1, q41, eh1.PUT_LOCALIZATION_FILE, true, kd0.b(), kd0.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192);
        this.S = kd0;
    }

    @DexIgnore
    public Object d() {
        return this.S.getLocaleString();
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.LOCALIZATION_FILE, (Object) this.S.a());
    }

    @DexIgnore
    public final kd0 s() {
        return this.S;
    }
}
