package com.fossil;

import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p40 implements Serializable {
    @DexIgnore
    public static /* synthetic */ String a(p40 p40, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                i = 2;
            }
            return p40.a(i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toJSONString");
    }

    @DexIgnore
    public abstract JSONObject a();

    @DexIgnore
    public String toString() {
        return a(0);
    }

    @DexIgnore
    public final String a(int i) {
        if (i == 0) {
            String jSONObject = a().toString();
            wg6.a(jSONObject, "toJSONObject().toString()");
            return jSONObject;
        }
        String jSONObject2 = a().toString(i);
        wg6.a(jSONObject2, "toJSONObject().toString(indentSpace)");
        return jSONObject2;
    }
}
