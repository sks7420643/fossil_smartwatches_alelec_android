package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl6<T> extends ro6<T> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater e; // = AtomicIntegerFieldUpdater.newUpdater(wl6.class, "_decision");
    @DexIgnore
    public volatile int _decision; // = 0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wl6(af6 af6, xe6<? super T> xe6) {
        super(af6, xe6);
        wg6.b(af6, "context");
        wg6.b(xe6, "uCont");
    }

    @DexIgnore
    public void a(Object obj, int i) {
        if (!q()) {
            super.a(obj, i);
        }
    }

    @DexIgnore
    public int j() {
        return 1;
    }

    @DexIgnore
    public final Object p() {
        if (r()) {
            return ff6.a();
        }
        Object b = zm6.b(d());
        if (!(b instanceof vk6)) {
            return b;
        }
        throw ((vk6) b).a;
    }

    @DexIgnore
    public final boolean q() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!e.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean r() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!e.compareAndSet(this, 0, 1));
        return true;
    }
}
