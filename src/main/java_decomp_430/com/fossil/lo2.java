package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo2<K, V> extends LinkedHashMap<K, V> {
    @DexIgnore
    public static /* final */ lo2 a;
    @DexIgnore
    public boolean zza; // = true;

    /*
    static {
        lo2 lo2 = new lo2();
        a = lo2;
        lo2.zza = false;
    }
    */

    @DexIgnore
    public lo2() {
    }

    @DexIgnore
    public static int b(Object obj) {
        if (obj instanceof byte[]) {
            return hn2.c((byte[]) obj);
        }
        if (!(obj instanceof kn2)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <K, V> lo2<K, V> zza() {
        return a;
    }

    @DexIgnore
    public final void clear() {
        zze();
        super.clear();
    }

    @DexIgnore
    public final Set<Map.Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[RETURN] */
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator it = entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Map.Entry entry = (Map.Entry) it.next();
                        if (map.containsKey(entry.getKey())) {
                            Object value = entry.getValue();
                            Object obj2 = map.get(entry.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                z2 = value.equals(obj2);
                                continue;
                            } else {
                                z2 = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!z2) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                z = false;
                if (!z) {
                    return true;
                }
            }
            z = true;
            if (!z) {
                return false;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int i = 0;
        for (Map.Entry entry : entrySet()) {
            i += b(entry.getValue()) ^ b(entry.getKey());
        }
        return i;
    }

    @DexIgnore
    public final V put(K k, V v) {
        zze();
        hn2.a(k);
        hn2.a(v);
        return super.put(k, v);
    }

    @DexIgnore
    public final void putAll(Map<? extends K, ? extends V> map) {
        zze();
        for (Object next : map.keySet()) {
            hn2.a(next);
            hn2.a(map.get(next));
        }
        super.putAll(map);
    }

    @DexIgnore
    public final V remove(Object obj) {
        zze();
        return super.remove(obj);
    }

    @DexIgnore
    public final lo2<K, V> zzb() {
        return isEmpty() ? new lo2<>() : new lo2<>(this);
    }

    @DexIgnore
    public final void zzc() {
        this.zza = false;
    }

    @DexIgnore
    public final boolean zzd() {
        return this.zza;
    }

    @DexIgnore
    public final void zze() {
        if (!this.zza) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final void zza(lo2<K, V> lo2) {
        zze();
        if (!lo2.isEmpty()) {
            putAll(lo2);
        }
    }

    @DexIgnore
    public lo2(Map<K, V> map) {
        super(map);
    }
}
