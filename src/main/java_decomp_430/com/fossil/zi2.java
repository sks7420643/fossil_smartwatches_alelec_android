package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zi2 extends fn2<zi2, a> implements to2 {
    @DexIgnore
    public static /* final */ zi2 zzj;
    @DexIgnore
    public static volatile yo2<zi2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public xi2 zzf;
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;
    @DexIgnore
    public boolean zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<zi2, a> implements to2 {
        @DexIgnore
        public a() {
            super(zi2.zzj);
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((zi2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(bj2 bj2) {
            this();
        }
    }

    /*
    static {
        zi2 zi2 = new zi2();
        zzj = zi2;
        fn2.a(zi2.class, zi2);
    }
    */

    @DexIgnore
    public static a w() {
        return (a) zzj.h();
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 2;
            this.zze = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int o() {
        return this.zzd;
    }

    @DexIgnore
    public final String p() {
        return this.zze;
    }

    @DexIgnore
    public final xi2 q() {
        xi2 xi2 = this.zzf;
        return xi2 == null ? xi2.v() : xi2;
    }

    @DexIgnore
    public final boolean r() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean s() {
        return this.zzh;
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final boolean v() {
        return this.zzi;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (bj2.a[i - 1]) {
            case 1:
                return new zi2();
            case 2:
                return new a((bj2) null);
            case 3:
                return fn2.a((ro2) zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\t\u0002\u0004\u0007\u0003\u0005\u0007\u0004\u0006\u0007\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                yo2<zi2> yo2 = zzk;
                if (yo2 == null) {
                    synchronized (zi2.class) {
                        yo2 = zzk;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzj);
                            zzk = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
