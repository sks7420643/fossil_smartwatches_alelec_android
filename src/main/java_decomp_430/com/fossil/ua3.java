package com.fossil;

import com.fossil.kj2;
import com.fossil.lj2;
import com.fossil.sj2;
import com.fossil.tj2;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public kj2 b;
    @DexIgnore
    public BitSet c;
    @DexIgnore
    public BitSet d;
    @DexIgnore
    public Map<Integer, Long> e;
    @DexIgnore
    public Map<Integer, List<Long>> f;
    @DexIgnore
    public /* final */ /* synthetic */ sa3 g;

    @DexIgnore
    public ua3(sa3 sa3, String str) {
        this.g = sa3;
        this.a = str;
        kj2.a v = kj2.v();
        v.a(true);
        this.b = (kj2) v.i();
        this.c = new BitSet();
        this.d = new BitSet();
        this.e = new p4();
        this.f = new p4();
    }

    @DexIgnore
    public final void a(sj2 sj2, BitSet bitSet, BitSet bitSet2, Map<Integer, Long> map) {
        this.c = bitSet;
        this.d = bitSet2;
        this.e = map;
        sj2.a A = sj2.A();
        A.b((Iterable<? extends Long>) ia3.a(bitSet));
        A.a((Iterable<? extends Long>) ia3.a(bitSet2));
        A.c(a());
        kj2.a v = kj2.v();
        v.a(false);
        v.a(sj2);
        v.a(A);
        this.b = (kj2) v.i();
    }

    @DexIgnore
    public /* synthetic */ ua3(sa3 sa3, String str, va3 va3) {
        this(sa3, str);
    }

    @DexIgnore
    public final void a(za3 za3) {
        int a2 = za3.a();
        Boolean bool = za3.c;
        if (bool != null) {
            this.d.set(a2, bool.booleanValue());
        }
        Boolean bool2 = za3.d;
        if (bool2 != null) {
            this.c.set(a2, bool2.booleanValue());
        }
        if (za3.e != null) {
            Long l = this.e.get(Integer.valueOf(a2));
            long longValue = za3.e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.e.put(Integer.valueOf(a2), Long.valueOf(longValue));
            }
        }
        if (za3.f != null) {
            List list = this.f.get(Integer.valueOf(a2));
            if (list == null) {
                list = new ArrayList();
                this.f.put(Integer.valueOf(a2), list);
            }
            list.add(Long.valueOf(za3.f.longValue() / 1000));
        }
    }

    @DexIgnore
    public final kj2 a(int i, boolean z, List<Integer> list) {
        kj2.a aVar;
        List<tj2> list2;
        kj2 kj2 = this.b;
        if (kj2 == null) {
            aVar = kj2.v();
        } else {
            aVar = (kj2.a) kj2.j();
        }
        aVar.a(i);
        sj2.a A = sj2.A();
        A.b((Iterable<? extends Long>) ia3.a(this.c));
        A.a((Iterable<? extends Long>) ia3.a(this.d));
        A.c(a());
        Map<Integer, List<Long>> map = this.f;
        if (map == null) {
            list2 = Collections.emptyList();
        } else {
            ArrayList arrayList = new ArrayList(map.size());
            for (Integer next : this.f.keySet()) {
                tj2.a t = tj2.t();
                t.a(next.intValue());
                List<Long> list3 = this.f.get(next);
                if (list3 != null) {
                    Collections.sort(list3);
                    for (Long longValue : list3) {
                        t.a(longValue.longValue());
                    }
                }
                arrayList.add((tj2) t.i());
            }
            list2 = arrayList;
        }
        if (aVar.j() && (!iu2.a() || !this.g.l().d(this.a, l03.B0) || !z)) {
            list2 = a(aVar.k().t(), list2, list);
        }
        A.d(list2);
        aVar.a(A);
        return (kj2) aVar.i();
    }

    @DexIgnore
    public final List<lj2> a() {
        Map<Integer, Long> map = this.e;
        if (map == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Integer intValue : this.e.keySet()) {
            int intValue2 = intValue.intValue();
            lj2.a r = lj2.r();
            r.a(intValue2);
            r.a(this.e.get(Integer.valueOf(intValue2)).longValue());
            arrayList.add((lj2) r.i());
        }
        return arrayList;
    }

    @DexIgnore
    public static List<tj2> a(List<tj2> list, List<tj2> list2, List<Integer> list3) {
        if (list.isEmpty()) {
            return list2;
        }
        ArrayList arrayList = new ArrayList(list2);
        p4 p4Var = new p4();
        for (tj2 next : list) {
            if (next.n() && next.q() > 0) {
                p4Var.put(Integer.valueOf(next.o()), Long.valueOf(next.b(next.q() - 1)));
            }
        }
        for (int i = 0; i < arrayList.size(); i++) {
            tj2 tj2 = (tj2) arrayList.get(i);
            Long l = (Long) p4Var.remove(tj2.n() ? Integer.valueOf(tj2.o()) : null);
            if (l != null && (list3 == null || !list3.contains(Integer.valueOf(tj2.o())))) {
                ArrayList arrayList2 = new ArrayList();
                if (l.longValue() < tj2.b(0)) {
                    arrayList2.add(l);
                }
                arrayList2.addAll(tj2.p());
                tj2.a aVar = (tj2.a) tj2.j();
                aVar.j();
                aVar.a((Iterable<? extends Long>) arrayList2);
                arrayList.set(i, (tj2) aVar.i());
            }
        }
        for (Integer num : p4Var.keySet()) {
            tj2.a t = tj2.t();
            t.a(num.intValue());
            t.a(((Long) p4Var.get(num)).longValue());
            arrayList.add((tj2) t.i());
        }
        return arrayList;
    }
}
