package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e50 extends b50 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ k70 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<e50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final LinkedHashSet<k70> a(k70 k70) {
            LinkedHashSet<k70> linkedHashSet = new LinkedHashSet<>();
            if (k70 == null) {
                vd6.a(linkedHashSet, k70.values());
            } else {
                linkedHashSet.add(k70);
            }
            return linkedHashSet;
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return (e50) b50.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new e50[i];
        }
    }

    @DexIgnore
    public e50(byte b, byte b2, k70 k70) throws IllegalArgumentException {
        super(b, b2, CREATOR.a(k70), false, false, true);
        this.h = k70;
    }

    @DexIgnore
    public final k70 getDayOfWeek() {
        return this.h;
    }
}
