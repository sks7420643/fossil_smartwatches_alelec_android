package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public jz5(int i, int i2, int i3, int i4, int i5, int i6) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final int c() {
        return this.f;
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof jz5)) {
            return false;
        }
        jz5 jz5 = (jz5) obj;
        return this.a == jz5.a && this.b == jz5.b && this.c == jz5.c && this.d == jz5.d && this.e == jz5.e && this.f == jz5.f;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((d.a(this.a) * 31) + d.a(this.b)) * 31) + d.a(this.c)) * 31) + d.a(this.d)) * 31) + d.a(this.e)) * 31) + d.a(this.f);
    }

    @DexIgnore
    public String toString() {
        return "TodayHeartRateModel(value=" + this.a + ", minValue=" + this.b + ", maxValue=" + this.c + ", startTime=" + this.d + ", endTime=" + this.e + ", midTime=" + this.f + ")";
    }
}
