package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dz2> CREATOR; // = new kz2();
    @DexIgnore
    public static /* final */ dz2 b; // = new dz2(0);
    @DexIgnore
    public /* final */ int a;

    /*
    static {
        Class<dz2> cls = dz2.class;
        new dz2(1);
    }
    */

    @DexIgnore
    public dz2(int i) {
        this.a = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof dz2) && this.a == ((dz2) obj).a;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(Integer.valueOf(this.a));
    }

    @DexIgnore
    public final String toString() {
        String str;
        int i = this.a;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", new Object[]{Integer.valueOf(i)});
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", new Object[]{str});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a);
        g22.a(parcel, a2);
    }
}
