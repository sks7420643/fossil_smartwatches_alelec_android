package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j03 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<j03> CREATOR; // = new m03();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ i03 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public j03(String str, i03 i03, String str2, long j) {
        this.a = str;
        this.b = i03;
        this.c = str2;
        this.d = j;
    }

    @DexIgnore
    public final String toString() {
        String str = this.c;
        String str2 = this.a;
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a, false);
        g22.a(parcel, 3, (Parcelable) this.b, i, false);
        g22.a(parcel, 4, this.c, false);
        g22.a(parcel, 5, this.d);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public j03(j03 j03, long j) {
        w12.a(j03);
        this.a = j03.a;
        this.b = j03.b;
        this.c = j03.c;
        this.d = j;
    }
}
