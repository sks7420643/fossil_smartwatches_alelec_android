package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nx0 {
    @DexIgnore
    public /* synthetic */ nx0(qg6 qg6) {
    }

    @DexIgnore
    public final hz0 a(byte b) {
        hz0 hz0;
        hz0[] values = hz0.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                hz0 = null;
                break;
            }
            hz0 = values[i];
            if (hz0.b == b) {
                break;
            }
            i++;
        }
        return hz0 != null ? hz0 : hz0.UNKNOWN;
    }
}
