package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cy2 extends kh2 implements by2 {
    @DexIgnore
    public cy2() {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        rx2 rx2;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            rx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IGoogleMapDelegate");
            if (queryLocalInterface instanceof rx2) {
                rx2 = queryLocalInterface;
            } else {
                rx2 = new qy2(readStrongBinder);
            }
        }
        a(rx2);
        parcel2.writeNoException();
        return true;
    }
}
