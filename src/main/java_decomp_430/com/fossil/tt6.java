package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt6 implements kt6 {
    @DexIgnore
    public /* final */ jt6 a; // = new jt6();
    @DexIgnore
    public /* final */ yt6 b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public tt6(yt6 yt6) {
        if (yt6 != null) {
            this.b = yt6;
            return;
        }
        throw new NullPointerException("sink == null");
    }

    @DexIgnore
    public jt6 a() {
        return this.a;
    }

    @DexIgnore
    public au6 b() {
        return this.b.b();
    }

    @DexIgnore
    public kt6 c(long j) throws IOException {
        if (!this.c) {
            this.a.c(j);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.c) {
            try {
                if (this.a.b > 0) {
                    this.b.a(this.a, this.a.b);
                }
                th = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                this.b.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                }
            }
            this.c = true;
            if (th != null) {
                bu6.a(th);
                throw null;
            }
        }
    }

    @DexIgnore
    public kt6 d(long j) throws IOException {
        if (!this.c) {
            this.a.d(j);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void flush() throws IOException {
        if (!this.c) {
            jt6 jt6 = this.a;
            long j = jt6.b;
            if (j > 0) {
                this.b.a(jt6, j);
            }
            this.b.flush();
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.c;
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.b + ")";
    }

    @DexIgnore
    public kt6 write(byte[] bArr) throws IOException {
        if (!this.c) {
            this.a.write(bArr);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public kt6 writeByte(int i) throws IOException {
        if (!this.c) {
            this.a.writeByte(i);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public kt6 writeInt(int i) throws IOException {
        if (!this.c) {
            this.a.writeInt(i);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public kt6 writeShort(int i) throws IOException {
        if (!this.c) {
            this.a.writeShort(i);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void a(jt6 jt6, long j) throws IOException {
        if (!this.c) {
            this.a.a(jt6, j);
            c();
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OutputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void close() throws IOException {
            tt6.this.close();
        }

        @DexIgnore
        public void flush() throws IOException {
            tt6 tt6 = tt6.this;
            if (!tt6.c) {
                tt6.flush();
            }
        }

        @DexIgnore
        public String toString() {
            return tt6.this + ".outputStream()";
        }

        @DexIgnore
        public void write(int i) throws IOException {
            tt6 tt6 = tt6.this;
            if (!tt6.c) {
                tt6.a.writeByte((int) (byte) i);
                tt6.this.c();
                return;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public void write(byte[] bArr, int i, int i2) throws IOException {
            tt6 tt6 = tt6.this;
            if (!tt6.c) {
                tt6.a.write(bArr, i, i2);
                tt6.this.c();
                return;
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public kt6 c() throws IOException {
        if (!this.c) {
            long l = this.a.l();
            if (l > 0) {
                this.b.a(this.a, l);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public OutputStream d() {
        return new a();
    }

    @DexIgnore
    public kt6 write(byte[] bArr, int i, int i2) throws IOException {
        if (!this.c) {
            this.a.write(bArr, i, i2);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public kt6 a(mt6 mt6) throws IOException {
        if (!this.c) {
            this.a.a(mt6);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public int write(ByteBuffer byteBuffer) throws IOException {
        if (!this.c) {
            int write = this.a.write(byteBuffer);
            c();
            return write;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public kt6 a(String str) throws IOException {
        if (!this.c) {
            this.a.a(str);
            c();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public long a(zt6 zt6) throws IOException {
        if (zt6 != null) {
            long j = 0;
            while (true) {
                long b2 = zt6.b(this.a, 8192);
                if (b2 == -1) {
                    return j;
                }
                j += b2;
                c();
            }
        } else {
            throw new IllegalArgumentException("source == null");
        }
    }
}
