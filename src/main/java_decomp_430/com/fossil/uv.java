package com.fossil;

import com.fossil.jv;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uv implements jv<cv, InputStream> {
    @DexIgnore
    public static /* final */ wr<Integer> b; // = wr.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);
    @DexIgnore
    public /* final */ iv<cv, cv> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements kv<cv, InputStream> {
        @DexIgnore
        public /* final */ iv<cv, cv> a; // = new iv<>(500);

        @DexIgnore
        public jv<cv, InputStream> a(nv nvVar) {
            return new uv(this.a);
        }
    }

    @DexIgnore
    public uv(iv<cv, cv> ivVar) {
        this.a = ivVar;
    }

    @DexIgnore
    public boolean a(cv cvVar) {
        return true;
    }

    @DexIgnore
    public jv.a<InputStream> a(cv cvVar, int i, int i2, xr xrVar) {
        iv<cv, cv> ivVar = this.a;
        if (ivVar != null) {
            cv a2 = ivVar.a(cvVar, 0, 0);
            if (a2 == null) {
                this.a.a(cvVar, 0, 0, cvVar);
            } else {
                cvVar = a2;
            }
        }
        return new jv.a<>(cvVar, new ls(cvVar, ((Integer) xrVar.a(b)).intValue()));
    }
}
