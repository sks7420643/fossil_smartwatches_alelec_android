package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kz {

    @DexIgnore
    public enum a {
        RUNNING(false),
        PAUSED(false),
        CLEARED(false),
        SUCCESS(true),
        FAILED(true);
        
        @DexIgnore
        public /* final */ boolean isComplete;

        @DexIgnore
        public a(boolean z) {
            this.isComplete = z;
        }

        @DexIgnore
        public boolean isComplete() {
            return this.isComplete;
        }
    }

    @DexIgnore
    kz a();

    @DexIgnore
    void b(jz jzVar);

    @DexIgnore
    boolean c(jz jzVar);

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean d(jz jzVar);

    @DexIgnore
    void e(jz jzVar);

    @DexIgnore
    boolean f(jz jzVar);
}
