package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b82 implements Parcelable.Creator<h72> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        int i = 0;
        Boolean bool = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                str = f22.e(parcel, a);
            } else if (a2 == 2) {
                i = f22.q(parcel, a);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                bool = f22.j(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new h72(str, i, bool);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new h72[i];
    }
}
