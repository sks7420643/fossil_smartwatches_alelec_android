package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tx implements bs<qx> {
    @DexIgnore
    public /* final */ bs<Bitmap> b;

    @DexIgnore
    public tx(bs<Bitmap> bsVar) {
        q00.a(bsVar);
        this.b = bsVar;
    }

    @DexIgnore
    public rt<qx> a(Context context, rt<qx> rtVar, int i, int i2) {
        qx qxVar = rtVar.get();
        hw hwVar = new hw(qxVar.e(), wq.a(context).c());
        rt<Bitmap> a = this.b.a(context, hwVar, i, i2);
        if (!hwVar.equals(a)) {
            hwVar.a();
        }
        qxVar.a(this.b, a.get());
        return rtVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof tx) {
            return this.b.equals(((tx) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
