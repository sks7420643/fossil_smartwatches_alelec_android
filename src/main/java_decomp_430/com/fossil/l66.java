package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l66 extends AsyncTask<Uri, Void, List<BelvedereResult>> {
    @DexIgnore
    public /* final */ BelvedereCallback<List<BelvedereResult>> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ k66 c;
    @DexIgnore
    public /* final */ n66 d;

    @DexIgnore
    public l66(Context context, k66 k66, n66 n66, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        this.b = context;
        this.c = k66;
        this.d = n66;
        this.a = belvedereCallback;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x012a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x012a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00e9 A[SYNTHETIC, Splitter:B:61:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f6 A[SYNTHETIC, Splitter:B:66:0x00f6] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0112 A[SYNTHETIC, Splitter:B:74:0x0112] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x011f A[SYNTHETIC, Splitter:B:79:0x011f] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0133 A[SYNTHETIC, Splitter:B:87:0x0133] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0140 A[SYNTHETIC, Splitter:B:92:0x0140] */
    /* renamed from: a */
    public List<BelvedereResult> doInBackground(Uri... uriArr) {
        FileOutputStream fileOutputStream;
        InputStream inputStream;
        Throwable th;
        InputStream inputStream2;
        InputStream inputStream3;
        FileOutputStream fileOutputStream2;
        ArrayList arrayList = new ArrayList();
        for (Uri uri : uriArr) {
            try {
                inputStream = this.b.getContentResolver().openInputStream(uri);
                try {
                    File c2 = this.d.c(this.b, uri);
                    if (inputStream != null) {
                        if (c2 != null) {
                            this.c.d("BelvedereResolveUriTask", String.format(Locale.US, "Copying media file into private cache - Uri: %s - Dest: %s", new Object[]{uri, c2}));
                            FileOutputStream fileOutputStream3 = new FileOutputStream(c2);
                            try {
                                byte[] bArr = new byte[1024];
                                while (true) {
                                    int read = inputStream.read(bArr);
                                    if (read <= 0) {
                                        break;
                                    }
                                    fileOutputStream3.write(bArr, 0, read);
                                }
                                arrayList.add(new BelvedereResult(c2, this.d.a(this.b, c2)));
                                if (inputStream != null) {
                                    try {
                                        inputStream.close();
                                    } catch (IOException e) {
                                        this.c.e("BelvedereResolveUriTask", "Error closing InputStream", e);
                                    }
                                }
                            } catch (FileNotFoundException e2) {
                                e = e2;
                                fileOutputStream2 = fileOutputStream3;
                                inputStream3 = inputStream;
                                this.c.e("BelvedereResolveUriTask", String.format(Locale.US, "File not found error copying file, uri: %s", new Object[]{uri}), e);
                                if (inputStream2 != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                            } catch (IOException e3) {
                                e = e3;
                                fileOutputStream = fileOutputStream3;
                                inputStream2 = inputStream;
                                try {
                                    this.c.e("BelvedereResolveUriTask", String.format(Locale.US, "IO Error copying file, uri: %s", new Object[]{uri}), e);
                                    if (inputStream2 != null) {
                                    }
                                    if (fileOutputStream != null) {
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    inputStream = inputStream2;
                                    if (inputStream != null) {
                                    }
                                    if (fileOutputStream != null) {
                                    }
                                    throw th;
                                }
                            } catch (Throwable th3) {
                                th = th3;
                                fileOutputStream = fileOutputStream3;
                                if (inputStream != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                throw th;
                            }
                            try {
                                fileOutputStream3.close();
                            } catch (IOException e4) {
                                this.c.e("BelvedereResolveUriTask", "Error closing FileOutputStream", e4);
                            }
                        }
                    }
                    k66 k66 = this.c;
                    Locale locale = Locale.US;
                    Object[] objArr = new Object[2];
                    objArr[0] = Boolean.valueOf(inputStream == null);
                    objArr[1] = Boolean.valueOf(c2 == null);
                    k66.w("BelvedereResolveUriTask", String.format(locale, "Unable to resolve uri. InputStream null = %s, File null = %s", objArr));
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e5) {
                            this.c.e("BelvedereResolveUriTask", "Error closing InputStream", e5);
                        }
                    }
                } catch (FileNotFoundException e6) {
                    e = e6;
                    inputStream3 = inputStream;
                    fileOutputStream2 = null;
                    this.c.e("BelvedereResolveUriTask", String.format(Locale.US, "File not found error copying file, uri: %s", new Object[]{uri}), e);
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e7) {
                            this.c.e("BelvedereResolveUriTask", "Error closing InputStream", e7);
                        }
                    }
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (IOException e8) {
                    e = e8;
                    inputStream2 = inputStream;
                    fileOutputStream = null;
                    this.c.e("BelvedereResolveUriTask", String.format(Locale.US, "IO Error copying file, uri: %s", new Object[]{uri}), e);
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e9) {
                            this.c.e("BelvedereResolveUriTask", "Error closing InputStream", e9);
                        }
                    }
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (Throwable th4) {
                    th = th4;
                    fileOutputStream = null;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e10) {
                            this.c.e("BelvedereResolveUriTask", "Error closing InputStream", e10);
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e11) {
                            this.c.e("BelvedereResolveUriTask", "Error closing FileOutputStream", e11);
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e12) {
                e = e12;
                inputStream3 = null;
                fileOutputStream2 = null;
                this.c.e("BelvedereResolveUriTask", String.format(Locale.US, "File not found error copying file, uri: %s", new Object[]{uri}), e);
                if (inputStream2 != null) {
                }
                if (fileOutputStream != null) {
                }
            } catch (IOException e13) {
                e = e13;
                inputStream2 = null;
                fileOutputStream = null;
                this.c.e("BelvedereResolveUriTask", String.format(Locale.US, "IO Error copying file, uri: %s", new Object[]{uri}), e);
                if (inputStream2 != null) {
                }
                if (fileOutputStream != null) {
                }
            } catch (Throwable th5) {
                th = th5;
                inputStream = null;
                fileOutputStream = null;
                if (inputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                throw th;
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public void onPostExecute(List<BelvedereResult> list) {
        super.onPostExecute(list);
        BelvedereCallback<List<BelvedereResult>> belvedereCallback = this.a;
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(list);
        }
    }
}
