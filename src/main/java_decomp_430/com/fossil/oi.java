package com.fossil;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.fossil.ji;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oi implements ji {
    @DexIgnore
    public /* final */ a a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends SQLiteOpenHelper {
        @DexIgnore
        public /* final */ ni[] a;
        @DexIgnore
        public /* final */ ji.a b;
        @DexIgnore
        public boolean c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.oi$a$a")
        /* renamed from: com.fossil.oi$a$a  reason: collision with other inner class name */
        public class C0034a implements DatabaseErrorHandler {
            @DexIgnore
            public /* final */ /* synthetic */ ji.a a;
            @DexIgnore
            public /* final */ /* synthetic */ ni[] b;

            @DexIgnore
            public C0034a(ji.a aVar, ni[] niVarArr) {
                this.a = aVar;
                this.b = niVarArr;
            }

            @DexIgnore
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.a.b(a.a(this.b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public a(Context context, String str, ni[] niVarArr, ji.a aVar) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, aVar.a, new C0034a(aVar, niVarArr));
            this.b = aVar;
            this.a = niVarArr;
        }

        @DexIgnore
        public ni a(SQLiteDatabase sQLiteDatabase) {
            return a(this.a, sQLiteDatabase);
        }

        @DexIgnore
        public synchronized void close() {
            super.close();
            this.a[0] = null;
        }

        @DexIgnore
        public synchronized ii k() {
            this.c = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.c) {
                close();
                return k();
            }
            return a(writableDatabase);
        }

        @DexIgnore
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.b.a((ii) a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.b.c(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.c = true;
            this.b.a(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.c) {
                this.b.d(a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.c = true;
            this.b.b(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public static ni a(ni[] niVarArr, SQLiteDatabase sQLiteDatabase) {
            ni niVar = niVarArr[0];
            if (niVar == null || !niVar.a(sQLiteDatabase)) {
                niVarArr[0] = new ni(sQLiteDatabase);
            }
            return niVarArr[0];
        }
    }

    @DexIgnore
    public oi(Context context, String str, ji.a aVar) {
        this.a = a(context, str, aVar);
    }

    @DexIgnore
    public final a a(Context context, String str, ji.a aVar) {
        return new a(context, str, new ni[1], aVar);
    }

    @DexIgnore
    public void close() {
        this.a.close();
    }

    @DexIgnore
    public String getDatabaseName() {
        return this.a.getDatabaseName();
    }

    @DexIgnore
    public void a(boolean z) {
        this.a.setWriteAheadLoggingEnabled(z);
    }

    @DexIgnore
    public ii a() {
        return this.a.k();
    }
}
