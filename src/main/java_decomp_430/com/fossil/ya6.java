package com.fossil;

import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ya6 implements X509TrustManager {
    @DexIgnore
    public static /* final */ X509Certificate[] f; // = new X509Certificate[0];
    @DexIgnore
    public /* final */ TrustManager[] a;
    @DexIgnore
    public /* final */ za6 b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ List<byte[]> d; // = new LinkedList();
    @DexIgnore
    public /* final */ Set<X509Certificate> e; // = Collections.synchronizedSet(new HashSet());

    @DexIgnore
    public ya6(za6 za6, xa6 xa6) {
        this.a = a(za6);
        this.b = za6;
        this.c = xa6.d();
        for (String a2 : xa6.c()) {
            this.d.add(a(a2));
        }
    }

    @DexIgnore
    public final TrustManager[] a(za6 za6) {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance("X509");
            instance.init(za6.a);
            return instance.getTrustManagers();
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        } catch (KeyStoreException e3) {
            throw new AssertionError(e3);
        }
    }

    @DexIgnore
    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        throw new CertificateException("Client certificates not supported!");
    }

    @DexIgnore
    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        if (!this.e.contains(x509CertificateArr[0])) {
            a(x509CertificateArr, str);
            a(x509CertificateArr);
            this.e.add(x509CertificateArr[0]);
        }
    }

    @DexIgnore
    public X509Certificate[] getAcceptedIssuers() {
        return f;
    }

    @DexIgnore
    public final boolean a(X509Certificate x509Certificate) throws CertificateException {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(x509Certificate.getPublicKey().getEncoded());
            for (byte[] equals : this.d) {
                if (Arrays.equals(equals, digest)) {
                    return true;
                }
            }
            return false;
        } catch (NoSuchAlgorithmException e2) {
            throw new CertificateException(e2);
        }
    }

    @DexIgnore
    public final void a(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        for (TrustManager trustManager : this.a) {
            ((X509TrustManager) trustManager).checkServerTrusted(x509CertificateArr, str);
        }
    }

    @DexIgnore
    public final void a(X509Certificate[] x509CertificateArr) throws CertificateException {
        if (this.c == -1 || System.currentTimeMillis() - this.c <= 15552000000L) {
            X509Certificate[] a2 = ra6.a(x509CertificateArr, this.b);
            int length = a2.length;
            int i = 0;
            while (i < length) {
                if (!a(a2[i])) {
                    i++;
                } else {
                    return;
                }
            }
            throw new CertificateException("No valid pins found in chain!");
        }
        l86 g = c86.g();
        g.w("Fabric", "Certificate pins are stale, (" + (System.currentTimeMillis() - this.c) + " millis vs " + 15552000000L + " millis) falling back to system trust.");
    }

    @DexIgnore
    public final byte[] a(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }
}
