package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr2 implements as2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;
    @DexIgnore
    public static /* final */ pk2<Boolean> c;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.sdk.dynamite.allow_remote_dynamite", false);
        b = vk2.a("measurement.collection.init_params_control_enabled", true);
        c = vk2.a("measurement.sdk.dynamite.use_dynamite2", false);
        vk2.a("measurement.id.sdk.dynamite.use_dynamite", 0);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzb() {
        return b.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return c.b().booleanValue();
    }
}
