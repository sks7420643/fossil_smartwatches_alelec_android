package com.fossil;

import com.fossil.fj2;
import com.fossil.fn2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj2 extends fn2<gj2, a> implements to2 {
    @DexIgnore
    public static /* final */ gj2 zzl;
    @DexIgnore
    public static volatile yo2<gj2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public int zzf;
    @DexIgnore
    public nn2<hj2> zzg; // = fn2.m();
    @DexIgnore
    public nn2<fj2> zzh; // = fn2.m();
    @DexIgnore
    public nn2<vi2> zzi; // = fn2.m();
    @DexIgnore
    public String zzj; // = "";
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<gj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(gj2.zzl);
        }

        @DexIgnore
        public final fj2 a(int i) {
            return ((gj2) this.b).b(i);
        }

        @DexIgnore
        public final int j() {
            return ((gj2) this.b).s();
        }

        @DexIgnore
        public final List<vi2> k() {
            return Collections.unmodifiableList(((gj2) this.b).t());
        }

        @DexIgnore
        public final a l() {
            f();
            ((gj2) this.b).w();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(jj2 jj2) {
            this();
        }

        @DexIgnore
        public final a a(int i, fj2.a aVar) {
            f();
            ((gj2) this.b).a(i, aVar);
            return this;
        }
    }

    /*
    static {
        gj2 gj2 = new gj2();
        zzl = gj2;
        fn2.a(gj2.class, gj2);
    }
    */

    @DexIgnore
    public static a x() {
        return (a) zzl.h();
    }

    @DexIgnore
    public static gj2 y() {
        return zzl;
    }

    @DexIgnore
    public final void a(int i, fj2.a aVar) {
        if (!this.zzh.zza()) {
            this.zzh = fn2.a(this.zzh);
        }
        this.zzh.set(i, (fj2) aVar.i());
    }

    @DexIgnore
    public final fj2 b(int i) {
        return this.zzh.get(i);
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final long o() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean p() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String q() {
        return this.zze;
    }

    @DexIgnore
    public final List<hj2> r() {
        return this.zzg;
    }

    @DexIgnore
    public final int s() {
        return this.zzh.size();
    }

    @DexIgnore
    public final List<vi2> t() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean v() {
        return this.zzk;
    }

    @DexIgnore
    public final void w() {
        this.zzi = fn2.m();
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (jj2.a[i - 1]) {
            case 1:
                return new gj2();
            case 2:
                return new a((jj2) null);
            case 3:
                return fn2.a((ro2) zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001\u0002\u0000\u0002\b\u0001\u0003\u0004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\b\u0003\b\u0007\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", hj2.class, "zzh", fj2.class, "zzi", vi2.class, "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                yo2<gj2> yo2 = zzm;
                if (yo2 == null) {
                    synchronized (gj2.class) {
                        yo2 = zzm;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzl);
                            zzm = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
