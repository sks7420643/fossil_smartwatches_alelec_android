package com.fossil;

import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z35 extends xt4<y35> {
    @DexIgnore
    void a(m35 m35, DianaComplicationRingStyle dianaComplicationRingStyle);

    @DexIgnore
    void a(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    void a(String str, String str2, String str3, boolean z);

    @DexIgnore
    void c();

    @DexIgnore
    void d(String str);

    @DexIgnore
    void e(boolean z);

    @DexIgnore
    void f(boolean z);

    @DexIgnore
    void g(int i);

    @DexIgnore
    void m();

    @DexIgnore
    void n();

    @DexIgnore
    void p();

    @DexIgnore
    void q();

    @DexIgnore
    void s(String str);

    @DexIgnore
    void u(String str);
}
