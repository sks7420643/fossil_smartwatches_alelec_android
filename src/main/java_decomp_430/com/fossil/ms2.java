package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms2 implements ns2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.service.fix_gmp_version", false);
        vk2.a("measurement.id.service.fix_gmp_version", 0);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
