package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek6<T> extends ck6<T> {
    @DexIgnore
    public /* final */ Thread d;
    @DexIgnore
    public /* final */ em6 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ek6(af6 af6, Thread thread, em6 em6) {
        super(af6, true);
        wg6.b(af6, "parentContext");
        wg6.b(thread, "blockedThread");
        this.d = thread;
        this.e = em6;
    }

    @DexIgnore
    public void a(Object obj, int i) {
        if (!wg6.a((Object) Thread.currentThread(), (Object) this.d)) {
            LockSupport.unpark(this.d);
        }
    }

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public final T p() {
        pn6 a = qn6.a();
        if (a != null) {
            a.b();
        }
        try {
            em6 em6 = this.e;
            T t = null;
            if (em6 != null) {
                em6.b(em6, false, 1, (Object) null);
            }
            while (!Thread.interrupted()) {
                em6 em62 = this.e;
                long C = em62 != null ? em62.C() : ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
                if (e()) {
                    em6 em63 = this.e;
                    if (em63 != null) {
                        em6.a(em63, false, 1, (Object) null);
                    }
                    pn6 a2 = qn6.a();
                    if (a2 != null) {
                        a2.d();
                    }
                    T b = zm6.b(d());
                    if (b instanceof vk6) {
                        t = b;
                    }
                    vk6 vk6 = (vk6) t;
                    if (vk6 == null) {
                        return b;
                    }
                    throw vk6.a;
                }
                pn6 a3 = qn6.a();
                if (a3 != null) {
                    a3.a(this, C);
                } else {
                    LockSupport.parkNanos(this, C);
                }
            }
            InterruptedException interruptedException = new InterruptedException();
            a((Throwable) interruptedException);
            throw interruptedException;
        } catch (Throwable th) {
            pn6 a4 = qn6.a();
            if (a4 != null) {
                a4.d();
            }
            throw th;
        }
    }
}
