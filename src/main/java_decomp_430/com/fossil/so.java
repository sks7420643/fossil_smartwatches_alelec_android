package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so<V> extends qo<V> {
    @DexIgnore
    public static <V> so<V> e() {
        return new so<>();
    }

    @DexIgnore
    public boolean a(Throwable th) {
        return super.a(th);
    }

    @DexIgnore
    public boolean b(V v) {
        return super.b(v);
    }

    @DexIgnore
    public boolean a(ip3<? extends V> ip3) {
        return super.a(ip3);
    }
}
