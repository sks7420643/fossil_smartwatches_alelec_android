package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class co5 implements Factory<bo5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public co5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static co5 a(Provider<ThemeRepository> provider) {
        return new co5(provider);
    }

    @DexIgnore
    public static bo5 b(Provider<ThemeRepository> provider) {
        return new CustomizeRingChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeRingChartViewModel get() {
        return b(this.a);
    }
}
