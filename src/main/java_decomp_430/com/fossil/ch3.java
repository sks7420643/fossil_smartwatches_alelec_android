package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ch3<S> extends Parcelable {
    @DexIgnore
    View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, zg3 zg3, kh3<S> kh3);

    @DexIgnore
    String a(Context context);

    @DexIgnore
    int b(Context context);

    @DexIgnore
    void b(long j);

    @DexIgnore
    Collection<u8<Long, Long>> m();

    @DexIgnore
    boolean n();

    @DexIgnore
    Collection<Long> o();

    @DexIgnore
    S p();
}
