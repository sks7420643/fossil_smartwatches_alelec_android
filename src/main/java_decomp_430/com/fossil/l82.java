package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l82 implements Parcelable.Creator<h82> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        k72 k72 = null;
        ArrayList<DataSet> arrayList = null;
        ArrayList<DataPoint> arrayList2 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                k72 = (k72) f22.a(parcel, a, k72.CREATOR);
            } else if (a2 == 2) {
                arrayList = f22.c(parcel, a, DataSet.CREATOR);
            } else if (a2 == 3) {
                arrayList2 = f22.c(parcel, a, DataPoint.CREATOR);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                iBinder = f22.p(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new h82(k72, (List<DataSet>) arrayList, (List<DataPoint>) arrayList2, iBinder);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new h82[i];
    }
}
