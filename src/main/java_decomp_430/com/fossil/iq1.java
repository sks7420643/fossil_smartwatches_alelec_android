package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq1 implements Factory<hq1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<zs1> b;
    @DexIgnore
    public /* final */ Provider<zs1> c;

    @DexIgnore
    public iq1(Provider<Context> provider, Provider<zs1> provider2, Provider<zs1> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static iq1 a(Provider<Context> provider, Provider<zs1> provider2, Provider<zs1> provider3) {
        return new iq1(provider, provider2, provider3);
    }

    @DexIgnore
    public hq1 get() {
        return new hq1((Context) this.a.get(), (zs1) this.b.get(), (zs1) this.c.get());
    }
}
