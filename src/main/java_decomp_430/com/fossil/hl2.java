package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl2 {
    @DexIgnore
    public static <T> dl2<T> a(dl2<T> dl2) {
        if ((dl2 instanceof jl2) || (dl2 instanceof gl2)) {
            return dl2;
        }
        if (dl2 instanceof Serializable) {
            return new gl2(dl2);
        }
        return new jl2(dl2);
    }

    @DexIgnore
    public static <T> dl2<T> a(T t) {
        return new il2(t);
    }
}
