package com.fossil;

import com.fossil.rv1;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wy1 {
    @DexIgnore
    <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t);

    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    boolean a(yw1 yw1);

    @DexIgnore
    <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    boolean c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    gv1 f();
}
