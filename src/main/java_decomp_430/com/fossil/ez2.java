package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez2 implements Parcelable.Creator<CameraPosition> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        LatLng latLng = null;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                latLng = (LatLng) f22.a(parcel, a, LatLng.CREATOR);
            } else if (a2 == 3) {
                f = f22.n(parcel, a);
            } else if (a2 == 4) {
                f2 = f22.n(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                f3 = f22.n(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new CameraPosition(latLng, f, f2, f3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new CameraPosition[i];
    }
}
