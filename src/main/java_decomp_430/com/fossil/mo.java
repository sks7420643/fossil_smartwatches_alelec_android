package com.fossil;

import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mo implements Runnable {
    @DexIgnore
    public lm a;
    @DexIgnore
    public String b;
    @DexIgnore
    public WorkerParameters.a c;

    @DexIgnore
    public mo(lm lmVar, String str, WorkerParameters.a aVar) {
        this.a = lmVar;
        this.b = str;
        this.c = aVar;
    }

    @DexIgnore
    public void run() {
        this.a.e().a(this.b, this.c);
    }
}
