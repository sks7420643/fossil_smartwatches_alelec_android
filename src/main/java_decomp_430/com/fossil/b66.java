package com.fossil;

import android.content.Context;
import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b66 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;

    @DexIgnore
    public b66(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void run() {
        b46.a(q36.r).h();
        m56.a(this.a, true);
        o46.b(this.a);
        y56.b(this.a);
        Thread.UncaughtExceptionHandler unused = q36.n = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new i46());
        if (n36.o() == o36.APP_LAUNCH) {
            q36.a(this.a, -1);
        }
        if (n36.q()) {
            q36.m.a((Object) "Init MTA StatService success.");
        }
    }
}
