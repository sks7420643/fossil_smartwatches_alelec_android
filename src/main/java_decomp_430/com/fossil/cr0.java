package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr0 extends uj1 {
    @DexIgnore
    public static /* final */ kp0 CREATOR; // = new kp0((qg6) null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public /* synthetic */ cr0(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readInt() != 0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.ACTIVITY_ID, (Object) Byte.valueOf(this.b)), bm0.IS_RESUME, (Object) Boolean.valueOf(this.c));
    }

    @DexIgnore
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b);
        order.put(this.c ? (byte) 1 : 0);
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(cr0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            cr0 cr0 = (cr0) obj;
            return this.c == cr0.c && this.b == cr0.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.SwitchActivityInstr");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.c).hashCode() + (this.b * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }
}
