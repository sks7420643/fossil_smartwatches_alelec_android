package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z90 extends x90 {
    @DexIgnore
    public /* final */ vd0 d;

    @DexIgnore
    public z90(e90 e90, byte b, vd0 vd0) {
        super(e90, b, cw0.b(vd0.getRequestId()));
        this.d = vd0;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a = super.a();
        try {
            cw0.a(a, bm0.MICRO_APP_EVENT, (Object) this.d);
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return a;
    }

    @DexIgnore
    public final vd0 d() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(getClass(), obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.d, ((z90) obj).d) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
    }

    @DexIgnore
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public z90(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(vd0.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (vd0) readParcelable;
        } else {
            wg6.a();
            throw null;
        }
    }
}
