package com.fossil;

import android.content.Context;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx2 {
    @DexIgnore
    public static boolean a;

    @DexIgnore
    public static synchronized int a(Context context) {
        synchronized (kx2.class) {
            w12.a(context, (Object) "Context is null");
            if (a) {
                return 0;
            }
            try {
                oy2 a2 = ly2.a(context);
                try {
                    ix2.a(a2.zze());
                    yy2.a(a2.zzf());
                    a = true;
                    return 0;
                } catch (RemoteException e) {
                    throw new bz2(e);
                }
            } catch (lv1 e2) {
                return e2.errorCode;
            }
        }
    }
}
