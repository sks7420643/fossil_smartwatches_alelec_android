package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.fossil.k40;
import com.fossil.q40;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj0 {
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<ii1> a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<ii1> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<ii1> c; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<ii1> d; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ Handler e;
    @DexIgnore
    public static /* final */ BroadcastReceiver f; // = new zh1();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new rh0();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new zf0();
    @DexIgnore
    public static /* final */ kj0 i; // = new kj0();

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            e = new Handler(myLooper);
            BroadcastReceiver broadcastReceiver = f;
            String[] strArr = {"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"};
            Context a2 = gk0.f.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (String addAction : strArr) {
                    intentFilter.addAction(addAction);
                }
                ce.a(a2).a(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = g;
            String[] strArr2 = {"com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED"};
            Context a3 = gk0.f.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (String addAction2 : strArr2) {
                    intentFilter2.addAction(addAction2);
                }
                ce.a(a3).a(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = h;
            String[] strArr3 = {"com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED"};
            Context a4 = gk0.f.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (String addAction3 : strArr3) {
                    intentFilter3.addAction(addAction3);
                }
                ce.a(a4).a(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public final long a(boolean z) {
        return 0;
    }

    @DexIgnore
    public final void a(k40.c cVar) {
        if (eg1.a[cVar.ordinal()] == 1) {
            for (ii1 ii1 : a) {
                kj0 kj0 = i;
                wg6.a(ii1, "it");
                kj0.a(ii1);
            }
            for (ii1 ii12 : b) {
                kj0 kj02 = i;
                wg6.a(ii12, "it");
                a(kj02, ii12, 0, 2);
            }
        }
    }

    @DexIgnore
    public final boolean b(ii1 ii1) {
        return a.contains(ii1);
    }

    @DexIgnore
    public final boolean c(ii1 ii1) {
        return b.contains(ii1);
    }

    @DexIgnore
    public final void d(ii1 ii1) {
        b.remove(ii1);
    }

    @DexIgnore
    public final boolean e(ii1 ii1) {
        a.add(ii1);
        a(ii1);
        return true;
    }

    @DexIgnore
    public final boolean f(ii1 ii1) {
        a.remove(ii1);
        return true;
    }

    @DexIgnore
    public final void a(ii1 ii1, q40.d dVar) {
        int i2 = eg1.b[dVar.ordinal()];
        if (i2 == 1) {
            return;
        }
        if (i2 == 2) {
            a(ii1, 0);
        } else if (i2 == 3) {
            yj1.b.b("HID_EXPONENT_BACK_OFF_TAG");
            a(ii1, 0);
        }
    }

    @DexIgnore
    public final void a(ii1 ii1) {
        if (a.contains(ii1) && ii1.s == q40.c.DISCONNECTED) {
            e.postDelayed(new tj1(ii1), 200);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(kj0 kj0, ii1 ii1, long j, int i2) {
        if ((i2 & 2) != 0) {
            j = 0;
        }
        kj0.a(ii1, j);
    }

    @DexIgnore
    public final void a(ii1 ii1, long j) {
        e.postDelayed(new ie0(ii1), j);
    }
}
