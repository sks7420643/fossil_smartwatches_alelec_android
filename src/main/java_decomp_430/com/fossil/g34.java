package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g34 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ String a; // = ThemeManager.l.a().b("onPrimaryButton");
    @DexIgnore
    public ArrayList<n35> b;
    @DexIgnore
    public d c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ g34 c;

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v1, types: [android.view.View, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r3v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(g34 g34, View view) {
            super(view);
            wg6.b(view, "view");
            this.c = g34;
            View findViewById = view.findViewById(2131361955);
            wg6.a((Object) findViewById, "view.findViewById(R.id.btn_add)");
            this.a = (FlexibleButton) findViewById;
            View findViewById2 = view.findViewById(2131363113);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.b = (TextView) findViewById2;
            if (!TextUtils.isEmpty(g34.c())) {
                int parseColor = Color.parseColor(g34.c());
                Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231027);
                if (drawable != null) {
                    drawable.setTint(parseColor);
                    this.a.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                }
            }
            cl4.a(this.a, this);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r3v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final void a(boolean z) {
            if (z) {
                this.a.a("flexible_button_disabled");
                this.a.setClickable(false);
                this.b.setText(PortfolioApp.get.instance().getString(2131886385));
                return;
            }
            this.a.a("flexible_button_primary");
            this.a.setClickable(true);
            this.b.setText(PortfolioApp.get.instance().getString(2131886372));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == 2131361955) {
                this.c.c.H0();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void G0();

        @DexIgnore
        void H0();

        @DexIgnore
        void a(n35 n35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(String str, String str2);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public g34(ArrayList<n35> arrayList, d dVar) {
        wg6.b(arrayList, "mData");
        wg6.b(dVar, "mListener");
        this.b = arrayList;
        this.c = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) this.b.get(i).hashCode();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == this.b.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            n35 n35 = this.b.get(i);
            wg6.a((Object) n35, "mData[position]");
            n35 n352 = n35;
            if (i != 0) {
                z = false;
            }
            bVar.a(n352, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.b.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558672, viewGroup, false);
            wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558652, viewGroup, false);
        wg6.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(ArrayList<n35> arrayList) {
        wg6.b(arrayList, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "setData - data=" + arrayList);
        this.b.clear();
        this.b.addAll(arrayList);
        this.b.add(new n35("", "", new ArrayList(), false));
        notifyDataSetChanged();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public CustomizeWidget b;
        @DexIgnore
        public CustomizeWidget c;
        @DexIgnore
        public CustomizeWidget d;
        @DexIgnore
        public TextView e;
        @DexIgnore
        public View f;
        @DexIgnore
        public FlexibleButton g;
        @DexIgnore
        public ImageView h;
        @DexIgnore
        public n35 i;
        @DexIgnore
        public View j;
        @DexIgnore
        public View o;
        @DexIgnore
        public View p;
        @DexIgnore
        public View q;
        @DexIgnore
        public /* final */ /* synthetic */ g34 r;

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v38, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r2v39, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v40, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v41, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(g34 g34, View view) {
            super(view);
            wg6.b(view, "view");
            this.r = g34;
            View findViewById = view.findViewById(2131363193);
            wg6.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            this.a = (TextView) findViewById;
            this.a.setOnClickListener(this);
            View findViewById2 = view.findViewById(2131363330);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.wa_top)");
            this.b = (CustomizeWidget) findViewById2;
            View findViewById3 = view.findViewById(2131363329);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.wa_middle)");
            this.c = (CustomizeWidget) findViewById3;
            View findViewById4 = view.findViewById(2131363328);
            wg6.a((Object) findViewById4, "view.findViewById(R.id.wa_bottom)");
            this.d = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(2131362640);
            wg6.a((Object) findViewById5, "view.findViewById(R.id.iv_watch_theme_background)");
            this.h = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(2131363278);
            wg6.a((Object) findViewById6, "view.findViewById(R.id.v_underline)");
            this.q = findViewById6;
            View findViewById7 = view.findViewById(2131362658);
            wg6.a((Object) findViewById7, "view.findViewById(R.id.line_bottom)");
            this.p = findViewById7;
            View findViewById8 = view.findViewById(2131362659);
            wg6.a((Object) findViewById8, "view.findViewById(R.id.line_center)");
            this.o = findViewById8;
            View findViewById9 = view.findViewById(2131362660);
            wg6.a((Object) findViewById9, "view.findViewById(R.id.line_top)");
            this.j = findViewById9;
            String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                int parseColor = Color.parseColor(b2);
                this.q.setBackgroundColor(parseColor);
                this.p.setBackgroundColor(parseColor);
                this.o.setBackgroundColor(parseColor);
                this.j.setBackgroundColor(parseColor);
            }
            View findViewById10 = view.findViewById(2131362650);
            wg6.a((Object) findViewById10, "view.findViewById(R.id.layout_right)");
            this.f = findViewById10;
            View findViewById11 = view.findViewById(2131361960);
            wg6.a((Object) findViewById11, "view.findViewById(R.id.btn_right)");
            this.g = (FlexibleButton) findViewById11;
            View findViewById12 = view.findViewById(2131363165);
            wg6.a((Object) findViewById12, "view.findViewById(R.id.tv_left)");
            this.e = (TextView) findViewById12;
            this.f.setOnClickListener(this);
            this.e.setOnClickListener(this);
            this.g.setOnClickListener(this);
            this.b.setOnClickListener(this);
            this.c.setOnClickListener(this);
            this.d.setOnClickListener(this);
            if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.get.instance().e())) {
                this.h.setImageResource(2131231013);
            }
        }

        @DexIgnore
        public final List<u8<View, String>> a() {
            u8[] u8VarArr = new u8[6];
            TextView textView = this.a;
            u8VarArr[0] = new u8(textView, textView.getTransitionName());
            View view = this.q;
            u8VarArr[1] = new u8(view, view.getTransitionName());
            ImageView imageView = this.h;
            if (imageView != null) {
                u8VarArr[2] = new u8(imageView, imageView.getTransitionName());
                View view2 = this.p;
                u8VarArr[3] = new u8(view2, view2.getTransitionName());
                View view3 = this.o;
                u8VarArr[4] = new u8(view3, view3.getTransitionName());
                View view4 = this.j;
                u8VarArr[5] = new u8(view4, view4.getTransitionName());
                return qd6.c(u8VarArr);
            }
            throw new rc6("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        public final List<u8<CustomizeWidget, String>> b() {
            Object r2 = this.b;
            Object r22 = this.c;
            Object r23 = this.d;
            return qd6.c(new u8(r2, r2.getTransitionName()), new u8(r22, r22.getTransitionName()), new u8(r23, r23.getTransitionName()));
        }

        @DexIgnore
        public void onClick(View view) {
            String str;
            String b2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.i == null) && view != null) {
                switch (view.getId()) {
                    case 2131361960:
                        this.r.c.G0();
                        return;
                    case 2131363165:
                        if (this.r.b.size() > 2) {
                            Object obj = this.r.b.get(1);
                            wg6.a(obj, "mData[1]");
                            n35 n35 = (n35) obj;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("current ");
                            n35 n352 = this.i;
                            sb.append(n352 != null ? n352.c() : null);
                            sb.append(" nextPreset ");
                            sb.append(n35.c());
                            local2.d("CustomizeAdapter", sb.toString());
                            d b3 = this.r.c;
                            n35 n353 = this.i;
                            if (n353 != null) {
                                boolean d2 = n353.d();
                                n35 n354 = this.i;
                                if (n354 != null) {
                                    b3.b(d2, n354.c(), n35.c(), n35.b());
                                    return;
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case 2131363193:
                        d b4 = this.r.c;
                        n35 n355 = this.i;
                        String str2 = "";
                        if (n355 == null || (str = n355.c()) == null) {
                            str = str2;
                        }
                        n35 n356 = this.i;
                        if (!(n356 == null || (b2 = n356.b()) == null)) {
                            str2 = b2;
                        }
                        b4.b(str, str2);
                        return;
                    case 2131363328:
                        this.r.c.a(this.i, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case 2131363329:
                        this.r.c.a(this.i, a(), b(), "middle", getAdapterPosition());
                        return;
                    case 2131363330:
                        this.r.c.a(this.i, a(), b(), "top", getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v13, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r6v15, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r6v19, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r6v32, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        public final void a(n35 n35, boolean z) {
            wg6.b(n35, "preset");
            if (z) {
                this.f.setSelected(true);
                this.g.setText(PortfolioApp.get.instance().getString(2131886381));
                this.g.a("flexible_button_right_applied");
                if (!TextUtils.isEmpty(this.r.c())) {
                    int parseColor = Color.parseColor(this.r.c());
                    Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231067);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        this.g.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                this.f.setClickable(false);
                if (this.r.b.size() == 2) {
                    this.e.setVisibility(4);
                }
            } else {
                this.f.setSelected(false);
                this.g.setText(PortfolioApp.get.instance().getString(2131886373));
                this.g.a("flexible_button_right_apply");
                this.g.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.f.setClickable(true);
                this.e.setVisibility(0);
            }
            this.i = n35;
            this.a.setText(n35.c());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(n35.a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "build with microApps=" + arrayList);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                o35 o35 = (o35) it.next();
                String c2 = o35.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && c2.equals("top")) {
                                this.b.c(o35.a());
                                this.b.h();
                            }
                        } else if (c2.equals("middle")) {
                            this.c.c(o35.a());
                            this.c.h();
                        }
                    } else if (c2.equals("bottom")) {
                        this.d.c(o35.a());
                        this.d.h();
                    }
                }
            }
        }
    }
}
