package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.fossil.a96;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y00 extends i86<Boolean> {
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public y10 h;

    @DexIgnore
    public static y00 o() {
        return c86.a(y00.class);
    }

    @DexIgnore
    public void a(j10 j10) {
        if (j10 == null) {
            throw new NullPointerException("event must not be null");
        } else if (this.g) {
            a("logCustom");
        } else {
            y10 y10 = this.h;
            if (y10 != null) {
                y10.a(j10);
            }
        }
    }

    @DexIgnore
    public String h() {
        return "com.crashlytics.sdk.android:answers";
    }

    @DexIgnore
    public String j() {
        return "1.4.7.32";
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public boolean m() {
        long lastModified;
        try {
            Context d = d();
            PackageManager packageManager = d.getPackageManager();
            String packageName = d.getPackageName();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            String num = Integer.toString(packageInfo.versionCode);
            String str = packageInfo.versionName == null ? "0.0" : packageInfo.versionName;
            if (Build.VERSION.SDK_INT >= 9) {
                lastModified = packageInfo.firstInstallTime;
            } else {
                lastModified = new File(packageManager.getApplicationInfo(packageName, 0).sourceDir).lastModified();
            }
            this.h = y10.a(this, d, g(), num, str, lastModified);
            this.h.c();
            this.g = new i96().e(d);
            return true;
        } catch (Exception e) {
            c86.g().e("Answers", "Error retrieving app properties", e);
            return false;
        }
    }

    @DexIgnore
    public String n() {
        return z86.b(d(), "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public Boolean c() {
        if (!c96.a(d()).a()) {
            c86.g().d("Fabric", "Analytics collection disabled, because data collection is disabled by Firebase.");
            this.h.b();
            return false;
        }
        try {
            yb6 a = vb6.d().a();
            if (a == null) {
                c86.g().e("Answers", "Failed to retrieve settings");
                return false;
            } else if (a.d.c) {
                c86.g().d("Answers", "Analytics collection enabled");
                this.h.a(a.e, n());
                return true;
            } else {
                c86.g().d("Answers", "Analytics collection disabled");
                this.h.b();
                return false;
            }
        } catch (Exception e) {
            c86.g().e("Answers", "Error dealing with settings", e);
            return false;
        }
    }

    @DexIgnore
    public void a(a96.a aVar) {
        y10 y10 = this.h;
        if (y10 != null) {
            y10.a(aVar.b(), aVar.a());
        }
    }

    @DexIgnore
    public final void a(String str) {
        l86 g2 = c86.g();
        g2.w("Answers", "Method " + str + " is not supported when using Crashlytics through Firebase.");
    }
}
