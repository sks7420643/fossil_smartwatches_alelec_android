package com.fossil;

import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
public final class d65$b$b extends sf6 implements ig6<il6, xe6<? super ArrayList<SecondTimezoneSetting>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $rawDataList;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<SecondTimezoneSetting> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        /* renamed from: a */
        public final int compare(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
            return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d65$b$b(ArrayList arrayList, xe6 xe6) {
        super(2, xe6);
        this.$rawDataList = arrayList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        d65$b$b d65_b_b = new d65$b$b(this.$rawDataList, xe6);
        d65_b_b.p$ = (il6) obj;
        return d65_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((d65$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ud6.a(this.$rawDataList, a.a);
            return this.$rawDataList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
