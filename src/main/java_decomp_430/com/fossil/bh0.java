package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bh0 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ ue1 a;

    @DexIgnore
    public bh0(ue1 ue1) {
        this.a = ue1;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        Intent intent2 = intent;
        if (intent2 != null && (action = intent.getAction()) != null && action.hashCode() == -1099894406 && action.equals("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED")) {
            m51 a2 = m51.f.a(intent2.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", 0));
            m51 a3 = m51.f.a(intent2.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", 0));
            if (wg6.a((BluetoothDevice) intent2.getParcelableExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE"), this.a.w)) {
                qs0 qs0 = qs0.h;
                m51 m51 = a3;
                nn0 nn0 = r4;
                nn0 nn02 = new nn0("hid_connection_state_changed", og0.CENTRAL_EVENT, this.a.t, "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.PREV_STATE, (Object) cw0.a((Enum<?>) a2)), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) a3)), 448);
                qs0.a(nn0);
                this.a.a(a2, m51);
            }
        }
    }
}
