package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ms1 implements rs1.b {
    @DexIgnore
    public /* final */ rs1 a;
    @DexIgnore
    public /* final */ rp1 b;
    @DexIgnore
    public /* final */ np1 c;

    @DexIgnore
    public ms1(rs1 rs1, rp1 rp1, np1 np1) {
        this.a = rs1;
        this.b = rp1;
        this.c = np1;
    }

    @DexIgnore
    public static rs1.b a(rs1 rs1, rp1 rp1, np1 np1) {
        return new ms1(rs1, rp1, np1);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return rs1.a(this.a, this.b, this.c, (SQLiteDatabase) obj);
    }
}
