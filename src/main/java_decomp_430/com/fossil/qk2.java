package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qk2 {
    @DexIgnore
    public static /* final */ p4<String, Uri> a; // = new p4<>();

    @DexIgnore
    public static synchronized Uri a(String str) {
        Uri uri;
        synchronized (qk2.class) {
            uri = a.get(str);
            if (uri == null) {
                String valueOf = String.valueOf(Uri.encode(str));
                uri = Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
                a.put(str, uri);
            }
        }
        return uri;
    }
}
