package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum xd1 {
    WORKOUT_SESSION_CONTROL(new byte[]{2});
    
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public xd1(byte[] bArr) {
        this.a = bArr;
    }
}
