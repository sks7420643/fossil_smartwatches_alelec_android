package com.fossil;

import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$2$1", f = "SleepDetailPresenter.kt", l = {109}, m = "invokeSuspend")
public final class bj5$h$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter.h this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$2$1$sessions$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super List<MFSleepSession>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bj5$h$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bj5$h$a bj5_h_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = bj5_h_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                SleepDetailPresenter sleepDetailPresenter = this.this$0.this$0.a;
                return sleepDetailPresenter.a(sleepDetailPresenter.f, (List<MFSleepSession>) this.this$0.this$0.a.m);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bj5$h$a(SleepDetailPresenter.h hVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = hVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        bj5$h$a bj5_h_a = new bj5$h$a(this.this$0, xe6);
        bj5_h_a.p$ = (il6) obj;
        return bj5_h_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((bj5$h$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 a3 = this.this$0.a.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(a3, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list = (List) obj;
        if (this.this$0.a.o == null || (!wg6.a((Object) this.this$0.a.o, (Object) list))) {
            this.this$0.a.o = list;
            if (this.this$0.a.i && this.this$0.a.j) {
                rm6 unused = this.this$0.a.l();
                rm6 unused2 = this.this$0.a.m();
            }
        }
        return cd6.a;
    }
}
