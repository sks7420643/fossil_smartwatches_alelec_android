package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class et<DataType, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ Class<DataType> a;
    @DexIgnore
    public /* final */ List<? extends zr<DataType, ResourceType>> b;
    @DexIgnore
    public /* final */ cy<ResourceType, Transcode> c;
    @DexIgnore
    public /* final */ v8<List<Throwable>> d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public interface a<ResourceType> {
        @DexIgnore
        rt<ResourceType> a(rt<ResourceType> rtVar);
    }

    @DexIgnore
    public et(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends zr<DataType, ResourceType>> list, cy<ResourceType, Transcode> cyVar, v8<List<Throwable>> v8Var) {
        this.a = cls;
        this.b = list;
        this.c = cyVar;
        this.d = v8Var;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public rt<Transcode> a(gs<DataType> gsVar, int i, int i2, xr xrVar, a<ResourceType> aVar) throws mt {
        return this.c.a(aVar.a(a(gsVar, i, i2, xrVar)), xrVar);
    }

    @DexIgnore
    public String toString() {
        return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }

    @DexIgnore
    public final rt<ResourceType> a(gs<DataType> gsVar, int i, int i2, xr xrVar) throws mt {
        List<Throwable> a2 = this.d.a();
        q00.a(a2);
        List list = a2;
        try {
            return a(gsVar, i, i2, xrVar, (List<Throwable>) list);
        } finally {
            this.d.a(list);
        }
    }

    @DexIgnore
    public final rt<ResourceType> a(gs<DataType> gsVar, int i, int i2, xr xrVar, List<Throwable> list) throws mt {
        int size = this.b.size();
        rt<ResourceType> rtVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            zr zrVar = (zr) this.b.get(i3);
            try {
                if (zrVar.a(gsVar.b(), xrVar)) {
                    rtVar = zrVar.a(gsVar.b(), i, i2, xrVar);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + zrVar, e2);
                }
                list.add(e2);
            }
            if (rtVar != null) {
                break;
            }
        }
        if (rtVar != null) {
            return rtVar;
        }
        throw new mt(this.e, (List<Throwable>) new ArrayList(list));
    }
}
