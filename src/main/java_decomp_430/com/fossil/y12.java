package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.n12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y12 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<y12> CREATOR; // = new h32();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public IBinder b;
    @DexIgnore
    public gv1 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public y12(int i, IBinder iBinder, gv1 gv1, boolean z, boolean z2) {
        this.a = i;
        this.b = iBinder;
        this.c = gv1;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public gv1 B() {
        return this.c;
    }

    @DexIgnore
    public boolean C() {
        return this.d;
    }

    @DexIgnore
    public boolean D() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof y12)) {
            return false;
        }
        y12 y12 = (y12) obj;
        return this.c.equals(y12.c) && p().equals(y12.p());
    }

    @DexIgnore
    public n12 p() {
        return n12.a.a(this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b, false);
        g22.a(parcel, 3, (Parcelable) B(), i, false);
        g22.a(parcel, 4, C());
        g22.a(parcel, 5, D());
        g22.a(parcel, a2);
    }
}
