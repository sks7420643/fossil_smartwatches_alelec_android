package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s51<T> extends u31<T> {
    @DexIgnore
    public s51() {
        super(new w40((byte) 1, (byte) 0));
    }

    @DexIgnore
    public byte[] a(short s, T t) {
        byte[] a = a(t);
        byte[] array = ByteBuffer.allocate(a.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put(this.a.getMajor()).put(this.a.getMinor()).putInt(0).putInt(a.length).put(a).putInt((int) h51.a.a(a, q11.CRC32)).array();
        wg6.a(array, "result.array()");
        return array;
    }
}
