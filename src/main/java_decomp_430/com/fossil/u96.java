package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum u96 {
    LOW,
    NORMAL,
    HIGH,
    IMMEDIATE;

    @DexIgnore
    public static <Y> int compareTo(y96 y96, Y y) {
        u96 u96;
        if (y instanceof y96) {
            u96 = ((y96) y).getPriority();
        } else {
            u96 = NORMAL;
        }
        return u96.ordinal() - y96.getPriority().ordinal();
    }
}
