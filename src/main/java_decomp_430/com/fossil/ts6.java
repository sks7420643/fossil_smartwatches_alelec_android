package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts6 extends IOException {
    @DexIgnore
    public /* final */ hs6 errorCode;

    @DexIgnore
    public ts6(hs6 hs6) {
        super("stream was reset: " + hs6);
        this.errorCode = hs6;
    }
}
