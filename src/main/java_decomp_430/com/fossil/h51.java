package com.fossil;

import java.util.zip.CRC32;
import java.util.zip.Checksum;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h51 {
    @DexIgnore
    public static /* final */ h51 a; // = new h51();

    @DexIgnore
    public final long a(byte[] bArr, q11 q11) {
        Checksum a2 = a(q11);
        a2.update(bArr, 0, bArr.length);
        return a2.getValue();
    }

    @DexIgnore
    public final long a(byte[] bArr, int i, int i2, q11 q11) {
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            return 0;
        }
        Checksum a2 = a(q11);
        a2.update(bArr, i, i2);
        return a2.getValue();
    }

    @DexIgnore
    public final Checksum a(q11 q11) {
        int i = l31.a[q11.ordinal()];
        if (i == 1) {
            return new CRC32();
        }
        if (i == 2) {
            return new vz0();
        }
        throw new kc6();
    }
}
