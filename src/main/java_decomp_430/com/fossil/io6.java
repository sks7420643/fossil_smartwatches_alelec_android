package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class io6 {
    @DexIgnore
    public static /* final */ Object a; // = new uo6("CONDITION_FALSE");

    /*
    static {
        new uo6("ALREADY_REMOVED");
        new uo6("LIST_EMPTY");
        new uo6("REMOVE_PREPARED");
    }
    */

    @DexIgnore
    public static final Object a() {
        return a;
    }

    @DexIgnore
    public static final jo6 a(Object obj) {
        jo6 jo6;
        wg6.b(obj, "$this$unwrap");
        qo6 qo6 = (qo6) (!(obj instanceof qo6) ? null : obj);
        return (qo6 == null || (jo6 = qo6.a) == null) ? (jo6) obj : jo6;
    }
}
