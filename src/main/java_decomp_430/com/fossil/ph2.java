package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ph2 extends IInterface {
    @DexIgnore
    boolean a(ph2 ph2) throws RemoteException;

    @DexIgnore
    int j() throws RemoteException;
}
