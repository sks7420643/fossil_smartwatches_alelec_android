package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xp3 {
    @DexIgnore
    <T> T a(Class<T> cls);

    @DexIgnore
    <T> ys3<T> b(Class<T> cls);

    @DexIgnore
    <T> ys3<Set<T>> c(Class<T> cls);

    @DexIgnore
    <T> Set<T> d(Class<T> cls);
}
