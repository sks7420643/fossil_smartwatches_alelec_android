package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sb3 extends sa2 implements pb3 {
    @DexIgnore
    public sb3() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 3) {
            a((gv1) ua2.a(parcel, gv1.CREATOR), (ob3) ua2.a(parcel, ob3.CREATOR));
        } else if (i == 4) {
            d((Status) ua2.a(parcel, Status.CREATOR));
        } else if (i == 6) {
            e((Status) ua2.a(parcel, Status.CREATOR));
        } else if (i == 7) {
            a((Status) ua2.a(parcel, Status.CREATOR), (GoogleSignInAccount) ua2.a(parcel, GoogleSignInAccount.CREATOR));
        } else if (i != 8) {
            return false;
        } else {
            a((xb3) ua2.a(parcel, xb3.CREATOR));
        }
        parcel2.writeNoException();
        return true;
    }
}
