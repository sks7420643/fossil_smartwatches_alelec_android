package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class jt3 implements xr3 {
    @DexIgnore
    public /* final */ gt3 a;

    @DexIgnore
    public jt3(gt3 gt3) {
        this.a = gt3;
    }

    @DexIgnore
    public final qc3 a(Intent intent) {
        return this.a.d(intent);
    }
}
