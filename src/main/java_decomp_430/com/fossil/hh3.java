package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh3 implements Comparable<hh3>, Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hh3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ Calendar a;
    @DexIgnore
    public /* final */ String b; // = nh3.e().format(this.a.getTime());
    @DexIgnore
    public /* final */ int c; // = this.a.get(2);
    @DexIgnore
    public /* final */ int d; // = this.a.get(1);
    @DexIgnore
    public /* final */ int e; // = this.a.getMaximum(7);
    @DexIgnore
    public /* final */ int f; // = this.a.getActualMaximum(5);
    @DexIgnore
    public /* final */ long g; // = this.a.getTimeInMillis();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<hh3> {
        @DexIgnore
        public hh3 createFromParcel(Parcel parcel) {
            return hh3.a(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        public hh3[] newArray(int i) {
            return new hh3[i];
        }
    }

    @DexIgnore
    public hh3(Calendar calendar) {
        calendar.set(5, 1);
        this.a = nh3.a(calendar);
    }

    @DexIgnore
    public static hh3 a(int i, int i2) {
        Calendar d2 = nh3.d();
        d2.set(1, i);
        d2.set(2, i2);
        return new hh3(d2);
    }

    @DexIgnore
    public static hh3 c(long j) {
        Calendar d2 = nh3.d();
        d2.setTimeInMillis(j);
        return new hh3(d2);
    }

    @DexIgnore
    public static hh3 d() {
        return new hh3(nh3.b());
    }

    @DexIgnore
    public int b(hh3 hh3) {
        if (this.a instanceof GregorianCalendar) {
            return ((hh3.d - this.d) * 12) + (hh3.c - this.c);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hh3)) {
            return false;
        }
        hh3 hh3 = (hh3) obj;
        if (this.c == hh3.c && this.d == hh3.d) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.c), Integer.valueOf(this.d)});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.d);
        parcel.writeInt(this.c);
    }

    @DexIgnore
    public hh3 b(int i) {
        Calendar a2 = nh3.a(this.a);
        a2.add(2, i);
        return new hh3(a2);
    }

    @DexIgnore
    public long c() {
        return this.a.getTimeInMillis();
    }

    @DexIgnore
    public int a() {
        int firstDayOfWeek = this.a.get(7) - this.a.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.e : firstDayOfWeek;
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(hh3 hh3) {
        return this.a.compareTo(hh3.a);
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public long a(int i) {
        Calendar a2 = nh3.a(this.a);
        a2.set(5, i);
        return a2.getTimeInMillis();
    }
}
