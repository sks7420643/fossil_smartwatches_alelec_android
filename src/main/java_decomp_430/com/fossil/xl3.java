package com.fossil;

import com.fossil.bm3;
import java.io.Serializable;
import java.lang.Enum;
import java.util.EnumMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl3<K extends Enum<K>, V> extends bm3.c<K, V> {
    @DexIgnore
    public /* final */ transient EnumMap<K, V> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K extends Enum<K>, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumMap<K, V> delegate;

        @DexIgnore
        public b(EnumMap<K, V> enumMap) {
            this.delegate = enumMap;
        }

        @DexIgnore
        public Object readResolve() {
            return new xl3(this.delegate);
        }
    }

    @DexIgnore
    public static <K extends Enum<K>, V> bm3<K, V> asImmutable(EnumMap<K, V> enumMap) {
        int size = enumMap.size();
        if (size == 0) {
            return bm3.of();
        }
        if (size != 1) {
            return new xl3(enumMap);
        }
        Map.Entry entry = (Map.Entry) pm3.b(enumMap.entrySet());
        return bm3.of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return this.e.containsKey(obj);
    }

    @DexIgnore
    public jo3<Map.Entry<K, V>> entryIterator() {
        return ym3.b(this.e.entrySet().iterator());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof xl3) {
            obj = ((xl3) obj).e;
        }
        return this.e.equals(obj);
    }

    @DexIgnore
    public V get(Object obj) {
        return this.e.get(obj);
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public jo3<K> keyIterator() {
        return qm3.e(this.e.keySet().iterator());
    }

    @DexIgnore
    public int size() {
        return this.e.size();
    }

    @DexIgnore
    public Object writeReplace() {
        return new b(this.e);
    }

    @DexIgnore
    public xl3(EnumMap<K, V> enumMap) {
        this.e = enumMap;
        jk3.a(!enumMap.isEmpty());
    }
}
