package com.fossil;

import com.fossil.jr6;
import com.fossil.sq6;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr6 implements Interceptor {
    @DexIgnore
    public /* final */ mr6 a;

    @DexIgnore
    public hr6(mr6 mr6) {
        this.a = mr6;
    }

    @DexIgnore
    public static Response a(Response response) {
        if (response == null || response.k() == null) {
            return response;
        }
        Response.a E = response.E();
        E.a((zq6) null);
        return E.a();
    }

    @DexIgnore
    public static boolean b(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        mr6 mr6 = this.a;
        Response b = mr6 != null ? mr6.b(chain.t()) : null;
        jr6 c = new jr6.a(System.currentTimeMillis(), chain.t(), b).c();
        yq6 yq6 = c.a;
        Response response = c.b;
        mr6 mr62 = this.a;
        if (mr62 != null) {
            mr62.a(c);
        }
        if (b != null && response == null) {
            fr6.a((Closeable) b.k());
        }
        if (yq6 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.a(chain.t());
            aVar.a(wq6.HTTP_1_1);
            aVar.a(504);
            aVar.a("Unsatisfiable Request (only-if-cached)");
            aVar.a(fr6.c);
            aVar.b(-1);
            aVar.a(System.currentTimeMillis());
            return aVar.a();
        } else if (yq6 == null) {
            Response.a E = response.E();
            E.a(a(response));
            return E.a();
        } else {
            try {
                Response a2 = chain.a(yq6);
                if (a2 == null && b != null) {
                }
                if (response != null) {
                    if (a2.n() == 304) {
                        Response.a E2 = response.E();
                        E2.a(a(response.p(), a2.p()));
                        E2.b(a2.J());
                        E2.a(a2.H());
                        E2.a(a(response));
                        E2.c(a(a2));
                        Response a3 = E2.a();
                        a2.k().close();
                        this.a.a();
                        this.a.a(response, a3);
                        return a3;
                    }
                    fr6.a((Closeable) response.k());
                }
                Response.a E3 = a2.E();
                E3.a(a(response));
                E3.c(a(a2));
                Response a4 = E3.a();
                if (this.a != null) {
                    if (yr6.b(a4) && jr6.a(a4, yq6)) {
                        return a(this.a.a(a4), a4);
                    }
                    if (zr6.a(yq6.e())) {
                        try {
                            this.a.a(yq6);
                        } catch (IOException unused) {
                        }
                    }
                }
                return a4;
            } finally {
                if (b != null) {
                    fr6.a((Closeable) b.k());
                }
            }
        }
    }

    @DexIgnore
    public final Response a(ir6 ir6, Response response) throws IOException {
        yt6 body;
        if (ir6 == null || (body = ir6.body()) == null) {
            return response;
        }
        a aVar = new a(this, response.k().source(), ir6, st6.a(body));
        String e = response.e("Content-Type");
        long contentLength = response.k().contentLength();
        Response.a E = response.E();
        E.a((zq6) new bs6(e, contentLength, st6.a((zt6) aVar)));
        return E.a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements zt6 {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ lt6 b;
        @DexIgnore
        public /* final */ /* synthetic */ ir6 c;
        @DexIgnore
        public /* final */ /* synthetic */ kt6 d;

        @DexIgnore
        public a(hr6 hr6, lt6 lt6, ir6 ir6, kt6 kt6) {
            this.b = lt6;
            this.c = ir6;
            this.d = kt6;
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            try {
                long b2 = this.b.b(jt6, j);
                if (b2 == -1) {
                    if (!this.a) {
                        this.a = true;
                        this.d.close();
                    }
                    return -1;
                }
                jt6.a(this.d.a(), jt6.p() - b2, b2);
                this.d.c();
                return b2;
            } catch (IOException e) {
                if (!this.a) {
                    this.a = true;
                    this.c.abort();
                }
                throw e;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.a && !fr6.a((zt6) this, 100, TimeUnit.MILLISECONDS)) {
                this.a = true;
                this.c.abort();
            }
            this.b.close();
        }

        @DexIgnore
        public au6 b() {
            return this.b.b();
        }
    }

    @DexIgnore
    public static sq6 a(sq6 sq6, sq6 sq62) {
        sq6.a aVar = new sq6.a();
        int b = sq6.b();
        for (int i = 0; i < b; i++) {
            String a2 = sq6.a(i);
            String b2 = sq6.b(i);
            if ((!"Warning".equalsIgnoreCase(a2) || !b2.startsWith("1")) && (a(a2) || !b(a2) || sq62.a(a2) == null)) {
                dr6.a.a(aVar, a2, b2);
            }
        }
        int b3 = sq62.b();
        for (int i2 = 0; i2 < b3; i2++) {
            String a3 = sq62.a(i2);
            if (!a(a3) && b(a3)) {
                dr6.a.a(aVar, a3, sq62.b(i2));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static boolean a(String str) {
        return "Content-Length".equalsIgnoreCase(str) || "Content-Encoding".equalsIgnoreCase(str) || "Content-Type".equalsIgnoreCase(str);
    }
}
