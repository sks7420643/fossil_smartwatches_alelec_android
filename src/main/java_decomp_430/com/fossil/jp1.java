package com.fossil;

import com.fossil.qp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp1 extends qp1 {
    @DexIgnore
    public /* final */ rp1 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ fo1<?> c;
    @DexIgnore
    public /* final */ ho1<?, byte[]> d;

    @DexIgnore
    public fo1<?> a() {
        return this.c;
    }

    @DexIgnore
    public ho1<?, byte[]> c() {
        return this.d;
    }

    @DexIgnore
    public rp1 d() {
        return this.a;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof qp1)) {
            return false;
        }
        qp1 qp1 = (qp1) obj;
        if (!this.a.equals(qp1.d()) || !this.b.equals(qp1.e()) || !this.c.equals(qp1.a()) || !this.d.equals(qp1.c())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SendRequest{transportContext=" + this.a + ", transportName=" + this.b + ", event=" + this.c + ", transformer=" + this.d + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qp1.a {
        @DexIgnore
        public rp1 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public fo1<?> c;
        @DexIgnore
        public ho1<?, byte[]> d;

        @DexIgnore
        public qp1.a a(rp1 rp1) {
            if (rp1 != null) {
                this.a = rp1;
                return this;
            }
            throw new NullPointerException("Null transportContext");
        }

        @DexIgnore
        public qp1.a a(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }

        @DexIgnore
        public qp1.a a(fo1<?> fo1) {
            if (fo1 != null) {
                this.c = fo1;
                return this;
            }
            throw new NullPointerException("Null event");
        }

        @DexIgnore
        public qp1.a a(ho1<?, byte[]> ho1) {
            if (ho1 != null) {
                this.d = ho1;
                return this;
            }
            throw new NullPointerException("Null transformer");
        }

        @DexIgnore
        public qp1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " transportContext";
            }
            if (this.b == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " event";
            }
            if (this.d == null) {
                str = str + " transformer";
            }
            if (str.isEmpty()) {
                return new jp1(this.a, this.b, this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public jp1(rp1 rp1, String str, fo1<?> fo1, ho1<?, byte[]> ho1) {
        this.a = rp1;
        this.b = str;
        this.c = fo1;
        this.d = ho1;
    }
}
