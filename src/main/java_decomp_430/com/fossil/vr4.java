package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr4 implements Factory<ur4> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;

    @DexIgnore
    public vr4(Provider<QuickResponseRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static vr4 a(Provider<QuickResponseRepository> provider) {
        return new vr4(provider);
    }

    @DexIgnore
    public static ur4 b(Provider<QuickResponseRepository> provider) {
        return new SetReplyMessageMappingUseCase(provider.get());
    }

    @DexIgnore
    public SetReplyMessageMappingUseCase get() {
        return b(this.a);
    }
}
