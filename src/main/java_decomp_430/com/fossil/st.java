package com.fossil;

import com.fossil.bt;
import com.fossil.fs;
import com.fossil.jv;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class st implements bt, fs.a<Object> {
    @DexIgnore
    public /* final */ bt.a a;
    @DexIgnore
    public /* final */ ct<?> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public vr e;
    @DexIgnore
    public List<jv<File, ?>> f;
    @DexIgnore
    public int g;
    @DexIgnore
    public volatile jv.a<?> h;
    @DexIgnore
    public File i;
    @DexIgnore
    public tt j;

    @DexIgnore
    public st(ct<?> ctVar, bt.a aVar) {
        this.b = ctVar;
        this.a = aVar;
    }

    @DexIgnore
    public boolean a() {
        List<vr> c2 = this.b.c();
        boolean z = false;
        if (c2.isEmpty()) {
            return false;
        }
        List<Class<?>> k = this.b.k();
        if (!k.isEmpty()) {
            while (true) {
                if (this.f == null || !b()) {
                    this.d++;
                    if (this.d >= k.size()) {
                        this.c++;
                        if (this.c >= c2.size()) {
                            return false;
                        }
                        this.d = 0;
                    }
                    vr vrVar = c2.get(this.c);
                    Class cls = k.get(this.d);
                    this.j = new tt(this.b.b(), vrVar, this.b.l(), this.b.n(), this.b.f(), this.b.b(cls), cls, this.b.i());
                    this.i = this.b.d().a(this.j);
                    File file = this.i;
                    if (file != null) {
                        this.e = vrVar;
                        this.f = this.b.a(file);
                        this.g = 0;
                    }
                } else {
                    this.h = null;
                    while (!z && b()) {
                        List<jv<File, ?>> list = this.f;
                        int i2 = this.g;
                        this.g = i2 + 1;
                        this.h = list.get(i2).a(this.i, this.b.n(), this.b.f(), this.b.i());
                        if (this.h != null && this.b.c(this.h.c.getDataClass())) {
                            this.h.c.a(this.b.j(), this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
        } else if (File.class.equals(this.b.m())) {
            return false;
        } else {
            throw new IllegalStateException("Failed to find any load path from " + this.b.h() + " to " + this.b.m());
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.g < this.f.size();
    }

    @DexIgnore
    public void cancel() {
        jv.a<?> aVar = this.h;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.a.a(this.e, obj, this.h.c, pr.RESOURCE_DISK_CACHE, this.j);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.a.a(this.j, exc, this.h.c, pr.RESOURCE_DISK_CACHE);
    }
}
