package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pf1 extends j61 {
    @DexIgnore
    public int A;

    @DexIgnore
    public pf1(ue1 ue1) {
        super(lx0.READ_RSSI, ue1);
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.A = ((s31) ok0).j;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.RSSI, (Object) Integer.valueOf(this.A)), 7));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.RSSI, (Object) Integer.valueOf(this.A));
    }

    @DexIgnore
    public ok0 l() {
        return new s31(this.y.v);
    }
}
