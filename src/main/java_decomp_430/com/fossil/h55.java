package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h55 implements Factory<g55> {
    @DexIgnore
    public static CommuteTimeSettingsDefaultAddressPresenter a(d55 d55, an4 an4, UserRepository userRepository) {
        return new CommuteTimeSettingsDefaultAddressPresenter(d55, an4, userRepository);
    }
}
