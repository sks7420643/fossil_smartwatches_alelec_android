package com.fossil;

import com.facebook.internal.FileLruCache;
import com.fossil.bm3;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm3<K, V> extends mm3<K, V> implements NavigableMap<K, V> {
    @DexIgnore
    public static /* final */ Comparator<Comparable> h; // = jn3.natural();
    @DexIgnore
    public static /* final */ lm3<Comparable, Object> i; // = new lm3<>(nm3.emptySet(jn3.natural()), zl3.of());
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient tn3<K> e;
    @DexIgnore
    public /* final */ transient zl3<V> f;
    @DexIgnore
    public transient lm3<K, V> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends dm3<K, V> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lm3$a$a")
        /* renamed from: com.fossil.lm3$a$a  reason: collision with other inner class name */
        public class C0028a extends tl3<Map.Entry<K, V>> {
            @DexIgnore
            public C0028a() {
            }

            @DexIgnore
            public vl3<Map.Entry<K, V>> delegateCollection() {
                return a.this;
            }

            @DexIgnore
            public Map.Entry<K, V> get(int i) {
                return ym3.a(lm3.this.e.asList().get(i), lm3.this.f.get(i));
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public zl3<Map.Entry<K, V>> createAsList() {
            return new C0028a();
        }

        @DexIgnore
        public bm3<K, V> map() {
            return lm3.this;
        }

        @DexIgnore
        public jo3<Map.Entry<K, V>> iterator() {
            return asList().iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends bm3.e {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<Object> comparator;

        @DexIgnore
        public c(lm3<?, ?> lm3) {
            super(lm3);
            this.comparator = lm3.comparator();
        }

        @DexIgnore
        public Object readResolve() {
            return createMap(new b(this.comparator));
        }
    }

    @DexIgnore
    public lm3(tn3<K> tn3, zl3<V> zl3) {
        this(tn3, zl3, (lm3) null);
    }

    @DexIgnore
    public static <K, V> lm3<K, V> a(Comparator<? super K> comparator, K k, V v) {
        zl3 of = zl3.of(k);
        jk3.a(comparator);
        return new lm3<>(new tn3(of, comparator), zl3.of(v));
    }

    @DexIgnore
    public static <K, V> lm3<K, V> copyOf(Map<? extends K, ? extends V> map) {
        return a(map, (jn3) h);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [java.util.SortedMap<K, ? extends V>, java.util.SortedMap] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static <K, V> lm3<K, V> copyOfSorted(SortedMap<K, ? extends V> r3) {
        Comparator<Comparable> comparator = r3.comparator();
        if (comparator == null) {
            comparator = h;
        }
        if (r3 instanceof lm3) {
            lm3<K, V> lm3 = r3;
            if (!lm3.isPartialView()) {
                return lm3;
            }
        }
        return a(comparator, true, r3.entrySet());
    }

    @DexIgnore
    public static <K, V> lm3<K, V> emptyMap(Comparator<? super K> comparator) {
        if (jn3.natural().equals(comparator)) {
            return of();
        }
        return new lm3<>(nm3.emptySet(comparator), zl3.of());
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> b<K, V> naturalOrder() {
        return new b<>(jn3.natural());
    }

    @DexIgnore
    public static <K, V> lm3<K, V> of() {
        return i;
    }

    @DexIgnore
    public static <K, V> b<K, V> orderedBy(Comparator<K> comparator) {
        return new b<>(comparator);
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> b<K, V> reverseOrder() {
        return new b<>(jn3.natural().reverse());
    }

    @DexIgnore
    public Map.Entry<K, V> ceilingEntry(K k) {
        return tailMap(k, true).firstEntry();
    }

    @DexIgnore
    public K ceilingKey(K k) {
        return ym3.a(ceilingEntry(k));
    }

    @DexIgnore
    public Comparator<? super K> comparator() {
        return keySet().comparator();
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? im3.of() : new a();
    }

    @DexIgnore
    public Map.Entry<K, V> firstEntry() {
        if (isEmpty()) {
            return null;
        }
        return (Map.Entry) entrySet().asList().get(0);
    }

    @DexIgnore
    public K firstKey() {
        return keySet().first();
    }

    @DexIgnore
    public Map.Entry<K, V> floorEntry(K k) {
        return headMap(k, true).lastEntry();
    }

    @DexIgnore
    public K floorKey(K k) {
        return ym3.a(floorEntry(k));
    }

    @DexIgnore
    public V get(Object obj) {
        int indexOf = this.e.indexOf(obj);
        if (indexOf == -1) {
            return null;
        }
        return this.f.get(indexOf);
    }

    @DexIgnore
    public Map.Entry<K, V> higherEntry(K k) {
        return tailMap(k, false).firstEntry();
    }

    @DexIgnore
    public K higherKey(K k) {
        return ym3.a(higherEntry(k));
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.e.isPartialView() || this.f.isPartialView();
    }

    @DexIgnore
    public Map.Entry<K, V> lastEntry() {
        if (isEmpty()) {
            return null;
        }
        return (Map.Entry) entrySet().asList().get(size() - 1);
    }

    @DexIgnore
    public K lastKey() {
        return keySet().last();
    }

    @DexIgnore
    public Map.Entry<K, V> lowerEntry(K k) {
        return headMap(k, false).lastEntry();
    }

    @DexIgnore
    public K lowerKey(K k) {
        return ym3.a(lowerEntry(k));
    }

    @DexIgnore
    @Deprecated
    public final Map.Entry<K, V> pollFirstEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final Map.Entry<K, V> pollLastEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    public Object writeReplace() {
        return new c(this);
    }

    @DexIgnore
    public lm3(tn3<K> tn3, zl3<V> zl3, lm3<K, V> lm3) {
        this.e = tn3;
        this.f = zl3;
        this.g = lm3;
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> lm3<K, V> a(cm3<K, V>... cm3Arr) {
        return a(jn3.natural(), false, cm3Arr, cm3Arr.length);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> lm3<K, V> of(K k, V v) {
        return a(jn3.natural(), k, v);
    }

    @DexIgnore
    public nm3<K> descendingKeySet() {
        return this.e.descendingSet();
    }

    @DexIgnore
    public lm3<K, V> descendingMap() {
        lm3<K, V> lm3 = this.g;
        if (lm3 != null) {
            return lm3;
        }
        if (isEmpty()) {
            return emptyMap(jn3.from(comparator()).reverse());
        }
        return new lm3<>((tn3) this.e.descendingSet(), this.f.reverse(), this);
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    public nm3<K> navigableKeySet() {
        return this.e;
    }

    @DexIgnore
    public vl3<V> values() {
        return this.f;
    }

    @DexIgnore
    public static <K, V> lm3<K, V> a(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        boolean z = false;
        if (map instanceof SortedMap) {
            Comparator comparator2 = ((SortedMap) map).comparator();
            if (comparator2 != null) {
                z = comparator.equals(comparator2);
            } else if (comparator == h) {
                z = true;
            }
        }
        if (z && (map instanceof lm3)) {
            lm3<K, V> lm3 = (lm3) map;
            if (!lm3.isPartialView()) {
                return lm3;
            }
        }
        return a(comparator, z, map.entrySet());
    }

    @DexIgnore
    public static <K, V> lm3<K, V> copyOf(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        jk3.a(comparator);
        return a(map, comparator);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> lm3<K, V> of(K k, V v, K k2, V v2) {
        return a(bm3.entryOf(k, v), bm3.entryOf(k2, v2));
    }

    @DexIgnore
    public lm3<K, V> headMap(K k) {
        return headMap(k, false);
    }

    @DexIgnore
    public nm3<K> keySet() {
        return this.e;
    }

    @DexIgnore
    public lm3<K, V> subMap(K k, K k2) {
        return subMap(k, true, k2, false);
    }

    @DexIgnore
    public lm3<K, V> tailMap(K k) {
        return tailMap(k, true);
    }

    @DexIgnore
    public static <K, V> lm3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return copyOf(iterable, (jn3) h);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> lm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return a(bm3.entryOf(k, v), bm3.entryOf(k2, v2), bm3.entryOf(k3, v3));
    }

    @DexIgnore
    public lm3<K, V> headMap(K k, boolean z) {
        tn3<K> tn3 = this.e;
        jk3.a(k);
        return a(0, tn3.headIndex(k, z));
    }

    @DexIgnore
    public lm3<K, V> subMap(K k, boolean z, K k2, boolean z2) {
        jk3.a(k);
        jk3.a(k2);
        jk3.a(comparator().compare(k, k2) <= 0, "expected fromKey <= toKey but %s > %s", (Object) k, (Object) k2);
        return headMap(k2, z2).tailMap(k, z);
    }

    @DexIgnore
    public lm3<K, V> tailMap(K k, boolean z) {
        tn3<K> tn3 = this.e;
        jk3.a(k);
        return a(tn3.tailIndex(k, z), size());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends bm3.b<K, V> {
        @DexIgnore
        public /* final */ Comparator<? super K> e;

        @DexIgnore
        public b(Comparator<? super K> comparator) {
            jk3.a(comparator);
            this.e = comparator;
        }

        @DexIgnore
        public b<K, V> a(K k, V v) {
            super.a(k, v);
            return this;
        }

        @DexIgnore
        public b<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            super.a(entry);
            return this;
        }

        @DexIgnore
        public b<K, V> a(Map<? extends K, ? extends V> map) {
            super.a(map);
            return this;
        }

        @DexIgnore
        public b<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a(iterable);
            return this;
        }

        @DexIgnore
        public lm3<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return lm3.emptyMap(this.e);
            }
            if (i != 1) {
                return lm3.a(this.e, false, this.b, i);
            }
            return lm3.a(this.e, this.b[0].getKey(), this.b[0].getValue());
        }
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> lm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return a(bm3.entryOf(k, v), bm3.entryOf(k2, v2), bm3.entryOf(k3, v3), bm3.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> lm3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable, Comparator<? super K> comparator) {
        jk3.a(comparator);
        return a(comparator, false, iterable);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> lm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return a(bm3.entryOf(k, v), bm3.entryOf(k2, v2), bm3.entryOf(k3, v3), bm3.entryOf(k4, v4), bm3.entryOf(k5, v5));
    }

    @DexIgnore
    public static <K, V> lm3<K, V> a(Comparator<? super K> comparator, boolean z, Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) pm3.a(iterable, (T[]) bm3.EMPTY_ENTRY_ARRAY);
        return a(comparator, z, entryArr, entryArr.length);
    }

    @DexIgnore
    public static <K, V> lm3<K, V> a(Comparator<? super K> comparator, boolean z, Map.Entry<K, V>[] entryArr, int i2) {
        if (i2 == 0) {
            return emptyMap(comparator);
        }
        if (i2 == 1) {
            return a(comparator, entryArr[0].getKey(), entryArr[0].getValue());
        }
        Object[] objArr = new Object[i2];
        Object[] objArr2 = new Object[i2];
        if (z) {
            for (int i3 = 0; i3 < i2; i3++) {
                K key = entryArr[i3].getKey();
                V value = entryArr[i3].getValue();
                bl3.a((Object) key, (Object) value);
                objArr[i3] = key;
                objArr2[i3] = value;
            }
        } else {
            Arrays.sort(entryArr, 0, i2, jn3.from(comparator).onKeys());
            K key2 = entryArr[0].getKey();
            objArr[0] = key2;
            objArr2[0] = entryArr[0].getValue();
            K k = key2;
            int i4 = 1;
            while (i4 < i2) {
                K key3 = entryArr[i4].getKey();
                V value2 = entryArr[i4].getValue();
                bl3.a((Object) key3, (Object) value2);
                objArr[i4] = key3;
                objArr2[i4] = value2;
                bm3.checkNoConflict(comparator.compare(k, key3) != 0, FileLruCache.HEADER_CACHEKEY_KEY, entryArr[i4 - 1], entryArr[i4]);
                i4++;
                k = key3;
            }
        }
        return new lm3<>(new tn3(new pn3(objArr), comparator), new pn3(objArr2));
    }

    @DexIgnore
    public final lm3<K, V> a(int i2, int i3) {
        if (i2 == 0 && i3 == size()) {
            return this;
        }
        if (i2 == i3) {
            return emptyMap(comparator());
        }
        return new lm3<>(this.e.getSubSet(i2, i3), this.f.subList(i2, i3));
    }
}
