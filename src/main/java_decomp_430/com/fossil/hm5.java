package com.fossil;

import com.fossil.vs4;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm5 extends dm5 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public /* final */ em5 e;
    @DexIgnore
    public /* final */ vs4 f;
    @DexIgnore
    public /* final */ z24 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return hm5.h;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.d<vs4.d, vs4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ hm5 a;

        @DexIgnore
        public b(hm5 hm5) {
            this.a = hm5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(vs4.d dVar) {
            wg6.b(dVar, "successResponse");
            if (this.a.e.isActive()) {
                this.a.e.d();
                this.a.e.W0();
            }
        }

        @DexIgnore
        public void a(vs4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = hm5.i.a();
            local.e(a2, "updateUserPassword onFailure: errorValue = " + cVar);
            if (this.a.e.isActive()) {
                this.a.e.d();
                int a3 = cVar.a();
                if (a3 == 400004) {
                    this.a.e.u0();
                } else if (a3 != 403005) {
                    this.a.e.e(cVar.a(), cVar.b());
                } else {
                    this.a.e.I0();
                }
            }
        }
    }

    /*
    static {
        String simpleName = hm5.class.getSimpleName();
        wg6.a((Object) simpleName, "ProfileChangePasswordPre\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public hm5(em5 em5, vs4 vs4, z24 z24) {
        wg6.b(em5, "mView");
        wg6.b(vs4, "mChangePasswordUseCase");
        wg6.b(z24, "mUseCaseHandler");
        this.e = em5;
        this.f = vs4;
        this.g = z24;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(h, "presenter starts");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(h, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(String str, String str2) {
        wg6.b(str, "oldPass");
        wg6.b(str2, "newPass");
        if (!hx5.b(PortfolioApp.get.instance())) {
            this.e.e(601, (String) null);
            return;
        }
        this.e.e();
        this.g.a(this.f, new vs4.b(str, str2), new b(this));
    }
}
