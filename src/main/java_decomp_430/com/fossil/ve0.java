package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ve0 extends an1 {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic d;

    @DexIgnore
    public ve0(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super(bluetoothDevice, i);
        this.c = i2;
        this.d = bluetoothGattCharacteristic;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.OFFSET, (Object) Integer.valueOf(this.c)), bm0.CHARACTERISTIC, (Object) this.d.getUuid().toString());
    }
}
