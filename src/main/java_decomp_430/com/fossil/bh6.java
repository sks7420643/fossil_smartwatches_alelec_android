package com.fossil;

import com.fossil.mi6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bh6 extends eh6 implements mi6 {
    @DexIgnore
    public bh6() {
    }

    @DexIgnore
    public ei6 computeReflected() {
        kh6.a(this);
        return this;
    }

    @DexIgnore
    public Object getDelegate() {
        return ((mi6) getReflected()).getDelegate();
    }

    @DexIgnore
    public Object invoke() {
        return get();
    }

    @DexIgnore
    public bh6(Object obj) {
        super(obj);
    }

    @DexIgnore
    public mi6.a getGetter() {
        return ((mi6) getReflected()).getGetter();
    }
}
