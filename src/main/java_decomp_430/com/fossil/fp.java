package com.fossil;

import com.fossil.dp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fp {
    @DexIgnore
    public dp<?> a;

    @DexIgnore
    public fp(dp<?> dpVar) {
        this.a = dpVar;
    }

    @DexIgnore
    public void a() {
        this.a = null;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        dp.g j;
        try {
            dp<?> dpVar = this.a;
            if (!(dpVar == null || (j = dp.j()) == null)) {
                j.a(dpVar, new gp(dpVar.a()));
            }
        } finally {
            super.finalize();
        }
    }
}
