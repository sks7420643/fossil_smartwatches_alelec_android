package com.fossil;

import android.app.ActivityManager;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.j96;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b40 {
    @DexIgnore
    public static /* final */ n20 a; // = n20.a("0");
    @DexIgnore
    public static /* final */ n20 b; // = n20.a("Unity");

    @DexIgnore
    public static void a(q20 q20, String str, String str2, long j) throws Exception {
        q20.a(1, n20.a(str2));
        q20.a(2, n20.a(str));
        q20.a(3, j);
    }

    @DexIgnore
    public static int b(n20 n20) {
        return q20.b(1, n20) + 0;
    }

    @DexIgnore
    public static void a(q20 q20, String str, String str2, String str3, String str4, String str5, int i, String str6) throws Exception {
        n20 a2 = n20.a(str);
        n20 a3 = n20.a(str2);
        n20 a4 = n20.a(str3);
        n20 a5 = n20.a(str4);
        n20 a6 = n20.a(str5);
        n20 a7 = str6 != null ? n20.a(str6) : null;
        q20.c(7, 2);
        q20.e(a(a2, a3, a4, a5, a6, i, a7));
        q20.a(1, a2);
        q20.a(2, a4);
        q20.a(3, a5);
        q20.c(5, 2);
        q20.e(b(a3));
        q20.a(1, a3);
        q20.a(6, a6);
        if (a7 != null) {
            q20.a(8, b);
            q20.a(9, a7);
        }
        q20.a(10, i);
    }

    @DexIgnore
    public static void a(q20 q20, String str, String str2, boolean z) throws Exception {
        n20 a2 = n20.a(str);
        n20 a3 = n20.a(str2);
        q20.c(8, 2);
        q20.e(a(a2, a3, z));
        q20.a(1, 3);
        q20.a(2, a2);
        q20.a(3, a3);
        q20.a(4, z);
    }

    @DexIgnore
    public static void a(q20 q20, int i, String str, int i2, long j, long j2, boolean z, Map<j96.a, String> map, int i3, String str2, String str3) throws Exception {
        q20 q202 = q20;
        n20 a2 = a(str);
        n20 a3 = a(str3);
        n20 a4 = a(str2);
        q202.c(9, 2);
        n20 n20 = a4;
        q202.e(a(i, a2, i2, j, j2, z, map, i3, a4, a3));
        q202.a(3, i);
        q202.a(4, a2);
        q202.d(5, i2);
        q202.a(6, j);
        q202.a(7, j2);
        q202.a(10, z);
        for (Map.Entry next : map.entrySet()) {
            q202.c(11, 2);
            q202.e(a((j96.a) next.getKey(), (String) next.getValue()));
            q202.a(1, ((j96.a) next.getKey()).protobufIndex);
            q202.a(2, n20.a((String) next.getValue()));
        }
        q202.d(12, i3);
        if (n20 != null) {
            q202.a(13, n20);
        }
        if (a3 != null) {
            q202.a(14, a3);
        }
    }

    @DexIgnore
    public static void a(q20 q20, String str, String str2, String str3) throws Exception {
        if (str == null) {
            str = "";
        }
        n20 a2 = n20.a(str);
        n20 a3 = a(str2);
        n20 a4 = a(str3);
        int b2 = q20.b(1, a2) + 0;
        if (str2 != null) {
            b2 += q20.b(2, a3);
        }
        if (str3 != null) {
            b2 += q20.b(3, a4);
        }
        q20.c(6, 2);
        q20.e(b2);
        q20.a(1, a2);
        if (str2 != null) {
            q20.a(2, a3);
        }
        if (str3 != null) {
            q20.a(3, a4);
        }
    }

    @DexIgnore
    public static void a(q20 q20, long j, String str, f40 f40, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, Map<String, String> map, k30 k30, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i, String str2, String str3, Float f, int i2, boolean z, long j2, long j3) throws Exception {
        n20 n20;
        q20 q202 = q20;
        String str4 = str3;
        n20 a2 = n20.a(str2);
        if (str4 == null) {
            n20 = null;
        } else {
            n20 = n20.a(str4.replace("-", ""));
        }
        n20 n202 = n20;
        n20 b2 = k30.b();
        if (b2 == null) {
            c86.g().d("CrashlyticsCore", "No log data to include with this event.");
        }
        k30.a();
        q202.c(10, 2);
        q202.e(a(j, str, f40, thread, stackTraceElementArr, threadArr, list, 8, map, runningAppProcessInfo, i, a2, n202, f, i2, z, j2, j3, b2));
        q202.a(1, j);
        q202.a(2, n20.a(str));
        a(q20, f40, thread, stackTraceElementArr, threadArr, list, 8, a2, n202, map, runningAppProcessInfo, i);
        a(q20, f, i2, z, i, j2, j3);
        a(q202, b2);
    }

    @DexIgnore
    public static void a(q20 q20, f40 f40, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, n20 n20, n20 n202, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) throws Exception {
        q20.c(3, 2);
        q20.e(a(f40, thread, stackTraceElementArr, threadArr, list, i, n20, n202, map, runningAppProcessInfo, i2));
        a(q20, f40, thread, stackTraceElementArr, threadArr, list, i, n20, n202);
        if (map != null && !map.isEmpty()) {
            a(q20, map);
        }
        if (runningAppProcessInfo != null) {
            q20.a(3, runningAppProcessInfo.importance != 100);
        }
        q20.d(4, i2);
    }

    @DexIgnore
    public static void a(q20 q20, f40 f40, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, n20 n20, n20 n202) throws Exception {
        q20.c(1, 2);
        q20.e(a(f40, thread, stackTraceElementArr, threadArr, list, i, n20, n202));
        a(q20, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            a(q20, threadArr[i2], list.get(i2), 0, false);
        }
        a(q20, f40, 1, i, 2);
        q20.c(3, 2);
        q20.e(a());
        q20.a(1, a);
        q20.a(2, a);
        q20.a(3, 0);
        q20.c(4, 2);
        q20.e(a(n20, n202));
        q20.a(1, 0);
        q20.a(2, 0);
        q20.a(3, n20);
        if (n202 != null) {
            q20.a(4, n202);
        }
    }

    @DexIgnore
    public static void a(q20 q20, Map<String, String> map) throws Exception {
        for (Map.Entry next : map.entrySet()) {
            q20.c(2, 2);
            q20.e(a((String) next.getKey(), (String) next.getValue()));
            q20.a(1, n20.a((String) next.getKey()));
            String str = (String) next.getValue();
            if (str == null) {
                str = "";
            }
            q20.a(2, n20.a(str));
        }
    }

    @DexIgnore
    public static void a(q20 q20, f40 f40, int i, int i2, int i3) throws Exception {
        q20.c(i3, 2);
        q20.e(a(f40, 1, i2));
        q20.a(1, n20.a(f40.b));
        String str = f40.a;
        if (str != null) {
            q20.a(3, n20.a(str));
        }
        int i4 = 0;
        for (StackTraceElement a2 : f40.c) {
            a(q20, 4, a2, true);
        }
        f40 f402 = f40.d;
        if (f402 == null) {
            return;
        }
        if (i < i2) {
            a(q20, f402, i + 1, i2, 6);
            return;
        }
        while (f402 != null) {
            f402 = f402.d;
            i4++;
        }
        q20.d(7, i4);
    }

    @DexIgnore
    public static void a(q20 q20, Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) throws Exception {
        q20.c(1, 2);
        q20.e(a(thread, stackTraceElementArr, i, z));
        q20.a(1, n20.a(thread.getName()));
        q20.d(2, i);
        for (StackTraceElement a2 : stackTraceElementArr) {
            a(q20, 3, a2, z);
        }
    }

    @DexIgnore
    public static void a(q20 q20, int i, StackTraceElement stackTraceElement, boolean z) throws Exception {
        q20.c(i, 2);
        q20.e(a(stackTraceElement, z));
        if (stackTraceElement.isNativeMethod()) {
            q20.a(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            q20.a(1, 0);
        }
        q20.a(2, n20.a(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            q20.a(3, n20.a(stackTraceElement.getFileName()));
        }
        int i2 = 4;
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            q20.a(4, (long) stackTraceElement.getLineNumber());
        }
        if (!z) {
            i2 = 0;
        }
        q20.d(5, i2);
    }

    @DexIgnore
    public static void a(q20 q20, Float f, int i, boolean z, int i2, long j, long j2) throws Exception {
        q20.c(5, 2);
        q20.e(a(f, i, z, i2, j, j2));
        if (f != null) {
            q20.a(1, f.floatValue());
        }
        q20.b(2, i);
        q20.a(3, z);
        q20.d(4, i2);
        q20.a(5, j);
        q20.a(6, j2);
    }

    @DexIgnore
    public static void a(q20 q20, n20 n20) throws Exception {
        if (n20 != null) {
            q20.c(6, 2);
            q20.e(a(n20));
            q20.a(1, n20);
        }
    }

    @DexIgnore
    public static int a(n20 n20, n20 n202, n20 n203, n20 n204, n20 n205, int i, n20 n206) {
        int b2 = b(n202);
        int b3 = q20.b(1, n20) + 0 + q20.b(2, n203) + q20.b(3, n204) + q20.l(5) + q20.j(b2) + b2 + q20.b(6, n205);
        if (n206 != null) {
            b3 = b3 + q20.b(8, b) + q20.b(9, n206);
        }
        return b3 + q20.e(10, i);
    }

    @DexIgnore
    public static int a(n20 n20, n20 n202, boolean z) {
        return q20.e(1, 3) + 0 + q20.b(2, n20) + q20.b(3, n202) + q20.b(4, z);
    }

    @DexIgnore
    public static int a(j96.a aVar, String str) {
        return q20.e(1, aVar.protobufIndex) + q20.b(2, n20.a(str));
    }

    @DexIgnore
    public static int a(int i, n20 n20, int i2, long j, long j2, boolean z, Map<j96.a, String> map, int i3, n20 n202, n20 n203) {
        int i4;
        int i5;
        int i6 = 0;
        int e = q20.e(3, i) + 0;
        if (n20 == null) {
            i4 = 0;
        } else {
            i4 = q20.b(4, n20);
        }
        int g = e + i4 + q20.g(5, i2) + q20.b(6, j) + q20.b(7, j2) + q20.b(10, z);
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                int a2 = a((j96.a) next.getKey(), (String) next.getValue());
                g += q20.l(11) + q20.j(a2) + a2;
            }
        }
        int g2 = g + q20.g(12, i3);
        if (n202 == null) {
            i5 = 0;
        } else {
            i5 = q20.b(13, n202);
        }
        int i7 = g2 + i5;
        if (n203 != null) {
            i6 = q20.b(14, n203);
        }
        return i7 + i6;
    }

    @DexIgnore
    public static int a(n20 n20, n20 n202) {
        int b2 = q20.b(1, 0) + 0 + q20.b(2, 0) + q20.b(3, n20);
        return n202 != null ? b2 + q20.b(4, n202) : b2;
    }

    @DexIgnore
    public static int a(long j, String str, f40 f40, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, n20 n20, n20 n202, Float f, int i3, boolean z, long j2, long j3, n20 n203) {
        long j4 = j;
        int b2 = q20.b(1, j) + 0 + q20.b(2, n20.a(str));
        int a2 = a(f40, thread, stackTraceElementArr, threadArr, list, i, n20, n202, map, runningAppProcessInfo, i2);
        int a3 = a(f, i3, z, i2, j2, j3);
        int l = b2 + q20.l(3) + q20.j(a2) + a2 + q20.l(5) + q20.j(a3) + a3;
        if (n203 == null) {
            return l;
        }
        int a4 = a(n203);
        return l + q20.l(6) + q20.j(a4) + a4;
    }

    @DexIgnore
    public static int a(f40 f40, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, n20 n20, n20 n202, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int a2 = a(f40, thread, stackTraceElementArr, threadArr, list, i, n20, n202);
        int l = q20.l(1) + q20.j(a2) + a2;
        boolean z = false;
        int i3 = l + 0;
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                int a3 = a((String) next.getKey(), (String) next.getValue());
                i3 += q20.l(2) + q20.j(a3) + a3;
            }
        }
        if (runningAppProcessInfo != null) {
            if (runningAppProcessInfo.importance != 100) {
                z = true;
            }
            i3 += q20.b(3, z);
        }
        return i3 + q20.g(4, i2);
    }

    @DexIgnore
    public static int a(f40 f40, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, n20 n20, n20 n202) {
        int a2 = a(thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        int l = q20.l(1) + q20.j(a2) + a2 + 0;
        for (int i2 = 0; i2 < length; i2++) {
            int a3 = a(threadArr[i2], list.get(i2), 0, false);
            l += q20.l(1) + q20.j(a3) + a3;
        }
        int a4 = a(f40, 1, i);
        int a5 = a();
        int a6 = a(n20, n202);
        return l + q20.l(2) + q20.j(a4) + a4 + q20.l(3) + q20.j(a5) + a5 + q20.l(3) + q20.j(a6) + a6;
    }

    @DexIgnore
    public static int a(String str, String str2) {
        int b2 = q20.b(1, n20.a(str));
        if (str2 == null) {
            str2 = "";
        }
        return b2 + q20.b(2, n20.a(str2));
    }

    @DexIgnore
    public static int a(Float f, int i, boolean z, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            i3 = 0 + q20.b(1, f.floatValue());
        }
        return i3 + q20.f(2, i) + q20.b(3, z) + q20.g(4, i2) + q20.b(5, j) + q20.b(6, j2);
    }

    @DexIgnore
    public static int a(n20 n20) {
        return q20.b(1, n20);
    }

    @DexIgnore
    public static int a(f40 f40, int i, int i2) {
        int i3 = 0;
        int b2 = q20.b(1, n20.a(f40.b)) + 0;
        String str = f40.a;
        if (str != null) {
            b2 += q20.b(3, n20.a(str));
        }
        int i4 = b2;
        for (StackTraceElement a2 : f40.c) {
            int a3 = a(a2, true);
            i4 += q20.l(4) + q20.j(a3) + a3;
        }
        f40 f402 = f40.d;
        if (f402 == null) {
            return i4;
        }
        if (i < i2) {
            int a4 = a(f402, i + 1, i2);
            return i4 + q20.l(6) + q20.j(a4) + a4;
        }
        while (f402 != null) {
            f402 = f402.d;
            i3++;
        }
        return i4 + q20.g(7, i3);
    }

    @DexIgnore
    public static int a() {
        return q20.b(1, a) + 0 + q20.b(2, a) + q20.b(3, 0);
    }

    @DexIgnore
    public static int a(StackTraceElement stackTraceElement, boolean z) {
        int i;
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            i = q20.b(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            i = q20.b(1, 0);
        }
        int b2 = i + 0 + q20.b(2, n20.a(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            b2 += q20.b(3, n20.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            b2 += q20.b(4, (long) stackTraceElement.getLineNumber());
        }
        if (z) {
            i2 = 2;
        }
        return b2 + q20.g(5, i2);
    }

    @DexIgnore
    public static int a(Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) {
        int b2 = q20.b(1, n20.a(thread.getName())) + q20.g(2, i);
        for (StackTraceElement a2 : stackTraceElementArr) {
            int a3 = a(a2, z);
            b2 += q20.l(3) + q20.j(a3) + a3;
        }
        return b2;
    }

    @DexIgnore
    public static n20 a(String str) {
        if (str == null) {
            return null;
        }
        return n20.a(str);
    }
}
