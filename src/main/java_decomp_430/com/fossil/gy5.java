package com.fossil;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import com.fossil.aw3;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gy5 {
    @DexIgnore
    public static /* final */ String a; // = "gy5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View a;

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public void run() {
            this.a.setEnabled(true);
        }
    }

    @DexIgnore
    public static Boolean a(List<PhoneNumber> list, PhoneNumber phoneNumber) {
        if (!(list == null || phoneNumber == null)) {
            for (PhoneNumber next : list) {
                if (next.getContact().getContactId() == phoneNumber.getContact().getContactId() && PhoneNumberUtils.compare(next.getNumber(), phoneNumber.getNumber())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public static String b(String str) {
        TelephonyManager telephonyManager = (TelephonyManager) PortfolioApp.T.getSystemService("phone");
        aw3 a2 = aw3.a();
        if (telephonyManager != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    return a2.a(a2.b(str, a()), aw3.c.INTERNATIONAL);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    @DexIgnore
    public static float c(float f) {
        return TypedValue.applyDimension(2, f, Resources.getSystem().getDisplayMetrics());
    }

    @DexIgnore
    public static Point c(float f, float f2, float f3, float f4) {
        double d;
        double b = b(f, f2, f3, f4);
        dz5 a2 = a(f, f2, f3, f4);
        if (a2 == dz5.TopLeft) {
            d = Math.toRadians(180.0d - Math.toDegrees(b));
        } else if (a2 == dz5.BottomLeft) {
            d = Math.toRadians(Math.toDegrees(b) + 180.0d);
        } else if (a2 == dz5.BottomRight) {
            d = Math.toRadians(360.0d - Math.toDegrees(b));
        } else {
            d = Math.toRadians(Math.toDegrees(b));
        }
        double d2 = (double) 2000.0f;
        return new Point((int) (Math.cos(d) * d2), (int) (d2 * Math.sin(d)));
    }

    @DexIgnore
    public static Boolean a(List<String> list, String str) {
        if (list != null) {
            for (String compare : list) {
                if (PhoneNumberUtils.compare(compare, str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public static boolean b() {
        if (Build.VERSION.SDK_INT >= 23) {
            NotificationManager notificationManager = (NotificationManager) PortfolioApp.T.getSystemService("notification");
            if (notificationManager != null) {
                int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
                if (currentInterruptionFilter == 3 || currentInterruptionFilter == 4 || currentInterruptionFilter == 2 || currentInterruptionFilter == 0) {
                    return true;
                }
                return false;
            }
        } else {
            try {
                if (Settings.Global.getInt(PortfolioApp.T.getContentResolver(), "zen_mode") != 0) {
                    return true;
                }
                return false;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, "isInDNDMode() - ex = " + e);
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public static String a() {
        TelephonyManager telephonyManager = (TelephonyManager) PortfolioApp.T.getSystemService("phone");
        if (telephonyManager != null) {
            try {
                if (telephonyManager.getNetworkCountryIso() != null && !TextUtils.isEmpty(telephonyManager.getNetworkCountryIso())) {
                    return telephonyManager.getNetworkCountryIso().toUpperCase();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return Locale.getDefault().getCountry().toUpperCase();
    }

    @DexIgnore
    public static int b(float f) {
        return (int) TypedValue.applyDimension(2, f, Resources.getSystem().getDisplayMetrics());
    }

    @DexIgnore
    public static double b(float f, float f2, float f3, float f4) {
        return Math.atan((double) (Math.abs(f2 - f4) / Math.abs(f3 - f)));
    }

    @DexIgnore
    public static float a(float f) {
        return f * Resources.getSystem().getDisplayMetrics().density;
    }

    @DexIgnore
    public static float a(int i, Context context) {
        return ((float) i) * context.getResources().getDisplayMetrics().density;
    }

    @DexIgnore
    public static void a(View view) {
        if (view != null) {
            view.setEnabled(false);
            new Handler().postDelayed(new a(view), 500);
        }
    }

    @DexIgnore
    public static dz5 a(float f, float f2, float f3, float f4) {
        if (f3 > f) {
            if (f4 > f2) {
                return dz5.BottomRight;
            }
            return dz5.TopRight;
        } else if (f4 > f2) {
            return dz5.BottomLeft;
        } else {
            return dz5.TopLeft;
        }
    }

    @DexIgnore
    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str2 = str.split(",")[0];
        if (str2.length() <= 20) {
            return str2;
        }
        return str2.substring(0, 17) + "...";
    }
}
