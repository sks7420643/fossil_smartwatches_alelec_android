package com.fossil;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r64 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ AppCompatImageView r;
    @DexIgnore
    public /* final */ FlexibleImageButton s;
    @DexIgnore
    public /* final */ FlexibleImageButton t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ DashBar x;
    @DexIgnore
    public /* final */ ConstraintLayout y;

    @DexIgnore
    public r64(Object obj, View view, int i, RTLImageView rTLImageView, AppCompatImageView appCompatImageView, FlexibleImageButton flexibleImageButton, FlexibleImageButton flexibleImageButton2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView2, DashBar dashBar, ConstraintLayout constraintLayout4) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = appCompatImageView;
        this.s = flexibleImageButton;
        this.t = flexibleImageButton2;
        this.u = flexibleTextView;
        this.v = flexibleButton;
        this.w = flexibleTextView2;
        this.x = dashBar;
        this.y = constraintLayout4;
    }
}
