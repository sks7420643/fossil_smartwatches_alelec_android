package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.p6;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SupportErrorDialogFragment;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jv1 extends kv1 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static /* final */ jv1 e; // = new jv1();
    @DexIgnore
    public String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"HandlerLeak"})
    public class a extends bb2 {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.a = context.getApplicationContext();
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int c = jv1.this.c(this.a);
            if (jv1.this.c(c)) {
                jv1.this.c(this.a, c);
            }
        }
    }

    @DexIgnore
    public static jv1 a() {
        return e;
    }

    @DexIgnore
    public boolean b(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, i2, onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    @DexIgnore
    public void c(Context context, int i) {
        a(context, i, (String) null, a(context, i, 0, "n"));
    }

    @DexIgnore
    public final void d(Context context) {
        new a(context).sendEmptyMessageDelayed(1, 120000);
    }

    @DexIgnore
    public Dialog a(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return a((Context) activity, i, g12.a(activity, a((Context) activity, i, "d"), i2), onCancelListener);
    }

    @DexIgnore
    public final String b() {
        String str;
        synchronized (d) {
            str = this.c;
        }
        return str;
    }

    @DexIgnore
    public int c(Context context) {
        return super.c(context);
    }

    @DexIgnore
    public final boolean c(int i) {
        return super.c(i);
    }

    @DexIgnore
    public final boolean a(Activity activity, tw1 tw1, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a((Context) activity, i, g12.a(tw1, a((Context) activity, i, "d"), 2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    @DexIgnore
    public final String b(int i) {
        return super.b(i);
    }

    @DexIgnore
    public final boolean a(Context context, gv1 gv1, int i) {
        PendingIntent a2 = a(context, gv1);
        if (a2 == null) {
            return false;
        }
        a(context, gv1.p(), (String) null, GoogleApiActivity.a(context, a2, i));
        return true;
    }

    @DexIgnore
    public static Dialog a(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, (AttributeSet) null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(f12.b(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        a(activity, (Dialog) create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    @DexIgnore
    public final vy1 a(Context context, uy1 uy1) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        vy1 vy1 = new vy1(uy1);
        context.registerReceiver(vy1, intentFilter);
        vy1.a(context);
        if (a(context, "com.google.android.gms")) {
            return vy1;
        }
        uy1.a();
        vy1.a();
        return null;
    }

    @DexIgnore
    public int a(Context context, int i) {
        return super.a(context, i);
    }

    @DexIgnore
    public Intent a(Context context, int i, String str) {
        return super.a(context, i, str);
    }

    @DexIgnore
    public PendingIntent a(Context context, int i, int i2) {
        return super.a(context, i, i2);
    }

    @DexIgnore
    public PendingIntent a(Context context, gv1 gv1) {
        if (gv1.D()) {
            return gv1.C();
        }
        return a(context, gv1.p(), 0);
    }

    @DexIgnore
    public static Dialog a(Context context, int i, g12 g12, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(f12.b(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String a2 = f12.a(context, i);
        if (a2 != null) {
            builder.setPositiveButton(a2, g12);
        }
        String e2 = f12.e(context, i);
        if (e2 != null) {
            builder.setTitle(e2);
        }
        return builder.create();
    }

    @DexIgnore
    public static void a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            SupportErrorDialogFragment.a(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        hv1.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @DexIgnore
    @TargetApi(20)
    public final void a(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        if (i == 18) {
            d(context);
        } else if (pendingIntent != null) {
            String d2 = f12.d(context, i);
            String c2 = f12.c(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            p6.d dVar = new p6.d(context);
            dVar.b(true);
            dVar.a(true);
            dVar.b((CharSequence) d2);
            p6.c cVar = new p6.c();
            cVar.a((CharSequence) c2);
            dVar.a((p6.e) cVar);
            if (o42.b(context)) {
                w12.b(s42.f());
                dVar.e(context.getApplicationInfo().icon);
                dVar.d(2);
                if (o42.c(context)) {
                    dVar.a(cv1.common_full_open_on_phone, (CharSequence) resources.getString(dv1.common_open_on_phone), pendingIntent);
                } else {
                    dVar.a(pendingIntent);
                }
            } else {
                dVar.e(17301642);
                dVar.c((CharSequence) resources.getString(dv1.common_google_play_services_notification_ticker));
                dVar.a(System.currentTimeMillis());
                dVar.a(pendingIntent);
                dVar.a((CharSequence) c2);
            }
            if (s42.i()) {
                w12.b(s42.i());
                String b = b();
                if (b == null) {
                    b = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel(b);
                    String b2 = f12.b(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(b, b2, 4));
                    } else if (!b2.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(b2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                dVar.b(b);
            }
            Notification a2 = dVar.a();
            if (i == 1 || i == 2 || i == 3) {
                i2 = nv1.GMS_AVAILABILITY_NOTIFICATION_ID;
                nv1.sCanceledAvailabilityNotification.set(false);
            } else {
                i2 = nv1.GMS_GENERAL_ERROR_NOTIFICATION_ID;
            }
            notificationManager.notify(i2, a2);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
