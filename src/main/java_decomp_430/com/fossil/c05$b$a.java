package com.fossil;

import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class c05$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ InactivityNudgeTimePresenter.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ c05$b$a a;

        @DexIgnore
        public a(c05$b$a c05_b_a) {
            this.a = c05_b_a;
        }

        @DexIgnore
        public final void run() {
            InactivityNudgeTimeDao inactivityNudgeTimeDao = this.a.this$0.this$0.j.getInactivityNudgeTimeDao();
            InactivityNudgeTimeModel c = this.a.this$0.this$0.g;
            if (c != null) {
                inactivityNudgeTimeDao.upsertInactivityNudgeTime(c);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c05$b$a(InactivityNudgeTimePresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        c05$b$a c05_b_a = new c05$b$a(this.this$0, xe6);
        c05_b_a.p$ = (il6) obj;
        return c05_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((c05$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.j.runInTransaction(new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
