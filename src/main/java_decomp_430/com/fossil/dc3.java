package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dc3 {
    @DexIgnore
    public static ScheduledExecutorService l;
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ PowerManager.WakeLock b;
    @DexIgnore
    public WorkSource c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ Map<String, Integer[]> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public AtomicInteger k;

    @DexIgnore
    public interface a {
    }

    /*
    static {
        new ec3();
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public dc3(Context context, int i2, String str) {
        this(context, i2, str, (String) null, context == null ? null : context.getPackageName());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (r2 == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005c, code lost:
        if (r13.j == 0) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005e, code lost:
        com.fossil.e42.a().a(r13.g, com.fossil.d42.a(r13.b, r6), 7, r13.e, r6, (java.lang.String) null, r13.d, b(), r14);
        r13.j++;
     */
    @DexIgnore
    public void a(long j2) {
        this.k.incrementAndGet();
        String a2 = a((String) null);
        synchronized (this.a) {
            boolean z = false;
            if ((!this.i.isEmpty() || this.j > 0) && !this.b.isHeld()) {
                this.i.clear();
                this.j = 0;
            }
            if (this.h) {
                Integer[] numArr = this.i.get(a2);
                if (numArr == null) {
                    this.i.put(a2, new Integer[]{1});
                    z = true;
                } else {
                    numArr[0] = Integer.valueOf(numArr[0].intValue() + 1);
                }
            }
            if (!this.h) {
            }
        }
        this.b.acquire();
        if (j2 > 0) {
            l.schedule(new fc3(this), j2, TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public final List<String> b() {
        return w42.a(this.c);
    }

    @DexIgnore
    public dc3(Context context, int i2, String str, String str2, String str3) {
        this(context, i2, str, (String) null, str3, (String) null);
    }

    @DexIgnore
    @SuppressLint({"UnwrappedWakeLock"})
    public dc3(Context context, int i2, String str, String str2, String str3, String str4) {
        this.a = this;
        this.h = true;
        this.i = new HashMap();
        Collections.synchronizedSet(new HashSet());
        this.k = new AtomicInteger(0);
        w12.a(context, (Object) "WakeLock: context must not be null");
        w12.a(str, (Object) "WakeLock: wakeLockName must not be empty");
        this.d = i2;
        this.f = null;
        this.g = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(str);
            this.e = valueOf.length() != 0 ? "*gcore*:".concat(valueOf) : new String("*gcore*:");
        } else {
            this.e = str;
        }
        this.b = ((PowerManager) context.getSystemService("power")).newWakeLock(i2, str);
        if (w42.a(context)) {
            this.c = w42.a(context, u42.a(str3) ? context.getPackageName() : str3);
            WorkSource workSource = this.c;
            if (workSource != null && w42.a(this.g)) {
                WorkSource workSource2 = this.c;
                if (workSource2 != null) {
                    workSource2.add(workSource);
                } else {
                    this.c = workSource;
                }
                try {
                    this.b.setWorkSource(this.c);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e2) {
                    Log.wtf("WakeLock", e2.toString());
                }
            }
        }
        if (l == null) {
            l = z32.a().a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        if (r1 != false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r12.j == 1) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
        com.fossil.e42.a().a(r12.g, com.fossil.d42.a(r12.b, r6), 8, r12.e, r6, (java.lang.String) null, r12.d, b());
        r12.j--;
     */
    @DexIgnore
    public void a() {
        boolean z;
        if (this.k.decrementAndGet() < 0) {
            Log.e("WakeLock", String.valueOf(this.e).concat(" release without a matched acquire!"));
        }
        String a2 = a((String) null);
        synchronized (this.a) {
            if (this.h) {
                Integer[] numArr = this.i.get(a2);
                if (numArr != null) {
                    if (numArr[0].intValue() == 1) {
                        this.i.remove(a2);
                        z = true;
                    } else {
                        numArr[0] = Integer.valueOf(numArr[0].intValue() - 1);
                    }
                }
                z = false;
            }
            if (!this.h) {
            }
        }
        a(0);
    }

    @DexIgnore
    public final void a(int i2) {
        if (this.b.isHeld()) {
            try {
                this.b.release();
            } catch (RuntimeException e2) {
                if (e2.getClass().equals(RuntimeException.class)) {
                    Log.e("WakeLock", String.valueOf(this.e).concat(" was already released!"), e2);
                } else {
                    throw e2;
                }
            }
            this.b.isHeld();
        }
    }

    @DexIgnore
    public final String a(String str) {
        if (this.h) {
            return !TextUtils.isEmpty(str) ? str : this.f;
        }
        return this.f;
    }

    @DexIgnore
    public void a(boolean z) {
        this.b.setReferenceCounted(z);
        this.h = z;
    }
}
