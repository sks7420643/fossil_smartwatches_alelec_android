package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r52 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<r52> CREATOR; // = new s52();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ l52 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public r52(String str, IBinder iBinder, boolean z, boolean z2) {
        this.a = str;
        this.b = a(iBinder);
        this.c = z;
        this.d = z2;
    }

    @DexIgnore
    public static l52 a(IBinder iBinder) {
        byte[] bArr;
        if (iBinder == null) {
            return null;
        }
        try {
            x52 zzb = r32.a(iBinder).zzb();
            if (zzb == null) {
                bArr = null;
            } else {
                bArr = (byte[]) z52.e(zzb);
            }
            if (bArr != null) {
                return new m52(bArr);
            }
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
            return null;
        } catch (RemoteException e) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e);
            return null;
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a, false);
        l52 l52 = this.b;
        if (l52 == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            l52 = null;
        } else {
            l52.asBinder();
        }
        g22.a(parcel, 2, (IBinder) l52, false);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public r52(String str, l52 l52, boolean z, boolean z2) {
        this.a = str;
        this.b = l52;
        this.c = z;
        this.d = z2;
    }
}
