package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xd0 {
    @DexIgnore
    public ArrayList<zd0> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements hg6<zd0, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationType a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(NotificationType notificationType) {
            super(1);
            this.a = notificationType;
        }

        @DexIgnore
        public Object invoke(Object obj) {
            return Boolean.valueOf(this.a == ((zd0) obj).getNotificationType());
        }
    }

    @DexIgnore
    public final zd0[] a() {
        Object[] array = this.a.toArray(new zd0[0]);
        if (array != null) {
            return (zd0[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final synchronized xd0 a(NotificationType notificationType, yd0 yd0) {
        vd6.a(this.a, new a(notificationType));
        if (yd0 != null) {
            if (!(yd0.getReplyMessages().length == 0)) {
                this.a.add(new zd0(notificationType, yd0));
            }
        }
        return this;
    }
}
