package com.fossil;

import com.fossil.cm3;
import com.fossil.dm3;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class on3<K, V> extends ul3<K, V> {
    @DexIgnore
    public static /* final */ on3<Object, Object> EMPTY; // = new on3((cm3<K, V>[]) null, (cm3<K, V>[]) null, bm3.EMPTY_ENTRY_ARRAY, 0, 0);
    @DexIgnore
    public static /* final */ double MAX_LOAD_FACTOR; // = 1.2d;
    @DexIgnore
    public /* final */ transient cm3<K, V>[] e;
    @DexIgnore
    public /* final */ transient cm3<K, V>[] f;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] g;
    @DexIgnore
    public /* final */ transient int h;
    @DexIgnore
    public /* final */ transient int i;
    @DexIgnore
    public transient ul3<V, K> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends ul3<V, K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a extends dm3<V, K> {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.on3$b$a$a")
            /* renamed from: com.fossil.on3$b$a$a  reason: collision with other inner class name */
            public class C0035a extends tl3<Map.Entry<V, K>> {
                @DexIgnore
                public C0035a() {
                }

                @DexIgnore
                public vl3<Map.Entry<V, K>> delegateCollection() {
                    return a.this;
                }

                @DexIgnore
                public Map.Entry<V, K> get(int i) {
                    Map.Entry entry = on3.this.g[i];
                    return ym3.a(entry.getValue(), entry.getKey());
                }
            }

            @DexIgnore
            public a() {
            }

            @DexIgnore
            public zl3<Map.Entry<V, K>> createAsList() {
                return new C0035a();
            }

            @DexIgnore
            public int hashCode() {
                return on3.this.i;
            }

            @DexIgnore
            public boolean isHashCodeFast() {
                return true;
            }

            @DexIgnore
            public bm3<V, K> map() {
                return b.this;
            }

            @DexIgnore
            public jo3<Map.Entry<V, K>> iterator() {
                return asList().iterator();
            }
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public im3<Map.Entry<V, K>> createEntrySet() {
            return new a();
        }

        @DexIgnore
        public K get(Object obj) {
            if (!(obj == null || on3.this.f == null)) {
                for (cm3 cm3 = on3.this.f[sl3.a(obj.hashCode()) & on3.this.h]; cm3 != null; cm3 = cm3.getNextInValueBucket()) {
                    if (obj.equals(cm3.getValue())) {
                        return cm3.getKey();
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        public int size() {
            return inverse().size();
        }

        @DexIgnore
        public Object writeReplace() {
            return new c(on3.this);
        }

        @DexIgnore
        public ul3<K, V> inverse() {
            return on3.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;
        @DexIgnore
        public /* final */ ul3<K, V> forward;

        @DexIgnore
        public c(ul3<K, V> ul3) {
            this.forward = ul3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.forward.inverse();
        }
    }

    @DexIgnore
    public on3(cm3<K, V>[] cm3Arr, cm3<K, V>[] cm3Arr2, Map.Entry<K, V>[] entryArr, int i2, int i3) {
        this.e = cm3Arr;
        this.f = cm3Arr2;
        this.g = entryArr;
        this.h = i2;
        this.i = i3;
    }

    @DexIgnore
    public static void a(Object obj, Map.Entry<?, ?> entry, cm3<?, ?> cm3) {
        while (cm3 != null) {
            bm3.checkNoConflict(!obj.equals(cm3.getValue()), "value", entry, cm3);
            cm3 = cm3.getNextInValueBucket();
        }
    }

    @DexIgnore
    public static <K, V> on3<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> on3<K, V> fromEntryArray(int i2, Map.Entry<K, V>[] entryArr) {
        cm3[] cm3Arr;
        cm3 cm3;
        int i3 = i2;
        Map.Entry<K, V>[] entryArr2 = entryArr;
        jk3.b(i3, entryArr2.length);
        int a2 = sl3.a(i3, 1.2d);
        int i4 = a2 - 1;
        cm3[] createEntryArray = cm3.createEntryArray(a2);
        cm3[] createEntryArray2 = cm3.createEntryArray(a2);
        if (i3 == entryArr2.length) {
            cm3Arr = entryArr2;
        } else {
            cm3Arr = cm3.createEntryArray(i2);
        }
        int i5 = 0;
        int i6 = 0;
        while (i5 < i3) {
            Map.Entry<K, V> entry = entryArr2[i5];
            K key = entry.getKey();
            V value = entry.getValue();
            bl3.a((Object) key, (Object) value);
            int hashCode = key.hashCode();
            int hashCode2 = value.hashCode();
            int a3 = sl3.a(hashCode) & i4;
            int a4 = sl3.a(hashCode2) & i4;
            cm3 cm32 = createEntryArray[a3];
            qn3.checkNoConflictInKeyBucket(key, entry, cm32);
            cm3 cm33 = createEntryArray2[a4];
            a(value, entry, cm33);
            if (cm33 == null && cm32 == null) {
                cm3 = (entry instanceof cm3) && ((cm3) entry).isReusable() ? (cm3) entry : new cm3(key, value);
            } else {
                cm3 = new cm3.a(key, value, cm32, cm33);
            }
            createEntryArray[a3] = cm3;
            createEntryArray2[a4] = cm3;
            cm3Arr[i5] = cm3;
            i6 += hashCode ^ hashCode2;
            i5++;
            i3 = i2;
        }
        return new on3(createEntryArray, createEntryArray2, cm3Arr, i4, i6);
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? im3.of() : new dm3.b(this, this.g);
    }

    @DexIgnore
    public V get(Object obj) {
        cm3<K, V>[] cm3Arr = this.e;
        if (cm3Arr == null) {
            return null;
        }
        return qn3.get(obj, cm3Arr, this.h);
    }

    @DexIgnore
    public int hashCode() {
        return this.i;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.g.length;
    }

    @DexIgnore
    public ul3<V, K> inverse() {
        if (isEmpty()) {
            return ul3.of();
        }
        ul3<V, K> ul3 = this.j;
        if (ul3 != null) {
            return ul3;
        }
        b bVar = new b();
        this.j = bVar;
        return bVar;
    }
}
