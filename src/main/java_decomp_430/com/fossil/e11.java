package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e11 extends vv0 {
    @DexIgnore
    public long J;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ e11(short s, ue1 ue1, int i, int i2) {
        super(du0.LEGACY_GET_SIZE_WRITTEN, s, lx0.LEGACY_GET_FILE_SIZE_WRITTEN, ue1, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            this.J = cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            cw0.a(jSONObject, bm0.SIZE_WRITTEN, (Object) Long.valueOf(this.J));
        }
        return jSONObject;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.SIZE_WRITTEN, (Object) Long.valueOf(this.J));
    }
}
