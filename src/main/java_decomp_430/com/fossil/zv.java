package com.fossil;

import com.fossil.jv;
import java.io.InputStream;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zv implements jv<URL, InputStream> {
    @DexIgnore
    public /* final */ jv<cv, InputStream> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements kv<URL, InputStream> {
        @DexIgnore
        public jv<URL, InputStream> a(nv nvVar) {
            return new zv(nvVar.a(cv.class, InputStream.class));
        }
    }

    @DexIgnore
    public zv(jv<cv, InputStream> jvVar) {
        this.a = jvVar;
    }

    @DexIgnore
    public boolean a(URL url) {
        return true;
    }

    @DexIgnore
    public jv.a<InputStream> a(URL url, int i, int i2, xr xrVar) {
        return this.a.a(new cv(url), i, i2, xrVar);
    }
}
