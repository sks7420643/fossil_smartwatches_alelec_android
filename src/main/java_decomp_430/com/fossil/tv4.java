package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tv4 implements Factory<String> {
    @DexIgnore
    public static String a(rv4 rv4) {
        String c = rv4.c();
        z76.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
