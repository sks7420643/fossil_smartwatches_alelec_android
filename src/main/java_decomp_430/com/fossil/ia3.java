package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.appevents.codeless.ViewIndexer;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.mj2;
import com.fossil.oj2;
import com.fossil.qj2;
import com.fossil.uj2;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia3 extends aa3 {
    @DexIgnore
    public ia3(ea3 ea3) {
        super(ea3);
    }

    @DexIgnore
    public static Object b(mj2 mj2, String str) {
        oj2 a = a(mj2, str);
        if (a == null) {
            return null;
        }
        if (a.o()) {
            return a.p();
        }
        if (a.q()) {
            return Long.valueOf(a.r());
        }
        if (a.s()) {
            return Double.valueOf(a.t());
        }
        return null;
    }

    @DexIgnore
    public final void a(uj2.a aVar, Object obj) {
        w12.a(obj);
        aVar.j();
        aVar.k();
        aVar.l();
        if (obj instanceof String) {
            aVar.b((String) obj);
        } else if (obj instanceof Long) {
            aVar.b(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.a(((Double) obj).doubleValue());
        } else {
            b().t().a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    @DexIgnore
    public final byte[] c(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            b().t().a("Failed to gzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final List<Integer> u() {
        Map<String, String> a = l03.a(this.b.c());
        if (a == null || a.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = l03.R.a(null).intValue();
        Iterator<Map.Entry<String, String>> it = a.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            if (((String) next.getKey()).startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt((String) next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            b().w().a("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    b().w().a("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            b().t().a("Failed to ungzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final void a(oj2.a aVar, Object obj) {
        w12.a(obj);
        aVar.j();
        aVar.k();
        aVar.l();
        if (obj instanceof String) {
            aVar.b((String) obj);
        } else if (obj instanceof Long) {
            aVar.a(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.a(((Double) obj).doubleValue());
        } else {
            b().t().a("Ignoring invalid (type) event param value", obj);
        }
    }

    @DexIgnore
    public static oj2 a(mj2 mj2, String str) {
        for (oj2 next : mj2.n()) {
            if (next.n().equals(str)) {
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public static void a(mj2.a aVar, String str, Object obj) {
        List<oj2> j = aVar.j();
        int i = 0;
        while (true) {
            if (i >= j.size()) {
                i = -1;
                break;
            } else if (str.equals(j.get(i).n())) {
                break;
            } else {
                i++;
            }
        }
        oj2.a y = oj2.y();
        y.a(str);
        if (obj instanceof Long) {
            y.a(((Long) obj).longValue());
        } else if (obj instanceof String) {
            y.b((String) obj);
        } else if (obj instanceof Double) {
            y.a(((Double) obj).doubleValue());
        }
        if (i >= 0) {
            aVar.a(i, y);
        } else {
            aVar.a(y);
        }
    }

    @DexIgnore
    public final String a(pj2 pj2) {
        List<oj2> n;
        if (pj2 == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        for (qj2 next : pj2.n()) {
            if (next != null) {
                a(sb, 1);
                sb.append("bundle {\n");
                if (next.n()) {
                    a(sb, 1, "protocol_version", (Object) Integer.valueOf(next.P()));
                }
                a(sb, 1, "platform", (Object) next.q0());
                if (next.y0()) {
                    a(sb, 1, "gmp_version", (Object) Long.valueOf(next.o()));
                }
                if (next.p()) {
                    a(sb, 1, "uploading_gmp_version", (Object) Long.valueOf(next.q()));
                }
                if (next.S()) {
                    a(sb, 1, "dynamite_version", (Object) Long.valueOf(next.T()));
                }
                if (next.J()) {
                    a(sb, 1, "config_version", (Object) Long.valueOf(next.K()));
                }
                a(sb, 1, "gmp_app_id", (Object) next.B());
                a(sb, 1, "admob_app_id", (Object) next.R());
                a(sb, 1, "app_id", (Object) next.w0());
                a(sb, 1, ViewIndexer.APP_VERSION_PARAM, (Object) next.x0());
                if (next.G()) {
                    a(sb, 1, "app_version_major", (Object) Integer.valueOf(next.H()));
                }
                a(sb, 1, "firebase_instance_id", (Object) next.F());
                if (next.w()) {
                    a(sb, 1, "dev_cert_hash", (Object) Long.valueOf(next.x()));
                }
                a(sb, 1, "app_store", (Object) next.v0());
                if (next.g0()) {
                    a(sb, 1, "upload_timestamp_millis", (Object) Long.valueOf(next.h0()));
                }
                if (next.i0()) {
                    a(sb, 1, "start_timestamp_millis", (Object) Long.valueOf(next.j0()));
                }
                if (next.k0()) {
                    a(sb, 1, "end_timestamp_millis", (Object) Long.valueOf(next.l0()));
                }
                if (next.m0()) {
                    a(sb, 1, "previous_bundle_start_timestamp_millis", (Object) Long.valueOf(next.n0()));
                }
                if (next.o0()) {
                    a(sb, 1, "previous_bundle_end_timestamp_millis", (Object) Long.valueOf(next.p0()));
                }
                a(sb, 1, "app_instance_id", (Object) next.v());
                a(sb, 1, "resettable_device_id", (Object) next.r());
                a(sb, 1, "device_id", (Object) next.I());
                a(sb, 1, "ds_id", (Object) next.N());
                if (next.s()) {
                    a(sb, 1, "limited_ad_tracking", (Object) Boolean.valueOf(next.t()));
                }
                a(sb, 1, "os_version", (Object) next.r0());
                a(sb, 1, "device_model", (Object) next.u());
                a(sb, 1, "user_default_language", (Object) next.s0());
                if (next.t0()) {
                    a(sb, 1, "time_zone_offset_minutes", (Object) Integer.valueOf(next.u0()));
                }
                if (next.y()) {
                    a(sb, 1, "bundle_sequential_index", (Object) Integer.valueOf(next.z()));
                }
                if (next.C()) {
                    a(sb, 1, "service_upload", (Object) Boolean.valueOf(next.D()));
                }
                a(sb, 1, "health_monitor", (Object) next.A());
                if (next.L() && next.M() != 0) {
                    a(sb, 1, "android_id", (Object) Long.valueOf(next.M()));
                }
                if (next.O()) {
                    a(sb, 1, "retry_counter", (Object) Integer.valueOf(next.Q()));
                }
                List<uj2> e0 = next.e0();
                if (e0 != null) {
                    for (uj2 next2 : e0) {
                        if (next2 != null) {
                            a(sb, 2);
                            sb.append("user_property {\n");
                            a(sb, 2, "set_timestamp_millis", (Object) next2.n() ? Long.valueOf(next2.o()) : null);
                            a(sb, 2, "name", (Object) i().c(next2.p()));
                            a(sb, 2, "string_value", (Object) next2.r());
                            a(sb, 2, "int_value", (Object) next2.s() ? Long.valueOf(next2.t()) : null);
                            a(sb, 2, "double_value", (Object) next2.v() ? Double.valueOf(next2.w()) : null);
                            a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<kj2> E = next.E();
                String w0 = next.w0();
                if (E != null) {
                    for (kj2 next3 : E) {
                        if (next3 != null) {
                            a(sb, 2);
                            sb.append("audience_membership {\n");
                            if (next3.n()) {
                                a(sb, 2, "audience_id", (Object) Integer.valueOf(next3.o()));
                            }
                            if (next3.s()) {
                                a(sb, 2, "new_audience", (Object) Boolean.valueOf(next3.t()));
                            }
                            a(sb, 2, "current_data", next3.p(), w0);
                            a(sb, 2, "previous_data", next3.r(), w0);
                            a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<mj2> b0 = next.b0();
                if (b0 != null) {
                    for (mj2 next4 : b0) {
                        if (next4 != null) {
                            a(sb, 2);
                            sb.append("event {\n");
                            a(sb, 2, "name", (Object) i().a(next4.p()));
                            if (next4.q()) {
                                a(sb, 2, "timestamp_millis", (Object) Long.valueOf(next4.r()));
                            }
                            if (next4.s()) {
                                a(sb, 2, "previous_timestamp_millis", (Object) Long.valueOf(next4.t()));
                            }
                            if (next4.v()) {
                                a(sb, 2, "count", (Object) Integer.valueOf(next4.w()));
                            }
                            if (!(next4.o() == 0 || (n = next4.n()) == null)) {
                                for (oj2 next5 : n) {
                                    if (next5 != null) {
                                        a(sb, 3);
                                        sb.append("param {\n");
                                        a(sb, 3, "name", (Object) i().b(next5.n()));
                                        a(sb, 3, "string_value", (Object) next5.p());
                                        a(sb, 3, "int_value", (Object) next5.q() ? Long.valueOf(next5.r()) : null);
                                        a(sb, 3, "double_value", (Object) next5.s() ? Double.valueOf(next5.t()) : null);
                                        a(sb, 3);
                                        sb.append("}\n");
                                    }
                                }
                            }
                            a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                a(sb, 1);
                sb.append("}\n");
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String a(wi2 wi2) {
        if (wi2 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        if (wi2.n()) {
            a(sb, 0, "filter_id", (Object) Integer.valueOf(wi2.o()));
        }
        a(sb, 0, BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, (Object) i().a(wi2.p()));
        String a = a(wi2.v(), wi2.w(), wi2.y());
        if (!a.isEmpty()) {
            a(sb, 0, "filter_type", (Object) a);
        }
        a(sb, 1, "event_count_filter", wi2.t());
        sb.append("  filters {\n");
        for (xi2 a2 : wi2.q()) {
            a(sb, 2, a2);
        }
        a(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String a(zi2 zi2) {
        if (zi2 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        if (zi2.n()) {
            a(sb, 0, "filter_id", (Object) Integer.valueOf(zi2.o()));
        }
        a(sb, 0, "property_name", (Object) i().c(zi2.p()));
        String a = a(zi2.r(), zi2.s(), zi2.v());
        if (!a.isEmpty()) {
            a(sb, 0, "filter_type", (Object) a);
        }
        a(sb, 1, zi2.q());
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public static String a(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("Dynamic ");
        }
        if (z2) {
            sb.append("Sequence ");
        }
        if (z3) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i, String str, sj2 sj2, String str2) {
        if (sj2 != null) {
            a(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (sj2.q() != 0) {
                a(sb, 4);
                sb.append("results: ");
                int i2 = 0;
                for (Long next : sj2.p()) {
                    int i3 = i2 + 1;
                    if (i2 != 0) {
                        sb.append(", ");
                    }
                    sb.append(next);
                    i2 = i3;
                }
                sb.append(10);
            }
            if (sj2.o() != 0) {
                a(sb, 4);
                sb.append("status: ");
                int i4 = 0;
                for (Long next2 : sj2.n()) {
                    int i5 = i4 + 1;
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(next2);
                    i4 = i5;
                }
                sb.append(10);
            }
            if (sj2.s() != 0) {
                a(sb, 4);
                sb.append("dynamic_filter_timestamps: {");
                int i6 = 0;
                for (lj2 next3 : sj2.r()) {
                    int i7 = i6 + 1;
                    if (i6 != 0) {
                        sb.append(", ");
                    }
                    sb.append(next3.n() ? Integer.valueOf(next3.o()) : null);
                    sb.append(":");
                    sb.append(next3.p() ? Long.valueOf(next3.q()) : null);
                    i6 = i7;
                }
                sb.append("}\n");
            }
            if (sj2.v() != 0) {
                a(sb, 4);
                sb.append("sequence_filter_timestamps: {");
                int i8 = 0;
                for (tj2 next4 : sj2.t()) {
                    int i9 = i8 + 1;
                    if (i8 != 0) {
                        sb.append(", ");
                    }
                    sb.append(next4.n() ? Integer.valueOf(next4.o()) : null);
                    sb.append(": [");
                    int i10 = 0;
                    for (Long longValue : next4.p()) {
                        long longValue2 = longValue.longValue();
                        int i11 = i10 + 1;
                        if (i10 != 0) {
                            sb.append(", ");
                        }
                        sb.append(longValue2);
                        i10 = i11;
                    }
                    sb.append("]");
                    i8 = i9;
                }
                sb.append("}\n");
            }
            a(sb, 3);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, String str, yi2 yi2) {
        if (yi2 != null) {
            a(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (yi2.n()) {
                a(sb, i, "comparison_type", (Object) yi2.o().name());
            }
            if (yi2.p()) {
                a(sb, i, "match_as_float", (Object) Boolean.valueOf(yi2.q()));
            }
            a(sb, i, "comparison_value", (Object) yi2.s());
            a(sb, i, "min_comparison_value", (Object) yi2.v());
            a(sb, i, "max_comparison_value", (Object) yi2.x());
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, xi2 xi2) {
        if (xi2 != null) {
            a(sb, i);
            sb.append("filter {\n");
            if (xi2.r()) {
                a(sb, i, "complement", (Object) Boolean.valueOf(xi2.s()));
            }
            a(sb, i, "param_name", (Object) i().b(xi2.t()));
            int i2 = i + 1;
            aj2 o = xi2.o();
            if (o != null) {
                a(sb, i2);
                sb.append("string_filter");
                sb.append(" {\n");
                if (o.n()) {
                    a(sb, i2, "match_type", (Object) o.o().name());
                }
                a(sb, i2, "expression", (Object) o.q());
                if (o.r()) {
                    a(sb, i2, "case_sensitive", (Object) Boolean.valueOf(o.s()));
                }
                if (o.v() > 0) {
                    a(sb, i2 + 1);
                    sb.append("expression_list {\n");
                    for (String append : o.t()) {
                        a(sb, i2 + 2);
                        sb.append(append);
                        sb.append("\n");
                    }
                    sb.append("}\n");
                }
                a(sb, i2);
                sb.append("}\n");
            }
            a(sb, i2, "number_filter", xi2.q());
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            a(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:9|10|11|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        b().t().a("Failed to load parcelable from buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r1.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r5 = move-exception;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001c */
    public final <T extends Parcelable> T a(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        T t = (Parcelable) creator.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }

    @DexIgnore
    public final boolean a(j03 j03, ra3 ra3) {
        w12.a(j03);
        w12.a(ra3);
        if (!lr2.a() || !l().a(l03.R0)) {
            if (!TextUtils.isEmpty(ra3.b) || !TextUtils.isEmpty(ra3.v)) {
                return true;
            }
            d();
            return false;
        } else if (!TextUtils.isEmpty(ra3.b) || !TextUtils.isEmpty(ra3.v)) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    @DexIgnore
    public static boolean a(List<Long> list, int i) {
        if (i >= (list.size() << 6)) {
            return false;
        }
        return ((1 << (i % 64)) & list.get(i / 64).longValue()) != 0;
    }

    @DexIgnore
    public static List<Long> a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            long j = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i << 6) + i2;
                if (i3 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i3)) {
                    j |= 1 << i2;
                }
            }
            arrayList.add(Long.valueOf(j));
        }
        return arrayList;
    }

    @DexIgnore
    public final List<Long> a(List<Long> list, List<Integer> list2) {
        int i;
        ArrayList arrayList = new ArrayList(list);
        for (Integer next : list2) {
            if (next.intValue() < 0) {
                b().w().a("Ignoring negative bit index to be cleared", next);
            } else {
                int intValue = next.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    b().w().a("Ignoring bit index greater than bitSet size", next, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf(((Long) arrayList.get(intValue)).longValue() & (~(1 << (next.intValue() % 64)))));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        while (true) {
            int i2 = size2;
            i = size;
            size = i2;
            if (size >= 0 && ((Long) arrayList.get(size)).longValue() == 0) {
                size2 = size - 1;
            }
        }
        return arrayList.subList(0, i);
    }

    @DexIgnore
    public final boolean a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(zzm().b() - j) > j2;
    }

    @DexIgnore
    public final long a(byte[] bArr) {
        w12.a(bArr);
        j().g();
        MessageDigest x = ma3.x();
        if (x != null) {
            return ma3.a(x.digest(bArr));
        }
        b().t().a("Failed to get MD5");
        return 0;
    }

    @DexIgnore
    public static <Builder extends qo2> Builder a(Builder builder, byte[] bArr) throws qn2 {
        sm2 b = sm2.b();
        if (b != null) {
            builder.a(bArr, b);
            return builder;
        }
        builder.a(bArr);
        return builder;
    }

    @DexIgnore
    public static int a(qj2.a aVar, String str) {
        if (aVar == null) {
            return -1;
        }
        for (int i = 0; i < aVar.n(); i++) {
            if (str.equals(aVar.d(i).p())) {
                return i;
            }
        }
        return -1;
    }
}
