package com.fossil;

import java.nio.ByteBuffer;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kq0 {
    @DexIgnore
    public short a; // = -1;
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ rg1 f;

    @DexIgnore
    public kq0(byte[] bArr, int i, rg1 rg1) {
        this.d = bArr;
        this.e = i;
        this.f = rg1;
        ByteBuffer wrap = ByteBuffer.wrap(this.d);
        wg6.a(wrap, "ByteBuffer.wrap(data)");
        this.b = wrap;
        byte[] bArr2 = this.d;
        this.c = bArr2.length;
        ByteBuffer wrap2 = ByteBuffer.wrap(bArr2);
        wg6.a(wrap2, "ByteBuffer.wrap(data)");
        this.b = wrap2;
    }

    @DexIgnore
    public final byte[] a() {
        int remaining = this.b.remaining();
        if (remaining <= 0) {
            return new byte[0];
        }
        this.a = (short) (this.a + 1);
        byte[] bArr = {(byte) (((ds0) this).a % 256)};
        int min = Math.min(remaining + bArr.length, this.e);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, min);
        this.b.get(copyOfRange, bArr.length, min - bArr.length);
        wg6.a(copyOfRange, "bytes");
        return copyOfRange;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public final int c() {
        return Math.min((this.a + 1) * (this.e - b()), this.c);
    }
}
