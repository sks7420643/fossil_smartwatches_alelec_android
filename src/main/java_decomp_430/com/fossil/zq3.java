package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq3 extends cr3<Void> {
    @DexIgnore
    public zq3(int i, int i2, Bundle bundle) {
        super(i, 2, bundle);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            a(null);
        } else {
            a(new br3(4, "Invalid response to one way request"));
        }
    }

    @DexIgnore
    public final boolean a() {
        return true;
    }
}
