package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ku6 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public Map optionMap; // = new HashMap();
    @DexIgnore
    public boolean required;
    @DexIgnore
    public String selected;

    @DexIgnore
    public ku6 addOption(ju6 ju6) {
        this.optionMap.put(ju6.getKey(), ju6);
        return this;
    }

    @DexIgnore
    public Collection getNames() {
        return this.optionMap.keySet();
    }

    @DexIgnore
    public Collection getOptions() {
        return this.optionMap.values();
    }

    @DexIgnore
    public String getSelected() {
        return this.selected;
    }

    @DexIgnore
    public boolean isRequired() {
        return this.required;
    }

    @DexIgnore
    public void setRequired(boolean z) {
        this.required = z;
    }

    @DexIgnore
    public void setSelected(ju6 ju6) throws cu6 {
        String str = this.selected;
        if (str == null || str.equals(ju6.getOpt())) {
            this.selected = ju6.getOpt();
            return;
        }
        throw new cu6(this, ju6);
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = getOptions().iterator();
        stringBuffer.append("[");
        while (it.hasNext()) {
            ju6 ju6 = (ju6) it.next();
            if (ju6.getOpt() != null) {
                stringBuffer.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                stringBuffer.append(ju6.getOpt());
            } else {
                stringBuffer.append("--");
                stringBuffer.append(ju6.getLongOpt());
            }
            stringBuffer.append(" ");
            stringBuffer.append(ju6.getDescription());
            if (it.hasNext()) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
