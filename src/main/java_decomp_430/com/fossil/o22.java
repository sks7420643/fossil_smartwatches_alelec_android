package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o22 extends i22 {
    @DexIgnore
    public /* final */ ow1<Status> a;

    @DexIgnore
    public o22(ow1<Status> ow1) {
        this.a = ow1;
    }

    @DexIgnore
    public final void f(int i) throws RemoteException {
        this.a.a(new Status(i));
    }
}
