package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm0 implements Parcelable.Creator<fo0> {
    @DexIgnore
    public /* synthetic */ nm0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        return new fo0(parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new fo0[i];
    }
}
