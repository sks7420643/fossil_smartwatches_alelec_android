package com.fossil;

import com.facebook.internal.FileLruCache;
import com.fossil.ox3;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uw3 extends rw3 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(uw3.class.getName());
    @DexIgnore
    public static /* final */ boolean b; // = nx3.d();
    @DexIgnore
    public static /* final */ long c; // = nx3.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b extends uw3 {
        @DexIgnore
        public /* final */ byte[] d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;

        @DexIgnore
        public b(int i) {
            super();
            if (i >= 0) {
                this.d = new byte[Math.max(i, 20)];
                this.e = this.d.length;
                return;
            }
            throw new IllegalArgumentException("bufferSize must be >= 0");
        }

        @DexIgnore
        public final void e(int i, int i2) {
            i(px3.a(i, i2));
        }

        @DexIgnore
        public final void h(int i) {
            if (i >= 0) {
                i(i);
            } else {
                e((long) i);
            }
        }

        @DexIgnore
        public final void i(int i) {
            if (uw3.b) {
                long c = uw3.c + ((long) this.f);
                long j = c;
                while ((i & -128) != 0) {
                    nx3.a(this.d, j, (byte) ((i & 127) | 128));
                    i >>>= 7;
                    j = 1 + j;
                }
                nx3.a(this.d, j, (byte) i);
                int i2 = (int) ((1 + j) - c);
                this.f += i2;
                this.g += i2;
                return;
            }
            while ((i & -128) != 0) {
                byte[] bArr = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr[i3] = (byte) ((i & 127) | 128);
                this.g++;
                i >>>= 7;
            }
            byte[] bArr2 = this.d;
            int i4 = this.f;
            this.f = i4 + 1;
            bArr2[i4] = (byte) i;
            this.g++;
        }

        @DexIgnore
        public final void e(long j) {
            if (uw3.b) {
                long c = uw3.c + ((long) this.f);
                long j2 = j;
                long j3 = c;
                while ((j2 & -128) != 0) {
                    nx3.a(this.d, j3, (byte) ((((int) j2) & 127) | 128));
                    j2 >>>= 7;
                    j3++;
                }
                nx3.a(this.d, j3, (byte) ((int) j2));
                int i = (int) ((1 + j3) - c);
                this.f += i;
                this.g += i;
                return;
            }
            long j4 = j;
            while ((j4 & -128) != 0) {
                byte[] bArr = this.d;
                int i2 = this.f;
                this.f = i2 + 1;
                bArr[i2] = (byte) ((((int) j4) & 127) | 128);
                this.g++;
                j4 >>>= 7;
            }
            byte[] bArr2 = this.d;
            int i3 = this.f;
            this.f = i3 + 1;
            bArr2[i3] = (byte) ((int) j4);
            this.g++;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends uw3 {
        @DexIgnore
        public /* final */ byte[] d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public c(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i + i2;
                if ((i | i2 | (bArr.length - i3)) >= 0) {
                    this.d = bArr;
                    this.f = i;
                    this.e = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)}));
            }
            throw new NullPointerException(FileLruCache.BufferFile.FILE_NAME_PREFIX);
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public final void a(int i, String str) throws IOException {
            e(i, 2);
            b(str);
        }

        @DexIgnore
        public final void b(int i, int i2) throws IOException {
            e(i, 0);
            h(i2);
        }

        @DexIgnore
        public final void c(int i, long j) throws IOException {
            e(i, 0);
            e(j);
        }

        @DexIgnore
        public final int d() {
            return this.e - this.f;
        }

        @DexIgnore
        public final void e(int i, int i2) throws IOException {
            a(px3.a(i, i2));
        }

        @DexIgnore
        public final void h(int i) throws IOException {
            if (i >= 0) {
                a(i);
            } else {
                e((long) i);
            }
        }

        @DexIgnore
        public final void e(long j) throws IOException {
            if (!uw3.b || d() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.d;
                    int i = this.f;
                    this.f = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i2 = this.f;
                    this.f = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e2) {
                    throw new d(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
                }
            } else {
                long c = uw3.c + ((long) this.f);
                while ((j & -128) != 0) {
                    nx3.a(this.d, c, (byte) ((((int) j) & 127) | 128));
                    this.f++;
                    j >>>= 7;
                    c = 1 + c;
                }
                nx3.a(this.d, c, (byte) ((int) j));
                this.f++;
            }
        }

        @DexIgnore
        public final void a(int i, sw3 sw3) throws IOException {
            e(i, 2);
            b(sw3);
        }

        @DexIgnore
        public final void b(sw3 sw3) throws IOException {
            a(sw3.size());
            sw3.writeTo((rw3) this);
        }

        @DexIgnore
        public final void c(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.d, this.f, i2);
                this.f += i2;
            } catch (IndexOutOfBoundsException e2) {
                throw new d(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), Integer.valueOf(i2)}), e2);
            }
        }

        @DexIgnore
        public final void a(int i, dx3 dx3) throws IOException {
            e(i, 2);
            b(dx3);
        }

        @DexIgnore
        public final void b(dx3 dx3) throws IOException {
            a(dx3.d());
            dx3.a(this);
        }

        @DexIgnore
        public final void a(int i) throws IOException {
            if (!uw3.b || d() < 10) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.d;
                    int i2 = this.f;
                    this.f = i2 + 1;
                    bArr[i2] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i3 = this.f;
                    this.f = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e2) {
                    throw new d(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
                }
            } else {
                long c = uw3.c + ((long) this.f);
                while ((i & -128) != 0) {
                    nx3.a(this.d, c, (byte) ((i & 127) | 128));
                    this.f++;
                    i >>>= 7;
                    c = 1 + c;
                }
                nx3.a(this.d, c, (byte) i);
                this.f++;
            }
        }

        @DexIgnore
        public final void b(String str) throws IOException {
            int i = this.f;
            try {
                int g = uw3.g(str.length() * 3);
                int g2 = uw3.g(str.length());
                if (g2 == g) {
                    this.f = i + g2;
                    int a = ox3.a((CharSequence) str, this.d, this.f, d());
                    this.f = i;
                    a((a - i) - g2);
                    this.f = a;
                    return;
                }
                a(ox3.a((CharSequence) str));
                this.f = ox3.a((CharSequence) str, this.d, this.f, d());
            } catch (ox3.c e2) {
                this.f = i;
                a(str, e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new d(e3);
            }
        }

        @DexIgnore
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            c(bArr, i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends IOException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = -6947486886997889499L;

        @DexIgnore
        public d() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        @DexIgnore
        public d(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        @DexIgnore
        public d(String str, Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.: " + str, th);
        }
    }

    @DexIgnore
    public static uw3 a(OutputStream outputStream, int i) {
        return new e(outputStream, i);
    }

    @DexIgnore
    public static int c(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    @DexIgnore
    public static int d(int i, int i2) {
        return f(i) + c(i2);
    }

    @DexIgnore
    public static long d(long j) {
        return (j >> 63) ^ (j << 1);
    }

    @DexIgnore
    public static int e(int i) {
        if (i > 4096) {
            return 4096;
        }
        return i;
    }

    @DexIgnore
    public static int e(int i, long j) {
        return f(i) + b(j);
    }

    @DexIgnore
    public static int f(int i) {
        return g(px3.a(i, 0));
    }

    @DexIgnore
    public static int g(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    @DexIgnore
    public abstract void a() throws IOException;

    @DexIgnore
    public abstract void a(int i) throws IOException;

    @DexIgnore
    public abstract void a(int i, dx3 dx3) throws IOException;

    @DexIgnore
    public abstract void a(int i, sw3 sw3) throws IOException;

    @DexIgnore
    public abstract void a(int i, String str) throws IOException;

    @DexIgnore
    public abstract void b(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void c(int i, long j) throws IOException;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends b {
        @DexIgnore
        public /* final */ OutputStream h;

        @DexIgnore
        public e(OutputStream outputStream, int i) {
            super(i);
            if (outputStream != null) {
                this.h = outputStream;
                return;
            }
            throw new NullPointerException("out");
        }

        @DexIgnore
        public void a(int i, String str) throws IOException {
            f(i, 2);
            b(str);
        }

        @DexIgnore
        public void b(int i, int i2) throws IOException {
            j(20);
            e(i, 0);
            h(i2);
        }

        @DexIgnore
        public void c(int i, long j) throws IOException {
            j(20);
            e(i, 0);
            e(j);
        }

        @DexIgnore
        public final void d() throws IOException {
            this.h.write(this.d, 0, this.f);
            this.f = 0;
        }

        @DexIgnore
        public void f(int i, int i2) throws IOException {
            a(px3.a(i, i2));
        }

        @DexIgnore
        public final void j(int i) throws IOException {
            if (this.e - this.f < i) {
                d();
            }
        }

        @DexIgnore
        public void a(int i, sw3 sw3) throws IOException {
            f(i, 2);
            b(sw3);
        }

        @DexIgnore
        public void b(sw3 sw3) throws IOException {
            a(sw3.size());
            sw3.writeTo((rw3) this);
        }

        @DexIgnore
        public void c(byte[] bArr, int i, int i2) throws IOException {
            int i3 = this.e;
            int i4 = this.f;
            if (i3 - i4 >= i2) {
                System.arraycopy(bArr, i, this.d, i4, i2);
                this.f += i2;
                this.g += i2;
                return;
            }
            int i5 = i3 - i4;
            System.arraycopy(bArr, i, this.d, i4, i5);
            int i6 = i + i5;
            int i7 = i2 - i5;
            this.f = this.e;
            this.g += i5;
            d();
            if (i7 <= this.e) {
                System.arraycopy(bArr, i6, this.d, 0, i7);
                this.f = i7;
            } else {
                this.h.write(bArr, i6, i7);
            }
            this.g += i7;
        }

        @DexIgnore
        public void a(int i, dx3 dx3) throws IOException {
            f(i, 2);
            b(dx3);
        }

        @DexIgnore
        public void b(dx3 dx3) throws IOException {
            a(dx3.d());
            dx3.a(this);
        }

        @DexIgnore
        public void a(int i) throws IOException {
            j(10);
            i(i);
        }

        @DexIgnore
        public void b(String str) throws IOException {
            int i;
            int i2;
            try {
                int length = str.length() * 3;
                int g = uw3.g(length);
                int i3 = g + length;
                if (i3 > this.e) {
                    byte[] bArr = new byte[length];
                    int a = ox3.a((CharSequence) str, bArr, 0, length);
                    a(a);
                    a(bArr, 0, a);
                    return;
                }
                if (i3 > this.e - this.f) {
                    d();
                }
                int g2 = uw3.g(str.length());
                i = this.f;
                if (g2 == g) {
                    this.f = i + g2;
                    int a2 = ox3.a((CharSequence) str, this.d, this.f, this.e - this.f);
                    this.f = i;
                    i2 = (a2 - i) - g2;
                    i(i2);
                    this.f = a2;
                } else {
                    i2 = ox3.a((CharSequence) str);
                    i(i2);
                    this.f = ox3.a((CharSequence) str, this.d, this.f, i2);
                }
                this.g += i2;
            } catch (ox3.c e) {
                this.g -= this.f - i;
                this.f = i;
                throw e;
            } catch (ArrayIndexOutOfBoundsException e2) {
                throw new d(e2);
            } catch (ox3.c e3) {
                a(str, e3);
            }
        }

        @DexIgnore
        public void a() throws IOException {
            if (this.f > 0) {
                d();
            }
        }

        @DexIgnore
        public void a(byte[] bArr, int i, int i2) throws IOException {
            c(bArr, i, i2);
        }
    }

    @DexIgnore
    public uw3() {
    }

    @DexIgnore
    public static uw3 a(byte[] bArr) {
        return b(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static uw3 b(byte[] bArr, int i, int i2) {
        return new c(bArr, i, i2);
    }

    @DexIgnore
    public static int c(int i, int i2) {
        return f(i) + b(i2);
    }

    @DexIgnore
    public static int d(int i, long j) {
        return f(i) + a(j);
    }

    @DexIgnore
    public static int c(int i) {
        if (i >= 0) {
            return g(i);
        }
        return 10;
    }

    @DexIgnore
    public static int d(int i) {
        return g(i) + i;
    }

    @DexIgnore
    public final void a(int i, long j) throws IOException {
        c(i, j);
    }

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        c(i, d(j));
    }

    @DexIgnore
    public static int b(int i, String str) {
        return f(i) + a(str);
    }

    @DexIgnore
    public final void a(int i, int i2) throws IOException {
        b(i, i2);
    }

    @DexIgnore
    public static int a(long j) {
        return c(j);
    }

    @DexIgnore
    public static int b(int i, sw3 sw3) {
        return f(i) + a(sw3);
    }

    @DexIgnore
    public static int a(String str) {
        int i;
        try {
            i = ox3.a((CharSequence) str);
        } catch (ox3.c unused) {
            i = str.getBytes(zw3.a).length;
        }
        return d(i);
    }

    @DexIgnore
    public static int b(int i, dx3 dx3) {
        return f(i) + a(dx3);
    }

    @DexIgnore
    public static int b(long j) {
        return c(d(j));
    }

    @DexIgnore
    public static int b(int i) {
        return c(i);
    }

    @DexIgnore
    public static int a(sw3 sw3) {
        return d(sw3.size());
    }

    @DexIgnore
    public static int a(dx3 dx3) {
        return d(dx3.d());
    }

    @DexIgnore
    public final void a(String str, ox3.c cVar) throws IOException {
        a.log(Level.WARNING, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", cVar);
        byte[] bytes = str.getBytes(zw3.a);
        try {
            a(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e2) {
            throw new d(e2);
        } catch (d e3) {
            throw e3;
        }
    }
}
