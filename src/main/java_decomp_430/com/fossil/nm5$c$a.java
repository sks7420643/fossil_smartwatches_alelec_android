package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1$1", f = "ThemesViewModel.kt", l = {68, 69}, m = "invokeSuspend")
public final class nm5$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemesViewModel.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nm5$c$a(ThemesViewModel.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        nm5$c$a nm5_c_a = new nm5$c$a(this.this$0, xe6);
        nm5_c_a.p$ = (il6) obj;
        return nm5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((nm5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = ThemesViewModel.h;
            local.d(e, "apply theme " + this.this$0.this$0.d);
            ThemeRepository e2 = this.this$0.this$0.g;
            String g = this.this$0.this$0.d;
            this.L$0 = il6;
            this.label = 1;
            if (e2.setCurrentThemeId(g, this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            this.this$0.this$0.f.a(true);
            this.this$0.this$0.b();
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ThemeManager a2 = ThemeManager.l.a();
        this.L$0 = il6;
        this.label = 2;
        if (a2.a((xe6<? super cd6>) this) == a) {
            return a;
        }
        this.this$0.this$0.f.a(true);
        this.this$0.this$0.b();
        return cd6.a;
    }
}
