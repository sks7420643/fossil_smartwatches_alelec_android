package com.fossil;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.zip.Adler32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dr1 implements rr1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ur1 b;
    @DexIgnore
    public /* final */ fr1 c;

    @DexIgnore
    public dr1(Context context, ur1 ur1, fr1 fr1) {
        this.a = context;
        this.b = ur1;
        this.c = fr1;
    }

    @DexIgnore
    public int a(rp1 rp1) {
        Adler32 adler32 = new Adler32();
        adler32.update(this.a.getPackageName().getBytes(Charset.forName("UTF-8")));
        adler32.update(rp1.a().getBytes(Charset.forName("UTF-8")));
        adler32.update(ByteBuffer.allocate(4).putInt(ft1.a(rp1.c())).array());
        if (rp1.b() != null) {
            adler32.update(rp1.b());
        }
        return (int) adler32.getValue();
    }

    @DexIgnore
    public final boolean a(JobScheduler jobScheduler, int i, int i2) {
        for (JobInfo next : jobScheduler.getAllPendingJobs()) {
            int i3 = next.getExtras().getInt("attemptNumber");
            if (next.getId() == i) {
                if (i3 >= i2) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(rp1 rp1, int i) {
        ComponentName componentName = new ComponentName(this.a, JobInfoSchedulerService.class);
        JobScheduler jobScheduler = (JobScheduler) this.a.getSystemService("jobscheduler");
        int a2 = a(rp1);
        if (a(jobScheduler, a2, i)) {
            mq1.a("JobInfoScheduler", "Upload for context %s is already scheduled. Returning...", (Object) rp1);
            return;
        }
        long b2 = this.b.b(rp1);
        fr1 fr1 = this.c;
        JobInfo.Builder builder = new JobInfo.Builder(a2, componentName);
        fr1.a(builder, rp1.c(), b2, i);
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putInt("attemptNumber", i);
        persistableBundle.putString("backendName", rp1.a());
        persistableBundle.putInt("priority", ft1.a(rp1.c()));
        if (rp1.b() != null) {
            persistableBundle.putString(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(rp1.b(), 0));
        }
        builder.setExtras(persistableBundle);
        mq1.a("JobInfoScheduler", "Scheduling upload for context %s with jobId=%d in %dms(Backend next call timestamp %d). Attempt %d", rp1, Integer.valueOf(a2), Long.valueOf(this.c.a(rp1.c(), b2, i)), Long.valueOf(b2), Integer.valueOf(i));
        jobScheduler.schedule(builder.build());
    }
}
