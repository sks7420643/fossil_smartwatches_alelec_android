package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik3<T> extends ak3<Iterable<T>> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ ak3<? super T> elementEquivalence;

    @DexIgnore
    public ik3(ak3<? super T> ak3) {
        jk3.a(ak3);
        this.elementEquivalence = ak3;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ik3) {
            return this.elementEquivalence.equals(((ik3) obj).elementEquivalence);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.elementEquivalence.hashCode() ^ 1185147655;
    }

    @DexIgnore
    public String toString() {
        return this.elementEquivalence + ".pairwise()";
    }

    @DexIgnore
    public boolean doEquivalent(Iterable<T> iterable, Iterable<T> iterable2) {
        Iterator<T> it = iterable.iterator();
        Iterator<T> it2 = iterable2.iterator();
        while (it.hasNext() && it2.hasNext()) {
            if (!this.elementEquivalence.equivalent(it.next(), it2.next())) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int doHash(Iterable<T> iterable) {
        int i = 78721;
        for (T hash : iterable) {
            i = (i * 24943) + this.elementEquivalence.hash(hash);
        }
        return i;
    }
}
