package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpHost;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m56 {
    @DexIgnore
    public static String a; // = null;
    @DexIgnore
    public static String b; // = null;
    @DexIgnore
    public static String c; // = null;
    @DexIgnore
    public static Random d; // = null;
    @DexIgnore
    public static DisplayMetrics e; // = null;
    @DexIgnore
    public static String f; // = null;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static String h; // = "";
    @DexIgnore
    public static int i; // = -1;
    @DexIgnore
    public static b56 j; // = null;
    @DexIgnore
    public static String k; // = null;
    @DexIgnore
    public static String l; // = null;
    @DexIgnore
    public static volatile int m; // = -1;
    @DexIgnore
    public static String n; // = null;
    @DexIgnore
    public static long o; // = -1;
    @DexIgnore
    public static String p; // = "";
    @DexIgnore
    public static String q; // = "__MTA_FIRST_ACTIVATE__";
    @DexIgnore
    public static int r; // = -1;
    @DexIgnore
    public static long s; // = -1;
    @DexIgnore
    public static int t;

    @DexIgnore
    public static void A(Context context) {
        int a2 = q56.a(context, q, 1);
        r = a2;
        if (a2 == 1) {
            q56.b(context, q, 0);
        }
    }

    @DexIgnore
    public static boolean B(Context context) {
        if (s < 0) {
            s = q56.a(context, "mta.qq.com.checktime", 0);
        }
        return Math.abs(System.currentTimeMillis() - s) > LogBuilder.MAX_INTERVAL;
    }

    @DexIgnore
    public static void C(Context context) {
        s = System.currentTimeMillis();
        q56.b(context, "mta.qq.com.checktime", s);
    }

    @DexIgnore
    public static int a() {
        return g().nextInt(Integer.MAX_VALUE);
    }

    @DexIgnore
    public static int a(Context context) {
        return q56.a(context, "mta.qq.com.difftime", 0);
    }

    @DexIgnore
    public static int a(Context context, boolean z) {
        if (z) {
            t = a(context);
        }
        return t;
    }

    @DexIgnore
    public static Long a(String str, String str2, int i2, int i3, Long l2) {
        if (!(str == null || str2 == null)) {
            if (str2.equalsIgnoreCase(".") || str2.equalsIgnoreCase("|")) {
                str2 = "\\" + str2;
            }
            String[] split = str.split(str2);
            if (split.length == i3) {
                try {
                    Long l3 = 0L;
                    for (String valueOf : split) {
                        l3 = Long.valueOf(((long) i2) * (l3.longValue() + Long.valueOf(valueOf).longValue()));
                    }
                    return l3;
                } catch (NumberFormatException unused) {
                }
            }
        }
        return l2;
    }

    @DexIgnore
    public static String a(int i2) {
        Calendar instance = Calendar.getInstance();
        instance.roll(6, i2);
        return new SimpleDateFormat("yyyyMMdd").format(instance.getTime());
    }

    @DexIgnore
    public static String a(long j2) {
        return new SimpleDateFormat("yyyyMMdd").format(new Date(j2));
    }

    @DexIgnore
    public static String a(Context context, String str) {
        if (n36.r()) {
            if (l == null) {
                l = t(context);
            }
            if (l != null) {
                return str + "_" + l;
            }
        }
        return str;
    }

    @DexIgnore
    public static String a(String str) {
        if (str == null) {
            return ShareWebViewClient.RESP_SUCC_CODE;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & 255;
                if (b3 < 16) {
                    stringBuffer.append(ShareWebViewClient.RESP_SUCC_CODE);
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (Throwable unused) {
            return ShareWebViewClient.RESP_SUCC_CODE;
        }
    }

    @DexIgnore
    public static void a(Context context, int i2) {
        t = i2;
        q56.b(context, "mta.qq.com.difftime", i2);
    }

    @DexIgnore
    public static boolean a(r36 r36) {
        if (r36 == null) {
            return false;
        }
        return c(r36.a());
    }

    @DexIgnore
    public static byte[] a(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read != -1) {
                byteArrayOutputStream.write(bArr2, 0, read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayInputStream.close();
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            }
        }
    }

    @DexIgnore
    public static long b(String str) {
        return a(str, ".", 100, 3, 0L).longValue();
    }

    @DexIgnore
    public static synchronized b56 b() {
        b56 b56;
        synchronized (m56.class) {
            if (j == null) {
                b56 b562 = new b56("MtaSDK");
                j = b562;
                b562.a(false);
            }
            b56 = j;
        }
        return b56;
    }

    @DexIgnore
    public static String b(Context context) {
        if (context == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        ActivityInfo activityInfo = resolveActivity.activityInfo;
        if (activityInfo != null && !activityInfo.packageName.equals("android")) {
            return resolveActivity.activityInfo.packageName;
        }
        return null;
    }

    @DexIgnore
    public static long c() {
        try {
            Calendar instance = Calendar.getInstance();
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            return instance.getTimeInMillis() + LogBuilder.MAX_INTERVAL;
        } catch (Throwable th) {
            j.a(th);
            return System.currentTimeMillis() + LogBuilder.MAX_INTERVAL;
        }
    }

    @DexIgnore
    public static long c(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    @DexIgnore
    public static boolean c(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }

    @DexIgnore
    public static String d() {
        if (c(n)) {
            return n;
        }
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        String str = String.valueOf((((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks())) / 1000000) + ZendeskConfig.SLASH + String.valueOf(e() / 1000000);
        n = str;
        return str;
    }

    @DexIgnore
    public static HttpHost d(Context context) {
        NetworkInfo activeNetworkInfo;
        String extraInfo;
        if (context == null) {
            return null;
        }
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0 || (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
                return null;
            }
            if ((activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) || (extraInfo = activeNetworkInfo.getExtraInfo()) == null) {
                return null;
            }
            if (!extraInfo.equals("cmwap") && !extraInfo.equals("3gwap")) {
                if (!extraInfo.equals("uniwap")) {
                    if (extraInfo.equals("ctwap")) {
                        return new HttpHost("10.0.0.200", 80);
                    }
                    String defaultHost = Proxy.getDefaultHost();
                    if (defaultHost != null && defaultHost.trim().length() > 0) {
                        return new HttpHost(defaultHost, Proxy.getDefaultPort());
                    }
                    return null;
                }
            }
            return new HttpHost("10.0.0.172", 80);
        } catch (Throwable th) {
            j.a(th);
        }
    }

    @DexIgnore
    public static long e() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    @DexIgnore
    public static synchronized String e(Context context) {
        synchronized (m56.class) {
            if (a == null || a.trim().length() == 0) {
                String a2 = r56.a(context);
                a = a2;
                if (a2 == null || a.trim().length() == 0) {
                    a = Integer.toString(g().nextInt(Integer.MAX_VALUE));
                }
                String str = a;
                return str;
            }
            String str2 = a;
            return str2;
        }
    }

    @DexIgnore
    public static synchronized String f(Context context) {
        String str;
        synchronized (m56.class) {
            if (c == null || c.trim().length() == 0) {
                c = r56.b(context);
            }
            str = c;
        }
        return str;
    }

    @DexIgnore
    public static DisplayMetrics g(Context context) {
        if (e == null) {
            e = new DisplayMetrics();
            ((WindowManager) context.getApplicationContext().getSystemService("window")).getDefaultDisplay().getMetrics(e);
        }
        return e;
    }

    @DexIgnore
    public static synchronized Random g() {
        Random random;
        synchronized (m56.class) {
            if (d == null) {
                d = new Random();
            }
            random = d;
        }
        return random;
    }

    @DexIgnore
    public static long h() {
        long j2 = o;
        if (j2 > 0) {
            return j2;
        }
        long j3 = 1;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            j3 = (long) (Integer.valueOf(bufferedReader.readLine().split("\\s+")[1]).intValue() * 1024);
            bufferedReader.close();
        } catch (Exception unused) {
        }
        o = j3;
        return j3;
    }

    @DexIgnore
    public static boolean h(Context context) {
        NetworkInfo[] allNetworkInfo;
        try {
            if (r56.a(context, "android.permission.ACCESS_WIFI_STATE")) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService("connectivity");
                if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
                    for (int i2 = 0; i2 < allNetworkInfo.length; i2++) {
                        if (allNetworkInfo[i2].getTypeName().equalsIgnoreCase("WIFI") && allNetworkInfo[i2].isConnected()) {
                            return true;
                        }
                    }
                }
                return false;
            }
            j.h("can not get the permission of android.permission.ACCESS_WIFI_STATE");
            return false;
        } catch (Throwable th) {
            j.a(th);
        }
    }

    @DexIgnore
    public static String i(Context context) {
        String str = b;
        if (str != null) {
            return str;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            if (applicationInfo == null) {
                return null;
            }
            String string = applicationInfo.metaData.getString("TA_APPKEY");
            if (string != null) {
                b = string;
                return string;
            }
            j.e("Could not read APPKEY meta-data from AndroidManifest.xml");
            return null;
        } catch (Throwable unused) {
            j.e("Could not read APPKEY meta-data from AndroidManifest.xml");
            return null;
        }
    }

    @DexIgnore
    public static String j(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            if (applicationInfo == null) {
                return null;
            }
            Object obj = applicationInfo.metaData.get("InstallChannel");
            if (obj != null) {
                return obj.toString();
            }
            j.g("Could not read InstallChannel meta-data from AndroidManifest.xml");
            return null;
        } catch (Throwable unused) {
            j.c("Could not read InstallChannel meta-data from AndroidManifest.xml");
            return null;
        }
    }

    @DexIgnore
    public static String k(Context context) {
        if (context == null) {
            return null;
        }
        return context.getClass().getName();
    }

    @DexIgnore
    public static String l(Context context) {
        TelephonyManager telephonyManager;
        String str = f;
        if (str != null) {
            return str;
        }
        try {
            if (!r56.a(context, "android.permission.READ_PHONE_STATE")) {
                j.c("Could not get permission of android.permission.READ_PHONE_STATE");
            } else if (n(context) && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null) {
                f = telephonyManager.getSimOperator();
            }
        } catch (Throwable th) {
            j.a(th);
        }
        return f;
    }

    @DexIgnore
    public static String m(Context context) {
        if (c(g)) {
            return g;
        }
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            g = str;
            if (str == null) {
                return "";
            }
        } catch (Throwable th) {
            j.a(th);
        }
        return g;
    }

    @DexIgnore
    public static boolean n(Context context) {
        return context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0;
    }

    @DexIgnore
    public static String o(Context context) {
        try {
            if (!r56.a(context, "android.permission.INTERNET") || !r56.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
                j.c("can not get the permission of android.permission.ACCESS_WIFI_STATE");
                return "";
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                String typeName = activeNetworkInfo.getTypeName();
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (typeName != null) {
                    if (typeName.equalsIgnoreCase("WIFI")) {
                        return "WIFI";
                    }
                    if (typeName.equalsIgnoreCase("MOBILE")) {
                        if (extraInfo == null) {
                            return "MOBILE";
                        }
                    } else if (extraInfo == null) {
                        return typeName;
                    }
                    return extraInfo;
                }
            }
            return "";
        } catch (Throwable th) {
            j.a(th);
        }
    }

    @DexIgnore
    public static Integer p(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return Integer.valueOf(telephonyManager.getNetworkType());
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static String q(Context context) {
        if (c(h)) {
            return h;
        }
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            h = str;
            if (str == null || h.length() == 0) {
                return "unknown";
            }
        } catch (Throwable th) {
            j.a(th);
        }
        return h;
    }

    @DexIgnore
    public static int r(Context context) {
        int i2 = i;
        if (i2 != -1) {
            return i2;
        }
        try {
            if (p56.a()) {
                i = 1;
            }
        } catch (Throwable th) {
            j.a(th);
        }
        i = 0;
        return 0;
    }

    @DexIgnore
    public static String s(Context context) {
        String path;
        if (c(k)) {
            return k;
        }
        try {
            if (r56.a(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                String externalStorageState = Environment.getExternalStorageState();
                if (!(externalStorageState == null || !externalStorageState.equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null)) {
                    StatFs statFs = new StatFs(path);
                    String str = String.valueOf((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1000000) + ZendeskConfig.SLASH + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
                    k = str;
                    return str;
                }
                return null;
            }
            j.h("can not get the permission of android.permission.WRITE_EXTERNAL_STORAGE");
            return null;
        } catch (Throwable th) {
            j.a(th);
        }
    }

    @DexIgnore
    public static String t(Context context) {
        try {
            if (l != null) {
                return l;
            }
            int myPid = Process.myPid();
            Iterator<ActivityManager.RunningAppProcessInfo> it = ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    l = next.processName;
                    break;
                }
            }
            return l;
        } catch (Throwable unused) {
        }
    }

    @DexIgnore
    public static String u(Context context) {
        return a(context, a56.a);
    }

    @DexIgnore
    public static synchronized Integer v(Context context) {
        Integer valueOf;
        synchronized (m56.class) {
            if (m <= 0) {
                m = q56.a(context, "MTA_EVENT_INDEX", 0);
                q56.b(context, "MTA_EVENT_INDEX", m + 1000);
            } else if (m % 1000 == 0) {
                try {
                    int i2 = m + 1000;
                    if (m >= 2147383647) {
                        i2 = 0;
                    }
                    q56.b(context, "MTA_EVENT_INDEX", i2);
                } catch (Throwable th) {
                    j.g(th);
                }
            }
            int i3 = m + 1;
            m = i3;
            valueOf = Integer.valueOf(i3);
        }
        return valueOf;
    }

    @DexIgnore
    public static String w(Context context) {
        try {
            return String.valueOf(c(context) / 1000000) + ZendeskConfig.SLASH + String.valueOf(h() / 1000000);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static JSONObject x(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("n", n56.a());
            String d2 = n56.d();
            if (d2 != null && d2.length() > 0) {
                jSONObject.put("na", d2);
            }
            int b2 = n56.b();
            if (b2 > 0) {
                jSONObject.put("fx", b2 / 1000000);
            }
            int c2 = n56.c();
            if (c2 > 0) {
                jSONObject.put("fn", c2 / 1000000);
            }
        } catch (Throwable th) {
            Log.w("MtaSDK", "get cpu error", th);
        }
        return jSONObject;
    }

    @DexIgnore
    public static String y(Context context) {
        List<Sensor> sensorList;
        if (c(p)) {
            return p;
        }
        try {
            SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
            if (!(sensorManager == null || (sensorList = sensorManager.getSensorList(-1)) == null)) {
                StringBuilder sb = new StringBuilder(sensorList.size() * 10);
                for (int i2 = 0; i2 < sensorList.size(); i2++) {
                    sb.append(sensorList.get(i2).getType());
                    if (i2 != sensorList.size() - 1) {
                        sb.append(",");
                    }
                }
                p = sb.toString();
            }
        } catch (Throwable th) {
            j.a(th);
        }
        return p;
    }

    @DexIgnore
    public static synchronized int z(Context context) {
        synchronized (m56.class) {
            if (r != -1) {
                int i2 = r;
                return i2;
            }
            A(context);
            int i3 = r;
            return i3;
        }
    }
}
