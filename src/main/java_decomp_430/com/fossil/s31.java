package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s31 extends ok0 {
    @DexIgnore
    public int j;

    @DexIgnore
    public s31(at0 at0) {
        super(hm0.READ_RSSI, at0);
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.g();
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return p51 instanceof k91;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.h;
    }

    @DexIgnore
    public void c(p51 p51) {
        this.j = ((k91) p51).b;
    }
}
