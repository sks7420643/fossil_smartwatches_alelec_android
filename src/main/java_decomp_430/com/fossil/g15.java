package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface g15 extends xt4<f15> {
    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, ArrayList<Alarm> arrayList, Alarm alarm);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void c(List<Alarm> list);

    @DexIgnore
    void h(boolean z);

    @DexIgnore
    void n(boolean z);

    @DexIgnore
    void r();

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    void s();

    @DexIgnore
    void w();
}
