package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g94 extends f94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q; // = new SparseIntArray();
    @DexIgnore
    public long O;

    /*
    static {
        Q.put(2131362082, 1);
        Q.put(2131363105, 2);
        Q.put(2131363193, 3);
        Q.put(2131362426, 4);
        Q.put(2131362025, 5);
        Q.put(2131362659, 6);
        Q.put(2131362660, 7);
        Q.put(2131362658, 8);
        Q.put(2131362546, 9);
        Q.put(2131362001, 10);
        Q.put(2131363339, 11);
        Q.put(2131363331, 12);
        Q.put(2131363338, 13);
        Q.put(2131363335, 14);
        Q.put(2131362467, 15);
        Q.put(2131362563, 16);
        Q.put(2131362469, 17);
        Q.put(2131362090, 18);
        Q.put(2131363330, 19);
        Q.put(2131363231, 20);
        Q.put(2131363329, 21);
        Q.put(2131363230, 22);
        Q.put(2131363328, 23);
        Q.put(2131363229, 24);
        Q.put(2131363258, 25);
        Q.put(2131362137, 26);
        Q.put(2131362885, 27);
    }
    */

    @DexIgnore
    public g94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 28, P, Q));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.O != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.O = 1;
        }
        g();
    }

    @DexIgnore
    public g94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[10], objArr[5], objArr[1], objArr[18], objArr[26], objArr[4], objArr[15], objArr[17], objArr[9], objArr[16], objArr[8], objArr[6], objArr[7], objArr[0], objArr[27], objArr[2], objArr[3], objArr[24], objArr[22], objArr[20], objArr[25], objArr[23], objArr[21], objArr[19], objArr[12], objArr[14], objArr[13], objArr[11]);
        this.O = -1;
        this.z.setTag((Object) null);
        a(view);
        f();
    }
}
