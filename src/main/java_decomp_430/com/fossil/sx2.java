package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sx2 extends IInterface {
    @DexIgnore
    x52 a(x52 x52, x52 x522, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a() throws RemoteException;

    @DexIgnore
    void a(Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(by2 by2) throws RemoteException;

    @DexIgnore
    void a(x52 x52, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException;

    @DexIgnore
    void b() throws RemoteException;

    @DexIgnore
    void b(Bundle bundle) throws RemoteException;

    @DexIgnore
    void c() throws RemoteException;

    @DexIgnore
    void d() throws RemoteException;

    @DexIgnore
    void e() throws RemoteException;

    @DexIgnore
    void onLowMemory() throws RemoteException;

    @DexIgnore
    void onPause() throws RemoteException;
}
