package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hj3;
import com.google.android.material.card.MaterialCardView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tg3 {
    @DexIgnore
    public static /* final */ int[] t; // = {16842912};
    @DexIgnore
    public static /* final */ double u; // = Math.cos(Math.toRadians(45.0d));
    @DexIgnore
    public /* final */ MaterialCardView a;
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public /* final */ dj3 c;
    @DexIgnore
    public /* final */ dj3 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Drawable h;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public hj3 l;
    @DexIgnore
    public ColorStateList m;
    @DexIgnore
    public Drawable n;
    @DexIgnore
    public LayerDrawable o;
    @DexIgnore
    public dj3 p;
    @DexIgnore
    public dj3 q;
    @DexIgnore
    public boolean r; // = false;
    @DexIgnore
    public boolean s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends InsetDrawable {
        @DexIgnore
        public a(tg3 tg3, Drawable drawable, int i, int i2, int i3, int i4) {
            super(drawable, i, i2, i3, i4);
        }

        @DexIgnore
        public boolean getPadding(Rect rect) {
            return false;
        }
    }

    @DexIgnore
    public tg3(MaterialCardView materialCardView, AttributeSet attributeSet, int i2, int i3) {
        this.a = materialCardView;
        this.c = new dj3(materialCardView.getContext(), attributeSet, i2, i3);
        this.c.a(materialCardView.getContext());
        this.c.b(-12303292);
        hj3.b m2 = this.c.n().m();
        TypedArray obtainStyledAttributes = materialCardView.getContext().obtainStyledAttributes(attributeSet, xf3.CardView, i2, wf3.CardView);
        if (obtainStyledAttributes.hasValue(xf3.CardView_cardCornerRadius)) {
            m2.a(obtainStyledAttributes.getDimension(xf3.CardView_cardCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        }
        this.d = new dj3();
        a(m2.a());
        Resources resources = materialCardView.getResources();
        this.e = resources.getDimensionPixelSize(pf3.mtrl_card_checked_icon_margin);
        this.f = resources.getDimensionPixelSize(pf3.mtrl_card_checked_icon_size);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final boolean A() {
        return this.a.getPreventCornerOverlap() && d() && this.a.getUseCompatPadding();
    }

    @DexIgnore
    public void B() {
        Drawable drawable = this.h;
        this.h = this.a.isClickable() ? n() : this.d;
        Drawable drawable2 = this.h;
        if (drawable != drawable2) {
            c(drawable2);
        }
    }

    @DexIgnore
    public void C() {
        int a2 = (int) ((z() || A() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) - p());
        MaterialCardView materialCardView = this.a;
        Rect rect = this.b;
        materialCardView.b(rect.left + a2, rect.top + a2, rect.right + a2, rect.bottom + a2);
    }

    @DexIgnore
    public void D() {
        this.c.b(this.a.getCardElevation());
    }

    @DexIgnore
    public void E() {
        if (!x()) {
            this.a.setBackgroundInternal(a((Drawable) this.c));
        }
        this.a.setForeground(a(this.h));
    }

    @DexIgnore
    public final void F() {
        Drawable drawable;
        if (!ui3.a || (drawable = this.n) == null) {
            dj3 dj3 = this.p;
            if (dj3 != null) {
                dj3.a(this.j);
                return;
            }
            return;
        }
        ((RippleDrawable) drawable).setColor(this.j);
    }

    @DexIgnore
    public void G() {
        this.d.a((float) this.g, this.m);
    }

    @DexIgnore
    public void a(TypedArray typedArray) {
        this.m = pi3.a(this.a.getContext(), typedArray, xf3.MaterialCardView_strokeColor);
        if (this.m == null) {
            this.m = ColorStateList.valueOf(-1);
        }
        this.g = typedArray.getDimensionPixelSize(xf3.MaterialCardView_strokeWidth, 0);
        this.s = typedArray.getBoolean(xf3.MaterialCardView_android_checkable, false);
        this.a.setLongClickable(this.s);
        this.k = pi3.a(this.a.getContext(), typedArray, xf3.MaterialCardView_checkedIconTint);
        b(pi3.b(this.a.getContext(), typedArray, xf3.MaterialCardView_checkedIcon));
        this.j = pi3.a(this.a.getContext(), typedArray, xf3.MaterialCardView_rippleColor);
        if (this.j == null) {
            this.j = ColorStateList.valueOf(yg3.a((View) this.a, nf3.colorControlHighlight));
        }
        ColorStateList a2 = pi3.a(this.a.getContext(), typedArray, xf3.MaterialCardView_cardForegroundColor);
        dj3 dj3 = this.d;
        if (a2 == null) {
            a2 = ColorStateList.valueOf(0);
        }
        dj3.a(a2);
        F();
        D();
        G();
        this.a.setBackgroundInternal(a((Drawable) this.c));
        this.h = this.a.isClickable() ? n() : this.d;
        this.a.setForeground(a(this.h));
    }

    @DexIgnore
    public void b(float f2) {
        this.c.c(f2);
        dj3 dj3 = this.d;
        if (dj3 != null) {
            dj3.c(f2);
        }
        dj3 dj32 = this.q;
        if (dj32 != null) {
            dj32.c(f2);
        }
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        this.j = colorStateList;
        F();
    }

    @DexIgnore
    public void d(ColorStateList colorStateList) {
        if (this.m != colorStateList) {
            this.m = colorStateList;
            G();
        }
    }

    @DexIgnore
    public final Drawable e() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = this.i;
        if (drawable != null) {
            stateListDrawable.addState(t, drawable);
        }
        return stateListDrawable;
    }

    @DexIgnore
    public final Drawable f() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        this.p = h();
        this.p.a(this.j);
        stateListDrawable.addState(new int[]{16842919}, this.p);
        return stateListDrawable;
    }

    @DexIgnore
    public final Drawable g() {
        if (!ui3.a) {
            return f();
        }
        this.q = h();
        return new RippleDrawable(this.j, (Drawable) null, this.q);
    }

    @DexIgnore
    public final dj3 h() {
        return new dj3(this.l);
    }

    @DexIgnore
    public void i() {
        Drawable drawable = this.n;
        if (drawable != null) {
            Rect bounds = drawable.getBounds();
            int i2 = bounds.bottom;
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i2 - 1);
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i2);
        }
    }

    @DexIgnore
    public dj3 j() {
        return this.c;
    }

    @DexIgnore
    public ColorStateList k() {
        return this.c.h();
    }

    @DexIgnore
    public Drawable l() {
        return this.i;
    }

    @DexIgnore
    public ColorStateList m() {
        return this.k;
    }

    @DexIgnore
    public final Drawable n() {
        if (this.n == null) {
            this.n = g();
        }
        if (this.o == null) {
            this.o = new LayerDrawable(new Drawable[]{this.n, this.d, e()});
            this.o.setId(2, rf3.mtrl_card_checked_layer_id);
        }
        return this.o;
    }

    @DexIgnore
    public float o() {
        return this.c.q();
    }

    @DexIgnore
    public final float p() {
        if (this.a.getPreventCornerOverlap()) {
            return (Build.VERSION.SDK_INT < 21 || this.a.getUseCompatPadding()) ? (float) ((1.0d - u) * ((double) this.a.getCardViewRadius())) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public float q() {
        return this.c.i();
    }

    @DexIgnore
    public ColorStateList r() {
        return this.j;
    }

    @DexIgnore
    public hj3 s() {
        return this.l;
    }

    @DexIgnore
    public int t() {
        ColorStateList colorStateList = this.m;
        if (colorStateList == null) {
            return -1;
        }
        return colorStateList.getDefaultColor();
    }

    @DexIgnore
    public ColorStateList u() {
        return this.m;
    }

    @DexIgnore
    public int v() {
        return this.g;
    }

    @DexIgnore
    public Rect w() {
        return this.b;
    }

    @DexIgnore
    public boolean x() {
        return this.r;
    }

    @DexIgnore
    public boolean y() {
        return this.s;
    }

    @DexIgnore
    public final boolean z() {
        return this.a.getPreventCornerOverlap() && !d();
    }

    @DexIgnore
    public final void c(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 23 || !(this.a.getForeground() instanceof InsetDrawable)) {
            this.a.setForeground(a(drawable));
        } else {
            ((InsetDrawable) this.a.getForeground()).setDrawable(drawable);
        }
    }

    @DexIgnore
    public final boolean d() {
        return Build.VERSION.SDK_INT >= 21 && this.c.z();
    }

    @DexIgnore
    public void b(boolean z) {
        this.s = z;
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        this.k = colorStateList;
        Drawable drawable = this.i;
        if (drawable != null) {
            o7.a(drawable, colorStateList);
        }
    }

    @DexIgnore
    public final float c() {
        return (this.a.getMaxCardElevation() * 1.5f) + (A() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.i = drawable;
        if (drawable != null) {
            this.i = o7.i(drawable.mutate());
            o7.a(this.i, this.k);
        }
        if (this.o != null) {
            this.o.setDrawableByLayerId(rf3.mtrl_card_checked_layer_id, e());
        }
    }

    @DexIgnore
    public final float b() {
        return this.a.getMaxCardElevation() + (A() ? a() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(boolean z) {
        this.r = z;
    }

    @DexIgnore
    public void a(int i2) {
        if (i2 != this.g) {
            this.g = i2;
            G();
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.c.a(colorStateList);
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        this.b.set(i2, i3, i4, i5);
        C();
    }

    @DexIgnore
    public void a(float f2) {
        a(this.l.a(f2));
        this.h.invalidateSelf();
        if (A() || z()) {
            C();
        }
        if (A()) {
            E();
        }
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int i4;
        int i5;
        if (this.o != null) {
            int i6 = this.e;
            int i7 = this.f;
            int i8 = (i2 - i6) - i7;
            int i9 = (i3 - i6) - i7;
            if (x9.o(this.a) == 1) {
                i4 = i8;
                i5 = i6;
            } else {
                i5 = i8;
                i4 = i6;
            }
            this.o.setLayerInset(2, i5, this.e, i4, i9);
        }
    }

    @DexIgnore
    public void a(hj3 hj3) {
        this.l = hj3;
        this.c.setShapeAppearanceModel(hj3);
        dj3 dj3 = this.d;
        if (dj3 != null) {
            dj3.setShapeAppearanceModel(hj3);
        }
        dj3 dj32 = this.q;
        if (dj32 != null) {
            dj32.setShapeAppearanceModel(hj3);
        }
        dj3 dj33 = this.p;
        if (dj33 != null) {
            dj33.setShapeAppearanceModel(hj3);
        }
    }

    @DexIgnore
    public final Drawable a(Drawable drawable) {
        int i2;
        int i3;
        if ((Build.VERSION.SDK_INT < 21) || this.a.getUseCompatPadding()) {
            int ceil = (int) Math.ceil((double) c());
            i3 = (int) Math.ceil((double) b());
            i2 = ceil;
        } else {
            i3 = 0;
            i2 = 0;
        }
        return new a(this, drawable, i3, i2, i3, i2);
    }

    @DexIgnore
    public final float a() {
        return Math.max(Math.max(a(this.l.i(), this.c.q()), a(this.l.k(), this.c.r())), Math.max(a(this.l.d(), this.c.d()), a(this.l.b(), this.c.c())));
    }

    @DexIgnore
    public final float a(aj3 aj3, float f2) {
        if (aj3 instanceof gj3) {
            return (float) ((1.0d - u) * ((double) f2));
        }
        return aj3 instanceof bj3 ? f2 / 2.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }
}
