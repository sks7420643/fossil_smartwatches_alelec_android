package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig0 {
    @DexIgnore
    public /* synthetic */ ig0(qg6 qg6) {
    }

    @DexIgnore
    public final zh0 a(byte b) {
        zh0 zh0;
        zh0[] values = zh0.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                zh0 = null;
                break;
            }
            zh0 = values[i];
            if (zh0.b == b) {
                break;
            }
            i++;
        }
        return zh0 != null ? zh0 : zh0.UNKNOWN;
    }
}
