package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ni2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle e;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ni2(ov2 ov2, Bundle bundle) {
        super(ov2);
        this.f = ov2;
        this.e = bundle;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.f.g.setConditionalUserProperty(this.e, this.a);
    }
}
