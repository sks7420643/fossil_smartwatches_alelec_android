package com.fossil;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so0 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.AUTHENTICATION}));
    @DexIgnore
    public Boolean C;
    @DexIgnore
    public /* final */ byte[] D; // = new byte[8];
    @DexIgnore
    public /* final */ byte[] E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public so0(ue1 ue1, q41 q41, byte[] bArr) {
        super(ue1, q41, eh1.VERIFY_SECRET_KEY, (String) null, 8);
        this.F = bArr;
        byte[] copyOf = Arrays.copyOf(this.F, 16);
        wg6.a(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        this.E = copyOf;
    }

    @DexIgnore
    public Object d() {
        Boolean bool = this.C;
        return Boolean.valueOf(bool != null ? bool.booleanValue() : false);
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        if (this.F.length < 16) {
            a(sk1.INVALID_PARAMETER);
            return;
        }
        new SecureRandom().nextBytes(this.D);
        if1.a((if1) this, (qv0) new tm1(this.w, mi0.A.e(), this.D), (hg6) new hl0(this), (hg6) new an0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.SECRET_KEY_CRC, (Object) Long.valueOf(h51.a.a(this.F, q11.CRC32)));
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(super.k(), bm0.IS_VALID_SECRET_KEY, (Object) this.C);
    }

    @DexIgnore
    public final void a(byte[] bArr) {
        sk1 sk1;
        if (bArr.length != 16) {
            sk1 = sk1.INVALID_DATA_LENGTH;
        } else {
            byte[] a = nq0.f.a(mi0.A.e(), xa1.a.a(u51.J.b(), this.E, u51.J.a(), bArr));
            if (a.length != 16) {
                sk1 = sk1.INVALID_DATA_LENGTH;
            } else {
                if (Arrays.equals(this.D, md6.a(a, 8, 16))) {
                    this.C = true;
                }
                sk1 = sk1.SUCCESS;
            }
        }
        a(km1.a(this.v, (eh1) null, sk1, (bn0) null, 5));
    }
}
