package com.fossil;

import com.portfolio.platform.PortfolioApp;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$setDate$1$1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class gh5$e$a extends sf6 implements ig6<il6, xe6<? super Date>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    public gh5$e$a(xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        gh5$e$a gh5_e_a = new gh5$e$a(xe6);
        gh5_e_a.p$ = (il6) obj;
        return gh5_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((gh5$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return PortfolioApp.get.instance().k();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
