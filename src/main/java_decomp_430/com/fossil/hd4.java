package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ RulerValuePicker B;
    @DexIgnore
    public /* final */ RulerValuePicker C;
    @DexIgnore
    public /* final */ ProgressButton D;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FossilCircleImageView q;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hd4(Object obj, View view, int i, FossilCircleImageView fossilCircleImageView, ConstraintLayout constraintLayout, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, RTLImageView rTLImageView, FossilCircleImageView fossilCircleImageView2, ConstraintLayout constraintLayout2, RulerValuePicker rulerValuePicker, RulerValuePicker rulerValuePicker2, ProgressButton progressButton, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleTextView flexibleTextView6) {
        super(obj, view, i);
        this.q = fossilCircleImageView;
        this.r = flexibleTextInputEditText;
        this.s = flexibleTextInputEditText2;
        this.t = flexibleButton;
        this.u = flexibleButton2;
        this.v = flexibleButton3;
        this.w = flexibleTextView3;
        this.x = flexibleTextView5;
        this.y = flexibleTextInputLayout;
        this.z = rTLImageView;
        this.A = constraintLayout2;
        this.B = rulerValuePicker;
        this.C = rulerValuePicker2;
        this.D = progressButton;
        this.E = flexibleTextInputEditText3;
        this.F = flexibleTextView6;
    }
}
