package com.fossil;

import com.fossil.fx6;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx6 extends fx6.a {
    @DexIgnore
    public static /* final */ fx6.a a; // = new mx6();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements fx6<zq6, Optional<T>> {
        @DexIgnore
        public /* final */ fx6<zq6, T> a;

        @DexIgnore
        public a(fx6<zq6, T> fx6) {
            this.a = fx6;
        }

        @DexIgnore
        public Optional<T> a(zq6 zq6) throws IOException {
            return Optional.ofNullable(this.a.a(zq6));
        }
    }

    @DexIgnore
    public fx6<zq6, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (fx6.a.a(type) != Optional.class) {
            return null;
        }
        return new a(retrofit3.b(fx6.a.a(0, (ParameterizedType) type), annotationArr));
    }
}
