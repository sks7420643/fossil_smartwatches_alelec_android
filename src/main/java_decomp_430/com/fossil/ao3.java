package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao3<E> extends zl3<E> {
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public ao3(E e) {
        jk3.a(e);
        this.element = e;
    }

    @DexIgnore
    public E get(int i) {
        jk3.a(i, 1);
        return this.element;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }

    @DexIgnore
    public jo3<E> iterator() {
        return qm3.a(this.element);
    }

    @DexIgnore
    public zl3<E> subList(int i, int i2) {
        jk3.b(i, i2, 1);
        return i == i2 ? zl3.of() : this;
    }
}
