package com.fossil;

import android.content.Context;
import com.fossil.ji;
import com.fossil.oh;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fh {
    @DexIgnore
    public /* final */ ji.c a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ oh.d d;
    @DexIgnore
    public /* final */ List<oh.b> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ oh.c g;
    @DexIgnore
    public /* final */ Executor h;
    @DexIgnore
    public /* final */ Executor i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Set<Integer> m;

    @DexIgnore
    public fh(Context context, String str, ji.c cVar, oh.d dVar, List<oh.b> list, boolean z, oh.c cVar2, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file) {
        this.a = cVar;
        this.b = context;
        this.c = str;
        this.d = dVar;
        this.e = list;
        this.f = z;
        this.g = cVar2;
        this.h = executor;
        this.i = executor2;
        this.j = z2;
        this.k = z3;
        this.l = z4;
        this.m = set;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        Set<Integer> set;
        if ((!(i2 > i3) || !this.l) && this.k && ((set = this.m) == null || !set.contains(Integer.valueOf(i2)))) {
            return true;
        }
        return false;
    }
}
