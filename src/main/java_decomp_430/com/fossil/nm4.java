package com.fossil;

import android.text.TextUtils;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nm4 {
    @DexIgnore
    public static mm4 a; // = new mm4();

    @DexIgnore
    public static void a(String str, boolean z) {
        a.a(str, z);
    }

    @DexIgnore
    public static String b(String str) {
        return a.a(str);
    }

    @DexIgnore
    public static void a() {
        a.a();
        im4.b().a();
    }

    @DexIgnore
    public static void b() {
        mm4 mm4 = a;
        if (mm4 != null && mm4.b() != null) {
            im4 b = im4.b();
            for (Map.Entry next : a.b().entrySet()) {
                if (b.a((String) next.getKey())) {
                    b.c((String) next.getKey());
                }
                b.a((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str.substring(str.lastIndexOf(File.separator) + 1);
    }
}
