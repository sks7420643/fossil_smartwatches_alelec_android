package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.j5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g5 {
    @DexIgnore
    public static void a(k5 k5Var, z4 z4Var, int i) {
        int i2;
        h5[] h5VarArr;
        int i3;
        if (i == 0) {
            int i4 = k5Var.s0;
            h5VarArr = k5Var.v0;
            i2 = i4;
            i3 = 0;
        } else {
            i3 = 2;
            int i5 = k5Var.t0;
            i2 = i5;
            h5VarArr = k5Var.u0;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            h5 h5Var = h5VarArr[i6];
            h5Var.a();
            if (!k5Var.u(4)) {
                a(k5Var, z4Var, i, i3, h5Var);
            } else if (!o5.a(k5Var, z4Var, i, i3, h5Var)) {
                a(k5Var, z4Var, i, i3, h5Var);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r2.e0 == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
        if (r2.f0 == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004c, code lost:
        r5 = false;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:190:0x0366  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0380  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0386  */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x0454  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x04ae  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x04b1  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x04b7  */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x04ba  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x04ce  */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x0367 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x017e  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0188  */
    public static void a(k5 k5Var, z4 z4Var, int i, int i2, h5 h5Var) {
        boolean z;
        boolean z2;
        d5 d5Var;
        j5 j5Var;
        i5 i5Var;
        i5 i5Var2;
        i5 i5Var3;
        int i3;
        j5 j5Var2;
        int i4;
        d5 d5Var2;
        d5 d5Var3;
        i5 i5Var4;
        j5 j5Var3;
        j5 j5Var4;
        d5 d5Var4;
        d5 d5Var5;
        i5 i5Var5;
        float f;
        int size;
        int i5;
        ArrayList<j5> arrayList;
        int i6;
        float f2;
        boolean z3;
        int i7;
        j5 j5Var5;
        boolean z4;
        int i8;
        k5 k5Var2 = k5Var;
        z4 z4Var2 = z4Var;
        h5 h5Var2 = h5Var;
        j5 j5Var6 = h5Var2.a;
        j5 j5Var7 = h5Var2.c;
        j5 j5Var8 = h5Var2.b;
        j5 j5Var9 = h5Var2.d;
        j5 j5Var10 = h5Var2.e;
        float f3 = h5Var2.k;
        j5 j5Var11 = h5Var2.f;
        j5 j5Var12 = h5Var2.g;
        boolean z5 = k5Var2.C[i] == j5.b.WRAP_CONTENT;
        if (i == 0) {
            z2 = j5Var10.e0 == 0;
            z = j5Var10.e0 == 1;
        } else {
            z2 = j5Var10.f0 == 0;
            z = j5Var10.f0 == 1;
        }
        boolean z6 = true;
        boolean z7 = z2;
        j5 j5Var13 = j5Var6;
        boolean z8 = z;
        boolean z9 = z6;
        boolean z10 = false;
        while (true) {
            j5 j5Var14 = null;
            if (z10) {
                break;
            }
            i5 i5Var6 = j5Var13.A[i2];
            int i9 = (z5 || z9) ? 1 : 4;
            int b = i5Var6.b();
            i5 i5Var7 = i5Var6.d;
            if (!(i5Var7 == null || j5Var13 == j5Var6)) {
                b += i5Var7.b();
            }
            int i10 = b;
            if (z9 && j5Var13 != j5Var6 && j5Var13 != j5Var8) {
                f2 = f3;
                z3 = z10;
                i7 = 6;
            } else if (!z7 || !z5) {
                f2 = f3;
                i7 = i9;
                z3 = z10;
            } else {
                f2 = f3;
                z3 = z10;
                i7 = 4;
            }
            i5 i5Var8 = i5Var6.d;
            if (i5Var8 != null) {
                if (j5Var13 == j5Var8) {
                    z4 = z7;
                    j5Var5 = j5Var10;
                    z4Var2.b(i5Var6.i, i5Var8.i, i10, 5);
                } else {
                    j5Var5 = j5Var10;
                    z4 = z7;
                    z4Var2.b(i5Var6.i, i5Var8.i, i10, 6);
                }
                z4Var2.a(i5Var6.i, i5Var6.d.i, i10, i7);
            } else {
                j5Var5 = j5Var10;
                z4 = z7;
            }
            if (z5) {
                if (j5Var13.s() == 8 || j5Var13.C[i] != j5.b.MATCH_CONSTRAINT) {
                    i8 = 0;
                } else {
                    i5[] i5VarArr = j5Var13.A;
                    i8 = 0;
                    z4Var2.b(i5VarArr[i2 + 1].i, i5VarArr[i2].i, 0, 5);
                }
                z4Var2.b(j5Var13.A[i2].i, k5Var2.A[i2].i, i8, 6);
            }
            i5 i5Var9 = j5Var13.A[i2 + 1].d;
            if (i5Var9 != null) {
                j5 j5Var15 = i5Var9.b;
                i5[] i5VarArr2 = j5Var15.A;
                if (i5VarArr2[i2].d != null && i5VarArr2[i2].d.b == j5Var13) {
                    j5Var14 = j5Var15;
                }
            }
            if (j5Var14 != null) {
                j5Var13 = j5Var14;
                z10 = z3;
            } else {
                z10 = true;
            }
            f3 = f2;
            z7 = z4;
            j5Var10 = j5Var5;
        }
        j5 j5Var16 = j5Var10;
        float f4 = f3;
        boolean z11 = z7;
        if (j5Var9 != null) {
            i5[] i5VarArr3 = j5Var7.A;
            int i11 = i2 + 1;
            if (i5VarArr3[i11].d != null) {
                i5 i5Var10 = j5Var9.A[i11];
                z4Var2.c(i5Var10.i, i5VarArr3[i11].d.i, -i5Var10.b(), 5);
                if (z5) {
                    int i12 = i2 + 1;
                    d5 d5Var6 = k5Var2.A[i12].i;
                    i5[] i5VarArr4 = j5Var7.A;
                    z4Var2.b(d5Var6, i5VarArr4[i12].i, i5VarArr4[i12].b(), 6);
                }
                ArrayList<j5> arrayList2 = h5Var2.h;
                if (arrayList2 != null && (size = arrayList2.size()) > 1) {
                    float f5 = (!h5Var2.n || h5Var2.p) ? f4 : (float) h5Var2.j;
                    float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    j5 j5Var17 = null;
                    i5 = 0;
                    float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    while (i5 < size) {
                        j5 j5Var18 = arrayList2.get(i5);
                        float f8 = j5Var18.g0[i];
                        if (f8 < f6) {
                            if (h5Var2.p) {
                                i5[] i5VarArr5 = j5Var18.A;
                                z4Var2.a(i5VarArr5[i2 + 1].i, i5VarArr5[i2].i, 0, 4);
                                arrayList = arrayList2;
                                i6 = size;
                                i5++;
                                size = i6;
                                arrayList2 = arrayList;
                                f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            } else {
                                f8 = 1.0f;
                                f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            }
                        }
                        if (f8 == f6) {
                            i5[] i5VarArr6 = j5Var18.A;
                            z4Var2.a(i5VarArr6[i2 + 1].i, i5VarArr6[i2].i, 0, 6);
                            arrayList = arrayList2;
                            i6 = size;
                            i5++;
                            size = i6;
                            arrayList2 = arrayList;
                            f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        } else {
                            if (j5Var17 != null) {
                                i5[] i5VarArr7 = j5Var17.A;
                                d5 d5Var7 = i5VarArr7[i2].i;
                                int i13 = i2 + 1;
                                d5 d5Var8 = i5VarArr7[i13].i;
                                i5[] i5VarArr8 = j5Var18.A;
                                arrayList = arrayList2;
                                d5 d5Var9 = i5VarArr8[i2].i;
                                d5 d5Var10 = i5VarArr8[i13].i;
                                i6 = size;
                                w4 c = z4Var.c();
                                c.a(f7, f5, f8, d5Var7, d5Var8, d5Var9, d5Var10);
                                z4Var2.a(c);
                            } else {
                                arrayList = arrayList2;
                                i6 = size;
                            }
                            f7 = f8;
                            j5Var17 = j5Var18;
                            i5++;
                            size = i6;
                            arrayList2 = arrayList;
                            f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        }
                    }
                }
                if (j5Var8 == null && (j5Var8 == j5Var9 || z9)) {
                    i5[] i5VarArr9 = j5Var6.A;
                    i5 i5Var11 = i5VarArr9[i2];
                    int i14 = i2 + 1;
                    i5 i5Var12 = j5Var7.A[i14];
                    d5 d5Var11 = i5VarArr9[i2].d != null ? i5VarArr9[i2].d.i : null;
                    i5[] i5VarArr10 = j5Var7.A;
                    d5 d5Var12 = i5VarArr10[i14].d != null ? i5VarArr10[i14].d.i : null;
                    if (j5Var8 == j5Var9) {
                        i5[] i5VarArr11 = j5Var8.A;
                        i5Var11 = i5VarArr11[i2];
                        i5Var12 = i5VarArr11[i14];
                    }
                    if (!(d5Var11 == null || d5Var12 == null)) {
                        if (i == 0) {
                            f = j5Var16.V;
                        } else {
                            f = j5Var16.W;
                        }
                        z4Var.a(i5Var11.i, d5Var11, i5Var11.b(), f, d5Var12, i5Var12.i, i5Var12.b(), 5);
                    }
                } else if (z11 || j5Var8 == null) {
                    int i15 = 8;
                    if (z8 && j5Var8 != null) {
                        int i16 = h5Var2.j;
                        boolean z12 = i16 > 0 && h5Var2.i == i16;
                        j5Var = j5Var8;
                        j5 j5Var19 = j5Var;
                        while (j5Var != null) {
                            j5 j5Var20 = j5Var.i0[i];
                            while (j5Var20 != null && j5Var20.s() == i15) {
                                j5Var20 = j5Var20.i0[i];
                            }
                            if (j5Var == j5Var8 || j5Var == j5Var9 || j5Var20 == null) {
                                j5Var2 = j5Var19;
                                i4 = 8;
                            } else {
                                j5 j5Var21 = j5Var20 == j5Var9 ? null : j5Var20;
                                i5 i5Var13 = j5Var.A[i2];
                                d5 d5Var13 = i5Var13.i;
                                i5 i5Var14 = i5Var13.d;
                                if (i5Var14 != null) {
                                    d5 d5Var14 = i5Var14.i;
                                }
                                int i17 = i2 + 1;
                                d5 d5Var15 = j5Var19.A[i17].i;
                                int b2 = i5Var13.b();
                                int b3 = j5Var.A[i17].b();
                                if (j5Var21 != null) {
                                    i5Var4 = j5Var21.A[i2];
                                    d5Var3 = i5Var4.i;
                                    i5 i5Var15 = i5Var4.d;
                                    d5Var2 = i5Var15 != null ? i5Var15.i : null;
                                } else {
                                    i5Var4 = j5Var.A[i17].d;
                                    d5Var3 = i5Var4 != null ? i5Var4.i : null;
                                    d5Var2 = j5Var.A[i17].i;
                                }
                                if (i5Var4 != null) {
                                    b3 += i5Var4.b();
                                }
                                int i18 = b3;
                                if (j5Var19 != null) {
                                    b2 += j5Var19.A[i17].b();
                                }
                                int i19 = b2;
                                int i20 = z12 ? 6 : 4;
                                if (d5Var13 == null || d5Var15 == null || d5Var3 == null || d5Var2 == null) {
                                    j5Var3 = j5Var21;
                                    j5Var2 = j5Var19;
                                    i4 = 8;
                                } else {
                                    j5Var3 = j5Var21;
                                    int i21 = i18;
                                    j5Var2 = j5Var19;
                                    i4 = 8;
                                    z4Var.a(d5Var13, d5Var15, i19, 0.5f, d5Var3, d5Var2, i21, i20);
                                }
                                j5Var20 = j5Var3;
                            }
                            if (j5Var.s() == i4) {
                                j5Var = j5Var2;
                            }
                            j5Var19 = j5Var;
                            i15 = 8;
                            j5Var = j5Var20;
                        }
                        i5 i5Var16 = j5Var8.A[i2];
                        i5Var = j5Var6.A[i2].d;
                        int i22 = i2 + 1;
                        i5Var2 = j5Var9.A[i22];
                        i5Var3 = j5Var7.A[i22].d;
                        if (i5Var != null) {
                            i3 = 5;
                        } else if (j5Var8 != j5Var9) {
                            i3 = 5;
                            z4Var2.a(i5Var16.i, i5Var.i, i5Var16.b(), 5);
                        } else {
                            i3 = 5;
                            if (i5Var3 != null) {
                                z4Var.a(i5Var16.i, i5Var.i, i5Var16.b(), 0.5f, i5Var2.i, i5Var3.i, i5Var2.b(), 5);
                            }
                        }
                        if (!(i5Var3 == null || j5Var8 == j5Var9)) {
                            z4Var2.a(i5Var2.i, i5Var3.i, -i5Var2.b(), i3);
                        }
                    }
                } else {
                    int i23 = h5Var2.j;
                    boolean z13 = i23 > 0 && h5Var2.i == i23;
                    j5 j5Var22 = j5Var8;
                    j5 j5Var23 = j5Var22;
                    while (j5Var22 != null) {
                        j5 j5Var24 = j5Var22.i0[i];
                        while (true) {
                            if (j5Var24 != null) {
                                if (j5Var24.s() != 8) {
                                    break;
                                }
                                j5Var24 = j5Var24.i0[i];
                            } else {
                                break;
                            }
                        }
                        if (j5Var24 != null || j5Var22 == j5Var9) {
                            i5 i5Var17 = j5Var22.A[i2];
                            d5 d5Var16 = i5Var17.i;
                            i5 i5Var18 = i5Var17.d;
                            d5 d5Var17 = i5Var18 != null ? i5Var18.i : null;
                            if (j5Var23 != j5Var22) {
                                d5Var17 = j5Var23.A[i2 + 1].i;
                            } else if (j5Var22 == j5Var8 && j5Var23 == j5Var22) {
                                i5[] i5VarArr12 = j5Var6.A;
                                d5Var17 = i5VarArr12[i2].d != null ? i5VarArr12[i2].d.i : null;
                            }
                            int b4 = i5Var17.b();
                            int i24 = i2 + 1;
                            int b5 = j5Var22.A[i24].b();
                            if (j5Var24 != null) {
                                i5Var5 = j5Var24.A[i2];
                                d5Var5 = i5Var5.i;
                                d5Var4 = j5Var22.A[i24].i;
                            } else {
                                i5Var5 = j5Var7.A[i24].d;
                                d5Var5 = i5Var5 != null ? i5Var5.i : null;
                                d5Var4 = j5Var22.A[i24].i;
                            }
                            if (i5Var5 != null) {
                                b5 += i5Var5.b();
                            }
                            if (j5Var23 != null) {
                                b4 += j5Var23.A[i24].b();
                            }
                            if (!(d5Var16 == null || d5Var17 == null || d5Var5 == null || d5Var4 == null)) {
                                if (j5Var22 == j5Var8) {
                                    b4 = j5Var8.A[i2].b();
                                }
                                int i25 = b4;
                                int b6 = j5Var22 == j5Var9 ? j5Var9.A[i24].b() : b5;
                                int i26 = i25;
                                d5 d5Var18 = d5Var5;
                                d5 d5Var19 = d5Var4;
                                int i27 = b6;
                                j5Var4 = j5Var24;
                                z4Var.a(d5Var16, d5Var17, i26, 0.5f, d5Var18, d5Var19, i27, z13 ? 6 : 4);
                                if (j5Var22.s() == 8) {
                                    j5Var23 = j5Var22;
                                }
                                j5Var22 = j5Var4;
                            }
                        }
                        j5Var4 = j5Var24;
                        if (j5Var22.s() == 8) {
                        }
                        j5Var22 = j5Var4;
                    }
                }
                if ((!z11 || z8) && j5Var8 != null) {
                    i5 i5Var19 = j5Var8.A[i2];
                    int i28 = i2 + 1;
                    i5 i5Var20 = j5Var9.A[i28];
                    i5 i5Var21 = i5Var19.d;
                    d5Var = i5Var21 == null ? i5Var21.i : null;
                    i5 i5Var22 = i5Var20.d;
                    d5 d5Var20 = i5Var22 == null ? i5Var22.i : null;
                    if (j5Var7 != j5Var9) {
                        i5 i5Var23 = j5Var7.A[i28].d;
                        d5Var20 = i5Var23 != null ? i5Var23.i : null;
                    }
                    d5 d5Var21 = d5Var20;
                    if (j5Var8 == j5Var9) {
                        i5[] i5VarArr13 = j5Var8.A;
                        i5 i5Var24 = i5VarArr13[i2];
                        i5Var20 = i5VarArr13[i28];
                        i5Var19 = i5Var24;
                    }
                    if (d5Var != null && d5Var21 != null) {
                        int b7 = i5Var19.b();
                        if (j5Var9 != null) {
                            j5Var7 = j5Var9;
                        }
                        z4Var.a(i5Var19.i, d5Var, b7, 0.5f, d5Var21, i5Var20.i, j5Var7.A[i28].b(), 5);
                        return;
                    }
                }
                return;
            }
        }
        if (z5) {
        }
        ArrayList<j5> arrayList22 = h5Var2.h;
        if (!h5Var2.n || h5Var2.p) {
        }
        float f62 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        j5 j5Var172 = null;
        i5 = 0;
        float f72 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (i5 < size) {
        }
        if (j5Var8 == null) {
        }
        if (z11) {
        }
        int i152 = 8;
        int i162 = h5Var2.j;
        if (i162 > 0 || h5Var2.i == i162) {
        }
        j5Var = j5Var8;
        j5 j5Var192 = j5Var;
        while (j5Var != null) {
        }
        i5 i5Var162 = j5Var8.A[i2];
        i5Var = j5Var6.A[i2].d;
        int i222 = i2 + 1;
        i5Var2 = j5Var9.A[i222];
        i5Var3 = j5Var7.A[i222].d;
        if (i5Var != null) {
        }
        z4Var2.a(i5Var2.i, i5Var3.i, -i5Var2.b(), i3);
        if (!z11) {
        }
        i5 i5Var192 = j5Var8.A[i2];
        int i282 = i2 + 1;
        i5 i5Var202 = j5Var9.A[i282];
        i5 i5Var212 = i5Var192.d;
        if (i5Var212 == null) {
        }
        i5 i5Var222 = i5Var202.d;
        if (i5Var222 == null) {
        }
        if (j5Var7 != j5Var9) {
        }
        d5 d5Var212 = d5Var20;
        if (j5Var8 == j5Var9) {
        }
        if (d5Var != null) {
        }
    }
}
