package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import com.fossil.n16;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a16 extends x06 {
    @DexIgnore
    public a16(Context context) {
        super(context);
    }

    @DexIgnore
    public boolean a(l16 l16) {
        return "file".equals(l16.d.getScheme());
    }

    @DexIgnore
    public n16.a a(l16 l16, int i) throws IOException {
        return new n16.a((Bitmap) null, c(l16), Picasso.LoadedFrom.DISK, a(l16.d));
    }

    @DexIgnore
    public static int a(Uri uri) throws IOException {
        int attributeInt = new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1);
        if (attributeInt == 3) {
            return 180;
        }
        if (attributeInt != 6) {
            return attributeInt != 8 ? 0 : 270;
        }
        return 90;
    }
}
