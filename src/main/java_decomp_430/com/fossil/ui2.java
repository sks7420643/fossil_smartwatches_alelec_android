package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ cs2 f;
    @DexIgnore
    public /* final */ /* synthetic */ ov2.c g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ui2(ov2.c cVar, Activity activity, cs2 cs2) {
        super(ov2.this);
        this.g = cVar;
        this.e = activity;
        this.f = cs2;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        ov2.this.g.onActivitySaveInstanceState(z52.a(this.e), this.f, this.b);
    }
}
