package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo2 extends ao2 {
    @DexIgnore
    public bo2() {
        super();
    }

    @DexIgnore
    public static <E> nn2<E> b(Object obj, long j) {
        return (nn2) cq2.f(obj, j);
    }

    @DexIgnore
    public final void a(Object obj, long j) {
        b(obj, j).k();
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        nn2 b = b(obj, j);
        nn2 b2 = b(obj2, j);
        int size = b.size();
        int size2 = b2.size();
        if (size > 0 && size2 > 0) {
            if (!b.zza()) {
                b = b.zza(size2 + size);
            }
            b.addAll(b2);
        }
        if (size > 0) {
            b2 = b;
        }
        cq2.a(obj, j, (Object) b2);
    }
}
