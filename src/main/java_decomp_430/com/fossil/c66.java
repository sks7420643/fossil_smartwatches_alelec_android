package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c66 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ r36 b;

    @DexIgnore
    public c66(Context context, r36 r36) {
        this.a = context;
        this.b = r36;
    }

    @DexIgnore
    public final void run() {
        Context context = this.a;
        if (context == null) {
            q36.m.d("The Context of StatService.onPause() can not be null!");
        } else {
            q36.b(context, m56.k(context), this.b);
        }
    }
}
