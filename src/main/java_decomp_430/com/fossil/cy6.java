package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cy6 implements fx6<zq6, Double> {
    @DexIgnore
    public static /* final */ cy6 a; // = new cy6();

    @DexIgnore
    public Double a(zq6 zq6) throws IOException {
        return Double.valueOf(zq6.string());
    }
}
