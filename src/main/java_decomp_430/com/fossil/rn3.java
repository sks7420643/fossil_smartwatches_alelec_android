package com.fossil;

import com.fossil.dn3;
import com.fossil.en3;
import com.fossil.im3;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rn3<E> extends hm3<E> {
    @DexIgnore
    public static /* final */ rn3<Object> EMPTY; // = new rn3<>(zl3.of());
    @DexIgnore
    public /* final */ transient en3.e<E>[] c;
    @DexIgnore
    public /* final */ transient en3.e<E>[] d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    public transient im3<E> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends im3.b<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return rn3.this.contains(obj);
        }

        @DexIgnore
        public E get(int i) {
            return rn3.this.c[i].getElement();
        }

        @DexIgnore
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return rn3.this.c.length;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<E> extends en3.e<E> {
        @DexIgnore
        public /* final */ en3.e<E> nextInBucket;

        @DexIgnore
        public c(E e, int i, en3.e<E> eVar) {
            super(e, i);
            this.nextInBucket = eVar;
        }

        @DexIgnore
        public en3.e<E> nextInBucket() {
            return this.nextInBucket;
        }
    }

    @DexIgnore
    public rn3(Collection<? extends dn3.a<? extends E>> collection) {
        en3.e<E> eVar;
        int size = collection.size();
        en3.e<E>[] eVarArr = new en3.e[size];
        if (size == 0) {
            this.c = eVarArr;
            this.d = null;
            this.e = 0;
            this.f = 0;
            this.g = im3.of();
            return;
        }
        int a2 = sl3.a(size, 1.0d);
        int i = a2 - 1;
        en3.e<E>[] eVarArr2 = new en3.e[a2];
        long j = 0;
        int i2 = 0;
        int i3 = 0;
        for (dn3.a aVar : collection) {
            Object element = aVar.getElement();
            jk3.a(element);
            int count = aVar.getCount();
            int hashCode = element.hashCode();
            int a3 = sl3.a(hashCode) & i;
            en3.e<E> eVar2 = eVarArr2[a3];
            if (eVar2 == null) {
                eVar = (aVar instanceof en3.e) && !(aVar instanceof c) ? (en3.e) aVar : new en3.e<>(element, count);
            } else {
                eVar = new c<>(element, count, eVar2);
            }
            i2 += hashCode ^ count;
            eVarArr[i3] = eVar;
            eVarArr2[a3] = eVar;
            j += (long) count;
            i3++;
        }
        this.c = eVarArr;
        this.d = eVarArr2;
        this.e = xo3.a(j);
        this.f = i2;
    }

    @DexIgnore
    public int count(Object obj) {
        en3.e<E>[] eVarArr = this.d;
        if (!(obj == null || eVarArr == null)) {
            for (en3.e<E> eVar = eVarArr[sl3.a(obj) & (eVarArr.length - 1)]; eVar != null; eVar = eVar.nextInBucket()) {
                if (gk3.a(obj, eVar.getElement())) {
                    return eVar.getCount();
                }
            }
        }
        return 0;
    }

    @DexIgnore
    public dn3.a<E> getEntry(int i) {
        return this.c[i];
    }

    @DexIgnore
    public int hashCode() {
        return this.f;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.e;
    }

    @DexIgnore
    public im3<E> elementSet() {
        im3<E> im3 = this.g;
        if (im3 != null) {
            return im3;
        }
        b bVar = new b();
        this.g = bVar;
        return bVar;
    }
}
