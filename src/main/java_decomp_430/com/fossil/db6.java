package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class db6 implements cb6 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Context c;

    @DexIgnore
    public db6(Context context, String str) {
        if (context != null) {
            this.c = context;
            this.b = str;
            this.a = this.c.getSharedPreferences(this.b, 0);
            return;
        }
        throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
    }

    @DexIgnore
    @TargetApi(9)
    public boolean a(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }

    @DexIgnore
    public SharedPreferences.Editor edit() {
        return this.a.edit();
    }

    @DexIgnore
    public SharedPreferences get() {
        return this.a;
    }

    @DexIgnore
    @Deprecated
    public db6(i86 i86) {
        this(i86.d(), i86.getClass().getName());
    }
}
