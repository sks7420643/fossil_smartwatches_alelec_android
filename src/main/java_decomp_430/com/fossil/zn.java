package com.fossil;

import com.fossil.am;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zn {
    @DexIgnore
    public String a;
    @DexIgnore
    public am.a b; // = am.a.ENQUEUED;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public pl e;
    @DexIgnore
    public pl f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public nl j;
    @DexIgnore
    public int k;
    @DexIgnore
    public ll l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public long o;
    @DexIgnore
    public long p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements v3<List<c>, List<am>> {
        @DexIgnore
        /* renamed from: a */
        public List<am> apply(List<c> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (c a : list) {
                arrayList.add(a.a());
            }
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public am.a b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b != bVar.b) {
                return false;
            }
            return this.a.equals(bVar.a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public String a;
        @DexIgnore
        public am.a b;
        @DexIgnore
        public pl c;
        @DexIgnore
        public int d;
        @DexIgnore
        public List<String> e;
        @DexIgnore
        public List<pl> f;

        @DexIgnore
        public am a() {
            List<pl> list = this.f;
            return new am(UUID.fromString(this.a), this.b, this.c, this.e, (list == null || list.isEmpty()) ? pl.c : this.f.get(0), this.d);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.d != cVar.d) {
                return false;
            }
            String str = this.a;
            if (str == null ? cVar.a != null : !str.equals(cVar.a)) {
                return false;
            }
            if (this.b != cVar.b) {
                return false;
            }
            pl plVar = this.c;
            if (plVar == null ? cVar.c != null : !plVar.equals(cVar.c)) {
                return false;
            }
            List<String> list = this.e;
            if (list == null ? cVar.e != null : !list.equals(cVar.e)) {
                return false;
            }
            List<pl> list2 = this.f;
            List<pl> list3 = cVar.f;
            if (list2 != null) {
                return list2.equals(list3);
            }
            if (list3 == null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            am.a aVar = this.b;
            int hashCode2 = (hashCode + (aVar != null ? aVar.hashCode() : 0)) * 31;
            pl plVar = this.c;
            int hashCode3 = (((hashCode2 + (plVar != null ? plVar.hashCode() : 0)) * 31) + this.d) * 31;
            List<String> list = this.e;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            List<pl> list2 = this.f;
            if (list2 != null) {
                i = list2.hashCode();
            }
            return hashCode4 + i;
        }
    }

    /*
    static {
        tl.a("WorkSpec");
        new a();
    }
    */

    @DexIgnore
    public zn(String str, String str2) {
        pl plVar = pl.c;
        this.e = plVar;
        this.f = plVar;
        this.j = nl.i;
        this.l = ll.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = str;
        this.c = str2;
    }

    @DexIgnore
    public long a() {
        long j2;
        boolean z = false;
        if (c()) {
            if (this.l == ll.LINEAR) {
                z = true;
            }
            if (z) {
                j2 = this.m * ((long) this.k);
            } else {
                j2 = (long) Math.scalb((float) this.m, this.k - 1);
            }
            return this.n + Math.min(18000000, j2);
        }
        long j3 = 0;
        if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j4 = this.n;
            if (j4 == 0) {
                j4 = this.g + currentTimeMillis;
            }
            if (this.i != this.h) {
                z = true;
            }
            if (z) {
                if (this.n == 0) {
                    j3 = this.i * -1;
                }
                return j4 + this.h + j3;
            }
            if (this.n != 0) {
                j3 = this.h;
            }
            return j4 + j3;
        }
        long j5 = this.n;
        if (j5 == 0) {
            j5 = System.currentTimeMillis();
        }
        return j5 + this.g;
    }

    @DexIgnore
    public boolean b() {
        return !nl.i.equals(this.j);
    }

    @DexIgnore
    public boolean c() {
        return this.b == am.a.ENQUEUED && this.k > 0;
    }

    @DexIgnore
    public boolean d() {
        return this.h != 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || zn.class != obj.getClass()) {
            return false;
        }
        zn znVar = (zn) obj;
        if (this.g != znVar.g || this.h != znVar.h || this.i != znVar.i || this.k != znVar.k || this.m != znVar.m || this.n != znVar.n || this.o != znVar.o || this.p != znVar.p || !this.a.equals(znVar.a) || this.b != znVar.b || !this.c.equals(znVar.c)) {
            return false;
        }
        String str = this.d;
        if (str == null ? znVar.d != null : !str.equals(znVar.d)) {
            return false;
        }
        if (this.e.equals(znVar.e) && this.f.equals(znVar.f) && this.j.equals(znVar.j) && this.l == znVar.l) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31;
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j2 = this.g;
        long j3 = this.h;
        long j4 = this.i;
        long j5 = this.m;
        long j6 = this.n;
        long j7 = this.o;
        long j8 = this.p;
        return ((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.j.hashCode()) * 31) + this.k) * 31) + this.l.hashCode()) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "{WorkSpec: " + this.a + "}";
    }

    @DexIgnore
    public zn(zn znVar) {
        pl plVar = pl.c;
        this.e = plVar;
        this.f = plVar;
        this.j = nl.i;
        this.l = ll.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = znVar.a;
        this.c = znVar.c;
        this.b = znVar.b;
        this.d = znVar.d;
        this.e = new pl(znVar.e);
        this.f = new pl(znVar.f);
        this.g = znVar.g;
        this.h = znVar.h;
        this.i = znVar.i;
        this.j = new nl(znVar.j);
        this.k = znVar.k;
        this.l = znVar.l;
        this.m = znVar.m;
        this.n = znVar.n;
        this.o = znVar.o;
        this.p = znVar.p;
    }
}
