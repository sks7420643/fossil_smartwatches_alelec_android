package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km3<E> extends nn3<E> implements co3<E> {
    @DexIgnore
    public km3(nm3<E> nm3, zl3<E> zl3) {
        super(nm3, zl3);
    }

    @DexIgnore
    public Comparator<? super E> comparator() {
        return delegateCollection().comparator();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public int indexOf(Object obj) {
        int indexOf = delegateCollection().indexOf(obj);
        if (indexOf < 0 || !get(indexOf).equals(obj)) {
            return -1;
        }
        return indexOf;
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        return indexOf(obj);
    }

    @DexIgnore
    public zl3<E> subListUnchecked(int i, int i2) {
        return new tn3(super.subListUnchecked(i, i2), comparator()).asList();
    }

    @DexIgnore
    public nm3<E> delegateCollection() {
        return (nm3) super.delegateCollection();
    }
}
