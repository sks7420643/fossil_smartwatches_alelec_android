package com.fossil;

import android.widget.ListView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ra extends ma {
    @DexIgnore
    public /* final */ ListView w;

    @DexIgnore
    public ra(ListView listView) {
        super(listView);
        this.w = listView;
    }

    @DexIgnore
    public void a(int i, int i2) {
        sa.b(this.w, i2);
    }

    @DexIgnore
    public boolean a(int i) {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038 A[RETURN] */
    public boolean b(int i) {
        ListView listView = this.w;
        int count = listView.getCount();
        if (count == 0) {
            return false;
        }
        int childCount = listView.getChildCount();
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int i2 = firstVisiblePosition + childCount;
        if (i > 0) {
            if (i2 < count || listView.getChildAt(childCount - 1).getBottom() > listView.getHeight()) {
                return true;
            }
            return false;
        } else if (i >= 0) {
            return false;
        } else {
            if (firstVisiblePosition <= 0 && listView.getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }
}
