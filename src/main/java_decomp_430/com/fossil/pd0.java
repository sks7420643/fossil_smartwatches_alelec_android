package com.fossil;

import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pd0 extends p40 implements Parcelable {
    @DexIgnore
    public u81 a;

    @DexIgnore
    public final void a(u81 u81) {
        this.a = u81;
    }

    @DexIgnore
    public final byte[] b() {
        u81 u81 = this.a;
        if (u81 != null) {
            int length = u81.a.length + 3 + d().length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            wg6.a(allocate, "ByteBuffer.allocate(size)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            u81 u812 = this.a;
            if (u812 != null) {
                allocate.put(u812.a);
                allocate.putShort((short) length);
                allocate.put((byte) c());
                allocate.put(d());
                byte[] array = allocate.array();
                wg6.a(array, "byteBuffer.array()");
                return array;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract byte[] d();
}
