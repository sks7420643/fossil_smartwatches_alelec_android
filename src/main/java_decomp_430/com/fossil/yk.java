package com.fossil;

import android.os.Parcelable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yk {
    @DexIgnore
    public /* final */ p4<String, Method> a;
    @DexIgnore
    public /* final */ p4<String, Method> b;
    @DexIgnore
    public /* final */ p4<String, Class> c;

    @DexIgnore
    public yk(p4<String, Method> p4Var, p4<String, Method> p4Var2, p4<String, Class> p4Var3) {
        this.a = p4Var;
        this.b = p4Var2;
        this.c = p4Var3;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(Parcelable parcelable);

    @DexIgnore
    public abstract void a(CharSequence charSequence);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public void a(boolean z, boolean z2) {
    }

    @DexIgnore
    public abstract void a(byte[] bArr);

    @DexIgnore
    public abstract boolean a(int i);

    @DexIgnore
    public boolean a(boolean z, int i) {
        if (!a(i)) {
            return z;
        }
        return d();
    }

    @DexIgnore
    public abstract yk b();

    @DexIgnore
    public abstract void b(int i);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public void b(boolean z, int i) {
        b(i);
        a(z);
    }

    @DexIgnore
    public abstract void c(int i);

    @DexIgnore
    public boolean c() {
        return false;
    }

    @DexIgnore
    public abstract boolean d();

    @DexIgnore
    public abstract byte[] e();

    @DexIgnore
    public abstract CharSequence f();

    @DexIgnore
    public abstract int g();

    @DexIgnore
    public abstract <T extends Parcelable> T h();

    @DexIgnore
    public abstract String i();

    @DexIgnore
    public <T extends al> T j() {
        String i = i();
        if (i == null) {
            return null;
        }
        return a(i, b());
    }

    @DexIgnore
    public int a(int i, int i2) {
        if (!a(i2)) {
            return i;
        }
        return g();
    }

    @DexIgnore
    public void b(byte[] bArr, int i) {
        b(i);
        a(bArr);
    }

    @DexIgnore
    public String a(String str, int i) {
        if (!a(i)) {
            return str;
        }
        return i();
    }

    @DexIgnore
    public void b(CharSequence charSequence, int i) {
        b(i);
        a(charSequence);
    }

    @DexIgnore
    public byte[] a(byte[] bArr, int i) {
        if (!a(i)) {
            return bArr;
        }
        return e();
    }

    @DexIgnore
    public void b(int i, int i2) {
        b(i2);
        c(i);
    }

    @DexIgnore
    public <T extends Parcelable> T a(T t, int i) {
        if (!a(i)) {
            return t;
        }
        return h();
    }

    @DexIgnore
    public void b(String str, int i) {
        b(i);
        b(str);
    }

    @DexIgnore
    public CharSequence a(CharSequence charSequence, int i) {
        if (!a(i)) {
            return charSequence;
        }
        return f();
    }

    @DexIgnore
    public void b(Parcelable parcelable, int i) {
        b(i);
        a(parcelable);
    }

    @DexIgnore
    public void a(al alVar) {
        if (alVar == null) {
            b((String) null);
            return;
        }
        b(alVar);
        yk b2 = b();
        a(alVar, b2);
        b2.a();
    }

    @DexIgnore
    public void b(al alVar, int i) {
        b(i);
        a(alVar);
    }

    @DexIgnore
    public final void b(al alVar) {
        try {
            b(a((Class<? extends al>) alVar.getClass()).getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(alVar.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    @DexIgnore
    public <T extends al> T a(T t, int i) {
        if (!a(i)) {
            return t;
        }
        return j();
    }

    @DexIgnore
    public final Method b(Class cls) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Method method = this.b.get(cls.getName());
        if (method != null) {
            return method;
        }
        Class a2 = a((Class<? extends al>) cls);
        System.currentTimeMillis();
        Method declaredMethod = a2.getDeclaredMethod("write", new Class[]{cls, yk.class});
        this.b.put(cls.getName(), declaredMethod);
        return declaredMethod;
    }

    @DexIgnore
    public <T extends al> T a(String str, yk ykVar) {
        try {
            return (al) a(str).invoke((Object) null, new Object[]{ykVar});
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public <T extends al> void a(T t, yk ykVar) {
        try {
            b((Class) t.getClass()).invoke((Object) null, new Object[]{t, ykVar});
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public final Method a(String str) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Class<yk> cls = yk.class;
        Method method = this.a.get(str);
        if (method != null) {
            return method;
        }
        System.currentTimeMillis();
        Method declaredMethod = Class.forName(str, true, cls.getClassLoader()).getDeclaredMethod("read", new Class[]{cls});
        this.a.put(str, declaredMethod);
        return declaredMethod;
    }

    @DexIgnore
    public final Class a(Class<? extends al> cls) throws ClassNotFoundException {
        Class cls2 = this.c.get(cls.getName());
        if (cls2 != null) {
            return cls2;
        }
        Class<?> cls3 = Class.forName(String.format("%s.%sParcelizer", new Object[]{cls.getPackage().getName(), cls.getSimpleName()}), false, cls.getClassLoader());
        this.c.put(cls.getName(), cls3);
        return cls3;
    }
}
