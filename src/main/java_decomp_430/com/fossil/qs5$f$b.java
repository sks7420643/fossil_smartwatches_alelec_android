package com.fossil;

import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
public final class qs5$f$b extends sf6 implements ig6<il6, xe6<? super List<? extends SKUModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qs5$f$b(PairingPresenter.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        qs5$f$b qs5_f_b = new qs5$f$b(this.this$0, xe6);
        qs5_f_b.p$ = (il6) obj;
        return qs5_f_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((qs5$f$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.u.getSupportedSku();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
