package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m45 implements Factory<l45> {
    @DexIgnore
    public static HomeDianaCustomizePresenter a(k45 k45, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, RingStyleRepository ringStyleRepository, DianaPresetRepository dianaPresetRepository, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, om4 om4, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, an4 an4, PortfolioApp portfolioApp) {
        return new HomeDianaCustomizePresenter(k45, watchAppRepository, complicationRepository, ringStyleRepository, dianaPresetRepository, setDianaPresetToWatchUseCase, om4, customizeRealDataRepository, userRepository, watchFaceRepository, an4, portfolioApp);
    }
}
