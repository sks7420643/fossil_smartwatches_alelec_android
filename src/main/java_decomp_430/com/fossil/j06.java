package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j06 {
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<l06>> a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, m06> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ q06 d;
    @DexIgnore
    public /* final */ n06 e;
    @DexIgnore
    public /* final */ ThreadLocal<ConcurrentLinkedQueue<c>> f;
    @DexIgnore
    public /* final */ ThreadLocal<Boolean> g;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<Class<?>>> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ThreadLocal<ConcurrentLinkedQueue<c>> {
        @DexIgnore
        public a(j06 j06) {
        }

        @DexIgnore
        public ConcurrentLinkedQueue<c> initialValue() {
            return new ConcurrentLinkedQueue<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ThreadLocal<Boolean> {
        @DexIgnore
        public b(j06 j06) {
        }

        @DexIgnore
        public Boolean initialValue() {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ l06 b;

        @DexIgnore
        public c(Object obj, l06 l06) {
            this.a = obj;
            this.b = l06;
        }
    }

    @DexIgnore
    public j06(q06 q06) {
        this(q06, "default");
    }

    @DexIgnore
    public final void a(l06 l06, m06 m06) {
        try {
            Object c2 = m06.c();
            if (c2 != null) {
                a(c2, l06);
            }
        } catch (InvocationTargetException e2) {
            a("Producer " + m06 + " threw an exception.", e2);
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b1, code lost:
        r2 = new java.util.concurrent.CopyOnWriteArraySet();
     */
    @DexIgnore
    public void b(Object obj) {
        Set putIfAbsent;
        if (obj != null) {
            this.d.a(this);
            Map<Class<?>, m06> b2 = this.e.b(obj);
            for (Class next : b2.keySet()) {
                m06 m06 = b2.get(next);
                m06 putIfAbsent2 = this.b.putIfAbsent(next, m06);
                if (putIfAbsent2 == null) {
                    Set<l06> set = (Set) this.a.get(next);
                    if (set != null && !set.isEmpty()) {
                        for (l06 a2 : set) {
                            a(a2, m06);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Producer method for type " + next + " found on type " + m06.a.getClass() + ", but already registered by type " + putIfAbsent2.a.getClass() + ".");
                }
            }
            Map<Class<?>, Set<l06>> a3 = this.e.a(obj);
            for (Class next2 : a3.keySet()) {
                Set set2 = (Set) this.a.get(next2);
                if (set2 == null && (putIfAbsent = this.a.putIfAbsent(next2, set2)) != null) {
                    set2 = putIfAbsent;
                }
                if (!set2.addAll(a3.get(next2))) {
                    throw new IllegalArgumentException("Object already registered.");
                }
            }
            for (Map.Entry next3 : a3.entrySet()) {
                m06 m062 = (m06) this.b.get((Class) next3.getKey());
                if (m062 != null && m062.b()) {
                    for (l06 l06 : (Set) next3.getValue()) {
                        if (!m062.b()) {
                            break;
                        } else if (l06.b()) {
                            a(l06, m062);
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("Object to register must not be null.");
    }

    @DexIgnore
    public void c(Object obj) {
        if (obj != null) {
            this.d.a(this);
            for (Map.Entry next : this.e.b(obj).entrySet()) {
                Class cls = (Class) next.getKey();
                m06 d2 = d(cls);
                m06 m06 = (m06) next.getValue();
                if (m06 == null || !m06.equals(d2)) {
                    throw new IllegalArgumentException("Missing event producer for an annotated method. Is " + obj.getClass() + " registered?");
                }
                ((m06) this.b.remove(cls)).a();
            }
            for (Map.Entry next2 : this.e.a(obj).entrySet()) {
                Set<l06> c2 = c((Class<?>) (Class) next2.getKey());
                Collection collection = (Collection) next2.getValue();
                if (c2 == null || !c2.containsAll(collection)) {
                    throw new IllegalArgumentException("Missing event handler for an annotated method. Is " + obj.getClass() + " registered?");
                }
                for (l06 next3 : c2) {
                    if (collection.contains(next3)) {
                        next3.a();
                    }
                }
                c2.removeAll(collection);
            }
            return;
        }
        throw new NullPointerException("Object to unregister must not be null.");
    }

    @DexIgnore
    public m06 d(Class<?> cls) {
        return (m06) this.b.get(cls);
    }

    @DexIgnore
    public String toString() {
        return "[Bus \"" + this.c + "\"]";
    }

    @DexIgnore
    public j06(q06 q06, String str) {
        this(q06, str, n06.a);
    }

    @DexIgnore
    public j06(q06 q06, String str, n06 n06) {
        this.a = new ConcurrentHashMap();
        this.b = new ConcurrentHashMap();
        this.f = new a(this);
        this.g = new b(this);
        this.h = new ConcurrentHashMap();
        this.d = q06;
        this.c = str;
        this.e = n06;
    }

    @DexIgnore
    public void a(Object obj) {
        if (obj != null) {
            this.d.a(this);
            boolean z = false;
            for (Class<?> c2 : a(obj.getClass())) {
                Set<l06> c3 = c(c2);
                if (c3 != null && !c3.isEmpty()) {
                    z = true;
                    for (l06 b2 : c3) {
                        b(obj, b2);
                    }
                }
            }
            if (!z && !(obj instanceof k06)) {
                a((Object) new k06(this, obj));
            }
            a();
            return;
        }
        throw new NullPointerException("Event to post must not be null.");
    }

    @DexIgnore
    public void a() {
        if (!this.g.get().booleanValue()) {
            this.g.set(true);
            while (true) {
                try {
                    c cVar = (c) this.f.get().poll();
                    if (cVar != null) {
                        if (cVar.b.b()) {
                            a(cVar.a, cVar.b);
                        }
                    } else {
                        return;
                    }
                } finally {
                    this.g.set(false);
                }
            }
        }
    }

    @DexIgnore
    public void a(Object obj, l06 l06) {
        try {
            l06.a(obj);
        } catch (InvocationTargetException e2) {
            a("Could not dispatch event: " + obj.getClass() + " to handler " + l06, e2);
            throw null;
        }
    }

    @DexIgnore
    public Set<l06> c(Class<?> cls) {
        return (Set) this.a.get(cls);
    }

    @DexIgnore
    public Set<Class<?>> a(Class<?> cls) {
        Set<Class<?>> set = (Set) this.h.get(cls);
        if (set != null) {
            return set;
        }
        Set<Class<?>> b2 = b(cls);
        Set<Class<?>> putIfAbsent = this.h.putIfAbsent(cls, b2);
        return putIfAbsent == null ? b2 : putIfAbsent;
    }

    @DexIgnore
    public static void a(String str, InvocationTargetException invocationTargetException) {
        Throwable cause = invocationTargetException.getCause();
        if (cause != null) {
            throw new RuntimeException(str + ": " + cause.getMessage(), cause);
        }
        throw new RuntimeException(str + ": " + invocationTargetException.getMessage(), invocationTargetException);
    }

    @DexIgnore
    public void b(Object obj, l06 l06) {
        this.f.get().offer(new c(obj, l06));
    }

    @DexIgnore
    public final Set<Class<?>> b(Class<?> cls) {
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        linkedList.add(cls);
        while (!linkedList.isEmpty()) {
            Class cls2 = (Class) linkedList.remove(0);
            hashSet.add(cls2);
            Class superclass = cls2.getSuperclass();
            if (superclass != null) {
                linkedList.add(superclass);
            }
        }
        return hashSet;
    }
}
