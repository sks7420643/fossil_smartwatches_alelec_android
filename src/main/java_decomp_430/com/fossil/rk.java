package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import com.fossil.wearables.fsl.goaltracking.GoalTracking;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rk extends wk implements qk {
    @DexIgnore
    public b b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public ArgbEvaluator d;
    @DexIgnore
    public /* final */ Drawable.Callback e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Drawable.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
            rk.this.invalidateSelf();
        }

        @DexIgnore
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            rk.this.scheduleSelf(runnable, j);
        }

        @DexIgnore
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            rk.this.unscheduleSelf(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Drawable.ConstantState {
        @DexIgnore
        public int a;
        @DexIgnore
        public xk b;
        @DexIgnore
        public AnimatorSet c;
        @DexIgnore
        public ArrayList<Animator> d;
        @DexIgnore
        public p4<Animator, String> e;

        @DexIgnore
        public b(Context context, b bVar, Drawable.Callback callback, Resources resources) {
            if (bVar != null) {
                this.a = bVar.a;
                xk xkVar = bVar.b;
                if (xkVar != null) {
                    Drawable.ConstantState constantState = xkVar.getConstantState();
                    if (resources != null) {
                        this.b = (xk) constantState.newDrawable(resources);
                    } else {
                        this.b = (xk) constantState.newDrawable();
                    }
                    xk xkVar2 = this.b;
                    xkVar2.mutate();
                    this.b = xkVar2;
                    this.b.setCallback(callback);
                    this.b.setBounds(bVar.b.getBounds());
                    this.b.a(false);
                }
                ArrayList<Animator> arrayList = bVar.d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.d = new ArrayList<>(size);
                    this.e = new p4<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = bVar.d.get(i);
                        Animator clone = animator.clone();
                        String str = bVar.e.get(animator);
                        clone.setTarget(this.b.a(str));
                        this.d.add(clone);
                        this.e.put(clone, str);
                    }
                    a();
                }
            }
        }

        @DexIgnore
        public void a() {
            if (this.c == null) {
                this.c = new AnimatorSet();
            }
            this.c.playTogether(this.d);
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a;
        }

        @DexIgnore
        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    @DexIgnore
    public rk() {
        this((Context) null, (b) null, (Resources) null);
    }

    @DexIgnore
    public static rk a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        rk rkVar = new rk(context);
        rkVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return rkVar;
    }

    @DexIgnore
    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, theme);
        }
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.a(drawable);
        }
        return false;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.b.b.draw(canvas);
        if (this.b.c.isStarted()) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.c(drawable);
        }
        return this.b.b.getAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.b.a;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.d(drawable);
        }
        return this.b.b.getColorFilter();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.a;
        if (drawable == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new c(drawable.getConstantState());
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.b.b.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.b.b.getIntrinsicWidth();
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.b.b.getOpacity();
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray a2 = e7.a(resources, theme, attributeSet, pk.e);
                    int resourceId = a2.getResourceId(0, 0);
                    if (resourceId != 0) {
                        xk a3 = xk.a(resources, resourceId, theme);
                        a3.a(false);
                        a3.setCallback(this.e);
                        xk xkVar = this.b.b;
                        if (xkVar != null) {
                            xkVar.setCallback((Drawable.Callback) null);
                        }
                        this.b.b = a3;
                    }
                    a2.recycle();
                } else if (GoalTracking.COLUMN_TARGET.equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, pk.f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.c;
                        if (context != null) {
                            a(string, tk.a(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.b.a();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.f(drawable);
        }
        return this.b.b.isAutoMirrored();
    }

    @DexIgnore
    public boolean isRunning() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return ((AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.b.c.isRunning();
    }

    @DexIgnore
    public boolean isStateful() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.b.b.isStateful();
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.b.b.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.setLevel(i);
        }
        return this.b.b.setLevel(i);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.b.b.setState(iArr);
    }

    @DexIgnore
    public void setAlpha(int i) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setAlpha(i);
        } else {
            this.b.b.setAlpha(i);
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, z);
        } else {
            this.b.b.setAutoMirrored(z);
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.b.b.setColorFilter(colorFilter);
        }
    }

    @DexIgnore
    public void setTint(int i) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.b(drawable, i);
        } else {
            this.b.b.setTint(i);
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, colorStateList);
        } else {
            this.b.b.setTintList(colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, mode);
        } else {
            this.b.b.setTintMode(mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.b.b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        Drawable drawable = this.a;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (!this.b.c.isStarted()) {
            this.b.c.start();
            invalidateSelf();
        }
    }

    @DexIgnore
    public void stop() {
        Drawable drawable = this.a;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.b.c.end();
        }
    }

    @DexIgnore
    public rk(Context context) {
        this(context, (b) null, (Resources) null);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ Drawable.ConstantState a;

        @DexIgnore
        public c(Drawable.ConstantState constantState) {
            this.a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            rk rkVar = new rk();
            rkVar.a = this.a.newDrawable();
            rkVar.a.setCallback(rkVar.e);
            return rkVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            rk rkVar = new rk();
            rkVar.a = this.a.newDrawable(resources);
            rkVar.a.setCallback(rkVar.e);
            return rkVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            rk rkVar = new rk();
            rkVar.a = this.a.newDrawable(resources, theme);
            rkVar.a.setCallback(rkVar.e);
            return rkVar;
        }
    }

    @DexIgnore
    public rk(Context context, b bVar, Resources resources) {
        this.d = null;
        this.e = new a();
        this.c = context;
        if (bVar != null) {
            this.b = bVar;
        } else {
            this.b = new b(context, bVar, this.e, resources);
        }
    }

    @DexIgnore
    public final void a(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i = 0; i < childAnimations.size(); i++) {
                a(childAnimations.get(i));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.d == null) {
                    this.d = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.d);
            }
        }
    }

    @DexIgnore
    public final void a(String str, Animator animator) {
        animator.setTarget(this.b.b.a(str));
        if (Build.VERSION.SDK_INT < 21) {
            a(animator);
        }
        b bVar = this.b;
        if (bVar.d == null) {
            bVar.d = new ArrayList<>();
            this.b.e = new p4<>();
        }
        this.b.d.add(animator);
        this.b.e.put(animator, str);
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
    }
}
