package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.x1;
import com.fossil.y1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l1 implements x1 {
    @DexIgnore
    public Context a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public q1 c;
    @DexIgnore
    public LayoutInflater d;
    @DexIgnore
    public x1.a e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public y1 h;
    @DexIgnore
    public int i;

    @DexIgnore
    public l1(Context context, int i2, int i3) {
        this.a = context;
        this.d = LayoutInflater.from(context);
        this.f = i2;
        this.g = i3;
    }

    @DexIgnore
    public void a(Context context, q1 q1Var) {
        this.b = context;
        LayoutInflater.from(this.b);
        this.c = q1Var;
    }

    @DexIgnore
    public abstract void a(t1 t1Var, y1.a aVar);

    @DexIgnore
    public abstract boolean a(int i2, t1 t1Var);

    @DexIgnore
    public boolean a(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public y1 b(ViewGroup viewGroup) {
        if (this.h == null) {
            this.h = (y1) this.d.inflate(this.f, viewGroup, false);
            this.h.a(this.c);
            a(true);
        }
        return this.h;
    }

    @DexIgnore
    public boolean b(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public x1.a c() {
        return this.e;
    }

    @DexIgnore
    public int getId() {
        return this.i;
    }

    @DexIgnore
    public void a(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup != null) {
            q1 q1Var = this.c;
            int i2 = 0;
            if (q1Var != null) {
                q1Var.b();
                ArrayList<t1> n = this.c.n();
                int size = n.size();
                int i3 = 0;
                for (int i4 = 0; i4 < size; i4++) {
                    t1 t1Var = n.get(i4);
                    if (a(i3, t1Var)) {
                        View childAt = viewGroup.getChildAt(i3);
                        t1 itemData = childAt instanceof y1.a ? ((y1.a) childAt).getItemData() : null;
                        View a2 = a(t1Var, childAt, viewGroup);
                        if (t1Var != itemData) {
                            a2.setPressed(false);
                            a2.jumpDrawablesToCurrentState();
                        }
                        if (a2 != childAt) {
                            a(a2, i3);
                        }
                        i3++;
                    }
                }
                i2 = i3;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.h).addView(view, i2);
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    @DexIgnore
    public void a(x1.a aVar) {
        this.e = aVar;
    }

    @DexIgnore
    public y1.a a(ViewGroup viewGroup) {
        return (y1.a) this.d.inflate(this.g, viewGroup, false);
    }

    @DexIgnore
    public View a(t1 t1Var, View view, ViewGroup viewGroup) {
        y1.a aVar;
        if (view instanceof y1.a) {
            aVar = (y1.a) view;
        } else {
            aVar = a(viewGroup);
        }
        a(t1Var, aVar);
        return (View) aVar;
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z) {
        x1.a aVar = this.e;
        if (aVar != null) {
            aVar.a(q1Var, z);
        }
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        x1.a aVar = this.e;
        if (aVar != null) {
            return aVar.a(c2Var);
        }
        return false;
    }

    @DexIgnore
    public void a(int i2) {
        this.i = i2;
    }
}
