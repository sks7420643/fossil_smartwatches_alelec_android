package com.fossil;

import android.content.Context;
import com.fossil.d15;
import com.fossil.m24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1", f = "HomeAlertsPresenter.kt", l = {85}, m = "invokeSuspend")
public final class sx4$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter.e this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements m24.e<d15.a, m24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ sx4$e$a a;

        @DexIgnore
        public a(sx4$e$a sx4_e_a) {
            this.a = sx4_e_a;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r9v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r9v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r9v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public void onSuccess(d15.a aVar) {
            String str;
            wg6.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onSuccess");
            ArrayList<AppWrapper> arrayList = new ArrayList<>();
            arrayList.addAll(aVar.a());
            if (this.a.this$0.a.u.B()) {
                ArrayList arrayList2 = new ArrayList();
                for (AppWrapper appWrapper : arrayList) {
                    InstalledApp installedApp = appWrapper.getInstalledApp();
                    Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                    if (isSelected == null) {
                        wg6.a();
                        throw null;
                    } else if (!isSelected.booleanValue()) {
                        arrayList2.add(appWrapper);
                    }
                }
                this.a.this$0.a.i().clear();
                this.a.this$0.a.i().addAll(arrayList);
                if (!arrayList2.isEmpty()) {
                    this.a.this$0.a.a((List<vx4>) arrayList2);
                    return;
                }
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886334);
                ox4 o = this.a.this$0.a.l;
                wg6.a((Object) a2, "notificationAppOverView");
                o.h(a2);
                HomeAlertsPresenter homeAlertsPresenter = this.a.this$0.a;
                if (homeAlertsPresenter.a(homeAlertsPresenter.i(), (List<vx4>) arrayList)) {
                    this.a.this$0.a.k();
                    return;
                }
                return;
            }
            this.a.this$0.a.i().clear();
            int i = 0;
            for (AppWrapper appWrapper2 : arrayList) {
                InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                Boolean isSelected2 = installedApp2 != null ? installedApp2.isSelected() : null;
                if (isSelected2 == null) {
                    wg6.a();
                    throw null;
                } else if (isSelected2.booleanValue()) {
                    this.a.this$0.a.i().add(appWrapper2);
                    i++;
                }
            }
            if (i == aVar.a().size()) {
                str = jm4.a((Context) PortfolioApp.get.instance(), 2131886334);
            } else if (i == 0) {
                str = jm4.a((Context) PortfolioApp.get.instance(), 2131886753);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(i);
                sb.append('/');
                sb.append(arrayList.size());
                str = sb.toString();
            }
            ox4 o2 = this.a.this$0.a.l;
            wg6.a((Object) str, "notificationAppOverView");
            o2.h(str);
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wg6.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onError");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<List<? extends DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ sx4$e$a a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1", f = "HomeAlertsPresenter.kt", l = {166}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sx4$e$a$b$a$a")
            @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.sx4$e$a$b$a$a  reason: collision with other inner class name */
            public static final class C0038a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0038a(a aVar, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0038a aVar = new C0038a(this.this$0, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0038a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    ff6.a();
                    if (this.label == 0) {
                        nc6.a(obj);
                        this.this$0.this$0.a.this$0.a.v.getDNDScheduledTimeDao().insertListDNDScheduledTime(this.this$0.$lDndScheduledTimeModel);
                        return cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, List list, xe6 xe6) {
                super(2, xe6);
                this.this$0 = bVar;
                this.$lDndScheduledTimeModel = list;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$lDndScheduledTimeModel, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = ff6.a();
                int i = this.label;
                if (i == 0) {
                    nc6.a(obj);
                    il6 il6 = this.p$;
                    dl6 c = this.this$0.a.this$0.a.c();
                    C0038a aVar = new C0038a(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(c, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }

        @DexIgnore
        public b(sx4$e$a sx4_e_a) {
            this.a = sx4_e_a;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DNDScheduledTimeModel> list) {
            if (list == null || list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
                DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
                arrayList.add(dNDScheduledTimeModel);
                arrayList.add(dNDScheduledTimeModel2);
                rm6 unused = ik6.b(this.a.this$0.a.e(), (af6) null, (ll6) null, new a(this, arrayList, (xe6) null), 3, (Object) null);
                return;
            }
            for (DNDScheduledTimeModel dNDScheduledTimeModel3 : list) {
                if (dNDScheduledTimeModel3.getScheduledTimeType() == 0) {
                    this.a.this$0.a.l.b(this.a.this$0.a.b(dNDScheduledTimeModel3.getMinutes()));
                } else {
                    this.a.this$0.a.l.a(this.a.this$0.a.b(dNDScheduledTimeModel3.getMinutes()));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1", f = "HomeAlertsPresenter.kt", l = {86}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super List<Alarm>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sx4$e$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sx4$e$a sx4_e_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sx4_e_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                AlarmHelper d = this.this$0.this$0.a.n;
                this.L$0 = il6;
                this.label = 1;
                if (d.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!this.this$0.this$0.a.i) {
                this.this$0.this$0.a.i = true;
                AlarmHelper d2 = this.this$0.this$0.a.n;
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                d2.d(applicationContext);
            }
            return this.this$0.this$0.a.t.getAllAlarmIgnoreDeletePinType();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Integer.valueOf(((Alarm) t).getTotalMinutes()), Integer.valueOf(((Alarm) t2).getTotalMinutes()));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sx4$e$a(HomeAlertsPresenter.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        sx4$e$a sx4_e_a = new sx4$e$a(this.this$0, xe6);
        sx4_e_a.p$ = (il6) obj;
        return sx4_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((sx4$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v15, types: [com.fossil.d15, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v8, types: [com.fossil.sx4$e$a$a, com.portfolio.platform.CoroutineUseCase$e] */
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 c2 = this.this$0.a.c();
            c cVar = new c(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj2 = gk6.a(c2, cVar, this);
            if (obj2 == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            obj2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List<Alarm> list = (List) obj2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a3 = HomeAlertsPresenter.x.a();
        local.d(a3, "GetAlarms onSuccess: size = " + list.size());
        this.this$0.a.g.clear();
        for (Alarm copy$default : list) {
            this.this$0.a.g.add(Alarm.copy$default(copy$default, (String) null, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 4095, (Object) null));
        }
        ArrayList f = this.this$0.a.g;
        if (f.size() > 1) {
            ud6.a(f, new d());
        }
        this.this$0.a.l.c(this.this$0.a.g);
        this.this$0.a.o.a(null, new a(this));
        HomeAlertsPresenter homeAlertsPresenter = this.this$0.a;
        homeAlertsPresenter.h = homeAlertsPresenter.u.F();
        this.this$0.a.l.m(this.this$0.a.h);
        this.this$0.a.f.a(this.this$0.a.l, new b(this));
        return cd6.a;
    }
}
