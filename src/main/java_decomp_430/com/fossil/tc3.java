package com.fossil;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b {
        @DexIgnore
        public /* final */ CountDownLatch a;

        @DexIgnore
        public a() {
            this.a = new CountDownLatch(1);
        }

        @DexIgnore
        public final void a() throws InterruptedException {
            this.a.await();
        }

        @DexIgnore
        public final void onCanceled() {
            this.a.countDown();
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            this.a.countDown();
        }

        @DexIgnore
        public final void onSuccess(Object obj) {
            this.a.countDown();
        }

        @DexIgnore
        public final boolean a(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.a.await(j, timeUnit);
        }

        @DexIgnore
        public /* synthetic */ a(pd3 pd3) {
            this();
        }
    }

    @DexIgnore
    public interface b extends jc3, lc3, mc3<Object> {
    }

    @DexIgnore
    public static <TResult> qc3<TResult> a(TResult tresult) {
        od3 od3 = new od3();
        od3.a(tresult);
        return od3;
    }

    @DexIgnore
    public static <TResult> TResult b(qc3<TResult> qc3) throws ExecutionException {
        if (qc3.e()) {
            return qc3.b();
        }
        if (qc3.c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(qc3.a());
    }

    @DexIgnore
    public static <TResult> qc3<TResult> a(Exception exc) {
        od3 od3 = new od3();
        od3.a(exc);
        return od3;
    }

    @DexIgnore
    public static <TResult> qc3<TResult> a() {
        od3 od3 = new od3();
        od3.f();
        return od3;
    }

    @DexIgnore
    public static <TResult> qc3<TResult> a(Executor executor, Callable<TResult> callable) {
        w12.a(executor, (Object) "Executor must not be null");
        w12.a(callable, (Object) "Callback must not be null");
        od3 od3 = new od3();
        executor.execute(new pd3(od3, callable));
        return od3;
    }

    @DexIgnore
    public static <TResult> TResult a(qc3<TResult> qc3) throws ExecutionException, InterruptedException {
        w12.a();
        w12.a(qc3, (Object) "Task must not be null");
        if (qc3.d()) {
            return b(qc3);
        }
        a aVar = new a((pd3) null);
        a((qc3<?>) qc3, (b) aVar);
        aVar.a();
        return b(qc3);
    }

    @DexIgnore
    public static <TResult> TResult a(qc3<TResult> qc3, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        w12.a();
        w12.a(qc3, (Object) "Task must not be null");
        w12.a(timeUnit, (Object) "TimeUnit must not be null");
        if (qc3.d()) {
            return b(qc3);
        }
        a aVar = new a((pd3) null);
        a((qc3<?>) qc3, (b) aVar);
        if (aVar.a(j, timeUnit)) {
            return b(qc3);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @DexIgnore
    public static void a(qc3<?> qc3, b bVar) {
        qc3.a(sc3.b, (mc3<? super Object>) bVar);
        qc3.a(sc3.b, (lc3) bVar);
        qc3.a(sc3.b, (jc3) bVar);
    }
}
