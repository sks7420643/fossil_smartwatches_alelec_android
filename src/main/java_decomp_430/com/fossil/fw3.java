package com.fossil;

import com.facebook.stetho.inspector.protocol.module.Database;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw3 extends lw3 {
    @DexIgnore
    public static volatile fw3[] x;
    @DexIgnore
    public hw3 a;
    @DexIgnore
    public hw3 b;
    @DexIgnore
    public hw3 c;
    @DexIgnore
    public hw3 d;
    @DexIgnore
    public hw3 e;
    @DexIgnore
    public hw3 f;
    @DexIgnore
    public hw3 g;
    @DexIgnore
    public hw3 h;
    @DexIgnore
    public hw3 i;
    @DexIgnore
    public hw3 j;
    @DexIgnore
    public hw3 k;
    @DexIgnore
    public hw3 l;
    @DexIgnore
    public hw3 m;
    @DexIgnore
    public hw3 n;
    @DexIgnore
    public hw3 o;
    @DexIgnore
    public hw3 p;
    @DexIgnore
    public int q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public ew3[] v;
    @DexIgnore
    public ew3[] w;

    @DexIgnore
    public fw3() {
        a();
    }

    @DexIgnore
    public static fw3[] b() {
        if (x == null) {
            synchronized (jw3.a) {
                if (x == null) {
                    x = new fw3[0];
                }
            }
        }
        return x;
    }

    @DexIgnore
    public fw3 a() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = 0;
        this.r = "";
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = ew3.b();
        this.w = ew3.b();
        return this;
    }

    @DexIgnore
    public fw3 a(iw3 iw3) throws IOException {
        while (true) {
            int j2 = iw3.j();
            switch (j2) {
                case 0:
                    return this;
                case 10:
                    if (this.a == null) {
                        this.a = new hw3();
                    }
                    iw3.a((lw3) this.a);
                    break;
                case 18:
                    if (this.b == null) {
                        this.b = new hw3();
                    }
                    iw3.a((lw3) this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new hw3();
                    }
                    iw3.a((lw3) this.c);
                    break;
                case 34:
                    if (this.d == null) {
                        this.d = new hw3();
                    }
                    iw3.a((lw3) this.d);
                    break;
                case 42:
                    if (this.e == null) {
                        this.e = new hw3();
                    }
                    iw3.a((lw3) this.e);
                    break;
                case 50:
                    if (this.f == null) {
                        this.f = new hw3();
                    }
                    iw3.a((lw3) this.f);
                    break;
                case 58:
                    if (this.g == null) {
                        this.g = new hw3();
                    }
                    iw3.a((lw3) this.g);
                    break;
                case 66:
                    if (this.h == null) {
                        this.h = new hw3();
                    }
                    iw3.a((lw3) this.h);
                    break;
                case 74:
                    iw3.i();
                    break;
                case 80:
                    this.q = iw3.d();
                    break;
                case 90:
                    this.r = iw3.i();
                    break;
                case 98:
                    iw3.i();
                    break;
                case 106:
                    this.s = iw3.i();
                    break;
                case 122:
                    this.t = iw3.i();
                    break;
                case 130:
                    this.u = iw3.i();
                    break;
                case 138:
                    iw3.i();
                    break;
                case 144:
                    iw3.c();
                    break;
                case 154:
                    int a2 = nw3.a(iw3, 154);
                    ew3[] ew3Arr = this.v;
                    int length = ew3Arr == null ? 0 : ew3Arr.length;
                    ew3[] ew3Arr2 = new ew3[(a2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.v, 0, ew3Arr2, 0, length);
                    }
                    while (length < ew3Arr2.length - 1) {
                        ew3Arr2[length] = new ew3();
                        iw3.a((lw3) ew3Arr2[length]);
                        iw3.j();
                        length++;
                    }
                    ew3Arr2[length] = new ew3();
                    iw3.a((lw3) ew3Arr2[length]);
                    this.v = ew3Arr2;
                    break;
                case 162:
                    int a3 = nw3.a(iw3, 162);
                    ew3[] ew3Arr3 = this.w;
                    int length2 = ew3Arr3 == null ? 0 : ew3Arr3.length;
                    ew3[] ew3Arr4 = new ew3[(a3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.w, 0, ew3Arr4, 0, length2);
                    }
                    while (length2 < ew3Arr4.length - 1) {
                        ew3Arr4[length2] = new ew3();
                        iw3.a((lw3) ew3Arr4[length2]);
                        iw3.j();
                        length2++;
                    }
                    ew3Arr4[length2] = new ew3();
                    iw3.a((lw3) ew3Arr4[length2]);
                    this.w = ew3Arr4;
                    break;
                case 170:
                    if (this.i == null) {
                        this.i = new hw3();
                    }
                    iw3.a((lw3) this.i);
                    break;
                case 176:
                    iw3.c();
                    break;
                case 186:
                    iw3.i();
                    break;
                case 194:
                    if (this.p == null) {
                        this.p = new hw3();
                    }
                    iw3.a((lw3) this.p);
                    break;
                case 202:
                    if (this.j == null) {
                        this.j = new hw3();
                    }
                    iw3.a((lw3) this.j);
                    break;
                case 208:
                    iw3.c();
                    break;
                case 218:
                    if (this.k == null) {
                        this.k = new hw3();
                    }
                    iw3.a((lw3) this.k);
                    break;
                case 226:
                    if (this.l == null) {
                        this.l = new hw3();
                    }
                    iw3.a((lw3) this.l);
                    break;
                case 234:
                    if (this.m == null) {
                        this.m = new hw3();
                    }
                    iw3.a((lw3) this.m);
                    break;
                case 242:
                    if (this.n == null) {
                        this.n = new hw3();
                    }
                    iw3.a((lw3) this.n);
                    break;
                case Database.MAX_EXECUTE_RESULTS:
                    if (this.o == null) {
                        this.o = new hw3();
                    }
                    iw3.a((lw3) this.o);
                    break;
                case 256:
                    iw3.c();
                    break;
                default:
                    if (nw3.b(iw3, j2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
