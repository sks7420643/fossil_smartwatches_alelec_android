package com.fossil;

import com.fossil.fx6;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy6 extends fx6.a {
    @DexIgnore
    public static iy6 a() {
        return new iy6();
    }

    @DexIgnore
    public fx6<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (type == String.class || type == Boolean.TYPE || type == Boolean.class || type == Byte.TYPE || type == Byte.class || type == Character.TYPE || type == Character.class || type == Double.TYPE || type == Double.class || type == Float.TYPE || type == Float.class || type == Integer.TYPE || type == Integer.class || type == Long.TYPE || type == Long.class || type == Short.TYPE || type == Short.class) {
            return yx6.a;
        }
        return null;
    }

    @DexIgnore
    public fx6<zq6, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == String.class) {
            return hy6.a;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return zx6.a;
        }
        if (type == Byte.class || type == Byte.TYPE) {
            return ay6.a;
        }
        if (type == Character.class || type == Character.TYPE) {
            return by6.a;
        }
        if (type == Double.class || type == Double.TYPE) {
            return cy6.a;
        }
        if (type == Float.class || type == Float.TYPE) {
            return dy6.a;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return ey6.a;
        }
        if (type == Long.class || type == Long.TYPE) {
            return fy6.a;
        }
        if (type == Short.class || type == Short.TYPE) {
            return gy6.a;
        }
        return null;
    }
}
