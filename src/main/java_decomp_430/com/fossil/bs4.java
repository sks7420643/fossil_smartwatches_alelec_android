package com.fossil;

import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs4 implements CoroutineUseCase.b {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public bs4(int i, String str, boolean z) {
        wg6.b(str, "serial");
        this.a = i;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }
}
