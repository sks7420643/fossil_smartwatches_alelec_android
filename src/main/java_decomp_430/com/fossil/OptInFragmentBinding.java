package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class OptInFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleSwitchCompat q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat u;

    @DexIgnore
    public OptInFragmentBinding(Object obj, View view, int i, FlexibleSwitchCompat flexibleSwitchCompat, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, ConstraintLayout constraintLayout4, FlexibleSwitchCompat flexibleSwitchCompat2, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = flexibleSwitchCompat;
        this.r = flexibleTextView;
        this.s = rTLImageView;
        this.t = constraintLayout4;
        this.u = flexibleSwitchCompat2;
    }
}
