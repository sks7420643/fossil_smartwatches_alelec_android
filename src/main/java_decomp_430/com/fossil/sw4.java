package com.fossil;

import com.portfolio.platform.data.InAppNotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sw4 extends j24 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void b(InAppNotification inAppNotification);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract int h();

    @DexIgnore
    public abstract boolean i();

    @DexIgnore
    public abstract void j();
}
