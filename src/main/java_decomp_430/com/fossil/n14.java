package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n14 implements Factory<FirmwareFileRepository> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public n14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static n14 a(b14 b14) {
        return new n14(b14);
    }

    @DexIgnore
    public static FirmwareFileRepository b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static FirmwareFileRepository c(b14 b14) {
        FirmwareFileRepository h = b14.h();
        z76.a(h, "Cannot return null from a non-@Nullable @Provides method");
        return h;
    }

    @DexIgnore
    public FirmwareFileRepository get() {
        return b(this.a);
    }
}
