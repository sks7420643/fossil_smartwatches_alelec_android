package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.rv1.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy1<O extends rv1.d> extends kx1 {
    @DexIgnore
    public /* final */ vv1<O> c;

    @DexIgnore
    public sy1(vv1<O> vv1) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = vv1;
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T a(T t) {
        this.c.a(t);
        return t;
    }

    @DexIgnore
    public final void a(gz1 gz1) {
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T b(T t) {
        this.c.b(t);
        return t;
    }

    @DexIgnore
    public final Context e() {
        return this.c.f();
    }

    @DexIgnore
    public final Looper f() {
        return this.c.h();
    }
}
