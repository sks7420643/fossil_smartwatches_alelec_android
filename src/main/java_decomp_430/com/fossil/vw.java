package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vw {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements vw {
        @DexIgnore
        public /* final */ ms a;
        @DexIgnore
        public /* final */ xt b;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> c;

        @DexIgnore
        public a(InputStream inputStream, List<ImageHeaderParser> list, xt xtVar) {
            q00.a(xtVar);
            this.b = xtVar;
            q00.a(list);
            this.c = list;
            this.a = new ms(inputStream, xtVar);
        }

        @DexIgnore
        public Bitmap a(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeStream(this.a.b(), (Rect) null, options);
        }

        @DexIgnore
        public void b() {
            this.a.c();
        }

        @DexIgnore
        public ImageHeaderParser.ImageType c() throws IOException {
            return ur.b(this.c, this.a.b(), this.b);
        }

        @DexIgnore
        public int a() throws IOException {
            return ur.a(this.c, this.a.b(), this.b);
        }
    }

    @DexIgnore
    int a() throws IOException;

    @DexIgnore
    Bitmap a(BitmapFactory.Options options) throws IOException;

    @DexIgnore
    void b();

    @DexIgnore
    ImageHeaderParser.ImageType c() throws IOException;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements vw {
        @DexIgnore
        public /* final */ xt a;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> b;
        @DexIgnore
        public /* final */ os c;

        @DexIgnore
        public b(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, xt xtVar) {
            q00.a(xtVar);
            this.a = xtVar;
            q00.a(list);
            this.b = list;
            this.c = new os(parcelFileDescriptor);
        }

        @DexIgnore
        public Bitmap a(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.c.b().getFileDescriptor(), (Rect) null, options);
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public ImageHeaderParser.ImageType c() throws IOException {
            return ur.b(this.b, this.c, this.a);
        }

        @DexIgnore
        public int a() throws IOException {
            return ur.a(this.b, this.c, this.a);
        }
    }
}
