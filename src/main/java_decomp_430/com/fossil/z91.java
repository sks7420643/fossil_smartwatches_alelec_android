package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z91 extends xg6 implements ig6<byte[], rg1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ub1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z91(ub1 ub1) {
        super(2);
        this.a = ub1;
    }

    @DexIgnore
    public Object invoke(Object obj, Object obj2) {
        byte[] bArr = (byte[]) obj;
        rg1 rg1 = (rg1) obj2;
        ig6<? super byte[], ? super rg1, cd6> ig6 = this.a.C;
        if (ig6 != null) {
            cd6 cd6 = (cd6) ig6.invoke(bArr, rg1);
        }
        return cd6.a;
    }
}
