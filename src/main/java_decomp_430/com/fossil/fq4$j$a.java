package com.fossil;

import android.media.session.MediaController;
import com.fossil.fq4;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$j$a extends xg6 implements hg6<fq4.b, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ MediaController $activeMediaController;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$j$a(MediaController mediaController) {
        super(1);
        this.$activeMediaController = mediaController;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((MusicControlComponent.b) obj));
    }

    @DexIgnore
    public final boolean invoke(MusicControlComponent.b bVar) {
        wg6.b(bVar, "musicController");
        String c = bVar.c();
        MediaController mediaController = this.$activeMediaController;
        wg6.a((Object) mediaController, "activeMediaController");
        return wg6.a((Object) c, (Object) mediaController.getPackageName());
    }
}
