package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o91 {
    @DexIgnore
    public static /* final */ HashMap<lc6<String, xh0>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ o91 b; // = new o91();

    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return r12;
     */
    @DexIgnore
    public final em0 a(String str, byte[] bArr) {
        em0 cb1;
        em0 zg0;
        if (bArr.length < 3) {
            oa1 oa1 = oa1.a;
            return null;
        }
        kl0 a2 = kl0.d.a(bArr[0]);
        xh0 a3 = xh0.o.a(bArr[1]);
        byte b2 = (byte) (bArr[2] & -1);
        if (a2 == null) {
            oa1 oa12 = oa1.a;
        }
        if (a3 == null) {
            oa1 oa13 = oa1.a;
            return null;
        }
        Byte b3 = a.get(new lc6(str, a3));
        byte byteValue = b3 != null ? b3.byteValue() : -1;
        byte b4 = (byte) ((byteValue + 1) % (cw0.b((byte) -1) + 1));
        a.put(new lc6(str, a3), Byte.valueOf(b2));
        if (byteValue == b2) {
            oa1 oa14 = oa1.a;
            Object[] objArr = {Byte.valueOf(b2), Byte.valueOf(byteValue)};
        } else if (b2 != b4) {
            oa1 oa15 = oa1.a;
            Object[] objArr2 = {Byte.valueOf(b2), Byte.valueOf(b4)};
        }
        byte[] a4 = md6.a(bArr, 3, bArr.length);
        switch (q71.a[a3.ordinal()]) {
            case 1:
                try {
                    Charset forName = Charset.forName("UTF-8");
                    wg6.a(forName, "Charset.forName(\"UTF-8\")");
                    JSONObject jSONObject = new JSONObject(new String(a4, forName)).getJSONObject("req");
                    int i = jSONObject.getInt("id");
                    wg6.a(jSONObject, "requestJSON");
                    cb1 = new cb1(b2, i, jSONObject);
                    break;
                } catch (JSONException e) {
                    qs0.h.a(e);
                    return null;
                }
            case 2:
                return new i71(b2);
            case 3:
                return null;
            case 4:
                try {
                    int i2 = ByteBuffer.wrap(a4).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                    t70 a5 = t70.c.a(a4[4]);
                    if (a5 != null) {
                        int i3 = q71.b[a5.ordinal()];
                        if (i3 != 1) {
                            if (i3 != 2) {
                                if (i3 == 3) {
                                    zg0 = new zg0(b2, i2, new u70());
                                    break;
                                } else if (i3 == 4) {
                                    byte b5 = a4[5];
                                    ByteBuffer order = ByteBuffer.wrap(a4, 6, 4).order(ByteOrder.LITTLE_ENDIAN);
                                    wg6.a(order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
                                    return new zg0(b2, i2, new w70(b5, order.getInt()));
                                } else {
                                    throw new kc6();
                                }
                            } else {
                                zg0 = new zg0(b2, i2, new v70());
                                break;
                            }
                        } else {
                            zg0 = new zg0(b2, i2, new q70());
                            break;
                        }
                    } else {
                        return null;
                    }
                } catch (Exception e2) {
                    qs0.h.a(e2);
                    return null;
                }
            case 5:
                try {
                    l70 a6 = l70.c.a(a4[0]);
                    if (a6 != null) {
                        return new ji1(b2, a6);
                    }
                    return null;
                } catch (IllegalArgumentException e3) {
                    qs0.h.a(e3);
                    return null;
                }
            case 6:
                try {
                    zg0 = zs0.CREATOR.a(b2, a4);
                    break;
                } catch (IllegalArgumentException e4) {
                    qs0.h.a(e4);
                    return null;
                }
            case 7:
                return new wl1(b2);
            case 8:
                try {
                    zg0 = se1.CREATOR.a(b2, a4);
                    break;
                } catch (IllegalArgumentException e5) {
                    qs0.h.a(e5);
                    return null;
                }
            case 9:
                return new si0(b2);
            case 10:
                return new op0(b2);
            case 11:
                try {
                    bd0 a7 = bd0.c.a(a4[0]);
                    if (a7 != null) {
                        cb1 = new lw0(b2, a7, a4[1]);
                        break;
                    } else {
                        return null;
                    }
                } catch (Exception e6) {
                    qs0.h.a(e6);
                    return null;
                }
            case 12:
                return a(b2, a4);
            default:
                throw new kc6();
        }
    }

    @DexIgnore
    public final p31 a(byte b2, byte[] bArr) {
        try {
            fd0 a2 = fd0.c.a(bArr[0]);
            ed0 a3 = ed0.c.a(bArr[1]);
            gd0 a4 = gd0.c.a(bArr[2]);
            byte[] a5 = md6.a(bArr, 3, bArr.length);
            if (!(a2 == null || a3 == null || a4 == null)) {
                return new p31(b2, a2, a3, a4, a5);
            }
        } catch (Exception e) {
            qs0.h.a(e);
        }
        return null;
    }

    @DexIgnore
    public final void a(String str) {
        synchronized (a) {
            Set<Map.Entry<lc6<String, xh0>, Byte>> entrySet = a.entrySet();
            wg6.a(entrySet, "eventSequenceManager.entries");
            for (Map.Entry entry : entrySet) {
                if (wg6.a(str, (String) ((lc6) entry.getKey()).getFirst())) {
                    HashMap<lc6<String, xh0>, Byte> hashMap = a;
                    Object key = entry.getKey();
                    wg6.a(key, "entry.key");
                    hashMap.put(key, (byte) -1);
                }
            }
            cd6 cd6 = cd6.a;
        }
    }
}
