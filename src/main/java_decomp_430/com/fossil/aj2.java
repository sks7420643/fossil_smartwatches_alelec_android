package com.fossil;

import com.fossil.fn2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj2 extends fn2<aj2, a> implements to2 {
    @DexIgnore
    public static /* final */ aj2 zzh;
    @DexIgnore
    public static volatile yo2<aj2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public nn2<String> zzg; // = fn2.m();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<aj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(aj2.zzh);
        }

        @DexIgnore
        public /* synthetic */ a(bj2 bj2) {
            this();
        }
    }

    @DexIgnore
    public enum b implements kn2 {
        UNKNOWN_MATCH_TYPE(0),
        REGEXP(1),
        BEGINS_WITH(2),
        ENDS_WITH(3),
        PARTIAL(4),
        EXACT(5),
        IN_LIST(6);
        
        @DexIgnore
        public /* final */ int zzi;

        /*
        static {
            new ej2();
        }
        */

        @DexIgnore
        public b(int i) {
            this.zzi = i;
        }

        @DexIgnore
        public static mn2 zzb() {
            return ij2.a;
        }

        @DexIgnore
        public final String toString() {
            return "<" + b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        @DexIgnore
        public final int zza() {
            return this.zzi;
        }

        @DexIgnore
        public static b zza(int i) {
            switch (i) {
                case 0:
                    return UNKNOWN_MATCH_TYPE;
                case 1:
                    return REGEXP;
                case 2:
                    return BEGINS_WITH;
                case 3:
                    return ENDS_WITH;
                case 4:
                    return PARTIAL;
                case 5:
                    return EXACT;
                case 6:
                    return IN_LIST;
                default:
                    return null;
            }
        }
    }

    /*
    static {
        aj2 aj2 = new aj2();
        zzh = aj2;
        fn2.a(aj2.class, aj2);
    }
    */

    @DexIgnore
    public static aj2 w() {
        return zzh;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (bj2.a[i - 1]) {
            case 1:
                return new aj2();
            case 2:
                return new a((bj2) null);
            case 3:
                return fn2.a((ro2) zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\f\u0000\u0002\b\u0001\u0003\u0007\u0002\u0004\u001a", new Object[]{"zzc", "zzd", b.zzb(), "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                yo2<aj2> yo2 = zzi;
                if (yo2 == null) {
                    synchronized (aj2.class) {
                        yo2 = zzi;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzh);
                            zzi = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final b o() {
        b zza = b.zza(this.zzd);
        return zza == null ? b.UNKNOWN_MATCH_TYPE : zza;
    }

    @DexIgnore
    public final boolean p() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String q() {
        return this.zze;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean s() {
        return this.zzf;
    }

    @DexIgnore
    public final List<String> t() {
        return this.zzg;
    }

    @DexIgnore
    public final int v() {
        return this.zzg.size();
    }
}
