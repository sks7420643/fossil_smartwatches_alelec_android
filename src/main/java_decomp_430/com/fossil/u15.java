package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u15 implements MembersInjector<NotificationDialLandingActivity> {
    @DexIgnore
    public static void a(NotificationDialLandingActivity notificationDialLandingActivity, NotificationDialLandingPresenter notificationDialLandingPresenter) {
        notificationDialLandingActivity.B = notificationDialLandingPresenter;
    }
}
