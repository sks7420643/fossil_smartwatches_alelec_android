package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j63 implements Callable<List<ab3>> {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ d63 d;

    @DexIgnore
    public j63(d63 d63, String str, String str2, String str3) {
        this.d = d63;
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.d.a.t();
        return this.d.a.l().b(this.a, this.b, this.c);
    }
}
