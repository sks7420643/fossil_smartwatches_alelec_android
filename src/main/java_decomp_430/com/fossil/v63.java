package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v63 {
    @DexIgnore
    u53 a();

    @DexIgnore
    t43 b();

    @DexIgnore
    Context c();

    @DexIgnore
    bb3 d();

    @DexIgnore
    k42 zzm();
}
