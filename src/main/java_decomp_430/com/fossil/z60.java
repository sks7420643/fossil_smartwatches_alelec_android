package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ long e; // = 4294967295L;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<z60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final z60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new z60(cw0.b(order.getInt(0)), order.getShort(4), order.getShort(6));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        public z60 createFromParcel(Parcel parcel) {
            return new z60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new z60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m74createFromParcel(Parcel parcel) {
            return new z60(parcel, (qg6) null);
        }
    }

    /*
    static {
        vg6 vg6 = vg6.a;
    }
    */

    @DexIgnore
    public z60(long j, short s, short s2) throws IllegalArgumentException {
        super(s60.TIME);
        this.b = j;
        this.c = s;
        this.d = s2;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.b).putShort(this.c).putShort(this.d).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        long j = e;
        long j2 = this.b;
        boolean z = true;
        if (0 <= j2 && j >= j2) {
            short s = this.c;
            if (s >= 0 && 1000 >= s) {
                short s2 = this.d;
                if (Short.MIN_VALUE > s2 || Short.MAX_VALUE < s2) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(ze0.a(ze0.b("timezoneOffsetInMinute("), this.d, ") is out of range ", "[-32768, 32767]."));
                }
                return;
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("millisecond("), this.c, ") is out of range ", "[0, 1000]."));
        }
        StringBuilder b2 = ze0.b("second(");
        b2.append(this.b);
        b2.append(") is out of range ");
        b2.append("[0, ");
        b2.append(e);
        b2.append("].");
        throw new IllegalArgumentException(b2.toString());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(z60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            z60 z60 = (z60) obj;
            return this.b == z60.b && this.c == z60.c && this.d == z60.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.TimeConfig");
    }

    @DexIgnore
    public final short getMillisecond() {
        return this.c;
    }

    @DexIgnore
    public final long getSecond() {
        return this.b;
    }

    @DexIgnore
    public final short getTimezoneOffsetInMinute() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return ((((Long.valueOf(this.b).hashCode() + (super.hashCode() * 31)) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.c));
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.d));
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("second", this.b);
            jSONObject.put("millisecond", Short.valueOf(this.c));
            jSONObject.put("timezone_offset_in_minute", Short.valueOf(this.d));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ z60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readLong();
        this.c = (short) parcel.readInt();
        this.d = (short) parcel.readInt();
        e();
    }
}
