package com.fossil;

import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.manager.WeatherManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en4$f$a extends sf6 implements ig6<il6, xe6<? super lc6<? extends Weather, ? extends Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ double $currentLat;
    @DexIgnore
    public /* final */ /* synthetic */ double $currentLong;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public en4$f$a(WeatherManager.f fVar, double d, double d2, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$currentLat = d;
        this.$currentLong = d2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        en4$f$a en4_f_a = new en4$f$a(this.this$0, this.$currentLat, this.$currentLong, xe6);
        en4_f_a.p$ = (il6) obj;
        return en4_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((en4$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            WeatherManager.f fVar = this.this$0;
            WeatherManager weatherManager = fVar.this$0;
            double d = this.$currentLat;
            double d2 = this.$currentLong;
            String str = fVar.$tempUnit$inlined;
            this.L$0 = il6;
            this.label = 1;
            obj = weatherManager.a(d, d2, str, (xe6<? super lc6<Weather, Boolean>>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
