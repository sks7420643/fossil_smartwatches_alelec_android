package com.fossil;

import android.content.Context;
import java.io.File;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i86<Result> implements Comparable<i86> {
    @DexIgnore
    public c86 a;
    @DexIgnore
    public h86<Result> b; // = new h86<>(this);
    @DexIgnore
    public Context c;
    @DexIgnore
    public f86<Result> d;
    @DexIgnore
    public j96 e;
    @DexIgnore
    public /* final */ t96 f; // = ((t96) getClass().getAnnotation(t96.class));

    @DexIgnore
    public void a(Context context, c86 c86, f86<Result> f86, j96 j96) {
        this.a = c86;
        this.c = new d86(context, h(), i());
        this.d = f86;
        this.e = j96;
    }

    @DexIgnore
    public void a(Result result) {
    }

    @DexIgnore
    public void b(Result result) {
    }

    @DexIgnore
    public boolean b(i86 i86) {
        if (k()) {
            for (Class isAssignableFrom : this.f.value()) {
                if (isAssignableFrom.isAssignableFrom(i86.getClass())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Result c();

    @DexIgnore
    public Context d() {
        return this.c;
    }

    @DexIgnore
    public Collection<ba6> e() {
        return this.b.c();
    }

    @DexIgnore
    public c86 f() {
        return this.a;
    }

    @DexIgnore
    public j96 g() {
        return this.e;
    }

    @DexIgnore
    public abstract String h();

    @DexIgnore
    public String i() {
        return ".Fabric" + File.separator + h();
    }

    @DexIgnore
    public abstract String j();

    @DexIgnore
    public boolean k() {
        return this.f != null;
    }

    @DexIgnore
    public final void l() {
        this.b.a(this.a.b(), null);
    }

    @DexIgnore
    public boolean m() {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(i86 i86) {
        if (b(i86)) {
            return 1;
        }
        if (i86.b(this)) {
            return -1;
        }
        if (k() && !i86.k()) {
            return 1;
        }
        if (k() || !i86.k()) {
            return 0;
        }
        return -1;
    }
}
