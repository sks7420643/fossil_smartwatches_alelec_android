package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in6<T> extends xm6<ym6> {
    @DexIgnore
    public /* final */ mk6<T> e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public in6(ym6 ym6, mk6<? super T> mk6) {
        super(ym6);
        wg6.b(ym6, "job");
        wg6.b(mk6, "continuation");
        this.e = mk6;
    }

    @DexIgnore
    public void b(Throwable th) {
        Object d = ((ym6) this.d).d();
        if (nl6.a() && !(!(d instanceof mm6))) {
            throw new AssertionError();
        } else if (d instanceof vk6) {
            this.e.a(((vk6) d).a, 0);
        } else {
            mk6<T> mk6 = this.e;
            Object b = zm6.b(d);
            mc6.a aVar = mc6.Companion;
            mk6.resumeWith(mc6.m1constructorimpl(b));
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "ResumeAwaitOnCompletion[" + this.e + ']';
    }
}
